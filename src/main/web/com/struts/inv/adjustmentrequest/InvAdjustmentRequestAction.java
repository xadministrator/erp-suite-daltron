package com.struts.inv.adjustmentrequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.txn.InvAdjustmentRequestController;
import com.ejb.txn.InvAdjustmentRequestControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.inv.adjustmententry.InvAdjustmentEntryTagList;
import com.struts.inv.adjustmentrequest.InvAdjustmentRequestForm;
import com.struts.inv.adjustmentrequest.InvAdjustmentRequestList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvAdjustmentDetails;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvAdjustmentRequestAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      InvAdjustmentRequestForm actionForm = (InvAdjustmentRequestForm)form;
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvAdjustmentRequestAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         // reset report
         
         actionForm.setReport(null);
         actionForm.setAttachment(null);
         String frParam = Common.getUserPermission(user, Constants.INV_ADJUSTMENT_REQUEST_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invAdjustmentRequest"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invAdjustmentRequestController EJB
*******************************************************/

         InvAdjustmentRequestControllerHome homeADJ = null;
         InvAdjustmentRequestController ejbADJ = null;
         LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
         
          InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;
         try {
            
            homeADJ = (InvAdjustmentRequestControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvAdjustmentRequestControllerEJB", InvAdjustmentRequestControllerHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
            
               homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvAdjustmentRequestAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbADJ = homeADJ.create();
            
            ejbII = homeII.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvAdjustmentRequestAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call InvAdjustmentRequestController EJB
   getGlFcPrecisionUnit
*******************************************************/
         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         short precisionUnit = 0;
         short costPrecisionUnit = 0;
         short quantityPrecisionUnit = 0;
         short adjustmentLineNumber = 0;
         boolean isInitialPrinting = false;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/inv/AdjustmentRequest/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
		 
         try {
         	
            precisionUnit = ejbADJ.getGlFcPrecisionUnit(user.getCmpCode());
        	 costPrecisionUnit = ejbADJ.getInvGpCostPrecisionUnit(user.getCmpCode());
            quantityPrecisionUnit = ejbADJ.getInvGpQuantityPrecisionUnit(user.getCmpCode());
        	 //quantityPrecisionUnit =(short)(10);
            if (isAccessedByDevice(request.getParameter("user-agent"))) adjustmentLineNumber=5;
            else adjustmentLineNumber = ejbADJ.getInvGpInventoryLineNumber(user.getCmpCode());        
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         


/*******************************************************
   -- Inv ADJ Save & Submit Mobile Action --
*******************************************************/
         
         if (request.getParameter("saveSubmitButton2") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvAdjustmentDetails details = new InvAdjustmentDetails();
           
           details.setAdjCode(actionForm.getAdjustmentCode());
           details.setAdjDocumentNumber("");
           details.setAdjReferenceNumber("");
           details.setAdjDescription(actionForm.getDescription());
           details.setAdjDate(new java.util.Date());
           details.setAdjType("REQUEST");
           details.setAdjCreatedBy(actionForm.getCreatedBy());
           details.setAdjDateCreated(new java.util.Date());
           details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
           details.setAdjDateLastModified(new java.util.Date());
           details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
           details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
           
           if (actionForm.getAdjustmentCode() == null) {
               
               details.setAdjCreatedBy(user.getUserName());
               details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
               
           }
           
           details.setAdjLastModifiedBy(user.getUserName());
           details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
           
           ArrayList alList = new ArrayList(); 
        
           for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
           	
           	   InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);           	   
           	              	             	   
	           if (Common.validateRequired(invADJList.getLocation()) &&
	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
           	   
           	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
           	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
           	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
           	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
           	   mdetails.setAlUomName(invADJList.getUnit());
           	   mdetails.setAlLocName(invADJList.getLocation());
           	   mdetails.setAlIiName(invADJList.getItemName());
           	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
           	   mdetails.setAlMisc(invADJList.getMisc());
           	   
           	   alList.add(mdetails);
           	
           }                     	
	   	   
           try {
           	
           	    ejbADJ.saveInvAdjEntryMobile(details, actionForm.getAdjustmentAccount(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
           	
           } catch (GlobalAccountNumberInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
			
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalExpiryDateNotFoundException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
        	   
           }catch (GlobalMiscInfoIsRequiredException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
        	   
           }catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

           }catch (EJBException ex) {
        	   if (log.isInfoEnabled()) {

        		   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
        				   " session: " + session.getId());
        	   }
               
               return(mapping.findForward("cmnErrorPage"));
           }
       	   
         }
         
         
/*******************************************************
   -- Inv ADJ Save As Draft Action --
*******************************************************/
         
         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvAdjustmentDetails details = new InvAdjustmentDetails();
           
           details.setAdjCode(actionForm.getAdjustmentCode());
           details.setAdjDocumentNumber(actionForm.getDocumentNumber());
           details.setAdjReferenceNumber("");
           details.setAdjDescription(actionForm.getDescription());
           details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setAdjType("REQUEST");
           details.setAdjCreatedBy(actionForm.getCreatedBy());
           details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
           details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
           details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
           details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
           details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
           
           if (actionForm.getAdjustmentCode() == null) {
               
               details.setAdjCreatedBy(user.getUserName());
               details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
               
           }
           
           details.setAdjLastModifiedBy(user.getUserName());
           details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
           
           ArrayList alList = new ArrayList(); 
        
           for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
           	
           	   InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);           	   
           	              	             	   
	           if (Common.validateRequired(invADJList.getLocation()) &&
	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
           	   
           	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
           	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
           	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
           	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
           	   mdetails.setAlUomName(invADJList.getUnit());
           	   mdetails.setAlLocName(invADJList.getLocation());
           	   mdetails.setAlIiName(invADJList.getItemName());
           	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
           	   mdetails.setAlMisc(invADJList.getMisc());
           	   System.out.println("invADJList.getMisc() : " + invADJList.getMisc());
           	   
           	   
           	   
           	   alList.add(mdetails);
           	
           }                     	
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
           try {
           	
           	    ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
           	
           } catch (GlobalAccountNumberInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
			
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalExpiryDateNotFoundException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
        	   
           }catch (GlobalMiscInfoIsRequiredException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
        	   
           }catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

           }catch (EJBException ex) {
        	   if (log.isInfoEnabled()) {

        		   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
        				   " session: " + session.getId());
        	   }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }}
       	   
/*******************************************************
   -- Inv ADJ Generate Issuance --
 *******************************************************/       	   
       	   
           
        else if (request.getParameter("generateIssuance") != null &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS) ) {
        	 System.out.print("SAVE REQUEST1");
        	
        	if (actionForm.getApprovalStatus().equals("APPROVED") || actionForm.getApprovalStatus().equals("N/A")){
        		  InvAdjustmentDetails details = new InvAdjustmentDetails();
                System.out.print("SAVE REQUEST5");
                details.setAdjCode(actionForm.getAdjustmentCode());
                details.setAdjDocumentNumber(null);
                details.setAdjReferenceNumber(actionForm.getReferenceNumber());
                details.setAdjDescription(actionForm.getDescription());
                details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setAdjType("ISSUANCE");
                details.setAdjCreatedBy(actionForm.getCreatedBy());
                details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
                details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
                details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
                details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
                details.setAdjPosted(EJBCommon.FALSE);
                
                if (actionForm.getAdjustmentCode() == null) {
                    
                    details.setAdjCreatedBy(user.getUserName());
                    details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                    
             }
                
                details.setAdjLastModifiedBy(user.getUserName());
                details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                
                ArrayList alList = new ArrayList(); 
              
             
                for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
                	 System.out.print("SAVE REQUEST3");
                	   InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);           	   
                	              	             	   
     	           if (Common.validateRequired(invADJList.getLocation()) &&
     	         	 	     Common.validateRequired(invADJList.getItemName()) &&
     	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
     	         	 	     Common.validateRequired(invADJList.getUnit()) &&
     	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
     	           
     	 
     	          double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                 
     	          InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
     	           System.out.print("SAVE REQUEST2"+Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
                	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
                	  
                	   
                	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
                	   System.out.println("served3");
                	   mdetails.setAlUomName(invADJList.getUnit());
                	   mdetails.setAlLocName(invADJList.getLocation());
                	   mdetails.setAlIiName(invADJList.getItemName());
                	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
                	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
                	   mdetails.setAlMisc(invADJList.getMisc());
                	   System.out.print("SAVE REQUEST");
                	   System.out.println("invADJList.getMisc()1 : " + invADJList.getMisc());
                	   
                	   
                	 
                	   alList.add(mdetails);
                	
                }                     	
        	
                try {
                	 System.out.print("SAVE!!! REQUEST1"+actionForm.getAdjustmentCode());
                	   ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                } catch (GlobalDocumentNumberNotUniqueException ex) {
                	
                		errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
                	
                } catch (GlobalRecordAlreadyDeletedException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
                	
                } catch (GlobalAccountNumberInvalidException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
                          	
                } catch (GlobalTransactionAlreadyApprovedException ex) {
     
                	  errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
                  
                } catch (GlobalTransactionAlreadyPendingException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
                	
                } catch (GlobalTransactionAlreadyPostedException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
                         
                } catch (GlobalNoApprovalRequesterFoundException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
                         
                } catch (GlobalNoApprovalApproverFoundException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
                	   
                } catch (GlobalInvItemLocationNotFoundException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
                	   
                } catch (GlJREffectiveDateNoPeriodExistException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
                         
                } catch (GlJREffectiveDatePeriodClosedException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
                         
                } catch (GlobalJournalNotBalanceException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
                	   
                } catch (GlobalInventoryDateException ex) {
                   	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch(GlobalBranchAccountNumberInvalidException ex) {
     			
                		errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

                } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
               	
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                           new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));

                } catch (GlobalExpiryDateNotFoundException ex) {
             	   
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
             			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
             	   
                }catch (GlobalMiscInfoIsRequiredException ex) {
             	   
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
             			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
             	   
                }catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
             			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

                }catch (EJBException ex) {
             	   if (log.isInfoEnabled()) {

             		   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
             				   " session: " + session.getId());
             	   }
                    
                    return(mapping.findForward("cmnErrorPage"));
                }
               
        	}
       	
/*******************************************************
   -- Inv ADJ Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
         	InvAdjustmentDetails details = new InvAdjustmentDetails();
            
            details.setAdjCode(actionForm.getAdjustmentCode());
            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
            details.setAdjDescription(actionForm.getDescription());
            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setAdjType("REQUEST");
            details.setAdjCreatedBy(actionForm.getCreatedBy());
            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));

            if (actionForm.getAdjustmentCode() == null) {
            	
            		details.setAdjCreatedBy(user.getUserName());
            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
 	                      
            }
                       
            details.setAdjLastModifiedBy(user.getUserName());
            details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
            
            ArrayList alList = new ArrayList();
                       
            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
            	
            	InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);
            	              	             	   
 	           	if (Common.validateRequired(invADJList.getLocation()) &&
 	         	 	     Common.validateRequired(invADJList.getItemName()) &&
 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
 	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
            	   
            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
            	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
                   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
            	   System.out.print(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit) +"here");
            	   mdetails.setAlUomName(invADJList.getUnit());
            	   mdetails.setAlLocName(invADJList.getLocation());
            	   mdetails.setAlIiName(invADJList.getItemName());           	             	 
            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
            	   mdetails.setAlMisc(invADJList.getMisc());
            	   
            	   
            	   
            	   
            	   alList.add(mdetails);
            	
            }               
            //validate attachment
            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
            
            if (!Common.validateRequired(filename1)) {                	       	    	
       	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentRequest.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentRequest.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentRequest"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
            
            try {
            	
            	    Integer adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), alList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    actionForm.setAdjustmentCode(adjustmentCode);
            	
            } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
               	
            } catch (GlobalRecordAlreadyDeletedException ex) {
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
            	
            } catch (GlobalAccountNumberInvalidException ex) {
            	
            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
                      	
            } catch (GlobalTransactionAlreadyApprovedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
            	
            } catch (GlobalTransactionAlreadyPendingException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
            	
            } catch (GlobalTransactionAlreadyPostedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
                     
            } catch (GlobalNoApprovalRequesterFoundException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
                     
            } catch (GlobalNoApprovalApproverFoundException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
            	   
            } catch (GlobalInvItemLocationNotFoundException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
               	   
            } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
           	
           	errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));
        	   
           } catch (GlobalExpiryDateNotFoundException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
        	   
           }catch (GlobalMiscInfoIsRequiredException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
        	   
           }catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

           }catch (EJBException ex) {
        	   if (log.isInfoEnabled()) {
        		   
        		   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
        				   " session: " + session.getId());
        	   }
           	
           	return(mapping.findForward("cmnErrorPage"));
           }
         // save attachment
            
            if (!Common.validateRequired(filename1)) {     
            	               	            	    		       	   		       	    		           	    		                  	 
        	        if (errors.isEmpty()) {
        	        	       	        	 
        	        	InputStream is = actionForm.getFilename1().getInputStream();
        	        	
        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
        	        	
        	        	new File(attachmentPath).mkdirs();       	        	
        	        	
        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
        	    			
        	    			fileExtension = attachmentFileExtension;
        	    			
            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;          	    			
            	    		
            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-1" + fileExtension); 
 	            	
        	    		int c;        
 	            	            	            	            	
 	            	while ((c = is.read()) != -1) {
 	            		
 	            		fos.write((byte)c);
 	            		
 	            	}
 	            	
 	            	is.close();
 					fos.close();
        	        	
        	        }          	                
             	           	    	
        	   }
        	   
        	   if (!Common.validateRequired(filename2)) {     
            	               	            	    		       	   		       	    		           	    		                  	 
        	        if (errors.isEmpty()) {
        	        	       	        	 
        	        	InputStream is = actionForm.getFilename2().getInputStream();
        	        	
        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
        	        	
        	        	new File(attachmentPath).mkdirs();       	        	
        	        	
        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
        	    			
        	    			fileExtension = attachmentFileExtension;
        	    			
            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;          	    			
            	    		
            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-2" + fileExtension); 
 	            	int c;        
 	            	            	            	            	
 	            	while ((c = is.read()) != -1) {
 	            		
 	            		fos.write((byte)c);
 	            		
 	            	}
 	            	
 	            	is.close();
 					fos.close();
        	        	
        	        }          	                
             	           	    	
        	   }
        	   
        	   if (!Common.validateRequired(filename3)) {     
            	               	            	    		       	   		       	    		           	    		                  	 
        	        if (errors.isEmpty()) {
        	        	       	        	 
        	        	InputStream is = actionForm.getFilename3().getInputStream();
        	        	
        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
        	        	
        	        	new File(attachmentPath).mkdirs();       	        	
        	        	
        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
        	    			
        	    			fileExtension = attachmentFileExtension;
        	    			
            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;          	    			
            	    		
            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-3" + fileExtension); 

 	            	int c;        
 	            	            	            	            	
 	            	while ((c = is.read()) != -1) {
 	            		
 	            		fos.write((byte)c);
 	            		
 	            	}
 	            	
 	            	is.close();
 					fos.close();
        	        	
        	        }          	                
             	           	    	
        	   }
        	   
        	   if (!Common.validateRequired(filename4)) {     
            	               	            	    		       	   		       	    		           	    		                  	 
        	        if (errors.isEmpty()) {
        	        	       	        	 
        	        	InputStream is = actionForm.getFilename4().getInputStream();
        	        	
        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
        	        	
        	        	new File(attachmentPath).mkdirs();       	        	
        	        	
        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
        	    			
        	    			fileExtension = attachmentFileExtension;
        	    			
            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;          	    			
            	    		
            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-4" + fileExtension); 
        	        	
 	            	int c;        
 	            	            	            	            	
 	            	while ((c = is.read()) != -1) {
 	            		
 	            		fos.write((byte)c);
 	            		
 	            	}
 	            	
 	            	is.close();
 					fos.close();
        	        	
        	        }          	                
             	           	    	
        	   }
            
           
/*******************************************************
   -- Inv ADJ Journal Action --
*******************************************************/

        /* } else if (request.getParameter("journalButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	    
         	if(Common.validateRequired(actionForm.getApprovalStatus())) {
         	
	         	InvAdjustmentDetails details = new InvAdjustmentDetails();
	            
	            details.setAdjCode(actionForm.getAdjustmentCode());
	            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
	            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
	            details.setAdjDescription(actionForm.getDescription());
	            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            details.setAdjType(actionForm.getType());
	            details.setAdjCreatedBy(actionForm.getCreatedBy());
	            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
	            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
	            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
	            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
	            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));

	            if (actionForm.getAdjustmentCode() == null) {
	            	
	            		details.setAdjCreatedBy(user.getUserName());
	            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	 	                      
	            }
	                       
	            details.setAdjLastModifiedBy(user.getUserName());
	            details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	            
	            ArrayList alList = new ArrayList();
	                                  
	            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
	            	
	            	InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);
	            	              	             	   
	 	           	if (Common.validateRequired(invADJList.getLocation()) &&
	 	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
	            	   
	            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
	            	   
	            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
	            	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
	            	   mdetails.setAlUomName(invADJList.getUnit());
	            	   mdetails.setAlLocName(invADJList.getLocation());
	            	   mdetails.setAlIiName(invADJList.getItemName());           	             	 
	            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
	            	   mdetails.setAlMisc(invADJList.getMisc());
	            	   alList.add(mdetails);
	            	
	            }                     	
	            //validate attachment
	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	            
	            if (!Common.validateRequired(filename1)) {                	       	    	
	       	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}
	            try {
	            	
	            	    Integer adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	    actionForm.setAdjustmentCode(adjustmentCode);
	    
	            } catch (GlobalDocumentNumberNotUniqueException ex) {
	               	
	               	  errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
	               	
	            }  catch (GlobalRecordAlreadyDeletedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
	            	
	            } catch (GlobalAccountNumberInvalidException ex) {
	            	
	            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
	                      	
	            } catch (GlobalTransactionAlreadyApprovedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
	            	
	            } catch (GlobalTransactionAlreadyPendingException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
	            	
	            } catch (GlobalTransactionAlreadyPostedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
	                     
	            } catch (GlobalNoApprovalRequesterFoundException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
	                     
	            } catch (GlobalNoApprovalApproverFoundException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
	            	   
	            } catch (GlobalInvItemLocationNotFoundException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
	               	   
	            } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
	           
	           } catch (GlobalInventoryDateException ex) {
	              	
              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
              	   
	           } catch(GlobalBranchAccountNumberInvalidException ex) {
				
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));

	            } catch (GlobalExpiryDateNotFoundException ex) {
	         	   
	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
	         	   
	            }catch (GlobalMiscInfoIsRequiredException ex) {
	         	   
	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	         	   
	            }catch (GlobalRecordInvalidException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

	            }catch (EJBException ex) {
	            	    if (log.isInfoEnabled()) {
	                	
	                   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                 }
	                
	                return(mapping.findForward("cmnErrorPage"));
	            }
	            
	            if (!errors.isEmpty()) {
		        	
		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invAdjustmentRequest"));
		               
		        }
	         // save attachment
	            
	            if (!Common.validateRequired(filename1)) {     
	            	               	            	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename1().getInputStream();
	        	        	
	        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-1" + fileExtension); 
	 	            	
	        	    		int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename2)) {     
	            	               	            	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename2().getInputStream();
	        	        	
	        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-2" + fileExtension); 
	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename3)) {     
	            	               	            	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename3().getInputStream();
	        	        	
	        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-3" + fileExtension); 

	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename4)) {     
	            	               	            	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename4().getInputStream();
	        	        	
	        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-4" + fileExtension); 
	        	        	
	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	            
	            
         	}
         	
            
            String path = "/invJournal.do?forward=1" + 
		     "&transactionCode=" + actionForm.getAdjustmentCode() +
		     "&transaction=ADJUSTMENT REQUEST" + 
		     "&transactionNumber=" + actionForm.getReferenceNumber() + 
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();
            
            return(new ActionForward(path));
            */
/*******************************************************
   -- Inv ADJ Print Action --
*******************************************************/             	
            
         } else if (request.getParameter("printButton") != null) {
             
             if(Common.validateRequired(actionForm.getApprovalStatus())) {
                 
                 InvAdjustmentDetails details = new InvAdjustmentDetails();
                 System.out.print("print action!!!");
                details.setAdjCode(actionForm.getAdjustmentCode());
 	            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
 	            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
 	            details.setAdjDescription(actionForm.getDescription());
 	            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
 	            details.setAdjType(actionForm.getType());
 	            details.setAdjCreatedBy(actionForm.getCreatedBy());
 	            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
 	            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
 	            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
 	            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
 	            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
 	                       
 	            if (actionForm.getAdjustmentCode() == null) {
 	            	  System.out.print("print1");
 	            		details.setAdjCreatedBy(user.getUserName());
 	            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
 	 	                      
 	            }
 	            
 	           details.setAdjLastModifiedBy(user.getUserName());
	           details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	            
	            ArrayList alList = new ArrayList();
	                            
	            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
	            	
	            	InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);
	            	              	             	   
	 	           	if (Common.validateRequired(invADJList.getLocation()) &&
	 	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnitCost())) continue;
	 	           System.out.print("print2");      	   
	            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
	            	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
	            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
	            	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
	            	   mdetails.setAlUomName(invADJList.getUnit());
	            	   mdetails.setAlLocName(invADJList.getLocation());
	            	   mdetails.setAlIiName(invADJList.getItemName());           	             	 
	            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));      
	            	   mdetails.setAlMisc(invADJList.getMisc());
	            	   
	            	   
	            	   alList.add(mdetails);
                
	            }                     	
	            //validate attachment
	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	            
	            if (!Common.validateRequired(filename1)) {                	       	    	
	       	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentRequest.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentRequest.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentRequest"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}
	            
	            try {
	            	
	            	    Integer adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	    actionForm.setAdjustmentCode(adjustmentCode);
	            	    System.out.print("printing3");
	            } catch (GlobalDocumentNumberNotUniqueException ex) {
	               	
	               	  errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("invAdjustmentRequest.error.documentNumberNotUnique"));    
	               	
	            }  catch (GlobalRecordAlreadyDeletedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
	            	
	            } catch (GlobalAccountNumberInvalidException ex) {
	            	
	            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.accountNumberInvalid"));                               	                 	                   
	                      	
	            } catch (GlobalTransactionAlreadyApprovedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyApproved"));
	            	
	            } catch (GlobalTransactionAlreadyPendingException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPending"));
	            	
	            } catch (GlobalTransactionAlreadyPostedException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.transactionAlreadyPosted"));
	                     
	            } catch (GlobalNoApprovalRequesterFoundException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.noApprovalRequesterFound"));
	                     
	            } catch (GlobalNoApprovalApproverFoundException ex) {
	            	
	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentRequest.error.noApprovalApproverFound"));
	            	   
	            } catch (GlobalInvItemLocationNotFoundException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invAdjustmentRequest.error.noItemLocationFound", ex.getMessage()));
	               	   
	            } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentRequest.error.journalNotBalance"));
	           
	           } catch (GlobalInventoryDateException ex) {
	              	
              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("invAdjustmentRequest.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
              	   
	           } catch(GlobalBranchAccountNumberInvalidException ex) {
				
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentRequest.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invAdjustmentRequest.error.noNegativeInventoryCostingCOA"));

	            } catch (GlobalExpiryDateNotFoundException ex) {
	         	   
	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
	         	   
	            }catch (GlobalMiscInfoIsRequiredException ex) {
	         	   
	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	         	   
	            }catch (GlobalRecordInvalidException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("invoiceRequest.error.insufficientStocks", ex.getMessage()));

	            }catch (EJBException ex) {
	            	    if (log.isInfoEnabled()) {
	                	
	                   log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                 }
	                
	                return(mapping.findForward("cmnErrorPage"));
	            }
	            
	         // save attachment
	            
	            if (!Common.validateRequired(filename1)) {     
	            	  System.out.print("print3");     	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename1().getInputStream();
	        	        	
	        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-1" + fileExtension); 
	 	            	
	        	    		int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename2)) {     
	        		   System.out.print("print4");	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename2().getInputStream();
	        	        	
	        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-2" + fileExtension); 
	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename3)) {     
	            	               	            	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename3().getInputStream();
	        	        	
	        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-3" + fileExtension); 

	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	        	   
	        	   if (!Common.validateRequired(filename4)) {     
	        		   System.out.print("print5"); 	    		       	   		       	    		           	    		                  	 
	        	        if (errors.isEmpty()) {
	        	        	       	        	 
	        	        	InputStream is = actionForm.getFilename4().getInputStream();
	        	        	
	        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	        	        	
	        	        	new File(attachmentPath).mkdirs();       	        	
	        	        	
	        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	        	    			
	        	    			fileExtension = attachmentFileExtension;
	        	    			
	            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	            	    		fileExtension = attachmentFileExtensionPDF;          	    			
	            	    		
	            	    	}
	        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getAdjustmentCode() + "-4" + fileExtension); 
	        	        	
	 	            	int c;        
	 	            	            	            	            	
	 	            	while ((c = is.read()) != -1) {
	 	            		
	 	            		fos.write((byte)c);
	 	            		
	 	            	}
	 	            	
	 	            	is.close();
	 					fos.close();
	        	        	
	        	        }          	                
	             	           	    	
	        	   }
	            
	            
	            if (!errors.isEmpty()) {
		        	
		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invAdjustmentRequest"));
		               
		        }
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 isInitialPrinting = true;
                 
             }  else {
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 return(mapping.findForward("invAdjustmentRequest"));
                 
             }
             
/*******************************************************
   -- Inv ADJ Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbADJ.deleteInvAdjEntry(actionForm.getAdjustmentCode(), user.getUserName(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
        
/*******************************************************
   -- Inv ADJ Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Inv ADJ Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	         	         	
         	int listSize = actionForm.getInvADJListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + adjustmentLineNumber; x++) {
	        	
            	ArrayList comboItem = new ArrayList();
            	
	        	InvAdjustmentRequestList invADJList = new InvAdjustmentRequestList(actionForm, 
	        	    null, new Integer(++lineNumber).toString(), null, null, null,0.0,0.0,null,0.0, 
					null, null, null, null);	
	        		        	    
	        	invADJList.setLocationList(actionForm.getLocationList());
	        	
	        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
	        	invADJList.setUnitList("Select Item First");
	        	
	        	actionForm.saveInvADJList(invADJList);
	        	System.out.print("faffz");      
	        }		                    
            
	        for (int i = 0; i<actionForm.getInvADJListSize(); i++ ) {
           	
           	   InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);
           	   
           	   invADJList.setLineNumber(new Integer(i+1).toString());
           	   
            }        
	        
	        return(mapping.findForward("invAdjustmentRequest"));
	        
/*******************************************************
   -- Inv ADJ Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getInvADJListSize(); i++) {
           	
           	   InvAdjustmentRequestList invADJList = actionForm.getInvADJByIndex(i);
           	   
           	   if (invADJList.isDeleteCheckbox()) {
           	   	
           	   		actionForm.deleteInvADJList(i);
           	   		i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("invAdjustmentRequest"));     
/*******************************************************
   -- INV ADJ View Attachment Action --
 *******************************************************/
	
	     } else if(request.getParameter("viewAttachmentButton1") != null) {
 	    	
 	  	File file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-1" + attachmentFileExtension );
 	  	File filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-1" + attachmentFileExtensionPDF );
 	  	String filename = "";
 	  	
 	  	if (file.exists()){
 	  		filename = file.getName();
 	  	}else if (filePDF.exists()) {
 	  		filename = filePDF.getName();
 	  	}	    	  	
 	  	
			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
 	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		
 	  		fileExtension = attachmentFileExtension;
	    			
	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;          	    			
	    		
	    	}
 	  	System.out.println(fileExtension + " <== file extension?");
 	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getAdjustmentCode() + "-1" + fileExtension);
 	  	
 	  	byte data[] = new byte[fis.available()];
	    	  
 	  	int ctr = 0;
 	  	int c = 0;
	      	
 	  	while ((c = fis.read()) != -1) {
 		  
	      		data[ctr] = (byte)c;
	      		ctr++;
	      		
 	  	}
 	  	
 	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
 	  	} else if (fileExtension == attachmentFileExtensionPDF ){
 	  		System.out.println("pdf");
 	  		
 	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
 	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
 	  	}
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("invAdjustmentRequest"));         
	         	
	         } else {
	         	
	         return (mapping.findForward("invAdjustmentRequestChild"));         
	         	
	       }
	         
	
	
	      } else if(request.getParameter("viewAttachmentButton2") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";
	    	  	
	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getAdjustmentCode() + "-2" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
		      	
	      	
	    	  
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("invAdjustmentRequest"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("invAdjustmentRequestChild"));         
	         	
	     }            
	
	
	      } else if(request.getParameter("viewAttachmentButton3") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		  
	    		  fileExtension = attachmentFileExtension;
		    			
	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getAdjustmentCode() + "-3" + fileExtension);
	    	  	
	    	  byte data[] = new byte[fis.available()];
		    	  
	    	  int ctr = 0;
	    	  int c = 0;
		      	
	    	  while ((c = fis.read()) != -1) {
	    		  
	    		  data[ctr] = (byte)c;
	    		  ctr++;
		      		
	    	  }
	    	  	
	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("invAdjustmentRequest"));         
	         	
	         }else {
	         	
	           return (mapping.findForward("invAdjustmentRequestChild"));         
	         	
	       }           
	
	
	      } else if(request.getParameter("viewAttachmentButton4") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getAdjustmentCode() + "-4" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("invAdjustmentRequest"));         
	         	
	         } else {
	         	
	          return (mapping.findForward("invAdjustmentRequestChild"));         
	         	
	      }

/*******************************************************
    -- Inv ADJ Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" + 
        actionForm.getRowSelected() + "].isItemEntered"))) {
        
      	InvAdjustmentRequestList invADJList = 
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());      	    	
      
      	
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbADJ.getInvUomByIiName(invADJList.getItemName(), user.getCmpCode());
      		 System.out.print("herer 1"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     
      	 //   double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
 			//System.out.print("Actual1"+actual);
      		 System.out.print("HERE");
 			
      		
      		invADJList.clearUnitList();
      		invADJList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			//invADJList.setActualQuantity(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
      			invADJList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				invADJList.setUnit(mUomDetails.getUomName());      				
      				// System.out.print("herer2.3"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     	
      			}
      		}
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(invADJList.getItemName()) && !Common.validateRequired(invADJList.getUnit())) {
      			
	      		//double unitCost = ejbADJ.getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDate(invADJList.getItemName(), invADJList.getUnit(), invADJList.getLocation(), 
	      		//		Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	      
	               double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invADJList.getItemName(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
               
	      
	      
	      	invADJList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
	     	invADJList.setEnableUnitCost(true);
	      	//	 System.out.print("herer 2"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     	
      		}
      		      		      		
      		// populate quantity and default adjust by if necessary
      		
      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {
      			
      		 		
		  		invADJList.setAdjustBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
		
		  		 double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		  		 System.out.print("Actual3 "+actual);
		
		  		invADJList.setActualQuantity(actual);
		  		
		  		
		  	}

      		        	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("invAdjustmentRequest"));
         
/*******************************************************
    -- Inv ADJ Location Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" + 
        actionForm.getRowSelected() + "].isLocationEntered")) && !Common.validateRequired(request.getParameter("invADJList[" + 
                actionForm.getRowSelected() + "].isItemEntered"))) {
    	  System.out.print("Actual 6");
      	InvAdjustmentRequestList invADJList = 
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());      	    	
                      
      	try {
          	      		
      		// populate quantity and default adjust by if necessary
      		
      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {
      		    
      			System.out.print("herer 4"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     
      		   double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			System.out.print("Actual 4 "+actual);
           //     	invADJList.setActualQuantity(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
      			
		  		invADJList.setAdjustBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
		  		invADJList.setEnableUnitCost(true);
		  		
		  	}

      		        	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("invAdjustmentRequest"));
 /*******************************************************
 -- Inv ADJ Item and Location Enter Action --
*******************************************************/

           } else if(!Common.validateRequired(request.getParameter("invADJList[" + 
             actionForm.getRowSelected() + "].isLocationEntered"))) {
             
           	InvAdjustmentRequestList invADJList = 
           		actionForm.getInvADJByIndex(actionForm.getRowSelected());      	    	
                           
           	try {
               	      		
           		// populate quantity and default adjust by if necessary
           		
           		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
           		    !Common.validateRequired(invADJList.getUnit())) {
           		 
           			try{
           			  System.out.print("Action Location"+actionForm.getLocation()+"Action L"+invADJList.getLocation() );
           			
           			
           			    try{
           				 System.out.print("herer 5"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     
           				 System.out.print(invADJList.getLocation()+" vs2 "+actionForm.getLocation());
           				 	double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           				 		System.out.print("Actual 5"+actual);
           				 		//invADJList.setActualQuantity(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
           				 	invADJList.setActualQuantity(actual);
           				 
           			    }catch (Exception ex)
           			    {   invADJList.setActualQuantity(0.0); System.out.print("A"); }
           			    // catch (FinderException ex)
        			  //  {   invADJList.setActualQuantity(0.0); } 
           			  
           			  
           			  
           			  
           			//	 System.out.print("herer 5"+invADJList.getItemName()+" <-IM "+ invADJList.getLocation()+"<-LOC"+ invADJList.getUnit()+"<-GU"+Common.convertStringToSQLDate(actionForm.getDate())+"<-GD"+new Integer(user.getCurrentBranch().getBrCode())+"<-BR"+new Integer(user.getCmpCode())+"<-US");     
           				// System.out.print(invADJList.getLocation()+" vs2 "+actionForm.getLocation());
           				// 	double actual = ejbADJ.getQuantityByIiNameAndUomName(invADJList.getItemName(),invADJList.getLocation(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           				// 		System.out.print("Actual 5"+actual);
           				// 		invADJList.setActualQuantity(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
           			
 
                   
           			 
           			//  invADJList.setActualQuantity("0.0");
           		  
           			 
           			
           	       } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                    log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId());System.out.print("B");
                    return mapping.findForward("cmnErrorPage"); 
                   }
                    }
           	 
           			invADJList.setAdjustBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
     		  		invADJList.setEnableUnitCost(true);
     		  		
     		  	}

           		        	                 	
               } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                    log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId()); System.out.print("C");
                    return mapping.findForward("cmnErrorPage"); 
                    
                 }
             
               }                 
              return(mapping.findForward("invAdjustmentRequest"));         
         
/*******************************************************
    -- Inv ADJ Adjust By Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" + 
        actionForm.getRowSelected() + "].isAdjustByEntered"))) {
        
      	InvAdjustmentRequestList invADJList = 
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());
                      
      	try {
          	      		
      		System.out.print("faff1");      
      		// refresh quantity and calculate quantity
      		
      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {
      			
      		
      			
      			
      			
	      		double itemAdjustBy = (Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));      		
	      		
	      		// populate unit cost field
	      		
	      		if (itemAdjustBy != 0) {
	      		
		      		double unitCost = ejbADJ.getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDate(invADJList.getItemName(), invADJList.getUnit(), invADJList.getLocation(), 
		      				Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		      		invADJList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
		      		invADJList.setEnableUnitCost(true);
		      	  System.out.print("Actual 6");
		      	
	      		} else {
	      			
	      			invADJList.setUnitCost(Common.convertDoubleToStringMoney(0d, costPrecisionUnit));
		      		invADJList.setEnableUnitCost(false);
	      			
	      		}
	      		
	        }
      		
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("invAdjustmentRequest"));
          

 /*******************************************************
         -- Inv ADJ Load Action --
 *******************************************************/

               }
               
               if (frParam != null) {
            	   System.out.print("LOADING333");
               	// Errors on saving
               	
                  if (!errors.isEmpty()) {
                  	
                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("invAdjustmentRequest"));
                     
                  }
                  
                  if (request.getParameter("forward") == null &&
                      actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                      	                	
                      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                      saveErrors(request, new ActionMessages(errors));
                    
                      return mapping.findForward("cmnMain");	
                      
                  }
                  
                  try {
                  	
                  	// location combo box
                  	
                  	ArrayList list = null;
                  	Iterator i = null;
                  	
                  	actionForm.clearLocationList();       	
                  	
                  	list = ejbADJ.getInvLocAll(user.getCmpCode());
                 
                  	if (list == null || list.size() == 0) {
                  		
                  		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
                  		
                  	} else {
                  		           		            		
                  		i = list.iterator();
                  		
                  		while (i.hasNext()) {
                  			
                  		    actionForm.setLocationList((String)i.next());
                  			
                  		}
                  		            		
                  	}  
                  	
                  	actionForm.clearUserList();           	
                	
                	ArrayList userList = ejbADJ.getAdUsrAll(user.getCmpCode());
                	
                	if (userList == null || userList.size() == 0) {
                		
                		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);
                		
                	} else {
                		           		            		
                		Iterator x = userList.iterator();
                		
                		while (x.hasNext()) {
                			
                			actionForm.setUserList((String)x.next());
                			
                		}
                		            		
                	}
                  	
                  	actionForm.clearInvADJList();               	  	
                  	
                  	if (request.getParameter("forward") != null || request.getParameter("forwardWastage") != null
                  			 || request.getParameter("forwardVariance") != null || isInitialPrinting) {

                  	    if (request.getParameter("forward") != null || request.getParameter("forwardWastage") != null
                     			 || request.getParameter("forwardVariance") != null) {
                  	        
                  	        actionForm.setAdjustmentCode(new Integer(request.getParameter("adjustmentCode")));
                  	        
                  	    }
                  	    
                  		InvModAdjustmentDetails mdetails = ejbADJ.getInvAdjByAdjCode(actionForm.getAdjustmentCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());            	
                  		
                  		actionForm.setDocumentNumber(mdetails.getAdjDocumentNumber());
                  		actionForm.setReferenceNumber(mdetails.getAdjReferenceNumber());
                  		actionForm.setDescription(mdetails.getAdjDescription());
                  		actionForm.setDate(Common.convertSQLDateToString(mdetails.getAdjDate()));
                  		actionForm.setType(mdetails.getAdjType());
                  		actionForm.setApprovalStatus(mdetails.getAdjApprovalStatus());
                  		actionForm.setPosted(mdetails.getAdjPosted() == 1 ? "YES" : "NO");
                  		actionForm.setCreatedBy(mdetails.getAdjCreatedBy());
                  		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getAdjDateCreated()));
                  		actionForm.setLastModifiedBy(mdetails.getAdjLastModifiedBy());
                  		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getAdjDateLastModified()));
                  		actionForm.setApprovedRejectedBy(mdetails.getAdjApprovedRejectedBy());
                  		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getAdjDateApprovedRejected()));
                  		actionForm.setPostedBy(mdetails.getAdjPostedBy());
                  		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getAdjDatePosted()));
                  		actionForm.setAdjustmentAccount(mdetails.getAdjCoaAccountNumber()); 
                  		actionForm.setAdjustmentAccountDescription(mdetails.getAdjCoaAccountDescription()); 
                  		actionForm.setReasonForRejection(mdetails.getAdjReasonForRejection());
                  		actionForm.setAdjustmentVoid(Common.convertByteToBoolean(mdetails.getAdjVoid()));
                  		actionForm.setCostVariance(Common.convertByteToBoolean(mdetails.getAdjIsCostVariance()));
                  		System.out.print("LOADING getAdjDocumentNumber "+ mdetails.getAdjDocumentNumber());
                  		System.out.print("LOADING");
                  		
                  		list = mdetails.getAdjAlList();           		
                  		         		            		            		
      		            i = list.iterator();
      		            
      		            int lineNumber = 0;
      		            
      		            while (i.hasNext()) {
      		            	
      		            	InvModAdjustmentLineDetails mAlDetails = (InvModAdjustmentLineDetails)i.next();		            			            			            	
      		            	
      		            
     	            	   System.out.print("EEEE = ");
     	            	   
      		            	InvAdjustmentRequestList adjAlList = new InvAdjustmentRequestList(actionForm,
      		            		mAlDetails.getAlCode(), new Integer(++lineNumber).toString(), 
      							mAlDetails.getAlLocName(), mAlDetails.getAlIiName(), mAlDetails.getAlIiDescription(), mAlDetails.getAlActualQuantity(),mAlDetails.getAlActualQuantityFind(),
      							Common.convertDoubleToStringMoney(mAlDetails.getAlAdjustQuantity(), quantityPrecisionUnit),
      							mAlDetails.getAlServed(),mAlDetails.getAlUomName(),
      							Common.convertDoubleToStringMoney(mAlDetails.getAlUnitCost(), costPrecisionUnit),
      							mAlDetails.getAlMisc(), mAlDetails.getAlPartNumber());		
      		            	System.out.print("LOADING getAlCode "+ mAlDetails.getAlCode());
      		            	System.out.print("LOADING getAlActualQuantity "+mAlDetails.getAlActualQuantity());
      		            	System.out.print("LOADING getAlAdjustQuantity "+mAlDetails.getAlAdjustQuantity());
      		            	System.out.print("LOADING getAlServed "+mAlDetails.getAlServed());
      		            	adjAlList.setLocationList(actionForm.getLocationList());            	
      		            	            	
      		            	
      		            	
      		            	adjAlList.clearUnitList();
      		            	ArrayList unitList = ejbADJ.getInvUomByIiName(mAlDetails.getAlIiName(), user.getCmpCode());
      		            	Iterator unitListIter = unitList.iterator();
      		            	while (unitListIter.hasNext()) {
      		            		
      		            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
      		            		adjAlList.setUnitList(mUomDetails.getUomName()); 
      		            		
      		            	}		            			            			            	
      		            	
      		            	actionForm.saveInvADJList(adjAlList);				         
      				      
      			        }	
      			        			        			        
      			        int remainingList = adjustmentLineNumber - (list.size() % adjustmentLineNumber) + list.size();
      			        
      			        for (int x = list.size() + 1; x <= remainingList; x++) {
      			        	
      			        	ArrayList comboItem = new ArrayList();
      			        	
      			        	InvAdjustmentRequestList invADJList = new InvAdjustmentRequestList(actionForm, 
      				        	    null, new Integer(x).toString(), null, null, null,0.0,0.0,null,0.0,
      								null, null, null, null);	

      			        	invADJList.setLocationList(actionForm.getLocationList());
      			        			        	
      			        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
      			        	invADJList.setUnitList("Select Item First");
      			        	
      			        	actionForm.saveInvADJList(invADJList);
      			        	
      			         }	
      			         
      			         this.setFormProperties(actionForm, precisionUnit, user.getCompany());

      			         if (request.getParameter("child") == null) {
      			         
      			         	return (mapping.findForward("invAdjustmentRequest"));         
      			         	
      			         } else {
      			         	
      			         	return (mapping.findForward("invAdjustmentRequestChild"));         
      			         	
      			         }
                  		
                  	}     
                  	
                  	// Populate line when not forwarding
                  	
                  	for (int x = 1; x <= adjustmentLineNumber; x++) {
      			            		
                  		InvAdjustmentRequestList invADJList = new InvAdjustmentRequestList(actionForm, 
      			        	    null, new Integer(x).toString(), null, null, null,0.0,0.0,null,0.0,
      							null, null, null, null);	

      		        	invADJList.setLocationList(actionForm.getLocationList());
      		        	
      		        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
      		        	invADJList.setUnitList("Select Item First");
      		        	
      		        	actionForm.saveInvADJList(invADJList);
      			    	
      	  	        }       	  	        
      	  	                            	           			
                  		           			   	
                  } catch(GlobalNoRecordFoundException ex) {
                  	            	
                  	errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentRequest.error.recordAlreadyDeleted"));
                  		
                  } catch(EJBException ex) {
      	     	
                     if (log.isInfoEnabled()) {
                     	
                        log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
                           " session: " + session.getId());
                     }
                     
                     return(mapping.findForward("cmnErrorPage"));
                     
                 } 
                  
                  // Errors on loading
               
                  if (!errors.isEmpty()) {
                	
                     saveErrors(request, new ActionMessages(errors));
                   
                  } else {
                	
                     if (request.getParameter("saveSubmitButton") != null && 
                        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                        	
                         try {
                         	
                         		ArrayList list = ejbADJ.getAdApprovalNotifiedUsersByAdjCode(actionForm.getAdjustmentCode(), user.getCmpCode());                   	
                         	                      	   
                         	   if (list.isEmpty()) {
                         	   	   
                         	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                         	   	       new ActionMessage("messages.documentSentForPosting"));
                         	   	       
                         	   } else if (list.contains("DOCUMENT POSTED")) {
                         	   	
                         	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                      	   	       new ActionMessage("messages.documentPosted"));     
                         	   	                      	   	   
                         	   } else {
                         	   	
                         	   	   Iterator i = list.iterator();
                         	   	   
                         	   	   String APPROVAL_USERS = "";
                         	   	   
                         	   	   while (i.hasNext()) {
                         	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                         	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                         	   	       
                         	   	       if (i.hasNext()) {
                         	   	       	
                         	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                         	   	       	
                         	   	       }
                         	   	                          	   	       
                         	   	   	   	                   	   	   	                      	   	   	
                         	   	   }
                         	   	   
                         	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                         	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                         	   	
                         	   }
                         	   
                         	   saveMessages(request, messages);
                         	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                         	
                        	
                         } catch(EJBException ex) {
      	     	
      		               if (log.isInfoEnabled()) {
      		               	
      		                  log.info("EJBException caught in InvAdjustmentRequestAction.execute(): " + ex.getMessage() +
      		                     " session: " + session.getId());
      		               }
      		               
      		               return(mapping.findForward("cmnErrorPage"));
      		               
      		           } 
                         
                     } else if (request.getParameter("saveAsDraftButton") != null || request.getParameter("generateIssuance") != null &&
                        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                        	
                    	 
                        actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                        if(request.getParameter("generateIssuance") != null)
                        {
                        	messages.add(ActionMessages.GLOBAL_MESSAGE,	new ActionMessage("invAdjustmentRequest.prompt.issuanceGenerated"));
                    		saveMessages(request, messages);
                        }
                        	
                     }

                     
                  }
                  
                  actionForm.reset(mapping, request);              

                  actionForm.setAdjustmentCode(null);
                  actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
                  actionForm.setPosted("NO");
                  actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
                  actionForm.setCreatedBy(user.getUserName());
                  actionForm.setLastModifiedBy(user.getUserName());
                  actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));                        
                  InvModAdjustmentDetails details = ejbADJ.getInvPrfDefaultAdjustmentRequestAccount(user.getCmpCode(), new Integer(user.getUserCode()));
                  actionForm.setAdjustmentAccount(details.getAdjCoaAccountNumber());
                  actionForm.setAdjustmentAccountDescription(details.getAdjCoaAccountDescription());
                  
                  this.setFormProperties(actionForm, precisionUnit, user.getCompany());

      /*******************************************************
      -- Check if the request was sent by a mobile device --
      *******************************************************/

                  if (isAccessedByDevice(request.getHeader("user-agent"))) {
                  					
                  	System.out.println("Mobile Device Detected.");
                  	return(mapping.findForward("invAdjustmentRequest2"));

                  }
                  else return(mapping.findForward("invAdjustmentRequest"));


               } else {
               	
                  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                  saveErrors(request, new ActionMessages(errors));

                  return(mapping.findForward("cmnMain"));
                  
               }
               
            } catch(Exception e) {

      /*******************************************************
         System Failed: Forward to error page 
      *******************************************************/
       
               if (log.isInfoEnabled()) {
            	
                  log.info("Exception caught in InvAdjustmentRequestAction.execute(): " + e.getMessage()
                     + " session: " + session.getId());
               }
               
               e.printStackTrace();
               return(mapping.findForward("cmnErrorPage"));
               
            }
         }


      	private void setFormProperties(InvAdjustmentRequestForm actionForm, short costPrecisionUnit, String adCompany) {
      		
      		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      			
      	
      			
      		if (actionForm.getPosted().equals("NO")) {
      				
      				if (actionForm.getAdjustmentCode() == null) {
      					//booleans for enable/disable of buttons
      					actionForm.setEnableFields(true);
      					actionForm.setShowSaveButton(true);
      					actionForm.setShowDeleteButton(false);
      					actionForm.setShowAddLinesButton(true);
      					actionForm.setShowDeleteLinesButton(true);
      					actionForm.setShowGenerateButton(false);	
      						
       					
      					
      					
      					for (int i=0; i<actionForm.getInvADJListSize(); i++) {
      					  	
      					  	  InvAdjustmentRequestList actionList = actionForm.getInvADJByIndex(i);
      					  	  actionList.setUnitCost(null);
      					  	  actionList.setEnableUnitCost(false);
      					  	
      					} 
      					
      				} else if (actionForm.getAdjustmentCode() != null &&
      				    Common.validateRequired(actionForm.getApprovalStatus())) {
      					
      				    actionForm.setEnableFields(true);
      					actionForm.setShowSaveButton(true);
      					actionForm.setShowDeleteButton(true);
      					actionForm.setShowAddLinesButton(true);
      					actionForm.setShowDeleteLinesButton(true);
      					 System.out.print("51approved>> "+ actionForm.getApprovedRejectedBy() );
          			//	 if(actionForm.getPostedBy().equals("YES")&& actionForm.getApprovalStatus().equals("N/A"))
      				//	{
      					//	actionForm.setShowGenerateButton(false);	
      					//}else{actionForm.setShowGenerateButton(false);	}
      				
      					
      					for (int i=0; i<actionForm.getInvADJListSize(); i++) {
      					  	
      					  	  InvAdjustmentRequestList actionList = actionForm.getInvADJByIndex(i);
      					  	
      					  		  
      					  	  if (Common.convertStringMoneyToDouble(actionList.getAdjustBy(), (short)8) < 0) {
      					  	      
      					  	      if (Common.validateRequired(actionList.getUnitCost())) 
      					  	          actionList.setUnitCost(Common.convertDoubleToStringMoney(0d, costPrecisionUnit));

      						  	  actionList.setEnableUnitCost(false);
      					  	  } else actionList.setEnableUnitCost(true);
      					  	
      					}
      				    
      				    	
      				} else {
      					
      					actionForm.setEnableFields(false);
      					actionForm.setShowSaveButton(false);
      					actionForm.setShowDeleteButton(true);
      					actionForm.setShowAddLinesButton(false);
      					actionForm.setShowDeleteLinesButton(false);
      					 System.out.print("3approved>> "+ actionForm.getApprovedRejectedBy() );
      					
          			//	 if(actionForm.getPostedBy().equals("YES")&& actionForm.getApprovalStatus().equals("N/A"))
      				//	{
      					//	actionForm.setShowGenerateButton(true);	
      				//	}else{actionForm.setShowGenerateButton(false);	}
      				}
      				
      			} else {
      				
      				actionForm.setEnableFields(false);
      				actionForm.setShowSaveButton(false);
      				actionForm.setShowDeleteButton(false);
      				actionForm.setShowAddLinesButton(false);
      				actionForm.setShowDeleteLinesButton(false);
      			//	actionForm.setShowGenerateButton(true);	
      				 System.out.print("4approved>> "+ actionForm.getApprovedRejectedBy() );
      			//	 if(actionForm.getPostedBy().equals("NO"))
  					//{
  					
      				 actionForm.setShowGenerateButton(true);	
  				//	}else{actionForm.setShowGenerateButton(true);	}
      			}
      			
      			
      			
      		} else {
      			
      			
      			actionForm.setEnableFields(false);
      			actionForm.setShowSaveButton(false);
      			actionForm.setShowDeleteButton(false);
      			actionForm.setShowAddLinesButton(false);
      			actionForm.setShowDeleteLinesButton(false);
      			 System.out.print("5approved>> "+ actionForm.getApprovedRejectedBy() );
      		
  			//	 if(actionForm.getPostedBy().equals("YES")&& actionForm.getApprovalStatus().equals("N/A"))
				//	{
				//		actionForm.setShowGenerateButton(false);	
			//		}else{actionForm.setShowGenerateButton(true);	}
      		}
      		
      		// view attachment	
      		
      		if (actionForm.getAdjustmentCode() != null) {
         		  System.out.print("print 5x a "+ actionForm.getAdjustmentCode() );
      			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

                  String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/inv/AdjustmentRequest/";
                  String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
                  String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
                  
                  System.out.print("print 6x a "+ actionForm.getAdjustmentCode() );
       			File file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-1" + attachmentFileExtension);
       			File filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-1" + attachmentFileExtensionPDF);
       		  System.out.print("print 7x a "+ actionForm.getAdjustmentCode() );
       			if (file.exists() || filePDF.exists()) {
       			
       				actionForm.setShowViewAttachmentButton1(true);	
       				
       			} else {
       				
       				actionForm.setShowViewAttachmentButton1(false);	
       				
       			}			
       			
       			file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-2" + attachmentFileExtension);
       			filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-2" + attachmentFileExtensionPDF);
       			
       			if (file.exists() || filePDF.exists()) {
       			  System.out.print("print 7x a "+ actionForm.getAdjustmentCode() );
       				actionForm.setShowViewAttachmentButton2(true);	
       				
       			} else {
       				
       				actionForm.setShowViewAttachmentButton2(false);	
       				
       			}
       			System.out.print("print 7x b "+ actionForm.getAdjustmentCode() );
       			file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-3" + attachmentFileExtension);
       			filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-3" + attachmentFileExtensionPDF);
       			
       			if (file.exists() || filePDF.exists()) {
       			  System.out.print("print 8x a "+ actionForm.getAdjustmentCode() );
       				actionForm.setShowViewAttachmentButton3(true);	
       				
       			} else {
       				System.out.print("print 7x c "+ actionForm.getAdjustmentCode() );
       				actionForm.setShowViewAttachmentButton3(false);	
       				
       			}
       			System.out.print("print 7x d "+ actionForm.getAdjustmentCode() );
       			file = new File(attachmentPath + actionForm.getAdjustmentCode() + "-4" + attachmentFileExtension);
       			filePDF = new File(attachmentPath + actionForm.getAdjustmentCode() + "-4" + attachmentFileExtensionPDF);
       			
       			if (file.exists() || filePDF.exists()) {
       			  System.out.print("print 9x a "+ actionForm.getAdjustmentCode() );
       				actionForm.setShowViewAttachmentButton4(true);	
       				
       			} else {
       				
       				actionForm.setShowViewAttachmentButton4(false);	
       				
       			}								
      			
      		} else {
      		  System.out.print("print 6x a "+ actionForm.getAdjustmentCode() );		
      			actionForm.setShowViewAttachmentButton1(false);				
      			actionForm.setShowViewAttachmentButton2(false);
      			actionForm.setShowViewAttachmentButton3(false);
      			actionForm.setShowViewAttachmentButton4(false);
      			
      		}
      		    
      	}
    	
    	private Boolean isAccessedByDevice(String user_agent) {
    		if (user_agent!=null) {
    			if (user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows CE)")||
    				user_agent.equals("Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)")||
    				user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)")
    			) return true;
    			return false;
    		}
    		else return false;
    	}
      	
      }