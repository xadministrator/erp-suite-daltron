package com.struts.inv.adjustmentrequest;

import java.io.Serializable;
import java.util.ArrayList;

public class InvAdjustmentRequestTagList implements Serializable {
	
	
	
	private String propertyCode = null;
	private String specs = null;
	private String serialNumber = null;
	private String custodian = null;
	private String expiryDate = null;
	
	
	private InvAdjustmentRequestList parentBean;
	
	public InvAdjustmentRequestTagList(InvAdjustmentRequestList parentBean,
			String propertyCode, String specs, String custodian, String serialNumber, String expiryDate){
		
		this.parentBean = parentBean;
		this.propertyCode = propertyCode;
		this.serialNumber = serialNumber;
		this.specs = specs;
		this.custodian = custodian;
		this.expiryDate = expiryDate;
	}
	
	public String getPropertyCode() {
		
		return propertyCode;
		
	}
	
	public void setPropertyCode(String propertyCode) {
		
		this.propertyCode = propertyCode;
		
	}
	public String getSpecs() {
		
		return specs;
		
	}
	
	public void setSpecs(String specs) {
		
		this.specs = specs;
		
	}
	
	public String getCustodian() {
		
		return custodian;
		
	}
	
	public void setCustodian(String custodian) {
		
		this.custodian = custodian;
		
	}
	
	public String getSerialNumber() {
		
		return serialNumber;
		
	}
	
	public void setSerialNumber(String serialNumber) {
		
		this.serialNumber = serialNumber;
		
	}
	
	public String getExpiryDate() {
		
		return expiryDate;
		
	}
	
	public void setExpiryDate(String expiryDate) {
		
		this.expiryDate = expiryDate;
		
	}

}