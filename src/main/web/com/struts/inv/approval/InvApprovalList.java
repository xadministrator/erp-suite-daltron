package com.struts.inv.approval;

import java.io.Serializable;

public class InvApprovalList implements Serializable {

	private Integer approvalCode = null;
   private Integer documentCode = null;
   private String document = null;
   private String department = null;
   private String date = null;
   private String documentNumber = null;
   private String branchCode = null;
   private String amount = null;
   private String reasonForRejection = null;
   
   private boolean approve = false;
   private boolean reject = false;
   private Integer brCode = null;
       
   private InvApprovalForm parentBean;
    
   public InvApprovalList(InvApprovalForm parentBean,
	  Integer approvalCode,
      Integer documentCode,
      String document,
      String department,
      String date,
      String documentNumber,
      String branchCode,
      String amount,
      Integer brCode) {

      this.parentBean = parentBean;
      this.approvalCode = approvalCode;
      this.documentCode = documentCode;
      this.document = document;
      this.department = department;
      this.date = date;
      this.documentNumber = documentNumber;
      this.branchCode = branchCode;
      this.amount = amount;
      this.brCode = brCode;
   }
   
   
   public Integer getApprovalCode() {

	      return approvalCode;

	   }

   public Integer getDocumentCode() {

      return documentCode;

   }
   
   public String getDocument() {
   	
   	  return document;
   	
   }
   
   public String getDepartment() {
	   
      return department;
      
   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getBranchCode() {
	   	
   	  return branchCode;
   	  
   }
   
   public String getAmount() {
   
      return amount;
   
   }

   public boolean getApprove() {
   	
   	  return approve;
   	
   }
   
   public void setApprove(boolean approve) {
   	
   	  this.approve = approve;
   	
   }
   
   public boolean getReject() {
   	
   	  return reject;
   	
   }
   
   public void setReject(boolean reject) {
   	
   	  this.reject = reject;
   	  
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
   
   public Integer getBrCode() {
	   return brCode;
   }
         
}