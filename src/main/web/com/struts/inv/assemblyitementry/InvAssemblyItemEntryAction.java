package com.struts.inv.assemblyitementry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.InvFixedCostAccountNotFoundException;
import com.ejb.exception.InvFreightCostAccountNotFoundException;
import com.ejb.exception.InvLaborCostAccountNotFoundException;
import com.ejb.exception.InvOverHeadCostAccountNotFoundException;
import com.ejb.exception.InvPowerCostAccountNotFoundException;
import com.ejb.txn.InvAssemblyItemEntryController;
import com.ejb.txn.InvAssemblyItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvItemDetails;
import com.util.InvModBillOfMaterialDetails;
import com.util.InvModItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvAssemblyItemEntryAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvAssemblyItemEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvAssemblyItemEntryForm actionForm = (InvAssemblyItemEntryForm)form;
			
			String ieParam = Common.getUserPermission(user, Constants.INV_ASSEMBLY_ITEM_ENTRY_ID);
			
			if (ieParam != null) {
				
				if (ieParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invAssemblyItemEntry");
					}
					
				}
				
				actionForm.setUserPermission(ieParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
Initialize InvAssemblyItemEntryController EJB
*******************************************************/
			
			InvAssemblyItemEntryControllerHome homeAIE = null;
			InvAssemblyItemEntryController ejbAIE = null;
			
			try {
				
				homeAIE = (InvAssemblyItemEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvAssemblyItemEntryControllerEJB", InvAssemblyItemEntryControllerHome.class);
				
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvAssemblyItemEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbAIE = homeAIE.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvAssemblyItemEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
   Call InvAssemblyItemEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfInvInventoryLineNumber
   getAdPrfInvQuantityPrecisionUnit
*******************************************************/			
			
			short precisionUnit = 0;
			short billOfMaterialLineNumber = 0;
			short quantityPrecisionUnit = 0;
			short costPrecisionUnit = 0;
			
			
			try {
				
				precisionUnit = ejbAIE.getGlFcPrecisionUnit(user.getCmpCode());
				billOfMaterialLineNumber = ejbAIE.getAdPrfInvInventoryLineNumber(user.getCmpCode());
				quantityPrecisionUnit = ejbAIE.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());
				costPrecisionUnit = ejbAIE.getAdPrfInvCostPrecisionUnit(user.getCmpCode());
				actionForm.setPrecisionUnit(costPrecisionUnit);
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}			
			
/*******************************************************
-- Inv IE Save Action --
*******************************************************/
			
			if (request.getParameter("saveButton") != null &&
					(actionForm.getUserPermission().equals(Constants.FULL_ACCESS) || actionForm.getUserPermission().equals(Constants.SPECIAL))) {
				
				InvItemDetails details = new InvItemDetails();
				details.setIiCode(actionForm.getItemCode());
				details.setIiName(actionForm.getItemName());
				details.setIiDescription(actionForm.getDescription());
				details.setIiPartNumber(actionForm.getPartNumber());
				details.setIiClass("Assembly");
				details.setIiAdLvCategory(actionForm.getCategory());
				details.setIiCostMethod(actionForm.getCostMethod());
				details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), costPrecisionUnit));
				details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), costPrecisionUnit));
				details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), costPrecisionUnit));
				
				details.setIiSpecificGravity(Common.convertStringMoneyToDouble(actionForm.getSpecificGravity(), quantityPrecisionUnit));
				details.setIiStandardFillSize(Common.convertStringMoneyToDouble(actionForm.getStandardFillSize(), quantityPrecisionUnit));

				details.setIiYield(Common.convertStringMoneyToDouble(actionForm.getYield(), quantityPrecisionUnit));

				details.setIiLaborCost(Common.convertStringMoneyToDouble(actionForm.getLaborCost(), costPrecisionUnit));
				details.setIiPowerCost(Common.convertStringMoneyToDouble(actionForm.getPowerCost(), costPrecisionUnit));
				details.setIiOverHeadCost(Common.convertStringMoneyToDouble(actionForm.getOverHeadCost(), costPrecisionUnit));
				details.setIiFreightCost(Common.convertStringMoneyToDouble(actionForm.getFreightCost(), costPrecisionUnit));
				details.setIiFixedCost(Common.convertStringMoneyToDouble(actionForm.getFixedCost(), costPrecisionUnit));
				
				System.out.println("LABOR ACC = "+actionForm.getLaborCostAccount());
				System.out.println("POWER ACC = "+actionForm.getPowerCostAccount());
				System.out.println("OVERHEAD ACC = "+actionForm.getOverHeadCostAccount());
				System.out.println("FREIGHT ACC = "+actionForm.getFreightCostAccount());
				System.out.println("FIXED ACC = "+actionForm.getFixedCostAccount());
				details.setGlCoaLaborCostAccount(actionForm.getLaborCostAccount());
				details.setGlCoaPowerCostAccount(actionForm.getPowerCostAccount());
				details.setGlCoaOverHeadCostAccount(actionForm.getOverHeadCostAccount());
				details.setGlCoaFreightCostAccount(actionForm.getFreightCostAccount());
				details.setGlCoaFixedCostAccount(actionForm.getFixedCostAccount());
				
				details.setIiLossPercentage(Common.convertStringMoneyToDouble(actionForm.getLossPercentage(), costPrecisionUnit));				
				details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), costPrecisionUnit));		
				details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));  
				details.setIiEnableAutoBuild(Common.convertBooleanToByte(actionForm.getEnableAutoBuild()));
				details.setIiDoneness(actionForm.getDoneness());
				details.setIiSidings(actionForm.getSidings());
				details.setIiRemarks(actionForm.getRemarks());
				details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
				details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
				details.setIiMarket(actionForm.getMarket());
				details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
				details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
				details.setIiUmcPackaging(actionForm.getPackaging());
				details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
				details.setIiTaxCode(actionForm.getTaxCode());
				
				details.setIiScSunday(Common.convertBooleanToByte(actionForm.getScSunday()));
				details.setIiScMonday(Common.convertBooleanToByte(actionForm.getScMonday()));
				details.setIiScTuesday(Common.convertBooleanToByte(actionForm.getScTuesday()));
				details.setIiScWednesday(Common.convertBooleanToByte(actionForm.getScWednesday()));
				details.setIiScThursday(Common.convertBooleanToByte(actionForm.getScThursday()));
				details.setIiScFriday(Common.convertBooleanToByte(actionForm.getScFriday()));
				details.setIiScSaturday(Common.convertBooleanToByte(actionForm.getScSaturday()));
				
				ArrayList bomList = new ArrayList();
				
				for (int i = 0; i<actionForm.getInvIEListSize(); i++) {
					
					InvAssemblyItemEntryList invIEList = actionForm.getInvIEByIndex(i);           	   
					
					if (Common.validateRequired(invIEList.getItemName()) &&
							Common.validateRequired(invIEList.getQuantityNeeded())) continue;
					
					InvModBillOfMaterialDetails mdetails = new InvModBillOfMaterialDetails();
					
					mdetails.setBomIiName(invIEList.getItemName());
					mdetails.setBomLocName(invIEList.getLocation());
					mdetails.setBomQuantityNeeded(Common.convertStringMoneyToDouble(invIEList.getQuantityNeeded(), quantityPrecisionUnit));
					mdetails.setBomUomName(invIEList.getUnit());
					
					bomList.add(mdetails);
					
				
					
				}
				
				try {
					
					ejbAIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getRetailUom(), actionForm.getDefaultLocation(), bomList, user.getCmpCode());
					
				} catch (GlobalInvItemLocationNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.noItemLocationFound", ex.getMessage()));                

				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.itemNameAlreadyExist", ex.getMessage()));
					
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));
					
				}  catch (InvLaborCostAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.laborCostAccountNotFound"));
					
				}  catch (InvPowerCostAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.powerCostAccountNotFound"));
					
				}  catch (InvOverHeadCostAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.overHeadCostAccountNotFound"));
					
				}  catch (InvFreightCostAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.freightCostAccountNotFound"));

				}  catch (InvFixedCostAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.fixedCostAccountNotFound"));

					
				} catch (GlobalRecordInvalidException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.itemNameShouldNotBeTheSameAsAssemblyName", ex.getMessage()));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
 -- Inv IE Price Levels Action --
 *******************************************************/
				
			} else if(request.getParameter("priceLevelsButton") != null) {
				
				if(actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
					
					InvItemDetails details = new InvItemDetails();
					details.setIiCode(actionForm.getItemCode());
					details.setIiName(actionForm.getItemName());
					details.setIiDescription(actionForm.getDescription());
					details.setIiPartNumber(actionForm.getPartNumber());
					details.setIiClass("Assembly");
					details.setIiAdLvCategory(actionForm.getCategory());
					details.setIiCostMethod(actionForm.getCostMethod());
					details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), costPrecisionUnit));
					details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), costPrecisionUnit));
					details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), costPrecisionUnit));
					details.setIiSpecificGravity(Common.convertStringMoneyToDouble(actionForm.getSpecificGravity(), quantityPrecisionUnit));
					details.setIiStandardFillSize(Common.convertStringMoneyToDouble(actionForm.getStandardFillSize(), quantityPrecisionUnit));

					details.setIiYield(Common.convertStringMoneyToDouble(actionForm.getYield(), quantityPrecisionUnit));
					
					details.setIiLaborCost(Common.convertStringMoneyToDouble(actionForm.getLaborCost(), costPrecisionUnit));
					details.setIiPowerCost(Common.convertStringMoneyToDouble(actionForm.getPowerCost(), costPrecisionUnit));
					details.setIiOverHeadCost(Common.convertStringMoneyToDouble(actionForm.getOverHeadCost(), costPrecisionUnit));
					details.setIiFreightCost(Common.convertStringMoneyToDouble(actionForm.getFreightCost(), costPrecisionUnit));
					details.setIiFixedCost(Common.convertStringMoneyToDouble(actionForm.getFixedCost(), costPrecisionUnit));

					details.setGlCoaLaborCostAccount(actionForm.getLaborCostAccount());
					details.setGlCoaPowerCostAccount(actionForm.getPowerCostAccount());
					details.setGlCoaOverHeadCostAccount(actionForm.getOverHeadCostAccount());
					details.setGlCoaFreightCostAccount(actionForm.getFreightCostAccount());
					details.setGlCoaFixedCostAccount(actionForm.getFixedCostAccount());
					
					details.setIiLossPercentage(Common.convertStringMoneyToDouble(actionForm.getLossPercentage(), costPrecisionUnit));				
					details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), costPrecisionUnit));		
					details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));  
					details.setIiEnableAutoBuild(Common.convertBooleanToByte(actionForm.getEnableAutoBuild()));
					details.setIiDoneness(actionForm.getDoneness());
					details.setIiSidings(actionForm.getSidings());
					details.setIiRemarks(actionForm.getRemarks());
					details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
					details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
					details.setIiMarket(actionForm.getMarket());
					details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
					details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
					details.setIiUmcPackaging(actionForm.getPackaging());
					details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
					details.setIiTaxCode(actionForm.getTaxCode());
					
					ArrayList bomList = new ArrayList();
					
					for (int i = 0; i<actionForm.getInvIEListSize(); i++) {
						
						InvAssemblyItemEntryList invIEList = actionForm.getInvIEByIndex(i);           	   
						
						if (Common.validateRequired(invIEList.getItemName()) &&
								Common.validateRequired(invIEList.getQuantityNeeded())) continue;
						
						InvModBillOfMaterialDetails mdetails = new InvModBillOfMaterialDetails();
						
						mdetails.setBomIiName(invIEList.getItemName());
						mdetails.setBomLocName(invIEList.getLocation());
						mdetails.setBomQuantityNeeded(Common.convertStringMoneyToDouble(invIEList.getQuantityNeeded(), quantityPrecisionUnit));
						ArrayList uomList = ejbAIE.getInvUomByIiName(invIEList.getItemName(), user.getCmpCode());
						
						Iterator j = uomList.iterator();
						
						while(j.hasNext()) {
							
							InvModUnitOfMeasureDetails uomDetails = (InvModUnitOfMeasureDetails)  j.next();
							
							invIEList.setUnitList(uomDetails.getUomName());
							
						}
						mdetails.setBomUomName(invIEList.getUnit());
						bomList.add(mdetails);
						
					}
					
					try {
						
						Integer itemCode = ejbAIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getRetailUom(), actionForm.getDefaultLocation(), bomList, user.getCmpCode());
						
						actionForm.setItemCode(itemCode);
						
					} catch (GlobalInvItemLocationNotFoundException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("assemblyItemEntry.error.noItemLocationFound", ex.getMessage()));                
						
					} catch (GlobalRecordAlreadyExistException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("assemblyItemEntry.error.itemNameAlreadyExist"));
						
					} catch (GlobalRecordAlreadyAssignedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				String path = "/invPriceLevels.do?forward=1" +
				"&itemCode=" + String.valueOf(actionForm.getItemCode()) +
				"&transaction=ASSEMBLY_ITEM_ENTRY" +
				"&itemDescription=" + actionForm.getDescription() +
				"&userPermission=" + actionForm.getUserPermission();
				
				return new ActionForward(path);
				
/*******************************************************
	 -- Inv IE Conversion Action --
*******************************************************/
				
			} else if (request.getParameter("conversionButton") != null) {
			    
				if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
					
					InvItemDetails details = new InvItemDetails();
				    details.setIiCode(actionForm.getItemCode());
				    details.setIiName(actionForm.getItemName());
				    details.setIiDescription(actionForm.getDescription());
				    details.setIiPartNumber(actionForm.getPartNumber());
				    details.setIiClass("Assembly");
				    details.setIiAdLvCategory(actionForm.getCategory());
				    details.setIiCostMethod(actionForm.getCostMethod());
				    details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), costPrecisionUnit));
				    details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), costPrecisionUnit));
					details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), costPrecisionUnit));
					details.setIiSpecificGravity(Common.convertStringMoneyToDouble(actionForm.getSpecificGravity(), quantityPrecisionUnit));
					details.setIiStandardFillSize(Common.convertStringMoneyToDouble(actionForm.getStandardFillSize(), quantityPrecisionUnit));

					details.setIiYield(Common.convertStringMoneyToDouble(actionForm.getYield(), quantityPrecisionUnit));
					
					details.setIiLaborCost(Common.convertStringMoneyToDouble(actionForm.getLaborCost(), costPrecisionUnit));
					details.setIiPowerCost(Common.convertStringMoneyToDouble(actionForm.getPowerCost(), costPrecisionUnit));
					details.setIiOverHeadCost(Common.convertStringMoneyToDouble(actionForm.getOverHeadCost(), costPrecisionUnit));
					details.setIiFreightCost(Common.convertStringMoneyToDouble(actionForm.getFreightCost(), costPrecisionUnit));
					details.setIiFixedCost(Common.convertStringMoneyToDouble(actionForm.getFixedCost(), costPrecisionUnit));

					details.setGlCoaLaborCostAccount(actionForm.getLaborCostAccount());
					details.setGlCoaPowerCostAccount(actionForm.getPowerCostAccount());
					details.setGlCoaOverHeadCostAccount(actionForm.getOverHeadCostAccount());
					details.setGlCoaFreightCostAccount(actionForm.getFreightCostAccount());
					details.setGlCoaFixedCostAccount(actionForm.getFixedCostAccount());
					
					details.setIiLossPercentage(Common.convertStringMoneyToDouble(actionForm.getLossPercentage(), costPrecisionUnit));				
					details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), costPrecisionUnit));		
				    details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));  
				    details.setIiEnableAutoBuild(Common.convertBooleanToByte(actionForm.getEnableAutoBuild()));
				    details.setIiDoneness(actionForm.getDoneness());
				    details.setIiSidings(actionForm.getSidings());
				    details.setIiRemarks(actionForm.getRemarks());
				    details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
				    details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
				    details.setIiMarket(actionForm.getMarket());
					details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
					details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
					details.setIiUmcPackaging(actionForm.getPackaging());
					details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
					details.setIiTaxCode(actionForm.getTaxCode());
				    ArrayList bomList = new ArrayList();
				    
				    for (int i = 0; i<actionForm.getInvIEListSize(); i++) {
				        
				        InvAssemblyItemEntryList invIEList = actionForm.getInvIEByIndex(i);           	   
				        
				        if (Common.validateRequired(invIEList.getItemName()) &&
				                Common.validateRequired(invIEList.getQuantityNeeded())) continue;
				        
				        InvModBillOfMaterialDetails mdetails = new InvModBillOfMaterialDetails();
				        
				        mdetails.setBomIiName(invIEList.getItemName());
				        mdetails.setBomLocName(invIEList.getLocation());
				        mdetails.setBomQuantityNeeded(Common.convertStringMoneyToDouble(invIEList.getQuantityNeeded(), quantityPrecisionUnit));
				        
				        ArrayList uomList = ejbAIE.getInvUomByIiName(invIEList.getItemName(), user.getCmpCode());
						
						Iterator j = uomList.iterator();
						
						while(j.hasNext()) {
							
							InvModUnitOfMeasureDetails uomDetails = (InvModUnitOfMeasureDetails)  j.next();
							
							invIEList.setUnitList(uomDetails.getUomName());
							
						}
				        mdetails.setBomUomName(invIEList.getUnit());
				        			        
				        bomList.add(mdetails);
				        
				    }
				    
				    try {
				        
				        Integer itemCode = ejbAIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getRetailUom(), actionForm.getDefaultLocation(), bomList, user.getCmpCode());
				        actionForm.setItemCode(itemCode);
				        
				    } catch (GlobalInvItemLocationNotFoundException ex) {
				        
				        errors.add(ActionMessages.GLOBAL_MESSAGE,
				                new ActionMessage("assemblyItemEntry.error.noItemLocationFound", ex.getMessage()));                
				        
				    } catch (GlobalRecordAlreadyExistException ex) {
				        
				        errors.add(ActionMessages.GLOBAL_MESSAGE,
				                new ActionMessage("assemblyItemEntry.error.itemNameAlreadyExist"));
				        
				    } catch (GlobalRecordAlreadyAssignedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));
				        
				    } catch (EJBException ex) {
				        
				        if (log.isInfoEnabled()) {
				            
				            log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
				                    " session: " + session.getId());
				            return mapping.findForward("cmnErrorPage"); 
				            
				        }
				        
				    }
				    
				}
			    
			    // create path
			    String path = "/invUnitOfMeasureConversion.do?forward=1" + 
			    "&itemCode=" + actionForm.getItemCode() +
			    "&itemName=" + actionForm.getItemName() +
				"&itemDescriptionn=" +actionForm.getDescription() +
			    "&unitMeasure=" + actionForm.getUnitMeasure() +
			    "&itemClass=Assembly" +
				"&userPermission=" + actionForm.getUserPermission();
			    
			    return(new ActionForward(path));			

/*******************************************************
-- Inv IE Delete Action --
*******************************************************/
				
			} else if(request.getParameter("deleteButton") != null) {
				
				try {
					
					ejbAIE.deleteInvIiEntry(actionForm.getItemCode(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.itemAlreadyAssigned"));
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.itemAlreadyDeleted"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}
				
/*******************************************************
	 -- Inv IE Unit Measure Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("isUnitMeasureEntered"))) {
			    
			    try {
			        
			        if (!actionForm.getUnitMeasure().equals(Constants.GLOBAL_NO_RECORD_FOUND)) { 
			            
			            actionForm.clearPackagingList();
			            actionForm.clearRetailUomList();
			            
			            ArrayList list = ejbAIE.getInvUomAllByUnitMeasureClass(actionForm.getUnitMeasure(), user.getCmpCode());
			            
			            if (list == null || list.size() == 0) {
			                
			                actionForm.setPackagingList(Constants.GLOBAL_NO_RECORD_FOUND);
			                actionForm.setRetailUomList(Constants.GLOBAL_NO_RECORD_FOUND);
			                
			            } else {
			                
			                Iterator i = list.iterator();
			                
			                while (i.hasNext()) {
			                    
			                	String uom = (String)i.next();
			                    actionForm.setPackagingList(uom);
			                	actionForm.setRetailUomList(uom);
			                    
			                }
			                
			            }
			            
			        }
			        
			    } catch (EJBException ex) {
			        
			        if (log.isInfoEnabled()) {
			            
			            log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			            return mapping.findForward("cmnErrorPage"); 
			            
			        }
			        
			    }  	
			    
			    return(mapping.findForward("invAssemblyItemEntry"));
			    
/*******************************************************
-- Inv IE Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
-- Inv IE Add Lines Action --
*******************************************************/
				
			} else if(request.getParameter("addLinesButton") != null) {
				
				int listSize = actionForm.getInvIEListSize();
				int lineNumber = 0;
				
				for (int x = listSize + 1; x <= listSize + billOfMaterialLineNumber; x++) {
					
					InvAssemblyItemEntryList invIEList = new InvAssemblyItemEntryList(actionForm, 
							null, new Integer(++lineNumber).toString(), null, null, null, null, null, null, null ,null);	       
					
					invIEList.setLocationList(actionForm.getLocationList());
					
					actionForm.saveInvIEList(invIEList);
					
				}	
				
				for (int i = 0; i<actionForm.getInvIEListSize(); i++) {
					
					InvAssemblyItemEntryList invIEList = actionForm.getInvIEByIndex(i);
					
					invIEList.setLineNumber(new Integer(i+1).toString());
					
				}        
				
				return(mapping.findForward("invAssemblyItemEntry"));
				
/*******************************************************
-- Inv II Delete Lines Action --
*******************************************************/
				
			} else if(request.getParameter("deleteLinesButton") != null) {
				
				for (int i = 0; i<actionForm.getInvIEListSize(); i++) {
					
					InvAssemblyItemEntryList invIEList = actionForm.getInvIEByIndex(i);
					
					if (invIEList.getDeleteCheckbox()) {
						
						actionForm.deleteInvIEList(i);
						i--;
					}
					
				}
				
				return(mapping.findForward("invAssemblyItemEntry"));	
				
/*******************************************************
-- Inv IE BOM Item Name Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("invIEList[" + 
					actionForm.getRowSelected() + "].isItemNameEntered"))) {

				InvAssemblyItemEntryList invIEList = 
					actionForm.getInvIEByIndex(actionForm.getRowSelected());				
				
				try {
					
					invIEList.setDescription(ejbAIE.getIiDescriptionByIiName(invIEList.getItemName(), user.getCmpCode()));
					
					// get unit measure
					// (ejbAIE.getIiUnitMeasureByIiName(invIEList.getItemName(), user.getCmpCode()));
					
					ArrayList uomList = ejbAIE.getInvUomByIiName(invIEList.getItemName(), user.getCmpCode());
					
					invIEList.clearUnitList();
					invIEList.setUnitList(Constants.GLOBAL_BLANK);
					
					Iterator i = uomList.iterator();
					
					while (i.hasNext()) {
						
						InvModUnitOfMeasureDetails mdetails = (InvModUnitOfMeasureDetails)i.next();
						
						invIEList.setUnitList(mdetails.getUomName());
						
						if(mdetails.isDefault()) {
							
							invIEList.setUnit(mdetails.getUomName());
							
						}
						
					}
					
					// get unit cost
					// double unitCost = ejbAIE.getIiUnitCostByIiName(invIEList.getItemName(), user.getCmpCode());
					
					double unitCost = 0d;
					
					if (!Common.validateRequired(invIEList.getItemName()) && !Common.validateRequired(invIEList.getUnit())) {
						
						unitCost = ejbAIE.getInvIiUnitCostByIiNameAndUomName(invIEList.getItemName(), invIEList.getUnit(), user.getCmpCode());
					
					}
					
					invIEList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
					
					invIEList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
					//invIEList.setQuantityNeeded("1");
					
				
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}

				return(mapping.findForward("invAssemblyItemEntry"));
				
/*******************************************************
 -- INV IE BOM Unit Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("invIEList[" + 
					actionForm.getRowSelected() + "].isUnitEntered"))) {
				
				InvAssemblyItemEntryList invIEList = 
					actionForm.getInvIEByIndex(actionForm.getRowSelected());
				
				try {
					
					// populate unit cost field
					
					if (!Common.validateRequired(invIEList.getItemName()) && !Common.validateRequired(invIEList.getItemName())) {
						
						double unitCost = ejbAIE.getInvIiUnitCostByIiNameAndUomName(invIEList.getItemName(), invIEList.getUnit(), user.getCmpCode());
						invIEList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));

					}
			
					// populate assembly item unit cost
					
					double totalUnitCost = 0;
					
					for (int i = 0; i < actionForm.getInvIEListSize(); i++ ){
						
						invIEList = actionForm.getInvIEByIndex(i);
						
						double quantityNeeded = Common.convertStringMoneyToDouble(invIEList.getQuantityNeeded(),
								quantityPrecisionUnit);
						double unitCost = Common.convertStringMoneyToDouble(invIEList.getUnitCost(), costPrecisionUnit);
						
						totalUnitCost = totalUnitCost + (quantityNeeded * unitCost);
						
					}
					
					actionForm.setUnitCost(Common.convertDoubleToStringMoney(totalUnitCost, costPrecisionUnit));
					
										
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  	
				
				return(mapping.findForward("invAssemblyItemEntry"));
		
/*******************************************************
-- Inv IE Load Action --
*******************************************************/
				
			}
			
			if (ieParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invAssemblyItemEntry");
					
				}
				
				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
					saveErrors(request, new ActionMessages(errors));
					
					return mapping.findForward("cmnMain");	
					
				}							
				
				ArrayList list = null;
				Iterator i = null;
				
				try {

					actionForm.clearTaxCodeList();           	

					list = ejbAIE.getArTcAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setTaxCodeList((String) i.next());

						}

					} 

					actionForm.clearCategoryList();           	
					
					list = ejbAIE.getAdLvInvItemCategoryAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setCategoryList((String)i.next());
							
						}
						
					}					
					
					actionForm.clearUnitMeasureList();
					
					list = ejbAIE.getInvUomAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setUnitMeasureList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setUnitMeasureList((String)i.next());
							
						}
						
					}
					
					actionForm.clearDefaultLocationList();
					
					list = ejbAIE.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setDefaultLocation(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setDefaultLocationList((String)i.next());
							
						}
						
					}   
					
					actionForm.clearDonenessList();           	
					
					list = ejbAIE.getAdLvInvDonenessAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setDonenessList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setDonenessList((String)i.next());
							
						}
						
					}					
					
					
					
					actionForm.clearLocationList();           	
					
					list = ejbAIE.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					actionForm.clearItemNameList();
					
					list = ejbAIE.getInvIiAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setItemNameList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setItemNameList((String)i.next());
							
						}
						
					}
					
					actionForm.clearSupplierList();
					
					list = ejbAIE.getApSplAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setSupplierList((String)i.next());
							
						}
						
					}	
					
					actionForm.clearPackagingList();
					actionForm.setPackagingList("Select Unit Measure first");
					actionForm.clearRetailUomList();
					actionForm.setRetailUomList("Select Unit Measure first");
					
					actionForm.clearInvIEList();
					
					if (request.getParameter("forward") != null) {
						
						InvModItemDetails iiDetails = ejbAIE.getInvIiByIiCode(
								new Integer(request.getParameter("iiCode")), user.getCmpCode());
						
						actionForm.setItemCode(iiDetails.getIiCode());
						actionForm.setItemName(iiDetails.getIiName());
						actionForm.setDescription(iiDetails.getIiDescription());
						actionForm.setPartNumber(iiDetails.getIiPartNumber());
						
						if(!actionForm.getUnitMeasureList().contains(iiDetails.getIiUomName())) {
							
							if (actionForm.getUnitMeasureList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearUnitMeasureList();
								
							}
							actionForm.setUnitMeasureList(iiDetails.getIiUomName());
							
						}
						actionForm.setUnitMeasure(iiDetails.getIiUomName());
						
						actionForm.setCategory(iiDetails.getIiAdLvCategory());
						actionForm.setCostMethod(iiDetails.getIiCostMethod());
						actionForm.setUnitCost(Common.convertDoubleToStringMoney(iiDetails.getIiUnitCost(), costPrecisionUnit));
						actionForm.setAverageCost(Common.convertDoubleToStringMoney(iiDetails.getIiAveCost(), costPrecisionUnit));
						actionForm.setPercentMarkup(Common.convertDoubleToStringMoney(iiDetails.getIiPercentMarkup(), costPrecisionUnit));
						actionForm.setShippingCost(Common.convertDoubleToStringMoney(iiDetails.getIiShippingCost(), costPrecisionUnit));						
						actionForm.setSpecificGravity(Common.convertDoubleToStringMoney(iiDetails.getIiSpecificGravity(), costPrecisionUnit));
						actionForm.setStandardFillSize(Common.convertDoubleToStringMoney(iiDetails.getIiStandardFillSize(), costPrecisionUnit));
						
						actionForm.setYield(Common.convertDoubleToStringMoney(iiDetails.getIiYield(),quantityPrecisionUnit));
	
						
						actionForm.setLaborCost(Common.convertDoubleToStringMoney(iiDetails.getIiLaborCost(), costPrecisionUnit));
						actionForm.setPowerCost(Common.convertDoubleToStringMoney(iiDetails.getIiPowerCost(), costPrecisionUnit));
						actionForm.setOverHeadCost(Common.convertDoubleToStringMoney(iiDetails.getIiOverHeadCost(), costPrecisionUnit));
						actionForm.setFreightCost(Common.convertDoubleToStringMoney(iiDetails.getIiFreightCost(), costPrecisionUnit));
						actionForm.setFixedCost(Common.convertDoubleToStringMoney(iiDetails.getIiFixedCost(), costPrecisionUnit));

						actionForm.setLaborCostAccount(iiDetails.getLaborCostAccount());
						actionForm.setLaborCostAccountDescription(iiDetails.getLaborCostAccountDescription());
						
						actionForm.setPowerCostAccount(iiDetails.getPowerCostAccount());
						actionForm.setPowerCostAccountDescription(iiDetails.getPowerCostAccountDescription());
						
						actionForm.setOverHeadCostAccount(iiDetails.getOverHeadCostAccount());
						actionForm.setOverHeadCostAccountDescription(iiDetails.getOverHeadCostAccountDescription());
						
						actionForm.setFreightCostAccount(iiDetails.getFreightCostAccount());
						actionForm.setFreightCostAccountDescription(iiDetails.getFreightCostAccountDescription());
						
						actionForm.setFixedCostAccount(iiDetails.getFixedCostAccount());
						actionForm.setFixedCostAccountDescription(iiDetails.getFixedCostAccountDescription());
						
						
						actionForm.setLossPercentage(Common.convertDoubleToStringMoney(iiDetails.getIiLossPercentage(),costPrecisionUnit));
						actionForm.setSalesPrice(Common.convertDoubleToStringMoney(iiDetails.getIiSalesPrice(), costPrecisionUnit));
						actionForm.setGrossProfit(Common.convertDoubleToStringMoney(iiDetails.getIiSalesPrice() - (iiDetails.getIiShippingCost() + iiDetails.getIiUnitCost()), costPrecisionUnit));
						actionForm.setEnable(Common.convertByteToBoolean(iiDetails.getIiEnable()));
						actionForm.setEnableAutoBuild(Common.convertByteToBoolean(iiDetails.getIiEnableAutoBuild()));
						
						if(!actionForm.getDonenessList().contains(iiDetails.getIiDoneness())) {
							
							if (actionForm.getDonenessList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearDonenessList();
								
							}
							actionForm.setDonenessList(iiDetails.getIiDoneness());
							
						}						
						actionForm.setDoneness(iiDetails.getIiDoneness());
						
						actionForm.setSidings(iiDetails.getIiSidings());
						actionForm.setRemarks(iiDetails.getIiRemarks());
						actionForm.setServiceCharge(Common.convertByteToBoolean(iiDetails.getIiServiceCharge()));
						actionForm.setNonInventoriable(Common.convertByteToBoolean(iiDetails.getIiNonInventoriable()));
						actionForm.setEnablePo(Common.convertByteToBoolean(iiDetails.getIiEnablePo()));
						actionForm.setMarket(iiDetails.getIiMarket());
						actionForm.setPoCycle(Common.convertShortToString(iiDetails.getIiPoCycle()));
						
						actionForm.setSupplier(iiDetails.getIiSplSpplrCode());
						
						actionForm.clearPackagingList();
						
						// get umc packaging list
						list = ejbAIE.getInvUomAllByUnitMeasureClass(actionForm.getUnitMeasure(), user.getCmpCode());
						
						if (list != null || list.size() > 0) {
							
							i = list.iterator();
							
							while (i.hasNext()) {
								
								String uom = (String)i.next();
								actionForm.setPackagingList(uom);
								actionForm.setRetailUomList(uom);
								
							}
							
						}	
						
						if(!actionForm.getPackagingList().contains(iiDetails.getIiUmcPackaging())) {
							
							actionForm.setPackagingList(iiDetails.getIiUmcPackaging());
							
						}
						
						if(!actionForm.getRetailUomList().contains(iiDetails.getIiRetailUomName())) {
							
							actionForm.setRetailUomList(iiDetails.getIiRetailUomName());
							
						}
						
						if(!actionForm.getDefaultLocationList().contains(iiDetails.getIiDefaultLocationName())) {
							
							actionForm.setDefaultLocationList(iiDetails.getIiDefaultLocationName());
							
						}
						
						actionForm.setPackaging(iiDetails.getIiUmcPackaging());
						actionForm.setRetailUom(iiDetails.getIiRetailUomName());
						actionForm.setDefaultLocation(iiDetails.getIiDefaultLocationName());
						actionForm.setOpenProduct(Common.convertByteToBoolean(iiDetails.getIiOpenProduct()));
						//actionForm.setTaxCode(iiDetails.getIiTaxCode());
						
						actionForm.setScSunday(Common.convertByteToBoolean(iiDetails.getIiScSunday()));
						actionForm.setScMonday(Common.convertByteToBoolean(iiDetails.getIiScMonday()));
						actionForm.setScTuesday(Common.convertByteToBoolean(iiDetails.getIiScTuesday()));
						actionForm.setScWednesday(Common.convertByteToBoolean(iiDetails.getIiScWednesday()));
						actionForm.setScThursday(Common.convertByteToBoolean(iiDetails.getIiScThursday()));
						actionForm.setScFriday(Common.convertByteToBoolean(iiDetails.getIiScFriday()));
						actionForm.setScSaturday(Common.convertByteToBoolean(iiDetails.getIiScSaturday()));
						
						if (!actionForm.getTaxCodeList().contains(iiDetails.getIiTaxCode())) {
	                         
	                         if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
	                             
	                             actionForm.clearTaxCodeList();
	                             
	                         }
	                         actionForm.setTaxCodeList(iiDetails.getIiTaxCode());
	                         
	                     }
	                     actionForm.setTaxCode(iiDetails.getIiTaxCode());
	                     
						// get bill of materials
						list = iiDetails.getIiBomList();
						
						i = list.iterator();
						
						int lineNumber = 0;
						
						double rawmat = 0;
						
						while(i.hasNext()) {
							
							InvModBillOfMaterialDetails bomDetails = (InvModBillOfMaterialDetails)i.next();
							
							double convertedCost = bomDetails.getBomIiItemCategory().contains("RAWMAT")&& !iiDetails.getIiAdLvCategory().contains("Bulk") ?
									(bomDetails.getBomQuantityNeeded() / 100) 
									* 
									ejbAIE.getInvIiUnitCostByIiNameAndUomName(bomDetails.getBomIiName(), bomDetails.getBomUomName(), user.getCmpCode()) 
									* 
									(iiDetails.getIiStandardFillSize() / 1000) 
									
									* 
									iiDetails.getIiSpecificGravity()
									:
										(bomDetails.getBomQuantityNeeded() /100) * ejbAIE.getInvIiUnitCostByIiNameAndUomName(bomDetails.getBomIiName(), bomDetails.getBomUomName(), user.getCmpCode())
									;
									
							
							InvAssemblyItemEntryList invIEList = new InvAssemblyItemEntryList(actionForm, bomDetails.getBomCode(),
									new Integer(++lineNumber).toString(), bomDetails.getBomIiName(), bomDetails.getBomLocName(),
									
									Common.convertDoubleToStringMoney(bomDetails.getBomQuantityNeeded(), quantityPrecisionUnit),
									Common.convertDoubleToStringMoney(convertedCost , costPrecisionUnit),
									bomDetails.getBomIiItemCategory(),
									Common.convertDoubleToStringMoney(ejbAIE.getInvIiUnitCostByIiNameAndUomName(bomDetails.getBomIiName(), bomDetails.getBomUomName(), user.getCmpCode()), costPrecisionUnit),
									
									bomDetails.getBomUomName(),
									ejbAIE.getIiDescriptionByIiName(bomDetails.getBomIiName(), user.getCmpCode()));
							
							invIEList.setItemNameList(actionForm.getItemNameList());
							invIEList.setLocationList(actionForm.getLocationList());
							
							ArrayList uomList = ejbAIE.getInvUomByIiName(invIEList.getItemName(), user.getCmpCode());
							
							Iterator j = uomList.iterator();
							
							while(j.hasNext()) {
								
								InvModUnitOfMeasureDetails uomDetails = (InvModUnitOfMeasureDetails)  j.next();
								
								invIEList.setUnitList(uomDetails.getUomName());
								
							}
							
							rawmat += convertedCost;
							actionForm.saveInvIEList(invIEList);
							
						}
						
						actionForm.setRawmat(Common.convertDoubleToStringMoney(rawmat, costPrecisionUnit));
						
						int remainingList = billOfMaterialLineNumber - (list.size() % billOfMaterialLineNumber) + list.size();						
						
						for (int x = list.size() + 1; x <= remainingList; x++) {
							
							InvAssemblyItemEntryList invIEList = new InvAssemblyItemEntryList(actionForm, 
									null, new Integer(x).toString(), null, null, null, null, null, null, null,null);
							
							invIEList.setItemNameList(actionForm.getItemNameList());
							invIEList.setLocationList(actionForm.getLocationList());
							
							invIEList.setUnitList(Constants.GLOBAL_BLANK);
							invIEList.setUnitList("Select Item First");
							
							actionForm.saveInvIEList(invIEList);
							
						}
						
						this.setFormProperties(actionForm);
						
						return(mapping.findForward("invAssemblyItemEntry"));  
						
					}
					
					// when not forwarding
					
					actionForm.setItemCode(null);  
					
					for (int x = 1; x <= billOfMaterialLineNumber; x++) {
						
						InvAssemblyItemEntryList invIEList = new InvAssemblyItemEntryList(actionForm,
								null, new Integer(x).toString(), null, null, null, null, null, null, null, null);	       
						
						invIEList.setItemNameList(actionForm.getItemNameList());
						invIEList.setLocationList(actionForm.getLocationList());
						
						invIEList.setUnitList(Constants.GLOBAL_BLANK);
						invIEList.setUnitList("Select Item First");
						
						actionForm.saveInvIEList(invIEList);
						
					}       	  	        					
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("assemblyItemEntry.error.noRecordFound")); 
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvAssemblyItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}           
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if ((request.getParameter("saveButton") != null &&
							(actionForm.getUserPermission().equals(Constants.FULL_ACCESS) || actionForm.getUserPermission().equals(Constants.SPECIAL)))) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
					
				}
				
				actionForm.reset(mapping, request);
				//actionForm.setCostMethod("Average");
				actionForm.setEnable(true);
				this.setFormProperties(actionForm);
				return(mapping.findForward("invAssemblyItemEntry"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
System Failed: Forward to error page 
*******************************************************/
			
			e.printStackTrace();
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvAssemblyItemEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
	}
	
	private void setFormProperties(InvAssemblyItemEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getItemCode() == null) {
				
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setShowDeleteButton(true);
				
			}
			
			actionForm.setShowAddLinesButton(true);
			actionForm.setShowDeleteLinesButton(true);
			
		} else {
			
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			
		}
		
	}	
	
}