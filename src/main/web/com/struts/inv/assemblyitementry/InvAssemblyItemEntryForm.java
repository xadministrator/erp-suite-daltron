package com.struts.inv.assemblyitementry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvAssemblyItemEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer itemCode = null;
	private String itemName = null;
	private String description = null;
	private String partNumber = null;
	private String unitMeasure = null;
	private ArrayList unitMeasureList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String costMethod = null;
	private ArrayList costMethodList = new ArrayList();
	private String unitCost = null;
	private String averageCost = null;
	private String percentMarkup = null;
	private String shippingCost = null;
	private String salesPrice = null;
	private String grossProfit = null;
	private String specificGravity = null;
	private String standardFillSize = null;
	private String yield = null;
	private String rawmat = null;

	private String laborCost = null;
	private String powerCost = null;
	private String overHeadCost = null;
	private String freightCost = null;
	private String fixedCost = null;

	private String laborCostAccount = null;
	private String laborCostAccountDescription = null;

	private String powerCostAccount = null;
	private String powerCostAccountDescription = null;

	private String overHeadCostAccount = null;
	private String overHeadCostAccountDescription = null;

	private String freightCostAccount = null;
	private String freightCostAccountDescription = null;

	private String fixedCostAccount = null;
	private String fixedCostAccountDescription = null;


	private String lossPercentage = null;
	private ArrayList itemNameList = new ArrayList();
	private boolean enable = false;
	private boolean virtualStore = false;
	private boolean showDeleteButton = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;

	private boolean enableAutoBuild = false;
	private String location = null;
	private ArrayList locationList = new ArrayList();

	private ArrayList invIEList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();

	private String remarks = null;
	private String sidings = null;
	private String doneness = null;
	private ArrayList donenessList = new ArrayList();
	private boolean serviceCharge = false;
	private boolean nonInventoriable = false;
	private String market = null;
	private boolean enablePo = false;
	private String poCycle = null;
	private String packaging = null;
	private ArrayList packagingList = new ArrayList();
	private String retailUom = null;
	private ArrayList retailUomList = new ArrayList();
	private String defaultLocation = null;
	private ArrayList defaultLocationList = new ArrayList();

	private String supplier = null;
	private ArrayList supplierList = new ArrayList();

	private String isUnitMeasureEntered = null;

	private boolean openProduct = false;
	private boolean dineInCharge = false;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();

	private boolean scSunday = false;
	private boolean scMonday = false;
	private boolean scTuesday = false;
	private boolean scWednesday = false;
	private boolean scThursday = false;
	private boolean scFriday = false;
	private boolean scSaturday = false;

	public int getRowSelected(){

		return rowSelected;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public InvAssemblyItemEntryList getInvIEByIndex(int index){

		return((InvAssemblyItemEntryList)invIEList.get(index));

	}

	public Object[] getInvIEList(){

		return(invIEList.toArray());

	}

	public int getInvIEListSize(){

		return(invIEList.size());

	}

	public void saveInvIEList(Object newInvIEList){

		invIEList.add(newInvIEList);

	}

	public void clearInvIEList(){

		invIEList.clear();

	}

	public void setRowSelected(Object selectedInvIEList, boolean isEdit){

		this.rowSelected = invIEList.indexOf(selectedInvIEList);

	}

	public void updateInvIERow(int rowSelected, Object newInvIEList){

		invIEList.set(rowSelected, newInvIEList);

	}

	public void deleteInvIEList(int rowSelected){

		invIEList.remove(rowSelected);

	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}



	public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public Integer getItemCode() {

		return itemCode;

	}

	public void setItemCode(Integer itemCode) {

		this.itemCode = itemCode;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public void setPartNumber(String partNumber) {

		this.partNumber = partNumber;

	}

	public String getUnitMeasure() {

		return unitMeasure;

	}

	public void setUnitMeasure(String unitMeasure) {

		this.unitMeasure = unitMeasure;

	}

	public ArrayList getUnitMeasureList() {

		return unitMeasureList;

	}

	public void setUnitMeasureList(String unitMeasure) {

		unitMeasureList.add(unitMeasure);

	}

	public void clearUnitMeasureList() {

		unitMeasureList.clear();
		unitMeasureList.add(Constants.GLOBAL_BLANK);

	}

	public String getCategory() {

		return category;

	}

	public void setCategory(String category) {

		this.category = category;

	}

	public ArrayList getCategoryList() {

		return categoryList;

	}

	public void setCategoryList(String category) {

		categoryList.add(category);

	}

	public void clearCategoryList() {

		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);

	}

	public String getCostMethod() {

		return costMethod;

	}

	public void setCostMethod(String costMethod) {

		this.costMethod = costMethod;

	}

	public ArrayList getCostMethodList() {

		return costMethodList;

	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAverageCost() {

		return averageCost;

	}

	public void setAverageCost(String averageCost) {

		this.averageCost = averageCost;

	}

	public String getPercentMarkup() {

		return percentMarkup;

	}

	public void setPercentMarkup(String percentMarkup) {

		this.percentMarkup = percentMarkup;

	}

	public String getShippingCost() {

		return shippingCost;

	}

	public void setShippingCost(String shippingCost) {

		this.shippingCost = shippingCost;

	}

	public String getSalesPrice() {

		return salesPrice;

	}

	public void setSalesPrice(String salesPrice) {

		this.salesPrice = salesPrice;

	}

	public String getGrossProfit() {

		return grossProfit;

	}

	public void setGrossProfit(String grossProfit) {

		this.grossProfit = grossProfit;

	}

	public String getSpecificGravity() {

		return specificGravity;

	}

	public void setSpecificGravity(String specificGravity) {

		this.specificGravity = specificGravity;

	}

	public String getStandardFillSize() {

		return standardFillSize;

	}

	public void setStandardFillSize(String standardFillSize) {

		this.standardFillSize = standardFillSize;

	}


	public String getYield() {

		return yield;

	}

	public void setYield(String yield) {

		this.yield = yield;

	}

	public String getRawmat() {

		return rawmat;

	}

	public void setRawmat(String rawmat) {

		this.rawmat = rawmat;

	}

	public String getLaborCost() {

		return laborCost;

	}

	public void setLaborCost(String laborCost) {

		this.laborCost = laborCost;

	}



	public String getPowerCost() {

		return powerCost;

	}

	public void setPowerCost(String powerCost) {

		this.powerCost = powerCost;

	}


	public String getOverHeadCost() {

		return overHeadCost;

	}

	public void setOverHeadCost(String overHeadCost) {

		this.overHeadCost = overHeadCost;

	}

	public String getFreightCost() {

		return freightCost;

	}

	public void setFreightCost(String freightCost) {

		this.freightCost = freightCost;

	}

	public String getFixedCost() {

		return fixedCost;

	}

	public void setFixedCost(String fixedCost) {

		this.fixedCost = fixedCost;

	}



	public String getLaborCostAccount() {

		return laborCostAccount;
	}

	public void setLaborCostAccount(String laborCostAccount) {

		this.laborCostAccount = laborCostAccount;
	}

	public String getLaborCostAccountDescription() {

		return laborCostAccountDescription;

	}

	public void setLaborCostAccountDescription(String laborCostAccountDescription) {

		this.laborCostAccountDescription = laborCostAccountDescription;
	}



	public String getPowerCostAccount() {

		return powerCostAccount;
	}

	public void setPowerCostAccount(String powerCostAccount) {

		this.powerCostAccount = powerCostAccount;
	}

	public String getPowerCostAccountDescription() {

		return powerCostAccountDescription;

	}

	public void setPowerCostAccountDescription(String powerCostAccountDescription) {

		this.powerCostAccountDescription = powerCostAccountDescription;
	}



	public String getOverHeadCostAccount() {

		return overHeadCostAccount;
	}

	public void setOverHeadCostAccount(String overHeadCostAccount) {

		this.overHeadCostAccount = overHeadCostAccount;
	}

	public String getOverHeadCostAccountDescription() {

		return overHeadCostAccountDescription;

	}

	public void setOverHeadCostAccountDescription(String overHeadCostAccountDescription) {

		this.overHeadCostAccountDescription = overHeadCostAccountDescription;
	}




	public String getFreightCostAccount() {

		return freightCostAccount;
	}

	public void setFreightCostAccount(String freightCostAccount) {

		this.freightCostAccount = freightCostAccount;
	}

	public String getFreightCostAccountDescription() {

		return freightCostAccountDescription;

	}

	public void setFreightCostAccountDescription(String freightCostAccountDescription) {

		this.freightCostAccountDescription = freightCostAccountDescription;
	}


	public String getFixedCostAccount() {

		return fixedCostAccount;
	}

	public void setFixedCostAccount(String fixedCostAccount) {

		this.fixedCostAccount = fixedCostAccount;
	}

	public String getFixedCostAccountDescription() {

		return fixedCostAccountDescription;

	}

	public void setFixedCostAccountDescription(String fixedCostAccountDescription) {

		this.fixedCostAccountDescription = fixedCostAccountDescription;
	}


	public String getLossPercentage() {

		return lossPercentage;

	}

	public void setLossPercentage(String lossPercentage) {

		this.lossPercentage = lossPercentage;

	}


	public ArrayList getItemNameList() {

		return itemNameList;

	}

	public void setItemNameList(String name) {

		itemNameList.add(name);

	}

	public void clearItemNameList() {

		itemNameList.clear();
		itemNameList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getEnable() {

		return enable;

	}

	public void setEnable(boolean enable) {

		this.enable = enable;

	}

	public boolean getVirtualStore() {

		return virtualStore;

	}

	public void getVirtualStore(boolean virtualStore) {

		this.virtualStore = virtualStore;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}



	public boolean getEnableAutoBuild() {

	    return enableAutoBuild;

	}

	public void setEnableAutoBuild(boolean enableAutoBuild) {

	    this.enableAutoBuild = enableAutoBuild;

	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String locationList) {

		this.locationList.add(locationList);

	}

	public void clearLocationList() {

   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getDoneness() {

		return doneness;

	}

	public void setDoneness(String doneness) {

		this.doneness = doneness;

	}

	public ArrayList getDonenessList() {

		return donenessList;

	}

	public void setDonenessList(String doneness) {

		donenessList.add(doneness);

	}

	public void clearDonenessList() {

		donenessList.clear();
		donenessList.add(Constants.GLOBAL_BLANK);

	}

	public String getRemarks() {

		return remarks;

	}

	public void setRemarks(String remarks) {

		this.remarks = remarks;

	}

	public String getSidings() {

		return sidings;

	}

	public void setSidings(String sidings) {

		this.sidings = sidings;

	}

	public boolean getServiceCharge() {

		return serviceCharge;

	}

	public void setServiceCharge(boolean serviceCharge) {

		this.serviceCharge = serviceCharge;

	}

	public boolean getNonInventoriable() {

		return nonInventoriable;

	}

	public void setNonInventoriable(boolean nonInventoriable) {

		this.nonInventoriable = nonInventoriable;

	}

	public String getSupplier() {

		return supplier;

	}

	public void setSupplier(String supplier) {

		this.supplier = supplier;

	}

	public ArrayList getSupplierList() {

		return supplierList;

	}

	public void setSupplierList(String supplier) {

		supplierList.add(supplier);

	}

	public void clearSupplierList() {

		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);

	}

	public String getMarket() {

	    return market;

	}

	public void setMarket(String market) {

	    this.market = market;

	}

	public String getPoCycle() {

	    return poCycle;

	}

	public void setPoCycle(String poCycle) {

	    this.poCycle = poCycle;

	}

	public String getPackaging() {

	    return packaging;

	}

	public void setPackaging(String packaging) {

	    this.packaging = packaging;

	}

	public ArrayList getPackagingList() {

		return packagingList;

	}

	public void setPackagingList(String packaging) {

	    packagingList.add(packaging);

	}

	public void clearPackagingList() {

	    packagingList.clear();
	    packagingList.add(Constants.GLOBAL_BLANK);

	}

	public String getRetailUom() {

	    return retailUom;

	}

	public void setRetailUom(String retailUom) {

	    this.retailUom = retailUom;

	}

	public ArrayList getRetailUomList() {

	    return retailUomList;

	}

	public void setRetailUomList(String retailUom) {

	    retailUomList.add(retailUom);

	}

	public void clearRetailUomList() {

		retailUomList.clear();
		retailUomList.add(Constants.GLOBAL_BLANK);

	}

	public String getDefaultLocation() {

	    return defaultLocation;

	}

	public void setDefaultLocation(String defaultLocation) {

	    this.defaultLocation = defaultLocation;

	}

	public ArrayList getDefaultLocationList() {

	    return defaultLocationList;

	}

	public void setDefaultLocationList(String defaultLocation) {

		defaultLocationList.add(defaultLocation);

	}

	public void clearDefaultLocationList() {

		defaultLocationList.clear();
		defaultLocationList.add(Constants.GLOBAL_BLANK);

	}

	public String getIsUnitMeasureEntered() {

	    return isUnitMeasureEntered;

	}

	public void setIsUnitMeasureEntered(String isUnitMeasureEntered) {

	    this.isUnitMeasureEntered = isUnitMeasureEntered;

	}

	public boolean getEnablePo() {

	    return enablePo;

	}

	public void setEnablePo(boolean enablePo) {

	    this.enablePo = enablePo;

	}

	public boolean getOpenProduct() {

	    return openProduct;

	}

	public void setOpenProduct(boolean openProduct) {

	    this.openProduct = openProduct;

	}


	public boolean getDineInCharge() {

	    return dineInCharge;

	}

	public void setDineInCharge(boolean dineInCharge) {

	    this.dineInCharge = dineInCharge;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getScSunday() {

		return scSunday;
	}

	public void setScSunday(boolean scSunday) {

		this.scSunday = scSunday;
	}

	public boolean getScMonday() {

		return scMonday;
	}

	public void setScMonday(boolean scMonday) {

		this.scMonday = scMonday;
	}

	public boolean getScTuesday() {

		return scTuesday;
	}

	public void setScTuesday(boolean scTuesday) {

		this.scTuesday = scTuesday;
	}

	public boolean getScWednesday() {

		return scWednesday;
	}

	public void setScWednesday(boolean scWednesday) {

		this.scWednesday = scWednesday;
	}

	public boolean getScThursday() {

		return scThursday;
	}

	public void setScThursday(boolean scThursday) {

		this.scThursday = scThursday;
	}

	public boolean getScFriday() {

		return scFriday;
	}

	public void setScFriday(boolean scFriday) {

		this.scFriday = scFriday;
	}

	public boolean getScSaturday() {

		return scSaturday;
	}

	public void setScSaturday(boolean scSaturday) {

		this.scSaturday = scSaturday;
	}


	public void reset(ActionMapping mapping, HttpServletRequest request) {

		itemName = null;
		description = null;
		partNumber = null;
		unitCost = null;
		percentMarkup = null;
		averageCost = null;
		shippingCost = null;
		salesPrice = null;
		grossProfit = null;
		specificGravity = null;
		standardFillSize = null;
		yield = null;
		rawmat = null;

		laborCost = null;
		powerCost = null;
		overHeadCost = null;
		freightCost = null;
		fixedCost = null;

		laborCostAccount = Constants.GLOBAL_BLANK;
		laborCostAccountDescription = Constants.GLOBAL_BLANK;
		powerCostAccount = Constants.GLOBAL_BLANK;
		powerCostAccountDescription = Constants.GLOBAL_BLANK;
		overHeadCostAccount = Constants.GLOBAL_BLANK;
		overHeadCostAccountDescription = Constants.GLOBAL_BLANK;
		freightCostAccount = Constants.GLOBAL_BLANK;
		freightCostAccountDescription = Constants.GLOBAL_BLANK;
		fixedCostAccount = Constants.GLOBAL_BLANK;
		fixedCostAccountDescription = Constants.GLOBAL_BLANK;

		lossPercentage = null;
		unitMeasure = Constants.GLOBAL_BLANK;
		category = Constants.GLOBAL_BLANK;
		costMethodList.clear();
		costMethodList.add(Constants.GLOBAL_BLANK);
		costMethodList.add("Average");
		costMethodList.add("FIFO");
		costMethodList.add("Standard");
		costMethod = Constants.GLOBAL_BLANK;
		enable = false;
		virtualStore = false;
		enableAutoBuild = false;
		nonInventoriable = false;
		serviceCharge = false;
		supplier = null;
		market = null;
		poCycle = null;
		packaging = Constants.GLOBAL_BLANK;
		retailUom = Constants.GLOBAL_BLANK;
		defaultLocation = Constants.GLOBAL_BLANK;
		isUnitMeasureEntered = null;
		enablePo = false;
		openProduct = false;
		dineInCharge = false;
		taxCode = Constants.GLOBAL_BLANK;

		scSunday = false;
		scMonday = false;
		scTuesday = false;
		scWednesday = false;
		scThursday = false;
		scFriday = false;
		scSaturday = false;

		/*taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);
		taxCodeList.add("Sales Tax");
		taxCodeList.add("Prepared Food Tax");
		*/
		for (int i=0; i<invIEList.size(); i++) {

			InvAssemblyItemEntryList actionList = (InvAssemblyItemEntryList)invIEList.get(i);
	  	  	actionList.setIsItemNameEntered(null);
	  	  	actionList.setIsLocationEntered(null);
	  	  	actionList.setIsUnitEntered(null);
		}

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		if (request.getParameter("saveButton") != null || request.getParameter("conversionButton") != null || request.getParameter("priceLevelsButton") != null) {

			if (Common.validateRequired(itemName)) {

				errors.add("itemName",
						new ActionMessage("assemblyItemEntry.error.itemNameRequired"));

			}

			if (Common.validateRequired(unitMeasure) || unitMeasure.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("unitMeasure",
						new ActionMessage("assemblyItemEntry.error.unitMeasureRequired"));

			}

			if (Common.validateRequired(category) || category.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("category",
						new ActionMessage("assemblyItemEntry.error.categoryRequired"));

			}

			if (Common.validateRequired(costMethod) || costMethod.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("costMethod",
						new ActionMessage("assemblyItemEntry.error.costMethodRequired"));

			}

			if (!Common.validateMoneyFormat(salesPrice)) {

				errors.add("salesPrice",
						new ActionMessage("assemblyItemEntry.error.salesPriceInvalid"));

			}

			if (!Common.validateMoneyFormat(unitCost)) {

				errors.add("unitCost",
						new ActionMessage("assemblyItemEntry.error.unitCostInvalid"));

			}

			if (!Common.validateMoneyFormat(percentMarkup)) {

				errors.add("percentMarkup",
						new ActionMessage("assemblyItemEntry.error.percentMarkupInvalid"));

			}

			if (!Common.validateMoneyFormat(shippingCost)) {

				errors.add("shippingCost",
						new ActionMessage("assemblyItemEntry.error.shippingCostInvalid"));

			}
			if (!Common.validateMoneyFormat(yield)) {

				errors.add("yield",
						new ActionMessage("assemblyItemEntry.error.yieldInvalid"));

			}

			/*
			if (!Common.validateMoneyFormat(laborCost)) {

				errors.add("laborCost",
						new ActionMessage("assemblyItemEntry.error.laborCostInvalid"));

			}


			if (!Common.validateMoneyFormat(powerCost)) {

				errors.add("powerCost",
						new ActionMessage("assemblyItemEntry.error.powerCostInvalid"));

			}

			if (!Common.validateMoneyFormat(overHeadCost)) {

				errors.add("overHeadCost",
						new ActionMessage("assemblyItemEntry.error.overHeadCostInvalid"));

			}

			if (!Common.validateMoneyFormat(freightCost)) {

				errors.add("freightCost",
						new ActionMessage("assemblyItemEntry.error.freightCostInvalid"));

			}
		
			if (!Common.validateMoneyFormat(fixedCost)) {

				errors.add("fixedCost",
						new ActionMessage("assemblyItemEntry.error.fixedCostInvalid"));

			}

          */
			if (!Common.validateMoneyFormat(lossPercentage)) {

				errors.add("shippingCost",
						new ActionMessage("assemblyItemEntry.error.lossPercentageInvalid"));

			}


			if (!Common.validateNumberFormat(poCycle)) {

				errors.add("poCycle",
						new ActionMessage("assemblyItemEntry.error.poCycleInvalid"));

			}




			Iterator i = invIEList.iterator();

			while (i.hasNext()) {

				InvAssemblyItemEntryList iiList = (InvAssemblyItemEntryList)i.next();

				if (Common.validateRequired(iiList.getLocation()) &&
						Common.validateRequired(iiList.getItemName()) &&
						Common.validateRequired(iiList.getQuantityNeeded())) continue;

				if(Common.validateRequired(iiList.getItemName()) || iiList.getItemName().equals(Constants.GLOBAL_BLANK)){
					errors.add("name",
							new ActionMessage("assemblyItemEntry.error.nameRequired", iiList.getLineNumber()));
				}
				if(Common.validateRequired(iiList.getQuantityNeeded())){
					errors.add("quantityNeeded",
							new ActionMessage("assemblyItemEntry.error.quantityNeededRequired", iiList.getLineNumber()));
				}
				if(!Common.validateNumberFormat(iiList.getQuantityNeeded())){
					errors.add("quantityNeeded",
							new ActionMessage("assemblyItemEntry.error.quantityNeededInvalid", iiList.getLineNumber()));
				}

				if(Common.validateRequired(iiList.getLocation()) || iiList.getLocation().equals(Constants.GLOBAL_BLANK)){
		            errors.add("location",
		            	new ActionMessage("assemblyItemEntry.error.locationRequired", iiList.getLineNumber()));
		        }

				if(Common.validateRequired(iiList.getUnit()) || iiList.getUnit().equals(Constants.GLOBAL_BLANK)){
					errors.add("unit",
							new ActionMessage("assemblyItemEntry.error.unitRequired", iiList.getLineNumber()));
				}

			}

		} else if (!Common.validateRequired(request.getParameter("isLocationEntered"))) {

			if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("location",
						new ActionMessage("assemblyItemEntry.error.locationRequired"));
			}

		}

		return errors;

	}

}