package com.struts.inv.assemblyitementry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvAssemblyItemEntryList implements Serializable {
	

	private Integer billOfMaterialCode = null;
	private String lineNumber = null;
	private String itemName = null;
	private ArrayList itemNameList = new ArrayList();
	private String quantityNeeded = null;
	private String rawCost = null;
	private String category = null;
	private String unitCost = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String description = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	
	private String isLocationEntered = null;
	private String isItemNameEntered = null;
	private String isUnitEntered = null;

	
		
	private boolean deleteCheckbox = false;
	
	private InvAssemblyItemEntryForm parentBean;
	
	public InvAssemblyItemEntryList(InvAssemblyItemEntryForm parentBean,
			Integer billOfMaterialCode,
			String lineNumber,
			String itemName,
			String location,
			String quantityNeeded,
			String rawCost,
			String category,
			String unitCost,
			String unit,
			String description){
		
		this.parentBean = parentBean;
		this.billOfMaterialCode = billOfMaterialCode;
		this.lineNumber = lineNumber;
		this.itemName = itemName;  
		this.location = location;
		this.quantityNeeded = quantityNeeded;
		this.rawCost = rawCost;
		this.category = category;
		this.unitCost = unitCost;
		this.unit = unit;
		this.description = description;
		
	}
	

	
	public Integer getBillOfMaterialCode(){
		
		return(billOfMaterialCode);
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public ArrayList getItemNameList() {
		
		return itemNameList;
		
	}
	
	public void setItemNameList(ArrayList itemNameList) {
		
		this.itemNameList = itemNameList;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getQuantityNeeded() {
		
		return quantityNeeded;
		
	}
	
	public void setQuantityNeeded(String quantityNeeded) {
		
		this.quantityNeeded = quantityNeeded;
		
	}
	
	public String getRawCost() {
		
		return rawCost;
		
	}
	
	public void setRawCost(String rawCost) {
		
		this.rawCost = rawCost;
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public void clearUnitList() {
		
		unitList.clear();
		
	}
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemNameEntered() {
		
		return isItemNameEntered;
		
	}
	
	public void setIsItemNameEntered(String isItemNameEntered) {
		
		if (isItemNameEntered != null && isItemNameEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isItemNameEntered = null;
		
	}	
	
	public String getIsLocationEntered() {
		

		return isLocationEntered;
		
	}
	
	public void setIsLocationEntered(String isLocationEntered) {	
		
		if (isLocationEntered != null && isLocationEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isLocationEntered = null;
		
	}
	
	public String getIsUnitEntered() {
		

		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}
	
	
}