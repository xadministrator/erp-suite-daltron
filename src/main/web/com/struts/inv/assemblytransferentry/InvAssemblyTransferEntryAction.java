package com.struts.inv.assemblytransferentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.txn.InvAssemblyTransferEntryController;
import com.ejb.txn.InvAssemblyTransferEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvAssemblyTransferDetails;
import com.util.InvModAssemblyTransferDetails;
import com.util.InvModAssemblyTransferLineDetails;
import com.util.InvModBuildOrderLineDetails;


public final class InvAssemblyTransferEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      InvAssemblyTransferEntryForm actionForm = (InvAssemblyTransferEntryForm)form;
      
      actionForm.setReport(null);
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvAssemblyTransferEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         String frParam = Common.getUserPermission(user, Constants.INV_ASSEMBLY_TRANSFER_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invAssemblyTransferEntry"));
                
               } 
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invAssemblyTransferEntryController EJB
*******************************************************/

         InvAssemblyTransferEntryControllerHome homeATR = null;
         InvAssemblyTransferEntryController ejbATR = null;

         try {
            
            homeATR = (InvAssemblyTransferEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvAssemblyTransferEntryControllerEJB", InvAssemblyTransferEntryControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()){
            
            	log.info("NamingException caught in InvAssemblyTransferEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            	
            }
            
            return(mapping.findForward("cmnErrorPage"));

         }

         try {
         	
            ejbATR = homeATR.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvAssemblyTransferEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call InvAssemblyTransferEntryController EJB
   getInvGpQuantityPrecisionUnit
*******************************************************/
         short precisionUnit = 0;
         short quantityPrecisionUnit = 0;
         boolean isInitialPrinting = false;
                  
         try {
         	
            quantityPrecisionUnit = ejbATR.getInvGpQuantityPrecisionUnit(user.getCmpCode());  
            precisionUnit = ejbATR.getGlFcPrecisionUnit(user.getCmpCode());
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Inv ATR Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvAssemblyTransferDetails details = new InvAssemblyTransferDetails();
           
           details.setAtrCode(actionForm.getAssemblyTransferCode());
           details.setAtrDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setAtrDocumentNumber(actionForm.getDocumentNumber());
           details.setAtrReferenceNumber(actionForm.getReferenceNumber());
           details.setAtrDescription(actionForm.getDescription());
           details.setAtrVoid(Common.convertBooleanToByte(actionForm.isAssembleVoid()));
                              
           if (actionForm.getAssemblyTransferCode() == null) {
           	
               details.setAtrCreatedBy(actionForm.getCreatedBy());
               details.setAtrDateCreated(new java.util.Date());
            
           }
                                 
           details.setAtrLastModifiedBy(user.getUserName());
           details.setAtrDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
       
           ArrayList atlList = new ArrayList(); 
          
           for (int i = 0; i<actionForm.getInvATRListSize(); i++) {
           	
           	   InvAssemblyTransferEntryList invATRList = actionForm.getInvATRByIndex(i);           	   
           	              	             	   
	           if (!invATRList.getAssemble()) continue;
           	   
           	   InvModAssemblyTransferLineDetails mdetails = new InvModAssemblyTransferLineDetails();
           	   
           	   mdetails.setAtlAssembleQuantity(Common.convertStringMoneyToDouble(invATRList.getAssembleQuantity(), quantityPrecisionUnit)); 
           	   mdetails.setAtlBolIlIiName(invATRList.getItemName());
           	   mdetails.setAtlBolIlLocName(invATRList.getLocation());
           	   mdetails.setAtlBolBorDcmntNumber(invATRList.getBuildOrder());
           	   mdetails.setAtlAssembleCost(Common.convertStringMoneyToDouble(invATRList.getAssembleCost(), precisionUnit));
           	   
           	   atlList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	   Integer assemblyTransferCode = ejbATR.saveInvAtrEntry(details, atlList, true, 
           	   	 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	   actionForm.setAssemblyTransferCode(assemblyTransferCode);
           	   
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.documentNumberNotUnique"));
           	    
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPosted"));
                    
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyVoid"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.effectiveDatePeriodClosed"));
           	   
           } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.assemblyQtyGreaterThanAvailableQtyException", ex.getMessage()));
           	   
           } catch (GlobalInventoryDateException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAssemblyTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAssemblyTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invAssemblyTransferEntry.error.noNegativeInventoryCostingCOA"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
               
           }
           
/*******************************************************
   -- Inv ATR Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
         	InvAssemblyTransferDetails details = new InvAssemblyTransferDetails();
            
            details.setAtrCode(actionForm.getAssemblyTransferCode());
            details.setAtrDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setAtrDocumentNumber(actionForm.getDocumentNumber());
            details.setAtrReferenceNumber(actionForm.getReferenceNumber());
            details.setAtrDescription(actionForm.getDescription());
            details.setAtrVoid(Common.convertBooleanToByte(actionForm.isAssembleVoid()));
                               
            if (actionForm.getAssemblyTransferCode() == null) {
            	
                details.setAtrCreatedBy(actionForm.getCreatedBy());
                details.setAtrDateCreated(new java.util.Date());
             
            }
                                  
            details.setAtrLastModifiedBy(user.getUserName());
            details.setAtrDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
        
            ArrayList atlList = new ArrayList(); 
           
            for (int i = 0; i<actionForm.getInvATRListSize(); i++) {
            	
            	   InvAssemblyTransferEntryList invATRList = actionForm.getInvATRByIndex(i);           	   
            	              	             	   
 	           if (!invATRList.getAssemble()) continue;
            	   
            	   InvModAssemblyTransferLineDetails mdetails = new InvModAssemblyTransferLineDetails();
            	   
            	   mdetails.setAtlAssembleQuantity(Common.convertStringMoneyToDouble(invATRList.getAssembleQuantity(), quantityPrecisionUnit)); 
            	   mdetails.setAtlBolIlIiName(invATRList.getItemName());
            	   mdetails.setAtlBolIlLocName(invATRList.getLocation());
            	   mdetails.setAtlBolBorDcmntNumber(invATRList.getBuildOrder());
            	   mdetails.setAtlAssembleCost(Common.convertStringMoneyToDouble(invATRList.getAssembleCost(), precisionUnit));
            	   
            	   atlList.add(mdetails);
            	
            }                     	
            
            try {
            	
            	   Integer assembleCode = ejbATR.saveInvAtrEntry(details, atlList, false,
            	   	 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	   actionForm.setAssemblyTransferCode(assembleCode);
            	   
            } catch (GlobalRecordAlreadyDeletedException ex) {
            	
            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.recordAlreadyDeleted"));
           
            } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invAssemblyTransferEntry.error.documentNumberNotUnique"));
            	
            } catch (GlobalTransactionAlreadyApprovedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyApproved"));
            	
            } catch (GlobalTransactionAlreadyPendingException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPending"));
            	
            } catch (GlobalTransactionAlreadyPostedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPosted"));
           
            } catch (GlobalTransactionAlreadyVoidException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyVoid"));           
                     
            } catch (GlJREffectiveDateNoPeriodExistException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.effectiveDateNoPeriodExist"));
                     
            } catch (GlJREffectiveDatePeriodClosedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAssemblyTransferEntry.error.effectiveDatePeriodClosed"));
            	   
            } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invAssemblyTransferEntry.error.assemblyQtyGreaterThanAvailableQtyException", ex.getMessage()));
               	   
            } catch (GlobalInventoryDateException ex) {
               	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAssemblyTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

            } catch(GlobalBranchAccountNumberInvalidException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invAssemblyTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

            } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
           	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("invAssemblyTransferEntry.error.noNegativeInventoryCostingCOA"));

            } catch (EJBException ex) {
            	    if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                 }
                
                return(mapping.findForward("cmnErrorPage"));
                
            }

/*******************************************************
    -- Inv ATR Journal Action --
*******************************************************/
           	
         } else if (request.getParameter("journalButton") != null &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
             
             if(Common.validateRequired(actionForm.getApprovalStatus())) {
                 
                 InvAssemblyTransferDetails details = new InvAssemblyTransferDetails();
                 
                 details.setAtrCode(actionForm.getAssemblyTransferCode());
                 details.setAtrDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setAtrDocumentNumber(actionForm.getDocumentNumber());
                 details.setAtrReferenceNumber(actionForm.getReferenceNumber());
                 details.setAtrDescription(actionForm.getDescription());
                 details.setAtrVoid(Common.convertBooleanToByte(actionForm.isAssembleVoid()));
                 
                 if (actionForm.getAssemblyTransferCode() == null) {
                     
                     details.setAtrCreatedBy(actionForm.getCreatedBy());
                     details.setAtrDateCreated(new java.util.Date());
                     
                 }
                 
                 details.setAtrLastModifiedBy(user.getUserName());
                 details.setAtrDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                 
                 ArrayList atlList = new ArrayList(); 
                 
                 for (int i = 0; i<actionForm.getInvATRListSize(); i++) {
                     
                     InvAssemblyTransferEntryList invATRList = actionForm.getInvATRByIndex(i);           	   
                     
                     if (!invATRList.getAssemble()) continue;
                     
                     InvModAssemblyTransferLineDetails mdetails = new InvModAssemblyTransferLineDetails();
                     
                     mdetails.setAtlAssembleQuantity(Common.convertStringMoneyToDouble(invATRList.getAssembleQuantity(), quantityPrecisionUnit)); 
                     mdetails.setAtlBolIlIiName(invATRList.getItemName());
                     mdetails.setAtlBolIlLocName(invATRList.getLocation());
                     mdetails.setAtlBolBorDcmntNumber(invATRList.getBuildOrder());
                     mdetails.setAtlAssembleCost(Common.convertStringMoneyToDouble(invATRList.getAssembleCost(), precisionUnit));
                     
                     atlList.add(mdetails);
                     
                 }                     	
                 
                 try {
                     
                     Integer assembleCode = ejbATR.saveInvAtrEntry(details, atlList, true,
                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                     actionForm.setAssemblyTransferCode(assembleCode);
                     
                 } catch (GlobalRecordAlreadyDeletedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.recordAlreadyDeleted"));
                     
                 } catch (GlobalDocumentNumberNotUniqueException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.documentNumberNotUnique"));
                     
                 } catch (GlobalTransactionAlreadyApprovedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyApproved"));
                     
                 } catch (GlobalTransactionAlreadyPendingException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPending"));
                     
                 } catch (GlobalTransactionAlreadyPostedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPosted"));
                     
                 } catch (GlobalTransactionAlreadyVoidException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyVoid"));           
                     
                 } catch (GlJREffectiveDateNoPeriodExistException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.effectiveDateNoPeriodExist"));
                     
                 } catch (GlJREffectiveDatePeriodClosedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.effectiveDatePeriodClosed"));
                     
                 } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.assemblyQtyGreaterThanAvailableQtyException", ex.getMessage()));
                     
                 } catch (GlobalInventoryDateException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
                     
                 } catch(GlobalBranchAccountNumberInvalidException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
                	
                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invAssemblyTransferEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {
                         
                         log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }
                     
                     return(mapping.findForward("cmnErrorPage"));
                     
                 }
                 
                 if (!errors.isEmpty()) {
                     
                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("invAssemblyTransferEntry"));
                     
                 }
             }
             
             String path = "/invJournal.do?forward=1" + 
             "&transactionCode=" + actionForm.getAssemblyTransferCode() +
             "&transaction=ASSEMBLY TRANSFER" + 
             "&transactionNumber=" + actionForm.getDocumentNumber() + 
             "&transactionDate=" + actionForm.getDate() +
             "&transactionEnableFields=" + actionForm.getEnableFields();
             
             return(new ActionForward(path));
     	      	
/*******************************************************
   -- Inv ATR Print Action --
*******************************************************/             	
                   	
         } else if (request.getParameter("printButton") != null) {
             
             if(Common.validateRequired(actionForm.getApprovalStatus())) {
                 
                 InvAssemblyTransferDetails details = new InvAssemblyTransferDetails();
                 
                 details.setAtrCode(actionForm.getAssemblyTransferCode());
                 details.setAtrDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setAtrDocumentNumber(actionForm.getDocumentNumber());
                 details.setAtrReferenceNumber(actionForm.getReferenceNumber());
                 details.setAtrDescription(actionForm.getDescription());
                 details.setAtrVoid(Common.convertBooleanToByte(actionForm.isAssembleVoid()));
                 
                 if (actionForm.getAssemblyTransferCode() == null) {
                     
                     details.setAtrCreatedBy(actionForm.getCreatedBy());
                     details.setAtrDateCreated(new java.util.Date());
                     
                 }
                 
                 details.setAtrLastModifiedBy(user.getUserName());
                 details.setAtrDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                 
                 ArrayList atlList = new ArrayList(); 
                 
                 for (int i = 0; i<actionForm.getInvATRListSize(); i++) {
                     
                     InvAssemblyTransferEntryList invATRList = actionForm.getInvATRByIndex(i);           	   
                     
                     if (!invATRList.getAssemble()) continue;
                     
                     InvModAssemblyTransferLineDetails mdetails = new InvModAssemblyTransferLineDetails();
                     
                     mdetails.setAtlAssembleQuantity(Common.convertStringMoneyToDouble(invATRList.getAssembleQuantity(), quantityPrecisionUnit)); 
                     mdetails.setAtlBolIlIiName(invATRList.getItemName());
                     mdetails.setAtlBolIlLocName(invATRList.getLocation());
                     mdetails.setAtlBolBorDcmntNumber(invATRList.getBuildOrder());
                     mdetails.setAtlAssembleCost(Common.convertStringMoneyToDouble(invATRList.getAssembleCost(), precisionUnit));
                     
                     atlList.add(mdetails);
                     
                 }                     	
                 
                 try {
                     
                     Integer assembleCode = ejbATR.saveInvAtrEntry(details, atlList, true,
                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                     actionForm.setAssemblyTransferCode(assembleCode);
                     
                 } catch (GlobalRecordAlreadyDeletedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.recordAlreadyDeleted"));
                     
                 } catch (GlobalDocumentNumberNotUniqueException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.documentNumberNotUnique"));
                     
                 } catch (GlobalTransactionAlreadyApprovedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyApproved"));
                     
                 } catch (GlobalTransactionAlreadyPendingException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPending"));
                     
                 } catch (GlobalTransactionAlreadyPostedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyPosted"));
                     
                 } catch (GlobalTransactionAlreadyVoidException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.transactionAlreadyVoid"));           
                     
                 } catch (GlJREffectiveDateNoPeriodExistException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.effectiveDateNoPeriodExist"));
                     
                 } catch (GlJREffectiveDatePeriodClosedException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.effectiveDatePeriodClosed"));
                     
                 } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.assemblyQtyGreaterThanAvailableQtyException", ex.getMessage()));
                     
                 } catch (GlobalInventoryDateException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
                     
                 } catch(GlobalBranchAccountNumberInvalidException ex) {
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invAssemblyTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
                	
                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invAssemblyTransferEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {
                         
                         log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }
                     
                     return(mapping.findForward("cmnErrorPage"));
                     
                 }
                 
                 if (!errors.isEmpty()) {
                     
                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("invAssemblyTransferEntry"));
                     
                 }
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 isInitialPrinting = true;
                 
             }  else {
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 return(mapping.findForward("invAssemblyTransferEntry"));
                 
             }
         	      	
/*******************************************************
   -- Inv ATR Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbATR.deleteInvAtrEntry(actionForm.getAssemblyTransferCode(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAssemblyTransferEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
        
/*******************************************************
   -- Inv ATR Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            
/*******************************************************
    -- Inv ATR Assemble Qty Entered Action --
*******************************************************/
            
         } else if(!Common.validateRequired(request.getParameter("invATRList[" + 
                 actionForm.getRowSelected() + "].isAssembleQtyEntered")) &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
             
             InvAssemblyTransferEntryList invATRList = actionForm.getInvATRByIndex(actionForm.getRowSelected());
             
             try {
                 
                 /// populate assemble cost
                 if (!Common.validateRequired(invATRList.getBuildOrder()) && 
                         !Common.validateRequired(invATRList.getItemName()) && 
                         !Common.validateRequired(invATRList.getLocation())) {
                     
                     // set assemble cost
                     double assembleCost = ejbATR.getAssembleCostByBorDocumentNumberAndIiNameAndLocNameAndAssembleQty(
                             invATRList.getBuildOrder(), invATRList.getItemName(), invATRList.getLocation(), 
                             Common.convertStringMoneyToDouble(invATRList.getAssembleQuantity(), quantityPrecisionUnit),
                             user.getCmpCode());
                     
                     invATRList.setAssembleCost(Common.convertDoubleToStringMoney(assembleCost, precisionUnit));
                     
                 }
                 
             } catch (EJBException ex) {
                 
                 if (log.isInfoEnabled()) {
                     
                     log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage"); 
                     
                 }
                 
             }  
             
             return(mapping.findForward("invAssemblyTransferEntry"));
             
/*******************************************************
    -- Inv ATR Load Action --
*******************************************************/
             
         }
      
         if (frParam != null) {
         	
         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		return(mapping.findForward("invAssemblyTransferEntry"));
         		
         	}
         	
         	if (request.getParameter("forward") == null &&
         			actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
         		saveErrors(request, new ActionMessages(errors));
         		
         		return mapping.findForward("cmnMain");	
         		
         	}
         	
         	try {
         		
         		ArrayList list = null;
         		Iterator i = null;
         		
         		actionForm.clearInvATRList();           	
         		
         		if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setAssemblyTransferCode(new Integer(request.getParameter("assemblyTransferCode")));
            			
            		}
         				         			
         			InvModAssemblyTransferDetails mdetails = ejbATR.getInvAtrByAtrCode(actionForm.getAssemblyTransferCode(), user.getCmpCode());
         			
         			actionForm.setDate(Common.convertSQLDateToString(mdetails.getAtrDate()));
         			actionForm.setDocumentNumber(mdetails.getAtrDocumentNumber());
         			actionForm.setReferenceNumber(mdetails.getAtrReferenceNumber());
         			actionForm.setDescription(mdetails.getAtrDescription());
         			actionForm.setAssembleVoid(Common.convertByteToBoolean(mdetails.getAtrVoid()));
         			actionForm.setApprovalStatus(mdetails.getAtrApprovalStatus());
         			actionForm.setReasonForRejection(mdetails.getAtrReasonForRejection());
         			actionForm.setPosted(mdetails.getAtrPosted() == 1 ? "YES" : "NO");
         			actionForm.setCreatedBy(mdetails.getAtrCreatedBy());
         			actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getAtrDateCreated()));
         			actionForm.setLastModifiedBy(mdetails.getAtrLastModifiedBy());
         			actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getAtrDateLastModified()));            		
         			actionForm.setApprovedRejectedBy(mdetails.getAtrApprovedRejectedBy());
         			actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getAtrDateApprovedRejected()));
         			actionForm.setPostedBy(mdetails.getAtrPostedBy());
         			actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getAtrDatePosted()));
         			
         			list = mdetails.getAtrAtlList();
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				InvModAssemblyTransferLineDetails mAtldetails = (InvModAssemblyTransferLineDetails)i.next();
         				
         				InvAssemblyTransferEntryList atrList = new InvAssemblyTransferEntryList(
         					actionForm, mAtldetails.getAtlBolIlIiName(), mAtldetails.getAtlBolIlLocName(),
							mAtldetails.getAtlBolIlIiUomName(), mAtldetails.getAtlBolBorDcmntNumber(), 
							Common.convertDoubleToStringMoney(mAtldetails.getAtlBolQtyRequired(), quantityPrecisionUnit),
							Common.convertDoubleToStringMoney(mAtldetails.getAtlBolQtyAssembled(), quantityPrecisionUnit),
							Common.convertDoubleToStringMoney(mAtldetails.getAtlAssembleQuantity(), quantityPrecisionUnit),
							Common.convertDoubleToStringMoney(mAtldetails.getAtlAssembleCost(), precisionUnit)); 	
         				
         				atrList.setAssemble(true);
         				actionForm.saveInvATRList(atrList); 
         				
         			}
         			
         			this.setFormProperties(actionForm); 
         			
                    if (actionForm.getEnableFields()) {				        			        				        
         				
         				try {
         					
         					list = ejbATR.getInvCompleteBol(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         					
         					i = list.iterator();	                 
         					
         					while (i.hasNext()) {
         						
         						InvModBuildOrderLineDetails mBolDetails = (InvModBuildOrderLineDetails)i.next();
         						     						
         						InvAssemblyTransferEntryList invATRList = new InvAssemblyTransferEntryList(
         							actionForm, mBolDetails.getBolIlIiName(), mBolDetails.getBolIlLocName(),
         							mBolDetails.getBolIlIiUomName(), mBolDetails.getBolBorDocumentNumber(), 
    								Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityRequired(), quantityPrecisionUnit),
    								Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityAssembled(), quantityPrecisionUnit),
    								Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit), null);
         						     						
         						actionForm.saveInvATRList(invATRList);
         						
         					}
         				} catch (GlobalNoRecordFoundException ex) {
         					
         				}
         			}
             		
             		return (mapping.findForward("invAssemblyTransferEntry"));
             		
                }               		
         	} catch(GlobalNoRecordFoundException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invAssemblyTransferEntry.error.noRecordFound"));
         		
         	} catch(EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in InvAssemblyTransferEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	} 
         	
         	
         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		
         	} else {
         		
         		if ((request.getParameter("saveSubmitButton") != null || request.getParameter("saveAsDraftButton") != null) && 
         				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         			
         			actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
         			
         		}
         	}
         	
         	actionForm.reset(mapping, request);              
         	actionForm.setAssemblyTransferCode(null);          
         	actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
         	actionForm.setPosted("NO");
         	actionForm.setApprovalStatus(null);
         	actionForm.setCreatedBy(user.getUserName());
         	actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
         	actionForm.setLastModifiedBy(user.getUserName());
         	actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
         	
         	try {
					
				ArrayList list = ejbATR.getInvCompleteBol(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
				
				Iterator i = list.iterator();	                 
				
				while (i.hasNext()) {
					
					InvModBuildOrderLineDetails mBolDetails = (InvModBuildOrderLineDetails)i.next();
					     						
					InvAssemblyTransferEntryList invATRList = new InvAssemblyTransferEntryList(
						actionForm, mBolDetails.getBolIlIiName(), mBolDetails.getBolIlLocName(),
						mBolDetails.getBolIlIiUomName(), mBolDetails.getBolBorDocumentNumber(), 
					Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityRequired(), quantityPrecisionUnit),
					Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityAssembled(), quantityPrecisionUnit),
					Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit), null);
					     						
					actionForm.saveInvATRList(invATRList);
					
				}
				
			} catch (GlobalNoRecordFoundException ex) {
				
			} catch(EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	} 
         	
         	this.setFormProperties(actionForm);
         	return(mapping.findForward("invAssemblyTransferEntry"));
         	
         } else {
         	
         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
         	saveErrors(request, new ActionMessages(errors));
         	
         	return(mapping.findForward("cmnMain"));
         	
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in InvAssemblyTransferEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
    }

	private void setFormProperties(InvAssemblyTransferEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getAssemblyTransferCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setEnableVoid(false);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(false);
					
				} else if (actionForm.getAssemblyTransferCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setEnableVoid(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);   
				    	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setEnableVoid(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);
					
				}
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setEnableVoid(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				
			}
		} else {
			
			actionForm.setEnableFields(false);
			actionForm.setEnableVoid(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
		
		}    
	}
}