package com.struts.inv.assemblytransferentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvAssemblyTransferEntryForm extends ActionForm implements Serializable {

	private Integer assemblyTransferCode = null;
	private String date = null;
	private String referenceNumber = null;
	private String documentNumber = null;
	private String description = null;
	private boolean assembleVoid = false;
	private String approvalStatus = null;
	private String posted = null;
	private String reasonForRejection = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	
	private ArrayList invATRList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean enableVoid = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	
	private String report = null;
    	
	public InvAssemblyTransferEntryList getInvATRByIndex(int index){
		
		return((InvAssemblyTransferEntryList)invATRList.get(index));
		
	}

	public Object[] getInvATRList(){
		
		return(invATRList.toArray());
		
	}

	public int getInvATRListSize(){
		
		return(invATRList.size());
		
	}

	public void saveInvATRList(Object newInvATRList){
		
		invATRList.add(newInvATRList);
		
	}

	public void clearInvATRList(){
		
		invATRList.clear();
		
	}

	public void deleteInvATRList(int rowSelected){
	
		invATRList.remove(rowSelected);

	}

	public int getRowSelected(){
		
		return rowSelected;
		
	}
	
	public void setRowSelected(Object selectedInvATRList, boolean isEdit){
	
		this.rowSelected = invATRList.indexOf(selectedInvATRList);
	
	}

	public void updateInvATRRow(int rowSelected, Object newInvATRList){
		
		invATRList.set(rowSelected, newInvATRList);
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public String getTxnStatus(){
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus){
		
		this.txnStatus = txnStatus;
		
	} 
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}

	public Integer getAssemblyTransferCode() {
		
		return assemblyTransferCode;
		
	}
	
	public void setAssemblyTransferCode(Integer assemblyTransferCode) {
		
		this.assemblyTransferCode = assemblyTransferCode;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		 
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public boolean isAssembleVoid() {
	
		return assembleVoid;
		
	}
	
	public void setAssembleVoid(boolean assembleVoid) {
	
		this.assembleVoid = assembleVoid;
		
	}
	
	public String getApprovalStatus() {
	
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getReasonForRejection() {
		
		return reasonForRejection;
		
	}
	
	public void setReasonForRejection(String reasonForRejection) {
		
		this.reasonForRejection = reasonForRejection;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getDateCreated() {
		
		return dateCreated;
		
	}
	
	public void setDateCreated(String dateCreated) {
		
		this.dateCreated = dateCreated;
		
	}
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}
	
	public String getDateLastModified() {
		
		return dateLastModified;
		
	}
	
	public void setDateLastModified(String dateLastModified) {
		
		this.dateLastModified = dateLastModified;
		
	}
		
	public String getApprovedRejectedBy() {
	
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}
	
	public String getDateApprovedRejected() {
	
		return dateApprovedRejected;
		
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		
		this.dateApprovedRejected = dateApprovedRejected;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}
	
	public String getDatePosted() {
	
		return datePosted;
		
	}
	
	public void setDatePosted(String datePosted) {
	
		this.datePosted = datePosted;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getEnableVoid() {
		
		return enableVoid;
		
	}
	
	public void setEnableVoid(boolean enableVoid) {
		
		this.enableVoid = enableVoid;
		
	}
	
	public boolean isShowSaveButton() {
	
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       date = null;
       documentNumber = null;
	   referenceNumber = null;
	   description = null;
	   assembleVoid = false;
	   approvalStatus = null;
	   posted = null;
	   reasonForRejection = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   
	   Iterator i = invATRList.iterator();
	   
	   while (i.hasNext()) {
	   	
	   	InvAssemblyTransferEntryList list = (InvAssemblyTransferEntryList)i.next();
	   	
	   	list.setAssemble(false);
	   	
	   } 
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
		 request.getParameter("journalButton") != null ||
		 request.getParameter("printButton") != null) { 

         if(Common.validateRequired(date)){
            
      	 	errors.add("date",
               new ActionMessage("invAssemblyTransferEntry.error.dateRequired"));
         
      	 }
         
		 if(!Common.validateDateFormat(date)){
	     
		 	errors.add("date", 
		       new ActionMessage("invAssemblyTransferEntry.error.dateInvalid"));
		 
		 }	 	         
		 
		 if((!Common.validateDateGreaterThanCurrent(date) || !Common.validateDateLessThanCurrent(date)) && Common.validateRequired(approvalStatus)) {
            
		 	errors.add("date", 
                new ActionMessage("invAssemblyTransferEntry.error.dateMustNotBeGreaterThanOrLessThanCurrentDate"));
         
		 }
         
         int numberOfLines = 0;
      	 
      	 Iterator i = invATRList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvAssemblyTransferEntryList ilList = (InvAssemblyTransferEntryList)i.next();      	 	 
			 
      	 	 if (!ilList.getAssemble()) continue;
      	 	     
      	 	 numberOfLines++;
      	 
      	 	 if(Common.validateRequired(ilList.getAssembleQuantity())){
   	         
   	         	errors.add("assembleQuantity",
   	            	new ActionMessage("invAssemblyTransferEntry.error.assembleQuantityRequired", String.valueOf(numberOfLines)));
   	         
   	         }
      	 	 
      	 	if(!Common.validateNumberFormat(ilList.getAssembleQuantity())){
      	         
      	       	errors.add("assembleQuantity",
      	           	new ActionMessage("invAssemblyTransferEntry.error.assembleQuantityInvalid", String.valueOf(numberOfLines)));
      	         
      	    }
      	 	
      	 	if(!Common.validateRequired(ilList.getAssembleQuantity()) && Common.convertStringMoneyToDouble(ilList.getAssembleQuantity(), (short)3) <= 0) {
	         	errors.add("assembleQuantity",
	         		new ActionMessage("invAssemblyTransferEntry.error.negativeOrZeroAssembleQuantityNotAllowed", ilList.getItemName()));
	         }
      	 	
      	 	if(!Common.validateDecimalExist(ilList.getAssembleQuantity()) && Common.validateNumberFormat(ilList.getAssembleQuantity())){
		        errors.add("assembleQuantity",
		           new ActionMessage("invAssemblyTransferEntry.error.decimalAssembleQuantityNotAllowed", ilList.getItemName()));
		     }
      	 	
      	 	if (Common.validateRequired(approvalStatus)) {
      	 	
	      	 	if (!Common.validateRequired(ilList.getAssembleQuantity()) &&
	    		 		Common.validateNumberFormat(ilList.getAssembleQuantity()) &&
	    				Common.convertStringMoneyToDouble(ilList.getAssembleQuantity(), (short)3) > Common.convertStringMoneyToDouble(ilList.getQuantityRequired(), (short)3) - Common.convertStringMoneyToDouble(ilList.getQuantityAssembled(), (short)3)) {
	    		 		
	    		 		errors.add("assembleQuantity",
	    			         new ActionMessage("invAssemblyTransferEntry.error.assembleQuantityGreaterThanRequired", String.valueOf(numberOfLines)));
	    		 		
	    		}
	      	 	
      	 	}
        }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("assemblytransfer",
               new ActionMessage("invAssemblyTransferEntry.error.assemblyTransferMustHaveLine"));
         	
        }	  	    
      } 
      
      return(errors);
      
   }	
}
