package com.struts.inv.assemblytransferentry;

import java.io.Serializable;

public class InvAssemblyTransferEntryList implements Serializable {

	private String itemName = null;
	private String location = null;
	private String unit = null;
	private String buildOrder = null;
	private String quantityRequired = null;
	private String quantityAssembled = null;
	private String assembleQuantity = null;
	private boolean assemble = false;
	
	private String assembleCost = null;
	private String isAssembleQtyEntered = null;
	
	private InvAssemblyTransferEntryForm parentBean;   

	public InvAssemblyTransferEntryList(InvAssemblyTransferEntryForm parentBean,
		String itemName, String location, String unit, String buildOrder,
		String quantityRequired, String quantityAssembled, String assembleQuantity,
		String assembleCost) {
	
		this.parentBean = parentBean;
		this.itemName = itemName;
		this.location = location;
		this.unit = unit;
		this.buildOrder = buildOrder;
		this.quantityRequired = quantityRequired;
		this.quantityAssembled = quantityAssembled;
		this.assembleQuantity = assembleQuantity;
		this.assembleCost = assembleCost;
		
	}
	
	public InvAssemblyTransferEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvAssemblyTransferEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
		
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public String getBuildOrder() {
	
		return buildOrder;
		
	}
	
	public void setBuildOrder(String buildOrder) {
	
		this.buildOrder = buildOrder;
		
	}
	
	public String getQuantityRequired() {
		
		return quantityRequired;
		
	}
	
	public void setQuantityRequired(String quantityRequired) {
		
		this.quantityRequired = quantityRequired;
		
	}
	
	public String getQuantityAssembled() {
		
		return quantityAssembled;
		
	}
	
	public void setQuantityAssembled(String quantityAssembled) {
		
		this.quantityAssembled = quantityAssembled;
		
	}
	
	public String getAssembleQuantity() {
		
		return assembleQuantity;
		
	}
	
	public void setAssembleQuantity(String assembleQuantity) {
		
    	this.assembleQuantity = assembleQuantity;
	
    }
	
	public boolean getAssemble() {
		
		return assemble;
		
	}
	
	public void setAssemble(boolean assemble) {
		
		this.assemble = assemble;
		
	}
	
	public String getAssembleCost() {
		
		return assembleCost;
		
	}
	
	public void setAssembleCost(String assembleCost) {
		
		this.assembleCost = assembleCost;
		
	}
	
	public String getIsAssembleQtyEntered(){
	    return isAssembleQtyEntered;
	}
	
	public void setIsAssembleQtyEntered(String isAssembleQtyEntered){
	    
	    if (isAssembleQtyEntered != null && isAssembleQtyEntered.equals("true")) {
	        
	        parentBean.setRowSelected(this, false);
	        
	    }				
	    
	    isAssembleQtyEntered = null;
	}
	
}
