package com.struts.inv.branchstocktransferinentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.exception.InvTagSerialNumberNotFoundException;
import com.ejb.txn.InvBranchStockTransferInEntryController;
import com.ejb.txn.InvBranchStockTransferInEntryControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.InvModBranchStockTransferDetails;
import com.util.InvModBranchStockTransferLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public class InvBranchStockTransferInEntryAction extends Action{

	private org.apache.commons.logging.Log log =
		org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,HttpServletRequest request,
		HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		InvBranchStockTransferInEntryForm actionForm = (InvBranchStockTransferInEntryForm)form;

		actionForm.setReport(null);
		actionForm.setAttachment(null);
		try{

/*******************************************************
   Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("InvStockTransferEntryAction: Company '" + user.getCompany() +
						"' User '" + user.getUserName() + "' performed this action on session " + session.getId());

				}

			}else{

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			actionForm.setAttachment(null);
			actionForm.setAttachmentPDF(null);
			String frParam = Common.getUserPermission(user, Constants.INV_BRANCH_STOCK_TRANSFER_IN_ENTRY_ID);

			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {


					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));
						return(mapping.findForward("invBranchStockTransferInEntry"));

					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

/*************************************************************
     Initialize invBranchStockTransferInEntryController EJB
*************************************************************/

			InvBranchStockTransferInEntryControllerHome homeBST = null;
			InvBranchStockTransferInEntryController ejbBST = null;
			
			  InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;
         

			try {

				homeBST = (InvBranchStockTransferInEntryControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/InvBranchStockTransferInEntryControllerEJB",
							InvBranchStockTransferInEntryControllerHome.class);
			
			homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);


			} catch(NamingException e) {

				if(log.isInfoEnabled()){

					log.info("NamingException caught in InvBranchStockTransferInEntryAction.execute(): " +
						e.getMessage() + " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			try {

				ejbBST = homeBST.create();
				
				ejbII = homeII.create();

			} catch(CreateException e) {

				if(log.isInfoEnabled()) {

					log.info("CreateException caught in InvBranchStockTransferInEntryAction.execute(): " +
						e.getMessage() + " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			ActionErrors errors = new ActionErrors();
			ActionMessages messages = new ActionMessages();

/*******************************************************
   Call invBranchStockTransferInEntryController EJB
   getGlFcPrecisionUnit
   getInvGpQuantityPrecisionUnit
   getInvGpInventoryLineNumber
*******************************************************/
			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
			short precisionUnit = 0;
			short quantityPrecisionUnit = 0;
			short branchStockTransferInLineNumber = 0;
			boolean isInitialPrinting = false;
			String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/inv/BSTI/";
     		long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
			String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
			String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
			try {

				precisionUnit = ejbBST.getGlFcPrecisionUnit(user.getCmpCode());
				quantityPrecisionUnit = ejbBST.getInvGpQuantityPrecisionUnit(user.getCmpCode());
				branchStockTransferInLineNumber = ejbBST.getInvGpInventoryLineNumber(user.getCmpCode());

			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
						ex.getMessage() + " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}
			boolean branchFromIsEqualToBranchTo = actionForm.getBranchFrom() != null
				&& user.getCurrentBranch() != null ? actionForm.getBranchFrom().equals(user.getCurrentBranch().getBrName()): false;

			if (branchFromIsEqualToBranchTo) {

				errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("invBranchStockTransferEntry.error.branchFromMustNotBeEqualToCurrentBranch"));

			}
/*******************************************************
	   -- Inv BST Save As Draft Action --
*******************************************************/

			if (request.getParameter("saveAsDraftButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

				details.setBstCode(actionForm.getBranchStockTransferCode());
				details.setBstNumber(actionForm.getTransferInNumber());
				details.setBstTransferOutNumber(actionForm.getTransferOutNumber());
				details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setBstBranchFrom(actionForm.getBranchFrom());
				details.setBstTransitLocation(actionForm.getTransitLocation());

				if (actionForm.getBranchStockTransferCode() == null) {

					details.setBstCreatedBy(user.getUserName());
					details.setBstDateCreated(
						Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

				}

				details.setBstLastModifiedBy(user.getUserName());
				details.setBstDateLastModified(
					Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				details.setBstDescription(actionForm.getDescription());

				// line items
				ArrayList bslList = new ArrayList();

				for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

					InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

					if (Common.validateRequired(invBSTList.getItemName()) &&
							Common.validateRequired(invBSTList.getLocation()) &&
							Common.validateRequired(invBSTList.getQuantity()) &&
							Common.validateRequired(invBSTList.getUnit())) continue;

					InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

					mdetails.setBslCode(invBSTList.getBranchStockTransferInLineCode());
					mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
					mdetails.setBslIiName(invBSTList.getItemName());
					mdetails.setBslLocationName(invBSTList.getLocation());
					mdetails.setBslQuantity(
						Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
					mdetails.setBslQuantityReceived(
						Common.convertStringMoneyToDouble(invBSTList.getQuantityReceived(), quantityPrecisionUnit));
					mdetails.setBslUomName(invBSTList.getUnit());
					mdetails.setBslUnitCost(Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
					mdetails.setBslIiDescription(invBSTList.getItemDescription());
					mdetails.setBslAmount(Common.convertStringMoneyToDouble(invBSTList.getAmount(), precisionUnit));
					mdetails.setBslShippingCost(Common.convertStringMoneyToDouble(invBSTList.getShippingCost(), precisionUnit));
					mdetails.setBslMisc(invBSTList.getMisc());



				   ArrayList tagList = new ArrayList();
	         	   //TODO:save and submit taglist
	         	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());

					String misc= invBSTList.getMisc();

		      	   if (isTraceMisc){



		      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
		      		   mdetails.setBslTagList(tagList);

		      	   }






					bslList.add(mdetails);

				}

					// validate attachment

				String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
				String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
				String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
				String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

				if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   	}
				try {

					ejbBST.saveInvBstEntry(details, bslList, true, actionForm.getType(), new Integer(user.getCurrentBranch().getBrCode()),
						user.getCmpCode());

				} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.quantityReceivedOverApplication"));

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

				} catch (GlobalTransactionAlreadyApprovedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

				} catch (GlobalTransactionAlreadyPendingException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

				} catch (GlobalTransactionAlreadyPostedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

				} catch (GlobalInvItemLocationNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound", ex.getMessage()));

				}catch (GlobalInvTagMissingException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagInventoriableError", ex.getMessage()));

				}catch (InvTagSerialNumberNotFoundException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagSerialNumberNotFoundError", ex.getMessage()));

				}catch (GlobalInvTagExistingException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagNonInventoriableError", ex.getMessage()));

				} catch (GlJREffectiveDateNoPeriodExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

				} catch (GlobalJournalNotBalanceException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));

				} catch (GlobalInventoryDateException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
							ex.getMessage()));

				} catch(GlobalBranchAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
							ex.getMessage()));

				/*} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyAssigned"));
*/
				}catch (GlobalExpiryDateNotFoundException ex) {

		    			errors.add(ActionMessages.GLOBAL_MESSAGE,
		    					new ActionMessage("errors.MiscInvalid", ex.getMessage()));

		    	}catch (GlobalMiscInfoIsRequiredException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	            } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
							ex.getMessage() + " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}
				// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

/********************************************************
   -- Inv BST Save & Submit Action --
*******************************************************/

			} else if (request.getParameter("saveSubmitButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

				details.setBstCode(actionForm.getBranchStockTransferCode());
				//Header tab fields
				details.setBstNumber(actionForm.getTransferInNumber());
				details.setBstTransferOutNumber(actionForm.getTransferOutNumber());
				details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setBstBranchFrom(actionForm.getBranchFrom());
				details.setBstTransitLocation(actionForm.getTransitLocation());

				System.out.println(actionForm.getBranchFrom() + " <= getBranchFrom from save and submit action");
				if (actionForm.getBranchStockTransferCode() == null) {

					details.setBstCreatedBy(user.getUserName());
					details.setBstDateCreated(
						Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

				}

				details.setBstLastModifiedBy(user.getUserName());
				details.setBstDateLastModified(
					Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				details.setBstDescription(actionForm.getDescription());

				ArrayList bslList = new ArrayList();

				for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

					InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

					if (Common.validateRequired(invBSTList.getItemName()) &&
							Common.validateRequired(invBSTList.getLocation()) &&
							Common.validateRequired(invBSTList.getQuantity()) &&
							Common.validateRequired(invBSTList.getUnit())) continue;

					InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

					mdetails.setBslCode(invBSTList.getBranchStockTransferInLineCode());
					mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
					mdetails.setBslIiName(invBSTList.getItemName());
					mdetails.setBslLocationName(invBSTList.getLocation());
					mdetails.setBslQuantity(Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
					mdetails.setBslQuantityReceived(Common.convertStringMoneyToDouble(invBSTList.getQuantityReceived(), quantityPrecisionUnit));
					mdetails.setBslUomName(invBSTList.getUnit());
					mdetails.setBslUnitCost(Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
					mdetails.setBslIiDescription(invBSTList.getItemDescription());
					mdetails.setBslAmount(Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
					mdetails.setBslShippingCost(Common.convertStringMoneyToDouble(invBSTList.getShippingCost(), precisionUnit));
					mdetails.setBslMisc(invBSTList.getMisc());

					 ArrayList tagList = new ArrayList();
		         	   //TODO:save and submit taglist
		         	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());

						String misc= invBSTList.getMisc();

			      	   if (isTraceMisc){



			      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
			      		   mdetails.setBslTagList(tagList);

			      	   }

					bslList.add(mdetails);

				}
				// validate attachment

				String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
				String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
				String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
				String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferInEntry"));

		   	    	}

		   	   	}

				try {

					Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, false, actionForm.getType(),
						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					actionForm.setBranchStockTransferCode(branchStockTransferCode);

				} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.quantityReceivedOverApplication"));

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

				} catch (GlobalTransactionAlreadyApprovedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

				} catch (GlobalTransactionAlreadyPendingException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

				} catch (GlobalTransactionAlreadyPostedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

				} catch (GlobalInvItemLocationNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound", ex.getMessage()));

				}catch (GlobalInvTagMissingException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagInventoriableError", ex.getMessage()));

				}catch (InvTagSerialNumberNotFoundException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagSerialNumberNotFoundError", ex.getMessage()));

				}catch (GlobalInvTagExistingException ex) {

		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invBranchStockTransferEntry.error.invTagNonInventoriableError", ex.getMessage()));

				} catch (GlJREffectiveDateNoPeriodExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

				} catch (GlobalJournalNotBalanceException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));

				} catch (GlobalInventoryDateException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
							ex.getMessage()));

				} catch(GlobalBranchAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
							ex.getMessage()));

				/*} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyAssigned"));*/

				} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

				} catch (GlobalExpiryDateNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("errors.MiscInvalid", ex.getMessage()));

				}catch (GlobalMiscInfoIsRequiredException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
							ex.getMessage() + " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));
				}
				// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }


/*******************************************************
   -- Inv BST Journal Action --
********************************************************/

			} else if (request.getParameter("journalButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				if(Common.validateRequired(actionForm.getApprovalStatus())) {

					InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

					details.setBstCode(actionForm.getBranchStockTransferCode());
					//Header tab fields
					details.setBstNumber(actionForm.getTransferInNumber());
					details.setBstTransferOutNumber(actionForm.getTransferOutNumber());
					details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
					details.setBstBranchFrom(actionForm.getBranchFrom());
					details.setBstTransitLocation(actionForm.getTransitLocation());

					if (actionForm.getBranchStockTransferCode() == null) {

						details.setBstCreatedBy(user.getUserName());
						details.setBstDateCreated(
								Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

					}

					details.setBstLastModifiedBy(user.getUserName());
					details.setBstDateLastModified(
							Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					details.setBstDescription(actionForm.getDescription());

					ArrayList bslList = new ArrayList();

					for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

						InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

						if (Common.validateRequired(invBSTList.getItemName()) &&
								Common.validateRequired(invBSTList.getLocation()) &&
								Common.validateRequired(invBSTList.getQuantity()) &&
								Common.validateRequired(invBSTList.getUnit())) continue;

						InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

						mdetails.setBslCode(invBSTList.getBranchStockTransferInLineCode());
						mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
						mdetails.setBslIiName(invBSTList.getItemName());
						mdetails.setBslLocationName(invBSTList.getLocation());
						mdetails.setBslQuantity(Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
						mdetails.setBslQuantityReceived(Common.convertStringMoneyToDouble(invBSTList.getQuantityReceived(), quantityPrecisionUnit));
						mdetails.setBslUomName(invBSTList.getUnit());
						mdetails.setBslUnitCost(Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
						mdetails.setBslIiDescription(invBSTList.getItemDescription());
						mdetails.setBslAmount(Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
						mdetails.setBslShippingCost(Common.convertStringMoneyToDouble(invBSTList.getShippingCost(), precisionUnit));
						mdetails.setBslMisc(invBSTList.getMisc());

						 ArrayList tagList = new ArrayList();
			         	   //TODO:save and submit taglist
			         	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());

							String misc= invBSTList.getMisc();

				      	   if (isTraceMisc){



				      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
				      		   mdetails.setBslTagList(tagList);

				      	   }
						bslList.add(mdetails);

					}
					// validate attachment

					String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
					String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
					String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
					String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

					if (!Common.validateRequired(filename1)) {

			   	    	if (actionForm.getFilename1().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename1().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename2)) {

			   	    	if (actionForm.getFilename2().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename2().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename3)) {

			   	    	if (actionForm.getFilename3().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename3().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename4)) {

			   	    	if (actionForm.getFilename4().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename4().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   	}

					try {

						Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, true, actionForm.getType(),
								new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setBranchStockTransferCode(branchStockTransferCode);

					} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.quantityReceivedOverApplication"));

					} catch (GlobalDocumentNumberNotUniqueException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

					} catch (GlobalRecordAlreadyDeletedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

					} catch (GlobalTransactionAlreadyApprovedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

					} catch (GlobalTransactionAlreadyPendingException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

					} catch (GlobalTransactionAlreadyPostedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

					} catch (GlobalInvItemLocationNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound",
										ex.getMessage()));

					}catch (GlobalInvTagMissingException ex) {

			           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                    new ActionMessage("invBranchStockTransferEntry.error.invTagInventoriableError", ex.getMessage()));

					}catch (GlobalInvTagExistingException ex) {

			           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                    new ActionMessage("invBranchStockTransferEntry.error.invTagNonInventoriableError", ex.getMessage()));

					} catch (GlJREffectiveDateNoPeriodExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

					} catch (GlJREffectiveDatePeriodClosedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

					} catch (GlobalJournalNotBalanceException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));

					} catch (GlobalInventoryDateException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
										ex.getMessage()));

					} catch(GlobalBranchAccountNumberInvalidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
										ex.getMessage()));

					/*} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyAssigned"));*/

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

		             }catch (GlobalExpiryDateNotFoundException ex) {

			    			errors.add(ActionMessages.GLOBAL_MESSAGE,
			    					new ActionMessage("errors.MiscInvalid", ex.getMessage()));

			    		}catch (GlobalMiscInfoIsRequiredException ex) {

			        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
									ex.getMessage() + " session: " + session.getId());

						}

						return(mapping.findForward("cmnErrorPage"));
					}

					if (!errors.isEmpty()) {

						saveErrors(request, new ActionMessages(errors));
						return(mapping.findForward("invBranchStockTransferInEntry"));

					}
					// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

				}

				String path = "/invJournal.do?forward=1" +  "&transactionCode=" +
				actionForm.getBranchStockTransferCode() + "&transaction=BRANCH STOCK TRANSFER IN" +
				"&transactionDate=" + actionForm.getDate() + "&transactionEnableFields=" +
				actionForm.getEnableFields();

				return(new ActionForward(path));

/******************************************************
	    -- Inv BST Delete Action --
******************************************************/

			} else if(request.getParameter("deleteButton") != null) {

				try {

					ejbBST.deleteInvBstEntry(actionForm.getBranchStockTransferCode(), actionForm.getType(), user.getUserName(), user.getCmpCode());

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBrachStockTransferInEntry.error.recordAlreadyDeleted"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
								ex.getMessage() + " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}

/*******************************************************
     -- Inv BST Print Action --
*******************************************************/

			} else if(request.getParameter("printButton") != null) {

				if(Common.validateRequired(actionForm.getApprovalStatus())) {

					InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

					details.setBstCode(actionForm.getBranchStockTransferCode());
					//Header tab fields
					details.setBstNumber(actionForm.getTransferInNumber());
					details.setBstTransferOutNumber(actionForm.getTransferOutNumber());
					details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
					details.setBstBranchFrom(actionForm.getBranchFrom());
					details.setBstTransitLocation(actionForm.getTransitLocation());
					System.out.println(details.getBstDate() + " potaena");

					if (actionForm.getBranchStockTransferCode() == null) {

						details.setBstCreatedBy(user.getUserName());
						details.setBstDateCreated(
							Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

					}

					details.setBstLastModifiedBy(user.getUserName());
					details.setBstDateLastModified(
						Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					details.setBstDescription(actionForm.getDescription());

					ArrayList bslList = new ArrayList();

					for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

						InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

						if (Common.validateRequired(invBSTList.getItemName()) &&
							Common.validateRequired(invBSTList.getLocation()) &&
							Common.validateRequired(invBSTList.getQuantity()) &&
							Common.validateRequired(invBSTList.getUnit())) continue;

						InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

						mdetails.setBslCode(invBSTList.getBranchStockTransferInLineCode());
						mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
						mdetails.setBslIiName(invBSTList.getItemName());
						mdetails.setBslLocationName(invBSTList.getLocation());
						mdetails.setBslQuantity(Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
						mdetails.setBslQuantityReceived(Common.convertStringMoneyToDouble(invBSTList.getQuantityReceived(), quantityPrecisionUnit));
						mdetails.setBslUomName(invBSTList.getUnit());
						mdetails.setBslUnitCost(Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
						mdetails.setBslIiDescription(invBSTList.getItemDescription());
						mdetails.setBslAmount(Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
						mdetails.setBslShippingCost(Common.convertStringMoneyToDouble(invBSTList.getShippingCost(), precisionUnit));
						mdetails.setBslMisc(invBSTList.getMisc());

						 ArrayList tagList = new ArrayList();
			         	   //TODO:save and submit taglist
			         	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());

							String misc= invBSTList.getMisc();

				      	   if (isTraceMisc){



				      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
				      		   mdetails.setBslTagList(tagList);

				      	   }
						bslList.add(mdetails);

					}

					// validate attachment

					String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
					String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
					String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
					String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

					if (!Common.validateRequired(filename1)) {

			   	    	if (actionForm.getFilename1().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename1().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename2)) {

			   	    	if (actionForm.getFilename2().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename2().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename3)) {

			   	    	if (actionForm.getFilename3().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename3().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename4)) {

			   	    	if (actionForm.getFilename4().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename4().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferInEntry"));

			   	    	}

			   	   	}
					try {

						Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, true, actionForm.getType(),
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setBranchStockTransferCode(branchStockTransferCode);

					} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.quantityReceivedOverApplication"));

					} catch (GlobalDocumentNumberNotUniqueException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

					} catch (GlobalRecordAlreadyDeletedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

					} catch (GlobalTransactionAlreadyApprovedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

					} catch (GlobalTransactionAlreadyPendingException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

					} catch (GlobalTransactionAlreadyPostedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

					} catch (GlobalInvItemLocationNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound", ex.getMessage()));

					}catch (GlobalInvTagMissingException ex) {

			           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                    new ActionMessage("invBranchStockTransferEntry.error.invTagInventoriableError", ex.getMessage()));

					}catch (GlobalInvTagExistingException ex) {

			           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                    new ActionMessage("invBranchStockTransferEntry.error.invTagNonInventoriableError", ex.getMessage()));

					} catch (GlJREffectiveDateNoPeriodExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

					} catch (GlJREffectiveDatePeriodClosedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

					} catch (GlobalJournalNotBalanceException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));

					} catch (GlobalInventoryDateException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

					} catch(GlobalBranchAccountNumberInvalidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
								ex.getMessage()));

					/*} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyAssigned"));*/

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

		             }catch (GlobalExpiryDateNotFoundException ex) {

			    			errors.add(ActionMessages.GLOBAL_MESSAGE,
			    					new ActionMessage("errors.MiscInvalid", ex.getMessage()));

			    		}catch (GlobalMiscInfoIsRequiredException ex) {

			        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
								ex.getMessage() + " session: " + session.getId());

						}

						return(mapping.findForward("cmnErrorPage"));
					}


					if (!errors.isEmpty()) {

						saveErrors(request, new ActionMessages(errors));
						return(mapping.findForward("invBranchStockTransferInEntry"));

					}
					// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }
					actionForm.setReport(Constants.STATUS_SUCCESS);

					isInitialPrinting = true;

				} else {

					actionForm.setReport(Constants.STATUS_SUCCESS);

					return(mapping.findForward("invBranchStockTransferInEntry"));

				}

/*******************************************************
     -- Inv BST Close Action --
*******************************************************/

			} else if(request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

/*******************************************************
      -- Inv BST Add Lines Action --
*******************************************************/

	    	} else if(request.getParameter("addLinesButton") != null) {

	    		int listSize = actionForm.getInvBSTListSize();
	    		int lineNumber = 0;

	    		for (int x = listSize + 1; x <= listSize + branchStockTransferInLineNumber; x++) {

	    			//	ArrayList comboItem = new ArrayList();

	    			InvBranchStockTransferInEntryList invBSTList = new
					InvBranchStockTransferInEntryList(actionForm, null, new Integer(++lineNumber).toString(),
							null, null, null, null, null, null, null, null, null, null);

	    			invBSTList.setLocation(Constants.GLOBAL_BLANK);
	    			invBSTList.setLocationList(actionForm.getLocationList());
	    			invBSTList.setUnitList(Constants.GLOBAL_BLANK);
	    			invBSTList.setUnitList("Select Item First");

	    			actionForm.saveInvBSTList(invBSTList);

	    		}

	    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++ ) {

	    			InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			invBSTList.setLineNumber(new Integer(i+1).toString());

	    		}

	    		return(mapping.findForward("invBranchStockTransferInEntry"));

/*******************************************************
	    -- Inv BST Delete Lines Action --
*******************************************************/

	    	} else if(request.getParameter("deleteLinesButton") != null) {

		    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    			InvBranchStockTransferInEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			if (invBSTList.isDeleteCheckbox()) {

	    				actionForm.deleteInvBSTList(i);
	    				i--;
	    			}

	    		}

	    		return(mapping.findForward("invBranchStockTransferInEntry"));


/*******************************************************
 -- Inv BST Transfer Out Number Enter Action --
*******************************************************/

			} else if(!Common.validateRequired(request.getParameter("isTransferOutNumberEntered"))) {

				if (actionForm.getType().equalsIgnoreCase("BST-OUT MATCHED")){

					try {

						if (!Common.validateRequired(actionForm.getDate())) {

							InvModBranchStockTransferDetails mdetails =
								ejbBST.getInvBstOutByBstCode(actionForm.getTransferOutCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


							//getInvBstByBstTransferOutNumberAndAdBranch
							//eto ang i-check mo
							actionForm.setBranchFrom(mdetails.getBstBranchFrom());
							actionForm.setTransitLocation(mdetails.getBstTransitLocation());
							actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
							actionForm.setDescription(mdetails.getBstDescription());
							actionForm.clearInvBSTList();
							Iterator i = mdetails.getBstBtlList().iterator();

							int lineNumber = 0;

							while (i.hasNext()) {

								InvModBranchStockTransferLineDetails mBslDetails =
									(InvModBranchStockTransferLineDetails)i.next();

								InvBranchStockTransferInEntryList bstBslList =
									new InvBranchStockTransferInEntryList(actionForm, mBslDetails.getBslCode(),
										new Integer(++lineNumber).toString(), mBslDetails.getBslIiName(),
										mBslDetails.getBslLocationName(),
										Common.convertDoubleToStringMoney(mBslDetails.getBslQuantity(),quantityPrecisionUnit),
										Common.convertDoubleToStringMoney(mBslDetails.getBslQuantityReceived(),quantityPrecisionUnit),
										mBslDetails.getBslUomName(),
										Common.convertDoubleToStringMoney(mBslDetails.getBslUnitCost(), precisionUnit),
										mBslDetails.getBslIiDescription(),
										Common.convertDoubleToStringMoney(mBslDetails.getBslAmount(), precisionUnit),
										Common.convertDoubleToStringMoney(mBslDetails.getBslShippingCost(), precisionUnit),
										mBslDetails.getBslMisc());

								bstBslList.setLocationList(actionForm.getLocationList());

								bstBslList.clearTagList();


								boolean isTraceMisc = ejbBST.getInvTraceMisc(mBslDetails.getBslIiName(), user.getCmpCode());

								bstBslList.setIsTraceMisc(isTraceMisc);
								if (isTraceMisc == true){
									ArrayList tagList = mBslDetails.getBslTagList();

									//TODO:save tagList


		                        	if(tagList.size()>0) {

		                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mBslDetails.getBslQuantity()));
		                        		bstBslList.setMisc(misc);
		                        		mBslDetails.setBslMisc(misc);

		                        	}else {
		                        		if(mBslDetails.getBslMisc()==null) {
		                        			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mBslDetails.getBslQuantity()));
		                        			bstBslList.setMisc(misc);
		                        			mBslDetails.setBslMisc(misc);
		                        		}
		                        	}


								}


								actionForm.saveInvBSTList(bstBslList);
								actionForm.setShowBSTMatchedLinesDetails(true);

							}

						}

					} catch (GlobalNoRecordFoundException ex) {

						actionForm.clearInvBSTList();

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("invBranchStockTransferEntry.error.noTransferOutFound"));
						saveErrors(request, new ActionMessages(errors));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
								ex.getMessage() + " session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}
				} else if (actionForm.getType().equalsIgnoreCase("ITEMS")){
					actionForm.setShowBSTMatchedLinesDetails(false);

				}

				return(mapping.findForward("invBranchStockTransferInEntry"));

/**************************************************
	   -- Inv BST Type Enter Action --
****************************************************/

				} else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

		         	InvBranchStockTransferInEntryList invBSTList = null;

		         	actionForm.clearInvBSTList();

		         	if (actionForm.getType().equalsIgnoreCase("BST-OUT MATCHED")){
		         		actionForm.setShowAddLinesButton(false);
			         	actionForm.setShowDeleteLinesButton(false);
			         	actionForm.setShowBSTMatchedLinesDetails(true);
			         	actionForm.clearInvBSTList();

		         	} else if (actionForm.getType().equalsIgnoreCase("ITEMS")){

		         		actionForm.setShowBSTMatchedLinesDetails(false);

			         		for (int x = 1; x <= branchStockTransferInLineNumber; x++) {

			    				invBSTList = new InvBranchStockTransferInEntryList(
			    					actionForm, null, new Integer(x).toString(),null, null, null, null, null, null, null, null,
			    					null, null);

			    				invBSTList.setLocationList(actionForm.getLocationList());

			    				invBSTList.setUnitList(Constants.GLOBAL_BLANK);
		    					invBSTList.setUnitList("Select Item First");

			    				actionForm.saveInvBSTList(invBSTList);

			    			}

		         		actionForm.setShowAddLinesButton(true);
			         	actionForm.setShowDeleteLinesButton(true);

		         	}
		         	System.out.println("IS TYPE ENTERED")	;
		         	actionForm.setIsTypeEntered(null);
					actionForm.setBranchFrom(null);
					actionForm.setTransitLocation(null);
		         	actionForm.setBranchStockTransferCode(null);
					actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
					actionForm.setPosted("NO");
					actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
					actionForm.setCreatedBy(user.getUserName());
					actionForm.setLastModifiedBy(user.getUserName());
					actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

					this.setFormProperties(actionForm, user.getCompany());

					return(mapping.findForward("invBranchStockTransferInEntry"));


/*******************************************************
   -- Ap BST View Attachment Action --
*******************************************************/

	         } else if(request.getParameter("viewAttachmentButton1") != null) {

	    	  	File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtensionPDF );
	    	  	String filename = "";

	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferInEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOutEntryChild"));

		        }



		      } else if(request.getParameter("viewAttachmentButton2") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtension );
		    	  	File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtensionPDF );
		    	  	String filename = "";

		    	  	if (file.exists()){
		    	  		filename = file.getName();
		    	  	}else if (filePDF.exists()) {
		    	  		filename = filePDF.getName();
		    	  	}

		 			System.out.println(filename + " <== filename?");
			        String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    	  		fileExtension = attachmentFileExtension;

		  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		  	    	}
		    	  	System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	}




		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferInEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOutEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton3") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtension );
		    	  File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtensionPDF );
		    	  String filename = "";

		    	  if (file.exists()){
		    		  filename = file.getName();
		    	  }else if (filePDF.exists()) {
		    		  filename = filePDF.getName();
		    	  }

		    	  System.out.println(filename + " <== filename?");
		    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    		  fileExtension = attachmentFileExtension;

		    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		    	  }
		    	  System.out.println(fileExtension + " <== file extension?");
		    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

		    	  byte data[] = new byte[fis.available()];

		    	  int ctr = 0;
		    	  int c = 0;

		    	  while ((c = fis.read()) != -1) {

		    		  data[ctr] = (byte)c;
		    		  ctr++;

		    	  }

		    	  if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  } else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  }

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferInEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOutEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton4") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtension );
		    	  File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtensionPDF );
		    	  String filename = "";

		    	  if (file.exists()){
		    		  filename = file.getName();
		    	  }else if (filePDF.exists()) {
		    		  filename = filePDF.getName();
		    	  }

		 			System.out.println(filename + " <== filename?");
			        String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    	  		fileExtension = attachmentFileExtension;

		  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		  	    	}
		    	  	System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	}
		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferInEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOutEntryChild"));

		        }
 /*******************************************************
     -- Inv BST Item Enter Action --
 *******************************************************/

		         } else if(!Common.validateRequired(request.getParameter("invBSTList[" +
		        		 actionForm.getRowSelected() + "].isItemEntered"))) {

		        	 InvBranchStockTransferInEntryList invBSTList =
		        		 actionForm.getInvBSTByIndex(actionForm.getRowSelected());

		        	 try {

		        		 // populate unit field class

		        		 ArrayList uomList = new ArrayList();
		        		 uomList = ejbBST.getInvBranchUomByIiName(invBSTList.getItemName(), user.getCmpCode());

		        		 invBSTList.clearUnitList();
		        		 invBSTList.setUnitList(Constants.GLOBAL_BLANK);

		        		 Iterator i = uomList.iterator();
		        		 while (i.hasNext()) {

		        			 InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

		        			 invBSTList.setUnitList(mUomDetails.getUomName());

		        			 if (mUomDetails.isDefault()) {

		        				 invBSTList.setUnit(mUomDetails.getUomName());

		        			 }

		        		 }

		        		 // populate unit cost field

		        		 if (!Common.validateRequired(invBSTList.getItemName()) &&
		        				 !Common.validateRequired(actionForm.getTransitLocation()) &&
		        				 !Common.validateRequired(invBSTList.getUnit()) &&
		        				 !Common.validateRequired(actionForm.getDate()) &&
		        				 !Common.validateRequired(actionForm.getBranchFrom())) {

                             double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invBSTList.getItemName(), invBSTList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                

                              /*
		        			 double unitCost = ejbBST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
		        					 invBSTList.getItemName(), actionForm.getTransitLocation(), invBSTList.getUnit(),
		        					 Common.convertStringToSQLDate(actionForm.getDate()),
		        					 actionForm.getBranchFrom(), user.getCmpCode());
                            */
		        			 double shippingCost = ejbBST.getInvIiShippingCostByIiUnitCostAndAdBranch(
	     	    						unitCost, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	     	    			 invBSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost + shippingCost, precisionUnit));

		        		 }

		        		 //TODO: populate taglist with empty values

		        		 invBSTList.clearTagList();


		          		boolean isTraceMisc =  ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());


		          		invBSTList.setIsTraceMisc(isTraceMisc);
		           		System.out.println(isTraceMisc + "<== trace misc item enter action");
		           		if (isTraceMisc){
		           			invBSTList.clearTagList();

		           			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), invBSTList.getQuantityReceived());


		           			invBSTList.setMisc(misc);

		           		}



		        		 // populate amount field

		        		 if (!Common.validateRequired(invBSTList.getUnitCost()) &&
		        				 !Common.validateRequired(invBSTList.getQuantity())) {

		        			 double amount = Common.convertStringMoneyToDouble(invBSTList.getQuantity(), precisionUnit) *
		        			 Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit);
		        			 invBSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

		        		 }

		        	 } catch (EJBException ex) {

		        		 if (log.isInfoEnabled()) {

		        			 log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
		        					 " session: " + session.getId());

		        			 return mapping.findForward("cmnErrorPage");

		        		 }

		        	 }

		        	 return(mapping.findForward("invBranchStockTransferInEntry"));

/*******************************************************
      -- Inv BST Load Action --
*******************************************************/
			}

			if (frParam != null) {

				// Errors on saving
				System.out.println(actionForm.getBranchFrom() + " getBranchFrom from load action");
				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("invBranchStockTransferInEntry"));

				}

				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("errors.responsibilityAccessNotAllowed"));
					saveErrors(request, new ActionMessages(errors));

					return mapping.findForward("cmnMain");

				}

				try {

					// location combo box

					ArrayList list = null;
					Iterator i = null;

					actionForm.clearLocationList();

					list = ejbBST.getInvLocAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();
						while (i.hasNext()) {

							String locName = (String)i.next();
							actionForm.setLocationList(locName);

						}

					}

					actionForm.clearUserList();

	            	ArrayList userList = ejbBST.getAdUsrAll(user.getCmpCode());

	            	if (userList == null || userList.size() == 0) {

	            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		Iterator x = userList.iterator();

	            		while (x.hasNext()) {

	            			actionForm.setUserList((String)x.next());

	            		}

	            	}

					// branch combo box

	    			actionForm.clearBranchFromList();

	    			list = ejbBST.getAdBrAll(user.getCmpCode());

	    			if (list == null || list.size() == 0) {

	    				actionForm.setBranchFromList(Constants.GLOBAL_NO_RECORD_FOUND);

	    			} else {

	    				i = list.iterator();
	    				while (i.hasNext()) {
	    					actionForm.setBranchFromList((String)i.next());
	    				}

	    			}

					actionForm.clearInvBSTList();

					if (request.getParameter("forward") != null || isInitialPrinting ) {

						if (request.getParameter("forward") != null) {

							actionForm.setBranchStockTransferCode(
									new Integer(request.getParameter("branchStockTransferCode")));

						}

						InvModBranchStockTransferDetails mdetails =
							ejbBST.getInvBstByBstCode(null, actionForm.getBranchStockTransferCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


						//Header tab fields
						actionForm.setTransferInNumber(mdetails.getBstNumber());
						actionForm.setDate(Common.convertSQLDateToString(mdetails.getBstDate()));
						actionForm.setBranchFrom(mdetails.getBstBranchFrom());
						actionForm.setTransitLocation(mdetails.getBstTransitLocation());
						actionForm.setDescription(mdetails.getBstDescription());
						actionForm.setTransferOutNumber(mdetails.getBstTransferOutNumber());
						actionForm.setType(mdetails.getBstType());
						System.out.println(actionForm.getBranchFrom() + " <= branchFrom");
						//Log tab fields
						actionForm.setCreatedBy(mdetails.getBstCreatedBy());
						actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getBstDateCreated()));
						actionForm.setLastModifiedBy(mdetails.getBstLastModifiedBy());
						actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getBstDateLastModified()));
						actionForm.setApprovedRejectedBy(mdetails.getBstApprovedRejectedBy());
						actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getBstDateApprovedRejected()));
						actionForm.setPostedBy(mdetails.getBstPostedBy());
						actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getBstDatePosted()));
						//Status tab fields

						actionForm.setApprovalStatus(mdetails.getBstApprovalStatus());
						actionForm.setReasonForRejection(mdetails.getBstReasonForRejection());
						actionForm.setPosted(mdetails.getBstPosted() == 1 ? "YES" : "NO");


						list = mdetails.getBstBtlList();

						i = list.iterator();

						int lineNumber = 0;

						while (i.hasNext()) {

							InvModBranchStockTransferLineDetails mBslDetails =
								(InvModBranchStockTransferLineDetails)i.next();

							InvBranchStockTransferInEntryList bstBslList =
								new InvBranchStockTransferInEntryList(actionForm, mBslDetails.getBslCode(),
									new Integer(++lineNumber).toString(), mBslDetails.getBslIiName(),
									mBslDetails.getBslLocationName(),
									Common.convertDoubleToStringMoney(mBslDetails.getBslQuantity(),quantityPrecisionUnit),
									Common.convertDoubleToStringMoney(mBslDetails.getBslQuantityReceived(),quantityPrecisionUnit),
									mBslDetails.getBslUomName(),
									Common.convertDoubleToStringMoney(mBslDetails.getBslUnitCost(), precisionUnit),
									mBslDetails.getBslIiDescription(),
									Common.convertDoubleToStringMoney(mBslDetails.getBslAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mBslDetails.getBslShippingCost(), precisionUnit),
									mBslDetails.getBslMisc());

							bstBslList.setLocationList(actionForm.getLocationList());

							 bstBslList.clearTagList();

			            	 boolean isTraceMisc = ejbBST.getInvTraceMisc(mBslDetails.getBslIiName(), user.getCmpCode());
			            	 bstBslList.setIsTraceMisc(isTraceMisc);



			            	 ArrayList tagList = new ArrayList();
	                         if(isTraceMisc) {


	                    		 tagList = mBslDetails.getBslTagList();



	                        	if(tagList.size()>0) {

	                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mBslDetails.getBslQuantity()));
	                        		bstBslList.setMisc(misc);

	                        	}else {
	                        		if(mBslDetails.getBslMisc()==null) {
	                        			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mBslDetails.getBslQuantity()));
	                        			bstBslList.setMisc(misc);
	                        		}
	                        	}



	                         }











							bstBslList.clearUnitList();

	    					ArrayList unitList = ejbBST.getInvBranchUomByIiName(mBslDetails.getBslIiName(),
	    							user.getCmpCode());

	    					Iterator unitListIter = unitList.iterator();

	    					while (unitListIter.hasNext()) {

	    						InvModUnitOfMeasureDetails mUomDetails =
	    							(InvModUnitOfMeasureDetails) unitListIter.next();

	    						bstBslList.setUnitList(mUomDetails.getUomName());

	    					}

							actionForm.saveInvBSTList(bstBslList);

						}

						int remainingList = branchStockTransferInLineNumber - (list.size() %
	    						branchStockTransferInLineNumber) + list.size();

	    				for (int x = list.size() + 1; x <= remainingList; x++) {

	    					ArrayList comboItem = new ArrayList();

	    					InvBranchStockTransferInEntryList invBSTList = new InvBranchStockTransferInEntryList(
	    						actionForm, null, new Integer(x).toString(),null, null, null, null, null, null, null,
	    						null, null, null);

	    					invBSTList.setLocationList(actionForm.getLocationList());

	    					invBSTList.setUnitList(Constants.GLOBAL_BLANK);
	    					invBSTList.setUnitList("Select Item First");

	    					actionForm.saveInvBSTList(invBSTList);

	    				}

						this.setFormProperties(actionForm, user.getCompany());

						if (request.getParameter("child") == null) {

							return (mapping.findForward("invBranchStockTransferInEntry"));

						} else {

							return (mapping.findForward("invBranchStockTransferOutEntryChild"));

						}

					}

				} catch(GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

				} catch(EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in invBranchStockTransferInEntryAction.execute(): " +
							ex.getMessage() + " session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));

				}

				// Errors on loading

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				} else {

					if (request.getParameter("saveSubmitButton") != null &&
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

						try {

							ArrayList list = ejbBST.getAdApprovalNotifiedUsersByBstCode(
								actionForm.getBranchStockTransferCode(), user.getCmpCode());

							if (list.isEmpty()) {

								messages.add(ActionMessages.GLOBAL_MESSAGE,
									new ActionMessage("messages.documentSentForPosting"));

							} else if (list.contains("DOCUMENT POSTED")) {

								messages.add(ActionMessages.GLOBAL_MESSAGE,
									new ActionMessage("messages.documentPosted"));

							} else {

								Iterator i = list.iterator();

								String APPROVAL_USERS = "";

								while (i.hasNext()) {

									APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

									if (i.hasNext()) {

										APPROVAL_USERS = APPROVAL_USERS + ", ";

									}


								}

								messages.add(ActionMessages.GLOBAL_MESSAGE,
									new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

							}

							saveMessages(request, messages);
							actionForm.setTxnStatus(Constants.STATUS_SUCCESS);


						} catch(EJBException ex) {

							if (log.isInfoEnabled()) {

								log.info("EJBException caught in InvBranchStockTransferInEntryAction.execute(): " +
									ex.getMessage() + " session: " + session.getId());

							}

							return(mapping.findForward("cmnErrorPage"));

						}

					} else if (request.getParameter("saveAsDraftButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					}


				}

				actionForm.reset(mapping, request);
				actionForm.setBranchStockTransferCode(null);
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setPosted("NO");
				actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setCreatedBy(user.getUserName());
				actionForm.setLastModifiedBy(user.getUserName());
				actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setShowBSTMatchedLinesDetails(true);

				this.setFormProperties(actionForm, user.getCompany());
				return(mapping.findForward("invBranchStockTransferInEntry"));

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

		} catch(Exception e) {

/*******************************************************
      System Failed: Forward to error page
*******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in InvBranchStockTransferInEntryAction.execute(): " + e.getMessage()
					+ " session: " + session.getId());

			}

			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));

		}

	}


	private void setFormProperties(InvBranchStockTransferInEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getBranchStockTransferCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(false);

				} else if (actionForm.getBranchStockTransferCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);


				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);

			}

		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);

		}
		// view attachment

		if (actionForm.getBranchStockTransferCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/inv/BSTI/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}

		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}

}
