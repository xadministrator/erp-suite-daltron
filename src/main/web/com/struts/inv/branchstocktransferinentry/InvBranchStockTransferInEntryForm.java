package com.struts.inv.branchstocktransferinentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.inv.branchstocktransferoutentry.InvBranchStockTransferOutEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;

public class InvBranchStockTransferInEntryForm extends ActionForm implements Serializable { 
    
    private Integer branchStockTransferCode = null;		
	private String date = null;
	private String transferInNumber = null;
	private String transferOutNumber = null;
	private String branchFrom = null;
	private String transitLocation = null;
	private ArrayList locationList = new ArrayList();
	private Integer transferOutCode = null;
	private String description = null;
	
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String reasonForRejection = null;
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;	
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	private ArrayList invBSTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showBSTMatchedLinesDetails = true;
		
	private String report = null;
	
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private ArrayList branchFromList = new ArrayList();
	private ArrayList userList = new ArrayList();
	
	private String misc = null;
	private String attachment = null;
	private String attachmentPDF = null;
	private String isTypeEntered = null;
	
	
	public ArrayList getUserList() {
		
		return userList;
		
	}
	
	public void setUserList(String user) {
		
	   userList.add(user);
		
	}
  
	public void clearUserList() {
	   	
	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getIsTypeEntered() {
		
		return isTypeEntered;
	}
	
	public void setIsTypeEntered(String isTypeEntered) {
		
		this.isTypeEntered = isTypeEntered;
	}
	
	public String getAttachment() {
	   	
   	   return attachment;
   	
	}
   
	public void setAttachment(String attachment) {
	   
   	   this.attachment = attachment;
   	}
	
	public String getAttachmentPDF() {
	   	
	   	   return attachmentPDF;
	   	
	}
   
	public void setAttachmentPDF(String attachmentPDF) {
	   
   	   this.attachmentPDF = attachmentPDF;
   	}
	
	public boolean getShowViewAttachmentButton1() {
	   	   
	   	   return showViewAttachmentButton1;
	   	
   }
   
   public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
   	
   	   this.showViewAttachmentButton1 = showViewAttachmentButton1;
   	
   }
   
   public boolean getShowViewAttachmentButton2() {
   	   
   	   return showViewAttachmentButton2;
   	
   }
   
   public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
   	
   	   this.showViewAttachmentButton2 = showViewAttachmentButton2;
   	
   }
   
   public boolean getShowViewAttachmentButton3() {
   	   
   	   return showViewAttachmentButton3;
   	
   }
   
   public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
   	
   	   this.showViewAttachmentButton3 = showViewAttachmentButton3;
   	
   }
   
   public boolean getShowViewAttachmentButton4() {
   	   
   	   return showViewAttachmentButton4;
   	
   }
   
   public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
   	
	   this.showViewAttachmentButton4 = showViewAttachmentButton4;
   	
   }
   public FormFile getFilename1() {
	   	
   	  return filename1;
	   	
   }
   
   public void setFilename1(FormFile filename1) {
   	  
   	  this.filename1 = filename1;
   	
   }
   
   public FormFile getFilename2() {
   	
   	  return filename2;
   	
   }
   
   public void setFilename2(FormFile filename2) {
   
   	  this.filename2 = filename2;
   	
   }
   
   public FormFile getFilename3() {
   	
   	  return filename3;
   	
   }
   
   public void setFilename3(FormFile filename3) {
   	  
   	  this.filename3 = filename3;
   	
   }
   
   public FormFile getFilename4() {
   	
   	  return filename4;
   	
   }
   
   public void setFilename4(FormFile filename4) {
   	  
   	  this.filename4 = filename4;
   	
   }
	   
	public String getReport() {	    
	    return report;	    
	}
	public void setReport(String report) {
	    this.report = report;	    
	}

	public int getRowSelected(){
		return rowSelected;
	}

	public InvBranchStockTransferInEntryList getInvBSTByIndex(int index){
		return((InvBranchStockTransferInEntryList)invBSTList.get(index));
	}
	
	public Object[] getInvBSTList(){
		return(invBSTList.toArray());
	}

	public int getInvBSTListSize(){
		return(invBSTList.size());
	}

	public void saveInvBSTList(Object newInvBSTList){
		invBSTList.add(newInvBSTList);
	}

	public void clearInvBSTList(){
		invBSTList.clear();
	}

	public void setRowSelected(Object selectedInvBSTList, boolean isEdit){
		this.rowSelected = invBSTList.indexOf(selectedInvBSTList);
	}

	public void updateInvBSTRow(int rowSelected, Object newInvBSTList){
		invBSTList.set(rowSelected, newInvBSTList);
	}

	public void deleteInvBSTList(int rowSelected){
		invBSTList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}
	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	
	
	public String getUserPermission(){
		return(userPermission);
	}
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}     	

	
	
	public Integer getBranchStockTransferCode() {
		return branchStockTransferCode;
	}
	public void setBranchStockTransferCode(Integer branchStockTransferCode) {
		this.branchStockTransferCode = branchStockTransferCode;
	}

	
	
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	
	
	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}		
	
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}

	
	
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

	
	
	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}

	
	
	public boolean getEnableFields() {
		return enableFields;
	}
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}

	
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getTransferInNumber() {
		return transferInNumber;
	}
	public void setTransferInNumber(String transferInNumber) {
		this.transferInNumber = transferInNumber;
	}
	
	public String getTransferOutNumber() {
		return transferOutNumber;
	}
	public void setTransferOutNumber(String transferOutNumber) {
		this.transferOutNumber = transferOutNumber;
	}	

	
	public String getBranchFrom() {
		return branchFrom;
	}
	public void setBranchFrom(String branchFrom) {
		this.branchFrom = branchFrom;
	}	

	
	public String getTransitLocation() {
		return transitLocation;
	}
	public void setTransitLocation(String transitLocation) {
		this.transitLocation = transitLocation;
	}	
	
	public ArrayList getLocationList() {
		return locationList;
	}
	
	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}
	
	public void clearLocationList() {
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}

	
	
	public String getReasonForRejection() {
	   	  return reasonForRejection;
   }
    public void setReasonForRejection(String reasonForRejection) {
        this.reasonForRejection = reasonForRejection;
    }
	
    
    
	public String getPosted() {
		return posted;
	}
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getMisc() {
		return misc;
	}
	public void setMisc(String misc) {
		this.misc = misc;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
		
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public Integer getTransferOutCode() {
		return transferOutCode;
	}
	public void setTransferOutCode(Integer transferOutCode) {
		this.transferOutCode = transferOutCode;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getType(){
		return type;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public ArrayList getTypeList(){
		return typeList;
	}
	
	public boolean isShowSaveButton() {
		return showSaveButton;
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowBSTMatchedLinesDetails() {
		return showBSTMatchedLinesDetails;
	}
	
	public void setShowBSTMatchedLinesDetails(boolean showBSTMatchedLinesDetails) {
		this.showBSTMatchedLinesDetails = showBSTMatchedLinesDetails;
	}
	
	public ArrayList getBranchFromList() {
		return branchFromList;
	}
	
	public void setBranchFromList(String branchFromList) {
		this.branchFromList.add(branchFromList);
	}
	
	public void clearBranchFromList() {
	   	
		branchFromList.clear();
		branchFromList.add(Constants.GLOBAL_BLANK);
	   	
	}

	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
		
       isTypeEntered = null;
       date = null;
       transferInNumber = null;
       transferOutNumber = null;
   	   branchFrom = null;
   	   transitLocation = null;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   reasonForRejection = null;
	   transferOutCode = null;
	   type = null;
	   typeList.clear();
	   typeList.add("BST-OUT MATCHED");
	   typeList.add("ITEMS");
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   attachment = null;
	   attachmentPDF = null;
	   
	   for (int i=0; i<invBSTList.size(); i++) {

			InvBranchStockTransferInEntryList actionList = (InvBranchStockTransferInEntryList)invBSTList.get(i);
			actionList.setIsItemEntered(null);
			
			
		
		}
	   
   }
	
	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";
		   
		   // Remove first $ character
		   misc = misc.substring(1);
		   
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;	
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);
		   
		   /*if(y==0)
			   return new ArrayList();*/
		   
		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {	        	

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);	 
				   }else{
					   miscList.add("null");
				   }
				   
				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   
			   }

		   	   }
		   return miscList;
	   }
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveSubmitButton") != null || 
				request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("journalButton") != null ||
				request.getParameter("printButton") != null) { 
			
			if(Common.validateRequired(date)){
				errors.add("date", new ActionMessage("invBranchStockTransferEntry.error.dateRequired"));
			}
			
			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("invBranchStockTransferEntry.error.dateInvalid"));
			}
			 System.out.println("date : " +date);
			 
			 System.out.println(date.lastIndexOf("/"));
	      	 System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
	      	 int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
	      	 if(year<1900){
	      		 errors.add("date", 
	      				 new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
	      	 }
	      	 
			if(Common.validateRequired(transferOutNumber)){
				errors.add("transferOutNumber", new ActionMessage("invBranchStockTransferEntry.error.transferOutNumberRequired"));
			}
			
			int numberOfLines = 0;
			
			Iterator i = invBSTList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvBranchStockTransferInEntryList bslList = (InvBranchStockTransferInEntryList)i.next();      	 	 
				
				if (Common.validateRequired(bslList.getItemName()) &&
						Common.validateRequired(bslList.getLocation()) &&
						Common.validateRequired(bslList.getQuantity()) &&
						Common.validateRequired(bslList.getUnit())) continue;
				
				numberOfLines++;
				/*
				try{
	      	 		String separator = "$";
	      	 		String misc = "";
	      		   // Remove first $ character
	      		   misc = bslList.getMisc().substring(1);
	      		   
	      		   // Counter
	      		   int start = 0;
	      		   int nextIndex = misc.indexOf(separator, start);
	      		   int length = nextIndex - start;	
	      		   int counter;
	      		   counter = Integer.parseInt(misc.substring(start, start + length));
	      		   
	      	 		ArrayList miscList = this.getExpiryDateStr(bslList.getMisc(), counter);
	      	 		System.out.println("bslList.getMisc() : " + bslList.getMisc());
	      	 		Iterator mi = miscList.iterator();
	     	 		
	      	 		int ctrError = 0;
	      	 		int ctr = 0;
	      	 		
	      	 		while(mi.hasNext()){
	      	 				String miscStr = (String)mi.next();
	      	 			    
	      	 				if(!Common.validateDateFormat(miscStr)){
	      	 					errors.add("date", 
	      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
	      	 					ctrError++;
	      	 				}
	      	 				
	      	 				System.out.println("miscStr: "+miscStr);
	      	 				if(miscStr=="null"){
	      	 					ctrError++;
	      	 				}
	      	 		}
	      	 		//if ctr==Error => No Date Will Apply
	      	 		//if ctr>error => Invalid Date
	      	 		System.out.println("CTR: "+  miscList.size());
	      	 		System.out.println("ctrError: "+  ctrError);
	      	 		System.out.println("counter: "+  counter);
	      	 			if(ctrError>0 && ctrError!=miscList.size()){
	      	 				errors.add("date", 
	      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
	      	 			}
	      	 			
	      	 		//if error==0 Add Expiry Date
	      	 		
	      	 		
	      	 	}catch(Exception ex){
	  	  	    	
	  	  	    }*/
				
				if(Common.validateRequired(bslList.getQuantityReceived())) {
					errors.add("quantityReceived", new ActionMessage("invBranchStockTransferEntry.error.quantityRequired", bslList.getLineNumber()));
				}
				
				if(!Common.validateNumberFormat(bslList.getQuantityReceived())){
					errors.add("quantityReceived", new ActionMessage("invBranchStockTransferEntry.error.quantityInvalid", bslList.getLineNumber()));
				}
				
				if(!Common.validateRequired(bslList.getQuantityReceived()) && 
						Common.convertStringMoneyToDouble(bslList.getQuantityReceived(), (short)3) > Common.convertStringMoneyToDouble(bslList.getQuantity(), (short)3)) {
					errors.add("quantityReceived", new ActionMessage("invBranchStockTransferEntry.error.QuantityReceivedGreaterThanQuantityNotAllowed", bslList.getLineNumber()));
				}
				
				if(!Common.validateRequired(bslList.getQuantityReceived()) && Common.convertStringMoneyToDouble(bslList.getQuantityReceived(), (short)3) < 0) {
					errors.add("quantityReceived", new ActionMessage("invBranchStockTransferEntry.error.negativeQuantityNotAllowed", bslList.getLineNumber()));
				}
				
			}
			
			if (numberOfLines == 0) {
				
				errors.add("branchStockTransfer",
						new ActionMessage("invBranchStockTransferEntry.error.branchStockTransferMustHaveLine"));
				
			}	  	    
			
		} else if (!Common.validateRequired(request.getParameter("isTransferOutNumberEntered"))) {
			
			if(Common.validateRequired(transferOutNumber) || transferOutNumber.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("transferOutNumber",
						new ActionMessage("invBranchStockTransferEntry.error.transferOutNumberRequired"));
			}

		}

		return(errors);	
	}
    
}

