package com.struts.inv.branchstocktransferinentry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.inv.branchstocktransferoutentry.InvBranchStockTransferOutEntryTagList;

public class InvBranchStockTransferInEntryList implements Serializable {

    private Integer branchStockTransferInLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String quantity = null;
	private String quantityReceived = null;
	private String unit = null;
	private String unitCost = null;
	private String amount = null;
	private String itemDescription = null;
	private ArrayList unitList = new ArrayList();
	private boolean deleteCheckbox = false;
	private boolean isTraceMisc = false;
	private String isTypeEntered = null;
	private String isItemEntered = null;

	private String branchFrom = null;
	private ArrayList branchFromList = new ArrayList();
	private String shippingCost = null;

	private String misc = null;
	private ArrayList tagList = new ArrayList();


	private InvBranchStockTransferInEntryForm parentBean;

	public InvBranchStockTransferInEntryList(InvBranchStockTransferInEntryForm parentBean,
		Integer branchStockTransferInLineCode, String lineNumber, String itemName, String location,
		String quantity, String quantityReceived, String unit, String unitCost, String itemDescription,
		String amount, String shippingCost, String misc) {

	    this.parentBean = parentBean;
	    this.branchStockTransferInLineCode = branchStockTransferInLineCode;
	    this.lineNumber = lineNumber;
	    this.itemName = itemName;
	    this.location = location;
	    this.quantity = quantity;
	    this.quantityReceived = quantityReceived;
	    this.unit = unit;
	    this.unitCost = unitCost;
	    this.itemDescription = itemDescription;
	    this.amount = amount;
	    this.shippingCost = shippingCost;
	    this.misc = misc;

	}

	public InvBranchStockTransferInEntryForm getParentBean() {

	    return parentBean;

	}
	public void setParentBean(InvBranchStockTransferInEntryForm parentBean) {

	    this.parentBean = parentBean;

	}

	public Integer getBranchStockTransferInLineCode() {

	    return branchStockTransferInLineCode;

	}
	public void setBranchStockTransferInLineCode(Integer branchStockTransferInLineCode) {

	    this.branchStockTransferInLineCode = branchStockTransferInLineCode;

	}

	public String getLineNumber() {

	    return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

	    this.lineNumber = lineNumber;

	}

	public String getItemName() {

	    return itemName;

	}

	public void setItemName(String itemName) {

	    this.itemName = itemName;

	}

	public String getLocation() {

	    return location;

	}
	public void setLocation(String location) {

	    this.location = location;

	}

	public String getQuantity() {

	    return quantity;

	}

	public void setQuantity(String quantity) {

	    this.quantity = quantity;

	}

	public String getQuantityReceived() {

	    return quantityReceived;

	}

	public void setQuantityReceived(String quantityReceived) {

	    this.quantityReceived = quantityReceived;

	}

	public InvBranchStockTransferInEntryTagList getTagListByIndex(int index){
	      return((InvBranchStockTransferInEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}


	public String getUnit() {

	    return unit;

	}
	public void setUnit(String unit) {

	    this.unit = unit;

	}

	public String getUnitCost() {

	    return unitCost;

	}
	public void setUnitCost(String unitCost) {

	    this.unitCost = unitCost;

	}

	public String getItemDescription() {

	    return itemDescription;

	}
	public void setItemDescription(String itemDescription) {

	    this.itemDescription = itemDescription;

	}

	public String getAmount() {

	    return amount;

	}
	public void setAmount(String amount) {

	    this.amount = amount;
	}

	public ArrayList getLocationList() {

	    return locationList;

	}
	public void setLocationList(ArrayList locationList) {

	    this.locationList = locationList;

	}

	public ArrayList getUnitList() {

	    return unitList;

	}
	public void setUnitList(String unit) {

	    unitList.add(unit);

	}

	public void clearUnitList() {

	    unitList.clear();

	}

	public boolean getIsTraceMisc() {

	    return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

	    this.isTraceMisc = isTraceMisc;

	}


	public boolean isDeleteCheckbox() {

	    return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

	    this.deleteCheckbox = deleteCheckbox;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsTypeEntered() {

	   	  return isTypeEntered;

	}

	public void setIsTypeEntered(String isTypeEntered) {

		if (isTypeEntered != null && isTypeEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isTypeEntered = null;

	}

	public String getBranchFrom() {

	    return branchFrom;

	}
	public void setBranchFrom(String branchFrom) {

	    this.branchFrom = branchFrom;

	}

	public ArrayList getBranchFromList() {

	    return branchFromList;

	}
	public void getBranchFromList(ArrayList branchFromList) {

	    this.branchFromList = branchFromList;

	}

	public String getShippingCost() {

		return shippingCost;

	}

	public void setShippingCost(String shippingCost) {

		this.shippingCost = shippingCost;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}

}
