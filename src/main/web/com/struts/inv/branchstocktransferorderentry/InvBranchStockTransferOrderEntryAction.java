package com.struts.inv.branchstocktransferorderentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvBranchStockTransferOrderEntryController;
import com.ejb.txn.InvBranchStockTransferOrderEntryControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.InvModBranchStockTransferDetails;
import com.util.InvModBranchStockTransferLineDetails;
import com.util.InvModUnitOfMeasureDetails;

public class InvBranchStockTransferOrderEntryAction extends Action{

	private org.apache.commons.logging.Log log =
		org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());


	public ActionForward execute(ActionMapping mapping,  ActionForm form,HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {

	    HttpSession session = request.getSession();
	    InvBranchStockTransferOrderEntryForm actionForm = (InvBranchStockTransferOrderEntryForm)form;



	    try{

/*******************************************************
   Check if user has a session
*******************************************************/

	    	User user = (User) session.getAttribute(Constants.USER_KEY);

	    	if (user != null) {

	    		if (log.isInfoEnabled()) {

	    			log.info("InvStockTransferEntryAction: Company '" + user.getCompany() + "' User '" +
	    				user.getUserName() + "' performed this action on session " + session.getId());

	    		}

	    	}else{

	    		if (log.isInfoEnabled()) {

	    			log.info("User is not logged on in session" + session.getId());

	    		}

	    		return(mapping.findForward("adLogon"));

	    	}
	    	actionForm.setReport(null);
		    actionForm.setAttachment(null);
		    actionForm.setAttachmentPDF(null);
	    	String frParam = Common.getUserPermission(user, Constants.INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID);

	    	if (frParam != null) {

	    		if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	    			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

	    			if (!fieldErrors.isEmpty()) {

	    				saveErrors(request, new ActionMessages(fieldErrors));
	    				return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    			}

	    		}

	    		actionForm.setUserPermission(frParam.trim());

	    	} else {

	    		actionForm.setUserPermission(Constants.NO_ACCESS);

	    	}

	    	if (request.getParameter("child") == null) {

	    		frParam = Common.getUserPermission(user, Constants.INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID);

	    	} else {

	    		frParam = Constants.FULL_ACCESS;

	    	}

/*************************************************************
     Initialize InvBranchStockTransferOrderEntryController EJB
*************************************************************/

	    	InvBranchStockTransferOrderEntryControllerHome homeBST = null;
	    	InvBranchStockTransferOrderEntryController ejbBST = null;
	    	
	    	
	    	 InvItemEntryControllerHome homeII = null;
             InvItemEntryController ejbII = null;
         

	    	try {

	    		homeBST = (InvBranchStockTransferOrderEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvBranchStockTransferOrderEntryControllerEJB",
						InvBranchStockTransferOrderEntryControllerHome.class);
						
						
		        homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

	    	} catch(NamingException e) {

	    		if(log.isInfoEnabled()){

	    			log.info("NamingException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    				e.getMessage() + " session: " + session.getId());

	    		}

	    		return(mapping.findForward("cmnErrorPage"));

	    	}

	    	try {

	    		ejbBST = homeBST.create();

                ejbII = homeII.create();
	    	} catch(CreateException e) {

	    		if(log.isInfoEnabled()) {

	    			log.info("CreateException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    				e.getMessage() + " session: " + session.getId());

	    		}

	    		return(mapping.findForward("cmnErrorPage"));

	    	}

	    	ActionErrors errors = new ActionErrors();
	    	ActionMessages messages = new ActionMessages();

/*******************************************************
   Call invBranchStockTransferOrderEntryController EJB
   getGlFcPrecisionUnit
   getInvGpQuantityPrecisionUnit
   getInvGpInventoryLineNumber
*******************************************************/
	    	MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
	    	short precisionUnit = 0;
	    	short quantityPrecisionUnit = 0;
	    	short branchStockTransferOrderLineNumber = 0;
	    	boolean isInitialPrinting = false;
	    	String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/inv/BSOS/";
	        long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
			String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
			String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

	    	try {

	    		precisionUnit = ejbBST.getGlFcPrecisionUnit(user.getCmpCode());
	    		quantityPrecisionUnit = ejbBST.getInvGpQuantityPrecisionUnit(user.getCmpCode());
	    		branchStockTransferOrderLineNumber = ejbBST.getInvGpInventoryLineNumber(user.getCmpCode());

	    	} catch(EJBException ex) {

	    		if (log.isInfoEnabled()) {

	    			log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    				ex.getMessage() + " session: " + session.getId());
	    		}

	    		return(mapping.findForward("cmnErrorPage"));

	    	}

	    	boolean branchFromIsEqualToBranchFrom =
	    		actionForm.getBranchFrom() != null && user.getCurrentBranch() != null ?
	    		actionForm.getBranchFrom().equals(user.getCurrentBranch().getBrName()) : false;

	    	if(branchFromIsEqualToBranchFrom) {
	    		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("invBranchStockTransferEntry.error.branchToMustNotBeEqualToCurrentBranch"));
	    	}


/*******************************************************
	   -- Inv BST Save As Draft Action --
*******************************************************/

	    	if (request.getParameter("saveAsDraftButton") != null &&
	    			actionForm.getUserPermission().equals(Constants.FULL_ACCESS) && !branchFromIsEqualToBranchFrom) {

	    		InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

	    		details.setBstCode(actionForm.getBranchStockTransferCode());
	    		details.setBstType(actionForm.getType());
	    		details.setBstNumber(actionForm.getTransferOrderNumber());
	    		details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
	    		details.setBstBranchFrom(actionForm.getBranchFrom());
	    		details.setBstTransitLocation(actionForm.getTransitLocation());

	    		if (actionForm.getBranchStockTransferCode() == null) {

	    			details.setBstCreatedBy(user.getUserName());
	    			details.setBstDateCreated(
	    				Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	    		}

	    		details.setBstLastModifiedBy(user.getUserName());
	    		details.setBstDateLastModified(
	    			Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    		details.setBstDescription(actionForm.getDescription());
	    		details.setBstVoid(Common.convertBooleanToByte(actionForm.getBrVoid()));

	    		// line items
	    		ArrayList bslList = new ArrayList();

	    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    			InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			if (Common.validateRequired(invBSTList.getItemName()) &&
	    					Common.validateRequired(invBSTList.getQuantity()) &&
							Common.validateRequired(invBSTList.getUnit())) continue;

	    			InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

	    			mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
	    			mdetails.setBslIiName(invBSTList.getItemName());
	    			mdetails.setBslLocationName(invBSTList.getLocation());
	    			mdetails.setBslQuantity(
	    					Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
	    			mdetails.setBslUomName(invBSTList.getUnit());
	    			mdetails.setBslUnitCost(
	    					Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
	    			mdetails.setBslIiDescription(invBSTList.getItemDescription());
	    			mdetails.setBslAmount(Common.convertStringMoneyToDouble(invBSTList.getAmount(), precisionUnit));
	    			//mdetails.setBslMisc(invBSTList.getMisc());
	    			System.out.println("invBSTList.getMisc() Action : "+invBSTList.getMisc());



            	   ArrayList tagList = new ArrayList();
	          	   //TODO:save and submit taglist
	          	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());
	 	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	 				String misc= invBSTList.getMisc();

	 				if (isTraceMisc){



		       		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
		       		   mdetails.setBslTagList(tagList);




	 				}



	    			bslList.add(mdetails);

	    		}

	    		// validate attachment

	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	            if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   	}

	    		try {

	    			ejbBST.saveInvBstEntry(details, bslList, true, new Integer(user.getCurrentBranch().getBrCode()),
	    				user.getCmpCode());

	    		} catch (GlobalDocumentNumberNotUniqueException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

	    		} catch (GlobalRecordAlreadyDeletedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

	    		} catch (GlobalTransactionAlreadyApprovedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

	    		} catch (GlobalTransactionAlreadyPendingException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

	    		} catch (GlobalTransactionAlreadyPostedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

	    		} catch (GlobalNoApprovalRequesterFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noApprovalRequesterFound"));

	    		} catch (GlobalNoApprovalApproverFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noApprovalApproverFound"));

	    		} catch (GlobalInvItemLocationNotFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound", ex.getMessage()));

	    		} catch (GlobalRecordInvalidException ex) {

		    			errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				new ActionMessage("invBranchStockTransferEntry.error.insufficientStocks", ex.getMessage()));

	    		} catch (GlJREffectiveDateNoPeriodExistException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

	    		} catch (GlJREffectiveDatePeriodClosedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

	    		} catch (GlobalJournalNotBalanceException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));
	    		/*
	    		} catch (GlobalInventoryDateException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
	    					ex.getMessage()));
	    		*/
	    		} catch(GlobalBranchAccountNumberInvalidException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
	    							ex.getMessage()));

	    		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

	    		} catch (GlobalNoRecordFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noBranchToMapping", ex.getMessage()));

	    		 }/*catch (GlobalMiscInfoIsRequiredException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	    		} */catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    					ex.getMessage() + " session: " + session.getId());
	    			}

	    			return(mapping.findForward("cmnErrorPage"));

	    		}
	    		// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }
/********************************************************
   -- Inv BST Save & Submit Action --
*******************************************************/

	    	} else if (request.getParameter("saveSubmitButton") != null &&
	    			actionForm.getUserPermission().equals(Constants.FULL_ACCESS) && !branchFromIsEqualToBranchFrom) {

	    		InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

	    		details.setBstCode(actionForm.getBranchStockTransferCode());
	    		//Header tab fields

	    		details.setBstType(actionForm.getType());
	    		details.setBstNumber(actionForm.getTransferOrderNumber());
	    		details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
	    		details.setBstBranchFrom(actionForm.getBranchFrom());
	    		details.setBstTransitLocation(actionForm.getTransitLocation());

	    		if (actionForm.getBranchStockTransferCode() == null) {

	    			details.setBstCreatedBy(user.getUserName());
	    			details.setBstDateCreated(
	    				Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));


	    		}

	    		details.setBstLastModifiedBy(user.getUserName());
	    		details.setBstDateLastModified(
	    				Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    		details.setBstDescription(actionForm.getDescription());
	    		details.setBstVoid(Common.convertBooleanToByte(actionForm.getBrVoid()));



	    		ArrayList bslList = new ArrayList();

	    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    			InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			if (Common.validateRequired(invBSTList.getItemName()) &&
	    					Common.validateRequired(invBSTList.getQuantity()) &&
							Common.validateRequired(invBSTList.getUnit())) continue;

	    			InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

	    			mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
	    			mdetails.setBslIiName(invBSTList.getItemName());
	    			mdetails.setBslLocationName(invBSTList.getLocation());
	    			mdetails.setBslQuantity(
	    				Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
	    			mdetails.setBslUomName(invBSTList.getUnit());
	    			mdetails.setBslUnitCost(
	    				Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
	    			mdetails.setBslIiDescription(invBSTList.getItemDescription());
	    			mdetails.setBslAmount(
	    					Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
	    			//mdetails.setBslMisc(invBSTList.getMisc());



	            	   ArrayList tagList = new ArrayList();
		          	   //TODO:save and submit taglist
		          	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());
		 	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
		 				String misc= invBSTList.getMisc();

		 				if (isTraceMisc){



			       		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
			       		   mdetails.setBslTagList(tagList);




		 				}




	    			bslList.add(mdetails);

	    		}
	    		// validate attachment

	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

		   	    	}

		   	   	}

	    		try {

	    			Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, false,
	    				new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	    			System.out.println("branchStockTransferCode-" + branchStockTransferCode.toString());

	    			actionForm.setBranchStockTransferCode(branchStockTransferCode);


	    		} catch (GlobalDocumentNumberNotUniqueException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

	    		} catch (GlobalRecordAlreadyDeletedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

	    		} catch (GlobalTransactionAlreadyApprovedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

	    		} catch (GlobalTransactionAlreadyPendingException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

	    		} catch (GlobalTransactionAlreadyPostedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

	    		} catch (GlobalNoApprovalRequesterFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noApprovalRequesterFound"));

	    		} catch (GlobalNoApprovalApproverFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noApprovalApproverFound"));

	    		} catch (GlobalInvItemLocationNotFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound",
	    					ex.getMessage()));

	    		} catch (GlobalRecordInvalidException ex) {

		    			errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				new ActionMessage("invBranchStockTransferEntry.error.insufficientStocks", ex.getMessage()));

	    		} catch (GlJREffectiveDateNoPeriodExistException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

	    		} catch (GlJREffectiveDatePeriodClosedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

	    		} catch (GlobalJournalNotBalanceException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));
	    		/*
	    		} catch (GlobalInventoryDateException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
	    					ex.getMessage()));
	    			*/
	    		} catch(GlobalBranchAccountNumberInvalidException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
	    							ex.getMessage()));

	    		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

	    		} catch (GlobalExpiryDateNotFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("errors.MiscInvalid", ex.getMessage()));

	    		} catch (GlobalNoRecordFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noBranchToMapping", ex.getMessage()));

	    		 }/*catch (GlobalMiscInfoIsRequiredException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	    		} */ catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    					ex.getMessage() + " session: " + session.getId());
	    			}

	    			return(mapping.findForward("cmnErrorPage"));
	    		}
	    		// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

/*******************************************************
   -- Inv BST Journal Action --
*******************************************************/

	    	} else if (request.getParameter("journalButton") != null &&
	    			actionForm.getUserPermission().equals(Constants.FULL_ACCESS) ) {

	    		if(Common.validateRequired(actionForm.getApprovalStatus()) && !branchFromIsEqualToBranchFrom) {

	    			InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

	    			details.setBstCode(actionForm.getBranchStockTransferCode());
	    			//Header tab fields
	    			details.setBstNumber(actionForm.getTransferOrderNumber());
	    			details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
	    			details.setBstBranchFrom(actionForm.getBranchFrom());
	    			details.setBstTransitLocation(actionForm.getTransitLocation());

	    			if (actionForm.getBranchStockTransferCode() == null) {

	    				details.setBstCreatedBy(user.getUserName());
	    				details.setBstDateCreated(
	    					Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	    			}

	    			details.setBstLastModifiedBy(user.getUserName());
	    			details.setBstDateLastModified(
	    					Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    			details.setBstDescription(actionForm.getDescription());
	    			details.setBstVoid(Common.convertBooleanToByte(actionForm.getBrVoid()));

	    			ArrayList bslList = new ArrayList();

	    			for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    				InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    				if (Common.validateRequired(invBSTList.getItemName()) &&
	    						Common.validateRequired(invBSTList.getQuantity()) &&
								Common.validateRequired(invBSTList.getUnit())) continue;

	    				InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

	    				mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
	    				mdetails.setBslIiName(invBSTList.getItemName());
	    				mdetails.setBslLocationName(invBSTList.getLocation());
	    				mdetails.setBslQuantity(
	    					Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
	    				mdetails.setBslUomName(invBSTList.getUnit());
	    				mdetails.setBslUnitCost(
	    					Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
	    				mdetails.setBslIiDescription(invBSTList.getItemDescription());
	    				mdetails.setBslAmount(
	    					Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
	    				//mdetails.setBslMisc(invBSTList.getMisc());



	             	   ArrayList tagList = new ArrayList();
	 	          	   //TODO:save and submit taglist
	 	          	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());
	 	 	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	 	 				String misc= invBSTList.getMisc();

	 	 				if (isTraceMisc){



	 		       		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	 		       		   mdetails.setBslTagList(tagList);




	 	 				}



	    				bslList.add(mdetails);

	    			}

	    			// validate attachment

		            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
		            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
		            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
		            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

		            if (!Common.validateRequired(filename1)) {

			   	    	if (actionForm.getFilename1().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename1().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename2)) {

			   	    	if (actionForm.getFilename2().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename2().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename3)) {

			   	    	if (actionForm.getFilename3().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename3().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename4)) {

			   	    	if (actionForm.getFilename4().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename4().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   	}

	    			try {

	    				Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, true,
	    					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    				actionForm.setBranchStockTransferCode(branchStockTransferCode);

	    			} catch (GlobalDocumentNumberNotUniqueException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

	    			} catch (GlobalRecordAlreadyDeletedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

	    			} catch (GlobalTransactionAlreadyApprovedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

	    			} catch (GlobalTransactionAlreadyPendingException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

	    			} catch (GlobalTransactionAlreadyPostedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

	    			} catch (GlobalNoApprovalRequesterFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noApprovalRequesterFound"));

	    			} catch (GlobalNoApprovalApproverFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noApprovalApproverFound"));

	    			} catch (GlobalInvItemLocationNotFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound", ex.getMessage()));

	    			} catch (GlobalRecordInvalidException ex) {

		    			errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				new ActionMessage("invBranchStockTransferEntry.error.insufficientStocks", ex.getMessage()));

	    			} catch (GlJREffectiveDateNoPeriodExistException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

	    			} catch (GlJREffectiveDatePeriodClosedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

	    			} catch (GlobalJournalNotBalanceException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));
	    			/*
	    			} catch (GlobalInventoryDateException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
	    						ex.getMessage()));
	    			*/
	    			} catch(GlobalBranchAccountNumberInvalidException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
	    								ex.getMessage()));

	    			} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

	    			} catch (GlobalNoRecordFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.noBranchToMapping", ex.getMessage()));

	    			 }/*catch (GlobalMiscInfoIsRequiredException ex) {

	  	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	  	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	    			}*/ catch (EJBException ex) {

	    				if (log.isInfoEnabled()) {

	    					log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    						ex.getMessage() + " session: " + session.getId());
	    				}

	    				return(mapping.findForward("cmnErrorPage"));
	    			}

	    			if (!errors.isEmpty()) {

	    				saveErrors(request, new ActionMessages(errors));
	    				return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    			}
	    			// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }


	    		}

	    		String path = "/invJournal.do?forward=1" +  "&transactionCode=" +
					actionForm.getBranchStockTransferCode() + "&transaction=BRANCH STOCK TRANSFER ORDER" +
					"&transactionDate=" + actionForm.getDate() + "&transactionEnableFields=" +
					actionForm.getEnableFields();

	    		return(new ActionForward(path));

/******************************************************
	    -- Inv BST Delete Action --
******************************************************/

	    	} else if(request.getParameter("deleteButton") != null) {

	    		try {

	    			ejbBST.deleteInvBstEntry(actionForm.getBranchStockTransferCode(), user.getUserName(), user.getCmpCode());

	    		} catch (GlobalRecordAlreadyDeletedException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBrachStockTransferOrderEntry.error.recordAlreadyDeleted"));

	    		} catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    					ex.getMessage() + " session: " + session.getId());

	    			}

	    			return(mapping.findForward("cmnErrorPage"));

	    		}

/*******************************************************
     -- Inv BST Print Action --
*******************************************************/

	    	} else if(request.getParameter("printButton") != null  && !branchFromIsEqualToBranchFrom) {

	    		if(Common.validateRequired(actionForm.getApprovalStatus())) {

	    			InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

	    			details.setBstCode(actionForm.getBranchStockTransferCode());
	    			//Header tab fields
	    			details.setBstNumber(actionForm.getTransferOrderNumber());
	    			details.setBstDate(Common.convertStringToSQLDate(actionForm.getDate()));
	    			details.setBstBranchFrom(actionForm.getBranchFrom());
	    			details.setBstTransitLocation(actionForm.getTransitLocation());


	    			if (actionForm.getBranchStockTransferCode() == null) {

	    				details.setBstCreatedBy(user.getUserName());
	    				details.setBstDateCreated(
	    					Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	    			}

	    			details.setBstLastModifiedBy(user.getUserName());
	    			details.setBstDateLastModified(
	    				Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    			details.setBstDescription(actionForm.getDescription());
	    			details.setBstVoid(Common.convertBooleanToByte(actionForm.getBrVoid()));

	    			ArrayList bslList = new ArrayList();

	    			for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    				InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    				if (Common.validateRequired(invBSTList.getItemName()) &&
	    						//Common.validateRequired(invBSTList.getLocation()) &&
								Common.validateRequired(invBSTList.getQuantity()) &&
								Common.validateRequired(invBSTList.getUnit())) continue;

	    				InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

	    				mdetails.setBslLineNumber(Common.convertStringToShort(invBSTList.getLineNumber()));
	    				mdetails.setBslIiName(invBSTList.getItemName());
	    				mdetails.setBslLocationName(invBSTList.getLocation());
	    				mdetails.setBslQuantity(
	    					Common.convertStringMoneyToDouble(invBSTList.getQuantity(), quantityPrecisionUnit));
	    				mdetails.setBslUomName(invBSTList.getUnit());
	    				mdetails.setBslUnitCost(
	    					Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit));
	    				mdetails.setBslIiDescription(invBSTList.getItemDescription());
	    				mdetails.setBslAmount(
	    					Common.convertStringMoneyToDouble(invBSTList.getAmount(), quantityPrecisionUnit));
	    				//mdetails.setBslMisc(invBSTList.getMisc());



	             	   ArrayList tagList = new ArrayList();
	 	          	   //TODO:save and submit taglist
	 	          	   boolean isTraceMisc = ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());
	 	 	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	 	 				String misc= invBSTList.getMisc();

	 	 				if (isTraceMisc){



	 		       		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	 		       		   mdetails.setBslTagList(tagList);




	 	 				}

	    				bslList.add(mdetails);

	    			}

	    			// validate attachment

		            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
		            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
		            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
		            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

		            if (!Common.validateRequired(filename1)) {

			   	    	if (actionForm.getFilename1().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename1NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename1().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename1SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename2)) {

			   	    	if (actionForm.getFilename2().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename2NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename2().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename2SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename3)) {

			   	    	if (actionForm.getFilename3().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename3NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename3().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename3SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename4)) {

			   	    	if (actionForm.getFilename4().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invBranchStockTransferEntry.error.filename4NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename4().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invBranchStockTransferEntry.error.filename4SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("invBranchStockTransferOrderEntry"));

			   	    	}

			   	   	}


	    			try {

	    				Integer branchStockTransferCode = ejbBST.saveInvBstEntry(details, bslList, true,
	    					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    				actionForm.setBranchStockTransferCode(branchStockTransferCode);

	    			} catch (GlobalDocumentNumberNotUniqueException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.documentNumberNotUnique"));

	    			} catch (GlobalRecordAlreadyDeletedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

	    			} catch (GlobalTransactionAlreadyApprovedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyApproved"));

	    			} catch (GlobalTransactionAlreadyPendingException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPending"));

	    			} catch (GlobalTransactionAlreadyPostedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.transactionAlreadyPosted"));

	    			} catch (GlobalNoApprovalRequesterFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noApprovalRequesterFound"));

	    			} catch (GlobalNoApprovalApproverFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noApprovalApproverFound"));

	    			} catch (GlobalInvItemLocationNotFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.noItemLocationFound",
	    						ex.getMessage()));

	    			} catch (GlobalRecordInvalidException ex) {

		    			errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				new ActionMessage("invBranchStockTransferEntry.error.insufficientStocks", ex.getMessage()));

	    			} catch (GlJREffectiveDateNoPeriodExistException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.effectiveDateNoPeriodExist"));

	    			} catch (GlJREffectiveDatePeriodClosedException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.effectiveDatePeriodClosed"));

	    			} catch (GlobalJournalNotBalanceException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.journalNotBalance"));
	    			/*
	    			} catch (GlobalInventoryDateException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate",
	    						ex.getMessage()));
	    			*/
	    			} catch(GlobalBranchAccountNumberInvalidException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.branchAccountNumberInvalid",
	    								ex.getMessage()));

	    			} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.noNegativeInventoryCostingCOA"));

	    			} catch (GlobalNoRecordFoundException ex) {

	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	    						new ActionMessage("invBranchStockTransferEntry.error.noBranchToMapping", ex.getMessage()));

	    			} catch (EJBException ex) {

	    				if (log.isInfoEnabled()) {

	    					log.info("EJBException caught in InvBranchStockTransferOrderEntryAction.execute(): " +
	    						ex.getMessage() + " session: " + session.getId());
	    				}

	    				return(mapping.findForward("cmnErrorPage"));
	    			}


	    			if (!errors.isEmpty()) {

	    				saveErrors(request, new ActionMessages(errors));
	    				return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    			}

	    			actionForm.setReport(Constants.STATUS_SUCCESS);

	    			isInitialPrinting = true;

	    			// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }


	    		} else {

	    			actionForm.setReport(Constants.STATUS_SUCCESS);

	    			return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    		}

/*******************************************************
     -- Inv BST Close Action --
*******************************************************/

	    	} else if(request.getParameter("closeButton") != null) {

	    		return(mapping.findForward("cmnMain"));

/*******************************************************
      -- Inv BST Add Lines Action --
*******************************************************/

	    	} else if(request.getParameter("addLinesButton") != null) {

	    		int listSize = actionForm.getInvBSTListSize();
	    		int lineNumber = 0;

	    		for (int x = listSize + 1; x <= listSize + branchStockTransferOrderLineNumber; x++) {

	    			//	ArrayList comboItem = new ArrayList();

	    			InvBranchStockTransferOrderEntryList invBSTList = new
					InvBranchStockTransferOrderEntryList(actionForm, null, new Integer(++lineNumber).toString(),
							null, null, null, null, null, null, null);

	    			invBSTList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
	    			invBSTList.setLocationList(actionForm.getLocationList());
	    			invBSTList.setUnitList(Constants.GLOBAL_BLANK);
	    			invBSTList.setUnitList("Select Item First");

	    			actionForm.saveInvBSTList(invBSTList);

	    		}

	    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++ ) {

	    			InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			invBSTList.setLineNumber(new Integer(i+1).toString());

	    		}

	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

/*******************************************************
    -- Inv BST Delete Lines Action --
*******************************************************/

	    	} else if(request.getParameter("deleteLinesButton") != null) {

	    		for (int i = 0; i<actionForm.getInvBSTListSize(); i++) {

	    			InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(i);

	    			if (invBSTList.isDeleteCheckbox()) {

	    				actionForm.deleteInvBSTList(i);
	    				i--;
	    			}

	    		}

	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

/*******************************************************
   -- Ap VOU View Attachment Action --
*******************************************************/

	         } else if(request.getParameter("viewAttachmentButton1") != null) {

	    	  	File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtensionPDF );
	    	  	String filename = "";

	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferOrderEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOrderEntryChild"));

		        }



		      } else if(request.getParameter("viewAttachmentButton2") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtension );
		    	  	File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtensionPDF );
		    	  	String filename = "";

		    	  	if (file.exists()){
		    	  		filename = file.getName();
		    	  	}else if (filePDF.exists()) {
		    	  		filename = filePDF.getName();
		    	  	}

		 			System.out.println(filename + " <== filename?");
			        String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    	  		fileExtension = attachmentFileExtension;

		  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		  	    	}
		    	  	System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + fileExtension);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	}




		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferOrderEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOrderEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton3") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtension );
		    	  File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtensionPDF );
		    	  String filename = "";

		    	  if (file.exists()){
		    		  filename = file.getName();
		    	  }else if (filePDF.exists()) {
		    		  filename = filePDF.getName();
		    	  }

		    	  System.out.println(filename + " <== filename?");
		    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    		  fileExtension = attachmentFileExtension;

		    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		    	  }
		    	  System.out.println(fileExtension + " <== file extension?");
		    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + fileExtension);

		    	  byte data[] = new byte[fis.available()];

		    	  int ctr = 0;
		    	  int c = 0;

		    	  while ((c = fis.read()) != -1) {

		    		  data[ctr] = (byte)c;
		    		  ctr++;

		    	  }

		    	  if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  } else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  }

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferOrderEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOrderEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton4") != null) {

		    	  File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtension );
		    	  File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtensionPDF );
		    	  String filename = "";

		    	  if (file.exists()){
		    		  filename = file.getName();
		    	  }else if (filePDF.exists()) {
		    		  filename = filePDF.getName();
		    	  }

		 			System.out.println(filename + " <== filename?");
			        String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		    	  		fileExtension = attachmentFileExtension;

		  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		  	    		fileExtension = attachmentFileExtensionPDF;

		  	    	}
		    	  	System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + fileExtension);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension == attachmentFileExtension) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	}
		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("invBranchStockTransferOrderEntry"));

		         } else {

		           return (mapping.findForward("invBranchStockTransferOrderEntryChild"));

		        }

/*******************************************************
    -- Inv BST Item Enter Action --
*******************************************************/

	    	} else if(!Common.validateRequired(request.getParameter("invBSTList[" +
	    			actionForm.getRowSelected() + "].isItemEntered"))) {

	    		InvBranchStockTransferOrderEntryList invBSTList =
	    			actionForm.getInvBSTByIndex(actionForm.getRowSelected());

	    		try {

	    			// populate unit field class

	    			ArrayList uomList = new ArrayList();
	    			uomList = ejbBST.getInvBranchUomByIiName(invBSTList.getItemName(), user.getCmpCode());

	    			invBSTList.clearUnitList();
	    			invBSTList.setUnitList(Constants.GLOBAL_BLANK);

	    			Iterator i = uomList.iterator();
	    			while (i.hasNext()) {

	    				InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

	    				invBSTList.setUnitList(mUomDetails.getUomName());

	    				if (mUomDetails.isDefault()) {

	    					invBSTList.setUnit(mUomDetails.getUomName());

	    				}

	    			}



	    			 //TODO: populate taglist with empty values

	    			invBSTList.clearTagList();


	         		boolean isTraceMisc =  ejbBST.getInvTraceMisc(invBSTList.getItemName(), user.getCmpCode());


	         		invBSTList.setIsTraceMisc(isTraceMisc);
	          		System.out.println(isTraceMisc + "<== trace misc item enter action");
	          		if (isTraceMisc){
	          			invBSTList.clearTagList();

	          			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), invBSTList.getQuantity());


	          			invBSTList.setMisc(misc);

	          		}



	    			// populate unit cost field

	    			if (!Common.validateRequired(invBSTList.getItemName()) &&
	    					!Common.validateRequired(invBSTList.getUnit())) {


                      double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invBSTList.getItemName(), invBSTList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                

                        /*

	    				double unitCost = ejbBST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
	    					invBSTList.getItemName(), invBSTList.getLocation(), invBSTList.getUnit(),
							Common.convertStringToSQLDate(actionForm.getDate()),
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                        */

	    				invBSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	    			}

	    			// populate amount field

	    			if (!Common.validateRequired(invBSTList.getUnitCost()) &&
	    					!Common.validateRequired(invBSTList.getQuantity())) {

	    				double amount = Common.convertStringMoneyToDouble(invBSTList.getQuantity(), precisionUnit) *
							Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit);
	    				invBSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	    			}

	    		} catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
	    						" session: " + session.getId());

	    				return mapping.findForward("cmnErrorPage");

	    			}

	    		}

	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

/*******************************************************
    Inv BST Location Enter Action --
*******************************************************/

	    	} else if(!Common.validateRequired(request.getParameter("invBSTList[" +
	    			actionForm.getRowSelected() + "].isLocationEntered"))) {

	    		InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(actionForm.getRowSelected());

	    		try {

	    			// populate unit cost field

	    			if (!Common.validateRequired(invBSTList.getItemName()) &&
	    					!Common.validateRequired(invBSTList.getUnit())) {

	    				double unitCost = ejbBST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
	    					invBSTList.getItemName(), invBSTList.getLocation(), invBSTList.getUnit(),
							Common.convertStringToSQLDate(actionForm.getDate()),
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    				invBSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	    			}

	    			// populate amount field

	    			if (!Common.validateRequired(invBSTList.getUnitCost()) &&
	    					!Common.validateRequired(invBSTList.getQuantity())) {

	    				double amount = Common.convertStringMoneyToDouble(invBSTList.getQuantity(), precisionUnit) *
							Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit);
	    				invBSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	    			}

	    		} catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
	    						" session: " + session.getId());
	    				return mapping.findForward("cmnErrorPage");

	    			}

	    		}

	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

/*******************************************************
   Inv BST Unit Enter Action --
*******************************************************/

	    	} else if(!Common.validateRequired(request.getParameter("invBSTList[" +
	    			actionForm.getRowSelected() + "].isUnitEntered"))) {

	    		InvBranchStockTransferOrderEntryList invBSTList = actionForm.getInvBSTByIndex(actionForm.getRowSelected());

	    		try {

	    			// populate unit cost field

	    			if (!Common.validateRequired(invBSTList.getItemName()) &&
	    					!Common.validateRequired(invBSTList.getUnit())) {

	    				double unitCost = ejbBST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
	    						invBSTList.getItemName(), invBSTList.getLocation(), invBSTList.getUnit(),
								Common.convertStringToSQLDate(actionForm.getDate()),
								new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    				invBSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	    			}

	    			// populate amount field

	    			if (!Common.validateRequired(invBSTList.getQuantity()) &&
	    					!Common.validateRequired(invBSTList.getUnitCost())) {

	    				double amount = Common.convertStringMoneyToDouble(invBSTList.getQuantity(), precisionUnit) *
						Common.convertStringMoneyToDouble(invBSTList.getUnitCost(), precisionUnit);
	    				invBSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	    			}

	    		} catch (EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
	    						" session: " + session.getId());

	    				return mapping.findForward("cmnErrorPage");

	    			}

	    		}

	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    	}

/*******************************************************
      -- Inv BST Load Action --
*******************************************************/

	    	if (frParam != null) {

	    		// Errors on saving

	    		if (!errors.isEmpty()) {

	    			saveErrors(request, new ActionMessages(errors));
	    			return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    		}

	    		if (request.getParameter("forward") == null &&
	    				actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	    			saveErrors(request, new ActionMessages(errors));

	    			return mapping.findForward("cmnMain");

	    		}

	    		try {

	    			// location combo box

	    			ArrayList list = null;
	    			Iterator i = null;

	    			actionForm.clearLocationList();
	    			actionForm.clearTransitLocationList();

	    			list = ejbBST.getInvLocAll(user.getCmpCode());

	    			if (list == null || list.size() == 0) {

	    				actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	    				actionForm.setTransitLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

	    			} else {

	    				i = list.iterator();
	    				while (i.hasNext()) {

	    					String locName = (String)i.next();
	    					actionForm.setLocationList(locName);
	    					actionForm.setTransitLocationList(locName);
	    				}

	    			}


	    			actionForm.clearUserList();

	            	ArrayList userList = ejbBST.getAdUsrAll(user.getCmpCode());

	            	if (userList == null || userList.size() == 0) {

	            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		Iterator x = userList.iterator();

	            		while (x.hasNext()) {

	            			actionForm.setUserList((String)x.next());

	            		}

	            	}

	    			// branch combo box

	    			actionForm.clearBranchFromList();

	    			list = ejbBST.getAdBrAll(user.getCmpCode());

	    			if (list == null || list.size() == 0) {

	    				actionForm.setBranchFromList(Constants.GLOBAL_NO_RECORD_FOUND);

	    			} else {

	    				i = list.iterator();
	    				while (i.hasNext()) {
	    					actionForm.setBranchFromList((String)i.next());
	    				}

	    			}



	    			actionForm.clearInvBSTList();

	    			if (request.getParameter("forward") != null || isInitialPrinting ) {

	    				if (request.getParameter("forward") != null ) {

	    					actionForm.setBranchStockTransferCode(new Integer(request.getParameter(
	    							"branchStockTransferCode")));

	    				}

	    				InvModBranchStockTransferDetails mdetails = ejbBST.getInvBstByBstCode(
	    						actionForm.getBranchStockTransferCode(), user.getCmpCode());

	    				//Header tab fields
	    				System.out.println("mdetails.getBstType() = =  ="+mdetails.getBstType());
	    				actionForm.setType(mdetails.getBstType());
	    				actionForm.setTransferOrderNumber(mdetails.getBstNumber());
	    				actionForm.setDate(Common.convertSQLDateToString(mdetails.getBstDate()));
	    				actionForm.setTransitLocation(mdetails.getBstTransitLocation());
	    				actionForm.setDescription(mdetails.getBstDescription());
	    				actionForm.setBrVoid(Common.convertByteToBoolean(mdetails.getBstVoid()));
	    				System.out.println(mdetails + "<== mdetails?");
	    				if (!actionForm.getBranchFromList().contains(mdetails.getBstBranchFrom())) {

	            			if (actionForm.getBranchFromList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

	            				actionForm.getBranchFromList();

	            			}

	            			actionForm.setBranchFromList(mdetails.getBstBranchFrom());
	            			System.out.println(actionForm.getBranchFrom()+ " Anu BA!!!");
	            		}

	    				actionForm.setBranchFrom(mdetails.getBstBranchFrom());

	    				//Log tab fields
	    				actionForm.setCreatedBy(mdetails.getBstCreatedBy());
	    				actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getBstDateCreated()));
	    				actionForm.setLastModifiedBy(mdetails.getBstLastModifiedBy());
	    				actionForm.setDateLastModified(Common.convertSQLDateToString(
	    						mdetails.getBstDateLastModified()));
	    				actionForm.setApprovedRejectedBy(mdetails.getBstApprovedRejectedBy());
	    				actionForm.setDateApprovedRejected(Common.convertSQLDateToString(
	    						mdetails.getBstDateApprovedRejected()));
	    				actionForm.setPostedBy(mdetails.getBstPostedBy());
	    				actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getBstDatePosted()));
	    				//Status tab fields
	    				actionForm.setApprovalStatus(mdetails.getBstApprovalStatus());
	    				actionForm.setReasonForRejection(mdetails.getBstReasonForRejection());
	    				actionForm.setPosted(mdetails.getBstPosted() == 1 ? "YES" : "NO");


	    				list = mdetails.getBstBtlList();

	    				i = list.iterator();

	    				int lineNumber = 0;

	    				while (i.hasNext()) {

	    					InvModBranchStockTransferLineDetails mBslDetails =
	    						(InvModBranchStockTransferLineDetails)i.next();

	    					InvBranchStockTransferOrderEntryList bstBslList = new
							InvBranchStockTransferOrderEntryList(actionForm, mBslDetails.getBslCode(),
								new Integer(++lineNumber).toString(), mBslDetails.getBslIiName(),
								mBslDetails.getBslLocationName(),
								Common.convertDoubleToStringMoney(mBslDetails.getBslQuantity(),quantityPrecisionUnit),
								mBslDetails.getBslUomName(),
								Common.convertDoubleToStringMoney(mBslDetails.getBslUnitCost(), precisionUnit),
								mBslDetails.getBslIiDescription(),
								Common.convertDoubleToStringMoney(mBslDetails.getBslAmount(), precisionUnit));

	    					bstBslList.setLocationList(actionForm.getLocationList());


	    					 boolean isTraceMisc = ejbBST.getInvTraceMisc(mBslDetails.getBslIiName(), user.getCmpCode());
	    					 bstBslList.setIsTraceMisc(isTraceMisc);

	    					 bstBslList.clearTagList();

			            	 ArrayList tagList = new ArrayList();
	                         if(isTraceMisc) {


	                    		 tagList = mBslDetails.getBslTagList();



	                        	if(tagList.size()>0) {

	                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mBslDetails.getBslQuantity()));
	                        		bstBslList.setMisc(misc);

	                        	}else {
	                        		if(mBslDetails.getBslMisc()==null) {
	                        			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mBslDetails.getBslQuantity()));
	                        			bstBslList.setMisc(misc);
	                        		}
	                        	}



	                         }








	    					bstBslList.clearUnitList();

	    					ArrayList unitList = ejbBST.getInvBranchUomByIiName(mBslDetails.getBslIiName(),
	    							user.getCmpCode());

	    					Iterator unitListIter = unitList.iterator();

	    					while (unitListIter.hasNext()) {

	    						InvModUnitOfMeasureDetails mUomDetails =
	    							(InvModUnitOfMeasureDetails) unitListIter.next();

	    						bstBslList.setUnitList(mUomDetails.getUomName());

	    					}

	    					actionForm.saveInvBSTList(bstBslList);

	    				}

	    				int remainingList = branchStockTransferOrderLineNumber - (list.size() %
	    						branchStockTransferOrderLineNumber) + list.size();

	    				for (int x = list.size() + 1; x <= remainingList; x++) {

	    					ArrayList comboItem = new ArrayList();

	    					InvBranchStockTransferOrderEntryList invBSTList = new InvBranchStockTransferOrderEntryList(
	    						actionForm, null, new Integer(x).toString(), null, null, null, null, null, null, null);

	    					invBSTList.setLocationList(actionForm.getLocationList());

	    					if(mdetails.getBstApprovalStatus() == null)
	    						invBSTList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);

	    					invBSTList.setUnitList(Constants.GLOBAL_BLANK);
	    					invBSTList.setUnitList("Select Item First");

	    					actionForm.saveInvBSTList(invBSTList);

	    				}

	    				this.setFormProperties(actionForm, user.getCompany());

	    				if (request.getParameter("child") == null) {
	    					System.out.println("not child??");
	    					return (mapping.findForward("invBranchStockTransferOrderEntry"));

	    				} else {
	    					System.out.println("Child??");
	    					return (mapping.findForward("invBranchStockTransferOrderEntryChild"));

	    				}

	    			}


	    			// Populate line when not forwarding

	    			for (int x = 1; x <= branchStockTransferOrderLineNumber; x++) {

	    				InvBranchStockTransferOrderEntryList invBSTList = new InvBranchStockTransferOrderEntryList(
	    					actionForm, null, new Integer(x).toString(), null, null, null, null, null, null, null);

	    				invBSTList.setLocationList(actionForm.getLocationList());
	    				invBSTList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);

	    				invBSTList.setUnitList(Constants.GLOBAL_BLANK);
	    				invBSTList.setUnitList("Select Item First");

	    				actionForm.saveInvBSTList(invBSTList);

	    			}


	    		} catch(GlobalNoRecordFoundException ex) {

	    			errors.add(ActionMessages.GLOBAL_MESSAGE,
	    					new ActionMessage("invBranchStockTransferEntry.error.recordAlreadyDeleted"));

	    		} catch(EJBException ex) {

	    			if (log.isInfoEnabled()) {

	    				log.info("EJBException caught in invBranchStockTransferOrderEntryAction.execute(): " +
	    					ex.getMessage() + " session: " + session.getId());

	    			}

	    			return(mapping.findForward("cmnErrorPage"));

	    		}

	    		// Errors on loading

	    		if (!errors.isEmpty()) {

	    			saveErrors(request, new ActionMessages(errors));

	    		} else {

	    			if (request.getParameter("saveSubmitButton") != null &&
	    					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	    					System.out.println(actionForm.getBranchStockTransferCode() + " anu ba yan");

	    				try {

	    					ArrayList list = ejbBST.getAdApprovalNotifiedUsersByBstCode(
	    							actionForm.getBranchStockTransferCode(), user.getCmpCode());

	    					if (list.isEmpty()) {

	    						messages.add(ActionMessages.GLOBAL_MESSAGE,
	    								new ActionMessage("messages.documentSentForPosting"));

	    					} else if (list.contains("DOCUMENT POSTED")) {

	    						messages.add(ActionMessages.GLOBAL_MESSAGE,
	    								new ActionMessage("messages.documentPosted"));

	    					} else {

	    						Iterator i = list.iterator();

	    						String APPROVAL_USERS = "";

	    						while (i.hasNext()) {

	    							APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

	    							if (i.hasNext()) {

	    								APPROVAL_USERS = APPROVAL_USERS + ", ";

	    							}


	    						}

	    						messages.add(ActionMessages.GLOBAL_MESSAGE,
	    								new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

	    					}

	    					saveMessages(request, messages);
	    					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);


	    				} catch(EJBException ex) {

	    					if (log.isInfoEnabled()) {

	    						log.info("EJBException caught in invBranchStockTransferOrderEntryAction.execute(): " +
	    								ex.getMessage() + " session: " + session.getId());

	    					}

	    					return(mapping.findForward("cmnErrorPage"));

	    				}

	    			} else if (request.getParameter("saveAsDraftButton") != null &&
	    					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	    				actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

	    			}


	    		}

	    		actionForm.reset(mapping, request);

	    		actionForm.setBranchStockTransferCode(null);
	    		actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
	    		actionForm.setPosted("NO");
	    		actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
	    		actionForm.setCreatedBy(user.getUserName());
	    		actionForm.setLastModifiedBy(user.getUserName());
	    		actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
	    		actionForm.setBranchFrom("DPSC");
	    		actionForm.setTransitLocation("IN TRANSIT");

	    		this.setFormProperties(actionForm, user.getCompany());
	    		return(mapping.findForward("invBranchStockTransferOrderEntry"));

	    	} else {

	    		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	    		saveErrors(request, new ActionMessages(errors));

	    		return(mapping.findForward("cmnMain"));

	    	}

	    } catch(Exception e) {

/*******************************************************
      System Failed: Forward to error page
*******************************************************/

	    	if (log.isInfoEnabled()) {

	    		log.info("Exception caught in InvBranchStockTransferOrderEntryAction.execute(): " + e.getMessage()
	    				+ " session: " + session.getId());
	    	}

	    	e.printStackTrace();
	    	return(mapping.findForward("cmnErrorPage"));

	    }

	}


	private void setFormProperties(InvBranchStockTransferOrderEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO") && !actionForm.getBrVoid()) {

				if (actionForm.getBranchStockTransferCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableBrVoid(false);

				} else if (actionForm.getBranchStockTransferCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableBrVoid(true);


				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableBrVoid(true);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				System.out.println("actionForm.getBrVoid()-" + actionForm.getBrVoid());
				if (!actionForm.getBrVoid()) {
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setEnableBrVoid(true);
				} else {
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setEnableBrVoid(false);

				}

			}

		} else {

			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableBrVoid(false);

		}
		// view attachment

		if (actionForm.getBranchStockTransferCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/inv/BSOS/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

 			File file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getBranchStockTransferCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}

		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}

}
