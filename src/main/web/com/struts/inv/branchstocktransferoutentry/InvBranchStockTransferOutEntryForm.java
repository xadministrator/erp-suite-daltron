package com.struts.inv.branchstocktransferoutentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;

public class InvBranchStockTransferOutEntryForm extends ActionForm implements Serializable { 

	private Integer branchStockTransferCode = null;		
	private String date = null;
	private String transferOutNumber = null;
	private String transferOrderNumber = null;
	private Integer transferOrderCode = null;
	private String branchTo = null;
	private String transitLocation = null;
	private ArrayList branchToList = new ArrayList();
	private ArrayList transferOrderNumberList = new ArrayList();
	private ArrayList transitLocationList = new ArrayList();
	private ArrayList locationList = new ArrayList();
	private ArrayList typeList = new ArrayList();
	private String type = null;
	private String description = null;
	private boolean brVoid = false;
	private boolean enableBrVoid = false;

	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String reasonForRejection = null;
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	private ArrayList invBSTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveSubmitButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false; 
	private boolean showBSOSMatchedLinesDetails = true;
	private String attachment = null;
	private String attachmentPDF = null;
	private String report = null;
	private ArrayList userList = new ArrayList();
	
	

	public ArrayList getUserList() {
		
		return userList;
		
	}
	
	public void setUserList(String user) {
		
	   userList.add(user);
		
	}
  
	public void clearUserList() {
	   	
	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);
	   	
	}
  
	public String getAttachment() {
		   	
		return attachment;
	   	
	}
	
	public void setAttachment(String attachment) {
	   	
		this.attachment = attachment;
		
	}
	
	public String getAttachmentPDF() {
	   	
		return attachmentPDF;
	   	
	}
	
	public void setAttachmentPDF(String attachmentPDF) {
	   	
		this.attachmentPDF = attachmentPDF;
		
	}
	
	public boolean getShowViewAttachmentButton1() {
	   	   
	   	   return showViewAttachmentButton1;
	   	
   }
   
   public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
   	
   	   this.showViewAttachmentButton1 = showViewAttachmentButton1;
   	
   }
   
   public boolean getShowViewAttachmentButton2() {
   	   
   	   return showViewAttachmentButton2;
   	
   }
   
   public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
   	
   	   this.showViewAttachmentButton2 = showViewAttachmentButton2;
   	
   }
   
   public boolean getShowViewAttachmentButton3() {
   	   
   	   return showViewAttachmentButton3;
   	
   }
   
   public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
   	
   	   this.showViewAttachmentButton3 = showViewAttachmentButton3;
   	
   }
   
   public boolean getShowViewAttachmentButton4() {
   	   
   	   return showViewAttachmentButton4;
   	
   }
   
   public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
   	
   	   this.showViewAttachmentButton4 = showViewAttachmentButton4;
   	
   }
   public FormFile getFilename1() {
	   	
   	  return filename1;
   	
   }
   
   public void setFilename1(FormFile filename1) {
   	  
   	  this.filename1 = filename1;
   	
   }
   
   public FormFile getFilename2() {
   	
   	  return filename2;
   	
   }
   
   public void setFilename2(FormFile filename2) {
   
   	  this.filename2 = filename2;
   	
   }
   
   public FormFile getFilename3() {
   	
   	  return filename3;
   	
   }
   
   public void setFilename3(FormFile filename3) {
   	  
   	  this.filename3 = filename3;
   	
   }
   
   public FormFile getFilename4() {
   	
   	  return filename4;
   	
   }
   
   public void setFilename4(FormFile filename4) {
   	  
   	  this.filename4 = filename4;
   	
   }
   public boolean isShowBSOSMatchedLinesDetails() {
		return showBSOSMatchedLinesDetails;
   }
	
   public void setShowBSOSMatchedLinesDetails(boolean showBSOSMatchedLinesDetails) {
		
		this.showBSOSMatchedLinesDetails = showBSOSMatchedLinesDetails;
		
   }
	
	public String getReport() {	    
		return report;	    
	}
	public void setReport(String report) {
		this.report = report;	    
	}

	public int getRowSelected(){
		return rowSelected;
	}

	public InvBranchStockTransferOutEntryList getInvBSTByIndex(int index){
		return((InvBranchStockTransferOutEntryList)invBSTList.get(index));
	}

	public Object[] getInvBSTList(){
		return(invBSTList.toArray());
	}

	public int getInvBSTListSize(){
		return(invBSTList.size());
	}

	public void saveInvBSTList(Object newInvBSTList){
		invBSTList.add(newInvBSTList);
	}

	public void clearInvBSTList(){
		invBSTList.clear();
	}

	public void setRowSelected(Object selectedInvBSTList, boolean isEdit){
		this.rowSelected = invBSTList.indexOf(selectedInvBSTList);
	}

	public void updateInvBSTRow(int rowSelected, Object newInvBSTList){
		invBSTList.set(rowSelected, newInvBSTList);
	}

	public void deleteInvBSTList(int rowSelected){
		invBSTList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}
	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}



	public String getUserPermission(){
		return(userPermission);
	}
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}     	



	public Integer getBranchStockTransferCode() {
		return branchStockTransferCode;
	}
	public void setBranchStockTransferCode(Integer branchStockTransferCode) {
		this.branchStockTransferCode = branchStockTransferCode;
	}



	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}



	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}



	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}		



	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}



	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}



	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}



	public String getDateLastModified() {
		return dateLastModified;
	}
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}



	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}

	public String getType(){
		return type;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public ArrayList getTypeList(){
		return typeList;
	}

	public boolean getEnableFields() {
		return enableFields;
	}
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}



	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Integer getTransferOrderCode() {
		return transferOrderCode;
	}
	public void setTransferOrderCode(Integer transferOrderCode) {
		this.transferOrderCode = transferOrderCode;
	}

	public String getTransferOutNumber() {
		return transferOutNumber;
	}
	public void setTransferOutNumber(String transferOutNumber) {
		this.transferOutNumber = transferOutNumber;
	}	
	
	public String getTransferOrderNumber() {
		return transferOrderNumber;
	}
	public void setTransferOrderNumber(String transferOrderNumber) {
		this.transferOrderNumber = transferOrderNumber;
	}	

	public ArrayList getTransferOrderNumberList() {
		   	
		return transferOrderNumberList;
		   	
	}
		   
	public void setTransferOrderNumberList(String transferOrderNumber) {
		   	
		transferOrderNumberList.add(transferOrderNumber);
		   	
	}
		   
	public void clearTransferOrderNumberList() {
		   	
		transferOrderNumberList.clear();
		transferOrderNumberList.add(Constants.GLOBAL_BLANK);
		   	
	}

	public String getBranchTo() {
		return branchTo;
	}
	public void setBranchTo(String branchTo) {
		this.branchTo = branchTo;
	}	



	public ArrayList getBranchToList() {
		return branchToList;
	}
	public void setBranchToList(String branchToList) {
		this.branchToList.add(branchToList);
	}

	public void clearBranchToList() {

		branchToList.clear();
		branchToList.add(Constants.GLOBAL_BLANK);

	}

	public String getTransitLocation() {
		return transitLocation;
	}
	public void setTransitLocation(String transitLocation) {
		this.transitLocation = transitLocation;
	}	

	public ArrayList getTransitLocationList() {
		return transitLocationList;
	}
	public void setTransitLocationList(String transitLocationList) {
		this.transitLocationList.add(transitLocationList);
	}

	public void clearTransitLocationList() {

		transitLocationList.clear();
		transitLocationList.add(Constants.GLOBAL_BLANK);

	}


	public ArrayList getLocationList() {
		return locationList;
	}

	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}



	public String getReasonForRejection() {
		return reasonForRejection;
	}
	public void setReasonForRejection(String reasonForRejection) {
		this.reasonForRejection = reasonForRejection;
	}



	public String getPosted() {
		return posted;
	}
	public void setPosted(String posted) {
		this.posted = posted;
	}

	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}

	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}

	public boolean getShowSaveSubmitButton() {
		return showSaveSubmitButton;
	}

	public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {
		this.showSaveSubmitButton = showSaveSubmitButton;
	}

	public boolean isShowSaveButton() {
		return showSaveButton;
	}

	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}

	public boolean getShowDeleteButton() {
		return showDeleteButton;
	}
	public void setShowDeleteButton(boolean showDeleteButton) {
		this.showDeleteButton = showDeleteButton;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getBrVoid() {
		return brVoid;
	}
	public void setBrVoid(boolean brVoid) {
		this.brVoid = brVoid;
	}

	public boolean getEnableBrVoid() {
		return enableBrVoid;
	}
	public void setEnableBrVoid(boolean enableBrVoid) {
		this.enableBrVoid = enableBrVoid;
	}


	public void reset(ActionMapping mapping, HttpServletRequest request){      

		date = null;
	//	transferOutNumber = null;
		transferOrderNumber = null;
		branchTo = null;
		transitLocation = null;
		description = null;
		brVoid = false;
		approvalStatus = null;
		posted = null;
		filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;   
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		type = null;
		typeList.clear();
		typeList.add("BSOS-MATCHED");
		typeList.add("ITEMS");
		postedBy = null;
		datePosted = null;	
		reasonForRejection = null;
		attachment = null;
		attachmentPDF = null;		
		for (int i=0; i<invBSTList.size(); i++) {

			InvBranchStockTransferOutEntryList actionList = (InvBranchStockTransferOutEntryList)invBSTList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			actionList.setIsLocationEntered(null);
		
		}
		
	}

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		//ActionErrors errors = new ActionErrors();

		Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		//y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		System.out.println("ctr :" + ctr);

		/*if(y==0)
			   return new ArrayList();*/

		ArrayList miscList = new ArrayList();

		for(int x=0; x<ctr; x++) {
			try {	        	

				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				/*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				String checker = misc.substring(start, start + length);
				if(checker!=""&& checker != " "){
					miscList.add(checker);	 
				}else{
					miscList.add("null");
				}

				//System.out.println(misc.substring(start, start + length));
			} catch (Exception ex) {

			}

		}
		return miscList;
	}


	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

		ActionErrors errors = new ActionErrors();

		if(request.getParameter("saveSubmitButton") != null || 
				request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("journalButton") != null ||
				request.getParameter("printButton") != null) { 

			if(Common.validateRequired(date)){
				errors.add("date", new ActionMessage("invBranchStockTransferEntry.error.dateRequired"));
			}

			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("invBranchStockTransferEntry.error.dateInvalid"));
			}

			System.out.println(date.lastIndexOf("/"));
			System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date", 
						new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
			}

			if(Common.validateRequired(branchTo)){
				errors.add("branchTo", new ActionMessage("invBranchStockTransferEntry.error.branchToRequired"));
			}

			if(Common.validateRequired(transitLocation)){
				errors.add("transitLocation", new ActionMessage("invBranchStockTransferEntry.error.transitLocationRequired"));
			}

			int numberOfLines = 0;
			
			 String isDupItem = "";
            int y=0;

            Iterator i = invBSTList.iterator();
   	 

			while (i.hasNext()) {

				InvBranchStockTransferOutEntryList bslList = (InvBranchStockTransferOutEntryList)i.next();      	 	 

				if (Common.validateRequired(bslList.getItemName()) &&
						Common.validateRequired(bslList.getQuantity()) &&
						Common.validateRequired(bslList.getUnit()) || Common.convertStringMoneyToDouble(bslList.getQuantity(), (short)10) == 0) continue;

				numberOfLines++;

			
				if(Common.validateRequired(bslList.getItemName()) || bslList.getItemName().equals(Constants.GLOBAL_BLANK)){
					errors.add("itemName", new ActionMessage("invBranchStockTransferEntry.error.itemNameRequired", bslList.getLineNumber()));
				}

				if(Common.validateRequired(bslList.getLocation()) || bslList.getLocation().equals(Constants.GLOBAL_BLANK)){
					errors.add("location", new ActionMessage("invBranchStockTransferEntry.error.locationRequired", bslList.getLineNumber()));
				}

				if(Common.validateRequired(bslList.getQuantity())) {
					errors.add("quantity", new ActionMessage("invBranchStockTransferEntry.error.quantityRequired", bslList.getLineNumber()));
				}

				if(!Common.validateNumberFormat(bslList.getQuantity())){
					errors.add("quantity", new ActionMessage("invBranchStockTransferEntry.error.quantityInvalid", bslList.getLineNumber()));
				}

				if(!Common.validateRequired(bslList.getQuantity()) && Common.convertStringMoneyToDouble(bslList.getQuantity(), (short)3) == 0) {
					errors.add("quantity", new ActionMessage("invBranchStockTransferEntry.error.zeroQuantityNotAllowed", bslList.getLineNumber()));
				}

				if(!Common.validateRequired(bslList.getQuantity()) && Common.convertStringMoneyToDouble(bslList.getQuantity(), (short)3) < 0) {
					errors.add("quantity", new ActionMessage("invBranchStockTransferEntry.error.negativeQuantityNotAllowed", bslList.getLineNumber()));
				}

				if(Common.validateRequired(bslList.getUnit()) || bslList.getUnit().equals(Constants.GLOBAL_BLANK)){
					errors.add("unit", new ActionMessage("invBranchStockTransferEntry.error.unitRequired", bslList.getLineNumber()));
				}

				if(Common.validateRequired(bslList.getUnitCost())){
					errors.add("unitCost",
							new ActionMessage("invBranchStockTransferEntry.error.unitCostRequired", bslList.getLineNumber()));
				}

				if(!Common.validateMoneyFormat(bslList.getUnitCost())){
					errors.add("unitCost",
							new ActionMessage("invBranchStockTransferEntry.error.unitCostInvalid", bslList.getLineNumber()));
				}
				
				if(Common.validateRequired(transferOutNumber)){
                   errors.add("transferOutNumber",
                      new ActionMessage("salesOrderEntry.error.documentNumberRequired"));
                }
                
                 for(int x=0;x<invBSTList.size();x++){
                
                    InvBranchStockTransferOutEntryList bslListTrace = (InvBranchStockTransferOutEntryList)invBSTList.get(x);  
                    
                    if(bslListTrace.getItemName().equals(bslList.getItemName()) && x!=y){
                        isDupItem = bslList.getItemName();
                        break;
                    
                    }
                               
                }
                
                y++;

			}
			
			if(isDupItem.length()>0){
               errors.add("branchStockTransfer",
                        new ActionMessage("invBranchStockTransferEntry.error.branchStockTransferItemDuplicateExist"));

            }

			if (numberOfLines == 0) {

				errors.add("branchStockTransfer",
						new ActionMessage("invBranchStockTransferEntry.error.branchStockTransferMustHaveLine"));

			}	  	    


		} else if (!Common.validateRequired(request.getParameter("isTransferOrderNumberEntered"))) {
			
			if(Common.validateRequired(transferOrderNumber) || transferOrderNumber.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("transferOrderNumber",
						new ActionMessage("invBranchStockTransferEntry.error.transferOrderNumberRequired"));
			}

		}
		
		return(errors);	
	}



}

