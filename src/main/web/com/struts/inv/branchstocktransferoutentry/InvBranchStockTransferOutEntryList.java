package com.struts.inv.branchstocktransferoutentry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.inv.adjustmententry.InvAdjustmentEntryTagList;

public class InvBranchStockTransferOutEntryList implements Serializable {

    private Integer branchStockTransferOutLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String quantity = null;

	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String itemDescription = null;
	private String amount = null;
	private boolean deleteCheckbox = false;
	private boolean isTraceMisc = false;

	private String isTypeEntered = null;
	private String isItemEntered = null;
	private String isLocationEntered = null;
	private String isUnitEntered = null;
	private String misc = null;
	private String partNumber = null;
	private ArrayList tagList = new ArrayList();


	private InvBranchStockTransferOutEntryForm parentBean;

	public InvBranchStockTransferOutEntryList(InvBranchStockTransferOutEntryForm parentBean,
			Integer branchStockTransferOutLineCode, String lineNumber, String itemName, String location,
			String quantity, String unit, String unitCost, String itemDescription, String amount, String misc, String partNumber) {

		    this.parentBean = parentBean;
		    this.branchStockTransferOutLineCode = branchStockTransferOutLineCode;
		    this.lineNumber = lineNumber;
		    this.itemName = itemName;
		    this.location = location;
		    this.quantity = quantity;
		    this.unit = unit;
		    this.unitCost = unitCost;
		    this.itemDescription = itemDescription;
		    this.amount = amount;
		    this.misc = misc;
		    this.partNumber = partNumber;

	}

	public InvBranchStockTransferOutEntryForm getParentBean() {

	    return parentBean;

	}
	public void setParentBean(InvBranchStockTransferOutEntryForm parentBean) {

	    this.parentBean = parentBean;

	}



	public Integer getBranchStockTransferOutLineCode() {

	    return branchStockTransferOutLineCode;

	}
	public void setBranchStockTransferOutLineCode(Integer branchStockTransferOutLineCode) {

	    this.branchStockTransferOutLineCode = branchStockTransferOutLineCode;

	}



	public String getLineNumber() {

	    return lineNumber;

	}
	public void setLineNumber(String lineNumber) {

	    this.lineNumber = lineNumber;

	}



	public String getItemName() {

	    return itemName;

	}
	public void setItemName(String itemName) {

	    this.itemName = itemName;

	}

	public InvBranchStockTransferOutEntryTagList getTagListByIndex(int index){
	      return((InvBranchStockTransferOutEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}


	public String getLocation() {

	    return location;

	}
	public void setLocation(String location) {

	    this.location = location;

	}



	public String getQuantity() {

	    return quantity;

	}
	public void setQuantity(String quantity) {

	    this.quantity = quantity;

	}



	public String getUnit() {

	    return unit;

	}
	public void setUnit(String unit) {

	    this.unit = unit;

	}



	public String getUnitCost() {

	    return unitCost;

	}
	public void setUnitCost(String unitCost) {

	    this.unitCost = unitCost;

	}



	public String getItemDescription() {

	    return itemDescription;

	}
	public void setItemDescription(String itemDescription) {

	    this.itemDescription = itemDescription;

	}



	public String getAmount() {

	    return amount;

	}
	public void setAmount(String amount) {

	    this.amount = amount;
	}



	public ArrayList getLocationList() {

	    return locationList;

	}
	public void setLocationList(ArrayList locationList) {

	    this.locationList = locationList;

	}



	public ArrayList getUnitList() {

	    return unitList;

	}
	public void setUnitList(String unit) {

	    unitList.add(unit);

	}



	public void clearUnitList() {

	    unitList.clear();

	}

	public boolean isDeleteCheckbox() {

	    return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

	    this.deleteCheckbox = deleteCheckbox;

	}

	public boolean getIsTraceMisc() {

	    return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

	    this.isTraceMisc = isTraceMisc;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}
	public String getIsTypeEntered() {

	   	  return isTypeEntered;

	}

	public void setIsTypeEntered(String isTypeEntered) {

		if (isTypeEntered != null && isTypeEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isTypeEntered = null;

	}
	public String getIsLocationEntered() {

		return isLocationEntered;

	}

	public void setIsLocationEntered(String isLocationEntered) {

		if (isLocationEntered != null && isLocationEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isLocationEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isUnitEntered = null;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}

	public String getPartNumber() {

		return partNumber;

	}

	public void setPartNumber(String partNumber) {

		this.partNumber = partNumber;
	}
}
