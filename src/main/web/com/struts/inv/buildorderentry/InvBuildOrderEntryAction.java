package com.struts.inv.buildorderentry;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArINVNoSalesOrderLinesFoundException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalOverapplicationNotAllowedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.InvBuildOrderEntryController;
import com.ejb.txn.InvBuildOrderEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.Debug;
import com.util.InvBuildOrderDetails;
import com.util.InvModBuildOrderDetails;
import com.util.InvModBuildOrderLineDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvBuildOrderEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvBuildOrderEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvBuildOrderEntryForm actionForm = (InvBuildOrderEntryForm)form;

         // reset report
	      
	     actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.INV_BUILD_ORDER_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invBuildOrderEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invBuildOrderEntryController EJB
*******************************************************/

         InvBuildOrderEntryControllerHome homeBOR = null;
         InvBuildOrderEntryController ejbBOR = null;

         try {
            
            homeBOR = (InvBuildOrderEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvBuildOrderEntryControllerEJB", InvBuildOrderEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvBuildOrderEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbBOR = homeBOR.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvBuildOrderEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call InvBuildOrderEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short quantityPrecisionUnit = 0;
         short buildOrderLineNumber = 0;
         boolean isInitialPrinting = false;
         
         try {
         	
            precisionUnit = ejbBOR.getGlFcPrecisionUnit(user.getCmpCode());
            quantityPrecisionUnit = ejbBOR.getInvGpQuantityPrecisionUnit(user.getCmpCode());
            buildOrderLineNumber = ejbBOR.getInvGpInventoryLineNumber(user.getCmpCode());        
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Inv BOR Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvBuildOrderDetails details = new InvBuildOrderDetails();
           
           details.setBorCode(actionForm.getBuildOrderCode());
           details.setBorReferenceNumber(actionForm.getReferenceNumber());
           details.setBorDocumentNumber(actionForm.getDocumentNumber());
           details.setBorDescription(actionForm.getDescription());
           details.setBorVoid(Common.convertBooleanToByte(actionForm.getBorVoid()));
           details.setBorDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setBorCreatedBy(actionForm.getCreatedBy());
           details.setBorDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
           details.setBorLastModifiedBy(actionForm.getLastModifiedBy());
           details.setBorDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                      
           if (actionForm.getBuildOrderCode() == null) {
           	
           		details.setBorCreatedBy(user.getUserName());
           		details.setBorDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
                      
           details.setBorLastModifiedBy(user.getUserName());
           details.setBorDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
           
           ArrayList bolList = new ArrayList(); 
          
           for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
           	
           	   InvBuildOrderEntryList invBORList = actionForm.getInvBORByIndex(i);           	   
           	              	             	   
	           if (Common.validateRequired(invBORList.getLocation()) &&
	         	 	     Common.validateRequired(invBORList.getItemName())&&
						 Common.validateRequired(invBORList.getQuantityRequired()) &&
						 Common.validateRequired(invBORList.getUnitCost()))continue;
           	   
           	   InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
           	   
           	   mdetails.setBolQuantityRequired(Common.convertStringMoneyToDouble(invBORList.getQuantityRequired(), quantityPrecisionUnit));
           	   mdetails.setBolIlLocName(invBORList.getLocation());
           	   mdetails.setBolIlIiName(invBORList.getItemName());
           	   mdetails.setBolMisc(invBORList.getMisc());
           	   System.out.println("invBORList.getMisc(): " + invBORList.getMisc());
           	   bolList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    ejbBOR.saveInvBorEntry(details, bolList, true,
           	    		new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPosted"));
                    
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.transactionAlreadyVoid"));
           
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.documentNumberNotUnique"));
                     
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.invitemLocationNotFound"));
           	   
           } catch (GlobalInventoryDateException ex) {
	           
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invBuildOrderEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));          	   
           	   
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Inv BOR Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
         	InvBuildOrderDetails details = new InvBuildOrderDetails();
            
            details.setBorCode(actionForm.getBuildOrderCode());
            details.setBorReferenceNumber(actionForm.getReferenceNumber());
            details.setBorDocumentNumber(actionForm.getDocumentNumber());
            details.setBorDescription(actionForm.getDescription());
            details.setBorVoid(Common.convertBooleanToByte(actionForm.getBorVoid()));
            details.setBorDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setBorCreatedBy(actionForm.getCreatedBy());
            details.setBorDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
            details.setBorLastModifiedBy(actionForm.getLastModifiedBy());
            details.setBorDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                       
                       
            if (actionForm.getBuildOrderCode() == null) {
            	
            		details.setBorCreatedBy(user.getUserName());
            		details.setBorDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
 	                      
            }
                       
            details.setBorLastModifiedBy(user.getUserName());
            details.setBorDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
            
            ArrayList bolList = new ArrayList();
                                  
            for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
            	
            	InvBuildOrderEntryList invBORList = actionForm.getInvBORByIndex(i);
            	              	             	   
 	           	if (Common.validateRequired(invBORList.getLocation()) &&
 	         	 	     Common.validateRequired(invBORList.getItemName())&& 
						 Common.validateRequired(invBORList.getQuantityRequired())) continue;
            	   
            	   InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
            	   
               	   mdetails.setBolQuantityRequired(Common.convertStringMoneyToDouble(invBORList.getQuantityRequired(), quantityPrecisionUnit));
               	   mdetails.setBolIlLocName(invBORList.getLocation());
               	   mdetails.setBolIlIiName(invBORList.getItemName());
               	mdetails.setBolMisc(invBORList.getMisc());
               	System.out.println("invBORList.getMisc(): " + invBORList.getMisc());
               	   bolList.add(mdetails);
               	
            }                     	
            
            try {
            	
            	    Integer buildOrderCode = ejbBOR.saveInvBorEntry(details, bolList, false,
            	    		new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    actionForm.setBuildOrderCode(buildOrderCode);
            	
            } catch (GlobalRecordAlreadyDeletedException ex) {
            	
            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
            	
            } catch (GlobalTransactionAlreadyApprovedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyApproved"));
            	
            } catch (GlobalTransactionAlreadyPendingException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPending"));
            	
            } catch (GlobalTransactionAlreadyPostedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPosted"));
            	   
            } catch (GlobalTransactionAlreadyVoidException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invBuildOrderEntry.error.transactionAlreadyVoid"));
               
           } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invBuildOrderEntry.error.documentNumberNotUnique"));
                         
           } catch (GlobalInvItemLocationNotFoundException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invBuildOrderEntry.error.invitemLocationNotFound"));
               	   
           } catch (GlobalInventoryDateException ex) {
	           
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invBuildOrderEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));               	                  	
                         	 	                        
            } catch (EJBException ex) {
            	    if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                 }
                
                return(mapping.findForward("cmnErrorPage"));
            }
 
/*******************************************************
       	 --  INV Print Action --
*******************************************************/             	
                   	
      } else if (request.getParameter("printButton") != null) {

      	if(Common.validateRequired(actionForm.getApprovalStatus())) {
      		
      		InvBuildOrderDetails details = new InvBuildOrderDetails();
      		
      		details.setBorCode(actionForm.getBuildOrderCode());
      		details.setBorReferenceNumber(actionForm.getReferenceNumber());
      		details.setBorDocumentNumber(actionForm.getDocumentNumber());
      		details.setBorDescription(actionForm.getDescription());
      		details.setBorVoid(Common.convertBooleanToByte(actionForm.getBorVoid()));
      		details.setBorDate(Common.convertStringToSQLDate(actionForm.getDate()));
      		details.setBorCreatedBy(actionForm.getCreatedBy());
      		details.setBorDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
      		details.setBorLastModifiedBy(actionForm.getLastModifiedBy());
      		details.setBorDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
      		
      		
      		if (actionForm.getBuildOrderCode() == null) {
      			
      			details.setBorCreatedBy(user.getUserName());
      			details.setBorDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
      			
      		}
      		
      		details.setBorLastModifiedBy(user.getUserName());
      		details.setBorDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
      		
      		ArrayList bolList = new ArrayList();
      		
      		for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
      			
      			InvBuildOrderEntryList invBORList = actionForm.getInvBORByIndex(i);
      			
      			if (Common.validateRequired(invBORList.getLocation()) &&
      					Common.validateRequired(invBORList.getItemName())&& 
						Common.validateRequired(invBORList.getQuantityRequired())) continue;
      			
      			InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
      			
      			mdetails.setBolQuantityRequired(Common.convertStringMoneyToDouble(invBORList.getQuantityRequired(), quantityPrecisionUnit));
      			mdetails.setBolIlLocName(invBORList.getLocation());
      			mdetails.setBolIlIiName(invBORList.getItemName());
      			mdetails.setBolIlIiUomName(invBORList.getUnit());
      			mdetails.setBolMisc(invBORList.getMisc());
      			bolList.add(mdetails);
      			
      		}                     	
      		
      		try {
      			
      			Integer buildOrderCode = ejbBOR.saveInvBorEntry(details, bolList, true,
      					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			actionForm.setBuildOrderCode(buildOrderCode);
      			
      		} catch (GlobalRecordAlreadyDeletedException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
      			
      		} catch (GlobalTransactionAlreadyApprovedException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.transactionAlreadyApproved"));
      			
      		} catch (GlobalTransactionAlreadyPendingException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPending"));
      			
      		} catch (GlobalTransactionAlreadyPostedException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPosted"));
      			
      		} catch (GlobalTransactionAlreadyVoidException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.transactionAlreadyVoid"));
      			
      		} catch (GlobalDocumentNumberNotUniqueException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.documentNumberNotUnique"));
      			
      		} catch (GlobalInvItemLocationNotFoundException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("invBuildOrderEntry.error.invitemLocationNotFound"));
      			
      		} catch (GlobalInventoryDateException ex) {
 	           
        	    errors.add(ActionMessages.GLOBAL_MESSAGE,
        	            new ActionMessage("invBuildOrderEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
      			      			
      		} catch (EJBException ex) {
      			if (log.isInfoEnabled()) {
      				
      				log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      		}
      		
      		if (!errors.isEmpty()) {
      			
      			saveErrors(request, new ActionMessages(errors));
      			return(mapping.findForward("invBuildOrderEntry"));
      			
      		}
      		
      		actionForm.setReport(Constants.STATUS_SUCCESS);
      		
      		isInitialPrinting = true;
      		
      	}  else {
      		
      		actionForm.setReport(Constants.STATUS_SUCCESS);
      		
      		return(mapping.findForward("invBuildOrderEntry"));
      		
      	}  	  
            
/*******************************************************
    -- Ar BOR SOL Save As Draft Action --
*******************************************************/
                 
     } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("SO MATCHED") &&
             actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         
    	 
    	 InvBuildOrderDetails details = new InvBuildOrderDetails();
         
         details.setBorCode(actionForm.getBuildOrderCode());
         details.setBorSoNumber(actionForm.getSoNumber());
         details.setBorReferenceNumber(actionForm.getReferenceNumber());
         details.setBorDocumentNumber(actionForm.getDocumentNumber());
         details.setBorDescription(actionForm.getDescription());
         details.setBorVoid(Common.convertBooleanToByte(actionForm.getBorVoid()));
         details.setBorDate(Common.convertStringToSQLDate(actionForm.getDate()));
         details.setBorCreatedBy(actionForm.getCreatedBy());
         details.setBorDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
         details.setBorLastModifiedBy(actionForm.getLastModifiedBy());
         details.setBorDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                    
         if (actionForm.getBuildOrderCode() == null) {
         	
         		details.setBorCreatedBy(user.getUserName());
         		details.setBorDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         }
                    
         details.setBorLastModifiedBy(user.getUserName());
         details.setBorDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         
         ArrayList solList = new ArrayList();
         
         for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
             
             InvBuildOrderEntryList invSOLList = actionForm.getInvBORByIndex(i);           	   
             
             if(!invSOLList.getIssueCheckbox()) continue;
             
             if (Common.validateRequired(invSOLList.getLocation()) &&
                     Common.validateRequired(invSOLList.getItemName()) &&
                     Common.validateRequired(invSOLList.getUnit()) &&
                     Common.validateRequired(invSOLList.getUnitCost()) && 
                     Common.validateRequired(invSOLList.getQuantityRequired())) continue;
             
             InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
             
             mdetails.setBolLineNumber(invSOLList.getLineNumber());
             mdetails.setBolIlIiName(invSOLList.getItemName());
             mdetails.setBolIlIiDescription(invSOLList.getItemDescription());
             mdetails.setBolIlLocName(invSOLList.getLocation());            	   
             mdetails.setBolQuantityRequired(Common.convertStringMoneyToDouble(invSOLList.getQuantityRequired(), quantityPrecisionUnit));
             mdetails.setBolIlIiUomName(invSOLList.getUnit());
             mdetails.setBolUnitCost(Common.convertStringMoneyToDouble(invSOLList.getUnitCost(), precisionUnit));
             mdetails.setBolTotalCost(Common.convertStringMoneyToDouble(invSOLList.getTotalCost(), precisionUnit));           	               	   
             mdetails.setBolTotalDiscount(Common.convertStringMoneyToDouble(invSOLList.getTotalDiscount(), precisionUnit));
             
             solList.add(mdetails);
             
         }                     	
         
         try {
        	 
             ejbBOR.saveInvBorSolEntry(details, solList, true, 
            		 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
             
         } catch (GlobalRecordAlreadyDeletedException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
             
         } catch (GlobalDocumentNumberNotUniqueException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.documentNumberNotUnique"));
             
         } catch (GlobalTransactionAlreadyApprovedException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyApproved"));
             
         } catch (GlobalTransactionAlreadyPendingException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPending"));
             
         } catch (GlobalTransactionAlreadyPostedException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPosted"));
             
         } catch (GlobalTransactionAlreadyVoidException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyVoid"));
             
         } catch (GlobalInvItemLocationNotFoundException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.invitemLocationNotFound", ex.getMessage()));
             
         } catch (GlobalTransactionAlreadyLockedException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.transactionAlreadyLocked"));
             
         } catch (GlobalInventoryDateException ex) {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invBuildOrderEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
             
    	 } catch (GlobalOverapplicationNotAllowedException ex) {

    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
    				 new ActionMessage("invBuildOrderEntry.error.quantityMustNotBeGreaterThanRemainingQuantity"));

         } catch (EJBException ex) {
             if (log.isInfoEnabled()) {
                 
                 log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                         " session: " + session.getId());
             }
             
             return(mapping.findForward("cmnErrorPage"));
         } 
         
                 
/*******************************************************
	-- Ar BOR SOL Save and Submit Action --
*******************************************************/
                      
      } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("SO MATCHED") &&
              actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
          
     	 
     	 InvBuildOrderDetails details = new InvBuildOrderDetails();
          
          details.setBorCode(actionForm.getBuildOrderCode());
          details.setBorSoNumber(actionForm.getSoNumber());
          details.setBorReferenceNumber(actionForm.getReferenceNumber());
          details.setBorDocumentNumber(actionForm.getDocumentNumber());
          details.setBorDescription(actionForm.getDescription());
          details.setBorVoid(Common.convertBooleanToByte(actionForm.getBorVoid()));
          details.setBorDate(Common.convertStringToSQLDate(actionForm.getDate()));
          details.setBorCreatedBy(actionForm.getCreatedBy());
          details.setBorDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
          details.setBorLastModifiedBy(actionForm.getLastModifiedBy());
          details.setBorDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                     
          if (actionForm.getBuildOrderCode() == null) {
          	
          		details.setBorCreatedBy(user.getUserName());
          		details.setBorDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
          }
                     
          details.setBorLastModifiedBy(user.getUserName());
          details.setBorDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
          
          ArrayList solList = new ArrayList();
          
          for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
              
              InvBuildOrderEntryList invSOLList = actionForm.getInvBORByIndex(i);           	   
              
              if(!invSOLList.getIssueCheckbox()) continue;
              
              if (Common.validateRequired(invSOLList.getLocation()) &&
                      Common.validateRequired(invSOLList.getItemName()) &&
                      Common.validateRequired(invSOLList.getUnit()) &&
                      Common.validateRequired(invSOLList.getUnitCost()) && 
                      Common.validateRequired(invSOLList.getQuantityRequired())) continue;
              
              InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
              
              mdetails.setBolLineNumber(invSOLList.getLineNumber());
              mdetails.setBolIlIiName(invSOLList.getItemName());
              mdetails.setBolIlIiDescription(invSOLList.getItemDescription());
              mdetails.setBolIlLocName(invSOLList.getLocation());            	   
              mdetails.setBolQuantityRequired(Common.convertStringMoneyToDouble(invSOLList.getQuantityRequired(), quantityPrecisionUnit));
              mdetails.setBolIlIiUomName(invSOLList.getUnit());
              mdetails.setBolUnitCost(Common.convertStringMoneyToDouble(invSOLList.getUnitCost(), precisionUnit));
              mdetails.setBolTotalCost(Common.convertStringMoneyToDouble(invSOLList.getTotalCost(), precisionUnit));           	               	   
              mdetails.setBolTotalDiscount(Common.convertStringMoneyToDouble(invSOLList.getTotalDiscount(), precisionUnit));
              
              solList.add(mdetails);
              
          }                     	
          
          try {
         	 
              Integer buildOrderCode = ejbBOR.saveInvBorSolEntry(details, solList, false , 
             		 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
              actionForm.setBuildOrderCode(buildOrderCode);
              
          } catch (GlobalRecordAlreadyDeletedException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
              
          } catch (GlobalDocumentNumberNotUniqueException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.documentNumberNotUnique"));
              
          } catch (GlobalTransactionAlreadyApprovedException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.transactionAlreadyApproved"));
              
          } catch (GlobalTransactionAlreadyPendingException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPending"));
              
          } catch (GlobalTransactionAlreadyPostedException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.transactionAlreadyPosted"));
              
          } catch (GlobalTransactionAlreadyVoidException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.transactionAlreadyVoid"));
              
          } catch (GlobalInvItemLocationNotFoundException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.invitemLocationNotFound", ex.getMessage()));
              
          } catch (GlobalTransactionAlreadyLockedException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.transactionAlreadyLocked"));
              
          } catch (GlobalInventoryDateException ex) {
              
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invBuildOrderEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
              
          } catch (GlobalOverapplicationNotAllowedException ex) {

        	  errors.add(ActionMessages.GLOBAL_MESSAGE,
        			  new ActionMessage("invBuildOrderEntry.error.quantityMustNotBeGreaterThanRemainingQuantity"));

          } catch (EJBException ex) {
              if (log.isInfoEnabled()) {
                  
                  log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
              }
              
              return(mapping.findForward("cmnErrorPage"));
          } 
          
                      
/*******************************************************
   -- Inv BOR Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbBOR.deleteInvBorEntry(actionForm.getBuildOrderCode(), user.getUserName(), 
           	    		new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
        
/*******************************************************
   -- Inv BOR Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Inv BOR Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	         	         	
         	int listSize = actionForm.getInvBORListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + buildOrderLineNumber; x++) {
	        	
            	ArrayList comboItem = new ArrayList();
            	
	        	InvBuildOrderEntryList invBORList = new InvBuildOrderEntryList(actionForm, 
	        	    null, null, null, null, null, null, null, null, null, null, null, false, null);	
	        	
	        	invBORList.setLocationList(actionForm.getLocationList());	        
	        	
	        	actionForm.saveInvBORList(invBORList);
	        	
	        }		                    
           
	        
	        return(mapping.findForward("invBuildOrderEntry"));
	        
/*******************************************************
	-- Inv BOR Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getInvBORListSize(); i++) {
           	
           	   InvBuildOrderEntryList invBORList = actionForm.getInvBORByIndex(i);
           	   
           	 if (invBORList.getDeleteCheckbox()) {
           	   	
           	   		actionForm.deleteInvBORList(i);
           	   		i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("invBuildOrderEntry"));        

/**************************************************
	-- INV BOR Type Enter Action --
*******************************************************/
	        
         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

        	 if (actionForm.getType().equalsIgnoreCase("SO MATCHED")){
        		 
		         actionForm.setShowSOMatchedLinesDetails(true);
        		 actionForm.setShowAddLinesButton(false);
		         actionForm.setShowDeleteLinesButton(false);
		         	
	         } else if (actionForm.getType().equalsIgnoreCase("ITEMS")){
	         		
        		 actionForm.setShowSOMatchedLinesDetails(false);
        		 actionForm.setShowAddLinesButton(true);
        		 actionForm.setShowDeleteLinesButton(true);
	         }

        	 actionForm.clearInvBORList();
	         
             for (int x = 1; x <= buildOrderLineNumber; x++) {
                 
            	 InvBuildOrderEntryList invSOList = new InvBuildOrderEntryList(actionForm, 
     	        	    null, null, null, null, null, null, null, null, null, null, null, false, null);	
                 
            	 invSOList.setLocationList(actionForm.getLocationList());	        
	        	
	        	actionForm.saveInvBORList(invSOList);
             }
        	 
        	 this.setFormProperties(actionForm);
        	 
        	 return(mapping.findForward("invBuildOrderEntry"));

	 		         
/*******************************************************
    -- Inv BOR SO Number Enter Action --
*******************************************************/        
		            
         } else if(!Common.validateRequired(request.getParameter("isSalesOrderEntered"))) {
	             
             if (actionForm.getType().equals("SO MATCHED")) {
	                 
                 try {
                     
                     ArModSalesOrderDetails mdetails = ejbBOR.getArSoBySoDocumentNumberAndAdBranch(
                             actionForm.getSoNumber(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                     
                     actionForm.clearInvBORList();
                     actionForm.setCustomerName(mdetails.getSoCstName());
                     actionForm.setDescription(mdetails.getSoDocumentNumber() + " " + mdetails.getSoDescription());
                     
                     Iterator i = mdetails.getSoSolList().iterator();
                     
                     while (i.hasNext()) {
                         
                         ArModSalesOrderLineDetails mSolDetails = (ArModSalesOrderLineDetails)i.next();
                         InvModBuildOrderLineDetails mBolDetails = new InvModBuildOrderLineDetails();
                         
                         InvBuildOrderEntryList invBOLList = new InvBuildOrderEntryList(
                        		 actionForm,
                        		 mBolDetails.getBolCode(), 
                                 mSolDetails.getSolLocName(), 
                                 mSolDetails.getSolIiName(),
                                 mSolDetails.getSolIiDescription(), 
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolQuantity(), quantityPrecisionUnit),
                                 mSolDetails.getSolUomName(),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolUnitPrice(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolAmount(), precisionUnit),
                                 Common.convertShortToString(mSolDetails.getSolLine()),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolRemaining(), quantityPrecisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolTotalDiscount(), precisionUnit),
                                 mSolDetails.getSolIssue(),
                                 mBolDetails.getBolMisc());

                         
                         invBOLList.setLocationList(actionForm.getLocationList());
                         
                         invBOLList.clearUnitList();
                         
                         ArrayList unitList = ejbBOR.getSolUomByIiName(mSolDetails.getSolIiName(), user.getCmpCode());
                         
                         Iterator unitListIter = unitList.iterator();
                         
                         while (unitListIter.hasNext()) {
                             
                             InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                             invBOLList.setUnitList(mUomDetails.getUomName()); 
                             
                         }
                         
                         actionForm.saveInvBORList(invBOLList);
                         
                     }	
                     
                 } catch (ArINVNoSalesOrderLinesFoundException ex) {
                     
                     actionForm.clearInvBORList();
                     
                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invBuildOrderEntry.error.noSalesOrderLinesFound"));
                     saveErrors(request, new ActionMessages(errors));
                     
                     
                 } catch (EJBException ex) {
                     
                     if (log.isInfoEnabled()) {
                         
                         log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                         return mapping.findForward("cmnErrorPage"); 
                         
                     }
                     
                 }
                 
             } 
             
             return(mapping.findForward("invBuildOrderEntry"));            

/*******************************************************
    -- Inv BOR Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invBORList[" + 
        actionForm.getRowSelected() + "].isItemEntered"))) {
        
      	InvBuildOrderEntryList invBORList = actionForm.getInvBORByIndex(actionForm.getRowSelected());      	    	
                      
      	try {
          	
      		// populate unit field class
      		
      		invBORList.setUnit(ejbBOR.getInvUomByIiName(invBORList.getItemName(), user.getCmpCode())); 
      		
      		// set unit cost
      		double unitCost = ejbBOR.getInvIiUnitCostByIiNameAndUomName(invBORList.getItemName(), invBORList.getUnit(), user.getCmpCode());
      		invBORList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
      		
      	} catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("invBuildOrderEntry"));

/*******************************************************
   -- Inv BOR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
         	// Errors on saving
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("invBuildOrderEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	// location combo box
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearLocationList();       	
            	
            	list = ejbBOR.getInvLocAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setLocationList((String)i.next());
            		}
            	}            	            
            	
            	actionForm.clearInvBORList();   
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			
            			actionForm.setBuildOrderCode(new Integer(request.getParameter("buildOrderCode")));
            			actionForm.setType(request.getParameter("type"));
            		}
            		
            		InvModBuildOrderDetails mdetails = ejbBOR.getInvBorByBorCode(actionForm.getBuildOrderCode(),
            				request.getParameter("type"), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());            	
            		
            		actionForm.setReferenceNumber(mdetails.getBorReferenceNumber());
            		actionForm.setDocumentNumber(mdetails.getBorDocumentNumber());
            		actionForm.setDescription(mdetails.getBorDescription());
            		actionForm.setBorVoid(Common.convertByteToBoolean(mdetails.getBorVoid()));
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getBorDate()));
            		actionForm.setApprovalStatus(mdetails.getBorApprovalStatus());
            		actionForm.setPosted(mdetails.getBorPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getBorCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getBorDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getBorLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getBorDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getBorApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getBorDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getBorPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getBorDatePosted()));
            		actionForm.setReasonForRejection(mdetails.getBorReasonForRejection());
            		
            		if(actionForm.getType().equals("SO MATCHED")) {
	            		actionForm.setSoNumber(mdetails.getBorSoNumber());
	            		actionForm.setCustomerName(mdetails.getBorCustomerName());
            		}
            		
            		list = mdetails.getBorBolList();           		
            		
            		i = list.iterator();
            		
            		short lineNumber = 1;
            		
            		ArrayList issuedSolList = new ArrayList();
            		
            		while (i.hasNext()) {
            			
        				InvModBuildOrderLineDetails mBolDetails = (InvModBuildOrderLineDetails)i.next();
            			
            			InvBuildOrderEntryList borBolList = new InvBuildOrderEntryList(
            					actionForm, 
            					mBolDetails.getBolCode(), 
            					mBolDetails.getBolIlLocName(), 
            					mBolDetails.getBolIlIiName(), 
            					mBolDetails.getBolIlIiDescription(),
								Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityRequired(), quantityPrecisionUnit), 
								mBolDetails.getBolIlIiUomName(), 
								Common.convertDoubleToStringMoney(mBolDetails.getBolUnitCost(), quantityPrecisionUnit), 
								Common.convertDoubleToStringMoney(mBolDetails.getBolTotalCost(), quantityPrecisionUnit),
								Common.convertShortToString(lineNumber++),
								Common.convertDoubleToStringMoney(mBolDetails.getBolRemaining(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(mBolDetails.getBolTotalDiscount(), quantityPrecisionUnit),
								mBolDetails.getBolIssue(),
								mBolDetails.getBolMisc()); 	
            			
            			
            			borBolList.setLocationList(actionForm.getLocationList());

                  		// set unit cost & total cost
            			if(actionForm.getType().equals("ITEMS")) {
	                  		double unitCost = ejbBOR.getInvIiUnitCostByIiNameAndUomName(borBolList.getItemName(), borBolList.getUnit(), user.getCmpCode());
	                  		borBolList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
	                  		borBolList.setTotalCost(Common.convertDoubleToStringMoney(mBolDetails.getBolQuantityRequired() * unitCost, precisionUnit));
            			
            			} else {
            				borBolList.clearUnitList();
                            ArrayList unitList = ejbBOR.getSolUomByIiName(mBolDetails.getBolIlIiName(), user.getCmpCode());
                            Iterator unitListIter = unitList.iterator();
                            while (unitListIter.hasNext()) {
                                
                                InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                borBolList.setUnitList(mUomDetails.getUomName()); 
                            }
                            issuedSolList.add(mBolDetails.getBolCode());
            			}
                  		
            			actionForm.saveInvBORList(borBolList);
            		}
            		
            		this.setFormProperties(actionForm);
            		
            		//displaying of UNISSUED SO
            		if(actionForm.getType().equals("SO MATCHED") && (actionForm.getEnableFields())) {
            				
                        ArrayList solList = ejbBOR.getArNotIssuedSolBySoNumber(actionForm.getBuildOrderCode(),
                        		issuedSolList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                        
                        Iterator j = solList.iterator();
                        
                        while (j.hasNext()) {
                            
                        	InvModBuildOrderLineDetails solDetails = (InvModBuildOrderLineDetails) j.next();

                        	InvBuildOrderEntryList borBolList = new InvBuildOrderEntryList(
                                    actionForm, 
                                    solDetails.getBolCode(), 
                                    solDetails.getBolIlLocName(), 
                                    solDetails.getBolIlIiName(),
                                    solDetails.getBolIlIiDescription(),
    								Common.convertDoubleToStringMoney(solDetails.getBolQuantityRequired(), quantityPrecisionUnit), 
    								solDetails.getBolIlIiUomName(), 
    								Common.convertDoubleToStringMoney(solDetails.getBolUnitCost(), quantityPrecisionUnit), 
    								Common.convertDoubleToStringMoney(solDetails.getBolTotalCost(), quantityPrecisionUnit),
    								Common.convertShortToString(lineNumber++),
    								Common.convertDoubleToStringMoney(solDetails.getBolRemaining(), quantityPrecisionUnit),
    								Common.convertDoubleToStringMoney(solDetails.getBolTotalDiscount(), quantityPrecisionUnit),
    								solDetails.getBolIssue(),
    								solDetails.getBolMisc()); 	
                        	
                            
                        	borBolList.setLocationList(actionForm.getLocationList()); 
                            
                        	borBolList.clearUnitList();
                            ArrayList unitList = ejbBOR.getSolUomByIiName(solDetails.getBolIlIiName(), user.getCmpCode());
                            Iterator unitListIter = unitList.iterator();
                            while (unitListIter.hasNext()) {
                                
                                InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                borBolList.setUnitList(mUomDetails.getUomName()); 
                            }	
                            actionForm.saveInvBORList(borBolList);
                        }
        			}
            		
            		if(actionForm.getType().equals("ITEMS")) {
	            		int remainingList = buildOrderLineNumber - (list.size() % buildOrderLineNumber) + list.size();
	            		
	            		for (int x = list.size() + 1; x <= remainingList; x++) {
	            			
	            			InvBuildOrderEntryList invBORList = new InvBuildOrderEntryList(actionForm, 
	            					null, null, null, null, null, null, null, null, null, null, null, false, null);	
	            			
	            			invBORList.setLocationList(actionForm.getLocationList());
	            			
	            			actionForm.saveInvBORList(invBORList);
	            		}	
            		}
            		
            		this.setFormProperties(actionForm);
            		
            		if (request.getParameter("child") == null) {
            			
            			return (mapping.findForward("invBuildOrderEntry"));         
            			
            		} else {
            			
            			return (mapping.findForward("invBuildOrderEntryChild"));         
            			
            		}
            	}     
            		
        		// Populate line when not forwarding
  
                    
        		for (int x = 1; x <= buildOrderLineNumber; x++) {
        			
        			InvBuildOrderEntryList invBORList = new InvBuildOrderEntryList(actionForm, 
        					null, null, null, null, null, null, null, null, null, null, null, false, null);	
        			
        			invBORList.setLocationList(actionForm.getLocationList());
        			
        			actionForm.saveInvBORList(invBORList);
        			
        		}       	
                        
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invBuildOrderEntry.error.recordAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 
            
            // Errors on loading
         
            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                   	   
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);                   	
                  	       
               } else if (request.getParameter("saveAsDraftButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }

               
            }
            
            actionForm.reset(mapping, request);              

            actionForm.setBuildOrderCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setShowSOMatchedLinesDetails(false);
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("invBuildOrderEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in InvBuildOrderEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(InvBuildOrderEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO") && !actionForm.getBorVoid()) {
				
				if (actionForm.getBuildOrderCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableBuildOrderVoid(false);
														
				} else if (actionForm.getBuildOrderCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableBuildOrderVoid(true);								    
				    	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableBuildOrderVoid(false);
										
				}
				
			} else if (actionForm.getPosted().equals("YES")) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setEnableBuildOrderVoid(false);
												
			} else if (actionForm.getBorVoid()) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableBuildOrderVoid(false);
				
			}
			
			
			
		} else {			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableBuildOrderVoid(false);
						
		}	
		    
	}
		
}