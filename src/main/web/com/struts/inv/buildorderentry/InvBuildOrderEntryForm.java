package com.struts.inv.buildorderentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.invoiceentry.ArInvoiceSalesOrderList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class InvBuildOrderEntryForm extends ActionForm implements Serializable {

	private Integer buildOrderCode = null;
	private String date = null;
	private String referenceNumber = null;
	private String documentNumber = null;
	private String description = null;
	private boolean borVoid = false;
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String reasonForRejection = null;
	
   
	private ArrayList invBORList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean enableBuildOrderVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;

	
	//MRS support
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String customer = null;
	private ArrayList customerList = new ArrayList();   
	private String soNumber = null;
	private String customerName = null;
	
	private boolean showSOMatchedLinesDetails = false;
	private String isCustomerEntered  = null;
	private String isTypeEntered = null;
	private String isSalesOrderEntered = null;

	
	private String report = null;
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
         
	public int getRowSelected(){
		return rowSelected;
	}

	public InvBuildOrderEntryList getInvBORByIndex(int index){
		return((InvBuildOrderEntryList)invBORList.get(index));
	}

	public Object[] getInvBORList(){
		return(invBORList.toArray());
	}

	public int getInvBORListSize(){
		return(invBORList.size());
	}

	public void saveInvBORList(Object newInvBORList){
		invBORList.add(newInvBORList);
	}

	public void clearInvBORList(){
		invBORList.clear();
	}

	public void setRowSelected(Object selectedInvBORList, boolean isEdit){
		this.rowSelected = invBORList.indexOf(selectedInvBORList);
	}

	public void updateInvBORRow(int rowSelected, Object newInvBORList){
		invBORList.set(rowSelected, newInvBORList);
	}

	public void deleteInvBORList(int rowSelected){
		invBORList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
     
	public Integer getBuildOrderCode() {
		return buildOrderCode;
	}
	
	public void setBuildOrderCode(Integer buildOrderCode) {
		this.buildOrderCode = buildOrderCode;
	}
	
	public String getApprovalStatus() {
		return approvalStatus;
	}
	
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public boolean getEnableFields() {
		return enableFields;
	}
	
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}	
	
	public ArrayList getLocationList() {
		return locationList;
	}
	
	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}
	
	public void clearLocationList() {
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getReasonForRejection() {
	   	
	   	  return reasonForRejection;
	   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
	
	public String getPosted() {
		return posted;
	}
	
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean getBorVoid() {
		return borVoid;
	}
	
	public void setBorVoid(boolean borVoid) {
		this.borVoid = borVoid;
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	
	public boolean getEnableBuildOrderVoid() {
	   	
	   	   return enableBuildOrderVoid;
	   	
	   }
	   
	public void setEnableBuildOrderVoid(boolean enableBuildOrderVoid) {
	   	
	   	   this.enableBuildOrderVoid = enableBuildOrderVoid;
	   	
	   }
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowSaveButton() {
		return showSaveButton;
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	
	//MRS support
	public String getType(){
		return type;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public ArrayList getTypeList(){
		return typeList;
	}

	public boolean isShowSOMatchedLinesDetails() {
		return showSOMatchedLinesDetails;
	}
	
	public void setShowSOMatchedLinesDetails(boolean showSOMatchedLinesDetails) {
		this.showSOMatchedLinesDetails = showSOMatchedLinesDetails;
	}
	
	public String getCustomer() {		
		return customer;	
	}
	
	public void setCustomer(String customer) {		
		this.customer = customer;
		
	}
	
	public ArrayList getCustomerList() {
		return customerList;
	}
	
	public void setCustomerList(String customer) {
		customerList.add(customer);
	}
	
	public void clearCustomerList() {
		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);
	}
	
	public String getSoNumber() {		
		return soNumber;		
	}
	
	public void setSoNumber(String soNumber) {		
		this.soNumber = soNumber;		
	}     

    public String getCustomerName() {
    	return customerName;
    }
    
    public void setCustomerName(String customerName) {
    	this.customerName = customerName;
    }

    public String getIsCustomerEntered() {		
		return isCustomerEntered;		
	}
	
	public String getIsTypeEntered() {
		return isTypeEntered;
	}

	public String getIsSalesOrderEntered() {	
		return isSalesOrderEntered;
	}
	
	public void setIsSalesOrderEntered(String isSalesOrderEntered) {
		this.isSalesOrderEntered = isSalesOrderEntered;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       location = Constants.GLOBAL_BLANK;
       date = null;
	   referenceNumber = null;
	   documentNumber = null;
	   description = null;
	   borVoid = false;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   reasonForRejection = null;
	   
	   //MRS support
	   type = null;
	   typeList.clear();
	   typeList.add("ITEMS");
	   typeList.add("SO MATCHED");
	   customer = Constants.GLOBAL_BLANK;
	   soNumber = null;
	   customerName = Constants.GLOBAL_BLANK;
	   isCustomerEntered = null;
	   isTypeEntered = null;
	   

	   for (int i=0; i<invBORList.size(); i++) {
		   
		   InvBuildOrderEntryList actionList = (InvBuildOrderEntryList)invBORList.get(i);
		   actionList.setIsItemEntered(null);
		   actionList.setIssueCheckbox(false);
	   } 
   }
	
	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		//ActionErrors errors = new ActionErrors();

		Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		//y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		System.out.println("ctr :" + ctr);

		/*if(y==0)
			   return new ArrayList();*/

		ArrayList miscList = new ArrayList();

		for(int x=0; x<ctr; x++) {
			try {	        	

				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				/*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				String checker = misc.substring(start, start + length);
				if(checker!=""&& checker != " "){
					miscList.add(checker);	 
				}else{
					miscList.add("null");
				}

				//System.out.println(misc.substring(start, start + length));
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		return miscList;
	}



   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || request.getParameter("saveAsDraftButton") != null || 
		  request.getParameter("printButton") != null) { 
      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("invBuildOrderEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	        errors.add("date", 
		       new ActionMessage("invBuildOrderEntry.error.dateInvalid"));
		 }
		 
		 if(type.equals("ITEMS")) {
			 
			 int numberOfLines = 0;
		      	 
	      	 Iterator i = invBORList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      		 
	      		 InvBuildOrderEntryList ilList = (InvBuildOrderEntryList)i.next();      	 	 
				 
	      	 	 if (Common.validateRequired(ilList.getLocation()) &&
	      	 	     Common.validateRequired(ilList.getItemName()) &&
					 Common.validateRequired(ilList.getQuantityRequired())) continue;
	      	 	     
	      	 	 numberOfLines++;
	      	 	 /*
	      	 	try{
	      	 		String separator = "$";
	      	 		String misc = "";
	      		   // Remove first $ character
	      		   misc = ilList.getMisc().substring(1);
	      		   
	      		   // Counter
	      		   int start = 0;
	      		   int nextIndex = misc.indexOf(separator, start);
	      		   int length = nextIndex - start;	
	      		   int counter;
	      		   counter = Integer.parseInt(misc.substring(start, start + length));
	      		   
	      	 		ArrayList miscList = this.getExpiryDateStr(ilList.getMisc(), counter);
	      	 		System.out.println("rilList.getMisc() : " + ilList.getMisc());
	      	 		Iterator mi = miscList.iterator();
	     	 		
	      	 		int ctrError = 0;
	      	 		int ctr = 0;
	      	 		
	      	 		while(mi.hasNext()){
	      	 				String miscStr = (String)mi.next();
	      	 			    
	      	 				if(!Common.validateDateFormat(miscStr)){
	      	 					errors.add("date", 
	      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
	      	 					ctrError++;
	      	 				}
	      	 				
	      	 				System.out.println("miscStr: "+miscStr);
	      	 				if(miscStr=="null"){
	      	 					ctrError++;
	      	 				}
	      	 		}
	      	 		//if ctr==Error => No Date Will Apply
	      	 		//if ctr>error => Invalid Date
	      	 		System.out.println("CTR: "+  miscList.size());
	      	 		System.out.println("ctrError: "+  ctrError);
	      	 		System.out.println("counter: "+  counter);
	      	 			if(ctrError>0 && ctrError!=miscList.size()){
	      	 				errors.add("date", 
	      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
	      	 			}
	      	 			
	      	 		//if error==0 Add Expiry Date
	      	 		
	      	 		
	      	 	}catch(Exception ex){
	  	  	    	ex.printStackTrace();
	  	  	    }*/
	      	 
		         if(Common.validateRequired(ilList.getLocation())){
		            errors.add("location",
		            	new ActionMessage("invBuildOrderEntry.error.locationRequired", String.valueOf(numberOfLines)));
		         }
		         if(Common.validateRequired(ilList.getItemName())){
		            errors.add("itemName",
		            	new ActionMessage("invBuildOrderEntry.error.itemNameRequired", String.valueOf(numberOfLines)));
		         }
		         if(Common.validateRequired(ilList.getQuantityRequired())){
		            errors.add("quantityRequired",
		            	new ActionMessage("invBuildOrderEntry.error.quantityRequiredRequired", String.valueOf(numberOfLines)));
		         }
		         if(!Common.validateNumberFormat(ilList.getQuantityRequired())){
		            errors.add("quantityRequired",
		            	new ActionMessage("invBuildOrderEntry.error.quantityRequiredInvalid", String.valueOf(numberOfLines)));
		         }
		        
		         if(!Common.validateRequired(ilList.getQuantityRequired()) && Common.convertStringMoneyToDouble(ilList.getQuantityRequired(), (short)3) <= 0) {
		         	errors.add("quantityRequired",
		         		new ActionMessage("invBuildOrderEntry.error.negativeOrZeroQuantityRequiredNotAllowed", ilList.getItemName()));
		         }
		         
		         if(!Common.validateDecimalExist(ilList.getQuantityRequired())){
			        errors.add("quantityRequired",
			           new ActionMessage("invBuildOrderEntry.error.decimalQuantityRequiredNotAllowed", ilList.getItemName()));
			     }
		         
	      	 }
	      	 
	      	 if (numberOfLines == 0) {
	      		 
	      		 errors.add("buildorder",
	      				 new ActionMessage("invBuildOrderEntry.error.buildorderMustHaveLine"));
	      		 
	      	 }
	      	 
		 } else {
			 
			 int numOfLines = 0;
				
			 Iterator i = invBORList.iterator();      	 
				
			 while (i.hasNext()) {
				 
				 InvBuildOrderEntryList solList = (InvBuildOrderEntryList)i.next();
				 
				 if(!solList.getIssueCheckbox()) continue; 
					
				 if (Common.validateRequired(solList.getLocation()) &&
						 Common.validateRequired(solList.getItemName()) &&
						 Common.validateRequired(solList.getQuantityRequired()) &&
						 Common.validateRequired(solList.getUnit()) &&
						 Common.validateRequired(solList.getUnitCost())) continue;
				
				 numOfLines++;
				
				 if(Common.validateRequired(solList.getLocation()) || solList.getLocation().equals(Constants.GLOBAL_BLANK)){
					 
					 errors.add("location",
							 new ActionMessage("invBuildOrderEntry.error.locationRequired", solList.getLineNumber()));
				 }
				 if(Common.validateRequired(solList.getItemName()) || solList.getItemName().equals(Constants.GLOBAL_BLANK)){
					 errors.add("itemName",
							 new ActionMessage("invBuildOrderEntry.error.itemNameRequired", solList.getLineNumber()));
				 }
				 if(Common.validateRequired(solList.getQuantityRequired())) {
					 errors.add("quantity",
							 new ActionMessage("invBuildOrderEntry.error.quantityRequired", solList.getLineNumber()));
				 }
				 if(!Common.validateNumberFormat(solList.getQuantityRequired())){
					 errors.add("quantity",
							 new ActionMessage("invBuildOrderEntry.error.quantityInvalid", solList.getLineNumber()));
				 }
				 if(!Common.validateRequired(solList.getQuantityRequired()) && Common.convertStringMoneyToDouble(solList.getQuantityRequired(), (short)3) <= 0) {
					 errors.add("quantity",
							 new ActionMessage("invBuildOrderEntry.error.negativeOrZeroQuantityNotAllowed", solList.getLineNumber()));
				 }
				 if(Common.validateRequired(solList.getUnit()) || solList.getUnit().equals(Constants.GLOBAL_BLANK)){
					 errors.add("unit",
							 new ActionMessage("invBuildOrderEntry.error.unitRequired", solList.getLineNumber()));
				 }
				 if(Common.validateRequired(solList.getUnitCost())){
					 errors.add("unitPrice",
							 new ActionMessage("invBuildOrderEntry.error.unitPriceRequired", solList.getLineNumber()));
				 }
				 if(!Common.validateMoneyFormat(solList.getUnitCost())){
					 errors.add("unitPrice",
							 new ActionMessage("invBuildOrderEntry.error.unitPriceInvalid", solList.getLineNumber()));
				 }
			 }
			 
			 if (numOfLines == 0) {

				 errors.add("buildorder",
						 new ActionMessage("invBuildOrderEntry.error.buildorderMustHaveLine"));
			 }
		 }
      } 

      return(errors);	
   }
}
