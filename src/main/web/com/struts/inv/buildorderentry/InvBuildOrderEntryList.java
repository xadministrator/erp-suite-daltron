package com.struts.inv.buildorderentry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvBuildOrderEntryList implements Serializable {

	private Integer buildOrderLineCode = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String quantityRequired = null;
	private String unit = null;
	private boolean deleteCheckbox = false;
	
	private boolean enableUnitCost = false;
	private String unitCost = null;
	private String totalCost = null;
	
	private String isItemEntered = null;
	
	private String misc = null;
	
	//SO MATCHED support
	private String lineNumber = null;
	private String remaining = null;
	private String totalDiscount = null;
	private boolean issueCheckbox = false;
	private String isUnitEntered = null;
	private ArrayList unitList = new ArrayList();
	
	
	private InvBuildOrderEntryForm parentBean;   

	public InvBuildOrderEntryList(InvBuildOrderEntryForm parentBean,
		Integer buildOrderLineCode, String location, String itemName, 
		String itemDescription, String quantityRequired, String unit,
		String unitCost, String totalCost, String lineNumber, 
		String remaining, String totalDiscount, boolean issueCheckBox, String misc){
	
		this.parentBean = parentBean;
		this.buildOrderLineCode = buildOrderLineCode;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.quantityRequired = quantityRequired;
		this.unit = unit;
		this.unitCost = unitCost;
		this.totalCost = totalCost;
		this.lineNumber = lineNumber;
		this.remaining = remaining;
		this.totalDiscount = totalDiscount;
		this.issueCheckbox = issueCheckBox;
		this.misc = misc;

	}
	
	public InvBuildOrderEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvBuildOrderEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public Integer getBuildOrderLineCode() {
		
		return buildOrderLineCode;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
		
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getQuantityRequired() {
		
		return quantityRequired;
		
	}
	
	public void setQuantityRequired(String quantityRequired) {
		
		this.quantityRequired = quantityRequired;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	
	public boolean getEnableUnitCost() {
		
		return enableUnitCost;
		
	}
	
	public void setEnableUnitCost(boolean enableUnitCost) {
		
		this.enableUnitCost = enableUnitCost;
		
	}
	
	public String getUnitCost() {
		
	    return unitCost;		
		
	}
	
	public void setUnitCost(String unitCost) {
	    
	    this.unitCost = unitCost;
		
	}
	
	public String getTotalCost() {
		
	    return totalCost;		
		
	}
	
	public void setTotalCost(String totalCost) {
	    
	    this.totalCost = totalCost;
		
	}
	
	//SO MATCHED support
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}

	public String getRemaining() {
		
		return remaining;
		
	}
	
	public void setRemaining(String remaining) {
		
		this.remaining = remaining;
		
	}
	
	public String getTotalDiscount() {
		
		return totalDiscount;
		
	}
	
	public void setTotalDiscount(String totalDiscount) {
		
		this.totalDiscount = totalDiscount;
		
	}

	public boolean getIssueCheckbox() {
		
		return issueCheckbox;
		
	}
	
	public void setIssueCheckbox(boolean issueCheckbox) {
		
		this.issueCheckbox = issueCheckbox;
		
	}		
	
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}

	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
		
		unitList.clear();
		
	}
	
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}
	
}

