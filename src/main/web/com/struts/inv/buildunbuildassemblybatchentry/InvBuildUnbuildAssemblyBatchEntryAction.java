package com.struts.inv.buildunbuildassemblybatchentry;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.InvBuildUnbuildAssemblyBatchEntryController;
import com.ejb.txn.InvBuildUnbuildAssemblyBatchEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvBuildUnbuildAssemblyBatchDetails;

public final class InvBuildUnbuildAssemblyBatchEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvBuildUnbuildAssemblyBatchEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         
         InvBuildUnbuildAssemblyBatchEntryForm actionForm = (InvBuildUnbuildAssemblyBatchEntryForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.INV_BUILD_UNBUILD_ASSEMBLY_BATCH_ENTRY_ID);
         
         if (frParam != null) {
        	 
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	        	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
            	   
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invbuildUnbuildAssemblyBatchEntry"));
                
               }
             
            }
	        
            actionForm.setUserPermission(frParam.trim());
          
         } else {
        	 
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invbuildUnbuildAssemblyBatchEntryController EJB
*******************************************************/

         
         
         InvBuildUnbuildAssemblyBatchEntryControllerHome homeIB = null;
         InvBuildUnbuildAssemblyBatchEntryController ejbIB = null;
         

         try {
        	 
            homeIB = (InvBuildUnbuildAssemblyBatchEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvBuildUnbuildAssemblyBatchEntryControllerEJB", InvBuildUnbuildAssemblyBatchEntryControllerHome.class);
            
         } catch(NamingException e) {
        	 
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
        	 
            ejbIB = homeIB.create();
            
         } catch(CreateException e) {
        	 
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- AR IB Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	
        	
        	 InvBuildUnbuildAssemblyBatchDetails details = new InvBuildUnbuildAssemblyBatchDetails();
           
           details.setBbCode(actionForm.getBuildUnbuildAssemblyBatchCodeCode());
           details.setBbName(actionForm.getBatchName());
           details.setBbDescription(actionForm.getDescription());
           details.setBbStatus(actionForm.getStatus());
           details.setBbType(actionForm.getType());
           
           if (actionForm.getBuildUnbuildAssemblyBatchCodeCode() == null) {
           	
           	   details.setBbCreatedBy(user.getUserName());
	           details.setBbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbIB.saveArBbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.recordAlreadyDeleted"));
                               	
          /* } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.transactionBatchClose"));           	                      	
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.saveRecordAlreadyAssigned"));            
           */
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }           
           
/*******************************************************
   -- AR IB BUA Action --
*******************************************************/

         } else if (request.getParameter("invoiceButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
        	 InvBuildUnbuildAssemblyBatchDetails details = new InvBuildUnbuildAssemblyBatchDetails();
           
           details.setBbCode(actionForm.getBuildUnbuildAssemblyBatchCodeCode());
           details.setBbName(actionForm.getBatchName());
           details.setBbDescription(actionForm.getDescription());
           details.setBbStatus(actionForm.getStatus());
           details.setBbType(actionForm.getType());
           
           if (actionForm.getBuildUnbuildAssemblyBatchCodeCode() == null) {
           	
           	   details.setBbCreatedBy(user.getUserName());
	           details.setBbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbIB.saveArBbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.recordAlreadyDeleted"));
                               	
           /*} catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.transactionBatchClose"));           	                      	
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.saveRecordAlreadyAssigned"));            
           
           	*/
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }     
           
           if (errors.isEmpty()) {
     	   
	     	   String path = null;
	     	
	     	   if (actionForm.getType().equals("BUA")) {
	     	   	
	         	 path = "/invBuildUnbuildAssemblyEntry.do.do?forwardBatch=1" +
				         "&batchName=" + actionForm.getBatchName();	
	     	                      	   	
	     	   } else {
	     	
	     	   	 path = "/arCreditMemoEntry.do?forwardBatch=1" +
			         	  "&batchName=" + actionForm.getBatchName();                   	   	 
	     	   	
	     	   }
	     	                      	   
	     	   return(new ActionForward(path));
	     	 
	        }

           
/*******************************************************
   -- AR IB Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbIB.deleteArBbEntry(actionForm.getBuildUnbuildAssemblyBatchCodeCode(), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.recordAlreadyDeleted"));
                    
            } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.deleteRecordAlreadyAssigned"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- AR IB Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- AR IB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("invBuildUnbuildAssemblyBatchEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {            	
            	
            	if (request.getParameter("forward") != null) {
            		            			            	    
            		actionForm.setBuildUnbuildAssemblyBatchCode(new Integer(request.getParameter("buildUnbuildAssemblyBatchCode")));            		            		
            		
            		InvBuildUnbuildAssemblyBatchDetails details = ejbIB.getArBbByBbCode(actionForm.getBuildUnbuildAssemblyBatchCodeCode(), user.getCmpCode());
            		
            		actionForm.setBatchName(details.getBbName());
            		actionForm.setDescription(details.getBbDescription());
            		actionForm.setCreatedBy(details.getBbCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(details.getBbDateCreated()));
            		actionForm.setStatus(details.getBbStatus());
            		actionForm.setType(details.getBbType());            		
            					         
			        this.setFormProperties(actionForm);
			         
			        return (mapping.findForward("invBuildUnbuildAssemblyBatchEntry"));   			         
			         
            		
            	}            	
	                    		           			   	
            } catch(GlobalNoRecordFoundException ex) {            	            	
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyBatchEntry.error.buildUnbuildAssemblyBatchAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setBuildUnbuildAssemblyBatchCode(null);          
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setStatus("OPEN");
            actionForm.setType("INVOICE");
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("invBuildUnbuildAssemblyBatchEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in InvBuildUnbuildAssemblyBatchEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(InvBuildUnbuildAssemblyBatchEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
							
			if (actionForm.getBuildUnbuildAssemblyBatchCodeCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				
			}									
						
		} else {
						
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
				    
	}
	
}