package com.struts.inv.buildunbuildassemblyentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApRINoPurchaseOrderLinesFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvCSTRemainingQuantityIsLessThanZeroException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvItemNotAssemblyException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvBuildUnbuildAssemblyEntryController;
import com.ejb.txn.InvBuildUnbuildAssemblyEntryControllerHome;
import com.struts.ap.receivingitementry.ApReceivingItemEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ArModCustomerDetails;
import com.util.InvBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyLineDetails;
import com.util.InvModItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvBuildUnbuildAssemblyEntryAction extends Action{
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		InvBuildUnbuildAssemblyEntryForm actionForm = (InvBuildUnbuildAssemblyEntryForm)form;
		
		try{
			
/*******************************************************
Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvBuildUnbuildAssemblyEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			}else{
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			
			actionForm.setReport(null);
			String frParam = Common.getUserPermission(user, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
						
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
Initialize InvBuildUnbuildAssemblyEntryController EJB
*******************************************************/
			
			InvBuildUnbuildAssemblyEntryControllerHome homeBUA = null;
			InvBuildUnbuildAssemblyEntryController ejbBUA = null;
			
			try {
				
				homeBUA = (InvBuildUnbuildAssemblyEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvBuildUnbuildAssemblyEntryControllerEJB", InvBuildUnbuildAssemblyEntryControllerHome.class);
				
			} catch(NamingException e) {
				if(log.isInfoEnabled()){
					log.info("NamingException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				return(mapping.findForward("cmnErrorPage"));
			}	
			
			try {
				
				ejbBUA = homeBUA.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			ActionMessages messages = new ActionMessages();
			
/*******************************************************
Call InvBuildUnbuildAssemblyEntryController EJB
getGlFcPrecisionUnit
getInvGpQuantityPrecisionUnit
getInvGpInventoryLineNumber
*******************************************************/
			
			short precisionUnit = 0;
			short quantityPrecisionUnit = 0;
			short buildUnbuildAssemblyLineNumber = 0;
			boolean isInitialPrinting = false;
			boolean enableInvoiceBatch = false;
			short journalLineNumber = 0;
			
			try {
				
				enableInvoiceBatch = Common.convertByteToBoolean(ejbBUA.getAdPrfEnableInvBuildUnbuildAssemblyBatch(user.getCmpCode()));
				
				precisionUnit = ejbBUA.getGlFcPrecisionUnit(user.getCmpCode());
				quantityPrecisionUnit = ejbBUA.getInvGpQuantityPrecisionUnit(user.getCmpCode());
				buildUnbuildAssemblyLineNumber = ejbBUA.getInvGpInventoryLineNumber(user.getCmpCode()); 
				journalLineNumber = ejbBUA.getInvGpInventoryLineNumber(user.getCmpCode());
				
				actionForm.setShowBatchName(enableInvoiceBatch);
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
/*******************************************************
-- Inv BUA Save As Draft Action --
*******************************************************/
			
			if (request.getParameter("saveAsDraftButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvBuildUnbuildAssemblyDetails details = new InvBuildUnbuildAssemblyDetails();
				
				details.setBuaType(actionForm.getType());
				details.setBuaCode(actionForm.getBuildUnbuildAssemblyCode());
				details.setBuaDocumentNumber(actionForm.getDocumentNumber());
				details.setBuaReceiveBoNumber(actionForm.getBoNumber());
				details.setBuaReferenceNumber(actionForm.getReferenceNumber());
				details.setBuaDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setBuaDueDate(Common.convertStringToSQLDate(actionForm.getDueDate()));
				details.setBuaDescription(actionForm.getDescription());
				details.setBuaVoid(Common.convertBooleanToByte(actionForm.getVoid()));
				details.setBuaReceiveBoNumber(actionForm.getBoNumber());

				if (actionForm.getBuildUnbuildAssemblyCode() == null) {
					
					details.setBuaCreatedBy(user.getUserName());
					details.setBuaDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					
				}
				
				details.setBuaLastModifiedBy(user.getUserName());
				details.setBuaDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				
				ArrayList blList = new ArrayList(); 
				
				for (int i = 0; i<actionForm.getInvBUAListSize(); i++) {
					
					InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);           	   
					
					if (Common.validateRequired(invBUAList.getLocation()) &&
							Common.validateRequired(invBUAList.getItemName()) &&
							Common.validateRequired(invBUAList.getBuildQuantityBy()) &&
							Common.validateRequired(invBUAList.getSpecificGravity()) &&
							Common.validateRequired(invBUAList.getStandardFillSize()) &&
							Common.validateRequired(invBUAList.getUnit())) continue;
					
					InvModBuildUnbuildAssemblyLineDetails mdetails = new InvModBuildUnbuildAssemblyLineDetails();
					
					mdetails.setBlBuildQuantity(Common.convertStringMoneyToDouble(invBUAList.getBuildQuantityBy(), quantityPrecisionUnit));
					mdetails.setBlBuildSpecificGravity(Common.convertStringMoneyToDouble(invBUAList.getSpecificGravity(), quantityPrecisionUnit));
					mdetails.setBlBuildStandardFillSize(Common.convertStringMoneyToDouble(invBUAList.getStandardFillSize(), quantityPrecisionUnit));
					
					System.out.println("invBUAList.getBlBlCode()=========="+invBUAList.getBlBlCode());
					
					mdetails.setBlBlCode(invBUAList.getBlBlCode());
					mdetails.setBlUomName(invBUAList.getUnit());
					mdetails.setBlLocName(invBUAList.getLocation());
					mdetails.setBlIiName(invBUAList.getItemName());
					mdetails.setBlLineNumber(Common.convertStringToShort(invBUAList.getLineNumber()));
					mdetails.setBlMisc(invBUAList.getMisc());
					blList.add(mdetails);
					
				}                     	
				
				try {
            	    
					Integer buildUnbuildAssemblyCode = ejbBUA.saveInvBuaEntry(details, blList, true, actionForm.getCustomer(), actionForm.getBatchName(),
							 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
				} catch (GlobalDocumentNumberNotUniqueException ex) {
	               	
	               	errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("buildUnbuildAssemblyEntry.error.documentNumberNotUnique"));    
	               	
	            } catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
					
				} catch (GlobalTransactionAlreadyApprovedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyApproved"));
					
				} catch (GlobalTransactionAlreadyPendingException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPending"));
					
				} catch (GlobalTransactionAlreadyPostedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPosted"));
					
				} catch (GlobalNoApprovalRequesterFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalRequesterFound"));
					
				} catch (GlobalNoApprovalApproverFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalApproverFound"));
					
				} catch (GlobalInvItemLocationNotFoundException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.noItemLocationFound", ex.getMessage()));
	           	   
				} catch (GlobalInvItemNotAssemblyException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.itemNotAssembly", ex.getMessage()));
	           	   
				} catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.mustNotBeZero", ex.getMessage()));
	           	
	           	} catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
		           
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
	           
	           } catch(GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.branchAccountNumberInvalid", ex.getMessage()));
	           	
	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	           	
	           	errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.noNegativeInventoryCostingCOA"));
	           	
	           } catch (GlobalRecordInvalidException ex) {
	        	   
	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			new ActionMessage("buildUnbuildAssemblyEntry.error.insufficientStocks", ex.getMessage()));
	        	
	           }catch (GlobalMiscInfoIsRequiredException ex) {
	        	   
	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	        	   
	           } catch (EJBException ex) {
	           	if (log.isInfoEnabled()) {
	           		
	           		log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
	           				" session: " + session.getId());
	           	}
	           	
	           	return(mapping.findForward("cmnErrorPage"));
	           }
				
				
/*******************************************************
   -- Inv BUA Customer Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerEntered")) && actionForm.getType().equals("ITEMS")) {
        	 
             try {
                 
                 ArModCustomerDetails mdetails = ejbBUA.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());
              

                 actionForm.setCustomerName(mdetails.getCstName());
                 actionForm.clearInvBUAList();

                 for (int x = 1; x <= journalLineNumber; x++) {
               		
               		InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
             				null, new Integer(x).toString(), null, null, null, null, null, null, null, null, null,null,null);	
               		
               		invBUAList.setLocationList(actionForm.getLocationList());
               		invBUAList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
               		invBUAList.setUnitList(Constants.GLOBAL_BLANK);
               		invBUAList.setUnitList("Select Item First");
                   		
                   		
                   		
                   		actionForm.saveInvBUAList(invBUAList);
                   		
                   	}
                 
             } catch (GlobalNoRecordFoundException ex) {
                 
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.customerNoRecordFound"));
                 saveErrors(request, new ActionMessages(errors));                           
                 
             } catch (EJBException ex) {
                 
                 if (log.isInfoEnabled()) {
                     
                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage"); 
                     
                 }
                 
             }  	
           
             
             return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
             
             
             
 
				
/*******************************************************
-- Inv BUA Save & Submit Action --
*******************************************************/
				
			} else if (request.getParameter("saveSubmitButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvBuildUnbuildAssemblyDetails details = new InvBuildUnbuildAssemblyDetails();
				
				details.setBuaType(actionForm.getType());
				details.setBuaCode(actionForm.getBuildUnbuildAssemblyCode());
				details.setBuaDocumentNumber(actionForm.getDocumentNumber());
				details.setBuaReceiveBoNumber(actionForm.getBoNumber());
				details.setBuaReferenceNumber(actionForm.getReferenceNumber());
				details.setBuaDescription(actionForm.getDescription());
				details.setBuaVoid(Common.convertBooleanToByte(actionForm.getVoid()));
				details.setBuaDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setBuaDueDate(Common.convertStringToSQLDate(actionForm.getDueDate()));
				
				if (actionForm.getBuildUnbuildAssemblyCode() == null) {
					
					details.setBuaCreatedBy(user.getUserName());
					details.setBuaDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					
				}
				
				details.setBuaLastModifiedBy(user.getUserName());
				details.setBuaDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				
				ArrayList blList = new ArrayList(); 
				
				for (int i = 0; i<actionForm.getInvBUAListSize(); i++) {
					
					InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);
					
					if (Common.validateRequired(invBUAList.getLocation()) &&
							Common.validateRequired(invBUAList.getItemName()) &&
							Common.validateRequired(invBUAList.getBuildQuantityBy()) &&
							Common.validateRequired(invBUAList.getSpecificGravity()) &&
							Common.validateRequired(invBUAList.getStandardFillSize()) &&
							Common.validateRequired(invBUAList.getUnit())) continue;
					
					InvModBuildUnbuildAssemblyLineDetails mdetails = new InvModBuildUnbuildAssemblyLineDetails();
					
					mdetails.setBlBuildQuantity(Common.convertStringMoneyToDouble(invBUAList.getBuildQuantityBy(), quantityPrecisionUnit));
					mdetails.setBlBuildSpecificGravity(Common.convertStringMoneyToDouble(invBUAList.getSpecificGravity(), quantityPrecisionUnit));
					mdetails.setBlBuildStandardFillSize(Common.convertStringMoneyToDouble(invBUAList.getStandardFillSize(), quantityPrecisionUnit));
					
					mdetails.setBlBlCode(invBUAList.getBlBlCode());
					mdetails.setBlUomName(invBUAList.getUnit());
					mdetails.setBlLocName(invBUAList.getLocation());
					mdetails.setBlIiName(invBUAList.getItemName());
					mdetails.setBlLineNumber(Common.convertStringToShort(invBUAList.getLineNumber()));
					mdetails.setBlMisc(invBUAList.getMisc());
					blList.add(mdetails);
					
				}                     	
				
				try {
					
					Integer buildUnbuildAssemblyCode = ejbBUA.saveInvBuaEntry(details, blList, false, actionForm.getCustomer(),actionForm.getBatchName(),
							 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					actionForm.setBuildUnbuildAssemblyCode(buildUnbuildAssemblyCode);
					
				} catch (GlobalDocumentNumberNotUniqueException ex) {
	               	
	               	errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("buildUnbuildAssemblyEntry.error.documentNumberNotUnique"));    
	               	
	            } catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
					
				} catch (GlobalAccountNumberInvalidException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.accountNumberInvalid"));                               	                 	                   
					
				} catch (GlobalTransactionAlreadyApprovedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyApproved"));
					
				} catch (GlobalTransactionAlreadyPendingException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPending"));
					
				} catch (GlobalTransactionAlreadyPostedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPosted"));
					
				} catch (GlobalNoApprovalRequesterFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalRequesterFound"));
					
				} catch (GlobalNoApprovalApproverFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalApproverFound"));
					
				} catch (GlobalInvItemLocationNotFoundException ex) {
		           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	           	    		new ActionMessage("buildUnbuildAssemblyEntry.error.noItemLocationFound", ex.getMessage()));
	           	    
				} catch (GlobalInvItemNotAssemblyException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.itemNotAssembly", ex.getMessage()));
	           	   
				} catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.mustNotBeZero", ex.getMessage()));
	                    
	            } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
		           
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		           
	           } catch(GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.branchAccountNumberInvalid", ex.getMessage()));
	           	
	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	           	
	           	errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.noNegativeInventoryCostingCOA"));
	           	
	           } catch (GlobalRecordInvalidException ex) {
	        	   
         	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		        		new ActionMessage("buildUnbuildAssemblyEntry.error.insufficientStocks", ex.getMessage()));
			    			
	           }catch (GlobalMiscInfoIsRequiredException ex) {
	        	   
	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	        	   
	           } catch (EJBException ex) {
	           	if (log.isInfoEnabled()) {
	           		
	           		log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
	           				" session: " + session.getId());
	           	}
	           	
	           	return(mapping.findForward("cmnErrorPage"));
	           }
	           
/*******************************************************
-- Inv BUA Journal Action --
*******************************************************/
			
			} else if (request.getParameter("journalButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				if(Common.validateRequired(actionForm.getApprovalStatus())) {
					
				
					InvBuildUnbuildAssemblyDetails details = new InvBuildUnbuildAssemblyDetails();
					
					details.setBuaType(actionForm.getType());
					details.setBuaCode(actionForm.getBuildUnbuildAssemblyCode());
					details.setBuaDocumentNumber(actionForm.getDocumentNumber());
					details.setBuaReceiveBoNumber(actionForm.getBoNumber());
					details.setBuaReferenceNumber(actionForm.getReferenceNumber());
					details.setBuaDate(Common.convertStringToSQLDate(actionForm.getDate()));
					details.setBuaDueDate(Common.convertStringToSQLDate(actionForm.getDueDate()));
					
					details.setBuaDescription(actionForm.getDescription());
					details.setBuaVoid(Common.convertBooleanToByte(actionForm.getVoid()));
		
					if (actionForm.getBuildUnbuildAssemblyCode() == null) {
						
						details.setBuaCreatedBy(user.getUserName());
						details.setBuaDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
						
					}
					
					details.setBuaLastModifiedBy(user.getUserName());
					details.setBuaDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					
					ArrayList blList = new ArrayList(); 
					
					for (int i = 0; i<actionForm.getInvBUAListSize(); i++) {
						
						InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);           	   
						
						if (Common.validateRequired(invBUAList.getLocation()) &&
								Common.validateRequired(invBUAList.getItemName()) &&
								Common.validateRequired(invBUAList.getBuildQuantityBy()) &&
								Common.validateRequired(invBUAList.getSpecificGravity()) &&
								Common.validateRequired(invBUAList.getStandardFillSize()) &&
								Common.validateRequired(invBUAList.getUnit())) continue;
						
						InvModBuildUnbuildAssemblyLineDetails mdetails = new InvModBuildUnbuildAssemblyLineDetails();
						
						mdetails.setBlBuildQuantity(Common.convertStringMoneyToDouble(invBUAList.getBuildQuantityBy(), quantityPrecisionUnit));
						mdetails.setBlBuildSpecificGravity(Common.convertStringMoneyToDouble(invBUAList.getSpecificGravity(), quantityPrecisionUnit));
						mdetails.setBlBuildStandardFillSize(Common.convertStringMoneyToDouble(invBUAList.getStandardFillSize(), quantityPrecisionUnit));
						
						mdetails.setBlBlCode(invBUAList.getBlBlCode());
						mdetails.setBlUomName(invBUAList.getUnit());
						mdetails.setBlLocName(invBUAList.getLocation());
						mdetails.setBlIiName(invBUAList.getItemName());
						mdetails.setBlLineNumber(Common.convertStringToShort(invBUAList.getLineNumber()));
						mdetails.setBlMisc(invBUAList.getMisc());
						blList.add(mdetails);
						
					}                     	
					
					try {
						
						Integer buildUnbuildAssemblyCode = ejbBUA.saveInvBuaEntry(details, blList, true, actionForm.getCustomer(),actionForm.getBatchName(),
								 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						actionForm.setBuildUnbuildAssemblyCode(buildUnbuildAssemblyCode);
		
					} catch (GlobalDocumentNumberNotUniqueException ex) {
		               	
		               	errors.add(ActionMessages.GLOBAL_MESSAGE,
		               			new ActionMessage("buildUnbuildAssemblyEntry.error.documentNumberNotUnique"));    
		               	
		            } catch (GlobalRecordAlreadyDeletedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
						
					} catch (GlobalTransactionAlreadyApprovedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyApproved"));
						
					} catch (GlobalTransactionAlreadyPendingException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPending"));
						
					} catch (GlobalTransactionAlreadyPostedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPosted"));
						
					} catch (GlobalNoApprovalRequesterFoundException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalRequesterFound"));
						
					} catch (GlobalNoApprovalApproverFoundException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalApproverFound"));
						
					} catch (GlobalInvItemLocationNotFoundException ex) {
			           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.noItemLocationFound", ex.getMessage()));
		           	   
					} catch (GlobalInvItemNotAssemblyException ex) {
			           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.itemNotAssembly", ex.getMessage()));
		           	   
					} catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
			           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.mustNotBeZero", ex.getMessage()));
		           	
		           	} catch (GlJREffectiveDateNoPeriodExistException ex) {
		       	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDateNoPeriodExist"));
		                    
		           } catch (GlJREffectiveDatePeriodClosedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDatePeriodClosed"));
		                    
		           } catch (GlobalJournalNotBalanceException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("buildUnbuildAssemblyEntry.error.journalNotBalance"));
		           	   
		           } catch (GlobalInventoryDateException ex) {
			           
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			           		new ActionMessage("buildUnbuildAssemblyEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
			           
		           } catch(GlobalBranchAccountNumberInvalidException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("buildUnbuildAssemblyEntry.error.branchAccountNumberInvalid", ex.getMessage()));
			           	
		           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		           	
		           	errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("buildUnbuildAssemblyEntry.error.noNegativeInventoryCostingCOA"));
		           	
		           }catch (GlobalMiscInfoIsRequiredException ex) {
		        	   
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
		        	   
		           } catch (EJBException ex) {
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
						}
						
						return(mapping.findForward("cmnErrorPage"));
					}
					
					if (!errors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(errors));
						return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
						
					} 
					
				}
				
				String path = "/invJournal.do?forward=1" + 
		     	"&transactionCode=" + actionForm.getBuildUnbuildAssemblyCode() +
		     	"&transaction=BUILD/UNBUILD ASSEMBLY" + 
		     	"&transactionNumber=" + actionForm.getReferenceNumber() + 
		     	"&transactionDate=" + actionForm.getDate() +
		     	"&transactionEnableFields=" + actionForm.getEnableFields();
				
            	return(new ActionForward(path));

/*******************************************************
   -- Inv BUA Print Action --
*******************************************************/             	
            
         } else if (request.getParameter("printButton") != null)  {
        	 
        	 if(Common.validateRequired(actionForm.getApprovalStatus())) {
        		 
        		 InvBuildUnbuildAssemblyDetails details = new InvBuildUnbuildAssemblyDetails();
				
        		 
        		 details.setBuaType(actionForm.getType());
				details.setBuaCode(actionForm.getBuildUnbuildAssemblyCode());
				details.setBuaDocumentNumber(actionForm.getDocumentNumber());
				details.setBuaReceiveBoNumber(actionForm.getBoNumber());
				details.setBuaReferenceNumber(actionForm.getReferenceNumber());
				details.setBuaDate(Common.convertStringToSQLDate(actionForm.getDueDate()));
				details.setBuaDueDate(Common.convertStringToSQLDate(actionForm.getDueDate()));
				details.setBuaDescription(actionForm.getDescription());
				details.setBuaVoid(Common.convertBooleanToByte(actionForm.getVoid()));
	
				if (actionForm.getBuildUnbuildAssemblyCode() == null) {
					
					details.setBuaCreatedBy(user.getUserName());
					details.setBuaDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
					
				}
				
				details.setBuaLastModifiedBy(user.getUserName());
				details.setBuaDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				
				ArrayList blList = new ArrayList(); 
				
				for (int i = 0; i<actionForm.getInvBUAListSize(); i++) {
					
					InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);           	   
					
					if (Common.validateRequired(invBUAList.getLocation()) &&
							Common.validateRequired(invBUAList.getItemName()) &&
							Common.validateRequired(invBUAList.getBuildQuantityBy()) &&
							Common.validateRequired(invBUAList.getSpecificGravity()) &&
							Common.validateRequired(invBUAList.getStandardFillSize()) &&
							Common.validateRequired(invBUAList.getUnit())) continue;
					
					InvModBuildUnbuildAssemblyLineDetails mdetails = new InvModBuildUnbuildAssemblyLineDetails();
					
					mdetails.setBlBuildQuantity(Common.convertStringMoneyToDouble(invBUAList.getBuildQuantityBy(), quantityPrecisionUnit));
					mdetails.setBlBuildSpecificGravity(Common.convertStringMoneyToDouble(invBUAList.getSpecificGravity(), quantityPrecisionUnit));
					mdetails.setBlBuildStandardFillSize(Common.convertStringMoneyToDouble(invBUAList.getStandardFillSize(), quantityPrecisionUnit));
					
					mdetails.setBlBlCode(invBUAList.getBlBlCode());
					mdetails.setBlUomName(invBUAList.getUnit());
					mdetails.setBlLocName(invBUAList.getLocation());
					mdetails.setBlIiName(invBUAList.getItemName());
					mdetails.setBlLineNumber(Common.convertStringToShort(invBUAList.getLineNumber()));
					mdetails.setBlMisc(invBUAList.getMisc());
					blList.add(mdetails);
					
				}                     	
				
				try {
					
					Integer buildUnbuildAssemblyCode = ejbBUA.saveInvBuaEntry(details, blList, true, actionForm.getCustomer(),actionForm.getBatchName(),
							 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					actionForm.setBuildUnbuildAssemblyCode(buildUnbuildAssemblyCode);
					actionForm.setReportType(actionForm.getReportType());
					
	
				} catch (GlobalDocumentNumberNotUniqueException ex) {
	               	
	               	errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("buildUnbuildAssemblyEntry.error.documentNumberNotUnique"));    
	               	
	            } catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
					
				} catch (GlobalTransactionAlreadyApprovedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyApproved"));
					
				} catch (GlobalTransactionAlreadyPendingException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPending"));
					
				} catch (GlobalTransactionAlreadyPostedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.transactionAlreadyPosted"));
					
				} catch (GlobalNoApprovalRequesterFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalRequesterFound"));
					
				} catch (GlobalNoApprovalApproverFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.noApprovalApproverFound"));
					
				} catch (GlobalInvItemLocationNotFoundException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.noItemLocationFound", ex.getMessage()));
	           	   
				} catch (GlobalInvItemNotAssemblyException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.itemNotAssembly", ex.getMessage()));
	           	   
				} catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
		           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.mustNotBeZero", ex.getMessage()));
	           	
	           	} catch (GlJREffectiveDateNoPeriodExistException ex) {
	       	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildAssemblyEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
		           
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		           		new ActionMessage("buildUnbuildAssemblyEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		           
	           } catch(GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.branchAccountNumberInvalid", ex.getMessage()));
		           	
	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	           	
	           	errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("buildUnbuildAssemblyEntry.error.noNegativeInventoryCostingCOA"));
	           	
	           }catch (GlobalMiscInfoIsRequiredException ex) {
	        	   
	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	        	   
	           } catch (EJBException ex) {
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
				}
					
				
	            
	            if (!errors.isEmpty()) {
		        	
		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
		               
		        }
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 isInitialPrinting = true;
                 
             }  else {
                 
                 actionForm.setReport(Constants.STATUS_SUCCESS);
                 
                 return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
                 
             }

        	 
 /*******************************************************
   -- Ap RI Purchase Order Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isPurchaseOrderEntered"))) {
         
            if (actionForm.getType().equals("BO MATCHED")) {

	         	try {
	             	 System.out.println("isPurchaseOrderEntered");
	             	 
	             	InvModBuildUnbuildAssemblyDetails mdetails = ejbBUA.getInvBoByBuaRcvBuaNumberAndCstcsutomerCodeAndAdBranch(
	                     actionForm.getBoNumber(), actionForm.getCustomer(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                 
	               
	                 actionForm.setCustomerName(mdetails.getBuaCstName());
	                 actionForm.setDescription(mdetails.getBuaDescription());
	               
	                 
	                 actionForm.clearInvBUAList();    
	                 
	                 int lineNumber = 0;         		            		            		
	 	             Iterator i = mdetails.getBuaBlList().iterator();
			          
			         while (i.hasNext()) {
			            	
			        	 
			        	 InvModBuildUnbuildAssemblyLineDetails mBlDetails = (InvModBuildUnbuildAssemblyLineDetails)i.next();
				          
			        	 System.out.println("SG="+Common.convertDoubleToStringMoney(mBlDetails.getBlBuildSpecificGravity(), precisionUnit));
			        	 InvBuildUnbuildAssemblyEntryList buaBlList = new InvBuildUnbuildAssemblyEntryList(actionForm,
									mBlDetails.getBlCode(), new Integer(++lineNumber).toString(), 
									mBlDetails.getBlLocName(), mBlDetails.getBlIiName(), mBlDetails.getBlIiDescription(),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildSpecificGravity(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildStandardFillSize(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlCstQuantity(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlRemaining(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildQuantity(), quantityPrecisionUnit),
									
									mBlDetails.getBlUomName(), mBlDetails.getBlCode(), mBlDetails.getBlMisc());      
							
							

					      
					        
							buaBlList.clearTagList();
							
					        String misc = mBlDetails.getBlBuildQuantity() + "_";
				      		String propertyCode = "";
				   		 	String specs = "";
				   		 	String serialNumber = "";
				   		 	String custodian = "";
				   		 	String expiryDate = "";
				   		 	String tgDocumentNumber = "";
				   		 	
					        if (mBlDetails.getTraceMisc()==1){
					        	ArrayList tagList = mBlDetails.getBlTagList();
	            				Iterator y = tagList.iterator();
	            				//TODO:save tagList
	            				
	            				//if (y!=null){
	        					
	        					if (tagList.size() == 0) {
	            					
	            					for (int x=1; x<=mBlDetails.getBlBuildQuantity(); x++) {
	        	            			
	            						 
	            						
	            						/*ApReceivingItemEntryTagList invTagList = new ApReceivingItemEntryTagList(buaBlList, "", "", "", "", "", "");
	        	            			
	            					
	        	            			
	        	            			buaBlList.saveTagList(invTagList);
	        	            			
	    	                   		   	if (expiryDate == null){
	    	                   		   		expiryDate = "";
	    	                   		   	}
	    	                   		   	propertyCode += " ,";
	    	                   		    specs += " ,";
	    	                   		    serialNumber += " ,";
	    	                   		    custodian += " ,";
	    	                   		    expiryDate += " ,";
	    	                   		    tgDocumentNumber += " ,";*/
	            					}
	            					misc = 	mBlDetails.getBlBuildQuantity() +"_"+
	            							propertyCode.substring(0,propertyCode.length()-1)+"@"+
	            					   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
	            					   		specs.substring(0,specs.length()-1)+">"+
	            					   		custodian.substring(0,custodian.length()-1)+";"+
	            					   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
	            					   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1); 
	            				}else{
	            					System.out.println("existing taglist");
		        					while (y.hasNext()){
		            					InvModTagListDetails tgDetails = (InvModTagListDetails)y.next();
		            				/*	
		            					ApReceivingItemEntryTagList apRiLTgList = new ApReceivingItemEntryTagList(
		            							apRilList, tgDetails.getTgPropertyCode(), tgDetails.getTgSpecs(),  tgDetails.getTgDocumentNumber(),
		            							tgDetails.getTgCustodian(), tgDetails.getTgSerialNumber(), Common.convertSQLDateToString(tgDetails.getTgExpiryDate()));
		            					System.out.println(tgDetails.getTgSpecs() + "<== specs from InvModTagListDetails");
		            					System.out.println(tgDetails.getTgSerialNumber() + "<== serial number from InvModTagListDetails");
		            					apRilList.saveTagList(apRiLTgList);*/
		            					if (tgDetails.getTgPropertyCode().equals("") || tgDetails.getTgPropertyCode()==null){
		            						propertyCode += " ,";
		            					}else{
		            						propertyCode += tgDetails.getTgPropertyCode() + ",";
		            					}
		            					if (tgDetails.getTgSerialNumber().equals("") || tgDetails.getTgPropertyCode()==null){
		            						serialNumber += " ,";
		            					}else{
		            						serialNumber += tgDetails.getTgSerialNumber() + ",";
		            					}
		            					if (tgDetails.getTgSpecs().equals("") || tgDetails.getTgPropertyCode()==null){
		            						specs += " ,";
		            					}else{
		            						specs += tgDetails.getTgSpecs() + ",";
		            					}
		            					
	    	                   		   	custodian += (tgDetails.getTgCustodian() == "") ? " ," : tgDetails.getTgCustodian() + ",";
	    	                   		 	expiryDate += (tgDetails.getTgExpiryDate() == null) ? " ," : Common.convertSQLDateToString(tgDetails.getTgExpiryDate()) + ",";
	    	                   		 	if (tgDetails.getTgDocumentNumber()==null){
		            						tgDocumentNumber += " ,";
		            					}else{
		            						tgDocumentNumber += tgDetails.getTgDocumentNumber() + ",";
		            					}
		            					
		            				}
		        					misc = 	mBlDetails.getBlBuildQuantity()+"_"+
		        							propertyCode.substring(0,propertyCode.length()-1)+"@"+
		        					   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
		        					   		specs.substring(0,specs.length()-1)+">"+
		        					   		custodian.substring(0,custodian.length()-1)+";"+
		        					   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
		        					   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1); 
	            				}
					        }
					        buaBlList.setMisc(misc);
            				

	        				
							buaBlList.clearUnitList();
							
							ArrayList unitList = ejbBUA.getInvUomByIiName(mBlDetails.getBlIiName(), user.getCmpCode());
							Iterator unitListIter = unitList.iterator();
							while (unitListIter.hasNext()) {
								
								InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
								buaBlList.setUnitList(mUomDetails.getUomName()); 
								
							}
							
							buaBlList.setLocationList(actionForm.getLocationList());	
					          	
							
							
							
					        actionForm.saveInvBUAList(buaBlList);
					      
				     }	
			           			        			        				         			                     
	             } catch (GlobalNoRecordFoundException ex) {
	
	             	actionForm.clearInvBUAList();
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.noPurchaseOrderFound"));
	                saveErrors(request, new ActionMessages(errors));
	                                				
			     } catch (ApRINoPurchaseOrderLinesFoundException ex) {
	
	             	actionForm.clearInvBUAList();
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.noPurchaseOrderLinesFound"));
	                saveErrors(request, new ActionMessages(errors));
			     
	                             	
	             } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
            
            } else {
            
	            try {
	             	
	            	ArModCustomerDetails mdetails = ejbBUA.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

	                 
/*	                 actionForm.setPaymentTerm(mdetails.getSplPytName());
	                 actionForm.setTaxCode(mdetails.getSplScTcName());
	                 actionForm.setTaxType(mdetails.getSplScTcType());
	                 actionForm.setTaxRate(mdetails.getSplScTcRate());*/
	                 actionForm.setCustomerName(mdetails.getCstName());
	                 actionForm.clearInvBUAList();


	                 int lineNumber = 0;
	                 
	                 for (int x = 1; x <= journalLineNumber; x++) {
	                	 
	                	 InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
	 							null, new Integer(++lineNumber).toString(), null, null, null, null, null, null, null, null, null, null, null);	
	 					
	 					invBUAList.setLocationList(actionForm.getLocationList());
	 					
	 					invBUAList.setUnitList(Constants.GLOBAL_BLANK);
	 					invBUAList.setUnitList("Select Item First");
	 					
	 					
	 					invBUAList.clearTagList();
	                 	
	             
	                 	/*
	            		for (int y=1; y<=10; y++) {
	            			
	            			ApReceivingItemEntryTagList apTagList = new ApReceivingItemEntryTagList(apRILList, "", "", "", "", "","");
	            			
	            			apRILList.saveTagList(apTagList);
	            		}
	                 	*/
	                 	actionForm.saveInvBUAList(invBUAList);
	                 	
	                 }
	                                  
	             } catch (GlobalNoRecordFoundException ex) {
	             	
	             	actionForm.clearInvBUAList();
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.supplierNoRecordFound"));
	                saveErrors(request, new ActionMessages(errors));
	             	
	             } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
            
            } 	
                        
            return(mapping.findForward("invBuildUnbuildAssemblyEntry"));            

     

/*******************************************************
   -- Inv BUA Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbBUA.deleteInvBuaEntry(actionForm.getBuildUnbuildAssemblyCode(), user.getUserName(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            	
				
/*******************************************************
-- Inv BUA Close Action --
*******************************************************/
				
			} else if(request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
-- Inv BUA Add Lines Action --
*******************************************************/
				
			} else if(request.getParameter("addLinesButton") != null) {
				
				int listSize = actionForm.getInvBUAListSize();
				int lineNumber = 0;
				
				for (int x = listSize + 1; x <= listSize + buildUnbuildAssemblyLineNumber; x++) {
					
					ArrayList comboItem = new ArrayList();
					
					InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
							null, new Integer(++lineNumber).toString(), null, null, null, null, null, null, null, null, null, null, null);	
					
					invBUAList.setLocationList(actionForm.getLocationList());
					
					invBUAList.setUnitList(Constants.GLOBAL_BLANK);
					invBUAList.setUnitList("Select Item First");
					
					actionForm.saveInvBUAList(invBUAList);
					
				}		                    
				
				for (int i = 0; i<actionForm.getInvBUAListSize(); i++ ) {
					
					InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);
					
					invBUAList.setLineNumber(new Integer(i+1).toString());
					
				}        
				
				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
/*******************************************************
-- Inv BUA Delete Lines Action --
*******************************************************/
				
			} else if(request.getParameter("deleteLinesButton") != null) {
				
				for (int i = 0; i<actionForm.getInvBUAListSize(); i++) {
					
					InvBuildUnbuildAssemblyEntryList invBUAList = actionForm.getInvBUAByIndex(i);
					
					if (invBUAList.isDeleteCheckbox()) {
						
						actionForm.deleteInvBUAList(i);
						i--;
					}
					
				}
				
				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));        
				
/*******************************************************
-- Inv BUA Location Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("invBUAList[" +
					actionForm.getRowSelected() + "].isLocationEntered"))) {
				
				InvBuildUnbuildAssemblyEntryList invBUAList = 
					actionForm.getInvBUAByIndex(actionForm.getRowSelected());
				
		      	try {
		          	      		
		      		// populate quantity and default build quantity by and new quantity if necessary
		      		
		      		if (!Common.validateRequired(invBUAList.getLocation()) && !Common.validateRequired(invBUAList.getItemName()) &&
		      		    !Common.validateRequired(invBUAList.getUnit())) {
		      		
				  		double itemRemainingQuantity = ejbBUA.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(invBUAList.getItemName(), invBUAList.getLocation(), invBUAList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());       		
				  		invBUAList.setQuantity(Common.convertDoubleToStringMoney(itemRemainingQuantity, quantityPrecisionUnit));
				  		invBUAList.setBuildQuantityBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
				  		
				  	}
		
		      		        	                 	
		          } catch (EJBException ex) {
		
		            if (log.isInfoEnabled()) {
		
		               log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
		               " session: " + session.getId());
		               return mapping.findForward("cmnErrorPage"); 
		               
		            }
		
		         }  				
				

				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
/*******************************************************
-- Inv BUA Item Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("invBUAList[" + 
					actionForm.getRowSelected() + "].isItemEntered"))) {
				
				InvBuildUnbuildAssemblyEntryList invBUAList = 
					actionForm.getInvBUAByIndex(actionForm.getRowSelected());			
					
				try {
					
					ArrayList uomList = new ArrayList();
					uomList = ejbBUA.getInvUomByIiName(invBUAList.getItemName(), user.getCmpCode());
					
					invBUAList.clearUnitList();
					invBUAList.setUnitList(Constants.GLOBAL_BLANK);
					
					Iterator i = uomList.iterator();
					while (i.hasNext()) {
						
						InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
						
						invBUAList.setUnitList(mUomDetails.getUomName());
						
						if (mUomDetails.isDefault()) {
							
							invBUAList.setUnit(mUomDetails.getUomName());      				
							
						}      			
						
					}
					
					InvModItemDetails mdetails = ejbBUA.getInvDetailsByIiName(invBUAList.getItemName(),  user.getCmpCode());

		      		if (!Common.validateRequired(invBUAList.getLocation()) && !Common.validateRequired(invBUAList.getItemName()) &&
		      		    !Common.validateRequired(invBUAList.getUnit())) {					
					
						double invItemRemainingQuantity = ejbBUA.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(invBUAList.getItemName(), invBUAList.getLocation(), invBUAList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());       		
						invBUAList.setQuantity(Common.convertDoubleToStringMoney(invItemRemainingQuantity, quantityPrecisionUnit));
						invBUAList.setBuildQuantityBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
						invBUAList.setSpecificGravity(Common.convertDoubleToStringMoney(mdetails.getIiSpecificGravity(), quantityPrecisionUnit));
						invBUAList.setStandardFillSize(Common.convertDoubleToStringMoney(mdetails.getIiStandardFillSize(), quantityPrecisionUnit));
						
					}
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  						
				
				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
/*******************************************************
-- Inv BUA Build Quantity By Action --
*******************************************************/ 
				
			} else if(!Common.validateRequired(request.getParameter("invBUAList[" + 
					actionForm.getRowSelected() + "].isBuildQuantityByEntered"))) {
				
				InvBuildUnbuildAssemblyEntryList invBUAList = 
					actionForm.getInvBUAByIndex(actionForm.getRowSelected());				
				
				try {
					
					// refresh quantity and calculate quantity
					
					double itemRemainingQuantity = ejbBUA.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(invBUAList.getItemName(), invBUAList.getLocation(), invBUAList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					double buildQuantityBy = Common.convertStringMoneyToDouble(invBUAList.getBuildQuantityBy(), quantityPrecisionUnit);
					
					invBUAList.setQuantity(Common.convertDoubleToStringMoney(itemRemainingQuantity, quantityPrecisionUnit));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
/*******************************************************
-- Inv BUA Unit Enter Action --
*******************************************************/

			} else if(!Common.validateRequired(request.getParameter("invBUAList[" + 
					actionForm.getRowSelected() + "].isUnitEntered"))) {

				InvBuildUnbuildAssemblyEntryList invBUAList = 
					actionForm.getInvBUAByIndex(actionForm.getRowSelected());				

				try {

					// populate qty field

					if (!Common.validateRequired(invBUAList.getLocation()) && !Common.validateRequired(invBUAList.getItemName()) &&
							!Common.validateRequired(invBUAList.getUnit())) {
						
						double invItemRemainingQuantity = ejbBUA.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(invBUAList.getItemName(), invBUAList.getLocation(), invBUAList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());       		
						invBUAList.setQuantity(new Double(invItemRemainingQuantity).toString());
						
					}

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 

					}

				}  	

				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
				
/*******************************************************
     -- Ap RI Type Enter Action --
  *******************************************************/

   } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {      		     	
   
    if (actionForm.getType().equals("ITEMS")) {

   	actionForm.clearInvBUAList();
   	
   	// Populate line when not forwarding
  	
  	for (int x = 1; x <= journalLineNumber; x++) {
  		
  		InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
				null, new Integer(x).toString(), null, null, null, null, null, null, null, null, null, null, null);	
  		
  		invBUAList.setLocationList(actionForm.getLocationList());
  		invBUAList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
  		invBUAList.setUnitList(Constants.GLOBAL_BLANK);
  		invBUAList.setUnitList("Select Item First");
      		
      		
      		
      		actionForm.saveInvBUAList(invBUAList);
      		
      	}
    	
    } else {
    
    	actionForm.clearInvBUAList();
    
    }
 
    return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
/*******************************************************
-- Inv BUA Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				// Errors on saving
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
					
				}
				
				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
					saveErrors(request, new ActionMessages(errors));
					
					return mapping.findForward("cmnMain");	
					
				}
				
				try {
					
					// location combo box
					
					ArrayList list = null;
					Iterator i = null;
					
					actionForm.clearLocationList();       	
					
					list = ejbBUA.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					actionForm.clearBatchNameList();           	
	                 
	                 list = ejbBUA.getInvOpenBbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                 
	                 if (list == null || list.size() == 0) {
	                     
	                     actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
	                     
	                 } else {
	                     
	                     i = list.iterator();
	                     
	                     while (i.hasNext()) {
	                         
	                         actionForm.setBatchNameList((String)i.next());
	                         
	                     }
	                     
	                 }
					
					if(actionForm.getUseCustomerPulldown()) {
	                     
	                     actionForm.clearCustomerList();    
	                     
	                     list = ejbBUA.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                     
	                     if (list == null || list.size() == 0) {
	                         
	                         actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);
	                         
	                     } else {
	                         
	                         i = list.iterator();
	                         
	                         while (i.hasNext()) {
	                             
	                             actionForm.setCustomerList((String)i.next());
	                             
	                         }
	                         
	                     } 
	                     
	                 }
					
					
					actionForm.clearInvBUAList();               	  	
					
					if (request.getParameter("forward") != null || isInitialPrinting) {
						
						if (request.getParameter("forward") != null) {
	                         
							actionForm.setBuildUnbuildAssemblyCode(new Integer(request.getParameter("buildUnbuildAssemblyCode")));
	                         
	                     }				
						
						
						InvModBuildUnbuildAssemblyDetails mdetails = ejbBUA.getInvBuaByBuaCode(actionForm.getBuildUnbuildAssemblyCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());            	
						actionForm.setType(mdetails.getBuaType());	
						System.out.println("mdetails.getBuaType()="+mdetails.getBuaType());
						actionForm.setDocumentNumber(mdetails.getBuaDocumentNumber());
						actionForm.setBoNumber(mdetails.getBuaReceiveBoNumber());
						actionForm.setReferenceNumber(mdetails.getBuaReferenceNumber());
						actionForm.setDate(Common.convertSQLDateToString(mdetails.getBuaDate()));
						actionForm.setDueDate(Common.convertSQLDateToString(mdetails.getBuaDueDate()));
						actionForm.setDescription(mdetails.getBuaDescription());
						actionForm.setVoid(Common.convertByteToBoolean(mdetails.getBuaVoid()));
						actionForm.setApprovalStatus(mdetails.getBuaApprovalStatus());
						actionForm.setPosted(mdetails.getBuaPosted() == 1 ? "YES" : "NO");
						actionForm.setCreatedBy(mdetails.getBuaCreatedBy());
						actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getBuaDateCreated()));
						actionForm.setLastModifiedBy(mdetails.getBuaLastModifiedBy());
						actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getBuaDateLastModified()));
						actionForm.setApprovedRejectedBy(mdetails.getBuaApprovedRejectedBy());
						actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getBuaDateApprovedRejected()));
						actionForm.setPostedBy(mdetails.getBuaPostedBy());
						actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getBuaDatePosted()));
						actionForm.setReasonForRejection(mdetails.getBuaReasonForRejection());
						
						
						if (!actionForm.getBatchNameList().contains(mdetails.getBuaBbName())) {
	                         
	                         actionForm.setBatchNameList(mdetails.getBuaBbName());
	                         
	                     }                 		
	                     actionForm.setBatchName(mdetails.getBuaBbName());
	                     
						if (!actionForm.getCustomerList().contains(mdetails.getBuaCstCustomerCode())) {
	                         
	                         if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
	                             
	                             actionForm.clearCustomerList();
	                             
	                         }
	                         actionForm.setCustomerList(mdetails.getBuaCstCustomerCode());
	                         
	                     }
	                     actionForm.setCustomer(mdetails.getBuaCstCustomerCode());
	                     actionForm.setCustomerName(mdetails.getBuaCstName());
	                     
						
						list = mdetails.getBuaBlList();           		
						
						i = list.iterator();
						
						int lineNumber = 0;
						
						while (i.hasNext()) {
							
							InvModBuildUnbuildAssemblyLineDetails mBlDetails = (InvModBuildUnbuildAssemblyLineDetails)i.next();
							
							InvBuildUnbuildAssemblyEntryList buaBlList = new InvBuildUnbuildAssemblyEntryList(actionForm,
									mBlDetails.getBlCode(), new Integer(++lineNumber).toString(), 
									mBlDetails.getBlLocName(), mBlDetails.getBlIiName(), mBlDetails.getBlIiDescription(),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildSpecificGravity(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildStandardFillSize(), precisionUnit),
									
									Common.convertDoubleToStringMoney(mBlDetails.getBlCstQuantity(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlRemaining(), precisionUnit),
									Common.convertDoubleToStringMoney(mBlDetails.getBlBuildQuantity(), quantityPrecisionUnit),
									
									mBlDetails.getBlUomName(), mBlDetails.getBlBlCode(), mBlDetails.getBlMisc());      
							
							buaBlList.clearUnitList();
							ArrayList unitList = ejbBUA.getInvUomByIiName(mBlDetails.getBlIiName(), user.getCmpCode());
							Iterator unitListIter = unitList.iterator();
							while (unitListIter.hasNext()) {
								
								InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
								buaBlList.setUnitList(mUomDetails.getUomName()); 
								
							}
							
							buaBlList.setLocationList(actionForm.getLocationList());		            	
							
							actionForm.saveInvBUAList(buaBlList);				         
							
						}	
						
						
						if (actionForm.getType().equals("ITEMS")) {
							
							
							int remainingList = buildUnbuildAssemblyLineNumber - (list.size() % buildUnbuildAssemblyLineNumber) + list.size();
							
							for (int x = list.size() + 1; x <= remainingList; x++) {
								
								InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
										null, new Integer(x).toString(), null, null, null, null, null, null, null, null, null, null, null);	
								
								invBUAList.setLocationList(actionForm.getLocationList());
								
								actionForm.saveInvBUAList(invBUAList);
								
							}
							
							
							
						}
						
							
						
						this.setFormProperties(actionForm);
						
						if (request.getParameter("child") == null) {
							
							return (mapping.findForward("invBuildUnbuildAssemblyEntry"));         
							
						} else {
							
							return (mapping.findForward("invBuildUnbuildAssemblyEntryChild"));         
							
						}
						
					}     
					
					// Populate line when not forwarding
					
					for (int x = 1; x <= buildUnbuildAssemblyLineNumber; x++) {
						
						ArrayList comboItem = new ArrayList();
						
						InvBuildUnbuildAssemblyEntryList invBUAList = new InvBuildUnbuildAssemblyEntryList(actionForm, 
								null, new Integer(x).toString(), null, null, null, null, null, null, null, null, null, null, null);	
						
						invBUAList.setLocationList(actionForm.getLocationList());
						
						invBUAList.setUnitList(Constants.GLOBAL_BLANK);
						invBUAList.setUnitList("Select Item First");
						
						actionForm.saveInvBUAList(invBUAList);
						
					}       	  	        
					
					
				} catch(GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("buildUnbuildAssemblyEntry.error.recordAlreadyDeleted"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				} 
				
				// Errors on loading
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveSubmitButton") != null && 
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
						
						try {
							
							ArrayList list = ejbBUA.getAdApprovalNotifiedUsersByBuaCode(actionForm.getBuildUnbuildAssemblyCode(), user.getCmpCode());                   	
							
							if (list.isEmpty()) {
								
								messages.add(ActionMessages.GLOBAL_MESSAGE,
									new ActionMessage("messages.documentSentForPosting"));
										
						    } else if (list.contains("DOCUMENT POSTED")) {
                   	   	
                   	   	  		messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       		new ActionMessage("messages.documentPosted"));     
								
							} else {
								
								Iterator i = list.iterator();
								
								String APPROVAL_USERS = "";
								
								while (i.hasNext()) {
									
									APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
									
									if (i.hasNext()) {
										
										APPROVAL_USERS = APPROVAL_USERS + ", ";
										
									}
									
									
								}
								
								messages.add(ActionMessages.GLOBAL_MESSAGE,
										new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
								
							}
							
							saveMessages(request, messages);
							actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
							
							
						} catch(EJBException ex) {
							
							if (log.isInfoEnabled()) {
								
								log.info("EJBException caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + ex.getMessage() +
										" session: " + session.getId());
							}
							
							return(mapping.findForward("cmnErrorPage"));
							
						} 
						
					} else if (request.getParameter("saveAsDraftButton") != null && 
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
					
				}
				
				actionForm.reset(mapping, request);              
				
				if (actionForm.getType() == null) {
		      		   
	            	actionForm.setType("BO MATCHED");
	                                 
	            }
				actionForm.setBuildUnbuildAssemblyCode(null);
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setDueDate(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setPosted("NO");
				actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setCreatedBy(user.getUserName());
				actionForm.setLastModifiedBy(user.getUserName());
				actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));                        
				
				this.setFormProperties(actionForm);
				return(mapping.findForward("invBuildUnbuildAssemblyEntry"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvBuildUnbuildAssemblyEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));
			
		}
	}
	
	
	private void setFormProperties(InvBuildUnbuildAssemblyEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getBuildUnbuildAssemblyCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
				    actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					
				} else if (actionForm.getBuildUnbuildAssemblyCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					
					
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					
				}
				
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				
			}
			
			
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			
		}
		
	}
	
}