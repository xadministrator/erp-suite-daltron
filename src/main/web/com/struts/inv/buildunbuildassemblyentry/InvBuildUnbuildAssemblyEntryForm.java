package com.struts.inv.buildunbuildassemblyentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class InvBuildUnbuildAssemblyEntryForm extends ActionForm implements Serializable {

	private Integer buildUnbuildAssemblyCode = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	
	private String reportType = null;
	ArrayList reportTypeList = new ArrayList();
	
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	
	private String customer = null;
	private ArrayList customerList = new ArrayList();
	private boolean useCustomerPulldown = true;
	private String isCustomerEntered  = null;
	private String customerName = null;
	private String date = null;
	private String dueDate = null;
	private String documentNumber = null;
	private String boNumber = null;
	private ArrayList boNumberList = new ArrayList();
	private String referenceNumber = null;
	private String description = null;
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String reasonForRejection = null;
    private boolean buaVoid = false;
    
    private String isPoNumberEntered = null;
   
	private ArrayList invBUAList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	private boolean showBatchName = false;
	
	
	private String report = null;
	
	public int getRowSelected(){
		return rowSelected;
	}

	public InvBuildUnbuildAssemblyEntryList getInvBUAByIndex(int index){
		return((InvBuildUnbuildAssemblyEntryList)invBUAList.get(index));
	}

	public Object[] getInvBUAList(){
		return(invBUAList.toArray());
	}

	public int getInvBUAListSize(){
		return(invBUAList.size());
	}

	public void saveInvBUAList(Object newInvBUAList){
		invBUAList.add(newInvBUAList);
	}

	public void clearInvBUAList(){
		invBUAList.clear();
	}

	public void setRowSelected(Object selectedInvBUAList, boolean isEdit){
		this.rowSelected = invBUAList.indexOf(selectedInvBUAList);
	}

	public void updateInvBUARow(int rowSelected, Object newInvBUAList){
		invBUAList.set(rowSelected, newInvBUAList);
	}

	public void deleteInvBUAList(int rowSelected){
		invBUAList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
     
	public Integer getBuildUnbuildAssemblyCode() {
		return buildUnbuildAssemblyCode;
	}
	
	public void setBuildUnbuildAssemblyCode(Integer buildUnbuildAssemblyCode) {
		this.buildUnbuildAssemblyCode = buildUnbuildAssemblyCode;
	}
	
	public String getType() {
	   	
   		return type;
   	
   }
   
   public void setType(String type) {
   	
   		this.type = type;
   	
   }
   
   public ArrayList getTypeList() {
   	
   		return typeList;
   	
   }
	
public String getBatchName() {
		
		return batchName;
		
	}
	
	public void setBatchName(String batchName) {
		
		this.batchName = batchName;
		
	}
	
	public ArrayList getBatchNameList() {
		
		return batchNameList;
		
	}
	
	public void setBatchNameList(String batchName) {
		
		batchNameList.add(batchName);
		
	}
	
	public void clearBatchNameList() {   	 
		
		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();

	}
	
	public String getViewType() {

		return viewType ;   	

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}
	
	public String getCustomer() {
		
		return customer;
		
	}
	
	public void setCustomer(String customer) {
		
		this.customer = customer;
		
	}
	
	public ArrayList getCustomerList() {
		
		return customerList;
		
	}
	
	public void setCustomerList(String customer) {
		
		customerList.add(customer);
		
	}
	
	public void clearCustomerList() {
		
		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getUseCustomerPulldown() {
		
		return useCustomerPulldown;
		
	}
	
	public void setUseCustomerPulldown(boolean useCustomerPulldown) {
		
		this.useCustomerPulldown = useCustomerPulldown;
		
	}
	
public String getIsCustomerEntered() {
		
		return isCustomerEntered;
		
	}

public String getCustomerName() {
	
	return customerName;
	
}

public void setCustomerName(String customerName) {
	
	this.customerName = customerName;
	
}
	
	
	public String getApprovalStatus() {
		return approvalStatus;
	}
	
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDueDate() {
		return dueDate;
	}
	
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	
	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public boolean getEnableFields() {
		return enableFields;
	}
	
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}	
	
	public ArrayList getLocationList() {
		return locationList;
	}
	
	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}
	
	public void clearLocationList() {
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getReasonForRejection() {
	   	
	  return reasonForRejection;
	   	  
    }
   
    public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
    }
	
	public String getPosted() {
		return posted;
	}
	
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getBoNumber() {
		return boNumber;
	}
	
	public void setBoNumber(String boNumber) {
		this.boNumber = boNumber;
	}
	
	
	public ArrayList getBoNumberList() {
	   	
	   	  return boNumberList;
	   	
 }
	   
 public void setBoNumberList(String boNumber) {
	   	
	   	  boNumberList.add(boNumber);
	   	
	   }
	   
 public void clearBoNumberList() {
	   	
	   	  boNumberList.clear();
	   	  boNumberList.add(Constants.GLOBAL_BLANK);
	   	
 }
 
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public boolean getVoid(){
		return buaVoid;
	}
	
	public void setVoid(boolean buaVoid){
		this.buaVoid = buaVoid;
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowSaveButton() {
		return showSaveButton;
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public boolean getShowBatchName() {
		
		return showBatchName;
		
	}
	
	public void setShowBatchName(boolean showBatchName) {
		
		this.showBatchName = showBatchName;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		type = null;
	   typeList.clear();
	   typeList.add("BO MATCHED");
	   typeList.add("ITEMS");
		customer = Constants.GLOBAL_BLANK;
		location = Constants.GLOBAL_BLANK;
		isCustomerEntered = null;
		customerName = Constants.GLOBAL_BLANK;
	   date = null;
	   dueDate = null;
	   documentNumber = null;
	   boNumber = null;
	   referenceNumber = null;
	   description = null;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   buaVoid = false;
	   reportTypeList.clear();
	   reportTypeList.add(Constants.GLOBAL_BLANK);
	   reportTypeList.add("PACKAGING ORDER REPORT");
	   reportTypeList.add("MANUFACTURING ORDER REPORT");
	   
	   viewTypeList.clear();
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
	   viewType = Constants.REPORT_VIEW_TYPE_PDF;

	   for (int i=0; i<invBUAList.size(); i++) {
	  	
	  	  InvBuildUnbuildAssemblyEntryList actionList = (InvBuildUnbuildAssemblyEntryList)invBUAList.get(i);
	  	  actionList.setIsLocationEntered(null);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsBuildQuantityByEntered(null);
	  	
	  } 

   }
	
	
	  public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";
		   
		   // Remove first $ character
		   misc = misc.substring(1);
		   
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;	
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);
		   
		   /*if(y==0)
			   return new ArrayList();*/
		   
		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {	        	

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);	 
				   }else{
					   miscList.add("null");
				   }
				   
				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   
			   }

		   	   }
		   return miscList;
	   }
	   

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
		 request.getParameter("journalButton") != null ||
		 request.getParameter("printButton") != null){
      	
    	  
    	  System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>1");
    	  if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("buildUnbuildAssemblyBatchEntry.error.batchNameRequired"));
			}
			
    	  
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("buildUnbuildAssemblyEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	        errors.add("date", 
		       new ActionMessage("buildUnbuildAssemblyEntry.error.dateInvalid"));
		 }
		 
		 
		
			

		 if(Common.validateRequired(dueDate)){
            errors.add("dueDate",
               new ActionMessage("buildUnbuildAssemblyEntry.error.dueDateRequired"));
         }
         
		 if(!Common.validateDateFormat(dueDate)){
	        errors.add("dueDate", 
		       new ActionMessage("buildUnbuildAssemblyEntry.error.dueDateInvalid"));
		 }
		 

	
		 if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("buildUnbuildAssemblyEntry.error.batchNameRequired"));
			}
			
			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("buildUnbuildAssemblyEntry.error.customerRequired"));
			}
			
			System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>2");
         int numberOfLines = 0;
      	 
      	 Iterator i = invBUAList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvBuildUnbuildAssemblyEntryList blList = (InvBuildUnbuildAssemblyEntryList)i.next();      	 	 
			 
      	 	 if (Common.validateRequired(blList.getLocation()) &&
      	 	     Common.validateRequired(blList.getItemName()) &&
      	 	     Common.validateRequired(blList.getBuildQuantityBy()) &&
      	 	     Common.validateRequired(blList.getUnit())) continue;
      	 	     
      	 	 numberOfLines++;
      	 	 /*
      	 	try{
      	 		String separator = "$";
      	 		String misc = "";
      		   // Remove first $ character
      		   misc = blList.getMisc().substring(1);
      		   
      		   // Counter
      		   int start = 0;
      		   int nextIndex = misc.indexOf(separator, start);
      		   int length = nextIndex - start;	
      		   int counter;
      		   counter = Integer.parseInt(misc.substring(start, start + length));
      		   
      	 		ArrayList miscList = this.getExpiryDateStr(blList.getMisc(), counter);
      	 		System.out.println("rilList.getMisc() : " + blList.getMisc());
      	 		Iterator mi = miscList.iterator();
     	 		
      	 		int ctrError = 0;
      	 		int ctr = 0;
      	 		
      	 		while(mi.hasNext()){
      	 				String miscStr = (String)mi.next();
      	 			    
      	 				if(!Common.validateDateFormat(miscStr)){
      	 					errors.add("date", 
      	 							new ActionMessage("buildUnbuildAssemblyEntry.error.expiryDateInvalid"));
      	 					ctrError++;
      	 				}
      	 				
      	 				System.out.println("miscStr: "+miscStr);
      	 				if(miscStr=="null"){
      	 					ctrError++;
      	 				}
      	 		}
      	 		//if ctr==Error => No Date Will Apply
      	 		//if ctr>error => Invalid Date
      	 		System.out.println("CTR: "+  miscList.size());
      	 		System.out.println("ctrError: "+  ctrError);
      	 		System.out.println("counter: "+  counter);
      	 			if(ctrError>0 && ctrError!=miscList.size()){
      	 				errors.add("date", 
      	      			  new ActionMessage("buildUnbuildAssemblyEntry.error.expiryDateNullInvalid"));
      	 			}
      	 			
      	 		//if error==0 Add Expiry Date
      	 		
      	 		
      	 	}catch(Exception ex){
  	  	    	
  	  	    }*/
  	  	    
      	 
	         if(Common.validateRequired(blList.getLocation()) || blList.getLocation().equals(Constants.GLOBAL_BLANK)){
	            errors.add("location",
	            	new ActionMessage("buildUnbuildAssemblyEntry.error.locationRequired", blList.getLineNumber()));
	         }
	         if(Common.validateRequired(blList.getItemName()) || blList.getItemName().equals(Constants.GLOBAL_BLANK)){
	            errors.add("itemName",
	            	new ActionMessage("buildUnbuildAssemblyEntry.error.itemNameRequired", blList.getLineNumber()));
	         }
	         if(Common.validateRequired(blList.getBuildQuantityBy())) {
	         	errors.add("buildQuantityBy",
	         		new ActionMessage("buildUnbuildAssemblyEntry.error.buildQuantityByRequired", blList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(blList.getBuildQuantityBy())){
	            errors.add("buildQuantityBy",
	               new ActionMessage("buildUnbuildAssemblyEntry.error.buildQuantityByInvalid", blList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(blList.getBuildQuantityBy()) && Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3) == 0) {
		        errors.add("buildQuantityBy",
		           new ActionMessage("buildUnbuildAssemblyEntry.error.zeroQuantityNotAllowed", blList.getLineNumber()));
		     }
		 	/*if(!Common.validateDecimalExist(blList.getBuildQuantityBy()) && Common.validateNumberFormat(blList.getBuildQuantityBy())){
		        errors.add("buildQuantityBy",
		           new ActionMessage("buildUnbuildAssemblyEntry.error.decimalQuantityNotAllowed", blList.getLineNumber()));
		     }*/
		 	if(Common.validateRequired(blList.getUnit()) || blList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("buildUnbuildAssemblyEntry.error.unitRequired", blList.getLineNumber()));
	         }
		 	
		 	
		 	System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>3");
		 	
		 // Items
      	 	if(type.equals("ITEMS")) {
      	 		/*
      	 		if(!Common.validateRequired(blList.getBuildQuantityBy()) && Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3) <= 0) {
          	 			System.out.println("negativeOrZeroQuantityNotAllowed");
          	 		errors.add("received",
          	 				new ActionMessage("buildUnbuildAssemblyEntry.error.negativeOrZeroQuantityNotAllowed", blList.getLineNumber()));
          	 	}
      	 		*/
      	 	}
      	 	// PO Matched 
      	 	if(type.equals("BO MATCHED")) {
      	 		
          	 	if(!Common.validateRequired(blList.getBuildQuantityBy()) && Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3) < 0) {
          	 		errors.add("received",
          	 				new ActionMessage("buildUnbuildAssemblyEntry.error.negativeQuantityNotAllowed", blList.getLineNumber()));
          	 	}
          	 	
          	 	// 0 qty received
      	 		if(!Common.validateRequired(blList.getBuildQuantityBy()) && Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3) == 0) {
      	 			numberOfLines--;
      	 		}
      	 		
      	 		if(approvalStatus.length() == 0 && Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3) > Common.convertStringMoneyToDouble(blList.getBuildQuantityBy(), (short)3)) {
      	 			errors.add("buildQuantityBy",
          	 				new ActionMessage("buildUnbuildAssemblyEntry.error.receivedQuantityGreaterThanRemainingQuantity", blList.getLineNumber()));
          	 	}
      	 		
      	 	}
      	 	System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>4");
       
	    }
	    
	    if (numberOfLines == 0) {
         	System.out.println("buildUnbuildAssemblyMustHaveLine");
         	errors.add("buildUnbuildAssembly",
               new ActionMessage("buildUnbuildAssemblyEntry.error.buildUnbuildAssemblyMustHaveLine"));
         	
        }
        
        	System.out.println("buildUnbuildAssemblyMustHaveLine");
	    System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>5");
    } else if (!Common.validateRequired(request.getParameter("isPurchaseOrderEntered"))) {
    	System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>6");
			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("buildUnbuildAssemblyEntry.error.customerRequired"));
			}
			
			if (type.equals("BO MATCHED")) {
			
				if(Common.validateRequired(boNumber)){
					errors.add("boNumber",
							new ActionMessage("buildUnbuildAssemblyEntry.error.boNumberRequired"));
				}
				
			}
			System.out.println("InvBuildUnbuildAssemblyEntryForm ------------>7");
      } else if (!Common.validateRequired(request.getParameter("isLocationEntered"))) {
      	
      	 if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("location",
               new ActionMessage("buildUnbuildAssemblyEntry.error.locationRequired"));
         }
      	      
      } 
      
      return(errors);	
   }
}
