package com.struts.inv.buildunbuildassemblyentry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvBuildUnbuildAssemblyEntryList implements Serializable {

	private Integer buildUnbuildAssemblyLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String specificGravity = null;
	private String standardFillSize = null;
	private String quantity = null;
	private String remaining = null;
	private String buildQuantityBy = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private Integer blBlCode = null;
	private boolean deleteCheckbox = false;
	
   
	private String isLocationEntered = null;
	private String isItemEntered = null;
	private String isBuildQuantityByEntered = null;
	private String isUnitEntered = null;
	private ArrayList tagList = new ArrayList();
	
	private String misc = null;
	private InvBuildUnbuildAssemblyEntryForm parentBean;   

	public InvBuildUnbuildAssemblyEntryList(InvBuildUnbuildAssemblyEntryForm parentBean,
		Integer buildUnbuildAssemblyLineCode, String lineNumber, String location,
		String itemName, String itemDescription, String specificGravity, String standardFillSize, String quantity, String remaining, String buildQuantityBy, String unit, 
		Integer blBlCode,
		String misc) {
	
		this.parentBean = parentBean;
		this.buildUnbuildAssemblyLineCode = buildUnbuildAssemblyLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.specificGravity = specificGravity;
		this.standardFillSize = standardFillSize;
		this.quantity = quantity;
		this.remaining = remaining;
		this.buildQuantityBy = buildQuantityBy;
		this.unit = unit;
		this.blBlCode = blBlCode;
		this.misc = misc;
	}
	
	public InvBuildUnbuildAssemblyEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvBuildUnbuildAssemblyEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public Integer getBuildUnbuildAssemblyLineCode() {
		
		return buildUnbuildAssemblyLineCode;
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}	
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getSpecificGravity() {
		
		return specificGravity;
		
	}
	
	public void setSpecificGravity(String specificGravity) {
		
		this.specificGravity = specificGravity;
		
	}
	
	public String getStandardFillSize() {
		
		return standardFillSize;
		
	}
	
	public void setStandardFillSize(String standardFillSize) {
		
		this.standardFillSize = standardFillSize;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}	
	
	public String getRemaining() {
		
		return remaining;
		
	}
	
	public void setRemaining(String remaining) {
		
		this.remaining = remaining;
		
	}
	
	public String getBuildQuantityBy() {
		
		return buildQuantityBy;
		
	}
	
	public void setBuildQuantityBy(String buildQuantityBy) {
		
		this.buildQuantityBy = buildQuantityBy;
		
	}
	
	
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}	
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
	   	
		unitList.clear();
	   	
	}
	
	public Integer getBlBlCode() {
		
		return blBlCode;
		
	}
	
	public boolean isDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getIsLocationEntered() {
		
		return isLocationEntered;
		
	}
	
	public void setIsLocationEntered(String isLocationEntered) {	
		
		if (isLocationEntered != null && isLocationEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isLocationEntered = null;
		
	}
	
	public String getIsBuildQuantityByEntered() {
		
		return isBuildQuantityByEntered;
		
	}
	
	public void setIsBuildQuantityByEntered(String isBuildQuantityByEntered) {
		
		if (isBuildQuantityByEntered != null && isBuildQuantityByEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isBuildQuantityByEntered = null;
		
	}
	
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}
	
	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}
	
	public void clearTagList(){
		tagList.clear();
	}
	
				
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}
}
