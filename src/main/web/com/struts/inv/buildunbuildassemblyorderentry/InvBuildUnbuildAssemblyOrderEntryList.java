package com.struts.inv.buildunbuildassemblyorderentry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvBuildUnbuildAssemblyOrderEntryList implements Serializable {

	private Integer buildUnbuildAssemblyLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String specificGravity = null;
	private String standardFillSize = null;
	private String quantity = null;
	private String received = null;
	

	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private boolean deleteCheckbox = false;
   
	private String isLocationEntered = null;
	private String isItemEntered = null;
	private String isQuantityEntered = null;
	private String isUnitEntered = null;
	private ArrayList tagList = new ArrayList();
	
	private String misc = null;
	private InvBuildUnbuildAssemblyOrderEntryForm parentBean;   

	public InvBuildUnbuildAssemblyOrderEntryList(InvBuildUnbuildAssemblyOrderEntryForm parentBean,
		Integer buildUnbuildAssemblyLineCode, String lineNumber, String location,
		String itemName, String itemDescription, String specificGravity, String standardFillSize, String quantity, String received, String unit, String misc) {
	
		this.parentBean = parentBean;
		this.buildUnbuildAssemblyLineCode = buildUnbuildAssemblyLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.specificGravity = specificGravity;
		this.standardFillSize = standardFillSize;
		this.quantity = quantity;
		this.received = received;
		this.unit = unit;
		this.misc = misc;
	}
	
	public InvBuildUnbuildAssemblyOrderEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvBuildUnbuildAssemblyOrderEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public Integer getBuildUnbuildAssemblyLineCode() {
		
		return buildUnbuildAssemblyLineCode;
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}	
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getSpecificGravity() {
		
		return specificGravity;
		
	}
	
	public void setSpecificGravity(String specificGravity) {
		
		this.specificGravity = specificGravity;
		
	}
	
public String getStandardFillSize() {
		
		return standardFillSize;
		
	}
	
	public void setStandardFillSize(String standardFillSize) {
		
		this.standardFillSize = standardFillSize;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}	
	
	public String getReceived() {
		
		return received;
		
	}
	
	public void setReceived(String received) {
		
		this.received = received;
		
	}
	
	
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}	
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
	   	
		unitList.clear();
	   	
	}	
	
	public boolean isDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getIsLocationEntered() {
		
		return isLocationEntered;
		
	}
	
	public void setIsLocationEntered(String isLocationEntered) {	
		
		if (isLocationEntered != null && isLocationEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isLocationEntered = null;
		
	}
	
	public String getIsQuantityEntered() {
		
		return isQuantityEntered;
		
	}
	
	public void setIsQuantityEntered(String isQuantityEntered) {
		
		if (isQuantityEntered != null && isQuantityEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isQuantityEntered = null;
		
	}
	
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}
	
	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}
	
	public void clearTagList(){
		tagList.clear();
	}
	
				
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}
}
