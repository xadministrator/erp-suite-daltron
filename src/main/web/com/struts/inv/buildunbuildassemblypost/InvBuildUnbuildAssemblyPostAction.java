package com.struts.inv.buildunbuildassemblypost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalInvCSTRemainingQuantityIsLessThanZeroException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvBuildUnbuildAssemblyPostController;
import com.ejb.txn.InvBuildUnbuildAssemblyPostControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvBuildUnbuildAssemblyDetails;

public final class InvBuildUnbuildAssemblyPostAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvBuildUnbuildAssemblyPostAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         InvBuildUnbuildAssemblyPostForm actionForm = (InvBuildUnbuildAssemblyPostForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.INV_BUILD_UNBUILD_ASSEMBLY_POST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invBuildUnbuildAssemblyPost");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize InvBuildUnbuildAssemblyPostController EJB
*******************************************************/

         InvBuildUnbuildAssemblyPostControllerHome homeBUP = null;
         InvBuildUnbuildAssemblyPostController ejbBUP = null;

         try {
          
            homeBUP = (InvBuildUnbuildAssemblyPostControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvBuildUnbuildAssemblyPostControllerEJB", InvBuildUnbuildAssemblyPostControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBUP = homeBUP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call InvBuildUnbuildAssemblyPostController EJB
     getGlFcPrecisionUnit
  *******************************************************/

         short precisionUnit = 0;
         boolean enableVoucherBatch = false; 
         
         try { 
         	
            precisionUnit = ejbBUP.getGlFcPrecisionUnit(user.getCmpCode());
            actionForm.setShowBatchName(enableVoucherBatch);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ap VP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("invBuildUnbuildAssemblyPost"));
	     
/*******************************************************
   -- Ap VP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("invBuildUnbuildAssemblyPost"));         

/*******************************************************
   -- Ap VP Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap VP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Inv BUP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();   	        	
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbBUP.getInvBuaPostableByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	     	}
            
            try {
            	
            	actionForm.clearInvBUPList();
            	
            	ArrayList list = ejbBUP.getInvBuaPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		InvBuildUnbuildAssemblyDetails mdetails = (InvBuildUnbuildAssemblyDetails)i.next();
            		
            		InvBuildUnbuildAssemblyPostList invBUPList = new InvBuildUnbuildAssemblyPostList(actionForm,
            		    mdetails.getBuaCode(),         		                		    
                        Common.convertSQLDateToString(mdetails.getBuaDate()),
						mdetails.getBuaDocumentNumber(),
                        mdetails.getBuaReferenceNumber(),
						mdetails.getBuaDescription());
            		    
            		actionForm.saveInvBUPList(invBUPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("buildUnbuildAssemblyPost.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invBuildUnbuildAssemblyPost");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("invBuildUnbuildAssemblyPost")); 

/*******************************************************
   -- Inv BUP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv BUP Post Action --
*******************************************************/

         } else if (request.getParameter("postButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
                        
		    for(int i=0; i<actionForm.getInvBUPListSize(); i++) {
		    
		       InvBuildUnbuildAssemblyPostList actionList = actionForm.getInvBUPByIndex(i);
		    	
		       if (actionList.getPost()) {
		       	
		       	try {
		       		
		       		ejbBUP.executeInvBuaPost(actionList.getBuildUnbuildAssemblyCode(), user.getUserName(),
		       			 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		       		
		       		actionForm.deleteInvBUPList(i);
		       		i--;
		       		
		       	} catch (GlJREffectiveDateNoPeriodExistException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.effectiveDateNoPeriod", actionList.getReferenceNumber()));
		       		
		       	} catch (GlJREffectiveDatePeriodClosedException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.effectiveDatePeriodClosed", actionList.getReferenceNumber()));
		       		
		       	} catch (GlobalJournalNotBalanceException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.journalNotBalance", actionList.getReferenceNumber()));
		       		
		       	} catch (GlobalTransactionAlreadyPostedException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.transactionAlreadyPosted", actionList.getReferenceNumber()));
		       		
		       	} catch (GlobalRecordAlreadyDeletedException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.recordAlreadyDeleted", actionList.getReferenceNumber()));
		       		
		       	} catch (GlobalInventoryDateException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));                 	     	
		       		
		       	} catch (GlobalInvItemLocationNotFoundException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.itemLocationNotFound", ex.getMessage()));                 	     	
		       		
		       	} catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
		       		
		       		errors.add(ActionMessages.GLOBAL_MESSAGE,
		       				new ActionMessage("buildUnbuildAssemblyPost.error.mustNotBeZero", ex.getMessage()));                 	     	

		       	} catch(GlobalBranchAccountNumberInvalidException ex) {
		           	
		           	errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("buildUnbuildAssemblyPost.error.branchAccountNumberInvalid", ex.getMessage()));
  	
		           	errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("buildUnbuildAssemblyPost.error.accountNumberInvalid", ex.getMessage()));

	             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	            	
	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("buildUnbuildAssemblyPost.error.noNegativeInventoryCostingCOA"));

	             } catch (GlobalRecordInvalidException ex) {
	            	 
 	                errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                		new ActionMessage("buildUnbuildAssemblyPost.error.insufficientStocks", ex.getMessage()));
		    			
		       	} catch (EJBException ex) {
		       		
		       		if (log.isInfoEnabled()) {
		       			
		       			log.info("EJBException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + ex.getMessage() +
		       					" session: " + session.getId());
		       			return mapping.findForward("cmnErrorPage"); 
		       			
		       		}
		       	}
		       }
	        }	
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invBuildUnbuildAssemblyPost");

            }
	        
	        try {
            	
            	
				actionForm.setLineCount(0);
            	actionForm.clearInvBUPList();
            	
            	ArrayList list = ejbBUP.getInvBuaPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		InvBuildUnbuildAssemblyDetails mdetails = (InvBuildUnbuildAssemblyDetails)i.next();
            		
            		InvBuildUnbuildAssemblyPostList invBUPList = new InvBuildUnbuildAssemblyPostList(actionForm,
            		    mdetails.getBuaCode(),          		                		    
                        Common.convertSQLDateToString(mdetails.getBuaDate()),
                        mdetails.getBuaDocumentNumber(),
                        mdetails.getBuaReferenceNumber(),
						mdetails.getBuaDescription());
            		    
            		actionForm.saveInvBUPList(invBUPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvBuildUnbuildAssemblyPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }	                       
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("invBuildUnbuildAssemblyPost")); 	     

/*******************************************************
   -- Inv BUP Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invBuildUnbuildAssemblyPost");

            }

            actionForm.clearInvBUPList();
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("invBuildUnbuildAssemblyPost"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      		e.printStackTrace();
          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvBuildUnbuildAssemblyPostAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}