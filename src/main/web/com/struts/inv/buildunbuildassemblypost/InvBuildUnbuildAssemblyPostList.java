package com.struts.inv.buildunbuildassemblypost;

import java.io.Serializable;

public class InvBuildUnbuildAssemblyPostList implements Serializable {

   private Integer buildUnbuildAssemblyCode = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null; 
   private String description = null;
   
   private boolean post = false;
       
   private InvBuildUnbuildAssemblyPostForm parentBean;
    
   public InvBuildUnbuildAssemblyPostList(InvBuildUnbuildAssemblyPostForm parentBean,
      Integer buildUnbuildAssemblyCode,
      String date,
	  String documentNumber,
      String referenceNumber,
	  String description) {

      this.parentBean = parentBean;
      this.buildUnbuildAssemblyCode = buildUnbuildAssemblyCode;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.description = description;
      
   }

   public Integer getBuildUnbuildAssemblyCode() {

      return buildUnbuildAssemblyCode;

   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }

   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
   
}