package com.struts.inv.findadjustment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.InvTagSerialNumberNotFoundException;
import com.ejb.txn.InvAdjustmentEntryController;
import com.ejb.txn.InvAdjustmentEntryControllerHome;
import com.ejb.txn.InvFindAdjustmentController;
import com.ejb.txn.InvFindAdjustmentControllerHome;
import com.struts.ar.findinvoice.ArFindInvoiceList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArInvoiceDetails;
import com.util.InvAdjustmentDetails;
import com.util.InvModAdjustmentDetails;

public final class InvFindAdjustmentAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindAdjustmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindAdjustmentForm actionForm = (InvFindAdjustmentForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_ADJUSTMENT_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindAdjustment");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize InvFindAdjustmentController EJB
*******************************************************/
			
			InvFindAdjustmentControllerHome homeFA = null;
			InvFindAdjustmentController ejbFA = null;
			InvAdjustmentEntryControllerHome homeADJ = null;
	         InvAdjustmentEntryController ejbADJ = null;
			
			try {
				
				homeFA = (InvFindAdjustmentControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindAdjustmentControllerEJB", InvFindAdjustmentControllerHome.class);
				
				homeADJ = (InvAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
		                lookUpHome("ejb/InvAdjustmentEntryControllerEJB", InvAdjustmentEntryControllerHome.class);
		            
								
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindAdjustmentAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFA = homeFA.create();
				ejbADJ = homeADJ.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindAdjustmentAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  
			
/*******************************************************
   -- Inv FA Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindAdjustment"));
				
/*******************************************************
   -- Inv FA Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindAdjustment"));                         
				
/*******************************************************
   -- Inv FA First Action --
*******************************************************/ 
								
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
   -- Inv FA Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
   -- Inv FA Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
   -- Inv FA Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}
					
					if (!Common.validateRequired(actionForm.getType())) {
						
						criteria.put("type", actionForm.getType());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
		        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
		        		
		        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
		        		
		        	}

		        	if (!Common.validateRequired(actionForm.getPosted())) {
		        		
		        		criteria.put("posted", actionForm.getPosted());
		        		
		        	}
		        	
		        	
		        	if (!actionForm.getAdjustmentVoid()) criteria.put("void", new Byte((byte)0));
		        	else criteria.put("void", new Byte((byte)1));
		        	
		        	if (!actionForm.getAdjustmentRequestGenerated()) criteria.put("generated", new Byte((byte)0));
		        	else criteria.put("generated", new Byte((byte)1));
					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFA.getInvAdjSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFAList();
					System.out.println("------>"+actionForm.getAdjustmentRequestGenerated());
					ArrayList list = ejbFA.getInvAdjByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvModAdjustmentDetails mdetails = (InvModAdjustmentDetails)i.next();

						InvFindAdjustmentList invFAList = new InvFindAdjustmentList(actionForm,
								mdetails.getAdjCode(),								
								Common.convertSQLDateToString(mdetails.getAdjDate()),
								mdetails.getAdjType(),
								mdetails.getAdjDocumentNumber(),
								mdetails.getAdjReferenceNumber(),
								mdetails.getAdjCoaAccountNumber(),
								mdetails.getAdjCoaAccountDescription());
						
						actionForm.saveInvFAList(invFAList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findAdjustment.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindAdjustmentAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
if (!errors.isEmpty()) {
	
	saveErrors(request, new ActionMessages(errors));
	return mapping.findForward("invFindAdjustment");
	
}

actionForm.reset(mapping, request);

if (actionForm.getTableType() == null) {
	
	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	
}	                       

return(mapping.findForward("invFindAdjustment"));

/*******************************************************
 -- Inv FA Close Action --
 *******************************************************/
	
} else if (request.getParameter("closeButton") != null) {

return(mapping.findForward("cmnMain"));

	/*******************************************************
	 -- Inv FA Open Action --
	 *******************************************************/
} else if (request.getParameter("invFAList[" + 
		actionForm.getRowSelected() + "].openButton") != null) {
	
	InvFindAdjustmentList invFAList =
		actionForm.getInvFAByIndex(actionForm.getRowSelected());

	String type;
	type=invFAList.getType();
	if (type.equals("GENERAL") || type.equals("WASTAGE") || type.equals("VARIANCE")||type.equals("ISSUANCE")){
	String path = "/invAdjustmentEntry.do?forward=1" +
	"&adjustmentCode=" + invFAList.getAdjustmentCode();
	 System.out.print("Type1 "+invFAList.getType() +" "+path);
	
	return(new ActionForward(path));}
	else { 
	 
	 String path1 = "/invAdjustmentRequest.do?forward=1" +
		"&adjustmentCode=" + invFAList.getAdjustmentCode();
	 System.out.print("Type2 "+invFAList.getType() +" "+path1);
		return(new ActionForward(path1));}
	 
 
/*******************************************************
-- Ar FI Credit Memo Button --
*******************************************************/

 } else if (request.getParameter("submitButton") != null &&
		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	 
	 
	 for (int i = 0; i<actionForm.getInvFAListSize(); i++) {

		 	InvFindAdjustmentList invFindAdjustment = actionForm.getInvFAByIndex(i);   
	 
		 	if(invFindAdjustment.getSubmit()){

		 		try {
		 			
		 			ejbADJ.executeInvAdjSubmit(invFindAdjustment.getAdjustmentCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		 			actionForm.deleteInvFAList(i);
           	     	i--;
		 			
		 		}catch (GlobalDocumentNumberNotUniqueException ex) {
		           	
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));    
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalAccountNumberInvalidException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));                               	                 	                   
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));
	           	   
	           } catch (GlobalInvItemLocationNotFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));
	           	   
	           } catch (GlobalInvTagMissingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.invTagInventoriableError", ex.getMessage()));
	           	   
	           } catch (InvTagSerialNumberNotFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.invTagSerialNumberDoesNotExistsError", ex.getMessage()));
	           	   
	           }catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
	              	
	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

	           } catch(GlobalBranchAccountNumberInvalidException ex) {
				
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (GlobalExpiryDateNotFoundException ex) {
	        	   
	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));
	        	   
	           }catch (GlobalMiscInfoIsRequiredException ex) {
	        	   
	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));
	        	   
	           }catch (GlobalRecordInvalidException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

	           }catch (EJBException ex) {
	        	   if (log.isInfoEnabled()) {

	        		   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
	        				   " session: " + session.getId());
	        	   }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
		 		if (!errors.isEmpty()) {

		               saveErrors(request, new ActionMessages(errors));
		               return mapping.findForward("invFindAdjustment");

		            } 
		 		
		 		
		 		
		 	}
	 
		 	
	 
	 }
	 
	 actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
     return(mapping.findForward("invFindAdjustment"));
		    	 
}

	
/*******************************************************
 -- Inv FA Load Action --
 *******************************************************/
				
			
			
			if (frParam != null) {
				
				actionForm.clearInvFAList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				
				if (actionForm.getTableType() == null || request.getParameter("findDraft") != null || request.getParameter("findAdjustmentRequestToGenerate") != null) {
					
					actionForm.setType(Constants.GLOBAL_BLANK);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setApprovalStatus("DRAFT"); 
					actionForm.setOrderBy(Constants.GLOBAL_BLANK);
					actionForm.setAdjustmentVoid(false);
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					HashMap criteria = new HashMap();
					criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
					if(request.getParameter("findDraft") != null) {
		            	
		            	return new ActionForward("/invFindAdjustment.do?goButton=1");
		            	
		            }
					else if (request.getParameter("findAdjustmentRequestToGenerate") != null) {
						
						actionForm.setType("REQUEST");
						actionForm.setDocumentNumberFrom(null);
						actionForm.setDocumentNumberTo(null);
						actionForm.setDateFrom(null);
						actionForm.setDateTo(null);
						actionForm.setReferenceNumber(null);
						actionForm.setPosted(Constants.GLOBAL_YES);
						actionForm.setApprovalStatus("APPROVED");
						actionForm.setOrderBy(Constants.GLOBAL_BLANK);
						actionForm.setAdjustmentVoid(false);
						actionForm.setLineCount(0);
						actionForm.setDisableNextButton(true);
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						actionForm.setDisableLastButton(true);
						actionForm.reset(mapping, request);
						
						criteria = new HashMap();
						criteria.put("posted", actionForm.getPosted());
			            criteria.put("approvalStatus", actionForm.getApprovalStatus());
						
						return new ActionForward("/invFindAdjustment.do?goButton=1");
						
					}
					
				} else {
					
					try {
						
						actionForm.clearInvFAList();
						
						ArrayList list = ejbFA.getInvAdjByCriteria(actionForm.getCriteria(),
								new Integer(actionForm.getLineCount()), 
								new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (list.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							list.remove(list.size() - 1);
							
						}
						
						Iterator i = list.iterator();
						
						while (i.hasNext()) {
							
							InvModAdjustmentDetails mdetails = (InvModAdjustmentDetails)i.next();

							InvFindAdjustmentList invFAList = new InvFindAdjustmentList(actionForm,
									mdetails.getAdjCode(),								
									Common.convertSQLDateToString(mdetails.getAdjDate()),
									mdetails.getAdjType(),
									mdetails.getAdjDocumentNumber(),
									mdetails.getAdjReferenceNumber(),
									mdetails.getAdjCoaAccountNumber(),
									mdetails.getAdjCoaAccountDescription());
							
							actionForm.saveInvFAList(invFAList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
							
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findAdjustment.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindAdjustmentAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				return(mapping.findForward("invFindAdjustment"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			/*******************************************************
			 System Failed: Forward to error page 
			 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindAdjustmentAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}