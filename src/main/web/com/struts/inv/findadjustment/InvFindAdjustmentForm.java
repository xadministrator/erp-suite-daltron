package com.struts.inv.findadjustment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvFindAdjustmentForm extends ActionForm implements Serializable {
	
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String posted = null;
	private boolean adjustmentVoid = false;
	private boolean adjustmentRequestGenerated = false;
	private ArrayList postedList = new ArrayList();  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFAList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindAdjustmentList getInvFAByIndex(int index) {
		
		return((InvFindAdjustmentList)invFAList.get(index));
		
	}
	
	public Object[] getInvFAList() {
		
		return invFAList.toArray();
		
	}
	
	public int getInvFAListSize() {
		
		return invFAList.size();
		
	}
	
	public void saveInvFAList(Object newInvFAList) {
		
		invFAList.add(newInvFAList);
		
	}
	
	public void clearInvFAList() {
		
		invFAList.clear();
		
	}
	
	public void deleteInvFAList(int rowSelected) {

		invFAList.remove(rowSelected);

	   }
	
	public void setRowSelected(Object selectedInvFAList, boolean isEdit) {
		
		this.rowSelected = invFAList.indexOf(selectedInvFAList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public ArrayList getApprovalStatusList() {
		
		return approvalStatusList;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}   	
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}
	
	public boolean getAdjustmentRequestGenerated() {
		return adjustmentRequestGenerated;
	}
	
	public void setAdjustmentRequestGenerated(boolean adjustmentRequestGenerated) {
		this.adjustmentRequestGenerated = adjustmentRequestGenerated;
	}
	
	
	public boolean getAdjustmentVoid() {
		return adjustmentVoid;
	}
	
	public void setAdjustmentVoid(boolean adjustmentVoid) {
		this.adjustmentVoid = adjustmentVoid;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		typeList.add("GENERAL");
		typeList.add("WASTAGE");
		typeList.add("VARIANCE");
		typeList.add("COST-VARIANCE");
		typeList.add("REQUEST");
		typeList.add("ISSUANCE");

	    approvalStatusList.clear();
	    approvalStatusList.add(Constants.GLOBAL_BLANK);
	    approvalStatusList.add("DRAFT");
	    approvalStatusList.add("APPROVED");
	    approvalStatusList.add("N/A");
	    approvalStatusList.add("PENDING");
	         
	    postedList.clear();
	    postedList.add(Constants.GLOBAL_BLANK);
	    postedList.add(Constants.GLOBAL_YES);
	    postedList.add(Constants.GLOBAL_NO);
	    
		showDetailsButton = null;
		hideDetailsButton = null;
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("REFERENCE NUMBER");
	    	orderByList.add("DOCUMENT NUMBER");
	    	
		  }		
		
		if (request.getParameter("findAdjustmentRequestToGenerate") != null) {
			
		}
		else adjustmentRequestGenerated = false;
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if (!Common.validateDateFormat(dateFrom)) {
			    	
				errors.add("dateFrom",
					new ActionMessage("findAdjustment.error.dateFromInvalid"));
			    		
			}         
			    		     					
			if (!Common.validateDateFormat(dateTo)) {
		    	
				errors.add("dateTo",
					new ActionMessage("findAdjustment.error.dateToInvalid"));
		    		
			}
			
		}
		
		return errors;
		
	}
	
}