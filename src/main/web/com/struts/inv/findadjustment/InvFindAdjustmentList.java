package com.struts.inv.findadjustment;

import java.io.Serializable;

public class InvFindAdjustmentList implements Serializable {
	
	private Integer adjustmentCode = null;
	private String type = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String account =  null;
	private String accountDescription = null;
	private String date = null;
	
	private String openButton = null;
	private boolean submit = false;
	
	private InvFindAdjustmentForm parentBean;
	
	public InvFindAdjustmentList(InvFindAdjustmentForm parentBean,
			Integer adjustmentCode,
			String date,
			String type,
			String documentNumber,
			String referenceNumber,
			String account,
			String accountDescription) {
		
		this.parentBean = parentBean;
		this.adjustmentCode = adjustmentCode;
		this.date = date;
		this.type = type;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.account = account;
		this.accountDescription = accountDescription;

	}
	
	public boolean getSubmit() {
	   	
	   	  return submit;
	   	
	   }
	   
	   public void setSubmit(boolean submit) {
	   	
	   	  this.submit = submit;
	   	
	   }
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getAdjustmentCode() {
		
		return adjustmentCode;
		
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getAccount() {
	
		return account;
		
	}
	
	public String getAccountDescription() {
		
		return accountDescription;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
}