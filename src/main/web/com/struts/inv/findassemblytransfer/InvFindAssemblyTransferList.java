package com.struts.inv.findassemblytransfer;

import java.io.Serializable;

public class InvFindAssemblyTransferList implements Serializable {
	
	private Integer assemblyTransferCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
	
	private String openButton = null;
	
	private InvFindAssemblyTransferForm parentBean;
	
	public InvFindAssemblyTransferList(InvFindAssemblyTransferForm parentBean,
			Integer assemblyTransferCode,
			String date,
			String documentNumber,
			String referenceNumber,
			String description) {
		
		this.parentBean = parentBean;
		this.assemblyTransferCode = assemblyTransferCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.description = description;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getAssemblyTransferCode() {
		
		return assemblyTransferCode;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}

	public String getDescription() {
		
		return description;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
}