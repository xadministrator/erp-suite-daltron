package com.struts.inv.findbranchstocktransfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindBranchStockTransferController;
import com.ejb.txn.InvFindBranchStockTransferControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModBranchStockTransferDetails;

public class InvFindBranchStockTransferAction extends Action{ 
    
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindBranchStockTransferAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindBranchStockTransferForm actionForm = (InvFindBranchStockTransferForm)form;
			
			String frParam = null;
			
			if (request.getParameter("child") == null) {
		         
		         	frParam = Common.getUserPermission(user, Constants.INV_FIND_BRANCH_STOCK_TRANSFER_ID);
		         
		    } else {
		         	
		         	frParam = Constants.FULL_ACCESS;
		         	
		    }
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						if (request.getParameter("child") == null) {

							return mapping.findForward("invFindBranchStockTransfer");
		                  
		                } else {
		                  	
		                	return mapping.findForward("invFindBranchStockTransferChild");
		                  
		                }
						
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize InvFindBranchStockTransferController EJB
*******************************************************/
			
			InvFindBranchStockTransferControllerHome homeFBST = null;
			InvFindBranchStockTransferController ejbFBST = null;
			
			try {
				
			    homeFBST = (InvFindBranchStockTransferControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindBranchStockTransferControllerEJB", InvFindBranchStockTransferControllerHome.class);
								
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindBranchStockTransferAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
			    ejbFBST = homeFBST.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindBranchStockTransferAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  
			
/*******************************************************
   -- Inv FBST Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				if (request.getParameter("child") == null) {

					return mapping.findForward("invFindBranchStockTransfer");
                  
                } else {
                  	
                	return mapping.findForward("invFindBranchStockTransferChild");
                  
                }
				
/*******************************************************
   -- Inv FBST Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				if (request.getParameter("child") == null) {

					return mapping.findForward("invFindBranchStockTransfer");
                  
                } else {
                  	
                	return mapping.findForward("invFindBranchStockTransferChild");
                  
                }

/*******************************************************
   -- Inv FBSI First Action --
*******************************************************/ 
				
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
   -- Inv FBST Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
   -- Inv FBST Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
   -- Inv FBST Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();

					if (request.getParameter("child") == null) {
					
						if (!Common.validateRequired(actionForm.getType())) {

						criteria.put("type", actionForm.getType());
						
						}
						
			        	if (!Common.validateRequired(actionForm.getStatusType())) {

							criteria.put("statusType", actionForm.getStatusType());
							
						}
			        	
			        	criteria.put("brVoid", new Byte(Common.convertBooleanToByte(actionForm.getBrVoid())));
						
					} else {
						
		        		criteria.put("type", actionForm.getType());
		        		criteria.put("statusType", "INCOMING");
		        		criteria.put("brVoid", new Byte((byte)0));
						actionForm.setType(actionForm.getType());
						
					}
					
					if (!Common.validateRequired(actionForm.getApprovalStatus())) {
		        		
		        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
		        		
		        	}

		        	if (!Common.validateRequired(actionForm.getPosted())) {
		        		
		        		criteria.put("posted", actionForm.getPosted());
		        		
		        	}
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getTransferOutNumber())) {
						
						criteria.put("referenceNumber", actionForm.getTransferOutNumber());
						
					}										
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}

					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFBST.getInvBstSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}				
				
				try {
					
					actionForm.clearInvFBSTList();
					
					ArrayList list = ejbFBST.getInvBstByCriteria(actionForm.getCriteria(),
						new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						actionForm.getOrderBy(), false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvModBranchStockTransferDetails mdetails = (InvModBranchStockTransferDetails)i.next();

						InvFindBranchStockTransferList invFBSTList = new InvFindBranchStockTransferList(actionForm,
								mdetails.getBstCode(),	
								mdetails.getBstType(),
								Common.convertSQLDateToString(mdetails.getBstDate()),								
								mdetails.getBstNumber(),
								mdetails.getBstTransferOutNumber(),
								mdetails.getBstTransferOrderNumber(),
								mdetails.getBstBranchFrom(),
								mdetails.getBstBranchTo());
						
						
						actionForm.saveInvFBSTList(invFBSTList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findBranchStockTransfer.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindBranchStockTransferAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
					if (request.getParameter("child") == null) {
			        	
						return(mapping.findForward("invFindBranchStockTransfer"));
			        	
			        } else {
			            
			            actionForm.setSelectedBstCode(request.getParameter("selectedBstCode"));
			            if (request.getParameter("selectedTransferOutNumber") != null 
			            		&& request.getParameter("selectedTransferOutNumberEntered") != null){
							actionForm.setSelectedTransferOutNumber(request.getParameter("selectedTransferOutNumber"));
							actionForm.setSelectedTransferOutNumberEntered(request.getParameter("selectedTransferOutNumberEntered"));
							
							
						}
						else if (request.getParameter("selectedTransferOrderNumber") != null 
								&& request.getParameter("selectedTransferOrderNumberEntered") != null){
							actionForm.setSelectedTransferOrderNumber(request.getParameter("selectedTransferOrderNumber"));
							actionForm.setSelectedTransferOrderNumberEntered(request.getParameter("selectedTransferOrderNumberEntered"));
								
						}
			            
			            /*actionForm.setSelectedTransferOutNumber(request.getParameter("selectedTransferOutNumber"));
			            actionForm.setSelectedTransferOutNumberEntered(request.getParameter("selectedTransferOutNumberSelected"));
			            actionForm.setSelectedTransferOrderNumber(request.getParameter("selectedTransferOrderNumber"));
			            actionForm.setSelectedTransferOrderNumberEntered(request.getParameter("selectedTransferOrderNumberSelected"));
			            */
		        		return mapping.findForward("invFindBranchStockTransferChild");
			        	
			        }
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                       
				
				if (request.getParameter("child") == null) {

					return mapping.findForward("invFindBranchStockTransfer");
                  
                } else {
                  	
                	return mapping.findForward("invFindBranchStockTransferChild");
                  
                }
				
/*******************************************************
 -- Inv FA Close Action --
 *******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 -- Inv FA Open Action --
 *******************************************************/
				
			} else if (request.getParameter("invFBSTList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindBranchStockTransferList invFBSTList =
					actionForm.getInvFBSTByIndex(actionForm.getRowSelected());
				
				String path = null;
				
				if(invFBSTList.getType().equals("OUT")) {
					System.out.println(" OUT");
					path = "/invBranchStockTransferOutEntry.do?forward=1&branchStockTransferCode=" + invFBSTList.getBstCode();
					
				} else if (invFBSTList.getType().equals("IN")) {
					System.out.println(" IN");
				    path = "/invBranchStockTransferInEntry.do?forward=1&branchStockTransferCode=" + invFBSTList.getBstCode();
					
				} else {
					System.out.println(" ORDER");
					path = "/invBranchStockTransferOrderEntry.do?forward=1&branchStockTransferCode=" + invFBSTList.getBstCode();
				}
				
				
				return(new ActionForward(path));
				
/*******************************************************
 -- Inv FA Load Action --
 *******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFBSTList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				
				if (actionForm.getTableType() == null || request.getParameter("findDraft") != null || request.getParameter("findOrderInDraft") != null|| request.getParameter("findInDraft") != null) {
										
					actionForm.setType("OUT");
				    actionForm.setStatusType(Constants.GLOBAL_BLANK);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setTransferOutNumber(null);
					actionForm.setTransferOrderNumber(null);
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setApprovalStatus("DRAFT"); 
					actionForm.setOrderBy(Constants.GLOBAL_BLANK);

					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);					
					actionForm.reset(mapping, request);
					
					if (request.getParameter("findOrderInDraft") != null) {
						
						actionForm.setType("ORDER");
						
					}
					else if (request.getParameter("findInDraft") != null) {
						
						actionForm.setType("IN");
						
					}
					
					HashMap criteria = new HashMap();
					criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

					if(request.getParameter("findDraft") != null || request.getParameter("findOrderInDraft") != null || request.getParameter("findInDraft") != null) {
		            	
		            	return new ActionForward("/invFindBranchStockTransfer.do?goButton=1");
		            	
		            }
					
				} else {
					
					try {
						
						actionForm.clearInvFBSTList();
						
						ArrayList list = ejbFBST.getInvBstByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (list.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							list.remove(list.size() - 1);
							
						}
						
						Iterator i = list.iterator();
						
						while (i.hasNext()) {
							
							InvModBranchStockTransferDetails mdetails = (InvModBranchStockTransferDetails)i.next();

							InvFindBranchStockTransferList invFSTList = new InvFindBranchStockTransferList(actionForm,
									mdetails.getBstCode(),			
									mdetails.getBstType(),
									Common.convertSQLDateToString(mdetails.getBstDate()),									
									mdetails.getBstNumber(),
									mdetails.getBstTransferOutNumber(),
									mdetails.getBstTransferOrderNumber(),
									mdetails.getBstBranchFrom(),
									mdetails.getBstBranchTo());
							
							actionForm.saveInvFBSTList(invFSTList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
		                   	
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findBranchStockTransfer.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindBranchStockTransferAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				if (request.getParameter("child") == null) {
					actionForm.clearInvFBSTList();
					return(mapping.findForward("invFindBranchStockTransfer"));
					
		        } else {
		        						
					try {
						
						actionForm.setSelectedBstCode(request.getParameter("selectedBSTCode"));
						if (request.getParameter("selectedTransferOutNumber") != null 
								&& request.getParameter("selectedTransferOutNumberEntered") != null){
							actionForm.setSelectedTransferOutNumber(request.getParameter("selectedTransferOutNumber"));
							actionForm.setSelectedTransferOutNumberEntered(request.getParameter("selectedTransferOutNumberEntered"));
							actionForm.setSelectedTransferOrderNumber(null);
							actionForm.setSelectedTransferOrderNumberEntered(null);
							actionForm.setType("OUT");	
						}
						else if (request.getParameter("selectedTransferOrderNumber") != null
								&& request.getParameter("selectedTransferOrderNumberEntered") != null){
							actionForm.setSelectedTransferOrderNumber(request.getParameter("selectedTransferOrderNumber"));
							actionForm.setSelectedTransferOrderNumberEntered(request.getParameter("selectedTransferOrderNumberEntered"));
							actionForm.setSelectedTransferOutNumber(null);
							actionForm.setSelectedTransferOutNumberEntered(null);
							actionForm.setType("ORDER");	
						}
						/*
						actionForm.setSelectedTransferOutNumber(request.getParameter("selectedTransferOutNumber"));
						actionForm.setSelectedTransferOutNumberEntered(request.getParameter("selectedTransferOutNumberEntered"));
						
						actionForm.setSelectedTransferOrderNumber(request.getParameter("selectedTransferOrderNumber"));
						actionForm.setSelectedTransferOrderNumberEntered(request.getParameter("selectedTransferOrderNumberEntered"));
					
						actionForm.setType("OUT");	
						*/
						
						System.out.println(request.getParameter("child") + " <=> child?");
						System.out.println(actionForm.getSelectedBstCode() + " <=> " + actionForm.getType());	
						System.out.println(actionForm.getSelectedTransferOutNumber() + " <=> getSelectedTransferOutNumber");
						System.out.println(actionForm.getSelectedTransferOrderNumber() + " <=> getSelectedTransferOrderNum");
						
						actionForm.setLineCount(0);
						/*
						if (actionForm.getType() == "ORDER"){
							actionForm.setStatusType(Constants.GLOBAL_BLANK);
						}else {
							actionForm.setStatusType("INCOMING");
						}*/
		        	    actionForm.setStatusType("INCOMING");
		        	    HashMap criteria = new HashMap();
		        	    criteria.put("type", actionForm.getType());
		        		criteria.put("posted", "YES");
		        		criteria.put("statusType", actionForm.getStatusType());
		        		criteria.put("brVoid", new Byte((byte)0));
		        		actionForm.setCriteria(criteria);						
						
						actionForm.clearInvFBSTList();
						
						ArrayList newList = ejbFBST.getInvBstByCriteria(actionForm.getCriteria(),
								new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(),true,  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
							
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						Iterator i = newList.iterator();
						
						while (i.hasNext()) {
							
							InvModBranchStockTransferDetails mdetails = (InvModBranchStockTransferDetails)i.next();

							InvFindBranchStockTransferList invFSTList = new InvFindBranchStockTransferList(actionForm,
									mdetails.getBstCode(),			
									mdetails.getBstType(),
									Common.convertSQLDateToString(mdetails.getBstDate()),									
									mdetails.getBstNumber(),
									mdetails.getBstTransferOutNumber(),
									mdetails.getBstTransferOrderNumber(),
									mdetails.getBstBranchFrom(), 
									mdetails.getBstBranchTo());
							
							actionForm.saveInvFBSTList(invFSTList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
						    
						    actionForm.setDisableFirstButton(false);
						    actionForm.setDisablePreviousButton(false);
						    
						} else {
						    
						    actionForm.setDisableFirstButton(true);
						    actionForm.setDisablePreviousButton(true);
						    
						}

					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
	        		return mapping.findForward("invFindBranchStockTransferChild");
		        	
		        }
				
				
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
	 System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindBranchStockTransferAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}

}
