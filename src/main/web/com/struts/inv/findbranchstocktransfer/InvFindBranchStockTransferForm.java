package com.struts.inv.findbranchstocktransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvFindBranchStockTransferForm extends ActionForm implements Serializable {
        	
	private String type = null;
	private ArrayList typeList = new ArrayList();
    private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String transferOutNumber = null;
	private String transferOrderNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	private boolean brVoid = false;
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String posted = null;
	private ArrayList postedList = new ArrayList();  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   
	private String statusType = null;
	private ArrayList statusTypeList = new ArrayList();  
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFBSTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private String selectedBstCode = null;
	private String selectedTransferOutNumber = null;
	private String selectedTransferOutNumberEntered = null;
	private String selectedTransferOrderNumber = null;
	private String selectedTransferOrderNumberEntered = null;

	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindBranchStockTransferList getInvFBSTByIndex(int index) {
		
		return((InvFindBranchStockTransferList)invFBSTList.get(index));
		
	}
	
	public Object[] getInvFBSTList() {
		
		return invFBSTList.toArray();
		
	}
	
	public int getInvFBSTListSize() {
		
		return invFBSTList.size();
		
	}
	
	public void saveInvFBSTList(Object newInvFBSTList) {
		
		invFBSTList.add(newInvFBSTList);
		
	}
	
	public void clearInvFBSTList() {
		
		invFBSTList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFBSTList, boolean isEdit) {
		
		this.rowSelected = invFBSTList.indexOf(selectedInvFBSTList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}	
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getTransferOutNumber() {
		
		return transferOutNumber;
		
	}
	
	public void setTransferOutNumber(String transferOutNumber) {
		
		this.transferOutNumber = transferOutNumber;
		
	}
	
	public String getTransferOrderNumber() {
		
		return transferOrderNumber;
		
	}
	
	public void setTransferOrderNumber(String transferOrderNumber) {
		
		this.transferOrderNumber = transferOrderNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
    public boolean getBrVoid() {
		
		return brVoid;
		
	}
	
	public void setBrVoid(boolean brVoid) {
		
		this.brVoid = brVoid;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public ArrayList getApprovalStatusList() {
		
		return approvalStatusList;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}   	
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}

	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}

	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public String getSelectedBstCode() {
		
		return selectedBstCode;
		
	}
	
	public void setSelectedBstCode(String selectedBstCode) {
		
		this.selectedBstCode = selectedBstCode;
		
	}
	
	public String getSelectedTransferOutNumberEntered() {
		
		return selectedTransferOutNumberEntered;
		
	}
	
	public void setSelectedTransferOutNumberEntered(String selectedTransferOutNumberEntered) {
		
		this.selectedTransferOutNumberEntered = selectedTransferOutNumberEntered;
		
	}

	public String getSelectedTransferOutNumber() {
		
		return selectedTransferOutNumber;
		
	}
	
	public void setSelectedTransferOutNumber(String selectedTransferOutNumber) {
		
		this.selectedTransferOutNumber = selectedTransferOutNumber;
		
	}
	
	public String getSelectedTransferOrderNumberEntered() {
		
		return selectedTransferOrderNumberEntered;
		
	}
	
	public void setSelectedTransferOrderNumberEntered(String selectedTransferOrderNumberEntered) {
		
		this.selectedTransferOrderNumberEntered = selectedTransferOrderNumberEntered;
		
	}

	public String getSelectedTransferOrderNumber() {
		
		return selectedTransferOrderNumber;
		
	}
	
	public void setSelectedTransferOrderNumber(String selectedTransferOrderNumber) {
		
		this.selectedTransferOrderNumber = selectedTransferOrderNumber;
		
	}

	public String getStatusType() {
		
		return statusType;
		
	}
	
	public void setStatusType(String statusType) {
		
		this.statusType = statusType;
		
	}
	
	public ArrayList getStatusTypeList() {
		
		return statusTypeList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {			
		
	    typeList.clear();
	    typeList.add("ORDER");
		typeList.add("OUT");
		typeList.add("IN");
		//type = "OUT";
		
		brVoid = false;

	    approvalStatusList.clear();
	    approvalStatusList.add(Constants.GLOBAL_BLANK);
	    approvalStatusList.add("DRAFT");
	    approvalStatusList.add("APPROVED");
	    approvalStatusList.add("N/A");
	    approvalStatusList.add("PENDING");
	         
	    postedList.clear();
	    postedList.add(Constants.GLOBAL_BLANK);
	    postedList.add(Constants.GLOBAL_YES);
	    postedList.add(Constants.GLOBAL_NO);
	    
		showDetailsButton = null;
		hideDetailsButton = null;
		
		statusTypeList.clear();
		statusTypeList.add("INCOMING");
		statusTypeList.add("OUTGOING");
		statusType = "OUTGOING";
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("DOCUMENT NUMBER");
	    	
		}		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		    if(Common.validateRequired(type)){
			
		        errors.add("type", new ActionMessage("findBranchStockTransfer.error.typeRequired"));
			 
		    }
		    
		    if(!Common.validateRequired(transferOutNumber) && !type.equals("IN")) {
		    	
		    	errors.add("transferOutNumber", new ActionMessage("findBranchStockTransfer.error.transferOutNumberInvalid"));
		    	
		    }
		    
		    if(!Common.validateRequired(transferOrderNumber) && !type.equals("OUT")) {
		    	
		    	errors.add("transferOrderNumber", new ActionMessage("findBranchStockTransfer.error.transferOrderNumberInvalid"));
		    	
		    }
		    
		    if (!Common.validateDateFormat(dateFrom)) {
			    	
				errors.add("dateFrom",
					new ActionMessage("findBranchStockTransfer.error.dateFromInvalid"));
			    		
			}         
			    		     					
			if (!Common.validateDateFormat(dateTo)) {
		    	
				errors.add("dateTo",
					new ActionMessage("findBranchStockTransfer.error.dateToInvalid"));
		    		
			}
			
		}
		
		return errors;
		
	}
	
}
