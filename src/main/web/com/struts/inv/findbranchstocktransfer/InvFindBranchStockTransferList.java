package com.struts.inv.findbranchstocktransfer;

import java.io.Serializable;

public class InvFindBranchStockTransferList implements Serializable {
    
    private Integer bstCode = null;	
	private String type = null;
	private String documentNumber = null;
	private String transferOutNumber = null;	
	private String transferOrderNumber = null;
	private String date = null;
	private String branchFrom = null;
	private String branchTo = null;
	
	private String openButton = null;
	
	private InvFindBranchStockTransferForm parentBean;
	
	public InvFindBranchStockTransferList(InvFindBranchStockTransferForm parentBean,
			Integer bstCode,
			String type,
			String date,			
			String documentNumber,
			String transferOutNumber,
			String transferOrderNumber,
			String branchFrom,
			String branchTo) {
		
		this.parentBean = parentBean;
		this.bstCode = bstCode;
		this.type = type;
		this.date = date;		
		this.documentNumber = documentNumber;
		this.transferOutNumber = transferOutNumber;	
		this.transferOrderNumber = transferOrderNumber;
		this.branchFrom = branchFrom;
		this.branchTo = branchTo;
		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getBstCode() {
		
		return bstCode;
		
	}
			
	public String getType() {
	    
	    return type;
	    
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getTransferOutNumber() {
		
		return transferOutNumber;
		
	}	
	
	public String getTransferOrderNumber() {
		
		return transferOrderNumber;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public String getBranchFrom(){
		
		return branchFrom;
		
	}
	
	public String getBranchTo(){
		
		return branchTo;
		
	}
	
}
