package com.struts.inv.findbuildorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindBuildOrderController;
import com.ejb.txn.InvFindBuildOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvBuildOrderDetails;

public final class InvFindBuildOrderAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 	Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindBuildOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindBuildOrderForm actionForm = (InvFindBuildOrderForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_BUILD_ORDER_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindBuildOrder");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
 	Initialize InvFindBuildOrderController EJB
*******************************************************/
			
			InvFindBuildOrderControllerHome homeFBO = null;
			InvFindBuildOrderController ejbFBO = null;
			
			try {
				
				homeFBO = (InvFindBuildOrderControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindBuildOrderControllerEJB", InvFindBuildOrderControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindBuildOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFBO = homeFBO.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindBuildOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  
			
/*******************************************************
	 -- Inv FBO Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindBuildOrder"));
				
/*******************************************************
     -- Inv FBO Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindBuildOrder"));                         
	
/*******************************************************
	-- Inv FBO First Action --
*******************************************************/ 
							
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
	-- Inv FBO Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
	-- Inv FBO Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
	-- Inv FBO Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}

					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFBO.getInvBorSizeByCriteria(actionForm.getCriteria(),
							 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFBOList();
					
					ArrayList list = ejbFBO.getInvBorByCriteria(actionForm.getCriteria(), actionForm.getSoMatched(), 
						new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvBuildOrderDetails mdetails = (InvBuildOrderDetails)i.next();
						
						InvFindBuildOrderList invBORList = new InvFindBuildOrderList(actionForm,
								mdetails.getBorCode(),								
								Common.convertSQLDateToString(mdetails.getBorDate()),
								mdetails.getBorDocumentNumber(),
								mdetails.getBorReferenceNumber(),
								mdetails.getBorDescription(),
								mdetails.getBorSoNumber(),
								new Boolean(actionForm.getSoMatched()));
						
						actionForm.saveInvFBOList(invBORList);
						
					}
					
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findBuildOrder.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindBuildOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invFindBuildOrder");
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                       
				
				return(mapping.findForward("invFindBuildOrder"));
				
/*******************************************************
	-- Inv FBO Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
	-- Inv FBO Open Action --
*******************************************************/
				
			} else if (request.getParameter("invFBOList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindBuildOrderList invFBOList =
					actionForm.getInvFBOByIndex(actionForm.getRowSelected());
				
				
				String type = "";
				if(invFBOList.getSoMatched().booleanValue() == true)
					type="SO MATCHED";
				else
					type="ITEMS";

				System.out.println("type : " + type);
				System.out.println("soMatched : " + invFBOList.getSoMatched());
				System.out.println("soNumber : " + invFBOList.getSoNumber());
				
				String path = "/invBuildOrderEntry.do?forward=1" +
				"&buildOrderCode=" + invFBOList.getBuildOrderCode()+
				"&showSOMatchedLinesDetails=" + invFBOList.getSoMatched() +
				"&type=" + type +
				"&soNumber=" + invFBOList.getSoNumber();

				System.out.println("path : " + path);
				
				return(new ActionForward(path));
				
/*******************************************************
	-- Inv FBO Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFBOList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setOrderBy("DOCUMENT NUMBER");
				
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				} else {
					
					try {
						
						actionForm.clearInvFBOList();
						
						ArrayList list = ejbFBO.getInvBorByCriteria(actionForm.getCriteria(), actionForm.getSoMatched(),
							new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (list.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							list.remove(list.size() - 1);
							
						}
						
						Iterator i = list.iterator();
						
						while (i.hasNext()) {
							
							InvBuildOrderDetails mdetails = (InvBuildOrderDetails)i.next();
							
							InvFindBuildOrderList invBORList = new InvFindBuildOrderList(actionForm,
									mdetails.getBorCode(),								
									Common.convertSQLDateToString(mdetails.getBorDate()),
									mdetails.getBorDocumentNumber(),
									mdetails.getBorReferenceNumber(),
									mdetails.getBorDescription(),
									mdetails.getBorSoNumber(),
									new Boolean(actionForm.getSoMatched()));
							
							actionForm.saveInvFBOList(invBORList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
		                   	
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findBuildOrder.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindBuildOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				return(mapping.findForward("invFindBuildOrder"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
 	System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindBuildOrderAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}