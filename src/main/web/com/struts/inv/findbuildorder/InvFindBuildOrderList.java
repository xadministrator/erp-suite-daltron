package com.struts.inv.findbuildorder;

import java.io.Serializable;

public class InvFindBuildOrderList implements Serializable {
	
	private Integer buildOrderCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
	
	private String soNumber = null;
	private Boolean soMatched = null;
	
	private String openButton = null;
	
	private InvFindBuildOrderForm parentBean;
	
	public InvFindBuildOrderList(InvFindBuildOrderForm parentBean,
			Integer buildOrderCode,
			String date,
			String documentNumber,
			String referenceNumber,
			String description,
			String soNumber,
			Boolean soMatched) {
		
		this.parentBean = parentBean;
		this.buildOrderCode = buildOrderCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.description = description;
		this.soNumber = soNumber;
		this.soMatched = soMatched;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getBuildOrderCode() {
		
		return buildOrderCode;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}

	public String getDescription() {
		
		return description;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public String getSoNumber() {
		
		return soNumber;
	}
	
	public Boolean getSoMatched() {
		
		return soMatched;
	}
	
}