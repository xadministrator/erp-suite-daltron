package com.struts.inv.findbuildorderline;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindBuildOrderLineController;
import com.ejb.txn.InvFindBuildOrderLineControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModBuildOrderLineDetails;

public final class InvFindBuildOrderLineAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvFindBuildOrderLineAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         InvFindBuildOrderLineForm actionForm = (InvFindBuildOrderLineForm)form;
         
         String frParam = null;
         
         frParam = Common.getUserPermission(user, Constants.INV_FIND_BUILD_ORDER_LINE_ID);
         	
         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  
                  return mapping.findForward("invFindBuildOrderLine");
                  	
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvFindBuildOrderLineController EJB
*******************************************************/

         InvFindBuildOrderLineControllerHome homeFBL = null;
         InvFindBuildOrderLineController ejbFBL = null;

         try {

            homeFBL = (InvFindBuildOrderLineControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvFindBuildOrderLineControllerEJB", InvFindBuildOrderLineControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvFindBuildOrderLineAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFBL = homeFBL.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvFindBuildOrderLineAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
/*******************************************************
   Call InvFindBuildOrderLineController EJB
   getGlFcPrecisionUnit
*******************************************************/         
         
         short precisionUnit = 0;
         
			try {
				
				precisionUnit = ejbFBL.getGlFcPrecisionUnit(user.getCmpCode());           
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvFindBuildOrderLineAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}	         
         
/*******************************************************
   -- Inv FBL First Action --
*******************************************************/ 

		if (request.getParameter("firstButton") != null){
			
			actionForm.setLineCount(0);			
			
/*******************************************************
   -- Inv FBL Previous Action --
*******************************************************/ 

		} else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Inv FBL Next Action --
*******************************************************/ 

         } else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Inv FBL Next Previous Action --
*******************************************************/

         if (request.getParameter("nextButton") != null || request.getParameter("previousButton") != null ||
         		request.getParameter("firstButton") != null || request.getParameter("lastButton") != null) {
                         
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	
	        if(request.getParameter("lastButton") != null) {
	        	
	        	int size = ejbFBL.getInvIncompleteBolSize( new Integer(user.getCurrentBranch().getBrCode()),
	        			user.getCmpCode()).intValue();
	        	
	        	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
	        	
	        }
	        		     	            
            try {
            	
            	actionForm.clearInvFBLList();
            	
            	ArrayList list = ejbFBL.getInvIncompleteBol(new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()),
					user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		InvModBuildOrderLineDetails mdetails = (InvModBuildOrderLineDetails)i.next();
      		
            		InvFindBuildOrderLineList invFBLList = new InvFindBuildOrderLineList(actionForm,
            		    mdetails.getBolCode(),
            		    mdetails.getBolIlIiName(),
            		    mdetails.getBolIlLocName(),
                        mdetails.getBolIlIiDescription(),
						Common.convertDoubleToStringMoney(mdetails.getBolQuantityRequired(), precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getBolQuantityAssembled(), precisionUnit),
						mdetails.getBolBorDocumentNumber());
            		    
            		actionForm.saveInvFBLList(invFBLList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findBuildOrderLine.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvFindBuildOrderLineAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
               return mapping.findForward("invFindBuildOrderLine");
               		
            }
                        
            actionForm.reset(mapping, request);
                           	
            return mapping.findForward("invFindBuildOrderLine");
               		
/*******************************************************
   -- Inv FBL Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv FBL Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearInvFBLList();

            ArrayList list = null;
            Iterator i = null;
                        
            actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.setDisableFirstButton(true);
            actionForm.setDisableLastButton(true);
            actionForm.reset(mapping, request);
                                                            
            // save criteria
        
        	try {
        	
	        	actionForm.setLineCount(0);
	        		        		                                       
	        	actionForm.clearInvFBLList();
	        	
	        	ArrayList newList = ejbFBL.getInvIncompleteBol(new Integer(actionForm.getLineCount()), 
	        	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()),
					user.getCmpCode());
	        	
	           actionForm.setDisablePreviousButton(true);
	           actionForm.setDisableFirstButton(true);
	            	
	           // check if next should be disabled
	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  newList.remove(newList.size() - 1);
	           	
	           }
	           
	           i = newList.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		InvModBuildOrderLineDetails mdetails = (InvModBuildOrderLineDetails)i.next();
	  		
	        		InvFindBuildOrderLineList invFBLList = new InvFindBuildOrderLineList(actionForm,
	            		    mdetails.getBolCode(),
	            		    mdetails.getBolIlIiName(),
	            		    mdetails.getBolIlLocName(),
	                        mdetails.getBolIlIiDescription(),
							Common.convertDoubleToStringMoney(mdetails.getBolQuantityRequired(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getBolQuantityAssembled(), precisionUnit),
							mdetails.getBolBorDocumentNumber());
	        		    
	        		actionForm.saveInvFBLList(invFBLList);
	        		
	        	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisableLastButton(true);
               
               if(actionForm.getLineCount() > 0) {
               	
               		actionForm.setDisableFirstButton(false);
               		actionForm.setDisablePreviousButton(false);
               		
               } else {
               	
               		actionForm.setDisableFirstButton(true);
               		actionForm.setDisablePreviousButton(true);
               		
               }
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findBuildOrderLine.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvFindBuildOrderLineAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }    
            
            	actionForm.setSelectedItemName(request.getParameter("selectedItemName"));
            	actionForm.setSelectedItemEntered(request.getParameter("selectedItemEntered"));
            	return(mapping.findForward("invFindBuildOrderLine"));                    
            
            

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvFindBuildOrderLineAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}