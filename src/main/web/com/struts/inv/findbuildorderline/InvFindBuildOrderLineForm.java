package com.struts.inv.findbuildorderline;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class InvFindBuildOrderLineForm extends ActionForm implements Serializable {
	  
	private String selectedItemName = null;
	private String selectedItemEntered = null;
	
	private String pageState = new String();
	private ArrayList invFBLList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindBuildOrderLineList getInvFBLByIndex(int index) {
		
		return((InvFindBuildOrderLineList)invFBLList.get(index));
		
	}
	
	public Object[] getInvFBLList() {
		
		return invFBLList.toArray();
		
	}
	
	public int getInvFBLListSize() {
		
		return invFBLList.size();
		
	}
	
	public void saveInvFBLList(Object newInvFBLList) {
		
		invFBLList.add(newInvFBLList);
		
	}
	
	public void clearInvFBLList() {
		
		invFBLList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFBLList, boolean isEdit) {
		
		this.rowSelected = invFBLList.indexOf(selectedInvFBLList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
		
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	  	
	public String getSelectedItemName() {
		
		return selectedItemName;
		
	}
	
	public void setSelectedItemName(String selectedItemName) {
		
		this.selectedItemName = selectedItemName;
		
	}
		
	public String getSelectedItemEntered() {
		
		return selectedItemEntered;
		
	}
	
	public void setSelectedItemEntered(String selectedItemEntered) {
		
		this.selectedItemEntered = selectedItemEntered;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		     			
		previousButton = null;
		nextButton = null;

	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		return errors;
		
	}
	
}