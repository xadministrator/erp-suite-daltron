package com.struts.inv.findbuildorderline;

import java.io.Serializable;

public class InvFindBuildOrderLineList implements Serializable {
	
	private Integer buildOrderLineCode = null;
	private String itemName = null;
	private String location = null;
	private String description = null;
	private String quantityRequired = null;
	private String quantityAssembled = null;
	private String documentNumber = null;
	
	private String openButton = null;
		
	private InvFindBuildOrderLineForm parentBean;
	
	public InvFindBuildOrderLineList(InvFindBuildOrderLineForm parentBean,
			Integer buildOrderLineCode,    
			String itemName, 
			String location,
			String description,
			String quantityRequired,
			String quantityAssembled,
			String documentNumber) {
		
		this.parentBean = parentBean;
		this.buildOrderLineCode = buildOrderLineCode;
		this.itemName = itemName;
		this.location = location;
		this.description = description;
		this.quantityRequired = quantityRequired;
		this.quantityAssembled = quantityAssembled;
		this.documentNumber = documentNumber;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getBuildOrderLineCode() {
		
		return buildOrderLineCode;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getQuantityRequired() {
		
		return quantityRequired;
		
	}
	
	public String getQuantityAssembled() {
		
		return quantityAssembled;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
		
}