package com.struts.inv.findbuildunbuildassembly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindBuildUnbuildAssemblyController;
import com.ejb.txn.InvFindBuildUnbuildAssemblyControllerHome;
import com.struts.ad.responsibility.AdBranchResponsibilityList;
import com.struts.ad.responsibility.AdResponsibilityList;
import com.struts.ap.findpurchaseorder.ApFindPurchaseOrderList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.ApModPurchaseOrderDetails;
import com.util.InvBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyDetails;

public final class InvFindBuildUnbuildAssemblyAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindBuildUnbuildAssemblyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindBuildUnbuildAssemblyForm actionForm = (InvFindBuildUnbuildAssemblyForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindBuildUnbuildAssembly");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}

/*******************************************************
   Initialize InvFindBuildUnbuildAssemblyController EJB
*******************************************************/
			
			InvFindBuildUnbuildAssemblyControllerHome homeFBA = null;
			InvFindBuildUnbuildAssemblyController ejbFBA = null;
			
			try {
				
				homeFBA = (InvFindBuildUnbuildAssemblyControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindBuildUnbuildAssemblyControllerEJB", InvFindBuildUnbuildAssemblyControllerHome.class);
								
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindBuildUnbuildAssemblyAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFBA = homeFBA.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindBuildUnbuildAssemblyAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			ActionMessages messages = new ActionMessages();
			
/*******************************************************
     Call ArFindInvoiceController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArInvoiceBatch
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
  *******************************************************/

     short quantityPrecisionUnit = 0;
     boolean enableInvoiceBatch = false;
     boolean enableShift = false;
     boolean useCustomerPulldown = true;
     Integer buaSize = null;
     
     try { 
     	
    	 quantityPrecisionUnit = ejbFBA.getInvGpQuantityPrecisionUnit(user.getCmpCode());
        enableInvoiceBatch = Common.convertByteToBoolean(ejbFBA.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
        actionForm.setShowBatchName(enableInvoiceBatch);
        useCustomerPulldown = Common.convertByteToBoolean(ejbFBA.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
        actionForm.setUseCustomerPulldown(useCustomerPulldown);	                  
     } catch (EJBException ex) {
     	
        if (log.isInfoEnabled()) {
        	
           log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
           " session: " + session.getId());
           
        }
        
        return(mapping.findForward("cmnErrorPage"));
     }	

			
/*******************************************************
   -- Inv FBA Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindBuildUnbuildAssembly"));
				
/*******************************************************
   -- Inv FBA Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindBuildUnbuildAssembly"));                         
				
/*******************************************************
   -- Inv FBA First Action --
*******************************************************/ 
								
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);				
				
/*******************************************************
   -- Inv FBA Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
   -- Inv FBA Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
   -- Inv FBA Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (actionForm.getReceiving()) {	        	
						
						criteria.put("receiving", new Byte((byte)1));
						
					}
					
					if (!actionForm.getReceiving()) {
						
						criteria.put("receiving", new Byte((byte)0));
						
					}
					
					if (!Common.validateRequired(actionForm.getBatchName())) {
		        		
		        		criteria.put("batchName", actionForm.getBatchName());
		        		
		        	}
		        	
		        
		
		        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
		        		
		        		criteria.put("customerCode", actionForm.getCustomerCode());
		        		
		        	}
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
					if (actionForm.getVoid()) {	        	
   		        		
						criteria.put("buaVoid", new Byte((byte)1));

					}

					if (!actionForm.getVoid()) {

						criteria.put("buaVoid", new Byte((byte)0));

					}
					
		        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
		        		
		        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
		        		
		        	}

		        	if (!Common.validateRequired(actionForm.getPosted())) {
		        		
		        		criteria.put("posted", actionForm.getPosted());
		        		
		        	}
		        	
		        	
		        	if(request.getParameter("child") == null) {
		        		
		        		System.out.println("CHILD IS NULL TRUE");
						
						if (!Common.validateRequired(actionForm.getApprovalStatus())) {
							
							criteria.put("approvalStatus", actionForm.getApprovalStatus());
							
						}
						
						if (!Common.validateRequired(actionForm.getPosted())) {
							
							criteria.put("posted", actionForm.getPosted());
							
						}
						
						if (actionForm.getReceiving()) {	        	
							
							criteria.put("receiving", new Byte((byte)1));
							
						}
						
						if (!actionForm.getReceiving()) {
							
							criteria.put("receiving", new Byte((byte)0));
							
						}
						
					
						
					} else {
						
						System.out.println("CHILD IS NULL FALSE");
						
						criteria.put("receiving", new Byte((byte)0));
						criteria.put("posted", "YES");
						
					}	
					
					// save criteria
		        	
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				} else if(request.getParameter("forward") != null){
					System.out.println("FORWARD-----------------------------------------"+request.getParameter("forward"));					  					    					    		                			              					        
					
					
					HashMap criteria = new HashMap();
					
        		   criteria.put("posted", Constants.GLOBAL_YES);
        		   actionForm.setPosted("YES");
        		   criteria.put("receiving", new Byte((byte)0));
        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);

	        	   actionForm.setLineCount(0);
	        	   actionForm.setCriteria(criteria);
	        	   actionForm.setOrderBy("DOCUMENT NUMBER");
	        	  
	            	
				
				
				
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFBA.getInvBuaSizeByCriteria(actionForm.getCriteria(),
							 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFBAList();
					System.out.println("getInvBuaByCriteria1");
					ArrayList list = ejbFBA.getInvBuaByCriteria(actionForm.getCriteria(),
						new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						actionForm.getOrderBy(),actionForm.getSelectedBoNumber() != null ? true : false,  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvModBuildUnbuildAssemblyDetails details = (InvModBuildUnbuildAssemblyDetails)i.next();
						
						InvFindBuildUnbuildAssemblyList invFBAList = new InvFindBuildUnbuildAssemblyList(actionForm,
								details.getBuaCode(),
								Common.convertByteToBoolean(details.getBuaReceiving()),
								details.getBuaDocumentNumber(),
								details.getBuaCstCustomerCode(),
								details.getBuaReferenceNumber(),
								details.getBuaDescription(),
								Common.convertSQLDateToString(details.getBuaDate()),
								Common.convertDoubleToStringMoney(details.getBuaQuantity(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(details.getBuaReceived(), quantityPrecisionUnit),
								Common.convertByteToBoolean(details.getBuaVoid()));
						
						actionForm.saveInvFBAList(invFBAList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findBuildUnbuildAssembly.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindBuildUnbuildAssemblyAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					if (request.getParameter("child") == null) {
						actionForm.setSelectedBoNumber(null);
						return(mapping.findForward("invFindBuildUnbuildAssembly"));
			        	
			        } else {
			            
			            actionForm.setSelectedBoNumber(request.getParameter("selectedBoNumber"));
			        	return mapping.findForward("invFindBuildUnbuildAssemblyChild");
			        	
			        }
					
			
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                       
				
				return(mapping.findForward("invFindBuildUnbuildAssembly"));
				
/*******************************************************
   -- Inv FBA Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
				
				
				
/*******************************************************
   -- Gl FRG Copy Action --
*******************************************************/      

          } else if (request.getParameter("invFBAList[" +
             actionForm.getRowSelected() + "].copyButton") != null){
             	 
     	 	     try {
     	 	    	 System.out.println("COPYYYYYYYYYYYYYYYYY");
     	 	    	InvFindBuildUnbuildAssemblyList invFBAList =
     	 	    		 actionForm.getInvFBAByIndex(actionForm.getRowSelected());
     	 	    	InvBuildUnbuildAssemblyDetails details = new InvBuildUnbuildAssemblyDetails(); 
     	 	    	
     	 	    	details.setBuaCode(invFBAList.getBuildUnbuildAssemblyCode());
     	 	    	
     	 	    	System.out.println("invFBAList.getBuildUnbuildAssemblyCode()="+invFBAList.getBuildUnbuildAssemblyCode());
     	 	    	System.out.println("actionForm.getNumberOfCopies()="+actionForm.getNumberOfCopies());
     	 	    	
     	 	    	
     	 	    	buaSize = ejbFBA.copyInvBuaEntry( details, actionForm.getNumberOfCopies(), user.getCurrentBranch().getBrCode(),user.getCmpCode());
     	        	 
     	                 
      	         } catch (EJBException ex) {
     	 	        	 
      	        	 if (log.isInfoEnabled()) {
     	 	        		 
      	        		 log.info("EJBException caught in InvFindBuildUnbuildAssemblyAction.execute(): " + ex.getMessage() +
      	        				 " session: " + session.getId());
     	               
      	        		 return mapping.findForward("cmnErrorPage");
     	 	        		 
      	        	 }
     	 	        	 
      	         }
				   				
			
/*******************************************************
   -- Inv FBA Open Action --
*******************************************************/
				
			} else if (request.getParameter("invFBAList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindBuildUnbuildAssemblyList invFBAList =
					actionForm.getInvFBAByIndex(actionForm.getRowSelected());
				
				
					String path = null;
				
					if (invFBAList.getReceiving() == false) {
								    				
						path = "/invBuildUnbuildAssemblyOrderEntry.do?forward=1" +
						"&buildUnbuildAssemblyCode=" + invFBAList.getBuildUnbuildAssemblyCode();
						
					} else {
						
						path = "/invBuildUnbuildAssemblyEntry.do?forward=1" +
						"&buildUnbuildAssemblyCode=" + invFBAList.getBuildUnbuildAssemblyCode();
						
					}
					
					
					
					
				
				return(new ActionForward(path));
				
/*******************************************************
 -- Inv FBA Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFBAList();
		            ArrayList list = null;
		            Iterator i = null;
		            
		            try {
		            	
		            	if(actionForm.getUseCustomerPulldown()) {
		            	    
		                	actionForm.clearCustomerCodeList();           	
		                	
		            		list = ejbFBA.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		            		
		            		if (list == null || list.size() == 0) {
		            			
		            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
		            			
		            		} else {
		            			
		            			i = list.iterator();
		            			
		            			while (i.hasNext()) {
		            				
		            				actionForm.setCustomerCodeList((String)i.next());
		            				
		            			}
		            			
		            		}
		            	
		            	}

		            	
		            	
		            	actionForm.clearBatchNameList();           	
		            	
		            	list = ejbFBA.getInvOpenBbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		            	
		            	if (list == null || list.size() == 0) {
		            		
		            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
		            		
		            	} else {
		            		           		            		
		            		i = list.iterator();
		            		
		            		while (i.hasNext()) {
		            			
		            		    actionForm.setBatchNameList((String)i.next());
		            			
		            		}
		            		
		            	}
		            	
		            	
		            	
		            } catch (EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }

		            } 
		            
				
				if (!errors.isEmpty()) {

		               saveErrors(request, new ActionMessages(errors));
		               
		            } else {

		               if (request.getParameter("goButton") != null) {

		                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
  
		               } else if (request.getParameter("invFBAList[" +
	                           actionForm.getRowSelected() + "].copyButton") != null)   {
		            	   
		            	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
		            	   
		            	   if (request.getParameter("invFBAList[" +
		            	   actionForm.getRowSelected() + "].copyButton") != null)   {
		            			
		            			messages.add(ActionMessages.GLOBAL_MESSAGE,	new ActionMessage("findBuildUnbuildAssembly.prompt.buaGenerated", buaSize));
		                		saveMessages(request, messages);
		            			
		            		}
		            		
		               }
		            }
				
				
				if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {
					System.out.println("--------------->="+ request.getParameter("receiving"));
					actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
					
					actionForm.setReceiving(false);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setApprovalStatus("DRAFT");   
					
					actionForm.setOrderBy(Constants.GLOBAL_BLANK);
					
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					HashMap criteria = new HashMap();
					criteria.put("buaVoid", new Byte((byte)0));
					criteria.put("receiving", new Byte((byte)0));
					criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
					
					actionForm.setCriteria(criteria);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
					if(request.getParameter("findDraft") != null) {
		            	
						return new ActionForward("/invFindBuildUnbuildAssembly.do?goButton=1");
		            	
		            }
					
				} else {
					System.out.println("--------------->2");
					try {
						
						HashMap c = actionForm.getCriteria();
		            	
		            	if(c.containsKey("receiving")) {
		            		
		            		Byte b = (Byte)c.get("receiving");
		            		
		            		actionForm.setReceiving(Common.convertByteToBoolean(b.byteValue()));
		            		
		            	}
		            	
		            	
		            	
		            	if(c.containsKey("buaVoid")) {
		            		
		            		Byte b = (Byte)c.get("buaVoid");
		            		
		            		actionForm.setVoid(Common.convertByteToBoolean(b.byteValue()));
		            		
		            	}
						

						actionForm.clearInvFBAList();
						System.out.println("getInvBuaByCriteria2");
						ArrayList newlist = ejbFBA.getInvBuaByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(),actionForm.getSelectedBoNumber() != null ? true : false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (list.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newlist.remove(newlist);
							
						}
						
						Iterator x = newlist.iterator();
						
						while (x.hasNext()) {
							
							InvModBuildUnbuildAssemblyDetails details = (InvModBuildUnbuildAssemblyDetails)x.next();
							
							InvFindBuildUnbuildAssemblyList invFBAList = new InvFindBuildUnbuildAssemblyList(actionForm,
									details.getBuaCode(),
									Common.convertByteToBoolean(details.getBuaReceiving()),
									details.getBuaDocumentNumber(),
									details.getBuaCstCustomerCode(),
									details.getBuaReferenceNumber(),
									details.getBuaDescription(),
									Common.convertSQLDateToString(details.getBuaDate()),
									Common.convertDoubleToStringMoney(details.getBuaQuantity(), quantityPrecisionUnit),
									Common.convertDoubleToStringMoney(details.getBuaReceived(), quantityPrecisionUnit),
									Common.convertByteToBoolean(details.getBuaVoid()));
							
							actionForm.saveInvFBAList(invFBAList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
		                   	
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findBuildUnbuildAssembly.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindBuildUnbuildAssemblyAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
					
					
				}
				
				if (request.getParameter("child") == null) {
		        	
					return(mapping.findForward("invFindBuildUnbuildAssembly"));
		        	
		        } else {
				
				System.out.println(">>>>>>>>>>>>>>>>>>>>");

		        	
					try {
						
						actionForm.setLineCount(0);
		        		
		        		HashMap criteria = new HashMap();
		        		criteria.put(new String("receiving"), new Byte((byte)0));
		        		criteria.put(new String("buaVoid"), new Byte((byte)0));
		            	criteria.put(new String("posted"), "YES");
		            			            	
		        		actionForm.setCriteria(criteria);						
						
						actionForm.clearInvFBAList();
						System.out.println("getInvBuaByCriteria3");
						System.out.println("actionForm.getSelectedBoNumber()="+actionForm.getSelectedBoNumber());
						ArrayList newList = ejbFBA.getInvBuaByCriteria(actionForm.getCriteria(),
								new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(),actionForm.getSelectedBoNumber() != null ? true : false,  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
							
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						i = newList.iterator();
						
						while (i.hasNext()) {
							

							InvModBuildUnbuildAssemblyDetails mdetails = (InvModBuildUnbuildAssemblyDetails)i.next();
							
							InvFindBuildUnbuildAssemblyList invFBOList  = new InvFindBuildUnbuildAssemblyList(actionForm,
									mdetails.getBuaCode(),
									Common.convertByteToBoolean(mdetails.getBuaReceiving()),
									mdetails.getBuaDocumentNumber(),
									mdetails.getBuaCstCustomerCode(),
									mdetails.getBuaReferenceNumber(),
									mdetails.getBuaDescription(),
									Common.convertSQLDateToString(mdetails.getBuaDate()),
									Common.convertDoubleToStringMoney(mdetails.getBuaQuantity(), quantityPrecisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getBuaReceived(), quantityPrecisionUnit),
									Common.convertByteToBoolean(mdetails.getBuaVoid()));
							
							
							actionForm.saveInvFBAList(invFBOList);
						
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
						    
						    actionForm.setDisableFirstButton(false);
						    actionForm.setDisablePreviousButton(false);
						    
						} else {
						    
						    actionForm.setDisableFirstButton(true);
						    actionForm.setDisablePreviousButton(true);
						    
						}

					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
				
					System.out.println("selectedBoNumber="+request.getParameter("selectedBoNumber"));
				actionForm.setSelectedBoNumber(request.getParameter("selectedBoNumber"));
				
				return(mapping.findForward("invFindBuildUnbuildAssemblyChild"));
		        }
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			/*******************************************************
			 System Failed: Forward to error page 
			 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindBuildUnbuildAssemblyAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}