package com.struts.inv.findbuildunbuildassembly;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvFindBuildUnbuildAssemblyForm extends ActionForm implements Serializable {
	
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	private boolean showBatchName = false;
	private String customerCode = null;
	private ArrayList customerCodeList = new ArrayList();
	private boolean receiving = false;
	private boolean buildAssemblyVoid = false;
	
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;
	private String selectedBoNumber = null;
	private Integer numberOfCopies = 0;
	private String dateFrom = null;
	private String dateTo = null;
	private boolean buaVoid = false;
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String posted = null;
	private ArrayList postedList = new ArrayList();  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFBAList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	private boolean useCustomerPulldown = true;

	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindBuildUnbuildAssemblyList getInvFBAByIndex(int index) {
		
		return((InvFindBuildUnbuildAssemblyList)invFBAList.get(index));
		
	}
	
	public Object[] getInvFBAList() {
		
		return invFBAList.toArray();
		
	}
	
	public int getInvFBAListSize() {
		
		return invFBAList.size();
		
	}
	
	public void saveInvFBAList(Object newInvFBAList) {
		
		invFBAList.add(newInvFBAList);
		
	}
	
	public void clearInvFBAList() {
		
		invFBAList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFBAList, boolean isEdit) {
		
		this.rowSelected = invFBAList.indexOf(selectedInvFBAList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
public String getBatchName() {
		
		return batchName;
		
	}
	
	public void setBatchName(String batchName) {
		
		this.batchName = batchName;
		
	}
	
	public ArrayList getBatchNameList() {
		
		return batchNameList;
		
	}
	
	public void setBatchNameList(String batchName) {
		
		batchNameList.add(batchName);
		
	}
	
	public void clearBatchNameList() {   	 
		
		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getShowBatchName() {
	   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
	
	public String getCustomerCode() {

	      return customerCode;

	   }

	   public void setCustomerCode(String customerCode) {

	      this.customerCode = customerCode;

	   }

	   public boolean getReceiving() {
			
			return receiving;
			
		}
	   
	   public void setReceiving(boolean receiving) {

		      this.receiving = receiving;

		   }
	   
	   public boolean getBuildAssemblyVoid() {
			
			return buildAssemblyVoid;
			
		}
	   
	   public void setBuildAssemblyVoid(boolean buildAssemblyVoid) {

		      this.buildAssemblyVoid = buildAssemblyVoid;

		   }
	   
	   public ArrayList getCustomerCodeList() {

	      return customerCodeList;

	   }

	   public void setCustomerCodeList(String customerCode) {

	      customerCodeList.add(customerCode);

	   }

	   public void clearCustomerCodeList() {

	      customerCodeList.clear();
	      customerCodeList.add(Constants.GLOBAL_BLANK);
	      
	   }
	
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getSelectedBoNumber() {
		
		return selectedBoNumber;
		
	}
	
	public void setSelectedBoNumber(String selectedBoNumber) {
		
		this.selectedBoNumber = selectedBoNumber;
		
	}
	
	public Integer getNumberOfCopies() {
		
		return numberOfCopies;
		
	}
	
	public void setNumberOfCopies(Integer numberOfCopies) {
		
		this.numberOfCopies = numberOfCopies;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public ArrayList getApprovalStatusList() {
		
		return approvalStatusList;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}   	
	
	public boolean getVoid() {
		
		return buaVoid;
		
	}
	
	public void setVoid(boolean buaVoid) {
		
		this.buaVoid = buaVoid;
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public boolean getUseCustomerPulldown() {
		
		return useCustomerPulldown;
		
	}
	
	public void setUseCustomerPulldown(boolean useCustomerPulldown) {
		
		this.useCustomerPulldown = useCustomerPulldown;
		
	}
	
	
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
	    approvalStatusList.clear();
	    approvalStatusList.add(Constants.GLOBAL_BLANK);
	    approvalStatusList.add("DRAFT");
	    approvalStatusList.add("APPROVED");
	    approvalStatusList.add("N/A");
	    approvalStatusList.add("PENDING");
	    
	    numberOfCopies = 0;
	    
	    postedList.clear();
	    postedList.add(Constants.GLOBAL_BLANK);
	    postedList.add(Constants.GLOBAL_YES);
	    postedList.add(Constants.GLOBAL_NO);
	    
	//    receiving = false;
	    buildAssemblyVoid = false;
	    buaVoid = false;
		showDetailsButton = null;
		hideDetailsButton = null;
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("REFERENCEf NUMBER");
	    	orderByList.add("DOCUMENT NUMBER");
		      
		  }		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(useCustomerPulldown) {
	      		if (!Common.validateStringExists(customerCodeList, customerCode)) {
	      			
	      			errors.add("customerCode",
	      					new ActionMessage("findInvoice.error.customerCodeInvalid"));
	      			
	      		}
	      	}
			
			
			if (!Common.validateDateFormat(dateFrom)) {
		    	
				errors.add("dateFrom",
					new ActionMessage("findBuildUnbuildAssembly.error.dateFromInvalid"));
		    		
			}         
		    		     					
			if (!Common.validateDateFormat(dateTo)) {
	    	
				errors.add("dateTo",
					new ActionMessage("findBuildUnbuildAssembly.error.dateToInvalid"));
	    		
			}			
			
		}
		
		return errors;
		
	}
}