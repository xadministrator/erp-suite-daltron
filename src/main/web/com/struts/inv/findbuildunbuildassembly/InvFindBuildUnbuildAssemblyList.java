package com.struts.inv.findbuildunbuildassembly;

import java.io.Serializable;

public class InvFindBuildUnbuildAssemblyList implements Serializable {
	
	private Integer buildUnbuildAssemblyCode = null;
	private boolean receiving = false;
	private String documentNumber = null;
	private String customerCode = null;
	private String referenceNumber = null;
	private String description = null;
	private String date = null;
	private String quantity = null;
	private String received = null;
	private boolean buaVoid = false;
	
	private String openButton = null;
	private String copyButton = null;
	
	private InvFindBuildUnbuildAssemblyForm parentBean;
	
	public InvFindBuildUnbuildAssemblyList(InvFindBuildUnbuildAssemblyForm parentBean,
			Integer buildUnbuildAssemblyCode,
			boolean receiving,
			String documentNumber,
			String customerCode,
			String referenceNumber,
			String description,
			String date,
			String quantity, 
			String received,
			boolean buaVoid) {
		
		this.parentBean = parentBean;
		this.buildUnbuildAssemblyCode = buildUnbuildAssemblyCode;
		this.receiving = receiving;
		this.documentNumber = documentNumber;
		this.customerCode = customerCode;
		this.referenceNumber = referenceNumber;
		this.description = description;
		this.date = date;
		this.quantity = quantity;
		this.received = received;
		this.buaVoid = buaVoid;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public void setCopyButton(String copyButton) {

	   parentBean.setRowSelected(this, false);

   }
	
	public Integer getBuildUnbuildAssemblyCode() {
		
		return buildUnbuildAssemblyCode;
		
	}
	
	public boolean getReceiving() {
		
		return receiving;
		
	}
	
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getCustomerCode() {
		
		return customerCode;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}	
	
	public String getReceived() {
		
		return received;
		
	}
	
	public void setReceived(String received) {
		
		this.received = received;
		
	}
	
	public boolean getVoid() {
		
		return buaVoid;
		
	}
	

}