package com.struts.inv.findbuildunbuildassemblybatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindBuildUnbuildAssemblyBatchController;
import com.ejb.txn.InvFindBuildUnbuildAssemblyBatchControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvBuildUnbuildAssemblyBatchDetails;


public final class InvFindBuildUnbuildAssemblyBatchAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvFindBuildUnbuildAssemblyBatchAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         InvFindBuildUnbuildAssemblyBatchForm actionForm = (InvFindBuildUnbuildAssemblyBatchForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invFindBuildUnbuildAssemblyBatch");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize InvFindBuildUnbuildAssemblyBatchController EJB
*******************************************************/
         
         InvFindBuildUnbuildAssemblyBatchControllerHome homeFIB = null;
         InvFindBuildUnbuildAssemblyBatchController ejbFIB = null;

         try {
          
            homeFIB = (InvFindBuildUnbuildAssemblyBatchControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvFindBuildUnbuildAssemblyBatchControllerEJB", InvFindBuildUnbuildAssemblyBatchControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvFindBuildUnbuildAssemblyBatchAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFIB = homeFIB.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvFindBuildUnbuildAssemblyBatchAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Ar FIB Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("invFindBuildUnbuildAssemblyBatch"));
	     
/*******************************************************
   -- Ar FIB Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("invFindBuildUnbuildAssemblyBatch"));    
	        
/*******************************************************
  -- Ar FIB First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);	        

/*******************************************************
   -- Ar FIB Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar FIB Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Ar FIB Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getStatus())) {
	        		
	        		criteria.put("status", actionForm.getStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateCreated())) {
	        		
	        		criteria.put("dateCreated", Common.convertStringToSQLDate(actionForm.getDateCreated()));
	        		
	        	}
	        		        	        		        	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFIB.getArBbSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearArFIBList();
            	
            	ArrayList list = ejbFIB.getArBbByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		InvBuildUnbuildAssemblyBatchDetails details = (InvBuildUnbuildAssemblyBatchDetails)i.next();
            		
            		InvFindBuildUnbuildAssemblyBatchList iblist = new InvFindBuildUnbuildAssemblyBatchList(
            			actionForm, 
            			details.getBbCode(),
            			details.getBbName(), 
            			details.getBbDescription(),
            			details.getBbStatus(),
            			Common.convertSQLDateToString(details.getBbDateCreated()),
            			details.getBbCreatedBy());
            			
            	   actionForm.saveArFIBList(iblist);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findBuildUnbuildAssemblyBatch.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvFindBuildUnbuildAssemblyBatchAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invFindBuildUnbuildAssemblyBatch");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("invFindBuildUnbuildAssemblyBatch")); 

/*******************************************************
   -- Ar FIB Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FIB Open Action --
*******************************************************/

         } else if (request.getParameter("arFIBList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             InvFindBuildUnbuildAssemblyBatchList arFIBList =
                actionForm.getArFIBByIndex(actionForm.getRowSelected());

  	         String path = "/invBuildUnbuildAssemblyBatchEntry.do?forward=1" +
			     "&buildUnbuildAssemblyBatchCode=" + arFIBList.getBuildUnbuildAssemblyBatchCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Ar FIB Load Action --
*******************************************************/

         }
         if (frParam != null) {
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invFindBuildUnbuildAssemblyBatch");

            }
            
            actionForm.clearArFIBList();
                        
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            
            
	        if (actionForm.getTableType() == null) {
	           actionForm.setBatchName(null);
	           actionForm.setStatus(Constants.GLOBAL_BLANK);
	           actionForm.setDateCreated(null);
	           actionForm.setOrderBy("BATCH NAME");
	           
	           actionForm.setLineCount(0);
	           actionForm.setDisableNextButton(true);
	           actionForm.setDisablePreviousButton(true);
	           actionForm.setDisableFirstButton(true);
	           actionForm.setDisableLastButton(true);
	           actionForm.reset(mapping, request);
	           
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	try {
                	
                	actionForm.clearArFIBList();
                	
                	ArrayList list = ejbFIB.getArBbByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	  list.remove(list.size() - 1);
    	           	
    	           }
                	
                	Iterator i = list.iterator();
                	
                	while (i.hasNext()) {
                		
                		InvBuildUnbuildAssemblyBatchDetails details = (InvBuildUnbuildAssemblyBatchDetails)i.next();
                		
                		InvFindBuildUnbuildAssemblyBatchList iblist = new InvFindBuildUnbuildAssemblyBatchList(
                			actionForm, 
                			details.getBbCode(),
                			details.getBbName(), 
                			details.getBbDescription(),
                			details.getBbStatus(),
                			Common.convertSQLDateToString(details.getBbDateCreated()),
                			details.getBbCreatedBy());
                			
                	   actionForm.saveArFIBList(iblist);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findBuildUnbuildAssemblyBatch.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in InvFindBuildUnbuildAssemblyBatchAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
            return(mapping.findForward("invFindBuildUnbuildAssemblyBatch"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvFindBuildUnbuildAssemblyBatchAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}