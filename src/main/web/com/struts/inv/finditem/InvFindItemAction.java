package com.struts.inv.finditem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindItemController;
import com.ejb.txn.InvFindItemControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModItemDetails;

public final class InvFindItemAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvFindItemAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         InvFindItemForm actionForm = (InvFindItemForm)form;

         String frParam = null;
         Integer brCode = null;

         if (request.getParameter("child") == null && request.getParameter("assembly") == null &&
         		request.getParameter("childAssembly") == null && request.getParameter("childRep") == null && request.getParameter("itemSchedule") == null) {

            frParam = Common.getUserPermission(user, Constants.INV_FIND_ITEM_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         	if (request.getParameter("childRep") != null){

         		brCode = new Integer(request.getParameter("selectedBranch"));

         	} else if (request.getParameter("childAssembly") == null){

         		brCode = new Integer(user.getCurrentBranch().getBrCode());

         	}

         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  if (request.getParameter("child") != null || request.getParameter("childRep") != null || request.getParameter("itemSchedule") != null){

                  	return mapping.findForward("invFindItemChild");

                  } else if (request.getParameter("childAssembly") != null) {

                  	return mapping.findForward("invFindItemAssemblyChild");

                  } else if (request.getParameter("assembly") != null) {

                  	return mapping.findForward("invFindAssemblyChild");

                  } else {

                  	return mapping.findForward("invFindItem");

                  }

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvFindItemController EJB
*******************************************************/

         InvFindItemControllerHome homeFI = null;
         InvFindItemController ejbFI = null;

         try {

            homeFI = (InvFindItemControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvFindItemControllerEJB", InvFindItemControllerHome.class);


         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvFindItemAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFI = homeFI.create();

         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvFindItemAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   Call InvFindItemController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;

			try {

				precisionUnit = ejbFI.getGlFcPrecisionUnit(user.getCmpCode());

			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}

/*******************************************************
   -- Inv FI Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_DETAILED);

            if (request.getParameter("child") != null || request.getParameter("childRep") != null || request.getParameter("itemSchedule") != null){

              	return mapping.findForward("invFindItemChild");

              } else if (request.getParameter("childAssembly") != null) {

              	return mapping.findForward("invFindItemAssemblyChild");

              } else if (request.getParameter("assembly") != null) {

              	return mapping.findForward("invFindAssemblyChild");

              } else {

              	return mapping.findForward("invFindItem");

              }

/*******************************************************
   -- Inv FI Hide Details Action --
*******************************************************/

	     } else if (request.getParameter("hideDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            if (request.getParameter("child") != null || request.getParameter("childRep") != null || request.getParameter("itemSchedule") != null){

              	return mapping.findForward("invFindItemChild");

              } else if (request.getParameter("childAssembly") != null) {

              	return mapping.findForward("invFindItemAssemblyChild");

              } else if (request.getParameter("assembly") != null) {

              	return mapping.findForward("invFindAssemblyChild");

              } else {

              	return mapping.findForward("invFindItem");

              }
/*******************************************************
    -- Inv FI First Action --
 *******************************************************/

	     } else if (request.getParameter("firstButton") != null){

	     	actionForm.setLineCount(0);

/*******************************************************
   -- Inv FI Previous Action --
*******************************************************/

         } else if (request.getParameter("previousButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);

/*******************************************************
   -- Inv FI Next Action --
*******************************************************/

         }else if(request.getParameter("nextButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);

         }

/*******************************************************
   -- Inv FI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            System.out.println("1");
            // create criteria

            if (request.getParameter("goButton") != null) {
            	System.out.println("2");
	        	HashMap criteria = new HashMap();

	        	if (!Common.validateRequired(actionForm.getItemName())) {

	        		criteria.put("itemName", actionForm.getItemName());

	        	}

	        	if (!Common.validateRequired(actionForm.getItemClass())) {

	        		criteria.put("itemClass", actionForm.getItemClass());

	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {

	        		criteria.put("category", actionForm.getCategory());

	        	}

	        	if (!Common.validateRequired(actionForm.getCostMethod())) {

	        		criteria.put("costMethod", actionForm.getCostMethod());

	        	}

	        	System.out.println("actionForm.getEnable()="+actionForm.getEnable());
	        	if (actionForm.getEnable()) {

	        		criteria.put("enable", new Byte((byte)1));

	            } else {
	            	criteria.put("enable", new Byte((byte)0));
	            }
	        	System.out.println("actionForm.getDisable()="+actionForm.getDisable());
	        	if (actionForm.getDisable()) {

	        		criteria.put("disable", new Byte((byte)1));

                } else {

                	criteria.put("disable", new Byte((byte)0));
                }



	        	if (!Common.validateRequired(actionForm.getItemDescription())) {

	        		criteria.put("itemDescription", actionForm.getItemDescription());

	        	}

	        	if (!Common.validateRequired(actionForm.getPartNumber())) {

	        		criteria.put("partNumber", actionForm.getPartNumber());

	        	}

	        	if (actionForm.getItemSchedule() != null && !actionForm.getEmergencyOrder()) {
	        		GregorianCalendar gcCurrDate = new GregorianCalendar();
		        	 gcCurrDate.setTime(Common.convertStringToSQLDate(actionForm.getBstDate()));
		             gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
		             gcCurrDate.set(Calendar.MILLISECOND, 0);
		             gcCurrDate.add(Calendar.DATE, 1);

		        	String currDayString = gcCurrDate.getTime().toString();

		        	SimpleDateFormat format =
		                    new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
		        	Date parsed = format.parse(currDayString);

		        	String parsedCurrDay = parsed.toString().substring(0, 3);

		        	System.out.println("CURRENT DAY " + parsedCurrDay);

		        	if(parsedCurrDay.equals("Mon")){
		        		criteria.put(new String("ScMonday"), new Byte((byte)1));
		        		System.out.println("MONDAY");
		        	}

		        	if(parsedCurrDay.equals("Tue")){
		        		criteria.put(new String("ScTuesday"), new Byte((byte)1));
		        		System.out.println("TUESDAY");
		        	}

		        	if(parsedCurrDay.equals("Wed")){
		        		criteria.put(new String("ScWednesday"), new Byte((byte)1));
		        		System.out.println("WEDNESDAY");
		        	}

		        	if(parsedCurrDay.equals("Thu")){
		        		criteria.put(new String("ScThursday"), new Byte((byte)1));
		        		System.out.println("THURSDAY");
		        	}

		        	if(parsedCurrDay.equals("Fri")){
		        		criteria.put(new String("ScFriday"), new Byte((byte)1));
		        		System.out.println("FRIDAY");
		        	}

		        	if(parsedCurrDay.equals("Sat")){
		        		criteria.put(new String("ScSaturday"), new Byte((byte)1));
		        		System.out.println("SATURDAY");
		        	}

		        	if(parsedCurrDay.equals("Sun")){
		        		criteria.put(new String("ScSunday"), new Byte((byte)1));
		        		System.out.println("SUNDAY");
		        	}
	        	}

	        	if (actionForm.getPrType() != null) {
	        		if (actionForm.getPrType().equals("Non-Inventoriable")) criteria.put("prType", new Byte((byte)1));
	        		else criteria.put("prType", new Byte((byte)0));
	        	}

	        	// save criteria

	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);

	     	}

            if(request.getParameter("lastButton") != null) {

            	int size = ejbFI.getInvIiSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();

            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {

            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));

            	} else {

            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);

            	}

            }

            try {

            	actionForm.clearInvFIList();

            	ArrayList list = ejbFI.getInvIiByCriteria(actionForm.getCriteria(),
            		new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
					actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {

	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);

	           } else {

	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);

	           }

	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {

	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);

	           } else {

	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);

	           	  //remove last record
	           	  list.remove(list.size() - 1);

	           }

            	Iterator i = list.iterator();

            	while (i.hasNext()) {

            		InvModItemDetails mdetails = (InvModItemDetails)i.next();

            		InvFindItemList invFIList = new InvFindItemList(actionForm,
            		    mdetails.getIiCode(),
            		    mdetails.getIiName(),
                        mdetails.getIiDescription(),
						mdetails.getIiClass(),
						mdetails.getIiPartNumber(),
						mdetails.getIiUomName(),
						mdetails.getIiAdLvCategory(),
						mdetails.getIiCostMethod(),
						Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit),
						mdetails.getIiDealPrice(),
						Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(),precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiQuantityOnHand(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getIiEnable()),
            		    mdetails.getIiDefaultLocationName());

            		actionForm.saveInvFIList(invFIList);

            	}

            } catch (GlobalNoRecordFoundException ex) {

               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findItem.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

               if (request.getParameter("child") != null || request.getParameter("childRep") != null || request.getParameter("itemSchedule") != null){

              	return mapping.findForward("invFindItemChild");

              } else if (request.getParameter("childAssembly") != null) {

              	return mapping.findForward("invFindItemAssemblyChild");

              } else if (request.getParameter("assembly") != null) {

              	return mapping.findForward("invFindAssemblyChild");

              } else {

              	return mapping.findForward("invFindItem");

              }
          }

            //actionForm.reset(mapping, request);

	        if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            }

            if (request.getParameter("child") != null || request.getParameter("childRep") != null || request.getParameter("itemSchedule") != null){

              	return mapping.findForward("invFindItemChild");

              } else if (request.getParameter("childAssembly") != null) {

              	return mapping.findForward("invFindItemAssemblyChild");

              } else if (request.getParameter("assembly") != null) {

              	return mapping.findForward("invFindAssemblyChild");

              } else {

              	return mapping.findForward("invFindItem");

              }

/*******************************************************
   -- Inv FI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv FI Open Action --
*******************************************************/

         } else if (request.getParameter("invFIList[" +
            actionForm.getRowSelected() + "].openButton") != null) {

             InvFindItemList invFIList =
                actionForm.getInvFIByIndex(actionForm.getRowSelected());

             String itemClass = invFIList.getItemClass();

             String path = null;

             if(itemClass.equals("Stock")) {

             	path = "/invItemEntry.do?forward=1" +
			     "&iiCode=" + invFIList.getItemCode();

             } else if(itemClass.equals("Assembly")) {

             	path = "/invAssemblyItemEntry.do?forward=1" +
			     "&iiCode=" + invFIList.getItemCode();

             }

             return(new ActionForward(path));

/*******************************************************
   -- Inv FI Load Action --
*******************************************************/

         }

         if (frParam != null) {

            actionForm.clearInvFIList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }


            ArrayList list = null;
            Iterator i = null;

            try {


            	actionForm.clearCategoryList();

            	list = ejbFI.getAdLvInvItemCategoryAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setCategoryList((String)i.next());

            		}

            	}

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            System.out.println("actionForm.getTableType()="+actionForm.getTableType());

	        if (actionForm.getTableType() == null) {

	        	actionForm.setItemName(null);
	        	actionForm.setItemClass(Constants.GLOBAL_BLANK);
	        	actionForm.setCategory(Constants.GLOBAL_BLANK);
	        	actionForm.setCostMethod(Constants.GLOBAL_BLANK);
	        	actionForm.setEnable(false);
	        	actionForm.setDisable(false);
	        	actionForm.setOrderBy("ITEM NAME");
	        	actionForm.setPartNumber(null);
	        	actionForm.setPrType(null);
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);

	            HashMap criteria = new HashMap();
	            criteria.put("enable", new Byte((byte)0));
	            criteria.put("disable", new Byte((byte)0));

	            actionForm.setCriteria(criteria);

	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            } else {

            	HashMap c = actionForm.getCriteria();

            	if(c.containsKey("enable")) {

            		Byte b = (Byte)c.get("enable");

            		actionForm.setEnable(Common.convertByteToBoolean(b.byteValue()));

            	}

            	if(c.containsKey("disable")) {

            		Byte b = (Byte)c.get("disable");

            		actionForm.setDisable(Common.convertByteToBoolean(b.byteValue()));

            	}

            	actionForm.setPrType(null);

            	try {

                	actionForm.clearInvFIList();

                	ArrayList newList = ejbFI.getInvIiByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {

    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);

    	           } else {

    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);

    	           }

    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);

    	           } else {

    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);

    	           	  //remove last record
    	           	  newList.remove(newList.size() - 1);

    	           }

                	Iterator j = newList.iterator();

                	while (j.hasNext()) {

                		InvModItemDetails mdetails = (InvModItemDetails)j.next();

                		InvFindItemList invFIList = new InvFindItemList(actionForm,
                		    mdetails.getIiCode(),
                		    mdetails.getIiName(),
                            mdetails.getIiDescription(),
    						mdetails.getIiClass(),
    						mdetails.getIiPartNumber(),
    						mdetails.getIiUomName(),
    						mdetails.getIiAdLvCategory(),
    						mdetails.getIiCostMethod(),
    						Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit),
    						mdetails.getIiDealPrice(),
    						Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit),
    						Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(),precisionUnit),
    						Common.convertDoubleToStringMoney(mdetails.getIiQuantityOnHand(), precisionUnit),
                		    Common.convertByteToBoolean(mdetails.getIiEnable()),
                		    mdetails.getIiDefaultLocationName());

                		actionForm.saveInvFIList(invFIList);

                	}

                } catch (GlobalNoRecordFoundException ex) {

                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);

                   if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findItem.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage");

                   }

                }

            }

	        if (request.getParameter("child") != null || request.getParameter("childAssembly") != null
	        		|| request.getParameter("childRep") != null) {
            	// child
                // save criteria
	        	System.out.println("child------------------------------");
	        	try {

	        	actionForm.setLineCount(0);

	        	HashMap criteria = new HashMap();
	        	actionForm.setEnable(true);
	        	actionForm.setDisable(false);

	        	criteria.put(new String("enable"), new Byte((byte)1));
	        	criteria.put(new String("disable"), new Byte((byte)0));

	        	criteria.put("customer", request.getParameter("customerEntered"));
	        	System.out.println("PR TYPE" + actionForm.getPrType());
	        	if (request.getParameter("prType") != null) {
	        		criteria.put("prType", request.getParameter("prType").equals("Non-Inventoriable") ? new Byte((byte)1) :new Byte ((byte)0));

	        	//	criteria.put("prType", new Byte((byte)1));
	        	}
	        	actionForm.setCriteria(criteria);

            	actionForm.clearInvFIList();

            	ArrayList newList = ejbFI.getInvIiByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
					actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           actionForm.setDisablePreviousButton(true);
	           actionForm.setDisableFirstButton(true);

	           // check if next should be disabled
	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);

	           } else {

	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);

	           	  //remove last record
	           	  newList.remove(newList.size() - 1);

	           }

               i = newList.iterator();

            	while (i.hasNext()) {

            		InvModItemDetails mdetails = (InvModItemDetails)i.next();

            		InvFindItemList invFIList = new InvFindItemList(actionForm,
            		    mdetails.getIiCode(),
            		    mdetails.getIiName(),
                        mdetails.getIiDescription(),
						mdetails.getIiClass(),
						mdetails.getIiPartNumber(),
						mdetails.getIiUomName(),
						mdetails.getIiAdLvCategory(),
						mdetails.getIiCostMethod(),
						Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit),
						mdetails.getIiDealPrice(),
						Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(),precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiQuantityOnHand(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getIiEnable()),
            		    mdetails.getIiDefaultLocationName());

            		actionForm.saveInvFIList(invFIList);

            	}

	        	} catch (GlobalNoRecordFoundException ex) {

	        		// disable prev next buttons
	        	   actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);

	               if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("findItem.error.noRecordFound"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

	            actionForm.setSelectedItemName(request.getParameter("selectedItemName"));
	            actionForm.setSelectedItemDescription(request.getParameter("selectedItemDescription"));
	            actionForm.setSelectedItemDefaultLocation(request.getParameter("selectedItemDefaultLocation"));
	            actionForm.setSelectedItemEntered(request.getParameter("selectedItemEntered"));
	            actionForm.setCustomerEntered(request.getParameter("customerEntered"));
	            actionForm.setItemSchedule(null);
	            actionForm.setBstDate(null);
	            if (request.getParameter("prType") != null) actionForm.setPrType(request.getParameter("prType"));
	            return(mapping.findForward("invFindItemChild"));

            } else if (request.getParameter("assembly") != null) {
                	// assembly
                    // save criteria

    	        	try {

    	        	actionForm.setLineCount(0);

    	        	HashMap criteria = new HashMap();
    	        	criteria.put(new String("enable"), new Byte((byte)1));
    	        	criteria.put(new String("itemClass"), new String("Assembly"));
    	        	actionForm.setCriteria(criteria);

                	actionForm.clearInvFIList();

                	ArrayList newList = ejbFI.getInvIiByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

    	           actionForm.setDisablePreviousButton(true);
    	           actionForm.setDisableFirstButton(true);

    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);

    	           } else {

    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);

    	           	  //remove last record
    	           	  newList.remove(newList.size() - 1);

    	           }

                   i = newList.iterator();

                	while (i.hasNext()) {

                		InvModItemDetails mdetails = (InvModItemDetails)i.next();

                		InvFindItemList invFIList = new InvFindItemList(actionForm,
                		    mdetails.getIiCode(),
                		    mdetails.getIiName(),
                            mdetails.getIiDescription(),
    						mdetails.getIiClass(),
    						mdetails.getIiPartNumber(),
    						mdetails.getIiUomName(),
    						mdetails.getIiAdLvCategory(),
    						mdetails.getIiCostMethod(),
    						Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit),
    						mdetails.getIiDealPrice(),
    						Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit),
    						Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(),precisionUnit),
    						Common.convertDoubleToStringMoney(mdetails.getIiQuantityOnHand(), precisionUnit),
                		    Common.convertByteToBoolean(mdetails.getIiEnable()),
                		    mdetails.getIiDefaultLocationName());

                		actionForm.saveInvFIList(invFIList);

                	}

	                } catch (GlobalNoRecordFoundException ex) {

	                   // disable prev next buttons
	    		       actionForm.setDisableNextButton(true);
	                   actionForm.setDisableLastButton(true);

	                   if(actionForm.getLineCount() > 0) {

	                   		actionForm.setDisableFirstButton(false);
	                   		actionForm.setDisablePreviousButton(false);

	                   } else {

	                   		actionForm.setDisableFirstButton(true);
	                   		actionForm.setDisablePreviousButton(true);

	                   }

	                   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("findItem.error.noRecordFound"));

	                } catch (EJBException ex) {

	                   if (log.isInfoEnabled()) {

	                      log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                      return mapping.findForward("cmnErrorPage");

	                   }

	                }

	                actionForm.setSelectedItemName(request.getParameter("selectedItemName"));
	                actionForm.setSelectedItemDescription(request.getParameter("selectedItemDescription"));
	                actionForm.setSelectedItemEntered(request.getParameter("selectedItemEntered"));
	                actionForm.setItemSchedule(null);
	                actionForm.setBstDate(null);
	                actionForm.setPrType(null);
	                return(mapping.findForward("invFindAssemblyChild"));





            } else if (request.getParameter("itemSchedule") != null) {
            	System.out.println("itemSchedule");

            	// child
                // save criteria

	        	try {

	        	actionForm.setLineCount(0);

	        	HashMap criteria = new HashMap();
	        	criteria.put(new String("enable"), new Byte((byte)1));


	        	 GregorianCalendar gcCurrDate = new GregorianCalendar();
	        	 gcCurrDate.setTime(Common.convertStringToSQLDate(request.getParameter("bstDate")));
	             gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
	             gcCurrDate.set(Calendar.MILLISECOND, 0);
	             gcCurrDate.add(Calendar.DATE, 1);

	        	String currDayString = gcCurrDate.getTime().toString();

	        	SimpleDateFormat format =
	                    new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
	        	Date parsed = format.parse(currDayString);

	        	String parsedCurrDay = parsed.toString().substring(0, 3);

	        	System.out.println("CURRENT DAY " + parsedCurrDay);

	        	if(parsedCurrDay.equals("Mon")){
	        		criteria.put(new String("ScMonday"), new Byte((byte)1));
	        		System.out.println("MONDAY");
	        	}

	        	if(parsedCurrDay.equals("Tue")){
	        		criteria.put(new String("ScTuesday"), new Byte((byte)1));
	        		System.out.println("TUESDAY");
	        	}

	        	if(parsedCurrDay.equals("Wed")){
	        		criteria.put(new String("ScWednesday"), new Byte((byte)1));
	        		System.out.println("WEDNESDAY");
	        	}

	        	if(parsedCurrDay.equals("Thu")){
	        		criteria.put(new String("ScThursday"), new Byte((byte)1));
	        		System.out.println("THURSDAY");
	        	}

	        	if(parsedCurrDay.equals("Fri")){
	        		criteria.put(new String("ScFriday"), new Byte((byte)1));
	        		System.out.println("FRIDAY");
	        	}

	        	if(parsedCurrDay.equals("Sat")){
	        		criteria.put(new String("ScSaturday"), new Byte((byte)1));
	        		System.out.println("SATURDAY");
	        	}

	        	if(parsedCurrDay.equals("Sun")){
	        		criteria.put(new String("ScSunday"), new Byte((byte)1));
	        		System.out.println("SUNDAY");
	        	}


	        	actionForm.setCriteria(criteria);

            	actionForm.clearInvFIList();
            	System.out.println("actionForm.getCriteria()"+actionForm.getCriteria());
            	ArrayList newList = ejbFI.getInvIiByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
					actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           actionForm.setDisablePreviousButton(true);
	           actionForm.setDisableFirstButton(true);

	           // check if next should be disabled
	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);

	           } else {

	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);

	           	  //remove last record
	           	  newList.remove(newList.size() - 1);

	           }

               i = newList.iterator();

            	while (i.hasNext()) {

            		InvModItemDetails mdetails = (InvModItemDetails)i.next();

            		InvFindItemList invFIList = new InvFindItemList(actionForm,
            		    mdetails.getIiCode(),
            		    mdetails.getIiName(),
                        mdetails.getIiDescription(),
						mdetails.getIiClass(),
						mdetails.getIiPartNumber(),
						mdetails.getIiUomName(),
						mdetails.getIiAdLvCategory(),
						mdetails.getIiCostMethod(),
						Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit),
						mdetails.getIiDealPrice(),
						Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(),precisionUnit),
						Common.convertDoubleToStringMoney(mdetails.getIiQuantityOnHand(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getIiEnable()),
            		    mdetails.getIiDefaultLocationName());

            		actionForm.saveInvFIList(invFIList);

            	}

	        	} catch (GlobalNoRecordFoundException ex) {

	        		// disable prev next buttons
	        	   actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);

	               if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("findItem.error.noRecordFound"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in InvFindItemAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

	            actionForm.setSelectedItemName(request.getParameter("selectedItemName"));
	            actionForm.setSelectedItemDescription(request.getParameter("selectedItemDescription"));
	            actionForm.setSelectedItemDefaultLocation(request.getParameter("selectedItemDefaultLocation"));
	            actionForm.setSelectedItemEntered(request.getParameter("selectedItemEntered"));
	            actionForm.setItemSchedule(request.getParameter("itemSchedule"));
	            actionForm.setBstDate(request.getParameter("bstDate"));
	            if (request.getParameter("prType") != null) actionForm.setPrType(request.getParameter("prType"));
	            return(mapping.findForward("invFindItemChild"));

            } else {
            	// find item
            	 actionForm.setItemSchedule(null);
            	return(mapping.findForward("invFindItem"));

            }

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvFindItemAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}