package com.struts.inv.finditem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class InvFindItemForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String costMethod = null;
	private ArrayList costMethodList = new ArrayList();   
	private boolean enable = false;
	private boolean disable = false;  
	private boolean emergencyOrder = false;  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String itemDescription = null;
	private String partNumber = null;
	
	private String selectedItemName = null;
	private String selectedItemDescription = null;
	private String selectedItemDefaultLocation = null;
	private String selectedItemEntered = null;
	private String customerEntered = null;
	private String itemSchedule = null;
	private String bstDate = null;
	private String prType = null;
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFIList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount(){
		return lineCount;   	 
	}
	
	public void setLineCount(int lineCount){  	
		this.lineCount = lineCount;
	}
	
	public InvFindItemList getInvFIByIndex(int index) {
		
		return((InvFindItemList)invFIList.get(index));
		
	}
	
	public Object[] getInvFIList() {
		
		return invFIList.toArray();
		
	}
	
	public int getInvFIListSize() {
		
		return invFIList.size();
		
	}
	
	public void saveInvFIList(Object newInvFIList) {
		
		invFIList.add(newInvFIList);
		
	}
	
	public void clearInvFIList() {
		
		invFIList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFIList, boolean isEdit) {
		
		this.rowSelected = invFIList.indexOf(selectedInvFIList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getItemName() {
	
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
		
	}	
	
	public String getItemClass() {
	
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
	
		this.itemClass = itemClass;
		
	}	
	
	public ArrayList getItemClassList() {
	
		return itemClassList;
		
	}	
	
	public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return categoryList;
		
	}
	
	public void setCategoryList(String category) {
	
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
	
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
    }
	
	public String getCostMethod() {
		
		return costMethod;
		
	}
	
	public void setCostMethod(String costMethod) {
		
		this.costMethod = costMethod;
		
	}
	
	public ArrayList getCostMethodList() {
	
		return costMethodList;
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		
	}
	
	public boolean getDisable() {
		
		return disable;
		
	}
	
	public void setDisable(boolean disable) {
		
		this.disable = disable;
		
	} 
	
	public boolean getEmergencyOrder() {
		
		return emergencyOrder;
		
	}
	
	public void setEmergencyOrder(boolean emergencyOrder) {
		
		this.emergencyOrder = emergencyOrder;
		
	} 
	
	public String getOrderBy() {
   	
		return orderBy;
   	  
	}
   
	public void setOrderBy(String orderBy) {
   	
		this.orderBy = orderBy;
   	  
	}
   
	public ArrayList getOrderByList() {
   	
		return orderByList;
   	  
	}	
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public String getSelectedItemName() {
		
		return selectedItemName;
		
	}
	
	public void setSelectedItemName(String selectedItemName) {
		
		this.selectedItemName = selectedItemName;
		
	}
	
	public String getSelectedItemDescription() {
		
		return selectedItemDescription;
		
	}
	
	public void setSelectedItemDescription(String selectedItemDescription) {
		
		this.selectedItemDescription = selectedItemDescription;
		
	}

	public String getSelectedItemDefaultLocation() {

		return selectedItemDefaultLocation;

	}

	public void setSelectedItemDefaultLocation(String selectedItemDefaultLocation) {

		this.selectedItemDefaultLocation = selectedItemDefaultLocation;

	}
	
	public String getSelectedItemEntered() {
		
		return selectedItemEntered;
		
	}
	
	public void setSelectedItemEntered(String selectedItemEntered) {
		
		this.selectedItemEntered = selectedItemEntered;
		
	}
	
public String getCustomerEntered() {
		
		return customerEntered;
		
	}
	
	public void setCustomerEntered(String customerEntered) {
		
		this.customerEntered = customerEntered;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getPartNumber() {
		
		return partNumber;
		
	}
	
	public void setPartNumber(String partNumber) {
		
		this.partNumber = partNumber;
		
	}
	
	public String getItemSchedule() {
		
		return itemSchedule;
		
	}
	
	public void setItemSchedule(String itemSchedule) {
		
		this.itemSchedule = itemSchedule;
		
	}
	
	public String getBstDate() {
		
		return bstDate;
		
	}
	
	public void setBstDate(String bstDate) {
		
		this.bstDate = bstDate;
		
	}
	
	public String getPrType() {
		
		return prType;
		
	}
	
	public void setPrType(String prType) {
		
		this.prType = prType;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		enable = false;
		disable = false;  
		emergencyOrder = false;
		
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");		
							
        costMethodList.clear();
        costMethodList.add(Constants.GLOBAL_BLANK);
        costMethodList.add("Average");
        costMethodList.add("FIFO");
        
		if (orderByList.isEmpty()) { 
    
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("ITEM NAME");
			orderByList.add("ITEM CLASS");
			orderByList.add("ITEM CATEGORY");
			
		}     		
		
		showDetailsButton = null;
		hideDetailsButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		}
		
		return errors;
		
	}
}