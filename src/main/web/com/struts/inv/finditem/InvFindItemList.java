package com.struts.inv.finditem;

import java.io.Serializable;

public class InvFindItemList implements Serializable {
	
	private Integer itemCode = null;
	private String itemName = null;
	private String description = null;
	private String itemClass = null;
	private String partNumber = null;
	private String unitMeasure = null;
	private String category = null;
	private String costMethod = null;
	private String unitCost = null;
	private String dealPrice = null;
	private String salesPrice = null;
	private String percentMarkup = null;
	private String quantityOnHand = null;
	private boolean enable = false;
	private String defaultLocation = null;
	
	private String openButton = null;
	
	private InvFindItemForm parentBean;
	
	public InvFindItemList(InvFindItemForm parentBean,
			Integer itemCode,    
			String itemName,  
			String description,
			String itemClass,
			String partNumber,
			String unitMeasure,
			String category,
			String costMethod,
			String unitCost,
			String dealPrice,
			String salesPrice,
			String percentMarkup,
			String quantityOnHand,
			boolean enable,
			String defaultLocation) {
		
		this.parentBean = parentBean;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.description = description;
		this.itemClass = itemClass;
		this.partNumber = partNumber;
		this.unitMeasure = unitMeasure;
		this.category = category;
		this.costMethod = costMethod;
		this.unitCost = unitCost;
		this.dealPrice = dealPrice;
		this.salesPrice = salesPrice;
		this.percentMarkup = percentMarkup;
		this.quantityOnHand = quantityOnHand;
		this.enable = enable;
		this.defaultLocation = defaultLocation;
		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getItemCode() {
		
		return itemCode;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public String getPartNumber() {
		
		return partNumber;
		
	}
	
	public String getUnitMeasure() {
		
		return unitMeasure;
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public String getCostMethod() {
		
		return costMethod;
		
	}
	
	public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public String getDealPrice() {
		
		return dealPrice;
		
	}
	
	public String getSalesPrice() {
		
		return salesPrice;
		
	}
	
	public String getPercentMarkup() {
		
		return percentMarkup;
		
	}
	
	
	public String getQuantityOnHand() {
		
		return quantityOnHand;
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public String getDefaultLocation() {
		
		return defaultLocation;
		
	}
	
}