package com.struts.inv.finditemlocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindItemLocationController;
import com.ejb.txn.InvFindItemLocationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.InvModItemLocationDetails;

public final class InvFindItemLocationAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindItemLocationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindItemLocationForm actionForm = (InvFindItemLocationForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_ITEM_LOCATION_ENTRY_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindItemLocation");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize InvFindItemLocationController EJB
*******************************************************/
			
			InvFindItemLocationControllerHome homeFIL = null;
			InvFindItemLocationController ejbFIL = null;
			
			try {
				
				homeFIL = (InvFindItemLocationControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindItemLocationControllerEJB", InvFindItemLocationControllerHome.class);
				
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindItemLocationAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFIL = homeFIL.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindItemLocationAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors(); 
			
/*******************************************************
   Call InvFindItemLocationController EJB
   getGlFcPrecisionUnit
*******************************************************/
			
			short quantityPrecisionUnit = 0;
			
			try { 
				
				quantityPrecisionUnit = ejbFIL.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());
				
			} catch (EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvFindItemLocationAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
			}				
			
/*******************************************************
   -- Inv FIL Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindItemLocation"));
				
/*******************************************************
   -- Inv FIL Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindItemLocation"));                         

/*******************************************************
   -- Inv FIL First Action --
*******************************************************/ 
								
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
   -- Inv FIL Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
   -- Inv FIL Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
   -- Inv FIL Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getItemName())) {
						
						criteria.put("itemName", actionForm.getItemName());
						
					}	
					
					if (!Common.validateRequired(actionForm.getItemDescription())) {
						
						criteria.put("itemDescription", actionForm.getItemDescription());
						
					}
					
					if (!Common.validateRequired(actionForm.getLocationName())) {
						
						criteria.put("locationName", actionForm.getLocationName());
						
					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
		        		
		        		criteria.put("category", actionForm.getCategory());
		        		
		        	}
					
					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				//get all InvFindItemLocationBranchLists
				
				ArrayList branchList = new ArrayList();
				
				actionForm.clearSelectedInvBrIlList();
				
				for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {
					
					InvFindItemLocationBranchList brList = (InvFindItemLocationBranchList)actionForm.getInvBrIlListByIndex(i);
					
					if(brList.getBranchCheckbox() == true) {
					
						AdModBranchItemLocationDetails mdetails = new AdModBranchItemLocationDetails();
						mdetails.setBilBrCode(brList.getBranchCode());
						
						actionForm.saveSelectedInvBrIlList(mdetails.getBilBrCode());
						
						branchList.add(mdetails);
						
					}
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFIL.getInvIlSizeByCriteria(actionForm.getCriteria(), branchList, user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFILList();
					
					ArrayList list = ejbFIL.getInvIlByCriteria(actionForm.getCriteria(),
							branchList,
							new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvModItemLocationDetails mdetails = (InvModItemLocationDetails)i.next();
						
						ArrayList brIlList = new ArrayList();
						
						for(int j=0; j<mdetails.getBrIlListSize(); j++) {
							
							AdModBranchItemLocationDetails brIlDetails = (AdModBranchItemLocationDetails)mdetails.getBrIlListByIndex(j);
							
							InvFindItemLocationBranchList brList = new InvFindItemLocationBranchList(actionForm, 
									brIlDetails.getBilBrName(), brIlDetails.getBilBrCode());
							
							brIlList.add(brList);
							
						}
						
						InvFindItemLocationList invFILList = new InvFindItemLocationList(actionForm,
								mdetails.getIlCode(),
								mdetails.getIlIiName(),
								mdetails.getIlLocName(),
								Common.convertDoubleToStringMoney(mdetails.getIlReorderPoint(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getIlReorderQuantity(), quantityPrecisionUnit),
								mdetails.getIlCoaGlSalesAccountNumber(),
								mdetails.getIlCoaGlSalesAccountDescription(),
								mdetails.getIlCoaGlInventoryAccountNumber(),
								mdetails.getIlCoaGlInventoryAccountDescription(),
								mdetails.getIlCoaGlCostOfSalesAccountNumber(),
								mdetails.getIlCoaGlCostOfSalesAccountDescription(),
								brIlList);
							
						
						actionForm.saveInvFILList(invFILList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findItemLocation.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindItemLocationAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invFindItemLocation");
					
				}
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	            
				
				return(mapping.findForward("invFindItemLocation"));
				
/*******************************************************
   -- Inv FIL Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
   -- Inv FIL Open Action --
*******************************************************/
				
			} else if (request.getParameter("invFILList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindItemLocationList invFILList =
					actionForm.getInvFILByIndex(actionForm.getRowSelected());
				
				String path = "/invItemLocationEntry.do?forward=1" +
				"&ilCode=" + invFILList.getItemLocationCode();
				
				return(new ActionForward(path));
				
/*******************************************************
   -- Inv FIL Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFILList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
								
				ArrayList list = null;
				Iterator i = null;
				
				try {
									
					actionForm.clearLocationNameList();
					
					list = ejbFIL.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationNameList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationNameList((String)i.next());
							
						}
						
					}
					
					
					actionForm.clearCategoryList();
	            	
	            	list = ejbFIL.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		
	            	} 
					
					actionForm.clearInvBrIlList();
					
					list = ejbFIL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						AdBranchDetails details = (AdBranchDetails)i.next();
						
						InvFindItemLocationBranchList invBrIlList = new InvFindItemLocationBranchList(actionForm,
								details.getBrName(), details.getBrCode());
						
						actionForm.saveInvBrIlList(invBrIlList);
						
					}
		    	           	            
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindItemLocationAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				} 
				
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setItemName(Constants.GLOBAL_BLANK);
					actionForm.setItemDescription(Constants.GLOBAL_BLANK);
					actionForm.setLocationName(Constants.GLOBAL_BLANK);
					actionForm.setOrderBy("ITEM NAME"); 
					
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				} else {
					
					try {
						
						actionForm.clearInvFILList();
						
						for(int j=0; j<actionForm.getInvBrIlListSize(); j++) {
							
							InvFindItemLocationBranchList invBrList = (InvFindItemLocationBranchList)actionForm.getInvBrIlListByIndex(j);
							invBrList.setBranchCheckbox(false);
							
						}
						
						for(int j=0; j<actionForm.getSelectedInvBrIlListSize(); j++) {
							
							Integer brSelected = actionForm.getSelectedInvBrIlListByIndex(j);
							
							for(int k=0; k<actionForm.getInvBrIlListSize(); k++) {
								
								InvFindItemLocationBranchList invBrList = (InvFindItemLocationBranchList)actionForm.getInvBrIlListByIndex(k);
								
								if(brSelected.equals(invBrList.getBranchCode())) {
									
									invBrList.setBranchCheckbox(true);
									break;
									
								}
								
							}
							
						}
						
						// get all InvFindItemLocationBranchLists
						
						ArrayList branchList = new ArrayList();
						
						for(int j=0; j<actionForm.getInvBrIlListSize(); j++) {
							
							InvFindItemLocationBranchList brList = (InvFindItemLocationBranchList)actionForm.getInvBrIlListByIndex(j);
							
							if(brList.getBranchCheckbox() == true) {
							
								AdModBranchItemLocationDetails mdetails = new AdModBranchItemLocationDetails();
								mdetails.setBilBrCode(brList.getBranchCode());
								
								branchList.add(mdetails);
								
							}
							
						}
						
						ArrayList newList = ejbFIL.getInvIlByCriteria(actionForm.getCriteria(),
								branchList,
								new Integer(actionForm.getLineCount()), 
								new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						Iterator j = newList.iterator();
						
						while (j.hasNext()) {
							
							InvModItemLocationDetails mdetails = (InvModItemLocationDetails)j.next();
							
							ArrayList brIlList = new ArrayList();
							
							for(int k=0; k<mdetails.getBrIlListSize(); k++) {
								
								AdModBranchItemLocationDetails brIlDetails = (AdModBranchItemLocationDetails)mdetails.getBrIlListByIndex(k);
								
								InvFindItemLocationBranchList brList = new InvFindItemLocationBranchList(actionForm, 
										brIlDetails.getBilBrName(), brIlDetails.getBilBrCode());
								
								brIlList.add(brList);
								
							}
							
							InvFindItemLocationList invFILList = new InvFindItemLocationList(actionForm,
									mdetails.getIlCode(),
									mdetails.getIlIiName(),
									mdetails.getIlLocName(),
									Common.convertDoubleToStringMoney(mdetails.getIlReorderPoint(), quantityPrecisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getIlReorderQuantity(), quantityPrecisionUnit),
									mdetails.getIlCoaGlSalesAccountNumber(),
									mdetails.getIlCoaGlSalesAccountDescription(),
									mdetails.getIlCoaGlInventoryAccountNumber(),
									mdetails.getIlCoaGlInventoryAccountDescription(),
									mdetails.getIlCoaGlCostOfSalesAccountNumber(),
									mdetails.getIlCoaGlCostOfSalesAccountDescription(),
									brIlList);
							
							actionForm.saveInvFILList(invFILList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
							
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findItemLocation.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindItemLocationAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				return(mapping.findForward("invFindItemLocation"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindItemLocationAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			e.printStackTrace();
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}