package com.struts.inv.finditemlocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class InvFindItemLocationForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String locationName = null;
	private ArrayList locationNameList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList();

	private ArrayList invBrIlList = new ArrayList(); 
	private ArrayList selectedInvBrIlCodeList = new ArrayList();
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFILList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindItemLocationList getInvFILByIndex(int index) {
		
		return((InvFindItemLocationList)invFILList.get(index));
		
	}
	
	public Object[] getInvFILList() {
		
		return invFILList.toArray();
		
	}
	
	public int getInvFILListSize() {
		
		return invFILList.size();
		
	}
	
	public void saveInvFILList(Object newInvFILList) {
		
		invFILList.add(newInvFILList);
		
	}
	
	public void clearInvFILList() {
		
		invFILList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFILList, boolean isEdit) {
		
		this.rowSelected = invFILList.indexOf(selectedInvFILList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getLocationName() {
		
		return locationName;
		
	}
	
	public void setLocationName(String locationName) {
		
		this.locationName = locationName;
		
	}
	
	public ArrayList getLocationNameList() {
		
		return locationNameList;
		
	}
	
	public void setLocationNameList(String locationName) {
		
		locationNameList.add(locationName);
		
	}
	
	public void clearLocationNameList() {
		
		locationNameList.clear();
		locationNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
	   	
	   	return orderByList;
	   	  
	}
	
public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return categoryList;
		
	}
	
	public void setCategoryList(String category) {
	
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
	
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
    }
	
	
	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}
	
	public InvFindItemLocationBranchList getInvBrIlListByIndex(int index){
		
		return ((InvFindItemLocationBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	public Object[] getSelectedInvBrIlList(){
		
		return selectedInvBrIlCodeList.toArray();
		
	}
	
	public Integer getSelectedInvBrIlListByIndex(int index){
		
		return ((Integer)selectedInvBrIlCodeList.get(index));
		
	}
	
	public int getSelectedInvBrIlListSize(){
		
		return(selectedInvBrIlCodeList.size());
		
	}
	
	public void saveSelectedInvBrIlList(Object newSelectedInvBrIlCodeList){
		
		selectedInvBrIlCodeList.add(newSelectedInvBrIlCodeList);   	  
		
	}
	
	public void clearSelectedInvBrIlList(){
		
		selectedInvBrIlCodeList.clear();
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		itemName = null;
		itemDescription = null;
		
		for (int i=0; i<invBrIlList.size(); i++) {
		  	
			InvFindItemLocationBranchList actionList = (InvFindItemLocationBranchList)invBrIlList.get(i);
		  	actionList.setBranchCheckbox(false);
		  	
		}
		
		if (orderByList.isEmpty()) { 
            
      	      orderByList.add(Constants.GLOBAL_BLANK);
      	      orderByList.add("ITEM NAME");
      	      orderByList.add("LOCATION NAME");
      	  
      	}             
        
		showDetailsButton = null;
		hideDetailsButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		}
		
		return errors;
		
	}
}