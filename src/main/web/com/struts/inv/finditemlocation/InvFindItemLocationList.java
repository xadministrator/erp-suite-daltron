package com.struts.inv.finditemlocation;

import java.io.Serializable;
import java.util.ArrayList;

public class InvFindItemLocationList implements Serializable {
	
	private Integer itemLocationCode = null;
	private String itemName = null;
	private String locationName = null;
	private String reorderPoint = null;
	private String reorderQuantity = null;
	private String salesAccount = null;
	private String salesAccountDesc = null;
	private String inventoryAccount = null;
	private String inventoryAccountDesc = null;
	private String costOfSalesAccount = null;
	private String costOfSalesAccountDesc = null;
	private ArrayList invBrIlList = new ArrayList();
	
	private String openButton = null;
	
	private InvFindItemLocationForm parentBean;
	
	public InvFindItemLocationList(InvFindItemLocationForm parentBean,
			Integer itemLocationCode,
			String itemName,
			String locationName,  
			String reorderPoint,
			String reorderQuantity,
			String salesAccount,
			String salesAccountDesc,
			String inventoryAccount,
			String inventoryAccountDesc,
			String costOfSalesAccount,
			String costOfSalesAccountDesc, 
			ArrayList invBrIlList) {
		
		this.parentBean = parentBean;
		this.itemLocationCode = itemLocationCode;
		this.itemName = itemName;
		this.locationName = locationName;
		this.reorderPoint = reorderPoint;
		this.reorderQuantity = reorderQuantity;
		this.salesAccount = salesAccount;
		this.salesAccountDesc = salesAccountDesc;
		this.inventoryAccount = inventoryAccount;
		this.inventoryAccountDesc = inventoryAccountDesc;
		this.costOfSalesAccount = costOfSalesAccount;
		this.costOfSalesAccountDesc = costOfSalesAccountDesc;
		this.invBrIlList = invBrIlList;
		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getItemLocationCode() {
		
		return itemLocationCode;
		
	}

	public String getItemName() {
		
		return itemName;
		
	}	
	
	public String getLocationName() {
		
		return locationName;
		
	}
	
	public String getReorderPoint() {
		
		return reorderPoint;
		
	}
	
	public String getReorderQuantity() {
		
		return reorderQuantity;
		
	}
	
	public String getSalesAccount() {
		
		return salesAccount;
		
	}

	public String getSalesAccountDesc() {
		
		return salesAccountDesc;
		
	}
	
	public String getInventoryAccount() {
		
		return inventoryAccount;
		
	}	
	
	public String getInventoryAccountDesc() {
		
		return inventoryAccountDesc;
		
	}
	
	public String getCostOfSalesAccount() {
		
		return costOfSalesAccount;
		
	}	
		
	public String getCostOfSalesAccountDesc() {
		
		return costOfSalesAccountDesc;
		
	}
	
	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}
	
	public InvFindItemLocationBranchList getInvBrIlListByIndex(int index){
		
		return ((InvFindItemLocationBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}

}