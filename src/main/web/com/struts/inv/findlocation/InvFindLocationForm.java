package com.struts.inv.findlocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class InvFindLocationForm extends ActionForm implements Serializable {
	
	private String locationName = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String contactPerson = null;

	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFLList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindLocationList getInvFLByIndex(int index) {
		
		return((InvFindLocationList)invFLList.get(index));
		
	}
	
	public Object[] getInvFLList() {
		
		return invFLList.toArray();
		
	}
	
	public int getInvFLListSize() {
		
		return invFLList.size();
		
	}
	
	public void saveInvFLList(Object newInvFLList) {
		
		invFLList.add(newInvFLList);
		
	}
	
	public void clearInvFLList() {
		
		invFLList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFLList, boolean isEdit) {
		
		this.rowSelected = invFLList.indexOf(selectedInvFLList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getLocationName() {
	
		return locationName;
		
	}
	
	public void setLocationName(String locationName) {
	
		this.locationName = locationName;
		
	}		
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public void setTypeList(String type) {
	
		typeList.add(type);
		
	}
	
	public void clearTypeList() {
	
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		
    }
	
	public String getContactPerson() {
		
		return contactPerson;
		
	}
	
	public void setContactPerson(String contactPerson) {
		
		this.contactPerson = contactPerson;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		showDetailsButton = null;
		hideDetailsButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		}
		
		return errors;
		
	}
}