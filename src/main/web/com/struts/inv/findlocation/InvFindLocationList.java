package com.struts.inv.findlocation;

import java.io.Serializable;

public class InvFindLocationList implements Serializable {
	
	private Integer locationCode = null;
	private String locationName = null;
	private String description = null;
	private String type = null;
	private String address = null;
	private String contactPerson = null;
	private String contactNumber = null;
	private String emailAddress = null;
	
	private String openButton = null;
	
	private InvFindLocationForm parentBean;
	
	public InvFindLocationList(InvFindLocationForm parentBean,
			Integer locationCode,
			String locationName,  
			String description,
			String type,
			String address,
			String contactPerson,
			String contactNumber,
			String emailAddress) {
		
		this.parentBean = parentBean;
		this.locationCode = locationCode;
		this.locationName = locationName;
		this.description = description;
		this.type = type;
		this.address = address;
		this.contactPerson = contactPerson;
		this.contactNumber = contactNumber;
		this.emailAddress = emailAddress;
		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getLocationCode() {
		
		return locationCode;
		
	}
	
	public String getLocationName() {
		
		return locationName;
		
	}	
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getType() {
		
		return type;
		
	}	
	
	public String getAddress() {
		
		return address;
		
	}
	
	public String getContactNumber() {
		
		return contactNumber;
		
	}
	
	public String getContactPerson() {
		
		return contactPerson;
		
	}	
	
	public String getEmailAddress() {
		
		return emailAddress;
		
	}	
	
}