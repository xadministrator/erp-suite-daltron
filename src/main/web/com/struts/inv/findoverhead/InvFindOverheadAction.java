package com.struts.inv.findoverhead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindOverheadController;
import com.ejb.txn.InvFindOverheadControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvOverheadDetails;

public final class InvFindOverheadAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindOverheadAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindOverheadForm actionForm = (InvFindOverheadForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_OVERHEAD_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindOverhead");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}

/*******************************************************
   Initialize InvFindOverheadController EJB
*******************************************************/
			
			InvFindOverheadControllerHome homeFO = null;
			InvFindOverheadController ejbFO = null;
			
			try {
				
				homeFO = (InvFindOverheadControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindOverheadControllerEJB", InvFindOverheadControllerHome.class);
								
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindOverheadAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFO = homeFO.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindOverheadAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  
			
/*******************************************************
   -- Inv FA Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindOverhead"));
				
/*******************************************************
   -- Inv FA Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindOverhead"));                         

/*******************************************************
   -- Inv FA First Action --
*******************************************************/ 
								
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
   -- Inv FA Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
   -- Inv FA Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
   -- Inv FA Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
						
						criteria.put("category", actionForm.getCategory());
						
					}
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
		        	if (!Common.validateRequired(actionForm.getPosted())) {
		        		
		        		criteria.put("posted", actionForm.getPosted());
		        		
		        	}					
					
					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFO.getInvOhSizeByCriteria(actionForm.getCriteria(),
						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFOList();
					
					ArrayList list = ejbFO.getInvOhByCriteria(actionForm.getCriteria(),
						new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvOverheadDetails details = (InvOverheadDetails)i.next();

						InvFindOverheadList invFOList = new InvFindOverheadList(actionForm,
								details.getOhCode(),								
								Common.convertSQLDateToString(details.getOhDate()),
								details.getOhLvShift(),
								details.getOhDocumentNumber(),
								details.getOhReferenceNumber(),
								details.getOhDescription());
						
						actionForm.saveInvFOList(invFOList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findOverhead.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindOverheadAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invFindOverhead");
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                       
				
				return(mapping.findForward("invFindOverhead"));
				
/*******************************************************
   -- Inv FA Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
   -- Inv FA Open Action --
*******************************************************/
				
			} else if (request.getParameter("invFOList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindOverheadList invFOList =
					actionForm.getInvFOByIndex(actionForm.getRowSelected());
				
				String path = "/invOverheadEntry.do?forward=1" +
				"&overheadCode=" + invFOList.getOverheadCode();
				
				return(new ActionForward(path));
				
/*******************************************************
   -- Inv FA Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFOList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
										
					actionForm.clearCategoryList();
					
					list = ejbFO.getAdLvInvOverheadCategoryAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setCategoryList((String)i.next());
							
						}
						
					}
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindOverheadAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				} 	
				
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setCategory(Constants.GLOBAL_BLANK);
					actionForm.setReferenceNumber(null);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setPosted(Constants.GLOBAL_BLANK);
					actionForm.setOrderBy(Constants.GLOBAL_BLANK);
					
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				} else {
					
					try {
						
						actionForm.clearInvFOList();
						
						ArrayList newList = ejbFO.getInvOhByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						Iterator j = newList.iterator();
						
						while (j.hasNext()) {
							
							InvOverheadDetails details = (InvOverheadDetails)j.next();

							InvFindOverheadList invFOList = new InvFindOverheadList(actionForm,
									details.getOhCode(),								
									Common.convertSQLDateToString(details.getOhDate()),
									details.getOhLvShift(),
									details.getOhDocumentNumber(),
									details.getOhReferenceNumber(),
									details.getOhDescription());
							
							actionForm.saveInvFOList(invFOList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
		                   	
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findOverhead.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindOverheadAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				return(mapping.findForward("invFindOverhead"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindOverheadAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}