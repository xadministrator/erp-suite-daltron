package com.struts.inv.findoverhead;

import java.io.Serializable;

public class InvFindOverheadList implements Serializable {
	
	private Integer overheadCode = null;
	private String category = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String date = null;
	private String description = null;
	
	private String openButton = null;
	
	private InvFindOverheadForm parentBean;
	
	public InvFindOverheadList(InvFindOverheadForm parentBean,
			Integer overheadCode,
			String date,
			String category,
			String documentNumber,
			String referenceNumber,
			String description) {
		
		this.parentBean = parentBean;
		this.overheadCode = overheadCode;
		this.date = date;
		this.category = category;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.description = description;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getOverheadCode() {
		
		return overheadCode;
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	

	public String getDate() {
		
		return date;
		
	}
	
}