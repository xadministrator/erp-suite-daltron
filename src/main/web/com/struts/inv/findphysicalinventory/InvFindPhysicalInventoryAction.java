package com.struts.inv.findphysicalinventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvFindPhysicalInventoryController;
import com.ejb.txn.InvFindPhysicalInventoryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModPhysicalInventoryDetails;

public final class InvFindPhysicalInventoryAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 Check if user has a session
 *******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvFindPhysicalInventoryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvFindPhysicalInventoryForm actionForm = (InvFindPhysicalInventoryForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_FIND_PHYSICAL_INVENTORY_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invFindPhysicalInventory");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
/*******************************************************
 Initialize InvFindPhysicalInventoryController EJB
 *******************************************************/

			InvFindPhysicalInventoryControllerHome homeFPI = null;
			InvFindPhysicalInventoryController ejbFPI = null;
			
			try {
				
				homeFPI = (InvFindPhysicalInventoryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvFindPhysicalInventoryControllerEJB", InvFindPhysicalInventoryControllerHome.class);
								
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvFindPhysicalInventoryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFPI = homeFPI.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvFindPhysicalInventoryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  
			
/*******************************************************
 -- Inv FPI Show Details Action --
 *******************************************************/

			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invFindPhysicalInventory"));
				
/*******************************************************
 -- Inv FPI Hide Details Action --
 *******************************************************/	     

			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invFindPhysicalInventory"));                         

/*******************************************************
 -- Inv FPI First Action --
*******************************************************/ 
				
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);

/*******************************************************
 -- Inv FPI Previous Action --
 *******************************************************/ 

			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
 -- Inv FPI Next Action --
 *******************************************************/ 

			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
 -- Inv FPI Go Action --
 *******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
		        		
		        		criteria.put("category", actionForm.getCategory());
		        		
		        	}
					
		        	if (!Common.validateRequired(actionForm.getLocation())) {
		        		
		        		criteria.put("location", actionForm.getLocation());
		        		
		        	}

					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFPI.getInvPiSizeByCriteria(actionForm.getCriteria(),
							 new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearInvFPIList();
					
					ArrayList list = ejbFPI.getInvPiByCriteria(actionForm.getCriteria(),
						new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						InvModPhysicalInventoryDetails mdetails = (InvModPhysicalInventoryDetails)i.next();

						InvFindPhysicalInventoryList invFPIList = new InvFindPhysicalInventoryList(actionForm,
								mdetails.getPiCode(),								
								Common.convertSQLDateToString(mdetails.getPiDate()),
								mdetails.getPiReferenceNumber(),
								mdetails.getPiDescription(),
								mdetails.getPiAdLvCategory(),
								mdetails.getPiLocName());
						
						actionForm.saveInvFPIList(invFPIList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findPhysicalInventory.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindPhysicalInventoryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invFindPhysicalInventory");
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                       
				
				return(mapping.findForward("invFindPhysicalInventory"));
				
/*******************************************************
 -- Inv FPI Close Action --
 *******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 -- Inv FPI Open Action --
 *******************************************************/
				
			} else if (request.getParameter("invFPIList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				InvFindPhysicalInventoryList invFPIList =
					actionForm.getInvFPIByIndex(actionForm.getRowSelected());
				
				String path = "/invPhysicalInventoryEntry.do?forward=1" +
				"&physicalInventoryCode=" + invFPIList.getPhysicalInventoryCode();
				
				return(new ActionForward(path));
				
/*******************************************************
 -- Inv FPI Load Action --
 *******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.clearInvFPIList();
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearCategoryList();           	
	            	
	            	list = ejbFPI.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
					
					actionForm.clearLocationList();
					
					list = ejbFPI.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}			
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvFindItemLocationAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				} 
				
				
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setReferenceNumber(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setCategory(Constants.GLOBAL_BLANK);
					actionForm.setLocation(Constants.GLOBAL_BLANK);
					actionForm.setOrderBy(Constants.GLOBAL_BLANK);
				    
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				} else {
					
					try {
						
						actionForm.clearInvFPIList();
						
						ArrayList newList = ejbFPI.getInvPiByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						Iterator j = newList.iterator();
						
						while (j.hasNext()) {
							
							InvModPhysicalInventoryDetails mdetails = (InvModPhysicalInventoryDetails)j.next();

							InvFindPhysicalInventoryList invFPIList = new InvFindPhysicalInventoryList(actionForm,
									mdetails.getPiCode(),								
									Common.convertSQLDateToString(mdetails.getPiDate()),
									mdetails.getPiReferenceNumber(),
									mdetails.getPiDescription(),
									mdetails.getPiAdLvCategory(),
									mdetails.getPiLocName());
							
							actionForm.saveInvFPIList(invFPIList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
		                   	
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findPhysicalInventory.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in InvFindPhysicalInventoryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
				}
				
				return(mapping.findForward("invFindPhysicalInventory"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvFindPhysicalInventoryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}