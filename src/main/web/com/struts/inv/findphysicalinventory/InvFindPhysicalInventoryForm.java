package com.struts.inv.findphysicalinventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvFindPhysicalInventoryForm extends ActionForm implements Serializable {
	
	private String referenceNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String location = null; 
	private ArrayList locationList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList(); 
	private String category = null;
	private ArrayList categoryList = new ArrayList(); 
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFPIList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindPhysicalInventoryList getInvFPIByIndex(int index) {
		
		return((InvFindPhysicalInventoryList)invFPIList.get(index));
		
	}
	
	public Object[] getInvFPIList() {
		
		return invFPIList.toArray();
		
	}
	
	public int getInvFPIListSize() {
		
		return invFPIList.size();
		
	}
	
	public void saveInvFPIList(Object newInvFPIList) {
		
		invFPIList.add(newInvFPIList);
		
	}
	
	public void clearInvFPIList() {
		
		invFPIList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFPIList, boolean isEdit) {
		
		this.rowSelected = invFPIList.indexOf(selectedInvFPIList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return categoryList;
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		showDetailsButton = null;
		hideDetailsButton = null;
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("REFERENCE NUMBER");
	    	orderByList.add("CATEGORY");
	    	orderByList.add("LOCATION");
	    	
		  }		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if (!Common.validateDateFormat(dateFrom)) {
			    	
				errors.add("dateFrom",
					new ActionMessage("findPhysicalInventory.error.dateFromInvalid"));
			    		
			}         
			    		     					
			if (!Common.validateDateFormat(dateTo)) {
		    	
				errors.add("dateTo",
					new ActionMessage("findPhysicalInventory.error.dateToInvalid"));
		    		
			}
			
		}
		
		return errors;
		
	}
	
}