package com.struts.inv.findphysicalinventory;

import java.io.Serializable;

public class InvFindPhysicalInventoryList implements Serializable {
	
	private Integer physicalInventoryCode = null;
	private String date = null;
	private String referenceNumber = null;
	private String description = null;
	private String category = null;
	private String location = null;
	
	private String openButton = null;
	
	private InvFindPhysicalInventoryForm parentBean;
	
	public InvFindPhysicalInventoryList(InvFindPhysicalInventoryForm parentBean,
			Integer physicalInventoryCode,
			String date,
			String referenceNumber,
			String description,
			String category,
			String location) {
		
		this.parentBean = parentBean;
		this.physicalInventoryCode = physicalInventoryCode;
		this.date = date;
		this.referenceNumber = referenceNumber;
		this.description = description;
		this.category = category;
		this.location = location;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getPhysicalInventoryCode() {
		
		return physicalInventoryCode;
		
	}
	
	public String getDate() {
		
		return date;
		
	}	
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
		
}