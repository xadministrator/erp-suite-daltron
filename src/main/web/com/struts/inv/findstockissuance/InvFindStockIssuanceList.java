package com.struts.inv.findstockissuance;

import java.io.Serializable;

public class InvFindStockIssuanceList implements Serializable {
	
	private Integer stockIssuance = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
	
	private String openButton = null;
	
	private InvFindStockIssuanceForm parentBean;
	
	public InvFindStockIssuanceList(InvFindStockIssuanceForm parentBean,
			Integer stockIssuance,
			String date,
			String documentNumber,
			String referenceNumber,
			String description) {
		
		this.parentBean = parentBean;
		this.stockIssuance = stockIssuance;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.description = description;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getStockIssuanceCode() {
		
		return stockIssuance;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}

	public String getDescription() {
		
		return description;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
}