package com.struts.inv.findstocktransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvFindStockTransferForm extends ActionForm implements Serializable {
        	
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String posted = null;
	private ArrayList postedList = new ArrayList();  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList invFSTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public InvFindStockTransferList getInvFSTByIndex(int index) {
		
		return((InvFindStockTransferList)invFSTList.get(index));
		
	}
	
	public Object[] getInvFSTList() {
		
		return invFSTList.toArray();
		
	}
	
	public int getInvFSTListSize() {
		
		return invFSTList.size();
		
	}
	
	public void saveInvFSTList(Object newInvFSTList) {
		
		invFSTList.add(newInvFSTList);
		
	}
	
	public void clearInvFSTList() {
		
		invFSTList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvFSTList, boolean isEdit) {
		
		this.rowSelected = invFSTList.indexOf(selectedInvFSTList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}	
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public ArrayList getApprovalStatusList() {
		
		return approvalStatusList;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}   	
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {			
		
	    approvalStatusList.clear();
	    approvalStatusList.add(Constants.GLOBAL_BLANK);
	    approvalStatusList.add("DRAFT");
	    approvalStatusList.add("APPROVED");
	    approvalStatusList.add("N/A");
	    approvalStatusList.add("PENDING");
	         
	    postedList.clear();
	    postedList.add(Constants.GLOBAL_BLANK);
	    postedList.add(Constants.GLOBAL_YES);
	    postedList.add(Constants.GLOBAL_NO);
	    
		showDetailsButton = null;
		hideDetailsButton = null;
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("REFERENCE NUMBER");
	    	orderByList.add("DOCUMENT NUMBER");
	    	
		}		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if (!Common.validateDateFormat(dateFrom)) {
			    	
				errors.add("dateFrom",
					new ActionMessage("findStockTransfer.error.dateFromInvalid"));
			    		
			}         
			    		     					
			if (!Common.validateDateFormat(dateTo)) {
		    	
				errors.add("dateTo",
					new ActionMessage("findStockTransfer.error.dateToInvalid"));
		    		
			}
			
		}
		
		return errors;
		
	}
	
}
