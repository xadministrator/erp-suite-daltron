package com.struts.inv.findstocktransfer;

import java.io.Serializable;

public class InvFindStockTransferList implements Serializable {
    
    private Integer stockTransferCode = null;	
	private String documentNumber = null;
	private String referenceNumber = null;	
	private String date = null;
	
	private String openButton = null;
	
	private InvFindStockTransferForm parentBean;
	
	public InvFindStockTransferList(InvFindStockTransferForm parentBean,
			Integer stockTransferCode,
			String date,			
			String documentNumber,
			String referenceNumber) {
		
		this.parentBean = parentBean;
		this.stockTransferCode = stockTransferCode;
		this.date = date;		
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;		

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getStockTransferCode() {
		
		return stockTransferCode;
		
	}
			
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}		
	
	public String getDate() {
		
		return date;
		
	}

}
