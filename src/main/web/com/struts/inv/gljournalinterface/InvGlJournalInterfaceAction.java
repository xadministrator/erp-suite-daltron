package com.struts.inv.gljournalinterface;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.InvGlJournalInterfaceController;
import com.ejb.txn.InvGlJournalInterfaceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

public final class InvGlJournalInterfaceAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvGlJournalInterfaceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         InvGlJournalInterfaceForm actionForm = (InvGlJournalInterfaceForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.INV_GL_JOURNAL_INTERFACE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invGlJournalInterface");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize InvGlJournalInterfaceController EJB
*******************************************************/

         InvGlJournalInterfaceControllerHome homeGJI = null;
         InvGlJournalInterfaceController ejbGJI = null;

         try {

            homeGJI = (InvGlJournalInterfaceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvGlJournalInterfaceControllerEJB", InvGlJournalInterfaceControllerHome.class);      
                
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvGlJournalInterfaceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbGJI = homeGJI.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvGlJournalInterfaceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         long importedJournals = 0L;

/*******************************************************
   -- Inv GJI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            try {
            	
            	importedJournals = ejbGJI.executeInvGlJriRun(
            		Common.convertStringToSQLDate(actionForm.getDateFrom()),
            		Common.convertStringToSQLDate(actionForm.getDateTo()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
                actionForm.setImportedJournals("IMPORTED JOURNALS : " + importedJournals);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvGlJournalInterfaceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

/*******************************************************
   -- Inv GJI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv GJI Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   
               }
            }

            actionForm.reset(mapping, request);

            return(mapping.findForward("invGlJournalInterface"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvGlJournalInterfaceAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}