package com.struts.inv.gljournalinterface;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvGlJournalInterfaceForm extends ActionForm implements Serializable {

   private String dateFrom = null;
   private String dateTo = null;
   private String importedJournals = null;

   private String goButton = null;
   private String closeButton = null;
   private String pageState = new String();
   private String userPermission = new String();
   private String txnStatus = new String();

   public void setGoButton(String goButton) {

      this.goButton = goButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getDateFrom() {

      return dateFrom;

   }

   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }

   public String getDateTo() {

      return dateTo;

   }

   public void setDateTo(String dateTo) {

      this.dateTo = dateTo;

   }
   
   public String getImportedJournals() {
   	
   	  return importedJournals;
   	
   }
   
   public void setImportedJournals(String importedJournals) {
   	
   	  this.importedJournals = importedJournals;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      dateFrom = null;
      dateTo = null;
      goButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (Common.validateRequired(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("invGlJournalInterface.error.dateFromRequired"));

         }

         if (Common.validateRequired(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("invGlJournalInterface.error.dateToRequired"));

         }
         
         if (!Common.validateDateFormat(dateFrom)) {
         	
         	errors.add("dateFrom",
               new ActionMessage("invGlJournalInterface.error.dateFromInvalid"));
         	
         }
         
         if (!Common.validateDateFormat(dateTo)) {
         	
         	errors.add("dateFrom",
               new ActionMessage("invGlJournalInterface.error.dateToInvalid"));
         	
         }

      }
      return errors;

   }
}