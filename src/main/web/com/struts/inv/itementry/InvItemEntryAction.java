package com.struts.inv.itementry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvAccumulatedDepreciationAccountNotFoundException;
import com.ejb.exception.InvDepreciationAccountNotFoundException;
import com.ejb.exception.InvFixedAssetAccountNotFoundException;
import com.ejb.exception.InvFixedAssetInformationNotCompleteException;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvItemDetails;
import com.util.InvModItemDetails;

public final class InvItemEntryAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try {

/*******************************************************
Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("InvItemEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			InvItemEntryForm actionForm = (InvItemEntryForm)form;

			String ieParam = Common.getUserPermission(user, Constants.INV_ITEM_ENTRY_ID);

			if (ieParam != null) {

				if (ieParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));

						return mapping.findForward("invItemEntry");
					}

				}

				actionForm.setUserPermission(ieParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

/*******************************************************
Initialize InvItemEntryController EJB
*******************************************************/

			InvItemEntryControllerHome homeIE = null;
			InvItemEntryController ejbIE = null;
			
			 AdLogControllerHome homeLOG = null;
	         AdLogController ejbLOG = null;

			try {

				homeIE = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

				
				 homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
		                    lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);

			} catch (NamingException e) {

				if (log.isInfoEnabled()) {

					log.info("NamingException caught in InvItemEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return mapping.findForward("cmnErrorPage");

			}

			try {

				ejbIE = homeIE.create();
				
				ejbLOG = homeLOG.create();

			} catch (CreateException e) {

				if (log.isInfoEnabled()) {

					log.info("CreateException caught in InvItemEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}
				return mapping.findForward("cmnErrorPage");

			}

			ActionErrors errors = new ActionErrors();

/*******************************************************
   Call InvItemEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfInvQuantityPrecisionUnit
*******************************************************/

			short precisionUnit = 0;
			short quantityPrecisionUnit = 0;

			try {

				precisionUnit = ejbIE.getGlFcPrecisionUnit(user.getCmpCode());
				quantityPrecisionUnit = ejbIE.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());

			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}

/*******************************************************
   -- Inv IE Save Action --
*******************************************************/

			if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


				System.out.println("pasok");
				if(request.getParameter("isUploading") !=null){





				}

				



				InvItemDetails details = new InvItemDetails();
				details.setIiCode(actionForm.getItemCode());
				details.setIiName(actionForm.getItemName());
				details.setIiDescription(actionForm.getDescription());
				details.setIiPartNumber(actionForm.getPartNumber());
				details.setIiShortName(actionForm.getShortName());
				details.setIiBarCode1(actionForm.getBarCode1());
				details.setIiBarCode2(actionForm.getBarCode2());
				details.setIiBarCode3(actionForm.getBarCode3());
				details.setIiBrand(actionForm.getBrand());
				details.setIiClass("Stock");
				details.setIiAdLvCategory(actionForm.getCategory());
				details.setIiCostMethod(actionForm.getCostMethod());
				details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit));
				details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), precisionUnit));
				details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), precisionUnit));
				details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), precisionUnit));
				details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				details.setIiVirtualStore(Common.convertBooleanToByte(actionForm.getVirtualStore()));
				details.setIiDoneness(actionForm.getDoneness());
				details.setIiSidings(actionForm.getSidings());
				details.setIiRemarks(actionForm.getRemarks());
				details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
				details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
				details.setIiServices(Common.convertBooleanToByte(actionForm.getServices()));
				details.setIiJobServices(Common.convertBooleanToByte(actionForm.getJobServices()));
				details.setIiIsVatRelief(Common.convertBooleanToByte(actionForm.getIsVatRelief()));
				details.setIiIsTax(Common.convertBooleanToByte(actionForm.getIsTax()));
				details.setIiIsProject(Common.convertBooleanToByte(actionForm.getIsProject()));
				details.setIiMarket(actionForm.getMarket());
				details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
				details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
				details.setIiUmcPackaging(actionForm.getPackaging());
				details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
				details.setIiFixedAsset(Common.convertBooleanToByte(actionForm.getFixedAsset()));
				details.setIiDateAcquired(Common.convertStringToSQLDate(actionForm.getDateAcquired()));
				details.setIiAcquisitionCost(actionForm.getAcquisitionCost());
				details.setIiLifeSpan(actionForm.getLifeSpan());
				details.setIiResidualValue(actionForm.getResidualValue());
				details.setIiCondition(actionForm.getCondition());
				details.setIiTaxCode(actionForm.getTaxCode());

				//details.setGlCoaFixedAssetAccount(actionForm.getFixedAssetAccount());
				//details.setGlCoaDepreciationAccount(actionForm.getDepreciationAccount());
				details.setIiTraceMisc(Common.convertBooleanToByte(actionForm.getTrackMisc()));
				System.out.println("SUNDAY---------->");
				System.out.println("SUNDAY---------->"+Common.convertBooleanToByte(actionForm.getScSunday()));
				details.setIiScSunday(Common.convertBooleanToByte(actionForm.getScSunday()));
				details.setIiScMonday(Common.convertBooleanToByte(actionForm.getScMonday()));
				details.setIiScTuesday(Common.convertBooleanToByte(actionForm.getScTuesday()));
				details.setIiScWednesday(Common.convertBooleanToByte(actionForm.getScWednesday()));
				details.setIiScThursday(Common.convertBooleanToByte(actionForm.getScThursday()));
				details.setIiScFriday(Common.convertBooleanToByte(actionForm.getScFriday()));
				details.setIiScSaturday(Common.convertBooleanToByte(actionForm.getScSaturday()));
				details.setIiCreatedBy(user.getUserName());
				details.setIiDateCreated(new java.util.Date());

				details.setIiLastModifiedBy(user.getUserName());
				details.setIiDateLastModified(new java.util.Date());
				try {
					System.out.println("SUPPLIER =================== "+actionForm.getSupplier());
					Integer invItemCode = ejbIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getCustomer(), actionForm.getRetailUom(), actionForm.getDefaultLocation(),
							actionForm.getFixedAssetAccount(), actionForm.getDepreciationAccount(), actionForm.getAccumulatedDepreciationAccount(),
							actionForm.getTrackMisc(),user.getCmpCode());

					
					String actionValue = actionForm.getItemCode()!=null?"UPDATE":"SAVE";
					String logValue = "";
					
					if(actionValue.equals("SAVE")){
						
						logValue = "NEW ITEM " + details.getIiName();
						
					}else {
						logValue = "UPDATE ITEM CODE " + invItemCode + " FOR ITEM " + details.getIiName();
					}
					
				    ejbLOG.addAdLogEntry(invItemCode,"INV ITEM ENTRY",  new java.util.Date(),user.getUserName(),actionValue, logValue,  user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.itemNameAlreadyExist"));

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));

				}  catch (InvFixedAssetAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.fixedAssetAccountNotFound"));

				}catch (InvDepreciationAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.depreciationAccountNotFound"));

				}catch (InvFixedAssetInformationNotCompleteException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.InvFixedAssetInformationNotCompleteException"));

				}catch (InvAccumulatedDepreciationAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.AccumulatedDepreciationAccountNotFoundException"));

				}catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

/*******************************************************
 	-- Inv IE Price Levels Action --
*******************************************************/

			} else if(request.getParameter("priceLevelsButton") != null || request.getParameter("priceLevelsDateButton") != null) {

				if(actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

					InvItemDetails details = new InvItemDetails();
					details.setIiCode(actionForm.getItemCode());
					details.setIiName(actionForm.getItemName());
					details.setIiDescription(actionForm.getDescription());
					details.setIiPartNumber(actionForm.getPartNumber());
					details.setIiShortName(actionForm.getShortName());
					details.setIiBarCode1(actionForm.getBarCode1());
					details.setIiBarCode2(actionForm.getBarCode2());
					details.setIiBarCode3(actionForm.getBarCode3());
					details.setIiBrand(actionForm.getBrand());
					details.setIiClass("Stock");
					details.setIiAdLvCategory(actionForm.getCategory());
					details.setIiCostMethod(actionForm.getCostMethod());
					details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit));
					details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), precisionUnit));
					details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), precisionUnit));
					details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), precisionUnit));
					details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));
					details.setIiVirtualStore(Common.convertBooleanToByte(actionForm.getVirtualStore()));
					details.setIiDoneness(actionForm.getDoneness());
					details.setIiSidings(actionForm.getSidings());
					details.setIiRemarks(actionForm.getRemarks());
					details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
					details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
					details.setIiServices(Common.convertBooleanToByte(actionForm.getServices()));
					details.setIiJobServices(Common.convertBooleanToByte(actionForm.getJobServices()));
					details.setIiIsVatRelief(Common.convertBooleanToByte(actionForm.getIsVatRelief()));
					details.setIiIsTax(Common.convertBooleanToByte(actionForm.getIsTax()));
					details.setIiIsProject(Common.convertBooleanToByte(actionForm.getIsProject()));
					details.setIiMarket(actionForm.getMarket());
					details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
					details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
					details.setIiUmcPackaging(actionForm.getPackaging());
					details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
					details.setIiFixedAsset(Common.convertBooleanToByte(actionForm.getFixedAsset()));
					details.setIiDateAcquired(Common.convertStringToSQLDate(actionForm.getDateAcquired()));
					details.setIiAcquisitionCost(actionForm.getAcquisitionCost());
					details.setIiLifeSpan(actionForm.getLifeSpan());
					details.setIiResidualValue(actionForm.getResidualValue());
					details.setIiCondition(actionForm.getCondition());
					details.setIiTaxCode(actionForm.getTaxCode());

					details.setIiCreatedBy(user.getUserName());
					details.setIiDateCreated(new java.util.Date());

					details.setIiLastModifiedBy(user.getUserName());
					details.setIiDateLastModified(new java.util.Date());

					try {

						Integer itemCode = ejbIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getCustomer(), actionForm.getRetailUom(),
								actionForm.getDefaultLocation(), actionForm.getFixedAssetAccount(), actionForm.getDepreciationAccount(),
								actionForm.getAccumulatedDepreciationAccount(),actionForm.getTrackMisc(), user.getCmpCode());

						
						String actionValue = actionForm.getItemCode()!=null?"UPDATE":"SAVE";
						String logValue = "";
						
						if(actionValue.equals("SAVE")){
							
							logValue = "NEW ITEM " + details.getIiName();
							
						}else {
							logValue = "UPDATE ITEM CODE " + itemCode + " FOR ITEM " + details.getIiName();
						}
						
					    ejbLOG.addAdLogEntry(itemCode,"INV ITEM ENTRY",  new java.util.Date(),user.getUserName(),actionValue, logValue,  user.getCmpCode());
						
						
						
						actionForm.setItemCode(itemCode);

					} catch (GlobalRecordAlreadyExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyExist"));

					} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));

					}  catch (InvFixedAssetAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.fixedAssetAccountNotFound"));

					}catch (InvDepreciationAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.depreciationAccountNotFound"));

					}catch (InvFixedAssetInformationNotCompleteException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.InvFixedAssetInformationNotCompleteException"));

					}catch (InvAccumulatedDepreciationAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.AccumulatedDepreciationAccountNotFoundException"));

					}catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

				}
			
				String path = "";
			
				if(request.getParameter("priceLevelsButton") != null){
					 path = "/invPriceLevels.do?forward=1" +
					"&itemCode=" + String.valueOf(actionForm.getItemCode()) +
					"&transaction=ITEM_ENTRY" +
					"&itemDescription=" + actionForm.getDescription() +
					"&userPermission=" + actionForm.getUserPermission();
				}
				
				if(request.getParameter("priceLevelsDateButton") != null){
					 path = "/invPriceLevelsDate.do?forward=1" +
					"&itemCode=" + String.valueOf(actionForm.getItemCode()) +
					"&transaction=ITEM_ENTRY" +
					"&itemDescription=" + actionForm.getDescription() +
					"&userPermission=" + actionForm.getUserPermission();
				}
				

				return new ActionForward(path);

/*******************************************************
	-- Inv IE Conversion Action --
*******************************************************/

			} else if (request.getParameter("conversionButton") != null) {

				if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

					InvItemDetails details = new InvItemDetails();
					details.setIiCode(actionForm.getItemCode());
					details.setIiName(actionForm.getItemName());
					details.setIiDescription(actionForm.getDescription());
					details.setIiPartNumber(actionForm.getPartNumber());
					details.setIiShortName(actionForm.getBarCode1());
					details.setIiBarCode1(actionForm.getBarCode1());
					details.setIiBarCode2(actionForm.getBarCode2());
					details.setIiBarCode3(actionForm.getBarCode3());
					details.setIiBrand(actionForm.getBrand());
					details.setIiClass("Stock");
					details.setIiAdLvCategory(actionForm.getCategory());
					details.setIiCostMethod(actionForm.getCostMethod());
					details.setIiUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit));
					details.setIiPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), precisionUnit));
					details.setIiShippingCost(Common.convertStringMoneyToDouble(actionForm.getShippingCost(), precisionUnit));
					details.setIiSalesPrice(Common.convertStringMoneyToDouble(actionForm.getSalesPrice(), precisionUnit));
					details.setIiEnable(Common.convertBooleanToByte(actionForm.getEnable()));
					details.setIiVirtualStore(Common.convertBooleanToByte(actionForm.getVirtualStore()));
					details.setIiDoneness(actionForm.getDoneness());
					details.setIiSidings(actionForm.getSidings());
					details.setIiRemarks(actionForm.getRemarks());
					details.setIiServiceCharge(Common.convertBooleanToByte(actionForm.getServiceCharge()));
					details.setIiNonInventoriable(Common.convertBooleanToByte(actionForm.getNonInventoriable()));
					details.setIiServices(Common.convertBooleanToByte(actionForm.getServices()));
					details.setIiJobServices(Common.convertBooleanToByte(actionForm.getJobServices()));
					details.setIiIsVatRelief(Common.convertBooleanToByte(actionForm.getIsVatRelief()));
					details.setIiIsTax(Common.convertBooleanToByte(actionForm.getIsTax()));
					details.setIiIsProject(Common.convertBooleanToByte(actionForm.getIsProject()));
					details.setIiMarket(actionForm.getMarket());
					details.setIiEnablePo(Common.convertBooleanToByte(actionForm.getEnablePo()));
					details.setIiPoCycle(Common.convertStringToShort(actionForm.getPoCycle()));
					details.setIiUmcPackaging(actionForm.getPackaging());
					details.setIiOpenProduct(Common.convertBooleanToByte(actionForm.getOpenProduct()));
					details.setIiFixedAsset(Common.convertBooleanToByte(actionForm.getFixedAsset()));
					details.setIiDateAcquired(Common.convertStringToSQLDate(actionForm.getDateAcquired()));
					details.setIiAcquisitionCost(actionForm.getAcquisitionCost());
					details.setIiLifeSpan(actionForm.getLifeSpan());
					details.setIiResidualValue(actionForm.getResidualValue());
					details.setIiCondition(actionForm.getCondition());
					details.setIiTaxCode(actionForm.getTaxCode());
					details.setIiCreatedBy(user.getUserName());
					details.setIiDateCreated(new java.util.Date());

					details.setIiLastModifiedBy(user.getUserName());
					details.setIiDateLastModified(new java.util.Date());
					try {

						Integer itemCode = ejbIE.saveInvIiEntry(details, actionForm.getUnitMeasure(), actionForm.getSupplier(), actionForm.getCustomer(),
								actionForm.getRetailUom(), actionForm.getDefaultLocation(),
								actionForm.getFixedAssetAccount(), actionForm.getDepreciationAccount(),actionForm.getAccumulatedDepreciationAccount(),
								actionForm.getTrackMisc(), user.getCmpCode());
						
						String actionValue = actionForm.getItemCode()!=null?"UPDATE":"SAVE";
						String logValue = "";
						
						if(actionValue.equals("SAVE")){
							
							logValue = "NEW ITEM " + details.getIiName();
							
						}else {
							logValue = "UPDATE ITEM CODE " + itemCode + " FOR ITEM " + details.getIiName();
						}
						
					    ejbLOG.addAdLogEntry(itemCode,"INV ITEM ENTRY",  new java.util.Date(),user.getUserName(),actionValue, logValue,  user.getCmpCode());
						
						
						actionForm.setItemCode(itemCode);

					} catch (GlobalRecordAlreadyExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyExist"));

					} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.itemNameAlreadyAssigned"));

					} catch (InvFixedAssetAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.fixedAssetAccountNotFound"));

					}catch (InvDepreciationAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.depreciationAccountNotFound"));

					}catch (InvFixedAssetInformationNotCompleteException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.InvFixedAssetInformationNotCompleteException"));

					}catch (InvAccumulatedDepreciationAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("itemEntry.error.AccumulatedDepreciationAccountNotFoundException"));

					}catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

				}

				// create path
				String path = "/invUnitOfMeasureConversion.do?forward=1" +
				"&itemCode=" + actionForm.getItemCode() +
				"&itemName=" + actionForm.getItemName() +
				"&itemDescription=" + actionForm.getDescription() +
				"&unitMeasure=" + actionForm.getUnitMeasure() +
				"&itemClass=Stock" +
				"&userPermission=" + actionForm.getUserPermission();

				return(new ActionForward(path));

/*******************************************************
-- Inv IE Delete Action --
*******************************************************/

			} else if(request.getParameter("deleteButton") != null) {

				try {

					ejbIE.deleteInvIiEntry(actionForm.getItemCode(), user.getCmpCode());

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.itemAlreadyAssigned"));

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.itemAlreadyDeleted"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}


/*******************************************************
    -- Inv IE Unit Measure Action --
*******************************************************/

			} else if(!Common.validateRequired(request.getParameter("isUnitMeasureEntered"))) {

			    try {

			        if (!actionForm.getUnitMeasure().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

			            actionForm.clearPackagingList();
			            actionForm.clearRetailUomList();
						System.out.println("pasoks 1");
						ArrayList list = ejbIE.getInvUomAllByUnitMeasureClass(actionForm.getUnitMeasure(), user.getCmpCode());
						System.out.println("pasoks 2");
						if (list == null || list.size() == 0) {

							actionForm.setPackagingList(Constants.GLOBAL_NO_RECORD_FOUND);
							actionForm.setRetailUomList(Constants.GLOBAL_NO_RECORD_FOUND);

						} else {
							System.out.println("pasoks 3");
							Iterator i = list.iterator();

							while (i.hasNext()) {

								String uom = (String)i.next();
								actionForm.setPackagingList(uom);
								actionForm.setRetailUomList(uom);

							}

						}

			        }

			    } catch (EJBException ex) {

			        if (log.isInfoEnabled()) {

			            log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			            return mapping.findForward("cmnErrorPage");

			        }

			    }

			    return(mapping.findForward("invItemEntry"));

/*******************************************************
   -- Inv IE Close Action --
*******************************************************/

			} else if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

/*******************************************************
-- Inv IE Item Entered Action --
*******************************************************/

			} else if (!Common.validateRequired(request.getParameter("isItemEntered"))) {

				return(mapping.findForward("invItemEntry"));

			}

/*******************************************************
-- Inv IE Load Action --
*******************************************************/

			if (ieParam != null) {

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invItemEntry");

				}

				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
					saveErrors(request, new ActionMessages(errors));

					return mapping.findForward("cmnMain");

				}


				ArrayList list = null;
				Iterator i = null;

				try {

	            	actionForm.clearCategoryList();

	            	list = ejbIE.getAdLvInvItemCategoryAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCategoryList((String)i.next());

	            		}

	            	}

	            	actionForm.clearTaxCodeList();

	                 list = ejbIE.getArTcAll(user.getCmpCode());

	                 if (list == null || list.size() == 0) {

	                     actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

	                 } else {

	                     i = list.iterator();

	                     while (i.hasNext()) {

	                         actionForm.setTaxCodeList((String) i.next());

	                     }

	                 }

					actionForm.clearUnitMeasureList();

					list = ejbIE.getInvUomAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setUnitMeasureList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setUnitMeasureList((String)i.next());

						}

					}

					actionForm.clearDefaultLocationList();

					list = ejbIE.getInvLocAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setDefaultLocation(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setDefaultLocationList((String)i.next());

						}

					}

	            	actionForm.clearDonenessList();

	            	list = ejbIE.getAdLvInvDonenessAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setDonenessList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setDonenessList((String)i.next());

	            		}

	            	}

	            	actionForm.clearSupplierList();

	            	list = ejbIE.getApSplAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setSupplierList((String)i.next());

	            		}

	            	}


	            	actionForm.clearCustomerList();

	            	list = ejbIE.getArCstAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCustomerList((String)i.next());

	            		}

	            	}

				    actionForm.clearPackagingList();
				    actionForm.setPackagingList("Select Unit Measure first");
				    actionForm.clearRetailUomList();
				    actionForm.setRetailUomList("Select Unit Measure first");


				    if (request.getParameter("forward") != null) {

						InvModItemDetails iiDetails = ejbIE.getInvIiByIiCode(
								new Integer(request.getParameter("iiCode")), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setItemCode(iiDetails.getIiCode());
						actionForm.setItemName(iiDetails.getIiName());
						actionForm.setDescription(iiDetails.getIiDescription());
						actionForm.setPartNumber(iiDetails.getIiPartNumber());
						actionForm.setShortName(iiDetails.getIiShortName());
						actionForm.setBarCode1(iiDetails.getIiBarCode1());
						actionForm.setBarCode2(iiDetails.getIiBarCode2());
						actionForm.setBarCode3(iiDetails.getIiBarCode3());
						actionForm.setBrand(iiDetails.getIiBrand());

						if(!actionForm.getUnitMeasureList().contains(iiDetails.getIiUomName())) {

							if (actionForm.getUnitMeasureList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

								actionForm.clearUnitMeasureList();

							}
							actionForm.setUnitMeasureList(iiDetails.getIiUomName());

						}
						actionForm.setUnitMeasure(iiDetails.getIiUomName());

						actionForm.setCategory(iiDetails.getIiAdLvCategory());
						actionForm.setCostMethod(iiDetails.getIiCostMethod());
						actionForm.setUnitCost(Common.convertDoubleToStringMoney(iiDetails.getIiUnitCost(), precisionUnit));
						actionForm.setAverageCost(Common.convertDoubleToStringMoney(iiDetails.getIiAveCost(), precisionUnit));
						actionForm.setPercentMarkup(Common.convertDoubleToStringMoney(iiDetails.getIiPercentMarkup(), precisionUnit));
						actionForm.setShippingCost(Common.convertDoubleToStringMoney(iiDetails.getIiShippingCost(), precisionUnit));
						actionForm.setSalesPrice(Common.convertDoubleToStringMoney(iiDetails.getIiSalesPrice(), precisionUnit));
						//actionForm.setGrossProfit(Common.convertDoubleToStringMoney(iiDetails.getIiAveCost() != 0 ? iiDetails.getIiSalesPrice() - (iiDetails.getIiShippingCost() + iiDetails.getIiAveCost()) : iiDetails.getIiSalesPrice() - (iiDetails.getIiShippingCost() + iiDetails.getIiUnitCost()), precisionUnit));
						actionForm.setGrossProfit(Common.convertDoubleToStringMoney(iiDetails.getIiSalesPrice() - (iiDetails.getIiShippingCost() + iiDetails.getIiUnitCost()), precisionUnit));


						actionForm.setEnable(Common.convertByteToBoolean(iiDetails.getIiEnable()));
						actionForm.setVirtualStore(Common.convertByteToBoolean(iiDetails.getIiVirtualStore()));

						if(!actionForm.getDonenessList().contains(iiDetails.getIiDoneness())) {

							if (actionForm.getDonenessList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

								actionForm.clearDonenessList();

							}
							actionForm.setDonenessList(iiDetails.getIiDoneness());

						}
						actionForm.setDoneness(iiDetails.getIiDoneness());

						actionForm.setSidings(iiDetails.getIiSidings());
						actionForm.setRemarks(iiDetails.getIiRemarks());
						actionForm.setServiceCharge(Common.convertByteToBoolean(iiDetails.getIiServiceCharge()));
						actionForm.setNonInventoriable(Common.convertByteToBoolean(iiDetails.getIiNonInventoriable()));
						actionForm.setServices(Common.convertByteToBoolean(iiDetails.getIiServices()));
						actionForm.setJobServices(Common.convertByteToBoolean(iiDetails.getIiJobServices()));
						actionForm.setIsVatRelief(Common.convertByteToBoolean(iiDetails.getIiIsVatRelief()));
						actionForm.setIsTax(Common.convertByteToBoolean(iiDetails.getIiIsTax()));
						actionForm.setIsProject(Common.convertByteToBoolean(iiDetails.getIiIsProject()));
						actionForm.setMarket(iiDetails.getIiMarket());
						actionForm.setEnablePo(Common.convertByteToBoolean(iiDetails.getIiEnablePo()));
						actionForm.setPoCycle(Common.convertShortToString(iiDetails.getIiPoCycle()));


						actionForm.setSupplier(iiDetails.getIiSplSpplrCode());
						actionForm.setCustomer(iiDetails.getIiCstCustomerCode());
						actionForm.clearPackagingList();
						actionForm.clearRetailUomList();

						// get umc packaging list
						list = ejbIE.getInvUomAllByUnitMeasureClass(iiDetails.getIiUomName(), user.getCmpCode());

						if (list != null || list.size() > 0) {

							i = list.iterator();

							while (i.hasNext()) {

								String uom = (String)i.next();
								actionForm.setPackagingList(uom);
								actionForm.setRetailUomList(uom);

							}

						}

						if(!actionForm.getPackagingList().contains(iiDetails.getIiUmcPackaging())) {

							actionForm.setPackagingList(iiDetails.getIiUmcPackaging());

						}

						if(!actionForm.getRetailUomList().contains(iiDetails.getIiRetailUomName())) {

							actionForm.setRetailUomList(iiDetails.getIiRetailUomName());

						}

						if(!actionForm.getDefaultLocationList().contains(iiDetails.getIiDefaultLocationName())) {

							actionForm.setDefaultLocationList(iiDetails.getIiDefaultLocationName());

						}

						actionForm.setPackaging(iiDetails.getIiUmcPackaging());
						actionForm.setRetailUom(iiDetails.getIiRetailUomName());
						actionForm.setDefaultLocation(iiDetails.getIiDefaultLocationName());
						actionForm.setOpenProduct(Common.convertByteToBoolean(iiDetails.getIiOpenProduct()));

						actionForm.setFixedAsset(Common.convertByteToBoolean(iiDetails.getIiFixedAsset()));
						actionForm.setDateAcquired(Common.convertSQLDateToString(iiDetails.getIiDateAcquired()));
						actionForm.setTrackMisc(Common.convertByteToBoolean(iiDetails.getIiTraceMisc()));

						actionForm.setScSunday(Common.convertByteToBoolean(iiDetails.getIiScSunday()));
						actionForm.setScMonday(Common.convertByteToBoolean(iiDetails.getIiScMonday()));
						actionForm.setScTuesday(Common.convertByteToBoolean(iiDetails.getIiScTuesday()));
						actionForm.setScWednesday(Common.convertByteToBoolean(iiDetails.getIiScWednesday()));
						actionForm.setScThursday(Common.convertByteToBoolean(iiDetails.getIiScThursday()));
						actionForm.setScFriday(Common.convertByteToBoolean(iiDetails.getIiScFriday()));
						actionForm.setScSaturday(Common.convertByteToBoolean(iiDetails.getIiScSaturday()));

						actionForm.setAcquisitionCost(iiDetails.getAcquisitionCost());
						actionForm.setLifeSpan(iiDetails.getLifeSpan());
						actionForm.setResidualValue(iiDetails.getResidualValue());
						System.out.println("Action iiDetails.getIiCondition(): " + iiDetails.getIiCondition());
						actionForm.setCondition(iiDetails.getIiCondition());
						//actionForm.setTaxCode(iiDetails.getIiTaxCode());

						 if (!actionForm.getTaxCodeList().contains(iiDetails.getIiTaxCode())) {

	                         if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

	                             actionForm.clearTaxCodeList();

	                         }
	                         actionForm.setTaxCodeList(iiDetails.getIiTaxCode());

	                     }
	                     actionForm.setTaxCode(iiDetails.getIiTaxCode());

						actionForm.setFixedAssetAccount(iiDetails.getFixedAssetAccount());
						actionForm.setFixedAssetAccountDescription(iiDetails.getFixedAssetAccountDescription());
						actionForm.setDepreciationAccount(iiDetails.getDepreciationAccount());
						actionForm.setDepreciationAccountDescription(iiDetails.getDepreciationAccountDescription());
						System.out.println("DA: " + iiDetails.getDepreciationAccountDescription());
						actionForm.setAccumulatedDepreciationAccount(iiDetails.getAccumulatedDepreciationAccount());
						System.out.println("AD: " + iiDetails.getAccumulatedDepreciationAccountDescription());
						actionForm.setAccumulatedDepreciationAccountDescription(iiDetails.getAccumulatedDepreciationAccountDescription());
						this.setFormProperties(actionForm);
						actionForm.setIsExist(true);
						
						actionForm.setCreatedBy(iiDetails.getIiCreatedBy());
						actionForm.setDateCreated(Common.convertSQLDateToString(iiDetails.getIiDateCreated()));
						actionForm.setLastModifiedBy(iiDetails.getIiLastModifiedBy());
						actionForm.setDateLastModified(Common.convertSQLDateToString(iiDetails.getIiDateLastModified()));
						
						return(mapping.findForward("invItemEntry"));

					}

					actionForm.setItemCode(null);
				//	actionForm.setIsExist(false);
					actionForm.setEnable(true);

				} catch (GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("itemEntry.error.noRecordFound"));

				} catch (EJBException ex) {

				    ex.printStackTrace();

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				} else {

					if ((request.getParameter("saveButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					}

				}

				actionForm.reset(mapping, request);
				actionForm.setItemCode(null);
				actionForm.setEnable(true);
				//actionForm.setCostMethod("Average");

				this.setFormProperties(actionForm);

							
				
				
				return(mapping.findForward("invItemEntry"));

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

		} catch(Exception e) {

/*******************************************************
System Failed: Forward to error page
*******************************************************/

			if (log.isInfoEnabled()) {

			    e.printStackTrace();

				log.info("Exception caught in InvItemEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}

			return mapping.findForward("cmnErrorPage");

		}
	}

	private void setFormProperties(InvItemEntryForm actionForm) {

		if (actionForm.getItemCode() == null) {

			actionForm.setShowDeleteButton(false);

		} else {

			actionForm.setShowDeleteButton(true);

		}

	}

}