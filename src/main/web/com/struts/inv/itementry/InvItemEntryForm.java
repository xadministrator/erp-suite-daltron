package com.struts.inv.itementry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvItemEntryForm extends ActionForm implements Serializable {

	private Integer itemCode = null;
	private String itemName = null;
	private String description = null;
	private String partNumber = null;
	private String shortName = null;
	private String barCode1 = null;
	private String barCode2 = null;
	private String barCode3 = null;
	private String brand = null;
	private String unitMeasure = null;
	private ArrayList unitMeasureList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String costMethod = null;
	private ArrayList costMethodList = new ArrayList();

	private String unitCost = null;
	private String averageCost = null;
	private String percentMarkup = null;
	private String shippingCost = null;
	private String salesPrice = null;
	private String grossProfit = null;
	private boolean enable = false;
	private boolean virtualStore = false;
	private boolean showDeleteButton = false;

	private String userPermission = new String();
	private String txnStatus = new String();

	private String remarks = null;
	private String sidings = null;
	private String doneness = null;
	private ArrayList donenessList = new ArrayList();
	private boolean serviceCharge = false;
	private boolean nonInventoriable = false;
	private boolean services = false;
	private boolean jobServices = false;
	private boolean isVatRelief = false;
	private boolean isTax = false;
	private boolean isProject = false;
	private String market = null;
	private boolean enablePo = false;
	private String poCycle = null;
	private String packaging = null;
	private ArrayList packagingList = new ArrayList();
	private String retailUom = null;
	private ArrayList retailUomList = new ArrayList();
	private String defaultLocation = null;
	private ArrayList defaultLocationList = new ArrayList();

	private String supplier = null;
	private ArrayList supplierList = new ArrayList();

	private String customer = null;
	private ArrayList customerList = new ArrayList();

	private boolean openProduct = false;
	private String condition = null;
	private ArrayList conditionList = new ArrayList();

	private String isUnitMeasureEntered = null;

	private boolean fixedAsset = false;
	private String dateAcquired = null;
	private double acquisitionCost = 0;
	private double ResidualValue = 0;
	private double lifeSpan = 0;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String fixedAssetAccount = null;
	private String fixedAssetAccountDescription = null;
	private String depreciationAccount = null;
	private String depreciationAccountDescription = null;
	private String accumulatedDepreciationAccount = null;
	private String accumulatedDepreciationAccountDescription = null;

	private boolean trackMisc = false;

	private boolean scSunday = false;
	private boolean scMonday = false;
	private boolean scTuesday = false;
	private boolean scWednesday = false;
	private boolean scThursday = false;
	private boolean scFriday = false;
	private boolean scSaturday = false;


	private FormFile uploadFile = null;
	private boolean createCategory = false;
	private boolean createUOM = false;
	private boolean createLocation = false;


	private boolean isExist = false;
	
	
	private String createdBy = null;
	private String dateCreated = null;
	
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	
	
	private String logEntry = null;

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public Integer getItemCode() {

		return itemCode;

	}

	public void setItemCode(Integer itemCode) {

		this.itemCode = itemCode;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public void setPartNumber(String partNumber) {

		this.partNumber = partNumber;

	}

	public String getShortName() {

		return shortName;

	}

	public void setShortName(String shortName) {

		this.shortName = shortName;

	}

	public String getBarCode1() {

		return barCode1;

	}

	public void setBarCode1(String barCode1) {

		this.barCode1 = barCode1;

	}

	public String getBarCode2() {

		return barCode2;

	}

	public void setBarCode2(String barCode2) {

		this.barCode2 = barCode2;

	}


	public String getBarCode3() {

		return barCode3;

	}

	public void setBarCode3(String barCode3) {

		this.barCode3 = barCode3;

	}


	public String getBrand() {

		return brand;

	}

	public void setBrand(String brand) {

		this.brand = brand;

	}

	public String getUnitMeasure() {

		return unitMeasure;

	}

	public void setUnitMeasure(String unitMeasure) {

		this.unitMeasure = unitMeasure;

	}

	public ArrayList getUnitMeasureList() {

		return unitMeasureList;

	}
	

	

	public void setUnitMeasureList(String unitMeasure) {

		unitMeasureList.add(unitMeasure);

	}

	public void clearUnitMeasureList() {

		unitMeasureList.clear();
		unitMeasureList.add(Constants.GLOBAL_BLANK);

	}

	public String getCategory() {

		return category;

	}

	public void setCategory(String category) {

		this.category = category;

	}

	public ArrayList getCategoryList() {

		return categoryList;

	}

	public void setCategoryList(String category) {

		categoryList.add(category);

	}

	public void clearCategoryList() {

		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);

	}

	public String getCostMethod() {

		return costMethod;

	}

	public void setCostMethod(String costMethod) {

		this.costMethod = costMethod;

	}

	public ArrayList getCostMethodList() {

		return costMethodList;

	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAverageCost() {

		return averageCost;

	}

	public void setAverageCost(String averageCost) {

		this.averageCost = averageCost;

	}

	public String getPercentMarkup() {

		return percentMarkup;

	}

	public void setPercentMarkup(String percentMarkup) {

		this.percentMarkup = percentMarkup;

	}

	public String getShippingCost() {

		return shippingCost;

	}

	public void setShippingCost(String shippingCost) {

		this.shippingCost = shippingCost;

	}

	public String getSalesPrice() {

		return salesPrice;

	}

	public void setSalesPrice(String salesPrice) {

		this.salesPrice = salesPrice;

	}

	public String getGrossProfit() {

		return grossProfit;

	}

	public void setGrossProfit(String grossProfit) {

		this.grossProfit = grossProfit;

	}

	public boolean getEnable() {

		return enable;

	}

	public void setEnable(boolean enable) {

		this.enable = enable;

	}

	public boolean getVirtualStore() {

		return virtualStore;

	}

	public void setVirtualStore(boolean virtualStore) {

		this.virtualStore = virtualStore;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public String getDoneness() {

		return doneness;

	}

	public void setDoneness(String doneness) {

		this.doneness = doneness;

	}

	public ArrayList getDonenessList() {

		return donenessList;

	}

	public void setDonenessList(String doneness) {

		donenessList.add(doneness);

	}

	public void clearDonenessList() {

		donenessList.clear();
		donenessList.add(Constants.GLOBAL_BLANK);

	}

	public String getRemarks() {

		return remarks;

	}

	public void setRemarks(String remarks) {

		this.remarks = remarks;

	}

	public String getSidings() {

		return sidings;

	}

	public void setSidings(String sidings) {

		this.sidings = sidings;

	}

	public boolean getServiceCharge() {

		return serviceCharge;

	}

	public void setServiceCharge(boolean serviceCharge) {

		this.serviceCharge = serviceCharge;

	}

	public boolean getNonInventoriable() {

		return nonInventoriable;

	}

	public void setNonInventoriable(boolean nonInventoriable) {

		this.nonInventoriable = nonInventoriable;

	}

	public boolean getServices() {

		return services;

	}

	public void setServices(boolean services) {

		this.services = services;

	}
	
	public boolean getJobServices() {

		return jobServices;

	}

	public void setJobServices(boolean jobServices) {

		this.jobServices = jobServices;

	}

public boolean getIsVatRelief() {

		return isVatRelief;

	}

	public void setIsVatRelief(boolean isVatRelief) {

		this.isVatRelief = isVatRelief;

	}


public boolean getIsTax() {

		return isTax;

	}

	public void setIsTax(boolean isTax) {

		this.isTax = isTax;

	}


public boolean getIsProject() {

		return isProject;

	}

	public void setIsProject(boolean isProject) {

		this.isProject = isProject;

	}

	public String getSupplier() {

		return supplier;

	}

	public void setSupplier(String supplier) {

		this.supplier = supplier;

	}

	public ArrayList getSupplierList() {

		return supplierList;

	}

	public void setSupplierList(String supplier) {

		supplierList.add(supplier);

	}

	public void clearSupplierList() {

		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);

	}


	public void setIsExist(boolean isExist) {

		this.isExist = isExist;

	}

	public boolean getIsExist() {

		return isExist;

	}

	public String getCustomer() {

		return customer;

	}

	public void setCustomer(String customer) {

		this.customer = customer;

	}

	public ArrayList getCustomerList() {

		return customerList;

	}

	public void setCustomerList(String customer) {

		customerList.add(customer);

	}

	public void clearCustomerList() {

		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);

	}



	public String getMarket() {

	    return market;

	}

	public void setMarket(String market) {

	    this.market = market;

	}

	public String getPoCycle() {

	    return poCycle;

	}

	public void setPoCycle(String poCycle) {

	    this.poCycle = poCycle;

	}

	public String getPackaging() {

	    return packaging;

	}

	public void setPackaging(String packaging) {

	    this.packaging = packaging;

	}

	public ArrayList getPackagingList() {

	    return packagingList;

	}

	public void setPackagingList(String packaging) {

	    packagingList.add(packaging);

	}

	public void clearPackagingList() {

	    packagingList.clear();
	    packagingList.add(Constants.GLOBAL_BLANK);

	}

	public String getRetailUom() {

	    return retailUom;

	}

	public void setRetailUom(String retailUom) {

	    this.retailUom = retailUom;

	}

	public ArrayList getRetailUomList() {

	    return retailUomList;

	}

	public void setRetailUomList(String retailUom) {

	    retailUomList.add(retailUom);

	}

	public void clearRetailUomList() {

		retailUomList.clear();
		retailUomList.add(Constants.GLOBAL_BLANK);

	}

	public String getDefaultLocation() {

	    return defaultLocation;

	}

	public void setDefaultLocation(String defaultLocation) {

	    this.defaultLocation = defaultLocation;

	}

	public ArrayList getDefaultLocationList() {

	    return defaultLocationList;

	}

	public void setDefaultLocationList(String defaultLocation) {

		defaultLocationList.add(defaultLocation);

	}

	public void clearDefaultLocationList() {

		defaultLocationList.clear();
		defaultLocationList.add(Constants.GLOBAL_BLANK);

	}

	public String getIsUnitMeasureEntered() {

	    return isUnitMeasureEntered;

	}

	public void setIsUnitMeasureEntered(String isUnitMeasureEntered) {

	    this.isUnitMeasureEntered = isUnitMeasureEntered;

	}

	public boolean getEnablePo() {

	    return enablePo;

	}

	public void setEnablePo(boolean enablePo) {

	    this.enablePo = enablePo;

	}

	public boolean getOpenProduct() {

	    return openProduct;

	}

	public void setOpenProduct(boolean openProduct) {

	    this.openProduct = openProduct;

	}

	public boolean getFixedAsset() {

		return fixedAsset;
	}

	public void setFixedAsset(boolean fixedAsset) {

		this.fixedAsset = fixedAsset;
	}

	public String getDateAcquired() {

		return dateAcquired;
	}

	public void setDateAcquired(String dateAcquired) {

		this.dateAcquired = dateAcquired;
	}

	public double getAcquisitionCost() {

		return acquisitionCost;
	}

	public void setAcquisitionCost(double acquisitionCost) {

		this.acquisitionCost = acquisitionCost;
	}

	public double getLifeSpan() {

		return lifeSpan;
	}

	public void setLifeSpan(double lifeSpan) {

		this.lifeSpan = lifeSpan;
	}

	public double getResidualValue() {

		return ResidualValue;
	}

	public void setResidualValue(double ResidualValue) {

		this.ResidualValue = ResidualValue;
	}


	public String getCondition() {

		return condition;

	}

	public void setCondition(String condition) {

		this.condition = condition;

	}

	public ArrayList getConditionList() {

		return conditionList;

	}

	public void setConditionList(String condition) {

		conditionList.add(condition);

	}

	public void clearConditionList() {

		conditionList.clear();
		conditionList.add(Constants.GLOBAL_BLANK);

	}

	public String getFixedAssetAccount() {

		return fixedAssetAccount;
	}

	public void setFixedAssetAccount(String fixedAssetAccount) {

		this.fixedAssetAccount = fixedAssetAccount;
	}

	public String getFixedAssetAccountDescription() {
		return fixedAssetAccountDescription;
	}

	public void setFixedAssetAccountDescription(String fixedAssetAccountDescription) {
		this.fixedAssetAccountDescription = fixedAssetAccountDescription;
	}

	public String getDepreciationAccount() {

		return depreciationAccount;
	}

	public void setDepreciationAccount(String depreciationAccount) {

		this.depreciationAccount = depreciationAccount;
	}

	public String getDepreciationAccountDescription() {
		return depreciationAccountDescription;
	}

	public void setDepreciationAccountDescription(String depreciationAccountDescription) {
		this.depreciationAccountDescription = depreciationAccountDescription;
	}

	public String getAccumulatedDepreciationAccount() {

		return accumulatedDepreciationAccount;
	}

	public void setAccumulatedDepreciationAccount(String accumulatedDepreciationAccount) {

		this.accumulatedDepreciationAccount = accumulatedDepreciationAccount;
	}

	public String getAccumulatedDepreciationAccountDescription() {
		return accumulatedDepreciationAccountDescription;
	}

	public void setAccumulatedDepreciationAccountDescription(String accumulatedDepreciationAccountDescription) {
		this.accumulatedDepreciationAccountDescription = accumulatedDepreciationAccountDescription;
	}


	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getTrackMisc() {

		return trackMisc;
	}

	public void setTrackMisc(boolean trackMisc) {

		this.trackMisc = trackMisc;
	}


	public boolean getScSunday() {

		return scSunday;
	}

	public void setScSunday(boolean scSunday) {

		this.scSunday = scSunday;
	}

	public boolean getScMonday() {

		return scMonday;
	}

	public void setScMonday(boolean scMonday) {

		this.scMonday = scMonday;
	}

	public boolean getScTuesday() {

		return scTuesday;
	}

	public void setScTuesday(boolean scTuesday) {

		this.scTuesday = scTuesday;
	}

	public boolean getScWednesday() {

		return scWednesday;
	}

	public void setScWednesday(boolean scWednesday) {

		this.scWednesday = scWednesday;
	}

	public boolean getScThursday() {

		return scThursday;
	}

	public void setScThursday(boolean scThursday) {

		this.scThursday = scThursday;
	}

	public boolean getScFriday() {

		return scFriday;
	}

	public void setScFriday(boolean scFriday) {

		this.scFriday = scFriday;
	}

	public boolean getScSaturday() {

		return scSaturday;
	}

	public void setScSaturday(boolean scSaturday) {

		this.scSaturday = scSaturday;
	}



	public FormFile getUploadFile(){
		return uploadFile;
	}
	public void setUploadFile(FormFile uploadFile){

		this.uploadFile = uploadFile;
	}



	public boolean getCreateCategory (){

		return createCategory;

	}

	public void setCreateCategory(boolean createCategory){
		this.createCategory = createCategory;
	}


	public boolean getCreateUOM(){

		return createUOM;

	}

	public void setCreateUOM(boolean createUOM){
		this.createUOM = createUOM;
	}


	public boolean getCreateLocation(){

		return createLocation;

	}

	public void setCreateLocation(boolean createLocation){
		this.createLocation = createLocation;
	}
	
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}

	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	
	
	public String getLogEntry() {
		return logEntry;
	}

	public void setLogEntry(String logEntry) {
		this.logEntry = logEntry;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		itemName = null;
		description = null;
		partNumber = null;
		shortName = null;
		barCode1 = null;
		barCode2 = null;
		barCode3 = null;
		unitCost = null;
		averageCost = null;
		percentMarkup = null;
		shippingCost = null;
		salesPrice = null;
		grossProfit = null;
		unitMeasure = Constants.GLOBAL_BLANK;
		category = Constants.GLOBAL_BLANK;
		costMethodList.clear();
		costMethodList.add(Constants.GLOBAL_BLANK);
		costMethodList.add("Average");
		costMethodList.add("FIFO");
		costMethodList.add("Standard");
		costMethod = Constants.GLOBAL_BLANK;
		enable = false;
		virtualStore = false;
		doneness = Constants.GLOBAL_BLANK;
		remarks = null;
		sidings = null;
		serviceCharge = false;
		nonInventoriable = false;
		services = false;
		jobServices = false;
		isVatRelief = false;
		isTax = false;
		isProject = false;
		supplier = null;
		market = null;
		poCycle = null;
		packaging = Constants.GLOBAL_BLANK;
		retailUom = Constants.GLOBAL_BLANK;
		defaultLocation = Constants.GLOBAL_BLANK;
		isUnitMeasureEntered = null;
		enablePo = false;
		openProduct = false;
		fixedAsset = false;
		dateAcquired = null;
		acquisitionCost = 0;
		ResidualValue = 0;
		lifeSpan = 0;
		fixedAssetAccount = Constants.GLOBAL_BLANK;
		fixedAssetAccountDescription = Constants.GLOBAL_BLANK;
		depreciationAccount = Constants.GLOBAL_BLANK;
		depreciationAccountDescription = Constants.GLOBAL_BLANK;
		accumulatedDepreciationAccount = Constants.GLOBAL_BLANK;
		accumulatedDepreciationAccountDescription = Constants.GLOBAL_BLANK;
		trackMisc = false;

		scSunday = false;
		scMonday = false;
		scTuesday = false;
		scWednesday = false;
		scThursday = false;
		scFriday = false;
		scSaturday = false;

		uploadFile = null;
		createCategory =false;
		createUOM = false;
		createLocation = false;

		conditionList.clear();
		conditionList.add(Constants.GLOBAL_BLANK);
		conditionList.add("Good");
		conditionList.add("Junk");
		conditionList.add("Damaged");
		conditionList.add("Disposed");
		taxCode = Constants.GLOBAL_BLANK;

		
		createdBy = null;
		dateCreated = null;
		
		lastModifiedBy = null;
		dateLastModified = null;
		
		
		logEntry = null;
		/*taxCodeList.clear();
		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);
		taxCodeList.add("Sales Tax");
		taxCodeList.add("Prepared Food Tax");*/
	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		if (request.getParameter("saveButton") != null || request.getParameter("conversionButton") != null || request.getParameter("priceLevelsButton") != null) {

			
		
			
			if (Common.validateRequired(itemName)) {

				errors.add("itemName",
						new ActionMessage("itemEntry.error.itemNameRequired"));

			}

			if (Common.validateRequired(unitMeasure) || unitMeasure.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("unitMeasure",
						new ActionMessage("itemEntry.error.unitMeasureRequired"));

			} else{
                            if(!unitMeasureList.contains(unitMeasure)){
                                errors.add("unitMeasure",
						new ActionMessage("itemEntry.error.unitMeasureNotExist"));
                            }
                        }


			if (Common.validateRequired(category) || category.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("category",
						new ActionMessage("itemEntry.error.categoryRequired"));

			}else{
                            if(!categoryList.contains(category)){
                                errors.add("category",
						new ActionMessage("itemEntry.error.categoryNotExist"));
                            }
                        }

			if (Common.validateRequired(costMethod) || costMethod.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

                            errors.add("costMethod",
						new ActionMessage("itemEntry.error.costMethodRequired"));

			}else{
                            if(!costMethodList.contains(costMethod)){
                                errors.add("costMethod",
						new ActionMessage("itemEntry.error.costMethodNotExist"));
                            }
                        }

			if (!Common.validateMoneyFormat(salesPrice)) {

				errors.add("salesPrice",
						new ActionMessage("itemEntry.error.salesPriceInvalid"));

			}

			if (!Common.validateMoneyFormat(percentMarkup)) {

				errors.add("percentMarkup",
						new ActionMessage("itemEntry.error.percentMarkupInvalid"));

			}

			if (!Common.validateMoneyFormat(shippingCost)) {

				errors.add("shippingCost",
						new ActionMessage("itemEntry.error.shippingCostInvalid"));

			}

			if (!Common.validateMoneyFormat(unitCost)) {

				errors.add("unitCost",
						new ActionMessage("itemEntry.error.unitCostInvalid"));

			}

			if (!Common.validateNumberFormat(poCycle)) {

				errors.add("poCycle",
						new ActionMessage("itemEntry.error.poCycleInvalid"));

			}

			if(!Common.validateDateFormat(dateAcquired)) {

				errors.add("dateAcquired", new ActionMessage("itemEntry.error.dateAcquiredInvalid"));
			}

		}

		return errors;

	}

}