package com.struts.inv.itemlocationentry;

import java.io.Serializable;

public class InvBranchItemLocationList implements Serializable {
	
	private String branchName = null;
	private Integer branchCode = null;
	private boolean branchCheckbox = false;
	private String coaSegment = null;
	private String type = null;
	private String bilRack = null;
	private String bilBin = null;
	private String bilReorderPoint = null;
	private String bilReorderQuantity = null;
	private String bilSalesAccount = null;
	private String bilSalesAccountDescription = null;
	private String bilInventoryAccount = null;
	private String bilInventoryAccountDescription = null;
	private String bilCostOfSalesAccount = null;
	private String bilCostOfSalesAccountDescription = null;
	private String bilWipAccount = null;
	private String bilWipAccountDescription = null;
	private String bilAccruedInventoryAccount = null;
	private String bilAccruedInventoryAccountDescription = null;
	private String bilSalesReturnAccount = null;
	private String bilSalesReturnAccountDescription = null;
	private String bilHist1Sales = null;
	private String bilHist2Sales = null;
	private String bilProjectedSales = null;
	private String bilDeliveryTime = null;
	private String bilDeliveryBuffer = null;
	private String bilOrderPerYear = null;
	
	
	private InvItemLocationEntryForm parentBean;
	
	private boolean bilSubjectToCommission = false;
	
	public InvBranchItemLocationList(InvItemLocationEntryForm parentBean, 
			String branchName, Integer branchCode, String coaSegment, String type) {
		
		this.parentBean = parentBean;
		this.branchName = branchName;
		this.branchCode = branchCode;
		this.coaSegment = coaSegment;
		this.type = type;
		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getCoaSegment() {
		
		return coaSegment;
		
	}
	
	public void setCoaSegment(String coaSegment) {
		
		this.coaSegment = coaSegment;
		
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public String getBilRack() {
		
		return bilRack;
		
	}
	
	public void setBilRack(String bilRack) {
		
		this.bilRack = bilRack;
		
	}
	
	public String getBilBin() {
		
		return bilBin;
		
	}
	
	public void setBilBin(String bilBin) {
		
		this.bilBin = bilBin;
		
	}
	
	
	
	public String getBilReorderPoint() {
		
		return bilReorderPoint;
		
	}
	
	public void setBilReorderPoint(String bilReorderPoint) {
		
		this.bilReorderPoint = bilReorderPoint;
		
	}
	
	public String getBilReorderQuantity() {
		
		return bilReorderQuantity;
		
	}
	
	public void setBilReorderQuantity(String bilReorderQuantity) {
		
		this.bilReorderQuantity = bilReorderQuantity;
		
	}
	
	public String getBilCostOfSalesAccount() {
		
		return bilCostOfSalesAccount;
		
	}
	
	public void setBilCostOfSalesAccount(String bilCostOfSalesAccount) {
		
		this.bilCostOfSalesAccount = bilCostOfSalesAccount;
		
	}
	
	public String getBilCostOfSalesAccountDescription() {
		
		return bilCostOfSalesAccountDescription;
		
	}
	
	public void setBilCostOfSalesAccountDescription(String bilCostOfSalesAccountDescription) {
		
		this.bilCostOfSalesAccountDescription = bilCostOfSalesAccountDescription;
		
	}
	
	public String getBilInventoryAccount() {
		
		return bilInventoryAccount;
		
	}
	
	public void setBilInventoryAccount(String bilInventoryAccount) {
		
		this.bilInventoryAccount = bilInventoryAccount;
		
	}
	
	public String getBilInventoryAccountDescription() {
		
		return bilInventoryAccountDescription;
		
	}
	
	public void setBilInventoryAccountDescription(String bilInventoryAccountDescription) {
		
		this.bilInventoryAccountDescription = bilInventoryAccountDescription;
		
	}
	
	public String getBilSalesAccount() {
		
		return bilSalesAccount;
		
	}
	
	public void setBilSalesAccount(String bilSalesAccount) {
		
		this.bilSalesAccount = bilSalesAccount;
		
	}
	
	public String getBilSalesAccountDescription() {
		
		return bilSalesAccountDescription;
		
	}
	
	public void setBilSalesAccountDescription(String bilSalesAccountDescription) {
		
		this.bilSalesAccountDescription = bilSalesAccountDescription;
		
	}
	
	public String getBilSalesReturnAccount() {
		
		return bilSalesReturnAccount;
		
	}
	
	public void setBilSalesReturnAccount(String bilSalesReturnAccount) {
		
		this.bilSalesReturnAccount = bilSalesReturnAccount;
		
	}
	
	public String getBilSalesReturnAccountDescription() {
		
		return bilSalesReturnAccountDescription;
		
	}
	
	public void setBilSalesReturnAccountDescription(String bilSalesReturnAccountDescription) {
		
		this.bilSalesReturnAccountDescription = bilSalesReturnAccountDescription;
		
	}
	
	public String getBilWipAccount() {
		
		return bilWipAccount;
		
	}
	
	public void setBilWipAccount(String bilWipAccount) {
		
		this.bilWipAccount = bilWipAccount;
		
	}
	
	public String getBilWipAccountDescription() {
		
		return bilWipAccountDescription;
		
	}
	
	public void setBilWipAccountDescription(String bilWipAccountDescription) {
		
		this.bilWipAccountDescription = bilWipAccountDescription;
		
	}
	
	public String getBilAccruedInventoryAccount() {
		
		return bilAccruedInventoryAccount;
		
	}
	
	public void setBilAccruedInventoryAccount(String bilAccruedInventoryAccount) {
		
		this.bilAccruedInventoryAccount = bilAccruedInventoryAccount;
		
	}
	
	public String getBilAccruedInventoryAccountDescription() {
		
		return bilAccruedInventoryAccountDescription;
		
	}
	
	public void setBilAccruedInventoryAccountDescription(String bilAccruedInventoryAccountDescription) {
		
		this.bilAccruedInventoryAccountDescription = bilAccruedInventoryAccountDescription;
		
	}
	
	public boolean getBilSubjectToCommission() {
		
		return this.bilSubjectToCommission;
		
	}
	
	public void setBilSubjectToCommission(boolean subjectToCommission) {
		
		this.bilSubjectToCommission = subjectToCommission;
		
	}
	
	
	public String getBilHist1Sales() {
		
		return bilHist1Sales;
		
	}
	
	public void setBilHist1Sales(String bilHist1Sales) {
		
		this.bilHist1Sales = bilHist1Sales;
		
	}
	
	
     public String getBilHist2Sales() {
		
		return bilHist2Sales;
		
	}
	
	public void setBilHist2Sales(String bilHist2Sales) {
		
		this.bilHist2Sales = bilHist2Sales;
		
	}
	
    public String getBilProjectedSales() {
		
		return bilProjectedSales;
		
	}
	
	public void setBilProjectedSales(String bilProjectedSales) {
		
		this.bilProjectedSales = bilProjectedSales;
		
	}
	
    public String getBilDeliveryTime() {
		
		return bilDeliveryTime;
	}
	
	public void setBilDeliveryTime(String bilDeliveryTime) {
		
		this.bilDeliveryTime = bilDeliveryTime;
		
	}
	
	public String getBilDeliveryBuffer() {
		
		return bilDeliveryBuffer;
	}
	
	public void setBilDeliveryBuffer(String bilDeliveryBuffer) {
		
		this.bilDeliveryBuffer = bilDeliveryBuffer;
		
	}
	
	public String getBilOrderPerYear() {
		
		return bilOrderPerYear;
	}
	
	public void setBilOrderPerYear(String bilOrderPerYear) {
		
		this.bilOrderPerYear = bilOrderPerYear;
		
	}
	
	

}