package com.struts.inv.itemlocationentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.InvILCoaGlAccruedInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlCostOfSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlSalesReturnAccountNotFoundException;
import com.ejb.exception.InvILCoaGlWipAccountNotFoundException;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.ejb.txn.InvItemLocationEntryController;
import com.ejb.txn.InvItemLocationEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.InvItemDetails;
import com.util.InvModItemLocationDetails;

public final class InvItemLocationEntryAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();

      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvItemLocationEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }

        InvItemLocationEntryForm actionForm = (InvItemLocationEntryForm)form;

        String frParam = Common.getUserPermission(user, Constants.INV_ITEM_LOCATION_ENTRY_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("invItemLocationEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize InvItemLocationEntryController EJB
*******************************************************/

        InvItemLocationEntryControllerHome homeILE = null;
        InvItemLocationEntryController ejbILE = null;
        
        AdLogControllerHome homeLOG = null;
        AdLogController ejbLOG = null;

        try {

            homeILE = (InvItemLocationEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvItemLocationEntryControllerEJB", InvItemLocationEntryControllerHome.class);
            
            homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);

        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvItemLocationEntryAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());

            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbILE = homeILE.create();
            ejbLOG = homeLOG.create();

        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvItemLocationEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();

/*******************************************************
   Call InvItemLocationEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

		short quantityPrecisionUnit = 0;
		boolean itemShowAll = false;
		boolean addByItemList = false;

		try {

			quantityPrecisionUnit = ejbILE.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());
			itemShowAll = Common.convertByteToBoolean(ejbILE.getAdPrfInvItemLocationShowAll(user.getCmpCode()));
			addByItemList = Common.convertByteToBoolean(ejbILE.getAdPrfInvItemLocationAddByItemList(user.getCmpCode()));
			actionForm.setItemShowAll(itemShowAll);
			actionForm.setAddByItemList(addByItemList);

		} catch(EJBException ex) {

			if (log.isInfoEnabled()) {

				log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
						" session: " + session.getId());
			}

			return(mapping.findForward("cmnErrorPage"));

		}

/*******************************************************
   -- Inv ILE Save Action --
*******************************************************/

        if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            InvModItemLocationDetails mdetails = new InvModItemLocationDetails();
            
            mdetails.setIlRack(actionForm.getRack());
            mdetails.setIlBin(actionForm.getBin());
            mdetails.setIlReorderPoint(Common.convertStringMoneyToDouble(actionForm.getReorderPoint(), quantityPrecisionUnit));
            mdetails.setIlReorderQuantity(Common.convertStringMoneyToDouble(actionForm.getReorderQuantity(), quantityPrecisionUnit));
            mdetails.setIlReorderLevel(Common.convertStringMoneyToDouble(actionForm.getReorderLevel(), quantityPrecisionUnit));
            mdetails.setIlCoaGlSalesAccountNumber(actionForm.getSalesAccount());
            mdetails.setIlCoaGlInventoryAccountNumber(actionForm.getInventoryAccount());
            mdetails.setIlCoaGlCostOfSalesAccountNumber(actionForm.getCostOfSalesAccount());
            mdetails.setIlCoaGlWipAccountNumber(actionForm.getWipAccount());
            mdetails.setIlCoaGlAccruedInventoryAccountNumber(actionForm.getAccruedInventoryAccount());
            mdetails.setIlCoaGlSalesReturnAccountNumber(actionForm.getSalesReturnAccount());
            mdetails.setIlSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
            mdetails.setIlLastModifiedBy(user.getUserName());
            mdetails.setIlDateLastModified(new java.util.Date());
            
            
            ArrayList itemNameSelectedList = new ArrayList();

            /*
            for (int i=0; i < actionForm.getItemNameSelectedList().length; i++) {

            	itemNameSelectedList.add(actionForm.getItemNameSelectedList()[i]);

            }
             */
            ArrayList locationNameSelectedList = new ArrayList();

			for (int i=0; i < actionForm.getLocationNameSelectedList().length; i++) {

				locationNameSelectedList.add(actionForm.getLocationNameSelectedList()[i]);

			}

			ArrayList categoryNameSelectedList = new ArrayList();
			/*
			if (actionForm.getCategoryNameSelectedList().length == 0) {

				for (int i=0; i < actionForm.getCategoryNameList().size(); i++) {

					categoryNameSelectedList = actionForm.getCategoryNameList();

				}

			}

			for (int i=0; i < actionForm.getCategoryNameSelectedList().length; i++) {

				categoryNameSelectedList.add(actionForm.getCategoryNameSelectedList()[i]);

			}
			 */
			//new code
			if (actionForm.getAddByItemList()) {

				for (int i=0; i < actionForm.getItemNameSelectedList().length; i++) {

	            	itemNameSelectedList.add(actionForm.getItemNameSelectedList()[i]);

	            }


				for (int i=0; i < actionForm.getCategoryNameList().size(); i++) {

					categoryNameSelectedList = actionForm.getCategoryNameList();

				}

			}else {

				for (int i=0; i < actionForm.getItemNameList().size(); i++) {

	            	itemNameSelectedList.add(actionForm.getItemNameList().get(i));

	            }


				for (int i=0; i < actionForm.getCategoryNameSelectedList().length; i++) {

					categoryNameSelectedList.add(actionForm.getCategoryNameSelectedList()[i]);

				}
			}

			ArrayList branchItemLocationList = new ArrayList();

			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {

				InvBranchItemLocationList invBRIlList = actionForm.getInvBrIlListByIndex(i);

				if(invBRIlList.getBranchCheckbox() == true) {

					AdModBranchItemLocationDetails mBrDetails = new AdModBranchItemLocationDetails();
					mBrDetails.setBilBrName(invBRIlList.getBranchName());

					if(invBRIlList.getBilRack() != null && !invBRIlList.getBilRack().equals("")) {

						mBrDetails.setBilRack(invBRIlList.getBilRack());
					} else {

						mBrDetails.setBilRack(actionForm.getRack());

					}
					
					
					if(invBRIlList.getBilBin() != null && !invBRIlList.getBilBin().equals("")) {

						mBrDetails.setBilBin(invBRIlList.getBilBin());
					} else {

						mBrDetails.setBilBin(actionForm.getBin());

					}

					if(invBRIlList.getBilReorderPoint() != null && !invBRIlList.getBilReorderPoint().equals("")) {

						mBrDetails.setBilReorderPoint(Common.convertStringMoneyToDouble(invBRIlList.getBilReorderPoint(), quantityPrecisionUnit));

					} else {

						mBrDetails.setBilReorderPoint(Common.convertStringMoneyToDouble(actionForm.getReorderPoint(), quantityPrecisionUnit));

					}

					if(invBRIlList.getBilReorderQuantity() != null && !invBRIlList.getBilReorderQuantity().equals("")) {

						mBrDetails.setBilReorderQuantity(Common.convertStringMoneyToDouble(invBRIlList.getBilReorderQuantity(), quantityPrecisionUnit));

					} else {

						mBrDetails.setBilReorderQuantity(Common.convertStringMoneyToDouble(actionForm.getReorderQuantity(), quantityPrecisionUnit));

					}

					if(invBRIlList.getBilSalesAccount() != null && !invBRIlList.getBilSalesAccount().equals("")) {

						mBrDetails.setBilCoaGlSalesAccountNumber(invBRIlList.getBilSalesAccount());

					} else {

						mBrDetails.setBilCoaGlSalesAccountNumber(actionForm.getSalesAccount());

					}

					if(invBRIlList.getBilInventoryAccount() != null && !invBRIlList.getBilInventoryAccount().equals("")) {

						mBrDetails.setBilCoaGlInventoryAccountNumber(invBRIlList.getBilInventoryAccount());

					} else {

						mBrDetails.setBilCoaGlInventoryAccountNumber(actionForm.getInventoryAccount());

					}

					if(invBRIlList.getBilCostOfSalesAccount() != null && !invBRIlList.getBilCostOfSalesAccount().equals("")) {

						mBrDetails.setBilCoaGlCostOfSalesAccountNumber(invBRIlList.getBilCostOfSalesAccount());

					} else {

						mBrDetails.setBilCoaGlCostOfSalesAccountNumber(actionForm.getCostOfSalesAccount());

					}

					if(invBRIlList.getBilWipAccount() != null && !invBRIlList.getBilWipAccount().equals("")) {

						mBrDetails.setBilCoaGlWipAccountNumber(invBRIlList.getBilWipAccount());

					} else {

						mBrDetails.setBilCoaGlWipAccountNumber(actionForm.getWipAccount());

					}

					if(invBRIlList.getBilAccruedInventoryAccount() != null && !invBRIlList.getBilAccruedInventoryAccount().equals("")) {

						mBrDetails.setBilCoaGlAccruedInventoryAccountNumber(invBRIlList.getBilAccruedInventoryAccount());

					} else {

						mBrDetails.setBilCoaGlAccruedInventoryAccountNumber(actionForm.getAccruedInventoryAccount());

					}


					if(invBRIlList.getBilSalesReturnAccount() != null && !invBRIlList.getBilSalesReturnAccount().equals("")) {

						mBrDetails.setBilCoaGlSalesReturnAccountNumber(invBRIlList.getBilSalesReturnAccount());

					} else {

						mBrDetails.setBilCoaGlSalesReturnAccountNumber(actionForm.getSalesReturnAccount());

					}


					mBrDetails.setBilHist1Sales(Common.convertStringMoneyToDouble(invBRIlList.getBilHist1Sales(), quantityPrecisionUnit));
					mBrDetails.setBilHist2Sales(Common.convertStringMoneyToDouble(invBRIlList.getBilHist2Sales(), quantityPrecisionUnit));
					mBrDetails.setBilProjectedSales(Common.convertStringMoneyToDouble(invBRIlList.getBilProjectedSales(), quantityPrecisionUnit));
					mBrDetails.setBilDeliveryTime(Common.convertStringMoneyToDouble(invBRIlList.getBilDeliveryTime(), quantityPrecisionUnit));
					mBrDetails.setBilDeliveryBuffer(Common.convertStringMoneyToDouble(invBRIlList.getBilDeliveryBuffer(), quantityPrecisionUnit));
					mBrDetails.setBilOrderPerYear(Common.convertStringMoneyToDouble(invBRIlList.getBilOrderPerYear(), quantityPrecisionUnit));


					mBrDetails.setBilSubjectToCommission(Common.convertBooleanToByte(invBRIlList.getBilSubjectToCommission()));

					mBrDetails.setBilLastModifiedBy(user.getUserName());
					mBrDetails.setBilDateLastModified(new java.util.Date());
					
					branchItemLocationList.add(mBrDetails);

				}

			}

            try {

            	ejbILE.saveInvIlEntry(mdetails, itemNameSelectedList, locationNameSelectedList, branchItemLocationList,
            			categoryNameSelectedList, actionForm.getItemClass(), new Integer(user.getCurrentResCode()), user.getCmpCode());
            	
            	
            	
            	
            
            	
            	

            } catch (InvILCoaGlSalesAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.salesAccountNotFound"));

            } catch (InvILCoaGlSalesReturnAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.salesReturnAccountNotFound"));

            } catch (InvILCoaGlInventoryAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.inventoryAccountNotFound"));

            } catch (InvILCoaGlCostOfSalesAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.costOfSalesAccountNotFound"));

            } catch (InvILCoaGlWipAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.wipAccountNotFound"));

            } catch (InvILCoaGlAccruedInventoryAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("itemLocationEntry.error.accruedInventoryAccountNotFound"));

            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in InvItemLocationEntryAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	ex.printStackTrace();
                  	return mapping.findForward("cmnErrorPage");

               	}

            }


    /*******************************************************
    -- Inv ILE Update Ave Sales Action --
 *******************************************************/

        } else if (request.getParameter("updateAveSalesButton") != null &&
             actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	ArrayList itemNameSelectedList = new ArrayList();

             for (int i=0; i < actionForm.getItemNameSelectedList().length; i++) {

             	itemNameSelectedList.add(actionForm.getItemNameSelectedList()[i]);

             }

             ArrayList locationNameSelectedList = new ArrayList();

 			for (int i=0; i < actionForm.getLocationNameSelectedList().length; i++) {

 				locationNameSelectedList.add(actionForm.getLocationNameSelectedList()[i]);

 			}

 			ArrayList categoryNameSelectedList = new ArrayList();

 			if (actionForm.getCategoryNameSelectedList().length == 0) {

 				for (int i=0; i < actionForm.getCategoryNameList().size(); i++) {

 					categoryNameSelectedList = actionForm.getCategoryNameList();

 				}

 			}

 			for (int i=0; i < actionForm.getCategoryNameSelectedList().length; i++) {

 				categoryNameSelectedList.add(actionForm.getCategoryNameSelectedList()[i]);

 			}

 			ArrayList branchItemLocationList = new ArrayList();

 			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {

 				InvBranchItemLocationList invBRIlList = actionForm.getInvBrIlListByIndex(i);

 				if(invBRIlList.getBranchCheckbox() == true) {

 					AdModBranchItemLocationDetails mBrDetails = new AdModBranchItemLocationDetails();
 					mBrDetails.setBilBrName(invBRIlList.getBranchName());

 					mBrDetails.setBilHist1Sales(Common.convertStringMoneyToDouble(invBRIlList.getBilHist1Sales(), quantityPrecisionUnit));
 					mBrDetails.setBilHist2Sales(Common.convertStringMoneyToDouble(invBRIlList.getBilHist2Sales(), quantityPrecisionUnit));
 					mBrDetails.setBilProjectedSales(Common.convertStringMoneyToDouble(invBRIlList.getBilProjectedSales(), quantityPrecisionUnit));
 					mBrDetails.setBilDeliveryTime(Common.convertStringMoneyToDouble(invBRIlList.getBilDeliveryTime(), quantityPrecisionUnit));
					mBrDetails.setBilDeliveryBuffer(Common.convertStringMoneyToDouble(invBRIlList.getBilDeliveryBuffer(), quantityPrecisionUnit));
					mBrDetails.setBilOrderPerYear(Common.convertStringMoneyToDouble(invBRIlList.getBilOrderPerYear(), quantityPrecisionUnit));


 					branchItemLocationList.add(mBrDetails);

 				}

 			}

             try {

             	ejbILE.updateInvIliAveSales(itemNameSelectedList, locationNameSelectedList, branchItemLocationList,
             			categoryNameSelectedList, actionForm.getItemClass(), new Integer(user.getCurrentResCode()), user.getCmpCode());

             } catch (EJBException ex) {

                	if (log.isInfoEnabled()) {

                   	log.info("EJBException caught in InvItemLocationEntryAction.execute(): " + ex.getMessage() +
                   	" session: " + session.getId());
                   	ex.printStackTrace();
                   	return mapping.findForward("cmnErrorPage");

                	}

             }

/*******************************************************
   -- Inv ILE Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
    -- Inv ILE Recalc Reorder Points Action --
*******************************************************/

        } else if (request.getParameter("recalcButton") != null &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	ArrayList itemNameSelectedList = new ArrayList();

            for (int i=0; i < actionForm.getItemNameSelectedList().length; i++) {

            	itemNameSelectedList.add(actionForm.getItemNameSelectedList()[i]);

            }

            ArrayList locationNameSelectedList = new ArrayList();

			for (int i=0; i < actionForm.getLocationNameSelectedList().length; i++) {

				locationNameSelectedList.add(actionForm.getLocationNameSelectedList()[i]);

			}
            // update reorder points

            ejbILE.saveInvIlReorderPointAndReorderQuantity(locationNameSelectedList, itemNameSelectedList, new Integer(user.getCurrentResCode()), user.getCmpCode());

/*******************************************************
    -- Inv Item Location Entry Delete Action --
*******************************************************/

                } else if (request.getParameter("deleteButton") != null &&
                		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                	ArrayList itemNameSelectedList = new ArrayList();

                	for (int i=0; i < actionForm.getItemNameSelectedList().length; i++) {

                		itemNameSelectedList.add(actionForm.getItemNameSelectedList()[i]);

                	}

                	ArrayList locationNameSelectedList = new ArrayList();

                	for (int i=0; i < actionForm.getLocationNameSelectedList().length; i++) {

                		locationNameSelectedList.add(actionForm.getLocationNameSelectedList()[i]);

                	}

                	ArrayList categoryNameSelectedList = new ArrayList();

                	for (int i=0; i < actionForm.getCategoryNameSelectedList().length; i++) {

                		categoryNameSelectedList.add(actionForm.getCategoryNameSelectedList()[i]);

                	}

                	try {

                		ejbILE.deleteInvIlEntry(itemNameSelectedList, locationNameSelectedList,
                				categoryNameSelectedList, actionForm.getItemClass(), user.getCmpCode());

                	} catch (GlobalRecordAlreadyAssignedException ex) {

                		errors.add(ActionMessages.GLOBAL_MESSAGE,
                				new ActionMessage("itemLocationEntry.error.recordAlreadyAssigned"));

                	} catch (EJBException ex) {

                		if (log.isInfoEnabled()) {

                			log.info("EJBException caught in InvItemLocationEntryAction.execute(): " + ex.getMessage() +
                					" session: " + session.getId());
                			ex.printStackTrace();
                			return mapping.findForward("cmnErrorPage");

                		}

                	}
/*******************************************************
 	-- Inv ADJ Item Enter Action --
*******************************************************/
         } else if(!Common.validateRequired(request.getParameter("findItem.isItemEntered"))) {

        	 try{

         		InvItemDetails mdetails = ejbILE.getInvItemByIiName(request.getParameter("findItem.itemName"),user.getCmpCode());

         		System.out.println("Check Point B, " + mdetails.getIiName());
         		if(!actionForm.getItemNameList().contains(mdetails.getIiName())){
         			System.out.println("Check Point C");
         			actionForm.setItemDescriptionList(mdetails.getIiName() + " - " + mdetails.getIiDescription());
     				actionForm.setItemNameList(mdetails.getIiName());

         		}


        	 }catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in InvItemLocationEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage");

                 }

             }
             return(mapping.findForward("invItemLocationEntry"));

/*******************************************************
   -- Inv ILE Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invItemLocationEntry");

            }

			if (request.getParameter("forward") == null &&
					actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return mapping.findForward("cmnMain");

			}

			actionForm.reset(mapping, request);

            ArrayList list = null;
            Iterator i = null;

	        try {

            	actionForm.clearItemNameList();
            	actionForm.clearItemDescriptionList();

            	if(actionForm.getItemShowAll()){

            		list = ejbILE.getInvIiAll(user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setItemNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				InvItemDetails mdetails = (InvItemDetails) i.next();

            				actionForm.setItemDescriptionList(mdetails.getIiAdLvCategory() + " - " + mdetails.getIiName() + " - " + mdetails.getIiDescription());
            				actionForm.setItemNameList(mdetails.getIiName());

            			}

            		}

            	}
            	actionForm.clearLocationNameList();

            	list = ejbILE.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while(i.hasNext()) {

            			actionForm.setLocationNameList((String)i.next());

            		}
            	}

            	actionForm.clearCategoryNameList();

            	list = ejbILE.getAdLvInvItemCategoryAll(user.getCmpCode());

            	if(actionForm.getItemShowAll()){

            		if (list == null || list.size() == 0) {

            			actionForm.setCategoryNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {


            			i = list.iterator();

            			while(i.hasNext()) {

            				actionForm.setCategoryNameList((String)i.next());

            			}
            		}
            	}else{

            		i = list.iterator();

        			while(i.hasNext()) {

        				actionForm.setCategoryNameList((String)i.next());

        			}

            	}

            	try {

            		actionForm.clearInvBrIlList();

            		list = ejbILE.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

            		i = list.iterator();

            		while(i.hasNext()) {

            			AdBranchDetails details = (AdBranchDetails)i.next();

            			InvBranchItemLocationList invBrIlList = new InvBranchItemLocationList(actionForm,
            					details.getBrName(), details.getBrCode(), details.getBrCoaSegment(), details.getBrType());

            			/*if ( request.getParameter("forward") == null ) {

            				invBrIlList.setBranchCheckbox(true);
            				invBrIlList.setBilSubjectToCommission(false);
            			}*/

            			actionForm.saveInvBrIlList(invBrIlList);

            		}

            	} catch (GlobalNoRecordFoundException ex) {

            	} catch (EJBException ex) {

            		if (log.isInfoEnabled()) {

            			log.info("EJBException caught in InvItemLocationAction.execute(): " + ex.getMessage() +
            					" session: " + session.getId());
            			return mapping.findForward("cmnErrorPage");

            		}

                }

            	if (request.getParameter("forward") != null) {

            	    InvModItemLocationDetails mdetails = ejbILE.getInvIlByIlCode(
            		    new Integer(request.getParameter("ilCode")), user.getCmpCode());

            	    if(!actionForm.getItemShowAll()){

        				actionForm.setItemDescriptionList(mdetails.getIlIiName() + " - " + mdetails.getIlIiDescription());
        				actionForm.setItemNameList(mdetails.getIlIiName());

            	    }



            	    String[] itemNameSelectedList = new String[1];
            	    itemNameSelectedList[0] = mdetails.getIlIiName();

            	    String[] locatioNameSelectedList = new String[1];
            	    locatioNameSelectedList[0] = mdetails.getIlLocName();

            	    String itemName = itemNameSelectedList[0];
            	    if(!actionForm.getItemNameList().contains(itemName)) {

            	    	if (actionForm.getItemNameList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            	    		actionForm.clearItemNameList();

            	    	}
            	    	actionForm.setItemNameList(itemName);

            	    }
					actionForm.setItemNameSelectedList(itemNameSelectedList);

					actionForm.setLocationNameSelectedList(locatioNameSelectedList);
					actionForm.setRack(mdetails.getIlRack());
					actionForm.setBin(mdetails.getIlBin());
					actionForm.setReorderPoint(Common.convertDoubleToStringMoney(mdetails.getIlReorderPoint(), quantityPrecisionUnit));
					actionForm.setReorderQuantity(Common.convertDoubleToStringMoney(mdetails.getIlReorderQuantity(), quantityPrecisionUnit));
					actionForm.setReorderLevel(Common.convertDoubleToStringMoney(mdetails.getIlReorderLevel(), quantityPrecisionUnit));
					actionForm.setSalesAccount(mdetails.getIlCoaGlSalesAccountNumber());
					actionForm.setSalesAccountDescription(mdetails.getIlCoaGlSalesAccountDescription());
					actionForm.setInventoryAccount(mdetails.getIlCoaGlInventoryAccountNumber());
					actionForm.setInventoryAccountDescription(mdetails.getIlCoaGlInventoryAccountDescription());
					actionForm.setCostOfSalesAccount(mdetails.getIlCoaGlCostOfSalesAccountNumber());
					actionForm.setCostOfSalesAccountDescription(mdetails.getIlCoaGlCostOfSalesAccountDescription());
					actionForm.setWipAccount(mdetails.getIlCoaGlWipAccountNumber());
					actionForm.setWipAccountDescription(mdetails.getIlCoaGlWipAccountDescription());
					actionForm.setAccruedInventoryAccount(mdetails.getIlCoaGlAccruedInventoryAccountNumber());
					actionForm.setAccruedInventoryAccountDescription(mdetails.getIlCoaGlAccruedInventoryAccountDescription());
					actionForm.setSubjectToCommission(Common.convertByteToBoolean(mdetails.getIlSubjectToCommission()));
					actionForm.setSalesReturnAccount(mdetails.getIlCoaGlSalesReturnAccountNumber());
					actionForm.setSalesReturnAccountDescription(mdetails.getIlCoaGlSalesReturnAccountDescription());

					for(int j=0; j<actionForm.getInvBrIlListSize(); j++) {

						InvBranchItemLocationList invBilList = (InvBranchItemLocationList)actionForm.getInvBrIlListByIndex(j);

						for(int k=0; k<mdetails.getBrIlListSize(); k++) {

							AdModBranchItemLocationDetails mBrDetails = (AdModBranchItemLocationDetails)mdetails.getBrIlListByIndex(k);

							if(invBilList.getBranchName().equals(mBrDetails.getBilBrName())) {

								invBilList.setBranchCode(mBrDetails.getBilBrCode());
								invBilList.setBranchName(mBrDetails.getBilBrName());
								invBilList.setBranchCheckbox(true);
								invBilList.setBilRack(mBrDetails.getBilRack());
								invBilList.setBilBin(mBrDetails.getBilBin());
								invBilList.setBilReorderPoint(Common.convertDoubleToStringMoney(mBrDetails.getBilReorderPoint(), quantityPrecisionUnit));
								invBilList.setBilReorderQuantity(Common.convertDoubleToStringMoney(mBrDetails.getBilReorderQuantity(), quantityPrecisionUnit));
								invBilList.setBilSalesAccount(mBrDetails.getBilCoaGlSalesAccountNumber());
								invBilList.setBilSalesAccountDescription(mBrDetails.getBilCoaGlSalesAccountDescription());
								invBilList.setBilSalesReturnAccount(mBrDetails.getBilCoaGlSalesReturnAccountNumber());
								invBilList.setBilSalesReturnAccountDescription(mBrDetails.getBilCoaGlSalesReturnAccountDescription());

								invBilList.setBilInventoryAccount(mBrDetails.getBilCoaGlInventoryAccountNumber());
								invBilList.setBilInventoryAccountDescription(mBrDetails.getBilCoaGlInventoryAccountDescription());
								invBilList.setBilCostOfSalesAccount(mBrDetails.getBilCoaGlCostOfSalesAccountNumber());
								invBilList.setBilCostOfSalesAccountDescription(mBrDetails.getBilCoaGlCostOfSalesAccountDescription());
								invBilList.setBilWipAccount(mBrDetails.getBilCoaGlWipAccountNumber());
								invBilList.setBilWipAccountDescription(mBrDetails.getBilCoaGlWipAccountDescription());
								invBilList.setBilAccruedInventoryAccount(mBrDetails.getBilCoaGlAccruedInventoryAccountNumber());
								invBilList.setBilAccruedInventoryAccountDescription(mBrDetails.getBilCoaGlAccruedInventoryAccountDescription());

								invBilList.setBilSubjectToCommission(Common.convertByteToBoolean(mBrDetails.getBilSubjectToCommission()));
								invBilList.setBilHist1Sales(Common.convertDoubleToStringMoney(mBrDetails.getBilHist1Sales(), quantityPrecisionUnit));
								invBilList.setBilHist2Sales(Common.convertDoubleToStringMoney(mBrDetails.getBilHist2Sales(), quantityPrecisionUnit));
								invBilList.setBilProjectedSales(Common.convertDoubleToStringMoney(mBrDetails.getBilProjectedSales(), quantityPrecisionUnit));
								invBilList.setBilDeliveryTime(Common.convertDoubleToStringMoney(mBrDetails.getBilDeliveryTime(), quantityPrecisionUnit));
								invBilList.setBilDeliveryBuffer(Common.convertDoubleToStringMoney(mBrDetails.getBilDeliveryBuffer(), quantityPrecisionUnit));
								invBilList.setBilOrderPerYear(Common.convertDoubleToStringMoney(mBrDetails.getBilOrderPerYear(), quantityPrecisionUnit));

								break;

							}

						}

					}

				    return(mapping.findForward("invItemLocationEntry"));

			    }

	        } catch (GlobalNoRecordFoundException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("itemLocationEntry.error.noRecordFound"));

	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvItemLocationEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());

                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (((request.getParameter("saveButton") != null || request.getParameter("recalcButton") != null) &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("invItemLocationEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in InvItemLocationEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
             e.printStackTrace();

         }

          return mapping.findForward("cmnErrorPage");

      }

   }

}