package com.struts.inv.itemlocationentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.inv.itemlocationentry.InvBranchItemLocationList;
import com.struts.util.Common;
import com.struts.util.Constants;


public class InvItemLocationEntryForm extends ActionForm implements Serializable {

	private String[] itemNameSelectedList = new String[0];
	private ArrayList itemNameList = new ArrayList();
	private String[] locationNameSelectedList = new String[0];
	private ArrayList locationNameList = new ArrayList();
	private String[] categoryNameSelectedList = new String[0];
	private ArrayList categoryNameList = new ArrayList();
	private String itemName = null;
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String rack = null;
	private String bin = null;
	private String reorderPoint = null;
	private String reorderQuantity = null;
	private String reorderLevel = null;
	private String dateTo = null;
	private String dateFrom = null;
	private String salesAccount = null;
	private String salesAccountDescription = null;
	private String inventoryAccount = null;
	private String inventoryAccountDescription = null;
	private String costOfSalesAccount = null;
	private String costOfSalesAccountDescription = null;
	private String wipAccount = null;
	private String wipAccountDescription = null;
	private String accruedInventoryAccount = null;
	private String accruedInventoryAccountDescription = null;
	private String salesReturnAccount = null;
	private String salesReturnAccountDescription = null;
	private ArrayList invBrIlList = new ArrayList();

	private String userPermission = new String();
	private String txnStatus = new String();

	private boolean subjectToCommission = false;
	private boolean itemShowAll = false;
	private ArrayList itemDescriptionList = new ArrayList();

	private boolean addByItemList = false;

	private String isItemEntered  = null;

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;

	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public ArrayList getItemNameList() {

		return itemNameList;

	}

	public void setItemNameList(String itemName) {

		itemNameList.add(itemName);

	}

	public void clearItemNameList() {

		itemNameList.clear();

	}

	public String[] getItemNameSelectedList() {

		return itemNameSelectedList;

	}

	public void setItemNameSelectedList(String[] itemNameSelectedList) {

		this.itemNameSelectedList = itemNameSelectedList;

	}

	public ArrayList getLocationNameList() {

		return locationNameList;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public void setLocationNameList(String location) {

		locationNameList.add(location);

	}

	public void clearLocationNameList() {

		locationNameList.clear();

	}

	public String[] getLocationNameSelectedList() {

		return locationNameSelectedList;

	}

	public void setLocationNameSelectedList(String[] locationNameSelectedList) {

		this.locationNameSelectedList = locationNameSelectedList;

	}

	public String[] getCategoryNameSelectedList() {

		return categoryNameSelectedList;

	}

	public void setCategoryNameSelectedList(String[] categoryNameSelectedList) {

		this.categoryNameSelectedList = categoryNameSelectedList;

	}

	public ArrayList getCategoryNameList() {

		return categoryNameList;

	}

	public void setCategoryNameList(String categoryName) {

		categoryNameList.add(categoryName);

	}

	public void clearCategoryNameList() {

		categoryNameList.clear();

	}

	public String getItemClass() {

		return itemClass;

	}


	public void setItemClass(String itemClass) {

		this.itemClass = itemClass;

	}

	public ArrayList getItemClassList() {

		return itemClassList;

	}

	public String getCostOfSalesAccount() {

		return costOfSalesAccount;

	}

	public void setCostOfSalesAccount(String costOfSalesAccount) {

		this.costOfSalesAccount = costOfSalesAccount;

	}

	public String getCostOfSalesAccountDescription() {

		return costOfSalesAccountDescription;

	}

	public void setCostOfSalesAccountDescription(String costOfSalesAccountDescription) {

		this.costOfSalesAccountDescription = costOfSalesAccountDescription;

	}

	public String getInventoryAccount() {

		return inventoryAccount;

	}

	public void setInventoryAccount(String inventoryAccount) {

		this.inventoryAccount = inventoryAccount;

	}

	public String getInventoryAccountDescription() {

		return inventoryAccountDescription;

	}

	public void setInventoryAccountDescription(String inventoryAccountDescription) {

		this.inventoryAccountDescription = inventoryAccountDescription;

	}
	
	public String getRack() {

		return rack;

	}

	public void setRack(String rack) {

		this.rack = rack;

	}
	
	public String getBin() {

		return bin;

	}

	public void setBin(String bin) {

		this.bin = bin;

	}
	
	

	public String getReorderPoint() {

		return reorderPoint;

	}

	public void setReorderPoint(String reorderPoint) {

		this.reorderPoint = reorderPoint;

	}

	public String getReorderQuantity() {

		return reorderQuantity;

	}

	public void setReorderQuantity(String reorderQuantity) {

		this.reorderQuantity = reorderQuantity;

	}

	public String getReorderLevel() {

		return reorderLevel;

	}

	public void setReorderLevel(String reorderLevel) {

		this.reorderLevel = reorderLevel;

	}

	public String getDateFrom() {

		return dateFrom;

	}

	public void setDateFrom(String dateFrom) {

		this.dateFrom = dateFrom;

	}

	public String getDateTo() {

		return dateTo;

	}

	public void setDateTo(String dateTo) {

		this.dateTo = dateTo;

	}

	public String getSalesAccount() {

		return salesAccount;

	}

	public void setSalesAccount(String salesAccount) {

		this.salesAccount = salesAccount;

	}

	public String getSalesAccountDescription() {

		return salesAccountDescription;

	}

	public void setSalesAccountDescription(String salesAccountDescription) {

		this.salesAccountDescription = salesAccountDescription;

	}



	public String getSalesReturnAccount() {

		return salesReturnAccount;

	}

	public void setSalesReturnAccount(String salesReturnAccount) {

		this.salesReturnAccount = salesReturnAccount;

	}

	public String getSalesReturnAccountDescription() {

		return salesReturnAccountDescription;

	}

	public void setSalesReturnAccountDescription(String salesReturnAccountDescription) {

		this.salesReturnAccountDescription = salesReturnAccountDescription;

	}

	public String getWipAccount() {

		return wipAccount;

	}

	public void setWipAccount(String wipAccount) {

		this.wipAccount = wipAccount;

	}

	public String getWipAccountDescription() {

		return wipAccountDescription;

	}

	public void setWipAccountDescription(String wipAccountDescription) {

		this.wipAccountDescription = wipAccountDescription;

	}

	public String getAccruedInventoryAccount() {

		return accruedInventoryAccount;

	}

	public void setAccruedInventoryAccount(String accruedInventoryAccount) {

		this.accruedInventoryAccount = accruedInventoryAccount;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}



	public String getAccruedInventoryAccountDescription() {

		return accruedInventoryAccountDescription;

	}

	public void setAccruedInventoryAccountDescription(String accruedInventoryAccountDescription) {

		this.accruedInventoryAccountDescription = accruedInventoryAccountDescription;

	}

	public Object[] getInvBrIlList(){

		return invBrIlList.toArray();

	}

	public InvBranchItemLocationList getInvBrIlListByIndex(int index){

		return ((InvBranchItemLocationList)invBrIlList.get(index));

	}

	public int getInvBrIlListSize(){

		return(invBrIlList.size());

	}

	public void saveInvBrIlList(Object newInvBrIlList){

		invBrIlList.add(newInvBrIlList);

	}

	public void clearInvBrIlList(){

		invBrIlList.clear();

	}

	public void setInvBrIlList(ArrayList invBrIlList) {

	   	  this.invBrIlList = invBrIlList;

	}

	public boolean getSubjectToCommission() {

		return subjectToCommission;

	}

	public void setSubjectToCommission(boolean subjecToCommision) {

		this.subjectToCommission = subjecToCommision;

	}

	public boolean getItemShowAll() {

		return itemShowAll;

	}

	public void setItemShowAll(boolean itemShowAll) {

		this.itemShowAll = itemShowAll;

	}

	public ArrayList getItemDescriptionList() {

		return itemDescriptionList;

	}

	public void setItemDescriptionList(String itemDesc) {

		itemDescriptionList.add(itemDesc);

	}

	public void clearItemDescriptionList() {

		itemDescriptionList.clear();

	}

	public boolean getAddByItemList() {
		return addByItemList;
	}

	public void setAddByItemList(boolean addByItemList) {
		this.addByItemList = addByItemList;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		for (int i=0; i<invBrIlList.size(); i++) {

		  	  InvBranchItemLocationList actionList = (InvBranchItemLocationList)invBrIlList.get(i);
		  	  actionList.setBranchCheckbox(false);
		  	  actionList.setBilSubjectToCommission(false);

		}

		rack = null;
		bin = null;
		reorderPoint = null;
		reorderQuantity = null;
		reorderLevel = null;
		salesAccount = null;
		salesAccountDescription = null;
		inventoryAccount = null;
		inventoryAccountDescription = null;
		costOfSalesAccount = null;
		costOfSalesAccountDescription = null;
		wipAccount = null;
		wipAccountDescription = null;
		accruedInventoryAccount = null;
		accruedInventoryAccountDescription = null;
		salesReturnAccount = null;
		salesReturnAccountDescription = null;
		itemNameSelectedList = new String[0];
		locationNameSelectedList = new String[0];
		categoryNameSelectedList = new String[0];
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;
		subjectToCommission = false;
		dateFrom = null;
		dateTo = null;
		isItemEntered = null;
		itemName = Constants.GLOBAL_BLANK;

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

			if (locationNameSelectedList.length == 0) {

				errors.add("categoryName",
						new ActionMessage("itemLocationEntry.error.locationNameSelectedListRequired"));

			}

			if (itemNameSelectedList.length == 0) {

				errors.add("categoryName",
						new ActionMessage("itemLocationEntry.error.itemNameSelectedListRequired"));

			}


			if (Common.validateRequired(salesAccount)) {

				errors.add("salesAccount",
						new ActionMessage("itemLocationEntry.error.salesAccountRequired"));

			}

			if (Common.validateRequired(inventoryAccount)) {

				errors.add("inventoryAccount",
						new ActionMessage("itemLocationEntry.error.inventoryAccountRequired"));

			}

			if (Common.validateRequired(costOfSalesAccount)) {

				errors.add("costOfSalesAccount",
						new ActionMessage("itemLocationEntry.error.costOfSalesAccountRequired"));

			}

			if (Common.validateRequired(wipAccount)) {

				errors.add("wipAccount",
						new ActionMessage("itemLocationEntry.error.wipAccountRequired"));

			}

			if (Common.validateRequired(accruedInventoryAccount)) {

				errors.add("accruedInventoryAccount",
						new ActionMessage("itemLocationEntry.error.accruedInventoryAccountRequired"));

			}

			if (Common.validateRequired(salesReturnAccount)) {

				errors.add("salesReturnAccount",
						new ActionMessage("itemLocationEntry.error.salesReturnAccountRequired"));

			}

			boolean branchChecked = false;
			Iterator i = invBrIlList.iterator();
			while (i.hasNext()) {

				InvBranchItemLocationList brList = (InvBranchItemLocationList)i.next();

				 if(brList.getBranchCheckbox()) {
			  		  branchChecked = true;
			  		  break;
			  	  }

			}

			if(!branchChecked) {

				errors.add("categoryName",
						new ActionMessage("itemLocationEntry.error.branchRequired"));

			}

		} else if (request.getParameter("recalcButton") != null) {


			if (locationNameSelectedList.length == 0) {

				errors.add("categoryName",
						new ActionMessage("itemLocationEntry.error.locationNameSelectedListRequired"));

			}

			if (itemNameSelectedList.length == 0) {

				errors.add("categoryName",
						new ActionMessage("itemLocationEntry.error.itemNameSelectedListRequired"));

			}

		}

		return errors;

	}

}