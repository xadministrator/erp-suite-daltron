package com.struts.inv.journal;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvJournalController;
import com.ejb.txn.InvJournalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModDistributionRecordDetails;

public final class InvJournalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvJournalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         InvJournalForm actionForm = (InvJournalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.INV_JOURNAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invJournal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize InvJournalController EJB
*******************************************************/

         InvJournalControllerHome homeJR = null;
         InvJournalController ejbJR = null;

         try {

            homeJR = (InvJournalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvJournalControllerEJB", InvJournalControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJR = homeJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call InvJournalController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short journalLineNumber = 0;
         
         try {
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode());
        	// precisionUnit = (short)(10);
            journalLineNumber = ejbJR.getAdPrfInvJournalLineNumber(user.getCmpCode());
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	     

/*******************************************************
   -- Inv JR Save Action --
*******************************************************/
         
         if (request.getParameter("saveButton") != null &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             	ArrayList drList = new ArrayList();
                int lineNumber = 0;  
                
                double TOTAL_DEBIT = 0;
                double TOTAL_CREDIT = 0;         
                           
                for (int i = 0; i < actionForm.getInvJRListSize(); i++) {
                	
            	   InvJournalList invJRList = actionForm.getInvJRByIndex(i);           	   
            	              	   
            	   if (Common.validateRequired(invJRList.getAccountNumber()) &&
            	   		Common.validateRequired(invJRList.getDebitAmount()) &&
            	   		Common.validateRequired(invJRList.getCreditAmount())) continue;
            	   
            	   byte isDebit = 0;
    	           double amount = 0d;

    		       if (!Common.validateRequired(invJRList.getDebitAmount())) {
    		       	
    		          isDebit = 1;
    		          amount = Common.convertStringMoneyToDouble(invJRList.getDebitAmount(), precisionUnit);
    		          
    		          TOTAL_DEBIT += amount;
    		          
    		       } else {
    		       	
    		          isDebit = 0;
    		          amount = Common.convertStringMoneyToDouble(invJRList.getCreditAmount(), precisionUnit); 
    		          
    		          TOTAL_CREDIT += amount;
    		       }
    	       
    		       lineNumber++;

            	   InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
            	   
            	   mdetails.setDrLine((short)(lineNumber));
            	   mdetails.setDrClass(invJRList.getDrClass());
            	   mdetails.setDrDebit(isDebit);
            	   mdetails.setDrAmount(amount);
            	   mdetails.setDrCoaAccountNumber(invJRList.getAccountNumber());
            	   
            	   drList.add(mdetails);
                	
                }
                
                TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
                TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
                
                if (TOTAL_DEBIT != TOTAL_CREDIT) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invJournal.error.journalNotBalance"));
                         
                    saveErrors(request, new ActionMessages(errors));
                    return (mapping.findForward("invJournal"));
                	
                }
                
                
                try {
                	
                	ejbJR.saveInvDrEntry(actionForm.getAdjustmentCode(), drList, actionForm.getTransaction(), new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

                } catch (GlobalAccountNumberInvalidException ex) {

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invJournal.error.accountNumberInvalid", ex.getMessage()));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in InvJournalAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 

                   }

                }
            
            
            
/*******************************************************
	-- Inv JR Add Lines Action --
*******************************************************/
	
	      } else if(request.getParameter("addLinesButton") != null) {
	      	
	      	int listSize = actionForm.getInvJRListSize();
	                                
	         for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	InvJournalList invJRList = new InvJournalList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	invJRList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveInvJRList(invJRList);
	        	
	        }	        
	        
	        return(mapping.findForward("invJournal"));

/*******************************************************
    -- Inv JR Delete Lines Action --
*******************************************************/

          } else if(request.getParameter("deleteLinesButton") != null) {
          	
          	for (int i = 0; i<actionForm.getInvJRListSize(); i++) {
            	
            	   InvJournalList invJRList = actionForm.getInvJRByIndex(i);
            	   
            	   if (invJRList.getDeleteCheckbox()) {
            	   	
            	   	   actionForm.deleteInvJRList(i);
            	   	   i--;
            	   }
            	   
             }
             
             for (int i = 0; i<actionForm.getInvJRListSize(); i++) {
            	
             	InvJournalList invJRList = actionForm.getInvJRByIndex(i);
            	   
             	invJRList.setLineNumber(String.valueOf(i+1));
            	   
             }
 	        
 	        return(mapping.findForward("invJournal"));	          

/*******************************************************
   -- Inv JR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Inv JR Back Action --
*******************************************************/

         } else if (request.getParameter("backButton") != null) {

            String path = null;
            
            if (actionForm.getTransaction().equals("ADJUSTMENT")) {
            	
            	path = "/invAdjustmentEntry.do?forward=1&adjustmentCode=" +
            	    actionForm.getAdjustmentCode();
            	            	    
            } else if (actionForm.getTransaction().equals("BUILD/UNBUILD ASSEMBLY")) {
            	
                path = "/invBuildUnbuildAssemblyEntry.do?forward=1&buildUnbuildAssemblyCode=" +
            	    actionForm.getAdjustmentCode();
            	    	
            } else if (actionForm.getTransaction().equals("OVERHEAD")) {
            	
            	path = "/invOverheadEntry.do?forward=1&overheadCode=" +
            	    actionForm.getAdjustmentCode();
            	    
            } else if (actionForm.getTransaction().equals("STOCK ISSUANCE")) {
            	
            	path = "/invStockIssuanceEntry.do?forward=1&stockIssuanceCode=" +
            	    actionForm.getAdjustmentCode();
            	    
            } else if (actionForm.getTransaction().equals("ASSEMBLY TRANSFER")) {
            	
            	path = "/invAssemblyTransferEntry.do?forward=1&assemblyTransferCode=" +
            	    actionForm.getAdjustmentCode();
            	    
            } else if (actionForm.getTransaction().equals("STOCK TRANSFER")) {
                
                path = "/invStockTransferEntry.do?forward=1&stockTransferCode=" + 
                	actionForm.getAdjustmentCode();
                
            } else if (actionForm.getTransaction().equals("BRANCH STOCK TRANSFER OUT")) {
                
                path = "/invBranchStockTransferOutEntry.do?forward=1&branchStockTransferCode=" + 
                	actionForm.getAdjustmentCode();
                
            } else if (actionForm.getTransaction().equals("BRANCH STOCK TRANSFER IN")) {
                
                path = "/invBranchStockTransferInEntry.do?forward=1&branchStockTransferCode=" + 
                	actionForm.getAdjustmentCode();

            }
            
            return(new ActionForward(path));
            
/*******************************************************
   -- Inv JR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invJournal");

            }
            
            try {
            	
        		ArrayList list = new ArrayList();
        		Iterator i = null;
        		
        		actionForm.clearInvJRList();

        		if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("ADJUSTMENT")) ||
        		    actionForm.getTransaction().equals("ADJUSTMENT")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
        			
        			list = ejbJR.getInvDrByAdjCode(actionForm.getAdjustmentCode(), user.getCmpCode());
	            			        			            			
        		} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("BUILD/UNBUILD ASSEMBLY")) ||
        		    actionForm.getTransaction().equals("BUILD/UNBUILD ASSEMBLY")) {
        		    
        		    if (request.getParameter("transactionCode") != null) 
        			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
        			
        			list = ejbJR.getInvDrByBuaCode(actionForm.getAdjustmentCode(), user.getCmpCode());
        		    
        		} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("OVERHEAD")) ||
            		    actionForm.getTransaction().equals("OVERHEAD")) {
            		    
        		    if (request.getParameter("transactionCode") != null) 
        			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
        			
        			list = ejbJR.getInvDrByOhCode(actionForm.getAdjustmentCode(), user.getCmpCode());
            		    
            	} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("STOCK ISSUANCE")) ||
            		    actionForm.getTransaction().equals("STOCK ISSUANCE")) {
        		    
	    		    if (request.getParameter("transactionCode") != null) 
	    			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
	    			
	    			list = ejbJR.getInvDrBySiCode(actionForm.getAdjustmentCode(), user.getCmpCode());
	        		    
	        	} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("ASSEMBLY TRANSFER")) ||
            		    actionForm.getTransaction().equals("ASSEMBLY TRANSFER")) {
        		    
	    		    if (request.getParameter("transactionCode") != null) 
	    			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
	    			
	    			list = ejbJR.getInvDrByAtrCode(actionForm.getAdjustmentCode(), user.getCmpCode());
	        		    
	        	} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("STOCK TRANSFER")) || 
	        	        actionForm.getTransaction().equals("STOCK TRANSFER")) {
	        	    
	        	    if (request.getParameter("transactionCode") != null) 
		    			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
		    			
	        	    list = ejbJR.getInvDrByStCode(actionForm.getAdjustmentCode(), user.getCmpCode());
	        	
	        	} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("BRANCH STOCK TRANSFER OUT")) || 
	        	        actionForm.getTransaction().equals("BRANCH STOCK TRANSFER IN")) {
	        	    
	        	    if (request.getParameter("transactionCode") != null) 
		    			   actionForm.setAdjustmentCode(new Integer(request.getParameter("transactionCode")));
		    			
	        	    list = ejbJR.getInvDrByBstCode(actionForm.getAdjustmentCode(), user.getCmpCode());
	        	}
        		
        		if (request.getParameter("forward") != null) {

	      			actionForm.setNumber(request.getParameter("transactionNumber"));
	       			actionForm.setDate(request.getParameter("transactionDate"));
	       			actionForm.setEnableFields(request.getParameter("transactionEnableFields"));
	       			
	       		}
        		
        		double totalDebit = 0;
        		double totalCredit = 0;
        		
        		i = list.iterator();        		       		
        			
        		while (i.hasNext()) {
    				
    				InvModDistributionRecordDetails mdetails = (InvModDistributionRecordDetails)i.next();
    				
    				String debitAmount = null;
    				String creditAmount = null;
    				
    				if (mdetails.getDrDebit() == 1) {
    					
    					debitAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalDebit = totalDebit + mdetails.getDrAmount();
    					
    				} else {
    					
    					creditAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalCredit = totalCredit + mdetails.getDrAmount();
    					
    				}
    				
    				ArrayList classList = new ArrayList();
    				classList.add(mdetails.getDrClass());
    				
    				InvJournalList invJRList = new InvJournalList(actionForm,
    				    mdetails.getDrCode(), 
    				    Common.convertShortToString(mdetails.getDrLine()),
    				    mdetails.getDrCoaAccountNumber(),
    				    debitAmount,
    				    creditAmount,
    	    		    mdetails.getDrClass(),
    				    mdetails.getDrCoaAccountDescription());
    				
    				invJRList.setDrClassList(classList);
    				        				
    				actionForm.saveInvJRList(invJRList);
    				        				
    			}
        		System.out.println("INV JOURNAL totalDebit="+totalDebit);
        		System.out.println("INV JOURNAL totalCredit="+totalCredit);
        		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(totalDebit, precisionUnit));
        		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(totalCredit, precisionUnit));
        		
        		int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
		        
		        for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	InvJournalList invJRList = new InvJournalList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	    
		        	invJRList.setDrClassList(this.getDrClassList());
			        	
			        actionForm.saveInvJRList(invJRList);
			        			        	
		        }
    			    		
    			this.setFormProperties(actionForm);    			    				      
    				            	
            } catch (GlobalNoRecordFoundException ex) {
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {
				
				saveErrors(request, new ActionMessages(errors));

			} else {
				
				if ((request.getParameter("saveButton") != null &&
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

				}
				
			}
            
			actionForm.reset(mapping, request);
			
			return(mapping.findForward("invJournal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
          if (log.isInfoEnabled()) {

             log.info("Exception caught in InvJournalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

       }

    }
   
   private void setFormProperties(InvJournalForm actionForm) {
	
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getEnableFields().equals("true")) {
			
	    		actionForm.setShowSaveButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
			
			} else {
				
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				
			}
			
		} else {
			
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			    		
		}

   }

	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
	 	classList.add("OTHER");
	 	
	 	return classList;
		
	}
	
}