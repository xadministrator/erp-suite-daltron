package com.struts.inv.journal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvJournalForm extends ActionForm implements Serializable {

   private Integer adjustmentCode = null;
   private String transaction = null;
   private String date = null;
   private String number = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String accountNumber = null;
   private String description = null;
   private String enableFields = null;

   private String backButton = null;
   private String closeButton = null;
   private ArrayList invJRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveButton = false;

   public int getRowSelected() {

      return rowSelected;

   }

   public InvJournalList getInvJRByIndex(int index) {

      return((InvJournalList)invJRList.get(index));

   }

   public Object[] getInvJRList() {

      return invJRList.toArray();

   }

   public int getInvJRListSize() {

      return invJRList.size();

   }

   public void saveInvJRList(Object newInvJRList) {

      invJRList.add(newInvJRList);

   }

   public void clearInvJRList() {
      invJRList.clear();
   }

   public void deleteInvJRList(int rowSelected) {

      invJRList.remove(rowSelected);

   }

   public void setBackButton(String backButton) {

      this.backButton = backButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public Integer getAdjustmentCode() {

      return adjustmentCode;

   }

   public void setAdjustmentCode(Integer adjustmentCode) {

      this.adjustmentCode = adjustmentCode;

   }

   public String getTransaction() {

      return transaction;

   }

   public void setTransaction(String transaction) {

      this.transaction = transaction;

   }

   public String getDate() {

      return date;

   }

   public void setDate(String date) {

      this.date = date;

   }

   public String getNumber() {

      return number;

   }

   public void setNumber(String number) {

      this.number = number;

   }
   
   public String getTotalDebit() {
   	
   	  return totalDebit;
   	
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	
   }
   
   public String getTotalCredit() {
   	
   	  return totalCredit;
   	
   }
   
   public void setTotalCredit(String totalCredit) {
   	
   	  this.totalCredit = totalCredit;
   	
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	
   }
   
   public void setAccountNumber(String accountNumber) {
   	
   	  this.accountNumber = accountNumber;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getEnableFields() {
   	
   	  return enableFields;
   	  
   }
   
   public void setEnableFields(String enableFields) {
   	
   	  this.enableFields = enableFields;
   	  
   }   
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      backButton = null;
      closeButton = null;    

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

    ActionErrors errors = new ActionErrors();
    if (request.getParameter("saveButton") != null) {
    	 
    	int numberOfLines = 0;
   	 
   	 Iterator i = invJRList.iterator();      	 
   	 
   	 while (i.hasNext()) {
   	 	
   	 	 InvJournalList drList = (InvJournalList)i.next();      	 	 
   	 	       	 	 
   	 	 if (Common.validateRequired(drList.getAccountNumber()) &&
   	 	     Common.validateRequired(drList.getDebitAmount()) &&
   	 	     Common.validateRequired(drList.getCreditAmount())) continue;
   	 	     
   	 	 numberOfLines++;
   	 
	         if(Common.validateRequired(drList.getAccountNumber())){
	            errors.add("accountNumber",
	               new ActionMessage("invJournal.error.accountNumberRequired", drList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(drList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("invJournal.error.debitAmountInvalid", drList.getLineNumber()));
	         }
	         if(!Common.validateMoneyFormat(drList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("invJournal.error.creditAmountInvalid", drList.getLineNumber()));
	         }
			 if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("invJournal.error.debitCreditAmountRequired", drList.getLineNumber()));
			 }
			 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("invJournal.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
      	
      	errors.add("transaction",
            new ActionMessage("invJournal.error.journalMustHaveLine"));
      	
      }
    	
    }
    
    return errors;

 }
}