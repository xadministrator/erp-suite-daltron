package com.struts.inv.lineitemtemplate;

import java.io.Serializable;
import java.util.ArrayList;

public class InvLineItemList implements Serializable {
	
	private Integer lineItemCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private boolean deleteCheckbox = false;
	
	private String isItemEntered = null;
	
	private InvLineItemTemplateForm parentBean;
	
	public InvLineItemList(InvLineItemTemplateForm parentBean,
			Integer lineItemCode,
			String lineNumber, 
			String location,
			String itemName, 
			String itemDescription, 
			String unit){
		
		this.parentBean = parentBean;
		this.lineItemCode = lineItemCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.unit = unit;
		
	}
	
	public Integer getLineItemCode(){
		
		return(lineItemCode);
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
			
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
		
		unitList.clear();
		
	}
			
	public boolean getDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowLISelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
}