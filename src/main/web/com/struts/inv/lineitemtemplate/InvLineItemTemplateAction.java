package com.struts.inv.lineitemtemplate;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.InvLineItemTemplateController;
import com.ejb.txn.InvLineItemTemplateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvLineItemTemplateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvLineItemTemplateAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    HttpSession session = request.getSession();
    
    try {

/*******************************************************
 Check if user has a session
*******************************************************/

       User user = (User) session.getAttribute(Constants.USER_KEY);

       if (user != null) {

          if (log.isInfoEnabled()) {

              log.info("InvLineItemTemplateAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
              "' performed this action on session " + session.getId());
          }

       } else {

          if (log.isInfoEnabled()) {

             log.info("User is not logged on in session" + session.getId());

          }

          return(mapping.findForward("adLogon"));

       }
       
       InvLineItemTemplateForm actionForm = (InvLineItemTemplateForm)form;
       
       if (request.getParameter("caller") != null) {
			
			actionForm.setCaller(request.getParameter("caller"));
			
		}

       
       String frParam = Common.getUserPermission(user, Constants.INV_LINE_ITEM_TEMPLATE_ID);

       if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
             if (!fieldErrors.isEmpty()) {

                saveErrors(request, new ActionMessages(fieldErrors));

                return mapping.findForward("invLineItemTemplate");
             }

          }

          actionForm.setUserPermission(frParam.trim());

       } else {

          actionForm.setUserPermission(Constants.NO_ACCESS);

       }

/*******************************************************
 Initialize InvLineItemTemplateController EJB
*******************************************************/

       InvLineItemTemplateControllerHome homeLIT = null;
       InvLineItemTemplateController ejbLIT = null;

       try {

          homeLIT = (InvLineItemTemplateControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvLineItemTemplateControllerEJB", InvLineItemTemplateControllerHome.class);
          
       } catch (NamingException e) {

          if (log.isInfoEnabled()) {

              log.info("NamingException caught in InvLineItemTemplateAction.execute(): " + e.getMessage() +
             " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

       try {

          ejbLIT = homeLIT.create();
          
       } catch (CreateException e) {

          if (log.isInfoEnabled()) {

              log.info("CreateException caught in InvLineItemTemplateAction.execute(): " + e.getMessage() +
             " session: " + session.getId());

          }
          return mapping.findForward("cmnErrorPage");

       }

       ActionErrors errors = new ActionErrors();
       
/*******************************************************
 Call InvLineItemTemplateController EJB
 getGlFcPrecisionUnit
*******************************************************/
       
       short journalLineNumber = 0;
       
       try {
       	
       	journalLineNumber = ejbLIT.getAdPrfInvJournalLineNumber(user.getCmpCode());   
       	
       } catch(EJBException ex) {
       	
       	if (log.isInfoEnabled()) {
       		
       		log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
       				" session: " + session.getId());
       	}
       	
       	return(mapping.findForward("cmnErrorPage"));
       	
       }
       
       
/*******************************************************
 -- Inv LIT Save Action --
*******************************************************/

       if (request.getParameter("saveButton") != null &&
       		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
       	
       	InvLineItemTemplateDetails details = new InvLineItemTemplateDetails();
       	
       	details.setLitName(actionForm.getTemplateName());
       	details.setLitDescription(actionForm.getDescription());
       	
       	ArrayList liList = new ArrayList();
       	
       	for (int i = 0; i<actionForm.getInvLIListSize(); i++) {
       		
       		InvLineItemList invLIList = actionForm.getInvLIByIndex(i);           	   
       		
       		if (Common.validateRequired(invLIList.getLocation()) &&
       				Common.validateRequired(invLIList.getItemName()) &&
					Common.validateRequired(invLIList.getUnit())) continue;
       		
       		InvModLineItemDetails mdetails = new InvModLineItemDetails();
       		
       		mdetails.setLiLine(Common.convertStringToShort(invLIList.getLineNumber()));
       		mdetails.setLiIiName(invLIList.getItemName());
       		mdetails.setLiLocName(invLIList.getLocation());
       		mdetails.setLiUomName(invLIList.getUnit());
       		
       		liList.add(mdetails);
       		
       	} 
       	
       	try {
       		
       		ejbLIT.addInvLitEntry(details, liList, user.getCmpCode());
       		
       	} catch (GlobalRecordAlreadyExistException ex) {
       		
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
       				new ActionMessage("lineItemTemplate.error.recordAlreadyExist"));
       		
       	} catch (GlobalInvItemLocationNotFoundException ex) {
       		
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
       				new ActionMessage("lineItemTemplate.error.itemLocationNotFound", ex.getMessage()));
       		
       	} catch (EJBException ex) {
       		
       		if (log.isInfoEnabled()) {
       			
       			log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
       					" session: " + session.getId());
       			return mapping.findForward("cmnErrorPage"); 
       			
       		}
       		
       	}
       	
/*******************************************************
 -- Inv LIT Update Action --
*******************************************************/

       } else if (request.getParameter("updateButton") != null &&
          actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          
          InvLineItemTemplateList invLITList = actionForm.getInvLITByIndex(actionForm.getRowLITSelected());
          
          InvLineItemTemplateDetails details = new InvLineItemTemplateDetails();
          
          details.setLitCode(invLITList.getTemplateCode());
          details.setLitName(actionForm.getTemplateName());
          details.setLitDescription(actionForm.getDescription());
          
          ArrayList liList = new ArrayList();
         	
         	for (int i = 0; i<actionForm.getInvLIListSize(); i++) {
         		
         		InvLineItemList invLIList = actionForm.getInvLIByIndex(i);           	   
         		
         		if (Common.validateRequired(invLIList.getLocation()) &&
         				Common.validateRequired(invLIList.getItemName()) &&
  					Common.validateRequired(invLIList.getUnit())) continue;
         		
         		InvModLineItemDetails mdetails = new InvModLineItemDetails();
         		
         		mdetails.setLiLine(Common.convertStringToShort(invLIList.getLineNumber()));
         		mdetails.setLiIiName(invLIList.getItemName());
         		mdetails.setLiLocName(invLIList.getLocation());
         		mdetails.setLiUomName(invLIList.getUnit());
         		
         		liList.add(mdetails);
         		
         	} 
          
          try {
          	
          	ejbLIT.updateInvLitEntry(details, liList, user.getCmpCode());

          } catch (GlobalRecordAlreadyDeletedException ex) {
              
             errors.add(ActionMessages.GLOBAL_MESSAGE,
               new ActionMessage("lineItemTemplate.error.recordAlreadyDeleted"));

          } catch (GlobalInvItemLocationNotFoundException ex) {
       		
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
       				new ActionMessage("lineItemTemplate.error.itemLocationNotFound", ex.getMessage()));
       		
       	} catch (EJBException ex) {

             if (log.isInfoEnabled()) {

                log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
                " session: " + session.getId());
                return mapping.findForward("cmnErrorPage"); 
                
             }

          }
          
/*******************************************************
 -- Inv LIT Cancel Action --
*******************************************************/

       } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
 -- Inv LIT Edit Action --
*******************************************************/

       } else if (request.getParameter("invLITList[" + 
          actionForm.getRowLITSelected() + "].editButton") != null &&
		  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
       
       	Object[] invLITList = actionForm.getInvLITList();
       	
       	// populate line item list
        
        ArrayList list = null;
        
        try {                                  
            
            list = ejbLIT.getInvLiAllByLitCode(((InvLineItemTemplateList)invLITList[actionForm.getRowLITSelected()]).getTemplateCode(), user.getCmpCode());
            
        } catch(GlobalNoRecordFoundException ex) {}
        
        actionForm.clearInvLIList();
		
        Iterator i = list.iterator();
        
        while(i.hasNext()) {
        	
        	InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();
        	
        	InvLineItemList iliList = new InvLineItemList(actionForm,
        			liDetails.getLiCode(),
        			Common.convertShortToString(liDetails.getLiLine()),
					liDetails.getLiLocName(),
					liDetails.getLiIiName(),
					liDetails.getLiIiDescription(),
					liDetails.getLiUomName());
        	
        	actionForm.saveInvLIList(iliList);
        	
        }
        
        actionForm.reset(mapping, request);
       	
        for(int j=0; j<actionForm.getInvLIListSize(); j++) {
        	
        	InvLineItemList liList = actionForm.getInvLIByIndex(j);
        	
        	// populate location
        	
        	liList.setLocationList(actionForm.getLocationList());
        	
        	// populate unit field class
       		
       		ArrayList uomList = new ArrayList();
       		uomList = ejbLIT.getInvUomByIiName(liList.getItemName(), user.getCmpCode());
       		
       		liList.clearUnitList();
       		liList.setUnitList(Constants.GLOBAL_BLANK);
       		
       		i = uomList.iterator();
       		while (i.hasNext()) {
       			
       			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
       			liList.setUnitList(mUomDetails.getUomName());
       			
       		}
        	
        }
        
        // populate template
        
        actionForm.setLitCode(((InvLineItemTemplateList)invLITList[actionForm.getRowLITSelected()]).getTemplateCode());
        actionForm.setTemplateName(((InvLineItemTemplateList)invLITList[actionForm.getRowLITSelected()]).getTemplateName());
        actionForm.setDescription(((InvLineItemTemplateList)invLITList[actionForm.getRowLITSelected()]).getDescription());
        
        return mapping.findForward("invLineItemTemplate");
       	
/*******************************************************
 -- Inv LIT Delete Action --
*******************************************************/
       	
       } else if (request.getParameter("invLITList[" +
          actionForm.getRowLITSelected() + "].deleteButton") != null &&
          actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          InvLineItemTemplateList invLITList =
            actionForm.getInvLITByIndex(actionForm.getRowLITSelected());

          try {
          	
	            ejbLIT.deleteInvLitEntry(invLITList.getTemplateCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("lineItemTemplate.error.deleteItemTemplateAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
             errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("lineItemTemplate.error.recordAlreadyDeleted"));
	           
          } catch(EJBException ex) {
          	
             if (log.isInfoEnabled()) {
             	
                log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
                " session: " + session.getId());
             }
             
             return(mapping.findForward("cmnErrorPage"));
             
          } 
          
/*******************************************************
 -- Inv LIT Item Enter Action --
*******************************************************/
          
       } else if(!Common.validateRequired(request.getParameter("invLIList[" + 
       		actionForm.getRowLISelected() + "].isItemEntered"))) {
       	
       	InvLineItemList invLIList = actionForm.getInvLIByIndex(actionForm.getRowLISelected());
       	
       	try {
       		
       		// populate unit field class
       		
       		ArrayList uomList = new ArrayList();
       		uomList = ejbLIT.getInvUomByIiName(invLIList.getItemName(), user.getCmpCode());
       		
       		invLIList.clearUnitList();
       		invLIList.setUnitList(Constants.GLOBAL_BLANK);
       		
       		Iterator i = uomList.iterator();
       		while (i.hasNext()) {
       			
       			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
       			
       			invLIList.setUnitList(mUomDetails.getUomName());
       			
       			if (mUomDetails.isDefault()) {
       				
       				invLIList.setUnit(mUomDetails.getUomName());      				
       				
       			}      			
       			
       		}
       		
       	} catch (EJBException ex) {
       		
       		if (log.isInfoEnabled()) {
       			
       			log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
       					" session: " + session.getId());
       			return mapping.findForward("cmnErrorPage"); 
       			
       		}
       		
       	}  
       	
       	return(mapping.findForward("invLineItemTemplate"));
       	
/*******************************************************
 -- Inv LIT Add Lines Action --
*******************************************************/
       	
       } else if(request.getParameter("addLinesButton") != null) {
       	
       	int listSize = actionForm.getInvLIListSize();
       	
       	for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
       		
       		InvLineItemList invLIList = new InvLineItemList(actionForm,
       				null, String.valueOf(x), null, null, null, null);
       		
       		invLIList.setLocationList(actionForm.getLocationList());
       		
       		actionForm.saveInvLIList(invLIList);
       		
       	}	        
       	
       	return(mapping.findForward("invLineItemTemplate"));
       	
/*******************************************************
 -- Inv LIT Delete Lines Action --
*******************************************************/
       	
       } else if(request.getParameter("deleteLinesButton") != null) {
       	
       	for (int i = 0; i<actionForm.getInvLIListSize(); i++) {
       		
       		InvLineItemList invLIList = actionForm.getInvLIByIndex(i);
       		
       		if (invLIList.getDeleteCheckbox()) {
       			
       			actionForm.deleteInvLIList(i);
       			i--;
       		}
       		
       	}
       	
       	for (int i = 0; i<actionForm.getInvLIListSize(); i++) {
       		
       		InvLineItemList invLIList = actionForm.getInvLIByIndex(i);
       		
       		invLIList.setLineNumber(String.valueOf(i+1));
       		
       	}
       	
       	return(mapping.findForward("invLineItemTemplate"));
       	
/******************************************************* 
 -- Inv LIT Close Action --
*******************************************************/
       	
       } else if (request.getParameter("closeButton") != null) {
       	
       	return(mapping.findForward(actionForm.getCaller()));
       	
/*******************************************************
 -- Ar TC Load Action --
*******************************************************/
       	
       }
       
       if (frParam != null) {
       	
       	if (!errors.isEmpty()) {
       		
       		saveErrors(request, new ActionMessages(errors));
       		return mapping.findForward("invLineItemTemplate");
       		
       	}
       	
       	actionForm.reset(mapping, request);
       	
       	ArrayList list = null;
       	Iterator i = null;
       	
       	try {
       		
       		actionForm.clearLocationList();
       		
       		list = ejbLIT.getInvLocAll(user.getCmpCode());
       		
       		if (list == null || list.size() == 0) {
       			
       			actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
       			
       		} else {
       			
       			i = list.iterator();
       			
       			while (i.hasNext()) {
       				
       				actionForm.setLocationList((String)i.next());
       				
       			}
       			
       		}
       		
       		actionForm.clearInvLIList();
       		
       		for (int x = 1; x <= journalLineNumber; x++) {
       			
       			InvLineItemList invLIList = new InvLineItemList(actionForm, 
       					null, new Integer(x).toString(), null, null, null, null);	
       			
       			invLIList.setLocationList(actionForm.getLocationList());
       			
       			invLIList.setUnitList(Constants.GLOBAL_BLANK);
       			invLIList.setUnitList("Select Item First");
       			
       			actionForm.saveInvLIList(invLIList);
       			
       		}
       		
       	} catch (EJBException ex) {
       		
       		if (log.isInfoEnabled()) {
       			
       			log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
       					" session: " + session.getId());
       			return mapping.findForward("cmnErrorPage"); 
       			
       		}
       		
       	}

       	try {
       		
       		actionForm.clearInvLITList();	        
       		
       		list = ejbLIT.getInvLitAll(user.getCmpCode()); 
       		
       		i = list.iterator();
       		
       		while(i.hasNext()) {
       			
       			InvLineItemTemplateDetails mdetails = (InvLineItemTemplateDetails)i.next();
       		
       			InvLineItemTemplateList invLITList = new InvLineItemTemplateList(actionForm,
       					mdetails.getLitCode(),
						mdetails.getLitName(),
						mdetails.getLitDescription());                            
       			
       			actionForm.saveInvLITList(invLITList);
       			
       		}
       		
       	} catch (GlobalNoRecordFoundException ex) {
       		       		       		
       	} catch (EJBException ex) {
       		
       		if (log.isInfoEnabled()) {
       			
       			log.info("EJBException caught in InvLineItemTemplateAction.execute(): " + ex.getMessage() +
       					" session: " + session.getId());
       			return mapping.findForward("cmnErrorPage"); 
       			
       		}
       		
       	}          
       	
       	if (!errors.isEmpty()) {
       		
       		saveErrors(request, new ActionMessages(errors));
       		
       	} else {
       		
       		if (request.getParameter("saveButton") != null || 
       				request.getParameter("updateButton") != null) {
       			
       			actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
       			
       		}
       	}
       	
       	actionForm.reset(mapping, request);
       	
       	actionForm.setPageState(Constants.PAGE_STATE_SAVE);
       	
       	return(mapping.findForward("invLineItemTemplate"));
       	
       } else {
       	
          errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
          saveErrors(request, new ActionMessages(errors));

          return(mapping.findForward("cmnMain"));

       }

    } catch(Exception e) {

/*******************************************************
 System Failed: Forward to error page 
*******************************************************/

       if (log.isInfoEnabled()) {

           log.info("Exception caught in InvLineItemTemplateAction.execute(): " + e.getMessage()
              + " session: " + session.getId());
       }

        return mapping.findForward("cmnErrorPage");

    }

 }
   
}