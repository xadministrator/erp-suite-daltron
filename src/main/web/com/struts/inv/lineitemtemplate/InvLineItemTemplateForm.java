package com.struts.inv.lineitemtemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvLineItemTemplateForm extends ActionForm implements Serializable {
	
	private Integer litCode = null;
	private String templateName = null;
	private String description = null;
	
	private String userPermission = new String();
	private String txnStatus = new String();
	private String pageState = new String();
	
	private ArrayList invLITList = new ArrayList();
	private int RowLITSelected = 0;
	private ArrayList invLIList = new ArrayList();
	private int RowLISelected = 0;
	private ArrayList locationList = new ArrayList();   
	
	private String caller = null;
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getLitCode() {
		
		return litCode;
		
	}
	
	public void setLitCode(Integer litCode) {
		
		this.litCode = litCode;
		
	}
	
	public String getTemplateName() {
		
		return templateName;
		
	}
	
	public void setTemplateName(String templateName) {
		
		this.templateName = templateName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}   
	
	public String getCaller() {
		
		return caller;
		
	}
	
	public void setCaller(String caller) {
		
		this.caller = caller;		
		
	}
	
	public InvLineItemTemplateList getInvLITByIndex(int index){
		
		return((InvLineItemTemplateList)invLITList.get(index));
		
	}
	
	public Object[] getInvLITList(){
		
		return(invLITList.toArray());
		
	}
	
	public int getInvLITListSize(){
		
		return(invLITList.size());
		
	}
	
	public void saveInvLITList(Object newInvLITList){
		
		invLITList.add(newInvLITList);
		
	}
	
	public void clearInvLITList(){
		
		invLITList.clear();
		
	}
	
	public void setRowLITSelected(Object selectedInvLITList, boolean isEdit){
		
		this.RowLITSelected = invLITList.indexOf(selectedInvLITList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public int getRowLITSelected(){
		
		return RowLITSelected;
		
	}
	
	public void updateInvLITRow(int rowSelected, Object newInvLITList){
		
		invLITList.set(rowSelected, newInvLITList);
		
	}
	
	public void deleteInvLITList(int rowSelected){
		
		invLITList.remove(rowSelected);
		
	}   
	
	public InvLineItemList getInvLIByIndex(int index){
		
		return((InvLineItemList)invLIList.get(index));
		
	}
	
	public Object[] getInvLIList(){
		
		return(invLIList.toArray());
		
	}
	
	public int getInvLIListSize(){
		
		return(invLIList.size());
		
	}
	
	public void saveInvLIList(Object newInvLIList){
		
		invLIList.add(newInvLIList);
		
	}
	
	public void clearInvLIList(){
		
		invLIList.clear();
		
	}
	
	public void setRowLISelected(Object selectedInvLIList, boolean isEdit){
		
		this.RowLISelected = invLIList.indexOf(selectedInvLIList);
		
	}
	
	public int getRowLISelected(){
		
		return RowLISelected;
		
	}
	
	
	public void updateInvLIRow(int rowLISelected, Object newInvLIList){
		
		invLIList.set(rowLISelected, newInvLIList);
		
	}
	
	public void deleteInvLIList(int rowSelected){
		
		invLIList.remove(rowSelected);
		
	}   
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		templateName = null;
		description = null;
		
		for (int i=0; i<invLIList.size(); i++) {
			
			InvLineItemList actionList = (InvLineItemList)invLIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setLocationList(locationList);
			
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("saveButton") != null) {
			
			if (Common.validateRequired(templateName)) {
				
				errors.add("templateName",
						new ActionMessage("lineItemTemplate.error.templateNameRequired"));
				
			}
			
			int numOfLines = 0;
			
			Iterator i = invLIList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvLineItemList liList = (InvLineItemList)i.next();      	 	 
				
				if (Common.validateRequired(liList.getLocation()) &&
						Common.validateRequired(liList.getItemName()) &&
						Common.validateRequired(liList.getUnit())) continue;
				
				numOfLines++;
				
				if(Common.validateRequired(liList.getLocation()) || liList.getLocation().equals(Constants.GLOBAL_BLANK)){
					errors.add("location",
							new ActionMessage("lineItemTemplate.error.locationRequired", liList.getLineNumber()));
				}
				
				if(Common.validateRequired(liList.getItemName()) || liList.getItemName().equals(Constants.GLOBAL_BLANK)){
					errors.add("itemName",
							new ActionMessage("lineItemTemplate.error.itemNameRequired", liList.getLineNumber()));
				}
				
				if(Common.validateRequired(liList.getUnit()) || liList.getUnit().equals(Constants.GLOBAL_BLANK)){
					errors.add("unit",
							new ActionMessage("lineItemTemplate.error.unitRequired", liList.getLineNumber()));
				}
				
			}
			
			if (numOfLines == 0) {
				
				errors.add("templateName",
						new ActionMessage("lineItemTemplate.error.templateMustHaveLine"));
				
			}
			
			
		}
		
		return errors;
		
	}
	
}