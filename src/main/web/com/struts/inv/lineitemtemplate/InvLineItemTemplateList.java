package com.struts.inv.lineitemtemplate;

import java.io.Serializable;

public class InvLineItemTemplateList implements Serializable {

    private Integer templateCode = null;
    private String templateName = null;
    private String description = null;
    
	private String editButton = null;
	private String deleteButton = null;

	private InvLineItemTemplateForm parentBean;
	
	public InvLineItemTemplateList(InvLineItemTemplateForm parentBean,
		Integer templateCode,
		String templateName,
		String description) {
	
		this.parentBean = parentBean;
		this.templateCode = templateCode;
		this.templateName = templateName;      
		this.description = description;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowLITSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowLITSelected(this, true);
	
	}   
	
	public Integer getTemplateCode() {
	
	    return templateCode;
	
	}
	
	public String getTemplateName() {
	   
	    return templateName;
	   
	}
	
	public String getDescription() {
	
	    return description;
	
	}
	
}