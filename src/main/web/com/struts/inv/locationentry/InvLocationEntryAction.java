package com.struts.inv.locationentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvLocFixedAssetException;
import com.ejb.txn.InvLocationEntryController;
import com.ejb.txn.InvLocationEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvLocationDetails;

public final class InvLocationEntryAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvLocationEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        InvLocationEntryForm actionForm = (InvLocationEntryForm)form;
      	       
        String frParam = Common.getUserPermission(user, Constants.INV_LOCATION_ENTRY_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("invLocationEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize InvLocationEntryController EJB
*******************************************************/

        InvLocationEntryControllerHome homeLE = null;
        InvLocationEntryController ejbLE = null;

        try {

            homeLE = (InvLocationEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvLocationEntryControllerEJB", InvLocationEntryControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in InvLocationEntryAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbLE = homeLE.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in InvLocationEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Inv LE Save Action --
*******************************************************/

        if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            InvLocationDetails details = new InvLocationDetails();
            details.setLocCode(actionForm.getLocationCode());
            details.setLocName(actionForm.getLocationName());
            details.setLocDescription(actionForm.getDescription());
            details.setLocLvType(actionForm.getType());
            details.setLocAddress(actionForm.getAddress());
            details.setLocContactPerson(actionForm.getContactPerson());
            details.setLocContactNumber(actionForm.getContactNumber());
            details.setLocEmailAddress(actionForm.getEmailAddress());
            details.setLocDepartment(actionForm.getDepartment());
            details.setLocBranch(actionForm.getBranch());
            details.setLocPosition(actionForm.getPosition());
            details.setLocDateHired(actionForm.getDateHired());
            details.setLocEmploymentStatus(actionForm.getEmploymentStatus());
            
            try {
            	
            	ejbLE.saveInvLocEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("locationEntry.error.recordAlreadyExist"));

            }catch (InvLocFixedAssetException ex) {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			new ActionMessage("locationEntry.error.fixedAssetInformationIncomplete")); 	        

	        }catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in InvLocationEntryAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }
            
/*******************************************************
-- Inv Loc Delete Action --
*******************************************************/
				
			} else if(request.getParameter("deleteButton") != null) {
				
				try {
					
					ejbLE.deleteInvLocEntry(actionForm.getLocationCode(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("locationEntry.error.locationAlreadyAssigned"));
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("locationEntry.error.locationAlreadyDeleted"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvLocationEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}

/*******************************************************
   -- Inv LE Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv LE Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invLocationEntry");

            }
            
			if (request.getParameter("forward") == null &&
					actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
				saveErrors(request, new ActionMessages(errors));
				
				return mapping.findForward("cmnMain");	
				
			}
			
			actionForm.reset(mapping, request);	            
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
            	actionForm.clearTypeList();           	
            	
            	list = ejbLE.getAdLvInvLocationTypeAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setTypeList((String)i.next());
            			
            		}

            	}

            	actionForm.clearDepartmentList();           	

            	list = ejbLE.getAdLvInvLocationDepartmentAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			actionForm.setDepartmentList((String)i.next());

            		}

            	}
            	
            	actionForm.clearBranchList();           	

            	list = ejbLE.getAdLvInvLocationBranchAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBranchList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			actionForm.setBranchList((String)i.next());

            		}

            	}
            	
            	if (request.getParameter("forward") != null) {
 	            		
            	    InvLocationDetails details = ejbLE.getInvLocByLocCode(
            		    new Integer(request.getParameter("locCode")), user.getCmpCode());
  
					actionForm.setLocationCode(details.getLocCode());
					actionForm.setLocationName(details.getLocName());
					actionForm.setDescription(details.getLocDescription());
					actionForm.setType(details.getLocLvType());
					actionForm.setAddress(details.getLocAddress());
					actionForm.setContactPerson(details.getLocContactPerson());
					actionForm.setContactNumber(details.getLocContactNumber());					
					actionForm.setEmailAddress(details.getLocEmailAddress());
					actionForm.setDepartment(details.getLocDepartment());
					actionForm.setBranch(details.getLocBranch());
					actionForm.setPosition(details.getLocPosition());
					actionForm.setDateHired(details.getLocDateHired());
					actionForm.setEmploymentStatus(details.getLocEmploymentStatus());
				    return(mapping.findForward("invLocationEntry"));  
				    
			    }          		                	            	
	              
	            actionForm.setLocationCode(null);  

	        } catch (GlobalNoRecordFoundException ex) {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			new ActionMessage("locationEntry.error.noRecordFound")); 	        

	        } catch (EJBException ex) {

	        	if (log.isInfoEnabled()) {

	        		log.info("EJBException caught in InvLocationEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if ((request.getParameter("saveButton") != null &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            return(mapping.findForward("invLocationEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in InvLocationEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}