package com.struts.inv.locationentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvLocationEntryForm extends ActionForm implements Serializable {
	
	private Integer locationCode = null;
	private String locationName = null;
	private String description = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String address = null;
	private String contactPerson = null;
	private String contactNumber = null;
	private String emailAddress = null;	
	private String userPermission = new String();
	private String txnStatus = new String();
		
	private String branch = null;
	private ArrayList branchList = new ArrayList();
	private String department = null;
	private ArrayList departmentList = new ArrayList();
	private String position = null;
	private String dateHired = null;
	private String employmentStatus = null;
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getLocationCode() {
		
		return locationCode;
		
	}
	
	public void setLocationCode(Integer locationCode) {
		
		this.locationCode = locationCode;
		
	}
	
	public String getLocationName() {
		
		return locationName;
		
	}
	
	public void setLocationName(String locationName) {
		
		this.locationName = locationName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public String getAddress() {
		
		return address;
		
	}
	public void setAddress(String address) {
		
		this.address = address;
		
	}
	
	public String getContactNumber() {
		
		return contactNumber;
		
	}
	
	public void setContactNumber(String contactNumber) {
		
		this.contactNumber = contactNumber;
		
	}
	
	public String getContactPerson() {
		
		return contactPerson;
		
	}
	
	public void setContactPerson(String contactPerson) {
		
		this.contactPerson = contactPerson;
		
	}
	
	public String getEmailAddress() {
		
		return emailAddress;
		
	}
	
	public void setEmailAddress(String emailAddress) {
		
		this.emailAddress = emailAddress;
		
	}
	
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
	}
	
	public void setTypeList(String type) {
		
		typeList.add(type);
		
	}
	
	public void clearTypeList() {
		
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getBranch() {
		
		return branch;
		
	}
	
	public void setBranch(String branch) {
		
		this.branch = branch;
		
	}
	
	public ArrayList getBranchList() {
		
		return branchList;
	}
	
	public void setBranchList(String branch) {
		
		branchList.add(branch);
		
	}
	
	public void clearBranchList() {
		
		branchList.clear();
		branchList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDepartment() {
		
		return department;
		
	}
	
	public void setDepartment(String department) {
		
		this.department = department;
		
	}
	
	public ArrayList getDepartmentList() {
		
		return departmentList;
	}
	
	public void setDepartmentList(String department) {
		
		departmentList.add(department);
		
	}
	
	public void clearDepartmentList() {
		
		departmentList.clear();
		departmentList.add(Constants.GLOBAL_BLANK);
		
	}

	public String getPosition() {

		return position;

	}

	public void setPosition(String position) {

		this.position = position;

	}
	
	public String getDateHired() {

		return dateHired;

	}

	public void setDateHired(String dateHired) {

		this.dateHired = dateHired;

	}
	
	public String getEmploymentStatus() {

		return employmentStatus;

	}

	public void setEmploymentStatus(String employmentStatus) {

		this.employmentStatus = employmentStatus;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		locationName = null;
		description = null;
		type = Constants.GLOBAL_BLANK;
		address = null;
		contactPerson = null;
		contactNumber = null;
		emailAddress = null;
		branch = Constants.GLOBAL_BLANK;
		department = Constants.GLOBAL_BLANK;
		position = Constants.GLOBAL_BLANK;
		dateHired = null;
		employmentStatus = Constants.GLOBAL_BLANK;
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if (Common.validateRequired(locationName)) {
				
				errors.add("locationName",
						new ActionMessage("locationEntry.error.locationNameRequired"));
				
			}
			
			if (Common.validateRequired(type)) {
				
				errors.add("type",
						new ActionMessage("locationEntry.error.typeRequired"));
				
			}

			if(!Common.validateDateFormat(dateHired)) {

				errors.add("dateAcquired", new ActionMessage("itemEntry.error.dateAcquiredInvalid"));
			}

		}
		
		return errors;
		
	}
	
}