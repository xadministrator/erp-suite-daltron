package com.struts.inv.overheadentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvOverheadEntryController;
import com.ejb.txn.InvOverheadEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModOverheadDetails;
import com.util.InvModOverheadLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvOverheadDetails;

public final class InvOverheadEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      InvOverheadEntryForm actionForm = (InvOverheadEntryForm)form;
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvOverheadEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         String frParam = Common.getUserPermission(user, Constants.INV_OVERHEAD_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invOverheadEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invOverheadEntryController EJB
*******************************************************/

         InvOverheadEntryControllerHome homeOH = null;
         InvOverheadEntryController ejbOH = null;

         try {
            
            homeOH = (InvOverheadEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvOverheadEntryControllerEJB", InvOverheadEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvOverheadEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbOH = homeOH.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvOverheadEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();   

/*******************************************************
   Call InvOverheadEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short quantityPrecisionUnit = 0;
         short overheadLineNumber = 0;
         boolean enableShift = false;
         
         try {
         	
            precisionUnit = ejbOH.getGlFcPrecisionUnit(user.getCmpCode());
            quantityPrecisionUnit = ejbOH.getInvGpQuantityPrecisionUnit(user.getCmpCode());
            overheadLineNumber = ejbOH.getInvGpInventoryLineNumber(user.getCmpCode()); 
            enableShift = Common.convertByteToBoolean(ejbOH.getAdPrfEnableInvShift(user.getCmpCode()));
       	 	actionForm.setShowShift(enableShift);      
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Inv OH Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvOverheadDetails details = new InvOverheadDetails();
           
           details.setOhCode(actionForm.getOverheadCode());
           details.setOhDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setOhDocumentNumber(actionForm.getDocumentNumber());
           details.setOhReferenceNumber(actionForm.getReferenceNumber());
           details.setOhDescription(actionForm.getDescription());
           details.setOhLvShift(actionForm.getShift());
           details.setOhCreatedBy(actionForm.getCreatedBy());
           details.setOhDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
           details.setOhLastModifiedBy(actionForm.getLastModifiedBy());
           details.setOhDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                      
           if (actionForm.getOverheadCode() == null) {
           	
           		details.setOhCreatedBy(user.getUserName());
           		details.setOhDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
                      
           details.setOhLastModifiedBy(user.getUserName());
           details.setOhDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
           
           ArrayList ohlList = new ArrayList(); 
          
           for (int i = 0; i<actionForm.getInvOHListSize(); i++) {
           	
           	   InvOverheadEntryList invOHList = actionForm.getInvOHByIndex(i);           	   
           	              	             	   
	           if (Common.validateRequired(invOHList.getOverheadMemoLineName()) &&
	         	 	     Common.validateRequired(invOHList.getQuantity()) &&
	         	 	     Common.validateRequired(invOHList.getUnit()) &&
	         	 	     Common.validateRequired(invOHList.getUnitCost())) continue;
           	   
           	   InvModOverheadLineDetails mdetails = new InvModOverheadLineDetails();
           	              	  
           	   mdetails.setOhlUnitCost(Common.convertStringMoneyToDouble(invOHList.getUnitCost(), precisionUnit));
           	   mdetails.setOhlQuantity(Common.convertStringMoneyToDouble(invOHList.getQuantity(), quantityPrecisionUnit));
           	   mdetails.setOhlUomName(invOHList.getUnit());
           	   mdetails.setOhlOmlName(invOHList.getOverheadMemoLineName());
           	   mdetails.setOhlLineNumber(Common.convertStringToShort(invOHList.getLineNumber()));
           	   
           	   ohlList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    ejbOH.saveInvOhEntry(details, ohlList, true,
           	    	 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	    
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invOverheadEntry.error.documentNumberNotUnique"));  	    
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.recordAlreadyDeleted"));
           	           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.transactionAlreadyPosted"));
                               	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.journalNotBalance"));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invOverheadEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Inv OH Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	InvOverheadDetails details = new InvOverheadDetails();
         	
         	details.setOhCode(actionForm.getOverheadCode());
         	details.setOhDocumentNumber(actionForm.getDocumentNumber());
         	details.setOhReferenceNumber(actionForm.getReferenceNumber());
         	details.setOhDescription(actionForm.getDescription());
         	details.setOhDate(Common.convertStringToSQLDate(actionForm.getDate()));
         	details.setOhLvShift(actionForm.getShift());
         	details.setOhCreatedBy(actionForm.getCreatedBy());
         	details.setOhDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
         	details.setOhLastModifiedBy(actionForm.getLastModifiedBy());
         	details.setOhDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
         	
         	if (actionForm.getOverheadCode() == null) {
         		
         		details.setOhCreatedBy(user.getUserName());
         		details.setOhDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         		
         	}
         	
         	details.setOhLastModifiedBy(user.getUserName());
         	details.setOhDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         	
         	ArrayList ohlList = new ArrayList();
         	
         	for (int i = 0; i<actionForm.getInvOHListSize(); i++) {
         		
         		InvOverheadEntryList invOHList = actionForm.getInvOHByIndex(i);
         		
         		if (Common.validateRequired(invOHList.getOverheadMemoLineName()) &&
         				Common.validateRequired(invOHList.getQuantity()) &&
						Common.validateRequired(invOHList.getUnit()) &&
						Common.validateRequired(invOHList.getUnitCost())) continue;
         		
         		InvModOverheadLineDetails mdetails = new InvModOverheadLineDetails();
         		
         		mdetails.setOhlUnitCost(Common.convertStringMoneyToDouble(invOHList.getUnitCost(), precisionUnit));
         		mdetails.setOhlQuantity(Common.convertStringMoneyToDouble(invOHList.getQuantity(), quantityPrecisionUnit));
         		mdetails.setOhlUomName(invOHList.getUnit());
         		mdetails.setOhlOmlName(invOHList.getOverheadMemoLineName());           	             	 
         		mdetails.setOhlLineNumber(Common.convertStringToShort(invOHList.getLineNumber()));
         		
         		ohlList.add(mdetails);
         		
         	}                     	
         	
         	try {
         		
         		Integer overheadCode = ejbOH.saveInvOhEntry(details, ohlList, false,
         				 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         		actionForm.setOverheadCode(overheadCode);
         		
         	} catch (GlobalDocumentNumberNotUniqueException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invOverheadEntry.error.documentNumberNotUnique"));  	    
         		
         	} catch (GlobalRecordAlreadyDeletedException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invOverheadEntry.error.recordAlreadyDeleted"));
         		
         	} catch (GlJREffectiveDateNoPeriodExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invOverheadEntry.error.effectiveDateNoPeriodExist"));
         		
         	} catch (GlJREffectiveDatePeriodClosedException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invOverheadEntry.error.effectiveDatePeriodClosed"));
         		
         	} catch (GlobalJournalNotBalanceException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invOverheadEntry.error.journalNotBalance"));

         	} catch(GlobalBranchAccountNumberInvalidException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invOverheadEntry.error.branchAccountNumberInvalid", ex.getMessage()));

         	} catch (EJBException ex) {
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         	}
         	
/*******************************************************
  -- Inv OH Journal Action --
*******************************************************/
         	
         } else if (request.getParameter("journalButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	if(!Common.validateRequired(actionForm.getPosted()) && actionForm.getPosted().equals("NO")) {
         		
         		InvOverheadDetails details = new InvOverheadDetails();
         		
         		details.setOhCode(actionForm.getOverheadCode());
         		details.setOhDocumentNumber(actionForm.getDocumentNumber());
         		details.setOhReferenceNumber(actionForm.getReferenceNumber());
         		details.setOhDescription(actionForm.getDescription());
         		details.setOhDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setOhLvShift(actionForm.getShift());
         		details.setOhCreatedBy(actionForm.getCreatedBy());
         		details.setOhDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
         		details.setOhLastModifiedBy(actionForm.getLastModifiedBy());
         		details.setOhDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
         		
         		if (actionForm.getOverheadCode() == null) {
         			
         			details.setOhCreatedBy(user.getUserName());
         			details.setOhDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         			
         		}
         		
         		details.setOhLastModifiedBy(user.getUserName());
         		details.setOhDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         		
         		ArrayList ohlList = new ArrayList();
         		
         		for (int i = 0; i<actionForm.getInvOHListSize(); i++) {
         			
         			InvOverheadEntryList invOHList = actionForm.getInvOHByIndex(i);
         			
         			if (Common.validateRequired(invOHList.getOverheadMemoLineName()) &&
         					Common.validateRequired(invOHList.getQuantity()) &&
							Common.validateRequired(invOHList.getUnit()) &&
							Common.validateRequired(invOHList.getUnitCost())) continue;
         			
         			InvModOverheadLineDetails mdetails = new InvModOverheadLineDetails();
         			
         			mdetails.setOhlUnitCost(Common.convertStringMoneyToDouble(invOHList.getUnitCost(), precisionUnit));
         			mdetails.setOhlQuantity(Common.convertStringMoneyToDouble(invOHList.getQuantity(), quantityPrecisionUnit));
         			mdetails.setOhlUomName(invOHList.getUnit());
         			mdetails.setOhlOmlName(invOHList.getOverheadMemoLineName());           	             	 
         			mdetails.setOhlLineNumber(Common.convertStringToShort(invOHList.getLineNumber()));
         			
         			ohlList.add(mdetails);
         			
         		}                     	
         		
         		try {
         			
         			Integer overheadCode = ejbOH.saveInvOhEntry(details, ohlList, true,
         					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         			actionForm.setOverheadCode(overheadCode);
         			
         		} catch (GlobalDocumentNumberNotUniqueException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.documentNumberNotUnique"));  	    
         			
         		} catch (GlobalRecordAlreadyDeletedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.recordAlreadyDeleted"));
         			
         		} catch (GlobalTransactionAlreadyPostedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.transactionAlreadyPosted"));
         			
         		} catch (GlJREffectiveDateNoPeriodExistException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.effectiveDateNoPeriodExist"));
         			
         		} catch (GlJREffectiveDatePeriodClosedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.effectiveDatePeriodClosed"));
         			
         		} catch (GlobalJournalNotBalanceException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.journalNotBalance"));
         			
         		} catch(GlobalBranchAccountNumberInvalidException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invOverheadEntry.error.branchAccountNumberInvalid", ex.getMessage()));
         			
         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {
         				
         				log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}
         			
         			return(mapping.findForward("cmnErrorPage"));
         		}
         		
         		if (!errors.isEmpty()) {
         			
         			saveErrors(request, new ActionMessages(errors));
         			return(mapping.findForward("invOverheadEntry"));
         			
         		}	         	
         		
         	}
         	
         	String path = "/invJournal.do?forward=1" + 
			"&transactionCode=" + actionForm.getOverheadCode() +
			"&transaction=OVERHEAD" + 
			"&transactionNumber=" + actionForm.getReferenceNumber() + 
			"&transactionDate=" + actionForm.getDate() +
			"&transactionEnableFields=" + actionForm.getEnableFields();
         	
         	return(new ActionForward(path));
         	
/*******************************************************
  -- Inv OH Delete Action --
*******************************************************/
         	
         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbOH.deleteInvOhEntry(actionForm.getOverheadCode(), user.getUserName(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
        
/*******************************************************
   -- Inv OH Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Inv OH Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	         	         	
         	int listSize = actionForm.getInvOHListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + overheadLineNumber; x++) {
	        	
            	ArrayList comboItem = new ArrayList();
            	
	        	InvOverheadEntryList invOHList = new InvOverheadEntryList(actionForm, 
	        	    null, new Integer(++lineNumber).toString(), null, null, null, null, null);	
	        		        	    
	        	invOHList.setOverheadMemoLineNameList(actionForm.getOverheadMemoLineNameList());
	        	
	        	invOHList.setUnitList(Constants.GLOBAL_BLANK);
	        	invOHList.setUnitList("Select Overhead First");
	        	
	        	actionForm.saveInvOHList(invOHList);
	        	
	        }		                    
            
	        for (int i = 0; i<actionForm.getInvOHListSize(); i++ ) {
           	
           	   InvOverheadEntryList invOHList = actionForm.getInvOHByIndex(i);
           	   
           	   invOHList.setLineNumber(new Integer(i+1).toString());
           	   
            }        
	        
	        return(mapping.findForward("invOverheadEntry"));
	        
/*******************************************************
   -- Inv OH Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getInvOHListSize(); i++) {
           	
           	   InvOverheadEntryList invOHList = actionForm.getInvOHByIndex(i);
           	   
           	   if (invOHList.isDeleteCheckbox()) {
           	   	
           	   		actionForm.deleteInvOHList(i);
           	   		i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("invOverheadEntry"));        

/*******************************************************
    -- Inv OH Overhead Memo Line Name Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invOHList[" + 
        actionForm.getRowSelected() + "].isOverheadMemoLineNameEntered"))) {
        
      	InvOverheadEntryList invOHList = 
      		actionForm.getInvOHByIndex(actionForm.getRowSelected());
      	      		                      
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbOH.getInvUomByOmlName(invOHList.getOverheadMemoLineName(), user.getCmpCode());
      		
      		invOHList.clearUnitList();
      		invOHList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			invOHList.setUnitList(mUomDetails.getUomName());
      			      			      			
      			if (mUomDetails.isDefault()) {
      				
      				invOHList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
      		// populate unit cost and amount field
      		
      		if (!Common.validateRequired(invOHList.getOverheadMemoLineName()) && !Common.validateRequired(invOHList.getUnit())) {
      		
	      		double unitCost = ejbOH.getInvOmlUnitCostByOmlNameAndUomName(invOHList.getOverheadMemoLineName(), invOHList.getUnit(), user.getCmpCode());
	      		invOHList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));	      			      	
				invOHList.setAmount(Common.convertDoubleToStringMoney(Common.convertStringMoneyToDouble(invOHList.getQuantity(), precisionUnit) * unitCost, precisionUnit));
				
      		}
      		      		      		      		        	                 	
      	} catch (GlobalNoRecordFoundException ex) {
      		
      	} catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("invOverheadEntry"));
         
/*******************************************************
    -- Inv OH Quantity Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invOHList[" + 
        actionForm.getRowSelected() + "].isQuantityEntered"))) {
        
      	InvOverheadEntryList invOHList = 
      		actionForm.getInvOHByIndex(actionForm.getRowSelected());
                      
      	try {
          	      		
      		
      		// refresh quantity and calculate quantity
      		
      		if (!Common.validateRequired(invOHList.getOverheadMemoLineName()) &&
      		    !Common.validateRequired(invOHList.getUnit())) {
      			      	
	      		// populate unit cost and amount field
	      		
	      		double unitCost = ejbOH.getInvOmlUnitCostByOmlNameAndUomName(invOHList.getOverheadMemoLineName(), invOHList.getUnit(), user.getCmpCode());
	      		invOHList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
	      		invOHList.setAmount(Common.convertDoubleToStringMoney(Common.convertStringMoneyToDouble(invOHList.getQuantity(), precisionUnit) * unitCost, precisionUnit));
	      		
	        }
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("invOverheadEntry"));
          

/*******************************************************
   -- Inv OH Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
         	// Errors on saving
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("invOverheadEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	// overhead memo line combo box
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearShiftList();           	
            	
            	list = ejbOH.getAdLvInvShiftAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setShiftList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearOverheadMemoLineNameList();       	
            	
            	list = ejbOH.getInvOmlAll(new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setOverheadMemoLineNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setOverheadMemoLineNameList((String)i.next());
            			
            		}
            		            		
            	}            	            
            	
            	actionForm.clearInvOHList();               	  	
            	
            	if (request.getParameter("forward") != null) {

            		actionForm.setOverheadCode(new Integer(request.getParameter("overheadCode")));
            		
            		InvModOverheadDetails mdetails = ejbOH.getInvOhByOhCode(actionForm.getOverheadCode(), user.getCmpCode());            	
            		
            		actionForm.setDocumentNumber(mdetails.getOhDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getOhReferenceNumber());
            		actionForm.setDescription(mdetails.getOhDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getOhDate()));
            		actionForm.setShift(mdetails.getOhLvShift());
            		actionForm.setPosted(mdetails.getOhPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getOhCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getOhDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getOhLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getOhDateLastModified()));
            		actionForm.setPostedBy(mdetails.getOhPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getOhDatePosted()));
            		
            		list = mdetails.getOhOhlList();      		
            		         		            		            		
		            i = list.iterator();
		            
		            int lineNumber = 0;
		            
		            while (i.hasNext()) {
		            	
		            	InvModOverheadLineDetails mOhlDetails = (InvModOverheadLineDetails)i.next();		            			            			            	
		            	
		            	InvOverheadEntryList ohOhlList = new InvOverheadEntryList(actionForm,
		            		mOhlDetails.getOhlCode(), new Integer(++lineNumber).toString(), 
							mOhlDetails.getOhlOmlName(),
							Common.convertDoubleToStringMoney(mOhlDetails.getOhlQuantity(), quantityPrecisionUnit),
							mOhlDetails.getOhlUomName(), 
							Common.convertDoubleToStringMoney(mOhlDetails.getOhlUnitCost(), precisionUnit),
							Common.convertDoubleToStringMoney(mOhlDetails.getOhlAmount(), precisionUnit));		            	
						
		            	ohOhlList.setOverheadMemoLineNameList(actionForm.getOverheadMemoLineNameList());            	
		            	            	
		            	ohOhlList.clearUnitList();
		            	ArrayList unitList = ejbOH.getInvUomByOmlName(mOhlDetails.getOhlOmlName(), user.getCmpCode());
		            	Iterator unitListIter = unitList.iterator();
		            	while (unitListIter.hasNext()) {
		            		
		            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
		            		ohOhlList.setUnitList(mUomDetails.getUomName()); 
		            		
		            	}		            			            			            	
		            	
		            	actionForm.saveInvOHList(ohOhlList);				         
				      
			        }	
			        			        			        
			        int remainingList = overheadLineNumber - (list.size() % overheadLineNumber) + list.size();
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	
			        	ArrayList comboItem = new ArrayList();
			        	
			        	InvOverheadEntryList invOHList = new InvOverheadEntryList(actionForm, 
				        	    null, new Integer(x).toString(), null, null, null, null, null);	

			        	invOHList.setOverheadMemoLineNameList(actionForm.getOverheadMemoLineNameList());
			        			        	
			        	invOHList.setUnitList(Constants.GLOBAL_BLANK);
			        	invOHList.setUnitList("Select Overhead First");
			        	
			        	actionForm.saveInvOHList(invOHList);
			        	
			         }	
			         
			         this.setFormProperties(actionForm);
			         
			         if (request.getParameter("child") == null) {
			         
			         	return (mapping.findForward("invOverheadEntry"));         
			         	
			         } else {
			         	
			         	return (mapping.findForward("invOverheadEntryChild"));         
			         	
			         }
            		
            	}     
            	
            	// Populate line when not forwarding
            	
            	for (int x = 1; x <= overheadLineNumber; x++) {
			            		
            		InvOverheadEntryList invOHList = new InvOverheadEntryList(actionForm, 
			        	    null, new Integer(x).toString(), null, null, null, null, null);	

            		invOHList.setOverheadMemoLineNameList(actionForm.getOverheadMemoLineNameList());
		        	
		        	invOHList.setUnitList(Constants.GLOBAL_BLANK);
		        	invOHList.setUnitList("Select Overhead First");
		        	
		        	actionForm.saveInvOHList(invOHList);
			    	
	  	        }       	  	        
	  	                            	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invOverheadEntry.error.recordAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvOverheadEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 
            
            // Errors on loading
         
            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null &&
               		
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
               	
               	  messages.add(ActionMessages.GLOBAL_MESSAGE,	
         	   	       new ActionMessage("messages.documentPosted"));
               	  
               	  saveMessages(request, messages);
            	  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               	
               } else if (request.getParameter("saveAsDraftButton") != null &&
               		
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }

               
            }
            
            actionForm.reset(mapping, request);              

            actionForm.setOverheadCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));                        
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("invOverheadEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in InvOverheadEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(InvOverheadEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getOverheadCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					
				} else if (actionForm.getOverheadCode() != null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
				    
				    	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					
				}
				
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				
			}
			
			
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			
		}
		    
	}
		
}