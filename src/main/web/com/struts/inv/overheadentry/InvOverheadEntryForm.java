package com.struts.inv.overheadentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvOverheadEntryForm extends ActionForm implements Serializable {

	private Integer overheadCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
    private String shift = null;
    private ArrayList shiftList = new ArrayList();
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String postedBy = null;
	private String datePosted = null;
	private String overheadMemoLineName = null;
	private ArrayList overheadMemoLineNameList = new ArrayList();
   
	private ArrayList invOHList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	private boolean showShift = false;
         
	public int getRowSelected(){
		return rowSelected;
	}

	public InvOverheadEntryList getInvOHByIndex(int index){
		return((InvOverheadEntryList)invOHList.get(index));
	}

	public Object[] getInvOHList(){
		return(invOHList.toArray());
	}

	public int getInvOHListSize(){
		return(invOHList.size());
	}

	public void saveInvOHList(Object newInvOHList){
		invOHList.add(newInvOHList);
	}

	public void clearInvOHList(){
		invOHList.clear();
	}

	public void setRowSelected(Object selectedInvOHList, boolean isEdit){
		this.rowSelected = invOHList.indexOf(selectedInvOHList);
	}

	public void updateInvOHRow(int rowSelected, Object newInvOHList){
		invOHList.set(rowSelected, newInvOHList);
	}

	public void deleteInvOHList(int rowSelected){
		invOHList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
     
	public Integer getOverheadCode() {
		return overheadCode;
	}
	
	public void setOverheadCode(Integer overheadCode) {
		this.overheadCode = overheadCode;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public boolean getEnableFields() {
		return enableFields;
	}
	
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getOverheadMemoLineName() {
		return overheadMemoLineName;
	}
	
	public void setOverheadMemoLineName(String overheadMemoLineName) {
		this.overheadMemoLineName = overheadMemoLineName;
	}	
	
	public ArrayList getOverheadMemoLineNameList() {
		return overheadMemoLineNameList;
	}
	
	public void setOverheadMemoLineNameList(String overheadMemoLineNameList) {
		this.overheadMemoLineNameList.add(overheadMemoLineNameList);
	}
	
	public void clearOverheadMemoLineNameList() {
	   	
   	  overheadMemoLineNameList.clear();
   	  overheadMemoLineNameList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getPosted() {
		return posted;
	}
	
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getShift() {
		
		return shift;
		
	}
	
	public void setShift(String shift) {
		
		this.shift = shift;
		
	}
	
	public ArrayList getShiftList() {
		
		return shiftList;
		
	}
	
	public void setShiftList(String shift) {
		
		shiftList.add(shift);
		
	}
	
	public void clearShiftList() {
		
		shiftList.clear();
		shiftList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowSaveButton() {
		return showSaveButton;
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public boolean getShowShift() {
		
		return showShift;
		
	}
	
	public void setShowShift(boolean showShift) {
		
		this.showShift = showShift;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       overheadMemoLineName = Constants.GLOBAL_BLANK;      
	   date = null;
	   documentNumber = null;
	   referenceNumber = null;
	   description = null;
	   shift = Constants.GLOBAL_BLANK;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   postedBy = null;
	   datePosted = null;	

	   
	   for (int i=0; i<invOHList.size(); i++) {
	  	
	  	  InvOverheadEntryList actionList = (InvOverheadEntryList)invOHList.get(i);
	  	  	  	 
	  	  actionList.setIsOverheadMemoLineNameEntered(null);
	  	  actionList.setIsQuantityEntered(null);
	  	
	  } 


   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
		 request.getParameter("journalButton") != null) { 
      	
      	 if(Common.validateRequired(date)){
      	 
            errors.add("date",
               new ActionMessage("invOverheadEntry.error.dateRequired"));
               
         }
         
		 if(!Common.validateDateFormat(date)){
		 
	        errors.add("date", 
		       new ActionMessage("invOverheadEntry.error.dateInvalid"));
		       
		 }
		 
		 if (showShift) {
		 
			 if(Common.validateRequired(shift)){
	      	 
	            errors.add("shift",
	               new ActionMessage("invOverheadEntry.error.shiftRequired"));
	               
	         }	 
		 
		 }
		 
		 if((!Common.validateDateGreaterThanCurrent(date) || !Common.validateDateLessThanCurrent(date)) && (Common.validateRequired(posted) || posted.equals(Constants.GLOBAL_NO))) {
            errors.add("date", 
                new ActionMessage("invOverheadEntry.error.dateMustNotBeGreaterThanOrLessThanCurrentDate"));
        
		 }
		 
         int numberOfLines = 0;
      	 
      	 Iterator i = invOHList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvOverheadEntryList ohlList = (InvOverheadEntryList)i.next();      	 	 
			 
      	 	 if (Common.validateRequired(ohlList.getOverheadMemoLineName()) &&
      	 	     Common.validateRequired(ohlList.getQuantity()) &&
      	 	     Common.validateRequired(ohlList.getUnit()) &&
      	 	     Common.validateRequired(ohlList.getUnitCost())) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(ohlList.getOverheadMemoLineName()) || ohlList.getOverheadMemoLineName().equals(Constants.GLOBAL_BLANK)
	         		|| ohlList.getOverheadMemoLineName().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	            errors.add("overheadMemoLineName",
	            	new ActionMessage("invOverheadEntry.error.overheadMemoLineNameRequired", ohlList.getLineNumber()));
	         }

	         if(Common.validateRequired(ohlList.getQuantity())) {
	         	errors.add("quantity",
	         		new ActionMessage("invOverheadEntry.error.quantityRequired", ohlList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(ohlList.getQuantity())){
	            errors.add("quantity",
	               new ActionMessage("invOverheadEntry.error.quantityInvalid", ohlList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(ohlList.getQuantity()) && Common.convertStringMoneyToDouble(ohlList.getQuantity(), (short)3) == 0) {
		         	errors.add("quantity",
		         		new ActionMessage("invOverheadEntry.error.zeroQuantityNotAllowed", ohlList.getLineNumber()));
		         }
		 	 if(Common.validateRequired(ohlList.getUnit()) || ohlList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("invOverheadEntry.error.unitRequired", ohlList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(ohlList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invOverheadEntry.error.unitCostRequired", ohlList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(ohlList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invOverheadEntry.error.unitCostInvalid", ohlList.getLineNumber()));
	         }	        
       
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("overhead",
               new ActionMessage("invOverheadEntry.error.overheadMustHaveLine"));
         	
        }	  	    
	    
	    	    	 
      } 
      
      return(errors);	
   }
}
