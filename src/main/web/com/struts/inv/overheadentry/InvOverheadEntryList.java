package com.struts.inv.overheadentry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvOverheadEntryList implements Serializable {

	private Integer overheadLineCode = null;
	private String lineNumber = null;
	private String overheadMemoLineName = null;
	private ArrayList overheadMemoLineNameList = new ArrayList();
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private boolean deleteCheckbox = false;
   
	private String isOverheadMemoLineNameEntered = null;
	private String isQuantityEntered = null;
    
	private InvOverheadEntryForm parentBean;   

	public InvOverheadEntryList(InvOverheadEntryForm parentBean,
		Integer overheadLineCode, String lineNumber, String overheadMemoLineName,
        String quantity, String unit, String unitCost, String amount) {
	
		this.parentBean = parentBean;
		this.overheadLineCode = overheadLineCode;
		this.lineNumber = lineNumber;
		this.overheadMemoLineName = overheadMemoLineName;
		this.quantity = quantity;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;

	}
	
	public InvOverheadEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvOverheadEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public Integer getOverheadLineCode() {
		
		return overheadLineCode;
		
	}
	
	public boolean isDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsOverheadMemoLineNameEntered() {
		
		return isOverheadMemoLineNameEntered;
		
	}
	
	public void setIsOverheadMemoLineNameEntered(String isOverheadMemoLineNameEntered) {
		
		if (isOverheadMemoLineNameEntered != null && isOverheadMemoLineNameEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isOverheadMemoLineNameEntered = null;
		
	}
	
	public String getIsQuantityEntered() {
		
		return isQuantityEntered;
		
	}
	
	public void setIsQuantityEntered(String isQuantityEntered) {	
		
		if (isQuantityEntered != null && isQuantityEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}				
		
		isQuantityEntered = null;
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getOverheadMemoLineName() {
		
		return overheadMemoLineName;
		
	}
	
	public void setOverheadMemoLineName(String overheadMemoLineName) {
		
		this.overheadMemoLineName = overheadMemoLineName;
		
	}
	
	public ArrayList getOverheadMemoLineNameList() {
		
		return overheadMemoLineNameList;
		
	}
	
	public void setOverheadMemoLineNameList(ArrayList overheadMemoLineNameList) {
		
		this.overheadMemoLineNameList = overheadMemoLineNameList;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
	   	
		unitList.clear();
	   	
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public String getUnitCost() {
		
		return unitCost;		
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
}
