package com.struts.inv.overheadmemolines;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/27/2005 9:47 AM
 * 
 */
public class InvBranchOverheadMemoLineList implements Serializable {
	
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	private String branchOverheadAccount = null;
	private String branchOverheadDescription = null;
	private String branchLiabilityAccount = null;
	private String branchLiabilityDescription = null;
	
	private InvOverheadMemoLineForm parentBean;
	
	public InvBranchOverheadMemoLineList(InvOverheadMemoLineForm parentBean, 
			String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getBranchOverheadAccount() {
		return this.branchOverheadAccount;
	}
	
	public void setBranchOverheadAccount(String branchOverheadAccount) {
		this.branchOverheadAccount = branchOverheadAccount;
	}
	
	public String getBranchOverheadDescription() {
		return this.branchOverheadDescription;
	}
	
	public void setBranchOverheadDescription(String branchOverheadDescription) {
		this.branchOverheadDescription = branchOverheadDescription;
	}
	
	public String getBranchLiabilityAccount() {
		return this.branchLiabilityAccount;
	}
	
	public void setBranchLiabilityAccount(String branchLiabilityAccount) {
		this.branchLiabilityAccount = branchLiabilityAccount;
	}
	
	public String getBranchLiabilityDescription() {
		return this.branchLiabilityDescription;
	}
	
	public void setBranchLiabilityDescription(String branchLiabilityDescription) {
		this.branchLiabilityDescription = branchLiabilityDescription;
	}
}

