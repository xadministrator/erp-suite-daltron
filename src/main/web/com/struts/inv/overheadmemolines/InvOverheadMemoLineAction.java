package com.struts.inv.overheadmemolines;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvOMLCoaGlLiabilityAccountNotFoundException;
import com.ejb.exception.InvOMLCoaGlOverheadAccountNotFoundException;
import com.ejb.txn.InvOverheadMemoLineController;
import com.ejb.txn.InvOverheadMemoLineControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchOverheadMemoLineDetails;
import com.util.AdResponsibilityDetails;
import com.util.InvModOverheadMemoLineDetails;

public final class InvOverheadMemoLineAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvOverheadMemoLineAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvOverheadMemoLineForm actionForm = (InvOverheadMemoLineForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_OVERHEAD_MEMO_LINE_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invOverheadMemoLines");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
Initialize InvOverheadMemoLineController EJB
*******************************************************/
			
			InvOverheadMemoLineControllerHome homeOML = null;
			InvOverheadMemoLineController ejbOML = null;
			
			try {
				
				homeOML = (InvOverheadMemoLineControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvOverheadMemoLineControllerEJB", InvOverheadMemoLineControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvOverheadMemoLineAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbOML = homeOML.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvOverheadMemoLineAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
Call InvOverheadMemoLineController EJB
getAdPrfInvQuantityPrecisionUnit
getGlFcPrecisionUnit
*******************************************************/			
			
			short precisionUnit = 0;
			
			try {
				
				precisionUnit = ejbOML.getGlFcPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}			        
			
/*******************************************************
-- Inv OML Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invOverheadMemoLines"));
				
/*******************************************************
-- Inv OML Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invOverheadMemoLines"));                  
				
/*******************************************************
-- Inv OML Save Action --
*******************************************************/
				
			} else if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvModOverheadMemoLineDetails mdetails = new InvModOverheadMemoLineDetails();
				
				mdetails.setOmlCode(actionForm.getOverheadMemoLineCode());
				mdetails.setOmlName(actionForm.getOverheadMemoLineName());
				mdetails.setOmlDescription(actionForm.getDescription());
				mdetails.setOmlUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit));
				mdetails.setOmlCoaOverheadAccount(actionForm.getOverheadAccount());
				mdetails.setOmlCoaLiabilityAccount(actionForm.getLiabilityAccount());

				// For Branch Setting
	            ArrayList branchList = new ArrayList();
	            
	            for(int i=0; i<actionForm.getInvBOMLListSize(); i++) {
		        	InvBranchOverheadMemoLineList bomlList = actionForm.getInvBOMLByIndex(i);
		        	
		        	if(bomlList.getBranchCheckbox() == true) {
		        		
		        		AdModBranchOverheadMemoLineDetails adBrOmlDetails = new AdModBranchOverheadMemoLineDetails();
		        		adBrOmlDetails.setBOMLBranchCode(bomlList.getBrCode());
			        	if (bomlList.getBranchOverheadAccount() == null || bomlList.getBranchOverheadAccount().length() < 1)
			        		bomlList.setBranchOverheadAccount(actionForm.getOverheadAccount());

		        		adBrOmlDetails.setBOMLOverheadAccountNumber(bomlList.getBranchOverheadAccount());

			        	if (bomlList.getBranchLiabilityAccount() == null || bomlList.getBranchLiabilityAccount().length() < 1)
			        		bomlList.setBranchLiabilityAccount(actionForm.getLiabilityAccount());

		        		adBrOmlDetails.setBOMLLiabilityAccountNumber(bomlList.getBranchLiabilityAccount());
		        		
			        	branchList.add(adBrOmlDetails);
		        	}
	            }
				
				try {
					
					ejbOML.addInvOmlEntry(mdetails, actionForm.getUnitMeasure(), branchList, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.recordAlreadyExist"));
					
				} catch (InvOMLCoaGlOverheadAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.overheadAccountNotFound"));
					
				} catch (InvOMLCoaGlLiabilityAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.liabilityAccountNotFound"));               	
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvOverheadMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
-- Inv OML Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
-- Inv OML Update Action --
*******************************************************/
				
			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvOverheadMemoLineList invOMLList =
					actionForm.getInvOMLByIndex(actionForm.getRowSelected());
				
				InvModOverheadMemoLineDetails mdetails = new InvModOverheadMemoLineDetails();
				
				mdetails.setOmlCode(actionForm.getOverheadMemoLineCode());
				mdetails.setOmlName(actionForm.getOverheadMemoLineName());
				mdetails.setOmlDescription(actionForm.getDescription());
				mdetails.setOmlUnitCost(Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit));
				mdetails.setOmlCoaOverheadAccount(actionForm.getOverheadAccount());
				mdetails.setOmlCoaLiabilityAccount(actionForm.getLiabilityAccount());

				// For Branch Setting
	            ArrayList branchList = new ArrayList();
	            
	            for(int i=0; i<actionForm.getInvBOMLListSize(); i++) {
		        	InvBranchOverheadMemoLineList bomlList = actionForm.getInvBOMLByIndex(i);
		        	
		        	if(bomlList.getBranchCheckbox() == true) {
		        		
		        		AdModBranchOverheadMemoLineDetails adBrOmlDetails = new AdModBranchOverheadMemoLineDetails();
		        		adBrOmlDetails.setBOMLBranchCode(bomlList.getBrCode());
			        	if (bomlList.getBranchOverheadAccount() == null || bomlList.getBranchOverheadAccount().length() < 1)
			        		bomlList.setBranchOverheadAccount(actionForm.getOverheadAccount());

		        		adBrOmlDetails.setBOMLOverheadAccountNumber(bomlList.getBranchOverheadAccount());

			        	if (bomlList.getBranchLiabilityAccount() == null || bomlList.getBranchLiabilityAccount().length() < 1)
			        		bomlList.setBranchLiabilityAccount(actionForm.getLiabilityAccount());

		        		adBrOmlDetails.setBOMLLiabilityAccountNumber(bomlList.getBranchLiabilityAccount());
		        		
			        	branchList.add(adBrOmlDetails);
		        	}
	            }

	            // get the selected responsibility name
	            AdResponsibilityDetails details = ejbOML.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	            
				try {
					
					ejbOML.updateInvOmlEntry(mdetails, actionForm.getUnitMeasure(), branchList, details.getRsName(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.recordAlreadyExist"));
					
				} catch (InvOMLCoaGlOverheadAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.overheadAccountNotFound"));
					
				} catch (InvOMLCoaGlLiabilityAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.liabilityAccountNotFound"));               	               	
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvOverheadMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
-- Inv OML Cancel Action --
*******************************************************/
				
			} else if (request.getParameter("cancelButton") != null) {
				
/*******************************************************
-- Inv OML Edit Action --
*******************************************************/
				
			} else if (request.getParameter("invOMLList[" + 
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				actionForm.reset(mapping, request);
				Object[] invBOMLList = actionForm.getInvOMLList();
				
				// get list of branches
	        	ArrayList branchList = new ArrayList();
	        	
	        	AdResponsibilityDetails details = ejbOML.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	        	
	         	try {
	         		branchList = ejbOML.getAdBrOMLAll(((InvOverheadMemoLineList)invBOMLList[actionForm.getRowSelected()]).getOverheadMemoLineCode(), details.getRsName(), user.getCmpCode());
	         	} catch(GlobalNoRecordFoundException ex) {
	         			
	         	}				
				
				actionForm.showInvOMLRow(branchList, actionForm.getRowSelected());
				
				return mapping.findForward("invOverheadMemoLines");
				
/*******************************************************
-- Inv OML Delete Action --
*******************************************************/
				
			} else if (request.getParameter("invOMLList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvOverheadMemoLineList invOMLList =
					actionForm.getInvOMLByIndex(actionForm.getRowSelected());
				
				try {
					
					ejbOML.deleteInvOmlEntry(invOMLList.getOverheadMemoLineCode(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.deleteOverheadMemoLineAlreadyAssigned"));
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("overheadMemoLine.error.overheadMemoLineAlreadyDeleted"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvOverheadMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}   
				
/*******************************************************
-- Inv OML Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				actionForm.reset(mapping, request);
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invOverheadMemoLines");
					
				}
				
				ArrayList list = null;
				Iterator i = null;

				actionForm.clearInvBOMLList();
				
				for (int j = 0; j < user.getBrnchCount(); j++) {

					 Branch branch = user.getBranch(j);
					 AdBranchDetails details = new AdBranchDetails();
					 details.setBrCode(new Integer(branch.getBrCode()));
					 details.setBrName(branch.getBrName());
					 InvBranchOverheadMemoLineList invBOMLList = new InvBranchOverheadMemoLineList(actionForm,
							details.getBrName(), details.getBrCode());
					 
					 if ( request.getParameter("forward") == null )
					 	invBOMLList.setBranchCheckbox(true);
					 
					 actionForm.saveInvBOMLList(invBOMLList);
				}
				
				try {
					
					actionForm.clearInvOMLList();	        
					
					actionForm.clearUnitMeasureList();           	
					
					list = ejbOML.getInvUomAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setUnitMeasureList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setUnitMeasureList((String)i.next());
							
						}
						
					} 	           
					
					list = ejbOML.getInvOmlAll(user.getCmpCode()); 
					
					i = list.iterator();
					while(i.hasNext()) {
						
						InvModOverheadMemoLineDetails mdetails = (InvModOverheadMemoLineDetails)i.next();
						
						InvOverheadMemoLineList invOMLList = new InvOverheadMemoLineList(actionForm,
								mdetails.getOmlCode(),
								mdetails.getOmlName(),
								mdetails.getOmlDescription(),
								mdetails.getOmlUomName(),
								Common.convertDoubleToStringMoney(mdetails.getOmlUnitCost(), precisionUnit),
								mdetails.getOmlCoaOverheadAccount(),							
								mdetails.getOmlCoaOverheadAccountDescription(),
								mdetails.getOmlCoaLiabilityAccount(),
								mdetails.getOmlCoaLiabilityAccountDescription());
						
						actionForm.saveInvOMLList(invOMLList);
					}
				} catch (GlobalNoRecordFoundException ex) {
					ex.printStackTrace();
				} catch (EJBException ex) {
					ex.printStackTrace();
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvOverheadMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}          
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveButton") != null || 
							request.getParameter("updateButton") != null) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
					
				}
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                                  
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				
				return(mapping.findForward("invOverheadMemoLines"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				e.printStackTrace();
				log.info("Exception caught in InvOverheadMemoLineAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
	
}