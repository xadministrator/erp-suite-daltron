package com.struts.inv.overheadmemolines;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdModBranchOverheadMemoLineDetails;

public class InvOverheadMemoLineForm extends ActionForm implements Serializable {
	
	private Integer overheadMemoLineCode = null;
	private String overheadMemoLineName = null;
	private String description = null;
	private String unitMeasure = null;
	private ArrayList unitMeasureList = new ArrayList();
	private String unitCost = null;
	private String overheadAccount = null;
	private String overheadAccountDescription = null;
	private String liabilityAccount = null;
	private String liabilityAccountDescription = null;
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList invOMLList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private ArrayList invBOMLList = new ArrayList();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public InvOverheadMemoLineList getInvOMLByIndex(int index) { 
		
		return((InvOverheadMemoLineList)invOMLList.get(index));
		
	}
	
	public Object[] getInvOMLList() {
		
		return(invOMLList.toArray());
		
	}
	
	public int getInvOMLListSize() {
		
		return(invOMLList.size());
		
	}
	
	public void saveInvOMLList(Object newInvOMLList) {
		
		invOMLList.add(newInvOMLList);
		
	}
	
	public void clearInvOMLList() {
		
		invOMLList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvOMLList, boolean isEdit) {
		
		this.rowSelected = invOMLList.indexOf(selectedInvOMLList);
		
		if(isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showInvOMLRow(ArrayList branchList, int rowSelected) {
		
		this.overheadMemoLineCode = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getOverheadMemoLineCode();
		this.overheadMemoLineName = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getOverheadMemoLineName();
		this.description = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getDescription();
		
		if(!this.unitMeasureList.contains(((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getUnitMeasure())) {
			
			if (this.unitMeasureList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				unitMeasureList.clear();
				unitMeasureList.add(Constants.GLOBAL_BLANK);
				
			}			
			this.unitMeasureList.add(((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getUnitMeasure());
			
		}		
		this.unitMeasure = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getUnitMeasure();
		
		this.unitCost = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getUnitCost();
		this.overheadAccount = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getOverheadAccount();
		this.overheadAccountDescription = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getOverheadAccountDescription();
		this.liabilityAccount = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getLiabilityAccount();
		this.liabilityAccountDescription = ((InvOverheadMemoLineList)invOMLList.get(rowSelected)).getLiabilityAccountDescription();
		
		Object[] obj = this.getInvBOMLList();
		
		this.clearInvBOMLList();
		
		for(int i=0; i<obj.length; i++) {
			InvBranchOverheadMemoLineList bomlList = (InvBranchOverheadMemoLineList)obj[i];
			
			if(branchList != null) {
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					AdModBranchOverheadMemoLineDetails mdetails = (AdModBranchOverheadMemoLineDetails)j.next();
					if (mdetails.getBOMLBranchCode().equals(bomlList.getBrCode())) {
						bomlList.setBranchCheckbox(true);
						bomlList.setBranchOverheadAccount( mdetails.getBOMLOverheadAccountNumber() );
						bomlList.setBranchOverheadDescription( mdetails.getBOMLOverheadAccountDesc() );
						bomlList.setBranchLiabilityAccount( mdetails.getBOMLLiabilityAccountNumber() );
						bomlList.setBranchLiabilityDescription( mdetails.getBOMLLiabilityAccountDesc() );
						break;
					}
				}
			}
			
			this.invBOMLList.add(bomlList);
		}
	}
	
	public void updateInvOMLRow(int rowSelected, Object newInvOMLList) {
		
		invOMLList.set(rowSelected, newInvOMLList);
		
	}
	
	public void deleteInvOMLList(int rowSelected) {
		
		invOMLList.remove(rowSelected);
		
	}
	
	public void setUpdateButton(String updateButton) {
		
		this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton) {
		
		this.cancelButton = cancelButton;
		
	}
	
	public void setSaveButton(String saveButton) {
		
		this.saveButton = saveButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}	
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return(pageState);
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getOverheadMemoLineCode() {
		
		return overheadMemoLineCode;
		
	}
	
	public void setOverheadMemoLineCode(Integer overheadMemoLineCode) {
		
		this.overheadMemoLineCode = overheadMemoLineCode;
		
	}
	
	public String getOverheadMemoLineName() {
		
		return overheadMemoLineName;
		
	}
	
	public void setOverheadMemoLineName(String overheadMemoLineName) {
		
		this.overheadMemoLineName = overheadMemoLineName;
		
	}	
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}	
	
	public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}	
	
	public String getUnitMeasure() {
		
		return unitMeasure;
		
	}
	
	public void setUnitMeasure(String unitMeasure) {
		
		this.unitMeasure = unitMeasure;
		
	}	
	
	public ArrayList getUnitMeasureList() {
		
		return unitMeasureList;
		
	}
	
	public void setUnitMeasureList(String unitMeasure) {
		
		unitMeasureList.add(unitMeasure);
		
	}	
	
	public void clearUnitMeasureList() {
		
		unitMeasureList.clear();
		unitMeasureList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getOverheadAccount() {
		
		return overheadAccount;
		
	}
	
	public void setOverheadAccount(String overheadAccount) {
		
		this.overheadAccount = overheadAccount;
		
	}
	
	public String getOverheadAccountDescription() {
		
		return overheadAccountDescription;
		
	}
	
	public void setOverheadAccountDescription(String overheadAccountDescription) {
		
		this.overheadAccountDescription = overheadAccountDescription;
		
	}
	
	public String getLiabilityAccount() {
		
		return liabilityAccount;
		
		
	}
	
	public void setLiabilityAccount(String liabilityAccount) {
		
		this.liabilityAccount = liabilityAccount;
		
	}
	
	public String getLiabilityAccountDescription() {
		
		return liabilityAccountDescription;
		
		
	}
	
	public void setLiabilityAccountDescription(String liabilityAccount) {
		
		this.liabilityAccount = liabilityAccount;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}	
	
	public Object[] getInvBOMLList(){
		
		return invBOMLList.toArray();
		
	}
	
	public InvBranchOverheadMemoLineList getInvBOMLByIndex(int index) {
		
		return ((InvBranchOverheadMemoLineList)invBOMLList.get(index));
		
	}
	
	public int getInvBOMLListSize(){
		
		return(invBOMLList.size());
		
	}
	
	public void saveInvBOMLList(Object newInvBOMLList){
		
		invBOMLList.add(newInvBOMLList);   	  
		
	}
	
	public void clearInvBOMLList(){
		
		invBOMLList.clear();
		
	}
	
	public void setInvBOMLList(ArrayList invBOMLList) {
		
		this.invBOMLList = invBOMLList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<invBOMLList.size(); i++) {
			
			InvBranchOverheadMemoLineList actionList = (InvBranchOverheadMemoLineList)invBOMLList.get(i);
			actionList.setBranchOverheadAccount("");
			actionList.setBranchOverheadDescription("");
			actionList.setBranchLiabilityAccount("");
			actionList.setBranchLiabilityDescription("");
			actionList.setBranchCheckbox(false);
			
		}
		
		overheadMemoLineName = null;
		description = null;
		unitMeasure = Constants.GLOBAL_BLANK;
		unitCost = null;
		overheadAccount = null;
		overheadAccountDescription = null;
		liabilityAccount = null;
		liabilityAccountDescription = null;
		saveButton = null;
		closeButton = null;
		updateButton = null;
		cancelButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;	   
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if(Common.validateRequired(overheadMemoLineName)) {
				
				errors.add("overheadMemoLineName", new ActionMessage("overheadMemoLine.error.overheadMemoLineNameRequired"));
				
			}
			
			if(Common.validateRequired(unitCost)) {
				
				errors.add("unitCost", new ActionMessage("overheadMemoLine.error.unitCostRequired"));
				
			}
			
			if(!Common.validateMoneyFormat(unitCost)){
				
				errors.add("unitCost", new ActionMessage("overheadMemoLine.error.unitCostInvalid"));
				
			}
			
			if(Common.validateRequired(unitMeasure)) {
				
				errors.add("unitMeasure", new ActionMessage("overheadMemoLine.error.unitMeasureRequired"));
				
			}
			
			if(Common.validateRequired(overheadAccount)) {
				
				errors.add("overheadAccount", new ActionMessage("overheadMemoLine.error.overheadAccountRequired"));
				
			}
			
			if(Common.validateRequired(liabilityAccount)) {
				
				errors.add("liabilityAccount", new ActionMessage("overheadMemoLine.error.liabilityAccountRequired"));
				
			}
			
		}
		
		return(errors);
	}
	
}
