package com.struts.inv.overheadmemolines;

import java.io.Serializable;

public class InvOverheadMemoLineList implements Serializable{
	
	private Integer overheadMemoLineCode = null;
	private String overheadMemoLineName = null;
	private String description = null;
	private String unitMeasure = null;
	private String unitCost = null;
	private String overheadAccount = null;
	private String overheadAccountDescription = null;
	private String liabilityAccount = null;
	private String liabilityAccountDescription;
	private String deleteButton = null;
	private String editButton = null;
	
	private InvOverheadMemoLineForm parentBean;
	
	public InvOverheadMemoLineList(InvOverheadMemoLineForm parentBean,
			Integer overheadMemoLineCode,
			String overheadMemoLineName,
			String description,
			String unitMeasure,
			String unitCost,
			String overheadAccount,
			String overheadAccountDescription,
			String liabilityAccount,
			String liabilityAccountDescription) {

			
		this.parentBean = parentBean;
		this.overheadMemoLineCode = overheadMemoLineCode;
		this.overheadMemoLineName = overheadMemoLineName;
		this.description = description;
		this.unitMeasure = unitMeasure;
		this.unitCost = unitCost;
		this.overheadAccount = overheadAccount;
		this.overheadAccountDescription = overheadAccountDescription;
		this.liabilityAccount = liabilityAccount;
		this.liabilityAccountDescription = liabilityAccountDescription;
		
	}
	
	public void setParentBean(InvOverheadMemoLineForm parentBean){
		
		this.parentBean = parentBean;
		
	}
	
	public void setDeleteButton(String deleteButton){
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public void setEditButton(String editButton){
		
		parentBean.setRowSelected(this, true);
		
	}

	public Integer getOverheadMemoLineCode() {
		
		return overheadMemoLineCode;
		
	}
	
	public String getOverheadMemoLineName() {
		
		return overheadMemoLineName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getUnitMeasure() {
		
		return unitMeasure;
		
	}
	
	public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public String getOverheadAccount() {
		
		return overheadAccount;
		
	}	
	
	public String getOverheadAccountDescription() {
		
		return overheadAccountDescription;
		
	}
	
	public String getLiabilityAccount() {
		
		return liabilityAccount;
		
	}
	
	public String getLiabilityAccountDescription() {
		
		return liabilityAccountDescription;
		
	}

}
