package com.struts.inv.physicalinventoryentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.InvPhysicalInventoryEntryController;
import com.ejb.txn.InvPhysicalInventoryEntryControllerHome;
import com.struts.ar.invoiceentry.ArInvoiceEntryList;
import com.struts.ar.invoiceentry.ArInvoiceLineItemList;
import com.struts.inv.adjustmententry.InvAdjustmentEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModPhysicalInventoryDetails;
import com.util.InvModPhysicalInventoryLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvPhysicalInventoryDetails;

public final class InvPhysicalInventoryEntryAction extends Action{

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try {

/*******************************************************
Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("InvPhysicalInventoryEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			InvPhysicalInventoryEntryForm actionForm = (InvPhysicalInventoryEntryForm)form;
			actionForm.setReport(null);

			String frParam = Common.getUserPermission(user, Constants.INV_PHYSICAL_INVENTORY_ENTRY_ID);

			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));

						return mapping.findForward("invPhysicalInventoryEntry");
					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

/*******************************************************
Initialize InvPhysicalInventoryEntryController EJB
*******************************************************/

			InvPhysicalInventoryEntryControllerHome homePIE = null;
			InvPhysicalInventoryEntryController ejbPIE = null;

			try {

				homePIE = (InvPhysicalInventoryEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvPhysicalInventoryEntryControllerEJB", InvPhysicalInventoryEntryControllerHome.class);


			} catch (NamingException e) {

				if (log.isInfoEnabled()) {

					log.info("NamingException caught in InvPhysicalInventoryEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return mapping.findForward("cmnErrorPage");

			}

			try {

				ejbPIE = homePIE.create();

			} catch (CreateException e) {

				if (log.isInfoEnabled()) {

					log.info("CreateException caught in InvPhysicalInventoryEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}
				return mapping.findForward("cmnErrorPage");

			}

			ActionErrors errors = new ActionErrors();

/*******************************************************
Call InvPhysicalInventoryEntryController EJB
getInvGpQuantityPrecisionUnit
*******************************************************/

			short quantityPrecisionUnit = 0;
			boolean isNotWastageAdjusted = false;
			boolean isNotVarianceAdjusted = false;

			short physicalinventoryLineNumber = 4;

			try {

				quantityPrecisionUnit = ejbPIE.getInvGpQuantityPrecisionUnit(user.getCmpCode());



			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in InvPhysicalInventoryEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}

/*******************************************************
 -- Save Action --
 *******************************************************/

		if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArrayList pilList = new ArrayList();

				for (int i=0; i<actionForm.getInvPIListSize(); i++) {

					InvPhysicalInventoryEntryList actionList = actionForm.getInvPIByIndex(i);

					if (Common.validateRequired(actionList.getLocation()) &&
		         	 	     Common.validateRequired(actionList.getItemName()) &&
		         	 	     Common.validateRequired(actionList.getUnit()) &&
		         	 	     Common.validateRequired(actionList.getEndingInventory())
		        		   ) continue;

					InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();

					mPilDetails.setPilCode(actionList.getPhysicalInventoryLineCode());
					mPilDetails.setPilEndingInventory(Common.convertStringMoneyToDouble(actionList.getEndingInventory(), quantityPrecisionUnit));
					mPilDetails.setPilWastage(Common.convertStringMoneyToDouble(actionList.getWastage(), quantityPrecisionUnit));
					mPilDetails.setPilVariance(Common.convertStringMoneyToDouble(actionList.getVariance(), quantityPrecisionUnit));
					mPilDetails.setPilIlIiName(actionList.getItemName());
					mPilDetails.setPilIlLocName(actionList.getLocation());
					mPilDetails.setPilUomName(actionList.getUnit());
					mPilDetails.setPilMisc(actionList.getMisc());
					pilList.add(mPilDetails);

				}

				InvModPhysicalInventoryDetails details = new InvModPhysicalInventoryDetails();

				details.setPiCode(actionForm.getPhysicalInventoryCode());
				details.setPiDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setPiReferenceNumber(actionForm.getReferenceNumber());
				details.setPiDescription(actionForm.getDescription());

				if (actionForm.getPhysicalInventoryCode() == null) {

					details.setPiCreatedBy(user.getUserName());
					details.setPiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

				}

				details.setPiLastModifiedBy(user.getUserName());
				details.setPiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
				details.setPiAdLvCategory(actionForm.getCategory());
				details.setPiWastageAccount(actionForm.getWastageAccount());
				details.setPiWastageAccountDescription(actionForm.getWastageAccountDescription());

				try {

					Integer physicalInventoryCode = ejbPIE.saveInvPiEntry(details, actionForm.getLocation(), pilList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					actionForm.setPhysicalInventoryCode(physicalInventoryCode);

				} catch (GlobalRecordAlreadyExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("physicalInventoryEntry.error.recordAlreadyExist"));

				}

/*******************************************************
 -- Save Action --
 *******************************************************/

		} else if (request.getParameter("printButton") != null &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

					actionForm.setReport(Constants.STATUS_SUCCESS);

					return(mapping.findForward("invPhysicalInventoryEntry"));

/*******************************************************
 -- Adjust Wastage Action --
 *******************************************************/

		} else if (request.getParameter("adjustWastageButton") != null &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			ArrayList pilList = new ArrayList();

			for (int i=0; i<actionForm.getInvPIListSize(); i++) {

				InvPhysicalInventoryEntryList actionList = actionForm.getInvPIByIndex(i);

				System.out.println(actionList.getLineNumber() + " = "+actionList.getItemName());

				if (Common.validateRequired(actionList.getLocation()) &&
	         	 	     Common.validateRequired(actionList.getItemName()) &&
	         	 	     Common.validateRequired(actionList.getUnit())
	         	 	   //&& Common.validateRequired(actionList.getEndingInventory())
	        		   ) continue;


				InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();

				mPilDetails.setPilCode(actionList.getPhysicalInventoryLineCode());
				mPilDetails.setPilEndingInventory(Common.convertStringMoneyToDouble(actionList.getEndingInventory(), quantityPrecisionUnit));
				mPilDetails.setPilWastage(Common.convertStringMoneyToDouble(actionList.getWastage(), quantityPrecisionUnit));
				mPilDetails.setPilVariance(Common.convertStringMoneyToDouble(actionList.getVariance(), quantityPrecisionUnit));
				mPilDetails.setPilIlLocName(actionList.getLocation());
				mPilDetails.setPilIlIiName(actionList.getItemName());
				mPilDetails.setPilUomName(actionList.getUnit());
				pilList.add(mPilDetails);

			}

			InvPhysicalInventoryDetails details = new InvModPhysicalInventoryDetails();

			details.setPiCode(actionForm.getPhysicalInventoryCode());
			details.setPiDate(Common.convertStringToSQLDate(actionForm.getDate()));
			details.setPiReferenceNumber(actionForm.getReferenceNumber());
			details.setPiDescription(actionForm.getDescription());

			if (actionForm.getPhysicalInventoryCode() == null) {

				details.setPiCreatedBy(user.getUserName());
				details.setPiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

			}

			details.setPiLastModifiedBy(user.getUserName());
			details.setPiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
			details.setPiAdLvCategory(actionForm.getCategory());

			try {

				Integer physicalInventoryCode = ejbPIE.saveInvPiEntry(details, actionForm.getLocation(), pilList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				actionForm.setPhysicalInventoryCode(physicalInventoryCode);

				Integer adjustmentCode = ejbPIE.executeInvWastageGeneration(physicalInventoryCode,
						actionForm.getWastageAccount(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				if (adjustmentCode != null) {

					String path = "/invAdjustmentEntry.do?forwardWastage=1" + "&adjustmentCode=" + adjustmentCode;

					return(new ActionForward(path));

			    } else {

			    	isNotWastageAdjusted = true;

			    }

			} catch (GlobalRecordAlreadyExistException ex) {

				errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("physicalInventoryEntry.error.recordAlreadyExist"));

			} catch(GlobalBranchAccountNumberInvalidException ex) {

				errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("physicalInventoryEntry.error.branchAccountNumberInvalid", ex.getMessage()));

			}



/*******************************************************
 -- Adjust Variance Action --
*******************************************************/

		} else if (request.getParameter("adjustVarianceButton") != null &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			Integer adjustmentCode = ejbPIE.executeInvVarianceGeneration(actionForm.getPhysicalInventoryCode(),
					actionForm.getVarianceAccount(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

			if (adjustmentCode != null) {

				String path = "/invAdjustmentEntry.do?forwardVariance=1" + "&adjustmentCode=" + adjustmentCode;

				return(new ActionForward(path));

			} else {

				isNotVarianceAdjusted = true;

			}

/*******************************************************
 -- Delete Action --
*******************************************************/

} else if (request.getParameter("deleteButton") != null &&
		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			ejbPIE.deleteInvPiEntry(actionForm.getPhysicalInventoryCode(), user.getUserName());

/*******************************************************
-- Close Action --
*******************************************************/

		} else if(request.getParameter("closeButton") != null) {

			return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Inv PI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {

         	int listSize = actionForm.getInvPIListSize();
         	int lineNumber = 0;

            for (int x = listSize + 1; x <= listSize + physicalinventoryLineNumber; x++) {

            	ArrayList comboItem = new ArrayList();

            	InvPhysicalInventoryEntryList physicalInventoryList = new InvPhysicalInventoryEntryList(actionForm,
						null, new Integer(x).toString(),
						null,
						null, null,null,"0.00","0.00","0.00","0.00", null);

            	physicalInventoryList.setLocationList(actionForm.getLocationList());

            	physicalInventoryList.setUnitList(Constants.GLOBAL_BLANK);
            	physicalInventoryList.setUnitList("Select Item First");

	        	actionForm.saveInvPIList(physicalInventoryList);

	        }

	        for (int i = 0; i<actionForm.getInvPIListSize(); i++ ) {

	        	InvPhysicalInventoryEntryList invPIList = actionForm.getInvPIByIndex(i);

	        	invPIList.setLineNumber(new Integer(i+1).toString());

            }

	        return(mapping.findForward("invPhysicalInventoryEntry"));

/*******************************************************
   -- Inv PI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {

         	for (int i = 0; i<actionForm.getInvPIListSize(); i++) {

           	   InvPhysicalInventoryEntryList invPIList = actionForm.getInvPIByIndex(i);

           	   if (invPIList.isDeleteCheckbox()) {

           	   		actionForm.deleteInvPIList(i);
           	   		i--;
           	   }

            }

	        return(mapping.findForward("invPhysicalInventoryEntry"));

/*******************************************************
-- Inv PI Category and Location Enter Action --
*******************************************************/

		} else if ((!Common.validateRequired(request.getParameter("isCategoryEntered")) ||
				!Common.validateRequired(request.getParameter("isLocationEntered"))  ||
				!Common.validateRequired(request.getParameter("isDateEntered"))) &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (!Common.validateRequired(actionForm.getCategory()) &&
					!Common.validateRequired(actionForm.getLocation()) &&
					!Common.validateRequired(actionForm.getDate())) {


				actionForm.clearInvPIList();

				try {

					ArrayList list = new ArrayList();
					list = ejbPIE.getInvIlByPiDateInvLocNameAndInvIiAdLvCategory(Common.convertStringToSQLDate(actionForm.getDate()), actionForm.getLocation(),
							actionForm.getCategory(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					Iterator i = list.iterator();

					int ctr = 0;

					while (i.hasNext()) {

						InvModPhysicalInventoryLineDetails pilDetails = (InvModPhysicalInventoryLineDetails)i.next();

						InvPhysicalInventoryEntryList physicalInventoryList = new InvPhysicalInventoryEntryList(actionForm,
								pilDetails.getPilCode(), new Integer(++ctr).toString(),
								pilDetails.getPilIlLocName(),
								pilDetails.getPilIlIiName(),
								pilDetails.getPilIlIiDescription(),
								pilDetails.getPilIlIiUnit(),
								Common.convertDoubleToStringMoney(pilDetails.getPilRemainingQuantity(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(pilDetails.getPilWastage(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(pilDetails.getPilEndingInventory(), quantityPrecisionUnit),
								Common.convertDoubleToStringMoney(pilDetails.getPilRemainingQuantity(), quantityPrecisionUnit),
								pilDetails.getPilMisc());


						physicalInventoryList.setLocationList(actionForm.getLocationList());

						physicalInventoryList.clearUnitList();

						ArrayList uomList = ejbPIE.getInvUomByIiName(pilDetails.getPilIlIiName(), user.getCmpCode());

						Iterator j = uomList.iterator();

						while(j.hasNext()) {

							InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
							physicalInventoryList.setUnitList(mUomDetails.getUomName());

						}

						actionForm.saveInvPIList(physicalInventoryList);

					}

				} catch (GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("physicalInventoryEntry.error.noItemFound"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvPhysicalInventoryEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}










			}

			if (!errors.isEmpty()) {

				saveErrors(request, new ActionMessages(errors));
				return(mapping.findForward("invPhysicalInventoryEntry"));

			}

			return(mapping.findForward("invPhysicalInventoryEntry"));


/*******************************************************
    -- Ar INV ILI Item Enter Action --
 *******************************************************/

		} else if(!Common.validateRequired(request.getParameter("invPIList[" +
                actionForm.getRowSelected() + "].isItemEntered"))) {

			System.out.println("--------------------->isItemEntered");

			InvPhysicalInventoryEntryList invPIList =
					actionForm.getInvPIByIndex(actionForm.getRowSelected());

			try {


				System.out.println("invPIList.getItemName()="+invPIList.getItemName());
				System.out.println("invPIList.getLocation()="+invPIList.getLocation());

				InvModPhysicalInventoryLineDetails pilDetails = ejbPIE.getInvIlByPiDateInvIiName(Common.convertStringToSQLDate(actionForm.getDate()),
						invPIList.getItemName(), invPIList.getLocation() , new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				invPIList.setActual(Common.convertDoubleToStringMoney(pilDetails.getPilRemainingQuantity(), quantityPrecisionUnit));
				invPIList.setWastage(Common.convertDoubleToStringMoney(pilDetails.getPilWastage(), quantityPrecisionUnit));
				invPIList.setEndingInventory(Common.convertDoubleToStringMoney(pilDetails.getPilEndingInventory(), quantityPrecisionUnit));
				invPIList.setVariance(Common.convertDoubleToStringMoney(pilDetails.getPilRemainingQuantity(), quantityPrecisionUnit));

				invPIList.setLocationList(actionForm.getLocationList());

				invPIList.clearUnitList();

				ArrayList uomList = ejbPIE.getInvUomByIiName(invPIList.getItemName(), user.getCmpCode());

				Iterator j = uomList.iterator();

				while(j.hasNext()) {

					InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
					invPIList.setUnitList(mUomDetails.getUomName());

				}

				//actionForm.saveInvPIList(physicalInventoryList);



			} catch (GlobalNoRecordFoundException ex) {

				errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("physicalInventoryEntry.error.noItemFound"));

			} catch (EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in InvPhysicalInventoryEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
					return mapping.findForward("cmnErrorPage");

				}

			}




         return(mapping.findForward("invPhysicalInventoryEntry"));
/*****************************************************
    ---INV Physical Inventory Unit Enter Action
*****************************************************/

		} else if(!Common.validateRequired(request.getParameter("invPIList[" +
				actionForm.getRowSelected() + "].isUnitEntered")) &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

		    	InvPhysicalInventoryEntryList invPIList =
					actionForm.getInvPIByIndex(actionForm.getRowSelected());

				try {

					// populate actual qty field

					if (!Common.validateRequired(invPIList.getItemName())) {

						double actual = ejbPIE.getQuantityByIiNameAndUomName(invPIList.getItemName(), actionForm.getLocation(), invPIList.getUnit(),
						        Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						invPIList.setActual(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
						invPIList.setVariance(Common.convertDoubleToStringMoney(actual, quantityPrecisionUnit));
						invPIList.setEndingInventory(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
						invPIList.setWastage(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));


					}

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvPhysicalInventoryEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				return(mapping.findForward("invPhysicalInventoryEntry"));

/*******************************************************
-- Load Action --
*******************************************************/

			}

			if (frParam != null) {

				System.out.println("-- Load Action --");

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("invPhysicalInventoryEntry"));

				}

				if (request.getParameter("forward") == null &&
	                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

	                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	                saveErrors(request, new ActionMessages(errors));

	                return mapping.findForward("cmnMain");

	            }

				ArrayList list = null;
				Iterator i = null;

				try {

					actionForm.clearCategoryList();

	            	list = ejbPIE.getAdLvInvItemCategoryAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCategoryList((String)i.next());

	            		}

	            	}

					actionForm.clearLocationList();

					list = ejbPIE.getInvLocAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setLocationList((String)i.next());

						}

					}



	            	actionForm.clearUserList();

	            	ArrayList userList = ejbPIE.getAdUsrAll(user.getCmpCode());

	            	if (userList == null || userList.size() == 0) {

	            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		Iterator x = userList.iterator();

	            		while (x.hasNext()) {

	            			actionForm.setUserList((String)x.next());

	            		}

	            	}

					actionForm.clearInvPIList();
					System.out.println("isNotWastageAdjusted="+isNotWastageAdjusted);
					System.out.println("isNotVarianceAdjusted="+isNotVarianceAdjusted);
					if (request.getParameter("forward") != null || isNotWastageAdjusted || isNotVarianceAdjusted) {

						if (request.getParameter("forward") != null) {

							actionForm.setPhysicalInventoryCode(new Integer(request.getParameter("physicalInventoryCode")));

						}

						InvModPhysicalInventoryDetails mdetails = ejbPIE.getInvPiByPiCode(Common.convertStringToSQLDate(actionForm.getDate()), actionForm.getPhysicalInventoryCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setReferenceNumber(mdetails.getPiReferenceNumber());
						actionForm.setDate(Common.convertSQLDateToString(mdetails.getPiDate()));
						actionForm.setDescription(mdetails.getPiDescription());
						actionForm.setLocation(mdetails.getPiLocName());
						actionForm.setCreatedBy(mdetails.getPiCreatedBy());
						actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getPiDateCreated()));
						actionForm.setLastModifiedBy(mdetails.getPiLastModifiedBy());
						actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getPiDateLastModified()));
						actionForm.setCategory(mdetails.getPiAdLvCategory());
						actionForm.setWastageAdjusted(Common.convertByteToBoolean(mdetails.getPiWastageAdjusted()));
						actionForm.setVarianceAdjusted(Common.convertByteToBoolean(mdetails.getPiVarianceAdjusted()));
						actionForm.setWastageAccount(mdetails.getPiWastageAccount());
						actionForm.setWastageAccountDescription(mdetails.getPiWastageAccountDescription());




						if (mdetails.getPiPilList() != null && !mdetails.getPiPilList().isEmpty()) {


							list = mdetails.getPiPilList();

							i = list.iterator();

							int lineNumber = 0;

							while (i.hasNext()) {

								InvModPhysicalInventoryLineDetails mPilDetails = (InvModPhysicalInventoryLineDetails)i.next();

								InvPhysicalInventoryEntryList piPilList = new InvPhysicalInventoryEntryList(actionForm,
										mPilDetails.getPilCode(), new Integer(++lineNumber).toString(),
										mPilDetails.getPilIlLocName(),
										mPilDetails.getPilIlIiName(), mPilDetails.getPilIlIiDescription(),
										mPilDetails.getPilIlIiUnit(),
										Common.convertDoubleToStringMoney(mPilDetails.getPilRemainingQuantity(), quantityPrecisionUnit),
										Common.convertDoubleToStringMoney(mPilDetails.getPilWastage(), quantityPrecisionUnit),
										Common.convertDoubleToStringMoney(mPilDetails.getPilEndingInventory(), quantityPrecisionUnit),
										Common.convertDoubleToStringMoney(mPilDetails.getPilVariance(), quantityPrecisionUnit),
										mPilDetails.getPilMisc());


								piPilList.setLocationList(actionForm.getLocationList());

								piPilList.clearUnitList();

								ArrayList uomList = ejbPIE.getInvUomByIiName(mPilDetails.getPilIlIiName(), user.getCmpCode());

								Iterator j = uomList.iterator();

								while(j.hasNext()) {

									InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
									piPilList.setUnitList(mUomDetails.getUomName());

								}





								// Set Remaining Quantity
								if (actionForm.getVarianceAdjusted()){

									piPilList.setActual(Common.convertDoubleToStringMoney(mPilDetails.getPilWastage() +
											mPilDetails.getPilEndingInventory() + mPilDetails.getPilVariance(), quantityPrecisionUnit));

								}

								if (!actionForm.getVarianceAdjusted()) {

									piPilList.setVariance(Common.convertDoubleToStringMoney(mPilDetails.getPilRemainingQuantity() -
											mPilDetails.getPilEndingInventory(), quantityPrecisionUnit));
								}




				            	actionForm.clearUserList();

				            	userList = ejbPIE.getAdUsrAll(user.getCmpCode());

				            	if (userList == null || userList.size() == 0) {

				            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

				            	} else {

				            		Iterator x = userList.iterator();

				            		while (x.hasNext()) {

				            			actionForm.setUserList((String)x.next());

				            		}

				            	}

								actionForm.saveInvPIList(piPilList);

							}

							try {

								list = ejbPIE.getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode(Common.convertStringToSQLDate(actionForm.getDate()), actionForm.getLocation(), actionForm.getCategory(), actionForm.getPhysicalInventoryCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

								i = list.iterator();

								while (i.hasNext()) {

									InvModPhysicalInventoryLineDetails mPilDetails = (InvModPhysicalInventoryLineDetails)i.next();

									InvPhysicalInventoryEntryList piPilList = new InvPhysicalInventoryEntryList(actionForm,
											mPilDetails.getPilCode(), new Integer(++lineNumber).toString(),
											mPilDetails.getPilIlLocName(),
											mPilDetails.getPilIlIiName(), mPilDetails.getPilIlIiDescription(),
											mPilDetails.getPilIlIiUnit(),
											Common.convertDoubleToStringMoney(mPilDetails.getPilRemainingQuantity(), quantityPrecisionUnit),
											Common.convertDoubleToStringMoney(mPilDetails.getPilWastage(), quantityPrecisionUnit),
											Common.convertDoubleToStringMoney(mPilDetails.getPilEndingInventory(), quantityPrecisionUnit),
											Common.convertDoubleToStringMoney(mPilDetails.getPilVariance(), quantityPrecisionUnit),
											mPilDetails.getPilMisc());

//									// Set Remaining Quantity
									if (actionForm.getVarianceAdjusted())

										piPilList.setActual(Common.convertDoubleToStringMoney(mPilDetails.getPilWastage() +
											mPilDetails.getPilEndingInventory() + mPilDetails.getPilVariance(), quantityPrecisionUnit));

									if (!actionForm.getVarianceAdjusted()) {

										piPilList.setVariance(Common.convertDoubleToStringMoney(mPilDetails.getPilRemainingQuantity() -
												mPilDetails.getPilEndingInventory(), quantityPrecisionUnit));
									}

									piPilList.clearUnitList();

									ArrayList uomList = ejbPIE.getInvUomByIiName(mPilDetails.getPilIlIiName(), user.getCmpCode());

									Iterator j = uomList.iterator();

									while(j.hasNext()) {

										InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
										piPilList.setUnitList(mUomDetails.getUomName());

									}

									actionForm.saveInvPIList(piPilList);

								}

							} catch (GlobalNoRecordFoundException ex) {

							}

							actionForm.setWastageAdjusted(Common.convertByteToBoolean(mdetails.getPiWastageAdjusted()));
							actionForm.setVarianceAdjusted(Common.convertByteToBoolean(mdetails.getPiVarianceAdjusted()));

			            	InvModPhysicalInventoryDetails piDetails = ejbPIE.getInvPrfDefaultVarianceAccount(new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
							actionForm.setVarianceAccount(piDetails.getPiVarianceAccount());
							actionForm.setVarianceAccountDescription(piDetails.getPiVarianceAccountDescription());

							this.setFormProperties(actionForm);
							return(mapping.findForward("invPhysicalInventoryEntry"));

						}



					}

					System.out.println("1--------------------->Populate line when not forwarding");
					short invoiceLineNumber = 4;
	                 for (int x = 1; x <= invoiceLineNumber; x++) {

	                     InvPhysicalInventoryEntryList physicalInventoryList = new InvPhysicalInventoryEntryList(actionForm,
									null, new Integer(x).toString(),
									null,
									null, null,null,"0.00","0.00","0.00","0.00", null);

	                     physicalInventoryList.setUnitList(Constants.GLOBAL_BLANK);
	                     physicalInventoryList.setUnitList("Select Item First");

	                     physicalInventoryList.setLocationList(actionForm.getLocationList());

	                     actionForm.saveInvPIList(physicalInventoryList);
	                 }

				} catch(GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("physicalInventoryEntry.error.noRecordFound"));

				} catch(EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in InvPhysicalInventoryEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

						return(mapping.findForward("cmnErrorPage"));

					}

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				} else {

					if ((request.getParameter("saveButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					}
				}

				actionForm.reset(mapping, request);


				actionForm.setPhysicalInventoryCode(null);
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setCreatedBy(user.getUserName());
				actionForm.setLastModifiedBy(user.getUserName());
				actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setWastageAdjusted(false);
				actionForm.setVarianceAdjusted(false);
            	InvModPhysicalInventoryDetails piDetails = ejbPIE.getInvPrfDefaultWastageAccount(new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
            	actionForm.setWastageAccount(piDetails.getPiWastageAccount());
            	actionForm.setWastageAccountDescription(piDetails.getPiWastageAccountDescription());

				this.setFormProperties(actionForm);
				return(mapping.findForward("invPhysicalInventoryEntry"));

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

		} catch(Exception e) {

/*******************************************************
System Failed: Forward to error page
*******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in InvPhysicalInventoryEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}

			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));

		}

	}

	private void setFormProperties(InvPhysicalInventoryEntryForm actionForm) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			actionForm.setShowAddLinesButton(true);
			actionForm.setShowDeleteLinesButton(true);

			if (actionForm.getWastageAdjusted() == false) {

				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				if (actionForm.getPhysicalInventoryCode() == null) {
					actionForm.setShowDeleteButton(false);
				} else {
					actionForm.setShowDeleteButton(true);
				}
				actionForm.setShowWastage(true);
				actionForm.setShowVariance(false);

			} else if (actionForm.getWastageAdjusted() == true && actionForm.getVarianceAdjusted() == false) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowWastage(false);
				actionForm.setShowVariance(true);

			} else if (actionForm.getVarianceAdjusted() == true && actionForm.getVarianceAdjusted() == true) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowWastage(false);
				actionForm.setShowVariance(false);

			}

		} else {

			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);

		}

	}

}