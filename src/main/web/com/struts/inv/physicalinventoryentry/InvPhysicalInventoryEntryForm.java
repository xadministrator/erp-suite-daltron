package com.struts.inv.physicalinventoryentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.invoiceentry.ArInvoiceLineItemList;
import com.struts.util.Common;
import com.struts.util.Constants;


public class InvPhysicalInventoryEntryForm extends ActionForm implements Serializable {
	
	private Integer physicalInventoryCode = null;
	private String date = null;
	private String referenceNumber = null;
	private String description = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;	
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList(); 
	private String wastageAccount = null;
	private String wastageAccountDescription = null;
	private String varianceAccount = null;
	private String varianceAccountDescription = null;
	private boolean wastageAdjusted = false;
	private boolean varianceAdjusted = false;
	
	private String isCategoryEntered = null;
	private String isLocationEntered = null;
	private String isDateEntered = null;
	private String isUnitEntered = null;
	
	private int rowSelected = 0;
	private ArrayList invPIList = new ArrayList();
	private ArrayList userList = new ArrayList();
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	private boolean showWastage = false;
	private boolean showVariance = false;
	
	private String packaging = null;
	private ArrayList packagingList = new ArrayList();
	
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	
	
	private String report = null;
	
	public int getRowSelected(){
		return rowSelected;
	}
	
	public InvPhysicalInventoryEntryList getInvPIByIndex(int index){
		
		return((InvPhysicalInventoryEntryList)invPIList.get(index));
		
	}
	
	public Object[] getInvPIList(){
		
		return(invPIList.toArray());
		
	}
	
	public int getInvPIListSize(){
		
		return(invPIList.size());
		
	}
	
	public void saveInvPIList(Object newInvPIList){
		
		invPIList.add(newInvPIList);
		
	}
	
	public void clearInvPIList(){
		
		invPIList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvPIList, boolean isEdit){
		
		this.rowSelected = invPIList.indexOf(selectedInvPIList);
		
	}
	
	public void updateInvPIRow(int rowSelected, Object newInvPIList){
		
		invPIList.set(rowSelected, newInvPIList);
		
	}
	
	public void deleteInvPIList(int rowSelected){
		
		invPIList.remove(rowSelected);
		
	}
	
	public String getTxnStatus(){
		
		String passTxnStatus = txnStatus;      
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus){
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getPhysicalInventoryCode() {
		
		return physicalInventoryCode;
		
	}
	
	public void setPhysicalInventoryCode(Integer physicalInventoryCode) {
		
		this.physicalInventoryCode = physicalInventoryCode;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getDateCreated() {
		
		return dateCreated;
		
	}
	
	public void setDateCreated(String dateCreated) {
		
		this.dateCreated = dateCreated;
		
	}
	
	public String getDateLastModified() {
		
		return dateLastModified;
		
	}
	
	public void setDateLastModified(String dateLastModified) {
		
		this.dateLastModified = dateLastModified;
		
	}
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return categoryList;
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getWastageAccount() {
		
		return wastageAccount;
		
	}
	
	public void setWastageAccount(String wastageAccount) {
		
		this.wastageAccount = wastageAccount;
		
	}
	
	public String getWastageAccountDescription() {
		
		return wastageAccountDescription;
		
	}
	
	public void setWastageAccountDescription(String wastageAccountDescription) {
		
		this.wastageAccountDescription = wastageAccountDescription;
		
	}
	
	public String getVarianceAccount() {
		
		return varianceAccount;
		
	}
	
	public void setVarianceAccount(String varianceAccount) {
		
		this.varianceAccount = varianceAccount;
		
	}
	
	public String getVarianceAccountDescription() {
		
		return varianceAccountDescription;
		
	}
	
	public void setVarianceAccountDescription(String varianceAccountDescription) {
		
		this.varianceAccountDescription = varianceAccountDescription;
		
	}
	
	public boolean getWastageAdjusted() {
		
		return wastageAdjusted;
		
	}
	
	public void setWastageAdjusted(boolean wastageAdjusted) {
		
		this.wastageAdjusted = wastageAdjusted;
		
	}
	
	public boolean getVarianceAdjusted() {
		
		return varianceAdjusted;
		
	}
	
	public void setVarianceAdjusted(boolean varianceAdjusted) {
		
		this.varianceAdjusted = varianceAdjusted;
		
	}
										 
										 
	
	public String getIsCategoryEntered() {
		
		return isCategoryEntered;
		
	}
	
	public void setIsCategoryEntered(String isCategoryEntered) {
		
		this.isCategoryEntered = isCategoryEntered;
		
	}
	
	public String getIsLocationEntered() {
		
		return isLocationEntered;
		
	}
	
	public void setIsLocationEntered(String isLocationEntered) {
		
		this.isLocationEntered = isLocationEntered;
		
	}
	
	public String getIsDateEntered() {
		
		return isDateEntered;
		
	}
	
	public void setIsDateEntered(String isDateEntered) {
		
		this.isDateEntered = isDateEntered;
		
	}
	
	public boolean isEnableFields() {
		
		return enableFields;
	
	}
	
	public void setEnableFields(boolean enableFields) {
	
		this.enableFields = enableFields;
	
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowSaveButton() {
		
		return showSaveButton;
	
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
	
		this.showSaveButton = showSaveButton;
	
	}
	

	public boolean getShowDeleteButton() {
	
		return showDeleteButton;
	
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
	
	}
	
	public boolean isShowWastage() {
		
		return showWastage;
	
	}
	
	public void setShowWastage(boolean showWastage) {
	
		this.showWastage = showWastage;
	
	}
	
	public boolean isShowVariance() {
		
		return showVariance;
	
	}
	
	public void setShowVariance(boolean showVariance) {
	
		this.showVariance = showVariance;
	
	}
	public String getPackaging() {
	    
	    return packaging;
	    
	}
	
	public void setPackaging(String packaging) {
	    
	    this.packaging = packaging;
	    
	}
	
	public ArrayList getPackagingList() {
		
		return packagingList;
		
	}
	
	public void setPackagingList(String packaging) {
		
	    packagingList.add(packaging);
		
	}
	
	public void clearPackagingList() {
		
	    packagingList.clear();
	    packagingList.add(Constants.GLOBAL_BLANK);
		
	}  
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {
		
		this.isUnitEntered = isUnitEntered;
		
	}
	
	public ArrayList getUserList() {
			
		return userList;
			
	}
		
	public void setUserList(String user) {
			
		userList.add(user);
			
	}
	   
	public void clearUserList() {
		   	
		userList.clear();
		userList.add(Constants.GLOBAL_BLANK);
		   	
	}
	
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getViewType() {

		return viewType ;

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {      
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		date = null;
		referenceNumber = null;
		location = Constants.GLOBAL_BLANK;
		category = Constants.GLOBAL_BLANK;
		description = null;
		wastageAccount = null;
		wastageAccountDescription = null;
		varianceAccount = null;
		varianceAccountDescription = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;
		isCategoryEntered = null;
		isLocationEntered = null;
		isDateEntered = null;
		
		System.out.println("RESET----------->");
		for (int i=0; i<invPIList.size(); i++) {
			
			InvPhysicalInventoryEntryList actionList = (InvPhysicalInventoryEntryList)invPIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("saveButton") != null) {
			
			
			
			if(Common.validateRequired(description)){
				errors.add("description",
						new ActionMessage("physicalInventoryEntry.error.descriptionIsRequired"));
			}
			
			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("physicalInventoryEntry.error.dateRequired"));
			}
			
			if(!Common.validateDateFormat(date)){
				errors.add("date", 
						new ActionMessage("physicalInventoryEntry.error.dateInvalid"));
			}
			System.out.println("category="+category);
			System.out.println("category.equals(Constants.GLOBAL_NO_RECORD_FOUND)="+category.equals(Constants.GLOBAL_NO_RECORD_FOUND));
			System.out.println("Common.validateRequired(category)="+Common.validateRequired(category));
			
			if(! (Common.validateRequired(category) && Common.validateRequired(location))) {
				
				if(Common.validateRequired(location)){
					errors.add("location",
							new ActionMessage("physicalInventoryEntry.error.locationRequired"));
				}
				
				if (Common.validateRequired(category)) {
					
					errors.add("category",
							new ActionMessage("physicalInventoryEntry.error.categoryRequired"));
					
				}
				
			}
			
			
			
			int numberOfLines = 0;
			
			Iterator i = invPIList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvPhysicalInventoryEntryList pilList = (InvPhysicalInventoryEntryList)i.next();    
				
				
				if (Common.validateRequired(pilList.getLocation()) &&
		      	 	     Common.validateRequired(pilList.getItemName()) &&
		      	 	     Common.validateRequired(pilList.getUnit()) &&
		      	 	     Common.validateRequired(pilList.getEndingInventory())   
		      	 	     
						) continue;
				
				
				numberOfLines++;

				if(!Common.validateMoneyFormat(pilList.getEndingInventory())){
					errors.add("endingInventory",
							new ActionMessage("physicalInventoryEntry.error.endingInventoryInvalid", pilList.getLineNumber()));
				}
				
			}
			
			if (numberOfLines == 0) {
				
				errors.add("physicalInventory",
						new ActionMessage("physicalInventoryEntry.error.physicalInventoryMustHaveLine"));
				
			}

		}
		
		if(request.getParameter("adjustWastageButton") != null) {
			
			if(Common.validateRequired(wastageAccount)){
				errors.add("wastageAccount",
						new ActionMessage("physicalInventoryEntry.error.wastageAccountRequired"));
			}
			
			
			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("physicalInventoryEntry.error.dateRequired"));
			}
			
			if(!Common.validateDateFormat(date)){
				errors.add("date", 
						new ActionMessage("physicalInventoryEntry.error.dateInvalid"));
			}
			
			if(Common.validateRequired(description)){
				errors.add("description",
						new ActionMessage("physicalInventoryEntry.error.descriptionIsRequired"));
			}
			
			if(! (Common.validateRequired(category) && Common.validateRequired(location))) {
				
				if(Common.validateRequired(location)){
					errors.add("location",
							new ActionMessage("physicalInventoryEntry.error.locationRequired"));
				}
				
				if (Common.validateRequired(category)) {
					
					errors.add("category",
							new ActionMessage("physicalInventoryEntry.error.categoryRequired"));
					
				}
				
			}
			
			int numberOfLines = 0;
			
			Iterator i = invPIList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvPhysicalInventoryEntryList pilList = (InvPhysicalInventoryEntryList)i.next();      	 	 
				System.out.println("-----------------pilList.getLineNumber()"+pilList.getLineNumber());
				System.out.println("-----------------pilList.getItemName()"+pilList.getItemName());
				System.out.println("-----------------Common.validateRequired(pilList.getLocation())"+Common.validateRequired(pilList.getLocation()));
				System.out.println("-----------------Common.validateRequired(pilList.getItemName())"+Common.validateRequired(pilList.getItemName()));
				System.out.println("-----------------Common.validateRequired(pilList.getUnit())"+Common.validateRequired(pilList.getUnit()));
				System.out.println("-----------------Common.validateRequired(pilList.getEndingInventory())"+Common.validateRequired(pilList.getEndingInventory()));
				
				
				if (Common.validateRequired(pilList.getLocation()) &&
		      	 	     Common.validateRequired(pilList.getItemName()) &&
		      	 	     Common.validateRequired(pilList.getUnit()) 
		      	 	    //&& Common.validateRequired(pilList.getEndingInventory())
		      	 	    		 
		      	 	    		  
		      	 	     
						) continue;
				numberOfLines++;
				
				
				if(Common.validateRequired(pilList.getLocation()) || pilList.getLocation().equals(Constants.GLOBAL_BLANK)){
		            errors.add("location",
		            	new ActionMessage("invAdjustmentEntry.error.locationRequired", pilList.getLineNumber()));
		         }
		         if(Common.validateRequired(pilList.getItemName()) || pilList.getItemName().equals(Constants.GLOBAL_BLANK)){
		            errors.add("itemName",
		            	new ActionMessage("invAdjustmentEntry.error.itemNameRequired", pilList.getLineNumber()));
		         }
		         /*if(Common.validateRequired(pilList.getEndingInventory())) {
		         	errors.add("endingInventory",
		         		new ActionMessage("invAdjustmentEntry.error.adjustByRequired", pilList.getLineNumber()));
		         }
		         */
			 	 if(!Common.validateNumberFormat(pilList.getEndingInventory())){
		            errors.add("endingInventory",
		               new ActionMessage("invAdjustmentEntry.error.adjustByInvalid", pilList.getLineNumber()));
		         }
			 	 
			 	 if(Common.validateRequired(pilList.getUnit()) || pilList.getUnit().equals(Constants.GLOBAL_BLANK)){
		            errors.add("unit",
		            	new ActionMessage("invAdjustmentEntry.error.unitRequired", pilList.getLineNumber()));
		         }
			 	 

				
				
			}
			
			
			if (numberOfLines == 0) {
				
				errors.add("physicalInventory",
						new ActionMessage("physicalInventoryEntry.error.physicalInventoryMustHaveLine"));
				
			}
			
		}
			
		if(request.getParameter("adjustVarianceButton") != null) {
			
			if(Common.validateRequired(varianceAccount)){
				errors.add("varianceAccount",
						new ActionMessage("physicalInventoryEntry.error.varianceAccountRequired"));
			}
			
		}
		
		if(request.getParameter("isDateEntered") != null) {
									
			if(!Common.validateDateFormat(date)){
				errors.add("date", 
						new ActionMessage("physicalInventoryEntry.error.dateInvalid"));
			}
			
		}
		
		return(errors);
		
	}
	
}
