package com.struts.inv.physicalinventoryentry;
        
import java.io.Serializable;
import java.util.ArrayList;

public class InvPhysicalInventoryEntryList implements Serializable {

   private Integer physicalInventoryLineCode = null;
   private String lineNumber = null;
   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String itemName = null;
   private String itemDescription = null;
   private String unit = null;
   private ArrayList unitList = new ArrayList();
   private String actual = null;
   private String wastage = null;
   private String endingInventory = null;
   private String variance = null;
   private String misc = null;
   
   private String isUnitEntered = null;
   private String isItemEntered = null;
   private String isLocationEntered = null;
   private boolean deleteCheckbox = false;
   
   private InvPhysicalInventoryEntryForm parentBean;
    
   public InvPhysicalInventoryEntryList(InvPhysicalInventoryEntryForm parentBean,
      Integer physicalInventoryLineCode,
      String lineNumber,
      String location, 
      String itemName,	
      String itemDescription,
	  String unit,
	  String actual,
	  String wastage,
      String endingInventory,
	  String variance,
	  String misc){

      this.parentBean = parentBean;
      this.physicalInventoryLineCode = physicalInventoryLineCode;
      this.lineNumber = lineNumber;
      this.location = location;
      this.itemName = itemName;    
      this.itemDescription = itemDescription;
      this.unit = unit;
      this.actual = actual;
      this.wastage = wastage;
      this.endingInventory = endingInventory;
      this.variance = variance;
      this.misc = misc;
   }
         
   public Integer getPhysicalInventoryLineCode(){
      return(physicalInventoryLineCode);
   }
   
   public String getLineNumber() {
   	  return(lineNumber);
   }
   
   public void setLineNumber(String lineNumber) {
   	  this.lineNumber = lineNumber;
   }
   
   
   public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	
	public void clearLocationList() {
		
		locationList.clear();
		
	   }

	  
	   public String getIsLocationEntered() {
		

		return isLocationEntered;
		
	}
	   
	   public boolean isDeleteCheckbox() {
			
			return deleteCheckbox;
			
		}
		
		public void setDeleteCheckbox(boolean deleteCheckbox) {
			
			this.deleteCheckbox = deleteCheckbox;
			
		}
	

   public String getItemName(){
      return(itemName);
   }
   
   public void setItemName(String itemName) {
   	  this.itemName = itemName;
   }

   public String getItemDescription(){
      return(itemDescription);
   }
	 
   public void setItemDescription(String itemDescription) {
	  this.itemDescription = itemDescription;
   }
   
   public String getUnit(){
      return(unit);
   }
	 
   public void setUnit(String unit) {
	    this.unit = unit;
   }


   public String getActual() {
 	  return(actual);
   }
 
   public void setActual(String actual) {
 	  this.actual = actual;
   }
 
   public String getWastage() {
 	  return(wastage);
   }
 
   public void setWastage(String wastage) {
 	  this.wastage = wastage;
   }
   
   public String getEndingInventory() {
   	  return(endingInventory);
   }
   
   public void setEndingInventory(String endingInventory) {
   	  this.endingInventory = endingInventory;
   }
   
   public String getVariance() {
   	  return(variance);
   }
   
   public void setVariance(String variance) {
   	  this.variance = variance;
   }
   
   public String getMisc() {
	   return misc;
   }
  
   public void setMisc(String misc) {
	   this.misc = misc;
   }

   public void clearUnitList() {
	
	 unitList.clear();
	
   }

   public ArrayList getUnitList() {
	
	 return unitList;
	
   }

   public void setUnitList(String unit) {
	
	 unitList.add(unit);
	
   }
   public String getIsUnitEntered() {
	

	return isUnitEntered;
	
}
   
   public String getIsItemEntered() {
		

		return isItemEntered;
		
	}
   
   
  
   
   

public void setIsUnitEntered(String isUnitEntered) {	
	
	if (isUnitEntered != null && isUnitEntered.equals("true")) {
		
		parentBean.setRowSelected(this, false);
		
	}				
	
	isUnitEntered = null;
	
}


public void setIsItemEntered(String isItemEntered) {	
	
	if (isItemEntered != null && isItemEntered.equals("true")) {
		
		parentBean.setRowSelected(this, false);
		
	}				
	
	isItemEntered = null;
	
}

public void setIsLocationEntered(String isLocationEntered) {	
	
	if (isLocationEntered != null && isLocationEntered.equals("true")) {
		
		parentBean.setRowSelected(this, false);
		
	}				
	
	isLocationEntered = null;
	
}

   
}