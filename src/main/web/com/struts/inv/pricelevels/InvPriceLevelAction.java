package com.struts.inv.pricelevels;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.InvPriceLevelController;
import com.ejb.txn.InvPriceLevelControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModItemDetails;
import com.util.InvPriceLevelDetails;

public final class InvPriceLevelAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

	   public ActionForward execute(ActionMapping mapping,  ActionForm form,
	      HttpServletRequest request, HttpServletResponse response)
	      throws Exception {
	    
	   	HttpSession session = request.getSession();
	   	
	   	try {
	   		
/*******************************************************
 Check if user has a session
*******************************************************/

	   	         User user = (User) session.getAttribute(Constants.USER_KEY);

	   	         if (user != null) {

	   	            if (log.isInfoEnabled()) {

	   	                log.info("InvPriceLevelAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
	   	                "' performed this action on session " + session.getId());
	   	            }

	   	         } else {

	   	            if (log.isInfoEnabled()) {

	   	               log.info("User is not logged on in session" + session.getId());

	   	            }

	   	            return(mapping.findForward("adLogon"));

	   	         }
	   	         
	   	         InvPriceLevelForm actionForm = (InvPriceLevelForm)form;
	   	         
	   	         String frParam = Common.getUserPermission(user, Constants.INV_PRICE_LEVEL_ID);

	   	         if (request.getParameter("userPermission") != null && 
	   	         	request.getParameter("userPermission").equals(Constants.QUERY_ONLY)) {
	   	         	
	   	         	actionForm.setUserPermission(Constants.QUERY_ONLY);
	   	         	
	   	         } else {
	   	         	
	   	         	if (frParam != null) {
	   	         		
	   	         		if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	   	         			
	   	         			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	   	         			if (!fieldErrors.isEmpty()) {
	   	         				
	   	         				saveErrors(request, new ActionMessages(fieldErrors));
	   	         				
	   	         				return mapping.findForward("invPriceLevels");
	   	         			}
	   	         			
	   	         		}
	   	         		
	   	         		actionForm.setUserPermission(frParam.trim());
	   	         		
	   	         	} else {
	   	         		
	   	         		actionForm.setUserPermission(Constants.NO_ACCESS);
	   	         		
	   	         	}
	   	         	
	   	         }
	   	      
/*******************************************************
  Initialize InvPriceLevelController EJB
*******************************************************/
	   	         
	   	         InvPriceLevelControllerHome homePL = null;
	   	         InvPriceLevelController ejbPL = null;
	   	         
	   	         
	   	         try {
	   	         	
	   	         	homePL = (InvPriceLevelControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/InvPriceLevelControllerEJB", InvPriceLevelControllerHome.class);      
	   	         	
	   	         } catch (NamingException e) {
	   	         	
	   	         	if (log.isInfoEnabled()) {
	   	         		
	   	         		log.info("NamingException caught in InvPriceLevelAction.execute(): " + e.getMessage() +
	   	         				" session: " + session.getId());
	   	         	}
	   	         	
	   	         	return mapping.findForward("cmnErrorPage");
	   	         	
	   	         }
	   	         
	   	         try {
	   	         	
	   	         	ejbPL = homePL.create();
	   	         	
	   	         } catch (CreateException e) {
	   	         	
	   	         	if (log.isInfoEnabled()) {
	   	         		
	   	         		log.info("CreateException caught in InvPriceLevelAction.execute(): " + e.getMessage() +
	   	         				" session: " + session.getId());
	   	         		
	   	         	}
	   	         	return mapping.findForward("cmnErrorPage");
	   	         	
	   	         }

	          ActionErrors errors = new ActionErrors();
	          
/*******************************************************
   -- Call InvPriceLevelController EJB
	  getGlFcPrecisionUnit
*******************************************************/	          
	       			
	          short precisionUnit = 0;
	          
	          try {
	          	
	          	precisionUnit = ejbPL.getGlFcPrecisionUnit(user.getCmpCode());
	          	
	          } catch(EJBException ex) {
	          	
	          	if (log.isInfoEnabled()) {
	          		
	          		log.info("EJBException caught in InvPriceLevelAction.execute(): " + ex.getMessage() +
	          				" session: " + session.getId());
	          	}
	          	
	          	return(mapping.findForward("cmnErrorPage"));
	          	
	          }			
	       			   		
/*******************************************************
   -- Inv PL Save Action --
*******************************************************/
	                
            if (request.getParameter("saveButton") != null &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	          
            	ArrayList list = new ArrayList();
            	
            	for (int i=0; i<actionForm.getInvPLListSize(); i++){
            		
            		InvPriceLevelList invPLList = actionForm.getInvPLByIndex(i);
            		
            		InvPriceLevelDetails details = new InvPriceLevelDetails();
            		
            		details.setPlAdLvPriceLevel(invPLList.getPriceLevel());
            		details.setPlAmount(Common.convertStringMoneyToDouble(invPLList.getAmount(), precisionUnit));
            		details.setPlPercentMarkup(Common.convertStringMoneyToDouble(invPLList.getPercentMarkup(), (short)3));
            		details.setPlShippingCost(Common.convertStringMoneyToDouble(invPLList.getShippingCost(), precisionUnit));
            		
            		list.add(details);
            		
            	}
            	
            	try {
					
					ejbPL.saveInvPl(list, actionForm.getItemCode(), user.getCmpCode());
					
					double unitCost = Common.convertStringMoneyToDouble(actionForm.getUnitCost(), precisionUnit);
					double averageCost = Common.convertStringMoneyToDouble(actionForm.getAverageCost(), precisionUnit);
					double shippingCost = Common.convertStringMoneyToDouble(actionForm.getShippingCost(), precisionUnit);
					if (averageCost == 0) averageCost = unitCost;
					
					Object priceLevelList[] = actionForm.getInvPLList();
					for (int i = 0; i < priceLevelList.length - 1; i++) {
						
						InvPriceLevelList actionList = (InvPriceLevelList)actionForm.getInvPLByIndex(i);
						
						double salesPrice = Common.convertStringMoneyToDouble(actionList.getAmount(), precisionUnit);
												
						actionList.setGrossProfit(Common.convertDoubleToStringMoney(salesPrice - (averageCost + shippingCost), precisionUnit));
						
					}

            	} catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvPriceLevelAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());	               
	               
	               return(mapping.findForward("cmnErrorPage"));
	             
	               }
            	
            	}
            	
            	
            	return (mapping.findForward("invPriceLevels"));
				
/*******************************************************
 -- Inv PL Back Action --
 *******************************************************/

            } else if (request.getParameter("backButton") != null) {
            	
            	String path = null;
            	
            	if (actionForm.getTransaction().equals("ITEM_ENTRY")) {
            		path = "/invItemEntry.do?forward=1&iiCode=" +
					actionForm.getItemCode();
            	} else if (actionForm.getTransaction().equals("ASSEMBLY_ITEM_ENTRY")) {
            		path = "/invAssemblyItemEntry.do?forward=1&iiCode=" +
					actionForm.getItemCode();
            	} 
            	
            	return(new ActionForward(path));
            	
/*******************************************************
 -- Inv IE Close Action --
 *******************************************************/
            	
            } else if (request.getParameter("closeButton") != null) {
            	
            	return(mapping.findForward("cmnMain"));
            
            }
            
/*******************************************************
-- Load Action --
*******************************************************/
            if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("invPriceLevels"));
					
				}
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearInvPLList();
					
					if(request.getParameter("forward") != null) {
						
						actionForm.setTransaction(request.getParameter("transaction"));
						actionForm.setItemCode(new Integer(Integer.parseInt(request.getParameter("itemCode"))));
						actionForm.setItemDescription(request.getParameter("itemDescription"));
						
						InvModItemDetails mdetails = ejbPL.getInvIiMarkupValueByIiCode(actionForm.getItemCode(), user.getCmpCode());
						
						actionForm.setUnitCost(Common.convertDoubleToStringMoney(mdetails.getIiUnitCost(), precisionUnit));						
						actionForm.setAverageCost(Common.convertDoubleToStringMoney(mdetails.getIiAveCost(), precisionUnit));
						actionForm.setShippingCost(Common.convertDoubleToStringMoney(mdetails.getIiShippingCost(), precisionUnit));
						actionForm.setPercentMarkup(Common.convertDoubleToStringMoney(mdetails.getIiPercentMarkup(), (short)3));
						actionForm.setSalesPrice(Common.convertDoubleToStringMoney(mdetails.getIiSalesPrice(), precisionUnit));
						
						list = ejbPL.getInvPriceLevelsByIiCode(actionForm.getItemCode(), user.getCmpCode());
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							InvPriceLevelDetails details = (InvPriceLevelDetails)i.next(); 
							InvPriceLevelList invPLList = new InvPriceLevelList(actionForm, 
									actionForm.getItemCode(), details.getPlAdLvPriceLevel(),
									Common.convertDoubleToStringMoney(details.getPlAmount(), precisionUnit),
									//Common.convertDoubleToStringMoney(mdetails.getIiAveCost() != 0 ? details.getPlAmount() - (details.getPlShippingCost() + mdetails.getIiAveCost()) : details.getPlAmount() - (details.getPlShippingCost() + mdetails.getIiUnitCost()), precisionUnit),
									Common.convertDoubleToStringMoney(details.getPlAmount() - (details.getPlShippingCost() + mdetails.getIiUnitCost()), precisionUnit),
									Common.convertDoubleToStringMoney(details.getPlPercentMarkup(), (short)3),
									Common.convertDoubleToStringMoney(details.getPlShippingCost(), precisionUnit));

							actionForm.saveInvPLList(invPLList);
							
						}
						
					}
					
					this.setFormProperties(actionForm);  
				
				} catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvPriceLevelAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());	               
	               
	               return(mapping.findForward("cmnErrorPage"));
	             
	               }
	               
	            }
	           
				if (!errors.isEmpty()) {
				
				   saveErrors(request, new ActionMessages(errors));
				   
				} else {
				
				   if ((request.getParameter("saveButton") != null &&
				       actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {
				
				       actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
				
				   }
				}
				
				actionForm.reset(mapping, request);
				
				return(mapping.findForward("invPriceLevels"));                                 
												
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
	   		
	   	} catch(Exception e) {
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
	   		
	   		if (log.isInfoEnabled()) {
	   			
	   			log.info("Exception caught in InvPriceLevelAction.execute(): " + e.getMessage()
	   					+ " session: " + session.getId());
	   		}
	   		
	   		e.printStackTrace();
	   		return mapping.findForward("cmnErrorPage");
	   		
	   	}
	   	
	   }
	   
	   private void setFormProperties(InvPriceLevelForm actionForm) {
	   	
	   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	   		
	   		actionForm.setShowSaveButton(true);
	   		actionForm.setEnableFields(true);
	   		
	   	} else {
	   		
	   		actionForm.setShowSaveButton(false);
	   		actionForm.setEnableFields(false);
	   		
	   	}
	   	
	   }
	
}

	               
