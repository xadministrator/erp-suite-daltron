package com.struts.inv.pricelevels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvPriceLevelForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private ArrayList invPLList = new ArrayList();
	private Integer itemCode = null;
	private String transaction = null;
	private String unitCost = null;
	private String averageCost = null;
	private String shippingCost = null;
	private String percentMarkup = null;
	private String salesPrice = null;
		
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private boolean showSaveButton = false;
	private boolean enableFields = false;
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public InvPriceLevelList getInvPLByIndex(int index) {
		
		return((InvPriceLevelList)invPLList.get(index));
		
	}
	
	public Object[] getInvPLList() {
		
		return invPLList.toArray();
		
	}
	
	public int getInvPLListSize() {
		
		return invPLList.size();
		
	}
	
	public void saveInvPLList(Object newInvPLList) {
		
		invPLList.add(newInvPLList);
		
	}
	
	public void clearInvPLList() {
		
		invPLList.clear();
		
	}
	
	public Integer getItemCode() {
		
		return itemCode;
		
	}
	
	public void setItemCode(Integer itemCode) {
		
		this.itemCode = itemCode;
		
	}
	
	public String getTransaction() {
		
		return transaction;
		
	}
	
	public void setTransaction(String transaction) {
		
		this.transaction = transaction;
		
	}
		
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getPercentMarkup() {
		
		return percentMarkup;
		
	}
	
	public void setPercentMarkup(String percentMarkup) {
		
		this.percentMarkup = percentMarkup;
		
	}
	
	public String getAverageCost(){
		return averageCost;
	}
	
	public void setAverageCost(String averageCost){
		this.averageCost = averageCost;
	}
	
	public String getShippingCost(){
		return shippingCost;
	}
	
	public void setShippingCost(String shippingCost){
		this.shippingCost = shippingCost;
	}
	
	public String getSalesPrice(){
		return salesPrice;
	}
	
	public void setSalesPrice(String salesPrice){
		this.salesPrice = salesPrice;
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null) {
						
			Iterator i = invPLList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvPriceLevelList plList = (InvPriceLevelList)i.next();      	
				
				if(!Common.validateMoneyFormat(plList.getAmount())){
					errors.add("amount",
							new ActionMessage("priceLevels.error.amountInvalid"));
				}
				
				if(Common.validateRequired(plList.getAmount())){
					errors.add("amount",
							new ActionMessage("priceLevels.error.amountRequired"));
				}
				
				if(!Common.validateMoneyFormat(plList.getPercentMarkup())){
					errors.add("percentMarkup",
							new ActionMessage("priceLevels.error.percentMarkupInvalid"));
				}
				
				if(!Common.validateMoneyFormat(plList.getShippingCost())){
					errors.add("shippingCost",
							new ActionMessage("priceLevels.error.shippingCostInvalid"));
				}
				
			}
		}
		
		return errors;
		
	}
	
	
}