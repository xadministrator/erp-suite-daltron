package com.struts.inv.pricelevels;

import java.io.Serializable;

public class InvPriceLevelList implements Serializable {

	private Integer itemCode;
	private String priceLevel;
	private String amount;
	private String grossProfit;
	private String percentMarkup;
	private String shippingCost;
	
	private InvPriceLevelForm parentBean;
	
	public InvPriceLevelList(InvPriceLevelForm parentBean, Integer itemCode, String priceLevel, String amount, String grossProfit, String percentMarkup, String shippingCost) {
		
		this.parentBean = parentBean;
		this.itemCode = itemCode;
		this.priceLevel = priceLevel;
		this.amount = amount;
		this.percentMarkup = percentMarkup;
		this.shippingCost = shippingCost;
		this.grossProfit = grossProfit;
		
	}
	
	public Integer getItemCode(){
		
		return itemCode;
		
	}
	
	public String getPriceLevel(){
		
		return priceLevel;
		
	}
	
	public void setPriceLevel(String priceLevel) {
		
		this.priceLevel = priceLevel;
		
	}
	
	public String getAmount(){
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public String getGrossProfit(){
		
		return grossProfit;
		
	}
	
	public void setGrossProfit(String grossProfit) {
		
		this.grossProfit = grossProfit;
		
	}	
	
	public String getPercentMarkup(){
		
		return percentMarkup;
		
	}
	
	public void setPercentMarkup(String percentMarkup) {
		
		this.percentMarkup = percentMarkup;
		
	}
	
	public String getShippingCost(){
		
		return shippingCost;
		
	}
	
	public void setShippingCost(String shippingCost) {
		
		this.shippingCost = shippingCost;
		
	}
	
}