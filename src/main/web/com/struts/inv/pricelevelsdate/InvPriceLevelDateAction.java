
package com.struts.inv.pricelevelsdate;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.InvPriceLevelDateController;
import com.ejb.txn.InvPriceLevelDateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ArCustomerDetails;
import com.util.ArModStandardMemoLineClassDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.InvItemDetails;
import com.util.InvModItemDetails;
import com.util.InvModPriceLevelDateDetails;
import com.util.InvPriceLevelDateDetails;

import java.util.ArrayList;
import java.util.Iterator;
import javax.ejb.FinderException;

public class InvPriceLevelDateAction extends Action{
    
     private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

     
    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
        HttpSession session = request.getSession();
        
        try{
            User user = (User) session.getAttribute(Constants.USER_KEY);
         
            if (user != null) {
                if (log.isInfoEnabled()) {

                    log.info("InvPriceLevelDateAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                       "' performed this action on session " + session.getId());
                }   
            }else{
         	
                if (log.isInfoEnabled()) {
            	
                    log.info("User is not logged on in session" + session.getId());
                }
                return(mapping.findForward("adLogon"));
            
            }
            
            InvPriceLevelDateForm actionForm = (InvPriceLevelDateForm)form;
            
            String frParam = Common.getUserPermission(user, Constants.INV_PRICE_LEVEL_ID);
			
            if (frParam != null) {

                if (frParam.trim().equals(Constants.FULL_ACCESS)) {

                    ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

                    if (!fieldErrors.isEmpty()) {

                        saveErrors(request, new ActionMessages(fieldErrors));

                        return mapping.findForward("invPriceLevelDate");
                    }

                }

                actionForm.setUserPermission(frParam.trim());
                
                
                

            } else {

                actionForm.setUserPermission(Constants.NO_ACCESS);

            }
           
/*******************************************************
Initialize InvPriceLevelDateController EJB
*******************************************************/
            
            InvPriceLevelDateControllerHome homePLD = null;
            InvPriceLevelDateController ejbPLD = null;

            try {

                homePLD = (InvPriceLevelDateControllerHome)com.util.EJBHomeFactory.
                   lookUpHome("ejb/InvPriceLevelDateControllerEJB", InvPriceLevelDateControllerHome.class);

            } catch(NamingException e) {
                if(log.isInfoEnabled()){
                   log.info("NamingException caught in InvPriceLevelDateAction.execute(): " + e.getMessage() +
                  " session: " + session.getId());
                }
               return(mapping.findForward("cmnErrorPage"));
            }

            try {

                ejbPLD = homePLD.create();

            } catch(CreateException e) {

                if(log.isInfoEnabled()) {

                    log.info("CreateException caught in InvPriceLevelDateAction.execute(): " + e.getMessage() +
                      " session: " + session.getId());
                }

                return(mapping.findForward("cmnErrorPage"));

            }

            ActionErrors errors = new ActionErrors();
            ActionMessages messages = new ActionMessages();
            
            short smcLineNumber = 3;
   
   	          boolean initializeSave = false;
/*******************************************************
   -- Call InvPriceLevelController EJB
	  getGlFcPrecisionUnit
*******************************************************/	          
	       			
	          short precisionUnit = 0;
	          
	          try {
	          	
	          	precisionUnit = ejbPLD.getGlFcPrecisionUnit(user.getCmpCode());
	          	
	          } catch(EJBException ex) {
	          	
	          	if (log.isInfoEnabled()) {
	          		
	          		log.info("EJBException caught in InvPriceLevelAction.execute(): " + ex.getMessage() +
	          				" session: " + session.getId());
	          	}
	          	
	          	return(mapping.findForward("cmnErrorPage"));
	          	
	          }			         
        
/*******************************************************
-- Ar PLD Save Price Level Date Class --
*******************************************************/
	
	    if (request.getParameter("saveButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	       
                boolean isError = false;

                ArrayList list = new ArrayList();
                ArrayList duplicateCheckList = new ArrayList();
                Integer II_CODE = actionForm.getItemCode();
                try{
                    for(int x=0;x<actionForm.getInvPLDListSize();x++){
               
                        InvPriceLevelDateList arSmcList = (InvPriceLevelDateList)actionForm.getInvPLDByIndex(x);
            
                     
                        if(arSmcList.getDescription() == Constants.GLOBAL_BLANK){
                        	System.out.println("desc found");
                            continue;
                        }
                     
                     
 
                        if(!arSmcList.getPriceLevel().equals("") && arSmcList.getPriceLevel()!=null){
                      
                            InvModPriceLevelDateDetails details = new InvModPriceLevelDateDetails();

                            //new lines check if already exist
                          
							details.setIiCode(II_CODE);
							details.setPdCode(Common.convertStringToInteger(arSmcList.getPdCode()));
							details.setPdDesc(arSmcList.getDescription());
							details.setPdAmount(Common.convertStringMoneyToDouble(arSmcList.getAmount(), precisionUnit));
							details.setPdAdLvPriceLevel(arSmcList.getPriceLevel());
							details.setPdMargin(Common.convertStringMoneyToDouble(arSmcList.getGrossProfit(), precisionUnit));
							details.setPdPercentMarkup(Common.convertStringMoneyToDouble(arSmcList.getPercentMarkup(), precisionUnit));
							details.setPdShippingCost(Common.convertStringMoneyToDouble(arSmcList.getShippingCost(), precisionUnit));
							details.setPdDateFrom(Common.convertStringToSQLDate(arSmcList.getDateFrom()));
							details.setPdDateTo(Common.convertStringToSQLDate(arSmcList.getDateTo()));
							details.setPdStatus(arSmcList.getStatus());


                            list.add(details);
                        }
                        
                        
                     

                    }

                  
                    if(list.size()<=0){
                        isError = true;
                        
                        //no input detected
                        
                        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("priceLevelsDate.prompt.noPriceLevelToSave"));

                    }else{
                        if(!isError){
                      
                            ejbPLD.saveInvPdEntry(list,  user.getCmpCode()); 
                           
                            actionForm.clearInvPLDList();
                            int lineNumber = 0;
                      
                            ArrayList pdlList = ejbPLD.getInvPriceLevelsDateByIiCode(II_CODE, user.getCmpCode());

                            for(int x=0;x<pdlList.size();x++){


								InvPriceLevelDateDetails details = (InvPriceLevelDateDetails)pdlList.get(x);
                               
                                InvPriceLevelDateList invPdlList = new InvPriceLevelDateList(
                                actionForm, 
                                Common.convertIntegerToString(details.getPdCode()),
                                details.getPdAdLvPriceLevel(),
                                details.getPdDesc(),
                                Common.convertDoubleToStringMoney(details.getPdAmount(),precisionUnit),
                                Common.convertDoubleToStringMoney(details.getPdPercentMarkup(),precisionUnit),
                                Common.convertDoubleToStringMoney(details.getPdShippingCost(),precisionUnit),
                                Common.convertDoubleToStringMoney(details.getPdMargin(),precisionUnit),
                                Common.convertSQLDateToString(details.getPdDateFrom()),       
                                Common.convertSQLDateToString(details.getPdDateTo()),    
                                details.getPdStatus()                   
                             
                          		);

                                actionForm.saveInvPLDList(invPdlList);
                            }


                            actionForm.setShowSaveButton(true);
                            actionForm.setShowAddLinesButton(true);
                        
                        
                        
                        
                        }

                    }
                    
              	}catch(GlobalNoRecordFoundException ex){
                	
					
                       errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("priceLevelsDate.error.descriptionNameAlreadyExist", ex.getMessage()));

              	   
                  
                    
                    
                }catch(Exception ex){
                    if (log.isInfoEnabled()) {

                        log.info("EJBException caught in InvPriceLevelDateAction.execute(): " + ex.getMessage() +
                                " session: " + session.getId());
                        return mapping.findForward("cmnErrorPage"); 

                     }
                }

                
                if (!errors.isEmpty()) {
			
                    System.out.println("error");
                    saveErrors(request, new ActionMessages(errors));
                    return mapping.findForward("invPriceLevelDate");

                }
                
                
			//	actionForm.setItemCode(new Integer(request.getParameter("itemCode")));
				
		//		actionForm.setItemCode(actionForm.getItemCode());
                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
       			 initializeSave =true;
               // return(mapping.findForward("invPriceLevelDate"));    
    
            }
            
            /*******************************************************
            -- Ar PDL Close Action --
            *******************************************************/
				
            else if (request.getParameter("closeButton") != null) {

                   return(mapping.findForward("cmnMain"));
            
            }
            /*******************************************************
            -- Ar PDL Add Line Button --
            *******************************************************/
            else if(request.getParameter("addLinesButton") != null  && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
               
                System.out.println("Add Line Button");
                int listSize = actionForm.getInvPLDListSize();
                int lineNumber = 0;
                
                for (int x = listSize + 1; x <= listSize + smcLineNumber; x++) {
                 
                    InvPriceLevelDateList invPdlList = new InvPriceLevelDateList( actionForm, 
                                null,
                                null,
                               null,
                               "0",
                                "0",
                               "0",
                               "0",
                                null,     
                                null, 
                                Constants.GLOBAL_BLANK );
                    
      
       
                    invPdlList.setPriceLevelList(actionForm.getPriceLevelList());
                    
                    invPdlList.setStatusList(actionForm.getStatusList());
                    //actionForm, null,String.valueOf(++lineNumber), null, null, "0", null);	       

               
                    actionForm.saveInvPLDList(invPdlList);
                 
                }	
                    
             
      
                return(mapping.findForward("invPriceLevelDate"));
            }
            /*******************************************************
            -- Ar PDL Delete Line --
            *******************************************************/
            else if (request.getParameter("invPLDList[" + 
					actionForm.getRowSelected() + "].deleteButton") != null ) {
                System.out.println("delete button");
                
            
                InvPriceLevelDateList invPdlList = actionForm.getInvPLDByIndex(actionForm.getRowSelected());
                actionForm.getInvPLDByIndex(actionForm.getRowSelected());

            
            
            	if(invPdlList.getPdCode() ==null){
            		actionForm.deleteInvPLDList(actionForm.getRowSelected());
            		System.out.println("delete not listed line");
            		
            	}else{
            	
            		ejbPLD.deleteInvPd(Common.convertStringToInt(invPdlList.getPdCode()));
            		actionForm.deleteInvPLDList(actionForm.getRowSelected());
            		System.out.println("delete  listed line");
            		
            		
            	}
            
            
            
            
                
                if (!errors.isEmpty()) {
			
                    System.out.println("error");
                    saveErrors(request, new ActionMessages(errors));
                    return mapping.findForward("invPriceLevelDate");

                }
                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
        
         
                return(mapping.findForward("invPriceLevelDate"));
            }
          
          
           
           
            
           
            if (frParam != null) {
               

                ArrayList list = null;
                Iterator i = null;
                
                
                //get pricelevels list
                actionForm.clearPriceLevelList();
                
              
                list = ejbPLD.getInvAdPriceLevels(user.getCmpCode());
                
               	list.add(0, Constants.GLOBAL_BLANK);
                
                actionForm.setPriceLevelList(list);
                
                
                
              

                
                
                if (request.getParameter("forward") != null || initializeSave) {
                
                	if(request.getParameter("itemCode")!=null){
                		actionForm.setItemCode(new Integer(request.getParameter("itemCode")));
                	
                	}
                	
                	
                	InvModItemDetails details = ejbPLD.getInvIiMarkupValueByIiCode(actionForm.getItemCode(), user.getCmpCode());
                	
                	actionForm.setItemName(details.getIiName());
                	actionForm.setItemDescription(details.getIiDescription());
                	
                	
                	ArrayList priceLevelDates = ejbPLD.getInvPriceLevelsDateByIiCode(actionForm.getItemCode(), user.getCmpCode());
                	
                	
                	actionForm.clearInvPLDList();
                	
                	
                	
                	for(int x=0;x<priceLevelDates.size();x++){
                		
                		InvPriceLevelDateDetails mdetails = (InvPriceLevelDateDetails)priceLevelDates.get(x);
                		
                		
                		
                		InvPriceLevelDateList pldList = new InvPriceLevelDateList(
	                		actionForm, 
	                		Integer.toString(mdetails.getPdCode()),  
	                		mdetails.getPdAdLvPriceLevel(), 
	                		mdetails.getPdDesc(), 
	                		Common.convertDoubleToStringMoney(mdetails.getPdAmount(), precisionUnit),
	                		Common.convertDoubleToStringMoney(mdetails.getPdMargin(), precisionUnit),
	                		Common.convertDoubleToStringMoney(mdetails.getPdPercentMarkup(), precisionUnit),
	                		Common.convertDoubleToStringMoney(mdetails.getPdShippingCost(), precisionUnit),
	                		Common.convertSQLDateToString(mdetails.getPdDateFrom()), 
	                		Common.convertSQLDateToString(mdetails.getPdDateTo()),  
							mdetails.getPdStatus());
                		
                		
                		
                		
                		pldList.setPriceLevelList(actionForm.getPriceLevelList());
                		pldList.setPriceLevel(mdetails.getPdAdLvPriceLevel());
                		
                		
                		pldList.setStatusList(actionForm.getStatusList());
                		pldList.setStatus(mdetails.getPdStatus());
                		
                		
                		actionForm.saveInvPLDList(pldList);
                		
                		
                	
                	}
                	
                	                
                
                }
               
                

                
                
                return(mapping.findForward("invPriceLevelDate"));
                
            } else {
				
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return(mapping.findForward("cmnMain"));

            }
            
            
            
           
        }catch(Exception e) {

            /*******************************************************
               System Failed: Forward to error page 
            *******************************************************/
 
            if (log.isInfoEnabled()) {

                log.info("Exception caught in ArInvoiceEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
            }
         
            e.printStackTrace();
            return(mapping.findForward("cmnErrorPage"));
         
        }
        
     
    
    }

}
