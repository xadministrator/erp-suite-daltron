package com.struts.inv.pricelevelsdate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvPriceLevelDateForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	
	private Integer itemCode = null;
	
	private String unitCost = null;
	private String averageCost = null;
	private String shippingCost = null;
	private String percentMarkup = null;
	private String salesPrice = null;

		
	private String userPermission = new String();
	private String txnStatus = new String();
	
	
	private boolean showSaveButton = false;

	private boolean enableFields = false;
	
	
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	
	private ArrayList invPLDList = new ArrayList();
	
	private ArrayList plList = new ArrayList();
	
	

	
	private ArrayList statusList = new ArrayList();

	private int rowSelected = 0;
	
    private String isPriceLevelDateEntered = null;
	
	private ArrayList selectedPLDList = new ArrayList();
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public InvPriceLevelDateList getInvPLDByIndex(int index) {
		
		return((InvPriceLevelDateList)invPLDList.get(index));
		
	}
	
	public Object[] getInvPLDList() {
		
		return invPLDList.toArray();
		
	}
	
	public int getInvPLDListSize() {
		
		return invPLDList.size();
		
	}
		
	public void saveInvPLDList(Object newInvPLDList) {
		
		invPLDList.add(newInvPLDList);
		
	}
	
	public void clearInvPLDList() {
		
		invPLDList.clear();
		
	}
	
	public Integer getItemCode() {
		
		return itemCode;
		
	}
	
	public void setItemCode(Integer itemCode) {
		
		this.itemCode = itemCode;
		
	}
	


		
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	

	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	
	public boolean getShowAddLinesButton() {
		
		return showAddLinesButton;
		
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		
		this.showAddLinesButton = showAddLinesButton;
		
	}
	
	public boolean getShowDeleteLinesButton() {
		
		return showDeleteLinesButton;
		
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		
		this.showDeleteLinesButton = showDeleteLinesButton;
		
	}
	
	
	public int getRowSelected(){
		return rowSelected;
	}

	
	public void setRowSelected(Object selectedInvPLDList, boolean isEdit){
		this.rowSelected = invPLDList.indexOf(selectedInvPLDList);
	}

	public void updateInvPLDRow(int rowSelected, Object newInvPLDList){
		invPLDList.set(rowSelected, newInvPLDList);
	}

	public void deleteInvPLDList(int rowSelected){
		invPLDList.remove(rowSelected);
	}


   public String getIsPriceLevelDateEntered(){
        return isPriceLevelDateEntered;
    }
    
    public void setIsCustomerClassEntered(String isPriceLevelDateEntered){
        this.isPriceLevelDateEntered = isPriceLevelDateEntered;
    }

	public void clearPriceLevelList(){
		plList.clear();
	}

	public ArrayList getPriceLevelList(){
	
		return plList;
	}

	public void setPriceLevelList(ArrayList plList){
	
		this.plList = plList;
	}


	public ArrayList getStatusList(){
		return statusList;
	}

	public void setStatusList(ArrayList statusList){
	
		this.statusList = statusList;
	}
	
	
		public String getUnitCost() {
		
		return unitCost;
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getPercentMarkup() {
		
		return percentMarkup;
		
	}
	
	public void setPercentMarkup(String percentMarkup) {
		
		this.percentMarkup = percentMarkup;
		
	}
	
	public String getAverageCost(){
		return averageCost;
	}
	
	public void setAverageCost(String averageCost){
		this.averageCost = averageCost;
	}
	
	public String getShippingCost(){
		return shippingCost;
	}
	
	public void setShippingCost(String shippingCost){
		this.shippingCost = shippingCost;
	}
	
	public String getSalesPrice(){
		return salesPrice;
	}
	
	public void setSalesPrice(String salesPrice){
		this.salesPrice = salesPrice;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		statusList.clear();
	
		statusList.add("ENABLE");
		statusList.add("DISABLE");
		
		
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null) {
						
						
			int index = 1;			
			Iterator i = invPLDList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvPriceLevelDateList plList = (InvPriceLevelDateList)i.next();      	
				
				
				if(plList.getPdCode()== Constants.GLOBAL_BLANK && !Common.validateRequired(plList.getDescription())){
					
					if(!Common.validateMoneyFormat(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevelsDate.error.amountInvalid",index));
					}
					
					if(Common.validateRequired(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevelsDate.error.amountRequired",index));
					}
					
					if(!Common.validateMoneyFormat(plList.getPercentMarkup())){
						errors.add("percentMarkup",
								new ActionMessage("priceLevelsDate.error.percentMarkupInvalid",index));
					}
					
					if(!Common.validateMoneyFormat(plList.getShippingCost())){
						errors.add("shippingCost",
								new ActionMessage("priceLevelsDate.error.shippingCostInvalid",index));
					}
					
					
					boolean dateFromPass = true;
					boolean dateToPass = true;
					
					if(Common.validateRequired(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.prompt.dateRequired",index));
						dateFromPass = false;
					}
	
					
					if(!Common.validateDateFormat(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid",index));
						dateFromPass = false;
					}
					
					
					if(Common.validateRequired(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateRequired",index));
						dateToPass = false;
					}
					
					
					if(!Common.validateDateFormat(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid",index));
						dateToPass = false;
					}
					
					
					if(dateFromPass && dateToPass){
						
						Date _dateFrom = Common.convertStringToSQLDate(plList.getDateFrom());
						Date _dateTo = Common.convertStringToSQLDate(plList.getDateTo());
						System.out.println("date from" + _dateFrom);
						System.out.println("date to" + _dateTo);
						
						
						
						 if(_dateFrom.after(_dateTo)){
                			errors.add("dates",
								new ActionMessage("priceLevelsDate.error.dateFromMustBeforeDateTo",index));
           				 }

					}
					
				
				
				
				}
				
				
				
				
			
				if(plList.getPdCode()!= Constants.GLOBAL_BLANK && !Common.validateRequired(plList.getDescription())){
				
				
				
					if(!Common.validateMoneyFormat(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevelsDate.error.amountInvalid",index));
					}
					
					if(Common.validateRequired(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevelsDate.error.amountRequired",index));
					}
					
					if(!Common.validateMoneyFormat(plList.getPercentMarkup())){
						errors.add("percentMarkup",
								new ActionMessage("priceLevelsDate.error.percentMarkupInvalid",index));
					}
					
					if(!Common.validateMoneyFormat(plList.getShippingCost())){
						errors.add("shippingCost",
								new ActionMessage("priceLevelsDate.error.shippingCostInvalid",index));
					}
					
					boolean dateFromPass = true;
					boolean dateToPass = true;
					
					if(Common.validateRequired(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.prompt.dateRequired",index));
						dateFromPass = false;
					}
	
					
					if(!Common.validateDateFormat(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid",index));
						dateFromPass = false;
					}
					
					
					if(Common.validateRequired(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateRequired",index));
						dateToPass = false;
					}
					
					
					if(!Common.validateDateFormat(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid",index));
						dateToPass = false;
					}
					
					
					
					
					if(dateFromPass && dateToPass){
						
						Date _dateFrom = Common.convertStringToSQLDate(plList.getDateFrom());
						Date _dateTo = Common.convertStringToSQLDate(plList.getDateTo());
						System.out.println("date from" + _dateFrom);
						System.out.println("date to" + _dateTo);
						
						
						
						 if(_dateFrom.after(_dateTo)){
                			errors.add("dates",
								new ActionMessage("priceLevelsDate.error.dateFromMustBeforeDateTo",index));
           				 }

					}
				}
				/*
				else{
				
					
					if(!Common.validateMoneyFormat(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevels.error.amountInvalid"));
					}
					
					if(Common.validateRequired(plList.getAmount())){
						errors.add("amount",
								new ActionMessage("priceLevels.error.amountRequired"));
					}
					
					if(!Common.validateMoneyFormat(plList.getPercentMarkup())){
						errors.add("percentMarkup",
								new ActionMessage("priceLevels.error.percentMarkupInvalid"));
					}
					
					if(!Common.validateMoneyFormat(plList.getShippingCost())){
						errors.add("shippingCost",
								new ActionMessage("priceLevels.error.shippingCostInvalid"));
					}
					
					if(Common.validateRequired(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.prompt.dateRequired"));
					}
	
					
					if(!Common.validateDateFormat(plList.getDateFrom())){
						errors.add("dateFrom",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid"));
					}
					
					
					if(Common.validateRequired(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateRequired"));
					}
					
					
					if(!Common.validateDateFormat(plList.getDateTo())){
						errors.add("dateTo",
								new ActionMessage("priceLevelsDate.error.dateFormatInvalid"));
					}
				
				}
				*/
			
				
				index++;
			}
		}
		
		return errors;
		
	}
	
	
}