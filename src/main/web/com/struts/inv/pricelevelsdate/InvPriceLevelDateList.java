package com.struts.inv.pricelevelsdate;

import java.io.Serializable;
import java.util.ArrayList;

public class InvPriceLevelDateList implements Serializable {



	private String pdCode;

	private ArrayList priceLevelList = new ArrayList();
	
	private String priceLevel;
	private String description;
	private String amount;
	private String grossProfit;
	private String percentMarkup;
	private String shippingCost;
	private String dateFrom;
	private String dateTo;
	private String status;
	private ArrayList statusList = new ArrayList();
	
	
	private String buttonDelete = null;
	
	
	
	private InvPriceLevelDateForm parentBean;
	
	public InvPriceLevelDateList(InvPriceLevelDateForm parentBean, String pdCode,  String priceLevel, String description, String amount, String grossProfit, String percentMarkup, String shippingCost, String dateFrom, String dateTo , String status) {
		this.parentBean = parentBean;
		this.pdCode = pdCode;
		this.priceLevel = priceLevel;
		this.description = description;
		this.amount = amount;
		this.percentMarkup = percentMarkup;
		this.shippingCost = shippingCost;
		this.grossProfit = grossProfit;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.status = status;
		
	}
	
	
	public String getPdCode(){
		
		return pdCode;
		
	}
	
	public void setPdCode(String pdCode){
		
		this.pdCode =  pdCode;
		
	}
	

	
	public ArrayList getPriceLevelList() {

		return priceLevelList;

	}

	public void setPriceLevelList(ArrayList priceLevelList) {

		this.priceLevelList = priceLevelList;

	}
	
	
	
	
	public String getPriceLevel(){
		
		return priceLevel;
		
	}
	
	public void setPriceLevel(String priceLevel) {
		
		this.priceLevel = priceLevel;
		
	}
	
	
		
	public String getDescription(){
		
		return description;
		
	}
	
	public void setDescription(String description){
		
		this.description = description;
		
	}
	
	public String getAmount(){
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public String getGrossProfit(){
		
		return grossProfit;
		
	}
	
	public void setGrossProfit(String grossProfit) {
		
		this.grossProfit = grossProfit;
		
	}	
	
	public String getPercentMarkup(){
		
		return percentMarkup;
		
	}
	
	public void setPercentMarkup(String percentMarkup) {
		
		this.percentMarkup = percentMarkup;
		
	}
	
	public String getShippingCost(){
		
		return shippingCost;
		
	}
	
	public void setShippingCost(String shippingCost) {
		
		this.shippingCost = shippingCost;
		
	}
	
	public String getDateFrom(){
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getStatus() {
		
		return status;
		
	}
	
	
	public void setStatus(String status) {
		
		this.status = status;
		
	}
	
	
	public ArrayList getStatusList() {

		return statusList;

	}

	public void setStatusList(ArrayList statusList) {

		this.statusList = statusList;

	}
	
	
		public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	
}