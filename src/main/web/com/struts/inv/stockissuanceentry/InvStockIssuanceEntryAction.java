package com.struts.inv.stockissuanceentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.ejb.txn.InvStockIssuanceEntryController;
import com.ejb.txn.InvStockIssuanceEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModBuildOrderStockDetails;
import com.util.InvModStockIssuanceDetails;
import com.util.InvModStockIssuanceLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvStockIssuanceDetails;

public final class InvStockIssuanceEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvStockIssuanceEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvStockIssuanceEntryForm actionForm = (InvStockIssuanceEntryForm)form;
         
         // reset report
         
         actionForm.setReport(null);
         
         String frParam = Common.getUserPermission(user, Constants.INV_STOCK_ISSUANCE_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invStockIssuanceEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize invStockIssuanceEntryController EJB
*******************************************************/

         InvStockIssuanceEntryControllerHome homeSI = null;
         InvStockIssuanceEntryController ejbSI = null;

    
         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;
         try {
            
            homeSI = (InvStockIssuanceEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvStockIssuanceEntryControllerEJB", InvStockIssuanceEntryControllerHome.class);
                
                homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvStockIssuanceEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbSI = homeSI.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in InvStockIssuanceEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call InvStockIssuanceEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         short precisionUnit =0;
         short quantityPrecisionUnit = 0;
         boolean isInitialPrinting = false;
         
         try {
         	
            quantityPrecisionUnit = ejbSI.getInvGpQuantityPrecisionUnit(user.getCmpCode());       
            precisionUnit = ejbSI.getGlFcPrecisionUnit(user.getCmpCode());
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Inv SI Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           InvStockIssuanceDetails details = new InvStockIssuanceDetails();
           
           details.setSiCode(actionForm.getStockIssuanceCode());
           details.setSiReferenceNumber(actionForm.getReferenceNumber());
           details.setSiDocumentNumber(actionForm.getDocumentNumber());
           details.setSiDescription(actionForm.getDescription());
           details.setSiDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setSiVoid(Common.convertBooleanToByte(actionForm.getVoidCheckbox()));
           details.setSiCreatedBy(actionForm.getCreatedBy());
           details.setSiDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
           details.setSiLastModifiedBy(actionForm.getLastModifiedBy());
           details.setSiDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                      
           if (actionForm.getStockIssuanceCode() == null) {
           	
           		details.setSiCreatedBy(user.getUserName());
           		details.setSiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
                      
           details.setSiLastModifiedBy(user.getUserName());
           details.setSiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
           
           ArrayList silList = new ArrayList(); 
          
           for (int i = 0; i<actionForm.getInvSIListSize(); i++) {
           	
           	   InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(i);           	   
           	              	             	   
	           if (!invSIList.getIssueCheckbox()) continue;
           	   
           	   InvModStockIssuanceLineDetails mdetails = new InvModStockIssuanceLineDetails();
           	   
          	   mdetails.setSilBosCode(invSIList.getBosCode());
           	   mdetails.setSilIlLocationName(invSIList.getLocation());
           	   mdetails.setSilIlIiName(invSIList.getItemName());
           	   mdetails.setSilIssueQuantity(Common.convertStringMoneyToDouble(invSIList.getIssueQuantity(), quantityPrecisionUnit));
           	   mdetails.setSilIlIiUomName(invSIList.getUnit());
           	   mdetails.setSilUnitCost(Common.convertStringMoneyToDouble(invSIList.getUnitCost(), precisionUnit));
           	   
           	   silList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    ejbSI.saveInvSiEntry(details, silList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));
           	                    	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPosted"));
                              	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.documentNumberNotUnique"));
           
           }catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.effectiveDatePeriodClosed"));
           	   
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	      errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.journalNotBalance"));
           	      
           } catch (GlobalInventoryDateException ex) {
              	
	          	  errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("invStockIssuanceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
			
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invStockIssuanceEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invStockIssuanceEntry.error.noNegativeInventoryCostingCOA"));

           }  catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Inv SI Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
         	InvStockIssuanceDetails details = new InvStockIssuanceDetails();
            
         	details.setSiCode(actionForm.getStockIssuanceCode());
            details.setSiReferenceNumber(actionForm.getReferenceNumber());
            details.setSiDocumentNumber(actionForm.getDocumentNumber());
            details.setSiDescription(actionForm.getDescription());
            details.setSiDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setSiVoid(Common.convertBooleanToByte(actionForm.getVoidCheckbox()));
            details.setSiCreatedBy(actionForm.getCreatedBy());
            details.setSiDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
            details.setSiLastModifiedBy(actionForm.getLastModifiedBy());
            details.setSiDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                       
            if (actionForm.getStockIssuanceCode() == null) {
            	
            		details.setSiCreatedBy(user.getUserName());
            		details.setSiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
 	                      
            }
                       
            details.setSiLastModifiedBy(user.getUserName());
            details.setSiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
            
            ArrayList silList = new ArrayList();
                                  
            for (int i = 0; i<actionForm.getInvSIListSize(); i++) {
            	
            	InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(i);
            	              	             	   
            	if (!invSIList.getIssueCheckbox()) continue;
            	   
            	   InvModStockIssuanceLineDetails mdetails = new InvModStockIssuanceLineDetails();
            	   
            	   mdetails.setSilBosCode(invSIList.getBosCode());
               	   mdetails.setSilIlLocationName(invSIList.getLocation());
               	   mdetails.setSilIlIiName(invSIList.getItemName());
               	   mdetails.setSilIssueQuantity(Common.convertStringMoneyToDouble(invSIList.getIssueQuantity(), quantityPrecisionUnit));
               	   mdetails.setSilIlIiUomName(invSIList.getUnit());
               	   mdetails.setSilUnitCost(Common.convertStringMoneyToDouble(invSIList.getUnitCost(), precisionUnit));
            	   
            	   silList.add(mdetails);
            	
            }                     	
            
            try {
            	
            	    Integer stockIssuanceCode = ejbSI.saveInvSiEntry(details, silList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    actionForm.setStockIssuanceCode(stockIssuanceCode);
            	
            } catch (GlobalRecordAlreadyDeletedException ex) {
            	
            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));                          	                 	                   
                      	
            } catch (GlobalTransactionAlreadyApprovedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyApproved"));
            	
            } catch (GlobalTransactionAlreadyPendingException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPending"));
            	
            } catch (GlobalTransactionAlreadyPostedException ex) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPosted"));
            	   
            } catch (GlobalInvItemLocationNotFoundException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invStockIssuanceEntry.error.noItemLocationFound", ex.getMessage()));
               	   
            } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.effectiveDateNoPeriodExist"));
           	   	   
            } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invStockIssuanceEntry.error.documentNumberNotUnique"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	      errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.effectiveDatePeriodClosed"));
           	      
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	      errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.journalNotBalance"));
           	      
           } catch (GlobalInventoryDateException ex) {
              	
          		  errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invStockIssuanceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {
			
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invStockIssuanceEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invStockIssuanceEntry.error.noNegativeInventoryCostingCOA"));

           }  catch (EJBException ex) {
            	    if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                 }
                
                return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Inv SI Journal Action --
*******************************************************/
      	
      } else if (request.getParameter("journalButton") != null &&
      		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	      	
      	
      		if(Common.validateRequired(actionForm.getApprovalStatus())) {
      			
      		
		      	InvStockIssuanceDetails details = new InvStockIssuanceDetails();
		        
		     	details.setSiCode(actionForm.getStockIssuanceCode());
		        details.setSiReferenceNumber(actionForm.getReferenceNumber());
		        details.setSiDocumentNumber(actionForm.getDocumentNumber());
		        details.setSiDescription(actionForm.getDescription());
		        details.setSiDate(Common.convertStringToSQLDate(actionForm.getDate()));
		        details.setSiVoid(Common.convertBooleanToByte(actionForm.getVoidCheckbox()));
		        details.setSiCreatedBy(actionForm.getCreatedBy());
		        details.setSiDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
		        details.setSiLastModifiedBy(actionForm.getLastModifiedBy());
		        details.setSiDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
		                   
		        if (actionForm.getStockIssuanceCode() == null) {
		        	
		        		details.setSiCreatedBy(user.getUserName());
		        		details.setSiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
			                      
		        }
		                   
		        details.setSiLastModifiedBy(user.getUserName());
		        details.setSiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
		        
		        ArrayList silList = new ArrayList();
		                              
		        for (int i = 0; i<actionForm.getInvSIListSize(); i++) {
		        	
		        	InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(i);
		        	              	             	   
		        	if (!invSIList.getIssueCheckbox()) continue;
		        	   
		        	   InvModStockIssuanceLineDetails mdetails = new InvModStockIssuanceLineDetails();
		        	   
		        	   mdetails.setSilBosCode(invSIList.getBosCode());
		           	   mdetails.setSilIlLocationName(invSIList.getLocation());
		           	   mdetails.setSilIlIiName(invSIList.getItemName());
		           	   mdetails.setSilIssueQuantity(Common.convertStringMoneyToDouble(invSIList.getIssueQuantity(), quantityPrecisionUnit));
		           	   mdetails.setSilIlIiUomName(invSIList.getUnit());
		           	   mdetails.setSilUnitCost(Common.convertStringMoneyToDouble(invSIList.getUnitCost(), precisionUnit));
		           	   
		        	   silList.add(mdetails);
		        	
		        }                     	
		        
		        try {
		        	
		        	    Integer stockIssuanceCode = ejbSI.saveInvSiEntry(details, silList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		        	    actionForm.setStockIssuanceCode(stockIssuanceCode);
		        	
		        } catch (GlobalRecordAlreadyDeletedException ex) {
		        	
		        	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                 new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));
		        	    
		        } catch (GlobalDocumentNumberNotUniqueException ex) {
		           	
		           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("invStockIssuanceEntry.error.documentNumberNotUnique"));
		                  	
		        } catch (GlobalTransactionAlreadyApprovedException ex) {
		        	
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                 new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyApproved"));
		        	
		        } catch (GlobalTransactionAlreadyPendingException ex) {
		        	
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                 new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPending"));
		        	
		        } catch (GlobalTransactionAlreadyPostedException ex) {
		        	
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                 new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPosted"));
		        	   
		        } catch (GlobalInvItemLocationNotFoundException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                 new ActionMessage("invStockIssuanceEntry.error.noItemLocationFound", ex.getMessage()));
		           	   
		        } catch (GlJREffectiveDateNoPeriodExistException ex) {
		       	
		       	   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("invStockIssuanceEntry.error.effectiveDateNoPeriodExist"));
		                
		       } catch (GlJREffectiveDatePeriodClosedException ex) {
		       	
		       	      errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("invStockIssuanceEntry.error.effectiveDatePeriodClosed"));
		       	      
		       } catch (GlobalJournalNotBalanceException ex) {
		       	
		       	      errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("invStockIssuanceEntry.error.journalNotBalance"));
		       	      
		       } catch (GlobalInventoryDateException ex) {
		           	
		           	  errors.add(ActionMessages.GLOBAL_MESSAGE,
		           		new ActionMessage("invStockIssuanceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

	           } catch(GlobalBranchAccountNumberInvalidException ex) {
				
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invStockIssuanceEntry.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invStockIssuanceEntry.error.noNegativeInventoryCostingCOA"));

		       }  catch (EJBException ex) {
		        	    if (log.isInfoEnabled()) {
		            	
		               log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		             }
		            
		            return(mapping.findForward("cmnErrorPage"));
		        }
		       
		        if (!errors.isEmpty()) {
	        	
		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invStockIssuanceEntry"));
		               
		        }
      		}
	      	
	      	String path = "/invJournal.do?forward=1" + 
			"&transactionCode=" + actionForm.getStockIssuanceCode() +
			"&transaction=STOCK ISSUANCE" + 
			"&transactionNumber=" + actionForm.getDocumentNumber() + 
			"&transactionDate=" + actionForm.getDate() +
			"&transactionEnableFields=" + actionForm.getEnableFields();
	      	
	      	return(new ActionForward(path));

/*******************************************************
    --  INV Print Action --
*******************************************************/             	
                  	
         }else if (request.getParameter("printButton") != null){
         	
         	if(Common.validateRequired(actionForm.getApprovalStatus())) {
         		
         		InvStockIssuanceDetails details = new InvStockIssuanceDetails();
         		
         		details.setSiCode(actionForm.getStockIssuanceCode());
         		details.setSiReferenceNumber(actionForm.getReferenceNumber());
         		details.setSiDocumentNumber(actionForm.getDocumentNumber());
         		details.setSiDescription(actionForm.getDescription());
         		details.setSiDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setSiVoid(Common.convertBooleanToByte(actionForm.getVoidCheckbox()));
         		details.setSiCreatedBy(actionForm.getCreatedBy());
         		details.setSiDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
         		details.setSiLastModifiedBy(actionForm.getLastModifiedBy());
         		details.setSiDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
         	
         		if (actionForm.getStockIssuanceCode() == null) {
         			
         			details.setSiCreatedBy(user.getUserName());
         			details.setSiDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         			
         		}
         		
         		details.setSiLastModifiedBy(user.getUserName());
         		details.setSiDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
         		
         		ArrayList silList = new ArrayList();
         		
         		for (int i = 0; i<actionForm.getInvSIListSize(); i++) {
         			
         			InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(i);
         			
         			if (!invSIList.getIssueCheckbox()) continue;
         			
         			InvModStockIssuanceLineDetails mdetails = new InvModStockIssuanceLineDetails();
         			
         			mdetails.setSilBosCode(invSIList.getBosCode());
         			mdetails.setSilIlLocationName(invSIList.getLocation());
         			mdetails.setSilIlIiName(invSIList.getItemName());
         			mdetails.setSilIssueQuantity(Common.convertStringMoneyToDouble(invSIList.getIssueQuantity(), quantityPrecisionUnit));
         			mdetails.setSilIlIiUomName(invSIList.getUnit());
         			mdetails.setSilUnitCost(Common.convertStringMoneyToDouble(invSIList.getUnitCost(), precisionUnit));
                	   
         			silList.add(mdetails);
         			
         		}                     	
         		
         		try {
         			
         			Integer stockIssuanceCode = ejbSI.saveInvSiEntry(details, silList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         			actionForm.setStockIssuanceCode(stockIssuanceCode);
         			
         		} catch (GlobalRecordAlreadyDeletedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));                          	                 	                   
         			
         		} catch (GlobalTransactionAlreadyApprovedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyApproved"));
         			
         		} catch (GlobalTransactionAlreadyPendingException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPending"));
         			
         		} catch (GlobalTransactionAlreadyPostedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.transactionAlreadyPosted"));
         			
         		} catch (GlobalInvItemLocationNotFoundException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.noItemLocationFound", ex.getMessage()));
         			
         		} catch (GlJREffectiveDateNoPeriodExistException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.effectiveDateNoPeriodExist"));
         			
         		} catch (GlobalDocumentNumberNotUniqueException ex) {
                   	
                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invStockIssuanceEntry.error.documentNumberNotUnique"));
         			
         		} catch (GlJREffectiveDatePeriodClosedException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.effectiveDatePeriodClosed"));
         			
         		} catch (GlobalJournalNotBalanceException ex) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("invStockIssuanceEntry.error.journalNotBalance"));
         	    
         		} catch (GlobalInventoryDateException ex) {
                   	
               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               		        new ActionMessage("invStockIssuanceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch(GlobalBranchAccountNumberInvalidException ex) {
     			
                		errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invStockIssuanceEntry.error.branchAccountNumberInvalid", ex.getMessage()));

                } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
               	
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                           new ActionMessage("invStockIssuanceEntry.error.noNegativeInventoryCostingCOA"));

         		}  catch (EJBException ex) {
         			if (log.isInfoEnabled()) {
         				
         				log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}
         			
         			return(mapping.findForward("cmnErrorPage"));
         		}
         		
         		if (!errors.isEmpty()) {
         			
         			saveErrors(request, new ActionMessages(errors));
         			return(mapping.findForward("invStockIssuanceEntry"));
         			
         		}
         		
         		actionForm.setReport(Constants.STATUS_SUCCESS);
         		
         		isInitialPrinting = true;
         		
         	}  else {
        	
        	actionForm.setReport(Constants.STATUS_SUCCESS);
          	          	        
	        	return(mapping.findForward("invStockIssuanceEntry"));
	        	
        }  
/*******************************************************
   -- Inv SI Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbSI.deleteInvSiEntry(actionForm.getStockIssuanceCode(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
        
/*******************************************************
   -- Inv SI Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
    -- Inv SI Item Selected Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("isItemEntered"))) {
                  
      	try {
      		
      		// populate line group
      		
      		actionForm.clearInvSIList();
      		
      		ArrayList list = ejbSI.getInvIncompleteBosByBolCode(actionForm.getBolCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      		      		
      		Iterator i = list.iterator();
      		
      		while (i.hasNext()) {
      			
      			InvModBuildOrderStockDetails mdetails = (InvModBuildOrderStockDetails)i.next();
      			
      			double issueQty = mdetails.getBosQuantityRequired() - mdetails.getBosQuantityIssued();
      			
      			InvStockIssuanceEntryList invSIList = new InvStockIssuanceEntryList(actionForm,
      					mdetails.getBosCode(), 
      					mdetails.getBosIiName(), null, 
      					Common.convertDoubleToStringMoney(mdetails.getBosQuantityRequired(), quantityPrecisionUnit),
      					Common.convertDoubleToStringMoney(mdetails.getBosQuantityIssued(), quantityPrecisionUnit),
      					mdetails.getBosIiUomName(),
      					Common.convertDoubleToStringMoney(issueQty, quantityPrecisionUnit),
      					Common.convertDoubleToStringMoney(mdetails.getBosIiUnitCost(), precisionUnit), 
      					Common.convertDoubleToStringMoney(issueQty * mdetails.getBosIiUnitCost(), precisionUnit),
						mdetails.getBosIiDescription());
      			
      			invSIList.setLocationList(actionForm.getLocationList());
      			
      			invSIList.clearUnitList();
      			ArrayList uomList = ejbSI.getInvUomByIiName(invSIList.getItemName(), user.getCmpCode());
				
				Iterator uomItr = uomList.iterator();
				
				while (uomItr.hasNext()) {
					
					InvModUnitOfMeasureDetails uomdetails = (InvModUnitOfMeasureDetails)uomItr.next();
					
					invSIList.setUnitList(uomdetails.getUomName());
					
				}
      			
				actionForm.saveInvSIList(invSIList);
      			      			
      		}
		   	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("invStockIssuanceEntry"));
         
/*****************************************************
  ---INV Stock Issuance Unit Enter Action
*****************************************************/			
         
      } else if(!Common.validateRequired(request.getParameter("invSIList[" + 
              actionForm.getRowSelected() + "].isUnitEntered")) &&
              actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
          
          InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(actionForm.getRowSelected());
          
          try {
              
              // populate qty, unit cost & total cost fields
              
              if (!Common.validateRequired(invSIList.getItemName())) {
                  
                  double QTY_RQRD = ejbSI.getQuantityRequiredByIiNameAndUomNameAndBosCode(invSIList.getItemName(),
                          invSIList.getUnit(), invSIList.getBosCode(), user.getCmpCode());
                  
                  invSIList.setQuantityRequired(Common.convertDoubleToStringMoney(QTY_RQRD, quantityPrecisionUnit));
                  
                  double QTY_ISSD = ejbSI.getQuantityIssuedByIiNameAndUomNameAndBosCode(invSIList.getItemName(),
                          invSIList.getUnit(), invSIList.getBosCode(), user.getCmpCode());
                 
                  invSIList.setQuantityIssued(Common.convertDoubleToStringMoney(QTY_ISSD, quantityPrecisionUnit));
                  
                  invSIList.setIssueQuantity(Common.convertDoubleToStringMoney(QTY_RQRD-QTY_ISSD, quantityPrecisionUnit));
                  
                  if (!Common.validateRequired(invSIList.getUnit())) {
                      
                      
                      double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invSIList.getItemName(), invSIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                

                      /*
                      
                      // set unit cost
                      double unitCost = ejbSI.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
                              invSIList.getItemName(), invSIList.getLocation(), invSIList.getUnit(), 
                              Common.convertStringToSQLDate(actionForm.getDate()), 
                              new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                      */
                      invSIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
                      invSIList.setAmount(Common.convertDoubleToStringMoney(unitCost * (QTY_RQRD-QTY_ISSD), precisionUnit));
                      
                  }
                  
              }
              
          } catch (EJBException ex) {
              
              if (log.isInfoEnabled()) {
                  
                  log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
              }
              
          }  	
          
          return(mapping.findForward("invStockIssuanceEntry"));
               
/*******************************************************
    -- Inv SI Location Selected Action --
*******************************************************/
          
      } else if(!Common.validateRequired(request.getParameter("invSIList[" + 
              actionForm.getRowSelected() + "].isLocationEntered")) &&
              actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
          
          InvStockIssuanceEntryList invSIList = actionForm.getInvSIByIndex(actionForm.getRowSelected());
          
          try {
              
              /// populate unit cost & total cost fields
              
              if (!Common.validateRequired(invSIList.getItemName()) && !Common.validateRequired(invSIList.getUnit())) {
                  
                    double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invSIList.getItemName(), invSIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
                  /*
                  // set unit cost
                  double unitCost = ejbSI.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
                          invSIList.getItemName(), invSIList.getLocation(), invSIList.getUnit(), 
                          Common.convertStringToSQLDate(actionForm.getDate()), 
                          new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                  */
                  invSIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
                  invSIList.setAmount(Common.convertDoubleToStringMoney(unitCost * Common.convertStringMoneyToDouble(invSIList.getIssueQuantity(), precisionUnit), precisionUnit));
                  
              }
              
          } catch (EJBException ex) {
              
              if (log.isInfoEnabled()) {
                  
                  log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
              }
              
          }  
          
          return(mapping.findForward("invStockIssuanceEntry"));
          
/*******************************************************
   -- Inv SI Load Action --
*******************************************************/
          
      }
      
         if (frParam != null) {
             
             if (!errors.isEmpty()) {
                 
                 saveErrors(request, new ActionMessages(errors));
                 return(mapping.findForward("invStockIssuanceEntry"));
                 
             }
             
             if (request.getParameter("forward") == null &&
                     actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                 
                 errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                 saveErrors(request, new ActionMessages(errors));
                 
                 return mapping.findForward("cmnMain");	
                 
             }
             
             try {
                 
                 // location combo box
                 
                 ArrayList list = null;
                 Iterator i = null;
                 
                 actionForm.clearLocationList();       	
                 
                 list = ejbSI.getInvLocAll(user.getCmpCode());
                 
                 if (list == null || list.size() == 0) {
                     
                     actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
                     
                 } else {
                     
                     i = list.iterator();
                     
                     while (i.hasNext()) {
                         
                         actionForm.setLocationList((String)i.next());
                         
                     }
                     
                 }            	            
                 
                 actionForm.clearInvSIList();
                 
                 
                 
                 // finder 
                 
                 if (request.getParameter("forward") != null || isInitialPrinting ) {
                     
                     if (request.getParameter("forward") != null) {
                         
                         actionForm.setStockIssuanceCode(new Integer(request.getParameter("stockIssuanceCode")));
                         
                     }
                     
                     InvModStockIssuanceDetails mdetails = ejbSI.getInvSiBySiCode(actionForm.getStockIssuanceCode(), user.getCmpCode());            	
                     
                     actionForm.setStockIssuanceCode(mdetails.getSiCode());
                     actionForm.setReferenceNumber(mdetails.getSiReferenceNumber());
                     actionForm.setDocumentNumber(mdetails.getSiDocumentNumber());
                     actionForm.setDescription(mdetails.getSiDescription());
                     actionForm.setLocation(mdetails.getSiSilIlLocationName());
                     actionForm.setItemName(mdetails.getSiSilIlIiName());
                     actionForm.setBuildOrder(mdetails.getSiSilBosBolBorDocumentNumber());
                     actionForm.setVoidCheckbox(Common.convertByteToBoolean(mdetails.getSiVoid()));
                     actionForm.setDate(Common.convertSQLDateToString(mdetails.getSiDate()));
                     actionForm.setApprovalStatus(mdetails.getSiApprovalStatus());
                     actionForm.setPosted(mdetails.getSiPosted() == 1 ? "YES" : "NO");
                     actionForm.setCreatedBy(mdetails.getSiCreatedBy());
                     actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getSiDateCreated()));
                     actionForm.setLastModifiedBy(mdetails.getSiLastModifiedBy());
                     actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getSiDateLastModified()));
                     actionForm.setApprovedRejectedBy(mdetails.getSiApprovedRejectedBy());
                     actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getSiDateApprovedRejected()));
                     actionForm.setPostedBy(mdetails.getSiPostedBy());
                     actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getSiDatePosted()));            		actionForm.setReasonForRejection(mdetails.getSiReasonForRejection());
                     actionForm.setBolCode(mdetails.getSiSilBolCode());
                     
                     list = mdetails.getSiSilList();           		
                     
                     i = list.iterator();
                     
                     int lineNumber = 0;
                     
                     while (i.hasNext()) {
                         
                         InvModStockIssuanceLineDetails mSilDetails = (InvModStockIssuanceLineDetails)i.next();		            			            			            	
                         
                         InvStockIssuanceEntryList siSilList = new InvStockIssuanceEntryList(actionForm,
                                 mSilDetails.getSilBosCode(),
                                 mSilDetails.getSilIlIiName(), mSilDetails.getSilIlLocationName(),
                                 Common.convertDoubleToStringMoney(mSilDetails.getSilBosQtyrequired(), quantityPrecisionUnit), 
                                 Common.convertDoubleToStringMoney(mSilDetails.getSilBosQtyIssued(), quantityPrecisionUnit),
                                 mSilDetails.getSilIlIiUomName(), 
                                 Common.convertDoubleToStringMoney(mSilDetails.getSilIssueQuantity(), quantityPrecisionUnit), 
                                 Common.convertDoubleToStringMoney(mSilDetails.getSilUnitCost(), precisionUnit), 
                                 Common.convertDoubleToStringMoney(mSilDetails.getSilIssueQuantity() * mSilDetails.getSilUnitCost(), precisionUnit),
                                 mSilDetails.getSilIlIiDescription());		            	
                         
                         siSilList.setLocationList(actionForm.getLocationList());          	
                         siSilList.setIssueCheckbox(true);
                       
                         ArrayList uomList = ejbSI.getInvUomByIiName(mSilDetails.getSilIlIiName(), user.getCmpCode());
                         
                         Iterator j = uomList.iterator();
                         while(j.hasNext()) {
                             
                             InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
                             siSilList.setUnitList(mUomDetails.getUomName());
                             
                             
                         }
                         actionForm.saveInvSIList(siSilList);
                         
                     }	
                     
                     this.setFormProperties(actionForm);
                     
                     // additional build orders
                     if (actionForm.getEnableFields()) {
                         
                         ArrayList bosList = ejbSI.getInvIncompleteBosByBolCode(actionForm.getBolCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                         
                         Iterator bosIter = bosList.iterator();
                         
                         while (bosIter.hasNext()) {
                             
                             InvModBuildOrderStockDetails bosDetails = (InvModBuildOrderStockDetails)bosIter.next();
                             
                             double issueQty = bosDetails.getBosQuantityRequired() - bosDetails.getBosQuantityIssued();
                             
                             InvStockIssuanceEntryList invSIList = new InvStockIssuanceEntryList(actionForm,
                                     bosDetails.getBosCode(), 
                                     bosDetails.getBosIiName(),
                                     null,
                                     Common.convertDoubleToStringMoney(bosDetails.getBosQuantityRequired(), quantityPrecisionUnit),
                                     Common.convertDoubleToStringMoney(bosDetails.getBosQuantityIssued(), quantityPrecisionUnit),
                                     bosDetails.getBosIiUomName(),
                                     Common.convertDoubleToStringMoney(issueQty, quantityPrecisionUnit), 
                                     Common.convertDoubleToStringMoney(bosDetails.getBosIiUnitCost(), precisionUnit),
                                     Common.convertDoubleToStringMoney(issueQty * bosDetails.getBosIiUnitCost(), precisionUnit),
                                     bosDetails.getBosIiDescription());
                             
                             invSIList.setLocationList(actionForm.getLocationList());
                             
                             ArrayList uomList = ejbSI.getInvUomByIiName(bosDetails.getBosIiName(), user.getCmpCode());
                             
                             Iterator j = uomList.iterator();
                             while(j.hasNext()) {
                                 
                                 InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
                                 invSIList.setUnitList(mUomDetails.getUomName());
                                 
                             }
                             
                             actionForm.saveInvSIList(invSIList);
                             
                         }
                         
                     }
                     
                     return (mapping.findForward("invStockIssuanceEntry"));         
                     
                     
                 }
                 
             } catch(GlobalNoRecordFoundException ex) {
                 
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invStockIssuanceEntry.error.recordAlreadyDeleted"));
                 
             } catch(EJBException ex) {
                 
                 if (log.isInfoEnabled()) {
                     
                     log.info("EJBException caught in InvStockIssuanceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }
                 
                 return(mapping.findForward("cmnErrorPage"));
                 
             } 
             
             // Errors on loading
             
             if (!errors.isEmpty()) {
                 
                 saveErrors(request, new ActionMessages(errors));
                 
             } else {
                 
                 if (request.getParameter("saveSubmitButton") != null && 
                         actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                     
                     
                     actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                     
                     
                 } else if (request.getParameter("saveAsDraftButton") != null && 
                         actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                     
                     actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                     
                 }
                 
                 
             }
             
             actionForm.reset(mapping, request);              
             
             actionForm.setStockIssuanceCode(null);
             actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
             actionForm.setPosted("NO");
             actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
             actionForm.setCreatedBy(user.getUserName());
             actionForm.setLastModifiedBy(user.getUserName());
             actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));                        
             
             this.setFormProperties(actionForm);
             return(mapping.findForward("invStockIssuanceEntry"));
             
         } else {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
             saveErrors(request, new ActionMessages(errors));
             
             return(mapping.findForward("cmnMain"));
             
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in StockIssuance.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(InvStockIssuanceEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getStockIssuanceCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowCloseButton(true);
					
				} else if (actionForm.getStockIssuanceCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowCloseButton(true);
	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowCloseButton(false);
					
				}
				
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowCloseButton(false);
				
			}
			
		} else {
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowCloseButton(false);
			
		}
		    
	}
		
}