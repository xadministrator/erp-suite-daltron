package com.struts.inv.stockissuanceentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class InvStockIssuanceEntryForm extends ActionForm implements Serializable {

	private Integer stockIssuanceCode = null;
	private Integer bolCode = null;
	private String date = null;
	private String referenceNumber = null;
	private String documentNumber = null;
	private String description = null;
	private String itemName = null;
	private String buildOrder = null;
	private boolean voidCheckbox = false;
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String reasonForRejection = null;
	
	private String location = null;
	private ArrayList locationList = new ArrayList();
	   
	private ArrayList invSIList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showSaveSubmitButton = false;
	private boolean showSaveAsDraftButton = false;
	private boolean showCloseButton = false;
	private boolean showDeleteButton = false;
	
	private String report = null;
         
	public int getRowSelected(){
		return rowSelected;
	}

	public InvStockIssuanceEntryList getInvSIByIndex(int index){
		return((InvStockIssuanceEntryList)invSIList.get(index));
	}

	public Object[] getInvSIList(){
		return(invSIList.toArray());
	}

	public int getInvSIListSize(){
		return(invSIList.size());
	}

	public void saveInvSIList(Object newInvSIList){
		invSIList.add(newInvSIList);
	}

	public void clearInvSIList(){
		invSIList.clear();
	}

	public void setRowSelected(Object selectedInvSIList, boolean isEdit){
		this.rowSelected = invSIList.indexOf(selectedInvSIList);
	}

	public void updateInvSIRow(int rowSelected, Object newInvSIList){
		invSIList.set(rowSelected, newInvSIList);
	}

	public void deleteInvSIList(int rowSelected){
		invSIList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Integer getStockIssuanceCode() {
		return stockIssuanceCode;
	}
	
	public void setStockIssuanceCode(Integer stockIssuanceCode) {
		this.stockIssuanceCode = stockIssuanceCode;
	}
	
	public Integer getBolCode() {
		
		return bolCode;
		
	}
	
	public void setBolCode(Integer bolCode) { 
		
		this.bolCode = bolCode;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
	}
	
	
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getDateApprovedRejected() {
		
		return dateApprovedRejected;
		
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		
		this.dateApprovedRejected = dateApprovedRejected;
		
	}
	
	public String getDateCreated() {
		
		return dateCreated;
		
	}
	
	public void setDateCreated(String dateCreated) {
		
		this.dateCreated = dateCreated;
		
	}
	
	public String getDateLastModified() {
		
		return dateLastModified;
		
	}
	
	public void setDateLastModified(String dateLastModified) {
		
		this.dateLastModified = dateLastModified;
		
	}
	
	public String getDatePosted() {
		
		return datePosted;
		
	}
	
	public void setDatePosted(String datePosted) {
		
		this.datePosted = datePosted;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}	
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(String locationList) {
		
		this.locationList.add(locationList);
		
	}
	
	public void clearLocationList() {
		
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public boolean getVoidCheckbox() {
		
		return voidCheckbox;
		
	}
	
	public void setVoidCheckbox(boolean voidCheckbox) {
		
		this.voidCheckbox = voidCheckbox;
		
	}
	
	public String getReasonForRejection() {
		
	   	
	  return reasonForRejection;
	   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getBuildOrder() {
		
		return buildOrder;
		
	}
	
	public void setBuildOrder(String buildOrder) {
		
		this.buildOrder = buildOrder;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	
	public boolean isShowSaveSubmitButton() {
		
		return showSaveSubmitButton;
		
	}
	
	public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {
		
		this.showSaveSubmitButton = showSaveSubmitButton;
		
	}
	
	public boolean isShowSaveAsDraftButton() {
		
		return showSaveAsDraftButton;
		
	}
	
	public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {
		
		this.showSaveAsDraftButton = showSaveAsDraftButton;
		
	}
	
	public boolean isShowCloseButton() {
		
		return showCloseButton;
		
	}
	
	public void setShowCloseButton(boolean showCloseButton) {
		
		this.showCloseButton = showCloseButton;
		
	}
	
	public boolean isShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       location = Constants.GLOBAL_BLANK;
	   date = null;
	   referenceNumber = null;
	   documentNumber = null;
	   itemName = null;
	   buildOrder = null;
	   description = null;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   reasonForRejection = null;
	   itemName = null;
	   
	   for (int i=0; i<invSIList.size(); i++) {
	  	
	  	  InvStockIssuanceEntryList actionList = (InvStockIssuanceEntryList)invSIList.get(i);

	  	  actionList.setIssueCheckbox(false);
	  	
	  } 


   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
		 request.getParameter("journalButton") != null || 
		 request.getParameter("printButton") != null) { 
      	
      	
      	 if(Common.validateRequired(itemName)){
            errors.add("itemName",
               new ActionMessage("invStockIssuanceEntry.error.itemNameRequired"));
         } 
      	
      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("invStockIssuanceEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	        errors.add("date", 
		       new ActionMessage("invStockIssuanceEntry.error.dateInvalid"));
		 }	 	         		 		 
         
         int numberOfLines = 0;
      	 
      	 Iterator i = invSIList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvStockIssuanceEntryList ilList = (InvStockIssuanceEntryList)i.next();      	 	 
			 
      	 	 if (!ilList.getIssueCheckbox()) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(ilList.getLocation())){
	            errors.add("location",
	            	new ActionMessage("invStockIssuanceEntry.error.locationRequired", String.valueOf(numberOfLines)));
	         }
	       
		 	 if(Common.validateRequired(ilList.getIssueQuantity())){
	            errors.add("issueQuantity",
	               new ActionMessage("invStockIssuanceEntry.error.issueQuantityRequired", String.valueOf(numberOfLines)));
	         }	    
		 	 
		 	if(!Common.validateNumberFormat(ilList.getIssueQuantity())){
	            errors.add("issueQuantity",
	               new ActionMessage("invStockIssuanceEntry.error.issueQuantityInvalid", String.valueOf(numberOfLines)));
	         }
		 	
		 	if(!Common.validateRequired(ilList.getIssueQuantity()) && Common.convertStringMoneyToDouble(ilList.getIssueQuantity(), (short)3) <= 0) {
	         	errors.add("issueQuantity",
	         		new ActionMessage("invStockIssuanceEntry.error.negativeOrZeroIssueQuantityNotAllowed", ilList.getItemName()));
	         }
		 	
		 	if (Common.validateRequired(approvalStatus)) {
		 	
			 	if (!Common.validateRequired(ilList.getIssueQuantity()) &&
			 		Common.validateNumberFormat(ilList.getIssueQuantity()) &&
					Common.convertStringMoneyToDouble(ilList.getIssueQuantity(), (short)3) > Common.convertStringMoneyToDouble(ilList.getQuantityRequired(), (short)3) - Common.convertStringMoneyToDouble(ilList.getQuantityIssued(), (short)3)) {
			 		
			 		errors.add("issueQuantity",
				         new ActionMessage("invStockIssuanceEntry.error.issueQuantityGreaterThanRequired", String.valueOf(numberOfLines)));
			 		
			 	}
			 	
		 	}
       
      	 }  	    
      	 
      	 if (numberOfLines == 0) {
      	 	
      	 	errors.add("issueQuantity",
 	               new ActionMessage("invStockIssuanceEntry.error.stockIssuanceMustHaveLine"));
      	 	
      	 }
	        	 
      } 
      
      return(errors);	
   }
}
