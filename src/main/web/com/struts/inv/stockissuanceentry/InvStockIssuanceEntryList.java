package com.struts.inv.stockissuanceentry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.inv.stockissuanceentry.InvStockIssuanceEntryForm;

public class InvStockIssuanceEntryList implements Serializable {

	private Integer bosCode = null;
	private String location = null;
	private ArrayList locationList;
	private String itemName = null;
	private String quantityRequired = null;
	private String quantityIssued = null;
	private String issueQuantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private boolean issueCheckbox = false;
	
	private String isUnitEntered = null;
	private String isLocationEntered = null;
	
	private boolean enableUnitCost = false;
	private String unitCost = null;
	private String amount = null;
	private String itemDescription = null;
	    
	private InvStockIssuanceEntryForm parentBean;   

	public InvStockIssuanceEntryList(InvStockIssuanceEntryForm parentBean,
		Integer bosCode, String itemName, String location, 
		String quantityRequired, String quantityIssued, String unit, String issueQuantity, 
		String unitCost, String amount, String itemDescription){
	
		this.parentBean = parentBean;
		this.bosCode = bosCode;
		this.location = location;
		this.itemName = itemName;
		this.quantityRequired = quantityRequired;
		this.quantityIssued = quantityIssued;
		this.unit = unit;
		this.issueQuantity = issueQuantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.itemDescription = itemDescription;

	}
	
	public InvStockIssuanceEntryForm getParentBean() {
		
		return parentBean;
		
	}
	
	public void setParentBean(InvStockIssuanceEntryForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public boolean getIssueCheckbox() {
		
		return issueCheckbox;
		
	}
	
	public void setIssueCheckbox(boolean issueCheckbox) {
		
		this.issueCheckbox = issueCheckbox;
		
	}
	
	public Integer getBosCode() {
		
		return bosCode;
		
	}
	
	public void setBosCode(Integer bosCode) {
		
		this.bosCode = bosCode;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
			
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getQuantityRequired() {
		
		return quantityRequired;
		
	}
	
	public void setQuantityRequired(String quantityRequired) {
		
		this.quantityRequired = quantityRequired;
		
	}
	
	public String getQuantityIssued() {
		
		return quantityIssued;
		
	}
	
	public void setQuantityIssued(String quantityIssued) {
		
		this.quantityIssued = quantityIssued;
		
	}
	
	public String getIssueQuantity() {
		
		return issueQuantity;
		
	}
	
	public void setIssueQuantity(String issueQuantity) {
		
		this.issueQuantity = issueQuantity;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	public void clearUnitList(){
		unitList.clear();
	}
	
	public ArrayList getUnitList(){
		return unitList;
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public String getUnitCost(){
		return unitCost;
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getAmount() {
		
	    return amount;		
		
	}
	
	public void setAmount(String amount) {
	    
	    this.amount = amount;
		
	}	
	
	public String getIsUnitEntered(){
	    return isUnitEntered;
	}
	
	public void setIsUnitEntered(String isUnitEntered){
	    
	    if (isUnitEntered != null && isUnitEntered.equals("true")) {
	        
	        parentBean.setRowSelected(this, false);
	        
	    }				
	    
	    isUnitEntered = null;
	}
	
	public String getIsLocationEntered(){
	    return isLocationEntered;
	}
	
	public void setIsLocationEntered(String isLocationEntered){
	    
	    if (isLocationEntered != null && isLocationEntered.equals("true")) {
	        
	        parentBean.setRowSelected(this, false);
	        
	    }				
	    
	    isLocationEntered = null;
	}
	
	public String getItemDescription() {
		
	    return itemDescription;		
		
	}
	
	public void setItemDescription(String itemDescription) {
	    
	    this.itemDescription = itemDescription;
		
	}
	
}
