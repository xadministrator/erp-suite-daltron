package com.struts.inv.stocktransferentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.ejb.txn.InvStockTransferEntryController;
import com.ejb.txn.InvStockTransferEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModStockTransferDetails;
import com.util.InvModStockTransferLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvStockTransferDetails;

public class InvStockTransferEntryAction extends Action{
    
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	
	    HttpSession session = request.getSession();
	    InvStockTransferEntryForm actionForm = (InvStockTransferEntryForm)form;
        
	    actionForm.setReport(null);
	    
	    try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvStockTransferEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         String frParam = Common.getUserPermission(user, Constants.INV_STOCK_TRANSFER_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invStockTransferEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }
 
/*******************************************************
     Initialize invStockTransferEntryController EJB
*******************************************************/
	
	   InvStockTransferEntryControllerHome homeST = null;
	   InvStockTransferEntryController ejbST = null;
	   
	    
       InvItemEntryControllerHome homeII = null;
       InvItemEntryController ejbII = null;
	
	   try {
	      
	      homeST = (InvStockTransferEntryControllerHome)com.util.EJBHomeFactory.
	          lookUpHome("ejb/InvStockTransferEntryControllerEJB", InvStockTransferEntryControllerHome.class);
	          
	    homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);
	      
	   } catch(NamingException e) {
	      if(log.isInfoEnabled()){
	          log.info("NamingException caught in InvStockTransferEntryAction.execute(): " + e.getMessage() +
	         " session: " + session.getId());
	      }
	      return(mapping.findForward("cmnErrorPage"));
	   }
	
	   try {
	   	
	   	 ejbII = homeII.create();
	      ejbST = homeST.create();
	      
	   } catch(CreateException e) {
	   	
	      if(log.isInfoEnabled()) {
	      	
	          log.info("CreateException caught in InvStockTransferEntryAction.execute(): " + e.getMessage() +
	             " session: " + session.getId());
	      }
	      
	      return(mapping.findForward("cmnErrorPage"));
	      
	   }
	
	   ActionErrors errors = new ActionErrors();
	   ActionMessages messages = new ActionMessages();   
	   
/*******************************************************
   Call invStockTransferEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

	   short precisionUnit = 0;
	   short quantityPrecisionUnit = 0;
	   short stockTransferLineNumber = 0;
	   boolean isInitialPrinting = false;
	     
	   try {
	     	
	      precisionUnit = ejbST.getGlFcPrecisionUnit(user.getCmpCode());
	      quantityPrecisionUnit = ejbST.getInvGpQuantityPrecisionUnit(user.getCmpCode());
	      stockTransferLineNumber = ejbST.getInvGpInventoryLineNumber(user.getCmpCode());        
	                
	   } catch(EJBException ex) {
	     	
	      if (log.isInfoEnabled()) {
	        	
	         log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
	            " session: " + session.getId());
	      }
	        
	      return(mapping.findForward("cmnErrorPage"));
	        
	   }
	   
/*******************************************************
   -- Inv ST Save As Draft Action --
*******************************************************/

     if (request.getParameter("saveAsDraftButton") != null &&
       actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
       	           	
       InvStockTransferDetails details = new InvStockTransferDetails();
       
       details.setStCode(actionForm.getStockTransferCode());
       details.setStDocumentNumber(actionForm.getDocumentNumber());
       details.setStReferenceNumber(actionForm.getReferenceNumber());
       details.setStDescription(actionForm.getDescription());
       details.setStDate(Common.convertStringToSQLDate(actionForm.getDate()));       
       details.setStCreatedBy(actionForm.getCreatedBy());
       details.setStDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
       details.setStLastModifiedBy(actionForm.getLastModifiedBy());
       details.setStDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                  
       if (actionForm.getStockTransferCode() == null) {
       	
       		details.setStCreatedBy(user.getUserName());
       		details.setStDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                      
       }
                  
       details.setStLastModifiedBy(user.getUserName());
       details.setStDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
       
       ArrayList stlList = new ArrayList(); 
      
       for (int i = 0; i<actionForm.getInvSTListSize(); i++) {
       	
       	   InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);           	   
       	              	             	   
           if (Common.validateRequired(invSTList.getLocationFrom()) &&
                     Common.validateRequired(invSTList.getLocationTo()) &&
         	 	     Common.validateRequired(invSTList.getItemName()) &&
         	 	     Common.validateRequired(invSTList.getQtyDelivered()) &&
         	 	     Common.validateRequired(invSTList.getUnit()) &&
         	 	     Common.validateRequired(invSTList.getUnitCost())) continue;
       	   
       	   InvModStockTransferLineDetails mdetails = new InvModStockTransferLineDetails();
       	   
       	   mdetails.setStlUnitCost(Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit));
       	   mdetails.setStlQuantityDelivered(Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), quantityPrecisionUnit));
       	   mdetails.setStlAmount(Common.convertStringMoneyToDouble(invSTList.getAmount(), quantityPrecisionUnit));
       	   mdetails.setStlUomName(invSTList.getUnit());
       	   mdetails.setStlLocationNameFrom(invSTList.getLocationFrom());
       	   mdetails.setStlLocationNameTo(invSTList.getLocationTo());
       	   mdetails.setStlIiName(invSTList.getItemName());
       	   mdetails.setStlLineNumber(Common.convertStringToShort(invSTList.getLineNumber()));
       	   mdetails.setStlMisc(invSTList.getMisc());
       	   stlList.add(mdetails);
       	
       }                     	
       
       try {
       	
       	    ejbST.saveInvStEntry(details, stlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
       	
       } catch (GlobalDocumentNumberNotUniqueException ex) {
       	
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
       			new ActionMessage("invStockTransferEntry.error.documentNumberNotUnique"));    
       	
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	
       	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));       	                                  	                 	                  
                 	
       } catch (GlobalTransactionAlreadyApprovedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyApproved"));
       	
       } catch (GlobalTransactionAlreadyPendingException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyPending"));
       	
       } catch (GlobalTransactionAlreadyPostedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyPosted"));
                
       } catch (GlobalNoApprovalRequesterFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noApprovalRequesterFound"));
                
       } catch (GlobalNoApprovalApproverFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noApprovalApproverFound"));
       	   
       } catch (GlobalInvItemLocationNotFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noItemLocationFound", ex.getMessage()));
       	   
       } catch (GlJREffectiveDateNoPeriodExistException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.effectiveDateNoPeriodExist"));
                
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.effectiveDatePeriodClosed"));
                
       } catch (GlobalJournalNotBalanceException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.journalNotBalance")); 
       	   
       } catch (GlobalInventoryDateException ex) {
           
           errors.add(ActionMessages.GLOBAL_MESSAGE,
           		new ActionMessage("invStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
      	   
       } catch(GlobalBranchAccountNumberInvalidException ex) {
		
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
      			new ActionMessage("invStockTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

       } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
      	
          errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("invStockTransferEntry.error.noNegativeInventoryCostingCOA"));

       } catch (GlobalRecordInvalidException ex) {
    	   
    	   errors.add(ActionMessages.GLOBAL_MESSAGE,
    			   new ActionMessage("buildUnbuildAssemblyPost.error.insufficientStocks", ex.getMessage()));
    	   
       }catch (EJBException ex) {
       	    if (log.isInfoEnabled()) {
           	
              log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
                 " session: " + session.getId());
            }
           
           return(mapping.findForward("cmnErrorPage"));
       }
       
/*******************************************************
   -- Inv ST Save & Submit Action --
*******************************************************/

     } else if (request.getParameter("saveSubmitButton") != null &&
       actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
       	           	
         InvStockTransferDetails details = new InvStockTransferDetails();
         
         details.setStCode(actionForm.getStockTransferCode());
         details.setStDocumentNumber(actionForm.getDocumentNumber());
         details.setStReferenceNumber(actionForm.getReferenceNumber());
         details.setStDescription(actionForm.getDescription());
         details.setStDate(Common.convertStringToSQLDate(actionForm.getDate()));       
         details.setStCreatedBy(actionForm.getCreatedBy());
         details.setStDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
         details.setStLastModifiedBy(actionForm.getLastModifiedBy());
         details.setStDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                   
         if (actionForm.getStockTransferCode() == null) {
            	
    		details.setStCreatedBy(user.getUserName());
    		details.setStDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                           
         }
                   
         details.setStLastModifiedBy(user.getUserName());
         details.setStDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
        
         ArrayList stlList = new ArrayList(); 
       
         for (int i = 0; i<actionForm.getInvSTListSize(); i++) {
        	
        	 InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);           	   
        	              	             	   
             if (Common.validateRequired(invSTList.getLocationFrom()) &&
                      Common.validateRequired(invSTList.getLocationTo()) &&
          	 	      Common.validateRequired(invSTList.getItemName()) &&
          	 	      Common.validateRequired(invSTList.getQtyDelivered()) &&
          	 	      Common.validateRequired(invSTList.getUnit()) &&
          	 	      Common.validateRequired(invSTList.getUnitCost())) continue;
        	   
        	 InvModStockTransferLineDetails mdetails = new InvModStockTransferLineDetails();
        
        	 mdetails.setStlUnitCost(Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit));
        	 mdetails.setStlQuantityDelivered(Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), quantityPrecisionUnit));
        	 mdetails.setStlAmount(Common.convertStringMoneyToDouble(invSTList.getAmount(), quantityPrecisionUnit));
        	 mdetails.setStlUomName(invSTList.getUnit());
        	 mdetails.setStlLocationNameFrom(invSTList.getLocationFrom());
        	 mdetails.setStlLocationNameTo(invSTList.getLocationTo());
        	 mdetails.setStlIiName(invSTList.getItemName());
        	 mdetails.setStlLineNumber(Common.convertStringToShort(invSTList.getLineNumber()));
        	 mdetails.setStlMisc(invSTList.getMisc());
        	 stlList.add(mdetails);
        	
        }                     	
             	
        
        try {
        	
    	     Integer stockTransferCode = ejbST.saveInvStEntry(details, stlList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    	     actionForm.setStockTransferCode(stockTransferCode);
        	
        } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
       			new ActionMessage("invStockTransferEntry.error.documentNumberNotUnique"));    
       	
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	
       	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));       	                                  	                 	                  
                 	
       } catch (GlobalTransactionAlreadyApprovedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyApproved"));
       	
       } catch (GlobalTransactionAlreadyPendingException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyPending"));
       	
       } catch (GlobalTransactionAlreadyPostedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.transactionAlreadyPosted"));
                
       } catch (GlobalNoApprovalRequesterFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noApprovalRequesterFound"));
                
       } catch (GlobalNoApprovalApproverFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noApprovalApproverFound"));
       	   
       } catch (GlobalInvItemLocationNotFoundException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.noItemLocationFound", ex.getMessage()));
       	   
       } catch (GlJREffectiveDateNoPeriodExistException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.effectiveDateNoPeriodExist"));
                
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.effectiveDatePeriodClosed"));
                
       } catch (GlobalJournalNotBalanceException ex) {
       	
       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                new ActionMessage("invStockTransferEntry.error.journalNotBalance"));
       	   
       } catch (GlobalInventoryDateException ex) {
           
           errors.add(ActionMessages.GLOBAL_MESSAGE,
           		new ActionMessage("invStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
      	   
       } catch(GlobalBranchAccountNumberInvalidException ex) {
		
       		errors.add(ActionMessages.GLOBAL_MESSAGE,
      			new ActionMessage("invStockTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

       } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
      	
          errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("invStockTransferEntry.error.noNegativeInventoryCostingCOA"));

       } catch (GlobalRecordInvalidException ex) {

    	   System.out.println("D2 PUMASOKKKKK");
    	   errors.add(ActionMessages.GLOBAL_MESSAGE,
    			   new ActionMessage("buildUnbuildAssemblyPost.error.insufficientStocks", ex.getMessage()));
    	   
       }catch (EJBException ex) {
       	    if (log.isInfoEnabled()) {
           	
              log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
                 " session: " + session.getId());
            }
           
           return(mapping.findForward("cmnErrorPage"));
       }

/*******************************************************
   -- Inv ST Journal Action --
*******************************************************/

     } else if (request.getParameter("journalButton") != null &&
       actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
       	    
     	  if(Common.validateRequired(actionForm.getApprovalStatus())) {
     	
       	     InvStockTransferDetails details = new InvStockTransferDetails();
             
             details.setStCode(actionForm.getStockTransferCode());
             details.setStDocumentNumber(actionForm.getDocumentNumber());
             details.setStReferenceNumber(actionForm.getReferenceNumber());
             details.setStDescription(actionForm.getDescription());
             details.setStDate(Common.convertStringToSQLDate(actionForm.getDate()));       
             details.setStCreatedBy(actionForm.getCreatedBy());
             details.setStDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
             details.setStLastModifiedBy(actionForm.getLastModifiedBy());
             details.setStDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                     
             if (actionForm.getStockTransferCode() == null) {
              	
          		  details.setStCreatedBy(user.getUserName());
          	  	  details.setStDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                             
             }
                     
             details.setStLastModifiedBy(user.getUserName());
             details.setStDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
          
             ArrayList stlList = new ArrayList(); 
         
             for (int i = 0; i<actionForm.getInvSTListSize(); i++) {
          	
          	   InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);           	   
          	              	             	   
               if (Common.validateRequired(invSTList.getLocationFrom()) &&
                        Common.validateRequired(invSTList.getLocationTo()) &&
            	 	    Common.validateRequired(invSTList.getItemName()) &&
            	 	    Common.validateRequired(invSTList.getQtyDelivered()) &&
            	 	    Common.validateRequired(invSTList.getUnit()) &&
            	 	    Common.validateRequired(invSTList.getUnitCost())) continue;
          	   
          	   InvModStockTransferLineDetails mdetails = new InvModStockTransferLineDetails();
          	   
          	   mdetails.setStlUnitCost(Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit));
          	   mdetails.setStlQuantityDelivered(Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), quantityPrecisionUnit));
          	   mdetails.setStlAmount(Common.convertStringMoneyToDouble(invSTList.getAmount(), quantityPrecisionUnit));
          	   mdetails.setStlUomName(invSTList.getUnit());
          	   mdetails.setStlLocationNameFrom(invSTList.getLocationFrom());
          	   mdetails.setStlLocationNameTo(invSTList.getLocationTo());
          	   mdetails.setStlIiName(invSTList.getItemName());
          	   mdetails.setStlLineNumber(Common.convertStringToShort(invSTList.getLineNumber()));
          	   mdetails.setStlMisc(invSTList.getMisc());
          	   stlList.add(mdetails);
          	
            }  
	           
            try {
            	
        	    Integer stockTransferCode = ejbST.saveInvStEntry(details, stlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        	    actionForm.setStockTransferCode(stockTransferCode);
    
            } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invStockTransferEntry.error.documentNumberNotUnique"));    
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));       	                                  	                 	                  
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.transactionAlreadyPosted"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invStockTransferEntry.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
               		new ActionMessage("invStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
          	   
           } catch(GlobalBranchAccountNumberInvalidException ex) {
			
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invStockTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invStockTransferEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalRecordInvalidException ex) {
        	   
        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("buildUnbuildAssemblyPost.error.insufficientStocks", ex.getMessage()));
        	   
           }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

            
            if (!errors.isEmpty()) {
	        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("invStockTransferEntry"));
	               
	        }
            
     	}
        
        String path = "/invJournal.do?forward=1" + 
	     "&transactionCode=" + actionForm.getStockTransferCode() +
	     "&transaction=STOCK TRANSFER" + 
	     "&transactionNumber=" + actionForm.getReferenceNumber() + 
	     "&transactionDate=" + actionForm.getDate() +
	     "&transactionEnableFields=" + actionForm.getEnableFields();
        
        return(new ActionForward(path));   
        
/*******************************************************
    -- Inv ST Delete Action --
*******************************************************/

      } else if(request.getParameter("deleteButton") != null) {
      	
         try {
        	
        	    ejbST.deleteInvStEntry(actionForm.getStockTransferCode(), user.getUserName(), user.getCmpCode());
        	
         } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));
                 
         } catch (EJBException ex) {
         
        	    if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               
             }
            
             return(mapping.findForward("cmnErrorPage"));
             
         }      
         
/*******************************************************
     -- Inv ST Print Action --
*******************************************************/         
      } else if(request.getParameter("printButton") != null) {
          
          if (Common.validateRequired(actionForm.getApprovalStatus())) {
              
	     	  InvStockTransferDetails details = new InvStockTransferDetails();
	           
	          details.setStCode(actionForm.getStockTransferCode());
	          details.setStDocumentNumber(actionForm.getDocumentNumber());
	          details.setStReferenceNumber(actionForm.getReferenceNumber());
	          details.setStDescription(actionForm.getDescription());
	          details.setStDate(Common.convertStringToSQLDate(actionForm.getDate()));       
	          details.setStCreatedBy(actionForm.getCreatedBy());
	          details.setStDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
	          details.setStLastModifiedBy(actionForm.getLastModifiedBy());
	          details.setStDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
	                   
	          if (actionForm.getStockTransferCode() == null) {
	            	
	    		  details.setStCreatedBy(user.getUserName());
	    	  	  details.setStDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                           
	          }
	                   
	          details.setStLastModifiedBy(user.getUserName());
	          details.setStDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	        
	          ArrayList stlList = new ArrayList(); 
	       
	          for (int i = 0; i<actionForm.getInvSTListSize(); i++) {
	        	
	        	  InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);           	   
	        	              	             	   
	              if (Common.validateRequired(invSTList.getLocationFrom()) &&
                        Common.validateRequired(invSTList.getLocationTo()) &&
	          	 	    Common.validateRequired(invSTList.getItemName()) &&
	          	 	    Common.validateRequired(invSTList.getQtyDelivered()) &&
	          	 	    Common.validateRequired(invSTList.getUnit()) &&
	          	 	    Common.validateRequired(invSTList.getUnitCost())) continue;
	        	   
	        	   InvModStockTransferLineDetails mdetails = new InvModStockTransferLineDetails();
	        	   
	        	   mdetails.setStlUnitCost(Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit));
	        	   mdetails.setStlQuantityDelivered(Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), quantityPrecisionUnit));
	        	   mdetails.setStlAmount(Common.convertStringMoneyToDouble(invSTList.getAmount(), quantityPrecisionUnit));
	        	   mdetails.setStlUomName(invSTList.getUnit());
	        	   mdetails.setStlLocationNameFrom(invSTList.getLocationFrom());
	        	   mdetails.setStlLocationNameTo(invSTList.getLocationTo());
	        	   mdetails.setStlIiName(invSTList.getItemName());
	        	   mdetails.setStlLineNumber(Common.convertStringToShort(invSTList.getLineNumber()));
	        	   mdetails.setStlMisc(invSTList.getMisc());
	        	   stlList.add(mdetails);
	        	
	          }  
	           
	          try {
	          	
		      	   Integer stockTransferCode = ejbST.saveInvStEntry(details, stlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		      	   actionForm.setStockTransferCode(stockTransferCode);
	  
	          } catch (GlobalDocumentNumberNotUniqueException ex) {
	             	
	         		errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			new ActionMessage("invStockTransferEntry.error.documentNumberNotUnique"));    
	 	
			  } catch (GlobalRecordAlreadyDeletedException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));       	                                  	                 	                  
			           	
			  } catch (GlobalTransactionAlreadyApprovedException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.transactionAlreadyApproved"));
			 	
			  } catch (GlobalTransactionAlreadyPendingException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.transactionAlreadyPending"));
			 	
			  } catch (GlobalTransactionAlreadyPostedException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.transactionAlreadyPosted"));
			          
			  } catch (GlobalNoApprovalRequesterFoundException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.noApprovalRequesterFound"));
			          
			  } catch (GlobalNoApprovalApproverFoundException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.noApprovalApproverFound"));
			 	    
			  } catch (GlobalInvItemLocationNotFoundException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.noItemLocationFound", ex.getMessage()));
			 	   
			  } catch (GlJREffectiveDateNoPeriodExistException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.effectiveDateNoPeriodExist"));
			          
			  } catch (GlJREffectiveDatePeriodClosedException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.effectiveDatePeriodClosed"));
			          
			  } catch (GlobalJournalNotBalanceException ex) {
			 	
			 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			            new ActionMessage("invStockTransferEntry.error.journalNotBalance"));
			 	    
			  } catch (GlobalInventoryDateException ex) {
		           
		           errors.add(ActionMessages.GLOBAL_MESSAGE,
		           		new ActionMessage("invStockTransferEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

			  } catch(GlobalBranchAccountNumberInvalidException ex) {
				
	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invStockTransferEntry.error.branchAccountNumberInvalid", ex.getMessage()));

			  } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
				  
				  errors.add(ActionMessages.GLOBAL_MESSAGE,
						  new ActionMessage("invStockTransferEntry.error.noNegativeInventoryCostingCOA"));
				  
			  } catch (GlobalRecordInvalidException ex) {
				  
				  errors.add(ActionMessages.GLOBAL_MESSAGE,
						  new ActionMessage("invStockTransferEntry.error.insufficientStocks", ex.getMessage()));
				  
			  }catch (EJBException ex) {
				  
				  if (log.isInfoEnabled()) {
					  
					  log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
							  " session: " + session.getId());
				  }
				  
				  return(mapping.findForward("cmnErrorPage"));
			  }
			
			  
			  if (!errors.isEmpty()) {
				
			       saveErrors(request, new ActionMessages(errors));
			       return(mapping.findForward("invStockTransferEntry"));
			       
			  }
		                  
			  actionForm.setReport(Constants.STATUS_SUCCESS);
        		
              isInitialPrinting = true;
                          
          } else {
      		
        	  actionForm.setReport(Constants.STATUS_SUCCESS);
        		
        	  return(mapping.findForward("invStockTransferEntry"));
        		
          }
                
      
/*******************************************************
     -- Inv ST Close Action --
*******************************************************/
	
	   } else if(request.getParameter("closeButton") != null) {
	   	
	      return(mapping.findForward("cmnMain"));
	      
/*******************************************************
      -- Inv ST Add Lines Action --
*******************************************************/
	
	    } else if(request.getParameter("addLinesButton") != null) {
	    	         	         	
	    	int listSize = actionForm.getInvSTListSize();
	    	int lineNumber = 0;
	                              
	       for (int x = listSize + 1; x <= listSize + stockTransferLineNumber; x++) {
	    	
		       	ArrayList comboItem = new ArrayList();
		       	
		    	InvStockTransferEntryList invSTList = new InvStockTransferEntryList(actionForm, 
		    	    null, new Integer(++lineNumber).toString(), null, null, null, null, null, 
					null, null, null, null);	
		    		        	    
		    	invSTList.setLocationList(actionForm.getLocationList());
		    	
		    	invSTList.setUnitList(Constants.GLOBAL_BLANK);
		    	invSTList.setUnitList("Select Item First");
		    	
		    	actionForm.saveInvSTList(invSTList);
	    	
		    }		                    
		       
		    for (int i = 0; i<actionForm.getInvSTListSize(); i++ ) {
	      	
	      	   InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);
	      	   
	      	   invSTList.setLineNumber(new Integer(i+1).toString());
	      	   
	        }        
	    
	        return(mapping.findForward("invStockTransferEntry"));
	        
/*******************************************************
    -- Inv ST Delete Lines Action --
*******************************************************/

      } else if(request.getParameter("deleteLinesButton") != null) {
      	
      	for (int i = 0; i<actionForm.getInvSTListSize(); i++) {
        	
    	   InvStockTransferEntryList invSTList = actionForm.getInvSTByIndex(i);
    	   
    	   if (invSTList.isDeleteCheckbox()) {
    	   	
    	   		actionForm.deleteInvSTList(i);
    	   		i--;
    	   }
    	   
         }
         	        
        return(mapping.findForward("invStockTransferEntry"));  
        
/*******************************************************
    -- Inv ST Item Enter Action --
*******************************************************/
        
      } else if(!Common.validateRequired(request.getParameter("invSTList[" + 
      		actionForm.getRowSelected() + "].isItemEntered"))) {
      	
      	InvStockTransferEntryList invSTList = 
      		actionForm.getInvSTByIndex(actionForm.getRowSelected());      	    	
      	
      	try {
      		
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbST.getInvUomByIiName(invSTList.getItemName(), user.getCmpCode());
      		
      		invSTList.clearUnitList();
      		invSTList.setUnitList(Constants.GLOBAL_BLANK);
      		
      		Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			invSTList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				invSTList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
          	try {
          		
          		// populate unit cost field
          		
          		if (!Common.validateRequired(invSTList.getItemName()) && !Common.validateRequired(invSTList.getUnit())) {
          			
          			
          			double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invSTList.getItemName(), invSTList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
          			/*
          			double unitCost = ejbST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invSTList.getItemName(), invSTList.getLocationFrom(), 
          					invSTList.getUnit(), Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
          			
          			*/
          			invSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
          			
          		}
          		
          		// populate amount field
          		
          		if (!Common.validateRequired(invSTList.getUnitCost()) && !Common.validateRequired(invSTList.getQtyDelivered())) {
          			
          			double amount = Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), precisionUnit) * Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit);
          			invSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));	        	
          			
          		}

      
          	
          	} catch (EJBException ex) {
          		
          		if (log.isInfoEnabled()) {
          			
          			log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
          					" session: " + session.getId());
          			return mapping.findForward("cmnErrorPage"); 
          			
          		}
          		
          	}
      		
      	} catch (EJBException ex) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());
      			return mapping.findForward("cmnErrorPage"); 
      			
      		}
      		
      	}  
      	
      	return(mapping.findForward("invStockTransferEntry"));
      	
/*******************************************************
    Inv ST Location Enter Action --
*******************************************************/
      	
      } else if(!Common.validateRequired(request.getParameter("invSTList[" + 
      		actionForm.getRowSelected() + "].isLocationEntered"))) {
      	
      	InvStockTransferEntryList invSTList = 
      		actionForm.getInvSTByIndex(actionForm.getRowSelected());      	    	
      	
      	try {
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(invSTList.getItemName()) && !Common.validateRequired(invSTList.getUnit())) {
      			
      			
      			double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invSTList.getItemName(), invSTList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                

      			/*
      			double unitCost = ejbST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invSTList.getItemName(), invSTList.getLocationFrom(), 
      					invSTList.getUnit(), Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			*/
      			
      			invSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
      			
      		}
      		
      		// populate amount field
      		
      		if (!Common.validateRequired(invSTList.getUnitCost()) && !Common.validateRequired(invSTList.getQtyDelivered())) {
      			
      			double amount = Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), precisionUnit) * Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit);
      			invSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));	        	
      			
      		}

      	
      	
      	} catch (EJBException ex) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());
      			return mapping.findForward("cmnErrorPage"); 
      			
      		}
      		
      	}  
      	
      	return(mapping.findForward("invStockTransferEntry"));
      	
/*******************************************************
   Inv ST Unit Enter Action --
*******************************************************/
      	
      } else if(!Common.validateRequired(request.getParameter("invSTList[" + 
      		actionForm.getRowSelected() + "].isUnitEntered"))) {
      	
      	InvStockTransferEntryList invSTList = 
      		actionForm.getInvSTByIndex(actionForm.getRowSelected());
      	
      	try {
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(invSTList.getItemName()) && !Common.validateRequired(invSTList.getUnit())
      				&& !Common.validateRequired(invSTList.getLocationFrom())) {	    		    
      			
      			double unitCost = ejbST.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invSTList.getItemName(), invSTList.getLocationFrom(), 
      					invSTList.getUnit(), Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			invSTList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
      				
      		}
      		
      		// populate amount field
      		
      		if (!Common.validateRequired(invSTList.getQtyDelivered()) && !Common.validateRequired(invSTList.getUnitCost())) {
      			
      			double amount = Common.convertStringMoneyToDouble(invSTList.getQtyDelivered(), precisionUnit) * Common.convertStringMoneyToDouble(invSTList.getUnitCost(), precisionUnit);
      			invSTList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
      			
      		}
      		
      	} catch (EJBException ex) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());
      			return mapping.findForward("cmnErrorPage"); 
      			
      		}
      		
      	}  	
      	
      	return(mapping.findForward("invStockTransferEntry"));
      	
/*******************************************************
      -- Inv ST Load Action --
*******************************************************/
	
	    }
	    
	    if (frParam != null) {
	    	
	    	// Errors on saving
	    	
	       if (!errors.isEmpty()) {
	       	
	          saveErrors(request, new ActionMessages(errors));
	          return(mapping.findForward("invStockTransferEntry"));
	          
	       }
	       
	       if (request.getParameter("forward") == null &&
	           actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
	           	                	
	           errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
	           saveErrors(request, new ActionMessages(errors));
	         
	           return mapping.findForward("cmnMain");	
	           
	       }
	       
	       try {
	       	
	       	// location combo box
	       	
	       	ArrayList list = null;
	       	Iterator i = null;
	       	
	       	actionForm.clearLocationList();       	
	       	
	       	list = ejbST.getInvLocAll(user.getCmpCode());
	       	
	       	if (list == null || list.size() == 0) {
	       		
	       		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	       		
	       	} else {
	       		           		            		
	       		i = list.iterator();
	       		
	       		while (i.hasNext()) {
	       			
	       		    actionForm.setLocationList((String)i.next());
	       			
	       		}
	       		            		
	       	}            	            
	       	
	       	actionForm.clearInvSTList();               	  	
	       	
	       	if (request.getParameter("forward") != null || isInitialPrinting ) {
	       	    
	       	    if (request.getParameter("forward") != null) {
	
	       	        actionForm.setStockTransferCode(new Integer(request.getParameter("stockTransferCode")));
	       	        
	       	    }
	       		
	       		InvModStockTransferDetails mdetails = ejbST.getInvStByStCode(actionForm.getStockTransferCode(), user.getCmpCode());            	
	       		
	       		actionForm.setDocumentNumber(mdetails.getStDocumentNumber());
	       		actionForm.setReferenceNumber(mdetails.getStReferenceNumber());
	       		actionForm.setDescription(mdetails.getStDescription());
	       		actionForm.setDate(Common.convertSQLDateToString(mdetails.getStDate()));	       		
	       		actionForm.setApprovalStatus(mdetails.getStApprovalStatus());
	       		actionForm.setPosted(mdetails.getStPosted() == 1 ? "YES" : "NO");
	       		actionForm.setCreatedBy(mdetails.getStCreatedBy());
	       		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getStDateCreated()));
	       		actionForm.setLastModifiedBy(mdetails.getStLastModifiedBy());
	       		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getStDateLastModified()));
	       		actionForm.setApprovedRejectedBy(mdetails.getStApprovedRejectedBy());
	       		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getStDateApprovedRejected()));
	       		actionForm.setPostedBy(mdetails.getStPostedBy());
	       		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getStDatePosted()));	       		
	       		actionForm.setReasonForRejection(mdetails.getStReasonForRejection());
	       		
	       		list = mdetails.getStStlList();           		
	       		         		            		            		
	            i = list.iterator();
	            
	            int lineNumber = 0;
	            
	            while (i.hasNext()) {
	            	
	            	InvModStockTransferLineDetails mStlDetails = (InvModStockTransferLineDetails)i.next();		            			            			            	
	            	
	            	InvStockTransferEntryList stStlList = new InvStockTransferEntryList(actionForm,
	            	    mStlDetails.getStlCode(), new Integer(++lineNumber).toString(), 
	            	    mStlDetails.getStlLocationNameFrom(), mStlDetails.getStlLocationNameTo(), 
	            	    mStlDetails.getStlIiName(), mStlDetails.getStlIiDescription(), 
	            	    Common.convertDoubleToStringMoney(mStlDetails.getStlQuantityDelivered(),quantityPrecisionUnit),						
	            	    mStlDetails.getStlUomName(), 
						Common.convertDoubleToStringMoney(mStlDetails.getStlUnitCost(), precisionUnit),
						Common.convertDoubleToStringMoney(mStlDetails.getStlAmount(), precisionUnit),
						mStlDetails.getStlMisc());		            	
					
	            	stStlList.setLocationList(actionForm.getLocationList());            	
	            	            	
	            	stStlList.clearUnitList();
	            	ArrayList unitList = ejbST.getInvUomByIiName(mStlDetails.getStlIiName(), user.getCmpCode());
	            	Iterator unitListIter = unitList.iterator();
	            	while (unitListIter.hasNext()) {
	            		
	            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
	            		stStlList.setUnitList(mUomDetails.getUomName()); 
	            		
	            	}		            			            			            	
	            	
	            	actionForm.saveInvSTList(stStlList);				         
			      
		        }	
		        			        			        
		        int remainingList = stockTransferLineNumber - (list.size() % stockTransferLineNumber) + list.size();
		        
		        for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ArrayList comboItem = new ArrayList();
		        	
		        	InvStockTransferEntryList invSTList = new InvStockTransferEntryList(actionForm, 
			        	    null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, null, null);	
	
		        	invSTList.setLocationList(actionForm.getLocationList());
		        			        	
		        	invSTList.setUnitList(Constants.GLOBAL_BLANK);
		        	invSTList.setUnitList("Select Item First");
		        	
		        	actionForm.saveInvSTList(invSTList);
		        	
		         }	
		         
		         this.setFormProperties(actionForm);
		         
		         if (request.getParameter("child") == null) {
		         
		         	return (mapping.findForward("invStockTransferEntry"));         
		         	
		         } else {
		         	
		         	return (mapping.findForward("invStockTransferEntryChild"));         
		         	
		         }
	       		
	        }     
	       	
	       	// Populate line when not forwarding
	       	
	       	for (int x = 1; x <= stockTransferLineNumber; x++) {
		            		
	       		InvStockTransferEntryList invSTList = new InvStockTransferEntryList(actionForm, 
		        	    null, new Integer(x).toString(), null, null, null, null, null, 
						null, null, null, null);	
	
	       		invSTList.setLocationList(actionForm.getLocationList());
	        	
	       		invSTList.setUnitList(Constants.GLOBAL_BLANK);
	       		invSTList.setUnitList("Select Item First");
	        	
	        	actionForm.saveInvSTList(invSTList);
		    	
	        }       	  	        
	                            	           			
	       		           			   	
	       } catch(GlobalNoRecordFoundException ex) {
	       	            	
	       		errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("invStockTransferEntry.error.recordAlreadyDeleted"));
	       		
	       } catch(EJBException ex) {
	 	
	          if (log.isInfoEnabled()) {
	          	
	             log.info("EJBException caught in invStockTransferEntryAction.execute(): " + ex.getMessage() +
	                " session: " + session.getId());
	          }
	          
	          return(mapping.findForward("cmnErrorPage"));
	          
	      } 
	       
	       // Errors on loading
	    
	       if (!errors.isEmpty()) {
	     	
	          saveErrors(request, new ActionMessages(errors));
	        
	       } else {
	     	
	          if (request.getParameter("saveSubmitButton") != null && 
	             actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	             	
	              try {
	              	
	              		ArrayList list = ejbST.getAdApprovalNotifiedUsersByAdjCode(actionForm.getStockTransferCode(), user.getCmpCode());                   	
	              	                      	   
	              	   if (list.isEmpty()) {
	              	   	   
	              	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
	              	   	       new ActionMessage("messages.documentSentForPosting"));
	              	   	       
	              	   } else if (list.contains("DOCUMENT POSTED")) {
	              	   	
	              	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
	           	   	       new ActionMessage("messages.documentPosted"));     
	              	   	                      	   	   
	              	   } else {
	              	   	
	              	   	   Iterator i = list.iterator();
	              	   	   
	              	   	   String APPROVAL_USERS = "";
	              	   	   
	              	   	   while (i.hasNext()) {
	              	   	   	                   	   	   	                   	   	   	                      	   	   	   	
	              	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
	              	   	       
	              	   	       if (i.hasNext()) {
	              	   	       	
	              	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
	              	   	       	
	              	   	       }
	              	   	                          	   	       
	              	   	   	   	                   	   	   	                      	   	   	
	              	   	   }
	              	   	   
	              	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
	              	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
	              	   	
	              	   }
	              	   
	              	   saveMessages(request, messages);
	              	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	              	
	             	
	              } catch(EJBException ex) {
	 	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvStockTransferEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	              
	          } else if (request.getParameter("saveAsDraftButton") != null && 
	             actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	             	
	             actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	             	
	          }
	
	          
	       }
	       
	       actionForm.reset(mapping, request);              
	
	       actionForm.setStockTransferCode(null);
	       actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
	       actionForm.setPosted("NO");
	       actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
	       actionForm.setCreatedBy(user.getUserName());
	       actionForm.setLastModifiedBy(user.getUserName());
	       actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));                        
	       
	       this.setFormProperties(actionForm);
	       return(mapping.findForward("invStockTransferEntry"));
	
	    } else {
	    	
	       errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	       saveErrors(request, new ActionMessages(errors));
	
	       return(mapping.findForward("cmnMain"));
	               
	     }
	            
   } catch(Exception e) {

/*******************************************************
      System Failed: Forward to error page 
*******************************************************/
	
	if (log.isInfoEnabled()) {
	
	   log.info("Exception caught in InvStockTransferEntryAction.execute(): " + e.getMessage()
	      + " session: " + session.getId());
	}
	
	e.printStackTrace();
	return(mapping.findForward("cmnErrorPage"));
	        
 }
   
}


private void setFormProperties(InvStockTransferEntryForm actionForm) {

	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
		
		if (actionForm.getPosted().equals("NO")) {
			
			if (actionForm.getStockTransferCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
				
			} else if (actionForm.getStockTransferCode() != null &&
			    Common.validateRequired(actionForm.getApprovalStatus())) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
			    
			    	
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				
			}
			
		} else {
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			
		}
		
		
		
	} else {
		
		
		actionForm.setEnableFields(false);
		actionForm.setShowSaveButton(false);
		actionForm.setShowDeleteButton(false);
		actionForm.setShowAddLinesButton(false);
		actionForm.setShowDeleteLinesButton(false);
		
	}
    
}
	   		
}
