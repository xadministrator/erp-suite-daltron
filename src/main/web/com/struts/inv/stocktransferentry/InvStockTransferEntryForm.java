package com.struts.inv.stocktransferentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;

public class InvStockTransferEntryForm extends ActionForm implements Serializable { 
    
    private Integer stockTransferCode = null;		
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;	
        
        
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String locationFrom = null;
	private String locationTo = null;
	private ArrayList locationList = new ArrayList();
	private String reasonForRejection = null;
   
	private ArrayList invSTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	
	private String report = null;
	
	public String getReport() {	    
	    return report;	    
	}
	
	public void setReport(String report) {
	    this.report = report;	    
	}

	public int getRowSelected(){
		return rowSelected;
	}

	public InvStockTransferEntryList getInvSTByIndex(int index){
		return((InvStockTransferEntryList)invSTList.get(index));
	}
	
	public Object[] getInvSTList(){
		return(invSTList.toArray());
	}

	public int getInvSTListSize(){
		return(invSTList.size());
	}

	public void saveInvSTList(Object newInvSTList){
		invSTList.add(newInvSTList);
	}

	public void clearInvSTList(){
		invSTList.clear();
	}

	public void setRowSelected(Object selectedInvSTList, boolean isEdit){
		this.rowSelected = invSTList.indexOf(selectedInvSTList);
	}

	public void updateInvSTRow(int rowSelected, Object newInvSTList){
		invSTList.set(rowSelected, newInvSTList);
	}

	public void deleteInvSTList(int rowSelected){
		invSTList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}     	
	
	public Integer getStockTransferCode() {
		return stockTransferCode;
	}
	
	public void setStockTransferCode(Integer stockTransferCode) {
		this.stockTransferCode = stockTransferCode;
	}
	
	public String getApprovalStatus() {
		return approvalStatus;
	}
	
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}		
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public boolean getEnableFields() {
		return enableFields;
	}
	
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getLocationFrom() {
		return locationFrom;
	}
	
	public void setLocationFrom(String locationFrom) {
		this.locationFrom = locationFrom;
	}
	
	public String getLocationTo() {
		return locationTo;
	}
	
	public void setLocationTo(String locationTo) {
		this.locationTo = locationTo;
	}	
	
	public ArrayList getLocationList() {
		return locationList;
	}
	
	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}
	
	public void clearLocationList() {
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getReasonForRejection() {
	   	
	   	  return reasonForRejection;
	   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
	
	public String getPosted() {
		return posted;
	}
	
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean isShowSaveButton() {
		return showSaveButton;
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		this.showSaveButton = showSaveButton;
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       locationFrom = Constants.GLOBAL_BLANK;
       locationTo = Constants.GLOBAL_BLANK;
	   date = null;
	   documentNumber = null;
	   referenceNumber = null;
	   description = null;	   
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   reasonForRejection = null;
	   
	   for (int i=0; i<invSTList.size(); i++) {
	  	
	  	  InvStockTransferEntryList actionList = (InvStockTransferEntryList)invSTList.get(i);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsUnitEntered(null);
	  	  actionList.setIsLocationEntered(null);
	  	
	  } 


   }

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";
		   
		   // Remove first $ character
		   misc = misc.substring(1);
		   
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;	
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);
		   
		   /*if(y==0)
			   return new ArrayList();*/
		   
		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {	        	

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);	 
				   }else{
					   miscList.add("null");
				   }
				   
				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   ex.printStackTrace();
			   }

		   	   }
		   return miscList;
	   }
	
   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
		 request.getParameter("journalButton") != null ||
		 request.getParameter("printButton") != null) { 
      	      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("invStockTransferEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	        errors.add("date", 
		       new ActionMessage("invStockTransferEntry.error.dateInvalid"));
		 }	 	         		 		 
         
         int numberOfLines = 0;
      	 
      	 Iterator i = invSTList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvStockTransferEntryList ilList = (InvStockTransferEntryList)i.next();      	 	 
			 
      	 	 if (Common.validateRequired(ilList.getLocationFrom()) &&
      	 	     Common.validateRequired(ilList.getLocationTo()) &&
      	 	     Common.validateRequired(ilList.getItemName()) &&
      	 	     Common.validateRequired(ilList.getQtyDelivered()) &&
      	 	     Common.validateRequired(ilList.getUnit()) &&
      	 	     Common.validateRequired(ilList.getUnitCost())) continue;
      	 	     
      	 	 numberOfLines++;
      	 	 /*
      	 	try{
      	 		String separator = "$";
      	 		String misc = "";
      		   // Remove first $ character
      		   misc = ilList.getMisc().substring(1);
      		   
      		   // Counter
      		   int start = 0;
      		   int nextIndex = misc.indexOf(separator, start);
      		   int length = nextIndex - start;	
      		   int counter;
      		   counter = Integer.parseInt(misc.substring(start, start + length));
      		   
      	 		ArrayList miscList = this.getExpiryDateStr(ilList.getMisc(), counter);
      	 		System.out.println("rilList.getMisc() : " + ilList.getMisc());
      	 		Iterator mi = miscList.iterator();
     	 		
      	 		int ctrError = 0;
      	 		int ctr = 0;
      	 		
      	 		while(mi.hasNext()){
      	 				String miscStr = (String)mi.next();
      	 			    
      	 				if(!Common.validateDateFormat(miscStr)){
      	 					errors.add("date", 
      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
      	 					ctrError++;
      	 				}
      	 				
      	 				System.out.println("miscStr: "+miscStr);
      	 				if(miscStr=="null"){
      	 					ctrError++;
      	 				}
      	 		}
      	 		//if ctr==Error => No Date Will Apply
      	 		//if ctr>error => Invalid Date
      	 		System.out.println("CTR: "+  miscList.size());
      	 		System.out.println("ctrError: "+  ctrError);
      	 		System.out.println("counter: "+  counter);
      	 			if(ctrError>0 && ctrError!=miscList.size()){
      	 				errors.add("date", 
      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
      	 			}
      	 			
      	 		//if error==0 Add Expiry Date
      	 		
      	 		
      	 	}catch(Exception ex){
  	  	    	ex.printStackTrace();
  	  	    }
  	  	    */
      	 
	         if(Common.validateRequired(ilList.getLocationFrom()) || ilList.getLocationFrom().equals(Constants.GLOBAL_BLANK)){
	            errors.add("locationFrom",
	            	new ActionMessage("invStockTransferEntry.error.locationFromRequired", ilList.getLineNumber()));
	         }
	         if(Common.validateRequired(ilList.getLocationTo()) || ilList.getLocationTo().equals(Constants.GLOBAL_BLANK)){
	            errors.add("locationTo",
	            	new ActionMessage("invStockTransferEntry.error.locationToRequired", ilList.getLineNumber()));
		     }
	         if(ilList.getLocationFrom().equals(ilList.getLocationTo())) {
	            errors.add("location",
	                new ActionMessage("invStockTransferEntry.error.locationMustNotBeTheSame", ilList.getLineNumber()));
	         }
	         if(Common.validateRequired(ilList.getItemName()) || ilList.getItemName().equals(Constants.GLOBAL_BLANK)){
	            errors.add("itemName",
	            	new ActionMessage("invStockTransferEntry.error.itemNameRequired", ilList.getLineNumber()));
	         }
	         if(Common.validateRequired(ilList.getQtyDelivered())) {
	         	errors.add("qtyDelivered",
	         		new ActionMessage("invStockTransferEntry.error.qtyDeliveredRequired", ilList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(ilList.getQtyDelivered())){
	            errors.add("qtyDelivered",
	               new ActionMessage("invStockTransferEntry.error.qtyDeliveredInvalid", ilList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(ilList.getQtyDelivered()) && Common.convertStringMoneyToDouble(ilList.getQtyDelivered(), (short)3) == 0) {
		        errors.add("qtyDelivered",
		           new ActionMessage("invStockTransferEntry.error.zeroQuantityNotAllowed", ilList.getLineNumber()));
		     }
		 	 if(!Common.validateRequired(ilList.getQtyDelivered()) && Common.convertStringMoneyToDouble(ilList.getQtyDelivered(), (short)3) < 0) {
		        errors.add("qtyDelivered",
		           new ActionMessage("invStockTransferEntry.error.negativeQuantityNotAllowed", ilList.getLineNumber()));
		     }
		 	 if(Common.validateRequired(ilList.getUnit()) || ilList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("invStockTransferEntry.error.unitRequired", ilList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(ilList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invStockTransferEntry.error.unitCostRequired", ilList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(ilList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invStockTransferEntry.error.unitCostInvalid", ilList.getLineNumber()));
	         }	        
       
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("stockTransfer",
               new ActionMessage("invStockTransferEntry.error.stockTransferMustHaveLine"));
         	
        }	  	    
	    
	    	    	 
      } 
      
      return(errors);	
   }
}

