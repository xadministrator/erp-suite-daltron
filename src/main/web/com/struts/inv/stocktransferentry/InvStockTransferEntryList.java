package com.struts.inv.stocktransferentry;

import java.io.Serializable;
import java.util.ArrayList;

public class InvStockTransferEntryList implements Serializable {
    
    private Integer stockTransferLineCode = null;
	private String lineNumber = null;
	private String locationFrom = null;
	private String locationTo = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String qtyDelivered = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private boolean deleteCheckbox = false;
   
	private String isItemEntered = null;	
	private String isLocationEntered = null;	
	private String isUnitEntered = null;
	
	private String misc = null;
    
	private InvStockTransferEntryForm parentBean;   

	public InvStockTransferEntryList(InvStockTransferEntryForm parentBean, 
		Integer stockTransferLineCode, String lineNumber, String locationFrom,
		String locationTo, String itemName, String itemDescription, String qtyDelivered,
		String unit, String unitCost, String amount, String misc) {
	    
	    this.parentBean = parentBean;
	    this.stockTransferLineCode = stockTransferLineCode;
	    this.lineNumber = lineNumber;
	    this.locationFrom = locationFrom;
	    this.locationTo = locationTo;
	    this.itemName = itemName;
	    this.itemDescription = itemDescription;
	    this.qtyDelivered = qtyDelivered;
	    this.unit = unit;
	    this.unitCost = unitCost;
	    this.amount = amount;
	    this.misc = misc;
	    
	}
	
	public InvStockTransferEntryForm getParentBean() {
	    
	    return parentBean;
	    
	}
	
	public void setParentBean(InvStockTransferEntryForm parentBean) {
	    
	    this.parentBean = parentBean;
	    
	}
	
	public Integer getStockTransferLineCode() {
	    
	    return stockTransferLineCode;
	    
	}
	
	public void setStockTransferLineCode(Integer stockTransferLineCode) {
	    
	    this.stockTransferLineCode = stockTransferLineCode;
	    
	}
	
	public String getLineNumber() {
	    
	    return lineNumber;
	    
	}
	
	public void setLineNumber(String lineNumber) {
	    
	    this.lineNumber = lineNumber;
	    
	}
	
	public String getLocationFrom() {
	    
	    return locationFrom;
	    
	}
	
	public void setLocationFrom(String locationFrom) {
	    
	    this.locationFrom = locationFrom;
	    
	}
	
	public String getLocationTo() {
	    
	    return locationTo;
	    
	}
	
	public void setLocationTo(String locationTo) {
	    
	    this.locationTo = locationTo;
	    
	}
	
	public String getItemName() {
	    
	    return itemName;
	    
	}
	
	public void setItemName(String itemName) {
	    
	    this.itemName = itemName;
	    
	}
	
	public String getItemDescription() {
	    
	    return itemDescription;
	    
	}
	
	public void setItemDescription(String itemDescription) {
	    
	    this.itemDescription = itemDescription;
	    
	}
		
	public String getQtyDelivered() {
	    
	    return qtyDelivered;
	    
	}
	
	public void setQtyDelivered(String qtyDelivered) {
	    
	    this.qtyDelivered = qtyDelivered;
	    
	}
	
	public String getUnit() {
	    
	    return unit;
	    
	}
	
	public void setUnit(String unit) {
	    
	    this.unit = unit;
	    
	}
	
	public String getUnitCost() {
	    
	    return unitCost;
	    
	}
	
	public void setUnitCost(String unitCost) {
	    
	    this.unitCost = unitCost;
	    
	}
	
	public String getAmount() {
	    
	    return amount;
	    	    
	}
	
	public void setAmount(String amount) {
	    
	    this.amount = amount;
	}
	
	public ArrayList getLocationList() {
	    
	    return locationList;
	    
	}
	
	public void setLocationList(ArrayList locationList) {
	    
	    this.locationList = locationList;
	    
	}
	
	public ArrayList getUnitList() {
	    
	    return unitList;
	    
	}
	
	public void setUnitList(String unit) {
	    
	    unitList.add(unit);
	    
	}
	
	public void clearUnitList() {
	    
	    unitList.clear();
	    
	}
	
	public boolean isDeleteCheckbox() {
	    
	    return deleteCheckbox;
	    
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
	    
	    this.deleteCheckbox = deleteCheckbox;
	    
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getIsLocationEntered() {
		
		return isLocationEntered;
		
	}
	
	public void setIsLocationEntered(String isLocationEntered) {
		
		if (isLocationEntered != null && isLocationEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isLocationEntered = null;
		
	}
					
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowSelected(this, false);
			
		}
		
		isUnitEntered = null;
		
	}
	
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}
}
