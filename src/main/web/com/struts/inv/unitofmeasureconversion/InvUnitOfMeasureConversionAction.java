package com.struts.inv.unitofmeasureconversion;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvUnitOfMeasureConversionController;
import com.ejb.txn.InvUnitOfMeasureConversionControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvModUnitOfMeasureConversionDetails;

public final class InvUnitOfMeasureConversionAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvUnitOfMeasureConversionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvUnitOfMeasureConversionForm actionForm = (InvUnitOfMeasureConversionForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_UNIT_OF_MEASURE_CONVERSION_ID);
			
			if (request.getParameter("userPermission") != null && 
					request.getParameter("userPermission").equals(Constants.QUERY_ONLY)) {
				
				actionForm.setUserPermission(Constants.QUERY_ONLY);
				
			} else {
				
				if (frParam != null) {
					
   					if (frParam.trim().equals(Constants.FULL_ACCESS)) {
   						
   						ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
   						if (!fieldErrors.isEmpty()) {
   							
   							saveErrors(request, new ActionMessages(fieldErrors));
   							
   							return mapping.findForward("invUnitOfMeasureConversion");
   						}
   						
   					}
   					
   					actionForm.setUserPermission(frParam.trim());
   					
   				} else {
   					
   					actionForm.setUserPermission(Constants.NO_ACCESS);
   					
   				}
   	        	
   	        }
			
/*******************************************************
Initialize InvUnitOfMeasureConversionController EJB
*******************************************************/
			
			InvUnitOfMeasureConversionControllerHome homeUMC = null;
			InvUnitOfMeasureConversionController ejbUMC = null;
			
			try {
				
				homeUMC = (InvUnitOfMeasureConversionControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvUnitOfMeasureConversionControllerEJB", InvUnitOfMeasureConversionControllerHome.class);
				
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvUnitOfMeasureConversionAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbUMC = homeUMC.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvUnitOfMeasureConversionAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
   Call InvItemEntryController EJB
   getAdPrfInvQuantityPrecisionUnit
*******************************************************/			
			
			short quantityPrecisionUnit = 0;
			
			try {
				
				quantityPrecisionUnit = ejbUMC.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvUnitOfMeasureConversionAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}			
			
/*******************************************************
   -- Inv UMC Save Action --
*******************************************************/
			
			if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArrayList umcList = new ArrayList();
				
				for (int i=0; i<actionForm.getInvUMCListSize(); i++) {
					
					InvUnitOfMeasureConversionList actionList = actionForm.getInvUMCByIndex(i);
					
					InvModUnitOfMeasureConversionDetails details = new InvModUnitOfMeasureConversionDetails();
					
					details.setUomName(actionList.getUomName());
					details.setUomShortName(actionList.getUomShortName());
					details.setUomAdLvClass(actionList.getUomAdLvClass());
					details.setUmcConversionFactor(Common.convertStringMoneyToDouble(actionList.getConversionFactor(), (short)8));
					details.setUmcBaseUnit(Common.convertBooleanToByte(actionList.getBaseUnit()));
					
					umcList.add(details);        			
				
				}
				
				try {
					
					ejbUMC.saveInvUMCEntry(actionForm.getItemCode(), umcList, user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasureConversion.error.noItemFound"));                
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvUnitOfMeasureConversionAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
				}	
				
/*******************************************************
  -- Inv UMC Close Action --
*******************************************************/
			
		} else if (request.getParameter("closeButton") != null) {
			
			return(mapping.findForward("cmnMain"));
			
/*******************************************************
  -- Inv UMC Back Action --
*******************************************************/
			
		} else if (request.getParameter("backButton") != null) {
			
			String path = null;
			
			if(actionForm.getItemClass().equals("Stock")) {
				
				path = "/invItemEntry.do?forward=1&iiCode=" + actionForm.getItemCode();
				
			} else if(actionForm.getItemClass().equals("Assembly")) {
				
				path = "/invAssemblyItemEntry.do?forward=1&iiCode=" + actionForm.getItemCode();
				
			}
			
			return(new ActionForward(path));
			
		} 
		
/*******************************************************
   -- Inv UMC Load Action --
*******************************************************/
		
		if (frParam != null) {
			
			if (!errors.isEmpty()) {
				
				saveErrors(request, new ActionMessages(errors));
				return mapping.findForward("invUnitOfMeasureConversion");
				
			}
			
			try {
				
				actionForm.clearInvUMCList();
				
				if (request.getParameter("forward") != null) {
					
					actionForm.setItemCode(new Integer(request.getParameter("itemCode")));
					actionForm.setItemClass(request.getParameter("itemClass"));
					actionForm.setItemName(request.getParameter("itemName"));
					actionForm.setItemDescription(request.getParameter("itemDescription"));
					actionForm.setUnitMeasure(request.getParameter("unitMeasure"));
					
				} 
				
				ArrayList list = ejbUMC.getInvUmcByIiCode(actionForm.getItemCode(), user.getCmpCode());
				Iterator i = list.iterator();        		       		
				
				while (i.hasNext()) {
					
					InvModUnitOfMeasureConversionDetails mdetails = (InvModUnitOfMeasureConversionDetails)i.next();
					
					InvUnitOfMeasureConversionList invUMCList = new InvUnitOfMeasureConversionList(actionForm,
							mdetails.getUomName(), mdetails.getUomShortName(), mdetails.getUomAdLvClass(),
							Common.convertDoubleToStringMoney(mdetails.getUmcConversionFactor(), (short)8),
							Common.convertByteToBoolean(mdetails.getUmcBaseUnit()));
					
					actionForm.saveInvUMCList(invUMCList);
					
				}
				
				this.setFormProperties(actionForm);    		
				
			} catch (GlobalNoRecordFoundException ex) {
				
			} catch (EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvUnitOfMeasureConversionAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
					return mapping.findForward("cmnErrorPage"); 
					
				}
				
			}
			
			if (!errors.isEmpty()) {
				
				saveErrors(request, new ActionMessages(errors));
				
			} else {
				
				if ((request.getParameter("saveButton") != null &&
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
			}
							
			return(mapping.findForward("invUnitOfMeasureConversion"));
				
		} else {
			
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			saveErrors(request, new ActionMessages(errors));
			
			return(mapping.findForward("cmnMain"));

        }

     } catch(Exception e) {
     	
/*******************************************************
System Failed: Forward to error page 
*******************************************************/
				
				if (log.isInfoEnabled()) {
					
					log.info("Exception caught in InvUnitOfMeasureConversionAction.execute(): " + e.getMessage()
							+ " session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
		}
		
    private void setFormProperties(InvUnitOfMeasureConversionForm actionForm) {
    	
    	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
    		
    		actionForm.setShowSaveButton(true);
			actionForm.setEnableFields(true);
			
    	} else {
    		
    		actionForm.setShowSaveButton(false);
    		actionForm.setEnableFields(false);
			    		
    	}

   }
    
}