package com.struts.inv.unitofmeasureconversion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvUnitOfMeasureConversionForm extends ActionForm implements Serializable {
	
	private Integer itemCode = null;
	private String itemName = null;	
	private String itemClass = null;
	private String unitMeasure = null;
	private String itemDescription = null;
	
	private ArrayList invUMCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private String size = null;
	private boolean showSaveButton = false;
	private boolean enableFields = false;
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public InvUnitOfMeasureConversionList getInvUMCByIndex(int index) { 
		
		return((InvUnitOfMeasureConversionList)invUMCList.get(index));
		
	}
	
	public Object[] getInvUMCList() {
		
		return(invUMCList.toArray());
		
	}
	
	public int getInvUMCListSize() {
		
		return(invUMCList.size());
		
	}
	
	public void saveInvUMCList(Object newInvUMCList) {
		
		invUMCList.add(newInvUMCList);
		
	}
	
	public void clearInvUMCList() {
		
		invUMCList.clear();
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getSize() {
		
		return size;
		
	}
	
	public void setSize(String size) {
		
		this.size = size;
		
	}
	
	public Integer getItemCode() {
		
		return itemCode;
		
	}
	
	public void setItemCode(Integer itemCode) {
		
		this.itemCode = itemCode;
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}	
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}	
	
	public String getUnitMeasure() {
		
		return unitMeasure;
		
	}
	
	public void setUnitMeasure(String unitMeasure) {
		
		this.unitMeasure = unitMeasure;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for(int i=0; i<invUMCList.size(); i++){
	   		
	   	 InvUnitOfMeasureConversionList actionList = (InvUnitOfMeasureConversionList)invUMCList.get(i);
	   	 actionList.setBaseUnit(false);
	   	 			   
	   }
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null) {
		
			Iterator i = invUMCList.iterator();      	 
			
			while (i.hasNext()) {
				
				InvUnitOfMeasureConversionList umcList = (InvUnitOfMeasureConversionList)i.next();      	 	 
				
				if(!Common.validateNumberFormat(umcList.getConversionFactor())){
					errors.add("quantity",
							new ActionMessage("unitOfMeasureConversion.error.quantityInvalid", umcList.getUomShortName()));
				}
				
				if(!Common.validateRequired(umcList.getConversionFactor()) && Common.convertStringMoneyToDouble(umcList.getConversionFactor(), (short)3) <= 0) {
					errors.add("quantity",
							new ActionMessage("unitOfMeasureConversion.error.negativeOrZeroQuantityNotAllowed", umcList.getConversionFactor()));
				}
				
			}
			
		}
		
		return(errors);
		
	}
	
}
