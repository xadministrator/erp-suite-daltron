package com.struts.inv.unitofmeasureconversion;

import java.io.Serializable;

public class InvUnitOfMeasureConversionList implements Serializable{
	
	private String uomName = null;
	private String uomShortName = null;
	private String uomAdLvClass = null;
	private String conversionFactor = null;
	private boolean baseUnit = false;
	
	private InvUnitOfMeasureConversionForm parentBean;
	
	public InvUnitOfMeasureConversionList(InvUnitOfMeasureConversionForm parentBean,
			String uomName, String uomShortName, String uomAdLvClass, String conversionFactor, boolean baseUnit) {
		
		this.parentBean = parentBean;
		this.uomName = uomName;
		this.uomShortName = uomShortName;
		this.uomAdLvClass = uomAdLvClass;
		this.conversionFactor = conversionFactor;
		this.baseUnit = baseUnit;

	}
	
	public String getUomName() {
		
		return uomName;
		
	}
	
	public void setUomName(String uomName) {
		
		this.uomName = uomName;
		
	}
	
	public String getUomShortName() {
		
		return uomShortName;
		
	}
	
	public void setUomShortName(String uomShortName) {
		
		this.uomShortName = uomShortName;
		
	}
	
	public String getConversionFactor() {
		
		return conversionFactor;
		
	}
	
	public void setConversionFactor(String conversionFactor) {
		
		this.conversionFactor = conversionFactor;
		
	}
	
	public boolean getBaseUnit() {
		
		return baseUnit;
		
	}
	
	public void setBaseUnit(boolean baseUnit) {
		
		this.baseUnit = baseUnit;
		
	}
	
	public String getUomAdLvClass() {
		
		return uomAdLvClass;
		
	}
	
	public void setUomAdLvClass(String uomAdLvClass) {
		
		this.uomAdLvClass = uomAdLvClass;
		
	}
	
}
