package com.struts.inv.unitofmeasures;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvUOMOneBaseUnitIsAllowedException;
import com.ejb.exception.InvUOMShortNameAlreadyExistException;
import com.ejb.txn.InvUnitOfMeasureController;
import com.ejb.txn.InvUnitOfMeasureControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.InvUnitOfMeasureDetails;

public final class InvUnitOfMeasureAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvUnitOfMeasureAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvUnitOfMeasureForm actionForm = (InvUnitOfMeasureForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.INV_UNIT_OF_MEASURE_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invUnitOfMeasures");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
Initialize InvUnitOfMeasureController EJB
*******************************************************/
			
			InvUnitOfMeasureControllerHome homeUOM = null;
			InvUnitOfMeasureController ejbUOM = null;
			
			try {
				
				homeUOM = (InvUnitOfMeasureControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvUnitOfMeasureControllerEJB", InvUnitOfMeasureControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvUnitOfMeasureAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbUOM = homeUOM.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvUnitOfMeasureAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
Call InvItemEntryController EJB
getAdPrfInvQuantityPrecisionUnit
*******************************************************/			
			
			short quantityPrecisionUnit = 0;
			
			try {
				
				quantityPrecisionUnit = ejbUOM.getAdPrfInvQuantityPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in InvItemEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}			        
			
/*******************************************************
-- Inv UOM Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("invUnitOfMeasures"));
				
/*******************************************************
-- Inv UOM Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("invUnitOfMeasures"));                  
				
/*******************************************************
-- Inv UOM Save Action --
*******************************************************/
				
			} else if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvUnitOfMeasureDetails details = new InvUnitOfMeasureDetails();
				details.setUomName(actionForm.getUnitOfMeasureName());
				details.setUomDescription(actionForm.getDescription());
				details.setUomShortName(actionForm.getShortName());
				details.setUomAdLvClass(actionForm.getUnitOfMeasureClass());
				details.setUomConversionFactor(Common.convertStringMoneyToDouble(actionForm.getConversionFactor(), (short)8));
				details.setUomBaseUnit(Common.convertBooleanToByte(actionForm.getBaseUnit()));
				details.setUomEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				
				try {
					
					ejbUOM.addInvUomEntry(details, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.recordAlreadyExist"));
					
				} catch (InvUOMShortNameAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.shortNameAlreadyExist"));
					
				} catch (InvUOMOneBaseUnitIsAllowedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.oneBaseUnitIsAllowed"));               	
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvUnitOfMeasureAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
-- Inv UOM Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
-- Inv UOM Update Action --
*******************************************************/
				
			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvUnitOfMeasureList invUOMList =
					actionForm.getInvUOMByIndex(actionForm.getRowSelected());
				
				InvUnitOfMeasureDetails details = new InvUnitOfMeasureDetails();
				details.setUomCode(invUOMList.getUnitOfMeasureCode());
				details.setUomName(actionForm.getUnitOfMeasureName());
				details.setUomDescription(actionForm.getDescription());
				details.setUomShortName(actionForm.getShortName());
				details.setUomAdLvClass(actionForm.getUnitOfMeasureClass());
				details.setUomConversionFactor(Common.convertStringMoneyToDouble(actionForm.getConversionFactor(), (short)8));
				details.setUomBaseUnit(Common.convertBooleanToByte(actionForm.getBaseUnit()));
				details.setUomEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				
				try {
					
					ejbUOM.updateInvUomEntry(details, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.recordAlreadyExist"));
					
				} catch (InvUOMShortNameAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.shortNameAlreadyExist"));
					
				} catch (InvUOMOneBaseUnitIsAllowedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.oneBaseUnitIsAllowed"));               	               	
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvUnitOfMeasureAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
-- Inv UOM Cancel Action --
*******************************************************/
				
			} else if (request.getParameter("cancelButton") != null) {
				
/*******************************************************
-- Inv UOM Edit Action --
*******************************************************/
				
			} else if (request.getParameter("invUOMList[" + 
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				actionForm.showInvUOMRow(actionForm.getRowSelected());
				
				return mapping.findForward("invUnitOfMeasures");
				
/*******************************************************
-- Inv UOM Delete Action --
*******************************************************/
				
			} else if (request.getParameter("invUOMList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				InvUnitOfMeasureList invUOMList =
					actionForm.getInvUOMByIndex(actionForm.getRowSelected());
				
				try {
					
					ejbUOM.deleteInvUomEntry(invUOMList.getUnitOfMeasureCode(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.deleteUnitOfMeasureAlreadyAssigned"));
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("unitOfMeasure.error.unitOfMeasureAlreadyDeleted"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvUnitOfMeasureAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}            
				
/*******************************************************
-- Inv UOM Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invUnitOfMeasures");
					
				}
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearInvUOMList();	        
					
					actionForm.clearUnitOfMeasureClassList();           	
					
					list = ejbUOM.getAdLvInvUnitOfMeasureClassAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setUnitOfMeasureClassList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setUnitOfMeasureClassList((String)i.next());
							
						}
						
					} 	           
					
					list = ejbUOM.getInvUomAll(user.getCmpCode()); 
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						InvUnitOfMeasureDetails details = (InvUnitOfMeasureDetails)i.next();
						
						InvUnitOfMeasureList invUOMList = new InvUnitOfMeasureList(actionForm,
								details.getUomCode(),
								details.getUomName(),
								details.getUomDescription(),
								details.getUomShortName(),
								details.getUomAdLvClass(),
								Common.convertDoubleToStringMoney(details.getUomConversionFactor(), (short)8),
								Common.convertByteToBoolean(details.getUomBaseUnit()),
								Common.convertByteToBoolean(details.getUomEnable()));
						
						actionForm.saveInvUOMList(invUOMList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvUnitOfMeasureAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}          
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveButton") != null || 
							request.getParameter("updateButton") != null) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
					
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                                  
				
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				actionForm.setEnable(true);
				
				return(mapping.findForward("invUnitOfMeasures"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in InvUnitOfMeasureAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
	
}