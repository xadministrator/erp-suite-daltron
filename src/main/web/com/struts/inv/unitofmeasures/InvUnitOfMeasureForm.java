package com.struts.inv.unitofmeasures;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
 
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvUnitOfMeasureForm extends ActionForm implements Serializable {
	
	private String unitOfMeasureName = null;	
	private String description = null;
	private String shortName = null;
	private String unitOfMeasureClass = null;
	private ArrayList unitOfMeasureClassList = new ArrayList();
	private String conversionFactor = null;
	private boolean baseUnit = false;
	private boolean enable = false;
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList invUOMList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public InvUnitOfMeasureList getInvUOMByIndex(int index) { 
		
		return((InvUnitOfMeasureList)invUOMList.get(index));
		
	}
	
	public Object[] getInvUOMList() {
		
		return(invUOMList.toArray());
		
	}
	
	public int getInvUOMListSize() {
		
		return(invUOMList.size());
		
	}
	
	public void saveInvUOMList(Object newInvUOMList) {
		
		invUOMList.add(newInvUOMList);
		
	}
	
	public void clearInvUOMList() {
		
		invUOMList.clear();
		
	}
	
	public void setRowSelected(Object selectedInvUOMList, boolean isEdit) {
	
		this.rowSelected = invUOMList.indexOf(selectedInvUOMList);
	
		if(isEdit) {
		
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showInvUOMRow(int rowSelected) {
		this.unitOfMeasureName = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getUnitOfMeasureName();
		this.description = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getDescription();
		this.shortName = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getShortName();
		this.unitOfMeasureClass = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getUnitOfMeasureClass();
		this.conversionFactor = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getConversionFactor();
		this.baseUnit = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getBaseUnit();
		this.enable = ((InvUnitOfMeasureList)invUOMList.get(rowSelected)).getEnable();
	}
	
	public void updateInvUOMRow(int rowSelected, Object newInvUOMList) {
	
		invUOMList.set(rowSelected, newInvUOMList);
		
	}
	
	public void deleteInvUOMList(int rowSelected) {
	
		invUOMList.remove(rowSelected);
		
	}
	
	public void setUpdateButton(String updateButton) {
	
		this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton) {
	
		this.cancelButton = cancelButton;
		
	}
	
	public void setSaveButton(String saveButton) {
	
		this.saveButton = saveButton;
		
	}
	
	public void setCloseButton(String closeButton) {
	
		this.closeButton = closeButton;
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}	
	
	public void setPageState(String pageState) {
	
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
	
		return(pageState);
		
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
		this.txnStatus = txnStatus;
		
	}
		
	public String getUserPermission() {
	
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
	
		this.userPermission = userPermission;
		
	}
	
	public String getUnitOfMeasureName() {
	
		return unitOfMeasureName;
		
	}
	
	public void setUnitOfMeasureName(String unitOfMeasureName) {
	
		this.unitOfMeasureName = unitOfMeasureName;
		
	}	
	
	public String getDescription() {
	
		return description;
		
	}
	
	public void setDescription(String description) {
	
		this.description = description;
		
	}	
	
	public String getShortName() {
	
		return shortName;
		
	}
	
	public void setShortName(String shortName) {
	
		this.shortName = shortName;
		
	}	
	
	public String getUnitOfMeasureClass() {
	
		return unitOfMeasureClass;
		
	}
	
	public void setUnitOfMeasureClass(String unitOfMeasureClass) {
	
		this.unitOfMeasureClass = unitOfMeasureClass;
		
	}	
	
	public ArrayList getUnitOfMeasureClassList() {
	
		return unitOfMeasureClassList;
		
	}
	
	public void setUnitOfMeasureClassList(String unitOfMeasureClass) {
	
		unitOfMeasureClassList.add(unitOfMeasureClass);
		
	}	
	
	public void clearUnitOfMeasureClassList() {
	
		unitOfMeasureClassList.clear();
		unitOfMeasureClassList.add(Constants.GLOBAL_BLANK);
		
	}
		
	public String getConversionFactor() {
	
		return conversionFactor;
		
	}
	
	public void setConversionFactor(String conversionFactor) {
	
		this.conversionFactor = conversionFactor;
		
	}	
	
	public boolean getBaseUnit() {
	
		return baseUnit;
		
	}
	
	public void setBaseUnit(boolean baseUnit) {
	
		this.baseUnit = baseUnit;
		
	}

	public boolean getEnable() {
	
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
	
		this.enable = enable;
		
	}

	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		unitOfMeasureName = null;
		description = null;
		shortName = null;
		unitOfMeasureClass = Constants.GLOBAL_BLANK;
		conversionFactor = null;
		baseUnit = false;
		enable = false;
		saveButton = null;
		closeButton = null;
		updateButton = null;
		cancelButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;	   

	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
		
			if(Common.validateRequired(unitOfMeasureName)) {
			
				errors.add("unitOfMeasureName", new ActionMessage("unitOfMeasure.error.unitOfMeasureNameRequired"));
				
			}
			
			if(Common.validateRequired(shortName)) {
			
				errors.add("shortName", new ActionMessage("unitOfMeasure.error.shortNameRequired"));
				
			}
			
			if(Common.validateRequired(unitOfMeasureClass)) {
				
				errors.add("unitOfMeasureClass", new ActionMessage("unitOfMeasure.error.unitOfMeasureClassRequired"));
					
			}
			
			if(Common.validateRequired(conversionFactor)) {
			
				errors.add("conversionFactor", new ActionMessage("unitOfMeasure.error.conversionFactorRequired"));
				
			}
			
			if(!Common.validateMoneyFormat(conversionFactor)) {
			
				errors.add("conversionFactor", new ActionMessage("unitOfMeasure.error.conversionFactorInvalid"));
				
			}
			
			if(!Common.validateRequired(conversionFactor) && Common.convertStringMoneyToDouble(conversionFactor, (short)8) == 0) {
			
		        errors.add("conversionFactor", new ActionMessage("unitOfMeasure.error.zeroQuantityNotAllowed"));
		        
		    }
			
		}
		
		return(errors);
	}
	
}
