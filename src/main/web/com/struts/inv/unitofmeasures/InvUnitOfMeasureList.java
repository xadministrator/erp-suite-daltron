package com.struts.inv.unitofmeasures;

import java.io.Serializable;

public class InvUnitOfMeasureList implements Serializable{
	
	private Integer unitOfMeasureCode = null;
	private String unitOfMeasureName = null;
	private String description = null;
	private String shortName = null;
	private String unitOfMeasureClass = null;
	private String conversionFactor = null;
	private boolean baseUnit = false;
	private boolean enable = false;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private InvUnitOfMeasureForm parentBean;
	
	public InvUnitOfMeasureList(InvUnitOfMeasureForm parentBean,
			Integer unitOfMeasureCode,
			String unitOfMeasureName,
			String description,
			String shortName,
			String unitOfMeasureClass,
			String conversionFactor,
			boolean baseUnit,
			boolean enable) {

			
		this.parentBean = parentBean;
		this.unitOfMeasureCode = unitOfMeasureCode;
		this.unitOfMeasureName = unitOfMeasureName;
		this.description = description;
		this.shortName = shortName;
		this.unitOfMeasureClass = unitOfMeasureClass;
		this.conversionFactor = conversionFactor;
		this.baseUnit = baseUnit;
		this.enable = enable;
		
	}
	
	public void setParentBean(InvUnitOfMeasureForm parentBean){
		
		this.parentBean = parentBean;
		
	}
	
	public void setDeleteButton(String deleteButton){
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public void setEditButton(String editButton){
		
		parentBean.setRowSelected(this, true);
		
	}

	public Integer getUnitOfMeasureCode() {
		
		return unitOfMeasureCode;
		
	}
	
	public String getUnitOfMeasureName() {
		
		return unitOfMeasureName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getShortName() {
		
		return shortName;
		
	}
	
	public String getConversionFactor() {
		
		return conversionFactor;
		
	}	
	
	public String getUnitOfMeasureClass() {
		
		return unitOfMeasureClass;
		
	}
		
	public boolean getBaseUnit() {
		
		return baseUnit;
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}

}
