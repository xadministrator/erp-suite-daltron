package com.struts.jreports.ad.approvallist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.AdRepApprovalListDetails;

public class AdRepApprovalListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepApprovalListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         AdRepApprovalListDetails details = (AdRepApprovalListDetails)i.next();

	     AdRepApprovalListData dtbData = new AdRepApprovalListData(
	     		details.getAlDocumentType(), new Double(details.getAlAmount()),
				details.getAlUserName(), details.getAlUserType(),
				(details.getAlEnable() ? "YES" : "NO"), details.getAlAndOr());
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("documentType".equals(fieldName)) {
      	
          value = ((AdRepApprovalListData)data.get(index)).getDocumentType();
      
      } else if("enable".equals(fieldName)) {
      
          value = ((AdRepApprovalListData)data.get(index)).getEnable();

      } else if("amount".equals(fieldName)) {
      
          value = ((AdRepApprovalListData)data.get(index)).getAmount();

      } else if("andOr".equals(fieldName)) {
        
            value = ((AdRepApprovalListData)data.get(index)).getAndOr();
            
      } else if("user".equals(fieldName)) {
        
            value = ((AdRepApprovalListData)data.get(index)).getUser();

      } else if("type".equals(fieldName)) {
        
            value = ((AdRepApprovalListData)data.get(index)).getType();

      } else if("enable".equals(fieldName)) {
      
          value = ((AdRepApprovalListData)data.get(index)).getEnable();
      
      }

      return(value);
   }
}
