package com.struts.jreports.ad.approvallist;

public class AdRepApprovalListData implements java.io.Serializable {
	
   private String documentType = null;
   private Double amount = null;
   private String user = null;
   private String type = null;
   private String enable = null;
   private String andOr = null;
   
   public AdRepApprovalListData(String documentType, Double amount, 
   		String user, String type, String enable, String andOr) {
      	
   	  this.documentType = documentType;
   	  this.amount = amount;
   	  this.user = user;
   	  this.type = type;
      this.enable = enable;
      this.andOr = andOr;

   }

   public String getDocumentType() {
   	
   	  return documentType;
   	  
   }
   
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getUser() {
   	
      return user;
      
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }
   
   public String getEnable() {
   	 
      return enable;
   
   }
   
   public String getAndOr() {
   	
   	  return andOr;
   	  
   }

}
