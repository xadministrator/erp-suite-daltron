package com.struts.jreports.ad.approvallist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class AdRepApprovalListForm extends ActionForm implements Serializable{

   private String documentType = null;
   private ArrayList documentTypeList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public void setGoButton(String goButton){
   	
      this.goButton = goButton;
      
   }

   public void setCloseButton(String closeButton){
   	
      this.closeButton = closeButton;
      
   }
   
   public String getDocumentType(){
   	
      return documentType;
   }
   
   public void setDocumentType(String documentType){
   	
      this.documentType = documentType;
   }
   
   public ArrayList getDocumentTypeList(){
   	
   	  return documentTypeList;
   	  
   }
   
   public void setDocumentTypeList(String documentType){
   	
   	  documentTypeList.add(documentType);
   	  
   }
   
   public void clearDocumentTypeList(){
   	
   	  documentTypeList.clear();
   	  documentTypeList.add(Constants.GLOBAL_BLANK);
   	  
   }

   
   public String getViewType(){
   	
   	  return viewType;
   	  
   }
   
   public void setViewType(String viewType){
   	  
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList(){
   	
   	  return viewTypeList;
   	  
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
   	
      return userPermission;
      
   }

   public void setUserPermission(String userPermission){
   	
      this.userPermission = userPermission;
      
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){      
      goButton = null;
      closeButton = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      documentType = Constants.GLOBAL_BLANK;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();

      return(errors);
   }
}
