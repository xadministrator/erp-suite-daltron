package com.struts.jreports.ad.bankaccountlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdRepBankAccountListController;
import com.ejb.txn.AdRepBankAccountListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class AdRepBankAccountListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("AdRepBankAccountListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         AdRepBankAccountListForm actionForm = (AdRepBankAccountListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AD_REP_BANK_ACCOUNT_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adRepBankAccountList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize AdRepBankAccountListController EJB
*******************************************************/

         AdRepBankAccountListControllerHome homeBAL = null;
         AdRepBankAccountListController ejbBAL = null;       

         try {
         	
            homeBAL = (AdRepBankAccountListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/AdRepBankAccountListControllerEJB", AdRepBankAccountListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in AdRepBankAccountListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbBAL = homeBAL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in AdRepBankAccountListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AD BAL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getBankName())) {
	        		
	        		criteria.put("bankName", actionForm.getBankName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getAccountName())) {
	        		
	        		criteria.put("accountName", actionForm.getAccountName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getAccountUse())) {
	        		
	        		criteria.put("accountUse", actionForm.getAccountUse());
	        		
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            ArrayList branchList = new ArrayList();
	     	
	     	for(int i=0; i<actionForm.getAdRepBrBankAccountListSize(); i++) {
	     	    
	     	    AdRepBranchBankAccountList adBbalList = (AdRepBranchBankAccountList)actionForm.getAdRepBrBankAccountListByIndex(i);
	     	    
	     	    if(adBbalList.getBranchCheckbox() == true) {                                          
	     	        
	     	        AdBranchDetails details = new AdBranchDetails();                                          
	     	        
	     	        details.setBrAdCompany(user.getCmpCode());	     	        
	     	        details.setBrCode(adBbalList.getBrCode());                    
	     	        
	     	        branchList.add(details);
	     	        
	     	    }
	     	}
	     	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbBAL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    	
		       list = ejbBAL.executeAdRepBankAccountList(actionForm.getCriteria(),
            	    branchList, actionForm.getOrderBy(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("bankAccountList.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in AdRepBankAccountListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adRepBankAccountList");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getBankName() != null) {
			
				parameters.put("bankName", actionForm.getBankName());
				
			}
			
			if (actionForm.getAccountName() != null) {
				
				parameters.put("accountName", actionForm.getAccountName());
					
			}
			
			if (actionForm.getAccountUse() != null) {
				
				parameters.put("accountUse", actionForm.getAccountUse());
					
			}
			
			String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getAdRepBrBankAccountListSize(); j++) {

      			AdRepBranchBankAccountList brList = (AdRepBranchBankAccountList)actionForm.getAdRepBrBankAccountListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);

			String filename = "/opt/ofs-resources/" + user.getCompany() + "/AdRepBankAccountList.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/AdRepBankAccountList.jasper");
		    
	        }
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new AdRepBankAccountListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new AdRepBankAccountListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new AdRepBankAccountListDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in AdRepBankAccountListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AD BAL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AD BAL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
		         } catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in AdRepBankAccountListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
         	
	         	actionForm.reset(mapping, request);
	         	
                // populate AdRepBranchBankAccountListList
         		
         		ArrayList list = new ArrayList();
         		actionForm.clearAdRepBrBankAccountList();
                
                list = ejbBAL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                Iterator i = list.iterator();
                
                while(i.hasNext()) {
                
                    AdBranchDetails details = (AdBranchDetails)i.next();
                
                    AdRepBranchBankAccountList adBbalList = new AdRepBranchBankAccountList(actionForm,
                        details.getBrBranchCode(),details.getBrName(),
                        details.getBrCode());
                    adBbalList.setBranchCheckbox(true);
                                 
                    actionForm.saveAdRepBrBankAccountList(adBbalList);
                    
                }
	         	
	         	return(mapping.findForward("adRepBankAccountList"));		          
	         	
	         } else {
	         	
	         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	         	saveErrors(request, new ActionMessages(errors));
	         	
	         	return(mapping.findForward("cmnMain"));
	         	
	         }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in AdRepBankAccountListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
