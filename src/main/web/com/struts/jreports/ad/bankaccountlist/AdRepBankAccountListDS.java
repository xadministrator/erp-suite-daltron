package com.struts.jreports.ad.bankaccountlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.AdRepBankAccountListDetails;

public class AdRepBankAccountListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepBankAccountListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 AdRepBankAccountListDetails details = (AdRepBankAccountListDetails)i.next();
                  
      	 AdRepBankAccountListData argData = new AdRepBankAccountListData(details.getBalBankName(), details.getBalBaName(),
	     		details.getBalBaAccountNumber(), details.getBalBaAccountType(), details.getBalBaAccountUse(), details.getBalBaDescription(),
				new Double(details.getBalBaAvailableBalance()), details.getBalBaEnable() == 1 ? "YES" : "NO", details.getBalBaCashAccount(),
				details.getBalBaCashAccountDescription(), details.getBalBaAdjustmentAccount(), details.getBalBaAdjustmentAccountDescription(),
				details.getBalBaInterestAccount(), details.getBalBaInterestAccountDescription(), details.getBalBaBankChargeAccount(), 
				details.getBalBaBankChargeAccountDescription(), details.getBalBaSalesDiscount(), details.getBalBaSalesDiscountDescription());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("bankName".equals(fieldName)){
         value = ((AdRepBankAccountListData)data.get(index)).getBankName();
      }else if("accountName".equals(fieldName)){
         value = ((AdRepBankAccountListData)data.get(index)).getAccountName();
      }else if("accountNumber".equals(fieldName)){
         value = ((AdRepBankAccountListData)data.get(index)).getAccountNumber();      
      }else if("accountType".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getAccountType(); 
      }else if("accountUse".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getAccountUse();
      }else if("description".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getDescription();
      }else if("availableBalance".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getAvailableBalance();
      }else if("enable".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getEnable();
      }else if("cashAccount".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getCashAccount(); 
      }else if("cashAccountDescription".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getCashAccountDescription();
      }else if("adjustmentAccount".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getAdjustmentAccount(); 
      }else if("adjustmentAccountDescription".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getAdjustmentAccountDescription();
      }else if("interestAccount".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getInterestAccount(); 
      }else if("interestAccountDescription".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getInterestAccountDescription(); 
      }else if("bankChargeAccount".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getBankChargeAccount(); 
      }else if("bankChargeAccountDescription".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getBankChargeAccountDescription();
      }else if("salesDiscount".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getSalesDiscount(); 
      }else if("salesDiscountDescription".equals(fieldName)){
        value = ((AdRepBankAccountListData)data.get(index)).getDiscountDescription();   
      }

      return(value);
   }
}
