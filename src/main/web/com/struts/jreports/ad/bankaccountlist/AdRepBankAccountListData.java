package com.struts.jreports.ad.bankaccountlist;

public class AdRepBankAccountListData implements java.io.Serializable {
	
   private String bankName = null;
   private String accountName = null;
   private String accountNumber = null;
   private String accountType = null;
   private String accountUse = null;
   private String description = null;
   private Double availableBalance = null;
   private String enable = null;
   
   //accounts and description
   private String cashAccount = null;
   private String cashAccountDescription = null;
   private String adjustmentAccount = null;
   private String adjustmentAccountDescription = null;
   private String interestAccount = null;
   private String interestAccountDescription = null;
   private String bankChargeAccount = null;
   private String bankChargeAccountDescription = null;
   private String salesDiscount = null;
   private String discountDescription = null;

   public AdRepBankAccountListData(String bankName, String accountName, String accountNumber, String accountType, 
   		String accountUse, String description, Double availableBalance, String enable, String cashAccount, String cashAccountDescription,
		String adjustmentAccount, String adjustmentAccountDescription, String interestAccount, String interestAccountDescription,
		String bankChargeAccount, String bankChargeAccountDescription, String salesDiscount, String discountDescription){
      	
      this.bankName = bankName;
      this.accountName = accountName;
      this.accountNumber = accountNumber;
      this.accountType = accountType;
      this.accountUse = accountUse;
      this.description = description;
      this.availableBalance = availableBalance;
      this.enable = enable;
      
      // accounts and description
      this.cashAccount = cashAccount;
      this.cashAccountDescription = cashAccountDescription;
      this.adjustmentAccount = adjustmentAccount;
      this.adjustmentAccountDescription = adjustmentAccountDescription;
      this.interestAccount = interestAccount;
      this.interestAccountDescription = interestAccountDescription;
      this.bankChargeAccount = bankChargeAccount;
      this.bankChargeAccountDescription = bankChargeAccountDescription;
      this.salesDiscount = salesDiscount;
      this.discountDescription = discountDescription;
      
   }

   public String getAccountName() {
   	return accountName;
   }
   public String getAccountNumber() {
   	return accountNumber;
   }
   public String getAccountType() {
   	return accountType;
   }
   public String getAccountUse() {
   	return accountUse;
   }
   public String getAdjustmentAccount() {
   	return adjustmentAccount;
   }
   public String getAdjustmentAccountDescription() {
   	return adjustmentAccountDescription;
   }
   public Double getAvailableBalance() {
   	return availableBalance;
   }
   public String getBankChargeAccount() {
   	return bankChargeAccount;
   }
   public String getBankChargeAccountDescription() {
   	return bankChargeAccountDescription;
   }
   public String getBankName() {
   	return bankName;
   }
   public String getCashAccount() {
   	return cashAccount;
   }
   public String getCashAccountDescription() {
   	return cashAccountDescription;
   }
   public String getDescription() {
   	return description;
   }
   public String getDiscountDescription() {
   	return discountDescription;
   }
   public String getEnable() {
   	return enable;
   }
   public String getInterestAccount() {
   	return interestAccount;
   }
   public String getInterestAccountDescription() {
   	return interestAccountDescription;
   }
   public String getSalesDiscount() {
   	return salesDiscount;
   }
}
