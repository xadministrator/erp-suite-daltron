package com.struts.jreports.ad.bankaccountlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class AdRepBankAccountListForm extends ActionForm implements Serializable{
	
	private String bankName = null;
	private String accountName = null;
	private String accountUse = null;
	private ArrayList accountUseList = new ArrayList();  
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	private ArrayList adRepBrBankAccountList = new ArrayList();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getBankName() {
		
		return bankName;
		
	}
	
	public void setBankName(String bankName) {
		
		this.bankName = bankName;
		
	}
	
	public String getAccountName() {
		
		return accountName;
		
	}
	
	public void setAccountName(String accountName) {
		
		this.accountName = accountName;
		
	}
	
	public String getAccountUse() {
		
		return accountUse;
		
	}
	
	public void setAccountUse(String accountUse) {
		
		this.accountUse = accountUse;
		
	}
	
	public ArrayList getAccountUseList() {
		
		return accountUseList;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getOrderBy() {
		
		return orderBy;   	
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public String getViewType() {
		
		return viewType;   	
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getAdRepBrBankAccountList(){
		
		return adRepBrBankAccountList.toArray();
		
	}
	
	public AdRepBranchBankAccountList getAdRepBrBankAccountListByIndex(int index){
		
		return ((AdRepBranchBankAccountList)adRepBrBankAccountList.get(index));
		
	}
	
	public int getAdRepBrBankAccountListSize(){
		
		return(adRepBrBankAccountList.size());
		
	}
	
	public void saveAdRepBrBankAccountList(Object newAdRepBrBankAccountList){
		
		adRepBrBankAccountList.add(newAdRepBrBankAccountList);   	  
		
	}
	
	public void clearAdRepBrBankAccountList(){
		
		adRepBrBankAccountList.clear();
		
	}
	
	public void setAdBrRepBankAccountList(ArrayList adRepBrBankAccountList) {
		
		this.adRepBrBankAccountList = adRepBrBankAccountList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {   
		
		for (int i=0; i<adRepBrBankAccountList.size(); i++) {
			
			AdRepBranchBankAccountList list = (AdRepBranchBankAccountList)adRepBrBankAccountList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		bankName = null;
		accountName = null;
		accountUseList.clear();
		accountUseList.add(Constants.GLOBAL_BLANK);
		accountUseList.add(Constants.AD_BANK_ACCOUNT_USE_INTERNAL);
		accountUseList.add(Constants.AD_BANK_ACCOUNT_USE_SUPPLIER);                
		accountUse = Constants.GLOBAL_BLANK;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		
		if (orderByList.isEmpty()) {
			
			orderByList.clear();
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("BANK NAME");
			orderByList.add("ACCOUNT NAME");
			orderByList.add("ACCOUNT USE");
			orderBy = "BANK NAME";   
			
		}    
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		}
		return(errors);
	}
}
