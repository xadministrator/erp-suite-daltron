package com.struts.jreports.ad.responsibilitylist;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdRepResponsibilityListController;
import com.ejb.txn.AdRepResponsibilityListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepResponsibilityListDetails;

public final class AdRepResponsibilityListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("AdRepResponsibilityListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         AdRepResponsibilityListForm actionForm = (AdRepResponsibilityListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.AD_REP_RESPONSIBILITY_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adRepResponsibilityList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize AdRepResponsibilityListController EJB
*******************************************************/

         AdRepResponsibilityListControllerHome homeRL = null;
         AdRepResponsibilityListController ejbRL = null;       

         try {
         	
            homeRL = (AdRepResponsibilityListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/AdRepResponsibilityListControllerEJB", AdRepResponsibilityListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in AdRepResponsibilityListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbRL = homeRL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in AdRepResponsibilityListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AD RL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            AdRepResponsibilityListDetails details = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getResponsibilityName())) {
	        		
	        		criteria.put("responsibilityName", actionForm.getResponsibilityName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            ArrayList branchList = new ArrayList();
	     	
	     	for(int i=0; i<actionForm.getadRepBrResponsibilityListSize(); i++) {
	     	    
	     	    AdRepBranchResponsibilityList adBrlList = (AdRepBranchResponsibilityList)actionForm.getAdRepBrResponsibilityListByIndex(i);
	     	    
	     	    if(adBrlList.getBranchCheckbox() == true) {                                          
	     	    	
	     	    	AdBranchDetails brDetails = new AdBranchDetails();                                          
	     	    	
	     	    	brDetails.setBrAdCompany(user.getCmpCode());	     	        
	     	    	brDetails.setBrCode(adBrlList.getBrCode());                    
	     	    	
	     	    	branchList.add(brDetails);
	     	    	
	     	    }
	     	}

		    try {
		    
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbRL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbRL.executeAdRepResponsibilityList(actionForm.getCriteria(), branchList, actionForm.getOrderBy(), actionForm.getFormFunction(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("responsibilityList.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in AdRepResponsibilityListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("adRepResponsibilityList"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("company", company);
		    parameters.put("responsibilityName", actionForm.getResponsibilityName());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    parameters.put("orderBy", actionForm.getOrderBy());
		    parameters.put("viewType", actionForm.getViewType());
		    
		    if(actionForm.getFormFunction()) {
		    	
		    	parameters.put("formFunction", "YES");
		    	
		    } else {
		    	
		    	parameters.put("formFunction", "NO");
		    	
		    }
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getadRepBrResponsibilityListSize(); j++) {

      			AdRepBranchResponsibilityList brList = (AdRepBranchResponsibilityList)actionForm.getAdRepBrResponsibilityListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/AdRepResponsibilityList.jasper";
		       
		    if (!new java.io.File(filename).exists()) {
		    		    		    
		       filename = servlet.getServletContext().getRealPath("jreports/AdRepResponsibilityList.jasper");
			    
		    }
		    		    	    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new AdRepResponsibilityListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					     new AdRepResponsibilityListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new AdRepResponsibilityListDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in AdRepResponsibilityListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AD RL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AD RL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	actionForm.reset(mapping, request);

	         	// populate AdRepBranchBankAccountList
         		
         		ArrayList list = new ArrayList();
         		actionForm.clearAdRepBrResponsibilityList();
                
                list = ejbRL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                Iterator i = list.iterator();
                
                while(i.hasNext()) {
                
                    AdBranchDetails details = (AdBranchDetails)i.next();
                
                    AdRepBranchResponsibilityList adBrlList = new AdRepBranchResponsibilityList(actionForm,
                        details.getBrBranchCode(), details.getBrName(),
                        details.getBrCode());
                    adBrlList.setBranchCheckbox(true);
                                 
                    actionForm.saveAdRepBrResponsibilityList(adBrlList);
                    
                }
	         	
	            return(mapping.findForward("adRepResponsibilityList"));		          
			            
	         } else {
	         	
	         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	         	saveErrors(request, new ActionMessages(errors));
	         	
	         	return(mapping.findForward("cmnMain"));
	         	
	         }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in AdRepResponsibilityListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}