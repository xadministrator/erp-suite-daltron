package com.struts.jreports.ad.responsibilitylist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.AdRepResponsibilityListDetails;

public class AdRepResponsibilityListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepResponsibilityListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         AdRepResponsibilityListDetails details = (AdRepResponsibilityListDetails)i.next();

	     AdRepResponsibilityListData dtbData = new AdRepResponsibilityListData(
	     		details.getRlResponsibilityName(), details.getRlResponsibilityDescription(),
				Common.convertSQLDateToString(details.getRlDateFrom()),
				Common.convertSQLDateToString(details.getRlDateTo()),
				details.getRlFormFunction());
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("responsibilityName".equals(fieldName)) {
      	
          value = ((AdRepResponsibilityListData)data.get(index)).getResponsibilityName();
      
      } else if("responsibilityDescription".equals(fieldName)) {
      
          value = ((AdRepResponsibilityListData)data.get(index)).getResponsibilityDescription();

      } else if("dateFrom".equals(fieldName)) {
      	
          value = ((AdRepResponsibilityListData)data.get(index)).getDateFrom();
      
      } else if("dateTo".equals(fieldName)) {
      
          value = ((AdRepResponsibilityListData)data.get(index)).getDateTo();

      } else if("formFunction".equals(fieldName)) {
        
            value = ((AdRepResponsibilityListData)data.get(index)).getFormFunction();
            
      }

      return(value);
   }
}
