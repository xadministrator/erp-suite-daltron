package com.struts.jreports.ad.responsibilitylist;


public class AdRepResponsibilityListData implements java.io.Serializable {
	
   private String responsibilityName = null;
   private String responsibilityDescription = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String formFunction = null;
   
   public AdRepResponsibilityListData(String responsibilityName, String responsibilityDescription,
   		String dateFrom, String dateTo, String formFunction) {
      	
      this.responsibilityName = responsibilityName;
      this.responsibilityDescription = responsibilityDescription;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;
      this.formFunction = formFunction;
      
   }

   public String getResponsibilityName() {
   	
   	  return responsibilityName;
   	  
   }
   
   public String getResponsibilityDescription() {
   	
   	  return responsibilityDescription;
   	  
   }
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public String getFormFunction() {
   	
   	  return formFunction;
   	  
   }
   
}
