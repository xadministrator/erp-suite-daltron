package com.struts.jreports.ad.responsibilitylist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class AdRepResponsibilityListForm extends ActionForm implements Serializable{
	
	private String responsibilityName = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private boolean formFunction = false;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	private ArrayList adRepBrResponsibilityList = new ArrayList();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getResponsibilityName(){
		
		return responsibilityName;
		
	}
	
	public void setResponsibilityName(String responsibilityName){
		
		this.responsibilityName = responsibilityName;
		
	}
	
	public String getDateFrom(){
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public boolean getFormFunction() {
		
		return formFunction;
		
	}
	
	public void setFormFunction(boolean formFunction) {
		
		this.formFunction = formFunction;
		
	}
	
	public String getViewType(){
		
		return viewType;
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public String getOrderBy(){
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy){
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList(){
		
		return orderByList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getAdRepBrResponsibilityList(){
		
		return adRepBrResponsibilityList.toArray();
		
	}
	
	public AdRepBranchResponsibilityList getAdRepBrResponsibilityListByIndex(int index){
		
		return ((AdRepBranchResponsibilityList)adRepBrResponsibilityList.get(index));
		
	}
	
	public int getadRepBrResponsibilityListSize(){
		
		return(adRepBrResponsibilityList.size());
		
	}
	
	public void saveAdRepBrResponsibilityList(Object newAdRepBrResponsibilityList){
		
		adRepBrResponsibilityList.add(newAdRepBrResponsibilityList);   	  
		
	}
	
	public void clearAdRepBrResponsibilityList(){
		
		adRepBrResponsibilityList.clear();
		
	}
	
	public void setAdRepBrResponsibilityList(ArrayList adRepBrResponsibilityList) {
		
		this.adRepBrResponsibilityList = adRepBrResponsibilityList;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<adRepBrResponsibilityList.size(); i++) {
			
			AdRepBranchResponsibilityList list = (AdRepBranchResponsibilityList)adRepBrResponsibilityList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		orderByList.clear();
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add("RESPONSIBILITY NAME");
		orderByList.add("RESPONSIBILITY DESC");
		orderBy = Constants.GLOBAL_BLANK;
		responsibilityName = null;
		dateFrom = null;
		dateTo = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		
		return(errors);
	}
}
