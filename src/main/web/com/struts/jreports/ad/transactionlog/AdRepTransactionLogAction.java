package com.struts.jreports.ad.transactionlog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdRepTransactionLogController;
import com.ejb.txn.AdRepTransactionLogControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepTransactionLogDetails;

public final class AdRepTransactionLogAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("AdRepTransactionLogAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         AdRepTransactionLogForm actionForm = (AdRepTransactionLogForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
  
         String frParam = Common.getUserPermission(user, Constants.AD_REP_TRANSACTION_LOG_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adRepTransactionLog");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize AdRepTransactionLogController EJB
*******************************************************/

         AdRepTransactionLogControllerHome homeTL = null;
         AdRepTransactionLogController ejbTL = null;       

         try {
         	
            homeTL = (AdRepTransactionLogControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/AdRepTransactionLogControllerEJB", AdRepTransactionLogControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in AdRepTransactionLogAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbTL = homeTL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in AdRepTransactionLogAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AD TL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            AdRepTransactionLogDetails details = null;
            
            String company = null;
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbTL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		       
		       HashMap criteria = new HashMap();
		       
		       if (!Common.validateRequired(actionForm.getType())) {
	        		
	        		criteria.put("type", actionForm.getType());
	        		
	           }
		       
		       if (!Common.validateRequired(actionForm.getDocDateFrom())) {
	        		
	        		criteria.put("docDateFrom", Common.convertStringToSQLDate(actionForm.getDocDateFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDocDateTo())) {
	        		
	        		criteria.put("docDateTo", Common.convertStringToSQLDate(actionForm.getDocDateTo()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getCreatedBy())) {
	        		
	        		criteria.put("createdBy", actionForm.getCreatedBy());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateCreatedFrom())) {
	        		
	        		criteria.put("dateCreatedFrom", Common.convertStringToSQLDate(actionForm.getDateCreatedFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateCreatedTo())) {
	        		
	        		criteria.put("dateCreatedTo", Common.convertStringToSQLDate(actionForm.getDateCreatedTo()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getLastModifiedBy())) {
	        		
	        		criteria.put("lastModifiedBy", actionForm.getLastModifiedBy());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateLastModifiedFrom())) {
	        		
	        		criteria.put("dateLastModifiedFrom", Common.convertStringToSQLDate(actionForm.getDateLastModifiedFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateLastModifiedTo())) {
	        		
	        		criteria.put("dateLastModifiedTo", Common.convertStringToSQLDate(actionForm.getDateLastModifiedTo()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getApprovedRejectedBy())) {
	        		
	        		criteria.put("approvedRejectedBy", actionForm.getApprovedRejectedBy());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateApprovedRejectedFrom())) {
	        		
	        		criteria.put("dateApprovedRejectedFrom", Common.convertStringToSQLDate(actionForm.getDateApprovedRejectedFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateApprovedRejectedTo())) {
	        		
	        		criteria.put("dateApprovedRejectedTo", Common.convertStringToSQLDate(actionForm.getDateApprovedRejectedTo()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getPostedBy())) {
	        		
	        		criteria.put("postedBy", actionForm.getPostedBy());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDatePostedFrom())) {
	        		
	        		criteria.put("datePostedFrom", Common.convertStringToSQLDate(actionForm.getDatePostedFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDatePostedTo())) {
	        		
	        		criteria.put("datePostedTo", Common.convertStringToSQLDate(actionForm.getDatePostedTo()));
	        		
	           } 
	           
	           if (!Common.validateRequired(actionForm.getDeletedBy())) {
	        		
	        		criteria.put("deletedBy", actionForm.getDeletedBy());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateDeletedFrom())) {
	        		
	        		criteria.put("dateDeletedFrom", Common.convertStringToSQLDate(actionForm.getDateDeletedFrom()));
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDateDeletedTo())) {
	        		
	        		criteria.put("dateDeletedTo", Common.convertStringToSQLDate(actionForm.getDateDeletedTo()));
	        		
	           } 
	           
	           ArrayList branchList = new ArrayList();

	           for(int i=0; i<actionForm.getAdRepBrTransactionLogListSize(); i++) {

	        	   AdRepBranchTransactionLogList adBtlList = (AdRepBranchTransactionLogList)actionForm.getAdRepBrTransactionLogListByIndex(i);

	        	   if(adBtlList.getBranchCheckbox() == true) {                                          

	        		   AdBranchDetails brDetails = new AdBranchDetails();                                          

	        		   brDetails.setBrAdCompany(user.getCmpCode());	     	        
	        		   brDetails.setBrCode(adBtlList.getBrCode());                    

	        		   branchList.add(brDetails);

	        	   }
	           }

		       list = ejbTL.executeAdRepTransactionLog(criteria, branchList, actionForm.getOrderBy(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("transactionLog.error.noRecordFound"));
                     		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in AdRepTransactionLogAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("adRepTransactionLog"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
		    
		    if (!Common.validateRequired(actionForm.getType())) {
	        		
	    		parameters.put("type", actionForm.getType());
	    		
	       }
		    
		    if (!Common.validateRequired(actionForm.getDocDateFrom())) {
	    		
	    		parameters.put("docDateFrom", actionForm.getDocDateFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDocDateTo())) {
	    		
	    		parameters.put("docDateTo", actionForm.getDocDateTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getCreatedBy())) {
	    		
	    		parameters.put("createdBy", actionForm.getCreatedBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateCreatedFrom())) {
	    		
	    		parameters.put("dateCreatedFrom", actionForm.getDateCreatedFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateCreatedTo())) {
	    		
	    		parameters.put("dateCreatedTo", actionForm.getDateCreatedTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getLastModifiedBy())) {
	    		
	    		parameters.put("lastModifiedBy", actionForm.getLastModifiedBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateLastModifiedFrom())) {
	    		
	    		parameters.put("dateLastModifiedFrom", actionForm.getDateLastModifiedFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateLastModifiedTo())) {
	    		
	    		parameters.put("dateLastModifiedTo", actionForm.getDateLastModifiedTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getApprovedRejectedBy())) {
	    		
	    		parameters.put("approvedRejectedBy", actionForm.getApprovedRejectedBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateApprovedRejectedFrom())) {
	    		
	    		parameters.put("dateApprovedRejectedFrom", actionForm.getDateApprovedRejectedFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateApprovedRejectedTo())) {
	    		
	    		parameters.put("dateApprovedRejectedTo", actionForm.getDateApprovedRejectedTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getPostedBy())) {
	    		
	    		parameters.put("postedBy", actionForm.getPostedBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDatePostedFrom())) {
	    		
	    		parameters.put("datePostedFrom", actionForm.getDatePostedFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDatePostedTo())) {
	    		
	    		parameters.put("datePostedTo", actionForm.getDatePostedTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDeletedBy())) {
	    		
	    		parameters.put("deletedBy", actionForm.getDeletedBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateDeletedFrom())) {
	    		
	    		parameters.put("dateDeletedFrom", actionForm.getDateDeletedFrom());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDateDeletedTo())) {
	    		
	    		parameters.put("dateDeletedTo", actionForm.getDateDeletedTo());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getViewType())) {
    		
    		parameters.put("viewType", actionForm.getViewType());
    		
	       }
	       
	       String branchMap = null;
	       boolean first = true;
	       for(int j=0; j < actionForm.getAdRepBrTransactionLogListSize(); j++) {

	    	   AdRepBranchTransactionLogList brList = (AdRepBranchTransactionLogList)actionForm.getAdRepBrTransactionLogListByIndex(j);

	    	   if(brList.getBranchCheckbox() == true) {
	    		   if(first) {
	    			   branchMap = brList.getBrName();
	    			   first = false;
	    		   } else {
	    			   branchMap = branchMap + ", " + brList.getBrName();
	    		   }
	    	   }

	       }
	       parameters.put("branchMap", branchMap);
	       
	       String filename = "/opt/ofs-resources/" + user.getCompany() + "/AdRepTransactionLog.jasper";
	       
	       if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/AdRepTransactionLog.jasper");
		    
	       }
	       	
		   try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new AdRepTransactionLogDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new AdRepTransactionLogDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new AdRepTransactionLogDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in AdRepTransactionLogAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AD TL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AD TL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
			        ArrayList list = ejbTL.getAdUsrAll(user.getCmpCode());
			       
			        actionForm.clearCreatedByList();           	
            	
	            		            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCreatedByList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		Iterator i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCreatedByList((String)i.next());
	            			
	            		}
	            			            		
	            	}  
	            	
	            	actionForm.clearLastModifiedByList();           	
            		            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setLastModifiedByList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		Iterator i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setLastModifiedByList((String)i.next());
	            			
	            		}
	            			            		
	            	}   	            		            	
	            	
	            	actionForm.clearApprovedRejectedByList();           	
            		            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setApprovedRejectedByList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		Iterator i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setApprovedRejectedByList((String)i.next());
	            			
	            		}
	            			            		
	            	}
	            	
	            	actionForm.clearPostedByList();           	
            		            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setPostedByList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		Iterator i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setPostedByList((String)i.next());
	            			
	            		}
	            			            		
	            	}
	            	
	            	actionForm.clearDeletedByList();           	
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setDeletedByList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		Iterator i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setDeletedByList((String)i.next());
	            			
	            		}
	            			            		
	            	}
	            				       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in AdRepTransactionLogAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            actionForm.reset(mapping, request);

         		// populate AdRepBranchBankAccountListList
         		
         		ArrayList brList = new ArrayList();
         		actionForm.clearAdRepBrTransactionLogList();
         		
         		brList = ejbTL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
         		
         		Iterator j = brList.iterator();
         		
         		while(j.hasNext()) {
         			
         			AdBranchDetails details = (AdBranchDetails)j.next();
         			
         			AdRepBranchTransactionLogList adBtlList = new AdRepBranchTransactionLogList(actionForm,
         					details.getBrBranchCode(), details.getBrName(),
							details.getBrCode());
         			adBtlList.setBranchCheckbox(true);
         			
         			actionForm.saveAdRepBrTransactionLogList(adBtlList);
         			
         		}

	            return(mapping.findForward("adRepTransactionLog"));		          
			            
			 } else {
			 	
			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));
			
			    return(mapping.findForward("cmnMain"));
			
			 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in AdRepTransactionLogAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
