package com.struts.jreports.ad.transactionlog;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.AdRepTransactionLogDetails;



public class AdRepTransactionLogDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepTransactionLogDS(ArrayList list) {
                                                             

      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         AdRepTransactionLogDetails details = (AdRepTransactionLogDetails)i.next();
                                    
	     AdRepTransactionLogData tlData = new AdRepTransactionLogData(
	     	 details.getTxlType(),
	         details.getTxlDate(), 
	         details.getTxlDocumentNumber(), 
	         new Double(details.getTxlAmount()),
	         details.getTxlApprovalStatus(),
	         details.getTxlPosted() == (byte)1 ? "YES" : "NO",
	         details.getTxlCreatedBy(),
	         details.getTxlDateCreated(),
	         details.getTxlLastModifiedBy(),
	         details.getTxlDateLastModified(),
	         details.getTxlApprovedRejectedBy(),
	         details.getTxlDateApprovedRejected(),
	         details.getTxlPostedBy(),
	         details.getTxlDatePosted(),
	         details.getTxlVoid() == (byte)1 ? "YES" : "NO");
		    
         data.add(tlData);
      }  	   	   	     	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("type".equals(fieldName)){
         value = ((AdRepTransactionLogData)data.get(index)).getType();       
      }else if("transactionDate".equals(fieldName)){
         value = ((AdRepTransactionLogData)data.get(index)).getTransactionDate();
         System.out.println("value: " + value);
      }else if("documentNumber".equals(fieldName)){
         value = ((AdRepTransactionLogData)data.get(index)).getDocumentNumber();
      }else if("amount".equals(fieldName)){
         value = ((AdRepTransactionLogData)data.get(index)).getAmount();
      }else if("approvalStatus".equals(fieldName)){
         value = ((AdRepTransactionLogData)data.get(index)).getApprovalStatus();         
      }else if("posted".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getPosted();         
      }else if("createdBy".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getCreatedBy();         
      }else if("dateCreated".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getDateCreated();    
        System.out.println("Date Created: " + value);
      }else if("lastModifiedBy".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getLastModifiedBy();         
      }else if("dateLastModified".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getDateLastModified(); 
        System.out.println("Date Last Modify: " + value);
      }else if("approvedRejectedBy".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getApprovedRejectedBy();         
      }else if("dateApprovedRejected".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getDateApprovedRejected();         
      }else if("postedBy".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getPostedBy();         
      }else if("datePosted".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getDatePosted();         
      }else if("txnVoid".equals(fieldName)){
        value = ((AdRepTransactionLogData)data.get(index)).getTxnVoid();         
      }

      return(value);
   }
}
