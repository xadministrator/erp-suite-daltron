package com.struts.jreports.ad.transactionlog;

public class AdRepTransactionLogData implements java.io.Serializable {

   private String type = null;
   private java.util.Date transactionDate = null;
   private String documentNumber = null;
   private Double amount = null;
   private String approvalStatus = null;
   private String posted = null;
   private String createdBy = null;
   private java.util.Date dateCreated = null;
   private String lastModifiedBy = null;
   private java.util.Date dateLastModified = null;
   private String approvedRejectedBy = null;
   private java.util.Date dateApprovedRejected = null;
   private String postedBy = null;
   private java.util.Date datePosted = null;
   private String txnvoid = null;
   
   public AdRepTransactionLogData(String type, java.util.Date transactionDate, String documentNumber,
   		Double amount, String approvalStatus, String posted, String createdBy, java.util.Date dateCreated, String lastModifiedBy,
   		java.util.Date dateLastModified, String approvedRejectedBy, java.util.Date dateApprovedRejected, 
		String postedBy, java.util.Date datePosted, String txnvoid) {
      	
      	this.type = type;
      	this.transactionDate = transactionDate;
      	this.documentNumber = documentNumber;
      	this.amount = amount;
      	this.approvalStatus = approvalStatus;
      	this.posted = posted;
        this.createdBy = createdBy;
      	this.dateCreated = dateCreated;
      	this.lastModifiedBy = lastModifiedBy;
      	this.dateLastModified = dateLastModified;
      	this.approvedRejectedBy = approvedRejectedBy;
        this.dateApprovedRejected = dateApprovedRejected;
      	this.postedBy = postedBy;
        this.datePosted = datePosted;
        this.txnvoid = txnvoid;
      	
   }

   public String getType() {
   	
   	    return type;
   
   }
   
   public void setType(String type) {
   	
   		this.type = type;
   	
   }
   
   public java.util.Date getTransactionDate() {
   	
   	    return transactionDate;
   
   }
   
   public void setTransactionDate(java.util.Date transactionDate) {
   	
   		this.transactionDate = transactionDate;
   	
   }
   
   public String getDocumentNumber() {
   	
   	    return documentNumber;
   
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   		this.documentNumber = documentNumber;
   	
   }
   
   public Double getAmount() {
   	
   	    return amount;
   
   }
   
   public void setAmount(Double amount) {
   	
   		this.amount = amount;
   	
   }

   public String getApprovalStatus() {
   	
   	    return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   		this.approvalStatus = approvalStatus;
   	
   }
   
   public String getPosted() {
   	
   	    return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   		this.posted = posted;
   	
   }
   
   public String getCreatedBy() {
   	
   	    return createdBy;
   
   }
   
   public void setCreatedBy(String createdBy) {
   	
   		this.createdBy = createdBy;
   	
   }
   
   public java.util.Date getDateCreated() {
   	
   	    return dateCreated;
   
   }
   
   public void setDateCreated(java.util.Date dateCreated) {
   	
   		this.dateCreated = dateCreated;
   	
   }
   
   public String getLastModifiedBy() {
   	
   	    return lastModifiedBy;
   
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   		this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public java.util.Date getDateLastModified() {
   	
   	    return dateLastModified;
   
   }
   
   public void setDateLastModified(java.util.Date dateLastModified) {
   	
   		this.dateLastModified = dateLastModified;
   	
   }
   
   public String getApprovedRejectedBy() {
   	
   	    return approvedRejectedBy;
   
   }
   
   public void setApprovedRejectedBy(String approvedRejectedBy) {
   	
   		this.approvedRejectedBy = approvedRejectedBy;
   	
   }
   
   public java.util.Date getDateApprovedRejected() {
   	
   	    return dateApprovedRejected;
   
   }
   
   public void setDateApprovedRejected(java.util.Date dateApprovedRejected) {
   	
   		this.dateApprovedRejected = dateApprovedRejected;
   	
   }
   
   public String getPostedBy() {
   	
   	    return postedBy;
   
   }
   
   public void setPostedBy(String postedBy) {
   	
   		this.postedBy = postedBy;
   	
   }
   
   public java.util.Date getDatePosted() {
   	
   	    return datePosted;
   
   }
   
   public void setDatePosted(java.util.Date datePosted) {
   	
   		this.datePosted = datePosted;
   	
   }
   
   public String getTxnVoid() {
	   	
  	    return txnvoid;
  
  }
  
  public void setTxnVoid(String txnvoid) {
  	
  		this.txnvoid = txnvoid;
  	
  }
}
