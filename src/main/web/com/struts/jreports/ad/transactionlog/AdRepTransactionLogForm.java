package com.struts.jreports.ad.transactionlog;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class AdRepTransactionLogForm extends ActionForm implements Serializable{
	
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String createdBy = null;
	private ArrayList createdByList = new ArrayList();
	private String docDateFrom = null;
	private String docDateTo = null;
	private String dateCreatedFrom = null;
	private String dateCreatedTo = null;
	private String lastModifiedBy = null;
	private ArrayList lastModifiedByList = new ArrayList();
	private String dateLastModifiedFrom = null;
	private String dateLastModifiedTo = null;
	private String approvedRejectedBy = null;
	private ArrayList approvedRejectedByList = new ArrayList();
	private String dateApprovedRejectedFrom = null;
	private String dateApprovedRejectedTo = null;
	private String postedBy = null;
	private ArrayList postedByList = new ArrayList();
	private String datePostedFrom = null;
	private String datePostedTo = null;
	private String deletedBy = null;
	private ArrayList deletedByList = new ArrayList();
	private String dateDeletedFrom = null;
	private String dateDeletedTo = null;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	
	private String userPermission = new String();
	
	private ArrayList adRepBrTransactionLogList = new ArrayList();
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}   
	
	public ArrayList getCreatedByList() {
		
		return createdByList;
		
	}
	
	public void setCreatedByList(String createdBy) {
		
		createdByList.add(createdBy);
		
	}
	
	public void clearCreatedByList() {
		
		createdByList.clear();
		createdByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDocDateFrom() {
		
		return docDateFrom;
		
	}
	
	public void setDocDateFrom(String docDateFrom) {
		
		this.docDateFrom = docDateFrom;
		
	}
	
	public String getDocDateTo() {
		
		return docDateTo;
		
	}
	
	public void setDocDateTo(String docDateTo) {
		
		this.docDateTo = docDateTo;
		
	}
	
	public String getDateCreatedFrom() {
		
		return dateCreatedFrom;
		
	}
	
	public void setDateCreatedFrom(String dateCreatedFrom) {
		
		this.dateCreatedFrom = dateCreatedFrom;
		
	}
	
	public String getDateCreatedTo() {
		
		return dateCreatedTo;
		
	}
	
	public void setDateCreatedTo(String dateCreatedTo) {
		
		this.dateCreatedTo = dateCreatedTo;
		
	}
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}   
	
	public ArrayList getLastModifiedByList() {
		
		return lastModifiedByList;
		
	}
	
	public void setLastModifiedByList(String lastModifiedBy) {
		
		lastModifiedByList.add(lastModifiedBy);
		
	}
	
	public void clearLastModifiedByList() {
		
		lastModifiedByList.clear();
		lastModifiedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateLastModifiedFrom() {
		
		return dateLastModifiedFrom;
		
	}
	
	public void setDateLastModifiedFrom(String dateLastModifiedFrom) {
		
		this.dateLastModifiedFrom = dateLastModifiedFrom;
		
	}
	
	public String getDateLastModifiedTo() {
		
		return dateLastModifiedTo;
		
	}
	
	public void setDateLastModifiedTo(String dateLastModifiedTo) {
		
		this.dateLastModifiedTo = dateLastModifiedTo;
		
	}
	
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}   
	
	public ArrayList getApprovedRejectedByList() {
		
		return approvedRejectedByList;
		
	}
	
	public void setApprovedRejectedByList(String approvedRejectedBy) {
		
		approvedRejectedByList.add(approvedRejectedBy);
		
	}
	
	public void clearApprovedRejectedByList() {
		
		approvedRejectedByList.clear();
		approvedRejectedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateApprovedRejectedFrom() {
		
		return dateApprovedRejectedFrom;
		
	}
	
	public void setDateApprovedRejectedFrom(String dateApprovedRejectedFrom) {
		
		this.dateApprovedRejectedFrom = dateApprovedRejectedFrom;
		
	}
	
	public String getDateApprovedRejectedTo() {
		
		return dateApprovedRejectedTo;
		
	}
	
	public void setDateApprovedRejectedTo(String dateApprovedRejectedTo) {
		
		this.dateApprovedRejectedTo = dateApprovedRejectedTo;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}   
	
	public ArrayList getPostedByList() {
		
		return postedByList;
		
	}
	
	public void setPostedByList(String postedBy) {
		
		postedByList.add(postedBy);
		
	}
	
	public void clearPostedByList() {
		
		postedByList.clear();
		postedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDatePostedFrom() {
		
		return datePostedFrom;
		
	}
	
	public void setDatePostedFrom(String datePostedFrom) {
		
		this.datePostedFrom = datePostedFrom;
		
	}
	
	public String getDatePostedTo() {
		
		return datePostedTo;
		
	}
	
	public void setDatePostedTo(String datePostedTo) {
		
		this.datePostedTo = datePostedTo;
		
	}
	
	public String getDeletedBy() {
		
		return deletedBy;
		
	}
	
	public void setDeletedBy(String deletedBy) {
		
		this.deletedBy = deletedBy;
		
	}   
	
	public ArrayList getDeletedByList() {
		
		return deletedByList;
		
	}
	
	public void setDeletedByList(String deletedBy) {
		
		deletedByList.add(deletedBy);
		
	}
	
	public void clearDeletedByList() {
		
		deletedByList.clear();
		deletedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateDeletedFrom() {
		
		return dateDeletedFrom;
		
	}
	
	public void setDateDeletedFrom(String dateDeletedFrom) {
		
		this.dateDeletedFrom = dateDeletedFrom;
		
	}
	
	public String getDateDeletedTo() {
		
		return dateDeletedTo;
		
	}
	
	public void setDateDeletedTo(String dateDeletedTo) {
		
		this.dateDeletedTo = dateDeletedTo;
		
	}
	
	public String getViewType() {
		
		return viewType;   	
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	} 
	
	public String getOrderBy(){
		return(orderBy);   	
	}
	
	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}
	
	public ArrayList getOrderByList(){
		return orderByList;
	}  
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Object[] getAdRepBrTransactionLogList(){
		
		return adRepBrTransactionLogList.toArray();
		
	}
	
	public AdRepBranchTransactionLogList getAdRepBrTransactionLogListByIndex(int index){
		
		return ((AdRepBranchTransactionLogList)adRepBrTransactionLogList.get(index));
		
	}
	
	public int getAdRepBrTransactionLogListSize(){
		
		return(adRepBrTransactionLogList.size());
		
	}
	
	public void saveAdRepBrTransactionLogList(Object newAdRepBrTransactionLogList){
		
		adRepBrTransactionLogList.add(newAdRepBrTransactionLogList);   	  
		
	}
	
	public void clearAdRepBrTransactionLogList(){
		
		adRepBrTransactionLogList.clear();
		
	}
	
	public void setAdRepBrTransactionLogList(ArrayList adRepBrTransactionLogList) {
		
		this.adRepBrTransactionLogList = adRepBrTransactionLogList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {      
		
		for (int i=0; i<adRepBrTransactionLogList.size(); i++) {
			
			AdRepBranchTransactionLogList list = (AdRepBranchTransactionLogList)adRepBrTransactionLogList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		typeList.add("GL JOURNAL");
		typeList.add("AP VOUCHER");
		typeList.add("AP CHECK");
		typeList.add("AP PURCHASE ORDER");
		typeList.add("AR INVOICE");
		typeList.add("AR RECEIPT");
		typeList.add("AR SALES ORDER");
		typeList.add("CM ADJUSTMENT");
		typeList.add("CM FUND TRANSFER");
		typeList.add("INV ADJUSTMENT");
		typeList.add("INV BUILD ASSEMBLY");		
		typeList.add("INV BUILD ORDER");
		typeList.add("INV STOCK ISSUANCE");
		typeList.add("INV ASSEMBLY TRANSFER");
		type = Constants.GLOBAL_BLANK;
		docDateFrom = null;
		docDateTo = null;
		createdBy = Constants.GLOBAL_BLANK;
		dateCreatedFrom = null;
		dateCreatedTo = null;
		lastModifiedBy = Constants.GLOBAL_BLANK;
		dateLastModifiedFrom = null;
		dateCreatedTo = null;
		approvedRejectedBy = Constants.GLOBAL_BLANK;
		dateApprovedRejectedFrom = null;
		dateApprovedRejectedTo = null;
		postedBy = Constants.GLOBAL_BLANK;
		datePostedFrom = null;
		datePostedTo = null;
		deletedBy = Constants.GLOBAL_BLANK;
		dateDeletedFrom = null;
		dateDeletedTo = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		if (orderByList.isEmpty()) {
			
			orderByList.clear();
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("TYPE");
			orderByList.add("TRANSACTION DATE");
			orderByList.add("CREATED BY");
			orderByList.add("DATE CREATED");
			orderByList.add("LAST MODIFIED BY");
			orderByList.add("DATE LAST MODIFIED");
			
			orderBy = "TYPE";   
			
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(docDateFrom)) {
				
				errors.add("docDateFrom", new ActionMessage("transactionLog.error.docDateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(docDateTo)) {
				
				errors.add("docDateTo", new ActionMessage("transactionLog.error.docDateToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateCreatedFrom)) {
				
				errors.add("dateCreatedFrom", new ActionMessage("transactionLog.error.dateCreatedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateCreatedTo)) {
				
				errors.add("dateCreatedTo", new ActionMessage("transactionLog.error.dateCreatedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateLastModifiedFrom)) {
				
				errors.add("dateLastModifiedFrom", new ActionMessage("transactionLog.error.dateLastModifiedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateLastModifiedTo)) {
				
				errors.add("dateLastModifiedTo", new ActionMessage("transactionLog.error.dateLastModifiedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateApprovedRejectedFrom)) {
				
				errors.add("dateApprovedRejectedFrom", new ActionMessage("transactionLog.error.dateApprovedRejectedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateApprovedRejectedTo)) {
				
				errors.add("dateApprovedRejectedTo", new ActionMessage("transactionLog.error.dateApprovedRejectedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(datePostedFrom)) {
				
				errors.add("datePostedFrom", new ActionMessage("transactionLog.error.datePostedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(datePostedTo)) {
				
				errors.add("datePostedTo", new ActionMessage("transactionLog.error.datePostedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateDeletedFrom)) {
				
				errors.add("dateDeletedFrom", new ActionMessage("transactionLog.error.dateDeletedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateDeletedTo)) {
				
				errors.add("dateDeletedTo", new ActionMessage("transactionLog.error.dateDeletedToInvalid"));
				
			}
			
		}
		return errors;
	}
}
