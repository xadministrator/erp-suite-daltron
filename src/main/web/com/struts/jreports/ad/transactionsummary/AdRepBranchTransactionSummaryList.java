package com.struts.jreports.ad.transactionsummary;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 11/11/2005 9:21 AM
 * 
 */
public class AdRepBranchTransactionSummaryList implements Serializable {
	
	private String brBranchCode = null;
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	
	private AdRepTransactionSummaryForm parentBean;
	
	public AdRepBranchTransactionSummaryList(AdRepTransactionSummaryForm parentBean, 
			String brBranchCode, String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		this.brBranchCode = brBranchCode;
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void getBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}

