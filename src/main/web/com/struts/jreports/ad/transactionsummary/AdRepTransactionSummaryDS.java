package com.struts.jreports.ad.transactionsummary;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.AdRepTransactionSummaryDetails;



public class AdRepTransactionSummaryDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepTransactionSummaryDS(ArrayList list) {
    
      Iterator i = list.iterator();
      
      int glj = 0;
      int apv = 0;
      int apd = 0;
      int arin = 0;
      int cma = 0;
      int apc = 0;
      int arc = 0;
      int arr = 0;
      int arso = 0;
      int cmf = 0;
      int inva = 0;
      int invbo = 0;
      int invba = 0;
      int invsi = 0;
      int invat = 0;
      int invo = 0;
      
      Integer intCtr = null;
   
      while (i.hasNext()) {
      	
         AdRepTransactionSummaryDetails details = (AdRepTransactionSummaryDetails)i.next();
         
         if(details.getTxlType()=="GL JOURNAL"){
         
         	glj++;
         	
         }
         
         if(details.getTxlType()=="AP VOUCHER"){
            
            	apv++;
            	
            }
         if(details.getTxlType()=="AP DEBIT MEMO"){
            
            	apd++;
            	
            }
         
         if(details.getTxlType()=="AP CHECK"){
            
            	apc++;
            	
            }
         
         if(details.getTxlType()=="AR INVOICE"){
            
            	arin++;
            	
            }
         
         if(details.getTxlType()=="AR CREDIT MEMO"){
            
            	arc++;
            	
            }
         if(details.getTxlType()=="AR RECEIPT"){
            
            	arr++;
            	
            }
         if(details.getTxlType()=="AR SALES ORDER"){
            
            	arso++;
            	
            }
         if(details.getTxlType()=="CM ADJUSTMENT"){
            
            	cma++;
            	
            }
         if(details.getTxlType()=="CM FUND TRANSFER"){
            
            	cmf++;
            	
            }
         if(details.getTxlType()=="INV ADJUSTMENT"){
            
            	inva++;
            	
            }
            
         if(details.getTxlType()=="INV BUILD ORDER"){
               
               	invbo++;
               	
            }
         if(details.getTxlType()=="INV BUILD ASSEMBLY"){
               
               	invba++;
               	
            }
            
         if(details.getTxlType()=="INV ASSEMBLY TRANSFER"){
               
               	invat++;
               	
            }
            
         if(details.getTxlType()=="INV STOCK ISSUANCE"){
               
               	invsi++;
               	
            }
            
         if(details.getTxlType()=="INV OVERHEAD"){
               
               	invo++;
               	
            }

      }  
      if(glj!=0){
      	
      		intCtr = new Integer(glj);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("GL JOURNAL",intCtr);
      		data.add(tlData);
      		
      } 
      if(apv!=0){
      	
      	intCtr = new Integer(apv);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AP VOUCHER",intCtr);
      		data.add(tlData);
      		
      } 
      if(apc!=0){
      	
      	intCtr = new Integer(apc);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AP CHECK",intCtr);
      		data.add(tlData);
      		
      } 
      if(apd!=0){
      	
      	intCtr = new Integer(apd);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AP DEBIT MEMO",intCtr);
      		data.add(tlData);
      		
      } 
      if(arin!=0){
      	
      	intCtr = new Integer(arin);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AR INVOICE",intCtr);
      		data.add(tlData);
      		
      }  
      if(arc!=0){
      	
      	intCtr = new Integer(arc);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AR CREDIT MEMO",intCtr);
      		data.add(tlData);
      		
      }  
      if(arr!=0){
      	
      	intCtr = new Integer(arr);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AR RECEIPT",intCtr);
      		data.add(tlData);
      		
      }  
      if(arso!=0){
      	
      	intCtr = new Integer(arso);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("AR SALES ORDER",intCtr);
      		data.add(tlData);
      		
      } 
      if(cma!=0){
      	
      	intCtr = new Integer(cma);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("CM ADJUSTMENT",intCtr);
      		data.add(tlData);
      		
      }  
      if(cmf!=0){
      	
      	intCtr = new Integer(cmf);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("CM FUND TRANSFER",intCtr);
      		data.add(tlData);
      		
      }  
      if(inva!=0){
      	
      	intCtr = new Integer(inva);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV ADJUSTMENT",intCtr);
      		data.add(tlData);
      		
      }  
      if(invbo!=0){
      	
      	intCtr = new Integer(invbo);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV BUILD ORDER",intCtr);
      		data.add(tlData);
      		
      }  
      if(invba!=0){
      	
      	intCtr = new Integer(invba);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV BUILD ASSEMBLY",intCtr);
      		data.add(tlData);
      		
      }  
      if(invat!=0){
      	
      	intCtr = new Integer(invat);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV ASSEMBLY TRANSFER",intCtr);
      		data.add(tlData);
      		
      }  
      if(invsi!=0){
      	
      	intCtr = new Integer(invsi);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV STOCK ISSUANCE",intCtr);
      		data.add(tlData);
      		
      }  
      if(invo!=0){
      	
      	intCtr = new Integer(invo);
      		AdRepTransactionSummaryData tlData = new AdRepTransactionSummaryData("INV OVERHEAD",intCtr);
      		data.add(tlData);
      		
      }  
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("type".equals(fieldName)){
      	 value = ((AdRepTransactionSummaryData)data.get(index)).getType();
      }else if ("typeCtr".equals(fieldName)){
      	 value = ((AdRepTransactionSummaryData)data.get(index)).getTypeCtr();
      }


      return(value);
   }
}
