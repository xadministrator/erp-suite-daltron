package com.struts.jreports.ad.transactionsummary;

public class AdRepTransactionSummaryData implements java.io.Serializable {

   private String type = null;
   private Integer typeCtr = null;

   
   public AdRepTransactionSummaryData(String type, Integer typeCtr) {
      	
      	this.type = type;
      	this.typeCtr = typeCtr;

      	
   }

   public String getType() {
   	
   	    return type;
   
   }
   
   public void setType(String type) {
   	
   		this.type = type;
   	
   }
   
   public Integer getTypeCtr() {
   	
   	    return typeCtr;
   
   }
   
   public void setTypeCtr(Integer typeCtr) {
   	
   		this.typeCtr = typeCtr;
   	
   }

}
