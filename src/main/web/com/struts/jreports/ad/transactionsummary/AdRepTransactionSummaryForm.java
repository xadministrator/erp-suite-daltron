package com.struts.jreports.ad.transactionsummary;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class AdRepTransactionSummaryForm extends ActionForm implements Serializable{
	
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String docDateFrom = null;
	private String docDateTo = null;
	private String createdBy = null;
	private ArrayList createdByList = new ArrayList();
	private String dateCreatedFrom = null;
	private String dateCreatedTo = null;
	private String lastModifiedBy = null;
	private ArrayList lastModifiedByList = new ArrayList();
	private String dateLastModifiedFrom = null;
	private String dateLastModifiedTo = null;
	private String approvedRejectedBy = null;
	private ArrayList approvedRejectedByList = new ArrayList();
	private String dateApprovedRejectedFrom = null;
	private String dateApprovedRejectedTo = null;
	private String postedBy = null;
	private ArrayList postedByList = new ArrayList();
	private String datePostedFrom = null;
	private String datePostedTo = null;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	
	private String userPermission = new String();
	
	private ArrayList adRepBrTransactionSummaryList = new ArrayList();
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public String getDocDateFrom() {
		
		return docDateFrom;
		
	}
	
	public void setDocDateFrom(String docDateFrom) {
		
		this.docDateFrom = docDateFrom;
		
	}
	
	public String getDocDateTo() {
		
		return docDateTo;
		
	}
	
	public void setDocDateTo(String docDateTo) {
		
		this.docDateTo = docDateTo;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}   
	
	public ArrayList getCreatedByList() {
		
		return createdByList;
		
	}
	
	public void setCreatedByList(String createdBy) {
		
		createdByList.add(createdBy);
		
	}
	
	public void clearCreatedByList() {
		
		createdByList.clear();
		createdByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateCreatedFrom() {
		
		return dateCreatedFrom;
		
	}
	
	public void setDateCreatedFrom(String dateCreatedFrom) {
		
		this.dateCreatedFrom = dateCreatedFrom;
		
	}
	
	public String getDateCreatedTo() {
		
		return dateCreatedTo;
		
	}
	
	public void setDateCreatedTo(String dateCreatedTo) {
		
		this.dateCreatedTo = dateCreatedTo;
		
	}
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}   
	
	public ArrayList getLastModifiedByList() {
		
		return lastModifiedByList;
		
	}
	
	public void setLastModifiedByList(String lastModifiedBy) {
		
		lastModifiedByList.add(lastModifiedBy);
		
	}
	
	public void clearLastModifiedByList() {
		
		lastModifiedByList.clear();
		lastModifiedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateLastModifiedFrom() {
		
		return dateLastModifiedFrom;
		
	}
	
	public void setDateLastModifiedFrom(String dateLastModifiedFrom) {
		
		this.dateLastModifiedFrom = dateLastModifiedFrom;
		
	}
	
	public String getDateLastModifiedTo() {
		
		return dateLastModifiedTo;
		
	}
	
	public void setDateLastModifiedTo(String dateLastModifiedTo) {
		
		this.dateLastModifiedTo = dateLastModifiedTo;
		
	}
	
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}   
	
	public ArrayList getApprovedRejectedByList() {
		
		return approvedRejectedByList;
		
	}
	
	public void setApprovedRejectedByList(String approvedRejectedBy) {
		
		approvedRejectedByList.add(approvedRejectedBy);
		
	}
	
	public void clearApprovedRejectedByList() {
		
		approvedRejectedByList.clear();
		approvedRejectedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateApprovedRejectedFrom() {
		
		return dateApprovedRejectedFrom;
		
	}
	
	public void setDateApprovedRejectedFrom(String dateApprovedRejectedFrom) {
		
		this.dateApprovedRejectedFrom = dateApprovedRejectedFrom;
		
	}
	
	public String getDateApprovedRejectedTo() {
		
		return dateApprovedRejectedTo;
		
	}
	
	public void setDateApprovedRejectedTo(String dateApprovedRejectedTo) {
		
		this.dateApprovedRejectedTo = dateApprovedRejectedTo;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}   
	
	public ArrayList getPostedByList() {
		
		return postedByList;
		
	}
	
	public void setPostedByList(String postedBy) {
		
		postedByList.add(postedBy);
		
	}
	
	public void clearPostedByList() {
		
		postedByList.clear();
		postedByList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDatePostedFrom() {
		
		return datePostedFrom;
		
	}
	
	public void setDatePostedFrom(String datePostedFrom) {
		
		this.datePostedFrom = datePostedFrom;
		
	}
	
	public String getDatePostedTo() {
		
		return datePostedTo;
		
	}
	
	public void setDatePostedTo(String datePostedTo) {
		
		this.datePostedTo = datePostedTo;
		
	}
	
	public String getViewType() {
		
		return viewType;   	
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	} 
	
	public String getOrderBy(){
		return(orderBy);   	
	}
	
	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}
	
	public ArrayList getOrderByList(){
		return orderByList;
	}  
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Object[] getAdRepBrTransactionSummaryList(){
		
		return adRepBrTransactionSummaryList.toArray();
		
	}
	
	public AdRepBranchTransactionSummaryList getAdRepBrTransactionSummaryListByIndex(int index){
		
		return ((AdRepBranchTransactionSummaryList)adRepBrTransactionSummaryList.get(index));
		
	}
	
	public int getAdRepBrTransactionSummaryListSize(){
		
		return(adRepBrTransactionSummaryList.size());
		
	}
	
	public void saveAdRepBrTransactionSummaryList(Object newAdRepBrTransactionSummaryList){
		
		adRepBrTransactionSummaryList.add(newAdRepBrTransactionSummaryList);   	  
		
	}
	
	public void clearAdRepBrTransactionSummaryList(){
		
		adRepBrTransactionSummaryList.clear();
		
	}
	
	public void setAdRepBrTransactionSummaryList(ArrayList adRepBrTransactionSummaryList) {
		
		this.adRepBrTransactionSummaryList = adRepBrTransactionSummaryList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {      
		
		for (int i=0; i<adRepBrTransactionSummaryList.size(); i++) {
			
			AdRepBranchTransactionSummaryList list = (AdRepBranchTransactionSummaryList)adRepBrTransactionSummaryList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		typeList.add("GL JOURNAL");
		typeList.add("AP VOUCHER");
		typeList.add("AP CHECK");
		typeList.add("AR INVOICE");
		typeList.add("AR RECEIPT");
		typeList.add("AR SALES ORDER");
		typeList.add("CM ADJUSTMENT");
		typeList.add("CM FUND TRANSFER"); 
		typeList.add("INV ADJUSTMENT");
		typeList.add("INV BUILD ASSEMBLY");
		typeList.add("INV OVERHEAD");
		typeList.add("INV BUILD ORDER");
		typeList.add("INV STOCK ISSUANCE");
		typeList.add("INV ASSEMBLY TRANSFER");
		type = Constants.GLOBAL_BLANK;
		docDateFrom = null;
		docDateTo = null;
		createdBy = Constants.GLOBAL_BLANK;
		dateCreatedFrom = null;
		dateCreatedTo = null;
		lastModifiedBy = Constants.GLOBAL_BLANK;
		dateLastModifiedFrom = null;
		dateCreatedTo = null;
		createdBy = Constants.GLOBAL_BLANK;
		dateCreatedFrom = null;
		dateCreatedTo = null;
		createdBy = Constants.GLOBAL_BLANK;
		dateCreatedFrom = null;
		dateCreatedTo = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		if (orderByList.isEmpty()) {
			
			orderByList.clear();
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("TYPE");
			orderByList.add("DOC DATE");
			orderByList.add("TRANSACTION DATE");
			orderByList.add("CREATED BY");
			orderByList.add("DATE CREATED");
			orderByList.add("LAST MODIFIED BY");
			orderByList.add("DATE LAST MODIFIED");
			
			orderBy = "TYPE";   
			
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(docDateFrom)) {
				
				errors.add("docDateFrom", new ActionMessage("transactionSummary.error.docDateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(docDateTo)) {
				
				errors.add("docDateTo", new ActionMessage("transactionSummary.error.docDateToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateCreatedFrom)) {
				
				errors.add("dateCreatedFrom", new ActionMessage("transactionSummary.error.dateCreatedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateCreatedTo)) {
				
				errors.add("dateCreatedTo", new ActionMessage("transactionSummary.error.dateCreatedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateLastModifiedFrom)) {
				
				errors.add("dateLastModifiedFrom", new ActionMessage("transactionSummary.error.dateLastModifiedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateLastModifiedTo)) {
				
				errors.add("dateLastModifiedTo", new ActionMessage("transactionSummary.error.dateLastModifiedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateApprovedRejectedFrom)) {
				
				errors.add("dateApprovedRejectedFrom", new ActionMessage("transactionSummary.error.dateApprovedRejectedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateApprovedRejectedTo)) {
				
				errors.add("dateApprovedRejectedTo", new ActionMessage("transactionSummary.error.dateApprovedRejectedToInvalid"));
				
			}
			
			if(!Common.validateDateFormat(datePostedFrom)) {
				
				errors.add("datePostedFrom", new ActionMessage("transactionLog.error.datePostedFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(datePostedTo)) {
				
				errors.add("datePostedTo", new ActionMessage("transactionSummary.error.datePostedToInvalid"));
				
			}
			
		}
		return errors;
	}
}
