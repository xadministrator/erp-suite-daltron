package com.struts.jreports.ad.userlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Constants;
import com.struts.util.Common;
import com.util.AdRepUserListDetails;

public class AdRepUserListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public AdRepUserListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         AdRepUserListDetails details = (AdRepUserListDetails)i.next();

         String passwordExpirationCode = Constants.AD_USR_NONE;
         
         if(details.getUlPasswordExpirationCode() == 0) {
         	
         	passwordExpirationCode = Constants.AD_USR_DAYS;
         	
         } else if(details.getUlPasswordExpirationCode() == 1) {
         	
         	passwordExpirationCode = Constants.AD_USR_ACCESS;
         	
         }
         	
         AdRepUserListData dtbData = new AdRepUserListData(details.getUlUserName(),	details.getUlUserDescription(),
	     		Common.convertSQLDateToString(details.getUlDateFrom()), Common.convertSQLDateToString(details.getUlDateTo()),
				passwordExpirationCode,	Common.convertShortToString(details.getUlPasswordExpirationDays()),
				Common.convertShortToString(details.getUlPasswordExpirationAccess()), details.getUlResponsibilityName(), 
				details.getUlResponsibilityDescription(), Common.convertSQLDateToString(details.getUlResponsibilityDateFrom()),
				Common.convertSQLDateToString(details.getUlResponsibilityDateTo()));
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("userName".equals(fieldName)) {
      	
          value = ((AdRepUserListData)data.get(index)).getUserName();
      
      } else if("userDescription".equals(fieldName)) {
      
          value = ((AdRepUserListData)data.get(index)).getUserDescription();

      } else if("dateFrom".equals(fieldName)) {
        
            value = ((AdRepUserListData)data.get(index)).getDateFrom();

      } else if("dateTo".equals(fieldName)) {
        
            value = ((AdRepUserListData)data.get(index)).getDateTo();

      } else if("passwordExpirationCode".equals(fieldName)) {
        
            value = ((AdRepUserListData)data.get(index)).getPasswordExpirationCode();

      } else if("passwordExpirationDays".equals(fieldName)) {
        
            value = ((AdRepUserListData)data.get(index)).getPasswordExpirationDays();

      } else if("passwordExpirationAccess".equals(fieldName)) {
        
            value = ((AdRepUserListData)data.get(index)).getPasswordExpirationAccess();

      } else if("responsibilityName".equals(fieldName)) {
      	
          value = ((AdRepUserListData)data.get(index)).getResponsibilityName();
      
      } else if("responsibilityDescription".equals(fieldName)) {
      
          value = ((AdRepUserListData)data.get(index)).getResponsibilityDescription();

      } else if("responsibilityDateFrom".equals(fieldName)) {
      
          value = ((AdRepUserListData)data.get(index)).getResponsibilityDateFrom();

      } else if("responsibilityDateTo".equals(fieldName)) {
      
          value = ((AdRepUserListData)data.get(index)).getResponsibilityDateTo();
      
      }

      return(value);
   }
}
