package com.struts.jreports.ad.userlist;

public class AdRepUserListData implements java.io.Serializable {
	
   private String userName = null;
   private String userDescription = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String passwordExpirationCode = null;
   private String passwordExpirationDays = null;
   private String passwordExpirationAccess = null;
   private String responsibilityName = null;
   private String responsibilityDescription = null;
   private String responsibilityDateFrom = null;
   private String responsibilityDateTo = null;
   
   public AdRepUserListData(String userName, String userDescription, String dateFrom, String dateTo,
   		String passwordExpirationCode, String passwordExpirationDays, String passwordExpirationAccess,
		String responsibilityName, String responsibilityDescription, String responsibilityDateFrom,
		String responsibilityDateTo) {
      	
      this.userName = userName;
      this.userDescription = userDescription;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;
      this.passwordExpirationCode = passwordExpirationCode;
      this.passwordExpirationDays = passwordExpirationDays;
      this.passwordExpirationAccess = passwordExpirationAccess;
      this.responsibilityName = responsibilityName;
      this.responsibilityDescription = responsibilityDescription;
      this.responsibilityDateFrom = responsibilityDateFrom;
      this.responsibilityDateTo = responsibilityDateTo;
      
   }

   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public String getPasswordExpirationAccess() {
   	
   	  return passwordExpirationAccess;
   	  
   }
   
   public String getPasswordExpirationCode() {
   	
   	  return passwordExpirationCode;
   	  
   }
   
   public String getPasswordExpirationDays() {
   	 
   	  return passwordExpirationDays;
   	  
   }
   
   public String getResponsibilityDateFrom() {
   
   	  return responsibilityDateFrom;
   	  
   }
   
   public String getResponsibilityDateTo() {
   	
   	  return responsibilityDateTo;
   	  
   }
   
   public String getResponsibilityName() {
   	
   	  return responsibilityName;
   	  
   }
   
   public String getResponsibilityDescription() {
   
   	  return responsibilityDescription;
   	  
   }
   
   public String getUserDescription() {
   
   	  return userDescription;
   	  
   }
   
   public String getUserName() {
   
   	  return userName;
   	  
   }
   
}
