package com.struts.jreports.ad.userlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class AdRepUserListForm extends ActionForm implements Serializable{

   private String userName = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public void setGoButton(String goButton){
   	
       this.goButton = goButton;
       
   }

   public void setCloseButton(String closeButton){
       
   	   this.closeButton = closeButton;
   	   
   }
   
   public String getUserName() {
   	
   	  return userName;
   	  
   }
   
   public void setUserName(String userName) {
   	
   	  this.userName = userName;
   	  
   }
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
         
   public String getViewType(){
   	  
   	  return viewType;
   	  
   }
   
   public void setViewType(String viewType){
   	
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList(){
   	
   	  return viewTypeList;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){      
      goButton = null;
      closeButton = null;
      orderByList.clear();
      orderByList.add(Constants.GLOBAL_BLANK);
      orderByList.add("USER NAME");
      orderByList.add("USER DESC");
      orderBy = Constants.GLOBAL_BLANK;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      userName = null;
      dateFrom = null;
      dateTo = null;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();

      return(errors);
   }
}
