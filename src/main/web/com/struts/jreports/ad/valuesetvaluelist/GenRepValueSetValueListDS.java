package com.struts.jreports.ad.valuesetvaluelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GenRepValueSetValueListDetails;

public class GenRepValueSetValueListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GenRepValueSetValueListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 GenRepValueSetValueListDetails details = (GenRepValueSetValueListDetails)i.next();
                  
      	 GenRepValueSetValueListData argData = new GenRepValueSetValueListData(details.getVslValueSetName(),
      	 		details.getVslValue(), details.getVslDescription(), details.getVslAccountType(), 
				details.getVslEnable() == 1 ? "YES" : "FALSE");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("valueSetName".equals(fieldName)){
         value = ((GenRepValueSetValueListData)data.get(index)).getValueSetName();
      }else if("value".equals(fieldName)){
         value = ((GenRepValueSetValueListData)data.get(index)).getValue();
      }else if("description".equals(fieldName)){
         value = ((GenRepValueSetValueListData)data.get(index)).getDescription();      
      }else if("accountType".equals(fieldName)){
        value = ((GenRepValueSetValueListData)data.get(index)).getAccountType();
      }else if("enable".equals(fieldName)){
        value = ((GenRepValueSetValueListData)data.get(index)).getEnable();  
      }

      return(value);
   }
}
