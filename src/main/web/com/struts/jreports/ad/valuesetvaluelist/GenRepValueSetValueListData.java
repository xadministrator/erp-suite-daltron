package com.struts.jreports.ad.valuesetvaluelist;

public class GenRepValueSetValueListData implements java.io.Serializable {
	
   private String valueSetName = null;
   private String value = null;
   private String description = null;
   private String accountType = null;
   private String enable = null;

   public GenRepValueSetValueListData(String valueSetName, String value, String description, 
   		String accountType, String enable){
      	
      this.valueSetName = valueSetName;
      this.value = value;
      this.description = description;
      this.accountType = accountType;
      this.enable = enable;

   }

   public String getValue() {
   	return value;
   }
   public String getDescription() {
   	return description;
   }
   public String getAccountType() {
   	return accountType;
   }
   public String getValueSetName() {
   	return valueSetName;
   }
   public String getEnable() {
   	return enable;
   }
}
