package com.struts.jreports.ad.valuesetvaluelist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class GenRepValueSetValueListForm extends ActionForm implements Serializable{

   private String valueSetName = null;
   private ArrayList valueSetNameList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private boolean enable = false;
   private boolean disable = false;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getValueSetName() {
   	
   	return valueSetName;
   	
   }
   
   public void setValueSetName(String valueSetName) {
   	
   	this.valueSetName = valueSetName;
   	
   }
   
   public ArrayList getValueSetNameList() {
   	
   	return valueSetNameList;
   	
   }
   
   public void setValueSetNameList(String valueSetName) {
   	
   	valueSetNameList.add(valueSetName);
   	
   }
   
   public void clearValueSetNameList() {
   	
   	valueSetNameList.clear();
   	valueSetNameList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void setGoButton(String goButton) {
   	
      this.goButton = goButton;
   
   }

   public void setCloseButton(String closeButton) {
   	
      this.closeButton = closeButton;
   
   }

   public String getOrderBy() {
   	
   	  return orderBy;   	
   
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   
   }
   
   public String getViewType() {
   	
   	  return viewType;   	
   
   }
   
   public void setViewType(String viewType) {
   	
   	  this.viewType = viewType;
   
   }
   
   public ArrayList getViewTypeList() {
   	
   	  return viewTypeList;
   
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission() {
   	
      return userPermission;
   
   }

   public void setUserPermission(String userPermission) {
   	
      this.userPermission = userPermission;
   
   }

   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
   
   public void setEnable(boolean enable) {
   	
   	  this.enable = enable;
   	  
   }
   
   public boolean getDisable() {
   	
   	  return disable;
   	  
   }
   
   public void setDisable(boolean disable) {
   	
   	  this.disable = disable;
   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {   
   	
      goButton = null;
      closeButton = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
   	  enable = false;
      disable = false;
      
      if (orderByList.isEmpty()) {
      	
		  orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("VALUE SET NAME");
	      orderBy = "VALUE SET NAME";   
	  
	  }    
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
      }
      return(errors);
   }
}
