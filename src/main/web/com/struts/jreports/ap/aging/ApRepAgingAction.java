package com.struts.jreports.ap.aging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepAgingController;
import com.ejb.txn.ApRepAgingControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ApRepAgingDetails;

public final class ApRepAgingAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepAgingAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepAgingForm actionForm = (ApRepAgingForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
  
         String frParam = Common.getUserPermission(user, Constants.AP_REP_AGING_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRepAging");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApRepAgingController EJB
*******************************************************/

         ApRepAgingControllerHome homeAG = null;
         ApRepAgingController ejbAG = null;       

         try {
         	
            homeAG = (ApRepAgingControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepAgingControllerEJB", ApRepAgingControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepAgingAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbAG = homeAG.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepAgingAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP AG Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            ApRepAgingDetails details = null;
            
            String company = null;
            int agingBucket = 0;
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbAG.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // get preference
		       AdPreferenceDetails adPreferenceDetails = ejbAG.getAdPreference(user.getCmpCode());
		       agingBucket = adPreferenceDetails.getPrfApAgingBucket();
		       
		       // execute report
		       
		       HashMap criteria = new HashMap();
		       
		       if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getSupplierType())) {
	        		
	        		criteria.put("supplierType", actionForm.getSupplierType());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getSupplierClass())) {
	        		
	        		criteria.put("supplierClass", actionForm.getSupplierClass());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        		
	           }
	           	           	           	           
	           if (actionForm.getIncludeUnpostedTransaction()) {
		    	
			    	criteria.put("includeUnpostedTransaction", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includeUnpostedTransaction", "NO");
			    	
			   }
			   
			   if (actionForm.getIncludePaid()) {
		    	
			    	criteria.put("includePaid", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includePaid", "NO");
			    	
			   }
			   
			   // branch map
			   
			   ArrayList adBrnchList = new ArrayList();
			   
			   for(int j=0; j < actionForm.getApRepBrAgingListSize(); j++) {
	                
	                ApRepBranchAgingList apRepBrAgingList = (ApRepBranchAgingList)actionForm.getApRepBrAgingByIndex(j);
	                
	                if(apRepBrAgingList.getBranchCheckbox() == true) {
	                    
	                    AdBranchDetails mdetails = new AdBranchDetails();	                    
	                    mdetails.setBrCode(apRepBrAgingList.getBrCode());
	                    adBrnchList.add(mdetails);
	                    
	                }
	                
	            }
			   
			   	    
		       list = ejbAG.executeApRepAging(criteria, actionForm.getAgingBy(), actionForm.getOrderBy(), 
		       								  actionForm.getGroupBy(), actionForm.getCurrency(), adBrnchList, user.getCmpCode());		       
		       		       		    		    		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("apAging.error.noRecordFound"));
                     		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ApRepAgingAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("apRepAging"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
		    parameters.put("date2", Common.convertStringToSQLDate(actionForm.getDate()));
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("groupBy", actionForm.getGroupBy());
		    
		    if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	    		parameters.put("supplierCode", actionForm.getSupplierCode());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getSupplierType())) {
	    		
	    		parameters.put("type", actionForm.getSupplierType());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getSupplierClass())) {
	    		
	    		parameters.put("class", actionForm.getSupplierClass());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDate())) {
	    		
	    		parameters.put("date", actionForm.getDate());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getAgingBy())) {
	    		
	    		parameters.put("agingBy", actionForm.getAgingBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getOrderBy())) {
	       	
	       		parameters.put("orderBy", actionForm.getOrderBy());
	       		
	       }
	       
	       if (actionForm.getIncludeUnpostedTransaction()) {
		    	
		    	parameters.put("includeUnposted", "YES");
		    	
		   } else {
		    	
		    	parameters.put("includeUnposted", "NO");
		    	
		   }
		   
		   if (actionForm.getIncludePaid()) {
		    	
		    	parameters.put("includePaid", "YES");
		    	
		   } else {
		    	
		    	parameters.put("includePaid", "NO");
		    	
		   }
		   
		   if(agingBucket > 1) {
			   
			   parameters.put("agingBucket1", "1-" + agingBucket + " days");
			   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
			   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
			   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
			   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");
			      
		   } else {
			   
			   parameters.put("agingBucket1", "1 day");
			   parameters.put("agingBucket2", "2 days");
			   parameters.put("agingBucket3", "3 days");
			   parameters.put("agingBucket4", "4 days");
			   parameters.put("agingBucket5", "Over 4 days");
			   
		   }
		   
		   String branchMap = null;
		   boolean first = true;
		   for(int j=0; j < actionForm.getApRepBrAgingListSize(); j++) {

			   ApRepBranchAgingList brList = (ApRepBranchAgingList)actionForm.getApRepBrAgingByIndex(j);

			   if(brList.getBranchCheckbox() == true) {
				   if(first) {
					   branchMap = brList.getBrName();
					   first = false;
				   } else {
					   branchMap = branchMap + ", " + brList.getBrName();
				   }
			   }

		   }
		   parameters.put("branchMap", branchMap);
		   
		   String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepAging.jasper";
	       
	       if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/ApRepAging.jasper");
		    
	       }		    		    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepAgingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ApRepAgingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ApRepAgingDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ApRepAgingAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP AG Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));		          		         		         
		          
/*******************************************************
   -- AP AG Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	             
	             try {
	             	
	             	actionForm.reset(mapping, request);
	                 
	                 ArrayList list = null;
	                 Iterator i = null;
	                 
	                 actionForm.clearApRepBrAgingList();
	                 
	                 list = ejbAG.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	                 
	                 i = list.iterator();
	                 
	                 while(i.hasNext()) {
	                     
	                     AdBranchDetails details = (AdBranchDetails)i.next();
	                     
	                     ApRepBranchAgingList apRepBrAgingList = new ApRepBranchAgingList(actionForm,
	                             details.getBrCode(),
	                             details.getBrBranchCode(), details.getBrName());
	                     apRepBrAgingList.setBranchCheckbox(true);
	                     	                     	                                       
	                     actionForm.saveApRepBrAgingList(apRepBrAgingList);
	                     
	                 }
	                 
	             } catch (GlobalNoRecordFoundException ex) {
	                 
	             } catch (EJBException ex) {
	                 
	                 if (log.isInfoEnabled()) {
	                     
	                     log.info("EJBException caught in ApRepAgingAction.execute(): " + ex.getMessage() +
	                             " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                 }
	                 
	             }  
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearSupplierTypeList();           	
            	
	            	list = ejbAG.getApStAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierTypeList((String)i.next());
	            			
	            		}
	            			            		
	            	}  
	            	
	            	 actionForm.clearSupplierClassList();           	
            	
	            	list = ejbAG.getApScAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierClassList((String)i.next());
	            			
	            		}
	            			            		
	            	}   	            		            	
	            				       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ApRepAgingAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("apRepAging"));		          
			            
			 } else {
			 	
			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));
			
			    return(mapping.findForward("cmnMain"));
			
			 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepAgingAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
