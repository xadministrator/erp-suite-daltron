package com.struts.jreports.ap.aging;

public class ApRepAgingData implements java.io.Serializable{
   
   private String groupBy = null;
   private String supplierCode = null;
   private String voucherNumber = null;
   private String referenceNumber = null;
   private String installmentNumber = null;
   private Double amount = null;
   private Double bucket0 = null;
   private Double bucket1 = null;
   private Double bucket2 = null;
   private Double bucket3 = null;
   private Double bucket4 = null;
   private Double bucket5 = null;
   private String transactionDate = null;
   private Double voucherAge = null;
   private String currencySymbol = null;
   private String description = null;
   
   public ApRepAgingData(String groupBy, String supplierCode, String voucherNumber, String referenceNumber,
       String installmentNumber, Double amount, Double bucket0, Double bucket1, Double bucket2,
       Double bucket3, Double bucket4, Double bucket5, String transactionDate, Double voucherAge, String currencySymbol,
       String description){
      
       this.groupBy = groupBy;
       this.supplierCode = supplierCode;
       this.voucherNumber = voucherNumber;
       this.referenceNumber = referenceNumber;
       this.installmentNumber = installmentNumber;
       this.amount = amount;
       this.bucket0 = bucket0;
       this.bucket1 = bucket1;
       this.bucket2 = bucket2;
       this.bucket3 = bucket3;
       this.bucket4 = bucket4;
       this.bucket5 = bucket5;
       this.transactionDate = transactionDate;       
       this.voucherAge = voucherAge;
       this.currencySymbol = currencySymbol;
       this.description =description;
       
   }

   public String getGroupBy() {
   	
   	   return groupBy;
   	
   }
   
   public String getSupplierCode() {
   	
   	   return supplierCode;
   	   
   }
   
   public String getVoucherNumber() {
   	
   	  return voucherNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public String getInstallmentNumber() {
   	
   	  return installmentNumber;
   	
   }
   
   public Double getAmount() {
   	
   	  return amount;
   	
   }
   
   public Double getBucket0() {
   	
   	  return bucket0;
   	  
   }
   
   public Double getBucket1() {
   	
   	  return bucket1;
   	
   }
   
   public Double getBucket2() {
   	
   	  return bucket2;
   	
   }
   
   public Double getBucket3() {
   	
   	  return bucket3;
   	
   }
   
   public Double getBucket4() {
   	
   	  return bucket4;
   	
   }
   
   public Double getBucket5() {
   	
   	  return bucket5;
   	
   }
   
   public String getTransactionDate() {
   	
   	  return transactionDate;
   	
   }
   
   public Double getVoucherAge() {
	   
	   return voucherAge;

   }

   public String getCurrencySymbol() {

	   return currencySymbol;

   }
   
   public String getDescription() {

	   return description;

   }
      
}
