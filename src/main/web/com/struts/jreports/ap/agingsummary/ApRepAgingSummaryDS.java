package com.struts.jreports.ap.agingsummary;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.aging.ApRepAgingData;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.ApRepAgingSummaryDetails;

public class ApRepAgingSummaryDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepAgingSummaryDS(ArrayList list){
   	
      Iterator i = list.iterator();
            
      while (i.hasNext()) {
      	
      	 
         ApRepAgingSummaryDetails details = (ApRepAgingSummaryDetails)i.next();
        
         String groupBy = null;
         
         if(details.getGroupBy().equals(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE)) {
         	
         	groupBy = details.getAgSupplierCode();
         	 
         } else if(details.getGroupBy().equals(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE)) {
         	
         	groupBy = details.getAgSupplierType();
         	
         } else if(details.getGroupBy().equals(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS)) {
         	
         	groupBy = details.getAgSupplierClass();
         	
         } else {
         	
         	groupBy = "";
         	
         }
         
         ApRepAgingSummaryData agData = new ApRepAgingSummaryData(groupBy,	         	          
	         new Double(details.getAgAmount()),
			 details.getAgBucket0() != 0d ? new Double(details.getAgBucket0()) : null,
	         details.getAgBucket1() != 0d ? new Double(details.getAgBucket1()) : null, 
	         details.getAgBucket2() != 0d ? new Double(details.getAgBucket2()) : null,
	         details.getAgBucket3() != 0d ? new Double(details.getAgBucket3()) : null, 	         
	         details.getAgBucket4() != 0d ? new Double(details.getAgBucket4()) : null,
	         details.getAgBucket5() != 0d ? new Double(details.getAgBucket5()) : null,
	        		 Common.convertCharToString(details.getAgVouFcSymbol()), details.getAgDescription());
		    
         data.add(agData);
      }     
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	  
      Object value = null;

      String fieldName = field.getName();
            
      if("groupBy".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getGroupBy();      
      }else if("amount".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getAmount();
      }else if("bucket0".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket0();
      }else if("bucket1".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket1();
      }else if("bucket2".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket2();
      }else if("bucket3".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket3();
      }else if("bucket4".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket4();
      }else if("bucket5".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getBucket5();   
      }else if("currencySymbol".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getCurrencySymbol();
      }else if("description".equals(fieldName)){
         value = ((ApRepAgingSummaryData)data.get(index)).getDescription();
      }
   	  
      return(value);
   }
}

