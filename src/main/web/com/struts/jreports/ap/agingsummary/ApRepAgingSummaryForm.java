package com.struts.jreports.ap.agingsummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepAgingSummaryForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String date = null;
   private String supplierType = null;
   private ArrayList supplierTypeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private String agingBy = null;
   private ArrayList agingByList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private boolean includePaid = false;
   private boolean includeUnpostedTransaction = false;
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String currency = null;
	private ArrayList currencyList = new ArrayList();
   private String report = null;

   private String userPermission = new String();
   
   private ArrayList apRepBrAgingSmmryList = new ArrayList();

   public String getSupplierCode() {
   	
   	  return supplierCode;
   	
   }
   
   public void setSupplierCode(String supplierCode) {
   	
   	  this.supplierCode = supplierCode;
   	
   }
   
   public String getDate() {
   	
   	  return date;
   	
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	
   }   
      
   public String getSupplierType() {
   	
      return supplierType;
      
   }

   public void setSupplierType(String supplierType) {
   	
      this.supplierType = supplierType;
      
   }

   public ArrayList getSupplierTypeList() {
   	
      return supplierTypeList;
      
   }

   public void setSupplierTypeList(String supplierType) {
   	
      supplierTypeList.add(supplierType);
      
   }

   public void clearSupplierTypeList() {
   	
      supplierTypeList.clear();
      supplierTypeList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getSupplierClass() {
   	
      return supplierClass;
      
   }

   public void setSupplierClass(String supplierClass) {
   	
      this.supplierClass = supplierClass;
      
   }

   public ArrayList getSupplierClassList() {
   	
      return supplierClassList;
      
   }

   public void setSupplierClassList(String supplierClass) {
   	
      supplierClassList.add(supplierClass);
      
   }

   public void clearSupplierClassList() {
   	
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getAgingBy() {
   	
   	  return agingBy ;   	
   	  
   }
   
   public void setAgingBy(String agingBy) {
   	
   	  this.agingBy = agingBy;
   	  
   }
   
   public ArrayList getAgingByList() {
   	
   	  return agingByList;
   	  
   }   
   
   public String getViewType() {
   	
   	  return viewType ;   	
   	  
   }
   
   public void setViewType(String viewType) {
   	
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList() {
   	
   	  return viewTypeList;
   	  
   }
   
   public boolean getIncludePaid() {
   	
   	  return includePaid;
   	
   }
   
   public void setIncludePaid(boolean includePaid) {
   	
   	  this.includePaid = includePaid;
   	
   }
   
   public boolean getIncludeUnpostedTransaction() {
   	
   	  return includeUnpostedTransaction;
   	
   }
   
   public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
   	
   	  this.includeUnpostedTransaction = includeUnpostedTransaction;
   	
   }
               
   public String getReport() {
   	
   	  return report;
   	  
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getGroupBy() {
	
	  return groupBy;
		
   }
	
   public void setGroupBy(String groupBy) {
		
   	  this.groupBy = groupBy;
			
   }
	
   public ArrayList getGroupByList() {
		
	  return groupByList;
			
   }
	
   public void setGroupByList(ArrayList groupByList) {
		
	  this.groupByList = groupByList;
			
   }
   
   public Object[] getApRepBrAgingSmmryList(){
       
       return apRepBrAgingSmmryList.toArray();
       
   }
   
   public ApRepBranchAgingSummaryList getApRepBrAgingSmmryByIndex(int index){
       
       return ((ApRepBranchAgingSummaryList)apRepBrAgingSmmryList.get(index));
       
   }
   
   public int getApRepBrAgingSmmryListSize(){
       
       return(apRepBrAgingSmmryList.size());
       
   }
   
   public void saveApRepBrAgingSmmryList(Object newApRepBrAgingSmmryList){
       
       apRepBrAgingSmmryList.add(newApRepBrAgingSmmryList);   	  
       
   }
   
   public void clearApRepBrAgingSmmryList(){
       
       apRepBrAgingSmmryList.clear();
       
   }
   
   public void setApRepBrAgingSmmryList(ArrayList apRepBrAgingSmmryList) {
       
       this.apRepBrAgingSmmryList = apRepBrAgingSmmryList;
       
   }
   
   public String getCurrency() {

		return currency;   	

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}
   
   public void reset(ActionMapping mapping, HttpServletRequest request) { 
       
       for (int i=0; i<apRepBrAgingSmmryList.size(); i++) {
           
           ApRepBranchAgingSummaryList  list = (ApRepBranchAgingSummaryList)apRepBrAgingSmmryList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
       supplierCode = null;
       supplierType = Constants.GLOBAL_BLANK;
       supplierClass = Constants.GLOBAL_BLANK;      
       date = Common.convertSQLDateToString(new Date());
       viewTypeList.clear();
       viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
       viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
       viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
       viewType = Constants.REPORT_VIEW_TYPE_PDF;
       agingByList.clear();
       agingByList.add(Constants.AP_AGING_BY_DUE_DATE);
       agingByList.add(Constants.AP_AGING_BY_VOUCHER_DATE);
       agingBy = Constants.AP_AGING_BY_VOUCHER_DATE;
       groupByList.clear();   
       groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE);
       groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE);
       groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS);      
       groupBy = Constants.AP_SL_ORDER_BY_SUPPLIER_CODE;
       currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		currencyList.add("PHP");
		currencyList.add("USD");
		currency = Constants.GLOBAL_BLANK;
       includePaid = false;
       includeUnpostedTransaction = false;      
       
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(date)) {
         	
		    errors.add("date", new ActionMessage("apAgingSummary.error.dateInvalid"));
		    
		 }
	 
      }
      return errors;
   }
}

