package com.struts.jreports.ap.annualprocurement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepAnnualProcurementController;
import com.ejb.txn.ApRepAnnualProcurementControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepAnnualProcurementAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepAnnualProcurementAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepAnnualProcurementForm actionForm = (ApRepAnnualProcurementForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AP_REP_INPUT_TAX_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRepAnnualProcurement");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApRepAnnualProcurementController EJB
*******************************************************/

         ApRepAnnualProcurementControllerHome homeAP = null;
         ApRepAnnualProcurementController ejbAP = null;       

         try {
         	
        	 homeAP = (ApRepAnnualProcurementControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepAnnualProcurementControllerEJB", ApRepAnnualProcurementControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepAnnualProcurementAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
        	 ejbAP = homeAP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepAnnualProcurementAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP IT Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getYear())) {
	        		
	        		criteria.put("year", actionForm.getYear());
	        		
	        	}
	        	if (!Common.validateRequired(actionForm.getDepartment())) {
	        		
	        		criteria.put("department", actionForm.getDepartment());
	        		
	        	}       		        
	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            // branch map
            
            ArrayList adBrnchList = new ArrayList();
            // ArrayList adBrnchList2 = new ArrayList();
            
            for(int j=0; j < actionForm.getApRepBrAnnlPrcrmntListSize(); j++) {
                
                ApRepBranchAnnualProcurementList apRepBrInptTaxList = (ApRepBranchAnnualProcurementList)actionForm.getApRepBrAnnlPrcrmntByIndex(j);
                
                if(apRepBrInptTaxList.getBranchCheckbox() == true) {
                    
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(apRepBrInptTaxList.getBrCode());
                    adBrnchList.add(mdetails);
                    
                }
                
            }
            

		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbAP.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		       System.out.println(actionForm.getViewMonth() + "<== getviewmonth");
		       System.out.println(actionForm.getYear() + "<== year");	
		       list = ejbAP.executeApRepAnnualProcurement(actionForm.getCriteria(), actionForm.getViewMonth(), adBrnchList, user.getCmpCode());
		       System.out.println(list + "<== list after ejbAP.execute");	   
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("annualProcurement.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ApRepAnnualProcurementAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRepAnnualProcurement");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("name", actionForm.getItemName());
		    parameters.put("year", actionForm.getYear());
			//parameters.put("printedBy", user.getUserName());
			//parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			parameters.put("department", actionForm.getDepartment());
			
			if (actionForm.getItemName() != null) {
			
				parameters.put("name", actionForm.getItemName());
				
			}
			
			if (actionForm.getYear() != null) {
				
				parameters.put("year", actionForm.getYear());
				
			}
			
			if (actionForm.getDepartment() != null) {
				
				parameters.put("department", actionForm.getDepartment());
				
			}
			System.out.println(actionForm.getDepartment() + "<== department");
	    	
	    	String branchMap = null;
	    	boolean first = true;
	    	for(int j=0; j < actionForm.getApRepBrAnnlPrcrmntListSize(); j++) {

	    		ApRepBranchAnnualProcurementList brList = (ApRepBranchAnnualProcurementList)actionForm.getApRepBrAnnlPrcrmntByIndex(j);

	    		if(brList.getBranchCheckbox() == true) {
	    			if(first) {
	    				branchMap = brList.getBrName();
	    				first = false;
	    			} else {
	    				branchMap = branchMap + ", " + brList.getBrName();
	    			}
	    		}

	    	}
	    	parameters.put("branchMap", branchMap);
	    		        
	    	String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepAnnualProcurement.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/ApRepAnnualProcurement.jasper");
		    
	        }	    	
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepAnnualProcurementDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ApRepAnnualProcurementDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ApRepAnnualProcurementDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ApRepAnnualProcurementAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP IT Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP IT Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	             
	             try {
	                
	             	 actionForm.reset(mapping, request);	             	
	             	
	                 ArrayList list = null;
	                 Iterator i = null;
	                 
	                 actionForm.clearApRepBrAnnlPrcrmntList();
	                 
	                 list = ejbAP.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	                 
	                 i = list.iterator();
	                 
	                 while(i.hasNext()) {
	                     
	                     AdBranchDetails details = (AdBranchDetails)i.next();
	                     
	                     ApRepBranchAnnualProcurementList apRepBrAnnlPrcrmntList = new ApRepBranchAnnualProcurementList(actionForm,
	                             details.getBrCode(),
	                             details.getBrBranchCode(),details.getBrName());
	                     apRepBrAnnlPrcrmntList.setBranchCheckbox(true);
	                     
	                     actionForm.saveApRepBrAnnlPrcrmntList(apRepBrAnnlPrcrmntList);
	                     
	                 }
	                 
	                 String userDepartment = ejbAP.getAdUsrDeptartment(user.getUserName(), user.getCmpCode());
	                 System.out.println(userDepartment);
	                 
	                 /*
	                 list = ejbAP.getAdLvDEPARTMENT(user.getCmpCode());
	                 i = list.iterator();
				       
	                 if (list == null || list.size() == 0) {  		
	                	 actionForm.setDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
	                 } else {     		            		
	                	 i = list.iterator();
	                	 while (i.hasNext()) {
							    actionForm.setDepartmentList((String)i.next());
	                	 }
	                 }*/
	                 actionForm.setDepartmentList(userDepartment);
	             } catch (GlobalNoRecordFoundException ex) {
	                 
	             } catch (EJBException ex) {
	                 
	                 if (log.isInfoEnabled()) {
	                     
	                     log.info("EJBException caught in ApRepAnnualProcurementAction.execute(): " + ex.getMessage() +
	                             " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                 }
	                 
	             } 
	         		         	
	            
	            return(mapping.findForward("apRepAnnualProcurement"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepInputTaxAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
