package com.struts.jreports.ap.annualprocurement;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepAnnualProcurementDetails;;

public class ApRepAnnualProcurementDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepAnnualProcurementDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         ApRepAnnualProcurementDetails details = (ApRepAnnualProcurementDetails)i.next();
	         
	         ApRepAnnualProcurementData argData = new ApRepAnnualProcurementData(
	         			details.getApItemName(),details.getApItemDesc(), details.getApInvItemCategory(), details.getApDepartment(),
	         			details.getApUnit(), details.getApMonth(),
						details.getApYear(), new Double(details.getApUnitCost()), new Double (details.getApTotalCost()),
						details.getApQtyMonth(), details.getApQtyJan(),details.getApQtyFeb(),details.getApQtyMrch(),
						details.getApQtyAprl(),details.getApQtyMay(),details.getApQtyJun(),details.getApQtyJul(),
						details.getApQtyAug(),details.getApQtySep(),details.getApQtyOct(),details.getApQtyNov(),
						details.getApQtyDec());
	         
	         data.add(argData);
	         System.out.println(details.getApDepartment() + "<== department under ApRepAnnualProcurementDS");
      }
   }
   
   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }
   
   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("itemName".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getItemName();
      }else if("itemDesc".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getItemDesc();
      }else if("itemCategory".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getItemCategory();
      }else if("department".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getDepartment();
    	  System.out.println(value + "<== department under getFieldValue");
      }else if("year".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getYear();
      }else if("unit".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getUnit();
      }else if("month".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getMonth();
      }else if("unitCost".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getUnitCost();
      }else if("totalCost".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getTotalCost();
    	  
      }else if("qtyJan".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyJan();
      }else if("qtyFeb".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyFeb();
      }else if("qtyMrch".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyMrch();
      }else if("qtyAprl".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyAprl();
      }else if("qtyMay".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyMay();
      }else if("qtyJun".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyJun();
      }else if("qtyJul".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyJul();
      }else if("qtyAug".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyAug();
      }else if("qtySep".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtySep();
      }else if("qtyOct".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyOct();
      }else if("qtyNov".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyNov();
      }else if("qtyDec".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyDec();
    	  
      }else if("qtyMonth".equals(fieldName)){
    	  value = ((ApRepAnnualProcurementData)data.get(index)).getQtyMonth();
      }  
      return(value);
      
   }
}
