package com.struts.jreports.ap.annualprocurement;

import java.io.Serializable;

public class ApRepAnnualProcurementData implements Serializable {
	
	private String itemName;
	private String unit;
	private String year;
	private String itemDesc;
	private String itemCategory;
	private String department;
	private Double unitCost;
	private Double totalCost;
	private Double qtyMonth;
	private Double qtyJan;
	private Double qtyFeb;
	private Double qtyMrch;
	private Double qtyAprl;
	private Double qtyMay;
	private Double qtyJun;
	private Double qtyJul;
	private Double qtySep;
	private Double qtyAug;
	private Double qtyOct;
	private Double qtyNov;
	private Double qtyDec;
	private String month;
	
	
	public ApRepAnnualProcurementData(String itemName, String itemDesc, String itemCategory, String department, String unit, String month,
					String year, Double unitCost, Double totalCost, Double qtyMonth, Double qtyJan,
					Double qtyFeb, Double qtyMrch, Double qtyAprl, Double qtyMay, Double qtyJun,
					Double qtyJul, Double qtyAug, Double qtySep, Double qtyOct, Double qtyNov,
					Double qtyDec) {
		
		this.itemName = itemName;
		this.itemDesc = itemDesc;
		this.itemCategory = itemCategory;
		this.department = department;
		this.unit = unit;
		this.year = year;
		this.unitCost = unitCost;
		this.totalCost = totalCost;
		this.qtyMonth = qtyMonth;
		this.qtyJan = qtyJan;
		this.qtyFeb = qtyFeb;
		this.qtyMrch = qtyMrch;
		this.qtyAprl = qtyAprl;
		this.qtyMay = qtyMay;
		this.qtyOct = qtyOct;
		this.qtyJun = qtyJun;
		this.qtyJul = qtyJul;
		this.qtyAug = qtyAug;
		this.qtySep = qtySep;
		this.qtyNov = qtyNov;
		this.qtyDec = qtyDec;
		
		this.month = month;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getItemDesc() {
		
		return itemDesc;
		
	}
	
	public String getItemCategory() {
		return itemCategory;
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	public String getMonth() {
		
		return month;
		
	}
	public String getYear() {
		
		return year;
		
	}
	public String getDepartment() {
		
		return department;
		
	}
	public Double getUnitCost() {
		
		return unitCost;
		
	}
	
	public Double getTotalCost() {
		
		return totalCost;
		
	}
	public Double getQtyMonth() {
		
		return qtyMonth;
		
	}

	public Double getQtyJan() {
		return qtyJan;
	}

	public Double getQtyFeb() {
		return qtyFeb;
	}

	public Double getQtyMrch() {
		return qtyMrch;
	}

	public Double getQtyAprl() {
		return qtyAprl;
	}

	public Double getQtyMay() {
		return qtyMay;
	}

	public Double getQtyJun() {
		return qtyJun;
	}

	public Double getQtyJul() {
		return qtyJul;
	}

	public Double getQtySep() {
		return qtySep;
	}

	public Double getQtyAug() {
		return qtyAug;
	}

	public Double getQtyOct() {
		return qtyOct;
	}

	public Double getQtyNov() {
		return qtyNov;
	}

	public Double getQtyDec() {
		return qtyDec;
	}

	

}
