package com.struts.jreports.ap.annualprocurement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepAnnualProcurementForm extends ActionForm implements Serializable{

   private String itemName = null;
   private String department = null;
   private ArrayList departmentList = new ArrayList();
   private String year = null;
   private String viewMonth = null;
   private ArrayList viewMonthList = new ArrayList();
   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();
   
   private ArrayList apRepBrAnnlPrcrmntList = new ArrayList();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getItemName(){
      return(itemName);
   }

   public void setItemName(String itemName){
      this.itemName = itemName;
   }
   
   public String getDepartment() {
	   return department;
   }
	
   public void setDepartment(String department) {
	   this.department = department;
   }
   
   public ArrayList getDepartmentList() {
	   return departmentList;
   }
   
   public void setDepartmentList(String departmentList) {
		
		  this.departmentList.add(departmentList);
		
	}
	public String getYear(){
	   return(year);
   }

   public void setYear(String year){
	   this.year = year;
   }
   
   
   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
  
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getViewMonth(){
	   return(viewMonth);   	
   }
	   
   public void setViewMonth(String viewMonth){
	   this.viewMonth = viewMonth;
   }
	   
   public ArrayList getViewMonthList(){
	   return viewMonthList;
   }

  


	public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Object[] getApRepBrAnnlPrcrmntList(){
       
       return apRepBrAnnlPrcrmntList.toArray();
       
   }
   
   public ApRepBranchAnnualProcurementList getApRepBrAnnlPrcrmntByIndex(int index){
       
       return ((ApRepBranchAnnualProcurementList)apRepBrAnnlPrcrmntList.get(index));
       
   }
   
   public int getApRepBrAnnlPrcrmntListSize(){
       
       return(apRepBrAnnlPrcrmntList.size());
       
   }
   
   public void saveApRepBrAnnlPrcrmntList(Object newApRepBrAnnlPrcrmntList){
       
	   apRepBrAnnlPrcrmntList.add(newApRepBrAnnlPrcrmntList);   	  
       
   }
   
   public void clearApRepBrAnnlPrcrmntList(){
       
	   apRepBrAnnlPrcrmntList.clear();
       
   }
   
   public void setApRepBrAnnlPrcrmntList(ArrayList apRepBrAnnlPrcrmntList) {
       
       this.apRepBrAnnlPrcrmntList = apRepBrAnnlPrcrmntList;
       
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){   
       
       for (int i=0; i<apRepBrAnnlPrcrmntList.size(); i++) {
           
           ApRepBranchAnnualProcurementList  list = (ApRepBranchAnnualProcurementList)apRepBrAnnlPrcrmntList.get(i);
           list.setBranchCheckbox(false);	       
           
       } 
       
      goButton = null;
      closeButton = null;
      itemName = null;  
      
      java.util.Date date = new java.util.Date();
      java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("yyyy");
      year = sdf.format(date);
      viewMonthList.clear();
      viewMonthList.add(Constants.GLOBAL_BLANK);
      viewMonthList.add("January");
      viewMonthList.add("February");
      viewMonthList.add("March");
      viewMonthList.add("April");
      viewMonthList.add("May");
      viewMonthList.add("June");
      viewMonthList.add("July");
      viewMonthList.add("August");
      viewMonthList.add("September");
      viewMonthList.add("October");
      viewMonthList.add("November");
      viewMonthList.add("December");
      viewMonth = Constants.GLOBAL_BLANK;
      departmentList.clear();
      //departmentList.add(Constants.GLOBAL_BLANK);
      
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;   
      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
        
		 
      }
      return(errors);
   }
}
