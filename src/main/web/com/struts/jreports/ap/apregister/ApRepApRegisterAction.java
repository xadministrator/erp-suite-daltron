package com.struts.jreports.ap.apregister;

import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApPaymentEntryController;
import com.ejb.txn.ApPaymentEntryControllerHome;
import com.ejb.txn.ApRepApRegisterController;
import com.ejb.txn.ApRepApRegisterControllerHome;
import com.struts.ap.paymententry.ApPaymentEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApCheckDetails;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApRepApRegisterDetails;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public final class ApRepApRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepApRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepApRegisterForm actionForm = (ApRepApRegisterForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
              actionForm.setStatusReport(null);

         String frParam = Common.getUserPermission(user, Constants.AP_REP_AP_REGISTER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRepApRegister");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApRepApRegisterController EJB
*******************************************************/

         ApRepApRegisterControllerHome homeARG = null;
         ApRepApRegisterController ejbARG = null;    
         
         ApPaymentEntryControllerHome homeCHK = null;
         ApPaymentEntryController ejbCHK = null;

         try {
         	
            homeARG = (ApRepApRegisterControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepApRegisterControllerEJB", ApRepApRegisterControllerHome.class);
            
            homeCHK = (ApPaymentEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApPaymentEntryControllerEJB", ApPaymentEntryControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepApRegisterAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbARG = homeARG.create();
            ejbCHK = homeCHK.create(); 
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepApRegisterAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP ARG Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            
         
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

                HashMap criteria = new HashMap();            	

                if (!Common.validateRequired(actionForm.getSupplierCode())) {

                        criteria.put("supplierCode", actionForm.getSupplierCode());

                }
                
                if (!Common.validateRequired(actionForm.getVoucherBatch())) {

                        criteria.put("voucherBatch", actionForm.getVoucherBatch());

                }

                if (!Common.validateRequired(actionForm.getType())) {

                        criteria.put("supplierType", actionForm.getType());
                }

                if (!Common.validateRequired(actionForm.getSupplierClass())) {

                        criteria.put("supplierClass", actionForm.getSupplierClass());
                }	        		        	

                if (!Common.validateRequired(actionForm.getDateFrom())) {

                        criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

                }

                if (!Common.validateRequired(actionForm.getDateTo())) {

                        criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

                }

                if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {

                        criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());

                }

                if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {

                        criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());

                }

                if (actionForm.getIncludedUnposted()) {	        	

                        criteria.put("includedUnposted", "YES");
                
                }
                
                if (!actionForm.getIncludedUnposted()) {
                	
                	criteria.put("includedUnposted", "NO");
                	
                }
                
                if (actionForm.getIncludedPayments()) {	        	
		        		
	        		criteria.put("includedPayments", "YES");
                
                }
                
                if (!actionForm.getIncludedPayments()) {
                	
                	criteria.put("includedPayments", "NO");
                	
                }

                if (!Common.validateRequired(actionForm.getPaymentStatus())) {

                	criteria.put("paymentStatus", actionForm.getPaymentStatus());

                }
                	 
	        	// save criteria

               
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            //branch map
            
            ArrayList adBrnchList = new ArrayList();
            // ArrayList adBrnchList2 = new ArrayList();
            
            for(int j=0; j < actionForm.getApRepBrApRgstrListSize(); j++) {
                
                ApRepBranchApRegisterList apRepBrApRgstrList = (ApRepBranchApRegisterList)actionForm.getApRepBrApRgstrByIndex(j);
                
                if(apRepBrApRgstrList.getBranchCheckbox() == true) {
                    
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(apRepBrApRgstrList.getBrCode());
                    adBrnchList.add(mdetails);
                    
                }
                
            }
            
            try {

               // get company

               AdCompanyDetails adCmpDetails = ejbARG.getAdCompany(user.getCmpCode());
               company = adCmpDetails.getCmpName();

               // execute report
               
               
             
                list = ejbARG.executeApRepApRegister(actionForm.getCriteria(),
            actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowEntries(), actionForm.getSummarize(), actionForm.getIncludeDirectCheck(),adBrnchList, user.getCmpCode());
 
               
              
            } catch (GlobalNoRecordFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
             new ActionMessage("apRegister.error.noRecordFound"));

            } catch(EJBException ex) {

                 if(log.isInfoEnabled()) {
                            log.info("EJBException caught in ApRepApRegisterAction.execute(): " + ex.getMessage() +
                            " session: " + session.getId());

                        }

                return(mapping.findForward("cmnErrorPage"));

            }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRepApRegister");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getSupplierCode() != null) {
			
				parameters.put("supplierCode", actionForm.getSupplierCode());
				
			}
			
			if (actionForm.getType() != null) {
			
				parameters.put("supplierType", actionForm.getType());
				
			}
			
			if (actionForm.getSupplierClass() != null) {
				
				parameters.put("supplierClass", actionForm.getSupplierClass());	
		    
		    }
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
			
		    }
	 
			parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
		    parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
		    
		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }
		    
		    parameters.put("paymentStatus", actionForm.getPaymentStatus());
		    parameters.put("groupBy", actionForm.getGroupBy());
		    parameters.put("showEntries", actionForm.getShowEntries() == true ? "YES" : "NO");

		    if (actionForm.getIncludedPayments()) {

		    	parameters.put("includedPayments", "YES");

		    } else {

		    	parameters.put("includedPayments", "NO");

		    }
		
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getApRepBrApRgstrListSize(); j++) {

		    	ApRepBranchApRegisterList brList = (ApRepBranchApRegisterList)actionForm.getApRepBrApRgstrByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBrName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBrName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = null;
		    
		    if(actionForm.getShowEntries()) {
		    	
		    	if (actionForm.getSummarize()) {
			    	
			    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepApRegisterSummary.jasper";
			    	
			    	if (!new java.io.File(filename).exists()) {
			    		
			    		filename = servlet.getServletContext().getRealPath("jreports/ApRepApRegisterSummary.jasper");
			    		
			    	} 
			    	
			    } else {
			    	
			    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepApRegisterEntries.jasper";
			    	
			    	if (!new java.io.File(filename).exists()) {
			    		
			    		filename = servlet.getServletContext().getRealPath("jreports/ApRepApRegisterEntries.jasper");
			    		
			    	}
			    }
		    	
		    	 
		    	
		    } else {

		    	if (actionForm.getSummarize() && !actionForm.getShowEntries()) {

		    		filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepApRegisterSummary.jasper";

		    		if (!new java.io.File(filename).exists()) {

		    			filename = servlet.getServletContext().getRealPath("jreports/ApRepApRegisterSummary.jasper");

		    		} 

		    	} else {
		    		filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepApRegister.jasper";

		    		/*
		    		 * if(actionForm.getReportType().equals("IMPORTATION")){
		    			
		    			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepApRegisterImported.jasper";

			    		if (!new java.io.File(filename).exists()) {

			    			filename = servlet.getServletContext().getRealPath("jreports/ApRepApRegisterImported.jasper");

			    		}
		    		 * 
		    		 * 
		    		 * 
		    		 * 
		    		 * 
		    		 */
		    		
		    		
		    		if (!new java.io.File(filename).exists()) {

		    			filename = servlet.getServletContext().getRealPath("jreports/ApRepApRegister.jasper");

		    		}
		    	}


		    }
		    
		    try {
		    	
                        ArrayList voucherList = null;
                        ArrayList statusList = new ArrayList();
		    	Report report = new Report();
                        String strBatchNumberHead = "";
                    
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		     	   System.out.println("filename " + filename.toString());
		       	   System.out.println("parameters " + parameters.size());
		       	   System.out.println("actio    nform " + actionForm.getGroupBy());
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
                        report.setBytes(
                           JasperRunManager.runReportToPdf(filename, parameters, 
                                 new ApRepApRegisterDS(list, actionForm.getGroupBy())));   
			       
			       
				        
                        } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ApRepApRegisterDS(list, actionForm.getGroupBy())));   
				        
                        } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_DIGIBANK)){
	               
                            
            
                            StringBuilder strB = new StringBuilder();
            
                            System.out.println("get voucher List");
                            voucherList = ejbARG.getAllVouchers(actionForm.getCriteria(),adBrnchList, user.getCmpCode());
                            System.out.println("get voucher List end");  
                            double toalAmount = 0d;
                            int numberOfTransaction = 0;
                            String functionalCurrencyName = "PHP";

                            for(int x=0;x<voucherList.size();x++) {

                               
                                ApRepApRegisterDetails apRepApRegisterDetails = (ApRepApRegisterDetails)voucherList.get(x);
             
                                functionalCurrencyName = apRepApRegisterDetails.getArgFunctionalCurrencyName();
                                System.out.println(apRepApRegisterDetails.getArgPosted() + " status posted  111");
                                //line type
                                strB.append("AC10");
                                //bank code
                                strB.append("     ");
                                //currency
                                System.out.println("pass 0");
                                strB.append(functionalCurrencyName);
                                //account number
                                System.out.println("pass 11");
                                if(apRepApRegisterDetails.getArgSplSupplierAccountNumber() == null){
                                    System.out.println("its null");
                                }
                                else{
                               
                                    System.out.println(apRepApRegisterDetails.getArgSplSupplierAccountNumber());
                                }
                                
                               System.out.println("pass end");
                                String strAccountNumber = apRepApRegisterDetails.getArgSplSupplierAccountNumber()==null?"":apRepApRegisterDetails.getArgSplSupplierAccountNumber();
                          
                                
                           
                                System.out.println("2");
                                //actually 16 but need 2 space at the end
                                if(strAccountNumber.length() < 14){
                                    int stringLength = strAccountNumber.length();
                                    int spaceAdd = 16 - stringLength;
                                    for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                      strB.append("0");  
                                    }

                                }
                                strB.append(strAccountNumber);
                                strB.append("  ");
                                System.out.println("3");
                                //amount
                                double amount = apRepApRegisterDetails.getArgAmount();

                                String strAmount = Double.toString(amount);

                                if(!strAmount.contains(".")){
                                    strAmount += "00";
                                }else{
                                    strAmount = strAmount.replaceAll(Pattern.quote("."),"");
                                }
                                
                                if(strAmount.length() < 13){
                                    int stringLength = strAmount.length();
                                    int spaceAdd = 13 - stringLength;
                                    for(int xIndex=0 ;xIndex<spaceAdd;xIndex++){
                                      strB.append("0");
                                    }
                                }
                                strB.append(strAmount); 
                                System.out.println("4");
                                //remarks
                                String strRemarks = apRepApRegisterDetails.getArgDescription()==null?"": apRepApRegisterDetails.getArgDescription();
                                strB.append(strRemarks);

                                if(strRemarks.length() < 2400){
                                    int stringLength = strAmount.length();
                                    int spaceAdd = 2400 - stringLength;
                                    for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                      strB.append(" ");
                                    }
                                }
                                System.out.println("5");
                                //voucher number
                                String strVoucherNumber =  apRepApRegisterDetails.getArgDocumentNumber();
                                strB.append(strVoucherNumber);

                                if(strVoucherNumber.length() < 60){
                                    int stringLength = strAmount.length();
                                    int spaceAdd = 60 - stringLength;
                                    for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                      strB.append(" ");
                                    }
                                }
                                
                                strB.append(System.getProperty("line.separator"));

                                toalAmount += amount;

                                numberOfTransaction++;

                            }
                            SimpleDateFormat dateFrmt = new SimpleDateFormat("yyyyMMdd");
                             //head type
                            String str1stHead = "AC01";
                    
                            
                            String formatDateStr = dateFrmt.format(Common.convertStringToSQLDate(actionForm.getDigiBankPostingDate()));
                            
                            //read file for digibank batch number/account number
                            String digiFileName = "c:/opt/ofs-resources/" + user.getCompany() + "/digibankAutoNumber.txt";

                          
                            String batchNumberNew = "ACP" + formatDateStr + "01";
                           
                            if (!new java.io.File(digiFileName).exists()) {
                                
                                File file = new File(digiFileName);
                                FileOutputStream is = new FileOutputStream(file);
                                OutputStreamWriter osw = new OutputStreamWriter(is);    
                                Writer w = new BufferedWriter(osw);
                                w.write(batchNumberNew);
                                w.close();

                            }else{
                                
                                String batchNumberRecent =  "ACP" + formatDateStr + "01";
                                FileReader fileReader = 
                                    new FileReader(digiFileName);

                                // Always wrap FileReader in BufferedReader.
                                BufferedReader bufferedReader = 
                                    new BufferedReader(fileReader);
                                String line = "";
                                while((line = bufferedReader.readLine()) != null) {
                                   batchNumberRecent = line.trim();
                                }   
                                
                                bufferedReader.close();
                                fileReader.close();
                                int numberCount = 1;
                                while(batchNumberRecent.equals(batchNumberNew)){
                               
                                    
                                    String nmbrStr = Integer.toString(numberCount);
                                    
                                    nmbrStr = nmbrStr.length()==1?("0" + nmbrStr):nmbrStr;
                                    
                                    batchNumberNew = "ACP" + formatDateStr + nmbrStr;
     
                                    numberCount++;
                                    
                                }
                                
                                
                            
                            } 
                               
                            //Batch Number Head
                            strBatchNumberHead = actionForm.getDigiBankBatchNumber();
                            if(strBatchNumberHead.trim().equals("")){
                                strBatchNumberHead = batchNumberNew;
                                
                                File file = new File(digiFileName);
                                FileOutputStream is = new FileOutputStream(file);
                                OutputStreamWriter osw = new OutputStreamWriter(is);    
                                Writer w = new BufferedWriter(osw);
                                w.write(batchNumberNew);
                                w.close();
                                
                                
                            }

                            if(strBatchNumberHead.length() < 9){
                                int stringLength = strBatchNumberHead.length();
                                int spaceAdd = 9 - stringLength;
                                for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                  strBatchNumberHead += " ";  
                                } 
                            }
            
                            //Currency Head
                            String strCurrencyHead = functionalCurrencyName;

                  
                            //Account Number Head
                            String strAccountNumberHead = actionForm.getDigiBankFundingAccountNumber();
                            if(strAccountNumberHead.length() < 13){
                                int stringLength = strAccountNumberHead.length();
                                int spaceAdd = 13 - stringLength;
                                for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                  strAccountNumberHead = "0" + strAccountNumberHead;
                                } 
                            }
               
                            //Posting Date YYYYMMDD Head
                    
                            String strPostingDateHead = dateFrmt.format(Common.convertStringToSQLDate(actionForm.getDigiBankPostingDate()));

                            //Total amount Head
                            String strAmountHead = Double.toString(toalAmount);

                            if(!strAmountHead.contains(".")){
                                strAmountHead += "00";
                            }else{
                                strAmountHead = strAmountHead.replaceAll(Pattern.quote("."),"");
                            }

                            if(strAmountHead.length() < 13){
                                int stringLength = strAmountHead.length();
                                int spaceAdd = 13 - stringLength;
                                for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                  strAmountHead = "0" + strAmountHead;
                                }
                            }
  
                            //Total number of Transaction Head
                            String strNoOfTransactionHead = Integer.toString(numberOfTransaction);
                            if(strNoOfTransactionHead.length() < 5){
                                int stringLength = strNoOfTransactionHead.length();
                                int spaceAdd = 5 - stringLength;
                                for(int xIndex=0 ; xIndex<spaceAdd;xIndex++){
                                  strNoOfTransactionHead = "0" + strNoOfTransactionHead;
                                }
                            }


                            String headerLineStr  = str1stHead + strBatchNumberHead + strCurrencyHead + strAccountNumberHead + strPostingDateHead+ strAmountHead + strNoOfTransactionHead + System.getProperty("line.separator");

                            strB.insert(0, headerLineStr);
 
                            report.setViewType(Constants.REPORT_VIEW_TYPE_DIGIBANK);
                           
                            report.setBytes(strB.toString().getBytes());   
				        
                        } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

                                report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
                                report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
                                    new ApRepApRegisterDS(list, actionForm.getGroupBy())));												    

                        }
                       
                       
                        if(actionForm.getEnablePayment()){
                            statusList.add("PAYMENT ENTRY STATUS");
                            statusList.add("VOUCHER CODE,AMOUNT,POSTED,VOIDED,INFO,STATUS");
                            
                           
                            for(int x=0;x<voucherList.size();x++){
                                
                                boolean isFailed = false;

                                ApRepApRegisterDetails apRegisterDetails = (ApRepApRegisterDetails)voucherList.get(x);
  
                                String supplierCode = apRegisterDetails.getArgSplSupplierCode();
                                String voucherCode = apRegisterDetails.getArgDocumentNumber();
                                String amount = Double.toString(apRegisterDetails.getArgAmount());
                                String posted = apRegisterDetails.getArgPosted()?"YES":"NO";
                                String voided = apRegisterDetails.getArgVoid()?"YES":"NO";
                                String info = "";
                                String status = "SUCCESS";                          
                                
                                if(!apRegisterDetails.getArgPosted()){  
                                    info = " \"NOT POSTED\" ";
                                    status = "FAILED";
                                    isFailed = true;
                                }
                                
                                if(apRegisterDetails.getArgVoid()){
                                    info += " \"VOIDED\" ";
                                    status = "FAILED";
                                    isFailed = true;
                                }

                                if(apRegisterDetails.getArgAmountPaid()>0){
                                    info += " \"ALREADY PAID OR HAVE AMOUNT PAID\"  ";
                                    status = "FAILED";
                                    isFailed = true;
                                }
                                
                                
                                ApCheckDetails details = new ApCheckDetails();
           
                                details.setChkCode(null);           
                                details.setChkDate(new java.util.Date());
                                details.setChkCheckDate(new java.util.Date());
                                details.setChkNumber(null);
                                details.setChkDocumentNumber(null);
                                details.setChkReferenceNumber(actionForm.getPaymentReferenceNumber());
                                details.setChkAmount(apRegisterDetails.getArgAmount());
                                details.setChkVoid(Common.convertBooleanToByte(false));
                                details.setChkCrossCheck(Common.convertBooleanToByte(false));
                                details.setChkDescription("TEST");           
                                details.setChkConversionDate(null);
                                details.setChkConversionRate(1);
                                details.setChkCreatedBy(user.getUserName());
                                details.setChkDateCreated(new java.util.Date());
                                details.setChkLastModifiedBy(user.getUserName());
                                details.setChkDateLastModified(new java.util.Date());
                                details.setChkMemo("");
                                details.setChkSupplierName(apRegisterDetails.getArgSplSupplierCode());

                               
                                ArrayList vpsList = new ArrayList();
  
                                ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();

                                mdetails.setAvVpsCode(apRegisterDetails.getArgVpsCode());
                                mdetails.setAvApplyAmount(apRegisterDetails.getArgAmount());
                                mdetails.setAvTaxWithheld(0);
                                mdetails.setAvDiscountAmount(0);
                                mdetails.setAvAllocatedPaymentAmount(0);

                                vpsList.add(mdetails);
           	
                                            
                                try {
                                        
                                    
                                    if(!isFailed){
                                        System.out.println("payment section");   
                                        Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
                                        apRegisterDetails.getArgCurrency(),
                                        supplierCode,
                                    /*batch name */   null ,
                                        vpsList, /*is draft */actionForm.getIsDraftPayment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
  
                                        System.out.println("payment section end");    
                                       ejbARG.getUpdateVoucherReferenceByDocumentNumber(voucherCode,strBatchNumberHead,user.getCmpCode());
                                        System.out.println("update reference number end"); 
                                        
                                    }

                                } catch (GlobalRecordAlreadyDeletedException ex) {

                                    info += " \"Already Deleted\"  ";
                                    status = "FAILED";
                                
 
                                } catch (GlobalDocumentNumberNotUniqueException ex) {

                                    info += " \"Document Number not Unique\"  ";
                                    status = "FAILED";
                                  

                                } catch (ApCHKCheckNumberNotUniqueException ex) {

                                    info += " \"Check Number not Unique\"  ";
                                    status = "FAILED";
                                  

                                } catch (GlobalConversionDateNotExistException ex) {

                                     info += " \"Convertion date not exist\"  ";
                                    status = "FAILED";          	                      	

                                } catch (GlobalTransactionAlreadyApprovedException ex) {

                                    info += " \"Transaction Already Approved\"  ";
                                    status = "FAILED";
                                   

                                } catch (GlobalTransactionAlreadyPendingException ex) {

                                    info += " \"Transaction Pending\"  ";
                                    status = "FAILED";
                                    
                                } catch (GlobalTransactionAlreadyPostedException ex) {

                                    info += " \"Transaction already Posted\"  ";
                                    status = "FAILED";
                                

                                } catch (GlobalTransactionAlreadyVoidException ex) {

                                    info += " \"Transaction already voided\"  ";
                                    status = "FAILED";
                                

                                } catch (ApVOUOverapplicationNotAllowedException ex) {

                                    info += " \"Over apply not allowed\"  ";
                                    status = "FAILED";
                                   

                                } catch (ApCHKVoucherHasNoWTaxCodeException ex) {

                                    info += " \"voucher has no wtax code\"  ";
                                    status = "FAILED";
                                

                                } catch (GlobalTransactionAlreadyLockedException ex) {

                                    info += " \"Transction already locked\"  ";
                                    status = "FAILED";
                            

                                } catch (GlobalNoApprovalRequesterFoundException ex) {

                                     info += " \"Approved request not found\"  ";
                                    status = "FAILED";
                                

                                } catch (GlobalNoApprovalApproverFoundException ex) {

                                     info += " \"No approva aproved found\"  ";
                                    status = "FAILED";
                                 

                                } catch (GlJREffectiveDateNoPeriodExistException ex) {

                                      info += " \"No period exist\"  ";
                                    status = "FAILED";
                      

                                } catch (GlJREffectiveDatePeriodClosedException ex) {

                                     info += " \"Period closed\"  ";
                                    status = "FAILED";
                              

                                } catch (GlobalJournalNotBalanceException ex) {

                                     info += " \"Journal not balance\"  ";
                                    status = "FAILED";
                               

                                } catch (GlobalBranchAccountNumberInvalidException ex) {

                                     info += " \"Branch account number invalid\"  ";
                                    status = "FAILED";
                                

                                } catch (EJBException ex) {
                                    if (log.isInfoEnabled()) {

                                       log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                                          " session: " + session.getId());
                                     }
                                    System.out.println(ex.toString());
                                    info += " \"Unknown error\"  ";
                                    status = "FAILED";
                                 
                                }
      
                               String statusLine = voucherCode + "," + amount + "," + posted + "," + voided + "," + info + "," + status;
                           
                               statusList.add(statusLine);
                         
                            
                            }
                            
                           
                            StringBuilder statusData = Common.convertStatusDataInHtml(statusList);
                            session.setAttribute(Constants.STATUS_REPORT_KEY,statusData);
                            actionForm.setStatusReport(Constants.STATUS_SUCCESS);
                        }
                       
           

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	System.out.print(ex.toString() + " error here");
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ApRepApRegisterAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP ARG Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP ARG Load Action --
*******************************************************/

             }
         
         if(frParam != null) {
             
             try {
                 
             	 actionForm.reset(mapping, request);
             	
                 ArrayList list = null;
                 Iterator i = null;
                 
                 actionForm.clearApRepBrApRgstrList();
                 
                 list = ejbARG.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                 
                 i = list.iterator();
                 
                 while(i.hasNext()) {
                     
                     AdBranchDetails details = (AdBranchDetails)i.next();
                     
                     ApRepBranchApRegisterList apRepBrApRgstrList = new ApRepBranchApRegisterList(actionForm,
                             details.getBrCode(),
                             details.getBrBranchCode(), details.getBrName());
                     apRepBrApRgstrList.setBranchCheckbox(true);
                     
                     actionForm.saveApRepBrApRgstrList(apRepBrApRgstrList);
                     
                 }
                 
             } catch (GlobalNoRecordFoundException ex) {
                 
             } catch (EJBException ex) {
                 
                 if (log.isInfoEnabled()) {
                     
                     log.info("EJBException caught in ApRepApRegisterAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage"); 
                     
                 }
                 
             } 
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearTypeList();           	
	            	
	            	list = ejbARG.getApStAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
                        
                        actionForm.clearVoucherBatchList();           	
                  
                        list = ejbARG.getApOpenVbAll(user.getUserDepartment() == null ? "" :user.getUserDepartment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                        if (list == null || list.size() == 0) {

                                actionForm.setVoucherBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

                        } else {

                            i = list.iterator();

                            while (i.hasNext()) {

                                actionForm.setVoucherBatchList((String)i.next());

                            }

                        }
                        
                        actionForm.clearBankAccountList();           	
            	
                        list = ejbARG.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                        if (list == null || list.size() == 0) {

                                actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);

                        } else {

                                i = list.iterator();

                                while (i.hasNext()) {

                                    actionForm.setBankAccountList((String)i.next());

                                }            		

                        }                	
            	 
	            	actionForm.clearSupplierClassList();           	
	            	
	            	list = ejbARG.getApScAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ApRepApRegisterAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("apRepApRegister"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepApRegisterAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
