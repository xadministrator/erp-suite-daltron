package com.struts.jreports.ap.apregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepApRegisterDetails;

public class ApRepApRegisterDS implements JRDataSource{

	  private ArrayList data = new ArrayList();

	   private int index = -1;

	   public ApRepApRegisterDS(ArrayList list, String groupBy) {
	   	
	      Iterator i = list.iterator();
	      String groupCoa = null;
	      
	      while (i.hasNext()) {
	      	
	         ApRepApRegisterDetails details = (ApRepApRegisterDetails)i.next();
	         
	         String group = null;
	         
	         if (groupBy.equals("SUPPLIER CODE")) {
	         	
	         	group = details.getArgSplSupplierCode() + "-" + details.getArgSplName();
	         	
	         } else if (groupBy.equals("SUPPLIER TYPE")) {
	         	
	         	group = details.getArgSplSupplierType();
	         	details.setArgSplAddress(null);
	         	
	         } else if (groupBy.equals("SUPPLIER CLASS")) {
	         	
	         	group = details.getArgSplSupplierClass();
	         	details.setArgSplAddress(null);
	         	
	         }
	         
	         groupCoa = details.getArgDrCoaAccountNumber();         
	         
	       
	         
	         
	         
		     ApRepApRegisterData argData = new ApRepApRegisterData(
		    		 details.getArgDate(), 
		     details.getArgSplSupplierCode(), 
		     details.getArgDescription(),
		     details.getArgDocumentNumber(), 
		     details.getArgSplSupplierCountry(),
		     details.getArgUomName(),
		     details.getArgItemName(),
		     details.getArgItemCategory(),
		     details.getArgQuantity(),
		     details.getArgReferenceNumber(), 
			 new Double(details.getArgAmount()),
			 new Double(details.getArgTaxAmount()), 
			 group, 
			 details.getArgSplName(), 
			 details.getArgSplTin(),
			 details.getArgDrCoaAccountNumber(), 
			 details.getArgDrCoaAccountDescription(), 
			 new Double(details.getArgDrDebitAmount()), 
			 new Double(details.getArgDrCreditAmount()),
			 new Double(details.getArgBalance()), 
			 details.getArgType(), 
			 details.getArgSplAddress(), 
			 new Double(details.getArgBeginningBalance()),
			 new Double(details.getArgPrevInterestIncome()), 
			 groupCoa, 
			 details.getArgPoNumber(), 
			 details.getArgCheckType());
		     
	         data.add(argData);
	      }
	      
	   }

	   public boolean next() throws JRException{
	      index++;
	      return (index < data.size());
	   }

	   public Object getFieldValue(JRField field) throws JRException{
	      Object value = null;

	      String fieldName = field.getName();

	      if("voucherDate".equals(fieldName)){
	         value = ((ApRepApRegisterData)data.get(index)).getVoucherDate();       
	      }else if("supplierCode".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getSupplierCode();
	      }else if("description".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getDescription();
	      }else if("voucherNumber".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getVoucherNumber();
	      }else if("supplierCountry".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getSupplierCountry();
	      }else if("itemName".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getItemName();
	          System.out.println("value="+value);
	      }else if("itemCategory".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getItemCategory();       
	      }else if("unit".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getUomName();
	      }else if("quantity".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getQuantity();  
	      }else if("referenceNumber".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getReferenceNumber();
	      }else if("amount".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getAmount();  
	      }else if("taxAmount".equals(fieldName)){
	          value = ((ApRepApRegisterData)data.get(index)).getTaxAmount(); 
	      }else if("groupBy".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getGroupBy();
	      }else if("supplierName".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getSupplierName();
	      }else if("supplierTin".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getSupplierTin();
	      }else if("accountNumber".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getAccountNumber();
	      }else if("accountDescription".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getAccountDescription();
	      }else if("debitAmount".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getDebitAmount();
	      }else if("creditAmount".equals(fieldName)){
	        value = ((ApRepApRegisterData)data.get(index)).getCreditAmount();
	      }else if("balance".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getBalance();
	      }else if("type".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getType();
	      }else if("supplierAddress".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getSupplierAddress();
	      }else if("beginningBalance".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getBeginningBalance();
	      }else if("prevInterestIncome".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getPrevInterestIncome();
	      }else if("poNumber".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getPoNumber();
	  	  	System.out.println("ApRepApRegisterData getPoNumber : "+value);
	      }else if("checkType".equals(fieldName)){
	    	value = ((ApRepApRegisterData)data.get(index)).getCheckType();
	      }

	      return(value);
	   }
}
