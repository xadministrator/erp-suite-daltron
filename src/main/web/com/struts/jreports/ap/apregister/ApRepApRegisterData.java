package com.struts.jreports.ap.apregister;





public class ApRepApRegisterData implements java.io.Serializable{
   
   private java.util.Date voucherDate = null;
   private String supplierCode = null;
   private String description = null;
   private String voucherNumber = null;
   private String referenceNumber = null;
   private Double amount = null;
   private Double taxAmount = null;
   private String groupBy = null;
   private String supplierName = null;
   private String supplierTin = null;
   
   private String supplierCountry = null;
   private String uomName = null;
   private String itemName = null;
   private String itemCategory = null;
   private Double quantity = null;
   
   private String accountNumber = null;
   private String accountDescription = null;
   private Double debitAmount = null;
   private Double creditAmount = null;
   private Double balance = null;
   private String type = null;
   private String supplierAddress = null;
   private Double beginningBalance = null;
   private Double prevInterestIncome = null;
   private String groupByCoa = null;
   private String poNumber = null;
   private String chkType = null;
   
   public ApRepApRegisterData(
		   java.util.Date voucherDate, String supplierCode, 
   String description,
   String voucherNumber,
   String supplierCountry,
   String uomName,
   String itemName,
   String itemCategory,
   Double quantity,
    String referenceNumber, 
    Double amount,
    Double taxAmount,
    String groupBy, 
    String supplierName,
   String supplierTin, 
   String accountNumber, 
   String accountDescription,
   Double debitAmount, 
   Double creditAmount,
   Double balance, 
   String type, 
   String supplierAddress,
   Double beginningBalance,
   Double prevInterestIncome,
   String groupByCoa,
   String poNumber, 
   String chkType) {
      	
      	this.voucherDate = voucherDate;
      	this.supplierCode = supplierCode;
      	this.description = description;
      	this.voucherNumber = voucherNumber;
      	this.referenceNumber = referenceNumber;
      	this.amount = amount;
      	this.taxAmount = taxAmount;
      	this.groupBy = groupBy;
      	this.supplierName = supplierName;
      	this.supplierTin = supplierTin;
      	
      	this.supplierCountry = supplierCountry;
      	this.uomName = uomName;
      	this.itemName = itemName;
      	this.itemCategory = itemCategory;
      	
      	this.quantity = quantity;

      	this.accountNumber = accountNumber;
      	this.accountDescription = accountDescription;
      	this.debitAmount = debitAmount;
      	this.creditAmount = creditAmount;
      	this.balance = balance;
      	this.type = type;
      	this.supplierAddress = supplierAddress;
      	this.beginningBalance = beginningBalance;
      	this.prevInterestIncome = prevInterestIncome;
      	this.groupByCoa = groupByCoa;
      	this.poNumber = poNumber;
      	this.chkType = chkType;
   }

   public java.util.Date getVoucherDate() {
   	
      return voucherDate;
      
   }
   
   public String getSupplierCode() {
   	
   	  return supplierCode;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public String getVoucherNumber() {
   	
   	  return voucherNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;

   }
      
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public Double getTaxAmount() {
	   	
	   	  return taxAmount;
	   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getSupplierName() {
   	
   	  return supplierName;
   	
   }
   
   public String getSupplierTin() {
   	
   	  return supplierTin;
   	
   }
   
   public String getSupplierCountry() {
	   	
	   	  return supplierCountry;
	   	
   }
   
   public String getUomName() {
	   	
	   	  return uomName;
	   	
   }
   
   public String getItemName() {
	   	
	   	  return itemName;
	   	
   }
   
   public String getItemCategory() {
	   	
	   	  return itemCategory;
	   	
   }
   
   
   
   public Double getQuantity() {
	   	
	   	  return quantity;
	   	
   }
    
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	 
   }
   
   public String getAccountDescription() {
   	
   	  return accountDescription;
   	
   }
   
   public Double getDebitAmount() {
   	
   	  return debitAmount;
   	
   }
   
   public Double getCreditAmount() {
   	
   	  return creditAmount;
   	
   }
   
   public Double getBalance() {
	   
	  return balance;
	   
   }
   
   public String getType() {
	   
	  return type;
	  
   }
   
   public String getSupplierAddress() {
	   
	   return supplierAddress;
	   
   }
   
   public Double getBeginningBalance() {
	   
	   return beginningBalance;
	   
   }
   
   public Double getPrevInterestIncome() {
	   
	   return prevInterestIncome;
	   
   }
   
   public String getGroupByCoa() {
	   
	   return groupByCoa;
	   
   }
   
   public String getPoNumber() {
	   
	   return poNumber;
	   
   }
   
   public String getCheckType() {
	   
	   return chkType;
	   
   }
}
