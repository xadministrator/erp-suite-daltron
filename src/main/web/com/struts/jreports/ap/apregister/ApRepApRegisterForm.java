package com.struts.jreports.ap.apregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepApRegisterForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private String voucherBatch = null;
   private ArrayList voucherBatchList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private boolean includedUnposted = false;
   private String report = null;
   private String statusReport = null;
   private String goButton = null;
   private String closeButton = null;
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();
   
   private String digiBankPostingDate = null;
   private String digiBankFundingAccountNumber = null;
   private String digiBankBatchNumber = null;
   private boolean enablePayment = true;
   private String paymentReferenceNumber = null;
   private String bankAccount = null;
   private boolean isDraftPayment = false;
   private ArrayList bankAccountList = new ArrayList();
   
   
   private boolean showEntries = false;
   private boolean includedPayments = false;
   private boolean summarize = false;
   private boolean includeDirectCheck = false;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList apRepBrApRgstrList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }
   
   public String getVoucherBatch(){
      return(voucherBatch);
   }

   public void setVoucherBatch(String voucherBatch){
      this.voucherBatch = voucherBatch;
   }

   public ArrayList getVoucherBatchList(){
      return(voucherBatchList);
   }

   public void setVoucherBatchList(String voucherBatch){
      voucherBatchList.add(voucherBatch);
   }

   public void clearVoucherBatchList(){
      voucherBatchList.clear();
      voucherBatchList.add(Constants.GLOBAL_BLANK);
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }

   public String getDocumentNumberFrom(){
      return(documentNumberFrom);
   }

   public void setDocumentNumberFrom(String documentNumberFrom){
      this.documentNumberFrom = documentNumberFrom;
   }

   public String getDocumentNumberTo(){
      return(documentNumberTo);
   }

   public void setDocumentNumberTo(String documentNumberTo){
      this.documentNumberTo = documentNumberTo;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getType(){
      return(type);
   }

   public void setType(String type){
      this.type = type;
   }

   public ArrayList getTypeList(){
      return(typeList);
   }

   public void setTypeList(String type){
      typeList.add(type);
   }

   public void clearTypeList(){
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
   }
 
   public String getSupplierClass(){
      return(supplierClass);
   }

   public void setSupplierClass(String supplierClass){
      this.supplierClass = supplierClass;
   }

   public ArrayList getSupplierClassList(){
      return(supplierClassList);
   }

   public void setSupplierClassList(String supplierClass){
      supplierClassList.add(supplierClass);
   }

   public void clearSupplierClassList(){
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
   } 

   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }
   
    
   public String getStatusReport() {
   	
   	  return statusReport;
   	
   }
   
   public void setStatusReport(String statusReport) {
   	
   	  this.statusReport = statusReport;
   	
   }

   public String getUserPermission(){
   	
   	return(userPermission);
   	
   }
   
   public void setUserPermission(String userPermission){
   	
   	this.userPermission = userPermission;
   	
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
   
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }
   
   public String getPaymentStatus() {
   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }
   
   public Object[] getApRepBrApRgstrList(){
       
       return apRepBrApRgstrList.toArray();
       
   }
   
   public ApRepBranchApRegisterList getApRepBrApRgstrByIndex(int index){
       
       return ((ApRepBranchApRegisterList)apRepBrApRgstrList.get(index));
       
   }
   
   public int getApRepBrApRgstrListSize(){
       
       return(apRepBrApRgstrList.size());
       
   }
   
   public void saveApRepBrApRgstrList(Object newApRepBrApRgstrList){
       
       apRepBrApRgstrList.add(newApRepBrApRgstrList);   	  
       
   }
   
   public void clearApRepBrApRgstrList(){
       
       apRepBrApRgstrList.clear();
       
   }
   
   public void setApRepBrApRgstrList(ArrayList apRepBrApRgstrList) {
       
       this.apRepBrApRgstrList = apRepBrApRgstrList;
       
   }
   
   
   public String getDigiBankPostingDate(){
       return this.digiBankPostingDate;
   }
   
   public void setDigiBankPostingDate(String digiBankPostingDate){
       this.digiBankPostingDate = digiBankPostingDate;
   }
   
   public String getDigiBankFundingAccountNumber(){
       return this.digiBankFundingAccountNumber;
   }
   
   public void setDigiBankFundingAccountNumber(String digiBankFundingAccountNumber){
       this.digiBankFundingAccountNumber = digiBankFundingAccountNumber;
   }
   
   public String getDigiBankBatchNumber(){
       return this.digiBankBatchNumber;
   }
   
   public void setDigiBankBatchNumber(String digiBankBatchNumber){
       this.digiBankBatchNumber = digiBankBatchNumber;
   }
   
   public boolean getEnablePayment(){
       return this.enablePayment;
   }
   
   
   public void setEnablePayment(boolean enablePayment ){
       this.enablePayment  = enablePayment;
   }        
           
   public String getPaymentReferenceNumber(){
       return this.paymentReferenceNumber;
   }     
   
   public void setPaymentReferenceNumber(String paymentReferenceNumber){
       this.paymentReferenceNumber  = paymentReferenceNumber;
   }
           
   
    public boolean getIsDraftPayment() {
    
        return isDraftPayment;
   	
   }
   
   public void setIsDraftPayment(boolean isDraftPayment) {
   	
   	  this.isDraftPayment = isDraftPayment;
   	
   }    
          
   
           
    public String getBankAccount() {
    
        return bankAccount;
   	
   }
   
   public void setBankAccount(String bankAccount) {
   	
   	  this.bankAccount = bankAccount;
   	
   }    
           
           
    public ArrayList getBankAccountList() {
   	
   	  return bankAccountList;
   	
   }
   
   public void setBankAccountList(String bankAccount) {
   	
   	  bankAccountList.add(bankAccount);
   	
   }
   
   public void clearBankAccountList() {
   	
   	  bankAccountList.clear();
   	  bankAccountList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public boolean getShowEntries() {
   	
   	  return showEntries;
   	
   }
   
   public void setShowEntries(boolean showEntries) {
   	
   	  this.showEntries = showEntries;
   	
   }

   public boolean getIncludedPayments() {

	   return includedPayments;

   }

   public void setIncludedPayments(boolean includedPayments) {

	   this.includedPayments = includedPayments;

   }
   
   public boolean getSummarize() {
	   
	   return summarize;
	   
   }
   
   public void setSummarize(boolean summarize) {
	   
	   this.summarize = summarize;
	   
   }
   
   public boolean getIncludeDirectCheck() {
	   
	   return includeDirectCheck;
	   
   }
   
   public void setIncludeDirectCheck(boolean includeDirectCheck) {
	   
	   this.includeDirectCheck = includeDirectCheck;
	   
   }

   
   
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){   

       for (int i=0; i<apRepBrApRgstrList.size(); i++) {
           
           ApRepBranchApRegisterList  list = (ApRepBranchApRegisterList)apRepBrApRgstrList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
      goButton = null;
      closeButton = null;
      supplierCode = null;
      voucherBatch = null;
      
      
      digiBankPostingDate = Common.convertSQLDateToString(new Date());;
      digiBankFundingAccountNumber = null;
      digiBankBatchNumber = null;
      bankAccount = Constants.GLOBAL_BLANK;
      
      supplierClass = Constants.GLOBAL_BLANK; 
      type = Constants.GLOBAL_BLANK;
      dateFrom = Common.convertSQLDateToString(new Date());;
      dateTo = Common.convertSQLDateToString(new Date());;
      documentNumberFrom = null;
      documentNumberTo = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_DIGIBANK);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      orderByList.clear();
      orderByList.add(Constants.AP_REGISTER_ORDER_BY_DATE);
      orderByList.add(Constants.AP_REGISTER_ORDER_BY_SUPPLIER_CODE);
      orderByList.add(Constants.AP_REGISTER_ORDER_BY_SUPPLIER_TYPE);
      orderByList.add(Constants.AP_REGISTER_ORDER_BY_DOCUMENT_NUMBER);
      orderBy = Constants.AP_REGISTER_ORDER_BY_DATE;   
      groupByList.clear();
      groupByList.add(Constants.GLOBAL_BLANK);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS);
      groupBy = Constants.AP_SL_ORDER_BY_SUPPLIER_CODE;
      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add(Constants.AP_FV_PAYMENT_STATUS_PAID);
      paymentStatusList.add(Constants.AP_FV_PAYMENT_STATUS_UNPAID);
      paymentStatus = Constants.GLOBAL_BLANK;
      isDraftPayment = false;

      
      
      enablePayment = false;
      includedUnposted = false;
      showEntries = false;
      includedPayments = false;
      summarize = false;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
        if(!Common.validateDateFormat(dateFrom)){
            errors.add("dateFrom", new ActionMessage("apRegister.error.dateFromInvalid"));
        }
        if(!Common.validateDateFormat(dateTo)){
           errors.add("dateTo", new ActionMessage("apRegister.error.dateToInvalid"));
        }
        if(!Common.validateStringExists(typeList, type)){
           errors.add("type", new ActionMessage("apRegister.error.typeInvalid"));
        }
        if(!Common.validateStringExists(supplierClassList, supplierClass)){
           errors.add("supplierClass", new ActionMessage("apRegister.error.supplierClassInvalid"));
        }	
        
        
        //Digibank validation codes
        if(viewType.equals("DIGIBANK")){
            
          
            if(enablePayment){
                if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                errors.add("bankAccount",
               new ActionMessage("apRegister.error.bankAccountRequired"));
                }
            
            
                if(Common.validateRequired(digiBankPostingDate)){
                errors.add("digiBankPostingDate",
                   new ActionMessage("apRegister.error.digiBankPostingDateRequired"));
                }
                
            }

            if(!Common.validateDateFormat(digiBankPostingDate)){
                errors.add("digiBankPostingDate", new ActionMessage("apRegister.error.digiBankPostingDateInvalid"));
            }
            
     
            
              if(Common.validateRequired(digiBankFundingAccountNumber )){
                errors.add("digiBankFundingAccountNumber ",
               new ActionMessage("apRegister.error.fundingAccountNumberRequired"));
            }
        }
      }
      return(errors);
   }
}
