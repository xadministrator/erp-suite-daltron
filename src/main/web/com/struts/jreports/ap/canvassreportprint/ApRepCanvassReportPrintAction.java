package com.struts.jreports.ap.canvassreportprint;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepCanvassReportPrintController;
import com.ejb.txn.ApRepCanvassReportPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;

public class ApRepCanvassReportPrintAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 Check if user has a session
 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApRepCanvassReportPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApRepCanvassReportPrintForm actionForm = (ApRepCanvassReportPrintForm)form; 
			
			String frParam= Common.getUserPermission(user, Constants.AP_REP_CANVASS_REPORT_PRINT_ID);
			
			if (frParam != null) {
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
			}
			
/*******************************************************
 Initialize ApRepCanvassReportPrintController EJB
 *******************************************************/
			
			ApRepCanvassReportPrintControllerHome homeCRP = null;
			ApRepCanvassReportPrintController ejbCRP = null;       
			
			try {
				
				homeCRP = (ApRepCanvassReportPrintControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/ApRepCanvassReportPrintControllerEJB", ApRepCanvassReportPrintControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApRepCanvassReportPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbCRP = homeCRP.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApRepCanvassReportPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report report = new Report();
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
 -- TO DO getPrByPrCode()
 *******************************************************/       
			
			if (frParam != null) {
				
				AdCompanyDetails adCmpDetails = null;
				ArrayList list = null;   
				
				try {
					
					ArrayList prCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
						
						prCodeList.add(new Integer(request.getParameter("purchaseRequisitionCode")));
						
					} else {
						
						int i = 0;
						
						while (true) {
							
							if (request.getParameter("purchaseRequisitionCode[" + i + "]") != null) {
								
								prCodeList.add(new Integer(request.getParameter("purchaseRequisitionCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
						
					}
					
					if (request.getParameter("totalBasedPo").equals("true")) {
						
						ejbCRP.selectApRepCanvassPerTotal(prCodeList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
					}
					
					list = ejbCRP.executeApRepCanvassReportPrint(prCodeList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()); 
					
					adCmpDetails = ejbCRP.getAdCompany(user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("canvassReportPrint.error.noRecordFound"));  
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApRepCanvassReportPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apRepCanvassReportPrint");
					
				}
				
				/** fill report parameters, fill report to pdf and set report session **/ 	 
				
				String nullString = null;
				
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("datePrinted", new Date());
				parameters.put("printedBy", user.getUserName());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCanvassReportPrint.jasper";
				
				if (!new java.io.File(filename).exists()) {
					
					filename = servlet.getServletContext().getRealPath("jreports/ApRepCanvassReportPrint.jasper");
					
				}
				
				try {
					
					report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
							new ApRepCanvassReportPrintDS(list)));   				     
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
					if(log.isInfoEnabled()) {
						
						log.info("Exception caught in ApRepCanvassReportPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					} 
					
					return(mapping.findForward("cmnErrorPage"));
					
				}              
					
				session.setAttribute(Constants.REPORT_KEY, report);
				
				return(mapping.findForward("apRepCanvassReportPrint"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("apRepCanvassReportPrint"));
				
			}
			
		} catch(Exception e) {
			
			e.printStackTrace();
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in ApRepCanvassReportPrintAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}        
			return(mapping.findForward("cmnErrorPage"));        
		}
	}
	
}