/*
 * opt/jreport/ApRepCanvassReportPrintDS.java
 *
 * Created on April 24, 2006, 3:17 PM
 *
 * @author  Aliza D.J. Anos
 */

package com.struts.jreports.ap.canvassreportprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ApRepCanvassReportPrintDetails;

public class ApRepCanvassReportPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCanvassReportPrintDS(ArrayList list){
   	
   		Iterator i = list.iterator();
	   	
	   	while (i.hasNext()) {
	   		
	   		ApRepCanvassReportPrintDetails mdetails = (ApRepCanvassReportPrintDetails) i.next();
	   		
	   		ApRepCanvassReportPrintData prData = null;
	   		
	   		prData = new ApRepCanvassReportPrintData(mdetails.getCrpPrNumber(), mdetails.getCrpPrDate(), 
	   				mdetails.getCrpPrDescription(), String.valueOf(mdetails.getCrpPrFcSymbol()), mdetails.getCrpPrlIiName(), 
					mdetails.getCrpPrlIiDescription(), mdetails.getCrpPrlLocName(), mdetails.getCrpPrlUomName(),  mdetails.getCrpPrlCnvRemarks(),
					mdetails.getCrpPrlCnvSplSupplierCode(), mdetails.getCrpPrlCnvSplName(), 
					new Double(mdetails.getCrpPrlCnvQuantity()), new Double(mdetails.getCrpPrlCnvUnitPrice()), 
					new Double(mdetails.getCrpPrlCnvAmount()), mdetails.getCrlPrlLastDeliveryDate(),
					Common.convertByteToBoolean(mdetails.getCrlPrlPO())
	   				);
	   		
		    data.add(prData);	    	                                                         
		    	
		}
      
   }

   public boolean next() throws JRException{
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException{
   	
      Object value = null;
      
      String fieldName = field.getName();
      
      if("documentNumber".equals(fieldName)) {
         value = ((ApRepCanvassReportPrintData)data.get(index)).getDocumentNumber();
      } else if("date".equals(fieldName)) {
         value = ((ApRepCanvassReportPrintData)data.get(index)).getDate();
      } else if("description".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getDescription();
      } else if("currencySymbol".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getCurrencySymbol();
      } else if("itemName".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getItemName();
      } else if("itemDescription".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getItemDescription();
      } else if("location".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getLocation();
      } else if("remarks".equals(fieldName)) {
          value = ((ApRepCanvassReportPrintData)data.get(index)).getRemarks();
      } else if("supplierCode".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getSupplierCode();
      } else if("supplierName".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getSupplierName();
      } else if("quantity".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getQuantity();
      } else if("unitPrice".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getUnitPrice();
      } else if("uomName".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getUomName();
      } else if("amount".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getAmount();
      } else if("lastDeliveryDate".equals(fieldName)) {
        value = ((ApRepCanvassReportPrintData)data.get(index)).getLastDeliveryDate();
      } else if("po".equals(fieldName)) {
          value = ((ApRepCanvassReportPrintData)data.get(index)).getPO();
      } 
      
      return(value);
   }
}
