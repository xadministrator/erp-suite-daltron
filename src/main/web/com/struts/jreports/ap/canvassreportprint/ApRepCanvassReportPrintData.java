package com.struts.jreports.ap.canvassreportprint;

import java.util.Date;

public class ApRepCanvassReportPrintData implements java.io.Serializable {

   private String documentNumber = null;
   private Date date = null;
   private String description = null;
   private String currencySymbol = null;
   private String itemName = null;
   private String itemDescription = null;
   private String location = null;
   private String uomName = null;
   private String remarks = null;
   private String supplierCode = null;
   private String supplierName = null;
   private Double quantity = null;
   private Double unitPrice = null;
   private Double amount = null;
   private String lastDeliveryDate = null;
   private boolean po = false;
   
   public ApRepCanvassReportPrintData(
   		String documentNumber, Date date, String description, String currencySymbol, String itemName, String itemDescription, String location,
		String uomName, String remarks, String supplierCode, String supplierName, Double quantity, Double unitPrice, Double amount, String lastDeliveryDate, boolean po) {
      	
      	this.documentNumber = documentNumber;
      	this.date = date;
      	this.description = description;
      	this.currencySymbol = currencySymbol;
      	this.itemName = itemName;
      	this.itemDescription = itemDescription;
      	this.location = location;
      	this.uomName = uomName;
      	this.remarks = remarks;
      	this.supplierCode = supplierCode;
      	this.supplierName = supplierName;
      	this.quantity = quantity;
      	this.unitPrice = unitPrice;
      	this.amount = amount;
      	this.lastDeliveryDate = lastDeliveryDate;
      	this.po = po;
      	
   }
   
   public String getDocumentNumber() {
   	
   		return documentNumber;
   		
   }
   
   public Date getDate() {
   	
   		return date;
   	
   }
   
   public String getDescription() {
   	
   		return description;
   	
   }
   
   public String getCurrencySymbol() {
   	
   		return currencySymbol;
   	
   }
   
   public String getItemName() {
   	
   		return itemName;
   	
   }
   
   public String getItemDescription() {
   	
   		return itemDescription;
   	
   }
   
   public String getLocation() {
   	
   		return location;
   	
   }
   
   public String getUomName() {
   	
   		return uomName;
   	
   }
   
   public String getRemarks() {
	   	
  		return remarks;
  	
  }
   
   public String getSupplierCode() {
   	
   		return supplierCode;
   	
   }
   
   public String getSupplierName() {
   	
   		return supplierName;
   	
   }
   
   public Double getQuantity() {
   	
   		return quantity;
   	
   }
   
   public Double getUnitPrice() {
   	
   		return unitPrice;
   	
   }
   
   public Double getAmount() {
   	
   		return amount;
   	
   }
   
   public String getLastDeliveryDate() {
   	
   		return lastDeliveryDate;
   	
   }
   
   public boolean getPO() {
	   	
  		return po;
  	
  }
 
}
