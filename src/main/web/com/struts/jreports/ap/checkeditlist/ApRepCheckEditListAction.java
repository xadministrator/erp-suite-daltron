package com.struts.jreports.ap.checkeditlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepCheckEditListController;
import com.ejb.txn.ApRepCheckEditListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;

public final class ApRepCheckEditListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepCheckPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepCheckEditListForm actionForm = (ApRepCheckEditListForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.AP_REP_CHECK_EDIT_LIST_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ApRepCheckEditListController EJB
*******************************************************/

         ApRepCheckEditListControllerHome homeVP = null;
         ApRepCheckEditListController ejbVP = null;       

         try {
         	
            homeVP = (ApRepCheckEditListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepCheckEditListControllerEJB", ApRepCheckEditListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepCheckEditListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbVP = homeVP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepCheckEditListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getApVouByVouCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list1 = new ArrayList();
			    ArrayList list2 = new ArrayList();
				    
				try {
					
					ArrayList chkCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
						chkCodeList.add(new Integer(request.getParameter("checkCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("checkCode[" + i + "]") != null) {
								
								chkCodeList.add(new Integer(request.getParameter("checkCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list1 = ejbVP.executeApRepCheckEditList(chkCodeList, user.getCmpCode());
	            	
	            	try {
	            		
	            		list2 = ejbVP.executeApRepCheckEditListSub(chkCodeList, user.getCmpCode());
	            	
	            	} catch (GlobalNoRecordFoundException ex) {
	            		
	            	}
	            	
			        // get company
			       
			        adCmpDetails = ejbVP.getAdCompany(user.getCmpCode());	            	
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkEditList.error.checkAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApRepCheckEditListAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
			    
			    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();
			    
			    String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckEditListSub.jasper";
	       
		        if (!new java.io.File(subreportFilename).exists()) {
		       		    		    
		           subreportFilename = servlet.getServletContext().getRealPath("jreports/ApRepCheckEditListSub.jasper");
			    
		        }
			    
			    JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
			    
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("apRepCheckEditListSub", subreport);
                parameters.put("apRepCheckEditListSubDS", new ApRepCheckEditListSubDS(list2));
                
                String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckEditList.jasper";
	       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckEditList.jasper");
			    
		        }
				 						    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepCheckEditListDS(list1, user.getUserName())));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ApRepCheckEditListAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("apRepCheckEditList"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("apRepCheckEditListForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ApRepCheckEditListAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
