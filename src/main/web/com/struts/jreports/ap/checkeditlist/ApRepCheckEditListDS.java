package com.struts.jreports.ap.checkeditlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ApRepCheckEditListDetails;


public class ApRepCheckEditListDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCheckEditListDS(ArrayList apRepCELList, String userName) {
      
      Date currentDate = new Date();
      
   	  Iterator i = apRepCELList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ApRepCheckEditListDetails details = (ApRepCheckEditListDetails)i.next();

   	    if(details.getCelDrDebit() == 1) {
   	    
	   	  	ApRepCheckEditListData apRepCELData = new ApRepCheckEditListData(
	   	     details.getCelChkBatchName(), details.getCelChkBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getCelChkTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), 
			 Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getCelChkNumber(), details.getCelChkDescription(), details.getCelChkDocumentNumber(), 
			 Common.convertSQLDateToString(details.getCelChkDate()), details.getCelSplSupplierCode(),
			 details.getCelSplSupplierName(), details.getCelChkAdBaName(),
			 new Double(details.getCelChkAmount()), details.getCelDrAccountNumber(), 
			 details.getCelDrAccountDescription(), details.getCelDrClass(), new Double(details.getCelDrAmount()), null);
	   	  	

             data.add(apRepCELData);
	   	  	
	    } else {
	   	
	    	ApRepCheckEditListData apRepCELData = new ApRepCheckEditListData(
	   	   	     details.getCelChkBatchName(), details.getCelChkBatchDescription(), 
				 Common.convertIntegerToString(new Integer(details.getCelChkTransactionTotal())),
				 Common.convertSQLDateToString(currentDate), 
				 Common.convertSQLDateTimeToString(currentDate), userName,
				 details.getCelChkNumber(), details.getCelChkDescription(), details.getCelChkDocumentNumber(), 
				 Common.convertSQLDateToString(details.getCelChkDate()), details.getCelSplSupplierCode(),
				 details.getCelSplSupplierName(), details.getCelChkAdBaName(),
				 new Double(details.getCelChkAmount()), details.getCelDrAccountNumber(), 
				 details.getCelDrAccountDescription(), details.getCelDrClass(), null, new Double(details.getCelDrAmount()));
    	
   	  	    data.add(apRepCELData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("batchName".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getBatchName(); 
     }else if("batchDescription".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getBatchDescription(); 
     }else if("transactionTotal".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getTransactionTotal(); 
     }else if("dateCreated".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getDateCreated();  
     }else if("timeCreated".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getTimeCreated();  
     }else if("createdBy".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getCreatedBy();
     }else if("checkNumber".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getCheckNumber();  
     }else if("checkDescription".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getCheckDescription();    
     }else if("documentNumber".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getDocumentNumber();  
     }else if("date".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getDate();   
     }else if("supplierCode".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getSupplierCode(); 
     }else if("supplierName".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getSupplierName(); 
     }else if("bankAccount".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getBankAccount(); 
     }else if("checkAmount".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getCheckAmount(); 
     }else if("accountNumber".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getAccountNumber();   
     }else if("accountDescription".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getAccountDescription(); 
     }else if("class".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getClassType();    
     }else if("debitAmount".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getDebitAmount();   
     }else if("creditAmount".equals(fieldName)){
        value = ((ApRepCheckEditListData)data.get(index)).getCreditAmount();                   
     }

      return(value);
   }
}
