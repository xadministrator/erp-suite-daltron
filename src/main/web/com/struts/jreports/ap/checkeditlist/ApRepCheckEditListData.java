package com.struts.jreports.ap.checkeditlist;

public class ApRepCheckEditListData implements java.io.Serializable {

	   private String dateCreated = null;
	   private String timeCreated = null;
	   private String createdBy = null;
	   private String batchName = null;
	   private String batchDescription = null;
	   private String transactionTotal = null;
	   private String checkNumber = null;
	   private String checkDescription = null;
	   private String documentNumber = null;
	   private String date = null;
	   private String supplierCode = null;
	   private String supplierName = null;
	   private String bankAccount = null;
	   private Double checkAmount = null;

	   private String accountNumber = null;
	   private String accountDescription = null;
	   private String classType = null;
	   private Double debitAmount = null;
	   private Double creditAmount = null;
	   
	   public ApRepCheckEditListData(String batchName, String batchDescription, String transactionTotal, String dateCreated, 
	      String timeCreated, String createdBy, String checkNumber, String checkDescription, String documentNumber,
	      String date, String supplierCode, String supplierName, String bankAccount, Double checkAmount, 
	      String accountNumber, String accountDescription, String classType, Double debitAmount, Double creditAmount) {
	      
	      this.dateCreated = dateCreated;
	      this.timeCreated = timeCreated;
	      this.createdBy = createdBy;
	      this.batchName = batchName;
	      this.batchDescription = batchDescription;
	      this.transactionTotal = transactionTotal;	
	      this.checkNumber = checkNumber;
	      this.checkDescription = checkDescription;
	      this.documentNumber = documentNumber;
	      this.date = date;
	      this.supplierCode = supplierCode;
	      this.supplierName = supplierName;
	      this.bankAccount = bankAccount;
	      this.checkAmount = checkAmount;
	      this.accountNumber = accountNumber;
	      this.accountDescription = accountDescription;
	      this.classType = classType;
	      this.debitAmount = debitAmount;
	      this.creditAmount = creditAmount;
	      	
	   }
	   
	   public String getDateCreated() {

	      return dateCreated;

	   }

	   public String getTimeCreated() {

	      return timeCreated;

	   }

	   public String getCreatedBy() {

	      return createdBy;

	   }

	   public String getBatchName() {

	       return batchName;

	  }

	   public String getBatchDescription() {

	       return batchDescription;

	  }

	   public String getTransactionTotal() {

	      return transactionTotal;

	  }

	   public String getCheckNumber() {

	      return checkNumber;

	   }

	   public String getCheckDescription() {

	      return checkDescription;

	   }

	   public String getDocumentNumber() {
	   
	      return documentNumber;

	   }

	   public String getDate() {

	      return date;

	   }

	   public String getSupplierCode() {

	      return supplierCode;

	   }

	   public String getSupplierName() {

	      return supplierName;

	   }

	   public String getBankAccount() {

	      return bankAccount;

	   }

	   public Double getCheckAmount() {

	      return checkAmount;

	   }

	   public String getAccountNumber() {

	      return accountNumber;

	   }

	   public String getAccountDescription() {

	      return accountDescription;

	   }

	   public String getClassType() {
	    
	      return classType ;

	   }

	   public Double getDebitAmount() {

	      return debitAmount;

	   }

	   public Double getCreditAmount() {
	    
	      return creditAmount;

	   } 
	}

