package com.struts.jreports.ap.checkeditlist;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepCheckEditListDetails;

public class ApRepCheckEditListSubDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCheckEditListSubDS(ArrayList apRepCELList) {
   	
 	  Iterator i = apRepCELList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ApRepCheckEditListDetails details = (ApRepCheckEditListDetails)i.next(); 
   	    
   	    ApRepCheckEditListSubData apRepCELData = new ApRepCheckEditListSubData(details.getCelChkDocumentNumber(), 
   	    		details.getCelVouNumber(), new Double(details.getCelAppliedAmount()));
   	    
   	    data.add(apRepCELData);

     }
   	  
   }
   	  
   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("documentNumber".equals(fieldName)){
         value = ((ApRepCheckEditListSubData)data.get(index)).getDocumentNumber(); 
      }else if("voucherNumber".equals(fieldName)){
         value = ((ApRepCheckEditListSubData)data.get(index)).getVoucherNumber(); 
      }else if("amountApplied".equals(fieldName)){
         value = ((ApRepCheckEditListSubData)data.get(index)).getAmountApplied(); 
      }

      return(value);
   }
}
