package com.struts.jreports.ap.checkeditlist;

public class ApRepCheckEditListSubData implements java.io.Serializable {
   
   private String documentNumber = null;
   private String voucherNumber = null;
   private Double amountApplied = null;
   
   public ApRepCheckEditListSubData(String documentNumber, String voucherNumber, Double amountApplied) {
      
      this.documentNumber = documentNumber;
      this.voucherNumber = voucherNumber;
      this.amountApplied = amountApplied;
      	
   }

   public String getDocumentNumber() {

      return documentNumber;

   }
   
   public String getVoucherNumber() {

      return voucherNumber;

   }

   public Double getAmountApplied() {

      return amountApplied;

   }
}
