package com.struts.jreports.ap.checkprint;

import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.imageio.ImageIO;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDefaultStyleProvider;
import net.sf.jasperreports.engine.JRTextElement;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepCheckPrintController;
import com.ejb.txn.ApRepCheckPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApModCheckDetails;

public final class ApRepCheckPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {
 
      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepCheckPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepCheckPrintForm actionForm = (ApRepCheckPrintForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.AP_REP_CHECK_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ApRepCheckPrintController EJB
*******************************************************/

         ApRepCheckPrintControllerHome homeCP = null;
         ApRepCheckPrintController ejbCP = null;       

         try {
         	
            homeCP = (ApRepCheckPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepCheckPrintControllerEJB", ApRepCheckPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepCheckPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCP = homeCP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepCheckPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report report = new Report();
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	                            
/*******************************************************
   -- TO DO getChkByChkCode()
*******************************************************/       

		if (frParam != null) {
			
				final String[] majorNames = {"", " Thousand", " Million",
				" Billion", " Trillion", " Quadrillion", " Quintillion"};
				
				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
				" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
				
				final String[] numNames = {"", " One", " Two", " Three", " Four",
				" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
				" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};
				
				AdCompanyDetails adCmpDetails = null;
				String baName = null;
				ArrayList list = null;   
				    
				try {
					
					ArrayList chkCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
						
						chkCodeList.add(new Integer(request.getParameter("checkCode")));
						
				    } else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("checkCode[" + i + "]") != null) {
								
								chkCodeList.add(new Integer(request.getParameter("checkCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
					
					list = ejbCP.executeApRepCheckPrint(chkCodeList, user.getCmpCode()); 
					
					adCmpDetails = ejbCP.getAdCompany(user.getCmpCode());
					
					Iterator chkIter = list.iterator();
			        
			        while (chkIter.hasNext()) {
			        	
			        	ApModCheckDetails mdetails = (ApModCheckDetails)chkIter.next();
			        	
			        	if(baName == null) {
			        		
			        		baName = mdetails.getChkBaName();
			        		
			        	}
			        	
			            int num = (int)Math.floor(mdetails.getChkAmount());
					
						String str = Common.convertDoubleToStringMoney(mdetails.getChkAmount(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());
						
						mdetails.setChkAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
						str + "/100 Only");	
						
						/*if (request.getParameter("forward") != null) {
							
							// set supplier name
							String supplierName = new String(request.getParameter("supplierName"));
							mdetails.setChkSplName(supplierName);
							
						}*/
						
			        }
			        			   
				} catch (GlobalNoRecordFoundException ex) {
				  	
				   errors.add(ActionMessages.GLOBAL_MESSAGE,
				              new ActionMessage("checkPrint.error.noRecordFound"));  
				   
				} catch(EJBException ex) {
					
				   if(log.isInfoEnabled()) {
				   	
				      log.info("EJBException caught in ApRepCheckPrintAction.execute(): " + ex.getMessage() +
					     " session: " + session.getId());
					     
				   }
				   
				   return(mapping.findForward("cmnErrorPage"));
				}
				
				if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("apRepCheckPrintForm");

	             }
				
			    /** fill report parameters, fill report to pdf and set report session **/ 	 
  
			    boolean useBankForm = ejbCP.getAdPrfUseBankForm(user.getCmpCode());
				
				if(useBankForm) {
					
					String nullString = null;
					   
					Map parameters = new HashMap();
					parameters.put("company", adCmpDetails.getCmpName());
					
					String filename = "/opt/ofs-resources/" + user.getCompany() + "/" + baName + "/ApRepCheckPrint.jasper";
					
					if (!new java.io.File(filename).exists()) {
					
					   filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckPrint.jasper";
					   
					   if (!new java.io.File(filename).exists()) {
					   	
					   	  filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckPrint.jasper");
					   	
					   }
				       
					} 
					    	    	    	        	    				
					try {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
										new ApRepCheckPrintDS(list)));   				     
						
					} catch (Exception ex) {
						
						if(log.isInfoEnabled()) {
							
							log.info("Exception caught in ApRepCheckPrintAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							
						} 
						
						return(mapping.findForward("cmnErrorPage"));
						
					}              
					
				} else {
					
					ApModCheckDetails mDetails = (ApModCheckDetails) list.get(0);				  
					
					// create jasper design report properties
					
					// jasper design
					
					JasperDesign jasperDesign = new JasperDesign();
					jasperDesign.setName("Check Print");
					
					jasperDesign.setPageWidth(595);
					jasperDesign.setPageHeight(842);		
					jasperDesign.setColumnWidth(515);
					jasperDesign.setColumnSpacing(0);
					jasperDesign.setLeftMargin(0);
					jasperDesign.setRightMargin(0);
					jasperDesign.setTopMargin(0);
					jasperDesign.setBottomMargin(0);
					
					// fonts
					
					JRDesignReportFont normalFont = new JRDesignReportFont();
					normalFont.setName("Normal_Font");
					normalFont.setDefault(true);
					normalFont.setFontName("Arial");
					normalFont.setSize(mDetails.getChkBaFontSize());
					normalFont.setPdfFontName(mDetails.getChkBaFontStyle());
					normalFont.setPdfEncoding("Cp1252");
					normalFont.setPdfEmbedded(false);
					jasperDesign.addFont(normalFont);
					
					JRDesignReportFont boldFont = new JRDesignReportFont();
					boldFont.setName("Bold_Font");
					boldFont.setDefault(false);
					boldFont.setFontName("Arial");
					boldFont.setSize(mDetails.getChkBaFontSize());
					boldFont.setBold(true);
					boldFont.setPdfFontName(mDetails.getChkBaFontStyle());
					boldFont.setPdfEncoding("Cp1252");
					boldFont.setPdfEmbedded(false);
					jasperDesign.addFont(boldFont);
					
					JRDesignReportFont italicFont = new JRDesignReportFont();
					italicFont.setName("Italic_Font");
					italicFont.setDefault(false);
					italicFont.setFontName("Arial");
					italicFont.setSize(mDetails.getChkBaFontSize());
					italicFont.setItalic(true);
					italicFont.setPdfFontName(mDetails.getChkBaFontStyle());
					italicFont.setPdfEncoding("Cp1252");
					italicFont.setPdfEmbedded(false);
					jasperDesign.addFont(italicFont);
					
					JRDesignReportFont staticTextFont = new JRDesignReportFont();
					staticTextFont.setName("Static_Text_Font");
					staticTextFont.setDefault(true);
					staticTextFont.setFontName("Arial");
					staticTextFont.setSize(8);
					staticTextFont.setPdfFontName(mDetails.getChkBaFontStyle());
					staticTextFont.setPdfEncoding("Cp1252");
					staticTextFont.setPdfEmbedded(false);
					jasperDesign.addFont(staticTextFont);
					
					//Fields
					JRDesignField field = new JRDesignField();
					field.setName("checkNumber");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("description");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("date");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("amount");
					field.setValueClass(java.lang.Double.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("currency");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("supplier");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("address");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("bankAccount");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("bankAccountNumber");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("amountInWords");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);

					field = new JRDesignField();
					field.setName("documentNumber");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("crossCheck");
					field.setValueClass(java.lang.Boolean.class);
					jasperDesign.addField(field);
					
					field = new JRDesignField();
					field.setName("remarks");
					field.setValueClass(java.lang.String.class);
					jasperDesign.addField(field);
					
					//Variables
					JRDesignGroup group = new JRDesignGroup();
					group.setName("checkNumber");
					
					//Parameter
					JRDesignParameter parameter = new JRDesignParameter(); 
					parameter.setName("crosscheck"); 
					parameter.setValueClass(java.awt.Image.class); 
					jasperDesign.addParameter(parameter); 
					
					//Groups				
					group.setMinHeightToStartNewPage(60);
					group.setStartNewColumn(false);
					group.setStartNewPage(true);
					group.setResetPageNumber(true);
					group.setReprintHeaderOnEachPage(true);
					JRDesignExpression expression = new JRDesignExpression();
					expression.setValueClass(java.lang.String.class);
					expression.setText("$F{checkNumber}");
					group.setExpression(expression);
					
					jasperDesign.addGroup(group);
					
					JRDesignBand band = new JRDesignBand();
					
					JRDesignTextField textField = new JRDesignTextField();				
					
					// title band
					
					band = new JRDesignBand();
					jasperDesign.setTitle(band);
					
					// page header band
					
					band = new JRDesignBand();
					band.setHeight(700);
					
					if (Common.convertByteToBoolean(mDetails.getChkCrossCheck())) {
						
						// for Crossed Checks

						JRDefaultStyleProvider jRDefaultStyleProvider = null;  
						
						JRDesignImage imageField = new JRDesignImage(jRDefaultStyleProvider);
						
						imageField.setScaleImage(JRDesignImage.SCALE_IMAGE_FILL_FRAME);
						imageField.setHorizontalAlignment(JRDesignImage.HORIZONTAL_ALIGN_LEFT);
						imageField.setVerticalAlignment(JRDesignImage.VERTICAL_ALIGN_TOP);						
						imageField.setMode(JRDesignImage.MODE_TRANSPARENT);
						imageField.setEvaluationTime(JRDesignExpression.EVALUATION_TIME_NOW);
						imageField.setHyperlinkType(JRDesignImage.HYPERLINK_TYPE_NONE);
						imageField.setX(mDetails.getChkBaAccountNumberLeft() + 10);
						imageField.setY(mDetails.getChkBaAccountNumberTop() + 10);
						imageField.setWidth(75);
						imageField.setHeight(33);
						imageField.setKey("image-1");
						imageField.setPen(JRDesignImage.PEN_NONE);
						imageField.setFill(JRDesignImage.FILL_SOLID);
						imageField.setStretchType(JRDesignImage.STRETCH_TYPE_NO_STRETCH);
						imageField.setPositionType(JRDesignImage.POSITION_TYPE_FIX_RELATIVE_TO_TOP);
						imageField.setPrintRepeatedValues(true);
						imageField.setRemoveLineWhenBlank(false);
						imageField.setPrintInFirstWholeBand(false);
						imageField.setPrintWhenDetailOverflows(false);
						imageField.setForecolor(Color.WHITE);
						imageField.setBackcolor(Color.WHITE);
						expression = new JRDesignExpression();
						expression.setValueClass(java.awt.Image.class);
						expression.setText("$P{crosscheck}");
						imageField.setExpression(expression);						
						band.addElement(imageField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaAccountNumberShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaAccountNumberLeft() + 10);
						textField.setY(mDetails.getChkBaAccountNumberTop());
						textField.setWidth(150);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);                    
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{bankAccountNumber}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}					
					
					if (Common.convertByteToBoolean(mDetails.getChkBaAccountNameShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaAccountNameLeft());
						textField.setY(mDetails.getChkBaAccountNameTop());
						textField.setWidth(800);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{bankAccount}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaNumberShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaNumberLeft());
						textField.setY(mDetails.getChkBaNumberTop());
						textField.setWidth(100);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{checkNumber}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaDateShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaDateLeft());
						textField.setY(mDetails.getChkBaDateTop());
						textField.setWidth(100);					
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{date}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaPayeeShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaPayeeLeft());
						textField.setY(mDetails.getChkBaPayeeTop());
						textField.setWidth(400);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						
						// check 1st character
						if(mDetails.getChkSplName().startsWith("*"))
							expression.setText("$F{supplier}");
						else
							expression.setText("\"***\" + $F{supplier} + \"***\"");
						
						textField.setExpression(expression);
						band.addElement(textField);
						
						// remarks - FAO
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaPayeeLeft());
						textField.setY(mDetails.getChkBaPayeeTop() - 15);
						textField.setWidth(800);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{remarks}");
						textField.setExpression(expression);
						textField.setBlankWhenNull(true);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaAmountShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaAmountLeft());
						textField.setY(mDetails.getChkBaAmountTop());
						textField.setWidth(150);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						textField.setPattern("\052\052\052#,##0.00\052\052\052");
						textField.setBlankWhenNull(true);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.Double.class);
						expression.setText("$F{amount}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					
					if (Common.convertByteToBoolean(mDetails.getChkBaWordAmountShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaWordAmountLeft());
						textField.setY(mDetails.getChkBaWordAmountTop());
						textField.setWidth(600);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("\"***\" + $F{amountInWords} + \"***\"");
						textField.setExpression(expression);
						textField.setBlankWhenNull(true);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaCurrencyShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaCurrencyLeft());
						textField.setY(mDetails.getChkBaCurrencyTop());
						textField.setWidth(100);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{currency}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaAddressShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaAddressLeft());
						textField.setY(mDetails.getChkBaAddressTop());
						textField.setWidth(500);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{address}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}					
					
					if (Common.convertByteToBoolean(mDetails.getChkBaMemoShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaMemoLeft());
						textField.setY(mDetails.getChkBaMemoTop());
						textField.setWidth(500);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{description}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					
					if (Common.convertByteToBoolean(mDetails.getChkBaDocNumberShow())) {
						
						textField = new JRDesignTextField();
						textField.setX(mDetails.getChkBaDocNumberLeft());
						textField.setY(mDetails.getChkBaDocNumberTop());
						textField.setWidth(500);
						textField.setHeight(15);
						textField.setFont(normalFont);
						textField.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
						expression = new JRDesignExpression();
						expression.setValueClass(java.lang.String.class);
						expression.setText("$F{documentNumber}");
						textField.setExpression(expression);
						band.addElement(textField);
						
					}
					jasperDesign.setPageHeader(band);
					
					// column header band
					
					band = new JRDesignBand();
					jasperDesign.setColumnHeader(band);
					
					// detail band
					
					band = new JRDesignBand();														
					jasperDesign.setDetail(band);
					
					// column footer band
					
					band = new JRDesignBand();
					jasperDesign.setColumnFooter(band);
					
					// page footer band
					
					band = new JRDesignBand();
					jasperDesign.setPageFooter(band);
					
					// summary band
					
					band = new JRDesignBand();
					jasperDesign.setSummary(band);
					
					try {
						
						// fill report parameters, fill report to pdf and set report session
						
						Map parameters = new HashMap();
						
						if (Common.convertByteToBoolean(mDetails.getChkCrossCheck())) {
							
							try { 
								
								String filename = "/opt/ofs-resources/" + user.getCompany() + "/crosscheck.jpg";
							       
						        if (!new java.io.File(filename).exists()) {
						       		    		    
						           filename = servlet.getServletContext().getRealPath("images/crosscheck.jpg");
							    
						        }

								Image img = ImageIO.read(new File(filename));
								
								parameters.put("crosscheck", img);
								
								
							} catch(Exception ex) {
								
								ex.printStackTrace();
								
							}
							
							
						}

						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(
								JasperRunManager.runReportToPdf(JasperCompileManager.compileReport(jasperDesign), parameters, 
										new ApRepCheckPrintDS(list)));  
						
						
					} catch (Exception ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("checkPrint.error.compilationError"));				
						
						saveErrors(request, new ActionMessages(errors));  
						return mapping.findForward("apRepCheckPrint");
						
					}										
					
				}
				
				session.setAttribute(Constants.REPORT_KEY, report);
				
				return(mapping.findForward("apRepCheckPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("apRepCheckPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ApRepCheckPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }

	 private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
	    String soFar;
	
	    if (number % 100 < 20){
	        soFar = numNames[number % 100];
	        number /= 100;
	       }
	    else {
	        soFar = numNames[number % 10];
	        number /= 10;
	
	        soFar = tensNames[number % 10] + soFar;
	        number /= 10;
	       }
	    if (number == 0) return soFar;
	    return numNames[number] + " Hundred" + soFar;
	}
	
	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
	    /* special case */
	    if (number == 0) { return "zero"; }
	
	    String prefix = "";
	
	    if (number < 0) {
	        number = -number;
	        prefix = "Negative";
	      }
	
	    String soFar = "";
	    int place = 0;
	
	    do {
	      int n = number % 1000;
	      if (n != 0){
	         String s = this.convertLessThanOneThousand(n, tensNames, numNames);
	         soFar = s + majorNames[place] + soFar;
	        }
	      place++;
	      number /= 1000;
	      } while (number > 0);
	
	    return (prefix + soFar).trim();
	}    
    
}
