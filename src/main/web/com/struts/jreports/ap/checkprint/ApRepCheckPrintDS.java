package com.struts.jreports.ap.checkprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.purchaseorderprint.ApRepPurchaseOrderPrintData;
import com.struts.util.Common;
import com.util.ApModCheckDetails;

public class ApRepCheckPrintDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCheckPrintDS(ArrayList list){
   	
   		Iterator i = list.iterator();
	   	
	   	while (i.hasNext()) {
	   		
	   		ApModCheckDetails mdetails = (ApModCheckDetails) i.next();
	   		
	   		ApRepCheckPrintData chkData = null;
	   		
	   		if(mdetails.getChkDrDebit() == 1) {
	   			
	   			chkData = new ApRepCheckPrintData(mdetails.getChkNumber(), mdetails.getChkDescription(),
	   					Common.convertSQLDateToString(mdetails.getChkCheckDate()), new Double(mdetails.getChkAmount()),
						mdetails.getChkFcName(), mdetails.getChkSplName(), mdetails.getChkSplAddress(),
						mdetails.getChkBaName(), mdetails.getChkBaAccountNumber(), mdetails.getChkAmountInWords(),
						Common.convertSQLDateToString(mdetails.getChkDate()), mdetails.getChkDrCoaAccountDescription(),
						mdetails.getChkDrCoaAccountNumber(), new Double(mdetails.getChkDrAmount()), null,
						mdetails.getChkDocumentNumber(), mdetails.getChkCreatedBy(), mdetails.getChkApprovedRejectedBy(),
						mdetails.getChkApprovalStatus(), mdetails.getChkPosted() == (byte)1 ? "YES" : "NO",
						mdetails.getChkCreatedByDescription(), mdetails.getChkApprovedRejectedByDescription(),
						mdetails.getChkDate(), mdetails.getChkBaName(), new Boolean(Common.convertByteToBoolean(mdetails.getChkCrossCheck())),
						mdetails.getChkSplRemarks(), mdetails.getBrnhCde(), mdetails.getBrnhNm(),
						mdetails.getChkMisc1(),mdetails.getChkMisc2(),mdetails.getChkMisc3(),mdetails.getChkMisc4(),mdetails.getChkMisc5(),mdetails.getChkMisc6()
	   					);
	   			
	   			System.out.println("1 Check Date : " + Common.convertSQLDateToString(mdetails.getChkCheckDate()));
	   			System.out.println("1 Date : " + Common.convertSQLDateToString(mdetails.getChkDate()));
	   			
	   		} else {
	   			
	   			chkData = new ApRepCheckPrintData(mdetails.getChkNumber(), mdetails.getChkDescription(),
	   					Common.convertSQLDateToString(mdetails.getChkCheckDate()), new Double(mdetails.getChkAmount()),
						mdetails.getChkFcName(), mdetails.getChkSplName(), mdetails.getChkSplAddress(),
						mdetails.getChkBaName(), mdetails.getChkBaAccountNumber(), mdetails.getChkAmountInWords(),
						Common.convertSQLDateToString(mdetails.getChkDate()), mdetails.getChkDrCoaAccountDescription(),
						mdetails.getChkDrCoaAccountNumber(), null, new Double(mdetails.getChkDrAmount()),
						mdetails.getChkDocumentNumber(), mdetails.getChkCreatedBy(), mdetails.getChkApprovedRejectedBy(),
						mdetails.getChkApprovalStatus(), mdetails.getChkPosted() == (byte)1 ? "YES" : "NO",
						mdetails.getChkCreatedByDescription(), mdetails.getChkApprovedRejectedByDescription(),
						mdetails.getChkDate(), mdetails.getChkBaName(), new Boolean(Common.convertByteToBoolean(mdetails.getChkCrossCheck())),
						mdetails.getChkSplRemarks(), mdetails.getBrnhCde(), mdetails.getBrnhNm(),
						mdetails.getChkMisc1(),mdetails.getChkMisc2(),mdetails.getChkMisc3(),mdetails.getChkMisc4(),mdetails.getChkMisc5(),mdetails.getChkMisc6()
	   					);

	   			System.out.println("2 Check Date : " + Common.convertSQLDateToString(mdetails.getChkCheckDate()));
	   			System.out.println("2 Date : " + Common.convertSQLDateToString(mdetails.getChkDate()));

	   		}
	   		
		    data.add(chkData);	    	                                                         
		    	
		}
      
   }

   public boolean next() throws JRException{
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException{
   	
      Object value = null;
      
      String fieldName = field.getName();
      
      if("checkNumber".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getCheckNumber();
      }else if("description".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getDescription();
      }else if("date".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getDate();
      }else if("amount".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getAmount();
      }else if("currency".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getCurrency();
      }else if("supplier".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getSupplier();
      }else if("address".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getAddress();
      }else if("bankAccount".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getBankAccount();
      }else if("bankAccountNumber".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getBankAccountNumber();
      }else if("amountInWords".equals(fieldName)){
         value = ((ApRepCheckPrintData)data.get(index)).getAmountInWords();
      }else if("documentNumber".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getDocumentNumber();
      	System.out.println("docNumber3" + value);
      }else if("accountNumber".equals(fieldName)){
        value = ((ApRepCheckPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
        value = ((ApRepCheckPrintData)data.get(index)).getAccountDescription();
      }else if("checkDate".equals(fieldName)){
        value = ((ApRepCheckPrintData)data.get(index)).getCheckDate();       
      }else if("debitAmount".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getDebitAmount(); 
      }else if("creditAmount".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getCreditAmount();
      }else if("createdBy".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getCreatedBy();
      }else if("approvedBy".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getApprovedBy();
      }else if("approvalStatus".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getApprovalStatus();
      }else if("posted".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getPosted();
      }else if("createdByDescription".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getCreatedByDescription();
      }else if("approvedRejectedByDescription".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getApprovedRejectedByDescription();
      }else if("date2".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getDate2();    
      }else if("bankName".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getBankName();
      }else if("crossCheck".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getCrossCheck();
      }else if("remarks".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getRemarks();
      }else if("branchCode".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getBranchCode();
      }else if("branchName".equals(fieldName)){
      	value = ((ApRepCheckPrintData)data.get(index)).getBranchName();
      	System.out.println("value : "+value);
      } else if("misc1".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc1();
	   	} else if("misc2".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc2();
	   	} else if("misc3".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc3();
	   	} else if("misc4".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc4();
	   	} else if("misc5".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc5();
	   	} else if("misc6".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc6();	
      }
      
      return(value);
   }
}
