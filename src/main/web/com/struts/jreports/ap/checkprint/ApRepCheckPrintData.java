package com.struts.jreports.ap.checkprint;

import java.util.Date;

public class ApRepCheckPrintData implements java.io.Serializable {

	private String checkNumber = null;
	private String description = null;
	private String date = null;
	private Double amount = null;
	private String currency = null;
	private String supplier = null;
	private String address = null;
	private String bankAccount = null;
	private String bankAccountNumber = null;
	private String amountInWords = null;

	private String documentNumber = null;
	private String accountNumber = null;
	private String accountDescription = null;
	private String checkDate = null; 
	private Double debitAmount = null;
	private Double creditAmount = null;
	private String createdBy = null;
	private String approvedBy = null;
	private String approvalStatus = null;
	private String posted = null;
	private String createdByDescription = null;
	private String approvedRejectedByDescription = null;
	private Date date2	= null;
	private String bankName = null;
	private Boolean crossCheck = null;
	private String remarks = null;

	private String branchCode = null;
	private String branchName = null;
	
	private String misc1 = null;
	private String misc2 = null;
	private String misc3 = null;
	private String misc4 = null;
	private String misc5 = null;
	private String misc6 = null;

	public ApRepCheckPrintData(
			String checkNumber, String description, String date, Double amount, String currency, String supplier, 
			String address, String bankAccount, String bankAccountNumber, String amountInWords, 
			String checkDate, String accountDescription, String  accountNumber, Double debitAmount, Double creditAmount,
			String documentNumber, String createdBy, String approvedBy, String approvalStatus, String posted, 
			String createdByDescription, String approvedRejectedByDescription, Date date2, String bankName, Boolean crossCheck, 
			String remarks, String branchCode, String branchName,
			String misc1, String misc2, String misc3,
			String misc4, String misc5, String mis6) {

		this.checkNumber = checkNumber;
		this.description = description;
		this.date = date;
		this.amount = amount;
		this.currency = currency;
		this.supplier = supplier;
		this.address = address;      	
		this.bankAccount = bankAccount;
		this.bankAccountNumber = bankAccountNumber;
		this.amountInWords = amountInWords;
		this.checkDate = checkDate;
		this.accountDescription = accountDescription;
		this.accountNumber = accountNumber;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.documentNumber = documentNumber;
		this.createdBy = createdBy;
		this.approvedBy = approvedBy;
		this.approvalStatus = approvalStatus;
		this.posted = posted;
		this.createdByDescription = createdByDescription;
		this.approvedRejectedByDescription = approvedRejectedByDescription;
		this.date2 = date2;
		this.bankName = bankName;
		this.crossCheck = crossCheck;
		this.remarks = remarks;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.misc1 = misc1;
		this.misc2 = misc2;
		this.misc3 = misc3;
		this.misc4 = misc4;
		this.misc5 = misc5;
		this.misc6 = misc6;

	}

	public String getCheckNumber() {

		return checkNumber;

	}      

	public String getDescription() {

		return description;

	}

	public String getDate() {

		return date;

	}

	public Double getAmount() {

		return amount;

	}

	public String getCurrency() {

		return currency;

	}

	public String getSupplier() {

		return supplier;

	}

	public String getAddress() {

		return address;

	}

	public String getBankAccount() {

		return bankAccount;

	}

	public String getBankAccountNumber() {

		return bankAccountNumber;

	}

	public String getAmountInWords() {

		return amountInWords;

	}

	public String getCheckDate() {

		return checkDate;

	}

	public String getAccountDescription() {

		return accountDescription;

	}

	public String getAccountNumber() {

		return accountNumber;

	}

	public Double getDebitAmount() {

		return debitAmount;

	}

	public Double getCreditAmount() {

		return creditAmount;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getCreatedBy() {

		return createdBy;

	}

	public String getApprovedBy() {

		return approvedBy;

	}

	public String getBankName() {

		return bankName;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public String getPosted() {

		return posted; 

	}

	public String getCreatedByDescription() {

		return createdByDescription;

	}

	public String getApprovedRejectedByDescription() {

		return approvedRejectedByDescription;

	}

	public Date getDate2() {

		return date2;

	}

	public Boolean getCrossCheck() {

		return crossCheck;

	}

	public String getRemarks() {

		return remarks;

	}

	public String getBranchCode() {

		return branchCode;

	}      

	public String getBranchName() {

		return branchName;

	}
	
	public String getMisc1() {
		
		return misc1;
		
	}

	public String getMisc2() {
		
		return misc2;
		
	}
	
	public String getMisc3() {
		
		return misc3;
		
	}
	
	public String getMisc4() {
			
			return misc4;
			
		}
	
	public String getMisc5() {
		
		return misc5;
		
	}
	
	public String getMisc6() {
		
		return misc6;
		
	}

}
