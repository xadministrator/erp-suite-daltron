package com.struts.jreports.ap.checkregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepCheckRegisterController;
import com.ejb.txn.ApRepCheckRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepCheckRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepCheckRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepCheckRegisterForm actionForm = (ApRepCheckRegisterForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AP_REP_CHECK_REGISTER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRepCheckRegister");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApRepCheckRegisterController EJB
*******************************************************/

         ApRepCheckRegisterControllerHome homeCR = null;
         ApRepCheckRegisterController ejbCR = null;       

         try {
         	
            homeCR = (ApRepCheckRegisterControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepCheckRegisterControllerEJB", ApRepCheckRegisterControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepCheckRegisterAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCR = homeCR.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepCheckRegisterAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP CR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

                HashMap criteria = new HashMap();            	

                if (!Common.validateRequired(actionForm.getSupplierCode())) {

                        criteria.put("supplierCode", actionForm.getSupplierCode());

                }

                if (!Common.validateRequired(actionForm.getBankAccount())) {

                        criteria.put("bankAccount", actionForm.getBankAccount());
                }

                if (!Common.validateRequired(actionForm.getType())) {

                        criteria.put("supplierType", actionForm.getType());
                }

                if (!Common.validateRequired(actionForm.getSupplierClass())) {

                        criteria.put("supplierClass", actionForm.getSupplierClass());
                }	        		        	

                if (!Common.validateRequired(actionForm.getDateFrom())) {

                        criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

                }

                if (!Common.validateRequired(actionForm.getDateTo())) {

                        criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

                }

                if (!Common.validateRequired(actionForm.getCheckNumberFrom())) {

                        criteria.put("checkNumberFrom", actionForm.getCheckNumberFrom());

                }

                if (!Common.validateRequired(actionForm.getCheckNumberTo())) {

                        criteria.put("checkNumberTo", actionForm.getCheckNumberTo());

                }

                if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {

                        criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());

                }

                if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {

                        criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());

                }

                if (!Common.validateRequired(actionForm.getReferenceNumber())) {

                        criteria.put("referenceNumber", actionForm.getReferenceNumber());

                }

                if (actionForm.getIncludedUnposted()) {	        	

                        criteria.put("includedUnposted", "YES");

                }

                if (!actionForm.getIncludedUnposted()) {

                        criteria.put("includedUnposted", "NO");

                }



                if (actionForm.getDirectCheckOnly()){
                    criteria.put("directCheckOnly", "YES");
                }

                if (actionForm.getReportType().contains("Inscribe Stocks")){
                    criteria.put("inscribeStock", "YES");
                }

                if (actionForm.getReportType().contains("Treasury Bills")){
                    criteria.put("treasuryBill", "YES");
                }





                // save criteria

                actionForm.setCriteria(criteria);
	        	
            }
            
            // branch map
            
            ArrayList adBrnchList = new ArrayList();
            // ArrayList adBrnchList2 = new ArrayList();
            
            for(int j=0; j < actionForm.getApRepBrChckRgstrListSize(); j++) {
                
                ApRepBranchCheckRegisterList apRepBrChckRgstrList = (ApRepBranchCheckRegisterList)actionForm.getApRepBrChckRgstrByIndex(j);
                
                if(apRepBrChckRgstrList.getBranchCheckbox() == true) {
                    
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(apRepBrChckRgstrList.getBrCode());
                    adBrnchList.add(mdetails);
                    
                }
                
            }
            
            
            String reportTitle = "Check Register";
            ArrayList bankAccountList = new ArrayList();
            
            for(int j=0; j < actionForm.getBankAccountSelectedList().length; j++) {
                
                String bankAccount = actionForm.getBankAccountSelectedList()[j];
                if(!bankAccount.equals("")){
                    bankAccountList.add(bankAccount); 
                }
  
            }
            
            //change report title but it depends on the report
            
            if(bankAccountList.size()==1){
                String bankAccount = (String)bankAccountList.get(0);
                
                if(bankAccount.contains("PETTY CASH")){
                    reportTitle = "Petty Cash Register";
                }
                
            }
            
            try {

               // get company

               AdCompanyDetails adCmpDetails = ejbCR.getAdCompany(user.getCmpCode());
               company = adCmpDetails.getCmpName();

               // execute report

               list = ejbCR.executeApRepCheckRegister(actionForm.getCriteria(),
                actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowEntries(), actionForm.getSummarize(), actionForm.getShowPdcOnly(), 
                actionForm.getDebitFirst(), actionForm.getIncludeVoidCheck(), actionForm.getCurrency(), actionForm.getAccountName(), bankAccountList, adBrnchList, user.getCmpCode());

               
               System.out.println("size list is : " + list.size());
               
               if(list.size()<=0){
                   throw new GlobalNoRecordFoundException();
               }
               
            } catch (GlobalNoRecordFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
             new ActionMessage("apCheckRegister.error.noRecordFound"));

            } catch(EJBException ex) {

                 if(log.isInfoEnabled()) {
                            log.info("EJBException caught in ApRepCheckRegisterAction.execute(): " + ex.getMessage() +
                            " session: " + session.getId());

                        }

                        return(mapping.findForward("cmnErrorPage"));

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRepCheckRegister");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
                    
                    parameters.put("reportTitle", reportTitle);
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
                    parameters.put("printedBy", user.getUserName());
                    parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
                    parameters.put("viewType", actionForm.getViewType());
			
                    
                    if (actionForm.getSupplierCode() != null) {

                            parameters.put("supplierCode", actionForm.getSupplierCode());

                    }

                    if (actionForm.getBankAccount() != null) {

                            parameters.put("bankAccount", actionForm.getBankAccount());

                    }
			
                    if (actionForm.getType() != null) {

                            parameters.put("supplierType", actionForm.getType());

                    }

                    if (actionForm.getSupplierClass() != null) {

                            parameters.put("supplierClass", actionForm.getSupplierClass());	

                    }
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
			
		    }

			parameters.put("checkNumberFrom", actionForm.getCheckNumberFrom());
		    parameters.put("checkNumberTo", actionForm.getCheckNumberTo());	 
			parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
		    parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
		    
		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }
		    
		    if (actionForm.getShowEntries()) {
		    	
		    	parameters.put("showEntries", "YES");
		    	
		    } else {
		    	
		    	parameters.put("showEntries", "NO");
		    	
		    }
		    
		    if (actionForm.getShowGroupTotalOnly()) {
		    	
		    	parameters.put("showGroupTotalOnly", "YES");
		    	
		    } else {
		    	
		    	parameters.put("showGroupTotalOnly", "NO");
		    	
		    }
		    
		    if (actionForm.getDebitFirst()) {
		    	
		    	parameters.put("debitFirst", "YES");
		    	
		    } else {
		    	
		    	parameters.put("debitFirst", "NO");
		    	
		    }
		    
		    parameters.put("groupBy", actionForm.getGroupBy());
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getApRepBrChckRgstrListSize(); j++) {

		    	ApRepBranchCheckRegisterList brList = (ApRepBranchCheckRegisterList)actionForm.getApRepBrChckRgstrByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBrName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBrName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = null;
		    
		    if (actionForm.getShowEntries()) {
		    	
		    	if (actionForm.getSummarize()) {
		    		
		    		filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckRegisterSummary.jasper");
		    		
		    	} else {
		    		
		    		filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckRegisterEntries.jasper";
			    	
			    	if (!new java.io.File(filename).exists()) {
			    		
			    		filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckRegisterEntries.jasper");
			    		
			    	}
			    	
		    	}
		    			    	
		    } else {
		    	
                        if(actionForm.getReportType().equals("Investor Incscribe Stocks")){
                            filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepDirectCheckInscribeStock.jasper";
		    	
                            if (!new java.io.File(filename).exists()) {

                                    filename = servlet.getServletContext().getRealPath("jreports/ApRepDirectCheckInscribeStock.jasper");

                            }
                            
                        }else if(actionForm.getReportType().equals("Investor Treasury Bills")){
                            
                            filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepDirectCheckTreasureyBill.jasper";
		    	
                            if (!new java.io.File(filename).exists()) {

                                    filename = servlet.getServletContext().getRealPath("jreports/ApRepDirectCheckTreasureyBill.jasper");

                            }
                            
                        }else{
                            filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckRegister.jasper";
		    	
                            if (!new java.io.File(filename).exists()) {

                                    filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckRegister.jasper");

                            }
                        }
		    	
		    	
		    }
    		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepCheckRegisterDS(list,actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ApRepCheckRegisterDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ApRepCheckRegisterDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ApRepCheckRegisterAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP CR Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP CR Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	             	             
	             try {
	                
	             	 actionForm.reset(mapping, request);
	             	
	                 ArrayList list = null;
	                 Iterator i = null;
	                 
	                 actionForm.clearApRepBrChckRgstrList();
	                 
	                 list = ejbCR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	                 
	                 i = list.iterator();
	                 
	                 while(i.hasNext()) {
	                     
	                     AdBranchDetails details = (AdBranchDetails)i.next();
	                     
	                     ApRepBranchCheckRegisterList apRepBrChckRgstrList = new ApRepBranchCheckRegisterList(actionForm,
	                             details.getBrCode(),
	                            details.getBrBranchCode(), details.getBrName());
	                     apRepBrChckRgstrList.setBranchCheckbox(true);
	                     
	                     actionForm.saveApRepBrChckRgstrList(apRepBrChckRgstrList);
	                     
	                 }
	                 
	             } catch (GlobalNoRecordFoundException ex) {
	                 
	             } catch (EJBException ex) {
	                 
	                 if (log.isInfoEnabled()) {
	                     
	                     log.info("EJBException caught in ApRepCheckRegisterAction.execute(): " + ex.getMessage() +
	                             " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                 }
	                 
	             } 
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;

	            	actionForm.clearBankAccountList();           	
	            	
	            	list = ejbCR.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}
			   
                        actionForm.clearAccountNameList();     
                        
                        list = ejbCR.getEnableGlCoa(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
                            actionForm.setAccountNameList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
                            i = list.iterator();

                            while (i.hasNext()) {

                                actionForm.setAccountNameList((String)i.next());

                            }
	            		
	            	}
                        
   
	            	actionForm.clearTypeList();           	
	            	
	            	list = ejbCR.getApStAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearSupplierClassList();           	
	            	
	            	list = ejbCR.getApScAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ApRepCheckRegisterAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("apRepCheckRegister"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepCheckRegisterAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
