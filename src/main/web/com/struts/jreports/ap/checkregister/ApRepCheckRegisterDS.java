package com.struts.jreports.ap.checkregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.aging.ApRepAgingData;
import com.struts.util.Common;
import com.util.ApRepCheckRegisterDetails;

public class ApRepCheckRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCheckRegisterDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	ApRepCheckRegisterDetails details = (ApRepCheckRegisterDetails)i.next();
             
         String group = null;
         String groupCoa = null;
         
         if (groupBy.equals("SUPPLIER CODE")) {
         	
         	group = details.getCrSplSupplierCode();
         	
         } else if (groupBy.equals("SUPPLIER TYPE")) {
         	
         	group = details.getCrSplSupplierType();
         	
         } else if (groupBy.equals("SUPPLIER CLASS")) {
         	
         	group = details.getCrSplSupplierClass();
         	
         }
         
         groupCoa = details.getCrDrGlCoaAccountNumber();
         
         ApRepCheckRegisterData argData = new ApRepCheckRegisterData(details.getCrDate(), 
	   	     details.getCrSplSupplierCode(), details.getCrDescription(), 
	   		 details.getCrCheckNumber(), details.getCrReferenceNumber(), 
	   		 new Double(details.getCrAmount()), group, details.getCrDrGlCoaAccountNumber(),
	   		 details.getCrDrGlCoaAccountDescription(), new Double(details.getCrDrDebit()), 
	   		 new Double(details.getCrDrCredit()), details.getCrDocumentNumber(), details.getCrSplSupplierCode2(),
			 details.getCrSplName(), details.getCrSplSupplierClass(), details.getCrCheckDate(), groupCoa, 
			 details.getCrBankAccount(), details.getCrCheckVoid(), details.getCrCheckVoidPosted(),
                         Common.convertCharToString(details.getCrFcSymbol()),
                        details.getCrInvestInscribeStock(), details.getCrInvestTreasuryBills(),details.getCrInvestSettlementDate(),details.getCrInvestMaturityDate(),
                        details.getCrInvestNextRunDate(), details.getCrInvestNumberOfDays(), details.getCrInvestBidYield(),details.getCrInvestCouponRate(),
                        details.getCrInvestSettlementAmount(), details.getCrInvestFaceValue(), details.getCrInvestPremAmount());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("checkDate".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getCheckDate();       
   	}else if("supplierCode".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getSupplierCode();
   	}else if("description".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getDescription();
   	}else if("checkNumber".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getCheckNumber();
   	}else if("referenceNumber".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getReferenceNumber();
   	}else if("amount".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getAmount();  
   	}else if("groupBy".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getGroupBy();
   	}else if("glCoaAccountNumber".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getGlCoaAccountNumber();
   	}else if("glCoaAccountDescription".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getGlCoaAccountDescription();
   	}else if("debit".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getDebit();
   	}else if("credit".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getCredit();
   	}else if("documentNumber".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getDocumentNumber();
   	}else if("supplierCode2".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getSupplierCode2();
   	}else if("supplierName".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getSupplierName();
   	}else if("supplierClass".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getSupplierClass();
   	}else if("groupCoa".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getGroupByCoa();
   	}else if("chkCheckDate".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getChkCheckDate();
   	}else if("bankAccount".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getBankAccount();
   	}else if("checkVoid".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getCheckVoid();
   	}else if("checkVoidPosted".equals(fieldName)){
   		value = ((ApRepCheckRegisterData)data.get(index)).getCheckVoidPosted();
   	}else if("currencySymbol".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getCurrencySymbol();
        
        }else if("inscribeStock".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getInscribeStock();
        }else if("treasuryBills".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getTreasuryBills();
        }else if("settlementDate".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getSettlementDate();
        }else if("maturityDate".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getMaturityDate();
        }else if("nextRunDate".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getNextRunDate();
        }else if("numberOfDays".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getNumberOfDays();
        }else if("bidYield".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getBidYield();
        }else if("couponRate".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getCouponRate();
        }else if("settlementAmount".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getSettlementAmount();
        }else if("faceValue".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getFaceValue();
        }else if("premAmount".equals(fieldName)){
                value = ((ApRepCheckRegisterData)data.get(index)).getPremAmount();
       
        
    }
   	   	
   	return(value);
   }
}
