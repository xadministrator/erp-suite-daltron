package com.struts.jreports.ap.checkregister;


public class ApRepCheckRegisterData implements java.io.Serializable{

	private java.util.Date checkDate = null;
	private String supplierCode = null;
	private String description = null;
	private String checkNumber = null;
	private String referenceNumber = null;
	private Double amount = null;
	private String groupBy = null;
	private String glCoaAccountNumber = null;
	private String glCoaAccountDescription = null;
	private Double debit;
	private Double credit;
	private String documentNumber = null;
	private String supplierCode2 = null;
	private String supplierName = null;
	private String supplierClass = null;
	private String groupByCoa = null;
	private String bankAccount = null;
	private Byte checkVoid = null;
	private Byte checkVoidPosted = null;
	private java.util.Date chkCheckDate = null;
	private String currencySymbol = null;
        
      
        private Byte inscribeStock = null;
        private Byte treasuryBills = null; 
        private java.util.Date settlementDate = null;
        private java.util.Date maturityDate = null;
        private java.util.Date nextRunDate = null;
        private Double numberOfDays = null;
        private Double bidYield = null;
        private Double couponRate = null;
        private Double settlementAmount = null;
        private Double faceValue = null;
        private Double premAmount = null;

                
        
        
        
	
	public ApRepCheckRegisterData(java.util.Date checkDate,  String supplierCode, String description,
			String checkNumber, String referenceNumber,  Double amount, String groupBy,
			String glCoaAccountNumber, String glCoaAccountDescription, Double debit, Double credit, String documentNumber,
			String supplierCode2, String supplierName, String supplierClass, java.util.Date chkCheckDate, String groupByCoa,
			String bankAccount, Byte checkVoid, Byte checkVoidPosted, String currencySymbol,
                        Byte inscribeStock, Byte treasuryBills, java.util.Date settlementDate, java.util.Date maturityDate, java.util.Date nextRunDate, Double numberOfDays, Double bidYield, Double couponRate,
                        Double settlementAmount, Double faceValue, Double premAmount) {

		this.checkDate = checkDate;
		this.supplierCode = supplierCode;
		this.description = description;
		this.checkNumber = checkNumber;
		this.referenceNumber = referenceNumber;
		this.amount = amount;
		this.groupBy = groupBy;
		this.glCoaAccountDescription = glCoaAccountDescription;
		this.glCoaAccountNumber = glCoaAccountNumber;
		this.debit = debit;
		this.credit = credit;
		this.documentNumber = documentNumber; 
		this.supplierCode2 = supplierCode2;
		this.supplierName = supplierName;
		this.supplierClass = supplierClass;
		this.groupByCoa = groupByCoa;
		this.chkCheckDate = chkCheckDate;
		this.bankAccount = bankAccount;
		this.checkVoid = checkVoid;
		this.checkVoidPosted = checkVoidPosted;
		this.currencySymbol = currencySymbol;
                
                this.inscribeStock = inscribeStock;
                this.treasuryBills = treasuryBills;
                this.settlementDate = settlementDate;
                this.maturityDate = maturityDate;
                this.nextRunDate = nextRunDate;
                this.numberOfDays = numberOfDays;
                this.bidYield = bidYield;
                this.couponRate = couponRate;
                this.settlementAmount = settlementAmount;
                this.faceValue = faceValue;
                this.premAmount = premAmount;


	}

	public java.util.Date getCheckDate() {

		return checkDate;

	}

	public String getSupplierCode() {

		return supplierCode;

	}

	public String getDescription() {

		return description;

	}

	public String getCheckNumber() {

		return checkNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public Double getAmount() {

		return amount;

	}


	public String getGroupBy() {

		return groupBy;

	}

	public String getGlCoaAccountDescription() {

		return glCoaAccountDescription;

	}

	public String getGlCoaAccountNumber() {

		return glCoaAccountNumber;

	}

	public Double getDebit() {

		return debit;

	}

	public Double getCredit() {

		return credit;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getSupplierCode2() {

		return supplierCode2;

	}

	public String getSupplierName() {

		return supplierName;

	}

	public String getSupplierClass() {

		return supplierClass;

	}

	public String getGroupByCoa() {

		return groupByCoa;

	}

	public String getBankAccount() {

		return bankAccount;

	}
	
	public Byte getCheckVoid() {

		return checkVoid;

	}
	
	public Byte getCheckVoidPosted() {

		return checkVoidPosted;

	}
	
	public java.util.Date getChkCheckDate() {

		return chkCheckDate;

	}
	
	public String getCurrencySymbol() {

		   return currencySymbol;

        }
        
        public Byte getInscribeStock(){
            return inscribeStock;
        }
        
        public Byte getTreasuryBills(){
            return treasuryBills;
        }
        
        public java.util.Date getSettlementDate(){
            return settlementDate;
        }
        
        public java.util.Date getMaturityDate(){
            return maturityDate;
        }
        
         public java.util.Date getNextRunDate(){
            return nextRunDate;
        }
         
        public Double getNumberOfDays(){
            return numberOfDays;
        }
        
        public Double getBidYield(){
            return bidYield;
        }
        
        public Double getCouponRate(){
            return couponRate;
        }
        
        public Double getSettlementAmount(){
            return settlementAmount;
        }
        
        public Double getFaceValue(){
            return faceValue;
        }
        
        public Double getPremAmount(){
            return premAmount;
        }
    
         

}
