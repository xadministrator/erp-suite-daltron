package com.struts.jreports.ap.checkregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepCheckRegisterForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String bankAccount = null;
   private String[] bankAccountSelectedList = new String[0];
   private ArrayList bankAccountList = new ArrayList();   
   private String accountName = null;
   private ArrayList accountNameList = new ArrayList();
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String checkNumberFrom = null;
   private String checkNumberTo = null;   
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private boolean includedUnposted = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String groupBy = null;
   private boolean showEntries = false;
   private boolean showGroupTotalOnly = false;
   private boolean debitFirst = false;
   private boolean summarize = false;
   private boolean showPdcOnly = false;
   private boolean directCheckOnly = false;
   private String includeVoidCheck = null;
   private ArrayList includeVoidCheckList = new ArrayList(); 
   private String referenceNumber = null;
   private ArrayList groupByList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String reportType = null;
   private ArrayList reportTypeList = new ArrayList();
	
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList apRepBrChckRgstrList = new ArrayList();
   
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }

   public String getCheckNumberFrom(){
      return(checkNumberFrom);
   }

   public void setCheckNumberFrom(String checkNumberFrom){
      this.checkNumberFrom = checkNumberFrom;
   }

   public String getCheckNumberTo(){
      return(checkNumberTo);
   }

   public void setCheckNumberTo(String checkNumberTo){
      this.checkNumberTo = checkNumberTo;
   }

   public String getDocumentNumberFrom(){
      return(documentNumberFrom);
   }

   public void setDocumentNumberFrom(String documentNumberFrom){
      this.documentNumberFrom = documentNumberFrom;
   }

   public String getDocumentNumberTo(){
      return(documentNumberTo);
   }

   public void setDocumentNumberTo(String documentNumberTo){
      this.documentNumberTo = documentNumberTo;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

    public String[] getBankAccountSelectedList() {

        return bankAccountSelectedList;

    }

    public void setBankAccountSelectedList(String[] bankAccountSelectedList) {

        this.bankAccountSelectedList = bankAccountSelectedList;

    }
   
   public String getBankAccount(){
      return(bankAccount);
   }

   public void setBankAccount(String bankAccount){
      this.bankAccount = bankAccount;
   }
   
   public String getReferenceNumber(){
	      return(referenceNumber);
   }

   public void setReferenceNumber(String referenceNumber){
	      this.referenceNumber = referenceNumber;
   }


    public String getAccountName(){
        return accountName;
    }
    
    public void setAccountName(String accountName){
        this.accountName = accountName;
    }
   
   
    public ArrayList getAccountNameList(){
      return(accountNameList);
   }

   public void setAccountNameList(String accountName){
      accountNameList.add(accountName);
   }

   public void clearAccountNameList(){
      accountNameList.clear();
      accountNameList.add(Constants.GLOBAL_BLANK);
   }
   
   
   
   public ArrayList getBankAccountList(){
      return(bankAccountList);
   }

   public void setBankAccountList(String bankAccount){
      bankAccountList.add(bankAccount);
   }

   public void clearBankAccountList(){
      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
   }

   public String getType(){
      return(type);
   }

   public void setType(String type){
      this.type = type;
   }

   public ArrayList getTypeList(){
      return(typeList);
   }

   public void setTypeList(String type){
      typeList.add(type);
   }

   public void clearTypeList(){
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
   }
 
   public String getSupplierClass(){
      return(supplierClass);
   }

   public void setSupplierClass(String supplierClass){
      this.supplierClass = supplierClass;
   }

   public ArrayList getSupplierClassList(){
      return(supplierClassList);
   }

   public void setSupplierClassList(String supplierClass){
      supplierClassList.add(supplierClass);
   }

   public void clearSupplierClassList(){
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
   } 

   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
  
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }
   
   public String getIncludeVoidCheck() {
	   	
	   	  return includeVoidCheck;
	   	  
	   }
	   
   public void setIncludeVoidCheck(String includeVoidCheck) {
	   	
	   	  this.includeVoidCheck = includeVoidCheck;
	   	  
   }
	  
   public ArrayList getIncludeVoidCheckList() {
	   	
	   	  return includeVoidCheckList;
	   	  
   }
 
   public boolean getShowEntries() {
   	
   	  return showEntries;
   	
   }
   
   public void setShowEntries(boolean showEntries) {
	   	
   	  this.showEntries = showEntries;
   	
   }
   
   public boolean getShowGroupTotalOnly() {
	   	
	   return showGroupTotalOnly;
	   	
   }
   
   public void setShowGroupTotalOnly(boolean showGroupTotalOnly) {
	   
	   this.showGroupTotalOnly = showGroupTotalOnly;
	   
   }
   
   public boolean getDebitFirst() {
	   	
	   return debitFirst;
	   	
   }
   
   public void setDebitFirst(boolean debitFirst) {
	   
	   this.debitFirst = debitFirst;
	   
   }

   
   public Object[] getApRepBrChckRgstrList(){
       
       return apRepBrChckRgstrList.toArray();
       
   }
   
   public ApRepBranchCheckRegisterList getApRepBrChckRgstrByIndex(int index){
       
       return ((ApRepBranchCheckRegisterList)apRepBrChckRgstrList.get(index));
       
   }
   
   public int getApRepBrChckRgstrListSize(){
       
       return(apRepBrChckRgstrList.size());
       
   }
   
   public void saveApRepBrChckRgstrList(Object newApRepBrChckRgstrList){
       
       apRepBrChckRgstrList.add(newApRepBrChckRgstrList);   	  
       
   }
   
   public void clearApRepBrChckRgstrList(){
       
       apRepBrChckRgstrList.clear();
       
   }
   
   public void setApRepBrChckRgstrList(ArrayList apRepBrChckRgstrList) {
       
       this.apRepBrChckRgstrList = apRepBrChckRgstrList;
       
   }
   
   public boolean getSummarize() {
   	
   	  return summarize;
   	
   }
   
   public void setSummarize(boolean summarize) {
   	
   	  this.summarize = summarize;
   	
   }
   
   public boolean getShowPdcOnly() {
	   
	   return showPdcOnly;
	   
   }

   public void setShowPdcOnly(boolean showPdcOnly) {

	   this.showPdcOnly = showPdcOnly;

   }
   
    public boolean getDirectCheckOnly() {
	   
	   return directCheckOnly;
	   
   }

   public void setDirectCheckOnly(boolean directCheckOnly) {

	   this.directCheckOnly = directCheckOnly;

   }
   
   public String getCurrency() {

		return currency;   	

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}
        
        public String getReportType() {

		return reportType;

	}
        
        
        public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return reportTypeList;

	}
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<apRepBrChckRgstrList.size(); i++) {
           
           ApRepBranchCheckRegisterList  list = (ApRepBranchCheckRegisterList)apRepBrChckRgstrList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
      goButton = null;
      closeButton = null;
      supplierCode = null;
      supplierClass = Constants.GLOBAL_BLANK; 
      type = Constants.GLOBAL_BLANK;
      bankAccount = Constants.GLOBAL_BLANK;
      bankAccountSelectedList = new String[0];
      accountName = null;
      referenceNumber = Constants.GLOBAL_BLANK;
      dateFrom = null;
      dateTo = null;
      checkNumberFrom = null;
      checkNumberTo = null;      
      documentNumberFrom = null;
      documentNumberTo = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      orderByList.clear();
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_DATE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_BANK_ACCOUNT);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_CODE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_TYPE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_CHECK_NUMBER);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_DOCUMENT_NUMBER);
      orderBy = Constants.AP_CHECK_REGISTER_ORDER_BY_DATE;
      groupByList.clear();
      groupByList.add(Constants.GLOBAL_BLANK);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS);
      currencyList.clear();
      currencyList.add(Constants.GLOBAL_BLANK);
      currencyList.add("PHP");
      currencyList.add("USD");
      
      reportTypeList.clear();
      reportTypeList.add(Constants.GLOBAL_BLANK);
      reportTypeList.add("Investor Incscribe Stocks");
      reportTypeList.add("Investor Treasury Bills");
        includedUnposted = false;
        showEntries = false;
        showGroupTotalOnly = false;
        summarize = false;
        showPdcOnly = false;
        directCheckOnly= false;
        includeVoidCheckList.clear();
      includeVoidCheckList.add(Constants.GLOBAL_BLANK);
      includeVoidCheckList.add("All with Void Checks");
      includeVoidCheckList.add("Void Checks Only");
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
            errors.add("dateFrom", new ActionMessage("apCheckRegister.error.dateFromInvalid"));
         }
         if(!Common.validateDateFormat(dateTo)){
            errors.add("dateTo", new ActionMessage("apCheckRegister.error.dateToInvalid"));
         }
         if(!Common.validateStringExists(bankAccountList, bankAccount)){
            errors.add("bankAccount", new ActionMessage("apCheckRegister.error.bankAccountInvalid"));
         }		 		 		 
         if(!Common.validateStringExists(typeList, type)){
            errors.add("type", new ActionMessage("apCheckRegister.error.typeInvalid"));
         }
         if(!Common.validateStringExists(supplierClassList, supplierClass)){
            errors.add("supplierClass", new ActionMessage("apCheckRegister.error.supplierClassInvalid"));
         }	 
}
      return(errors);
   }
}
