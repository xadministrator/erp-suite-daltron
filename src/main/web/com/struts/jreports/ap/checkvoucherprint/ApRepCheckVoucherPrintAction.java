package com.struts.jreports.ap.checkvoucherprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepCheckVoucherPrintController;
import com.ejb.txn.ApRepCheckVoucherPrintControllerHome;
import com.struts.jreports.ap.apregister.ApRepApRegisterDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckVoucherPrintDetails;

public final class ApRepCheckVoucherPrintAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {

		HttpSession session = request.getSession();

		try {

			/*******************************************************
   Check if user has a session
			 *******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("ApRepCheckVoucherPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());

				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			ApRepCheckVoucherPrintForm actionForm = (ApRepCheckVoucherPrintForm)form;

			String frParam= Common.getUserPermission(user, Constants.AP_REP_CHECK_VOUCHER_PRINT_ID);

			if (frParam != null) {

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);
			}

			/*******************************************************
   Initialize ApRepCheckVoucherPrintController EJB
			 *******************************************************/

			ApRepCheckVoucherPrintControllerHome homeCVP = null;
			ApRepCheckVoucherPrintController ejbCVP = null;       

			try {

				homeCVP = (ApRepCheckVoucherPrintControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ApRepCheckVoucherPrintControllerEJB", ApRepCheckVoucherPrintControllerHome.class);

			} catch(NamingException e) {

				if(log.isInfoEnabled()) {

					log.info("NamingException caught in ApRepCheckVoucherPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			try {

				ejbCVP = homeCVP.create();

			} catch(CreateException e) {

				if(log.isInfoEnabled()) {

					log.info("CreateException caught in ApRepCheckVoucherPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			ActionErrors errors = new ActionErrors();

			/*** get report session and if not null set it to null **/

			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);

			if(reportSession != null) {

				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);

			}

			/*******************************************************
   -- Close Action --
			 *******************************************************/

			if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

			}  

			/*******************************************************
   -- TO DO getChkByChkCode() --
			 *******************************************************/       

			if (frParam != null) {
				double TOTAL_AMOUNT = 0d;
				String TOTAL_AMOUNT_WORDS = "";
				final String[] majorNames = {"", " Thousand", " Million",
						" Billion", " Trillion", " Quadrillion", " Quintillion"};

				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
						" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};

				final String[] numNames = {"", " One", " Two", " Three", " Four",
						" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
						" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};

				AdCompanyDetails adCmpDetails = null;
				String baName = null;
				ArrayList list = null;
				ArrayList listSub = null;
				ApRepCheckVoucherPrintDetails details = null;

				try {

					ArrayList chkCodeList = new ArrayList();

					if (request.getParameter("forward") != null) {

						chkCodeList.add(new Integer(request.getParameter("checkCode")));

					} else {

						int i = 0;

						while (true) {

							if (request.getParameter("checkCode[" + i + "]") != null) {

								chkCodeList.add(new Integer(request.getParameter("checkCode[" + i + "]")));

								i++;

							} else {

								break;

							}

						}												

					}



					list = ejbCVP.executeApRepCheckVoucherPrint(chkCodeList, user.getCmpCode()); 

					try {

						listSub = ejbCVP.executeApRepCheckVoucherPrintSub(chkCodeList, user.getCmpCode());

					} catch (GlobalNoRecordFoundException ex) {

					}

					// get company

					adCmpDetails = ejbCVP.getAdCompany(user.getCmpCode());
				
					Iterator i = list.iterator();

					while (i.hasNext()) {
					
						details = (ApRepCheckVoucherPrintDetails)i.next();

						if(baName == null) {

							baName = details.getCvChkAdBaName();

						}
						TOTAL_AMOUNT+=details.getCvChkDrDebit()==0?details.getCvChkDrAmount():0d;
						int num = (int)Math.floor(details.getCvChkAmount());
						System.out.println("amount is pasok" + num);
						String str = Common.convertDoubleToStringMoney(details.getCvChkAmount(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());

						details.setCvChkAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
								str + "/100 Only");	

                                                System.out.println("SUPPLIER NAME: " + new String(request.getParameter("supplierName")));
						/*if (request.getParameter("forward") != null) {

							// set supplier name
							String supplierName = new String(request.getParameter("supplierName"));
							details.setCvChkSplName(supplierName);

						}*/

					}
					

					int num = (int)Math.floor(TOTAL_AMOUNT);
					System.out.println("amount is pasok" + num);
					String str = Common.convertDoubleToStringMoney(details.getCvChkAmount(), (short)2);
					str = str.substring(str.indexOf('.') + 1, str.length());
					TOTAL_AMOUNT_WORDS = this.convert(num, majorNames, tensNames, numNames) + " And " + 
							str + "/100 Only";

				} catch(GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("checkVoucherPrint.error.checkVoucherAlreadyDeleted"));

				} catch(GenVSVNoValueSetValueFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("checkVoucherPrint.error.noValueSetValueFound"));

				} catch(EJBException ex) {

					if(log.isInfoEnabled()) {

						log.info("EJBException caught in ApRepCheckVoucherPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));
				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apRepCheckVoucherPrintForm");

				}

				/** fill report parameters, fill report to pdf and set report session **/ 	    	               

				String nullString = null;

				ApRepCheckVoucherPrintDetails vouDetails = (ApRepCheckVoucherPrintDetails) list.get(0);

				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("branchName", vouDetails.getCvChkBranchName());
				parameters.put("companyAddress", adCmpDetails.getCmpAddress());
				parameters.put("preparedBy", vouDetails.getCvChkApprovedRejectedByDescription());
				parameters.put("voucherNumber", vouDetails.getCvVouNumber());
				System.out.println("voucherNumber>>" + vouDetails.getCvVouNumber());
				parameters.put("createdBy", vouDetails.getCvChkCreatedByDescription());
				parameters.put("approvedBy", vouDetails.getCvChkApprovedRejectedByDescription());
				parameters.put("postedBy", vouDetails.getCvVouPostedBy());
				parameters.put("totalAmount", 	TOTAL_AMOUNT );
				parameters.put("totalAmountInWords",TOTAL_AMOUNT_WORDS);

				//String subreportFilename = null;

				//if(listSub != null) {

					//subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckVoucherPrintSub.jasper";

				//	if(new java.io.File(subreportFilename).exists()) {

				//		JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
					//	parameters.put("apRepCheckVoucherPrintSub", subreport);
				//		parameters.put("apRepCheckVoucherPrintSubDS", new ApRepCheckVoucherPrintSubDS(listSub));
				//		parameters.put("showVouchers", new Boolean(true));

				//	}

			//	} else {

				//	parameters.put("showVouchers", new Boolean(false));

			//	}
				String filename = "";
				
				//if (user.getCurrentBranch().getBrBranchCode().equals("Deecon")) {
				//	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckVoucherPrintDeecon.jasper";
				//}
				//else if (user.getCurrentBranch().getBrBranchCode().equals("Premier")) {
				//	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckVoucherPrintPremier.jasper";
				//}
				//else if (user.getCurrentBranch().getBrBranchCode().equals("Plant 3")) {
				//	filename = "/opt/ofs-resources/" + user.getCompany() +  "/ApRepCheckVoucherPrintPlant3.jasper";
				//}
				//else if (user.getCurrentBranch().getBrBranchCode().equals("Precision")) {
				//	filename = "/opt/ofs-resources/" + user.getCompany() +  "/ApRepCheckVoucherPrintPrecision.jasper";
				//} else {
					//filename = "/opt/ofs-resources/" + user.getCompany() +  "/ApRepCheckVoucherPrint.jasper";
				//}
				
				String checkVoucherType = ejbCVP.getAdPrfApCheckVoucherDataSource(user.getCmpCode());
				
				
				if(checkVoucherType.equals("AP VOUCHER")){
                                    System.out.println("VOUCHER TEST");
					filename = "/opt/ofs-resources/" + user.getCompany() +  "/ApRepCheckVoucherPrint.jasper";
					if (!new java.io.File(filename).exists()) {

						filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckVoucherPrint.jasper";

						if (!new java.io.File(filename).exists()) {

							filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckVoucherPrint.jasper");

						}

					} 
					
				}else{
                                    System.out.println("DIRECT CHECK TEST");
					filename = "/opt/ofs-resources/" + user.getCompany() +  "/ApRepCheckVoucherPrint.jasper";
					if (!new java.io.File(filename).exists()) {

						filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCheckVoucherPrint.jasper";

						if (!new java.io.File(filename).exists()) {

							filename = servlet.getServletContext().getRealPath("jreports/ApRepCheckVoucherPrint.jasper");
						}

					} 
					
				}
				
				
				

				try {

					Report report = new Report();

					report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					report.setBytes(
							JasperRunManager.runReportToPdf(filename, parameters, 
									new ApRepCheckVoucherPrintDS(list, checkVoucherType)));				     

					session.setAttribute(Constants.REPORT_KEY, report);

				} catch (Exception ex) {

					if(log.isInfoEnabled()) {

						log.info("Exception caught in ApRepCheckVoucherPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					} 

					ex.printStackTrace();
					return(mapping.findForward("cmnErrorPage"));

				}              

				return(mapping.findForward("apRepCheckVoucherPrint"));

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("apRepCheckVoucherPrintForm"));

			}

		} catch(Exception e) {

			/*******************************************************
   System Failed: Forward to error page 
			 *******************************************************/
			if(log.isInfoEnabled()) {

				log.info("Exception caught in ApRepCheckVoucherPrintAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());

			}        

			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));        
		}
	}

	private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
		String soFar;

		if (number % 100 < 20){
			soFar = numNames[number % 100];
			number /= 100;
		}
		else {
			soFar = numNames[number % 10];
			number /= 10;

			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0) return soFar;
		return numNames[number] + " Hundred" + soFar;
	}

	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
		/* special case */
		if (number == 0) { return "zero"; }

		String prefix = "";

		if (number < 0) {
			number = -number;
			prefix = "Negative";
		}

		String soFar = "";
		int place = 0;

		do {
			int n = number % 1000;
			if (n != 0){
				String s = this.convertLessThanOneThousand(n, tensNames, numNames);
				soFar = s + majorNames[place] + soFar;
			}
			place++;
			number /= 1000;
		} while (number > 0);

		return (prefix + soFar).trim();
	}     
}
