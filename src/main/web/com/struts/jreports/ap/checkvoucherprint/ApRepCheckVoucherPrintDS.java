package com.struts.jreports.ap.checkvoucherprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ApRepCheckVoucherPrintDetails;



public class ApRepCheckVoucherPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepCheckVoucherPrintDS(ArrayList list, String dataSource) {
   	
   	//if(dataSource.equals("AP DISTRIBUTION RECORD")) {
   	
   	Iterator i = list.iterator();
   	while(i.hasNext()) {
   		
   		ApRepCheckVoucherPrintDetails details = (ApRepCheckVoucherPrintDetails)i.next();
   		
   		if(dataSource.equals("AP VOUCHER")) {
				System.out.println("A: " + details.getCvVouNumber());
   			ApRepCheckVoucherPrintData apRepCVPData = new ApRepCheckVoucherPrintData(details.getCvChkSplName(), details.getCvChkDocumentNumber(), Common.convertSQLDateToString(details.getCvChkDate()),
   					details.getCvChkNumber(),details.getCvChkAmountInWords(),new Double(details.getCvChkAmount()), details.getCvVouNumber(), Common.convertSQLDateToString(details.getCvVouDate()), details.getCvVouDescription(), 
					new Double(details.getCvApplyAmount()), new Double(details.getCvApplyTaxAmount()), new Double(details.getCvApplyNetAmount()), new Double(details.getCvApplyWithholdingTaxAmount()), 
					details.getCvChkDrCoaAccountNumber(), details.getCvChkDrCoaAccountDescription(), details.getCvVouPostedBy(), 
					details.getCvChkCurrencyDescription(), Common.convertCharToString(details.getCvChkCurrencySymbol()), details.getCvChkDrCoaNaturalDesc(), 
					details.getCvVouReferenceNumber(), details.getCvChkReferenceNumber(), details.getCvChkAdBaName(),
					details.getCvChkAdBaAccountNumber(), details.getCvVouDrCoaAccountNumber(), details.getCvVouDrCoaNaturalDesc(), details.getCvChkSplAddress(), 
					details.getCvChkMemo(), details.getCvChkBranchCode(), details.getCvChkBranchName(), details.getCvTaxCode(), details.getCvWithholdingTaxCode(), details.getCvChkCreatedBy(), details.getCvChkCheckedBy(), details.getCvChkApprovedRejectedBy(), details.getCvChkApprovedBy(),
					details.getCvApplyAmountDue(), details.getCvVouPostedBy(), details.getCvCheckType(),details.getCvChkSplCode());
   			System.out.println("CHECK A");
   			
   			
   			
   			data.add(apRepCVPData);
   			
   		} else {
   			
   			
   			ApRepCheckVoucherPrintData apRepCVPData = null;
   			
   			if (details.getCvChkDrDebit() == 1) {
   				System.out.println("CHECK B");
   				System.out.println("B: " + details.getCvVouNumber());
   				apRepCVPData = new ApRepCheckVoucherPrintData(details.getCvChkSplName(),
   						Common.convertSQLDateToString(details.getCvChkDate()), Common.convertSQLDateToString(details.getCvChkCheckDate()), details.getCvChkDrCoaAccountDescription(),
   						details.getCvVouNumber(), details.getCvChkDrCoaAccountNumber(), new Double(details.getCvChkDrAmount()),
						null, details.getCvChkDocumentNumber(), details.getCvChkNumber(), details.getCvChkAmountInWords(), 
						new Double(details.getCvChkAmount()), details.getCvChkCreatedBy(), details.getCvChkCheckedBy(), details.getCvChkApprovedRejectedBy(), Common.convertCharToString(details.getCvChkCurrencySymbol()),
						details.getCvChkCurrencyDescription(), details.getCvChkAdBaName(), details.getCvChkAdBaAccountNumber(),
						details.getCvChkDescription(), details.getCvApprovalStatus(),
						details.getCvPosted() == (byte)1 ? "YES" : "NO", details.getCvChkCreatedByDescription(), details.getCvChkCheckByDescription(),
						details.getCvChkApprovedRejectedByDescription(), details.getCvChkDate(), details.getCvChkCheckDate(),
						details.getCvShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false), 
						new Boolean(Common.convertByteToBoolean(details.getCvChkCrossCheck())),
						details.getCvChkDateCreated(), details.getCvChkDateApprovedRejected(),
						details.getCvChkReferenceNumber(), details.getCvChkSplAddress(), details.getCvChkMemo(), details.getCvChkBranchCode(), details.getCvChkBranchName(), details.getCvTaxCode(), details.getCvWithholdingTaxCode(),
						details.getCvChkApprovedRejectedBy(), details.getCvApplyAmountDue(), details.getCvVouPostedBy(), details.getCvCheckType(),details.getCvChkSplCode());
   				
   			} else {
   				System.out.println("CHECK C");
   				System.out.println("C: " + details.getCvVouNumber());
   				apRepCVPData = new ApRepCheckVoucherPrintData(details.getCvChkSplName(),
   						Common.convertSQLDateToString(details.getCvChkDate()), Common.convertSQLDateToString(details.getCvChkCheckDate()), details.getCvChkDrCoaAccountDescription(), 
   						details.getCvVouNumber(), details.getCvChkDrCoaAccountNumber(), null,
						new Double(details.getCvChkDrAmount()), details.getCvChkDocumentNumber(), details.getCvChkNumber(), details.getCvChkAmountInWords(),
						new Double(details.getCvChkAmount()), details.getCvChkCreatedBy(), details.getCvChkCheckedBy(), details.getCvChkApprovedRejectedBy(), 
						Common.convertCharToString(details.getCvChkCurrencySymbol()), details.getCvChkCurrencyDescription(), details.getCvChkAdBaName(), 
						details.getCvChkAdBaAccountNumber(), details.getCvChkDescription(), details.getCvApprovalStatus(),
						details.getCvPosted() == (byte)1 ? "YES" : "NO", details.getCvChkCreatedByDescription(), details.getCvChkCheckByDescription(),
						details.getCvChkApprovedRejectedByDescription(), details.getCvChkDate(),details.getCvChkCheckDate(),
						details.getCvShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
						new Boolean(Common.convertByteToBoolean(details.getCvChkCrossCheck())),
						details.getCvChkDateCreated(), details.getCvChkDateApprovedRejected(),
						details.getCvChkReferenceNumber(), details.getCvChkSplAddress(), details.getCvChkMemo(), details.getCvChkBranchCode(), details.getCvChkBranchName(), details.getCvTaxCode(), details.getCvWithholdingTaxCode(),
						details.getCvChkApprovedRejectedBy(), details.getCvApplyAmountDue(), details.getCvVouPostedBy(), details.getCvCheckType(),details.getCvChkSplCode());
   				
   			}
   			
   			data.add(apRepCVPData);
   			
   		}
   		
   	}
   	
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("supplierName".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getSupplier();  
     }else if("date".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getDate();
     }else if("checkDate".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckDate();       
     }else if("accountDescription".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getAccountDescription(); 
     }else if("accountNumber".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getAccountNumber();    
     }else if("debitAmount".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getDebitAmount(); 
     }else if("creditAmount".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCreditAmount();
     }else if("documentNumber".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getDocumentNumber();        
     }else if("checkNumber".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckNumber();
     }else if("amountInWords".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getAmountInWords();
     }else if("amount".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getAmount();
        System.out.println("amount="+value);
     }else if("createdBy".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCreatedBy();
        System.out.print("Created By: " + value);
     }else if("checkedBy".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckedBy();
        System.out.print("Checked By: " + value);
     }else if("approvedBy".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getApprovedBy();
     }else if("currencySymbol".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCurrencySymbol();
     }else if("currencyDescription".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCurrencyDescription();
     }else if("bankName".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getBankName();
     }else if("bankAccountNumber".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getBankAccountNumber();
     }else if("description".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getDescription();
     }else if("approvalStatus".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getApprovalStatus();
     }else if("posted".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getPosted();
        System.out.print("Posted By: " + value);
     }else if("createdByDescription".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCreatedByDescription();
     }else if("checkedByDescription".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckedByDescription();
     }else if("approvedRejectedBy".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getapprovedRejectedBy();
        System.out.print("approvedRejected By: " + value);
     }else if("approvedRejectedByDescription".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getApprovedRejectedByDescription();
     }else if("date2".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getDate2();  
        
     }else if("checkDate2".equals(fieldName)){
         value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckDate2();  
     }else if("showDuplicate".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getShowDuplicate();
     }else if("crossCheck".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getCrossCheck();
     }else if("dateCreated".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getDateCreated();
     }else if("dateApprovedRejected".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getDateApprovedRejected();
     }else if("voucherNumber".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getVoucherNumber();
     	System.out.println("voucherNumber: " +value);
     }else if("voucherDate".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getVoucherDate();
     }else if("applyAmount".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getApplyAmount();
     }else if("applyTaxAmount".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getApplyTaxAmount();
     }else if("applyNetAmount".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getApplyNetAmount();
     }else if("applyWithholdingTaxAmount".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getApplyWithholdingTaxAmount();
     }else if("naturalAcctDesc".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getNaturalAccntDesc();
     }else if("vouReferenceNumber".equals(fieldName)){
     	value = ((ApRepCheckVoucherPrintData)data.get(index)).getVouReferenceNumber();
     }else if("referenceNumber".equals(fieldName)){
      	value = ((ApRepCheckVoucherPrintData)data.get(index)).getReferenceNumber();
     }else if("vouAccountNumber".equals(fieldName)){
      	value = ((ApRepCheckVoucherPrintData)data.get(index)).getVouAccountNumber();
     }else if("vouNaturalAccntDesc".equals(fieldName)){
      	value = ((ApRepCheckVoucherPrintData)data.get(index)).getVouNaturalAccountDescription();
     }else if("supplierAddress".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getSupplierAddress();  
     }else if("memo".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getMemo();  
     }else if("branchCode".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getBranchCode();  
     }else if("branchName".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getBranchName();  
     }else if("taxCode".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getTaxCode();  
     }else if("wTaxCode".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getWithholdingTaxCode();  
     }else if("amountDue".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getApplyAmountDue();  
     }else if("postedBy".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getPostedBy();  
     }else if("checkType".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getCheckType();  
     }else if("supplierCode".equals(fieldName)){
        value = ((ApRepCheckVoucherPrintData)data.get(index)).getSupplierCode();
        System.out.print(value+"aafadasdasdadasdas");
     }
      
      
     return(value);
   }
}
