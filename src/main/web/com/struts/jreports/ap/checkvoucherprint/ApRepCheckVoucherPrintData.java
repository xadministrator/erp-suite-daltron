package com.struts.jreports.ap.checkvoucherprint;

import java.util.Date;

public class ApRepCheckVoucherPrintData implements java.io.Serializable {

	private String supplier = null;
	private String date = null; 
	private String checkDate = null; 
	private String accountDescription = null;
	private String accountNumber = null;
	private Double debitAmount = null;
	private Double creditAmount = null;
	private String documentNumber = null;
	private String checkNumber = null;
	private String amountInWords = null;
	private Double amount = null; 
	private String createdBy = null;
	private String checkedBy = null;
	private String approvedBy = null;
	private String currencySymbol = null;
	private String currencyDescription = null;
	private String bankName = null;
	private String bankAccountNumber = null;
	private String description = null;
	private String referenceNumber = null;
	private String approvalStatus = null;
	private String posted = null;
	private String createdByDescription = null;
	private String checkedByDescription = null;
	private String approvedRejectedBy = null;
	private String approvedRejectedByDescription = null;
	
	private Date date2	= null;
	private Date checkDate2 = null; 
	private Date dateCreated = null;
	private Date dateApprovedRejected = null;
	private Boolean showDuplicate = null;
	private Boolean crossCheck = null;
	private String supplierAddress = null;
	private String memo = null;
	private String branchName = null;
	private String branchCode = null;
	private String supplierCode = null;
	// voucher data source
	private String voucherNumber = null;
	private String voucherDate = null;
	private Double applyAmount = null;
	private Double applyTaxAmount = null;
	private Double applyNetAmount = null;
	private Double applyWithholdingTaxAmount = null;
	private String naturalAccntDesc = null;
	private String vouReferenceNumber = null;
	private String vouAccountNumber = null;
	private String vouNaturalAccountDescription = null;

	private String taxCode = null;
	private String wTaxCode = null;
	private Double amountDue = null;
	private String postedBy = null;;
	private String checkType = null;
	
	public ApRepCheckVoucherPrintData(String supplier, String date, String checkDate, String accountDescription, String voucherNumber, String  accountNumber,
			Double debitAmount, Double creditAmount, String documentNumber, String checkNumber, String amountInWords, Double amount,
			String createdBy, String checkedBy, String approvedBy, String currencySymbol, String currencyDescription, String bankName, 
			String bankAccountNumber, String description, String approvalStatus, String posted, String createdByDescription, String checkedByDescription,
			String approvedRejectedByDescription, 
			Date date2, Date checkDate2,
			Boolean showDuplicate, Boolean crossCheck, Date dateCreated,
			Date dateApprovedRejected, String referenceNumber, String supplierAddress, String memo, String branchCode, String branchName, String taxCode, String wTaxCode,
			String approvedRejectedBy, Double amountDue, String postedBy, String checkType,String supplierCode) {
 

		this.supplier = supplier;
		
		this.date = date;
		this.checkDate = checkDate;
		
		this.accountDescription = accountDescription;
		this.voucherNumber = voucherNumber;
		this.accountNumber = accountNumber;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.documentNumber = documentNumber;
		this.checkNumber = checkNumber;
		this.amountInWords = amountInWords;
		this.amount = amount;
		this.createdBy = createdBy;
		this.checkedBy = checkedBy;
		this.approvedBy = approvedBy;
		this.currencySymbol = currencySymbol;
		this.currencyDescription = currencyDescription;
		this.bankName = bankName;
		this.bankAccountNumber = bankAccountNumber;
		this.description = description;
		this.approvalStatus = approvalStatus;
		this.posted = posted;
		this.createdByDescription = createdByDescription;
		this.checkedByDescription = checkedByDescription;
		this.approvedRejectedByDescription = approvedRejectedByDescription;
		
		this.date2 = date2;
		this.checkDate2 = checkDate2;
		
		this.showDuplicate = showDuplicate;
		this.crossCheck = crossCheck;
		this.dateCreated = dateCreated;
		this.dateApprovedRejected = dateApprovedRejected;
		this.referenceNumber = referenceNumber;
		this.supplierAddress = supplierAddress;
		this.memo = memo;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.taxCode = taxCode;
		this.wTaxCode = wTaxCode;
		this.approvedRejectedBy = approvedRejectedBy;
		this.amountDue = amountDue;
		this.postedBy = postedBy;
		this.checkType = checkType;
		this.supplierCode = supplierCode;
	}
		
	public ApRepCheckVoucherPrintData(String supplier, String documentNumber, String checkDate, String checkNumber,  String amountInWords, Double amount,String voucherNumber,
			String voucherDate, String description, Double applyAmount, Double applyTaxAmount, Double applyNetAmount, Double applyWithholdingTaxAmount,
			String accountNumber, String accountDescription, String posted, String currencyDescription, String currencySymbol, String naturalAccntDesc,
			String vouReferenceNumber, String referenceNumber, String bankName, String bankAccountNumber,
			String vouAccountNumber, String vouNaturalAccountDescription, String supplierAddress, String memo, String branchCode, String branchName, String taxCode, String wTaxCode,
			String createdBy, String checkedBy, String approvedRejectedBy, String approvedBy, Double amountDue, String postedBy, String checkType,String supplierCode) {

		this.supplier = supplier;
		this.documentNumber = documentNumber;
		this.checkDate = checkDate;
		this.checkNumber = checkNumber;
		this.voucherNumber = voucherNumber;
		this.amountInWords = amountInWords;
		this.amount = amount;
		this.voucherDate = voucherDate;
		this.description = description;
		this.applyAmount = applyAmount;
		this.applyTaxAmount = applyTaxAmount;
		this.applyNetAmount = applyNetAmount;
		this.applyWithholdingTaxAmount = applyWithholdingTaxAmount;
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.posted = posted;
		this.currencyDescription = currencyDescription;
		this.currencySymbol = currencySymbol;  
		this.naturalAccntDesc = naturalAccntDesc;
		this.vouReferenceNumber = vouReferenceNumber;
		this.referenceNumber = referenceNumber;
		this.bankName = bankName;
		this.bankAccountNumber = bankAccountNumber;
		this.vouAccountNumber = vouAccountNumber;
		this.vouNaturalAccountDescription = vouNaturalAccountDescription;
		this.supplierAddress = supplierAddress;
		this.memo = memo;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.taxCode = taxCode;
		this.wTaxCode = wTaxCode;
		this.createdBy = createdBy;
		this.checkedBy = checkedBy;
		this.approvedBy = approvedBy; 
		this.approvedRejectedBy = approvedRejectedBy;
		this.amountDue = amountDue;
		this.postedBy = postedBy;
		this.checkType = checkType;
		this.supplierCode = supplierCode;
	}


	public String getSupplier() {

		return(supplier);

	}

	public String getDate() {

		return(date);

	}

	public String getCheckDate() {

		return(checkDate);

	}

	public String getAccountDescription() {

		return(accountDescription);

	}

	public String getAccountNumber() {

		return(accountNumber);

	}

	public Double getDebitAmount() {

		return(debitAmount);

	}

	public Double getCreditAmount() {

		return(creditAmount);

	}

	public String getDocumentNumber() {

		return (documentNumber);

	}

	public String getCheckNumber() {

		return(checkNumber);

	}     

	public String getAmountInWords() {

		return(amountInWords);

	}

	public Double getAmount() {

		return(amount);

	}

	public String getCreatedBy() {

		return(createdBy);

	}

	public String getCheckedBy() {

		return(checkedBy);

	}

	public String getApprovedBy() {

		return(approvedBy);

	}
	public String getapprovedRejectedBy() {

		return(approvedRejectedBy);

	}

	public String getCurrencySymbol() {

		return(currencySymbol);

	}

	public String getCurrencyDescription() {

		return(currencyDescription);

	}

	public String getBankName() {

		return(bankName);

	}

	public String getBankAccountNumber() {

		return(bankAccountNumber);

	}

	public String getDescription() {

		return(description);

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public String getPosted() {

		return posted; 

	}

	public String getCreatedByDescription() {

		return createdByDescription;

	}
	
	public String getCheckedByDescription() {

		return checkedByDescription;

	}

	public String getApprovedRejectedByDescription() {

		return approvedRejectedByDescription;

	}

	public Date getDate2() {

		return date2;

	}
	
	public Date getCheckDate2() {

		return(checkDate2);

	}

	public Boolean getShowDuplicate() {

		return showDuplicate;

	}

	public Boolean getCrossCheck() {

		return crossCheck;

	}

	public Date getDateCreated() {

		return dateCreated;

	}

	public Date getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public String getVoucherNumber() {

		return voucherNumber;
	}

	public String getVoucherDate() {

		return voucherDate;

	}

	public Double getApplyAmount() {

		return applyAmount;

	}

	public Double getApplyTaxAmount() {

		return applyTaxAmount;

	}

	public Double getApplyNetAmount() {

		return applyNetAmount;

	}

	public Double getApplyWithholdingTaxAmount() {

		return applyWithholdingTaxAmount;

	}

	public String getNaturalAccntDesc() {

		return naturalAccntDesc;

	}

	public String getVouReferenceNumber() {

		return vouReferenceNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getVouAccountNumber() {

		return vouAccountNumber;

	}

	public String getVouNaturalAccountDescription() {

		return vouNaturalAccountDescription;

	}

	public String getSupplierAddress() {

		return supplierAddress;

	}

	public String getMemo() {

		return memo;

	}

	public String getBranchCode() {

		return branchCode;

	}

	public String getBranchName() {

		return branchName;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public String getWithholdingTaxCode() {

		return wTaxCode;

	}
	
	public Double getApplyAmountDue() {

		return amountDue;

	}
	
	public String getPostedBy() {

		return postedBy;

	}

	public String getCheckType() {

		return checkType;

	}
	public String getSupplierCode() {

		return supplierCode;

	}
}
