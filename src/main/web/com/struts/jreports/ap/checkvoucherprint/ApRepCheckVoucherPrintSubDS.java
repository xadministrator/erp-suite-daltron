package com.struts.jreports.ap.checkvoucherprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepCheckVoucherPrintDetails;

public class ApRepCheckVoucherPrintSubDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public ApRepCheckVoucherPrintSubDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while(i.hasNext()) {
			
			ApRepCheckVoucherPrintDetails details = (ApRepCheckVoucherPrintDetails)i.next(); 
			
			ApRepCheckVoucherPrintSubData arRepInvPrintData = new ApRepCheckVoucherPrintSubData(
					details.getCvChkDocumentNumber(), details.getCvVouNumber(), details.getCvVouDate(), new Double(details.getCvChkAmount()), 
					details.getCvPoDocumentNumber(), details.getCvChkDate(), details.getCvVouReferenceNumber(), 
					new Double(details.getCvVouGrossAmount()), new Double(details.getCvVouWithholdingTaxAmount()), new Double(details.getCvVouNetAmount()));
			
			data.add(arRepInvPrintData);
			
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		
		Object value = null;
		
		String fieldName = field.getName();
		
		if("documentNumber".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getDocumentNumber();
		} else if("voucherNumber".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getVoucherNumber(); 
		} else if("voucherDate".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getVoucherDate();
		} else if("poNumber".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getPoNumber();
		} else if("poDate".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getPoDate();
		} else if("amount".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getAmount();
		} else if("referenceNumber".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getReferenceNumber();
		} else if("grossAmount".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getGrossAmount();
		} else if("withholdingTaxAmount".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getWithholdingTaxAmount();
		} else if("netAmount".equals(fieldName)) {
			value = ((ApRepCheckVoucherPrintSubData)data.get(index)).getNetAmount();
		} 
		
		return(value);
	}
}