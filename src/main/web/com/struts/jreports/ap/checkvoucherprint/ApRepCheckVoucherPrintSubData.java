package com.struts.jreports.ap.checkvoucherprint;

import java.util.Date;

public class ApRepCheckVoucherPrintSubData implements java.io.Serializable {
	
	private String documentNumber = null;
	private String voucherNumber = null;
	private Date voucherDate = null;
	private String poNumber = null;
	private Date poDate = null;
	private Double amount = null;
	private String referenceNumber = null;
	private Double grossAmount = null;
	private Double withholdingTaxAmount = null;
	private Double netAmount = null;
	
	public ApRepCheckVoucherPrintSubData(String documentNumber, String voucherNumber, Date voucherDate, Double amount, String poNumber, Date poDate,
			String referenceNumber, Double grossAmount, Double withholdingTaxAmount, Double netAmount) {
		
		this.documentNumber = documentNumber;
		this.voucherNumber = voucherNumber;
		this.voucherDate = voucherDate;
		this.poNumber = poNumber;
		this.poDate = poDate;
		this.amount = amount;
		this.referenceNumber = referenceNumber;
		this.grossAmount = grossAmount;
		this.withholdingTaxAmount = withholdingTaxAmount;
		this.netAmount = netAmount;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getVoucherNumber() {
		
		return voucherNumber;
		
	}
	
	public Date getVoucherDate() {
		
		return voucherDate;
		
	}
	
	public String getPoNumber() {
		
		return poNumber;
		
	}
	
	public Date getPoDate() {
		
		return poDate;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public Double getGrossAmount() {
		
		return grossAmount;
		
	}
	
	public Double getWithholdingTaxAmount() {
		
		return withholdingTaxAmount;
		
	}
	
	public Double getNetAmount() {
		
		return netAmount;
		
	}
	
}