package com.struts.jreports.ap.debitmemoprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepDebitMemoPrintController;
import com.ejb.txn.ApRepDebitMemoPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;

public final class ApRepDebitMemoPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
           
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepDebitMemoPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepDebitMemoPrintForm actionForm = (ApRepDebitMemoPrintForm)form;
         
         String frParam= Common.getUserPermission(user, Constants.AP_REP_DEBIT_MEMO_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ApRepDebitMemoPrintController EJB
*******************************************************/

         ApRepDebitMemoPrintControllerHome homeDM = null;
         ApRepDebitMemoPrintController ejbDM = null;       

         try {
         	
            homeDM = (ApRepDebitMemoPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepDebitMemoPrintControllerEJB", ApRepDebitMemoPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepDebitMemoPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDM = homeDM.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepDebitMemoPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	          
/*******************************************************
   -- TO DO getApVouByVouCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list = null;           

				try {
					
					ArrayList vouCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		vouCodeList.add(new Integer(request.getParameter("debitMemoCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("voucherCode[" + i + "]") != null) {
								
								vouCodeList.add(new Integer(request.getParameter("voucherCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbDM.executeApRepDebitMemoPrint(vouCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbDM.getAdCompany(user.getCmpCode());	            	
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoPrint.error.debitMemoAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApDebitMemoPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("apRepDebitMemoPrintForm");

	             }
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				   
				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();   
			    	   
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("companyAddress", companyAddress);
				parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());
								    	    	    	        	    
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepDebitMemoPrint.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/ApRepDebitMemoPrint.jasper");
			    
		        }
								
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepDebitMemoPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ApRepDebitMemoPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("apRepDebitMemoPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("apRepDebitMemoPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ApRepDebitMemoPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
