package com.struts.jreports.ap.debitmemoprint;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.voucherprint.ApRepVoucherPrintData;
import com.struts.util.Common;
import com.util.ApRepDebitMemoPrintDetails;


public class ApRepDebitMemoPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepDebitMemoPrintDS(ArrayList apRepVPList) {

   	  Iterator i = apRepVPList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ApRepDebitMemoPrintDetails details = (ApRepDebitMemoPrintDetails)i.next();
   	    
   	    if(details.getDpDrDebit() == 1) {
   	    
	   	  	ApRepDebitMemoPrintData apRepVPData = new ApRepDebitMemoPrintData(details.getDpVouSplSupplierCode(),
	   	  	    Common.convertSQLDateToString(details.getDpVouDate()),
	   	  	    details.getDpVouSplAddress(),
	   	  	    details.getDpVouDocumentNumber(),
	   	  	    details.getDpVouDrNumber(),
	   	  	    details.getDpVouSplTin(),
	   	  	    details.getDpVouDmVoucherNumber(),
	   	  	    details.getDpVouDescription(),
	   	  	    new Double(details.getDpVouBillAmount()),
	   	  	    details.getDpVouCreatedBy(),
	   	  	    Common.convertSQLDateToString(new Date()),
	   	  	    details.getDpVouApprovedBy(),
	   	  	    details.getDpDrCoaAccountNumber(),
	   	  	    details.getDpDrCoaAccountDescription(),
	   	  	    new Double(details.getDpDrAmount()),
	   	  	    null,
				details.getDpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				details.getDpVouApprovalStatus(),
				details.getDpVouCreatedByDescription(),
	   	  	    details.getDpVouApprovedRejectedByDescription());
	   	  	

             data.add(apRepVPData);
	   	  	
	    } else {
	   	
	   	    ApRepDebitMemoPrintData apRepVPData = new ApRepDebitMemoPrintData(details.getDpVouSplSupplierCode(),
	   	  	    Common.convertSQLDateToString(details.getDpVouDate()),
	   	  	    details.getDpVouSplAddress(),
	   	  	    details.getDpVouDocumentNumber(),
	   	  	    details.getDpVouDrNumber(),
	   	  	    details.getDpVouSplTin(),
	   	  	    details.getDpVouDmVoucherNumber(),
	   	  	    details.getDpVouDescription(),
	   	  	    new Double(details.getDpVouBillAmount()),
	   	  	    details.getDpVouCreatedBy(),
	   	  	    Common.convertSQLDateToString(new Date()),
	   	  	    details.getDpVouApprovedBy(),
	   	  	    details.getDpDrCoaAccountNumber(),
	   	  	    details.getDpDrCoaAccountDescription(),
	   	  	    null,
	   	  	    new Double(details.getDpDrAmount()),
				details.getDpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				details.getDpVouApprovalStatus(),
				details.getDpVouCreatedByDescription(),
	   	  	    details.getDpVouApprovedRejectedByDescription());
    	
   	  	    data.add(apRepVPData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("supplier".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getSupplier();       
      }else if("address".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getAddress();
      }else if("voucherNumber".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getVoucherNumber();
      }else if("drNumber".equals(fieldName)){
          value = ((ApRepDebitMemoPrintData)data.get(index)).getDrNumber();  
      }else if("tin".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getTin();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getReferenceNumber();
      }else if("description".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getDescription();
      }else if("amount".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getAmount();
      }else if("createdBy".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getCreatedBy();
      }else if("datePrinted".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getDatePrinted();
      }else if("approvedBy".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getApprovedBy();
      }else if("accountNumber".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getAccountDescription();
      }else if("debitAmount".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getDebitAmount();
      }else if("creditAmount".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getCreditAmount();
      }else if("showDuplicate".equals(fieldName)){
        value = ((ApRepDebitMemoPrintData)data.get(index)).getShowDuplicate();
      }else if("date".equals(fieldName)){
        value = ((ApRepDebitMemoPrintData)data.get(index)).getDate();
      }else if("approvalStatus".equals(fieldName)){
        value = ((ApRepDebitMemoPrintData)data.get(index)).getApprovalStatus();
        System.out.println("approvalStatus: " + value);
      }else if("createdByDescription".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getCreatedByDescription();
         System.out.println("createdByDescription: " + value);
      }else if("approvedByDescription".equals(fieldName)){
         value = ((ApRepDebitMemoPrintData)data.get(index)).getApprovedByDescription();
         System.out.println("approvedByDescription: " + value);
      }

      return(value);
   }
}
