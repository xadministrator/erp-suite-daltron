package com.struts.jreports.ap.debitmemoprint;

public class ApRepDebitMemoPrintData implements java.io.Serializable {

   private String supplier = null;
   private String date = null;
   private String address = null;
   private String voucherNumber = null;
   private String drNumber = null;
   
   private String tin = null;
   private String referenceNumber = null;
   private String description = null;
   private Double amount = null;
   private String createdBy = null;
   private String datePrinted = null;
   private String approvedBy = null;   
   
   private String accountNumber = null;
   private String accountDescription = null;
   private Double debitAmount = null;
   private Double creditAmount = null;
   
   private Boolean showDuplicate = null;
   private String approvalStatus = null;
   
   private String createdByDescription = null;
   private String approvedByDescription = null;
   
   public ApRepDebitMemoPrintData(String supplier,
	   String date, 
	   String address,
	   String voucherNumber,
	   String drNumber,
	   String tin,
	   String referenceNumber,
	   String description,
	   Double amount,
	   String createdBy,
	   String datePrinted,
	   String approvedBy, 
	   String accountNumber,
	   String accountDescription,
	   Double debitAmount,
	   Double creditAmount,
	   Boolean showDuplicate, 
	   String approvalStatus,
	   String createdByDescription,
	   String approvedByDescription) {
      	
       this.supplier = supplier;
       this.date = date;
       this.address = address;
       this.voucherNumber = voucherNumber;
       this.drNumber = drNumber;
       this.tin = tin;
       this.referenceNumber = referenceNumber;
       this.description = description;
       this.amount = amount;
       this.createdBy = createdBy;
       this.datePrinted = datePrinted;
       this.approvedBy = approvedBy;
       this.accountNumber = accountNumber;
       this.accountDescription = accountDescription;
       this.debitAmount = debitAmount;
       this.creditAmount = creditAmount;
       this.showDuplicate = showDuplicate;
       this.approvalStatus = approvalStatus;
       this.createdByDescription = createdByDescription;
       this.approvedByDescription = approvedByDescription;
      	
   }

   public String getSupplier() {
   	
      return supplier;
      
   }
   
   public String getDate() {
	   
	  return date;
	   
   }
   
   public String getAddress() {
   	
      return address;
      
   }
   
   public String getVoucherNumber() {
   	
      return voucherNumber;
      
   }
   
   public String getDrNumber() {
	   	
	      return drNumber;
	      
   }
   
   
   
   public String getTin() {
   	
      return tin;
      
   }
   
   public String getReferenceNumber() {
   	
      return referenceNumber;
      
   }
   
   public String getDescription() {
   	
      return description;
      
   }
   
   public Double getAmount() {
   	
      return amount;
      
   }
   
   public String getCreatedBy() {
   	
      return createdBy;
      
   }
   
   public String getDatePrinted() {
   	
      return datePrinted;
      
   }
   
   public String getApprovedBy() {
   	
      return approvedBy;
      
   }
   
   public String getAccountNumber() {
   	
      return accountNumber;
      
   }
   
   public String getAccountDescription() {
   	
      return accountDescription;
      
   }
   
   public Double getDebitAmount() {
   	
      return debitAmount;
      
   }
   
   public Double getCreditAmount() {
   	
      return creditAmount;
      
   }
   
   public Boolean getShowDuplicate() {

	   return showDuplicate;

   }

   public String getApprovalStatus() {

	   return approvalStatus;

   }
   
   public String getCreatedByDescription() {

		return createdByDescription;

	}
  
	public String getApprovedByDescription() {

		return approvedByDescription;

	}
   
}
