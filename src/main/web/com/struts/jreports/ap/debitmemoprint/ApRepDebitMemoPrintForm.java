package com.struts.jreports.ap.debitmemoprint;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class ApRepDebitMemoPrintForm extends ActionForm implements Serializable {

   private String userPermission = new String();
   private Integer debitMemoCode = null;

   public String getUserPermission() {

      return userPermission;

   } 

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  
   }       
}