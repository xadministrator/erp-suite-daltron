package com.struts.jreports.ap.inputtax;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepInputTaxDetails;

public class ApRepInputTaxDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepInputTaxDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         ApRepInputTaxDetails details = (ApRepInputTaxDetails)i.next();
	         
	         ApRepInputTaxData argData = new ApRepInputTaxData(
	         			details.getTiTinOfVendor(),details.getTiRegisteredName(),details.getTiLastName(),
						details.getTiFirstName(),details.getTiMiddleName(),details.getTiAddress1(),
						details.getTiAddress2(),new Double(details.getTiNetAmount()),				
						new Double(details.getTiInputTax()));
	         
	         data.add(argData);
	         
      }
   }
   
   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }
   
   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("tinOfVendor".equals(fieldName)){
         value = ((ApRepInputTaxData)data.get(index)).getTinOfVendor();
      }else if("registeredName".equals(fieldName)){
      	 value = ((ApRepInputTaxData)data.get(index)).getRegisteredName();
      }else if("lastName".equals(fieldName)){
      	 value = ((ApRepInputTaxData)data.get(index)).getLastName();
      }else if("firstName".equals(fieldName)){
     	 value = ((ApRepInputTaxData)data.get(index)).getFirstName();
      }else if("middleName".equals(fieldName)){
     	 value = ((ApRepInputTaxData)data.get(index)).getMiddleName();
      }else if("address1".equals(fieldName)){
     	 value = ((ApRepInputTaxData)data.get(index)).getAddress1();
      }else if("address2".equals(fieldName)){
     	 value = ((ApRepInputTaxData)data.get(index)).getAddress2();
      }else if("netAmount".equals(fieldName)){
     	 value = ((ApRepInputTaxData)data.get(index)).getNetAmount();      
      }else if("inputTax".equals(fieldName)){
      	 value = ((ApRepInputTaxData)data.get(index)).getInputTax();
      }      
      return(value);
      
   }
}
