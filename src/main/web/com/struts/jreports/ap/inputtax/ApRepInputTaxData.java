package com.struts.jreports.ap.inputtax;

import java.io.Serializable;

public class ApRepInputTaxData implements Serializable {
	
	private String tinOfVendor;
	private String registeredName;
	private String lastName;
	private String firstName;
	private String middleName;
	private String address1;
	private String address2;
	private Double netAmount;
	private Double inputTax;
	
	public ApRepInputTaxData(String tinOfVendor, String registeredName, String lastName, 
					String firstName, String middleName, String address1, String address2, Double netAmount, 
					Double inputTax) {
		
		this.tinOfVendor = tinOfVendor;
		this.registeredName = registeredName;
		this.lastName = lastName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.address1 = address1;
		this.address2 = address2;
		this.netAmount = netAmount;
		this.inputTax = inputTax;
		
	}
	
	public String getTinOfVendor() {
		
		return tinOfVendor;
		
	}
	
	public String getRegisteredName() {
		
		return registeredName;
		
	}
	
	public String getLastName() {
		
		return lastName;
		
	}
	
	public String getFirstName() {
		
		return firstName;
		
	}
	
	public String getMiddleName() {
		
		return middleName;
		
	}
	
	public String getAddress1() {
		
		return address1;
		
	}
	
	public String getAddress2() {
		
		return address2;
		
	}

	public Double getNetAmount() {
		
		return netAmount;
		
	}
		
	public Double getInputTax() {
		
		return inputTax;
		
	}
}
