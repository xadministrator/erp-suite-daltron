package com.struts.jreports.ap.inputtax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepInputTaxForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String dateFrom = null;
   private String dateTo = null;      
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();
   
   private ArrayList apRepBrInptTxList = new ArrayList();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }
   
   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }
   
   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
  
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Object[] getApRepBrInptTxList(){
       
       return apRepBrInptTxList.toArray();
       
   }
   
   public ApRepBranchInputTaxList getApRepBrInptTxByIndex(int index){
       
       return ((ApRepBranchInputTaxList)apRepBrInptTxList.get(index));
       
   }
   
   public int getApRepBrInptTxListSize(){
       
       return(apRepBrInptTxList.size());
       
   }
   
   public void saveApRepBrInptTxList(Object newApRepBrInptTxList){
       
       apRepBrInptTxList.add(newApRepBrInptTxList);   	  
       
   }
   
   public void clearApRepBrInptTxList(){
       
       apRepBrInptTxList.clear();
       
   }
   
   public void setApRepBrInptTxList(ArrayList apRepBrInptTxList) {
       
       this.apRepBrInptTxList = apRepBrInptTxList;
       
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){   
       
       for (int i=0; i<apRepBrInptTxList.size(); i++) {
           
           ApRepBranchInputTaxList  list = (ApRepBranchInputTaxList)apRepBrInptTxList.get(i);
           list.setBranchCheckbox(false);	       
           
       } 
       
      goButton = null;
      closeButton = null;
      supplierCode = null;  
      dateFrom = Common.convertSQLDateToString(new java.util.Date());
      dateTo = Common.convertSQLDateToString(new java.util.Date());
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("inputTax.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
		    errors.add("dateTo", new ActionMessage("inputTax.error.dateToInvalid"));
		 }	
		 
      }
      return(errors);
   }
}
