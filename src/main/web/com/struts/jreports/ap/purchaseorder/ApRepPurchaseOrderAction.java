package com.struts.jreports.ap.purchaseorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepPurchaseOrderController;
import com.ejb.txn.ApRepPurchaseOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepPurchaseOrderAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
			Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApRepPurchaseOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApRepPurchaseOrderForm actionForm = (ApRepPurchaseOrderForm)form;
			
			// reset report to null
			actionForm.setReport(null);
			
			String frParam = Common.getUserPermission(user, Constants.AP_REP_RECEIVING_ITEMS_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("apRepPurchaseOrder");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
/*******************************************************
 			Initialize ApRepPurchaseOrderController EJB
*******************************************************/
			
			ApRepPurchaseOrderControllerHome homeRR = null;
			ApRepPurchaseOrderController ejbRR = null;       
			
			try {
				
				homeRR = (ApRepPurchaseOrderControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ApRepPurchaseOrderControllerEJB", ApRepPurchaseOrderControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApRepPurchaseOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbRR = homeRR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApRepPurchaseOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
			-- AP RR Go Action --
*******************************************************/
			
			if(request.getParameter("goButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArrayList list = null;
				
				String company = null;
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();            	
					
					if (!Common.validateRequired(actionForm.getSupplierCode())) {
						
						criteria.put("supplierCode", actionForm.getSupplierCode());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}	        		        
					
					if (!Common.validateRequired(actionForm.getItemName())) {
						
						criteria.put("itemName", actionForm.getItemName());
						
					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
						
						criteria.put("category", actionForm.getCategory());
						
					}
					
					if (!Common.validateRequired(actionForm.getLocation())) {
						
						criteria.put("location", actionForm.getLocation());
						
					}
					
					if (actionForm.getIncludeUnposted()){
						
						criteria.put("includeUnposted", "yes");
						
					} else {
						
						criteria.put("includeUnposted", "no");
						
					}
					
					if (actionForm.getIncludeCancelled()){
						
						criteria.put("includeCancelled", "yes");
						
					} else {
						
						criteria.put("includeCancelled", "no");
						
					}

					// save criteria
					
					actionForm.setCriteria(criteria);
					
				}
				
				// branch map
				
				ArrayList adBrnchList = new ArrayList();
				// ArrayList adBrnchList2 = new ArrayList();
				
				for(int j=0; j < actionForm.getApRepBrPrchsOrdrListSize(); j++) {
				    
				    ApRepBranchPurchaseOrderList apRepBrPrchsOrdrList = (ApRepBranchPurchaseOrderList)actionForm.getApRepBrPrchsOrdrByIndex(j);
				    
				    if(apRepBrPrchsOrdrList.getBranchCheckbox() == true) {
				        
				        AdBranchDetails mdetails = new AdBranchDetails();	                    
				        mdetails.setBrCode(apRepBrPrchsOrdrList.getBrCode());
				        adBrnchList.add(mdetails);
				        
				    }
				    
				}
				
				try {
					
					// get company
					
					AdCompanyDetails adCmpDetails = ejbRR.getAdCompany(user.getCmpCode());
					company = adCmpDetails.getCmpName();
					
					// execute report
					
					list = ejbRR.executeApRepPurchaseOrder(actionForm.getCriteria(),actionForm.getOrderBy(), actionForm.getSummarize(), adBrnchList, user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("purchaseOrderRep.error.noRecordFound"));
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApRepPurchaseOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apRepPurchaseOrder");
					
				}
				
				
				// fill report parameters, fill report to pdf and set report session
				
				Map parameters = new HashMap();
				parameters.put("company", company);
				parameters.put("branchName", user.getCurrentBranch().getBrName());
				parameters.put("dateToday", new java.util.Date());
				parameters.put("printedBy", user.getUserName());
				parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
				
				if (actionForm.getIncludeUnposted()){
					
					parameters.put("includeUnposted", "yes");
					
				} else {
					
					parameters.put("includeUnposted", "no");
					
				}
				
				
				if (actionForm.getIncludeCancelled()){
					
					parameters.put("includeCancelled", "yes");
					
				} else {
					
					parameters.put("includeCancelled", "no");
					
				}
				
				if (actionForm.getSupplierCode() != null) {
					
					parameters.put("supplierCode", actionForm.getSupplierCode());
					
				}
				
				if (actionForm.getDateFrom() != null) {
					
					parameters.put("dateFrom", actionForm.getDateFrom());
				}
				
				if (actionForm.getDateTo() != null) {
					
					parameters.put("dateTo", actionForm.getDateTo());
				}	    		    	
				
				if (actionForm.getItemName() != null) {
					
					parameters.put("itemName", actionForm.getSupplierCode());
					
				}
				
				if (actionForm.getCategory() != null) {
					
					parameters.put("category", actionForm.getCategory());
					
				}
				
				if (actionForm.getLocation() != null) {
					
					parameters.put("location", actionForm.getLocation());
					
				}
				
				String branchMap = null;
				boolean first = true;
				for(int j=0; j < actionForm.getApRepBrPrchsOrdrListSize(); j++) {

					ApRepBranchPurchaseOrderList brList = (ApRepBranchPurchaseOrderList)actionForm.getApRepBrPrchsOrdrByIndex(j);

					if(brList.getBranchCheckbox() == true) {
						if(first) {
							branchMap = brList.getBrName();
							first = false;
						} else {
							branchMap = branchMap + ", " + brList.getBrName();
						}
					}

				}
				parameters.put("branchMap", branchMap);
				
				String filename = null;
				if (actionForm.getSummarize()) {
					
					filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseOrderSummary.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseOrderSummary.jasper");

					}
					
				} else {
				
				filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseOrder.jasper";

				if (!new java.io.File(filename).exists()) {
					
					filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseOrder.jasper");

				}	
				}
				
				try {
					
					Report report = new Report();
					
					if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(
								JasperRunManager.runReportToPdf(filename, parameters, 
										new ApRepPurchaseOrderDS(list)));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
						report.setBytes(
								JasperRunManagerExt.runReportToXls(filename, parameters, 
										new ApRepPurchaseOrderDS(list)));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
						report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
								new ApRepPurchaseOrderDS(list)));												    
						
					}
					
					session.setAttribute(Constants.REPORT_KEY, report);
					actionForm.setReport(Constants.STATUS_SUCCESS);		    	
					
				} catch(Exception ex) {
					
					if(log.isInfoEnabled()) {
						log.info("Exception caught in ApRepPurchaseOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}		    	    
				
/*******************************************************
			-- AP RR Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
			-- INV RR Load Action --
*******************************************************/
				
			}
			
			if(frParam != null) {
			    
			    try {
			    	
			    	actionForm.reset(mapping, request);
			        
			        ArrayList list = null;
			        Iterator i = null;
			        
			        actionForm.clearApRepBrPrchsOrdrList();
			        
			        list = ejbRR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
			        
			        i = list.iterator();
			        
			        while(i.hasNext()) {
			            
			            AdBranchDetails details = (AdBranchDetails)i.next();
			            
			            ApRepBranchPurchaseOrderList apRepBrPrchsOrdrList = new ApRepBranchPurchaseOrderList(actionForm,
			                    details.getBrCode(),
			                    details.getBrBranchCode(), details.getBrName());
			            apRepBrPrchsOrdrList.setBranchCheckbox(true);
			            
			            actionForm.saveApRepBrPrchsOrdrList(apRepBrPrchsOrdrList);
			            
			        }
			        
			    } catch (GlobalNoRecordFoundException ex) {
			        
			    } catch (EJBException ex) {
			        
			        if (log.isInfoEnabled()) {
			            
			            log.info("EJBException caught in ApRepPurchaseOrderAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			            return mapping.findForward("cmnErrorPage"); 
			            
			        }
			        
			    } 
			    
			    try {
			        
			        ArrayList list = null;			       			       
			        Iterator i = null;
					
					actionForm.clearCategoryList();           	
					
					list = ejbRR.getAdLvInvItemCategoryAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setCategoryList((String)i.next());
							
						}
						
					}
					
					actionForm.clearLocationList();           	
					
					list = ejbRR.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}

				return(mapping.findForward("apRepPurchaseOrder"));		          
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			
/*******************************************************
			 System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in InvRepItemCostingAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}   
			
			return(mapping.findForward("cmnErrorPage"));   
			
		}
		
	}
}
