package com.struts.jreports.ap.purchaseorder;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepPurchaseOrderDetails;

public class ApRepPurchaseOrderDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepPurchaseOrderDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         ApRepPurchaseOrderDetails details = (ApRepPurchaseOrderDetails)i.next();
	         
	         ApRepPurchaseOrderData argData = new ApRepPurchaseOrderData( details.getPoDate(), details.getPoBranchCode(),
	         		details.getPoItemName(), details.getPoItemDescription(), details.getPoLocation(),
					details.getPoDocNo(), details.getPoNumber(), details.getPoSupplierName(), details.getPoUnit(),
					new Double(details.getPoQuantity()), new Double(details.getPoUnitCost()),
					new Double(details.getPoAmount()), new Double(details.getPoQuantityReceived()),
					details.getPoVoid(), details.getPoCreatedBy(), details.getPoApprovedRejectedBy(),
					details.getPoDescription(), details.getPoReferenceNumber(), details.getPoPaymentTermName(), details.getPoSupplierAddress(), details.getPoSupplierPhoneNumber(),
					details.getPoSupplierFaxNumber());

	         data.add(argData);
	         
      }
      
   }
   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getDate();
   		
   	} else if("branchCode".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getPoBranchCode();	
	
   	} else if("itemName".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getLocation();
   		
   	} else if("documentNumber".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getPoDocNo();
   		
   	} else if("poNumber".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getPoNumber();
   		
   	} else if("supplierName".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getSupplierName();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getUnit();
   		
   	}else if("quantity".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getQuantity();
   		
   	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getUnitCost();
   		
   	} else if("amount".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getAmount();
   		
   	} else if("quantityReceived".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getQuantityReceived();
   		
   	} else if("poVoid".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getPoVoid();
   		
   	} else if("createdBy".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getCreatedBy();
   		
   	} else if("approvedRejectedBy".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getApprovedRejectedBy();
   		
   	} else if("description".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getDescription();
   		
   	} else if("referenceNumber".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getReferenceNumber();
   		
   	} else if("paymentTermName".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getReferenceNumber();
   		
   	} else if("supplierAddress".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getSupplierAddress();
   		
   	} else if("supplierPhoneNumber".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getSupplierPhoneNumber();
   		
   	} else if("supplierFaxNumber".equals(fieldName)) {
   		
   		value = ((ApRepPurchaseOrderData)data.get(index)).getSupplierFaxNumber();
   		
   	}
   	
   	return(value);
   	
   }
   
}
