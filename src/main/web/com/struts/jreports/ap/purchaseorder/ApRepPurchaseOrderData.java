package com.struts.jreports.ap.purchaseorder;

import java.util.Date;

public class ApRepPurchaseOrderData implements java.io.Serializable {

	private Date date;
	private String branchCode;
	private String itemName;
	private String itemDescription;
	private String location;
	private String documentNumber;
	private String poNumber;
	private String supplierName;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;
	private Double quantityReceived;
	
	private String poVoid;
	private String createdBy;
	private String approvedRejectedBy;
	private String description;
	private String referenceNumber;
	private String paymentTermName;
	private String supplierAddress;
	private String supplierPhoneNumber;
	private String supplierFaxNumber;

	public ApRepPurchaseOrderData(	Date date, String branchCode, String itemName, String itemDescription, String location,
			String documentNumber, String poNumber, String supplierName, String unit, Double quantity, Double unitCost, 
			Double amount, Double quantityReceived, String poVoid, String createdBy, String approvedRejectedBy,
			String description, String referenceNumber, String paymentTermName, String supplierAddress, String supplierPhoneNumber, String supplierFaxNumber) {
		
		this.date = date;
		this.branchCode = branchCode;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.documentNumber = documentNumber;
		this.poNumber = poNumber;
		this.supplierName = supplierName;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.quantityReceived = quantityReceived;
		this.poVoid = poVoid;
		this.createdBy = createdBy;
		this.approvedRejectedBy = approvedRejectedBy;
		this.description = description;
		this.referenceNumber = referenceNumber;		
		this.paymentTermName = paymentTermName;
		this.supplierAddress = supplierAddress;
		this.supplierPhoneNumber = supplierPhoneNumber;
		this.supplierFaxNumber = supplierFaxNumber;
	}

	
	public Date getDate() {
		
		return date;
		
	}
	
	public void setDate(Date date) {
		
		this.date = date;
		
	}
	
	public String getPoBranchCode() {
		
		return branchCode;
		
	}
	
	public void setPoBranchCode(String branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public String getPoDocNo() {
		
		return documentNumber;
		
	}
	
	public void setPoDocNo(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	
	public String getPoNumber() {
		
		return poNumber;
		
	}
	
	public void setPoNumber(String poNumber) {
		
		this.poNumber = poNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
	
	}
	
	public void setAmount(Double amount) {
	
		this.amount = amount;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDescription) {
	
		this.itemDescription = itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public void setLocation(String location) {
	
		this.location = location;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public void setUnitCost(Double unitCost) {
	
		this.unitCost = unitCost;
	
	}
	
	public String getSupplierName() {
		
		return supplierName;

	}
	
	public void setSupplierName(String supplierName) {
		
		this.supplierName = supplierName;
		
	}
	
	public String getUnit() {
		
		return unit;

	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public Double getQuantityReceived() {
		
			return quantityReceived;
		
		}
		
	public void setQuantityReceived(Double quantityReceived) {
		
		this.quantityReceived = quantityReceived;
		
	}
	
	public String getPoVoid() {
		
		return poVoid;
	}
	
	public void setPoVoid(String poVoid) {
		
		this.poVoid = poVoid;
	}
	
	public String getCreatedBy() {
		
		return createdBy;
	}
	
	public void setCreatedBY(String createdBy) {
		
		this.createdBy = createdBy;
	}
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getDescription() {
		
		return description;
	}
	
	public void setDescription(String description) {
		
		this.description = description;
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
	}
	
	public String getPaymentTermName() {
		
		return referenceNumber;
	}
	
	public void setPaymentTermName(String paymentTermName) {
		
		this.paymentTermName = paymentTermName;
	}
	
	public void setSupplierAddress(String supplierAddress) {
		
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierAddress() {
		
		return supplierAddress;
	}
	
	public void setSupplierPhoneNumber(String supplierPhoneNumber) {
		
		this.supplierPhoneNumber = supplierPhoneNumber;
	}
	public String getSupplierPhoneNumber() {
		
		return supplierPhoneNumber;
	}
	
	public void setSupplierFaxNumber(String supplierFaxNumber) {
		
		this.supplierFaxNumber = supplierFaxNumber;
	}
	public String getSupplierFaxNumber() {
		
		return supplierFaxNumber;
	}
} // ApRepPurchaseOrderData class
