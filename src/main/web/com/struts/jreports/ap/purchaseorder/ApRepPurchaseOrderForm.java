package com.struts.jreports.ap.purchaseorder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepPurchaseOrderForm extends ActionForm implements Serializable{

	private String itemName = null;
	private String category = null;
	private boolean includeUnposted = false;
	private boolean includeCancelled = false;
	private boolean summarize = false;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String supplierCode = null;
	private String dateFrom = null;
	private String dateTo = null;      
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	private ArrayList apRepBrPrchsOrdrList = new ArrayList();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public boolean getIncludeUnposted(){
		
		return(includeUnposted);
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public boolean getIncludeCancelled(){
		
		return(includeCancelled);
		
	}
	
	public void setIncludeCancelled(boolean includeCancelled) {
		
		this.includeCancelled = includeCancelled;
		
	}
	
	public boolean getSummarize(){
		
		return(summarize);
		
	}
	
	public void setSummarize(boolean summarize) {
		
		this.summarize = summarize;
		
	}
	
	public String getItemName(){
		
		return(itemName);
		
	}
	
	public void setItemName(String itemName){
		
		this.itemName = itemName;
		
	}
	
	public String getCategory (){
		
		return(category );
		
	}
	
	public void setCategory (String category ){
		
		this.category  = category ;
		
	}
	
	public ArrayList getCategoryList(){
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category){
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList(){
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation (){
		
		return(location );
		
	}
	
	public void setLocation (String location ){
		
		this.location  = location ;
		
	}
	
	public ArrayList getLocationList(){
		
		return(locationList);
		
	}
	
	public void setLocationList(String location){
		
		locationList.add(location);
		
	}
	
	public void clearLocationList(){
		
		locationList.clear();
		
		locationList.add(Constants.GLOBAL_BLANK);
		
	}	
	
	public String getSupplierCode(){
		
		return(supplierCode);
		
	}
	
	public void setSupplierCode(String supplierCode){
		
		this.supplierCode = supplierCode;
		
	}
	
	public String getDateFrom(){
		
		return(dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return(dateTo);
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getViewType(){
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){

		this.userPermission = userPermission;
		
	}
	
	public Object[] getApRepBrPrchsOrdrList(){
	    
	    return apRepBrPrchsOrdrList.toArray();
	    
	}
	
	public ApRepBranchPurchaseOrderList getApRepBrPrchsOrdrByIndex(int index){
	    
	    return ((ApRepBranchPurchaseOrderList)apRepBrPrchsOrdrList.get(index));
	    
	}
	
	public int getApRepBrPrchsOrdrListSize(){
	    
	    return(apRepBrPrchsOrdrList.size());
	    
	}
	
	public void saveApRepBrPrchsOrdrList(Object newApRepBrPrchsOrdrList){
	    
	    apRepBrPrchsOrdrList.add(newApRepBrPrchsOrdrList);   	  
	    
	}
	
	public void clearApRepBrPrchsOrdrList(){
	    
	    apRepBrPrchsOrdrList.clear();
	    
	}
	
	public void setApRepBrPrchsOrdrList(ArrayList apRepBrPrchsOrdrList) {
	    
	    this.apRepBrPrchsOrdrList = apRepBrPrchsOrdrList;
	    
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
	    
	    for (int i=0; i<apRepBrPrchsOrdrList.size(); i++) {
	        
	        ApRepBranchPurchaseOrderList  list = (ApRepBranchPurchaseOrderList)apRepBrPrchsOrdrList.get(i);
	        list.setBranchCheckbox(false);	       
	        
	    } 
				
		goButton = null;
		closeButton = null;
		supplierCode = null;  
		
		itemName = null;
		category = null;
		location = null;
		supplierCode = null;
		includeUnposted = false;
		includeCancelled = false;
		summarize = false;
				
		dateFrom = Common.convertSQLDateToString(new java.util.Date());
		dateTo = Common.convertSQLDateToString(new java.util.Date());
		
		orderByList.clear();
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_DATE);
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_DOCUMENT_NUMBER);
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_LOCATION);
		orderByList.add(Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_PO_NUMBER);
		orderBy = Constants.AP_REP_PURCHASE_ORDER_ORDER_BY_DATE;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(Common.validateRequired(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("purchaseOrderRep.error.dateFromRequired"));
				
			}
			
			if(Common.validateRequired(dateTo)){
				
				errors.add("dateTo", new ActionMessage("purchaseOrderRep.error.dateToRequired"));
				
			}	
			
			if(!Common.validateDateFormat(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("purchaseOrderRep.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)){
				
				errors.add("dateTo", new ActionMessage("purchaseOrderRep.error.dateToInvalid"));
				
			}	

		}

		return(errors);
	
	}

}
