package com.struts.jreports.ap.purchaseorderprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepPurchaseOrderPrintController;
import com.ejb.txn.ApRepPurchaseOrderPrintControllerHome;
import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApRepPurchaseOrderPrintDetails;

public final class ApRepPurchaseOrderPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {
      	
 /*******************************************************
      	 Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("ApRepPurchaseOrderPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	ApRepPurchaseOrderPrintForm actionForm = (ApRepPurchaseOrderPrintForm)form;
      	
      	String frParam = Common.getUserPermission(user, Constants.AP_REP_PURCHASE_ORDER_PRINT_ID);
      	
      	if (frParam != null) {
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	/*******************************************************
      	 Initialize ApRepPurchaseOrderPrintController EJB
      	 *******************************************************/
      	
      	ApRepPurchaseOrderPrintControllerHome homePOP = null;
      	ApRepPurchaseOrderPrintController ejbPOP = null;       
      	
      	try {
      		
      		homePOP = (ApRepPurchaseOrderPrintControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/ApRepPurchaseOrderPrintControllerEJB", ApRepPurchaseOrderPrintControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in ApRepPurchaseOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbPOP = homePOP.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in ApRepPurchaseOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
      	/*******************************************************
      	 --  Go Action --
      	 *******************************************************/
      	
      	if(frParam != null) {
      		
			final String[] majorNames = {"", " Thousand", " Million",
			" Billion", " Trillion", " Quadrillion", " Quintillion"};
			
			final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
			" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
			
			final String[] numNames = {"", " One", " Two", " Three", " Four",
			" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
			" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
			" Nineteen"};
      		
      		AdCompanyDetails adCmpDetails = null;
      		ApRepPurchaseOrderPrintDetails details = null;
      		
      		ArrayList list = null;           
      		
      		String VIEW_TYP = "";
      		try {
      			
      			ArrayList poCodeList = new ArrayList();
      			
      			if (request.getParameter("forward") != null) {
      				
      				poCodeList.add(new Integer(request.getParameter("purchaseOrderCode")));
      				VIEW_TYP = request.getParameter("viewType");
      			}
      			
      			list = ejbPOP.executeApRepPurchaseOrderPrint(poCodeList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			
      			// get company
      			
      			adCmpDetails = ejbPOP.getAdCompany(user.getCmpCode());
      			
      			// get parameters
      			
      			Iterator i = list.iterator();
      			
      			while (i.hasNext()) {
      				
      				details = (ApRepPurchaseOrderPrintDetails)i.next();
      				
		            int num = (int)Math.floor(details.getPopPlTotalAmount());
					
					String str = Common.convertDoubleToStringMoney(details.getPopPlTotalAmount(), (short)2);
					str = str.substring(str.indexOf('.') + 1, str.length());
					details.setPopPlTotalAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
					str + "/100 Only");	
      				
      				
      			}
      			
      		} catch (GlobalNoRecordFoundException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("purchaseOrderPrint.error.noRecordFound"));
      			
      		} catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in ApRepPurchaseOrderPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}
      		
      		if(!errors.isEmpty()) {
      			
      			saveErrors(request, new ActionMessages(errors));
      			return(mapping.findForward("apRepPurchaseOrderPrint"));
      			
      		}	
      		
      		// fill report parameters, fill report to pdf and set report session
      		
      		Map parameters = new HashMap();
      		parameters.put("company", adCmpDetails.getCmpName());
      		parameters.put("createdBy", details.getPopPoCreatedBy());
      		parameters.put("approvedBy", details.getPopPoApprovedRejectedBy());
      		parameters.put("checkedBy", details.getPopPoCheckedBy());
      		parameters.put("lastPageNumber", list.size() % 20 == 0 ? new Integer(list.size() / 20) : new Integer((list.size() / 20) + 1));
      		
      		String filename = "/opt/ofs-resources/" +user.getCompany()+ "/ApRepPurchaseOrderPrint.jasper";	
       		
       		if (!new java.io.File(filename).exists()) {
       			
       			filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseOrderPrint.jasper");
       			
       		}
      	   
      		   
      	
      		try {
			    
		    Report report = new Report();
		    
		    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
			        		  new ApRepPurchaseOrderPrintDS(list)));   
				        
			   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
							   new ApRepPurchaseOrderPrintDS(list)));   
				        
			   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
						   new ApRepPurchaseOrderPrintDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       //actionForm.setReport(Constants.STATUS_SUCCESS);	
		    
      			
      		} catch(Exception ex) {
      			
      			if(log.isInfoEnabled()) {
      				
      				log.info("Exception caught in ApRepPurchaseOrderPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			} 
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}              
      		
      		return(mapping.findForward("apRepPurchaseOrderPrint"));
      		
      	} else {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));
      		
      		return(mapping.findForward("apRepPurchaseOrderPrintForm"));
      		
      	}
      	
      } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	  e.printStackTrace();
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepPurchaseOrderPrintAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
   
	 private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
		    String soFar;
		
		    if (number % 100 < 20){
		        soFar = numNames[number % 100];
		        number /= 100;
		       }
		    else {
		        soFar = numNames[number % 10];
		        number /= 10;
		
		        soFar = tensNames[number % 10] + soFar;
		        number /= 10;
		       }
		    if (number == 0) return soFar;
		    return numNames[number] + " Hundred" + soFar;
	 }
   
	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
	    /* special case */
	    if (number == 0) { return "zero"; }
	
	    String prefix = "";
	
	    if (number < 0) {
	        number = -number;
	        prefix = "Negative";
	      }
	
	    String soFar = "";
	    int place = 0;
	
	    do {
	      int n = number % 1000;
	      if (n != 0){
	         String s = this.convertLessThanOneThousand(n, tensNames, numNames);
	         soFar = s + majorNames[place] + soFar;
	        }
	      place++;
	      number /= 1000;
	      } while (number > 0);
	
	    return (prefix + soFar).trim();
	}    
}
