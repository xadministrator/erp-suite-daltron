package com.struts.jreports.ap.purchaseorderprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.purchaseorder.ApRepPurchaseOrderData;
import com.struts.util.Common;
import com.util.ApRepPurchaseOrderPrintDetails;

public class ApRepPurchaseOrderPrintDS implements JRDataSource{

	private ArrayList data = new ArrayList();

	private int index = -1;

	public ApRepPurchaseOrderPrintDS(ArrayList list) {

		Iterator i = list.iterator();

		while (i.hasNext()) {

			ApRepPurchaseOrderPrintDetails details = (ApRepPurchaseOrderPrintDetails)i.next();

			ApRepPurchaseOrderPrintData dtbData = new ApRepPurchaseOrderPrintData(
					Common.convertSQLDateToString(details.getPopPoDate()), Common.convertSQLDateToString(details.getPopPoDeliveryPeriod()), details.getPopPoSupplierName(),
					details.getPopPoContactPerson(), details.getPopPoContactNumber(),
					details.getPopPoDocumentNumber(), details.getPopPoReferenceNumber(),
					details.getPopPoDescription(), details.getPopPllIiName(), details.getPopPlIiDescription(),
					details.getPopPlIiPartNumber(), details.getPopPlIiBarCode1(), details.getPopPlIiBarCode2(), details.getPopPlIiBarCode3(), details.getPopPlIiBrand(),
					details.getPopPlLocName(), details.getPopPlPropertyCode(), details.getPopPlSerialNumber(), details.getPopPlSpecs(), details.getPopPlCustodian(), details.getPopPlExpiryDate(),
					new Double(details.getPopPlQuantity()), details.getPopPlUomName(),
					new Double(details.getPopPlUnitCost()), new Double(details.getPopPlAmount()), new Double(details.getPopPlTotalAmount()),
					new String(details.getPopPlTotalAmountInWords()), new Double(details.getPopPlEndingBalance()), details.getPopPoShipTo(),
					details.getPopPoPaymentTermName(), new Double(details.getPopPlDiscount1()),
					new Double(details.getPopPlDiscount2()), new Double(details.getPopPlDiscount3()),
					new Double(details.getPopPlDiscount4()), new Double(details.getPopPlTotalDiscount()),
					details.getPopPlDiscount(), details.getPopPoBillTo(), details.getPopPlUnitOfMeasureShortName(),
					new Double(details.getPopPlTaxAmount()), new Double(details.getPopPoTaxRate()),
					new Double(details.getPopPlUnitCostWoTax()), details.getPopPoTaxType(), details.getPopPoCurrency(),
					details.getPopPoSupplierAddress(), details.getPopPoSupplierCode(), new Double(details.getPopPlUnitCostTaxInclusive()),
					details.getPopPlBranchName(), details.getPopPlBranchCode(), details.getPopPlApprovedRejectedBy(), details.getPopPlSupplierPhoneNumber(), details.getPopPlSupplierFaxNumber(),
					details.getPopPoUserResponsibility(), details.getPopPoApprovalStatus(),
					details.getPopPoMisc1(),details.getPopPoMisc2(),details.getPopPoMisc3(),
					details.getPopPoMisc4(),details.getPopPoMisc5(),details.getPopPoMisc6()
					);
			data.add(dtbData);

		}

	}

	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}

	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;

		String fieldName = field.getName();

		if("date".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDate();
		} else if("deliveryPeriod".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDeliveryPeriod();
		} else if("supplierName".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getSupplierName();
		} else if("contactPerson".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getContactPerson();
		} else if("contactNumber".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getContactNumber();
		} else if("documentNumber".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDocumentNumber();
		} else if("referenceNumber".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getReferenceNumber();
		} else if("description".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDescription();
		} else if("itemName".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemName();
		} else if("itemDescription".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemDescription();
		} else if("itemPartNumber".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemPartNumber();
		} else if("itemBarCode1".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemBarCode1();
		} else if("itemBarCode2".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemBarCode2();
		} else if("itemBarCode3".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemBarCode3();
		} else if("itemBrand".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemBrand();
		} else if("itemLocation".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemLocation();
		} else if("itemPropertyCode".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemPropertyCode();
		} else if("itemSerialNumber".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemSerialNumber();
		} else if("itemSpecs".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemSpecs();
		} else if("itemCustodian".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemCustodian();
		} else if("itemExpiryDate".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getItemExpiryDate();
		} else if("quantity".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getQuantity();
		} else if("unit".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUnit();
		} else if("unitCost".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUnitCost();
		} else if("amount".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getAmount();
		} else if("totalAmount".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTotalAmount();
		} else if("totalAmountInWords".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTotalAmountInWords();
		} else if("endingBalance".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getEndingBalance();
		} else if("shipTo".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getShipTo();
		} else if("paymentTermName".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getPaymentTermName();
			System.out.println("paymentTermName" + value);
		} else if("discount1".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDiscount1();
		} else if("discount2".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDiscount2();
		} else if("discount3".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDiscount3();
		} else if("discount4".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDiscount4();
		} else if("totalDiscount".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTotalDiscount();
		} else if("discount".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getDiscount();
		} else if("billTo".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getBillTo();
		} else if("unitOfMeasureShortName".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUnitOfMeasureShortName();
		} else if("taxAmount".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTaxAmount();
		} else if("taxRate".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTaxRate();
		} else if("unitCostWoTax".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUnitCostWoTax();
		} else if("taxType".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getTaxType();
		} else if("currency".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getCurrency();

		} else if("supplierAddress".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getSupplierAddress();
		} else if("supplierCode".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getSupplierCode();
		} else if("unitCostTaxInclusive".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUnitCostTaxInclusive();
		} else if("branchCode".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getBranchCode();
		} else if("branchName".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getBranchName();
		} else if("approvedRejectedBy".equals(fieldName)) {
			value = ((ApRepPurchaseOrderPrintData)data.get(index)).getApprovedRejectedBy();
		} else if("supplierPhoneNumber".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getSupplierPhoneNumber();
	   	} else if("supplierFaxNumber".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getSupplierFaxNumber();
	   	} else if("userResponsibility".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getUserResponsibility();
	   	} else if("approvalStatus".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getApprovalStatus();
	   	} else if("misc1".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc1();
	   	} else if("misc2".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc2();
	   	} else if("misc3".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc3();
	   	} else if("misc4".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc4();
	   	} else if("misc5".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc5();
	   	} else if("misc6".equals(fieldName)) {
	   		value = ((ApRepPurchaseOrderPrintData)data.get(index)).getMisc6();
	   	}

		return(value);

	}

}
