package com.struts.jreports.ap.purchaseorderprint;

public class ApRepPurchaseOrderPrintData implements java.io.Serializable {

	private String date = null;
	private String deliveryPeriod = null;
	private String supplierName = null;
	private String contactPerson = null;
	private String contactNumber = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
	private String itemName = null;
	private String itemDescription = null;
	private String itemPartNumber = null;
	private String itemBarCode1 = null;
	private String itemBarCode2 = null;
	private String itemBarCode3 = null;
	private String itemBrand = null;

	private String itemLocation = null;

	private String itemPropertyCode = null;
	private String itemSerialNumber = null;
	private String itemSpecs = null;
	private String itemCustodian = null;
	private String itemExpiryDate = null;

	private Double quantity = null;
	private String unit = null;
	private Double unitCost = null;
	private Double amount = null;
	private Double totalAmount = null;
	private String totalAmountInWords = null;
	private Double endingBalance = null;
	private String shipTo = null;
	private String paymentTermName = null;
	private Double discount1 = null;
	private Double discount2 = null;
	private Double discount3 = null;
	private Double discount4 = null;
	private Double totalDiscount = null;
	private String discount = null;
	private String billTo = null;
	private String unitOfMeasureShortName = null;
	private Double taxAmount = null;
	private Double taxRate = null;
	private Double unitCostWoTax = null;
	private String taxType = null;
	private String currency = null;
	private String supplierAddress = null;
	private String supplierCode = null;
	private Double unitCostTaxInclusive = null;
	private String branchName = null;
	private String branchCode = null;
	private String approvedRejectedBy = null;
	private String supplierPhoneNumber = null;
	private String supplierFaxNumber = null;
	private String userResponsibility = null;
	private String approvalStatus = null;
	private String misc1 = null;
	private String misc2 = null;
	private String misc3 = null;
	private String misc4 = null;
	private String misc5 = null;
	private String misc6 = null;

	public ApRepPurchaseOrderPrintData(String date, String deliveryPeriod, String supplierName, String contactPerson, String contactNumber,
			String documentNumber, String referenceNumber, String description, String itemName, String itemDescription,
			String itemPartNumber, String itemBarCode1, String itemBarCode2, String itemBarCode3, String itemBrand,
			String itemLocation, String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,
			Double quantity, String unit, Double unitCost,
			Double amount, Double totalAmount, String totalAmountInWords, Double endingBalance, String shipTo, String paymentTermName, Double discount1,
			Double discount2, Double discount3, Double discount4, Double totalDiscount, String discount,
			String billTo, String unitOfMeasureShortName, Double taxAmount, Double taxRate, Double unitCostWoTax,
			String taxType, String currency, String supplierAddress, String supplierCode, Double unitCostTaxInclusive,
			String branchName, String branchCode, String approvedRejectedBy, String supplierPhoneNumber, String supplierFaxNumber,
			String userResponsibility, String approvalStatus,
			String misc1, String misc2, String misc3,
			String misc4, String misc5, String mis6
			) {

		this.date = date;
		this.deliveryPeriod = deliveryPeriod;
		this.supplierName = supplierName;
		this.contactPerson = contactPerson;
		this.contactNumber = contactNumber;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.description = description;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemPartNumber = itemPartNumber;
		this.itemBarCode1 = itemBarCode1;
		this.itemBarCode2 = itemBarCode2;
		this.itemBarCode3 = itemBarCode3;
		this.itemBrand = itemBrand;
		this.itemLocation = itemLocation;
		this.itemPropertyCode = itemPropertyCode;
		this.itemSerialNumber = itemSerialNumber;
		this.itemSpecs = itemSpecs;
		this.itemCustodian = itemCustodian;
		this.itemExpiryDate = itemExpiryDate;
		this.quantity = quantity;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.totalAmount = totalAmount;
		this.totalAmountInWords = totalAmountInWords;
		this.endingBalance = endingBalance;
		this.shipTo = shipTo;
		this.paymentTermName = paymentTermName;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.discount = discount;
		this.billTo = billTo;
		this.unitOfMeasureShortName = unitOfMeasureShortName;
		this.taxAmount = taxAmount;
		this.taxRate = taxRate;
		this.unitCostWoTax = unitCostWoTax;
		this.taxType = taxType;
		this.currency = currency;
		this.supplierAddress = supplierAddress;
		this.supplierCode = supplierCode;
		this.unitCostTaxInclusive = unitCostTaxInclusive;
		this.branchName = branchName;
		this.branchCode = branchCode;
		this.approvedRejectedBy = approvedRejectedBy;
		this.supplierPhoneNumber = supplierPhoneNumber;
		this.supplierFaxNumber = supplierFaxNumber;
		this.userResponsibility = userResponsibility;
		this.approvalStatus = approvalStatus;
		this.misc1 = misc1;
		this.misc2 = misc2;
		this.misc3 = misc3;
		this.misc4 = misc4;
		this.misc5 = misc5;
		this.misc6 = misc6;
	}

	public String getDate() {

		return date;

	}


	public String getDeliveryPeriod() {

		return deliveryPeriod;

	}

	public String getSupplierName() {

		return supplierName;

	}

	public String getContactPerson() {

		return contactPerson;

	}

	public String getContactNumber() {

		return contactNumber;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getDescription(){

		return description;
	}


	public String getItemName() {

		return itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public String getItemPartNumber() {

		return itemPartNumber;

	}

	public String getItemBarCode1() {

		return itemBarCode1;

	}

	public String getItemBarCode2() {

		return itemBarCode2;

	}


	public String getItemBarCode3() {

		return itemBarCode3;

	}


	public String getItemBrand() {

		return itemBrand;

	}
	public String getItemLocation() {

		return itemLocation;

	}

	public String getItemPropertyCode() {

		return itemPropertyCode;

	}

	public String getItemSerialNumber() {

		return itemSerialNumber;

	}

	public String getItemSpecs() {

		return itemSpecs;

	}

	public String getItemCustodian() {

		return itemCustodian;

	}


	public String getItemExpiryDate() {

		return itemExpiryDate;

	}


	public Double getQuantity() {

		return quantity;

	}

	public String getUnit() {

		return unit;

	}


	public Double getUnitCost() {

		return unitCost;

	}


	public Double getAmount() {

		return amount;

	}

	public Double getTotalAmount() {

		return totalAmount;

	}

	public String getTotalAmountInWords() {

		return totalAmountInWords;

	}

	public Double getEndingBalance() {

		return endingBalance;

	}

	public String getShipTo() {

		return shipTo;

	}

	public String getPaymentTermName() {

		return paymentTermName;

	}

	public Double getDiscount1() {

		return discount1;

	}

	public Double getDiscount2() {

		return discount2;

	}

	public Double getDiscount3() {

		return discount3;

	}

	public Double getDiscount4() {

		return discount4;

	}

	public Double getTotalDiscount() {

		return totalDiscount;

	}

	public String getDiscount() {

		return discount;

	}

	public String getBillTo() {

		return billTo;

	}

	public String getUnitOfMeasureShortName() {

		return unitOfMeasureShortName;

	}

	public Double getTaxAmount() {

		return taxAmount;

	}

	public Double getTaxRate() {

		return taxRate;

	}

	public Double getUnitCostWoTax() {

		return unitCostWoTax;

	}

	public String getTaxType() {

		return taxType;

	}

public String getCurrency() {

		return currency;

	}

	public String getSupplierAddress() {

		return supplierAddress;

	}

	public String getSupplierCode() {

		return supplierCode;

	}

	public Double getUnitCostTaxInclusive() {

		return unitCostTaxInclusive;

	}
	public String getBranchCode() {

		return branchCode;

	}

	public String getBranchName() {

		return branchName;

	}

	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public String getSupplierPhoneNumber() {

		return supplierPhoneNumber;

	}

	public String getSupplierFaxNumber() {

		return supplierFaxNumber;

	}

	public String getUserResponsibility() {

		return userResponsibility;

	}


	public String getApprovalStatus() {

		return approvalStatus;

	}

	public String getMisc1() {

			return misc1;

		}

	public String getMisc2() {

		return misc2;

	}

	public String getMisc3() {

		return misc3;

	}

	public String getMisc4() {

			return misc4;

		}

	public String getMisc5() {

		return misc5;

	}

	public String getMisc6() {

		return misc6;

	}
}
