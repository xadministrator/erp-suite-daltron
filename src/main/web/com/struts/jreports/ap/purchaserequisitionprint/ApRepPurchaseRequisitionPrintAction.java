package com.struts.jreports.ap.purchaserequisitionprint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepPurchaseRequisitionPrintController;
import com.ejb.txn.ApRepPurchaseRequisitionPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApRepPurchaseRequisitionPrintDetails;

public class ApRepPurchaseRequisitionPrintAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 Check if user has a session
 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApRepPurchaseRequisitionPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApRepPurchaseRequisitionPrintForm actionForm = (ApRepPurchaseRequisitionPrintForm)form; 
			
			String frParam= Common.getUserPermission(user, Constants.AP_REP_PURCHASE_REQUISITION_PRINT_ID);
			
			if (frParam != null) {
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
			}
			
/*******************************************************
 Initialize ApRepPurchaseRequisitionPrintController EJB
 *******************************************************/
			
			ApRepPurchaseRequisitionPrintControllerHome homePRP = null;
			ApRepPurchaseRequisitionPrintController ejbPRP = null;       
			
			try {
				
				homePRP = (ApRepPurchaseRequisitionPrintControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/ApRepPurchaseRequisitionPrintControllerEJB", ApRepPurchaseRequisitionPrintControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApRepPurchaseRequisitionPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbPRP = homePRP.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApRepPurchaseRequisitionPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report report = new Report();
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
 -- TO DO getPrByPrCode()
 *******************************************************/       
			
			if (frParam != null) {
				
				AdCompanyDetails adCmpDetails = null;
				ArrayList list = null;   
				ApRepPurchaseRequisitionPrintDetails details = null;
				boolean isService = false; 
				
				try {
					
					ArrayList prCodeList = new ArrayList();
					System.out.println(request.getParameter("forward") + "<== forward, quotation? ==>" + request.getParameter("quotation"));
					if (request.getParameter("forward") != null) {
						
						prCodeList.add(new Integer(request.getParameter("purchaseRequisitionCode")));
						
					} else {
						
						int i = 0;
						
						while (true) {
							
							if (request.getParameter("purchaseRequisitionCode[" + i + "]") != null) {
								
								prCodeList.add(new Integer(request.getParameter("purchaseRequisitionCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
						
					}
					
					list = ejbPRP.executeApRepPurchaseRequisitionPrint(prCodeList, user.getCmpCode()); 
					
					adCmpDetails = ejbPRP.getAdCompany(user.getCmpCode());
					
					// get parameters
	      			
	      			Iterator i = list.iterator();
	      			
	      			while (i.hasNext()) {
	      				
	      				details = (ApRepPurchaseRequisitionPrintDetails)i.next();
	      				isService = details.getPrpPrIsService();
	      			}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("purchaseRequisitionPrint.error.noRecordFound"));  
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApRepPurchaseRequisitionPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apRepPurchaseRequisitionPrint");
					
				}
				
				/** fill report parameters, fill report to pdf and set report session **/ 	 
				if (request.getParameter("quotation") == null ){
					String nullString = null;
					System.out.println("purchase requisition");
					Map parameters = new HashMap();
					parameters.put("company", adCmpDetails.getCmpName());
					
					String filename = "" ;// "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionPrint.jasper";
					
					if (isService){
		      			System.out.println("Service");
		      			
		      			SimpleDateFormat df = new SimpleDateFormat("dd MMMMM, yyyy");
		      			java.util.Date now = new java.util.Date();
		      			
		      			System.out.println(df.format(now) + "<== sample date format");
		      			
		      			
		      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionPrintServices.jasper";
		          		
		          		if (!new java.io.File(filename).exists()) {
	
		      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionPrint.jasper");
	
		      			}
		      		}else{
		      			System.out.println("Normal");
		      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionPrint.jasper";
		      			          		
		          		if (!new java.io.File(filename).exists()) {
	
		      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionPrint.jasper");
	
		      			}
		      		}
					
					
					try {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
								new ApRepPurchaseRequisitionPrintDS(list)));   				     
						
					} catch (Exception ex) {
						
						ex.printStackTrace();
						
						if(log.isInfoEnabled()) {
							
							log.info("Exception caught in ApRepPurchaseRequisitionPrintAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							
						} 
						
						return(mapping.findForward("cmnErrorPage"));
						
					}              
				}else{
					String nullString = null;
					
					Map parameters = new HashMap();
					parameters.put("company", adCmpDetails.getCmpName());
					
					String filename = "" ;// "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionPrint.jasper";
					
					
		      		
	      			System.out.println("quotation");
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepQuotationRequestOpenQuotation.jasper";
	      			          		
	          		if (!new java.io.File(filename).exists()) {

	      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionPrint.jasper");

	      			}
	      		
					
					
					try {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
								new ApRepPurchaseRequisitionPrintDS(list)));   				     
						
					} catch (Exception ex) {
						
						ex.printStackTrace();
						
						if(log.isInfoEnabled()) {
							
							log.info("Exception caught in ApRepPurchaseRequisitionPrintAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							
						} 
						
						return(mapping.findForward("cmnErrorPage"));
						
					}
				}
				session.setAttribute(Constants.REPORT_KEY, report);
				
				return(mapping.findForward("apRepPurchaseRequisitionPrint"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("apRepPurchaseRequisitionPrint"));
				
			}
			
		} catch(Exception e) {
			
			e.printStackTrace();
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in ApRepPurchaseRequisitionPrintAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}        
			return(mapping.findForward("cmnErrorPage"));        
		}
	}
	
}