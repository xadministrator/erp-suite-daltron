/*
 * opt/jreport/ApRepPurchaseRequisitionPrintDS.java
 *
 * Created on April 24, 2006, 10:00 AM
 *
 * @author  Aliza D.J. Anos
 */

package com.struts.jreports.ap.purchaserequisitionprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepPurchaseRequisitionPrintDetails;

public class ApRepPurchaseRequisitionPrintDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepPurchaseRequisitionPrintDS(ArrayList list){

   		Iterator i = list.iterator();

	   	while (i.hasNext()) {

	   		ApRepPurchaseRequisitionPrintDetails mdetails = (ApRepPurchaseRequisitionPrintDetails) i.next();

	   		ApRepPurchaseRequisitionPrintData prData = null;

	   		prData = new ApRepPurchaseRequisitionPrintData(mdetails.getPrpPrNumber(), mdetails.getPrpPrDate(), mdetails.getPrpPrDeliveryPeriod(),
	   				mdetails.getPrpPrDescription(), String.valueOf(mdetails.getPrpFcSymbol()), mdetails.getPrpPrlIiName(), mdetails.getPrpPrlIiDescription(),
	   				mdetails.getPrpPrlIiPartNumber(), mdetails.getPrpPrlIiBarCode1(), mdetails.getPrpPrlIiBarCode2(), mdetails.getPrpPrlIiBarCode3(), mdetails.getPrpPrlIiBrand(),
	   				mdetails.getPrpPrlPropertyCode(), mdetails.getPrpPrlSerialNumber(), mdetails.getPrpPrlSpecs(), mdetails.getPrpPrlCustodian(), mdetails.getPrpPrlExpiryDate(),
	   				mdetails.getPrpPrlLocName(), new Double(mdetails.getPrpPrlQuantity()), new Double(mdetails.getPrpPrlGrandTotalAmount()),
					mdetails.getPrpPrlUomShortName(), new Double(mdetails.getPrpPrlAmount()),mdetails.getPrpPrlApprovedBy(),new Double(mdetails.getPrpPrlUnitCost()),mdetails.getPrpPrDepartment(),
					mdetails.getPrpPrMisc1(),mdetails.getPrpPrMisc2(),mdetails.getPrpPrMisc3(),
					mdetails.getPrpPrMisc4(),mdetails.getPrpPrMisc5(),mdetails.getPrpPrMisc6()
	   				);

		    data.add(prData);

		}

   }

   public boolean next() throws JRException{

      index++;
      return (index < data.size());

   }

   public Object getFieldValue(JRField field) throws JRException{

      Object value = null;

      String fieldName = field.getName();

      if("documentNumber".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getDocumentNumber();
      } else if("date".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getDate();
      } else if("deliveryPeriod".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getDeliveryPeriod();
      } else if("description".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getDescription();
      } else if("currencySymbol".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getCurrencySymbol();
      } else if("itemName".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemName();
      } else if("itemDescription".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemDescription();
      }	else if("itemPartNumber".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemPartNumber();
      }	else if("itemBarCode1".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemBarCode1();
      } else if("itemBarCode2".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemBarCode2();
      } else if("itemBarCode3".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemBarCode3();
      } else if("itemBrand".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemBrand();
      }	else if("itemProperyCode".equals(fieldName)) {
	    value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemPropertyCode();
      } else if("itemSerialNumber".equals(fieldName)) {
	    value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemSerialNumber();
      } else if("itemSpecs".equals(fieldName)) {
	    value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemSpecs();
      } else if("itemCustodian".equals(fieldName)) {
	    value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemCustodian();
      } else if("itemExpiryDate".equals(fieldName)) {
	    value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getItemExpiryDate();
      } else if("quantity".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getQuantity();
      } else if("grandTotalAmount".equals(fieldName)) {
    	value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getGrandTotalAmount();
      } else if("uomShortName".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getUomShortName();
      } else if("amount".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getAmount();
      } else if("approvedBy".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getApprovedBy();
      } else if("unitCost".equals(fieldName)) {
    	value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getUnitCost();
      } else if("department".equals(fieldName)) {
        value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getDepartment();
      } else if("misc1".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc1();
      } else if("misc2".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc2();
      } else if("misc3".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc3();
      } else if("misc4".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc4();
      } else if("misc5".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc5();
      } else if("misc6".equals(fieldName)) {
          value = ((ApRepPurchaseRequisitionPrintData)data.get(index)).getMisc6();
       }

      return(value);
   }
}
