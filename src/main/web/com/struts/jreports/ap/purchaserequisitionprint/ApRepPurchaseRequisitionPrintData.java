package com.struts.jreports.ap.purchaserequisitionprint;

import java.util.Date;

public class ApRepPurchaseRequisitionPrintData implements java.io.Serializable {

   private String supplier = null;
   private String supplierAddress = null;

   private String documentNumber = null;
   private Date date = null;
   private Date deliveryPeriod = null;
   private String description = null;
   private String currencySymbol = null;
   private String itemName = null;
   private String itemDescription = null;
   private String itemPartNumber = null;
   private String itemBarCode1 = null;
   private String itemBarCode2 = null;
   private String itemBarCode3 = null;
   private String itemBrand = null;

   private String itemPropertyCode = null;
   private String itemSerialNumber = null;
   private String itemSpecs = null;
   private String itemCustodian = null;
   private String itemExpiryDate = null;

   private String location = null;
   private Double quantity = null;
   private String uomShortName = null;
   private Double amount = null;
   private String approvedBy = null;
   private Double unitCost = null;
   private String department = null;
   private Double grandTotalAmount = null;
   private String misc1 = null;
   private String misc2 = null;
   private String misc3 = null;
   private String misc4 = null;
   private String misc5 = null;
   private String misc6 = null;


   public ApRepPurchaseRequisitionPrintData(
   		String documentNumber, Date date, Date deliveryPeriod, String description, String currencySymbol, String itemName, String itemDescription,
   		String itemPartNumber, String itemBarCode1, String itemBarCode2, String itemBarCode3, String itemBrand,
		String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,
   		String location, Double quantity, Double grandTotalAmount, String uomShortName, Double amount,String approvedBy,Double unitCost,String department,
		String misc1, String misc2, String misc3,
		String misc4, String misc5, String misc6
		   ) {

      	this.documentNumber = documentNumber;
      	this.date = date;
      	this.deliveryPeriod = deliveryPeriod;
      	this.description = description;
      	this.currencySymbol = currencySymbol;
      	this.itemName = itemName;
      	this.itemDescription = itemDescription;
      	this.itemPartNumber = itemPartNumber;
      	this.itemBarCode1 = itemBarCode1;
      	this.itemBarCode2 = itemBarCode2;
      	this.itemBarCode3 = itemBarCode3;
      	this.itemBrand = itemBrand;
      	this.itemPropertyCode = itemPropertyCode;
      	this.itemSerialNumber = itemSerialNumber;
      	this.itemSpecs = itemSpecs;
      	this.itemCustodian = itemCustodian;
      	this.itemExpiryDate = itemExpiryDate;
      	this.location = location;
      	this.quantity = quantity;
      	this.grandTotalAmount = grandTotalAmount;
      	this.uomShortName = uomShortName;
      	this.amount = amount;
      	this.approvedBy= approvedBy;
      	this.unitCost= unitCost;
      	this.department =department;
      	this.misc1 = misc1;
      	this.misc2 = misc2;
      	this.misc3 = misc3;
      	this.misc4 = misc4;
      	this.misc5 = misc5;
      	this.misc6 = misc6;

   }

   public String getDocumentNumber() {

   		return documentNumber;

   }

   public Date getDate() {

   		return date;

   }

   public Date getDeliveryPeriod() {

  		return deliveryPeriod;

  }

   public String getDescription() {

   		return description;

   }

   public String getCurrencySymbol() {

   		return currencySymbol;

   }

   public String getItemName() {

   		return itemName;

   }

   public String getItemDescription() {

   		return itemDescription;

   }

   public String getItemPartNumber() {

  		return itemPartNumber;

  }

   public String getItemBarCode1() {

  		return itemBarCode1;

  }

   public String getItemBarCode2() {

 		return itemBarCode2;

 }

   public String getItemBarCode3() {

 		return itemBarCode3;

 }


   public String getItemBrand() {

 		return itemBrand;

  }

   public String getItemPropertyCode() {

		return itemPropertyCode;

}

   public String getItemSerialNumber() {

		return itemSerialNumber;

}

   public String getItemSpecs() {

		return itemSpecs;

}

   public String getItemCustodian() {

		return itemCustodian;

}


   public String getItemExpiryDate() {

		return itemExpiryDate;

}

   public String getLocation() {

   		return location;

   }

   public Double getQuantity() {

   		return quantity;

   }

   public String getUomShortName() {

   		return uomShortName;

   }

   public Double getAmount() {

   		return amount;

   }
   public String getApprovedBy() {

		return approvedBy;

	}
   public double getUnitCost() {

		return unitCost;

   }
   public double getGrandTotalAmount() {

		return grandTotalAmount;

  }

   public String getDepartment() {

		return department;

   }

   public String getMisc1() {

		return misc1;

  }

   public String getMisc2() {

		return misc2;

  }

   public String getMisc3() {

		return misc3;

  }

   public String getMisc4() {

		return misc4;

 }

   public String getMisc5() {

		return misc5;

 }

   public String getMisc6() {

		return misc6;

 }

}
