package com.struts.jreports.ap.purchaserequisitionregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepPurchaseRequisitionRegisterController;
import com.ejb.txn.ApRepPurchaseRequisitionRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepPurchaseRequisitionRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("ApRepPurchaseRequisitionRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	ApRepPurchaseRequisitionRegisterForm actionForm = (ApRepPurchaseRequisitionRegisterForm)form;
      	
      	// reset report to null
      	actionForm.setReport(null);
      	
      	String frParam = Common.getUserPermission(user, Constants.AP_REP_PURCHASE_REQUISITION_REGISTER_ID);
      	
      	if (frParam != null) {
      		
      		if (frParam.trim().equals(Constants.FULL_ACCESS)) {
      			
      			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
      			if (!fieldErrors.isEmpty()) {
      				
      				saveErrors(request, new ActionMessages(fieldErrors));
      				
      				return mapping.findForward("apRepPurchaseRequisitionRegister");
      			}
      			
      		}
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	
/*******************************************************
   Initialize InvRepAdjustmentController EJB
*******************************************************/
      	
      	ApRepPurchaseRequisitionRegisterControllerHome homePR = null;
      	ApRepPurchaseRequisitionRegisterController ejbPR = null;       
      	
      	try {
      		
      		homePR = (ApRepPurchaseRequisitionRegisterControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/ApRepPurchaseRequisitionRegisterControllerEJB", ApRepPurchaseRequisitionRegisterControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in ApRepPurchaseRequistionRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbPR = homePR.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in ApRepPurchaseRequisitionRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
/*******************************************************
   -- AP PR REGISTER Go Action --
*******************************************************/

      	if(request.getParameter("goButton") != null &&
      			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      		
      		
      		if	(actionForm.getReportType().equals("Prod List")){
	      		
      		} else if(actionForm.getReportType().equals("RM Used")){
      			
      		}else{
      			System.out.println("Report Type NULL");
      			ArrayList list = null;
	      		String company = null;
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			if (!Common.validateRequired(actionForm.getType()) &&  !(actionForm.getType().equals("NO RECORD FOUND")) ) {
	      				criteria.put("type", actionForm.getType());
	      			}
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getApRepBrArListSize(); j++) {
	      			ApRepPurchaseRequisitionRegisterBranchList apRepPrchsRqstnBrList = (ApRepPurchaseRequisitionRegisterBranchList)actionForm.getApRepBrArByIndex(j);
	      			if(apRepPrchsRqstnBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(apRepPrchsRqstnBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbPR.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbPR.executeApRepPurchaseRequisitionRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(), adBrnchList,"BLANK", user.getCmpCode());
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in ApRepPurchaseRequisitionRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("apRepPurchaseRequisitionRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		System.out.println("4");
	      		parameters.put("type", actionForm.getType());
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		if (actionForm.getShowLineItems()) {
	      			parameters.put("showLineItems", "YES");
	      		} else {
	      			parameters.put("showLineItems", "NO");
	      		}
	      		
	      		parameters.put("orderBy", actionForm.getOrderBy());
	      		parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getApRepBrArListSize(); j++) {
	      			ApRepPurchaseRequisitionRegisterBranchList brList = (ApRepPurchaseRequisitionRegisterBranchList)actionForm.getApRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
				      		if (actionForm.getShowLineItems() && !actionForm.getGroupBy().equals("ITEM NAME")) {
				      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionRegisterLineItems.jasper";
				      			System.out.println("filename  = "+filename);
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionRegisterLineItems.jasper");
				      			}
				      		}  else  if (actionForm.getShowLineItems() && actionForm.getGroupBy().equals("ITEM NAME")){
				      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionRegisterLineItemsGroupByItems.jasper";
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionRegisterLineItemsGroupByItems.jasper");
				      			}
				      		} else {
				      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepPurchaseRequisitionRegister.jasper";
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/ApRepPurchaseRequisitionRegister.jasper");
				      			}
				      		}

				      		try {
				      			Report report = new Report();
				      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				      				report.setBytes(
				      						JasperRunManager.runReportToPdf(filename, parameters, 
				      								new ApRepPurchaseRequisitionRegisterDS(list,actionForm.getGroupBy())));   
				      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
				      				report.setBytes(
				      						JasperRunManagerExt.runReportToXls(filename, parameters, 
				      								new ApRepPurchaseRequisitionRegisterDS(list, actionForm.getGroupBy())));   
				      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				      						new ApRepPurchaseRequisitionRegisterDS(list, actionForm.getGroupBy())));												    
				      			}
				      			session.setAttribute(Constants.REPORT_KEY, report);
				      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
				      		} catch(Exception ex) {
				      			if(log.isInfoEnabled()) {
				      				log.info("Exception caught in ApRepPurchaseRequisitionRegisterAction.execute(): " + ex.getMessage() +
				      						" session: " + session.getId());
				      			}
				      			return(mapping.findForward("cmnErrorPage"));
				      		}
      		
								
      		
      		
      		}    		        	          
/*******************************************************
   -- AP PR REGISTER Close Action --
*******************************************************/
		    
      	} else if (request.getParameter("closeButton") != null) {
      		
      		return(mapping.findForward("cmnMain"));
         	
/*******************************************************
   -- AP PR REGISTER Load Action --
*******************************************************/
      		
      	}
      	
      	if(frParam != null) {
      		
      		try {
      			
      			actionForm.reset(mapping, request);
      			
      			ArrayList list = null;
      			Iterator i = null;
      			
      			actionForm.clearApRepBrArList();
      			
      			list = ejbPR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
      			
      			i = list.iterator();
      			//int ctr = 1;
      			while(i.hasNext()) {
      				
      				AdBranchDetails details = (AdBranchDetails)i.next();
      				
      				ApRepPurchaseRequisitionRegisterBranchList apRepPrchsRqstnBrList = new ApRepPurchaseRequisitionRegisterBranchList(actionForm,
      						details.getBrCode(),
							details.getBrBranchCode(), details.getBrName());
      				if (details.getBrHeadQuarter() == 1) apRepPrchsRqstnBrList.setBranchCheckbox(true);
      				//ctr++;
      				
      				actionForm.saveApRepBrArList(apRepPrchsRqstnBrList);
      				
      			}
      			
      			actionForm.clearTypeList();

            	list = ejbPR.getAdLvPurchaseRequisitionType(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		actionForm.setTypeList(Constants.GLOBAL_BLANK);
            		while (i.hasNext()) {
            			
            		    actionForm.setTypeList((String)i.next());
            			
            		}
            		
            		            		
            	}
      			
      		} catch (GlobalNoRecordFoundException ex) {
      			
      		} catch (EJBException ex) {
      			
      			if (log.isInfoEnabled()) {
      				
      				log.info("EJBException caught in ApRepPurchaseRequisitionRegisterAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				return mapping.findForward("cmnErrorPage"); 
      				
      			}
      			
      		} 
      		
      		return(mapping.findForward("apRepPurchaseRequisitionRegister"));		          
      		
      	} else {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));
      		
      		return(mapping.findForward("cmnMain"));
      		
      	}
      	
      } catch(Exception e) { 
      	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	if(log.isInfoEnabled()) {
      		
      		log.info("Exception caught in ApRepPurchaseRequisitionRegisterAction.execute(): " + e.getMessage()
      				+ " session: " + session.getId());
      		
      	}   
      	
      	return(mapping.findForward("cmnErrorPage"));   
      	
      }
   }
}
