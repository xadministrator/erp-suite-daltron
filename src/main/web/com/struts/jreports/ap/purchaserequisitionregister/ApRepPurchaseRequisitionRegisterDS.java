package com.struts.jreports.ap.purchaserequisitionregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepPurchaseRequisitionRegisterDetails;

public class ApRepPurchaseRequisitionRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepPurchaseRequisitionRegisterDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	ApRepPurchaseRequisitionRegisterDetails details = (ApRepPurchaseRequisitionRegisterDetails)i.next();
             
         String group = new String("");
         
         if (groupBy.equals("ITEM NAME")) {
         	
         	group = details.getPrItemName();
         	
         } else if (groupBy.equals("DATE")) {
         	
         	group = details.getPrDate().toString();
         	
         } 
         
         ApRepPurchaseRequisitionRegisterData argData = new ApRepPurchaseRequisitionRegisterData(details.getPrDate(), 
	   	     details.getPrDescription(), details.getPrReferenceNumber(), 
	   		 new Double(details.getPrAmount()), group, details.getPrDocumentNumber(), details.getPrType(),details.getPrAccountDescription(),
			 details.getPrItemName(), details.getPrItemDesc(), details.getPrDepartment(), details.getPrApSupplier(), details.getPrApPoNumber(), details.getPrPoDate(), details.getPrLocation(),
			 new Double (details.getPrQuantity()), details.getPrUnit(), new Double(details.getPrUnitCost()));
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getDate();       
   	}else if("description".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getDescription();
   	}else if("referenceNumber".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getReferenceNumber();
   	}else if("amount".equals(fieldName)){
   		 
   		value = Math.abs(((ApRepPurchaseRequisitionRegisterData)data.get(index)).getAmount()); 
   	}else if("groupBy".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getGroupBy();
   	}else if("documentNumber".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getDocumentNumber();
   	}else if("type".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getType();
   	}else if("itemName".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getItemName();
   		System.out.print(value+"item");
   	}else if("supplier".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getSupplier();
   	}else if("itemDesc".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getItemDesc();
   	}else if("department".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getDepartment();
   	}else if("poNumber".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getPoNumber();
   	}else if("poDate".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getPoDate();
   	}else if("location".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getLocation();
   	}else if("quantity".equals(fieldName)){
   		value = Math.abs(((ApRepPurchaseRequisitionRegisterData)data.get(index)).getQuantity()); 
   		 
   	}else if("unit".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getUnit();
   	}else if("unitCost".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getUnitCost();
   	}else if("accountDescription".equals(fieldName)){
   		value = ((ApRepPurchaseRequisitionRegisterData)data.get(index)).getAccountDescription();
   		System.out.print(value+"Account description");
   	}
   	return(value);
   }
}
