package com.struts.jreports.ap.purchaserequisitionregister;

public class ApRepPurchaseRequisitionRegisterData implements java.io.Serializable{
	
	   private java.util.Date date = null;
	   private String description = null;
	   private String referenceNumber = null;
	   private Double amount = null;
	   private String groupBy = null;
	   private String documentNumber = null;
	   private String type = null;
	   private String accountDescription = null;
	   private String itemName = null;
	   private String itemDesc = null;
	   private String department = null;
	   private String supplier = null;
	   private String poNumber = null;
	   private java.util.Date poDate = null;
	   private String location = null;
	   private Double quantity = null;
	   private String unit = null;
	   private Double unitCost = null;
	   
	   public ApRepPurchaseRequisitionRegisterData(java.util.Date date, String description, String referenceNumber,  
	   		Double amount, String groupBy, String documentNumber, String type,String accountDescription, String itemName, 
	   		String itemDesc, String department, String supplier, String poNumber, java.util.Date poDate,  String location, Double quantity, String unit, Double unitCost) {
	      	
	      	this.date = date;
	       	this.description = description;
	       	this.referenceNumber = referenceNumber;
	      	this.amount = amount;
	      	this.groupBy = groupBy;
	      	this.documentNumber = documentNumber; 
	      	this.type = type;
	      	this.accountDescription = accountDescription;
	      	this.department = department;
	      	this.supplier = supplier;
	      	this.poNumber = poNumber;
	      	this.poDate = poDate;
	      	this.itemName = itemName;
	      	this.itemDesc = itemDesc;
	      	this.location = location;
	      	this.quantity = quantity;
	      	this.unit = unit;
	      	this.unitCost = unitCost;
	      		      	
	   }

	   public java.util.Date getDate() {
	   	
	      return date;
	      
	   }
	   
	   public String getDescription() {
	   	
	   	  return description;
	   	  
	   }
	   
	   public String getReferenceNumber() {
	   	
	   	  return referenceNumber;

	   }
	      
	   public Double getAmount() {
	   	
	   	  return amount;
	   	  
	   }
	   
	   
	   public String getGroupBy() {
	   	
	   	  return groupBy;
	   	  
	   }
	   
	   public String getDocumentNumber() {
	   	
	   	  return documentNumber;
	   	
	   }
	   
	   public String getType() {
	   	
	   	  return type;
	   	  
	   }
	   public String getAccountDescription() {
		   	
		   	  return accountDescription;
		   	  
		   }
	   
	   public String getItemName() {
	   	
	   	  return itemName;
	   	  
	   }
	   
	   public String getItemDesc() {
		   
		   return itemDesc;
		   
	   }
	   
	   public String getLocation() {
	   	
	   	return location;
	   
	   }

	   public Double getQuantity() {
	   	
	   	return quantity;
	   
	   }
	   
	   public String getUnit() {
	   	
	   	return unit;
	   	
	   }
	   
	   public Double getUnitCost() {
	   	
	   	return unitCost;
	   	
	   }
	   
	   public String getDepartment() {
		   
		   return department;
	   }
	   
	   public String getSupplier() {
		   
		   return supplier;
	   }
	   
	   public String getPoNumber() {
		   
		   return poNumber;
	   }
	   
	   public java.util.Date getPoDate() {
		   	
		      return poDate;
		      
	   }

	 
}
