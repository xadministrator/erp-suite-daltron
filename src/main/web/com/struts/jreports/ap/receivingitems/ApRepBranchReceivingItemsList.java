package com.struts.jreports.ap.receivingitems;

import java.io.Serializable;

public class ApRepBranchReceivingItemsList implements Serializable {
	
	private String brBranchCode = null;
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;	
	
	private ApRepReceivingItemsForm parentBean;
	
	public ApRepBranchReceivingItemsList(ApRepReceivingItemsForm parentBean, Integer brCode, 
			String brBranchCode, String brName) {
		
		this.parentBean = parentBean;
		this.brCode = brCode;
		this.brName = brName;				
		this.brBranchCode = brBranchCode;
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void setBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}