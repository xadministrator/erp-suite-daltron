package com.struts.jreports.ap.receivingitems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepReceivingItemsController;
import com.ejb.txn.ApRepReceivingItemsControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepReceivingItemsAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
			Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApRepReceivingItemsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApRepReceivingItemsForm actionForm = (ApRepReceivingItemsForm)form;
			
			// reset report to null
			actionForm.setReport(null);
			
			String frParam = Common.getUserPermission(user, Constants.AP_REP_RECEIVING_ITEMS_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("apRepReceivingItems");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
/*******************************************************
 			Initialize ApRepReceivingItemsController EJB
*******************************************************/
			
			ApRepReceivingItemsControllerHome homeRR = null;
			ApRepReceivingItemsController ejbRR = null;       
			
			try {
				
				homeRR = (ApRepReceivingItemsControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ApRepReceivingItemsControllerEJB", ApRepReceivingItemsControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApRepReceivingItemsAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbRR = homeRR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApRepReceivingItemsAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
			-- AP RR Go Action --
*******************************************************/
			
			if(request.getParameter("goButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArrayList list = null;
				
				String company = null;
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();            	
					
					if (!Common.validateRequired(actionForm.getSupplierCode())) {
						
						criteria.put("supplierCode", actionForm.getSupplierCode());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}	        		        
					
					if (!Common.validateRequired(actionForm.getItemName())) {
						
						criteria.put("itemName", actionForm.getItemName());
						
					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
						
						criteria.put("category", actionForm.getCategory());
						
					}
					
					if (!Common.validateRequired(actionForm.getLocation())) {
						
						criteria.put("location", actionForm.getLocation());
						
					}
					
					if (!Common.validateRequired(actionForm.getPoReceivingDocumentNumberFrom())) {
		        		
		        		criteria.put("poReceivingDocumentNumberFrom", actionForm.getPoReceivingDocumentNumberFrom());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getPoReceivingDocumentNumberTo())) {
		        		
		        		criteria.put("poReceivingDocumentNumberTo", actionForm.getPoReceivingDocumentNumberTo());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getVoucherDocumentNumberFrom())) {
		        		
		        		criteria.put("voucherDocumentNumberFrom", actionForm.getVoucherDocumentNumberFrom());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getVoucherDocumentNumberTo())) {
		        		
		        		criteria.put("voucherDocumentNumberTo", actionForm.getVoucherDocumentNumberTo());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getCheckDocumentNumberFrom())) {
		        		
		        		criteria.put("checkDocumentNumberFrom", actionForm.getCheckDocumentNumberFrom());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getCheckDocumentNumberTo())) {
		        		
		        		criteria.put("checkDocumentNumberTo", actionForm.getCheckDocumentNumberTo());
		        		
		        	}
					
					if (actionForm.getIncludeUnposted()){
						
						criteria.put("includeUnposted", "YES");
						
					} else {
						
						criteria.put("includeUnposted", "NO");
						
					}
					
					if (actionForm.getIncludedPoReceiving()){
						
						criteria.put("includedPoReceiving", "YES");
						
					} else {
						
						criteria.put("includedPoReceiving", "NO");
						
					}
					
					if (actionForm.getIncludedVoucher()){
						
						criteria.put("includedVoucher", "YES");
						
					} else {
						
						criteria.put("includedVoucher", "NO");
						
					}
					
					if (actionForm.getIncludedDirectCheck()){
						
						criteria.put("includedDirectCheck", "YES");
						
					} else {
						
						criteria.put("includedDirectCheck", "NO");
						
					}

					// save criteria
					
					actionForm.setCriteria(criteria);
					
				}
				
				// branch map
				
				ArrayList adBrnchList = new ArrayList();
				ArrayList adBrnchList2 = new ArrayList();
				String branchName = "";
				for(int j=0; j < actionForm.getApRepBrRcvngItmsListSize(); j++) {
				    
				    ApRepBranchReceivingItemsList apRepBrRcvngItmsList = (ApRepBranchReceivingItemsList)actionForm.getApRepBrRcvngItmsByIndex(j);
				    
				    if(apRepBrRcvngItmsList.getBranchCheckbox() == true) {
				        
				        AdBranchDetails mdetails = new AdBranchDetails();	                    
				        mdetails.setBrCode(apRepBrRcvngItmsList.getBrCode());
				        branchName = apRepBrRcvngItmsList.getBrName();
				        adBrnchList.add(mdetails);
				        
				    }
				    
				}
				
				if(adBrnchList.isEmpty()) {			       
				    
				    adBrnchList.addAll(adBrnchList2);
				}

				try {
					
					// get company
					
					AdCompanyDetails adCmpDetails = ejbRR.getAdCompany(user.getCmpCode());
					company = adCmpDetails.getCmpName();
					//AdBranchDetails adBranchDetails = ejbRR.get
					// execute report
					
					list = ejbRR.executeApRepReceivingItems(actionForm.getCriteria(), actionForm.getOrderBy(), actionForm.getGroupBy(), adBrnchList, user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receivingItemsRep.error.noRecordFound"));
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApRepReceivingItemsAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apRepReceivingItems");
					
				}
				
				
				// fill report parameters, fill report to pdf and set report session
				
				Map parameters = new HashMap();
				parameters.put("company", company);
				parameters.put("branchName", user.getCurrentBranch().getBrName());
				parameters.put("dateToday", new java.util.Date());
				parameters.put("printedBy", user.getUserDescription());
				parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
				parameters.put("orderBy", actionForm.getOrderBy());
				parameters.put("groupBy", actionForm.getGroupBy());
				parameters.put("viewType", actionForm.getViewType());
				
				
				if (actionForm.getIncludeUnposted()){
					
					parameters.put("includeUnposted", "yes");
					
				} else {
					
					parameters.put("includeUnposted", "no");
					
				}
				
				if (actionForm.getIncludedPoReceiving()){
					
					parameters.put("includedPoReceiving", "yes");
					
				} else {
					
					parameters.put("includedPoReceiving", "no");
					
				}
				
				if (actionForm.getIncludedVoucher()){
					
					parameters.put("includedVoucher", "yes");
					
				} else {
					
					parameters.put("includedVoucher", "no");
					
				}
				
				if (actionForm.getIncludedDirectCheck()){
					
					parameters.put("includedDirectCheck", "yes");
					
				} else {
					
					parameters.put("includedDirectCheck", "no");
					
				}
				
				if (actionForm.getSupplierCode() != null) {
					
					parameters.put("supplierCode", actionForm.getSupplierCode());
					
				}
				
				if (actionForm.getDateFrom() != null) {
					
					parameters.put("dateFrom", actionForm.getDateFrom());
				}
				
				if (actionForm.getDateTo() != null) {
					
					parameters.put("dateTo", actionForm.getDateTo());
				}	    		    	
				
				if (actionForm.getItemName() != null) {
					
					parameters.put("itemName", actionForm.getSupplierCode());
					
				}
				
				if (actionForm.getCategory() != null) {
					
					parameters.put("category", actionForm.getCategory());
					
				}
				
				if (actionForm.getLocation() != null) {
					
					parameters.put("location", actionForm.getLocation());
					
				}
				
				if (actionForm.getPoReceivingDocumentNumberFrom() != null) {
					
					parameters.put("poReceivingDocumentNumberFrom", actionForm.getPoReceivingDocumentNumberFrom());
					
				}
				
				if (actionForm.getPoReceivingDocumentNumberTo() != null) {
					
					parameters.put("poReceivingDocumentNumberTo", actionForm.getPoReceivingDocumentNumberTo());
					
				}
			    
				if (actionForm.getVoucherDocumentNumberFrom() != null) {
					
					parameters.put("voucherDocumentNumberFrom", actionForm.getVoucherDocumentNumberFrom());
					
				}
				
				if (actionForm.getVoucherDocumentNumberTo() != null) {
					
					parameters.put("voucherDocumentNumberTo", actionForm.getVoucherDocumentNumberTo());
					
				}
				
				if (actionForm.getCheckDocumentNumberFrom() != null) {
					
					parameters.put("checkDocumentNumberFrom", actionForm.getCheckDocumentNumberFrom());
					
				}
				
				if (actionForm.getCheckDocumentNumberTo() != null) {
					
					parameters.put("checkDocumentNumberTo", actionForm.getCheckDocumentNumberTo());
					
				}
				
				String branchMap = null;
				boolean first = true;
				for(int j=0; j < actionForm.getApRepBrRcvngItmsListSize(); j++) {

					ApRepBranchReceivingItemsList brList = (ApRepBranchReceivingItemsList)actionForm.getApRepBrRcvngItmsByIndex(j);

					if(brList.getBranchCheckbox() == true) {
						if(first) {
							branchMap = brList.getBrName();
							first = false;
						} else {
							branchMap = branchMap + ", " + brList.getBrName();
						}
					}

				}
				parameters.put("branchMap", branchMap);
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingItems.jasper";

				if (!new java.io.File(filename).exists()) {
					
					filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingItems.jasper");

				}	    	
				
				try {
					
					Report report = new Report();
					
					if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(
								JasperRunManager.runReportToPdf(filename, parameters, 
										new ApRepReceivingItemsDS(list, actionForm.getGroupBy())));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
						report.setBytes(
								JasperRunManagerExt.runReportToXls(filename, parameters, 
										new ApRepReceivingItemsDS(list, actionForm.getGroupBy())));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
						report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
								new ApRepReceivingItemsDS(list, actionForm.getGroupBy())));												    
						
					}
					
					session.setAttribute(Constants.REPORT_KEY, report);
					actionForm.setReport(Constants.STATUS_SUCCESS);		    	
					
				} catch(Exception ex) {
					
					if(log.isInfoEnabled()) {
						log.info("Exception caught in ApRepReceivingItemsAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}		    	    
				
/*******************************************************
			-- AP RR Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 			-- AP RR Supplier Enter Action --
*******************************************************/
						          
			} else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
						     	
			   	return(mapping.findForward("apRepReceivingItems"));
			   	
/*******************************************************
			-- AP RR Item Enter Action --
*******************************************************/
			   	
			} else if (!Common.validateRequired(request.getParameter("isItemEntered"))) {
		     	
			   	return(mapping.findForward("apRepReceivingItems"));	  	
				
/*******************************************************
			-- INV RR Load Action --
*******************************************************/
				
			}
			
			if(frParam != null) {
			    
			    try {
			    	
			    	actionForm.reset(mapping, request);
			        
			        ArrayList list = null;
			        Iterator i = null;
			        
			        actionForm.clearApRepBrRcvngItmsList();
			        
			        list = ejbRR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
			        
			        i = list.iterator();
			        
			        while(i.hasNext()) {
			            
			            AdBranchDetails details = (AdBranchDetails)i.next();
			            
			            ApRepBranchReceivingItemsList apRepBrRcvngItmsList = new ApRepBranchReceivingItemsList(actionForm,
			                    details.getBrCode(),
			                    details.getBrBranchCode(),details.getBrName());
			            apRepBrRcvngItmsList.setBranchCheckbox(true);
			            
			            actionForm.saveApRepBrRcvngItmsList(apRepBrRcvngItmsList);
			            
			        }
			        
			    } catch (GlobalNoRecordFoundException ex) {
			        
			    } catch (EJBException ex) {
			        
			        if (log.isInfoEnabled()) {
			            
			            log.info("EJBException caught in ApRepReceivingItemsAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			            return mapping.findForward("cmnErrorPage"); 
			            
			        }
			        
			    } 
				
				try {
					
					ArrayList list = null;			       			       
					Iterator i = null;
					
					actionForm.setIncludedPoReceiving(true);
					
					actionForm.clearCategoryList();           	
					
					list = ejbRR.getAdLvInvItemCategoryAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setCategoryList((String)i.next());
							
						}
						
					}
					
					actionForm.clearLocationList();           	
					
					list = ejbRR.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}

				return(mapping.findForward("apRepReceivingItems"));		          
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			
/*******************************************************
			 System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in InvRepItemCostingAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}   
			
			return(mapping.findForward("cmnErrorPage"));   
			
		}
		
	}
}
