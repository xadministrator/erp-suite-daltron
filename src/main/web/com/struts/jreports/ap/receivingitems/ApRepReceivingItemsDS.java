package com.struts.jreports.ap.receivingitems;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepReceivingItemsDetails;

public class ApRepReceivingItemsDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepReceivingItemsDS(ArrayList list, String groupBy) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         ApRepReceivingItemsDetails details = (ApRepReceivingItemsDetails)i.next();
	         
	         String group = null;
	         
	         if (groupBy.equals("SUPPLIER CODE")) {
	         	
	         	group = details.getRrSupplierName();
	         	
	         } else if (groupBy.equals("DATE")) {
	         	
	         	group = details.getRrDate().toString();
	         	
	         } else if (groupBy.equals("ITEM NAME")) {
	         	
	         	group = details.getRrItemName();
	         	
	         }
	         
	         ApRepReceivingItemsData argData = new ApRepReceivingItemsData( details.getRrDate(),
	         		details.getRrItemName(), details.getRrItemDescription(), details.getRrLocation(),
					details.getRrDocNo(), details.getRrSupplierName(), details.getRrUnit(),
					new Double(details.getRrQuantity()), new Double(details.getRrUnitCost()),
					new Double(details.getRrAmount()), details.getRrQcNumber(), details.getRrQcExpiryDate(),
					
					group, details.getRrRefNo(), details.getRrDesc());

	         data.add(argData);
	         
      }
      
   }
   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getDate();
   		
   	} else if("itemName".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getLocation();
   		
   	} else if("documentNumber".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getDocNo();
   		
   	} else if("supplierCode".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getSupplierName();
   		System.out.println("supplierCode="+value);
   	
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getUnit();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getQuantity();
   		
   	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getUnitCost();
   		
   	} else if("amount".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getAmount();
   		
   	} else if("qcNumber".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getQcNumber();
   		
   	} else if("qcExpiryDate".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getExpiryDate();
   		
   		
   		
   	} else if("groupBy".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getGroupBy();
   		
   	} else if("referenceNumber".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getRefNo();
   	
   	} else if("description".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getDescription();
   	
   	}else if("supplierName".equals(fieldName)) {
   		
   		value = ((ApRepReceivingItemsData)data.get(index)).getSupplierName();
   	}
   	
   	return(value);
   	
   }
   
}
