package com.struts.jreports.ap.receivingitems;

import java.util.Date;

public class ApRepReceivingItemsData implements java.io.Serializable {

	private Date date;
	private String itemName;
	private String itemDescription;
	private String location;
	private String documentNumber;
	private String supplierName;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;
	private String qcNumber;
	private Date expiryDate;
	private String groupBy;
	private String referenceNumber;
	private String description;
		
	public ApRepReceivingItemsData(	Date date, String itemName, String itemDescription, String location,
			String documentNumber, String supplierName, String unit, Double quantity, Double unitCost, 
			Double amount, String qcNumber, Date expiryDate,
			String groupBy, String referenceNumber, String description) {
		
		this.date = date;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.supplierName = supplierName;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.qcNumber = qcNumber;
		this.expiryDate = expiryDate;
		this.groupBy = groupBy;
		this.description = description;
		
	}

	
	public Date getDate() {
		
		return date;
		
	}
	
	public String getDocNo() {
		
		return documentNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
	
	}
	
	public String getQcNumber() {
		
		return qcNumber;

	}
	
	public Date getExpiryDate() {
		
		return expiryDate;
		
	}

	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public String getSupplierName() {
		
		return supplierName;

	}
	
	public String getUnit() {
		
		return unit;

	}
	
	public String getGroupBy() {
		
		return groupBy;

	}
	
	public String getRefNo() {
		
		return referenceNumber;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
		
} // ApRepReceivingItemsData class
