package com.struts.jreports.ap.receivingitems;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepReceivingItemsForm extends ActionForm implements Serializable{

	private String itemName = null;
	private String category = null;
	private boolean includeUnposted = false;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String supplierCode = null;
	private String dateFrom = null;
	private String dateTo = null;      
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private boolean includedPoReceiving = true;
	private boolean includedVoucher = false;
	private boolean includedDirectCheck = false;
	private String poReceivingDocumentNumberFrom = null;
	private String poReceivingDocumentNumberTo = null;
	private String voucherDocumentNumberFrom = null;
	private String voucherDocumentNumberTo = null;
	private String checkDocumentNumberFrom = null;
	private String checkDocumentNumberTo = null;
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	private String isSupplierEntered = null;
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	private ArrayList apRepBrRcvngItmsList = new ArrayList();	
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public boolean getIncludeUnposted(){
		
		return(includeUnposted);
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public String getItemName(){
		
		return(itemName);
		
	}
	
	public void setItemName(String itemName){
		
		this.itemName = itemName;
		
	}
	
	public String getCategory (){
		
		return(category );
		
	}
	
	public void setCategory (String category ){
		
		this.category  = category ;
		
	}
	
	public ArrayList getCategoryList(){
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category){
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList(){
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation (){
		
		return(location );
		
	}
	
	public void setLocation (String location ){
		
		this.location  = location ;
		
	}
	
	public ArrayList getLocationList(){
		
		return(locationList);
		
	}
	
	public void setLocationList(String location){
		
		locationList.add(location);
		
	}
	
	public void clearLocationList(){
		
		locationList.clear();
		
		locationList.add(Constants.GLOBAL_BLANK);
		
	}	
	
	public String getSupplierCode(){
		
		return(supplierCode);
		
	}
	
	public void setSupplierCode(String supplierCode){
		
		this.supplierCode = supplierCode;
		
	}
	
	public String getDateFrom(){
		
		return(dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return(dateTo);
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getViewType(){
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){

		this.userPermission = userPermission;
		
	}
	
	public Object[] getApRepBrRcvngItmsList(){
	    
	    return apRepBrRcvngItmsList.toArray();
	    
	}
	
	public ApRepBranchReceivingItemsList getApRepBrRcvngItmsByIndex(int index){
	    
	    return ((ApRepBranchReceivingItemsList)apRepBrRcvngItmsList.get(index));
	    
	}
	
	public int getApRepBrRcvngItmsListSize(){
	    
	    return(apRepBrRcvngItmsList.size());
	    
	}
	
	public void saveApRepBrRcvngItmsList(Object newApRepBrRcvngItmsList){
	    
	    apRepBrRcvngItmsList.add(newApRepBrRcvngItmsList);   	  
	    
	}
	
	public void clearApRepBrRcvngItmsList(){
	    
	    apRepBrRcvngItmsList.clear();
	    
	}
	
	public void setApRepBrRcvngItmsList(ArrayList apRepBrRcvngItmsList) {
	    
	    this.apRepBrRcvngItmsList = apRepBrRcvngItmsList;
	    
	}
	
	public boolean getIncludedPoReceiving(){
		
		return(includedPoReceiving);
		
	}
	
	public void setIncludedPoReceiving(boolean includedPoReceiving) {
		
		this.includedPoReceiving = includedPoReceiving;
		
	}
	
	public boolean getIncludedVoucher(){
		
		return(includedVoucher);
		
	}
	
	public void setIncludedVoucher(boolean includedVoucher) {
		
		this.includedVoucher = includedVoucher;
		
	}
	
	public boolean getIncludedDirectCheck(){
		
		return(includedDirectCheck);
		
	}
	
	public void setIncludedDirectCheck(boolean includedDirectCheck) {
		
		this.includedDirectCheck = includedDirectCheck;
		
	}
	
	public String getPoReceivingDocumentNumberFrom(){
	      
		return(poReceivingDocumentNumberFrom);
	   
	}

	public void setPoReceivingDocumentNumberFrom(String poReceivingDocumentNumberFrom){
	
		this.poReceivingDocumentNumberFrom = poReceivingDocumentNumberFrom;
	
	}

	public String getPoReceivingDocumentNumberTo(){
	      
		return(poReceivingDocumentNumberTo);
	
	}

	public void setPoReceivingDocumentNumberTo(String poReceivingDocumentNumberTo){
	      
		this.poReceivingDocumentNumberTo = poReceivingDocumentNumberTo;
	   
	}
	
	public String getVoucherDocumentNumberFrom(){
	      
		return(voucherDocumentNumberFrom);
	   
	}

	public void setVoucherDocumentNumberFrom(String voucherDocumentNumberFrom){
	
		this.voucherDocumentNumberFrom = voucherDocumentNumberFrom;
	
	}

	public String getVoucherDocumentNumberTo(){
	      
		return(voucherDocumentNumberTo);
	
	}

	public void setVoucherDocumentNumberTo(String voucherDocumentNumberTo){
	      
		this.voucherDocumentNumberTo = voucherDocumentNumberTo;
	   
	}
	
	public String getCheckDocumentNumberFrom(){
	      
		return(checkDocumentNumberFrom);
	   
	}

	public void setCheckDocumentNumberFrom(String checkDocumentNumberFrom){
	
		this.checkDocumentNumberFrom = checkDocumentNumberFrom;
	
	}

	public String getCheckDocumentNumberTo(){
	      
		return(checkDocumentNumberTo);
	
	}

	public void setCheckDocumentNumberTo(String checkDocumentNumberTo){
	      
		this.checkDocumentNumberTo = checkDocumentNumberTo;
	   
	}
	
	public String getGroupBy(){
	   	
		return(groupBy);   	
	}
	   
	public void setGroupBy(String groupBy){
	
		this.groupBy = groupBy;
	
	}
	   
	public ArrayList getGroupByList(){
	
		return groupByList;
	   
	}
	
	public String getIsSupplierEntered() {
	
		return isSupplierEntered;
	
	}
	   
	public void setIsSupplierEntered(String isSupplierEntered) {
	
		this.isSupplierEntered = isSupplierEntered;
	   
	}
		
	public void reset(ActionMapping mapping, HttpServletRequest request){
	    
	    for (int i=0; i<apRepBrRcvngItmsList.size(); i++) {
	        
	        ApRepBranchReceivingItemsList  list = (ApRepBranchReceivingItemsList)apRepBrRcvngItmsList.get(i);
	        list.setBranchCheckbox(false);	       
	        
	    } 
		
		goButton = null;
		closeButton = null;
		itemName = null;
		category = null;
		location = null;
		supplierCode = null;
		includeUnposted = false;
		includedPoReceiving = false;
		includedVoucher = false;
		includedDirectCheck = false;
				
		dateFrom = Common.convertSQLDateToString(new java.util.Date());
		dateTo = Common.convertSQLDateToString(new java.util.Date());
		
		orderByList.clear();
		orderByList.add(Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_DATE);
		orderByList.add(Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_DOCUMENT_NUMBER);
		orderByList.add(Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_LOCATION);
		orderBy = Constants.AP_REP_RECEIVING_ITEMS_ORDER_BY_DATE;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;   
		
		groupByList.clear();
	    groupByList.add(Constants.GLOBAL_BLANK);
	    groupByList.add(Constants.AP_RI_GROUP_BY_SUPPLIER_CODE);
	    groupByList.add(Constants.AP_RI_GROUP_BY_ITEM_NAME);
	    groupByList.add(Constants.AP_RI_GROUP_BY_DATE);
	    groupBy = Constants.GLOBAL_BLANK;   
	    
	    poReceivingDocumentNumberFrom = null;
	    poReceivingDocumentNumberTo = null;
	    voucherDocumentNumberFrom = null;
	    voucherDocumentNumberTo = null;
	    checkDocumentNumberFrom = null;
	    checkDocumentNumberTo = null;
	    
	    isSupplierEntered = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(Common.validateRequired(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("receivingItemsRep.error.dateFromRequired"));
				
			}
			
			if(Common.validateRequired(dateTo)){
				
				errors.add("dateTo", new ActionMessage("receivingItemsRep.error.dateToRequired"));
				
			}	
			
			if(!Common.validateDateFormat(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("receivingItemsRep.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)){
				
				errors.add("dateTo", new ActionMessage("receivingItemsRep.error.dateToInvalid"));
				
			}	
			if(includedPoReceiving == false && includedVoucher == false 
			
					&& includedDirectCheck == false ) {
			 	
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("receivingItemsRep.error.sourceMustBeEntered"));
			
			}

		}

		return(errors);
	
	}

}
