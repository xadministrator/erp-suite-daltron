package com.struts.jreports.ap.receivingreportprint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepReceivingReportPrintController;
import com.ejb.txn.ApRepReceivingReportPrintControllerHome;
import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApRepReceivingReportPrintDetails;

public final class ApRepReceivingReportPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {
      	
 /*******************************************************
      	 Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("ApRepReceivingReportPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));

      	}
      	
      	
      	ApRepReceivingReportPrintForm actionForm = (ApRepReceivingReportPrintForm)form;
      	
      	String frParam = Common.getUserPermission(user, Constants.AP_REP_RECEIVING_REPORT_PRINT_ID);
      	
      	if (frParam != null) {
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	/*******************************************************
      	 Initialize ApRepReceivingReportPrintController EJB
      	 *******************************************************/
      	
      	ApRepReceivingReportPrintControllerHome homeRRP = null;
      	ApRepReceivingReportPrintController ejbRRP = null;       
      	
      	try {
      		
      		homeRRP = (ApRepReceivingReportPrintControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/ApRepReceivingReportPrintControllerEJB", ApRepReceivingReportPrintControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in ApRepReceivingReportPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbRRP = homeRRP.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in ApRepReceivingReportPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
      	/*******************************************************
      	 -- Go Action --
      	 *******************************************************/
      	
      	if(frParam != null) {
      		
      		AdCompanyDetails adCmpDetails = null;
      		ApRepReceivingReportPrintDetails details = null;
      		
      		boolean isService = false; 
      		boolean isInventoriable = false;
      		ArrayList list = null;  
      		ArrayList listSub = null;
      		String recRep = "";
      		String inspectionRep = "";
      		String releaseRep = "";
      		String reportType = "";
      		String VIEW_TYP = "";
      		
      		try {
      			
      			ArrayList poCodeList = new ArrayList();
      			
      			if (request.getParameter("forward") != null) {
      				
      				poCodeList.add(new Integer(request.getParameter("receivingItemCode")));
      				VIEW_TYP = request.getParameter("viewType");
					
      				
      			}
      			
      			System.out.println("RECEIVING REP: "+request.getParameter("receivingReport").toString());
      			recRep=request.getParameter("receivingReport").toString();
      			inspectionRep=request.getParameter("inspectedReport").toString();
      			releaseRep=request.getParameter("releasedReport").toString();
      			
      			if(recRep.trim().equals("true")){
      				reportType = "Accountability";
      			}else if (inspectionRep.trim().equals("true")){
      				reportType = "Inspection";
      			}else if (releaseRep.trim().equals("true")){
      				reportType = "PassOut";
      			}else{
      				reportType = "NormalFormat";
      			}
      			
      			list = ejbRRP.executeApRepReceivingReportPrint(poCodeList, user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode(), reportType);
      		
      			try {
            		
            		listSub = ejbRRP.executeApRepReceivingReportPrintSub(poCodeList, user.getCmpCode());
            		
            	} catch (GlobalNoRecordFoundException ex) {
            		
            	}
            	
      			// get company
      			
      			adCmpDetails = ejbRRP.getAdCompany(user.getCmpCode());
      			
      			// get parameters
      			
      			Iterator i = list.iterator();
      			
      			while (i.hasNext()) {
      				
      				details = (ApRepReceivingReportPrintDetails)i.next();
      				System.out.println(details.getRrpTgDocumentNumber() + "<== tg document number on action");
      				System.out.println(details.getRrpPllIiName() + "<== getRrpPllIiName on action");
      				isService = details.getRrpPlIsService();
      				isInventoriable = details.getRrpPlIsInventoriable();
      			}
      			
      			
      		} catch (GlobalNoRecordFoundException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("receivingReportPrint.error.noRecordFound"));
      			
      		} catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in ApRepReceivingReportPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}
      		
      		if(!errors.isEmpty()) {
      			
      			saveErrors(request, new ActionMessages(errors));
      			return(mapping.findForward("apRepReceivingReportPrint"));
      			
      		}	
      		
      		// fill report parameters, fill report to pdf and set report session
      		
      		Map parameters = new HashMap();
      		parameters.put("company", adCmpDetails.getCmpName());
      		parameters.put("createdBy", details.getRrpPoCreatedBy());
      		parameters.put("approvedBy", details.getRrpPoApprovedRejectedBy());
      		parameters.put("checkedBy", details.getRrpPoCheckedBy());
      		parameters.put("lastPageNumber", list.size() % 20 == 0 ? new Integer(list.size() / 20) : new Integer((list.size() / 20) + 1));
      		parameters.put("poType", details.getRrpPoType());
      		
      		String subreportFilename = null;
      		
      		if(listSub != null) {
			
				subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrintSub.jasper";
																				
				if(new java.io.File(subreportFilename).exists()) {
					
					JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
					parameters.put("ApRepReceivingReportPrintSub", subreport);
	                parameters.put("ApRepReceivingReportPrintSubDS", new ApRepReceivingReportPrintSubDS(listSub));
					parameters.put("showJournal", new Boolean(true));												
				}
				
			} else {
				
				parameters.put("showJournal", new Boolean(false));

			}
      		System.out.println("Accountability1");
      		System.out.println("recRep: "+recRep);
  			String filename = "";
      		if (!recRep.trim().equals("false")) {
      			System.out.println("Accountability2");
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingAssetAccountabilityFormtPrint.jasper";
          		
          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

      			}
      		}else if (isService){
      			System.out.println("Service");
      			
      			SimpleDateFormat df = new SimpleDateFormat("dd MMMMM, yyyy");
      			java.util.Date now = new java.util.Date();
      			
      			System.out.println(df.format(now) + "<== sample date format");
      			
      			
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepCertificateOfAcceptancePrint.jasper";
          		
          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

      			}
          		
          		
      		}else if (!inspectionRep.trim().equals("false")){
      		
      			System.out.println("printInspectionButton");
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrintInspection.jasper";
	          		
          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

      			}
      			
      		}else if (!releaseRep.trim().equals("false")){
      			
      			System.out.println("printReleaseButton");
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrintPassOut.jasper";
          		
          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

      			}
      			
      		}else{
      			/*
      			System.out.println("Normal");
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrint.jasper";
      			          		
          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

      			}*/
      			
      			if(isInventoriable){
          			System.out.println("isInventoriable");
          			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrint.jasper";
          			          		
              		if (!new java.io.File(filename).exists()) {

          				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

          			}
          		}else {
          				
          			System.out.println("is not inventoriable");
          			filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepReceivingReportPrintCustodian.jasper";
    	          		
              		if (!new java.io.File(filename).exists()) {

          				filename = servlet.getServletContext().getRealPath("jreports/ApRepReceivingReportPrint.jasper");

          			}
          		}
      		}
      		
      		
      		
      		try {
      			
      			
      			

			    
			    Report report = new Report();
			    
			    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {
			       	
			       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				       report.setBytes(
				          JasperRunManager.runReportToPdf(filename, parameters, 
				        		  new ApRepReceivingReportPrintDS(list)));   
					        
				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
		               
		               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		               report.setBytes(
						   JasperRunManagerExt.runReportToXls(filename, parameters, 
								   new ApRepReceivingReportPrintDS(list)));   
					        
				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){
					   
					   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
					   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
							   new ApRepReceivingReportPrintDS(list)));												    
					        
				   }
				   
			       session.setAttribute(Constants.REPORT_KEY, report);
			       //actionForm.setReport(Constants.STATUS_SUCCESS);
			       
		
      		} catch(Exception ex) {
      			
      			if(log.isInfoEnabled()) {
      				
      				log.info("Exception caught in ApRepReceivingReportPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			} 
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}              
      		
      		return(mapping.findForward("apRepReceivingReportPrint"));
      		
      	} else {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));
      		
      		return(mapping.findForward("apRepReceivingReportPrintForm"));
      		
      	}
      	
      } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/      	
      	  e.printStackTrace();
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepReceivingReportPrintAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
	              
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
