package com.struts.jreports.ap.receivingreportprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.purchaseorderprint.ApRepPurchaseOrderPrintData;
import com.struts.util.Common;
import com.util.ApRepReceivingReportPrintDetails;

public class ApRepReceivingReportPrintDS implements JRDataSource{

	private ArrayList data = new ArrayList();

	private int index = -1;

	public ApRepReceivingReportPrintDS(ArrayList list) {

		Iterator i = list.iterator();

		while (i.hasNext()) {

			ApRepReceivingReportPrintDetails details = (ApRepReceivingReportPrintDetails)i.next();

			ApRepReceivingReportPrintData dtbData = new ApRepReceivingReportPrintData(Common.convertSQLDateToString(details.getRrpPoDate()), details.getRrpPoDocumentNumber(),
					details.getRrpPoReferenceNumber(), details.getRrpPoRcvPoNumber(), details.getRrpPoDescription(), details.getRrpPllIiName(), details.getRrpPlLocName(),
					new Double(details.getRrpPlQuantityOrdered()), new Double(details.getRrpPlQuantity()),	details.getRrpPlUomName(), new Double(details.getRrpPlUnitCost()),
					new Double(details.getRrpPlAmount()), new Double(details.getRrpPlTaxAmount()), details.getRrpPlItemDescription(), details.getRrpPoSupplierName(), details.getRrpPoSupplierAddress(), details.getRrpPoContactPerson(), details.getRrpPoContactNumber(),
					new Double(details.getRrpPlEndingBalance()), Common.convertSQLDateToString(details.getRrpPoRcvPoDate()), details.getRrpPaymentTerm(),
					new Double(details.getRrpPoTaxRate()), new Double(details.getRrpPlUnitCostWoTax()), new Double(details.getRrpPlUnitCostTaxInclusive()),
					details.getRrpPlLocPosition(), details.getRrpPlLocBranch(), details.getRrpPlLocDateHired(),
					details.getRrpPlLocDepartment(), details.getRrpPlLocEmploymentStatus(),
					details.getRrpPlIiRemarks() ,details.getBranchCode(), details.getBranchName(),
					details.getRrpPoInspectedBy(), details.getRrpPoInspectedBy2(), details.getRrpPoInspectedBy3(), details.getRrpPoInspectedBy4(), details.getRrpPoDepartment(),
					Common.convertSQLDateToString(details.getRrpPoDateInspected()), details.getRrpPoReleasedBy(), details.getRrpPoReleaserPosition(), Common.convertSQLDateToString(details.getRrpPoDateReleased()),
					details.getRrpPoCheckedBy(),details.getRrpPoApprovedRejectedBy(),details.getRrpTgExpiryDateString(),
					details.getRrpTgCustodian(), details.getRrpTgCustodianPosition(), details.getRrpTgPropertyCode(), details.getRrpTgSpecs(), details.getRrpTgSerialNumber(),
					details.getRrpTgDocumentNumber(),
					details.getRrpPoMisc1(),details.getRrpPoMisc2(),details.getRrpPoMisc3(),
					details.getRrpPoMisc4(),details.getRrpPoMisc5(),details.getRrpPoMisc6()
					);

			data.add(dtbData);

		}

	}

	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}

	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;

		String fieldName = field.getName();

		if("date".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDate();
		} else if("documentNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDocumentNumber();
		} else if("referenceNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getReferenceNumber();
		} else if("poNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getpoNumber();
		} else if("description".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDescription();
		} else if("itemName".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getItemName();
		} else if("itemLocation".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getItemLocation();
		} else if("quantityOrdered".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getQuantityOrdered();
		} else if("quantity".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getQuantity();
		} else if("unit".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getUnit();
		} else if("unitCost".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getUnitCost();
		} else if("amount".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getAmount();
		} else if("taxAmount".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getTaxAmount();
		} else if("itemDescription".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getItemDescription();
		} else if("supplierName".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getSupplierName();
		} else if("contactPerson".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getContactPerson();
		} else if("contactNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getContactNumber();
		} else if("endingBalance".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getEndingBalance();
		} else if("poDate".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getPoDate();
		} else if("paymentTerm".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getPaymentTerm();
		} else if("taxRate".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getTaxRate();
		} else if("unitCostWoTax".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getUnitCostWoTax();
		} else if("unitCostTaxInclusive".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getUnitCostTaxInclusive();
		} else if("branchCode".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getBranchCode();
		} else if("branchName".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getBranchName();
		} else if("inspectedBy".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getInspectedBy();
		} else if("inspectedBy2".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getInspectedBy2();
		}else if("inspectedBy3".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getInspectedBy3();
		}else if("inspectedBy4".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getInspectedBy4();
		}else if("department".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDepartment();
		} else if("dateInspected".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDateInspected();
			System.out.println("dateInspected--------->"+value);
		} else if("releasedBy".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getReleasedBy();
		}else if("releaserPosition".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getReleaserPosition();
		} else if("dateReleased".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getDateReleased();
			System.out.println("dateReleased--------->"+value);
		} /****/else if("locPosition".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getLocPosition();
		} else if("locBranch".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getLocBranch();
		} else if("locDateHired".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getLocDateHired();
		} else if("locDepartment".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getLocDepartment();
		} else if("locEmploymentStatus".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getLocEmploymentStatus();
		} else if("itemRemarks".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getItemRemarks();
		}else if("supplierAddress".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getSupplierAddress();

		}else if("checkedBy".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getCheckedBy();
			System.out.print(value+"checkedBy");
		}else if("approvedBy".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getApprovedBy();

		}else if("itemExpiryDate".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getExpiryDate();
		}else if("itemCustodian".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getCustodian();
		}else if("custodianPosition".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getCustodianPosition();
		}else if("itemPropertyCode".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getPropertyCode();
		}else if("itemSpecs".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getSpecs();
		}else if("itemSerialNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getSerialNumber();
		}else if("tgDocumentNumber".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getTgDocumentNumber();
		}else if("misc1".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc1();
		}else if("misc2".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc2();
		}else if("misc3".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc3();
		}else if("misc4".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc4();
		}else if("misc5".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc5();
		}else if("misc6".equals(fieldName)) {
			value = ((ApRepReceivingReportPrintData)data.get(index)).getMisc6();
		}
		return(value);

	}

}
