package com.struts.jreports.ap.receivingreportprint;

public class ApRepReceivingReportPrintData implements java.io.Serializable {
	
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	
	private String poNumber = null;
	private String description = null;
	private String itemName = null;
	private String itemLocation = null;
	private Double quantityOrdered = null;
	private Double quantity = null;
	private String unit = null;
	private Double unitCost = null;
	private Double amount = null;
	private Double taxAmount = null;
	private String itemDescription = null;
	private String supplierName = null;
	private String supplierAddress = null;
	private String contactPerson = null;
	private String contactNumber = null;
	private Double endingBalance = null;
	private String poDate = null;
	private String paymentTerm = null;
	private Double taxRate = null;
	private Double unitCostWoTax = null;
	private Double unitCostTaxInclusive = null;
	private String branchCode = null;
	private String branchName = null;
	
	private String inspectedBy = null;
	private String inspectedBy2 = null;
	private String inspectedBy3 = null;
	private String inspectedBy4 = null;
	private String dateInspected = null;
	private String releasedBy = null;
	private String releaserPosition = null;
	private String dateReleased = null;
	private String department = null;
	
	private String locPosition = null;
	private String locBranch = null;
	private String locDateHired = null;
	private String locDepartment = null;
	private String locEmploymentStatus = null;
	private String itemRemarks = null;
	
	private String checkedBy=null;
	private String approvedBy=null;
	//inv tag values
	private String expiryDate=null;
	private String custodian=null;
	private String custodianPosition=null;
	private String propertyCode=null;
	private String specs = null;
	private String serialNumber =null;
	private String tgDocumentNumber =null;
	
	private String misc1 =null;
	private String misc2 =null;
	private String misc3 =null;
	private String misc4 =null;
	private String misc5 =null;
	private String misc6 =null;
	
	public ApRepReceivingReportPrintData(String date, String documentNumber, String referenceNumber, String poNumber,
			String description, String itemName, String itemLocation, Double quantityOrdered, Double quantity, String unit, Double unitCost, Double amount,
			Double taxAmount, String itemDescription, String supplierName, String supplierAddress, String contactPerson, String contactNumber, Double endingBalance, String poDate, 
			String paymentTerm, Double taxRate, Double unitCostWoTax, Double unitCostTaxInclusive, String locPosition, String locBranch, String locDateHired,
			String locDepartment, String locEmploymentStatus, String itemRemarks, String branchCode, String branchName,
			String inspectedBy, String inspectedBy2, String inspectedBy3, String inspectedBy4, String department,
			String dateInspected, String releasedBy, String releaserPosition, String dateReleased, String checkedBy,String approvedBy,
			String expiryDate, String custodian, String custodianPosition, String propertyCode, String specs, String serialNumber, String tgDocumentNumber,
			String misc1, String misc2, String misc3,
			String misc4, String misc5, String misc6
			) {
		
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.poNumber = poNumber;
		this.description = description;
		this.itemName = itemName;
		this.itemLocation = itemLocation;
		this.quantity = quantity;
		this.quantityOrdered = quantityOrdered;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.taxAmount = taxAmount;
		this.itemDescription = itemDescription;
		this.supplierName = supplierName;
		this.supplierAddress = supplierAddress;
		this.contactPerson = contactPerson;
		this.contactNumber = contactNumber;
		this.endingBalance = endingBalance;
		this.poDate = poDate;
		this.paymentTerm = paymentTerm;
		this.taxRate = taxRate;
		this.unitCostWoTax = unitCostWoTax;
		this.unitCostTaxInclusive = unitCostTaxInclusive;
		this.branchCode = branchCode;
		this.branchName = branchName;

		this.inspectedBy = inspectedBy;
		this.inspectedBy2 = inspectedBy2;
		this.inspectedBy3 = inspectedBy3;
		this.inspectedBy4 = inspectedBy4;
		this.dateInspected = dateInspected;
		this.releasedBy = releasedBy;
		this.dateReleased = dateReleased;
		this.department = department;
		
		this.locBranch = locBranch;
		this.locDateHired = locDateHired;
		this.locDepartment = locDepartment;
		this.locEmploymentStatus = locEmploymentStatus;
		this.itemRemarks = itemRemarks;
		this.locPosition =locPosition;
	
		this.checkedBy=checkedBy;
		this.approvedBy=approvedBy;
		
		this.expiryDate=expiryDate;
		this.custodian=custodian;
		this.custodianPosition = custodianPosition;
		this.propertyCode = propertyCode;
		this.serialNumber = serialNumber;
		this.specs = specs;
		this.tgDocumentNumber = tgDocumentNumber;
		
		this.misc1 = misc1;
		this.misc2 = misc2;
		this.misc3 = misc3;
		this.misc4 = misc4;
		this.misc5 = misc5;
		this.misc6 = misc6;
	}
	
	public String getDepartment() {
		
		return department;
		
	}

	public String getDate() {
		
		return date;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getpoNumber() {
		
		return poNumber;
		
	}
	
	public String getDescription(){
		
		return description;
	}
	
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getItemLocation() {
		
		return itemLocation;
		
	}
	
	public Double getQuantity() {
		
		return quantity;
		
	}
	
	public Double getQuantityOrdered() {
		
		return quantityOrdered;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	
	public Double getUnitCost() {
		
		return unitCost;
		
	}
	
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	public Double getTaxAmount() {
		
		return taxAmount;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public String getSupplierName() {
		
		return supplierName;
		
	}
	
	public String getSupplierAddress() {
		
		return supplierAddress;
		
	}
	
	public String getContactPerson() {
		
		return contactPerson;
		
	}
	
	public String getContactNumber() {
		
		return contactNumber;
		
	}
	
	public Double getEndingBalance() {
		
		return endingBalance;
		
	}
	
	public String getPoDate() {
		
		return poDate;
		
	}
	
	public String getPaymentTerm() {
		
		return paymentTerm;
		
	}
	
	public Double getTaxRate() {

		return taxRate;

	}

	public Double getUnitCostWoTax() {

		return unitCostWoTax;

	}
	
	public Double getUnitCostTaxInclusive() {
		
		return unitCostTaxInclusive;
		
	}
	
	public String getBranchCode() {

		return branchCode;

	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public String getInspectedBy() {
		
		return inspectedBy;
	}
	
	public String getInspectedBy2() {
		
		return inspectedBy2;
	}
	public String getInspectedBy3() {
		
		return inspectedBy3;
	}
	
	public String getInspectedBy4() {
		
		return inspectedBy4;
	}
	public String getDateInspected(){
		
		return dateInspected;
		
	}
	
	public String getReleasedBy(){
		
		return releasedBy;
		
	}
	public String getReleaserPosition(){
		
		return releaserPosition;
		
	}
	
	public String getDateReleased(){
		
		return dateReleased;
		
	}
	
	public String getLocBranch() {

		return locBranch;

	}
	
	public String getLocDateHired() {
		
		return locDateHired;
		
	}
	
	public String getLocDepartment() {

		return locDepartment;

	}
	
	public String getLocEmploymentStatus() {
		
		return locEmploymentStatus;
		
	}

	public String getItemRemarks() {

		return itemRemarks;

	}

	public String getLocPosition() {

		return locPosition;

	}
	
	public String getCheckedBy() {

		return checkedBy;

	}
	public String getApprovedBy() {

		return approvedBy;

	}
	public String getExpiryDate() {
		return expiryDate;
	}

	public String getCustodian() {
		return custodian;
	}
	
	public String getCustodianPosition() {
		return custodianPosition;
	}

	public String getPropertyCode() {
		return propertyCode;
	}

	public String getSpecs() {
		return specs;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getTgDocumentNumber() {
		return tgDocumentNumber;
	}
	
	public String getMisc1() {
		return misc1;
	}
	
	public String getMisc2() {
		return misc2;
	}
	
	public String getMisc3() {
		return misc3;
	}
	
	public String getMisc4() {
		return misc4;
	}
	
	public String getMisc5() {
		return misc5;
	}
	
	public String getMisc6() {
		return misc6;
	}
}
