/*
 * opt/jreport/ArRepReceiptPrintData.java
 *
 * Created on May 14, 2004, 3:40 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.ap.receivingreportprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.gl.journalprint.GlRepJournalPrintData;
import com.struts.util.Common;
import com.util.ApModDistributionRecordDetails;
import com.util.GlRepJournalPrintDetails;

public class ApRepReceivingReportPrintSubDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepReceivingReportPrintSubDS(ArrayList list) {
   	
   	  Iterator i = list.iterator();
   	  while(i.hasNext()) {

   		  ApModDistributionRecordDetails mdetails = (ApModDistributionRecordDetails)i.next();

   		  if(mdetails.getDrDebit() == 1) {

   			  ApRepReceivingReportPrintSubData apRepRPData = new ApRepReceivingReportPrintSubData(
   					  mdetails.getDrCoaAccountNumber(),mdetails.getDrCoaAccountDescription(),
   					  new Double(mdetails.getDrAmount()), null, new Boolean(false), null,null,mdetails.getDrClass());

   			  data.add(apRepRPData);

   		  } else {

   			  ApRepReceivingReportPrintSubData apRepRPData = new ApRepReceivingReportPrintSubData(
   					  mdetails.getDrCoaAccountNumber(),mdetails.getDrCoaAccountDescription(),
   					  null, new Double(mdetails.getDrAmount()) , new Boolean(false), null,null,mdetails.getDrClass());

   			  data.add(apRepRPData);

   		  }

   	  }
                                                                     
   }

   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
	
   public Object getFieldValue(JRField field) throws JRException {
	   	
	      Object value = null;

	      String fieldName = field.getName();
	      if("accountNumber".equals(fieldName)){
	         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getAccountNumber();
	      }else if("accountDescription".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getAccountDescription();  
	      }else if("debitAmount".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getDebitAmount();       
	      }else if("creditAmount".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getCreditAmount();       
	      }else if("showDuplicate".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getShowDuplicate();       
	      }else if("category".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getCategory();       
	      }else if("source".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getSource();       
	      }else if("drclass".equals(fieldName)){
		         value = ((ApRepReceivingReportPrintSubData)data.get(index)).getDrclass();       
	      }   
	      
	      return(value);
	   }
}
