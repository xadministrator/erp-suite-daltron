package com.struts.jreports.ap.supplierclasslist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepSupplierClassListDetails;

public class ApRepSupplierClassListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepSupplierClassListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 ApRepSupplierClassListDetails details = (ApRepSupplierClassListDetails)i.next();
                  
	     ApRepSupplierClassListData argData = new ApRepSupplierClassListData(details.getSclScName(), 
	     		details.getSclScDescription(), details.getSclTaxName(), details.getSclWithholdingTaxName(),
				details.getSclPayableAccountNumber(), details.getSclPayableAccountDescription(), 
				details.getSclExpenseAccountNumber(), details.getSclExpenseAccountDescription(),
				details.getSclEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("supplierClassName".equals(fieldName)){
         value = ((ApRepSupplierClassListData)data.get(index)).getSupplierClassName();
      }else if("description".equals(fieldName)){
         value = ((ApRepSupplierClassListData)data.get(index)).getDescription();
      }else if("taxName".equals(fieldName)){
         value = ((ApRepSupplierClassListData)data.get(index)).getTaxName();
      }else if("withholdingTaxName".equals(fieldName)){
        value = ((ApRepSupplierClassListData)data.get(index)).getWithholdingTaxName();   
      }else if("payableAccountNumber".equals(fieldName)){
         value = ((ApRepSupplierClassListData)data.get(index)).getPayableAccountNumber();         
      }else if("payableAccountDescription".equals(fieldName)){
         value = ((ApRepSupplierClassListData)data.get(index)).getPayableAccountDescription();
      }else if("expenseAccountNumber".equals(fieldName)){
        value = ((ApRepSupplierClassListData)data.get(index)).getExpenseAccountNumber();         
      }else if("expenseAccountDescription".equals(fieldName)){
        value = ((ApRepSupplierClassListData)data.get(index)).getExpenseAccountDescription();         
      }else if("enable".equals(fieldName)){
        value = ((ApRepSupplierClassListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
