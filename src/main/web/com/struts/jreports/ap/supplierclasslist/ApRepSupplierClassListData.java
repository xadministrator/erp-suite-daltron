package com.struts.jreports.ap.supplierclasslist;


public class ApRepSupplierClassListData implements java.io.Serializable {
	
   private String supplierClassName = null;
   private String description = null;
   private String taxName = null;
   private String withholdingTaxName = null;
   private String payableAccountNumber = null;
   private String payableAccountDescription = null;
   private String expenseAccountNumber = null;
   private String expenseAccountDescription = null;
   private String enable = null;

   public ApRepSupplierClassListData(String supplierClassName, String description, String taxName,
   		String withholdingTaxName, String payableAccountNumber, String payableAccountDescription, 
   		String expenseAccountNumber, String expenseAccountDescription, String enable){
      	
      this.supplierClassName = supplierClassName;
      this.description = description;
      this.taxName = taxName;
      this.withholdingTaxName = withholdingTaxName;
      this.payableAccountNumber = payableAccountNumber;
      this.payableAccountDescription = payableAccountDescription;
      this.expenseAccountNumber = expenseAccountNumber;
      this.expenseAccountDescription = expenseAccountDescription;
      this.enable = enable;
      
   }

   public String getDescription() {
   	
   	return description;
   
   }
   
   public void setDescription(String description) {
   
   	this.description = description;
   
   }
   
   public String getEnable() {
   
   	return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	this.enable = enable;
   
   }
   
   public String getExpenseAccountDescription() {
   
   	return expenseAccountDescription;
   
   }
   
   public void setExpenseAccountDescription(String expenseAccountDescription) {
  
   	this.expenseAccountDescription = expenseAccountDescription;
   
   }
   
   public String getExpenseAccountNumber() {
  
   	return expenseAccountNumber;
  
   }
   
   public void setExpenseAccountNumber(String expenseAccountNumber) {
   
   	this.expenseAccountNumber = expenseAccountNumber;
   
   }
   
   public String getPayableAccountDescription() {
   
   	return payableAccountDescription;
   
   }
   
   public void setPayableAccountDescription(String payableAccountDescription) {
   
   	this.payableAccountDescription = payableAccountDescription;
   
   }
   
   public String getPayableAccountNumber() {
   
   	return payableAccountNumber;
   
   }
   
   public void setPayableAccountNumber(String payableAccountNumber) {
   
   	this.payableAccountNumber = payableAccountNumber;
   
   }
  
   public String getSupplierClassName() {
   
   	return supplierClassName;
   
   }
   
   public void setSupplierClassName(String supplierClassName) {
   
   	this.supplierClassName = supplierClassName;
   
   }
   
   public String getTaxName() {
   
   	return taxName;
   
   }
   
   public void setTaxName(String taxName) {
   
    this.taxName = taxName;
   
   }
   
   public String getWithholdingTaxName() {
   
   	return withholdingTaxName;
   
   }
   
   public void setWithholdingTaxName(String withholdingTaxName) {
   
   	this.withholdingTaxName = withholdingTaxName;
   
   }
}
