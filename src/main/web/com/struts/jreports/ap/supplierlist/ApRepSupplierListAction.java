package com.struts.jreports.ap.supplierlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepSupplierListController;
import com.ejb.txn.ApRepSupplierListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ApRepSupplierListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepSupplierListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepSupplierListForm actionForm = (ApRepSupplierListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AP_REP_SUPPLIER_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRepSupplierList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApRepSupplierListController EJB
*******************************************************/

         ApRepSupplierListControllerHome homeSL = null;
         ApRepSupplierListController ejbSL = null;       

         try {
         	
            homeSL = (ApRepSupplierListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepSupplierListControllerEJB", ApRepSupplierListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepSupplierListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSL = homeSL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepSupplierListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP SL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        	}

	        	if (!Common.validateRequired(actionForm.getType())) {
	        		
	        		criteria.put("supplierType", actionForm.getType());
	        	}

	        	if (!Common.validateRequired(actionForm.getSupplierClass())) {
	        		
	        		criteria.put("supplierClass", actionForm.getSupplierClass());
	        	}	        		        	
	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            // branch map
            
            ArrayList adBrnchList = new ArrayList();
            // ArrayList adBrnchList2 = new ArrayList();
            
            for(int j=0; j < actionForm.getApRepBrSpplrLstListSize(); j++) {
                
                ApRepBranchSupplierList apRepBrSpplrLstList = (ApRepBranchSupplierList)actionForm.getApRepBrSpplrLstByIndex(j);
                
                if(apRepBrSpplrLstList.getBranchCheckbox() == true) {
                    
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(apRepBrSpplrLstList.getBrCode());
                    adBrnchList.add(mdetails);
                    
                }
                
            }
            

		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbSL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbSL.executeApRepSupplierList(actionForm.getCriteria(),
            	    actionForm.getOrderBy(), actionForm.getIncludeDirectChecks(), adBrnchList, user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("apSupplierList.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ApRepSupplierListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRepSupplierList");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDate());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getSupplierCode() != null) {
			
				parameters.put("supplierCode", actionForm.getSupplierCode());
				
			}
			
			if (actionForm.getType() != null) {
			
				parameters.put("supplierType", actionForm.getType());
				
			}
			
			if (actionForm.getSupplierClass() != null) {
				
				parameters.put("supplierClass", actionForm.getSupplierClass());	
		    
		    }
			
			String branchMap = null;
			boolean first = true;
			for(int j=0; j < actionForm.getApRepBrSpplrLstListSize(); j++) {

				ApRepBranchSupplierList brList = (ApRepBranchSupplierList)actionForm.getApRepBrSpplrLstByIndex(j);

				if(brList.getBranchCheckbox() == true) {
					if(first) {
						branchMap = brList.getBrName();
						first = false;
					} else {
						branchMap = branchMap + ", " + brList.getBrName();
					}
				}

			}
			parameters.put("branchMap", branchMap);
			String filename = "";
			
			if(actionForm.getIncludeDirectChecks()){
				filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepSupplierListWithDC.jasper";
			}else{
				filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepSupplierList.jasper";
			}
			
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/ApRepSupplierList.jasper");
		    
	        }
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepSupplierListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ApRepSupplierListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ApRepSupplierListDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ApRepSupplierListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP SL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP SL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	             
	             try {
	             	
	             	 actionForm.reset(mapping, request);
	                 
	                 ArrayList list = null;
	                 Iterator i = null;
	                 
	                 actionForm.clearApRepBrSpplrLstList();
	                 
	                 list = ejbSL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	                 
	                 i = list.iterator();
	                 
	                 while(i.hasNext()) {
	                     
	                     AdBranchDetails details = (AdBranchDetails)i.next();
	                     
	                     ApRepBranchSupplierList apRepBrSpplrLstList = new ApRepBranchSupplierList(actionForm,
	                             details.getBrCode(),
	                             details.getBrBranchCode(), details.getBrName());
	                     apRepBrSpplrLstList.setBranchCheckbox(true);
	                     
	                     actionForm.saveApRepBrSpplrLstList(apRepBrSpplrLstList);
	                     
	                 }
	                 
	             } catch (GlobalNoRecordFoundException ex) {
	                 
	             } catch (EJBException ex) {
	                 
	                 if (log.isInfoEnabled()) {
	                     
	                     log.info("EJBException caught in ApRepSupplierListAction.execute(): " + ex.getMessage() +
	                             " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                 }
	                 
	             } 
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearTypeList();           	
	            	
	            	list = ejbSL.getApStAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearSupplierClassList();           	
	            	
	            	list = ejbSL.getApScAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ApRepSupplierListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("apRepSupplierList"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ApRepSupplierListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
