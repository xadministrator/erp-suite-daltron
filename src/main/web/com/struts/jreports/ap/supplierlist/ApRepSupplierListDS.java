package com.struts.jreports.ap.supplierlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepSupplierListDetails;

public class ApRepSupplierListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepSupplierListDS(ArrayList list) {

      Iterator i = list.iterator();

      while (i.hasNext()) {

         ApRepSupplierListDetails details = (ApRepSupplierListDetails)i.next();

	     ApRepSupplierListData argData = new ApRepSupplierListData(details.getSlSplSupplierCode(),
	     details.getSlSplName(), details.getSlSplContact(), details.getSlSplPhone(),
	     details.getSlSplTin(), details.getSlSplAddress(), new Double(details.getSlSplBalance()), new Double(details.getSlSplPtdBalance()),
	     new Double(details.getSlSplPtdInterest()), new Double(details.getSlSplNetTransactions()),
	     new Double(details.getSlSplBegBalance()), new Double(details.getSlSplInterestIncome()), details.getSlSplPaymentTerm(),
	     new Double(details.getSlSplAmntDC()));

         data.add(argData);
      }

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("supplierCode".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getSupplierCode();
      }else if("supplierName".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getSupplierName();
      }else if("contact".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getContact();
      }else if("phone".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getPhone();
      }else if("tinNumber".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getTinNumber();
      }else if("address".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getAddress();
      }else if("balance".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getBalance();
      }else if("ptdBalance".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getPtdBalance();
      }else if("ptdInterest".equals(fieldName)){
         value = ((ApRepSupplierListData)data.get(index)).getPtdInterest();
      }else if("netTransactions".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getNetTransactions();
      }else if("begBalance".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getBegBalance();
      }else if("interestIncome".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getInterestIncome();
      }else if("paymentTerm".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getPaymentTerm();
      }else if("amountDC".equals(fieldName)){
          value = ((ApRepSupplierListData)data.get(index)).getAmountDC();
      }

      return(value);
   }
}
