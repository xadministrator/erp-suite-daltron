package com.struts.jreports.ap.supplierlist;


public class ApRepSupplierListData implements java.io.Serializable {

   private String supplierCode = null;
   private String supplierName = null;
   private String contact = null;
   private String phone = null;
   private String tinNumber = null;
   private String address = null;
   private Double balance = null;
   private Double ptdBalance = null;
   private Double ptdInterest = null;

   private Double netTransactions = null;
   private Double begBalance = null;
   private Double interestIncome = null;
   private Double amountDC = null;

   private String paymentTerm = null;

   public ApRepSupplierListData(String supplierCode, String supplierName,
      String contact, String phone, String tinNumber, String address, Double balance,
      Double ptdBalance, Double ptdInterest, Double netTransactions,
      Double begBalance, Double interestIncome, String paymentTerm, Double amountDC){

      this.supplierCode = supplierCode;
      this.supplierName = supplierName;
      this.contact = contact ;
      this.phone = phone;
      this.tinNumber = tinNumber;
      this.address = address;
      this.balance = balance ;
      this.ptdBalance = ptdBalance;
      this.ptdInterest = ptdInterest;

      this.netTransactions = netTransactions;
      this.begBalance = begBalance;
      this.interestIncome = interestIncome;
      this.paymentTerm = paymentTerm;
      this.amountDC = amountDC;
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public String getSupplierName(){
      return(supplierName);
   }

   public String getContact(){
      return(contact);
   }

   public String getPhone(){
      return(phone);
   }

   public String getTinNumber(){
   	  return(tinNumber);
   }

   public String getAddress(){
   	  return(address);
   }

   public Double getBalance(){
      return(balance);
   }

   public Double getPtdBalance() {
	  return(ptdBalance);
   }

   public Double getPtdInterest() {
	  return(ptdInterest);
   }

   public Double getNetTransactions() {

	   return(netTransactions);

   }

   public Double getBegBalance() {

	   return(begBalance);

   }

   public Double getInterestIncome() {

	   return (interestIncome);

   }

   public String getPaymentTerm() {

	   return (paymentTerm);

   }

   public Double getAmountDC() {

	   return (amountDC);

   }


}
