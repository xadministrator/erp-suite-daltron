package com.struts.jreports.ap.supplierlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepSupplierListForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String date = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private boolean includeDirectChecks = false;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList apRepBrSpplrLstList = new ArrayList();   

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public boolean getIncludeDirectChecks() {

	      return includeDirectChecks;

   }

   public void setIncludeDirectChecks(boolean includeDirectChecks) {

	      this.includeDirectChecks = includeDirectChecks;

   }
   
   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }

   public String getDate(){
      return(date);
   }

   public void setDate(String date){
      this.date = date;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getType(){
      return(type);
   }

   public void setType(String type){
      this.type = type;
   }

   public ArrayList getTypeList(){
      return(typeList);
   }

   public void setTypeList(String type){
      typeList.add(type);
   }

   public void clearTypeList(){
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
   }
 
   public String getSupplierClass(){
      return(supplierClass);
   }

   public void setSupplierClass(String supplierClass){
      this.supplierClass = supplierClass;
   }

   public ArrayList getSupplierClassList(){
      return(supplierClassList);
   }

   public void setSupplierClassList(String supplierClass){
      supplierClassList.add(supplierClass);
   }

   public void clearSupplierClassList(){
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
   } 

   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Object[] getApRepBrSpplrLstList(){
       
       return apRepBrSpplrLstList.toArray();
       
   }
   
   public ApRepBranchSupplierList getApRepBrSpplrLstByIndex(int index){
       
       return ((ApRepBranchSupplierList)apRepBrSpplrLstList.get(index));
       
   }
   
   public int getApRepBrSpplrLstListSize(){
       
       return(apRepBrSpplrLstList.size());
       
   }
   
   public void saveApRepBrSpplrLstList(Object newApRepBrSpplrLstList){
       
       apRepBrSpplrLstList.add(newApRepBrSpplrLstList);   	  
       
   }
   
   public void clearApRepBrSpplrLstList(){
       
       apRepBrSpplrLstList.clear();
       
   }
   
   public void setApRepBrSpplrLstList(ArrayList apRepBrSpplrLstList) {
       
       this.apRepBrSpplrLstList = apRepBrSpplrLstList;
       
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){  
       
       for (int i=0; i<apRepBrSpplrLstList.size(); i++) {
           
           ApRepBranchSupplierList  list = (ApRepBranchSupplierList)apRepBrSpplrLstList.get(i);
           list.setBranchCheckbox(false);	       
           
       } 
       
      goButton = null;
      closeButton = null;
      supplierCode = null;
      supplierClass = Constants.GLOBAL_BLANK; 
      type = Constants.GLOBAL_BLANK;
      date = Common.convertSQLDateToString(new java.util.Date());
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      includeDirectChecks = false;
      
      if (orderByList.isEmpty()) {
      	
		  orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE);
	      orderByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS);
	      orderByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE);
	      orderBy = Constants.AP_SL_ORDER_BY_SUPPLIER_CODE;   
	  
	  }    
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(date)){
		    errors.add("date", new ActionMessage("apSupplierList.error.dateInvalid"));
		 }	 
         if(Common.validateRequired(date)){
		    errors.add("date", new ActionMessage("apSupplierList.error.dateRequired"));
		 }			 		 		 
		 if(!Common.validateStringExists(typeList, type)){
	            errors.add("type", new ActionMessage("apSupplierList.error.typeInvalid"));
		 }
		 if(!Common.validateStringExists(supplierClassList, supplierClass)){
	            errors.add("supplierClass", new ActionMessage("apSupplierList.error.supplierClassInvalid"));
		 }	 
      }
      return(errors);
   }
}
