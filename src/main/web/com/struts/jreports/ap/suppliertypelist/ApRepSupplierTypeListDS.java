package com.struts.jreports.ap.suppliertypelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepSupplierTypeListDetails;

public class ApRepSupplierTypeListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepSupplierTypeListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 ApRepSupplierTypeListDetails details = (ApRepSupplierTypeListDetails)i.next();
                  
	     ApRepSupplierTypeListData argData = new ApRepSupplierTypeListData(details.getStlStName(),
	     		details.getStlStDescription(), details.getStlBankAccount(), details.getStlEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("supplierTypeName".equals(fieldName)){
         value = ((ApRepSupplierTypeListData)data.get(index)).getSupplierTypeName();
      }else if("description".equals(fieldName)){
         value = ((ApRepSupplierTypeListData)data.get(index)).getDescription();
      }else if("bankAccount".equals(fieldName)){
         value = ((ApRepSupplierTypeListData)data.get(index)).getBankAccount();      
      }else if("enable".equals(fieldName)){
        value = ((ApRepSupplierTypeListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
