package com.struts.jreports.ap.suppliertypelist;


public class ApRepSupplierTypeListData implements java.io.Serializable {
	
   private String supplierTypeName = null;
   private String description = null;
   private String bankAccount = null;
   private String enable = null;

   public ApRepSupplierTypeListData(String supplierTypeName, String description, String bankAccount, String enable){
      	
      this.supplierTypeName = supplierTypeName;
      this.description = description;
      this.bankAccount = bankAccount;
      this.enable = enable;
      
   }

   public String getDescription() {
   	
   	return description;
   
   }
   
   public void setDescription(String description) {
   
   	this.description = description;
   
   }
   
   public String getEnable() {
   
   	return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	this.enable = enable;
   
   }

   public String getSupplierTypeName() {
   
   	return supplierTypeName;
   
   }
   
   public void setSupplierTypeName(String supplierTypeName) {
   
   	this.supplierTypeName = supplierTypeName;
   
   }
   
   public String getBankAccount() {
   
   	return bankAccount;
   
   }
   
   public void setBankAccount(String bankAccount) {
   
    this.bankAccount = bankAccount;
   
   }
   
}
