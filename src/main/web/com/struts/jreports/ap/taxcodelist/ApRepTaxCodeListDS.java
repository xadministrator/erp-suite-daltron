package com.struts.jreports.ap.taxcodelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepTaxCodeListDetails;

public class ApRepTaxCodeListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepTaxCodeListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ApRepTaxCodeListDetails details = (ApRepTaxCodeListDetails)i.next();
                  
	     ApRepTaxCodeListData argData = new ApRepTaxCodeListData(details.getTclTaxName(), details.getTclTaxDescription(),
	     		details.getTclTaxType(), new Double(details.getTclTaxRate()), details.getTclCoaGlTaxAccountNumber(),
				details.getTclCoaGlTaxAccountDescription(), details.getTclEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("taxName".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getTaxName();
      }else if("taxDescription".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getTaxDescription();
      }else if("taxType".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getTaxType();
      }else if("taxRate".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getTaxRate();
      }else if("accountNumber".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getAccountNumber();         
      }else if("accountDescription".equals(fieldName)){
         value = ((ApRepTaxCodeListData)data.get(index)).getAccountDescription();
      }else if("enable".equals(fieldName)){
        value = ((ApRepTaxCodeListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
