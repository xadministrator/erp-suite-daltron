package com.struts.jreports.ap.vouchereditlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepVoucherEditListController;
import com.ejb.txn.ApRepVoucherEditListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApRepVoucherPrintDetails;
import com.util.ApRepVoucherEditListDetails;

public final class ApRepVoucherEditListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRepVoucherPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRepVoucherEditListForm actionForm = (ApRepVoucherEditListForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.AP_REP_VOUCHER_EDIT_LIST_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ApRepVoucherEditListController EJB
*******************************************************/

         ApRepVoucherEditListControllerHome homeVP = null;
         ApRepVoucherEditListController ejbVP = null;       

         try {
         	
            homeVP = (ApRepVoucherEditListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepVoucherEditListControllerEJB", ApRepVoucherEditListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ApRepVoucherEditListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbVP = homeVP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ApRepVoucherEditListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getApVouByVouCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list = null;           
				    
				try {
					
					ArrayList vouCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		vouCodeList.add(new Integer(request.getParameter("voucherCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("voucherCode[" + i + "]") != null) {
								
								vouCodeList.add(new Integer(request.getParameter("voucherCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbVP.executeApRepVoucherEditList(vouCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbVP.getAdCompany(user.getCmpCode());	            	
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEditList.error.voucherAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApRepVoucherEditListAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
			    
			    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();   
			    	   
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());	
				
				ApRepVoucherEditListDetails velDetails = (ApRepVoucherEditListDetails) list.get(0);
				parameters.put("postedBy", velDetails.getVelPostedBy());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepVoucherEditList.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/ApRepVoucherEditList.jasper");
			    
		        }
				 						    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ApRepVoucherEditListDS(list, user.getUserName())));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ApRepVoucherEditListAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("apRepVoucherEditList"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("apRepVoucherEditListForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ApRepVoucherEditListAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
