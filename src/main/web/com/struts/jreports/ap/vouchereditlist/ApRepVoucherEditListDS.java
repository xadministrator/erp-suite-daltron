package com.struts.jreports.ap.vouchereditlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ApRepVoucherEditListDetails;


public class ApRepVoucherEditListDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepVoucherEditListDS(ArrayList apRepVELList, String userName) {
      
      Date currentDate = new Date();
      
   	  Iterator i = apRepVELList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ApRepVoucherEditListDetails details = (ApRepVoucherEditListDetails)i.next();
   	    
   	    if(details.getVelDrDebit() == 1) {
   	    
	   	  	ApRepVoucherEditListData apRepVELData = new ApRepVoucherEditListData(
	   	     details.getVelVouBatchName(), details.getVelVouBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getVelVouTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getVelVouType(), details.getVelVouDocumentNumber(), details.getVelVouReferenceNumber(), 
			 Common.convertSQLDateToString(details.getVelVouDate()), details.getVelSplSupplierCode(), 
			 new Double(details.getVelVouAmountDue()), details.getVelVouDescription(), details.getVelDrAccountNumber(), 
			 details.getVelDrAccountDescription(), details.getVelDrClass(), new Double(details.getVelDrAmount()), null, 
			 details.getVelSplSupplierName(), details.getVelWithholdingTaxCode(), details.getVelApprovedBy(),
			 Common.convertCharToString(details.getVelCurrencySymbol()));
	   	  	

             data.add(apRepVELData);
	   	  	
	    } else {
	   	
	    	ApRepVoucherEditListData apRepVELData = new ApRepVoucherEditListData(
    		 details.getVelVouBatchName(), details.getVelVouBatchDescription(), 
    		 Common.convertIntegerToString(new Integer(details.getVelVouTransactionTotal())),
   	   	     Common.convertSQLDateToString(currentDate), Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getVelVouType(), details.getVelVouDocumentNumber(), details.getVelVouReferenceNumber(), 
			 Common.convertSQLDateToString(details.getVelVouDate()), details.getVelSplSupplierCode(), 
			 new Double(details.getVelVouAmountDue()), details.getVelVouDescription(), details.getVelDrAccountNumber(), 
			 details.getVelDrAccountDescription(), details.getVelDrClass(), null, new Double(details.getVelDrAmount()),
			 details.getVelSplSupplierName(), details.getVelWithholdingTaxCode(), details.getVelApprovedBy(),
			 Common.convertCharToString(details.getVelCurrencySymbol()));    	
   	  	    data.add(apRepVELData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

	  if("batchName".equals(fieldName)){
	    value = ((ApRepVoucherEditListData)data.get(index)).getBatchName(); 
	  }else if("batchDescription".equals(fieldName)){
	    value = ((ApRepVoucherEditListData)data.get(index)).getBatchDescription();       
	  }else if("transactionTotal".equals(fieldName)){
	    value = ((ApRepVoucherEditListData)data.get(index)).getTransactionTotal(); 
	  }else if("dateCreated".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getDateCreated();  
      }else if("timeCreated".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getTimeCreated();  
      }else if("createdBy".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getCreatedBy();
      }else if("type".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getType();    
      }else if("documentNumber".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getDocumentNumber();  
      }else if("referenceNumber".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getReferenceNumber(); 
      }else if("date".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getDate();   
      }else if("supplierCode".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getSupplierCode(); 
      }else if("amountDue".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getAmountDue(); 
      }else if("accountNumber".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getAccountNumber();   
      }else if("accountDescription".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getAccountDescription(); 
      }else if("class".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getClassType();    
      }else if("debitAmount".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getDebitAmount();   
      }else if("creditAmount".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getCreditAmount();                   
      }else if("description".equals(fieldName)){
        value = ((ApRepVoucherEditListData)data.get(index)).getDescription();                   
      }else if("supplierName".equals(fieldName)){
          value = ((ApRepVoucherEditListData)data.get(index)).getSupplierName();                   
      }else if("withholdingTaxCode".equals(fieldName)){
          value = ((ApRepVoucherEditListData)data.get(index)).getWithholdingTaxCode();
      }else if("approvedBy".equals(fieldName)){
          value = ((ApRepVoucherEditListData)data.get(index)).getApprovedBy();                   
      }else if("currencySymbol".equals(fieldName)){
          value = ((ApRepVoucherEditListData)data.get(index)).getCurrencySymbol();                   
      }


      return(value);
   }
}
