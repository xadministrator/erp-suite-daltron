
package com.struts.jreports.ap.voucherprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApRepVoucherPrintController;
import com.ejb.txn.ApRepVoucherPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApModCheckDetails;
import com.util.ApRepVoucherPrintDetails;

public final class ApRepVoucherPrintAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApRepVoucherPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApRepVoucherPrintForm actionForm = (ApRepVoucherPrintForm)form;

         String frParam= Common.getUserPermission(user, Constants.AP_REP_VOUCHER_PRINT_ID);

         if (frParam != null) {

             actionForm.setUserPermission(frParam.trim());

         } else {

             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ApRepVoucherPrintController EJB
*******************************************************/

         ApRepVoucherPrintControllerHome homeVP = null;
         ApRepVoucherPrintController ejbVP = null;

         try {

            homeVP = (ApRepVoucherPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ApRepVoucherPrintControllerEJB", ApRepVoucherPrintControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in ApRepVoucherPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbVP = homeVP.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in ApRepVoucherPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- TO DO getApVouByVouCode() --
*******************************************************/

		if (frParam != null) {

				final String[] majorNames = {"", " Thousand", " Million",
				" Billion", " Trillion", " Quadrillion", " Quintillion"};

				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
				" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};

				final String[] numNames = {"", " One", " Two", " Three", " Four",
				" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
				" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};


			    AdCompanyDetails adCmpDetails = null;
			    AdBranchDetails adBranchDetails = null;
			    ArrayList list = null;
				boolean isSingleTransaction = false;
				String voucherType = "";

				try {


					ArrayList vouCodeList = new ArrayList();

					if (request.getParameter("forward") != null) {


						isSingleTransaction = true;
	            		vouCodeList.add(new Integer(request.getParameter("voucherCode")));

					} else {

						int i = 0;

						while (true) {

							if (request.getParameter("voucherCode[" + i + "]") != null) {

								vouCodeList.add(new Integer(request.getParameter("voucherCode[" + i + "]")));

								i++;

							} else {

								break;

							}

						}

					}

	            	list = ejbVP.executeApRepVoucherPrint(vouCodeList, user.getCmpCode());

			        // get company

			        adCmpDetails = ejbVP.getAdCompany(user.getCmpCode());

					Iterator chkIter = list.iterator();

			        while (chkIter.hasNext()) {

			        	ApRepVoucherPrintDetails mdetails = (ApRepVoucherPrintDetails)chkIter.next();

			        	voucherType = mdetails.getVpVouType();


			            int num = (int)Math.floor(mdetails.getVpVouAmountDue());

						String str = Common.convertDoubleToStringMoney(mdetails.getVpVouAmountDue(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());

						mdetails.setVpAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " +
						str + "/100 Only");

						/*if (request.getParameter("forward") != null) {

							// set supplier name
							String supplierName = new String(request.getParameter("supplierName"));
							mdetails.setChkSplName(supplierName);

						}*/

			        }


	            } catch(GlobalNoRecordFoundException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherPrint.error.voucherAlreadyDeleted"));

	            } catch(GenVSVNoValueSetValueFoundException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherPrint.error.noValueSetValueFound"));

	            } catch(EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApVoucherPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }

	               return(mapping.findForward("cmnErrorPage"));

	           }

	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("apRepVoucherPrintForm");

	             }

				/** fill report parameters, fill report to pdf and set report session **/

			    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("branchName", user.getCurrentBranch().getBrName());
				parameters.put("companyAddress", companyAddress);
				parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());

				ApRepVoucherPrintDetails vpDetails = (ApRepVoucherPrintDetails) list.get(0);
				parameters.put("postedBy", vpDetails.getVpVouPostedBy());

				String filename = "";
				//if (user.getCurrentBranch().getBrBranchCode().equals("Deecon")) {
				//	filename = "/opt/ofs-resources/" + user.getCompany() + user.get + "/ApRepVoucherPrintDeecon.jasper";
			  //  } else if (user.getCurrentBranch().getBrBranchCode().equals("Premier")) {
			 //  	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepVoucherPrintPremier.jasper";
			 //  } else if (user.getCurrentBranch().getBrBranchCode().equals("Plant 3")) {
			   // 	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepVoucherPrintPlant3.jasper";
			 //  } else if (user.getCurrentBranch().getBrBranchCode().equals("Precision")) {
			 //	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepVoucherPrintPrecision.jasper";
			  // } else {
			  // 	filename = "/opt/ofs-resources/" + user.getCompany() + "/ApRepVoucherPrint.jasper";
			   //}

				//if (user.getCurrentBranch().getBrBranchCode().equals("Deecon"))
				//{
				//filename="/opt/ofs-resources/"+ user.getCompany()+"/ApRepVoucherPrint.jasper";
				//}

				if(isSingleTransaction) {
					if(voucherType.equals("ITEMS")) {
						filename="/opt/ofs-resources/"+ user.getCompany()+"/ApRepVoucherItemPrint.jasper";
					}else {
						filename="/opt/ofs-resources/"+ user.getCompany()+"/ApRepVoucherPrint.jasper";
					}

				}else {
					filename="/opt/ofs-resources/"+ user.getCompany()+"/ApRepVoucherPrint.jasper";
				}

				if (!new java.io.File(filename).exists()) {

		           filename = servlet.getServletContext().getRealPath("jreports/ApRepVoucherPrint.jasper");

		        }

				try {

				    Report report = new Report();

				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters,
				        new ApRepVoucherPrintDS(list)));

				    session.setAttribute(Constants.REPORT_KEY, report);

				} catch(Exception ex) {

				    if(log.isInfoEnabled()) {

				      log.info("Exception caught in ApRepVoucherPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());

				   }

				    return(mapping.findForward("cmnErrorPage"));

				}

				return(mapping.findForward("apRepVoucherPrint"));

		  } else {

		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));

		    return(mapping.findForward("apRepVoucherPrintForm"));

		 }

     } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
          if(log.isInfoEnabled()) {

             log.info("Exception caught in ApRepVoucherPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());

          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
	 private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
		    String soFar;

		    if (number % 100 < 20){
		        soFar = numNames[number % 100];
		        number /= 100;
		       }
		    else {
		        soFar = numNames[number % 10];
		        number /= 10;

		        soFar = tensNames[number % 10] + soFar;
		        number /= 10;
		       }
		    if (number == 0) return soFar;
		    return numNames[number] + " Hundred" + soFar;
		}

		private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
		    /* special case */
		    if (number == 0) { return "zero"; }

		    String prefix = "";

		    if (number < 0) {
		        number = -number;
		        prefix = "Negative";
		      }

		    String soFar = "";
		    int place = 0;

		    do {
		      int n = number % 1000;
		      if (n != 0){
		         String s = this.convertLessThanOneThousand(n, tensNames, numNames);
		         soFar = s + majorNames[place] + soFar;
		        }
		      place++;
		      number /= 1000;
		      } while (number > 0);

		    return (prefix + soFar).trim();
		}

}
