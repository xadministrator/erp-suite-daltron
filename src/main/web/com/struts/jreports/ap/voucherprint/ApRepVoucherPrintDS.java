package com.struts.jreports.ap.voucherprint;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.gl.journalprint.GlRepJournalPrintData;
import com.struts.util.Common;
import com.util.ApRepVoucherPrintDetails;


public class ApRepVoucherPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepVoucherPrintDS(ArrayList apRepVPList) {

   	  Iterator i = apRepVPList.iterator();

   	  while(i.hasNext()) {

   	    ApRepVoucherPrintDetails details = (ApRepVoucherPrintDetails)i.next();

   	    if(details.getVpDrDebit() == 1) {

	   	  	ApRepVoucherPrintData apRepVPData = new ApRepVoucherPrintData(
	   	  		details.getVpVouType(),
	   	  		details.getVpVouSplName(),
	   	  		details.getVpScSupplierClassName(),
	   	  	    Common.convertSQLDateToString(details.getVpVouDate()),
	   	  	    details.getVpVouSplAddress(),
	   	  	    details.getVpVouDocumentNumber(),
	   	  	    details.getVpVouSplTin(),
	   	  	    details.getVpVouReferenceNumber(),
	   	  	    details.getVpVouDescription(),
	   	  	    new Double(details.getVpVouAmountDue()),
	   	  	    details.getVpAmountInWords(),
	   	  	    details.getVpVouCreatedBy(),
	   	  	    details.getVpChkCreatedByDescription(),
	   	  	    details.getVpChkCheckByDescription(),
	   	  	    details.getVpChkApprovedRejectedByDescription(),
	   	  	    Common.convertSQLDateToString(new Date()),
	   	  	    details.getVpVouCheckedBy(),
	   	  	    details.getVpVouApprovedBy(),
	   	  	    details.getVpDrCoaAccountNumber(),
	   	  	    details.getVpDrCoaAccountDescription(),
	   	  	    new Double(details.getVpDrAmount()),
	   	  	    null,
	   	  	    details.getVpVouTerms(), Common.convertSQLDateToString(details.getVpVouDueDate()),
				Common.convertSQLDateToString(details.getVpVouDate()),
				details.getVpApprovalStatus(),
				details.getVpPosted() == (byte)1 ? "YES" : "NO",
				details.getVpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				details.getVpPoNumber(), new Double(details.getVpAmountWoTax()),
				details.getVpVouWithholdingTaxName(),
				details.getVpDrCoaNaturalDesc(), Common.convertCharToString(details.getVpVouFcSymbol()), details.getVpBrName(),
				details.getVpBrCode(), details.getVpVouTaxName(), Common.convertSQLDateToString(details.getVpVouDateCreated()),
				Common.convertSQLDateTimeToString(details.getVpVouDateCreated()), details.getVpPlTaxAmount(), details.getVpPlAmount(), details.getVpDrTaxCoaAccountDescription(),
				details.getVpPoReferenceNumber(), details.getVpPlQuantity(), details.getVpVouPostedBy(),details.getVpVouSplCode(),
				details.getVpVouMisc1(),details.getVpVouMisc2(),details.getVpVouMisc3(),
				details.getVpVouMisc4(),details.getVpVouMisc5(),details.getVpVouMisc6(),
				Common.convertByteToBoolean(details.getVpVouScVatReliefVoucherItem()),
				Common.convertByteToBoolean(details.getVpVliVatRelief()),
				details.getVpVliItemCode(),
				details.getVpVliItemDescription(),
				details.getVpVliLocation(),
				details.getVpVliUnit(),
				details.getVpVliUnitAmount(),
				details.getVpVliQuantity(),
				details.getVpVliAmount(),
				details.getVpVliDiscount(),
				details.getVpVliSupplierName(),
				details.getVpVliTin(),
				details.getVpVliAddress(),
				details.getVpVliProjectCode(),
				details.getVpVliProjectName(),
				details.getVpVliProjectClientId(),
				details.getVpVliProjectTypeCode(),
				details.getVpVliProjectTypeValue(),
				details.getVpVliProjectPhaseName(),
				Common.convertByteToBoolean(details.getVpVliTax()),
				details.getVpVliPropertyCode(),
				details.getVpVliSerialNumber(),
				details.getVpVliSpecs(),
				details.getVpVliCustodian(),
				details.getVpVliExpiryDate()
	   	    		);


             data.add(apRepVPData);

	    } else {

	   	    ApRepVoucherPrintData apRepVPData = new ApRepVoucherPrintData(
	   	    	details.getVpVouType(),
	   	    	details.getVpVouSplName(),
	   	    	details.getVpScSupplierClassName(),
	   	  	    Common.convertSQLDateToString(details.getVpVouDate()),
	   	  	    details.getVpVouSplAddress(),
	   	  	    details.getVpVouDocumentNumber(),
	   	  	    details.getVpVouSplTin(),
	   	  	    details.getVpVouReferenceNumber(),
	   	  	    details.getVpVouDescription(),
	   	  	    new Double(details.getVpVouAmountDue()),
	   	  	    details.getVpAmountInWords(),
	   	  	    details.getVpVouCreatedBy(),
	   	  	    details.getVpChkCreatedByDescription(),
	   	  	    details.getVpChkCheckByDescription(),
	   	  	    details.getVpChkApprovedRejectedByDescription(),
	   	  	    Common.convertSQLDateToString(new Date()),
	   	  	    details.getVpVouCheckedBy(),
	   	  	    details.getVpVouApprovedBy(),
	   	  	    details.getVpDrCoaAccountNumber(),
	   	  	    details.getVpDrCoaAccountDescription(),
	   	  	    null,
	   	  	    new Double(details.getVpDrAmount()), details.getVpVouTerms(), Common.convertSQLDateToString(details.getVpVouDueDate()),
				Common.convertSQLDateToString(details.getVpVouDate()),
				details.getVpApprovalStatus(),
				details.getVpPosted() == (byte)1 ? "YES" : "NO",
				details.getVpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				details.getVpPoNumber(), new Double(details.getVpAmountWoTax()),
				details.getVpVouWithholdingTaxName(),
				details.getVpDrCoaNaturalDesc(), Common.convertCharToString(details.getVpVouFcSymbol()), details.getVpBrName(),
				details.getVpBrCode(), details.getVpVouTaxName(), Common.convertSQLDateToString(details.getVpVouDateCreated()),
				Common.convertSQLDateTimeToString(details.getVpVouDateCreated()), details.getVpPlTaxAmount(), details.getVpPlAmount(), details.getVpDrTaxCoaAccountDescription(),
				details.getVpPoReferenceNumber(), details.getVpPlQuantity(), details.getVpVouPostedBy(),details.getVpVouSplCode(),
				details.getVpVouMisc1(),details.getVpVouMisc2(),details.getVpVouMisc3(),
				details.getVpVouMisc4(),details.getVpVouMisc5(),details.getVpVouMisc6(),
				Common.convertByteToBoolean(details.getVpVouScVatReliefVoucherItem()),
				Common.convertByteToBoolean(details.getVpVliVatRelief()),
				details.getVpVliItemCode(),
				details.getVpVliItemDescription(),
				details.getVpVliLocation(),
				details.getVpVliUnit(),
				details.getVpVliUnitAmount(),
				details.getVpVliQuantity(),
				details.getVpVliAmount(),
				details.getVpVliDiscount(),
				details.getVpVliSupplierName(),
				details.getVpVliTin(),
				details.getVpVliAddress(),
				details.getVpVliProjectCode(),
				details.getVpVliProjectName(),
				details.getVpVliProjectClientId(),
				details.getVpVliProjectTypeCode(),
				details.getVpVliProjectTypeValue(),
				details.getVpVliProjectPhaseName(),
				Common.convertByteToBoolean(details.getVpVliTax()),
				details.getVpVliPropertyCode(),
				details.getVpVliSerialNumber(),
				details.getVpVliSpecs(),
				details.getVpVliCustodian(),
				details.getVpVliExpiryDate()
	   	    		);

   	  	    data.add(apRepVPData);
   	    }


   	  }

   }

   public boolean next() throws JRException {

      index++;
      return (index < data.size());

   }

   public Object getFieldValue(JRField field) throws JRException {

      Object value = null;

      String fieldName = field.getName();
      if("voucherType".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getSupplier();
      }else if("supplierName".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getSupplier();
      }else if("supplierClass".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getSupplier();
      }else if("date".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getDate();
      }else if("address".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getAddress();
      }else if("voucherNumber".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getVoucherNumber();
      }else if("tin".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getTin();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getReferenceNumber();
      }else if("description".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getDescription();
      }else if("amount".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getAmount();
      }else if("amountInWords".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getAmountInWords();
      }else if("createdBy".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getCreatedBy();
      }else if("createdByDescription".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getCreatedByDescription();
      }else if("checkedByDescription".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getCheckedByDescription();
      }else if("approvedByDescription".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getApprovedByDescription();
      }else if("datePrinted".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getDatePrinted();
      }else if("approvedBy".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getApprovedBy();
      }else if("checkedBy".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getCheckedBy();
      }else if("accountNumber".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getAccountDescription();
      }else if("debitAmount".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getDebitAmount();
      }else if("creditAmount".equals(fieldName)){
         value = ((ApRepVoucherPrintData)data.get(index)).getCreditAmount();
      }else if("paymentTerms".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getPaymentTerms();
      }else if("dueDate".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getDueDate();
      }else if("date1".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getDate1();
      }else if("approvalStatus".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getApprovalStatus();
      }else if("posted".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getPosted();
      }else if("showDuplicate".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getShowDuplicate();
      }else if("poNumber".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getPoNumber();
      }else if("amountWoTax".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getAmountWoTax();
      }else if("withholdingTaxCode".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getWithholdingTaxCode();
      }else if("naturalAccntDesc".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getNaturalAccntDesc();
      }else if("currencySymbol".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getCurrencySymbol();
      }else if("branchName".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getBranchName();
      }else if("branchCode".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getBranchCode();
      }else if("taxCode".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getTaxCode();
      }else if("dateCreated".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getDateCreated();
        System.out.println("Date2: "+value);
      }else if("timeCreated".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getTimeCreated();
          System.out.println("Time2: "+value);
      }else if("plTaxAmount".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getplTaxAmount();
      }else if("plAmount".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getplAmount();
      }else if("taxAccountDescription".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getTaxAccountDescription();
      }else if("poReferenceNumber".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getpoReferenceNumber();
      }else if("plQuantity".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getplQuantity();
      }else if("postedBy".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getPostedBy();
      }else if("supplierCode".equals(fieldName)){
        value = ((ApRepVoucherPrintData)data.get(index)).getSupplierCode();
      }else if("misc1".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc1() ;
      }else if("misc2".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc2();
      }else if("misc3".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc3();
      }else if("misc4".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc4() ;
      }else if("misc5".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc5();
      }else if("misc6".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getMisc6();
      }else if("scVatReliefVoucherItem".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getIsVatReliefVoucherItem();
      }else if("vliVatRelief".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliIsVatRelief();
      }else if("vliItemCode".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliItemCode();
      }else if("vliItemDescription".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliItemCode();
      }else if("vliLocation".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliLocation();
      }else if("vliUnit".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliUnit();
      }else if("vliUnitAmount".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliUnitAmount();
      }else if("vliQuantity".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliQuantity();
      }else if("vliAmount".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliAmount();
      }else if("vliLocation".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliLocation();
      }else if("vliDiscount".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliDiscount();
      }else if("vliSupplierName".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliSupplierName();
      }else if("vliTin".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliTin();
      }else if("vliAddress".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliAddress();
      }else if("vliProjectCode".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectCode();
      }else if("vliProjectName".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectName();
      }else if("vliProjectClientId".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectClientId();
      }else if("vliProjectTypeCode".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectTypeCode();
      }else if("vliProjectTypeValue".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectTypeValue();
      }else if("vliProjectPhaseName".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliProjectPhaseName();
      }else if("vliIsTax".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliIsTax();
      }else if("vliPropertyCode".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliPropertyCode();
      }else if("vliSerialNumber".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliSerialNumber();
      }else if("vliSpecs".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliSpecs();
      }else if("vliCustodian".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliCustodian();
      }else if("vliExpiryDate".equals(fieldName)){
          value = ((ApRepVoucherPrintData)data.get(index)).getVliExpiryDate();
      }

      return(value);
   }
}
