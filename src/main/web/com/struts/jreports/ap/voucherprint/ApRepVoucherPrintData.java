package com.struts.jreports.ap.voucherprint;

public class ApRepVoucherPrintData implements java.io.Serializable {

   private String voucherType = null;
   private String supplier = null;
   private String supplierClassName = null;
   private String date = null;
   private String date1 = null;
   private String address = null;
   private String voucherNumber = null;
   private String tin = null;
   private String referenceNumber = null;
   private String description = null;
   private Double amount = null;
   private String amountInWords = null;
   private String createdBy = null;
   private String createdByDescription = null;
   private String checkedByDescription = null;
   private String approvedByDescription = null;
   private String datePrinted = null;
   private String checkedBy = null;
   private String approvedBy = null;
   private String dateCreated = null;


   private String paymentTerms = null;
   private String dueDate = null;

   private String accountNumber = null;
   private String accountDescription = null;
   private Double debitAmount = null;
   private Double creditAmount = null;
   private Double amountWoTax = null;

   private String approvalStatus = null;
   private String posted = null;

   private Boolean showDuplicate = null;
   private String poNumber = null;
   private String withholdingTaxCode = null;
   private String naturalAccntDesc = null;
   private String currencySymbol = null;

   private String branchName = null;
   private String branchCode = null;
   private String timeCreated = null;
   private String taxCode = null;
   private Double plTaxAmount = null;
   private Double plAmount = null;
   private String taxAccountDescription = null;
   private String poReferenceNumber = null;
   private Double plQuantity = null;
   private String postedBy = null;
   private String supplierCode = null;
   private String misc1 = null;
   private String misc2 = null;
   private String misc3 = null;
   private String misc4 = null;
   private String misc5 = null;
   private String misc6 = null;

   //voucher line item fields
   private Boolean isVatReliefVoucherItem = null;
   private Boolean vliIsVatRelief = null;
   private String vliItemCode = null;
   private String vliItemDescription = null;
   private String vliLocation = null;
   private String vliUnit = null;
   private Double vliUnitAmount = null;
   private Double vliQuantity = null;
   private Double vliAmount = null;
   private Double vliDiscount = null;
   private String vliSupplierName = null;
   private String vliTin = null;
   private String vliAddress = null;
   private String vliProjectCode = null;
   private String vliProjectName = null;
   private String vliProjectClientId = null;
   private String vliProjectTypeCode = null;
   private String vliProjectTypeValue = null;
   private String vliProjectPhaseName = null;
   private Boolean vliIsTax = null;
   private String vliPropertyCode = null;
   private String vliSerialNumber = null;
   private String vliSpecs = null;
   private String vliCustodian = null;
   private String vliExpiryDate = null;



   public ApRepVoucherPrintData(String voucherType,
	   String supplier,
	   String supplierClassName,
	   String date,
	   String address,
	   String voucherNumber,
	   String tin,
	   String referenceNumber,
	   String description,
	   Double amount,
	   String amountInWords,
	   String createdBy,
	   String createdByDescription,
	   String checkedByDescription,
	   String approvedByDescription,
	   String datePrinted,
	   String checkedBy,
	   String approvedBy,
	   String accountNumber,
	   String accountDescription,
	   Double debitAmount,
	   Double creditAmount,
       String paymentTerms,
       String dueDate,
       String date1,
	   String approvalStatus,
	   String posted,
	   Boolean showDuplicate,
	   String poNumber,
	   Double amountWoTax,
	   String withholdingTaxCode,
	   String naturalAccntDesc,
	   String currencySymbol,
	   String branchName,
	   String branchCode,
	   String taxCode,
	   String dateCreated,
	   String timeCreated,
	   Double plTaxAmount,
	   Double plAmount,
	   String taxAccountDescription,
	   String poReferenceNumber,
	   Double plQuantity,
	   String postedBy,
	   String supplierCode,
	   String misc1,
	   String misc2,
	   String misc3,
	   String misc4,
	   String misc5,
	   String misc6,
	   Boolean isVatReliefVoucherItem,
	   Boolean vliIsVatRelief,
	   String vliItemCode,
	   String vliItemDescription,
	   String vliLocation,
	   String vliUnit,
	   Double vliUnitAmount,
	   Double vliQuantity,
	   Double vliAmount,
	   Double vliDiscount,
   	   String vliSupplierName,
       String vliTin,
       String vliAddress,
       String vliProjectCode,
       String vliProjectName,
       String vliProjectClientId,
       String vliProjectTypeCode,
       String vliProjectTypeValue,
       String vliProjectPhaseName,
       Boolean vliIsTax,
       String vliPropertyCode,
       String vliSerialNumber,
       String vliSpecs,
       String vliCustodian,
       String vliExpiryDate

		   ) {

	   this.voucherType = voucherType;
       this.supplier = supplier;
       this.supplierClassName = supplierClassName;
       this.date = date;
       this.address = address;
       this.voucherNumber = voucherNumber;
       this.tin = tin;
       this.referenceNumber = referenceNumber;
       this.description = description;
       this.amount = amount;
       this.amountInWords = amountInWords;
       this.createdBy = createdBy;
       this.datePrinted = datePrinted;
       this.checkedBy = checkedBy;
       this.approvedBy = approvedBy;
       this.accountNumber = accountNumber;
       this.accountDescription = accountDescription;
       this.debitAmount = debitAmount;
       this.creditAmount = creditAmount;
       this.paymentTerms = paymentTerms;
       this.dueDate = dueDate;
       this.date1 = date1;
       this.approvalStatus = approvalStatus;
       this.posted = posted;
       this.showDuplicate = showDuplicate;
       this.poNumber = poNumber;
       this.amountWoTax = amountWoTax;
       this.withholdingTaxCode = withholdingTaxCode;
       this.naturalAccntDesc = naturalAccntDesc;
       this.currencySymbol = currencySymbol;
       this.branchName = branchName;
       this.branchCode = branchCode;
       this.createdByDescription = createdByDescription;
       this.checkedByDescription = checkedByDescription;
       this.approvedByDescription = approvedByDescription;
       this.taxCode = taxCode;
       this.dateCreated = dateCreated;
       this.timeCreated = timeCreated;
       this.plTaxAmount = plTaxAmount;
       this.plAmount = plAmount;
       this.taxAccountDescription = taxAccountDescription;
       this.poReferenceNumber = poReferenceNumber;
       this.plQuantity = plQuantity;
       this.postedBy = postedBy;
       this.supplierCode = supplierCode;
       this.misc1 = misc1;
       this.misc2 = misc2;
       this.misc3 = misc3;
       this.misc4 = misc4;
       this.misc5 = misc5;
       this.misc6 = misc6;
       this.isVatReliefVoucherItem = isVatReliefVoucherItem;
       this.vliItemCode = vliItemCode;
       this.vliItemDescription = vliItemDescription;
       this.vliLocation = vliLocation;
       this.vliUnit = vliUnit;
       this.vliUnitAmount = vliUnitAmount;
       this.vliQuantity = vliQuantity;
       this.vliAmount = vliAmount;
       this.vliDiscount = vliDiscount;
       this.vliIsVatRelief = isVatReliefVoucherItem;
       this.vliSupplierName = vliSupplierName;
       this.vliTin = vliTin;
       this.vliAddress = vliAddress;
       this.vliProjectCode = vliProjectCode;
       this.vliProjectName = vliProjectName;
       this.vliProjectTypeCode = vliProjectTypeCode;
       this.vliProjectTypeValue = vliProjectTypeValue;
       this.vliProjectPhaseName = vliProjectPhaseName;
       this.vliIsTax = vliIsTax;
       this.vliPropertyCode =vliPropertyCode;
       this.vliSerialNumber =vliSerialNumber;
       this.vliSpecs =vliSpecs;
       this.vliCustodian = vliCustodian;
       this.vliExpiryDate = vliExpiryDate;
   }

   public String getVoucherType() {

	      return voucherType;

	   }


   public String getSupplier() {

      return supplier;

   }

   public String getSupplierClassName() {
	   return supplierClassName;
   }

   public String getDate() {

      return date;

   }

   public String getAddress() {

      return address;

   }

   public String getVoucherNumber() {

      return voucherNumber;

   }

   public String getTin() {

      return tin;

   }

   public String getReferenceNumber() {

      return referenceNumber;

   }

   public String getDescription() {

      return description;

   }

   public Double getAmount() {

      return amount;

   }

   public String getCreatedBy() {

      return createdBy;

   }

   public String getCreatedByDescription() {

		return createdByDescription;

	}

   public String getCheckedByDescription() {

		return checkedByDescription;

	}

	public String getApprovedByDescription() {

		return approvedByDescription;

	}

   public String getDatePrinted() {

      return datePrinted;

   }

   public String getCheckedBy() {

      return checkedBy;

   }

   public String getApprovedBy() {

      return approvedBy;

   }

   public String getAccountNumber() {

      return accountNumber;

   }

   public String getAccountDescription() {

      return accountDescription;

   }

   public Double getDebitAmount() {

      return debitAmount;

   }

   public Double getCreditAmount() {

      return creditAmount;

   }

   public String getPaymentTerms() {

      return paymentTerms;

   }

   public String getDueDate() {

      return dueDate;

   }

   public String getDate1() {

      return date1;

   }

   public String getApprovalStatus() {

   	  return approvalStatus;

   }


   public String getPosted() {

   	  return posted;

   }

   public Boolean getShowDuplicate() {

   	  return showDuplicate;

   }

   public String getPoNumber() {

   	  return poNumber;

   }

   public Double getAmountWoTax() {

   	  return amountWoTax;

   }

   public String getWithholdingTaxCode() {

   	  return withholdingTaxCode;

   }

   public String getNaturalAccntDesc() {

   	  return naturalAccntDesc;

   }

   public String getCurrencySymbol() {

   	  return currencySymbol;

   }

   public String getAmountInWords() {

	   return amountInWords;
   }

   public String getBranchName() {

	   return branchName;

   }

   public String getBranchCode() {

	   return branchCode;

   }

   public String getTaxCode() {

	   return taxCode;

   }

   public String getDateCreated() {

	   return dateCreated;

   }



   public String getTimeCreated() {

	   return timeCreated;

   }

   public Double getplTaxAmount() {

      return plTaxAmount;

   }

   public Double getplAmount() {

      return plAmount;

   }

   public String getTaxAccountDescription() {

	   return taxAccountDescription;

   }

   public String getpoReferenceNumber() {

	   return poReferenceNumber;

   }
   public Double getplQuantity() {

      return plQuantity;

   }
   public String getPostedBy() {

	      return postedBy;

	   }
   public String getSupplierCode() {

	      return supplierCode;

	   }

   public String getMisc1() {

	      return misc1;

	   }
   public String getMisc2() {

	      return misc2;

	   }
   public String getMisc3() {

	      return misc3;

	   }

   public String getMisc4() {

	      return misc4;

	   }
   public String getMisc5() {

	      return misc5;

	   }
	public String getMisc6() {

	      return misc6;

	   }


	public Boolean getIsVatReliefVoucherItem() {
		return isVatReliefVoucherItem;
	}

	public String  getVliItemCode() {
		return vliItemCode;
	}

	public String getVliItemDescription() {
		return vliItemDescription;
	}
	public String getVliLocation () {
		return vliLocation;
	}
	public String getVliUnit() {
		return vliUnit;
	}
	public Double getVliUnitAmount() {
		return vliUnitAmount;
	}
	public Double getVliQuantity() {
		return vliQuantity;
	}
	public Double getVliAmount() {
		return vliAmount;
	}
	public Double getVliDiscount() {
		return vliDiscount;
	}
	public Boolean getVliIsVatRelief() {
		return vliIsVatRelief;
	}
	public String getVliSupplierName () {
		return vliSupplierName;
	}
	public String getVliTin () {
		return vliTin;
	}
	public String getVliAddress () {
		return vliAddress;
	}
	public String getVliProjectCode() {
		return vliProjectCode;
	}
	public String getVliProjectName() {
		return vliProjectName;
	}
	public String getVliProjectClientId() {
		return vliProjectClientId;
	}
	public String getVliProjectTypeCode () {
		return vliProjectTypeCode;
	}
	public String getVliProjectTypeValue () {
		return vliProjectTypeValue;
	}
	public String getVliProjectPhaseName () {
		return vliProjectPhaseName;
	}
	public Boolean getVliIsTax () {
		return vliIsTax;
	}

	public String getVliPropertyCode () {
		return vliPropertyCode;
	}

	public String getVliSerialNumber() {
		return vliSerialNumber;
	}

	public String getVliSpecs() {
		return vliSpecs;
	}

	public String getVliCustodian() {
		return vliCustodian;
	}

	public String getVliExpiryDate() {
		return vliExpiryDate;
	}
}
