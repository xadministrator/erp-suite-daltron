package com.struts.jreports.ap.withholdingtaxexpanded;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ApRepWithholdingTaxExpandedDetails;

public class ApRepWithholdingTaxExpandedDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ApRepWithholdingTaxExpandedDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ApRepWithholdingTaxExpandedDetails details = (ApRepWithholdingTaxExpandedDetails)i.next();
                  
	     ApRepWithholdingTaxExpandedData argData = new ApRepWithholdingTaxExpandedData(
	     new Long(details.getWteSequenceNumber()), details.getWteSplTinNumber(), details.getWteSplSupplierName(),
	     details.getWteNatureOfPayment(), details.getWteAtcCode(), new Double(details.getWteTaxBase()),
	     new Double(details.getWteRate()), new Double(details.getWteTaxWithheld()), details.getWteTxnDate());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("sequenceNumber".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getSequenceNumber();
      }else if("tinNumber".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getTinNumber();   
      }else if("supplierCode".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getSupplierCode();
      }else if("natureOfIncomePayment".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getNatureOfIncomePayment();
      }else if("atcCode".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getAtcCode();
      }else if("taxBase".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getTaxBase();  
      }else if("taxRate".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getTaxRate(); 
      }else if("taxWithheld".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getTaxWithheld();               
      }else if("txnDate".equals(fieldName)){
         value = ((ApRepWithholdingTaxExpandedData)data.get(index)).getTxnDate();               
      }      
      return(value);
   }
}
