package com.struts.jreports.ap.withholdingtaxexpanded;

import java.util.Date;


public class ApRepWithholdingTaxExpandedData implements java.io.Serializable {
	
   private Long sequenceNumber = null;
   private String tinNumber = null;
   private String supplierCode = null;
   private String natureOfIncomePayment = null;
   private String atcCode = null;
   private Double taxBase = null;
   private Double taxRate = null;
   private Double taxWithheld = null;
   private Date txnDate = null;

   public ApRepWithholdingTaxExpandedData(Long sequenceNumber, String tinNumber, String supplierCode,
      String natureOfIncomePayment, String atcCode, Double taxBase, Double taxRate, Double taxWithheld, Date txnDate) {
      	
      this.sequenceNumber = sequenceNumber;
      this.tinNumber = tinNumber;
      this.supplierCode = supplierCode;
      this.natureOfIncomePayment = natureOfIncomePayment ;
      this.atcCode = atcCode;
      this.taxBase = taxBase;
      this.taxRate = taxRate;
      this.taxWithheld = taxWithheld;
      this.txnDate = txnDate;
      
   }

	
	public Long getSequenceNumber() {
		return (this.sequenceNumber); 
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber; 
	}

	public String getTinNumber() {
		return (this.tinNumber); 
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber; 
	}

	public String getSupplierCode() {
		return (this.supplierCode); 
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode; 
	}

	public String getNatureOfIncomePayment() {
		return (this.natureOfIncomePayment); 
	}

	public void setNatureOfIncomePayment(String natureOfIncomePayment) {
		this.natureOfIncomePayment = natureOfIncomePayment; 
	}

	public String getAtcCode() {
		return (this.atcCode); 
	}

	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode; 
	}

	public Double getTaxBase() {
		return (this.taxBase); 
	}

	public void setTaxBase(Double taxBase) {
		this.taxBase = taxBase; 
	}

	public Double getTaxRate() {
		return (this.taxRate); 
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate; 
	}

	
	public Double getTaxWithheld() {
		return (this.taxWithheld); 
	}

	public void setTaxWithheld(Double taxWithheld) {
		this.taxWithheld = taxWithheld; 
	}
	
	public Date getTxnDate() {
		return (this.txnDate); 
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate; 
	}

}
