package com.struts.jreports.ap.withholdingtaxexpanded;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApRepWithholdingTaxExpandedForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String dateFrom = null;
   private String dateTo = null;
   private boolean includeUnposted = false;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();  
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList apRepBrWthhldngTxExpnddList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }
   
   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }
   
   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getType(){
      return(type);
   }

   public void setType(String type){
      this.type = type;
   }

   public ArrayList getTypeList(){
      return(typeList);
   }

   public void setTypeList(String type){
      typeList.add(type);
   }

   public void clearTypeList(){
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
   }
 
   public String getSupplierClass(){
      return(supplierClass);
   }

   public void setSupplierClass(String supplierClass){
      this.supplierClass = supplierClass;
   }

   public ArrayList getSupplierClassList(){
      return(supplierClassList);
   }

   public void setSupplierClassList(String supplierClass){
      supplierClassList.add(supplierClass);
   }

   public void clearSupplierClassList(){
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
   } 
   
   public boolean getIncludeUnposted() {
   	
   	  return includeUnposted;
   	
   }
   
   public void setIncludeUnposted(boolean includeUnposted) {
   	
   	  this.includeUnposted = includeUnposted;
   	
   }
  
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Object[] getApRepBrWthhldngTxExpnddList(){
       
       return apRepBrWthhldngTxExpnddList.toArray();
       
   }
   
   public ApRepBranchWithholdingTaxExpandedList getApRepBrWthhldngTxExpnddByIndex(int index){
       
       return ((ApRepBranchWithholdingTaxExpandedList)apRepBrWthhldngTxExpnddList.get(index));
       
   }
   
   public int getApRepBrWthhldngTxExpnddListSize(){
       
       return(apRepBrWthhldngTxExpnddList.size());
       
   }
   
   public void saveApRepBrWthhldngTxExpnddList(Object newApRepBrWthhldngTxExpnddList){
       
       apRepBrWthhldngTxExpnddList.add(newApRepBrWthhldngTxExpnddList);   	  
       
   }
   
   public void clearApRepBrWthhldngTxExpnddList(){
       
       apRepBrWthhldngTxExpnddList.clear();
       
   }
   
   public void setApRepBrWthhldngTxExpnddList(ArrayList apRepBrWthhldngTxExpnddList) {
       
       this.apRepBrWthhldngTxExpnddList = apRepBrWthhldngTxExpnddList;
       
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<apRepBrWthhldngTxExpnddList.size(); i++) {
           
           ApRepBranchWithholdingTaxExpandedList  list = (ApRepBranchWithholdingTaxExpandedList)apRepBrWthhldngTxExpnddList.get(i);
           list.setBranchCheckbox(false);	       
           
       } 
       
      goButton = null;
      closeButton = null;
      supplierCode = null;
      supplierClass = Constants.GLOBAL_BLANK; 
      type = Constants.GLOBAL_BLANK;
      dateFrom = Common.convertSQLDateToString(new java.util.Date());
      dateTo = Common.convertSQLDateToString(new java.util.Date());
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
	  
	  includeUnposted = false;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("withholdingTaxExpanded.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
		    errors.add("dateTo", new ActionMessage("withholdingTaxExpanded.error.dateToInvalid"));
		 }	
		 
      }
      return(errors);
   }
}
