package com.struts.jreports.ar.aging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepAgingController;
import com.ejb.txn.ArRepAgingControllerHome;
import com.struts.jreports.gl.detailtrialbalance.GlRepDetailTrialBalanceDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ArRepAgingDetails;

public final class ArRepAgingAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepAgingAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepAgingForm actionForm = (ArRepAgingForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_AGING_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepAging");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepAgingController EJB
*******************************************************/

         ArRepAgingControllerHome homeAG = null;
         ArRepAgingController ejbAG = null;       

         try {
         	
            homeAG = (ArRepAgingControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepAgingControllerEJB", ArRepAgingControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepAgingAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbAG = homeAG.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepAgingAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP AG Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            ArRepAgingDetails details = null;
            
            String company = null;
            int agingBucket = 0;
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbAG.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();

		       // get preference
		       AdPreferenceDetails adPreferenceDetails = ejbAG.getAdPreference(user.getCmpCode());
		       agingBucket = adPreferenceDetails.getPrfArAgingBucket();
		       
		       // execute report
		       
		       HashMap criteria = new HashMap();
		       
		       if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	           }
		       
	           if (!Common.validateRequired(actionForm.getCustomerName())) {
	        		
	        		criteria.put("customerName", actionForm.getCustomerName());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getCustomerType())) {
	        		
	        		criteria.put("customerType", actionForm.getCustomerType());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        		
	           }
	           
	           if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        		
	           }
	           
	           if (actionForm.getIncludePaid()) {
		    	
			    	criteria.put("includePaid", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includePaid", "NO");
			    	
			   }
	           	           
	           if (actionForm.getIncludeUnpostedTransaction()) {
		    	
			    	criteria.put("includeUnpostedTransaction", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includeUnpostedTransaction", "NO");
			    	
			   }
	           
	           if (actionForm.getIncludeAdvance()) {
			    	
			    	criteria.put("includeAdvance", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includeAdvance", "NO");
			    	
			   }
	           
	           if (actionForm.getIncludeAdvanceOnly()) {
			    	
			    	criteria.put("includeAdvanceOnly", "YES");
			    	
			   } else {
			    	
			    	criteria.put("includeAdvanceOnly", "NO");
			    	
			   }
	           
	           ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrAgListSize(); i++) {
	           	
	           		ArRepBranchAgingList brAgList = (ArRepBranchAgingList)actionForm.getArRepBrAgListByIndex(i);
	           	
	           		if(brAgList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brAgList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       list = ejbAG.executeArRepAging(criteria, branchList, actionForm.getAgingBy(), actionForm.getOrderBy(), 
		       		  						  actionForm.getGroupBy(), actionForm.getCurrency(),user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("arAging.error.noRecordFound"));
                     		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepAgingAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("arRepAging"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
		    parameters.put("date2", Common.convertStringToSQLDate(actionForm.getDate()));
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("groupBy", actionForm.getGroupBy());
		    
		    if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	    		parameters.put("customerCode", actionForm.getCustomerCode());
	    		
	       }
		    
		    if (!Common.validateRequired(actionForm.getCustomerName())) {
        		
	    		parameters.put("customerName", actionForm.getCustomerName());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getCustomerType())) {
	    		
	    		parameters.put("type", actionForm.getCustomerType());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getCustomerClass())) {
	    		
	    		parameters.put("class", actionForm.getCustomerClass());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getDate())) {
	    		
	    		parameters.put("date", actionForm.getDate());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getAgingBy())) {
	    		
	    		parameters.put("agingBy", actionForm.getAgingBy());
	    		
	       }
	       
	       if (!Common.validateRequired(actionForm.getOrderBy())) {
	       	
	       		parameters.put("orderBy", actionForm.getOrderBy());
	       		
	       }
	       
	       if (actionForm.getIncludePaid()) {
		    	
		    	parameters.put("includePaid", "YES");
		    	
		   } else {
		    	
		    	parameters.put("includePaid", "NO");
		    	
		   }
	       
	       if (actionForm.getIncludeUnpostedTransaction()) {
		    	
		    	parameters.put("includeUnposted", "YES");
		    	
		   } else {
		    	
		    	parameters.put("includeUnposted", "NO");
		    	
		   }
	       
	       if (actionForm.getIncludeAdvance()) {
		    	
	    	   parameters.put("includeAdvance", "YES");
		    	
		   } else {
		    	
			   parameters.put("includeAdvance", "NO");
		    	
		   }
	       
	       if (actionForm.getIncludeAdvanceOnly()) {
		    	
	    	   parameters.put("includeAdvanceOnly", "YES");
		    	
		   } else {
		    	
			   parameters.put("includeAdvanceOnly", "NO");
		    	
		   }
	       
	       if (agingBucket > 1) {
	    	   
	    	   parameters.put("agingBucket1", "1-" + agingBucket + " days");
			   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
			   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
			   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
			   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");
			   
	       } else {
			   
			   parameters.put("agingBucket1", "1 day");
			   parameters.put("agingBucket2", "2 days");
			   parameters.put("agingBucket3", "3 days");
			   parameters.put("agingBucket4", "4 days");
			   parameters.put("agingBucket5", "Over 4 days");
			   
		   }
	       	       
	       String branchMap = null;
		   boolean first = true;
		   for(int j=0; j < actionForm.getArRepBrAgListSize(); j++) {

			   ArRepBranchAgingList brList = (ArRepBranchAgingList)actionForm.getArRepBrAgListByIndex(j);

			   if(brList.getBranchCheckbox() == true) {
				   if(first) {
					   branchMap = brList.getBranchName();
					   first = false;
				   } else {
					   branchMap = branchMap + ", " + brList.getBranchName();
				   }
			   }

		   }
		   parameters.put("branchMap", branchMap);
	       
	       String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepAging.jasper";
	       
	       if (!new java.io.File(filename).exists()) {
	       		    		    
	          filename = servlet.getServletContext().getRealPath("jreports/ArRepAging.jasper");
		    
	       }
		    		    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepAgingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepAgingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepAgingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepAgingDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepAgingAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP AG Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP AG Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCustomerTypeList();           	
            	
	            	list = ejbAG.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerTypeList((String)i.next());
	            			
	            		}
	            			            		
	            	}
	            	
	            	actionForm.clearCustomerBatchList();           	
	         		
	         		list = ejbAG.getAdLvCustomerBatchAll(user.getCmpCode());
	         		
	         		if (list == null || list.size() == 0) {
	         			
	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
	         			
	         		} else {
	         			
	         			i = list.iterator();
	         			
	         			while (i.hasNext()) {
	         				
	         				actionForm.setCustomerBatchList((String)i.next());
	         				
	         			}
	         			
	         		}
	            	
	            	actionForm.clearCustomerClassList();           	
            	
	            	list = ejbAG.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            			            		
	            	}   
	            	
	            	actionForm.clearArRepBrAgList();
	         		
	         		list = ejbAG.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchAgingList arRepBrAgList = new ArRepBranchAgingList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			arRepBrAgList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrAgList(arRepBrAgList);
	         			
	         		}
	            				       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepAgingAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("arRepAging"));		          
			            
			 } else {
			 	
			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));
			
			    return(mapping.findForward("cmnMain"));
			
			 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepAgingAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
