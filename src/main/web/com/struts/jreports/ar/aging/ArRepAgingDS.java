package com.struts.jreports.ar.aging;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.agingsummary.ApRepAgingSummaryData;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.ArRepAgingDetails;

public class ArRepAgingDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepAgingDS(ArrayList list){
      
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepAgingDetails details = (ArRepAgingDetails)i.next();
         
         String groupBy = null;
         
         if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE)) {
         	
         	groupBy = details.getAgCustomerCode() + "-" + details.getAgCustomerName();
         	 
         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_NAME)) {
          	
          	groupBy = details.getAgCustomerName();
          	
         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE)) {
         	
         	groupBy = details.getAgCustomerType();
         	
         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS)) {
         	
         	groupBy = details.getAgCustomerClass();
         	
         } else {
         	
         	groupBy = "";
         	
         }
                                    
	     ArRepAgingData agData = new ArRepAgingData(groupBy,
	    	 details.getAgCustomerCode(),
	     	 details.getAgCustomerName(),
	     	 details.getAgCustomerClass(),
	         details.getAgInvoiceNumber(), 
	         details.getAgReferenceNumber(),
	         details.getAgInstallmentNumber() != 0 ? Common.convertShortToString(details.getAgInstallmentNumber()) : null, 
	         new Double(details.getAgInstallmentAmount()),
	         new Double(details.getAgAmount()),
			 details.getAgBucket0() != 0d ? new Double(details.getAgBucket0()) : null,
	         details.getAgBucket1() != 0d ? new Double(details.getAgBucket1()) : null, 
	         details.getAgBucket2() != 0d ? new Double(details.getAgBucket2()) : null,
	         details.getAgBucket3() != 0d ? new Double(details.getAgBucket3()) : null, 	         
	         details.getAgBucket4() != 0d ? new Double(details.getAgBucket4()) : null,
	         details.getAgBucket5() != 0d ? new Double(details.getAgBucket5()) : null,
	         Common.convertSQLDateToString(details.getAgTransactionDate()),
	         details.getAgTransactionDate(),
	         Common.convertSQLDateToString(details.getAgIpsDueDate()),
	         details.getAgLastOrDate(),
	         details.getAgAgedInMonth(),
	         details.getAgInvoiceAge() != 0d ? new Double(details.getAgInvoiceAge()) : null,
	         Common.convertCharToString(details.getAgVouFcSymbol()), details.getAgDescription(), 
	         details.getAgSalesPerson());
		    
         data.add(agData);
      }     
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("groupBy".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getGroupBy();
      }else if("customer".equals(fieldName)){
        value = ((ArRepAgingData)data.get(index)).getCustomerCode();
      }else if("customerName".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getCustomerName();
      }else if("customerClass".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getCustomerClass();
      }else if("invoiceNumber".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getInvoiceNumber();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getReferenceNumber();
      }else if("installmentNumber".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getInstallmentNumber();
      }else if("amount".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getAmount();
      }else if("installmentAmount".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getInstallmentAmount();
      }else if("bucket0".equals(fieldName)) {
      	 value = ((ArRepAgingData)data.get(index)).getBucket0();
      }else if("bucket1".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getBucket1();
      }else if("bucket2".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getBucket2();
      }else if("bucket3".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getBucket3();
      }else if("bucket4".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getBucket4();
      }else if("bucket5".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getBucket5();
      }else if("transactionDate".equals(fieldName)){
        value = ((ArRepAgingData)data.get(index)).getTransactionDate();  
      }else if("transactionDate2".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getTransactionDate2();
      }else if("dueDate".equals(fieldName)){
        value = ((ArRepAgingData)data.get(index)).getDueDate(); 
      }else if("lastOrDate".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getLastOrDate();
      }else if("agedInMonth".equals(fieldName)){
          value = ((ArRepAgingData)data.get(index)).getAgedInMonth();
      }else if("invoiceAge".equals(fieldName)){
        value = ((ArRepAgingData)data.get(index)).getInvoiceAge();   
      }else if("currencySymbol".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getCurrencySymbol();
      }else if("description".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getDescription();
      }else if("salesPerson".equals(fieldName)){
         value = ((ArRepAgingData)data.get(index)).getSalesperson();
      }

      return(value);
   }
}
