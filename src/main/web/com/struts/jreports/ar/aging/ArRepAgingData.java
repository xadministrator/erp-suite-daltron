package com.struts.jreports.ar.aging;

import java.util.Date;

public class ArRepAgingData implements java.io.Serializable{
   
   private String groupBy = null;
   private String customerCode = null;
   private String customerName = null;
   private String customerClass = null;
   private String invoiceNumber = null;
   private String referenceNumber = null;
   private String installmentNumber = null;
   private Double installmentAmount = null;
   private Double amount = null;
   private Double bucket0 = null;
   private Double bucket1 = null;
   private Double bucket2 = null;
   private Double bucket3 = null;
   private Double bucket4 = null;
   private Double bucket5 = null;
   private String transactionDate = null;   
   private Date transactionDate2 = null;  
   private String dueDate = null;
   private Date lastOrdate = null;
   private Double agedInMonth = null;
   private Double invoiceAge = null;
   private String currencySymbol = null;
   private String description = null;
   private String salesPerson = null;
   
   
   public ArRepAgingData(String groupBy, String customerCode, String customerName, String customerClass, String invoiceNumber, String referenceNumber,
       String installmentNumber, Double installmentAmount, Double amount, Double bucket0 ,Double bucket1, Double bucket2,
       Double bucket3, Double bucket4, Double bucket5, String transactionDate, Date transactionDate2, String dueDate, Date lastOrdate, Double agedInMonth, Double invoiceAge, String currencySymbol,
       String description, String salesPerson){
      
       this.groupBy = groupBy;
       this.customerCode = customerCode;
       this.customerName = customerName;
       this.customerClass = customerClass;
       this.invoiceNumber = invoiceNumber;
       this.referenceNumber = referenceNumber;
       this.installmentNumber = installmentNumber;
       this.installmentAmount = installmentAmount;
       this.amount = amount;
       this.bucket0 = bucket0;
       this.bucket1 = bucket1;
       this.bucket2 = bucket2;
       this.bucket3 = bucket3;
       this.bucket4 = bucket4;
       this.bucket5 = bucket5;
       this.transactionDate = transactionDate;
       this.transactionDate2 = transactionDate2;
       this.dueDate = dueDate;
       this.lastOrdate = lastOrdate;
       this.agedInMonth = agedInMonth;
       this.invoiceAge = invoiceAge;
       this.currencySymbol = currencySymbol;
       this.description = description;
       this.salesPerson = salesPerson;
   }

   public String getGroupBy() {
   	
   	   return groupBy;
   	
   }
   
   public String getCustomerCode() {
   	
   	   return customerCode;
   	   
   }
   
   public String getCustomerName() {
	   
	   return customerName;
	   
   }
   
   public String getCustomerClass() {
	   
	   return customerClass;
	   
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public String getInstallmentNumber() {
   	
   	  return installmentNumber;
   	
   }
   
   public Double getInstallmentAmount() {
	   	
	   	  return installmentAmount;
	   	
	   }
   
   public Double getAmount() {
   	
   	  return amount;
   	
   }
   
   public Double getBucket0() {
   	
   	  return bucket0;
   	  
   }
   
   public Double getBucket1() {
   	
   	  return bucket1;
   	
   }
   
   public Double getBucket2() {
   	
   	  return bucket2;
   	
   }
   
   public Double getBucket3() {
   	
   	  return bucket3;
   	
   }
   
   public Double getBucket4() {
   	
   	  return bucket4;
   	
   }
   
   public Double getBucket5() {
   	
   	  return bucket5;
   	
   }
   
   public String getTransactionDate() {
   	
   	  return transactionDate;
   	
   }
   
   public Date getTransactionDate2() {
	   	
	   	  return transactionDate2;
	   	
	   }
   
   public String getDueDate() {
	   
	  return dueDate;
	  
   }
   
   public Date getLastOrDate() {
	   
		  return lastOrdate;
		  
	   }
   
   public Double getAgedInMonth() {
	   
		  return agedInMonth;
		  
	   }
   
   
   
   public Double getInvoiceAge() {
	   
	   return invoiceAge;
	   
   }
   
   public String getCurrencySymbol() {

	   return currencySymbol;

   }
      
   public String getDescription() {

	   return description;

   }
   
   public String getSalesperson() {

	   return salesPerson;

   }
}
