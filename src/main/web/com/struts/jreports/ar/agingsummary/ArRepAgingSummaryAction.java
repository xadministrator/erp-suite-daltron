package com.struts.jreports.ar.agingsummary;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepAgingSummaryController;
import com.ejb.txn.ArRepAgingSummaryControllerHome;
import com.struts.jreports.gl.detailtrialbalance.GlRepDetailTrialBalanceDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ArRepAgingSummaryDetails;

public class ArRepAgingSummaryAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepAgingSummaryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepAgingSummaryForm actionForm = (ArRepAgingSummaryForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_AGING_SUMMARY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepAgingSummary");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepAgingController EJB
*******************************************************/

         ArRepAgingSummaryControllerHome homeAG = null;
         ArRepAgingSummaryController ejbAG = null;       

         try {
         	
            homeAG = (ArRepAgingSummaryControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepAgingSummaryControllerEJB", ArRepAgingSummaryControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepAgingSummaryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbAG = homeAG.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepAgingSummaryAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP AG Go Action --
*******************************************************/
        
         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            ArRepAgingSummaryDetails details = null;
            
            String company = null;
            int agingBucket = 0;      
            
            DataSource dataSource =  (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
            Connection conn = null;
            CallableStatement stmt = null;      
            
            ResultSet rs = null;	
            	
            try {
            
                // Get Company
                AdCompanyDetails adCmpDetails = ejbAG.getAdCompany(user.getCmpCode());
                company = adCmpDetails.getCmpName();

                // Get Criteria
                HashMap criteria = getCriteria(actionForm);
            
                // Get Branches
                ArrayList branchList = new ArrayList();
                for (int i = 0; i < actionForm.getArRepBrAgListSize(); i++) {

                    ArRepBranchAgingSummaryList brAgList = (ArRepBranchAgingSummaryList) actionForm
                            .getArRepBrAgListByIndex(i);

                    if (brAgList.getBranchCheckbox() == true) {
                        AdBranchDetails brDetails = new AdBranchDetails();
                        brDetails.setBrCode(brAgList.getBranchCode());
                        branchList.add(brDetails);
                    }
                }
            
                String storedProcedureName = "{ call sp_ArRepAgingSummary(?,?,?) }";

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = sdf.parse(actionForm.getDate());

              //  LocalDate dateSelected = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                System.out.println("date = " + date);
            
                conn = dataSource.getConnection();
                stmt = (CallableStatement) conn.prepareCall(storedProcedureName);
                stmt.setDate(1, new java.sql.Date(date.getTime()));
                stmt.setByte(2, Common.convertBooleanToByte(!actionForm.getIncludeUnpostedTransaction()));
                rs = stmt.executeQuery();

                int rowcount = stmt.getInt(3);

                System.out.println("rowcount = " + rowcount);

                if (rowcount <= 0) {
                    throw new GlobalNoRecordFoundException();
                }

                list = ejbAG.executeArRepAgingSummarySP(rs, criteria, branchList, actionForm.getAgingBy(),
                        actionForm.getGroupBy(), actionForm.getCurrency(), user.getCmpCode());


             } catch (GlobalNoRecordFoundException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
              new ActionMessage("arAgingSummary.error.noRecordFound"));

             } catch(EJBException ex) {

                  if(log.isInfoEnabled()) {
                             log.info("EJBException caught in ArRepAgingSummaryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());

                         }

                         return(mapping.findForward("cmnErrorPage"));

             }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("arRepAgingSummary"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    // Fill report parameters, Fill report to pdf and Set report session
                Map parameters = setReportParameters(user, actionForm, company);

                // Get Preference
                AdPreferenceDetails adPreferenceDetails = ejbAG.getAdPreference(user.getCmpCode());
                agingBucket = adPreferenceDetails.getPrfArAgingBucket(); // Return 30

                if (agingBucket > 1) {

                    parameters.put("agingBucket1", "1-" + agingBucket + " days");
                    parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
                    parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
                    parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
                    parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");

                } else {

                    parameters.put("agingBucket1", "1 day");
                    parameters.put("agingBucket2", "2 days");
                    parameters.put("agingBucket3", "3 days");
                    parameters.put("agingBucket4", "4 days");
                    parameters.put("agingBucket5", "Over 4 days");
                }
		    
		    
		    
		    String branchMap = null;
                boolean first = true;
                for (int j = 0; j < actionForm.getArRepBrAgListSize(); j++) {

                    ArRepBranchAgingSummaryList brList = (ArRepBranchAgingSummaryList) actionForm
                            .getArRepBrAgListByIndex(j);

                    if (brList.getBranchCheckbox() == true) {
                        if (first) {
                            branchMap = brList.getBranchName();
                            first = false;
                        } else {
                            branchMap = branchMap + ", " + brList.getBranchName();
                        }
                    }
                }

                parameters.put("branchMap", branchMap);

		    
		    
		    
		    
	
	       
	       String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepAgingSummary.jasper";
	       
	       if (!new java.io.File(filename).exists()) {
	       		    		    
	          filename = servlet.getServletContext().getRealPath("jreports/ArRepAgingSummary.jasper");
		    
	       }
		    		    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepAgingSummaryDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepAgingSummaryDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepAgingSummaryDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepAgingSummaryDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepAgingSummaryAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   		        	          
/*******************************************************
   -- AP AG Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP AG Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
		            actionForm.reset(mapping, request);

			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCustomerTypeList();           	
            	
	            	list = ejbAG.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerTypeList((String)i.next());
	            			
	            		}
	            			            		
	            	} 
	            	
	            	actionForm.clearCustomerBatchList();           	
	         		
                        list = ejbAG.getAdLvCustomerBatchAll(user.getCmpCode());

                        if (list == null || list.size() == 0) {

                                actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

                        } else {

                                i = list.iterator();

                                while (i.hasNext()) {

                                        actionForm.setCustomerBatchList((String)i.next());

                                }

                        }
                        
                        
                        actionForm.clearInvoiceBatchList();
                      
                        list = ejbAG.getArIbAll(user.getCurrentBranch().getBrCode(), user.getCmpCode());
                        
	            	if (list == null || list.size() == 0) {
	            		
                            actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
                            i = list.iterator();

                            while (i.hasNext()) {

                                actionForm.setInvoiceBatchList((String)i.next());

                            }
	            			            		
	            	}   
                        
                        
                        
                        actionForm.clearCustomerClassList();           	
            	
	            	list = ejbAG.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            			            		
	            	}   
	            	
	            	actionForm.clearArRepBrAgList();
	         		
	         		list = ejbAG.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchAgingSummaryList arRepBrAgList = new ArRepBranchAgingSummaryList(actionForm, 
	         					details.getBrBranchCode(),details.getBrName(), details.getBrCode());
	         			arRepBrAgList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrAgList(arRepBrAgList);
	         			
	         		}
	            				       
				} catch(EJBException ex) {
			    	
			        if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepAgingSummaryAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepAgingSummary"));		          
			            
			 } else {
			 	
			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));
			
			    return(mapping.findForward("cmnMain"));
			
			 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepAgingSummaryAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
    
    private Map setReportParameters(User user, ArRepAgingSummaryForm actionForm, String company) {
        Map parameters = new HashMap();
        parameters.put("company", company);
        parameters.put("printedBy", user.getUserName());
        parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
        parameters.put("date2", Common.convertStringToSQLDate(actionForm.getDate()));
        parameters.put("viewType", actionForm.getViewType());
        parameters.put("groupBy", actionForm.getGroupBy());

        if (!Common.validateRequired(actionForm.getCustomerCode())) {

            parameters.put("customerCode", actionForm.getCustomerCode());
        }

        if (!Common.validateRequired(actionForm.getCustomerName())) {

            parameters.put("customerName", actionForm.getCustomerName());
        }

        if (!Common.validateRequired(actionForm.getCustomerType())) {

            parameters.put("type", actionForm.getCustomerType());
        }

        if (!Common.validateRequired(actionForm.getCustomerClass())) {

            parameters.put("class", actionForm.getCustomerClass());
        }

        if (!Common.validateRequired(actionForm.getDate())) {

            parameters.put("date", actionForm.getDate());
        }

        if (!Common.validateRequired(actionForm.getAgingBy())) {

            parameters.put("agingBy", actionForm.getAgingBy());
        }

        if (actionForm.getIncludePaid()) {

            parameters.put("includePaid", "YES");

        } else {

            parameters.put("includePaid", "NO");
        }

        if (actionForm.getIncludeUnpostedTransaction()) {

            parameters.put("includeUnposted", "YES");

        } else {

            parameters.put("includeUnposted", "NO");
        }

        if (actionForm.getIncludeAdvance()) {

            parameters.put("includeAdvance", "YES");

        } else {

            parameters.put("includeAdvance", "NO");
        }

        if (actionForm.getIncludeAdvanceOnly()) {

            parameters.put("includeAdvanceOnly", "YES");

        } else {

            parameters.put("includeAdvanceOnly", "NO");
        }
        return parameters;
    }
    
    
    private HashMap getCriteria(ArRepAgingSummaryForm actionForm) {
        HashMap criteria = new HashMap();

        if (!Common.validateRequired(actionForm.getCustomerCode())) {

            criteria.put("customerCode", actionForm.getCustomerCode());
        }

        if (!Common.validateRequired(actionForm.getCustomerName())) {

            criteria.put("customerName", actionForm.getCustomerName());
        }

        if (!Common.validateRequired(actionForm.getCustomerType())) {

            criteria.put("customerType", actionForm.getCustomerType());
        }

        if (!Common.validateRequired(actionForm.getCustomerBatch())) {

            criteria.put("customerBatch", actionForm.getCustomerBatch());
        }

        if (!Common.validateRequired(actionForm.getInvoiceBatch())) {

            criteria.put("invoiceBatch", actionForm.getInvoiceBatch());
        }

        if (!Common.validateRequired(actionForm.getCustomerClass())) {

            criteria.put("customerClass", actionForm.getCustomerClass());
        }

        if (!Common.validateRequired(actionForm.getDate())) {

            criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
        }

        if (actionForm.getIncludePaid()) {

            criteria.put("includePaid", "YES");

        } else {

            criteria.put("includePaid", "NO");
        }

        if (actionForm.getIncludeUnpostedTransaction()) {

            criteria.put("includeUnpostedTransaction", "YES");

        } else {

            criteria.put("includeUnpostedTransaction", "NO");
        }

        if (actionForm.getIncludeAdvance()) {

            criteria.put("includeAdvance", "YES");

        } else {

            criteria.put("includeAdvance", "NO");
        }

        if (actionForm.getIncludeAdvanceOnly()) {

            criteria.put("includeAdvanceOnly", "YES");

        } else {

            criteria.put("includeAdvanceOnly", "NO");
        }
        return criteria;
    }
}


