package com.struts.jreports.ar.agingsummary;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.aging.ApRepAgingData;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.ArRepAgingSummaryDetails;


public class ArRepAgingSummaryDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();

	   private int index = -1;

	   public ArRepAgingSummaryDS(ArrayList list){
	      
	      Iterator i = list.iterator();
	      
	      while (i.hasNext()) {
	      	
	         ArRepAgingSummaryDetails details = (ArRepAgingSummaryDetails)i.next();
	         
	         String groupBy = null;
	         
	         if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE)) {
	         	
	         	groupBy = details.getAgCustomerCode() + "-" + details.getAgCustomerName();
	         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_NAME)) {
		         	
		         	groupBy = details.getAgCustomerName();
		         	
	         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE)) {
	         	
	         	groupBy = details.getAgCustomerType();
	         	
	         } else if(details.getGroupBy().equals(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS)) {
	         	
	         	groupBy = details.getAgCustomerClass();
	         	
	         } else {
	         	
	         	groupBy = "";
	         	
	         }
	         
		     ArRepAgingSummaryData agData = new ArRepAgingSummaryData(groupBy,		         
		         new Double(details.getAgAmount()),
				 details.getAgBucket0() != 0d ? new Double(details.getAgBucket0()) : null,
		         details.getAgBucket1() != 0d ? new Double(details.getAgBucket1()) : null, 
		         details.getAgBucket2() != 0d ? new Double(details.getAgBucket2()) : null,
		         details.getAgBucket3() != 0d ? new Double(details.getAgBucket3()) : null, 	         
		         details.getAgBucket4() != 0d ? new Double(details.getAgBucket4()) : null,
		         details.getAgBucket5() != 0d ? new Double(details.getAgBucket5()) : null,
		        		 Common.convertCharToString(details.getAgVouFcSymbol()), details.getAgDescription());
			    
	         data.add(agData);
	      }     
	      
	   }

	   public boolean next() throws JRException{
	      index++;
	      return (index < data.size());
	   }

	   public Object getFieldValue(JRField field) throws JRException{
	      Object value = null;

	      String fieldName = field.getName();

	      if("groupBy".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getGroupBy();	      
	      }else if("amount".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getAmount();
	      }else if("bucket0".equals(fieldName)) {
	      	 value = ((ArRepAgingSummaryData)data.get(index)).getBucket0();
	      }else if("bucket1".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getBucket1();
	      }else if("bucket2".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getBucket2();
	      }else if("bucket3".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getBucket3();
	      }else if("bucket4".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getBucket4();
	      }else if("bucket5".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getBucket5();	        	      
	      }else if("currencySymbol".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getCurrencySymbol();
	      }else if("description".equals(fieldName)){
	         value = ((ArRepAgingSummaryData)data.get(index)).getDescription();
	      }

	      return(value);
	   }
}
