
package com.struts.jreports.ar.agingsummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepAgingSummaryForm extends ActionForm implements Serializable {

	private String customerCode = null;
	private String customerName = null;
	private String date = null;
	private String customerType = null;
	private ArrayList customerTypeList = new ArrayList();
	private String customerBatch = null;
	private ArrayList customerBatchList = new ArrayList();
        private String invoiceBatch = null;
        private ArrayList invoiceBatchList = new ArrayList();
	private String customerClass = null;
	private ArrayList customerClassList = new ArrayList();
	private String agingBy = null;
	private ArrayList agingByList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private boolean includePaid = false;
	private boolean includeUnpostedTransaction = false;
	private boolean includeAdvance = false;
	private boolean includeAdvanceOnly = false;
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();	   
	private String report = null;

	private ArrayList arRepBrAgList = new ArrayList();

	private String userPermission = new String();

	public String getCustomerCode() {

		return customerCode;

	}

	public void setCustomerCode(String customerCode) {

		this.customerCode = customerCode;

	}

	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}   

	public String getCustomerName() {

		return customerName;

	}

	public void setCustomerName(String customerName) {

		this.customerName = customerName;

	}

	public String getCustomerType() {

		return customerType;

	}

	public void setCustomerType(String customerType) {

		this.customerType = customerType;

	}

	public ArrayList getCustomerTypeList() {

		return customerTypeList;

	}

	public void setCustomerTypeList(String customerType) {

		customerTypeList.add(customerType);

	}

	public void clearCustomerTypeList() {

		customerTypeList.clear();
		customerTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
	
	
           
           public String getInvoiceBatch() {
	   	
	      return invoiceBatch;
	      
	   }

	   public void setInvoiceBatch(String invoiceBatch) {
	   	
	      this.invoiceBatch = invoiceBatch;
	      
	   }
           
           
           
           
           
           public ArrayList getInvoiceBatchList() {
	   	
	      return invoiceBatchList;
	      
	   }

	   public void setInvoiceBatchList(String invoiceBatch) {
	   	
	      invoiceBatchList.add(invoiceBatch);
	      
	   }
	   
	   public void clearInvoiceBatchList() {
	   	
                invoiceBatchList.clear();
                invoiceBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
             

	public String getCustomerClass() {

		return customerClass;

	}

	public void setCustomerClass(String customerClass) {

		this.customerClass = customerClass;

	}

	public ArrayList getCustomerClassList() {

		return customerClassList;

	}

	public void setCustomerClassList(String customerClass) {

		customerClassList.add(customerClass);

	}

	public void clearCustomerClassList() {

		customerClassList.clear();
		customerClassList.add(Constants.GLOBAL_BLANK);

	}

	public String getAgingBy() {

		return agingBy ;   	

	}

	public void setAgingBy(String agingBy) {

		this.agingBy = agingBy;

	}

	public ArrayList getAgingByList() {

		return agingByList;

	}   

	public String getViewType() {

		return viewType ;   	

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}
	
	public String getCurrency() {

		return currency;   	

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public boolean getIncludePaid() {

		return includePaid;

	}

	public void setIncludePaid(boolean includePaid) {

		this.includePaid = includePaid;

	}

	public boolean getIncludeUnpostedTransaction() {

		return includeUnpostedTransaction;

	}

	public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {

		this.includeUnpostedTransaction = includeUnpostedTransaction;

	}
	
	public boolean getIncludeAdvance() {

		return includeAdvance;

	}

	public void setIncludeAdvance(boolean includeAdvance) {

		this.includeAdvance = includeAdvance;

	}
	
	public boolean getIncludeAdvanceOnly() {

		return includeAdvanceOnly;

	}

	public void setIncludeAdvanceOnly(boolean includeAdvanceOnly) {

		this.includeAdvanceOnly = includeAdvanceOnly;

	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}

	public String getGroupBy() {

		return groupBy;

	}

	public void setGroupBy(String groupBy) {

		this.groupBy = groupBy;

	}

	public ArrayList getGroupByList() {

		return groupByList;

	}

	public void setGroupByList(ArrayList groupByList) {

		this.groupByList = groupByList;

	}

	public Object[] getArRepBrAgList(){

		return arRepBrAgList.toArray();

	}

	public ArRepBranchAgingSummaryList getArRepBrAgListByIndex(int index){

		return ((ArRepBranchAgingSummaryList)arRepBrAgList.get(index));

	}

	public int getArRepBrAgListSize(){

		return(arRepBrAgList.size());

	}

	public void saveArRepBrAgList(Object newArRepBrAgList){

		arRepBrAgList.add(newArRepBrAgList);   	  

	}

	public void clearArRepBrAgList(){

		arRepBrAgList.clear();

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {      

		for (int i=0; i<arRepBrAgList.size(); i++) {

			ArRepBranchAgingSummaryList actionList = (ArRepBranchAgingSummaryList)arRepBrAgList.get(i);
			actionList.setBranchCheckbox(false);

		}

		customerCode = null;
		customerName = null;
		customerType = Constants.GLOBAL_BLANK;
		customerClass = Constants.GLOBAL_BLANK;
		date = Common.convertSQLDateToString(new Date());
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_CSV);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		agingByList.clear();
		agingByList.add(Constants.AR_AGING_BY_DUE_DATE);
		agingByList.add(Constants.AR_AGING_BY_INVOICE_DATE);
		agingByList.add(Constants.AR_AGING_BY_EFFECTIVITY_DAYS);
		agingByList.add(Constants.AR_AGING_BY_RECEIVED_DATE);
		agingBy = Constants.AR_AGING_BY_INVOICE_DATE;
		groupByList.clear();
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_NAME);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
		groupBy = Constants.AR_CL_ORDER_BY_CUSTOMER_CODE;
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		currencyList.add("PHP");
		currencyList.add("USD");
		currency = Constants.GLOBAL_BLANK;
		includePaid = false;
		includeUnpostedTransaction = false;
		includeAdvance = false;
		includeAdvanceOnly = false;

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if(!Common.validateDateFormat(date)) {

				errors.add("date", new ActionMessage("arAgingSummary.error.dateInvalid"));

			}

		}
		return errors;
	}
}
