package com.struts.jreports.ar.creditablewithholdingtax;

import java.io.Serializable;

public class ArRepBranchCreditableWithholdingTaxList implements Serializable {
	
	private String brBranchCode = null;
	private String branchName = null;
	private Integer branchCode = null;
	private boolean branchCheckbox = false;
	
	private ArRepCreditableWithholdingTaxForm parentBean;
	
	public ArRepBranchCreditableWithholdingTaxList(ArRepCreditableWithholdingTaxForm parentBean, String brBranchCode, 
			String branchName, Integer branchCode) {
		
		this.parentBean = parentBean;
		this.branchName = branchName;
		this.branchCode = branchCode;
		this.brBranchCode = brBranchCode;
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void setBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}