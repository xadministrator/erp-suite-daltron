package com.struts.jreports.ar.creditablewithholdingtax;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepCreditableWithholdingTaxDetails;

public class ArRepCreditableWithholdingTaxDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepCreditableWithholdingTaxDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepCreditableWithholdingTaxDetails details = (ArRepCreditableWithholdingTaxDetails)i.next();
                  
	     ArRepCreditableWithholdingTaxData argData = new ArRepCreditableWithholdingTaxData(
	     new Long(details.getCwtSequenceNumber()), details.getCwtCstTinNumber(), details.getCwtCstCustomerName(),
	     details.getCwtNatureOfPayment(), details.getCwtAtcCode(),  
	     new Double(details.getCwtTaxBase()), new Double(details.getCwtRate()), new Double(details.getCwtTaxWithheld()));
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("sequenceNumber".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getSequenceNumber();
      }else if("tinNumber".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getTinNumber();   
      }else if("customerCode".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getCustomerCode();
      }else if("natureOfIncomePayment".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getNatureOfIncomePayment();
      }else if("atcCode".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getAtcCode();      
      }else if("taxBase".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getTaxBase();  
      }else if("taxRate".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getTaxRate(); 
      }else if("taxWithheld".equals(fieldName)){
         value = ((ArRepCreditableWithholdingTaxData)data.get(index)).getTaxWithheld();               
      }
      return(value);
   }
}
