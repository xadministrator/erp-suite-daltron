package com.struts.jreports.ar.creditablewithholdingtax;


public class ArRepCreditableWithholdingTaxData implements java.io.Serializable {
	
   private Long sequenceNumber = null;
   private String tinNumber = null;
   private String customerCode = null;
   private String natureOfIncomePayment = null;
   private String atcCode = null;   
   private Double taxBase = null;
   private Double taxRate = null;
   private Double taxWithheld = null;

   public ArRepCreditableWithholdingTaxData(Long sequenceNumber, String tinNumber, String customerCode,
      String natureOfIncomePayment, String atcCode, Double taxBase, Double taxRate, Double taxWithheld) {
      	
      this.sequenceNumber = sequenceNumber;
      this.tinNumber = tinNumber;
      this.customerCode = customerCode;
      this.natureOfIncomePayment = natureOfIncomePayment ;
      this.atcCode = atcCode;      
      this.taxBase = taxBase;
      this.taxRate = taxRate;
      this.taxWithheld = taxWithheld;
      
   }

	
	public Long getSequenceNumber() {
		return (this.sequenceNumber); 
	}

	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber; 
	}

	public String getTinNumber() {
		return (this.tinNumber); 
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber; 
	}

	public String getCustomerCode() {
		return (this.customerCode); 
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode; 
	}

	public String getNatureOfIncomePayment() {
		return (this.natureOfIncomePayment); 
	}

	public void setNatureOfIncomePayment(String natureOfIncomePayment) {
		this.natureOfIncomePayment = natureOfIncomePayment; 
	}

	public String getAtcCode() {
		return (this.atcCode); 
	}

	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode; 
	}

	public Double getTaxBase() {
		return (this.taxBase); 
	}

	public void setTaxBase(Double taxBase) {
		this.taxBase = taxBase; 
	}

	public Double getTaxRate() {
		return (this.taxRate); 
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate; 
	}

	
	public Double getTaxWithheld() {
		return (this.taxWithheld); 
	}

	public void setTaxWithheld(Double taxWithheld) {
		this.taxWithheld = taxWithheld; 
	}
}
