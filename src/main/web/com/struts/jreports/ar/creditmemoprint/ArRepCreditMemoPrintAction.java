package com.struts.jreports.ar.creditmemoprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepCreditMemoPrintController;
import com.ejb.txn.ArRepCreditMemoPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoicePrintDetails;

public final class ArRepCreditMemoPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
           
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepCreditMemoPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepCreditMemoPrintForm actionForm = (ArRepCreditMemoPrintForm)form;
         
         String frParam= Common.getUserPermission(user, Constants.AR_REP_CREDIT_MEMO_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ArRepCreditMemoPrintController EJB
*******************************************************/

         ArRepCreditMemoPrintControllerHome homeCM = null;
         ArRepCreditMemoPrintController ejbCM = null;       

         try {
         	
            homeCM = (ArRepCreditMemoPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepCreditMemoPrintControllerEJB", ArRepCreditMemoPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepCreditMemoPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCM = homeCM.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepCreditMemoPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	       
/*******************************************************
   -- TO DO getArInvByInvCode() --
*******************************************************/       

		if (frParam != null) {
			
                final String[] majorNames = {"", " Thousand", " Million",
				" Billion", " Trillion", " Quadrillion", " Quintillion"};
				
				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
				" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
				
				final String[] numNames = {"", " One", " Two", " Three", " Four",
				" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
				" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};

			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list = null;   
			    ArrayList listSub = null;
			    ArRepInvoicePrintDetails details = null;
  
				try {
					
	            	ArrayList invCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
					
	            		invCodeList.add(new Integer(request.getParameter("creditMemoCode")));

					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("invoiceCode[" + i + "]") != null) {
								
								invCodeList.add(new Integer(request.getParameter("invoiceCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbCM.executeArRepInvoicePrint(invCodeList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (new java.io.File("/opt/ofs-resources/" + user.getCompany() + "/ArRepCreditMemoPrintSub.jasper").exists()) {
	            		
	            		listSub = ejbCM.executeArRepInvoicePrintSub(invCodeList, user.getCmpCode());
	            		
	            	}
	            	
			        // get company
			       
			        adCmpDetails = ejbCM.getAdCompany(user.getCmpCode());   
			        
			        Iterator i = list.iterator();
			        
			        while (i.hasNext()) {
			        	
			        	details = (ArRepInvoicePrintDetails)i.next();
			        	
			            int num = (int)Math.floor(details.getIpInvAmount());
					
						String str = Common.convertDoubleToStringMoney(details.getIpInvAmount(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());
						
						details.setIpInvAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
						str + "/100 Only");	

			        }
           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoPrint.error.creditMemoAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArRepCreditMemoPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("arRepCreditMemoPrintForm");

	             }
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				
				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();	            	

				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("companyAddress", companyAddress);
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepCreditMemoPrint.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/ArRepCreditMemoPrint.jasper");
			    
		        }
		        
		        String subreportFilename = null;
				
				if(listSub != null) {
					
					subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepCreditMemoPrintSub.jasper";
					
					if (new java.io.File(subreportFilename).exists()) {
						
						JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
						parameters.put("arRepCreditMemoPrintSub", subreport);
						parameters.put("arRepCreditMemoPrintSubDS", new ArRepCreditMemoPrintSubDS(listSub));
						
					}
					
				} 
				    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepCreditMemoPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {

				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ArRepCreditMemoPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("arRepCreditMemoPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("arRepCreditMemoPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ArRepCreditMemoPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
   
	private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
	    String soFar;
	
	    if (number % 100 < 20){
	        soFar = numNames[number % 100];
	        number /= 100;
	       }
	    else {
	        soFar = numNames[number % 10];
	        number /= 10;
	
	        soFar = tensNames[number % 10] + soFar;
	        number /= 10;
	       }
	    if (number == 0) return soFar;
	    return numNames[number] + " Hundred" + soFar;
	}
    
	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
	    /* special case */
	    if (number == 0) { return "zero"; }
	
	    String prefix = "";
	
	    if (number < 0) {
	        number = -number;
	        prefix = "Negative";
	      }
	
	    String soFar = "";
	    int place = 0;
	
	    do {
	      int n = number % 1000;
	      if (n != 0){
	         String s = this.convertLessThanOneThousand(n, tensNames, numNames);
	         soFar = s + majorNames[place] + soFar;
	        }
	      place++;
	      number /= 1000;
	      } while (number > 0);
	
	    return (prefix + soFar).trim();
	} 
}
