/*
 * opt/jreport/ArRepCreditMemoPrintDS.java
 *
 * Created on March 11, 2004, 2:22 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.ar.creditmemoprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.debitmemoprint.ApRepDebitMemoPrintData;
import com.struts.util.Common;
import com.util.ArRepInvoicePrintDetails;

public class ArRepCreditMemoPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;
   double lineNumber = 0;
   

   public ArRepCreditMemoPrintDS(ArrayList list) {
	  
   	  Iterator i = list.iterator();
   	  lineNumber = 0;
   	  
   	  while(i.hasNext()) {
   		
   	    ArRepInvoicePrintDetails details = (ArRepInvoicePrintDetails)i.next();
   	    
   	  	ArRepCreditMemoPrintData arRepCMData = new ArRepCreditMemoPrintData(details.getIpInvType(), details.getIpInvCustomerName(),
   	  	Common.convertSQLDateToString(details.getIpInvDate()), details.getIpInvCustomerAddress(), details.getIpInvNumber(), details.getIpInvCmReferenceNumber(),
   	  	details.getIpInvDescription(), new Double(details.getIpIlAmount()), details.getIpInvApprovedRejectedBy(), 
   	  	details.getIpInvCreatedBy(), details.getIpInvCurrencyDescription(), details.getIpInvAmountInWords(), 
		Common.convertCharToString(details.getIpInvCurrencySymbol()),
		details.getIpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false), details.getIpInvReferenceNumber(),
		details.getIpSlpSalespersonCode(), details.getIpSlpName(), details.getIpInvCmInvoiceNumber(), details.getIpInvCmInvoiceDate(),
		details.getIpInvCustomerCity(), details.getIpInvApprovalStatus(), details.getIpInvCreatedByDescription(),
		details.getIpInvApprovedRejectedByDescription(), new Double(details.getIpIlQuantity()), new Double(details.getIpIlUnitPrice()) , details.getIpIlDescription(), details.getIpIlName(), new Double(details.getIpIliDiscount1()), new Double(details.getIpIliTotalDiscount()), new Double(++lineNumber),
		new Double(details.getIpInvAmount()), new Double(details.getIpInvTotalTax()),new Double(details.getIpInvWithholdingTaxAmount()));
   	  	


        data.add(arRepCMData);

   	  }
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      
      
      
      if("invoiceType".equals(fieldName)){
          value = ((ArRepCreditMemoPrintData)data.get(index)).getInvoiceType();    
      }else if("customer".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getCustomer();       
      }else if("date".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getDate();
      }else if("address".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getAddress();
      }else if("invoiceNumber".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getInvoiceNumber();
      }else if("cmReferenceNumber".equals(fieldName)){
          value = ((ArRepCreditMemoPrintData)data.get(index)).getCmReferenceNumber();
      }else if("description".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getDescription();
      }else if("amount".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getAmount();
      }else if("approvedBy".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getApprovedBy();
      }else if("createdBy".equals(fieldName)){
         value = ((ArRepCreditMemoPrintData)data.get(index)).getCreatedBy();
      }else if("currencySymbol".equals(fieldName)){
        value = ((ArRepCreditMemoPrintData)data.get(index)).getCurrencySymbol();
	  }else if("amountInWords".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getAmountInWords();
	  }else if("currencyDescription".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getCurrencyDescription();
      }else if("showDuplicate".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getShowDuplicate();
      }else if("referenceNumber".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getReferenceNumber();
      }else if("salespersonCode".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getSalespersonCode();
      }else if("salespersonName".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getSalespersonName();
      }else if("creditedInvoiceNumber".equals(fieldName)){
	    value = ((ArRepCreditMemoPrintData)data.get(index)).getCreditedInvoiceNumber();
      }else if("creditedInvoiceDate".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getCreditedInvoiceDate();
      }else if("customerCity".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getCustomerCity();
      }else if("approvalStatus".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getApprovalStatus();
      }else if("createdByDescription".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getCreatedByDescription();
      }else if("approvedByDescription".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getApprovedByDescription();
      }else if("quantity".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getQuantity();
      }else if("unitPrice".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getUnitPrice();
    	  System.out.print(value);
      }else if("IlDescription".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getIlDescription();
      }else if("IlName".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getIlName();  
      }else if("lineNumber".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getNumber();
      }else if("discount1".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getDiscount1();
      }else if("totalDiscount".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getTotalDiscount();
      }else if("creditAmount".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getCreditAmount();
      }else if("totalTaxAmount".equals(fieldName)){
    	
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getTotalTaxAmount();
      }else if("totalWTaxAmount".equals(fieldName)){
    	  value = ((ArRepCreditMemoPrintData)data.get(index)).getTotalWTaxAmount();
      }

      return(value);
   }
}
