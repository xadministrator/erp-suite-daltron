package com.struts.jreports.ar.creditmemoprint;


public class ArRepCreditMemoPrintData implements java.io.Serializable {
   
   private String invoiceType =null;
   private String customer = null;
   private String date = null;
   private String address = null;
   private String invoiceNumber = null;
   private String cmReferenceNumber = null;
   private String description = null;
   private Double amount = null;
   private String approvedBy = null;
   private String createdBy = null;
   private String currencyDescription = null;
   private String amountInWords = null;
   private String currencySymbol = null;
   
   private Boolean showDuplicate = null;
   private String referenceNumber = null;
   private String salespersonCode = null;
   private String salespersonName = null;
   private String creditedInvoiceNumber = null;
   private String creditedInvoiceDate = null;
   private String customerCity = null;

   private String approvalStatus = null;

   private String createdByDescription = null;
   private String approvedByDescription = null;
	
   private Double quantity = null;
   private Double unitPrice = null;
   private String IlDescription = null;
   private String IlName = null;
   private Double number = null;
   private Double discount1 = null;
   private Double totalDiscount = null;
   private Double creditAmount = null;
   private Double totalTaxAmount = null;
   private Double totalWTaxAmount = null;
   
   public ArRepCreditMemoPrintData(String invoiceType, String customer, String date, String address, String invoiceNumber, String cmReferenceNumber,
        String description, Double amount, String approvedBy, String createdBy, String currencyDescription,
        String amountInWords, String currencySymbol, Boolean showDuplicate, String referenceNumber,
		String salespersonCode, String salespersonName, String creditedInvoiceNumber, String creditedInvoiceDate, 
   		String customerCity, String approvalStatus, String createdByDescription, String approvedByDescription, Double quantity,
   		Double unitPrice, String IlDescription, String IlName, Double number, Double discount1, Double totalDiscount, Double creditAmount, Double totalTaxAmount, Double totalWTaxAmount) {
      	
	    this.invoiceType = invoiceType;
      	this.customer = customer;
      	this.date = date;
      	this.address = address;
      	this.invoiceNumber = invoiceNumber;
      	this.cmReferenceNumber = cmReferenceNumber;
      	this.description = description;
      	this.amount = amount;
      	this.approvedBy = approvedBy;
      	this.createdBy = createdBy;
        this.currencyDescription = currencyDescription;
        this.amountInWords = amountInWords;
        this.currencySymbol = currencySymbol;
        this.showDuplicate = showDuplicate;
        this.referenceNumber = referenceNumber;
        this.salespersonCode = salespersonCode;
        this.salespersonName = salespersonName;
        this.creditedInvoiceNumber = creditedInvoiceNumber;
        this.creditedInvoiceDate = creditedInvoiceDate;
        this.customerCity = customerCity;
        this.approvalStatus = approvalStatus;
        this.createdByDescription = createdByDescription;
        this.approvedByDescription = approvedByDescription;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.IlDescription = IlDescription;
        this.IlName  = IlName;
        this.number = number;
        this.discount1 = discount1;
        this.totalDiscount = totalDiscount;
        this.creditAmount = creditAmount;
        this.totalTaxAmount = totalTaxAmount;
        this.totalWTaxAmount = totalWTaxAmount;
   }

   public String getInvoiceType() {
		return (this.invoiceType); 
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType; 
	}
   
	public String getCustomer() {
		return (this.customer); 
	}

	public void setCustomer(String customer) {
		this.customer = customer; 
	}

	public String getDate() {
		return (this.date); 
	}

	public void setDate(String date) {
		this.date = date; 
	}

	public String getAddress() {
		return (this.address); 
	}

	public void setAddress(String address) {
		this.address = address; 
	}

	public String getInvoiceNumber() {
		return (this.invoiceNumber); 
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber; 
	}
	
	public String getCmReferenceNumber() {
		return (this.cmReferenceNumber); 
	}

	public void setCmReferenceNumber(String cmReferenceNumber) {
		this.cmReferenceNumber = cmReferenceNumber; 
	}

	public String getDescription() {
		return (this.description); 
	}

	public void setDescription(String description) {
		this.description = description; 
	}

	public Double getAmount() {
		return (this.amount); 
	}

	public void setAmount(Double amount) {
		this.amount = amount; 
	}

	public String getApprovedBy() {
		return (this.approvedBy); 
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy; 
	}

	public String getCreatedBy() {
		return (this.createdBy); 
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy; 
	}

	public String getCurrencyDescription() {
		return (this.currencyDescription); 
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription; 
	}

	public String getAmountInWords() {
		return (this.amountInWords); 
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords= amountInWords; 
	}

	public String getCurrencySymbol() {
		return (this.currencySymbol); 
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol; 
	}
	
	public Double getCreditAmount() {
		return (this.creditAmount); 
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount; 
	}
	
	
	public Boolean getShowDuplicate() {
   	
   	   return showDuplicate;
   
    }
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getSalespersonCode() {
		
		return salespersonCode;
		
	}
	
	public String getSalespersonName() {
		
		return salespersonName;
		
	}
	
	public String getCreditedInvoiceNumber() {
		
		return creditedInvoiceNumber;

	}

	public String getCreditedInvoiceDate() {

		return creditedInvoiceDate;

	}

	public String getCustomerCity() {

		return customerCity;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public String getCreatedByDescription() {

		return createdByDescription;

	}

	public String getApprovedByDescription() {

		return approvedByDescription;

	}
	public Double getQuantity() {

		return quantity;

	}
	public Double getUnitPrice() {

		return unitPrice;

	}
	public String getIlDescription() {

		return IlDescription;

	}
	public String getIlName() {

		return IlName;

	}
	public Double getNumber() {

		return number;

	}
	public Double getDiscount1() {

		return discount1;

	}
	public Double getTotalDiscount() {

		return totalDiscount;

	}
	
	public Double getTotalTaxAmount() {

		return totalTaxAmount;

	}
	
	
	public Double getTotalWTaxAmount() {

		return totalWTaxAmount;

	}
	
	
}
