package com.struts.jreports.ar.creditmemoprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepInvoicePrintDetails;

public class ArRepCreditMemoPrintSubDS implements JRDataSource {

	   private ArrayList data = new ArrayList();

	   private int index = -1;

	   public ArRepCreditMemoPrintSubDS(ArrayList list) {
	   	
	 	  Iterator i = list.iterator();
	   	  
	   	  while(i.hasNext()) {
	   	  	
	   	  	ArRepInvoicePrintDetails details = (ArRepInvoicePrintDetails)i.next(); 
	   	    
	   	  	ArRepCreditMemoPrintSubData arRepInvPrintDrData = new ArRepCreditMemoPrintSubData(details.getIpDrCoaAccountNumber(),
	   	  			details.getIpDrCoaAccountDescription() , new Boolean(Common.convertByteToBoolean(details.getIpDrDebit())), 
	   	  			new Double(details.getIpDrAmount()));
	   	    
	   	    data.add(arRepInvPrintDrData);

	     }
	   	  
	   }
	   	  
	   public boolean next() throws JRException {
	   	
	      index++;

	      if (index == data.size()) {

	          index = -1;
	          return false;

	      } else return true;

	      
	   }

	   public Object getFieldValue(JRField field) throws JRException {
	   	
	      Object value = null;

	      String fieldName = field.getName();

	      if("accountNumber".equals(fieldName)){
	         value = ((ArRepCreditMemoPrintSubData)data.get(index)).getAccountNumber(); 
	      }else if("accountDescription".equals(fieldName)){
	      	 value = ((ArRepCreditMemoPrintSubData)data.get(index)).getAccountDescription();
	      }else if("isDebit".equals(fieldName)){
	      	 value = ((ArRepCreditMemoPrintSubData)data.get(index)).getIsDebit();
	      }else if("amount".equals(fieldName)){
	      	 value = ((ArRepCreditMemoPrintSubData)data.get(index)).getAmount();
	      }

	      return(value);
	   }
}
