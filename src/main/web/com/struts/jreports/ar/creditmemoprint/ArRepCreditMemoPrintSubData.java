package com.struts.jreports.ar.creditmemoprint;

public class ArRepCreditMemoPrintSubData implements java.io.Serializable {

	private String accountNumber = null;
	private String accountDescription = null;
	private Boolean isDebit = null;
	private Double amount = null;
	
	public ArRepCreditMemoPrintSubData(String accountNumber, String accountDescription, Boolean isDebit, Double amount) {
		
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.isDebit = isDebit;
		this.amount = amount;
		
	}
	
	public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public Boolean getIsDebit() {
		
		return isDebit;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	public String getAccountDescription() {
		
		return accountDescription;
		
	}
	
}