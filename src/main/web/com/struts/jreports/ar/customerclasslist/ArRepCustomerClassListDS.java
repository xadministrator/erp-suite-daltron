package com.struts.jreports.ar.customerclasslist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepCustomerClassListDetails;

public class ArRepCustomerClassListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepCustomerClassListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 ArRepCustomerClassListDetails details = (ArRepCustomerClassListDetails)i.next();
                  
	     ArRepCustomerClassListData argData = new ArRepCustomerClassListData(details.getCclCcName(), 
	     		details.getCclCcDescription(), details.getCclTaxName(), details.getCclWithholdingTaxName(),
				details.getCclReceivableAccountNumber(), details.getCclReceivableAccountDescription(), 
				details.getCclRevenueAccountNumber(), details.getCclRevenueAccountDescription(),
				details.getCclEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("customerClassName".equals(fieldName)){
         value = ((ArRepCustomerClassListData)data.get(index)).getCustomerClassName();
      }else if("description".equals(fieldName)){
         value = ((ArRepCustomerClassListData)data.get(index)).getDescription();
      }else if("taxName".equals(fieldName)){
         value = ((ArRepCustomerClassListData)data.get(index)).getTaxName();
      }else if("withholdingTaxName".equals(fieldName)){
        value = ((ArRepCustomerClassListData)data.get(index)).getWithholdingTaxName();   
      }else if("receivableAccountNumber".equals(fieldName)){
         value = ((ArRepCustomerClassListData)data.get(index)).getReceivableAccountNumber();         
      }else if("receivableAccountDescription".equals(fieldName)){
         value = ((ArRepCustomerClassListData)data.get(index)).getReceivableAccountDescription();
      }else if("revenueAccountNumber".equals(fieldName)){
        value = ((ArRepCustomerClassListData)data.get(index)).getRevenueAccountNumber();         
      }else if("revenueAccountDescription".equals(fieldName)){
        value = ((ArRepCustomerClassListData)data.get(index)).getRevenueAccountDescription();         
      }else if("enable".equals(fieldName)){
        value = ((ArRepCustomerClassListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
