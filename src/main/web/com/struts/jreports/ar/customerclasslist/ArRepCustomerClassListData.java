package com.struts.jreports.ar.customerclasslist;


public class ArRepCustomerClassListData implements java.io.Serializable {
	
   private String customerClassName = null;
   private String description = null;
   private String taxName = null;
   private String withholdingTaxName = null;
   private String receivableAccountNumber = null;
   private String receivableAccountDescription = null;
   private String revenueAccountNumber = null;
   private String revenueAccountDescription = null;
   private String enable = null;

   public ArRepCustomerClassListData(String customerClassName, String description, String taxName,
   		String withholdingTaxName, String receivableAccountNumber, String receivableAccountDescription, 
   		String revenueAccountNumber, String revenueAccountDescription, String enable){
      	
      this.customerClassName = customerClassName;
      this.description = description;
      this.taxName = taxName;
      this.withholdingTaxName = withholdingTaxName;
      this.receivableAccountNumber = receivableAccountNumber;
      this.receivableAccountDescription = receivableAccountDescription;
      this.revenueAccountNumber = revenueAccountNumber;
      this.revenueAccountDescription = revenueAccountDescription;
      this.enable = enable;
      
   }

   public String getDescription() {
   	
   	return description;
   
   }
   
   public void setDescription(String description) {
   
   	this.description = description;
   
   }
   
   public String getEnable() {
   
   	return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	this.enable = enable;
   
   }
   
   public String getRevenueAccountDescription() {
   
   	return revenueAccountDescription;
   
   }
   
   public void setRevenueAccountDescription(String revenueAccountDescription) {
  
   	this.revenueAccountDescription = revenueAccountDescription;
   
   }
   
   public String getRevenueAccountNumber() {
  
   	return revenueAccountNumber;
  
   }
   
   public void setRevenueAccountNumber(String revenueAccountNumber) {
   
   	this.revenueAccountNumber = revenueAccountNumber;
   
   }
   
   public String getReceivableAccountDescription() {
   
   	return receivableAccountDescription;
   
   }
   
   public void setReceivableAccountDescription(String receivableAccountDescription) {
   
   	this.receivableAccountDescription = receivableAccountDescription;
   
   }
   
   public String getReceivableAccountNumber() {
   
   	return receivableAccountNumber;
   
   }
   
   public void setReceivableAccountNumber(String receivableAccountNumber) {
   
   	this.receivableAccountNumber = receivableAccountNumber;
   
   }
  
   public String getCustomerClassName() {
   
   	return customerClassName;
   
   }
   
   public void setCustomerClassName(String customerClassName) {
   
   	this.customerClassName = customerClassName;
   
   }
   
   public String getTaxName() {
   
   	return taxName;
   
   }
   
   public void setTaxName(String taxName) {
   
    this.taxName = taxName;
   
   }
   
   public String getWithholdingTaxName() {
   
   	return withholdingTaxName;
   
   }
   
   public void setWithholdingTaxName(String withholdingTaxName) {
   
   	this.withholdingTaxName = withholdingTaxName;
   
   }
}
