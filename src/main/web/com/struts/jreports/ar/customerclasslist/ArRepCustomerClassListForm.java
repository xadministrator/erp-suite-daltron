package com.struts.jreports.ar.customerclasslist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class ArRepCustomerClassListForm extends ActionForm implements Serializable{

   private String customerClassName = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getCustomerClassName() {
   
   	   return customerClassName;
   
   }

   public void setCustomerClassName(String customerClassName) {
   	
      this.customerClassName = customerClassName;
   
   }

   public void setGoButton(String goButton) {
   	
      this.goButton = goButton;
   
   }

   public void setCloseButton(String closeButton) {
   	
      this.closeButton = closeButton;
   
   }

   public String getOrderBy() {
   	
   	  return orderBy;   	
   
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   
   }
   
   public String getViewType() {
   	
   	  return viewType;   	
   
   }
   
   public void setViewType(String viewType) {
   	
   	  this.viewType = viewType;
   
   }
   
   public ArrayList getViewTypeList() {
   	
   	  return viewTypeList;
   
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission() {
   	
      return userPermission;
   
   }

   public void setUserPermission(String userPermission) {
   	
      this.userPermission = userPermission;
   
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {   
   	
      goButton = null;
      closeButton = null;
      customerClassName = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      
      if (orderByList.isEmpty()) {
      	
		  orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("CUSTOMER CLASS NAME");
	      orderBy = "CUSTOMER CLASS NAME";   
	  
	  }    
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
      }
      return(errors);
   }
}
