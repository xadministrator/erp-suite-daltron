package com.struts.jreports.ar.customerlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepCustomerListController;
import com.ejb.txn.ArRepCustomerListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepCustomerListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepCustomerListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepCustomerListForm actionForm = (ArRepCustomerListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
 
         String frParam = Common.getUserPermission(user, Constants.AR_REP_CUSTOMER_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepCustomerList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepCustomerListController EJB
*******************************************************/

         ArRepCustomerListControllerHome homeCL = null;
         ArRepCustomerListController ejbCL = null;       

         try {
         	
            homeCL = (ArRepCustomerListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepCustomerListControllerEJB", ArRepCustomerListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepCustomerListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCL = homeCL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepCustomerListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR CL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerName", actionForm.getCustomerName());
	        		
	        	}
	        	

	        	if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        	}

	        	if (!Common.validateRequired(actionForm.getType())) {
	        		
	        		criteria.put("customerType", actionForm.getType());
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	        		        	
	 
	        	if (!Common.validateRequired(actionForm.getCustomerRegion())) {
	        		
	        		criteria.put("customerRegion", actionForm.getCustomerRegion());

	        	}	  
	        	
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbCL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrCstListSize(); i++) {
	           	
	           		ArRepBranchCustomerList brCstList = (ArRepBranchCustomerList)actionForm.getArRepBrCstListByIndex(i);
	           	
	           		if(brCstList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brCstList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbCL.executeArRepCustomerList(actionForm.getCriteria(), branchList, actionForm.getDate(), actionForm.getSalesPerson(),
            	    actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getSplitBySalesperson(), actionForm.getIncludedZeroes(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("customerList.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepCustomerListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepCustomerList");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDate());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
        	HashMap criteria = new HashMap();  
        			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getCustomerCode() != null) {
				
				parameters.put("customerName", actionForm.getCustomerName());
				
			}
			
			if (actionForm.getType() != null) {
			
				parameters.put("customerType", actionForm.getType());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }
			
			if (actionForm.getCustomerRegion() != null) {
				
				parameters.put("customerRegion", actionForm.getCustomerRegion());	

		    }			
			
        	if (actionForm.getSplitBySalesperson()) {	        	
	        		
        		criteria.put("splitBySalesperson", "YES");
            
            }
            
            if (!actionForm.getSplitBySalesperson()) {
            	
            	criteria.put("splitBySalesperson", "NO");
            	
            }
            
        	if (actionForm.getIncludedZeroes()) {	        	
        		
        		criteria.put("includedZeroes", "YES");
            
            }
            
            if (!actionForm.getIncludedZeroes()) {
            	
            	criteria.put("includedZeroes", "NO");
            	
            }
            
		    String source = "";
		    
			String branchMap = null;
			boolean first = true;
			
		    if(actionForm.getIncludedZeroes() == true) {
		    	
		    	if(first == false)
		    		source += ", ";
		    	
		    	source += "Include Zeroes";
		    	
		    }
			
			for(int j=0; j < actionForm.getArRepBrCstListSize(); j++) {

				ArRepBranchCustomerList brList = (ArRepBranchCustomerList)actionForm.getArRepBrCstListByIndex(j);

				if(brList.getBranchCheckbox() == true) {
					if(first) {
						branchMap = brList.getBranchName();
						first = false;
					} else {
						branchMap = branchMap + ", " + brList.getBranchName();
					}
				}

			}
			parameters.put("branchMap", branchMap);
			
			String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepCustomerList.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/ArRepCustomerList.jasper");
		    
	        }
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepCustomerListDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepCustomerListDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepCustomerListDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepCustomerListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AR CL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AR CL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
		            actionForm.reset(mapping, request);
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearTypeList();           	
	            	
	            	list = ejbCL.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbCL.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
	            	
	            	actionForm.clearArRepBrCstList();
	         		
	         		list = ejbCL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchCustomerList arRepBrCstList = new ArRepBranchCustomerList(actionForm, 
	         					details.getBrBranchCode() , details.getBrName(), details.getBrCode());
	         			arRepBrCstList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrCstList(arRepBrCstList);
	         			
	         		}
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepCustomerListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepCustomerList"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepCustomerListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
