package com.struts.jreports.ar.customerlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepCustomerListDetails;

public class ArRepCustomerListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepCustomerListDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepCustomerListDetails details = (ArRepCustomerListDetails)i.next();
                  
         
         String group = null;     
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getClCstCustomerCode();
         	
         }
         
         if (groupBy.equals("SALESPERSON")) {
          	
          	group = details.getClCstCustomerCode();
          	
          }
	     ArRepCustomerListData argData = new ArRepCustomerListData(details.getClCstCustomerCode(), 
	     details.getClCstName(), details.getClCstContact(), details.getClCstPhone(), 
	     details.getClCstTin(), new Double(details.getClCstBalance()), new Double(details.getClCstCreditLimit()),
	     details.getClCstAddress(), details.getClCstFax(), details.getClCstPaymentTerm(), details.getClCstType(),
	     details.getCustomerRegion(), details.getClCstDealPrice(), details.getSalesPerson(), details.getSalesPersonCrt(), group, details.getDate(),
	     details.getInvNumber(), details.getInvReferenceNumber(), details.getInvCmReferenceNumber(), details.getRctDate(), details.getIncludedNegativeBalances(), 
	     details.getClRcptBalance(), details.getClCmBalance(), details.getCsAiApplyAmount(), details.getCsAiCreditableWTax(),
	     details.getCsAiDiscountAmount(), details.getCsAiAppliedDeposit());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("customerCode".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getCustomerCode();
      }else if("customerName".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getCustomerName();
      }else if("contact".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getContact();
      }else if("phone".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getPhone();
      }else if("tinNumber".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getTinNumber();         
      }else if("balance".equals(fieldName)){
         value = ((ArRepCustomerListData)data.get(index)).getBalance();
      }else if("creditLimit".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getCreditLimit();
      }else if("address".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getAddress();
      }else if("fax".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getFax();
      }else if("paymentTerm".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getPaymentTerm();
      }else if("type".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getType();
      }else if("customerRegion".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getCustomerRegion();
      }else if("dealPrice".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getDealPrice();
      }else if("salesPerson".equals(fieldName)){
        value = ((ArRepCustomerListData)data.get(index)).getSalesPerson();
      }else if("groupBy".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getGroupBy();
      }else if("date".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getDate();
      }else if("invoiceNumer".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getInvoiceNumber();
      }else if("referenceNumer".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getReferenceNumber();
      }else if("cmInvoiceNumer".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getCmReferenceNumber();
      }else if("includeNegativeBalances".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getIncludeNegativeBalances();
      }else if("receiptBalance".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getreceiptBalance();
      }else if("cmBalance".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getcmBalance();
      }else if("applyAmount".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getapplyAmount();
      }else if("creditableWTaxAmount".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getcreditableWTaxAmount();
      }else if("discountAmount".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getdiscountAmount();
      }else if("appliedDepositAmount".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getappliedDepositAmount();
      }else if("receiptDate".equals(fieldName)){
    	  value = ((ArRepCustomerListData)data.get(index)).getReceiptDate();
      }else if("salesPersonCRT".equals(fieldName)){
          value = ((ArRepCustomerListData)data.get(index)).getSalesPersonCrt();
      }
      return(value);
   }
}
