package com.struts.jreports.ar.customerlist;


public class ArRepCustomerListData implements java.io.Serializable {
	
   private String customerCode = null;
   private String customerName = null;
   private String contact = null;
   private String phone = null;
   private String tinNumber = null;
   private Double balance = null;
   private Double creditLimit = null;
   private String address = null;
   private String fax = null;
   private String paymentTerm = null;
   private String type = null;
   private String customerRegion = null;
   private String dealPrice = null;
   private String salesPerson= null;
   private String salesPersonCrt= null;
   private String groupBy = null;
   private String date = null;
   private boolean includeNegativeBalances = false;
   private Double receiptBalance = null;
   private String invoiceNumber = null;
   private String referencenumber = null;
   private String cmReferenceNumber = null;
   private Double cmBalance = null;
   private Double applyAmount = null;
   private Double creditableWTaxAmount = null;
   private Double discountAmount = null;
   private Double appliedDepositAmount = null;
   private String receiptDate = null;
   
   public ArRepCustomerListData(String customerCode, String customerName,
      String contact, String phone, String tinNumber, Double balance, Double creditLimit,
      String address, String fax, String paymentTerm, String type, String customerRegion, String dealPrice, String salesPerson, String salesPersonCrt, 
      String groupBy, String date, String invoiceNumber, String referenceNumber, String cmReferenceNumber, String receiptDate, boolean includeNegativeBalances, Double receiptBalance, Double cmBalance,
      Double applyAmount, Double creditableWTaxAmount, Double discountAmount, Double appliedDepositAmount){
      	
      this.customerCode = customerCode;
      this.customerName = customerName;
      this.contact = contact ;
      this.phone = phone;
      this.tinNumber = tinNumber;
      this.balance = balance ;
      this.creditLimit = creditLimit;
      this.address = address;
      this.fax = fax;
      this.paymentTerm = paymentTerm;
      this.type = type;
      this.customerRegion = customerRegion;
      this.dealPrice = dealPrice;
      this.salesPerson = salesPerson;
      this.salesPersonCrt = salesPersonCrt;
      this.groupBy = groupBy;
      this.date = date;
      this.invoiceNumber = invoiceNumber;
      this.referencenumber = referenceNumber;
      this.cmReferenceNumber = cmReferenceNumber;
      this.receiptDate = receiptDate;
      this.includeNegativeBalances = includeNegativeBalances;
      this.receiptBalance = receiptBalance;
      this.cmBalance = cmBalance;
      this.applyAmount = applyAmount;
      this.creditableWTaxAmount = creditableWTaxAmount;
      this.discountAmount = discountAmount;
      this.appliedDepositAmount = appliedDepositAmount;

  
   }

   public String getCustomerCode(){
      return(customerCode);
   }

   public String getCustomerName(){
      return(customerName);
   }
   
   public String getContact(){
      return(contact);
   }

   public String getPhone(){
      return(phone);
   }
   
   public String getTinNumber(){
   	  return(tinNumber);
   }

   public Double getBalance(){
      return(balance);
   }

   public Double getcmBalance(){
      return(cmBalance);
   }
   
   public Double getCreditLimit(){
    return(creditLimit);
    
 }
   public String getAddress(){
	   return(address);
   }
   
   public String getFax(){
	   return(fax);
   }
   
   public String getPaymentTerm(){
	   return(paymentTerm);
   }
   
   public String getType() {
	   return(type);
   }
   
   public String getGroupBy() {
		
		return (groupBy);
		
	}
   public String getCustomerRegion() {
	   return(customerRegion);
   }
   
   public String getDealPrice() {
	   return(dealPrice);
   }

   public String getSalesPerson(){
	   return(salesPerson);
   }
   
   public String getSalesPersonCrt(){
	   return(salesPersonCrt);
   }
   
   public String getInvoiceNumber(){
	   return(invoiceNumber);
   }
   
   public String getReferenceNumber(){
	   return(referencenumber);
   }
    
   public String getCmReferenceNumber(){
	   return(cmReferenceNumber);
   }
   
   public String getReceiptDate(){
	   return(receiptDate);
   }
   
   public String getDate(){
	   return(date);
   }
   
   public boolean getIncludeNegativeBalances(){
	   return(includeNegativeBalances);
   }
   
   public Double getreceiptBalance(){
	    return(receiptBalance);
	    
	 }
   
   public Double getapplyAmount(){
	    return(applyAmount);
	    
	 }
   
   public Double getcreditableWTaxAmount(){
	    return(creditableWTaxAmount);
	    
	 }
   
   public Double getdiscountAmount(){
	    return(discountAmount);
	    
	 }
   
   public Double getappliedDepositAmount(){
	    return(appliedDepositAmount);
	    
	 }
}
