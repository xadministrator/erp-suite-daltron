package com.struts.jreports.ar.customerlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepCustomerListForm extends ActionForm implements Serializable{

	private String customerCode = null;
	private String salesPerson = null;
	private String date = null;
	private String region = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String customerClass = null;
	private String customerRegion = null;
	private ArrayList customerClassList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private ArrayList groupByList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private String customerName = null;
	private String groupBy = null;
	private boolean splitBySalesperson = false;
	private boolean includedZeroes = false;

	private HashMap criteria = new HashMap();
	private ArrayList arRepBrCstList = new ArrayList();

	private String userPermission = new String();

	public HashMap getCriteria() {

		return criteria;

	}
	
	

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public String getCustomerName() {

		return customerName;

	}

	public void setCustomerName(String customerName) {

		this.customerName = customerName;

	}
	
	public String getCustomerCode(){
		return(customerCode);
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}
	
	public String getSalesPerson(){
		return(salesPerson);
	}

	public void setSalesPerson(String salesPerson){
		this.salesPerson = salesPerson;
	}

	public String getDate(){
		return(date);
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getRegion(){
		return(region);
	}

	public void setRegion(String region){
		this.region = region;
	}
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}

	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}

	public String getType(){
		return(type);
	}

	public void setType(String type){
		this.type = type;
	}

	public ArrayList getTypeList(){
		return(typeList);
	}

	public void setTypeList(String type){
		typeList.add(type);
	}

	public void clearTypeList(){
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
	}

	public String getCustomerClass(){
		return(customerClass);
	}

	public void setCustomerClass(String customerClass){
		this.customerClass = customerClass;
	}

	public String getCustomerRegion(){
		return(region);
	}

	public void setCustomerRegion(String region){
		this.region = region;
	}

	public ArrayList getCustomerClassList(){
		return(customerClassList);
	}

	public void setCustomerClassList(String customerClass){
		customerClassList.add(customerClass);
	}

	public void clearCustomerClassList(){
		customerClassList.clear();
		customerClassList.add(Constants.GLOBAL_BLANK);
	} 

	public String getOrderBy(){
		return(orderBy);   	
	}

	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}

	public ArrayList getOrderByList(){
		return orderByList;
	}
	
	public String getGroupBy(){
		return(groupBy);   	
	}

	public void setGroupBy(String groupBy){
		this.groupBy = groupBy;
	}

	public ArrayList getGroupByList(){
		return groupByList;
	}

	public String getViewType(){
		return(viewType);   	
	}

	public void setViewType(String viewType){
		this.viewType = viewType;
	}

	public ArrayList getViewTypeList(){
		return viewTypeList;
	}

	public String getReport() {

		return report;

	}
	
	public boolean getSplitBySalesperson() {

		return splitBySalesperson;

	}

	public void setSplitBySalesperson(boolean splitBySalesperson) {

		this.splitBySalesperson = splitBySalesperson;

	}
	
	public boolean getIncludedZeroes() {

		return includedZeroes;

	}

	public void setIncludedZeroes(boolean includedZeroes) {

		this.includedZeroes = includedZeroes;

	}

	public void setReport(String report) {

		this.report = report;

	}
	
	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}

	public Object[] getArRepBrCstList(){

		return arRepBrCstList.toArray();

	}

	public ArRepBranchCustomerList getArRepBrCstListByIndex(int index){

		return ((ArRepBranchCustomerList)arRepBrCstList.get(index));

	}

	public int getArRepBrCstListSize(){

		return(arRepBrCstList.size());

	}

	public void saveArRepBrCstList(Object newArRepBrCstList){

		arRepBrCstList.add(newArRepBrCstList);   	  

	}

	public void clearArRepBrCstList(){

		arRepBrCstList.clear();

	}

	public void reset(ActionMapping mapping, HttpServletRequest request){      

		for (int i=0; i<arRepBrCstList.size(); i++) {

			ArRepBranchCustomerList actionList = (ArRepBranchCustomerList)arRepBrCstList.get(i);
			actionList.setBranchCheckbox(false);

		}

		goButton = null;
		closeButton = null;
		customerCode = null;
		salesPerson = null;
		customerRegion = null;
		customerClass = Constants.GLOBAL_BLANK;
		splitBySalesperson = false;
		includedZeroes = false;
		type = Constants.GLOBAL_BLANK;
		date = Common.convertSQLDateToString(new java.util.Date());
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;

		if (orderByList.isEmpty()) {

			orderByList.clear();
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
			orderByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_NAME);
			orderByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
			orderByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
			orderByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_REGION);
			orderByList.add(Constants.AR_CL_ORDER_BY_SALESPERSON);
			orderBy = Constants.AR_CL_ORDER_BY_SALESPERSON;   

		}  
		
		if (groupByList.isEmpty()) {

			groupByList.clear();
			groupByList.add(Constants.GLOBAL_BLANK);
			groupByList.add(Constants.AR_CL_GROUP_BY_CUSTOMER_CODE);
			groupByList.add(Constants.AR_CL_GROUP_BY_SALESPERSON_CODE);
			groupBy = Constants.AR_CL_GROUP_BY_CUSTOMER_CODE;   

		}

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("customerList.error.dateInvalid"));
			}	 
			if(Common.validateRequired(date)){
				errors.add("date", new ActionMessage("customerList.error.dateRequired"));
			}			 		 		 
			if(!Common.validateStringExists(typeList, type)){
				errors.add("type", new ActionMessage("customerList.error.typeInvalid"));
			}
			if(!Common.validateStringExists(customerClassList, customerClass)){
				errors.add("customerClass", new ActionMessage("customerList.error.customerClassInvalid"));
			}	 
		}
		return(errors);
	}
}
