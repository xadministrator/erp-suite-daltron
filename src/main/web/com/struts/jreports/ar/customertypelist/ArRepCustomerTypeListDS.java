package com.struts.jreports.ar.customertypelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepCustomerTypeListDetails;

public class ArRepCustomerTypeListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepCustomerTypeListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 ArRepCustomerTypeListDetails details = (ArRepCustomerTypeListDetails)i.next();
                  
	     ArRepCustomerTypeListData argData = new ArRepCustomerTypeListData(details.getCtlCtName(),
	     		details.getCtlCtDescription(), details.getCtlBankAccount(), details.getCtlEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("customerTypeName".equals(fieldName)){
         value = ((ArRepCustomerTypeListData)data.get(index)).getCustomerTypeName();
      }else if("description".equals(fieldName)){
         value = ((ArRepCustomerTypeListData)data.get(index)).getDescription();
      }else if("bankAccount".equals(fieldName)){
         value = ((ArRepCustomerTypeListData)data.get(index)).getBankAccount();      
      }else if("enable".equals(fieldName)){
        value = ((ArRepCustomerTypeListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
