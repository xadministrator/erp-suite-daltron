package com.struts.jreports.ar.customertypelist;


public class ArRepCustomerTypeListData implements java.io.Serializable {
	
   private String customerTypeName = null;
   private String description = null;
   private String bankAccount = null;
   private String enable = null;

   public ArRepCustomerTypeListData(String customerTypeName, String description, String bankAccount, String enable){
      	
      this.customerTypeName = customerTypeName;
      this.description = description;
      this.bankAccount = bankAccount;
      this.enable = enable;
      
   }

   public String getDescription() {
   	
   	return description;
   
   }
   
   public void setDescription(String description) {
   
   	this.description = description;
   
   }
   
   public String getEnable() {
   
   	return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	this.enable = enable;
   
   }

   public String getCustomerTypeName() {
   
   	return customerTypeName;
   
   }
   
   public void setCustomerTypeName(String customerTypeName) {
   
   	this.customerTypeName = customerTypeName;
   
   }
   
   public String getBankAccount() {
   
   	return bankAccount;
   
   }
   
   public void setBankAccount(String bankAccount) {
   
    this.bankAccount = bankAccount;
   
   }
   
}
