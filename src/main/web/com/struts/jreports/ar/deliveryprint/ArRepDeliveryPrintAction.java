package com.struts.jreports.ar.deliveryprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepDeliveryReceiptController;
import com.ejb.txn.ArRepDeliveryPrintController;
import com.ejb.txn.ArRepDeliveryPrintControllerBean;
import com.ejb.txn.ArRepDeliveryPrintControllerBean;
import com.ejb.txn.ArRepDeliveryPrintControllerHome;
import com.struts.jreports.ar.deliveryprint.ArRepDeliveryPrintDS;
import com.struts.jreports.ar.deliveryprint.ArRepDeliveryPrintForm;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.ReportParameter;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArRepDeliveryPrintDetails;
import com.util.ArRepDeliveryPrintDetails;

public final class ArRepDeliveryPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

    	  /*******************************************************
    	     Check if user has a session
    	  *******************************************************/

    	           User user = (User) session.getAttribute(Constants.USER_KEY);
    	           /*
    	           if (user != null) {

    	              if (log.isInfoEnabled()) {

    	                  log.info("ArRepDeliveryPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
    	                  "' performed this action on session " + session.getId());

    	              }

    	           } else {

    	               if (log.isInfoEnabled()) {

    	                  log.info("User is not logged on in session" + session.getId());

    	              }

    	              return(mapping.findForward("adLogon"));

    	           }
    	            */
    	           
    	           System.out.println("pasok1");
    	           ArRepDeliveryPrintForm actionForm = (ArRepDeliveryPrintForm)form;

    	           String frParam= Common.getUserPermission(user, Constants.AR_SALES_ORDER_ENTRY_ID);

    	           if (frParam != null) {

    	               actionForm.setUserPermission(frParam.trim());

    	           } else {

    	        	  
    	               actionForm.setUserPermission(Constants.NO_ACCESS);
    	           }
    	           
    	      
    	           
    	           System.out.println("pasok2");
    	  /*******************************************************
    	     Initialize ArRepDeliveryPrintController EJB
    	  *******************************************************/

    	           ArRepDeliveryPrintControllerHome homeDP = null;
    	           ArRepDeliveryPrintController ejbDP = null;
    	           
    	  

    	           try {

    	              homeDP = (ArRepDeliveryPrintControllerHome)com.util.EJBHomeFactory.
    	                lookUpHome("ejb/ArRepDeliveryPrintControllerEJB", ArRepDeliveryPrintControllerHome.class);

    	           } catch(NamingException e) {

    	              if(log.isInfoEnabled()) {

    	                  log.info("NamingException caught in ArRepDeliveryPrintAction.execute(): " + e.getMessage() +
    	                 " session: " + session.getId());

    	              }

    	              return(mapping.findForward("cmnErrorPage"));

    	           }

    	           try {

    	              ejbDP = homeDP.create();

    	           } catch(CreateException e) {

    	               if(log.isInfoEnabled()) {

    	                   log.info("CreateException caught in ArRepDeliveryPrintAction.execute(): " + e.getMessage() +
    	                  " session: " + session.getId());

    	               }

    	               return(mapping.findForward("cmnErrorPage"));

    	           }

    	       ActionErrors errors = new ActionErrors();

    	  	 /*** get report session and if not null set it to null **/

    	  	 Report reportSession =
    	  	    (Report)session.getAttribute(Constants.REPORT_KEY);

    	  	 if(reportSession != null) {

    	  	    reportSession.setBytes(null);
    	  	    session.setAttribute(Constants.REPORT_KEY, reportSession);

    	  	 }

  /*******************************************************
     -- TO DO executeArRepDeliveryPrint() --
  *******************************************************/

    	  		if (frParam != null) {
    	  			ArrayList reportParameters = new ArrayList();
    	                  final String[] majorNames = {"", " Thousand", " Million",
    	  				" Billion", " Trillion", " Quadrillion", " Quintillion"};

    	  				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
    	  				" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};

    	  				final String[] numNames = {"", " One", " Two", " Three", " Four",
    	  				" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
    	  				" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
    	  				" Nineteen"};

    	  			 
    	  			    ArRepDeliveryPrintDetails details = null;
    	  			  AdCompanyDetails adCmpDetails = null;
    				    AdBranchDetails adBranchDetails = null;
    	  			    ArrayList dvCodeList = new ArrayList();
  			    
    	  			    Integer dvCode  = null;
    	  			  Integer soCode  = null;
    	  			   String reportType = "";
    	  			   String VIEW_TYP = "";
    	  				ArrayList list = new ArrayList();
    	  			
    	  				try {

    	  				

    	  					if (request.getParameter("forward") != null) {

    	  						
    	  						System.out.println("dv code is: " + request.getParameter("dvCode"));
    	  						
    	  	            		
    	  	            		soCode = new Integer(request.getParameter("soCode"));    	  	            	
    	  	            		reportType = request.getParameter("reportType");
    	  	            		VIEW_TYP = request.getParameter("viewType");
    	  	            		
    	  	            		System.out.println("dv code is: " + dvCode);
    	  	            		System.out.println("so code is: " + soCode);
    	  	            		System.out.println("report type is: " + reportType);
    	  	            		System.out.println("view type is: " + VIEW_TYP);

    	  					} 

    	  					
    	  					if(reportType.contains("SINGLE")) {
    	  						dvCode = new Integer(request.getParameter("dvCode"));
    	  						list = ejbDP.executeArRepDeliveryByDvCode(dvCode,  user.getCmpCode());
    	  					}else {
    	  						list = ejbDP.executeArRepDeliveryPrintAll(soCode, user.getCmpCode());
    	  					}
    	  					
    	  					
    	  	            

    	  	         

    	  	            	System.out.println("user.getCurrentBranch(): " + user.getCurrentBranch().getBrBranchCode());

    	  	        

    	  	           

    	  			        // get company

    	  			        adCmpDetails = ejbDP.getAdCompany(user.getCmpCode());
    	  			        
    	  			        
    	  			      

    	  	           
    	  	            } catch(EJBException ex) {

    	  	               if (log.isInfoEnabled()) {

    	  	                  log.info("EJBException caught in ArRepDeliveryPrintAction.execute(): " + ex.getMessage() +
    	  	                     " session: " + session.getId());
    	  	               }

    	  	               return(mapping.findForward("cmnErrorPage"));

    	  	           }

    	  	            if (!errors.isEmpty()) {

    	  	                saveErrors(request, new ActionMessages(errors));
    	  	                return mapping.findForward("arRepDeliveryPrint");

    	  	             }

    	  				/** fill report parameters, fill report to pdf and set report session **/
    	  	            ArModSalesOrderDetails soDetails = ejbDP.getArSalesOrderBySoCode(soCode);

    	  				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

    	  				Map parameters = new HashMap();
    	  				parameters.put("company", adCmpDetails.getCmpName());
    	  				parameters.put("companyAddress", companyAddress);
    	  				parameters.put("tinNumber", adCmpDetails.getCmpTin());
    	  				parameters.put("customerCode",soDetails.getSoCstCustomerCode());
    	  				System.out.println("doc num in action: " + soDetails.getSoDocumentNumber());
    	  				parameters.put("soDocumentNumber", soDetails.getSoDocumentNumber());
    	  				parameters.put("soReferenceNumber", soDetails.getSoReferenceNumber());
    	  				parameters.put("lastPageNumber", list.size() % 8 == 0 ? new Integer(list.size() / 8) : new Integer((list.size() / 8) + 1));
    	  				parameters.put("printedBy",user.getUserName());
    	  				parameters.put("datePrinted",Common.convertSQLDateTimeToString(new java.util.Date()));
    	  				
    	  				//report parameters


    	  				System.out.println("size param in inv report is : "  + reportParameters.size());
    	  	      		for(int x=0;x<reportParameters.size();x++) {
    	  	      			ReportParameter reportParameter = (ReportParameter)reportParameters.get(x);
    	  	      			parameters.put(reportParameter.getParameterName(), reportParameter.getParameterValue());
    	  	      		}



    	  				

    	  				String filename = null;
    	  				filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryPrint.jasper";

    					if (!new java.io.File(filename).exists()) {

    						filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryPrint.jasper";

    				        if (!new java.io.File(filename).exists()) {

    				           filename = servlet.getServletContext().getRealPath("jreports/ArRepDeliveryPrint.jasper");

    				        }
    					}

    	  				

    	  				try {

    	  				    Report report = new Report();

    	  				    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {

    	  				       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
    	  					       report.setBytes(
    	  					          JasperRunManager.runReportToPdf(filename, parameters,
    	  					        		  new ArRepDeliveryPrintDS(list)));

    	  					   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

    	  			               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
    	  			               report.setBytes(
    	  							   JasperRunManagerExt.runReportToXls(filename, parameters,
    	  									   new ArRepDeliveryPrintDS(list)));

    	  					   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){

    	  						   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
    	  						   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
    	  								   new ArRepDeliveryPrintDS(list)));

    	  					   } else {
    	  						   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
    	  					       report.setBytes(
    	  					          JasperRunManager.runReportToPdf(filename, parameters,
    	  					        		  new ArRepDeliveryPrintDS(list)));

    	  					   }

    	  				       session.setAttribute(Constants.REPORT_KEY, report);
    	  				       //actionForm.setReport(Constants.STATUS_SUCCESS);

    	  				} catch(Exception ex) {

    	  				    if(log.isInfoEnabled()) {

    	  				      log.info("Exception caught in ArRepDeliveryPrintAction.execute(): " + ex.getMessage() +
    	  					      " session: " + session.getId());

    	  				   }

    	  				    ex.printStackTrace();

    	  				    return(mapping.findForward("cmnErrorPage"));

    	  				}

    	  				return(mapping.findForward("arRepDeliveryPrint"));

    	  		  } else {

    	  		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
    	  		    saveErrors(request, new ActionMessages(errors));

    	  		    return(mapping.findForward("arRepDeliveryPrint"));

    	  		 }

    	       } catch(Exception e) {


    	  /*******************************************************
    	     System Failed: Forward to error page
    	  *******************************************************/
    	            if(log.isInfoEnabled()) {

    	               log.info("Exception caught in ArRepDeliveryPrintAction.execute(): " + e.getMessage()
    	                  + " session: " + session.getId());

    	            }

    	            e.printStackTrace();
    	            return(mapping.findForward("cmnErrorPage"));
    	         }
    	      }

    	
}
