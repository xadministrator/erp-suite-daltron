package com.struts.jreports.ar.deliveryprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepDeliveryPrintDetails;
import com.util.ArRepDeliveryReceiptDetails;

public class ArRepDeliveryPrintDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepDeliveryPrintDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	ArRepDeliveryPrintDetails details = (ArRepDeliveryPrintDetails)i.next();
        
         
         ArRepDeliveryPrintData argData = new ArRepDeliveryPrintData(details.getDpSoCode(), details.getDpSoDocumentNumber(), details.getDpSoReferenceNumber(), details.getDpCstCustomerCode(),
     			details.getDpDvCode(),details.getDpDvContainer(),details.getDpDvDeliveryNumber(), details.getDpDvBookingTime(), details.getDpDvTabsFeePullOut(), details.getDpDvReleasedDate(),
    			details.getDpDvDeliveredDate(), details.getDpDvDeliveredTo(), details.getDpDvPlateNo(), details.getDpDvDriverName(), details.getDpDvEmptyReturnDate(), details.getDpDvEmptyReturnTo(),
    			details.getDpDvTabsFeeReturn(), details.getDpDvStatus(), details.getDpDvRemarks());
		    	
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	
   	
   	
   	if("soCode".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getSoCode();       
   	}else if("soDocumentNumber".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getSoDocumentNumber();
   	}else if("soReferenceNumber".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getSoReferenceNumber();
   	}else if("cstCustomerCode".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getCstCustomerCode();  
   	}else if("dvCode".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getDvCode();
   	}else if("container".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getContainer();
   	}else if("deliveryNumber".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getDeliveryNumber();
   	}else if("bookingTime".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getBookingTime();
   	}else if("tabsFeePullOut".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getTabsFeePullOut();
   	}else if("releasedDate".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getReleasedDate();
   	}else if("deliveredDate".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getDeliveredDate();
   	}else if("deliveredTo".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getDeliveredTo();
   	}else if("plateNo".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getPlateNo();
   	}else if("driverName".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getDriverName();
   	}else if("emptyReturnDate".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getEmptyReturnDate();
   	}else if("emptyReturnTo".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getEmptyReturnTo();
   	}else if("tabsFeeReturn".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getTabsFeeReturn();
   	}else if("status".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getStatus();
   	}else if("remarks".equals(fieldName)){
   		value = ((ArRepDeliveryPrintData)data.get(index)).getRemarks();
   	}
   	
   	return(value);
   }
}
