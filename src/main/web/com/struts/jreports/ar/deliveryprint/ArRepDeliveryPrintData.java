package com.struts.jreports.ar.deliveryprint;

public class ArRepDeliveryPrintData implements java.io.Serializable{
  
	   private String soCode = null;
	   private String soDocumentNumber = null;
	   private String soReferenceNumber = null;
	   private String cstCustomerCode = null;
	   private String dvCode = null;
	   private String container = null;
	   private String deliveryNumber = null;
	   private String bookingTime = null;
	   private String tabsFeePullOut = null;
	   private String releasedDate = null;
	   private String deliveredDate = null;
	   private String deliveredTo = null;
	   private String plateNo = null;
	   private String driverName = null;
	   private String emptyReturnDate = null;
	   private String emptyReturnTo = null;
	   private String tabsFeeReturn = null;
	   private String status = null;
	   private String remarks = null;
	
	public ArRepDeliveryPrintData(String soCode, String soDocumentNumber, String soReferenceNumber, String cstCustomerCode,
			String dvCode, String container, String deliveryNumber, String bookingTime, String tabsFeePullOut, String releasedDate,
			String deliveredDate, String deliveredTo, String plateNo, String driverName, String emptyReturnDate, String emptyReturnTo,
			String tabsFeeReturn, String status, String remarks) {
		
			this.soCode = soCode;
			this.soDocumentNumber = soDocumentNumber;
			this.soReferenceNumber = soReferenceNumber;
			this.cstCustomerCode = cstCustomerCode;
			this.dvCode = dvCode;
			this.container = container;
			this.deliveryNumber = deliveryNumber;
			this.bookingTime = bookingTime;
			this.tabsFeePullOut = tabsFeePullOut;
			this.releasedDate = releasedDate;
			this.deliveredDate = deliveredDate;
			this.deliveredTo = deliveredTo;
			this.plateNo = plateNo;
			this.driverName = driverName;
			this.emptyReturnDate = emptyReturnDate;
			this.emptyReturnTo = emptyReturnTo;
			this.tabsFeeReturn = tabsFeeReturn;
			this.status = status;
			this.remarks = remarks;
		
	}

	public String getSoCode() {
		return soCode;
	}

	public String getSoDocumentNumber() {
		return soDocumentNumber;
	}

	public String getSoReferenceNumber() {
		return soReferenceNumber;
	}

	public String getCstCustomerCode() {
		return cstCustomerCode;
	}

	public String getDvCode() {
		return dvCode;
	}

	public String getContainer() {
		return container;
	}

	public String getDeliveryNumber() {
		return deliveryNumber;
	}

	public String getBookingTime() {
		return bookingTime;
	}

	public String getTabsFeePullOut() {
		return tabsFeePullOut;
	}

	public String getReleasedDate() {
		return releasedDate;
	}

	public String getDeliveredDate() {
		return deliveredDate;
	}

	public String getDeliveredTo() {
		return deliveredTo;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public String getEmptyReturnDate() {
		return emptyReturnDate;
	}

	public String getEmptyReturnTo() {
		return emptyReturnTo;
	}

	public String getTabsFeeReturn() {
		return tabsFeeReturn;
	}

	public String getStatus() {
		return status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setSoCode(String soCode) {
		this.soCode = soCode;
	}

	public void setSoDocumentNumber(String soDocumentNumber) {
		this.soDocumentNumber = soDocumentNumber;
	}

	public void setSoReferenceNumber(String soReferenceNumber) {
		this.soReferenceNumber = soReferenceNumber;
	}

	public void setCstCustomerCode(String cstCustomerCode) {
		this.cstCustomerCode = cstCustomerCode;
	}

	public void setDvCode(String dvCode) {
		this.dvCode = dvCode;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}

	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}

	public void setTabsFeePullOut(String tabsFeePullOut) {
		this.tabsFeePullOut = tabsFeePullOut;
	}

	public void setReleasedDate(String releasedDate) {
		this.releasedDate = releasedDate;
	}

	public void setDeliveredDate(String deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public void setDeliveredTo(String deliveredTo) {
		this.deliveredTo = deliveredTo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public void setEmptyReturnDate(String emptyReturnDate) {
		this.emptyReturnDate = emptyReturnDate;
	}

	public void setEmptyReturnTo(String emptyReturnTo) {
		this.emptyReturnTo = emptyReturnTo;
	}

	public void setTabsFeeReturn(String tabsFeeReturn) {
		this.tabsFeeReturn = tabsFeeReturn;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
