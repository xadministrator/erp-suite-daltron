package com.struts.jreports.ar.deliveryprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepDeliveryPrintForm extends ActionForm implements Serializable{

	 private String userPermission = new String();

	   public String getUserPermission() {

	      return userPermission;

	   } 

	   public void setUserPermission(String userPermission) {

	      this.userPermission = userPermission;

	   }   
	      
	   public void reset(ActionMapping mapping, HttpServletRequest request) {
	   	  
	   }  
}
