package com.struts.jreports.ar.deliveryreceipt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepDeliveryReceiptController;
import com.ejb.txn.ArRepDeliveryReceiptControllerHome;
import com.struts.jreports.ar.deliveryreceipt.ArRepDeliveryReceiptBranchList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepDeliveryReceiptAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepAdjustmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepDeliveryReceiptForm actionForm = (ArRepDeliveryReceiptForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_DELIVERY_RECEIPT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepDeliveryReceipt");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepDeliveryReceiptController EJB
*******************************************************/

         ArRepDeliveryReceiptControllerHome homeDR = null;
         ArRepDeliveryReceiptController ejbDR = null;       

         try {
         	
            homeDR = (ArRepDeliveryReceiptControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepDeliveryReceiptControllerEJB", ArRepDeliveryReceiptControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepDeliveryReceiptAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDR = homeDR.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepDeliveryReceipttAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- INV DR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap(); 
	        	                
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	if (actionForm.getIncludedUnposted()) {	        	
		        	   		        		
	        		criteria.put("includedUnposted", "YES");
                
                }
                
                if (!actionForm.getIncludedUnposted()) {
                	
                	criteria.put("includedUnposted", "NO");
                	
                }
                
                if (actionForm.getIncludedMiscReceipts()) {	        	
		        		
                criteria.put("includedMiscReceipts", "YES");

                }

                if (!actionForm.getIncludedMiscReceipts()) {

                criteria.put("includedMiscReceipts", "NO");

                }

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            // branch map
            
            ArrayList adBrnchList = new ArrayList();
            // ArrayList adBrnchList2 = new ArrayList();
            
            for(int j=0; j < actionForm.getArRepDlvryRcptBrListSize(); j++) {
            	
                ArRepDeliveryReceiptBranchList arRepDlvryRcptBrList = (ArRepDeliveryReceiptBranchList)actionForm.getArRepDlvryRcptBrByIndex(j);
                
                if(arRepDlvryRcptBrList.getBranchCheckbox() == true) {
                    
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(arRepDlvryRcptBrList.getBrCode());
                    adBrnchList.add(mdetails);
                    
                }
                
            }
            
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbDR.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbDR.executeArRepDeliveryReceipt(actionForm.getCriteria(),
            	    actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(), adBrnchList, user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("arRepDeliveryReceipt.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepDeliveryReceiptAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepDeliveryReceipt");

            }

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
			
		    }
			
		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }
		    
		    if (actionForm.getIncludedMiscReceipts()) {
		    	
		    	parameters.put("includedMiscReceipts", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedMiscReceipts", "NO");
		    	
		    }
		    
		    if (actionForm.getShowLineItems()) {
		    	
		    	parameters.put("showLineItems", "YES");
		    	
		    } else {
		    	
		    	parameters.put("showLineItems", "NO");
		    	
		    }
		    
		    parameters.put("orderBy", actionForm.getOrderBy());
		    parameters.put("groupBy", actionForm.getGroupBy());
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepDlvryRcptBrListSize(); j++) {

		    	ArRepDeliveryReceiptBranchList brList = (ArRepDeliveryReceiptBranchList)actionForm.getArRepDlvryRcptBrByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBrName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBrName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = null;
		    
		    if (actionForm.getShowLineItems() && !actionForm.getGroupBy().equals("ITEM NAME")) {
		    	
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryReceiptLineItems.jasper";
		    	
		    	if (!new java.io.File(filename).exists()) {
		    		
		    		filename = servlet.getServletContext().getRealPath("jreports/ArRepDeliveryReceiptLineItems.jasper");
		    		
		    	}
		    	
		    }  else  if (actionForm.getShowLineItems() && actionForm.getGroupBy().equals("ITEM NAME")){
		    	
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryReceiptLineItemsGroupByItems.jasper";
		    	
		    	if (!new java.io.File(filename).exists()) {
		    		
		    		filename = servlet.getServletContext().getRealPath("jreports/ArRepDeliveryReceiptLineItemsGroupByItems.jasper");
		    		
		    	}
		    	
		    } else {
		    	
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryReceipt.jasper";
		    	
		    	if (!new java.io.File(filename).exists()) {
		    		
		    		filename = servlet.getServletContext().getRealPath("jreports/ArRepDeliveryReceipt.jasper");
		    		
		    	}
		    	
		    }
		 
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepDeliveryReceiptDS(list,actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepDeliveryReceiptDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepDeliveryReceiptDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepDeliveryReceiptAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV DR Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV DR Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	             	             
	             try {
	                
	             	 actionForm.reset(mapping, request);
	             	
	                 ArrayList list = null;
	                 Iterator i = null;
	                 
	                 actionForm.clearArRepDlvryRcptBrList();
	                 
	                 list = ejbDR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	                 
	                 i = list.iterator();
	                 //int ctr = 1;
	                 while(i.hasNext()) {
	                     
	                     AdBranchDetails details = (AdBranchDetails)i.next();
	                     
	                     ArRepDeliveryReceiptBranchList arRepDlvryRcptBrList = new ArRepDeliveryReceiptBranchList(actionForm,
	                             details.getBrCode(),
	                             details.getBrBranchCode(), details.getBrName());
	                     if (details.getBrHeadQuarter() == 1) arRepDlvryRcptBrList.setBranchCheckbox(true);
	                     //ctr++;
	                     
	                     actionForm.saveArRepDlvryRcptBrList(arRepDlvryRcptBrList);
	                     
	                 }
	                 
	             } catch (GlobalNoRecordFoundException ex) {
	                 
	             } catch (EJBException ex) {
	                 
	                 if (log.isInfoEnabled()) {
	                     
	                     log.info("EJBException caught in ArRepDeliveryReceiptAction.execute(): " + ex.getMessage() +
	                             " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                 }
	                 
	             } 
	         	
	            return(mapping.findForward("arRepDeliveryReceipt"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepDeliveryReceiptAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
