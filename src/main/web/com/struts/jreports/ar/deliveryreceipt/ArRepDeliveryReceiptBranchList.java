package com.struts.jreports.ar.deliveryreceipt;

import java.io.Serializable;

public class ArRepDeliveryReceiptBranchList implements Serializable {
	
	private String brName = null;
	private String brBranchCode = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;	
	
	private ArRepDeliveryReceiptForm parentBean;
	
	public ArRepDeliveryReceiptBranchList(ArRepDeliveryReceiptForm parentBean, Integer brCode, 
			String brBranchCode, String brName) {
		
		this.parentBean = parentBean;
		this.brCode = brCode;
		this.brName = brName;				
		this.brBranchCode = brBranchCode;
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void setBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}