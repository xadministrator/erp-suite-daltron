package com.struts.jreports.ar.deliveryreceipt;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepDeliveryReceiptDetails;

public class ArRepDeliveryReceiptDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepDeliveryReceiptDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	ArRepDeliveryReceiptDetails details = (ArRepDeliveryReceiptDetails)i.next();
             
         String group = new String("");
         
         if (groupBy.equals("ITEM NAME")) {
         	
         	group = details.getDrItemName();
         	
         } else if (groupBy.equals("CUSTOMER")) {
         	
         	group = details.getDrCustomerName();
         	
         } 
         
         ArRepDeliveryReceiptData argData = new ArRepDeliveryReceiptData(details.getDrCustomerCode(), details.getDrCustomerName(), 
        	 details.getDrDate(), details.getDrDescription(), details.getDrReferenceNumber(), 
	   		 new Double(details.getDrAmount()), group, details.getDrDocumentNumber(),
			 details.getDrItemName(), details.getDrLocation(),
			 new Double (details.getDrQuantity()), details.getDrUnit(), new Double(details.getDrUnitPrice()));
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getDate();       
   	}else if("description".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getDescription();
   	}else if("referenceNumber".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getReferenceNumber();
   	}else if("amount".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getAmount();  
   	}else if("groupBy".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getGroupBy();
   	}else if("documentNumber".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getDocumentNumber();
   	}else if("customerName".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getCustomerName();
   	}else if("itemName".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getItemName();
   	}else if("location".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getLocation();
   	}else if("quantity".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getQuantity();
   	}else if("unit".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getUnit();
   	}else if("unitPrice".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getUnitPrice();
   	}else if("customer".equals(fieldName)){
   		value = ((ArRepDeliveryReceiptData)data.get(index)).getCustomerCode();
   	}
   	
   	return(value);
   }
}
