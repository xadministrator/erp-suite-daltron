package com.struts.jreports.ar.deliveryreceipt;

public class ArRepDeliveryReceiptData implements java.io.Serializable{
	
	private String customerCode = null;
	private String customerName = null;
	private java.util.Date date = null;
	private String description = null;
	private String referenceNumber = null;
	private Double amount = null;
	private String groupBy = null;
	private String documentNumber = null;
	private String itemName = null;
	private String location = null;
	private Double quantity = null;
	private String unit = null;
	private Double unitPrice = null;
	
	public ArRepDeliveryReceiptData(String customerCode, String customerName, java.util.Date date, String description, String referenceNumber,  
			Double amount, String groupBy, String documentNumber, String itemName, 
			String location, Double quantity, String unit, Double unitPrice) {
		
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.date = date;
		this.description = description;
		this.referenceNumber = referenceNumber;
		this.amount = amount;
		this.groupBy = groupBy;
		this.documentNumber = documentNumber; 
		this.itemName = itemName;
		this.location = location;
		this.quantity = quantity;
		this.unit = unit;
		this.unitPrice = unitPrice;
		
	}
	
	public String getCustomerCode() {
		
		return customerCode;
		
	}
	
	public String getCustomerName() {
		
		return customerName;
		
	}
	
	public java.util.Date getDate() {
		
		return date;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	
	public String getGroupBy() {
		
		return groupBy;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public Double getQuantity() {
		
		return quantity;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public Double getUnitPrice() {
		
		return unitPrice;
		
	}
}
