package com.struts.jreports.ar.deliveryreceipt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepDeliveryReceiptForm extends ActionForm implements Serializable{

   private String customerName = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private boolean includedUnposted = false;
   private boolean includedMiscReceipts = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String groupBy = null;
   private boolean showLineItems = false;
   private ArrayList groupByList = new ArrayList();

   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList arRepDlvryRcptBrList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getDateFrom(){
   		return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getCustomerName(){
      return(customerName);
   }

   public void setCustomerName(String customerName){
      this.customerName = customerName;
   }
   
   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public boolean getIncludedMiscReceipts() {
   	
   	  return includedMiscReceipts;
   	
   }
   
   public void setIncludedMiscReceipts(boolean includedMiscReceipts) {
   	
   	  this.includedMiscReceipts = includedMiscReceipts;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
  
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }
 
   public boolean getShowLineItems() {
   	
   	  return showLineItems;
   	
   }
   
   public Object[] getArRepDlvryRcptBrList(){
       
       return arRepDlvryRcptBrList.toArray();
       
   }
   
   public ArRepDeliveryReceiptBranchList getArRepDlvryRcptBrByIndex(int index){
       
       return ((ArRepDeliveryReceiptBranchList)arRepDlvryRcptBrList.get(index));
       
   }
   
   public int getArRepDlvryRcptBrListSize(){
       
       return(arRepDlvryRcptBrList.size());
       
   }
   
   public void saveArRepDlvryRcptBrList(Object newArRepDlvryRcptBrList){
       
   	arRepDlvryRcptBrList.add(newArRepDlvryRcptBrList);   	  
       
   }
   
   public void clearArRepDlvryRcptBrList(){
       
   	arRepDlvryRcptBrList.clear();
       
   }
   
   public void setArRepDlvryRcptBrList(ArrayList arRepDlvryRcptBrList) {
       
       this.arRepDlvryRcptBrList = arRepDlvryRcptBrList;
       
   }
   
   public void setShowLineItems(boolean showLineItems) {
   	
   	  this.showLineItems = showLineItems;
   	
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<arRepDlvryRcptBrList.size(); i++) {
           
           ArRepDeliveryReceiptBranchList  list = (ArRepDeliveryReceiptBranchList)arRepDlvryRcptBrList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
      goButton = null;
      closeButton = null;
      customerName = null;
      dateFrom = null;
      dateTo = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      orderByList.clear();
      orderByList.add(Constants.AR_REP_DELIVERY_RECEIPT_ORDER_BY_DATE);
      orderByList.add(Constants.AR_REP_DELIVERY_RECEIPT_ORDER_BY_DOC_NUM);
      orderBy = Constants.AR_REP_DELIVERY_RECEIPT_ORDER_BY_DATE;
      groupByList.clear();
      groupByList.add(Constants.GLOBAL_BLANK);
      groupByList.add(Constants.AR_REP_DELIVERY_RECEIPT_GROUP_BY_ITEM_NAME);
      groupByList.add(Constants.AR_REP_DELIVERY_RECEIPT_GROUP_BY_CUSTOMER);
      groupBy = Constants.GLOBAL_BLANK;
      includedUnposted = false;
      includedMiscReceipts = false;
	  showLineItems = false;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("arRepDeliveryReceipt.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("arRepDeliveryReceipt.error.dateToInvalid"));
		 }
		 if(!showLineItems && (groupBy.equals(Constants.AR_REP_DELIVERY_RECEIPT_GROUP_BY_ITEM_NAME)  ||
		 		groupBy.equals(Constants.AR_REP_DELIVERY_RECEIPT_GROUP_BY_CUSTOMER))) {
	 		errors.add("groupBy", new ActionMessage("arRepDeliveryReceipt.error.groupByInvalid"));
		 } 
      }
      return(errors);
   }
}
