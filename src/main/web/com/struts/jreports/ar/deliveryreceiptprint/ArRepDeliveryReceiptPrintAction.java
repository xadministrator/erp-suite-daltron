package com.struts.jreports.ar.deliveryreceiptprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepDeliveryReceiptPrintController;
import com.ejb.txn.ArRepDeliveryReceiptPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.ReportParameter;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArRepDeliveryReceiptPrintDetails;
import com.util.ArRepInvoicePrintDetails;

public final class ArRepDeliveryReceiptPrintAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	   
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ArRepDeliveryReceiptPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ArRepDeliveryReceiptPrintForm actionForm = (ArRepDeliveryReceiptPrintForm)form; 
			
			String frParam= Common.getUserPermission(user, Constants.AR_REP_DELIVERY_RECEIPT_PRINT_ID);
			
			if (frParam != null) {
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
			}
			
/*******************************************************
   Initialize ArRepDeliveryReceiptPrintController EJB
*******************************************************/
			
			ArRepDeliveryReceiptPrintControllerHome homeDR = null;
			ArRepDeliveryReceiptPrintController ejbDR = null;       
			
			try {
				
				homeDR = (ArRepDeliveryReceiptPrintControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArRepDeliveryReceiptPrintControllerEJB", ArRepDeliveryReceiptPrintControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in ArRepDeliveryReceiptPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbDR = homeDR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ArRepDeliveryReceiptPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
   -- TO DO executeArRepDeliveryReceiptPrint() --
*******************************************************/       
			
			if (frParam != null) {
				
				ArrayList reportParameters = new ArrayList();
				
				final String[] majorNames = {"", " Thousand", " Million",
						" Billion", " Trillion", " Quadrillion", " Quintillion"};
				
				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
						" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
				
				final String[] numNames = {"", " One", " Two", " Three", " Four",
						" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
						" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};
				
				AdCompanyDetails adCmpDetails = null;
				ArrayList list = null;   
				
				String BR_BRNCH_CODE = user.getCurrentBranch().getBrBranchCode();
				
				try {
					
					ArrayList invCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
						
						invCodeList.add(new Integer(request.getParameter("invoiceCode")));
						
					} else {
						
						int i = 0;
						
						while (true) {
							
							if (request.getParameter("invoiceCode[" + i + "]") != null) {
								
								invCodeList.add(new Integer(request.getParameter("invoiceCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
						
					}
					
					list = ejbDR.executeArRepDeliveryReceiptPrint(invCodeList, user.getCmpCode());
					
					// get company
					
					adCmpDetails = ejbDR.getAdCompany(user.getCmpCode()); 
					
					
					ArRepDeliveryReceiptPrintDetails details = null;
					 Iterator i = list.iterator();

				        while (i.hasNext()) {

				        	details = (ArRepDeliveryReceiptPrintDetails)i.next();

				          

							reportParameters =  Common.convertStringToReportParameters(details.getReportParameters(), new ArrayList());

				        }
					
					
					
					
					
					
					
					
				} catch(GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("deliveryReceiptPrint.error.invoiceAlreadyDeleted"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArRepDeliveryReceiptPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("arRepDeliveryReceiptPrintForm");
					
				}
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				
				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();	            	
				
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("companyAddress", companyAddress);
				parameters.put("tinNumber", adCmpDetails.getCmpTin());
				
				//report parameters


				System.out.println("size param in inv report is : "  + reportParameters.size());
	      		for(int x=0;x<reportParameters.size();x++) {
	      			ReportParameter reportParameter = (ReportParameter)reportParameters.get(x);
	      			parameters.put(reportParameter.getParameterName(), reportParameter.getParameterValue());
	      		}
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/" + BR_BRNCH_CODE + "/ArRepDeliveryReceiptInvoicePrint.jasper";
				
				if (!new java.io.File(filename).exists()) {
					
					filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDeliveryReceiptInvoicePrint.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = servlet.getServletContext().getRealPath("jreports/ArRepDeliveryReceiptInvoicePrint.jasper");
						
					}					
				}
				
				
				try {
					
					Report report = new Report();
					
					report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					report.setBytes(
							JasperRunManager.runReportToPdf(filename, parameters, 
									new ArRepDeliveryReceiptPrintDS(list)));  
					
					session.setAttribute(Constants.REPORT_KEY, report);
					
				} catch(Exception ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("Exception caught in ArRepDeliveryReceiptPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					} 
					
					return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("arRepDeliveryReceiptPrint"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("arRepDeliveryReceiptPrint"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in ArRepDeliveryReceiptPrintAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}
			
			return(mapping.findForward("cmnErrorPage"));
			
		}
		
	}
	
	private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
		
		String soFar;
		
		if (number % 100 < 20){
			soFar = numNames[number % 100];
			number /= 100;
		}
		else {
			soFar = numNames[number % 10];
			number /= 10;
			
			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0) return soFar;
		return numNames[number] + " Hundred" + soFar;
		
	}
	
	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
		
		/* special case */
		if (number == 0) { return "zero"; }
		
		String prefix = "";
		
		if (number < 0) {
			number = -number;
			prefix = "Negative";
		}
		
		String soFar = "";
		int place = 0;
		
		do {
			int n = number % 1000;
			if (n != 0){
				String s = this.convertLessThanOneThousand(n, tensNames, numNames);
				soFar = s + majorNames[place] + soFar;
			}
			place++;
			number /= 1000;
		} while (number > 0);
		
		return (prefix + soFar).trim();
		
	} 
	
}
