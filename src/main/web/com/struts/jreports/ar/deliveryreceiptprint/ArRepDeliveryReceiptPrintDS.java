/*
 * opt/jreport/ArRepDeliveryReceiptPrintDS.java
 *
 * Created on March 11, 2004, 10:56 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.ar.deliveryreceiptprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepDeliveryReceiptPrintDetails;

public class ArRepDeliveryReceiptPrintDS implements JRDataSource {

	private ArrayList data = new ArrayList();

	private int index = -1;

	public ArRepDeliveryReceiptPrintDS(ArrayList list) {

		String invoiceNumber = new String();
		int lineNumber = 0;
		Iterator i = list.iterator();
		while(i.hasNext()) {

			ArRepDeliveryReceiptPrintDetails details = (ArRepDeliveryReceiptPrintDetails)i.next();

			if (!invoiceNumber.equals(details.getDrpInvNumber())) {

				invoiceNumber = details.getDrpInvNumber();
				lineNumber = 0;

			}

			String customerAddress = "";

			if (!details.getDrpInvCustomerAddress().equals(", , ")) {

				customerAddress = details.getDrpInvCustomerAddress();

			}

			ArRepDeliveryReceiptPrintData arRepDRData = new ArRepDeliveryReceiptPrintData(details.getDrpInvNumber(),
					Common.convertSQLDateToString(details.getDrpInvDate()), details.getDrpInvCustomerName(),
					customerAddress, details.getDrpInvCreatedBy(), details.getDrpInvApprovedRejectedBy(), new Double(details.getDrpInvAmountDue()),
					details.getDrIliIiName(), details.getDrpIliDescription(),
					details.getDrpIliPropertyCode(), details.getDrpIliSerialNumber(), details.getDrpIliSpecs() , details.getDrpIliCustodian() ,details.getDrpIliExpiryDate(), details.getDrpIliUom(),
					details.getDrpIliUomShortName(), new Double(details.getDrpIliUnitPrice()),
					new Double(details.getDrpIliQuantity()), new Double(details.getDrpIliAmount()), new Double(details.getDrpIliTaxAmount()),
					details.getDrpInvCustomerCode(), details.getDrpSlpSalespersonCode(), details.getDrpSoDocumentNumber(),
					details.getDrpInvPaymentTerm(), new Double(details.getDrpIliDiscount1()),
					new Double(details.getDrpIliDiscount2()), new Double(details.getDrpIliDiscount3()),
					new Double(details.getDrpIliDiscount4()), new Double(details.getDrpIliTotalDiscount()), details.getDrpInvCreatedByDescription(),
					details.getDrpInvBillingHeader(), details.getDrpInvBillingFooter(),
					details.getDrpInvBillingHeader2(), details.getDrpInvBillingFooter2(),
					details.getDrpInvBillingHeader3(), details.getDrpInvBillingFooter3(), details.getDrpInvReferenceNumber(),
					details.getDrpInvSoReferenceNumber(), details.getDrpInvShipToAddress(), details.getDrpInvClientPo(),details.getDrpInvDescription());

			data.add(arRepDRData);

		}

	}

	public boolean next() throws JRException {

		index++;
		return (index < data.size());

	}

	public Object getFieldValue(JRField field) throws JRException {

		Object value = null;

		String fieldName = field.getName();

		if("customer".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getCustomer();

		}else if("address".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getAddress();

		}else if("date".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDate();

		}else if("invoiceNumber".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getInvoiceNumber();
		}else if("amountDue".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getAmountDue();

		}else if("itemDescription".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemDescription();

		}else if("itemPropertyCode".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemPropertyCode();

		}else if("itemSerialNumber".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemSerialNumber();

		}else if("itemSpecs".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemSpecs();

		}else if("itemCustodian".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemCustodian();

		}else if("itemExpiryDate".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getItemExpiryDate();

		}else if("quantity".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getQuantity();

		}else if("unitPrice".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getUnitPrice();

		}else if("amount".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getAmount();

		}else if("taxAmount".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getTaxAmount();

		}else if("name".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getName();

		}else if("unitOfMeasure".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getUnitOfMeasure();

		}else if("unitOfMeasureShortName".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getUnitOfMeasureShortName();

		}else if("createdBy".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getCreatedBy();
			System.out.print("-->"+value+"<--");
		}else if("approvedBy".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getApprovedBy();
			System.out.print("-->"+value+"<--");
		}else if("customerCode".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getCustomerCode();

		}else if("salesperson".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getSalesperson();

		}else if("soNumber".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getSoNumber();

		}else if("paymentTerm".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getPaymentTerm();

		}else if("discount1".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDiscount1();

		}else if("discount2".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDiscount2();

		}else if("discount3".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDiscount3();

		}else if("discount4".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDiscount4();

		}else if("totalDiscount".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getTotalDiscount();

		}else if("createdByDescription".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getCreatedByDescription();

		}else if("billingHeader".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingHeader();

		}else if("billingFooter".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingFooter();

		}else if("billingHeader2".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingHeader2();

		}else if("billingFooter2".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingFooter2();

		}else if("billingHeader3".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingHeader3();

		}else if("billingFooter3".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getBillingFooter3();

		}else if("referenceNumber".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getReferenceNumber();

		}else if("soReferenceNumber".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getSoReference();

		}else if("shipToAddress".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getShipToAddress();

		}else if("clientPo".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getClientPo();

		}
		else if("description".equals(fieldName)){

			value = ((ArRepDeliveryReceiptPrintData)data.get(index)).getDescription();

		}

		return(value);

	}

}
