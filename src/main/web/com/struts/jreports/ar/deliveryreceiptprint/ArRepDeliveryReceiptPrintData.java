package com.struts.jreports.ar.deliveryreceiptprint;


public class ArRepDeliveryReceiptPrintData implements java.io.Serializable {

	private String invoiceNumber;
	private String date;
	private String customer;
	private String address;
	private String createdBy;
	private String approvedBy;
	private Double amountDue;
	private String clientPo;
	private String name;
	private String itemDescription;
	private String itemPropertyCode;
	private String itemSerialNumber;
	private String itemSpecs;
	private String itemCustodian;
	private String itemExpiryDate;
	private String unitOfMeasure;
	private String unitOfMeasureShortName;
	private Double unitPrice;
	private Double quantity;
	private Double amount;
	private Double taxAmount;
	private String customerCode;
	private String salesperson;
	private String soNumber;
	private String paymentTerm;
	private Double discount1;
	private Double discount2;
	private Double discount3;
	private Double discount4;
	private Double totalDiscount;
	private String createdByDescription = null;

	private String billingHeader;
	private String billingFooter;
	private String billingHeader2;
	private String billingFooter2;
	private String billingHeader3;
	private String billingFooter3;

	private String referenceNumber;
	private String soReference;
	private String shipToAddress;
	private String description;


	public ArRepDeliveryReceiptPrintData(String invoiceNumber, String date, String customer, String address,
			String createdBy, String approvedBy, Double amountDue, String name, String itemDescription,

			String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,
			String unitOfMeasure,
			String unitOfMeasureShortName, Double unitPrice, Double quantity, Double amount, Double taxAmount, String customerCode,
			String salesperson, String soNumber, String paymentTerm, Double discount1, Double discount2,
			Double discount3, Double discount4, Double totalDiscount, String createdByDescription, String billingHeader, String billingFooter,
			String billingHeader2, String billingFooter2, String billingHeader3, String billingFooter3, String referenceNumber,
			String soReference, String shipToAddress, String clientPo,String description) {

		this.invoiceNumber = invoiceNumber;
		this.date = date;
		this.customer = customer;
		this.address = address;
		this.createdBy = createdBy;
		this.approvedBy = approvedBy;
		this.amountDue = amountDue;

		this.name = name;
		this.itemDescription = itemDescription;
		this.itemPropertyCode = itemPropertyCode;
		this.itemSerialNumber = itemSerialNumber;
		this.itemSpecs = itemSpecs;
		this.itemCustodian = itemCustodian;
		this.itemExpiryDate = itemExpiryDate;
		this.unitOfMeasure = unitOfMeasure;
		this.unitOfMeasureShortName = unitOfMeasureShortName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.amount = amount;
		this.taxAmount = taxAmount;
		this.customerCode = customerCode;
		this.salesperson = salesperson;
		this.soNumber = soNumber;
		this.paymentTerm = paymentTerm;
		this.discount1 =discount1;
		this.discount2 =discount2;
		this.discount3 =discount3;
		this.discount4 =discount4;
		this.totalDiscount=totalDiscount;
		this.createdByDescription = createdByDescription;
		this.billingHeader = billingHeader;
		this.billingFooter = billingFooter;
		this.billingHeader2 = billingHeader2;
		this.billingFooter2 = billingFooter2;
		this.billingHeader3 = billingHeader3;
		this.billingFooter3 = billingFooter3;
		this.referenceNumber = referenceNumber;
		this.soReference = soReference;
		this.shipToAddress = shipToAddress;
		this.clientPo = clientPo;
		this.description =description;
	}

	public String getCustomer() {

		return(customer);

	}

	public String getAddress() {

		return(address);

	}

	public Double getAmountDue() {

		return(amountDue);

	}

	public String getDate() {

		return(date);

	}

	public String getInvoiceNumber() {

		return(invoiceNumber);

	}

	public String getItemDescription() {

		return(itemDescription);

	}

	public String getItemPropertyCode() {

		return(itemPropertyCode);

	}

	public String getItemSerialNumber() {

		return(itemSerialNumber);

	}


	public String getItemSpecs() {

		return(itemSpecs);

	}

	public String getItemCustodian() {

		return(itemCustodian);

	}

	public String getItemExpiryDate() {

		return(itemExpiryDate);

	}

	public Double getQuantity() {

		return(quantity);

	}

	public Double getUnitPrice() {

		return(unitPrice);

	}

	public Double getAmount() {

		return(amount);

	}

	public Double getTaxAmount() {

		return(amount);

	}

	public String getName() {

		return(name);

	}

	public String getUnitOfMeasure() {

		return(unitOfMeasure);

	}

	public String getUnitOfMeasureShortName() {

		return unitOfMeasureShortName;

	}

	public String getCreatedBy() {

		return(createdBy);

	}

	public String getApprovedBy() {

		return(approvedBy);

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getSalesperson() {

		return salesperson;

	}

	public String getSoNumber() {

		return soNumber;

	}

	public String getPaymentTerm() {

		return paymentTerm;

	}

	public Double getDiscount1() {

		return discount1;

	}

	public Double getDiscount2() {

		return discount2;

	}

	public Double getDiscount3() {

		return discount3;

	}

	public Double getDiscount4() {

		return discount4;

	}

	public Double getTotalDiscount() {

		return totalDiscount;

	}

	public String getCreatedByDescription() {

		return createdByDescription;

	}

	public String getBillingHeader() {

		return billingHeader;

	}

	public String getBillingFooter() {

		return billingFooter;

	}

	public String getBillingHeader2() {

		return billingHeader2;

	}

	public String getBillingFooter2() {

		return billingFooter2;

	}

	public String getBillingHeader3() {

		return billingHeader3;

	}

	public String getBillingFooter3() {

		return billingFooter3;

	}

	public String getReferenceNumber() {

		return referenceNumber;
	}

	public String getSoReference() {

		return soReference;

	}

	public String getShipToAddress() {

		return shipToAddress;

	}

	public String getClientPo() {

		return clientPo;

	}
	public String getDescription() {

		return description;

	}

}
