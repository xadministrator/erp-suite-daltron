package com.struts.jreports.ar.invoiceeditlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepInvoiceEditListDetails;


public class ArRepInvoiceEditListDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepInvoiceEditListDS(ArrayList arRepIELList, String userName) {

   	  Date currentDate = new Date();
   	
   	  Iterator i = arRepIELList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ArRepInvoiceEditListDetails details = (ArRepInvoiceEditListDetails)i.next();
   	    
   	    if(details.getIelDrDebit() == 1) {
   	    
	   	  	ArRepInvoiceEditListData arRepIELData = new ArRepInvoiceEditListData(
	   	     details.getIelInvBatchName(), details.getIelInvBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getIelInvTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), 
			 Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getIelInvType(), details.getIelInvDocumentNumber(), 
			 Common.convertSQLDateToString(details.getIelInvDate()), details.getIelCstCustomerCode(),
			 details.getIelCstCustomerName(), new Double(details.getIelInvAmountDue()), details.getIelInvDescription(), 
			 details.getIelDrAccountNumber(), details.getIelDrAccountDescription(), details.getIelDrClass(), 
			 new Double(details.getIelDrAmount()), null, details.getIelSalesPersonCode());
	   	  		
	   	  	 System.out.println("details.getIelSalesPersonCode(): " + details.getIelSalesPersonCode());

             data.add(arRepIELData);
	   	  	
	    } else {
	   	
	    	ArRepInvoiceEditListData arRepIELData = new ArRepInvoiceEditListData(
   	   	     details.getIelInvBatchName(), details.getIelInvBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getIelInvTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), 
			 Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getIelInvType(), details.getIelInvDocumentNumber(), 
			 Common.convertSQLDateToString(details.getIelInvDate()), details.getIelCstCustomerCode(),
			 details.getIelCstCustomerName(), new Double(details.getIelInvAmountDue()), details.getIelInvDescription(),
			 details.getIelDrAccountNumber(), details.getIelDrAccountDescription(), details.getIelDrClass(), null, 
			 new Double(details.getIelDrAmount()), details.getIelSalesPersonCode());
    	
	    	System.out.println("details.getIelSalesPersonCode(): " + details.getIelSalesPersonCode());
   	  	    data.add(arRepIELData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

	  if("batchName".equals(fieldName)){
	    value = ((ArRepInvoiceEditListData)data.get(index)).getBatchName(); 
	  }else if("batchDescription".equals(fieldName)){
	    value = ((ArRepInvoiceEditListData)data.get(index)).getBatchDescription();       
	  }else if("transactionTotal".equals(fieldName)){
	    value = ((ArRepInvoiceEditListData)data.get(index)).getTransactionTotal(); 
	  }else if("dateCreated".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getDateCreated();  
      }else if("timeCreated".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getTimeCreated();  
      }else if("createdBy".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getCreatedBy();
      }else if("type".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getType();    
      }else if("documentNumber".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getDocumentNumber();  
      }else if("date".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getDate();   
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getCustomerCode(); 
      }else if("customerName".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getCustomerName();   
      }else if("amountDue".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getAmountDue(); 
      }else if("accountNumber".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getAccountNumber();   
      }else if("accountDescription".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getAccountDescription(); 
      }else if("class".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getClassType();    
      }else if("debitAmount".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getDebitAmount();   
      }else if("creditAmount".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getCreditAmount();                   
      }else if("description".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getDescription();                   
      }else if("salesPersonCode".equals(fieldName)){
        value = ((ArRepInvoiceEditListData)data.get(index)).getSalesPersonCode(); 
        System.out.println("salesPersonCode: " + value);
      }

      return(value);
   }
}
