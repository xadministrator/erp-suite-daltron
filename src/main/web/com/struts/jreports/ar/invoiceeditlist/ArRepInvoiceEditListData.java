package com.struts.jreports.ar.invoiceeditlist;

public class ArRepInvoiceEditListData implements java.io.Serializable {

	   private String batchName = null;
	   private String batchDescription = null;
	   private String transactionTotal = null;
	   private String dateCreated = null;
	   private String timeCreated = null;
	   private String createdBy = null;
	   private String type = null;
	   private String documentNumber = null;
	   private String date = null;
	   private String customerCode = null;
	   private String customerName = null;
	   private Double amountDue = null;
	   private String description = null;

	   private String accountNumber = null;
	   private String accountDescription = null;
	   private String classType = null;
	   private Double debitAmount = null;
	   private Double creditAmount = null;
	   private String salesPersonCode = null;
	   
	   public ArRepInvoiceEditListData(String batchName, String batchDescription, String transactionTotal, String dateCreated, String timeCreated, String createdBy, String type,
	   	String documentNumber, String date, String customerCode, String customerName, Double amountDue, String description, String accountNumber, 
	   	String accountDescription, String classType, Double debitAmount, Double creditAmount, String salesPersonCode) {
	      
	      this.batchName = batchName;
	      this.batchDescription = batchDescription;
	      this.transactionTotal = transactionTotal;
	      this.dateCreated = dateCreated;
	      this.timeCreated = timeCreated;
	      this.createdBy = createdBy;
	      this.type = type;
	      this.documentNumber = documentNumber;
	      this.date = date;
	      this.customerCode = customerCode;
	      this.customerName = customerName;
	      this.amountDue = amountDue;
	      this.accountNumber = accountNumber;
	      this.accountDescription = accountDescription;
	      this.classType = classType;
	      this.debitAmount = debitAmount;
	      this.creditAmount = creditAmount;
	      this.description = description;
	      this.salesPersonCode = salesPersonCode;	
	      
	   }
	   
	   public String getBatchName() {
	   	
	   	  return batchName;
	   	 
	   }
	   
	   public String getBatchDescription() {
	   	
	   	  return batchDescription;
	   	
	   }
	   
	   public String getTransactionTotal() {
	   	
	   	  return transactionTotal;
	   	
	   }

	   public String getDateCreated() {

	      return dateCreated;

	   }

	   public String getTimeCreated() {

	      return timeCreated;

	   }

	   public String getCreatedBy() {

	      return createdBy;

	   }

	   public String getType() {

	      return type;

	   }

	   public String getDocumentNumber() {
	   
	      return documentNumber;

	   }

	   public String getDate() {

	      return date;

	   }

	   public String getCustomerCode() {

	      return customerCode;

	   }
	   
	   public String getCustomerName() {

	      return customerName;

	   }

	   public Double getAmountDue() {

	      return amountDue;

	   }

	   public String getAccountNumber() {

	      return accountNumber;

	   }

	   public String getAccountDescription() {

	      return accountDescription;

	   }

	   public String getClassType() {
	    
	      return classType ;

	   }

	   public Double getDebitAmount() {

	      return debitAmount;

	   }

	   public Double getCreditAmount() {
	    
	      return creditAmount;

	   } 

	   public String getDescription() {

		   return description;

	   }

	   public String getSalesPersonCode() {

		   return salesPersonCode;

	   }
	   
}

