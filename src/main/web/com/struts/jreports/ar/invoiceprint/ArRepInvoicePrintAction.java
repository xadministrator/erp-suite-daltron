package com.struts.jreports.ar.invoiceprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepInvoicePrintController;
import com.ejb.txn.ArRepInvoicePrintControllerHome;
import com.struts.jreports.ar.sales.ArRepSalesDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.ReportParameter;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoicePrintDetails;

public final class ArRepInvoicePrintAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArRepInvoicePrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ArRepInvoicePrintForm actionForm = (ArRepInvoicePrintForm)form;

         String frParam= Common.getUserPermission(user, Constants.AR_REP_INVOICE_PRINT_ID);

         if (frParam != null) {

             actionForm.setUserPermission(frParam.trim());

         } else {

             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ArRepInvoicePrintController EJB
*******************************************************/

         ArRepInvoicePrintControllerHome homeIP = null;
         ArRepInvoicePrintController ejbIP = null;

         try {

            homeIP = (ArRepInvoicePrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepInvoicePrintControllerEJB", ArRepInvoicePrintControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in ArRepInvoicePrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbIP = homeIP.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in ArRepInvoicePrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- TO DO executeArRepInvoicePrint() --
*******************************************************/

		if (frParam != null) {
			ArrayList reportParameters = new ArrayList();
                final String[] majorNames = {"", " Thousand", " Million",
				" Billion", " Trillion", " Quadrillion", " Quintillion"};

				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
				" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};

				final String[] numNames = {"", " One", " Two", " Three", " Four",
				" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
				" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
				" Nineteen"};

			    AdCompanyDetails adCmpDetails = null;
			    AdBranchDetails adBranchDetails = null;
			    ArrayList list = null;
			    ArrayList listSub = null;
			    ArrayList listSubDr = null;
			    ArRepInvoicePrintDetails details = null;
			    ArrayList invCodeList = new ArrayList();

			    String BR_BRNCH_CODE = user.getCurrentBranch().getBrBranchCode();
			    String VIEW_TYP = "";

			    String invoiceType = "";
			    String reportType = "";

			    System.out.println("BR_BRNCH_CODE: " + BR_BRNCH_CODE);
				try {



					if (request.getParameter("forward") != null) {

	            		invCodeList.add(new Integer(request.getParameter("invoiceCode")));
	            		VIEW_TYP = request.getParameter("viewType");
	            		invoiceType = request.getParameter("invoiceType");
	            		reportType = request.getParameter("reportType");

					} else {

						int i = 0;

						while (true) {

							if (request.getParameter("invoiceCode[" + i + "]") != null) {

								invCodeList.add(new Integer(request.getParameter("invoiceCode[" + i + "]")));

								i++;

							} else {

								break;

							}

						}

					}

	            	list = ejbIP.executeArRepInvoicePrint(invCodeList, user.getCmpCode());

	            	/*ArRepInvoicePrintDetails ripDetails = new ArRepInvoicePrintDetails();
	            	ripDetails.setIpDrCoaAccountDescription("");
	            	ripDetails.setIpInvNumber(((ArRepInvoicePrintDetails)list.get(0)).getIpInvNumber());

	            	if(list.size() % 8 != 0) {

	            		int num = 8 - (list.size() % 8);

	            		for(int i=0; i<num; i++) {

	            			list.add(ripDetails);

	            		}

	            	}*/

	            	System.out.println("user.getCurrentBranch(): " + user.getCurrentBranch().getBrBranchCode());

	            	try {

	            		listSub = ejbIP.executeArRepInvoicePrintSub(invCodeList, user.getCmpCode());

	            	} catch (GlobalNoRecordFoundException ex) {

	            	}

			       // if (new java.io.File("/opt/ofs-resources/" + user.getCompany() + "/ArRepInvoicePrintSubDr.jasper").exists()) {

			        //	listSubDr = ejbIP.executeArRepInvoicePrintSubDr(invCodeList, user.getCmpCode());

			      //  }

			      //  if (new java.io.File("/opt/ofs-resources/" + user.getCompany() + "/" + BR_BRNCH_CODE + "/ArRepInvoicePrintSubDr.jasper").exists()) {

			        //	listSubDr = ejbIP.executeArRepInvoicePrintSubDr(invCodeList, user.getCmpCode());

			      //  }

	             if (new java.io.File("/opt/ofs-resources/" + user.getCompany() + "/" + BR_BRNCH_CODE + "/ArRepInvoicePrintSubDr.jasper").exists()) {

			    	listSubDr = ejbIP.executeArRepInvoicePrintSubDr(invCodeList, user.getCmpCode());

			      }



			        // get company

			        adCmpDetails = ejbIP.getAdCompany(user.getCmpCode());

			        Iterator i = list.iterator();

			        while (i.hasNext()) {

			        	details = (ArRepInvoicePrintDetails)i.next();

			            int num = (int)Math.floor(details.getIpInvAmount());

						String str = Common.convertDoubleToStringMoney(details.getIpInvAmount(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());

						details.setIpInvAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " +
						str + "/100 Only");

						reportParameters =  Common.convertStringToReportParameters(details.getIpReportParameter(), new ArrayList());

			        }

	            } catch(GlobalNoRecordFoundException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invoicePrint.error.invoiceAlreadyDeleted"));

	            } catch(EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArRepInvoicePrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }

	               return(mapping.findForward("cmnErrorPage"));

	           }

	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("arRepInvoicePrintForm");

	             }

				/** fill report parameters, fill report to pdf and set report session **/

				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("companyAddress", companyAddress);
				parameters.put("tinNumber", adCmpDetails.getCmpTin());
				parameters.put("createdBy", details.getIpInvCreatedByDesc());
                                System.out.println("details.getIpInvCreatedByDescription() : " + details.getIpInvCreatedByDescription());
				parameters.put("approvedBy", details.getIpInvApprovedRejectedByDescription());
				parameters.put("lastPageNumber", list.size() % 8 == 0 ? new Integer(list.size() / 8) : new Integer((list.size() / 8) + 1));

				//report parameters


				System.out.println("size param in inv report is : "  + reportParameters.size());
	      		for(int x=0;x<reportParameters.size();x++) {
	      			ReportParameter reportParameter = (ReportParameter)reportParameters.get(x);
	      			parameters.put(reportParameter.getParameterName(), reportParameter.getParameterValue());
	      		}



				byte invDebitMemo = ejbIP.getInvDebitMemo(invCodeList, user.getCmpCode());

				String filename = null;

				if (invDebitMemo == 0){

					String subreportFilename = null;

					if(listSub != null) {

						subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/" + BR_BRNCH_CODE + "/ArRepInvoicePrintSub.jasper";

						if (new java.io.File(subreportFilename).exists()) {

							JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
							parameters.put("arRepInvoicePrintSub", subreport);
							parameters.put("arRepInvoicePrintSubDS", new ArRepInvoicePrintSubDS(listSub));
							parameters.put("showSummary", new Boolean(true));

						}else{
							subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepInvoicePrintSub.jasper";

							if (new java.io.File(subreportFilename).exists()) {

								JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
								parameters.put("arRepInvoicePrintSub", subreport);
								parameters.put("arRepInvoicePrintSubDS", new ArRepInvoicePrintSubDS(listSub));
								parameters.put("showSummary", new Boolean(true));

							}
						}

					} else {

						parameters.put("showSummary", new Boolean(false));

					}

					System.out.println("invoice type is: " +invoiceType);

					System.out.println("report type is: " +reportType);

					String jasperFileName = "ArRepInvoicePrint.jasper";
					if(invoiceType.equals("ITEMS")) {

						
						jasperFileName = Common.getLookUpJasperFile("INVOICE_ITEM_PRINT_LOOK_UP", reportType , user.getCompany());


						System.out.println("item report " + jasperFileName);


				    	if(jasperFileName.equals("")) {
				    		jasperFileName = "ArRepInvoicePrint.jasper";
				    	}

					}
					else if(invoiceType.equals("MEMO LINES")) {
						jasperFileName = Common.getLookUpJasperFile("INVOICE_MEMO_LINE_PRINT_LOOK_UP", reportType , user.getCompany());


						System.out.println("memo line report " + jasperFileName);
				    	if(jasperFileName.equals("")) {
				    		jasperFileName = "ArRepInvoicePrint.jasper";
				    	}
					}else if(invoiceType.equals("SO MATCHED")) {
						System.out.println("pasok");
						jasperFileName = Common.getLookUpJasperFile("INVOICE_SO_MATCHED_PRINT_LOOK_UP", reportType , user.getCompany());

						System.out.println("so matched report " + jasperFileName);
						
				
						System.out.println("jasper  is " + jasperFileName);
				    	if(jasperFileName.equals("")) {
				    		System.out.println("not found: " + reportType);
				    		jasperFileName = "ArRepInvoicePrint.jasper";
				    	}
					}else if(invoiceType.equals("JO MATCHED")) {
						System.out.println("pasok jo matched");
						jasperFileName = Common.getLookUpJasperFile("INVOICE_JO_MATCHED_PRINT_LOOK_UP", reportType , user.getCompany());

						System.out.println("jasper  is " + jasperFileName);
				    	if(jasperFileName.equals("")) {
				    		System.out.println("not found: " + reportType);
				    		jasperFileName = "ArRepJobOrderInvoicePrint.jasper";
				    	}
					}else {
						
						System.out.println("default jo match invoice print");
						jasperFileName = "ArRepJobOrderInvoicePrint.jasper";

					}

					System.out.println("br code is: " + BR_BRNCH_CODE);
					filename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/" + jasperFileName;
				//	filename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/ArRepInvoicePrint.jasper";



					if (!new java.io.File(filename).exists()) {
						System.out.println("customer report not exist: " + filename);
						
						filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepInvoicePrint.jasper";

						if (!new java.io.File(filename).exists()) {

							filename = servlet.getServletContext().getRealPath("jreports/ArRepInvoicePrint.jasper");

						}
					}

				} else {

					filename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/ArRepDebitMemoPrint.jasper";

					if (!new java.io.File(filename).exists()) {

						filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDebitMemoPrint.jasper";

				        if (!new java.io.File(filename).exists()) {

				           filename = servlet.getServletContext().getRealPath("jreports/ArRepDebitMemoPrint.jasper");

				        }
					}


			        String subreportFilename = null;

					if(listSubDr != null) {

						subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/ArRepInvoicePrintSubDr.jasper";

						if (new java.io.File(subreportFilename).exists()) {
							System.out.println("sub");
							JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
							parameters.put("arRepInvoicePrintSubDr", subreport);
							parameters.put("arRepInvoicePrintSubDrDS", new ArRepInvoicePrintSubDrDS(listSubDr));

						}else{

							subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepInvoicePrintSubDr.jasper";

							if (new java.io.File(subreportFilename).exists()) {
								System.out.println("sub");
								JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
								parameters.put("arRepInvoicePrintSubDr", subreport);
								parameters.put("arRepInvoicePrintSubDrDS", new ArRepInvoicePrintSubDrDS(listSubDr));

							}

						}



					}

				}

				try {

				    Report report = new Report();

				    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {

				       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					       report.setBytes(
					          JasperRunManager.runReportToPdf(filename, parameters,
					        		  new ArRepInvoicePrintDS(list, ejbIP.getPrfArSalesInvoiceDataSource(user.getCmpCode()))));

					   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

			               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
			               report.setBytes(
							   JasperRunManagerExt.runReportToXls(filename, parameters,
									   new ArRepInvoicePrintDS(list, ejbIP.getPrfArSalesInvoiceDataSource(user.getCmpCode()))));

					   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){

						   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
						   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
								   new ArRepInvoicePrintDS(list, ejbIP.getPrfArSalesInvoiceDataSource(user.getCmpCode()))));

					   } else {
						   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					       report.setBytes(
					          JasperRunManager.runReportToPdf(filename, parameters,
					        		  new ArRepInvoicePrintDS(list, ejbIP.getPrfArSalesInvoiceDataSource(user.getCmpCode()))));

					   }

				       session.setAttribute(Constants.REPORT_KEY, report);
				       //actionForm.setReport(Constants.STATUS_SUCCESS);

				} catch(Exception ex) {

				    if(log.isInfoEnabled()) {

				      log.info("Exception caught in ArRepInvoicePrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());

				   }

				    ex.printStackTrace();

				    return(mapping.findForward("cmnErrorPage"));

				}

				return(mapping.findForward("arRepInvoicePrint"));

		  } else {

		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));

		    return(mapping.findForward("arRepInvoicePrintForm"));

		 }

     } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
          if(log.isInfoEnabled()) {

             log.info("Exception caught in ArRepInvoicePrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());

          }

          e.printStackTrace();
          return(mapping.findForward("cmnErrorPage"));
       }
    }

	private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
	    String soFar;

	    if (number % 100 < 20){
	        soFar = numNames[number % 100];
	        number /= 100;
	       }
	    else {
	        soFar = numNames[number % 10];
	        number /= 10;

	        soFar = tensNames[number % 10] + soFar;
	        number /= 10;
	       }
	    if (number == 0) return soFar;
	    return numNames[number] + " Hundred" + soFar;
	}

	private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
	    /* special case */
	    if (number == 0) { return "zero"; }

	    String prefix = "";

	    if (number < 0) {
	        number = -number;
	        prefix = "Negative";
	      }

	    String soFar = "";
	    int place = 0;

	    do {
	      int n = number % 1000;
	      if (n != 0){
	         String s = this.convertLessThanOneThousand(n, tensNames, numNames);
	         soFar = s + majorNames[place] + soFar;
	        }
	      place++;
	      number /= 1000;
	      } while (number > 0);

	    return (prefix + soFar).trim();
	}
}
