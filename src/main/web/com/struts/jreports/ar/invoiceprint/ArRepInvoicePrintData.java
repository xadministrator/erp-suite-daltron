package com.struts.jreports.ar.invoiceprint;

import java.util.Date;

public class ArRepInvoicePrintData implements java.io.Serializable {

	private String invoiceType = null;
	private String customer = null;
	private String address = null;
	private String date = null;
	private Date date2 = null;
	private String invoiceNumber = null;
	private String invoiceBatch = null;
	private String number = null;
	private Integer lineNumber = null;
	private String description = null;
	private String itemPropertyCode = null;
	private String itemSerialNumber = null;
	private String itemSpecs = null;
	private String itemCustodian = null;
	private String itemExpiryDate = null;
	private String quantity = null;
	private Double unitPrice = null;
	private Double unitPriceWoVat = null;
	private Double amount = null;
	private String isTax = null;
	private String dueDate = null;
	private String effectivityDate = null;
	private String currency = null;
	private String currencySymbol = null;
	private String currencyDescription = null;
	private String amountInWords = null;
	private String branchCode = null;
	private String branchName = null;

	// added fields
	private String billingHeader = null;
	private String billingFooter = null;
	private String billingHeader2 = null;
	private String billingFooter2 = null;
	private String billingHeader3 = null;
	private String billingFooter3 = null;
	private String billingSignatory = null;
	private String signatoryTitle = null;
	private String billToAddress = null;
	private String billToContact = null;
	private String billToAltContact = null;
	private String billToPhone = null;
	private String shipToAddress = null;
	private String shipToContact = null;
	private String shipToAltContact = null;
	private String shipToPhone = null;
	private Double taxAmount = null;
	private Double amountWoVat = null;

	private Double discountAmount = null;
	private String discountDescription = null;
	private String unitOfMeasure = null;
	private String paymentTermName = null;
	private String paymentTermDescription = null;
	private String itemCategory = null;
	private String referenceNumber = null;
	private String customerCode = null;
	private String itemName = null;
	private String unitOfMeasureShortName = null;
	private String salespersonCode = null;
	private String invoiceDescription = null;
	private Double invoiceAmount = null;
	private Double invoiceAmountUnearnedInterest = null;

	private String salespersonName = null;

	private String customerDescription = null;
	private String customerTin = null;
	private String customerContactPerson = null;
	private String customerPhoneNumber = null;
	private String customerMobileNumber = null;
	private String customerFax = null;
	private String customerEmail = null;


	private String soNumber = null;
	private String joNumber = null;
	
	private String jaLineNumber = null;
	private String jaRemarks = null;
	private Double jaQuantity = null;
	private Double JaUnitCost = null;
	private Double JaAmount = null;
	private Boolean JaSo = null;
	private String JaPeIdNumber = null;
	private String JaPeName = null;
	private Boolean jobServices = null;
	
	
	
	

	private Double discount1 = null;
	private Double discount2 = null;
	private Double discount3 = null;
	private Double discount4 = null;

	private String clientPo = null;

	private Double taxRate = null;

	private Boolean showDuplicate = null;

	private Double amountDue = null;
	private String createdBy = null;
	private Date ipsDueDate = null;

	private String taxCode = null;
	private String withholdingTaxCode = null;
	private Double withholdingTaxAmount = null;

	private Double quantity2 = null;
	private Double totalDiscount = null;

	private String createdByDesc = null;
	private String checkedByDescription = null;
	private String approvedRejectedByDescription = null;

	private String partNumber = null;
	private String soReference = null;
	private String approvalStatus = null;
	private String dealPrice = null;
	private boolean isDraft = false;




	public ArRepInvoicePrintData(String invoiceType, String customer, String address, String date, Date date2,
			String invoiceNumber, String invoiceBatch, String number, Integer lineNumber, String description,
			String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,
			String quantity, Double unitPrice, Double amount, String isTax,

			String dueDate, String effectivityDate,
			String currency, String currencySymbol, String currencyDescription, String amountInWords,
			Boolean showDuplicate, String billingHeader, String billingFooter,
			String billingHeader2, String billingFooter2, String billingHeader3, String billingFooter3,
			String billingSignatory, String signatoryTitle, String billToAddress, String billToContact,
			String billToAltContact, String billToPhone, String shipToAddress, String shipToContact,
			String shipToAltContact, String shipToPhone, Double taxAmount, Double amountWoVat,
			Double discountAmount, String discountDescription, String unitOfMeasure,
			String paymentTermName, String paymentTermDescription, String itemCategory, String referenceNumber,
			String customerCode, String itemName, String unitOfMeasureShortName, String salespersonCode,
			String invoiceDescription, Double invoiceAmount, Double invoiceAmountUnearnedInterest, String salespersonName, String customerDescription,
			String customerTin, String customerContactPerson, String customerPhoneNumber, String customerMobileNumber, String customerFax, String customerEmail,
			String soNumber, String joNumber, Double unitPriceWoVat, Double discount1, Double discount2,
			Double discount3, Double discount4, String clientPo, Double taxRate, String taxCode, String withholdingTaxCode, Double withholdingTaxAmount,
			Double quantity2, Double totalDiscount, String createdBy, String createdByDesc, String checkedByDescription, String approvedRejectedByDescription,
			String branchCode, String branchName, String partNumber, String soReference, String approvalStatus, String dealPrice, boolean isDraft,

			String jaLineNumber, String jaPeIdNumber, String jaPeName, Double jaQuantity, Double jaUnitCost, Double jaAmount, String jaRemarks, Boolean jaSo, Boolean jobServices
			
			
			) {

		this.invoiceType = invoiceType;
		this.customer = customer;
		this.address  = address;
		this.date  = date;
		this.date2 = date2;
		this.invoiceNumber  = invoiceNumber;
		this.invoiceBatch = invoiceBatch;
		this.number = number;
		this.lineNumber = lineNumber;
		this.description = description;
		this.itemPropertyCode = itemPropertyCode;
		this.itemSerialNumber = itemSerialNumber;
		this.itemSpecs = itemSpecs;
		this.itemCustodian = itemCustodian;
		this.itemExpiryDate = itemExpiryDate;

		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.unitPriceWoVat = unitPriceWoVat;
		this.amount = amount;
		this.isTax = isTax;
		this.dueDate = dueDate;
		this.effectivityDate = effectivityDate;
		this.currency = currency;
		this.currencySymbol = currencySymbol;
		this.currencyDescription = currencyDescription;
		this.amountInWords = amountInWords;
		this.showDuplicate = showDuplicate;

		this.billingHeader = billingHeader;
		this.billingFooter = billingFooter;
		this.billingHeader2 = billingHeader2;
		this.billingFooter2 = billingFooter2;
		this.billingHeader3 = billingHeader3;
		this.billingFooter3 = billingFooter3;
		this.billingSignatory = billingSignatory;
		this.signatoryTitle = signatoryTitle;
		this.billToAddress = billToAddress;
		this.billToContact = billToContact;
		this.billToAltContact = billToAltContact;
		this.billToPhone = billToPhone;
		this.shipToAddress = shipToAddress;
		this.shipToContact = shipToContact;
		this.shipToAltContact = shipToAltContact;
		this.shipToPhone = shipToPhone;
		this.taxAmount = taxAmount;
		this.amountWoVat = amountWoVat;
		this.taxCode = taxCode;
		this.withholdingTaxCode = withholdingTaxCode;

		this.discountAmount = discountAmount;
		this.discountDescription = discountDescription;
		this.unitOfMeasure = unitOfMeasure;
		this.paymentTermName = paymentTermName;
		this.paymentTermDescription = paymentTermDescription;
		this.itemCategory = itemCategory;
		this.referenceNumber = referenceNumber;
		this.customerCode = customerCode;
		this.itemName = itemName;
		this.unitOfMeasureShortName = unitOfMeasureShortName;
		this.salespersonCode = salespersonCode;
		this.invoiceDescription = invoiceDescription;
		this.invoiceAmount = invoiceAmount;
		this.invoiceAmountUnearnedInterest = invoiceAmountUnearnedInterest;
		this.salespersonName = salespersonName;
		this.customerDescription = customerDescription;
		this.customerTin = customerTin;
		this.customerContactPerson = customerContactPerson;
		this.customerPhoneNumber = customerPhoneNumber;
		this.customerMobileNumber = customerMobileNumber;
		this.customerFax = customerFax;
		this.customerEmail = customerEmail;


		
		
		
		
		

		this.soNumber = soNumber;

		this.joNumber = joNumber;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;

		this.clientPo = clientPo;

		this.taxRate = taxRate;
		this.withholdingTaxAmount = withholdingTaxAmount;

		this.quantity2 = quantity2;
		this.totalDiscount = totalDiscount;
		this.createdBy = createdBy;

		this.createdByDesc = createdByDesc;
		this.checkedByDescription =checkedByDescription;
		this.approvedRejectedByDescription = approvedRejectedByDescription;

		this.branchCode = branchCode;
		this.branchName = branchName;

		this.partNumber = partNumber;
		this.soReference = soReference;
		this.approvalStatus = approvalStatus;
		this.dealPrice = dealPrice;
		this.isDraft = isDraft;

		this.jaLineNumber = jaLineNumber;
		this.JaPeIdNumber = jaPeIdNumber;
		this.JaPeName = jaPeName;
		this.jaQuantity = jaQuantity;
		this.JaUnitCost = jaUnitCost;
		this.JaAmount = jaAmount;
		this.jaRemarks = jaRemarks;
		this.JaSo = jaSo;
		this.jobServices = jobServices;
		

	}

	public ArRepInvoicePrintData(String invoiceType, String customer, String invoiceNumber, String invoiceBatch, String address, String billToContact, String billToAddress,
			Double amount, String paymentName, Date ipsDueDate, Double amountDue, String createdBy, String createdByDesc, String branchCode, String branchName, String partNumber) {

		this.invoiceType = invoiceType;
		this.customer = customer;
		this.invoiceNumber = invoiceNumber;
		this.invoiceBatch = invoiceBatch;
		this.address = address;
		this.billToContact = billToContact;
		this.billToAddress = billToAddress;
		this.amount = amount;

		this.paymentTermName = paymentName;
		this.ipsDueDate = ipsDueDate;
		this.amountDue = amountDue;
		this.createdBy = createdBy;
		this.createdByDesc = createdByDesc;
		//	this.branchCode = branchCode;
		//	this.branchName = branchName;

	}

	public String getInvoiceType() {

		return(invoiceType);

	}
	
	
	public String getCustomer() {

		return(customer);

	}

	public String getAddress() {

		return(address);

	}

	public String getDate() {

		return(date);

	}

	public Date getDate2() {

		return(date2);

	}

	public String getInvoiceNumber() {

		return(invoiceNumber);

	}

	public String getInvoiceBatch() {

		return(invoiceBatch);

	}

	public String getNumber() {

		return(number);

	}

	public Integer getLineNumber() {

		return(lineNumber);

	}

	public String getDescription() {

		return(description);

	}

	public String getItemPropertyCode() {

		return(itemPropertyCode);

	}


	public String getItemSerialNumber() {

		return(itemSerialNumber);

	}

	public String getItemSpecs() {

		return(itemSpecs);

	}

	public String getItemCustodian() {

		return(itemCustodian);

	}

	public String getItemExpiryDate() {

		return(itemExpiryDate);

	}

	public String getItem() {

		return(description);

	}

	public String getQuantity() {

		return(quantity);

	}

	public Double getUnitPrice() {

		return(unitPrice);

	}

	public Double getUnitPriceWoVat() {

		return(unitPriceWoVat);

	}

	public Double getAmount() {

		return(amount);

	}

	public String getIsTax() {
		return(isTax);
	}

	public String getDueDate() {

		return(dueDate);

	}

	public String getEffectivityDate() {

		return(effectivityDate);

	}

	public String getCurrency() {

		return(currency);

	}

	public String getCurrencySymbol() {

		return(currencySymbol);

	}

	public String getCurrencyDescription() {

		return(currencyDescription);

	}

	public String getAmountInWords() {

		return(amountInWords);

	}

	public Boolean getShowDuplicate() {

		return showDuplicate;

	}

	public String getBillingHeader() {

		return billingHeader;

	}

	public String getBillingFooter() {

		return billingFooter;

	}

	public String getBillingHeader2() {

		return billingHeader2;

	}

	public String getBillingFooter2() {

		return billingFooter2;

	}

	public String getBillingHeader3() {

		return billingHeader3;

	}

	public String getBillingFooter3() {

		return billingFooter3;

	}

	public String getBillingSignatory() {

		return billingSignatory;

	}

	public String getSignatoryTitle() {

		return signatoryTitle;

	}

	public String getBillToAddress() {

		return billToAddress;

	}

	public String getBillToContact() {

		return billToContact;

	}

	public String getBillToAltContact() {

		return billToAltContact;

	}

	public String getBillToPhone() {

		return billToPhone;

	}

	public String getShipToAddress() {

		return shipToAddress;

	}

	public String getShipToContact() {

		return shipToContact;

	}

	public String getShipToAltContact() {

		return shipToAltContact;

	}

	public String getShipToPhone() {

		return shipToPhone;

	}

	public Double getTaxAmont() {

		return taxAmount;
	}

	public Double getAmountWoVat() {

		return amountWoVat;
	}

	public Double getDiscountAmount() {

		return discountAmount;

	}

	public String getDiscountDescription() {

		return discountDescription;

	}

	public String getUnitOfMeasure() {

		return unitOfMeasure;

	}

	public String getPaymentTermName() {

		return paymentTermName;

	}

	public String getPaymentTermDescription() {

		return paymentTermDescription;

	}

	public String getItemCategory() {

		return itemCategory;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getItemName() {

		return itemName;

	}

	public String getUnitOfMeasureShortName() {

		return unitOfMeasureShortName;

	}

	public String getSalespersonCode() {

		return salespersonCode;

	}

	public String getInvoiceDescription() {

		return invoiceDescription;

	}

	public Double getInvoiceAmount() {

		return invoiceAmount;

	}

	public Double getInvoiceAmountUnearnedInterest() {

		return invoiceAmountUnearnedInterest;

	}

	public String getSalespersonName() {

		return salespersonName;

	}

	public String getCustomerDescription() {

		return customerDescription;

	}

	public String getCustomerTin() {

		return customerTin;

	}


	public String getCustomerContactPerson() {

		return customerContactPerson;

	}

	public String getCustomerPhoneNumber() {

		return customerPhoneNumber;

	}

	public String getCustomerMobileNumber() {

		return customerMobileNumber;

	}

	public String getCustomerFax() {

		return customerFax;

	}

	public String getCustomerEmail() {

		return customerEmail;

	}

	public String getSoNumber() {

		return soNumber;

	}
	
	public String getJoNumber() {

		return joNumber;

	}

	public Double getDiscount1() {

		return discount1;

	}

	public Double getDiscount2() {

		return discount2;

	}

	public Double getDiscount3() {

		return discount3;

	}

	public Double getDiscount4() {

		return discount4;

	}

	public String getClientPo() {

		return clientPo;

	}


	public Double getTaxRate() {

		return taxRate;

	}

	public Double getAmountDue() {

		return amountDue;

	}

	public String getCreatedBy() {

		return createdBy;

	}

	public Date getIpsDueDate() {

		return ipsDueDate;

	}

	public String getTaxCode() {

		return taxCode;
	}

	public String getWithholdingTaxCode() {

		return withholdingTaxCode;
	}

	public Double getWithholdingTaxAmount() {

		return withholdingTaxAmount;
	}

	public Double getQuantity2() {

		return quantity2;
	}

	public Double getTotalDiscount() {

		return totalDiscount;
	}

	public String getCreatedByDesc() {

		return createdByDesc;

	}

	public String getCheckedByDescription() {

		return checkedByDescription;

	}

	public String getApprovedRejectedByDescription() {

		return approvedRejectedByDescription;

	}


	public String getBranchCode() {

		return branchCode;

	}

	public String getBranchName() {

		return branchName;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public String getSoReference() {

		return soReference;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public String getDealPrice() {

		return dealPrice;

	}

	public boolean getIsDraft() {

		return isDraft;

	}

	public String getJaLineNumber() {
		return jaLineNumber;
	}

	public String getJaRemarks() {
		return jaRemarks;
	}

	public Double getJaQuantity() {
		return jaQuantity;
	}

	public Double getJaUnitCost() {
		return JaUnitCost;
	}

	public Double getJaAmount() {
		return JaAmount;
	}

	public Boolean getJaSo() {
		return JaSo;
	}

	public String getJaPeIdNumber() {
		return JaPeIdNumber;
	}

	public String getJaPeName() {
		return JaPeName;
	}

	public Boolean getJobServices() {
		return jobServices;
	}

	

}
