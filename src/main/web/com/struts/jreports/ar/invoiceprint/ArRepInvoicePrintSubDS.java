package com.struts.jreports.ar.invoiceprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepInvoicePrintDetails;

public class ArRepInvoicePrintSubDS implements JRDataSource {

	   private ArrayList data = new ArrayList();

	   private int index = -1;

	   public ArRepInvoicePrintSubDS(ArrayList list) {
	   	
	 	  Iterator i = list.iterator();
	   	  
	   	  while(i.hasNext()) {
	   	  	
	   	  	ArRepInvoicePrintDetails details = (ArRepInvoicePrintDetails)i.next(); 
	   	    
	   	  	ArRepInvoicePrintSubData arRepInvPrintData = new ArRepInvoicePrintSubData(details.getIpInvDate(), 
	   	  			new Double(details.getIpIlAmount()), details.getIpIlCategory());
	   	    
	   	    data.add(arRepInvPrintData);

	     }
	   	  
	   }
	   	  
	   public boolean next() throws JRException {
	   	
	      index++;

	      if (index == data.size()) {

	          index = -1;
	          return false;

	      } else return true;

	      
	   }

	   public Object getFieldValue(JRField field) throws JRException {
	   	
	      Object value = null;

	      String fieldName = field.getName();

	      if("date".equals(fieldName)){
	         value = ((ArRepInvoicePrintSubData)data.get(index)).getDate(); 
	      }else if("amount".equals(fieldName)){
	         value = ((ArRepInvoicePrintSubData)data.get(index)).getAmount(); 
	      }else if("itemCategory".equals(fieldName)){
	      	 value = ((ArRepInvoicePrintSubData)data.get(index)).getItemCategory();
	      }

	      return(value);
	   }
}
