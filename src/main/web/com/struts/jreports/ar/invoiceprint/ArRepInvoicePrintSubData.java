package com.struts.jreports.ar.invoiceprint;

import java.util.Date;

public class ArRepInvoicePrintSubData implements java.io.Serializable {

	private Date date = null;
	private Double amount = null;
	private String itemCategory = null;
	
	public ArRepInvoicePrintSubData(Date date, Double amount, String itemCategory) {
		
		this.date = date;
		this.amount = amount;
		this.itemCategory = itemCategory;
		
	}
	
	public Date getDate() {
		
		return date;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	public String getItemCategory() {
		
		return itemCategory;
		
	}
	
}