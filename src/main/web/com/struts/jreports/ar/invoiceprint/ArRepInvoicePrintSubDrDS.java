package com.struts.jreports.ar.invoiceprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepInvoicePrintDetails;

public class ArRepInvoicePrintSubDrDS implements JRDataSource {

	   private ArrayList data = new ArrayList();

	   private int index = -1;

	   public ArRepInvoicePrintSubDrDS(ArrayList list) {
	   	
	 	  Iterator i = list.iterator();
	   	  
	   	  while(i.hasNext()) {
	   	  	
	   	  	ArRepInvoicePrintDetails details = (ArRepInvoicePrintDetails)i.next(); 
	   	    
	   	  	ArRepInvoicePrintSubDrData arRepInvPrintDrData = new ArRepInvoicePrintSubDrData(details.getIpDrCoaAccountNumber(),
	   	  			new Boolean(Common.convertByteToBoolean(details.getIpDrDebit())), new Double(details.getIpDrAmount()));
	   	    
	   	    data.add(arRepInvPrintDrData);

	     }
	   	  
	   }
	   	  
	   public boolean next() throws JRException {
	   	
	      index++;

	      if (index == data.size()) {

	          index = -1;
	          return false;

	      } else return true;

	      
	   }

	   public Object getFieldValue(JRField field) throws JRException {
	   	
	      Object value = null;

	      String fieldName = field.getName();

	      if("accountNumber".equals(fieldName)){
	         value = ((ArRepInvoicePrintSubDrData)data.get(index)).getAccountNumber(); 
	      }else if("isDebit".equals(fieldName)){
	      	 value = ((ArRepInvoicePrintSubDrData)data.get(index)).getIsDebit();
	      }else if("amount".equals(fieldName)){
	      	 value = ((ArRepInvoicePrintSubDrData)data.get(index)).getAmount();
	      }

	      return(value);
	   }
}
