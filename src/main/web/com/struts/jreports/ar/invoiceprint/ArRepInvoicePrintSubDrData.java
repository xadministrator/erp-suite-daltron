package com.struts.jreports.ar.invoiceprint;

public class ArRepInvoicePrintSubDrData implements java.io.Serializable {

	private String accountNumber = null;
	private Boolean isDebit = null;
	private Double amount = null;
	
	public ArRepInvoicePrintSubDrData(String accountNumber, Boolean isDebit, Double amount) {
		
		this.accountNumber = accountNumber;
		this.isDebit = isDebit;
		this.amount = amount;
		
	}
	
	public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public Boolean getIsDebit() {
		
		return isDebit;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
}