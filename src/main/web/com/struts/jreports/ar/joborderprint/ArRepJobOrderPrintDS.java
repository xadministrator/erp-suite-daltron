package com.struts.jreports.ar.joborderprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintData;
import com.struts.util.Common;
import com.util.ArRepJobOrderPrintDetails;
import com.util.ArRepSalesOrderPrintDetails;

public class ArRepJobOrderPrintDS implements JRDataSource{

	private ArrayList data = new ArrayList();

	private int index = -1;

	public ArRepJobOrderPrintDS(ArrayList list) {

		Iterator i = list.iterator();

		while (i.hasNext()) {

			ArRepJobOrderPrintDetails details = (ArRepJobOrderPrintDetails)i.next();

			System.out.println("technician: "+  details.getJpJoTechnician());
			ArRepJobOrderPrintData joData = new ArRepJobOrderPrintData(
					
					details.getJpJaPeIdNumber(),details.getJpJaPeName(), details.getJpJaQuantity(), details.getJpJaUnitCost(), 
					details.getJpJaAmount(), details.getJpJaRemarks(), details.getJpJoJobOrderStatus(),

					Common.convertSQLDateToString(details.getJpJoDate()),
			        details.getJpJoReferenceNumber(), details.getJpJoDocumentNumber(), details.getJpJoTransactionType(),
					details.getJpJoDescription(), details.getJpJoBillTo(), details.getJpJoShipTo(), details.getJpJoTechnician(),
					new Double(details.getJpJolQuantity()), details.getJpJolUom(),
			        details.getJpJolIiCode(), details.getJpJolLocName(), new Double(details.getJpJolUnitPrice()),
			        new Double(details.getJpJolAmount()), details.getJpJoCstCustomerCode(), details.getJpJoCstName(),
					details.getJpJoCstAddress(), new Double(details.getJpJoCstCreditLimit()),

					details.getJpJoCstContactPerson(), details.getJpJoCstPhoneNumber(), details.getJpJoCstMobileNumber(), details.getJpJoCstFax(), details.getJpJoCstEmail(),
					details.getJpJolIiDescription(),
					details.getJpJolPropertyCode(), details.getJpJolSerialNumber(), details.getJpJolSpecs(), details.getJpJolCustodian(), details.getJpJolExpiryDate(),
					new String (details.getJpJoFcSymbol() + ""), new Double(details.getJpJoCstCbBalance()),
					new Double(details.getJpJoCstPendingOrder()), new Double(details.getJpJoAmount()), details.getJpJolIiStockStatus(),
					new Double(details.getJpJoCstLastReceiptAmount()), details.getJpJoCstLastReceiptDate(), details.getJpJoPaymentTerm(),
					new Double(details.getJpJoAgBucket0()), new Double(details.getJpJoAgBucket1()), new Double(details.getJpJoAgBucket2()),
					new Double(details.getJpJoAgBucket3()), new Double(details.getJpJoAgBucket4()), new Double(details.getJpJoAgBucket5()),
					details.getJpJoCstSlsSalespersonCode(), details.getJpJoCstSlsName(), details.getJpJoCstCity(),
					new Double(details.getJpJolTotalDiscount()), new Double(details.getJpJolDiscount1()),
					new Double(details.getJpJolDiscount2()), new Double(details.getJpJolDiscount3()),
					new Double(details.getJpJolDiscount4()), new Double(details.getJpJoAdvanceAmount()), new Double(details.getJpJolTaxAmount()),
					new Double(details.getJpJoTaxRate()), new Double(details.getJpJolUnitPriceWoTax()),
					details.getJpJoTaxType(), new Double(details.getJpJolQuantityOnHand()), details.getSpIlCategory(), details.getJpJoCustomerDealPrice(),
					details.getJpJoMemo()
					);
			data.add(joData);

		}

	}

	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}

	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;

		String fieldName = field.getName();

		if("date".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getDate();
		} else if("personelIdNumber".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getPersonelIdNumber();
		} else if("personelName".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getPersonelName();
		} else if("jobOrderAssignmentQuantity".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderAssignmentQuantity();
		} else if("jobOrderAssignmentUnitCost".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderAssignmentUnitCost();
		} else if("jobOrderAssignmentAmount".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderAssignmentAmount();
		} else if("jobOrderAssignmentRemarks".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderAssignmentRemarks();
		} else if("jobOrderStatus".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderStatus();
		} else if("referenceNumber".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getReferenceNumber();
		} else if("documentNumber".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getDocumentNumber();
		} else if("transactionType".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getTransactionType();
		} else if("description".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getDescription();
		} else if("quantity".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getQuantity();
		} else if("unit".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getUnit();
		} else if("itemName".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemName();
		} else if("itemLocation".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemLocation();
		} else if("unitPrice".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getUnitPrice();
		} else if("amount".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAmount();
		} else if("customerCode".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerCode();
		} else if("customerName".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerName();
		} else if("customerAddress".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerAddress();
		} else if("customerCreditLimit".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerCreditLimit();
		} else if("customerContactPerson".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerContactPerson();
		} else if("customerPhoneNumber".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerPhoneNumber();
		} else if("customerMobileNumber".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerMobileNumber();
		} else if("customerFax".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerFax();
		} else if("customerEmail".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerEmail();
		} else if("itemDescription".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemDescription();
		} else if("itemPropertyCode".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemPropertyCode();
		} else if("itemSerialNumber".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemSerialNumber();
		} else if("itemSpecs".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemSpecs();
		} else if("itemCustodian".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemCustodian();
		} else if("itemExpiryDate".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemExpiryDate();
		} else if("currencySymbol".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCurrencySymbol();
		} else if("customerBalance".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerBalance();
		} else if("pendingOrder".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getPendingOrder();
		} else if("jobOrderAmount".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getJobOrderAmount();
		} else if("stockStatus".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getStockStatus();
		} else if("lastReceiptAmount".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getLastReceiptAmount();
		} else if("lastReceiptDate".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getLastReceiptDate();
		} else if("agingBucket0".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket0();
		} else if("agingBucket1".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket1();
		} else if("agingBucket2".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket2();
		} else if("agingBucket3".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket3();
		} else if("agingBucket4".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket4();
		} else if("agingBucket5".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getAgingBucket5();
		} else if("paymentTerm".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getPaymentTerm();
		} else if("salespersonCode".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getSalespersonCode();
		} else if("salespersonName".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getSalespersonName();
		} else if("customerCity".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getCustomerCity();
		} else if("itemTotalDiscount".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemTotalDiscount();
		} else if("itemDiscount1".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemDiscount1();
		} else if("itemDiscount2".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemDiscount2();
		} else if("itemDiscount3".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemDiscount3();
		} else if("itemDiscount4".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getItemDiscount4();
		} else if("soAdvanceAmount".equals(fieldName)) {
		    value = ((ArRepJobOrderPrintData)data.get(index)).getSoAdvanceAmount();
		} else if("taxAmount".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getTaxAmount();
		} else if("taxRate".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getTaxRate();
		} else if("unitPriceWoTax".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getUnitPriceWoTax();
		} else if("taxType".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getTaxType();
		} else if("quantityOnHand".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getQuantityOnHand();
		} else if("itemCategory".equals(fieldName)) {
			value = ((ArRepJobOrderPrintData)data.get(index)).getItemCategory();
		}else if("dealPrice".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getDealPrice();
		}else if("memo".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getMemo();
		}else if("billTo".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getBillTo();
		}else if("shipTo".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getShipTo();
		}else if("technician".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getTechnician();
		}else if("taxCode".equals(fieldName)){
			value = ((ArRepJobOrderPrintData)data.get(index)).getTaxType();
		}

		return(value);

	}

}
