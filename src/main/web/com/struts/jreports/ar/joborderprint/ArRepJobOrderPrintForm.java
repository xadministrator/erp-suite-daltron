package com.struts.jreports.ar.joborderprint;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ArRepJobOrderPrintForm extends ActionForm implements Serializable {
	
	private String userPermission = new String();
	private Integer jobOrderCode = null;
	
	public String getUserPermission() {
		
		return userPermission;
		
	} 
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}   
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
	}       
}
