package com.struts.jreports.ar.jobstatusreport;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepJobStatusReportDetails;


public class ArRepJobStatusReportDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepJobStatusReportDS(ArrayList list, String groupBy) {
   	
   	  Iterator i = list.iterator();
   	
	  int ctr=0;
	  
      while (i.hasNext()) {
      	
    	
    	  ArRepJobStatusReportDetails details = (ArRepJobStatusReportDetails)i.next();
         
         String group = null;
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getJoCstCustomerCode();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
         	
         	group = details.getJoCstCustomerType();
         	
         } else if (groupBy.equals("CUSTOMER CLASS")) {
         	
         	group = details.getJoCstCustomerClass();
         	
         }else if (groupBy.equals("JO NUMBER")) {
       	
       	   group = details.getJoDocumentNumber();
       	
         }
                
   
       	
         
         ArRepJobStatusReportData argData = new ArRepJobStatusReportData(details.getJoDate(), details.getJoType(),
        	     details.getJoCstCustomerCode(), details.getJoDescription(),
        	     details.getJoDocumentNumber(), details.getJoReferenceNumber(), 
        	     new Double(details.getJoOrderQty()), new Double(details.getJoInvoiceQty()),
        	     new Double(details.getJoAmount()), new Double(details.getJoTaxAmount()), group, details.getJoSlsSalespersonCode(), details.getJoSlsName(),
        		 details.getJoCstName(), details.getJoCstCustomerCode2(), details.getJoJobOrderStatus(), details.getJoOrderStatus(),
        		 details.getJolIIName(), details.getJolDescription(), details.getJolQuantity(), details.getJolUnitPrice(), details.getJolAmount(), details.getJolUnit(),
        		 details.getJaPersonelCode(),  details.getJaPersonelName(), details.getJaUnitCost(), details.getJaQuantity(), details.getJaAmount(), details.getJaRemarks(),
        		 
        		 details.getJoApprovalStatus(), details.getJoApprovedRejectedBy(), details.getJoInvoiceNumbers());
        		    
	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("jobOrderDate".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getJobOrderDate();       
      }else if("jobOrderType".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getJobOrderType();
      }else if("customerCode".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getCustomerCode();
      }else if("customerName".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getCustomerName();
      }else if("description".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getDescription();   
      }else if("documentNumber".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getDocumentNumber();   
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getReferenceNumber();
      }else if("orderQty".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getOrderQty();
      }else if("invoiceQty".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getInvoiceQty();
      }else if("amount".equals(fieldName)){
         value = ((ArRepJobStatusReportData)data.get(index)).getAmount();
      }else if("taxAmount".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getTaxAmount();
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getGroupBy();
      }else if("salespersonCode".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getSalespersonCode();
      }else if("salespersonName".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getSalespersonName();
      }else if("customerName".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getCustomerName();
      }else if("customerCode2".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getCustomerCode2();   
     	 
        
      }else if("jolIiName".equals(fieldName)){
        value =((ArRepJobStatusReportData)data.get(index)).getJolIiName();                                                                                            
      }else if("jolIiDescription".equals(fieldName)){
        value =((ArRepJobStatusReportData)data.get(index)).getJolIiDesc();          
      }else if("jolQuantity".equals(fieldName)){
          value =((ArRepJobStatusReportData)data.get(index)).getJolQuantity();                                                                                            
      }else if("jolUnitPrice".equals(fieldName)){
          value =((ArRepJobStatusReportData)data.get(index)).getJolUnitPrice();
      }else if("jolAmount".equals(fieldName)){
          value =((ArRepJobStatusReportData)data.get(index)).getJolAmount();     
      }else if("jolUnit".equals(fieldName)){
          value =((ArRepJobStatusReportData)data.get(index)).getJolUnit();
          
      // details.getJaPersonelCode(),  details.getJaPersonelName(), details.getJaUnitCost(), details.getJaQuantity(), details.getJaAmount(), details.getJaRemarks(),
 		 
      }else if("personelCode".equals(fieldName)){
          value =((ArRepJobStatusReportData)data.get(index)).getJaPersonelCode();                                                                                            
      }else if("personelName".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJaPersonelName();          
      }else if("personelQuantity".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJaQuantity();                                                                                            
      }else if("personelUnitCost".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJaUnitCost();
      }else if("personelAmount".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJaAmount();     
      }else if("personelRemarks".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJaRemarks();
      }else if("jobOrderStatus".equals(fieldName)){
    	  value =((ArRepJobStatusReportData)data.get(index)).getJobOrderStatus();
          
  
          
      }else if("orderStatus".equals(fieldName)){
        value = ((ArRepJobStatusReportData)data.get(index)).getOrderStatus();
      }else if("approvalStatus".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getApprovalStatus();
      }else if("approvedRejectedBy".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getApprovedRejectedBy();
      }else if("invoiceNumbers".equals(fieldName)){
          value = ((ArRepJobStatusReportData)data.get(index)).getInvoiceNumbers();
      }

      return(value);
   }
}
