package com.struts.jreports.ar.jobstatusreport;


public class ArRepJobStatusReportData implements java.io.Serializable {

   private java.util.Date jobOrderDate = null;
   private String jobOrderType = null;
   private String customerCode = null;

   private String description = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private Double orderQty = null;
   private Double invoiceQty = null;
   private Double amount = null;
   private Double taxAmount = null;
   private String groupBy = null;
   private String salespersonCode = null;
   private String salespersonName = null;
   private String customerName = null;
   private String customerCode2 = null;
   private String jobOrderStatus = null;
   
   private String jolIiName = null;
   private String jolIiDesc = null;
   private Double jolQuantity = null;
   private Double jolUnitPrice= null;
   private Double jolAmount= null;
   private String jolUnit= null;

   private String jaPersonelCode = null;
	private String jaPersonelName = null;
	private Double jaUnitCost = null;
	private Double jaQuantity = null;
	private Double jaAmount = null;
	private String jaRemarks = null;
   
   
   
   private String orderStatus = null;
   private String approvalStatus = null;
   private String approvedRejectedBy = null;
   private String invoiceNumbers = null;
   
   
   
   
   
   public ArRepJobStatusReportData(java.util.Date jobOrderDate, String jobOrderType, String customerCode, 
   String description, String documentNumber, String referenceNumber,
   Double orderQty, Double invoiceQty,
   Double amount, Double taxAmount, String groupBy, String salespersonCode, 
   String salespersonName, String customerName, String customerCode2, String jobOrderStatus, String orderStatus, 
   String jolIiName, String jolIiDesc, Double jolQuantity, Double jolUnitPrice, Double jolAmount, String jolUnit,
   String jaPersonelCode, String jaPersonelName, Double jaUnitCost, Double jaQuantity, Double jaAmount, String jaRemarks,
   String approvalStatus, String approvedRejectedBy, String invoiceNumbers) {
      	
      	this.jobOrderDate = jobOrderDate;  
      	this.jobOrderType = jobOrderType;
      	this.customerCode = customerCode;

      	this.description = description;
      	this.documentNumber = documentNumber;
      	this.referenceNumber = referenceNumber;
      	this.orderQty = orderQty;
      	this.invoiceQty = invoiceQty;
      	this.amount = amount;
      	this.taxAmount = taxAmount;
      	this.groupBy = groupBy;
      	this.salespersonCode = salespersonCode;
      	this.salespersonName = salespersonName;
      	this.customerName = customerName;
      	this.customerCode2 = customerCode2;
      	this.jobOrderStatus = jobOrderStatus;
      	this.jolIiName = jolIiName;
      	this.jolIiDesc = jolIiDesc;
      	this.jolQuantity = jolQuantity;
      	this.jolUnitPrice = jolUnitPrice;
      	this.jolAmount = jolAmount;
      	this.jolUnit = jolUnit;
      	
      	this.jaPersonelCode = jaPersonelCode;
      	this.jaPersonelName = jaPersonelName;
      	this.jaUnitCost = jaUnitCost;
      	this.jaQuantity = jaQuantity;
      	this.jaAmount = jaAmount;
      	this.jaRemarks = jaRemarks;
      	
      	this.orderStatus = orderStatus;
      	this.approvalStatus = approvalStatus;
      	this.approvedRejectedBy = approvedRejectedBy;
      	this.invoiceNumbers = invoiceNumbers;

   }

   public java.util.Date getJobOrderDate() {
   	
      return jobOrderDate;
      
   }

   public String getJobOrderType() {
	   	
	   	  return jobOrderType;
	   	  
   }
   
   
   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   


   
   public String getJaPersonelCode() {
	return jaPersonelCode;
}

public String getJaPersonelName() {
	return jaPersonelName;
}

public Double getJaQuantity() {
	return jaQuantity;
}

public Double getJaAmount() {
	return jaAmount;
}

public String getJaRemarks() {
	return jaRemarks;
}

public void setJaPersonelCode(String jaPersonelCode) {
	this.jaPersonelCode = jaPersonelCode;
}

public void setJaPersonelName(String jaPersonelName) {
	this.jaPersonelName = jaPersonelName;
}

public void setJaUnitCost(Double jaUnitCost) {
	this.jaUnitCost = jaUnitCost;
}

public void setJaQuantity(Double jaQuantity) {
	this.jaQuantity = jaQuantity;
}

public void setJaAmount(Double jaAmount) {
	this.jaAmount = jaAmount;
}

public void setJaRemarks(String jaRemarks) {
	this.jaRemarks = jaRemarks;
}

public String getDescription() {
   	
   	  return description;
   	  
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;

   }
   
   public Double getOrderQty() {
	   	
   	  return orderQty;
   	  
   }
   
   public Double getInvoiceQty() {
	   	
   	  return invoiceQty;
   	  
   }
   
      
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public Double getTaxAmount() {
	   	
   	  return taxAmount;
   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getSalespersonCode() {
   	
   	  return salespersonCode;
   	
   }
   
   public String getSalespersonName() {
   	
   	  return salespersonName;
   	
   }
   
   public String getCustomerName() {
   	
   	  return customerName;
   	
   }

   public String getCustomerCode2() {
   	
   	  return customerCode2;
   	  
   }
   
   
   public String getJobOrderStatus() {
	   	
   	  return jobOrderStatus;
	   	  
   }
	   
   public String getOrderStatus() {
	   	
	   	  return orderStatus;
	   	  
   }
   public String getJolIiName() {
	   	
	  return jolIiName;
	   	  
   }
   
   public String getApprovalStatus() {
	   
	   return approvalStatus;
	   
   }
   
   public String getApprovedRejectedBy() {
	   
	   return approvedRejectedBy;
	   
   }
   
   public String getInvoiceNumbers() {
	   
	   return invoiceNumbers;
	   
   }

public String getJolIiDesc() {
	return jolIiDesc;
}

public Double getJolQuantity() {
	return jolQuantity;
}

public Double getJolUnitPrice() {
	return jolUnitPrice;
}

public Double getJolAmount() {
	return jolAmount;
}

public String getJolUnit() {
	return jolUnit;
}

public Double getJaUnitCost() {
	return jaUnitCost;
}
   
   
   
   
}
