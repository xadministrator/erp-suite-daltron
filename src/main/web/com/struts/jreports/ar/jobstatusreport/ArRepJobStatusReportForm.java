package com.struts.jreports.ar.jobstatusreport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepJobStatusReportForm extends ActionForm implements Serializable{
	
	private String jobOrderType = null;
	private ArrayList jobOrderTypeList = new ArrayList();
	private String customerCode = null;
	private String customerBatch = null;
	private ArrayList customerBatchList = new ArrayList();
	private String customerType = null;
	private ArrayList customerTypeList = new ArrayList();
	private String customerClass = null;
	private ArrayList customerClassList = new ArrayList();
	private String personel = null;
	private ArrayList personelList = new ArrayList();
	private String personelType = null;
	private ArrayList personelTypeList = new ArrayList();
	private String jobOrderStatus = null;
	private ArrayList jobOrderStatusList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumberFrom = null;
	private String referenceNumberTo = null;
	private String posted = null;
	private ArrayList postedList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private String isCustomerEntered = null;
	private boolean includeUnposted = false;

	private ArrayList orderStatusList = new ArrayList();
	private ArrayList invoiceStatusList = new ArrayList();
	private String orderStatus = null;
	private String invoiceStatus = null;
	

	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getCustomerCode(){
		return(customerCode);
	}
	
	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}
	
	public String getDateFrom(){
		return(dateFrom);
	}
	
	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}
	
	public String getDateTo(){
		return(dateTo);
	}
	
	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}
	
	public String getDocumentNumberFrom(){
		return(documentNumberFrom);
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom){
		this.documentNumberFrom = documentNumberFrom;
	}
	
	public String getDocumentNumberTo(){
		return(documentNumberTo);
	}
	
	public void setDocumentNumberTo(String documentNumberTo){
		this.documentNumberTo = documentNumberTo;
	}
	
	public String getReferenceNumberFrom(){
		return(referenceNumberFrom);
	}
	
	public void setReferenceNumberFrom(String referenceNumberFrom){
		this.referenceNumberFrom = referenceNumberFrom;
	}
	
	public String getReferenceNumberTo(){
		return(referenceNumberTo);
	}
	
	public void setReferenceNumberTo(String referenceNumberTo){
		this.referenceNumberTo = referenceNumberTo;
	}
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}
	
	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}
	
	public String getIsCustomerEntered() {
		return isCustomerEntered;
	}
	
	public void setIsCustomerEntered(String isCustomerEntered) {
		this.isCustomerEntered = isCustomerEntered;
	}
	
	public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   public String getJobOrderType() {
		   return jobOrderType;
	   }
	   
	   public void setJobOrderType(String jobOrderType) {
		   this.jobOrderType  = jobOrderType;
	   }
	   
	   
	   public ArrayList getJobOrderTypeList() {
		   	
	      return jobOrderTypeList;
	      
	   }

	   public void setJobOrderTypeList(String jobOrderType) {
	   	
		   jobOrderTypeList.add(jobOrderType);
	      
	   }
	   
	   public void clearJobOrderTypeList() {
	   	
		   jobOrderTypeList.clear();
		   jobOrderTypeList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   public String getPersonel() {
		   return personel;
	   }
   
	   public void setPersonel(String personel) {
		   this.personel = personel;
	   }
	   
	   public String getPersonelType() {
		   return personelType;
	   }
   
	   public void setPersonelType(String personelType) {
		   this.personelType = personelType;
	   }
	   
	   public String getJobOrderStatus() {
		   return jobOrderStatus;
	   }
   
	   public void setJobOrderStatus(String jobOrderStatus) {
		   this.jobOrderStatus = jobOrderStatus;
	   }
	   
	   
	   
	   public ArrayList getPersonelList() {
		   	
	      return personelList;
	      
	   }

	   public void setPersonelList(String personel) {
	   	
		   personelList.add(personel);
	      
	   }
	   
	   public void clearPersonelList() {
	   	
		   personelList.clear();
		   personelList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   public ArrayList getPersonelTypeList() {
		   	
	      return personelTypeList;
	      
	   }

	   public void setPersonelTypeList(String personelType) {
	   	
		   personelTypeList.add(personelType);
	      
	   }
	   
	   public void clearPersonelTypeList() {
	   	
		   personelTypeList.clear();
		   personelTypeList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   
	   public ArrayList getJobOrderStatusList() {
		   	
	      return jobOrderStatusList;
	      
	   }

	   public void setJobOrderStatusList(String jobOrderStatus) {
	   	
		   jobOrderStatusList.add(jobOrderStatus);
	      
	   }
	   
	   public void clearJobOrderStatusList() {
	   	
		   jobOrderStatusList.clear();
		   jobOrderStatusList.add(Constants.GLOBAL_BLANK);
	      
	   }
	
	public String getCustomerType(){
		return(customerType);
	}
	
	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}
	
	public ArrayList getCustomerTypeList(){
		return(customerTypeList);
	}
	
	public void setCustomerTypeList(String customerType){
		customerTypeList.add(customerType);
	}
	
	public void clearCustomerTypeList(){
		customerTypeList.clear();
		customerTypeList.add(Constants.GLOBAL_BLANK);
	}
	
	public String getCustomerClass(){
		return(customerClass);
	}
	
	public void setCustomerClass(String customerClass){
		this.customerClass = customerClass;
	}
	
	public ArrayList getCustomerClassList(){
		return(customerClassList);
	}
	
	public void setCustomerClassList(String customerClass){
		customerClassList.add(customerClass);
	}
	
	public void clearCustomerClassList(){
		customerClassList.clear();
		customerClassList.add(Constants.GLOBAL_BLANK);
	}
	

	public String getOrderBy(){
		return(orderBy);   	
	}
	
	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}
	
	public ArrayList getOrderByList(){
		return orderByList;
	}
	
	public String getGroupBy(){
		return(groupBy);   	
	}
	
	public void setGroupBy(String groupBy){
		this.groupBy = groupBy;
	}
	
	public ArrayList getGroupByList(){
		return groupByList;
	}
	
	public String getApprovalStatus(){
		return(approvalStatus);   	
	}
	
	public void setApprovalStatus(String approvalStatus){
		this.approvalStatus = approvalStatus;
	}
	
	public ArrayList getApprovalStatusList(){
		return approvalStatusList;
	}
	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	
	public ArrayList getOrderStatusList(){
		return(orderStatusList);   	
	}
	
	public ArrayList getInvoiceStatusList(){
		return(invoiceStatusList);   	
	}
	
	public String getOrderStatus(){
		return(orderStatus);   	
	}
	
	public void setOrderStatus(String orderStatus){
		this.orderStatus = orderStatus;
	}
	
	public String getInvoiceStatus(){
		return(invoiceStatus);   	
	}
	
	public void setInvoiceStatus(String invoiceStatus){
		this.invoiceStatus = invoiceStatus;
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
	public boolean getIncludeUnposted() {
		
		return includeUnposted;
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}

	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}
	

	
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
		
	
		
		goButton = null;
		closeButton = null;
		customerCode = null;
		customerClass = Constants.GLOBAL_BLANK; 
		customerType = Constants.GLOBAL_BLANK;
		personel = Constants.GLOBAL_BLANK;
		jobOrderType = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = null;
		documentNumberFrom = null;
		documentNumberTo = null;
		referenceNumberFrom = null;
		referenceNumberTo = null;
		isCustomerEntered = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		postedList.clear();
		postedList.add(Constants.GLOBAL_BLANK);
		postedList.add(Constants.GLOBAL_YES);
		postedList.add(Constants.GLOBAL_NO);
		posted = Constants.GLOBAL_NO;
		orderByList.clear();
		orderByList.add("DATE");
		orderByList.add("CUSTOMER CODE");
		orderByList.add("CUSTOMER TYPE");
		orderByList.add("DOCUMENT NUMBER");
		orderByList.add("REFERENCE NUMBER");
		orderBy = "DATE";   
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
		
		groupByList.add(Constants.AR_CL_ORDER_BY_JO_NUMBER);
		groupBy = Constants.GLOBAL_BLANK;
		approvalStatusList.clear();
		approvalStatusList.add(Constants.GLOBAL_BLANK);
		approvalStatusList.add("N/A");
		approvalStatusList.add("PENDING");
		approvalStatusList.add("APPROVED");
		approvalStatus = Constants.GLOBAL_BLANK;
		includeUnposted = false;
		orderStatusList.clear();
		orderStatusList.add("");
		orderStatusList.add("Good");
		orderStatusList.add("Bad");
		orderStatus = "";
		invoiceStatusList.clear();
		invoiceStatusList.add("");
		invoiceStatusList.add("SERVED");
		invoiceStatusList.add("UNSERVED");
		invoiceStatus = "";
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)){
				errors.add("dateFrom", new ActionMessage("salesOrder.error.dateFromInvalid"));
			}
			if(!Common.validateDateFormat(dateTo)){
				errors.add("dateTo", new ActionMessage("salesOrder.error.dateToInvalid"));
			}
			if(!Common.validateStringExists(customerTypeList, customerType)){
				errors.add("customerType", new ActionMessage("salesOrder.error.customerTypeInvalid"));
			}
			if(!Common.validateStringExists(customerClassList, customerClass)){
				errors.add("customerClass", new ActionMessage("salesOrder.error.customerClassInvalid"));
			}	 
		}
		return(errors);
	}
	
}
