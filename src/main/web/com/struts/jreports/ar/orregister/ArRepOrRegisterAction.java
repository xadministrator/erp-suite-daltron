package com.struts.jreports.ar.orregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepOrRegisterController;
import com.ejb.txn.ArRepOrRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepOrRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepOrRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepOrRegisterForm actionForm = (ArRepOrRegisterForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_OR_REGISTER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepOrRegister");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepOrRegisterController EJB
*******************************************************/

         ArRepOrRegisterControllerHome homeORR = null;
         ArRepOrRegisterController ejbORR = null;       

         try {
         	
            homeORR = (ArRepOrRegisterControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepOrRegisterControllerEJB", ArRepOrRegisterControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepOrRegisterAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbORR = homeORR.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepOrRegisterAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR SR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerType())) {
	        		
	        		criteria.put("customerType", actionForm.getCustomerType());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	           }

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptBatchName())) {
	        		
	        		criteria.put("receiptBatchName", actionForm.getReceiptBatchName());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptType())) {
	        		
	        		criteria.put("receiptType", actionForm.getReceiptType());
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberFrom())) {
	        		
	        		criteria.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberTo())) {
	        		
	        		criteria.put("receiptNumberTo", actionForm.getReceiptNumberTo());
	        		
	        	}
	        	
	        	if (actionForm.getunpostedOnly()) {	        	

	        		criteria.put("unpostedOnly", "YES");

	        	}

	        	if (!actionForm.getunpostedOnly()) {

	        		criteria.put("unpostedOnly", "NO");

	        	}
	        	
	        	if (actionForm.getIncludedUnposted()) {	        	
		        	   		        		
	        		criteria.put("includedUnposted", "YES");
                
                }
                
                if (!actionForm.getIncludedUnposted()) {
                	
                	criteria.put("includedUnposted", "NO");
                	
                }
                
                if (actionForm.getIncludedMiscReceipts()) {	        	
                	
                	criteria.put("includedMiscReceipts", "YES");
                	
                }
                
                if (!actionForm.getIncludedMiscReceipts()) {
                	
                	criteria.put("includedMiscReceipts", "NO");
                	
                }
                
                if (actionForm.getdetailedReport()) {	        	
                	
                	criteria.put("detailedReport", "YES");
                	
                }
                
                if (!actionForm.getdetailedReport()) {
                	
                	criteria.put("detailedReport", "NO");
                	
                }
                	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbORR.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrOrListSize(); i++) {
	           	
	           		ArRepBranchOrRegisterList brOrList = (ArRepBranchOrRegisterList)actionForm.getArRepBrOrListByIndex(i);
	           	
	           		if(brOrList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brOrList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbORR.executeArRepOrRegister(actionForm.getCriteria(), branchList,
            	    actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getdetailedReport(), actionForm.getShowEntries(), actionForm.getSummarize(), 
					actionForm.getIncludedPr(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("orRegister.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepOrRegisterAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepOrRegister");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
            Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getReceiptBatchName() != null) {
				
				parameters.put("receiptBatchName", actionForm.getReceiptBatchName());
				
			}
			
			if (actionForm.getBankAccount() != null) {
			
				parameters.put("bankAccount", actionForm.getBankAccount());
				
			}
			
			if (actionForm.getCustomerType() != null) {
			
				parameters.put("customerType", actionForm.getCustomerType());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", actionForm.getDateFrom());				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", actionForm.getDateTo());	    	
			
		    }
		    
		    if (actionForm.getReceiptType() != null) {
		    	
		    	parameters.put("receiptType", actionForm.getReceiptType());
		    	
		    }
	 
			parameters.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
		    parameters.put("receiptNumberTo", actionForm.getReceiptNumberTo());
		    
		    if (actionForm.getunpostedOnly()) {

		    	parameters.put("unpostedOnly", "YES");

		    } else {

		    	parameters.put("unpostedOnly", "NO");
		    	
		    }

		    if (actionForm.getIncludedUnposted() && !actionForm.getunpostedOnly()) {
		    		
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }
		    
		    if(actionForm.getIncludedPr()) {
		    	
		    	parameters.put("includedPr", "YES"); 
		    	
		    } else {
		    	
		    	parameters.put("includedPr", "NO");
		    	
		    }
		    
		    
		    parameters.put("groupBy", actionForm.getGroupBy());
		    parameters.put("showEntries", actionForm.getShowEntries() == true ? "YES" : "NO");
		    if (actionForm.getIncludedMiscReceipts()) {
		    	
		    	parameters.put("includedMiscReceipt", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedMiscReceipt", "NO");
		    	
		    }
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepBrOrListSize(); j++) {

		    	ArRepBranchOrRegisterList brList = (ArRepBranchOrRegisterList)actionForm.getArRepBrOrListByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBranchName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBranchName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = null;
		    
		    if(actionForm.getShowEntries()) {
		    	
		    	if (actionForm.getSummarize()) {
		    		
		    		filename = servlet.getServletContext().getRealPath("jreports/ArRepOrRegisterSummary.jasper");
		    		
		    	} else {
		    		
		    		filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepOrRegisterEntries.jasper";
				       
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/ArRepOrRegisterEntries.jasper");
				    
			        }
			        
		    	}
		    	
		    } else {
		    	
		    	if (!actionForm.getIncludedPr()) {
		    	
			    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepOrRegister.jasper";
				       
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/ArRepOrRegister.jasper");
				    
			        }
			        else if (actionForm.getdetailedReport()) {
			        	
			        	filename = "/opt/ofs-resources/" + user.getCompany() + "/xArRepOrRegister.jasper";
					       
				        if (!new java.io.File(filename).exists()) {
				       		    		    
				           filename = servlet.getServletContext().getRealPath("jreports/ArRepOrRegister.jasper");
					    
				        }	
			        }
		    	} else {
		    		filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepOrRegisterPr.jasper";
				       
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/ArRepOrRegisterPr.jasper");
				    
			        }
		    	}
		    	
		    }
		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepOrRegisterDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepOrRegisterDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepOrRegisterDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepOrRegisterAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AR SR Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AR SR Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.reset(mapping, request);
			    	
			        ArrayList list = null;			       			       
			        Iterator i = null;

	            	actionForm.clearReceiptBatchNameList();

	            	list = ejbORR.getArRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setReceiptBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setReceiptBatchNameList((String)i.next());
	            			
	            		}
	            		
	            	}
		    	
	         		actionForm.reset(mapping, request);
		            			        
			        actionForm.clearBankAccountList();           	
	            	
	            	list = ejbORR.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
	            	actionForm.clearCustomerBatchList();           	
	         		
	         		list = ejbORR.getAdLvCustomerBatchAll(user.getCmpCode());
	         		
	         		if (list == null || list.size() == 0) {
	         			
	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
	         			
	         		} else {
	         			
	         			i = list.iterator();
	         			
	         			while (i.hasNext()) {
	         				
	         				actionForm.setCustomerBatchList((String)i.next());
	         				
	         			}
	         			
	         		}
			       
	            	actionForm.clearCustomerTypeList();           	
	            	
	            	list = ejbORR.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbORR.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
	            	actionForm.clearArRepBrOrList();
	         		
	         		list = ejbORR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchOrRegisterList arRepBrOrList = new ArRepBranchOrRegisterList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			
	         			arRepBrOrList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrOrList(arRepBrOrList);
	         			
	         		}
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepOrRegisterAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("arRepOrRegister"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepOrRegisterAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
