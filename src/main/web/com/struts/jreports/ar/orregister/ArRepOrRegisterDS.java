package com.struts.jreports.ar.orregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepOrRegisterDetails;

public class ArRepOrRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepOrRegisterDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepOrRegisterDetails details = (ArRepOrRegisterDetails)i.next();
         
    
         String group = null;
         String groupCoa = null;
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getOrrCstCustomerCode() + "-" + details.getOrrCstName();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
         	
         	group = details.getOrrCstCustomerType();
         	
         } else if (groupBy.equals("CUSTOMER CLASS")) {
         	
         	group = details.getOrrCstCustomerClass();
         	
         } else if (groupBy.equals("SALESPERSON")) {
          	
          	group = details.getOrrSlsSalespersonCode();
          	
          }
         
         groupCoa = details.getOrrDrCoaAccountNumber();
                  
	     ArRepOrRegisterData argData = new ArRepOrRegisterData(
	    		 details.getOrrDate(), 
	    		 details.getOrrCstCustomerCode(), 
	    		 details.getOrrDescription(), 
			     details.getOrrReceiptNumber(), 
			     details.getOrrBatchName(),
			     details.getOrrPaymentMethod(),
			     details.getOrrReferenceNumber(),
				 new Double(details.getOrrAmount()), 
				 group, 
				 details.getOrrCstName(), 
				 details.getOrrDrCoaAccountNumber(), 
				 details.getOrrDrCoaAccountDescription(), 
				 new Double(details.getOrrDrDebitAmount()), 
				 new Double(details.getOrrDrCreditAmount()),
				 details.getOrrSlsSalespersonCode(), 
				 details.getOrrSlsName(), 
				 new Double(details.getOrrReceiptNetAmount()),
				 new Double(details.getOrrReceiptTaxAmount()), 
				 new Double(details.getOrrTotalVat()),
				 groupCoa, 
				 details.getOrrReceiptAppliedInvoices(),
				 details.getOrrPrDate(),
				 details.getOrrPrNumber(),
				 details.getOrrCheckDate(),
				 details.getOrrCheckNumber(),
				 details.getOrrInvoiceNumber(),
				 details.getOrrPosted(),
				 details.getOrrBankAccount(),
				 details.getOrrInvoiceReferenceNumber());
				    
	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("receiptDate".equals(fieldName)){
         value = ((ArRepOrRegisterData)data.get(index)).getReceiptDate();
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getCustomerCode();
      }else if("description".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getDescription();     
      }else if("receiptNumber".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getReceiptNumber();     
      }else if("batchName".equals(fieldName)){
          value = ((ArRepOrRegisterData)data.get(index)).getBatchName();
      }else if("paymentMethod".equals(fieldName)){
          value = ((ArRepOrRegisterData)data.get(index)).getPaymentMethod();
      }else if("referenceNumber".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getReferenceNumber();           
      }else if("amount".equals(fieldName)){
         value = ((ArRepOrRegisterData)data.get(index)).getAmount();     
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getGroupBy();
      }else if("customerName".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getCustomerName();
      }else if("accountNumber".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getAccountDescription();
      }else if("debitAmount".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getDebitAmount();
      }else if("creditAmount".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getCreditAmount();
      }else if("salespersonCode".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getSalespersonCode();
      }else if("salespersonName".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getSalespersonName();
      }else if("netAmount".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getNetAmount();
      }else if("taxAmount".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getTaxAmount();
      }else if("totalVat".equals(fieldName)){
    	value = ((ArRepOrRegisterData)data.get(index)).getTotalVat();
      }else if("groupCoa".equals(fieldName)){
        value = ((ArRepOrRegisterData)data.get(index)).getGroupByCoa();
      }else if("appliedInvoiceNumbers".equals(fieldName)){
          value = ((ArRepOrRegisterData)data.get(index)).getAppliedInvoiceNumbers();
      }else if("prDate".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getPrDate();
      }else if("prNumber".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getPrNumber();
      }else if("checkDate".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getCheckDate();
      }else if("checkNumber".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getCheckNumber();
      }else if("invoiceNumber".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getInvoiceNumber();
      }else if("posted".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getPosted();
      }else if("bankAccount".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getBankAccount();
      }else if("invoiceReferenceNumber".equals(fieldName)){
    	  value = ((ArRepOrRegisterData)data.get(index)).getInvoiceReferenceNumber();
      }
       
      return(value);
   }
}
