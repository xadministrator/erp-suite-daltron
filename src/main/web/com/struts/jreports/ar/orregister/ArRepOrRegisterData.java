package com.struts.jreports.ar.orregister;


public class ArRepOrRegisterData implements java.io.Serializable {

   private java.util.Date receiptDate = null;
   private String customerCode = null;
   private String description = null;
   private String receiptNumber = null;
   private String batchName = null;
   private String paymentMethod = null;
   private String referenceNumber = null;
   private Double amount = null;
   private String groupBy = null;
   private String customerName = null;
   private String accountNumber = null;
   private String accountDescription = null;
   private Double debitAmount = null;
   private Double creditAmount = null;
   private String salespersonCode = null;
   private String salespersonName = null;
   private Double netAmount = null;
   private Double taxAmount = null;
   private Double totalVat = null;
   private String groupByCoa = null;
   private String appliedInvoiceNumbers = null;
   private java.util.Date prDate = null;
   private String prNumber = null;
   private java.util.Date checkDate = null;
   private String checkNumber = null;
   private String invoiceNumber = null;
   private String posted = null;
   private String bankAccount = null;
   private String invoiceReferenceNumber = null;
   
   public ArRepOrRegisterData(java.util.Date receiptDate, String customerCode,
   String description, String receiptNumber, String batchName, String paymentMethod,  String referenceNumber, 
   Double amount, String groupBy, String customerName, String accountNumber, 
   String accountDescription, Double debitAmount, Double creditAmount,
   String salespersonCode, String salespersonName, Double netAmount, 
   Double taxAmount, Double totalVat, String groupByCoa, String appliedInvoiceNumbers,
   java.util.Date prDate, String prNumber, java.util.Date checkDate, 
   String checkNumber, String invoiceNumber, String posted, String bankAccount, String invoiceReferenceNumber) {
      	
      	this.receiptDate = receiptDate;
      	this.customerCode = customerCode;
      	this.description = description;
      	this.receiptNumber = receiptNumber;
      	this.batchName = batchName;
      	this.paymentMethod = paymentMethod;
      	this.referenceNumber = referenceNumber;
      	this.amount = amount;
      	this.groupBy = groupBy;
      	this.customerName = customerName;
      	this.accountNumber = accountNumber;
      	this.accountDescription = accountDescription;
      	this.debitAmount = debitAmount;
      	this.creditAmount = creditAmount;
      	this.salespersonCode = salespersonCode;
      	this.salespersonName = salespersonName;
      	this.netAmount = netAmount;
      	this.taxAmount = taxAmount;
      	this.totalVat = totalVat;
      	this.groupByCoa = groupByCoa;
      	this.appliedInvoiceNumbers = appliedInvoiceNumbers;
      	this.prDate = prDate;
      	this.prNumber = prNumber;
      	this.checkDate = checkDate;
      	this.checkNumber = checkNumber;
      	this.invoiceNumber = invoiceNumber;
      	this.posted = posted;
      	this.bankAccount = bankAccount;
      	this.invoiceReferenceNumber = invoiceReferenceNumber;
   }

   public java.util.Date getReceiptDate() {
   	
      return receiptDate;
      
   }

   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }

   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	  
   }
   
   public String getBatchName() {
	   	
   	  return batchName;
   	  
   }
   
   public String getPaymentMethod() {
	   	
   	  return paymentMethod;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
 
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getCustomerName() {
   	
   	  return customerName;
   	
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	
   }
   
   public String getAccountDescription() {
   	
   	  return accountDescription;
   	
   }
   
   public Double getDebitAmount() {
   	
   	  return debitAmount;
   	
   }
   
   public Double getCreditAmount() {
   	
   	  return creditAmount;
   	
   }
   
   public String getSalespersonCode() {
   	
   	  return salespersonCode;
   	
   }
   
   public String getSalespersonName() {
   	
   	  return salespersonName;
   	
   }
   
   public Double getNetAmount() {
   	
   	  return netAmount;
   	
   }
    
   public Double getTaxAmount() {
   	
   	  return taxAmount;
   	
   }
   
   public Double getTotalVat(){
	   return totalVat;
   }
   
   public String getGroupByCoa() {
   	
   	  return groupByCoa;
   	  
   }
   
   public String getAppliedInvoiceNumbers() {
	   
	   return appliedInvoiceNumbers;
	   
   }
   
   public java.util.Date getPrDate() {
	   
	   return prDate;
	   
   }
   
   public String getPrNumber() {
	   
	   return prNumber;
	   
   }
   
   public java.util.Date getCheckDate() {
	   
	   return checkDate;
	   
   }

   public String getCheckNumber() {
	   
	   return checkNumber;
	   
   }
   
   public String getInvoiceNumber() {
	   
	   return invoiceNumber;
	   
   }

   public String getPosted() {
	   
	   return posted;
	   
   }   
   
   public String getBankAccount() {
	   
	   return bankAccount;
	   
   }
   
   public String getInvoiceReferenceNumber() {
	   
	   return invoiceReferenceNumber;
	   
   }
   
}
