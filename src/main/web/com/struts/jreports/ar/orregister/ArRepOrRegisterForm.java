package com.struts.jreports.ar.orregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepOrRegisterForm extends ActionForm implements Serializable{

   private String customerCode = null;
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String customerBatch = null;
   private ArrayList customerBatchList = new ArrayList();
   private String customerType = null;
   private ArrayList customerTypeList = new ArrayList();
   private String customerClass = null;
   private ArrayList customerClassList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String receiptType = null;
   private ArrayList receiptTypeList = new ArrayList();
   private String receiptNumberFrom = null;
   private String receiptNumberTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private boolean includedUnposted = false;
   private boolean unpostedOnly = false;
   private boolean includedPr = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private boolean showEntries = false;
   private boolean detailedReport = false;
   
   private HashMap criteria = new HashMap();
   private ArrayList arRepBrOrList = new ArrayList();
   private boolean includedMiscReceipts = false;
	private String receiptBatchName = null;
	private ArrayList receiptBatchNameList = new ArrayList();

   private String userPermission = new String();
   
   private boolean summarize = false;

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getCustomerCode(){
   	
      return(customerCode);
      
   }

   public void setCustomerCode(String customerCode){
   	
      this.customerCode = customerCode;
      
   }

   public String getDateFrom(){
   	
      return(dateFrom);
      
   }

   public void setDateFrom(String dateFrom){
   	
      this.dateFrom = dateFrom;
      
   }

   public String getDateTo(){
   	
      return(dateTo);
      
   }

   public void setDateTo(String dateTo){
   	
      this.dateTo = dateTo;
      
   }

   public String getReceiptNumberFrom(){
   	
      return(receiptNumberFrom);
      
   }

   public void setReceiptNumberFrom(String receiptNumberFrom){
   	
      this.receiptNumberFrom = receiptNumberFrom;
      
   }

   public String getReceiptNumberTo(){
   	
      return(receiptNumberTo);
      
   }

   public void setReceiptNumberTo(String receiptNumberTo){
   	
      this.receiptNumberTo = receiptNumberTo;
      
   }

   public void setGoButton(String goButton){
   	
      this.goButton = goButton;
      
   }

   public void setCloseButton(String closeButton){
   	
      this.closeButton = closeButton;
      
   }

   public String getBankAccount(){
   	
      return(bankAccount);
      
   }

   public void setBankAccount(String bankAccount){
   	
      this.bankAccount = bankAccount;
      
   }

   public ArrayList getBankAccountList(){
   	
      return(bankAccountList);
      
   }

   public void setBankAccountList(String bankAccount){
   	
      bankAccountList.add(bankAccount);
      
   }
   
   public void clearBankAccountList(){
   	
      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }

   public String getCustomerType(){
   	
      return(customerType);
      
   }

   public void setCustomerType(String customerType){
   	
      this.customerType = customerType;
      
   }
	public String getReceiptBatchName() {

		return receiptBatchName;

	}

	public void setReceiptBatchName(String receiptBatchName) {

		this.receiptBatchName = receiptBatchName;

	}

	public ArrayList getReceiptBatchNameList() {

		return receiptBatchNameList;

	}

	public void setReceiptBatchNameList(String receiptBatchName) {

		receiptBatchNameList.add(receiptBatchName);

	}

	public void clearReceiptBatchNameList() {   	 

		receiptBatchNameList.clear();
		receiptBatchNameList.add(Constants.GLOBAL_BLANK);

	}

   public ArrayList getCustomerTypeList(){
   	
      return(customerTypeList);
      
   }

   public void setCustomerTypeList(String customerType){
   	
      customerTypeList.add(customerType);
      
   }

   public void clearCustomerTypeList(){
   	
      customerTypeList.clear();
      customerTypeList.add(Constants.GLOBAL_BLANK);
      
   }
 
   public String getCustomerClass(){
   	
      return(customerClass);
      
   }

   public void setCustomerClass(String customerClass){
   	
      this.customerClass = customerClass;
      
   }

   public ArrayList getCustomerClassList(){
   	
      return(customerClassList);
      
   }

   public void setCustomerClassList(String customerClass){
   	
      customerClassList.add(customerClass);
      
   }

   public void clearCustomerClassList(){
   	
      customerClassList.clear();
      customerClassList.add(Constants.GLOBAL_BLANK);
      
   } 

   public String getOrderBy(){
   	
   	  return(orderBy);   	
   	  
   }
   
   public void setOrderBy(String orderBy){
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList(){
   	
   	  return orderByList;
   	  
   }
   
   public String getViewType(){
   	
   	  return(viewType);   	
   	  
   }
   
   public void setViewType(String viewType){
   	
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList(){
   	
   	  return viewTypeList;
   	  
   }
   
   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public boolean getunpostedOnly() {

		return unpostedOnly;

	}

	public void setunpostedOnly(boolean unpostedOnly) {

		this.unpostedOnly = unpostedOnly;

	}
	
	public boolean getdetailedReport() {

		return detailedReport;

	}

	public void setdetailedReport(boolean detailedReport) {

		this.detailedReport = detailedReport;

	}

	
   public boolean getIncludedPr() {
	   	
   	  return includedPr;
   	
   }
   
   public void setIncludedPr(boolean includedPr) {
   	
   	  this.includedPr = includedPr;
   	
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
   	
      return(userPermission);
      
   }

   public void setUserPermission(String userPermission){
   	
      this.userPermission = userPermission;
      
   }
   
   public String getReceiptType(){
   	
   	  return(receiptType);
   	  
   }
   
   public void setReceiptType(String receiptType){
   	
   	  this.receiptType = receiptType;
   	  
   }
   
   public ArrayList getReceiptTypeList(){
   	
   	  return(receiptTypeList);
   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
   
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }
   
   public Object[] getArRepBrOrList(){
   	
   	  return arRepBrOrList.toArray();
   	
   }
   
   public ArRepBranchOrRegisterList getArRepBrOrListByIndex(int index){
   	
   	  return ((ArRepBranchOrRegisterList)arRepBrOrList.get(index));
   	
   }
   
   public int getArRepBrOrListSize(){
   	
   	  return(arRepBrOrList.size());
   	
   }
   
   public void saveArRepBrOrList(Object newArRepBrOrList){
   	
   	  arRepBrOrList.add(newArRepBrOrList);   	  
   	
   }
   
   public void clearArRepBrOrList(){
   	
   	  arRepBrOrList.clear();
   	
   }
   
   public boolean getShowEntries() {
   	
   	  return showEntries;
   	
   }
   
   public void setShowEntries(boolean showEntries) {
   	
   	  this.showEntries = showEntries;
   	
   }
   
   public boolean getIncludedMiscReceipts() {
   	
   	  return includedMiscReceipts;
   	
   }
   
   public void setIncludedMiscReceipts(boolean includedMiscReceipts) {
   	
   	  this.includedMiscReceipts = includedMiscReceipts;
   	
   }
   
   public boolean getSummarize() {
   	
   	  return summarize;
   	
   }
   
   public void setSummarize(boolean summarize) {
   	
   	  this.summarize = summarize;
   	
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){    
   	
   	for (int i=0; i<arRepBrOrList.size(); i++) {
   		
   		ArRepBranchOrRegisterList actionList = (ArRepBranchOrRegisterList)arRepBrOrList.get(i);
   		actionList.setBranchCheckbox(false);
   		
   	}
   	
   	goButton = null;
   	closeButton = null;
   	customerCode = null;
   	bankAccount = Constants.GLOBAL_BLANK;
   	customerClass = Constants.GLOBAL_BLANK; 
   	customerType = Constants.GLOBAL_BLANK;
   	dateFrom = null;
   	dateTo = null;
   	receiptTypeList.clear();
   	receiptTypeList.add(Constants.GLOBAL_BLANK);
   	receiptTypeList.add(Constants.AR_FR_RECEIPT_TYPE_COLLECTION);
   	receiptTypeList.add(Constants.AR_FR_RECEIPT_TYPE_MISC);
   	receiptType = Constants.GLOBAL_BLANK;  
   	receiptNumberFrom = null;
   	receiptNumberTo = null;
   	viewTypeList.clear();
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
   	viewType = Constants.REPORT_VIEW_TYPE_PDF;
   	orderByList.clear();
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_NONE);
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_DATE);
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_BANK_ACCOUNT);
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_CUSTOMER_CODE);
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_CUSTOMER_TYPE);
   	orderByList.add(Constants.AR_OR_REGISTER_ORDER_BY_RECEIPT_NUMBER);
   	orderBy = Constants.GLOBAL_BLANK; 
   	groupByList.clear();
   	groupByList.add(Constants.GLOBAL_BLANK);
   	groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
   	groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
   	groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
   	groupByList.add(Constants.AR_CL_ORDER_BY_SALESPERSON);
   	groupBy = Constants.GLOBAL_BLANK;
   	includedUnposted = false;
   	includedPr = false;
   	showEntries = false;
   	detailedReport = false;
   	includedMiscReceipts = false;
   	summarize = false;
   	unpostedOnly = false;
   	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("orRegister.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("orRegister.error.dateToInvalid"));
		 }
		 if(!Common.validateStringExists(customerTypeList, customerType)){
	            errors.add("customerType", new ActionMessage("orRegister.error.customerTypeInvalid"));
		 }
		 if(!Common.validateStringExists(customerClassList, customerClass)){
	            errors.add("customerClass", new ActionMessage("orRegister.error.customerClassInvalid"));
		 }
		 if(!Common.validateStringExists(bankAccountList, bankAccount)){
	            errors.add("bankAccount", new ActionMessage("orRegister.error.bankAccountInvalid"));
		 }	 
      }
      return(errors);
   }
   
}
