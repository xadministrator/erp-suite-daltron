package com.struts.jreports.ar.outputtax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepOutputTaxController;
import com.ejb.txn.ArRepOutputTaxControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepOutputTaxAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepOutputTaxAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepOutputTaxForm actionForm = (ArRepOutputTaxForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
  
         String frParam = Common.getUserPermission(user, Constants.AR_REP_OUTPUT_TAX_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepOutputTax");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepOutputTaxController EJB
*******************************************************/

         ArRepOutputTaxControllerHome homeOT = null;
         ArRepOutputTaxController ejbOT = null;       

         try {
         	
            homeOT = (ArRepOutputTaxControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepOutputTaxControllerEJB", ArRepOutputTaxControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepOutputTaxAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbOT = homeOT.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepOutputTaxAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR OT Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	}	        		        	
	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbOT.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrOtListSize(); i++) {
	           	
	           		ArRepBranchOutputTaxList brOtList = (ArRepBranchOutputTaxList)actionForm.getArRepBrOtListByIndex(i);
	           	
	           		if(brOtList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brOtList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbOT.executeArRepOutputTax(actionForm.getCriteria(), branchList, user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("outputTax.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepOutputTaxAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepOutputTax");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", new java.util.Date());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}						
		    
		    if (actionForm.getDateFrom() != null) {
	        		
	    		parameters.put("dateFrom", actionForm.getDateFrom());
	    	}
	    	
	    	if (actionForm.getDateTo() != null) {
	    		
	    		parameters.put("dateTo", actionForm.getDateTo());
	    	}	    		    		    	
	    	
	    	String branchMap = null;
	    	boolean first = true;
	    	for(int j=0; j < actionForm.getArRepBrOtListSize(); j++) {

	    		ArRepBranchOutputTaxList brList = (ArRepBranchOutputTaxList)actionForm.getArRepBrOtListByIndex(j);

	    		if(brList.getBranchCheckbox() == true) {
	    			if(first) {
	    				branchMap = brList.getBranchName();
	    				first = false;
	    			} else {
	    				branchMap = branchMap + ", " + brList.getBranchName();
	    			}
	    		}

	    	}
	    	parameters.put("branchMap", branchMap);
	    	
	    	String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepOutputTax.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/ArRepOutputTax.jasper");
		    
	        }
	        	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepOutputTaxDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepOutputTaxDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepOutputTaxDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepOutputTaxAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AR OT Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AR OT Load Action --
*******************************************************/

             }
         
         if(frParam != null) {
         	
         	try {
         	
         		actionForm.reset(mapping, request);
         		
	         	actionForm.clearArRepBrOtList();
	     		
	     		ArrayList list = ejbOT.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	     		
	     		Iterator i = list.iterator();
	     		
	     		while(i.hasNext()) {
	     			
	     			AdBranchDetails details = (AdBranchDetails)i.next();
	     			
	     			ArRepBranchOutputTaxList arRepBrOtList = new ArRepBranchOutputTaxList(actionForm, 
	     					details.getBrBranchCode(),details.getBrName(), details.getBrCode());
	     			arRepBrOtList.setBranchCheckbox(true);
	     			
	     			actionForm.saveArRepBrOtList(arRepBrOtList);
	     			
	     		}
	     		
         	} catch(EJBException ex) {
         		
         		if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepOutputTaxAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
         		
         	}
         	
         	return(mapping.findForward("arRepOutputTax"));		          
         	
         } else {
         	
         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
         	saveErrors(request, new ActionMessages(errors));
         	
         	return(mapping.findForward("cmnMain"));
         	
         }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepOutputTaxAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
