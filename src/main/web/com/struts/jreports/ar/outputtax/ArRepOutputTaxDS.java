package com.struts.jreports.ar.outputtax;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepOutputTaxDetails;

public class ArRepOutputTaxDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepOutputTaxDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepOutputTaxDetails details = (ArRepOutputTaxDetails)i.next();
                  
	     ArRepOutputTaxData argData = new ArRepOutputTaxData(details.getOtTinOfCustomer(),details.getOtRegisteredName(),
	     					details.getOtLastName(),details.getOtFirstName(),details.getOtMiddleName(),
							details.getOtAddress1(),details.getOtAddress2(),new Double(details.getOtNetAmount()),
							new Double(details.getOtOutputTax()), details.getOtTransaction());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("tinOfCustomer".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getTinOfCustomer();
      }else if("registeredName".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getRegisteredName();
      }else if("lastName".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getLastName();
      }else if("firstName".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getFirstName();
      }else if("middleName".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getMiddleName();
      }else if("address1".equals(fieldName)){
         value = ((ArRepOutputTaxData)data.get(index)).getAddress1();         
      }else if("address2".equals(fieldName)){
        value = ((ArRepOutputTaxData)data.get(index)).getAddress2();         
      }else if("netAmount".equals(fieldName)){
        value = ((ArRepOutputTaxData)data.get(index)).getNetAmount();         
      }else if("outputTax".equals(fieldName)){
        value = ((ArRepOutputTaxData)data.get(index)).getOutputTax();         
      }else if("transaction".equals(fieldName)){
        value = ((ArRepOutputTaxData)data.get(index)).getTransaction();         
      }
      
      return(value);
   }
}
