package com.struts.jreports.ar.outputtax;


public class ArRepOutputTaxData implements java.io.Serializable {
	
   private String tinOfCustomer = null;
   private String registeredName = null;
   private String lastName = null;
   private String firstName = null;
   private String middleName = null;
   private String address1 = null;
   private String address2 = null;
   private Double netAmount = null;
   private Double outputTax = null;
   private String transaction = null;

   public ArRepOutputTaxData(String tinOfCustomer, String registeredName, String lastName, 
      String firstName, String middleName, String address1, String address2, Double netAmount, 
      Double outputTax, String transaction) {
      	
      this.tinOfCustomer = tinOfCustomer;
      this.registeredName = registeredName;
      this.lastName = lastName;
      this.firstName = firstName;
      this.middleName = middleName;
      this.address1 = address1;
      this.address2 = address2;
      this.netAmount = netAmount;
      this.outputTax = outputTax;
      this.transaction = transaction;
      
   }
   
   public String getTinOfCustomer() {
   	
   		return tinOfCustomer;
   		
   }
   
   public String getRegisteredName() {
   	
   		return registeredName;
   		
   }
   
   public String getLastName() {
   	
   		return lastName;
   		
   }
   
   public String getFirstName() {
   	
   		return firstName;
   		
   }
   
   public String getMiddleName() {
   	
   		return middleName;
   		
   }
   
   public String getAddress1() {
   	
   		return address1;
   		   		
   }
         
   public String getAddress2() {
   		
   		return address2;
   		
   }
   
   public Double getNetAmount() {
   	
   		return netAmount;
   		
   }
   
   public Double getOutputTax() {
   	
   		return outputTax;
   		
   }
   
   public String getTransaction() {
	   
	   return transaction;
	   
   }
   
} 
