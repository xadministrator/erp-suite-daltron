package com.struts.jreports.ar.outputtax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepOutputTaxForm extends ActionForm implements Serializable{

   private String customerCode = null;
   private String dateFrom = null;
   private String dateTo = null;      
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();
   private ArrayList arRepBrOtList = new ArrayList();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getCustomerCode(){
      return(customerCode);
   }

   public void setCustomerCode(String customerCode){
      this.customerCode = customerCode;
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }
   
   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }
   
   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Object[] getArRepBrOtList(){
   	
   	  return arRepBrOtList.toArray();
   	
   }
   
   public ArRepBranchOutputTaxList getArRepBrOtListByIndex(int index){
   	
   	  return ((ArRepBranchOutputTaxList)arRepBrOtList.get(index));
   	
   }
   
   public int getArRepBrOtListSize(){
   	
   	  return(arRepBrOtList.size());
   	
   }
   
   public void saveArRepBrOtList(Object newArRepBrOtList){
   	
   	  arRepBrOtList.add(newArRepBrOtList);   	  
   	
   }
   
   public void clearArRepBrOtList(){
   	
   	  arRepBrOtList.clear();
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){   
   	
   	  for (int i=0; i<arRepBrOtList.size(); i++) {
	  	
   	  	  ArRepBranchOutputTaxList actionList = (ArRepBranchOutputTaxList)arRepBrOtList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
   	
      goButton = null;
      closeButton = null;
      customerCode = null;
      dateFrom = Common.convertSQLDateToString(new java.util.Date());
      dateTo = Common.convertSQLDateToString(new java.util.Date());
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("outputTax.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
		    errors.add("dateTo", new ActionMessage("outputTax.error.dateToInvalid"));
		 }	
		 	 
      }
      return(errors);
   }
}
