package com.struts.jreports.ar.pdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepPostDatedCheckController;
import com.ejb.txn.ArRepPostDatedCheckControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public class ArRepPdcAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
		      HttpServletRequest request, HttpServletResponse response)    
		      throws Exception {
		
		HttpSession session = request.getSession();
		
		try{
			
/*******************************************************
   Check if user has a session
*******************************************************/

		     User user = (User) session.getAttribute(Constants.USER_KEY);
		     
		     if (user != null) {
		     	
		        if (log.isInfoEnabled()) {
		        	
		            log.info("ArRepPdcAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
		            "' performed this action on session " + session.getId());
		            
		        }
		        
		     } else {
		     	
		         if (log.isInfoEnabled()) {
		         	
		            log.info("User is not logged on in session" + session.getId());
		           
		        }
		        
		        return(mapping.findForward("adLogon"));
		        
		     }
		     
	         ArRepPdcForm actionForm = (ArRepPdcForm)form;
	         
	   	     // reset report to null
   	         actionForm.setReport(null);

             String frParam = Common.getUserPermission(user, Constants.AR_REP_PDC_ID);

             if (frParam != null) {

	      	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	
	      	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
                     if (!fieldErrors.isEmpty()) {

                        saveErrors(request, new ActionMessages(fieldErrors));
                        
                        return mapping.findForward("arRepPdc");
                     }
	
	              }
	
                  actionForm.setUserPermission(frParam.trim());

           } else {

              actionForm.setUserPermission(Constants.NO_ACCESS);

           }
             
 /*******************************************************
     Initialize ArRepAgingController EJB
  *******************************************************/

           ArRepPostDatedCheckControllerHome homePdc = null;
           ArRepPostDatedCheckController ejbPdc = null;       

           try {
           	
              homePdc = (ArRepPostDatedCheckControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArRepPostDatedCheckControllerEJB", ArRepPostDatedCheckControllerHome.class);
              
           } catch(NamingException e) {
           	
              if(log.isInfoEnabled()) {
              	
                  log.info("NamingException caught in ArRepPdcAction.execute(): " + e.getMessage() +
                 " session: " + session.getId());
                 
              }
              
              return(mapping.findForward("cmnErrorPage"));
              
           }

           try {
           	
              ejbPdc = homePdc.create();
              
           } catch(CreateException e) {
           	
               if(log.isInfoEnabled()) {
               	
                   log.info("CreateException caught in ArRepPdcAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
                  
               }
              
               return(mapping.findForward("cmnErrorPage"));
              
           }
           
          ActionErrors errors = new ActionErrors();
           
/*** get report session and if not null set it to null **/
  	 
	  	  Report reportSession = 
	  	    (Report)session.getAttribute(Constants.REPORT_KEY);
	  	    
	  	  if(reportSession != null) {
	  	 	
	  	     reportSession.setBytes(null);
	  	     session.setAttribute(Constants.REPORT_KEY, reportSession);
	  	    
	  	  }

/*******************************************************
   -- AP AG Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;            
            
            String company = null;
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbPdc.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		       
		       HashMap criteria = new HashMap();
		       
		       if (!Common.validateRequired(actionForm.getMaturityDateFrom())) {
	        		
	        		criteria.put("maturityDateFrom", Common.convertStringToSQLDate(actionForm.getMaturityDateFrom()));
	        		
	           }
		       
		       if (!Common.validateRequired(actionForm.getMaturityDateTo())) {
        		
		       		criteria.put("maturityDateTo", Common.convertStringToSQLDate(actionForm.getMaturityDateTo()));
        		
		       }
		       
		       if (!Common.validateRequired(actionForm.getStatus())) {
		       	
		       		
		       		if(actionForm.getStatus().equals("CANCELLED")){
		       					       					       			
		       			criteria.put("isCancelled", new Byte(Common.convertBooleanToByte(true)));
		       			
		       		} else {
		       			
		       			criteria.put("status", actionForm.getStatus());
			       		
		       		}
		       		
		       }
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrPdcListSize(); i++) {
	           	
	           		ArRepBranchPdcList brPdcList = (ArRepBranchPdcList)actionForm.getArRepBrPdcListByIndex(i);
	           	
	           		if(brPdcList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brPdcList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       list = ejbPdc.executeArRepPostDatedCheckList(criteria, branchList, user.getCmpCode());
		       
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("repPdc.error.noRecordFound"));
                     		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepPdcAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("arRepPdc"));
		       
		    }	
            
//		    fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));		    
		    parameters.put("viewType", actionForm.getViewType());		    		   
	       
	       if (!Common.validateRequired(actionForm.getStatus())) {
	       	    	       	    
	       		parameters.put("status", actionForm.getStatus());
	       		
	       }
	       
	       if (!Common.validateRequired(actionForm.getMaturityDateFrom())) {
	       		
	       		parameters.put("maturityDateFrom", actionForm.getMaturityDateFrom());
	       		
	       }
	       
	       if (!Common.validateRequired(actionForm.getMaturityDateTo())) {
	       		
	       		parameters.put("maturityDateTo", actionForm.getMaturityDateTo());
	       		
	       }
	       
	       String branchMap = null;
		   boolean first = true;
		   for(int j=0; j < actionForm.getArRepBrPdcListSize(); j++) {

			   ArRepBranchPdcList brList = (ArRepBranchPdcList)actionForm.getArRepBrPdcListByIndex(j);

			   if(brList.getBranchCheckbox() == true) {
				   if(first) {
					   branchMap = brList.getBranchName();
					   first = false;
				   } else {
					   branchMap = branchMap + ", " + brList.getBranchName();
				   }
			   }

		   }
		   parameters.put("branchMap", branchMap);
	       
	       String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepPdc.jasper";
	       
	       if (!new java.io.File(filename).exists()) {
	       	  
	          filename = servlet.getServletContext().getRealPath("jreports/ArRepPdc.jasper");		      
		      
	       }
	
	       try {
	    	
	       Report report = new Report();
       
	       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	       	
	       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		       report.setBytes(
		          JasperRunManager.runReportToPdf(filename, parameters, 
			        new ArRepPdcDS(list)));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
               
               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
               report.setBytes(
				   JasperRunManagerExt.runReportToXls(filename, parameters, 
				        new ArRepPdcDS(list)));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
			   
			   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
			   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
			       new ArRepPdcDS(list)));												    
			        
		   }
		   
	       session.setAttribute(Constants.REPORT_KEY, report);
	       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepPdcAction.execute(): " + ex.getMessage() +
		           		" session: " + session.getId());		   
		        }
		        
		        return(mapping.findForward("cmnErrorPage"));
		   }
	    
/*******************************************************
    -- AP AG Close Action --
 *******************************************************/

	    } else if (request.getParameter("closeButton") != null) {
	     		
	        	return(mapping.findForward("cmnMain"));
	        	
/*******************************************************
   -- AP AG Load Action --
*******************************************************/
         } if(frParam != null) {
         	
         	try {
         		
         		actionForm.reset(mapping, request);
         	
	         	actionForm.clearArRepBrPdcList();
	     		
	     		ArrayList list = ejbPdc.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	     		
	     		Iterator i = list.iterator();
	     		
	     		while(i.hasNext()) {
	     			
	     			AdBranchDetails details = (AdBranchDetails)i.next();
	     			
	     			ArRepBranchPdcList arRepBrPdcList = new ArRepBranchPdcList(actionForm, 
	     					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	     			arRepBrPdcList.setBranchCheckbox(true);
	     			
	     			actionForm.saveArRepBrPdcList(arRepBrPdcList);
	     			
	     		}
	     		
         	} catch (EJBException ex) {
         		
         		if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepPdcAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
         		
         	}
         	         		    			                 	
             return(mapping.findForward("arRepPdc"));		          
		            
		 } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("cmnMain"));
		
		 }
	        
        } catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				      	
				log.info("Exception caught in ArRepPdcAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				            
			}   
					           
			return(mapping.findForward("cmnErrorPage"));   
					           
	    }		
	}

}
