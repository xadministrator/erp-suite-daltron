package com.struts.jreports.ar.pdc;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepPostDatedCheckDetails;

public class ArRepPdcDS implements JRDataSource {

	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public ArRepPdcDS(ArrayList list) {
		
		Iterator i = list.iterator();
	      
	    while (i.hasNext()) {
	      	
	       ArRepPostDatedCheckDetails details = (ArRepPostDatedCheckDetails)i.next();
	                                    
		   ArRepPdcData pdcData = new ArRepPdcData(details.getPdcCustomerCode(), details.getPdcCustomerName(), 
		   		details.getPdcDescription(), Common.convertSQLDateToString(details.getPdcDateReceived()), 
				Common.convertSQLDateToString(details.getPdcMaturityDate()),details.getPdcCheckNumber(),
				new Double(details.getPdcAmount()));	
		   
	         data.add(pdcData);
	      }     
	      
	}
	
	public boolean next() throws JRException{
	      index++;
	      return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException {
      Object value = null;

      String fieldName = field.getName();

      if("customerCode".equals(fieldName)) {
         value = ((ArRepPdcData)data.get(index)).getCustomerCode();
      } else if("description".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getDescription();
      } else if("dateReceived".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getDateReceived();
      } else if("maturityDate".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getMaturityDate();
      } else if("checkNumber".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getCheckNumber();
      } else if("amount".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getAmount();
      } else if("customerName".equals(fieldName)) {
      	 value = ((ArRepPdcData)data.get(index)).getCustomerName();
      }
      
      return(value);
   }
}
