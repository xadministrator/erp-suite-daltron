package com.struts.jreports.ar.pdc;

import java.io.Serializable;

public class ArRepPdcData implements Serializable {
	
	private String customerCode = null;
	private String customerName = null;
	private String description = null;
	private String dateReceived = null;
	private String maturityDate = null;
	private String checkNumber = null;
	private Double amount = null;
	
	public ArRepPdcData(String customerCode, String customerName, String description, String dateReceived, 
			String maturityDate, String checkNumber, Double amount) {
		
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.description = description;
		this.dateReceived = dateReceived;
		this.maturityDate = maturityDate;
		this.checkNumber = checkNumber;
		this.amount = amount;
		
	}
	
	public String getCustomerCode() {
		
		return customerCode;
		
	}
	
	public String getCustomerName() {
		
		return customerName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getDateReceived() {
		
		return dateReceived;
		
	}
	
	public String getMaturityDate() {
		
		return maturityDate;
		
	}
	
	public String getCheckNumber() {
		
		return checkNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
}
