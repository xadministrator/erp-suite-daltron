package com.struts.jreports.ar.pdc;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepPdcForm extends ActionForm implements Serializable{
	private String maturityDateFrom = null;
	private String maturityDateTo = null;
	private String status = null;
	private ArrayList statusList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	
	private ArrayList arRepBrPdcList = new ArrayList();
	
	public String userPermission = null;
	
	public String getMaturityDateFrom() {
		
		return maturityDateFrom;
		
	}
	
	public void setMaturityDateFrom(String maturityDateFrom) {
		
		this.maturityDateFrom = maturityDateFrom;
		
	}
	
	public String getMaturityDateTo() {
		
		return maturityDateTo;
		
	}
	
	public void setMaturityDateTo(String maturityDateTo) {
		
		this.maturityDateTo = maturityDateTo;
		
	}
	
	public String getStatus() {
		
		return status;
		
	}
		
	public void setStatus( String status ) {
		
		this.status = status;
		
	}
	
	
	public ArrayList getStatusList() {
		
		return statusList;
		
	}
	
	public void setStatusList(String status) {
		
		statusList.add(status);
		
	}
	
	public void clearStatusList() {
		
		statusList.clear();
		statusList.add(Constants.GLOBAL_BLANK);
		
	}
		
	public String getViewType() {
		
		return viewType;
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}	
	
	public void setViewTypeList(String viewType) {
		
		viewTypeList.add(viewType);
		
	}
	
	public void clearViewTypeList() {
		
		viewTypeList.clear();
		viewTypeList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getArRepBrPdcList(){
		
		return arRepBrPdcList.toArray();
		
	}
	
	public ArRepBranchPdcList getArRepBrPdcListByIndex(int index){
		
		return ((ArRepBranchPdcList)arRepBrPdcList.get(index));
		
	}
	
	public int getArRepBrPdcListSize(){
		
		return(arRepBrPdcList.size());
		
	}
	
	public void saveArRepBrPdcList(Object newArRepBrPdcList){
		
		arRepBrPdcList.add(newArRepBrPdcList);   	  
		
	}
	
	public void clearArRepBrPdcList(){
		
		arRepBrPdcList.clear();
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<arRepBrPdcList.size(); i++) {
		  	
			  ArRepBranchPdcList actionList = (ArRepBranchPdcList)arRepBrPdcList.get(i);
		  	  actionList.setBranchCheckbox(false);
		  	
		}
		
		maturityDateFrom = null;
		maturityDateTo = null;		
		statusList.clear();
		statusList.add(Constants.GLOBAL_BLANK);
		statusList.add("OPEN");
		statusList.add("CANCELLED");
		statusList.add("MATURED"); 
		statusList.add("CLEARED");
		status = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
	      ActionErrors errors = new ActionErrors();
	      if (request.getParameter("goButton") != null) {
	      	
	         if(!Common.validateDateFormat(maturityDateFrom)) {
	         	
			    errors.add("maturityDateFrom", new ActionMessage("repPdc.error.maturityDateFromInvalid"));
			    
			 }
	         
	         if(!Common.validateDateFormat(maturityDateTo)) {
	         	
	         	errors.add("maturityDateTo", new ActionMessage("repPdc.error.maturityDateToInvalid"));
	         	
	         }
		 
	      }	      
	      return errors;
	      
	}
}
