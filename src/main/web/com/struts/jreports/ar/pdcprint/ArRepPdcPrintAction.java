package com.struts.jreports.ar.pdcprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepPdcPrintController;
import com.ejb.txn.ArRepPdcPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArModPdcDetails;

public final class ArRepPdcPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepPdcPrintAction:Company '" + user.getCompany() + "' Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepPdcPrintForm actionForm = (ArRepPdcPrintForm)form;
         
         String frParam= Common.getUserPermission(user, Constants.AR_REP_PDC_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ArRepPdcPrintController EJB
*******************************************************/

         ArRepPdcPrintControllerHome homeRP = null;
         ArRepPdcPrintController ejbRP = null;       

         try {
         	
            homeRP = (ArRepPdcPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepPdcPrintControllerEJB", ArRepPdcPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepPdcPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbRP = homeRP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepPdcPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }

/*******************************************************
   -- TO DO getArRctByRctCode() --
*******************************************************/       

		if (frParam != null) {
			
				final String[] majorNames = {"", " Thousand", " Million",
					" Billion", " Trillion", " Quadrillion", " Quintillion"};
					
				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
					" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
					
				final String[] numNames = {"", " One", " Two", " Three", " Four",
					" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
					" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
					" Nineteen"};
	        
			    AdCompanyDetails adCmpDetails = null;
			    String baName = null;
			    ArrayList list = null; 
				    
				try {
					
	            	ArrayList pdcCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		pdcCodeList.add(new Integer(request.getParameter("pdcCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("pdcCode[" + i + "]") != null) {
								
								pdcCodeList.add(new Integer(request.getParameter("pdcCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbRP.executeArRepPdcPrint(pdcCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbRP.getAdCompany(user.getCmpCode()); 	     
			        
			        Iterator pdcIter = list.iterator();
			        
			        while (pdcIter.hasNext()) {
			        	
			        	ArModPdcDetails mdetails = (ArModPdcDetails)pdcIter.next();
			        	
			        	if(baName == null) {
			        		
			        		baName = mdetails.getRctBaName();
			        		
			        	}
			        	
			        	int num = (int)Math.floor(mdetails.getPdcAmount());
			        	
			        	String str = Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), (short)2);
			        	str = str.substring(str.indexOf('.') + 1, str.length());
			        	
			        	mdetails.setPdcAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
			        	str + "/100 Only");	
			        }

           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcPrint.error.pdcAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArRepPdcPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("arRepPdcPrintForm");

	             }
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				   
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				
				String filename = null;
				
				/*
				ArModPdcDetails pdcDetails = (ArModPdcDetails)list.iterator().next();
				
            	if(pdcDetails.getPdcType().equals("MEMO LINES") && pdcDetails.getPdcType().equals("ITEMS")) {
            		
            		filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepMiscReceiptPrint.jasper";
            		
            		if(!new java.io.File(filename).exists()) {
                		
                		filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepReceiptPrint.jasper";
                		
                	}
            		
            	}
            	*/
            		
        		filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepPdcPrint.jasper";
            		
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepPdcPrint.jasper");
			    
		        }
	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepPdcPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ArRepPdcPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("arRepPdcPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("arRepPdcPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ArRepPdcPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
   
   
   private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
   	String soFar;
   	
   	if (number % 100 < 20){
   		soFar = numNames[number % 100];
   		number /= 100;
   	}
   	else {
   		soFar = numNames[number % 10];
   		number /= 10;
   		
   		soFar = tensNames[number % 10] + soFar;
   		number /= 10;
   	}
   	if (number == 0) return soFar;
   	return numNames[number] + " Hundred" + soFar;
   }
   
   private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
   	/* special case */
   	if (number == 0) { return "zero"; }
   	
   	String prefix = "";
   	
   	if (number < 0) {
   		number = -number;
   		prefix = "Negative";
   	}
   	
   	String soFar = "";
   	int place = 0;
   	
   	do {
   		int n = number % 1000;
   		if (n != 0){
   			String s = this.convertLessThanOneThousand(n, tensNames, numNames);
   			soFar = s + majorNames[place] + soFar;
   		}
   		place++;
   		number /= 1000;
   	} while (number > 0);
   	
   	return (prefix + soFar).trim();
   }    
   
}
