/*
 * opt/jreport/ArRepPdcPrintData.java
 *
 * Created on July 17, 2008, 6:57 PM
 *
 * @author  Reginal Cris Pasco, Ariel Joseph De Guzman
 */

package com.struts.jreports.ar.pdcprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArModReceiptDetails;
import com.util.ArModPdcDetails;

public class ArRepPdcPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepPdcPrintDS(ArrayList list) {
   	
   	  Iterator i = list.iterator();
   	  while(i.hasNext()) {

        ArModPdcDetails mdetails = (ArModPdcDetails)i.next();

        ArRepPdcPrintData arRepRPData = new ArRepPdcPrintData(
        		mdetails.getPdcCstName(),
        		mdetails.getPdcCstAddress(), 
        		mdetails.getPdcDescription(),
        		new Double(mdetails.getPdcAmount()),
        		mdetails.getPdcAmountInWords(),
        		Common.convertSQLDateToString(mdetails.getPdcDateReceived()),
        		mdetails.getPdcCheckNumber(),	//receipt?
        		mdetails.getPdcReferenceNumber(),
        		mdetails.getPdcCstCustomerCode(),
        		mdetails.getPdcInvoiceNumbers(),
        		mdetails.getPdcBankName(),
        		mdetails.getPdcCheckNumber(),
        		mdetails.getPdcCstTin());

        data.add(arRepRPData);

   	  }
                                                                     
   }

   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("customerName".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getCustomerName(); 
      }else if("address".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getAddress();
      }else if("description".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getDescription();
      }else if("amount".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getAmount(); 
      }else if("amountInWords".equals(fieldName)){
          value = ((ArRepPdcPrintData)data.get(index)).getAmountInWords(); 
      }else if("dateReceived".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getDateReceived();      
      }else if("receiptNumber".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getReceiptNumber();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepPdcPrintData)data.get(index)).getReferenceNumber();
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepPdcPrintData)data.get(index)).getCustomerCode();
      }else if("invoiceNumbers".equals(fieldName)){
        value = ((ArRepPdcPrintData)data.get(index)).getInvoiceNumbers();
      }else if("bankName".equals(fieldName)){
        value = ((ArRepPdcPrintData)data.get(index)).getBankName();
      }else if("checkNumber".equals(fieldName)){
          value = ((ArRepPdcPrintData)data.get(index)).getCheckNumber();
      }else if("customerTin".equals(fieldName)){
        value = ((ArRepPdcPrintData)data.get(index)).getCustomerTin();        
      }else if("totalDiscount".equals(fieldName)){
      
      }
      return(value);
    }
}
