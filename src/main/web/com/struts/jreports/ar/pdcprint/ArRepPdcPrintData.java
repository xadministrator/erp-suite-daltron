package com.struts.jreports.ar.pdcprint;

import java.util.Comparator;

import com.util.ArRepSalesDetails;

public class ArRepPdcPrintData implements java.io.Serializable {
   
   private String customerName = null;
   private String address = null;
   private String description = null;
   private Double amount = null;
   private String amountInWords = null;
   private String dateReceived = null;
   private String receiptNumber = null;
   private String referenceNumber = null;
   private String customerCode = null;
   private String invoiceNumbers = null;
   private String bankAccount = null;
   private String bankName = null;
   private String checkNumber = null;
   private String customerTin = null;
   
   
   public ArRepPdcPrintData(String customerName, String address, String description, Double amount, String amountInWords, 
        String dateReceived, String receiptNumber, String referenceNumber, String customerCode, String invoiceNumbers, 
        String bankName, String checkNumber, String customerTin) {
      	
      	this.customerName = customerName;
      	this.address = address;
      	this.description = description;
      	this.amount = amount;
      	this.amountInWords = amountInWords;
      	this.dateReceived = dateReceived;
      	this.receiptNumber = receiptNumber;
      	this.referenceNumber = referenceNumber;
      	this.customerCode = customerCode;
      	this.invoiceNumbers = invoiceNumbers;
      	this.bankName = bankName;
      	this.checkNumber = checkNumber;
		this.customerTin = customerTin;
		
   }
	
	public String getCustomerName() {
		return (this.customerName); 
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName; 
	}

	public String getAddress() {
		return (this.address); 
	}

	public void setAddress(String address) {
		this.address = address; 
	}

	public String getDescription() {
		return (this.description); 
	}

	public void setDescription(String description) {
		this.description = description; 
	}

	public Double getAmount() {
		return (this.amount); 
	}
	
	public void setAmount(Double amount) {
		this.amount = amount; 
	}

	public String getAmountInWords() {
		return (this.amountInWords); 
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords; 
	}
	
	public String getDateReceived() {
		return (this.dateReceived); 
	}

	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived; 
	}

	public String getReceiptNumber() {
		return (this.receiptNumber); 
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber; 
	}

	public String getReferenceNumber() {
		return (this.referenceNumber); 
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber; 
	}
	
	public String getCustomerCode() {
		return customerCode;
	}
									 
	public String getInvoiceNumbers() {
		return invoiceNumbers;
	}
	
	public String getBankAccoutn() {
		return bankAccount;
	}

	public String getBankName() {
		return bankName;
	}
	
	public String getCheckNumber() {
		return checkNumber;
	}
	
	public String getCustomerTin() {
		return customerTin;
	}
			
}
