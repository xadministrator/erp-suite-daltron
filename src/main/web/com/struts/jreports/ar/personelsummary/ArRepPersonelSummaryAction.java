package com.struts.jreports.ar.personelsummary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepPersonelSummaryController;
import com.ejb.txn.ArRepPersonelSummaryControllerHome;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepPersonelSummaryDetails;

public final class ArRepPersonelSummaryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepPersonelSummaryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepPersonelSummaryForm actionForm = (ArRepPersonelSummaryForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_PERSONEL_SUMMARY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepPersonelSummary");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepPersonelSummaryController EJB
*******************************************************/

         ArRepPersonelSummaryControllerHome homePS = null;
         ArRepPersonelSummaryController ejbPS = null;
     

         try {
         	
        	 homePS = (ArRepPersonelSummaryControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepPersonelSummaryControllerEJB", ArRepPersonelSummaryControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepPersonelSummaryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
        	 ejbPS = homePS.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepPersonelSummaryAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR PS Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getJobOrderType())) {
	        		
	        		criteria.put("jobOrderType", actionForm.getJobOrderType());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	    

	        	if (!Common.validateRequired(actionForm.getCustomerType())) {

	        		criteria.put("customerType", actionForm.getCustomerType());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	
	       
                
                if (!Common.validateRequired(actionForm.getPaymentStatus())) {
	        		
	        		criteria.put("paymentStatus", actionForm.getPaymentStatus());
	        		
	        	}
                	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbPS.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrPesListSize(); i++) {
	           	
	           		ArRepBranchPersonelSummaryList brSrList = (ArRepBranchPersonelSummaryList)actionForm.getArRepBrPesListByIndex(i);
	           	
	           		if(brSrList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brSrList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbPS.executeArRepPersonelSummary(actionForm.getCriteria(), branchList, actionForm.getPersonelName(),
		       		actionForm.getGroupBy(), actionForm.getIncludeUnposted(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		           
		       
		       
		       Collections.sort(list, sortRctByPersonel);
		       
		       
		       if(list.size()<=0) {
		    	   throw new GlobalNoRecordFoundException();
		       }
		       
		       
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("salesRegister.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepPersonelSummaryAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepPersonelSummary");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("groupBy", actionForm.getGroupBy());
			parameters.put("viewType", actionForm.getViewType());
			parameters.put("includeUnposted",actionForm.getIncludeUnposted()?"YES":"NO");
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getJobOrderType() != null) {
				
				parameters.put("jobOrderType", actionForm.getJobOrderType());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }

			if (actionForm.getCustomerType() != null) {

				parameters.put("customerType", actionForm.getCustomerType());	

			}
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", actionForm.getDateFrom());				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", actionForm.getDateTo());	    	
			
		    }
	 
			parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
		    parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
		    
		  
		    
		    parameters.put("paymentStatus", actionForm.getPaymentStatus());
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepBrPesListSize(); j++) {

		    	ArRepBranchPersonelSummaryList brList = (ArRepBranchPersonelSummaryList)actionForm.getArRepBrPesListByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBranchName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBranchName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepPersonelSummary.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	        	filename = servlet.getServletContext().getRealPath("jreports/ArRepPersonelSummary.jasper");
		    
	        }
    		    	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepPersonelSummaryDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepPersonelSummaryDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepPersonelSummaryDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepPersonelSummaryAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        ex.printStackTrace();
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		   
/*******************************************************
   -- AR PS Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		     	  
		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AR PS Customer Enter Action --
 *******************************************************/
		          
		     } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {
		     	
		     	return(mapping.findForward("arRepPersonelSummary"));		          
		          
/*******************************************************
   -- AR PS Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.reset(mapping, request);
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbPS.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   

	            	actionForm.clearCustomerTypeList();           	

	            	list = ejbPS.getArCtAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            			actionForm.setCustomerTypeList((String)i.next());

	            		}

	            	}  
	            	
	            	actionForm.clearArRepBrPesList();
	         		
	         		list = ejbPS.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchPersonelSummaryList arRepBrSrList = new ArRepBranchPersonelSummaryList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			arRepBrSrList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrPesList(arRepBrSrList);
	         			
	         		}
	         		
	         		
	         		
	         		actionForm.clearJobOrderTypeList();
	              	
	              	
	              	list = ejbPS.getAllJobOrderTypeName(user.getCmpCode());
	              	
	              	
	              	
	              	i = list.iterator();
	              	while(i.hasNext()) {
	              		actionForm.setJobOrderTypeList((String)i.next());
	              	}
	              	

	              	actionForm.clearPersonelNameList();          	
	                	
	                	list = ejbPS.getArPeAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                	System.out.println("PERSONEL list.size()="+list.size());
	                	if (list == null || list.size() == 0) {
	                		
	                		actionForm.setPersonelName(Constants.GLOBAL_NO_RECORD_FOUND);
	                		
	                	} else {
	                		           		            		
	                		i = list.iterator();
	                		
	                		while (i.hasNext()) {
	                			
	                		    actionForm.setPersonelNameList((String)i.next());
	                			
	                		}
	                		            		
	                	}
	                	
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepPersonelSummaryAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepPersonelSummary"));
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
        	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepPersonelSummaryAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
   
   private static Comparator sortRctByPersonel = new Comparator() {
   	
   	public int compare(Object r1, Object r2) {
   		
   		ArRepPersonelSummaryDetails receipt1 = (ArRepPersonelSummaryDetails) r1;
   		ArRepPersonelSummaryDetails receipt2 = (ArRepPersonelSummaryDetails) r2;
   		
   		return receipt1.getJaPersonelCode().compareTo(receipt2.getJaPersonelCode());
   		
   	}
   	
   };
}
