package com.struts.jreports.ar.personelsummary;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepPersonelSummaryDetails;
import com.util.ArRepSalespersonDetails;

public class ArRepPersonelSummaryDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepPersonelSummaryDS(ArrayList list, String groupBy) {
   	
   	  Iterator i = list.iterator();
   	
      while (i.hasNext()) {
      	
         ArRepPersonelSummaryDetails details = (ArRepPersonelSummaryDetails)i.next();
         
         String group = null;

         if (groupBy.equals("PERSONEL NAME")) {
         	
         	group = details.getJaPersonelName();
         	
         } else if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getJoCstCustomerCode() + "-" + details.getJoCstCustomerName();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
          	
          	group = details.getJoCstCustomerType();
          	
          }
         
         group = details.getJaPersonelName();
         
	     ArRepPersonelSummaryData argData = new ArRepPersonelSummaryData(Common.convertSQLDateToString(details.getJoDate()), details.getJoType(),
	 			details.getJoCstCustomerCode(), details.getJoCstCustomerName(), details.getJoCstCustomerType(), details.getJoDocumentNumber(), details.getJoQuantity(), details.getJoAmount(),
				details.getJaPersonelCode(), details.getJaPersonelName(), details.getJaQuantity(),details.getJaAmount(),
				group, details.getJoPaymentStatus());
		    
	     System.out.println("11");
	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();
      if("joDate".equals(fieldName)){
          value = ((ArRepPersonelSummaryData)data.get(index)).getJoDate();    
      }else if("personelCode".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getPersonelCode();       
      }else if("personelName".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getPersonelName();
      }else if("customerCode".equals(fieldName)){
          value = ((ArRepPersonelSummaryData)data.get(index)).getCustomerCode();
       }else if("customerName".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getCustomerName();   
      }else if("documentNumber".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getDocumentNumber();
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepPersonelSummaryData)data.get(index)).getGroupBy();
      }else if("joQuantity".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getJoQuantity();       
      }else if("joAmount".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getJoAmount();       
      }else if("customerType".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getCustomerType();       
      }else if("jaQuantity".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getJaQuantity();       
      }else if("jaAmount".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getJaAmount();       
      }else if("paymentStatus".equals(fieldName)){
         value = ((ArRepPersonelSummaryData)data.get(index)).getPaymentStatus();       
      }else if("groupBy".equals(fieldName)){
          value = ((ArRepPersonelSummaryData)data.get(index)).getGroupBy();       
       }

      return(value);
      
   }
}
