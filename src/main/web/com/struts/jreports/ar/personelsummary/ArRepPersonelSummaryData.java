package com.struts.jreports.ar.personelsummary;

import java.util.Date;


public class ArRepPersonelSummaryData implements java.io.Serializable {
	
	private String jobOrderType = null;
	private String joDate = null;
	private String customerCode = null;
	private String customerName = null;
	private String customerType = null;
	private String documentNumber= null;
	private Double joQuantity = null;
	private Double joAmount = null;

	private String personelCode = null;
	private String personelName = null;
	private Double jaQuantity = null;
	private Double jaAmount = null;
	

	private String groupBy = null;

	private String paymentStatus = null;

	
	public ArRepPersonelSummaryData(String joDate, String jobOrderType,
			String customerCode, String customerName, String customerType, String documentNumber, Double joQuantity, Double joAmount,
			String personelCode, String personelName, Double jaQuantity, Double jaAmount,
			String groupBy,  String paymentStatus) {
		
		this.joDate = joDate;
		this.documentNumber = documentNumber;
		this.jobOrderType = jobOrderType;
		this.personelCode = personelCode;
		this.personelName = personelName;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.joQuantity = joQuantity;
		this.joAmount =joAmount;
		this.jaQuantity = jaQuantity;
		this.jaAmount = jaAmount;
		this.groupBy = groupBy;

		this.paymentStatus = paymentStatus;
		this.customerType = customerType;

   }
	
	
	 public String getJoDate() {
		   	
	   	  return joDate;
	   	  
	   }


    public String getJobOrderType() {
		   	
   	  return jobOrderType;
   	  
   }

  
	
   public String getDocumentNumber() {
		return documentNumber;
	}

	public Double getJoQuantity() {
		return joQuantity;
	}

	public Double getJoAmount() {
		return joAmount;
	}

	public Double getJaQuantity() {
		return jaQuantity;
	}

	public Double getJaAmount() {
		return jaAmount;
	}

	
	public String getPersonelCode() {
		return personelCode;
	}
	
public String getPersonelName() {
   	
   	  return personelName;
   	  
   }

   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   
   public String getCustomerName() {
	   
	   return customerName;
	   
   }
   
  
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
 
   public String getPaymentStatus() {
	   
	   return paymentStatus;
	   
   }
   
   public String getCustomerType() {
	   
	   return customerType;
	   
   }
   
  
   
}
