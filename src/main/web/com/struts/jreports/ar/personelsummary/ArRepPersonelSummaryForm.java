package com.struts.jreports.ar.personelsummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepPersonelSummaryForm extends ActionForm implements Serializable{

   private String jobOrderType = null;
   private ArrayList jobOrderTypeList = new ArrayList();
   
   private String personelName = null;
   private ArrayList personelNameList = new ArrayList();
   
   private String customerCode = null;

   private String customerClass = null;
   private ArrayList customerClassList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();


   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String isCustomerEntered = null;
   private String customerType = null;
   private ArrayList customerTypeList = new ArrayList();
   
   private boolean includeUnposted = false;
   
   private ArrayList arRepBrPesList = new ArrayList();

   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getPersonelName() {
	   
	   return(personelName);
	   
   }
   
   public void setPersonelName(String personelName) {
	
	   this.personelName = personelName;
	      
   }
   
   public String getCustomerCode(){
   	
      return(customerCode);
      
   }

   public void setCustomerCode(String customerCode){
   	
      this.customerCode = customerCode;
      
   }
   
   
   public boolean getIncludeUnposted() {
	   return includeUnposted;
   }
   
   public void setIncludeUnposted(boolean includeUnposted) {
	   this.includeUnposted = includeUnposted;
   }

   public String getDateFrom(){
   	
      return(dateFrom);
      
   }

   public void setDateFrom(String dateFrom){
   	
      this.dateFrom = dateFrom;
      
   }

   public String getDateTo(){
   	
      return(dateTo);
      
   }

   public void setDateTo(String dateTo){
   	
      this.dateTo = dateTo;
      
   }

   public String getDocumentNumberFrom(){
   	
      return(documentNumberFrom);
      
   }

   public void setDocumentNumberFrom(String documentNumberFrom){
   	
      this.documentNumberFrom = documentNumberFrom;
      
   }

   public String getDocumentNumberTo(){
   	
      return(documentNumberTo);
      
   }

   public void setDocumentNumberTo(String documentNumberTo){
   	
      this.documentNumberTo = documentNumberTo;
      
   }

   public void setGoButton(String goButton){
   	
      this.goButton = goButton;
      
   }

   public void setCloseButton(String closeButton){
   	
      this.closeButton = closeButton;
      
   }
   
   public String getIsCustomerEntered() {
   	
   	  return isCustomerEntered;
   	  
   }
   
   public void setIsCustomerEntered(String isCustomerEntered) {
   	
   	  this.isCustomerEntered = isCustomerEntered;
   	  
   }

   public String getCustomerClass(){
   	
      return(customerClass);
      
   }

   public void setCustomerClass(String customerClass){
   	
      this.customerClass = customerClass;
      
   }

   public ArrayList getCustomerClassList(){
   	
      return(customerClassList);
      
   }

   public void setCustomerClassList(String customerClass){
   	
      customerClassList.add(customerClass);
      
   }

   public void clearCustomerClassList(){
   	
      customerClassList.clear();
      customerClassList.add(Constants.GLOBAL_BLANK);
      
   } 
   
   public String getJobOrderType() {
	   return jobOrderType;
   }
   
   public void setJobOrderType(String jobOrderType) {
	   this.jobOrderType = jobOrderType;
   }
   
   
   public ArrayList getJobOrderTypeList(){
	   	
      return(jobOrderTypeList);
      
   }

   public void setJobOrderTypeList(String jobOrderType){
   	
	   jobOrderTypeList.add(jobOrderType);
      
   }

   public void clearJobOrderTypeList(){
   	
	   jobOrderTypeList.clear();
	   jobOrderTypeList.add(Constants.GLOBAL_BLANK);
      
   } 
   
   
   public ArrayList getPersonelNameList(){
	   	
      return(personelNameList);
      
   }

   public void setPersonelNameList(String personelName){
   	
	   personelNameList.add(personelName);
      
   }

   public void clearPersonelNameList(){
   	
	   personelNameList.clear();
	   personelNameList.add(Constants.GLOBAL_BLANK);
      
   } 
	   
   
   
   

   public String getGroupBy(){
   	
   	return(groupBy);   	
   	
   }
   
   public void setGroupBy(String groupBy){
   	
   	this.groupBy = groupBy;
   	
   }
   
   public ArrayList getGroupByList(){
   	
   	return groupByList;
   	
   }
   
   public String getViewType(){
   	
   	  return(viewType);   	
   	  
   }
   
   public void setViewType(String viewType){
   	
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList(){
   	
   	  return viewTypeList;
   	  
   }
   
 
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
   	
      return(userPermission);
      
   }

   public void setUserPermission(String userPermission){
   	
      this.userPermission = userPermission;
      
   }
   
   public ArrayList getArRepBrPesList() {
	   return arRepBrPesList;
   }
   
   public ArRepBranchPersonelSummaryList getArRepBrPesListByIndex(int index){
   	
   	  return (ArRepBranchPersonelSummaryList)arRepBrPesList.get(index);
   	
   }
   
   public int getArRepBrPesListSize(){
   	
   	  return(arRepBrPesList.size());
   	
   }
   
   public void saveArRepBrPesList(Object newArRepBrPesList){
   	
	   arRepBrPesList.add(newArRepBrPesList);   	  
   	
   }
   
   public void clearArRepBrPesList(){
   	
	   arRepBrPesList.clear();
   	
   }

   public String getCustomerType() {

	   return customerType;

   }

   public void setCustomerType(String customerType) {

	   this.customerType = customerType;

   }

   public ArrayList getCustomerTypeList() {

	   return customerTypeList;

   }

   public void setCustomerTypeList(String customerType) {

	   customerTypeList.add(customerType);

   }

   public void clearCustomerTypeList() {

	   customerTypeList.clear();
	   customerTypeList.add(Constants.GLOBAL_BLANK);

   }
   
   public String getPaymentStatus() {
	   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){      
   	
   	  for (int i=0; i<arRepBrPesList.size(); i++) {
	  	
   	  	  ArRepBranchPersonelSummaryList actionList = (ArRepBranchPersonelSummaryList)arRepBrPesList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
   	includeUnposted = false;
      goButton = null;
      closeButton = null;
      personelName = null;
      jobOrderType = null;
      customerCode = null;
      customerClass = Constants.GLOBAL_BLANK; 
      customerType = Constants.GLOBAL_BLANK;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      isCustomerEntered = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
    
      groupByList.clear();
      groupByList.add(Constants.AR_REP_SALESPERSON_ORDER_BY);
      groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
      groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
      groupBy = Constants.GLOBAL_BLANK;   

      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("salesRegister.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("salesRegister.error.dateToInvalid"));
		 }
		 if(!Common.validateStringExists(customerClassList, customerClass)){
	            errors.add("customerClass", new ActionMessage("salesRegister.error.customerClassInvalid"));
		 }	 
      }
      return(errors);
   }
}
