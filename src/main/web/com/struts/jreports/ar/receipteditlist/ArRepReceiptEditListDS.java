package com.struts.jreports.ar.receipteditlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepReceiptEditListDetails;


public class ArRepReceiptEditListDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepReceiptEditListDS(ArrayList arRepIELList, String userName) {
      
   	  Date currentDate = new Date();
   	
   	  Iterator i = arRepIELList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    ArRepReceiptEditListDetails details = (ArRepReceiptEditListDetails)i.next();
   	    
   	    if(details.getRelDrDebit() == 1) {
   	    
	   	  	ArRepReceiptEditListData arRepRELData = new ArRepReceiptEditListData(
	   	     details.getRelRctBatchName(), details.getRelRctBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getRelRctTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), 
			 Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getRelRctNumber(), Common.convertSQLDateToString(details.getRelRctDate()), 
			 details.getRelCstCustomerCode(), details.getRelCstCustomerName(), details.getRelRctReferenceNumber(), 
			 details.getRelRctDescription(), new Double(details.getRelRctAmount()), details.getRelDrAccountNumber(), 
			 details.getRelDrAccountDescription(), details.getRelDrClass(), new Double(details.getRelDrAmount()), null);
	   	  	

             data.add(arRepRELData);
	   	  	
	    } else {
	   	
	    	ArRepReceiptEditListData arRepRELData = new ArRepReceiptEditListData(
   	   	     details.getRelRctBatchName(), details.getRelRctBatchDescription(), 
			 Common.convertIntegerToString(new Integer(details.getRelRctTransactionTotal())),
			 Common.convertSQLDateToString(currentDate), 
			 Common.convertSQLDateTimeToString(currentDate), userName,
			 details.getRelRctNumber(), Common.convertSQLDateToString(details.getRelRctDate()), 
			 details.getRelCstCustomerCode(), details.getRelCstCustomerName(), details.getRelRctReferenceNumber(), 
			 details.getRelRctDescription(), new Double(details.getRelRctAmount()), details.getRelDrAccountNumber(), 
			 details.getRelDrAccountDescription(), details.getRelDrClass(), null, new Double(details.getRelDrAmount()));
    	
   	  	    data.add(arRepRELData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

	  if("batchName".equals(fieldName)){
	    value = ((ArRepReceiptEditListData)data.get(index)).getBatchName(); 
	  }else if("batchDescription".equals(fieldName)){
	    value = ((ArRepReceiptEditListData)data.get(index)).getBatchDescription();       
	  }else if("transactionTotal".equals(fieldName)){
	    value = ((ArRepReceiptEditListData)data.get(index)).getTransactionTotal(); 
	  }else if("dateCreated".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getDateCreated();  
      }else if("timeCreated".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getTimeCreated();  
      }else if("createdBy".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getCreatedBy();
      }else if("receiptNumber".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getReceiptNumber();    
      }else if("date".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getDate();   
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getCustomerCode(); 
      }else if("customerName".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getCustomerName(); 
      }else if("referenceNumber".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getReferenceNumber();
      }else if("receiptDescription".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getReceiptDescription();      
      }else if("amount".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getAmount(); 
      }else if("accountNumber".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getAccountNumber();   
      }else if("accountDescription".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getAccountDescription(); 
      }else if("class".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getClassType();    
      }else if("debitAmount".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getDebitAmount();   
      }else if("creditAmount".equals(fieldName)){
        value = ((ArRepReceiptEditListData)data.get(index)).getCreditAmount();                   
      }

      return(value);
   }
}
