package com.struts.jreports.ar.receipteditlist;

public class ArRepReceiptEditListData implements java.io.Serializable {

	   private String batchName = null;
	   private String batchDescription = null;
	   private String transactionTotal = null;
	   private String dateCreated = null;
	   private String timeCreated = null;
	   private String createdBy = null;
	   private String receiptNumber = null;	   
	   private String date = null;
	   private String customerCode = null;
	   private String customerName = null;
	   private String referenceNumber = null;
	   private String receiptDescription = null;
	   private Double amount = null;

	   private String accountNumber = null;
	   private String accountDescription = null;
	   private String classType = null;
	   private Double debitAmount = null;
	   private Double creditAmount = null;
	   
	   public ArRepReceiptEditListData(String batchName, String batchDescription, String transactionTotal, 
	    String dateCreated, String timeCreated, String createdBy, String receiptNumber, String date, 
		String customerCode, String customerName, String referenceNumber, String receiptDescription,
		Double amount, String accountNumber, String accountDescription, String classType, Double debitAmount, 
		Double creditAmount) {
	      
	      this.batchName = batchName;
	      this.batchDescription = batchDescription;
	      this.transactionTotal = transactionTotal;
	      this.dateCreated = dateCreated;
	      this.timeCreated = timeCreated;
	      this.createdBy = createdBy;
	      this.receiptNumber = receiptNumber;
	      this.date = date;
	      this.customerCode = customerCode;
	      this.customerName = customerName;
	      this.referenceNumber = referenceNumber;
	      this.receiptDescription = receiptDescription;
	      this.amount = amount;
	      this.accountNumber = accountNumber;
	      this.accountDescription = accountDescription;
	      this.classType = classType;
	      this.debitAmount = debitAmount;
	      this.creditAmount = creditAmount;
	      	
	   }
	   
	   public String getBatchName() {
	   	
	   	  return batchName;
	   	 
	   }
	   
	   public String getBatchDescription() {
	   	
	   	  return batchDescription;
	   	
	   }
	   
	   public String getTransactionTotal() {
	   	
	   	  return transactionTotal;
	   	
	   }

	   public String getDateCreated() {

	      return dateCreated;

	   }

	   public String getTimeCreated() {

	      return timeCreated;

	   }

	   public String getCreatedBy() {

	      return createdBy;

	   }

	   public String getReceiptNumber() {

	      return receiptNumber;

	   }

	   public String getDate() {

	      return date;

	   }

	   public String getCustomerCode() {

	      return customerCode;

	   }
	   
	   public String getCustomerName() {

	      return customerName;

	   }
	   
	   public String getReferenceNumber() {
	   
	      return referenceNumber;

	   }
	   
	   public String getReceiptDescription() {
	   	
	   	  return receiptDescription;
	   	
	   }

	   public Double getAmount() {

	      return amount;

	   }

	   public String getAccountNumber() {

	      return accountNumber;

	   }

	   public String getAccountDescription() {

	      return accountDescription;

	   }

	   public String getClassType() {
	    
	      return classType ;

	   }

	   public Double getDebitAmount() {

	      return debitAmount;

	   }

	   public Double getCreditAmount() {
	    
	      return creditAmount;

	   } 
	   
}

