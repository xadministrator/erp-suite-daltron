package com.struts.jreports.ar.receipteditlist;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepReceiptEditListDetails;

public class ArRepReceiptEditListSubDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepReceiptEditListSubDS(ArrayList arRepIELList) {
   	
 	  Iterator i = arRepIELList.iterator();
 	  
 	  while(i.hasNext()) {
   	  	
   	    ArRepReceiptEditListDetails details = (ArRepReceiptEditListDetails)i.next();
   	    
   	    ArRepReceiptEditListSubData arRepRELData = new ArRepReceiptEditListSubData(
   	     details.getRelRctNumber(), details.getRelInvNumber(), new Double(details.getRelInvAmountApplied()));
   	  	
        data.add(arRepRELData);

 	  }
                                                      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

	if("receiptNumber".equals(fieldName)){
	  value = ((ArRepReceiptEditListSubData)data.get(index)).getReceiptNumber(); 
	}else if("invoiceNumber".equals(fieldName)){
	  value = ((ArRepReceiptEditListSubData)data.get(index)).getInvoiceNumber();       
	}else if("amountApplied".equals(fieldName)){
	  value = ((ArRepReceiptEditListSubData)data.get(index)).getAmountApplied(); 
	}

      return(value);
   }
}
