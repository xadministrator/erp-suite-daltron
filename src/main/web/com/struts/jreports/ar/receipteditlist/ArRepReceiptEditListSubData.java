package com.struts.jreports.ar.receipteditlist;

public class ArRepReceiptEditListSubData implements java.io.Serializable {
         
   private String receiptNumber = null;  
   private String invoiceNumber = null;
   private Double amountApplied = null;
	   
   public ArRepReceiptEditListSubData(String receiptNumber, String invoiceNumber, Double amountApplied) {
      
      this.receiptNumber = receiptNumber;
      this.invoiceNumber = invoiceNumber;
      this.amountApplied = amountApplied;
      	
   }
	   
   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	 
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	
   }
   
   public Double getAmountApplied() {
   	
   	  return amountApplied;
   	
   }

	   
}
