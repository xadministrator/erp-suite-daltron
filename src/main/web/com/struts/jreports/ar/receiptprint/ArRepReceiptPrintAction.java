package com.struts.jreports.ar.receiptprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepReceiptPrintController;
import com.ejb.txn.ArRepReceiptPrintControllerHome;
import com.struts.jreports.ap.checkvoucherprint.ApRepCheckVoucherPrintSubDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArModReceiptDetails;

public final class ArRepReceiptPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepReceiptPrintAction:Company '" + user.getCompany() + "' Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepReceiptPrintForm actionForm = (ArRepReceiptPrintForm)form;
         
         String frParam= Common.getUserPermission(user, Constants.AR_REP_RECEIPT_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ArRepReceiptPrintController EJB
*******************************************************/

         ArRepReceiptPrintControllerHome homeRP = null;
         ArRepReceiptPrintController ejbRP = null;       

         try {
         	
            homeRP = (ArRepReceiptPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepReceiptPrintControllerEJB", ArRepReceiptPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepReceiptPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbRP = homeRP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepReceiptPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }

/*******************************************************
   -- TO DO getArRctByRctCode() --
*******************************************************/       

		if (frParam != null) {
			
				final String[] majorNames = {"", " Thousand", " Million",
					" Billion", " Trillion", " Quadrillion", " Quintillion"};
					
				final String[] tensNames = {"", " Ten", " Twenty", " Thirty",
					" Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
					
				final String[] numNames = {"", " One", " Two", " Three", " Four",
					" Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
					" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen",
					" Nineteen"};
	        
			    AdCompanyDetails adCmpDetails = null;
			    String baName = null;
			    ArrayList list = null; 
			    ArrayList listSub = null;
			    String BR_BRNCH_CODE = user.getCurrentBranch().getBrBranchCode();
				    
				try {
					
	            	ArrayList rctCodeList = new ArrayList();
	            	
					
					if (request.getParameter("forward") != null) {
												
	            		rctCodeList.add(new Integer(request.getParameter("receiptCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("receiptCode[" + i + "]") != null) {
								
								rctCodeList.add(new Integer(request.getParameter("receiptCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbRP.executeArRepReceiptPrint(rctCodeList, user.getCmpCode());
	            	
	            	try {
	            		
	            		listSub = ejbRP.executeArRepReceiptPrintSub(rctCodeList, user.getCmpCode());
	            		
	            	} catch (GlobalNoRecordFoundException ex) {
	            		
	            	}
	            	
			        // get company
			       
			        adCmpDetails = ejbRP.getAdCompany(user.getCmpCode()); 	     
			        
			        Iterator rctIter = list.iterator();
			        
			        while (rctIter.hasNext()) {
			        	
			        	ArModReceiptDetails mdetails = (ArModReceiptDetails)rctIter.next();
			        	
			        	if(baName == null) {
			        		
			        		baName = mdetails.getRctBaName();
			        		
			        	}
			        	
			        	int num = (int)Math.floor(mdetails.getRctAmount());
			        	
			        	String str = Common.convertDoubleToStringMoney(mdetails.getRctAmount(), (short)2);
			        	str = str.substring(str.indexOf('.') + 1, str.length());
			        	
			        	mdetails.setRctAmountInWords(this.convert(num, majorNames, tensNames, numNames) + " And " + 
			        	str + "/100 Only");	
			        }

           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receiptPrint.error.receiptAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArRepReceiptPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("arRepReceiptPrintForm");

	             }
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				   
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				
				String subreportFilename = null;
				
				if(listSub != null) {
					
					subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepReceiptPrintSub.jasper";
					
					if(new java.io.File(subreportFilename).exists()) {
						
						JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
						parameters.put("ArRepReceiptPrintSub", subreport);
		                parameters.put("ArRepReceiptPrintSubDS", new ArRepReceiptPrintSubDS(listSub));
						parameters.put("showJournal", new Boolean(true));												
					}
					
				} else {
					
					parameters.put("showJournal", new Boolean(false));
					
				}
				
				String filename = null;
				
				ArModReceiptDetails rctDetails = (ArModReceiptDetails)list.iterator().next();
				System.out.println("rctDetails.getRctType()="+rctDetails.getRctType());
				System.out.println("rctDetails.getRctMiscType()="+rctDetails.getRctMiscType());
				
            	if(rctDetails.getRctType().equals("MISC") && rctDetails.getRctMiscType().equals("ITEMS")) {
            		
        
            		
            		filename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/ArRepMiscReceiptPrint.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepMiscReceiptPrint.jasper";
						
						if (!new java.io.File(filename).exists()) {
							
							filename = servlet.getServletContext().getRealPath("jreports/ArRepMiscReceiptPrint.jasper");
							
						}						
					}
					
					
            		
            	} else {
            		
            		
            		filename = "/opt/ofs-resources/" + user.getCompany() + "/" +BR_BRNCH_CODE + "/ArRepReceiptPrint.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepReceiptPrint.jasper";
						
						if (!new java.io.File(filename).exists()) {
							
							filename = servlet.getServletContext().getRealPath("jreports/ArRepReceiptPrint.jasper");
							
						}						
					}
					
            		
            		
            		
            	}
            	   
		        
	        	    				
				try {
					   
            
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepReceiptPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ArRepReceiptPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("arRepReceiptPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("arRepReceiptPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ArRepReceiptPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
   
   
   private String convertLessThanOneThousand(int number, String[] tensNames, String[] numNames) {
   	String soFar;
   	
   	if (number % 100 < 20){
   		soFar = numNames[number % 100];
   		number /= 100;
   	}
   	else {
   		soFar = numNames[number % 10];
   		number /= 10;
   		
   		soFar = tensNames[number % 10] + soFar;
   		number /= 10;
   	}
   	if (number == 0) return soFar;
   	return numNames[number] + " Hundred" + soFar;
   }
   
   private String convert(int number, String[] majorNames, String[] tensNames, String[] numNames) {
   	/* special case */
   	if (number == 0) { return "zero"; }
   	
   	String prefix = "";
   	
   	if (number < 0) {
   		number = -number;
   		prefix = "Negative";
   	}
   	
   	String soFar = "";
   	int place = 0;
   	
   	do {
   		int n = number % 1000;
   		if (n != 0){
   			String s = this.convertLessThanOneThousand(n, tensNames, numNames);
   			soFar = s + majorNames[place] + soFar;
   		}
   		place++;
   		number /= 1000;
   	} while (number > 0);
   	
   	return (prefix + soFar).trim();
   }    
   
}
