/*
 * opt/jreport/ArRepReceiptPrintData.java
 *
 * Created on May 14, 2004, 3:40 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.ar.receiptprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArModReceiptDetails;

public class ArRepReceiptPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepReceiptPrintDS(ArrayList list) {
	   double totalAmount = 0d;
		double totalAmountASD = 0d;
		double totalAmountRPT =0d;
		double totalAmountPD =0d;
		double totalAmountWTR =0d;
		double totalAmountOTH =0d;

		String RCT_TYP="";


	   System.out.println("ArRepReceiptPrintDS list="+list.size());
   	  Iterator i = list.iterator();
   	  while(i.hasNext()) {

        ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();

        totalAmountASD += mdetails.getRctInvoiceType().equals("ASD")? new Double(mdetails.getRctAmount()) : 0d;
        totalAmountRPT += mdetails.getRctInvoiceType().equals("RPT")? new Double(mdetails.getRctAmount()) : 0d;
        totalAmountPD += mdetails.getRctInvoiceType().equals("PD")? new Double(mdetails.getRctAmount()) : 0d;
        totalAmountWTR += mdetails.getRctInvoiceType().equals("WTR")? new Double(mdetails.getRctAmount()) : 0d;
        totalAmountOTH += mdetails.getRctInvoiceType().equals("OTH")? new Double(mdetails.getRctAmount()) : 0d;


        System.out.println("totalAmount="+totalAmount);
        System.out.println("totalAmountASD="+totalAmountASD);
        ArRepReceiptPrintData arRepRPData = new ArRepReceiptPrintData(
        		mdetails.getRctCstName(),
        		mdetails.getRctCstAddress(),
        		mdetails.getRctDescription(),
        		mdetails.getRctAmount(),
        		totalAmountASD,
        		totalAmountRPT,
        		totalAmountPD,
        		totalAmountWTR,
        		totalAmountOTH,
        		mdetails.getRctAmountInWords(),
        		Common.convertSQLDateToString(mdetails.getRctDate()),
        		mdetails.getRctNumber(),
        		mdetails.getRctReferenceNumber(),
        		mdetails.getRctShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),

        		mdetails.getRctIlName(),
        		mdetails.getRctIlDescription(),

        		mdetails.getRctIlItemPropertyCode(),
        		mdetails.getRctIlItemSerialNumber(),
        		mdetails.getRctIlItemSpecs(),
        		mdetails.getRctIlItemCustodian(),
        		mdetails.getRctIlItemExpiryDate(),

        		new Double(mdetails.getRctIlQuantity()),
        		mdetails.getRctIlUomShortName(),
        		new Double(mdetails.getRctIlUnitPrice()),
        		new Double(mdetails.getRctIlAmount()),

        		mdetails.getRctCstCustomerCode(),
        		mdetails.getRctAiInvoiceNumber(),
        		new Double(mdetails.getRctAiApplyAmount()),
        		mdetails.getRctSlpSalespersonCode(),
        		mdetails.getRctCstCity(),
        		mdetails.getRctBaBankName(),
        		mdetails.getRctPaymentMethod(),
        		mdetails.getRctCheckNo(),
        		new Double(mdetails.getRctTotalTax()),
        		new Double(mdetails.getRctTcRate()),

        		new Double(mdetails.getRctIlUnitPriceWoVat()),
        		new Double(mdetails.getRctIliDiscount1()),
        		new Double(mdetails.getRctIliDiscount2()),
        		new Double(mdetails.getRctIliDiscount3()),
        		new Double(mdetails.getRctIliDiscount4()),
        		mdetails.getRctIliDiscount(),
        		mdetails.getRctCstTin(),
        		new Double(mdetails.getRctIliTotalDiscount()),
        		new Double(mdetails.getRctIlTax()),
        		mdetails.getRctIlAdLvCategory(),

        		mdetails.getRctChequeNumber(),
        		mdetails.getRctVoucherNumber(),
        		mdetails.getRctCardNumber1(),
        		mdetails.getRctCardNumber2(),
        		mdetails.getRctCardNumber3(),
        		new Double(mdetails.getRctAmountCash()),
        		new Double(mdetails.getRctAmountCheque()),
        		new Double(mdetails.getRctAmountVoucher()),
        		new Double(mdetails.getRctAmountCard1()),
        		new Double(mdetails.getRctAmountCard2()),
        		new Double(mdetails.getRctAmountCard3())

        		);
		        data.add(arRepRPData);


   	  }



   }


   public boolean next() throws JRException {

      index++;
      return (index < data.size());

   }

   public Object getFieldValue(JRField field) throws JRException {

      Object value = null;

      String fieldName = field.getName();

      if("customerName".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getCustomerName();
      }else if("address".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getAddress();
      }else if("description".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getDescription();
      }else if("amount".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getAmount();
      }else if("amountASD".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountASD();
      }else if("amountRPT".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountRPT();
      }else if("amountPD".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountPD();
      }else if("amountWTR".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountWTR();
      }else if("amountOTH".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountOTH();
      }else if("date".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getDate();
      }else if("receiptNumber".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getReceiptNumber();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepReceiptPrintData)data.get(index)).getReferenceNumber();
      }else if("showDuplicate".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getShowDuplicate();
      }else if("amountInWords".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getAmountInWords();
      }else if("itemName".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getItemName();
      }else if("itemDescription".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getItemDescription();
      }else if("itemPropertyCode".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getItemPropertyCode();
      }else if("itemSerialNumber".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getItemSerialNumber();
      }else if("itemSpecs".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getItemSpecs();
      }else if("itemCustodian".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getItemCustodian();
      }else if("itemExpiryDate".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getItemExpiryDate();
      }else if("quantity".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getQuantity();
      }else if("unitOfMeasureShortName".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getUnitOfMeasureShortName();
      }else if("unitPrice".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getUnitPrice();
      }else if("itemAmount".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getItemAmount();
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getCustomerCode();
      }else if("invoiceNumber".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getInvoiceNumber();
      }else if("applyAmount".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getApplyAmount();
      }else if("salesperson".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getSalesperson();
      }else if("customerCity".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getCustomerCity();
      }else if("bankName".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getBankName();
      }else if("paymentMethod".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getPaymentMethod();
      }else if("checkNo".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getCheckNo();
      }else if("totalTax".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getTotalTax();
      }else if("taxRate".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getTaxRate();
      }else if("unitPriceWoVat".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getUnitPriceWoVat();
      }else if("discount1".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getDiscount1();
      }else if("discount2".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getDiscount2();
      }else if("discount3".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getDiscount3();
      }else if("discount4".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getDiscount4();
      }else if("discount".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getDiscount();
      }else if("customerTin".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getCustomerTin();
      }else if("totalDiscount".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getTotalDiscount();
      }else if("itemTax".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getItemTax();
      }else if("itemCategory".equals(fieldName)){
        value = ((ArRepReceiptPrintData)data.get(index)).getItemCategory();
      }else if("chequeNumber".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getChequeNumber();
      }else if("voucherNumber".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getVoucherNumber();
      }else if("cardNumber1".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getCardNumber1();
      }else if("cardNumber2".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getCardNumber2();
      }else if("cardNumber3".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getCardNumber3();
      }else if("amountCash".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountCash();
      }else if("amountCheque".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountCheque();
      }else if("amountVoucher".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountVoucher();
      }else if("amountCard1".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountCard1();
      }else if("amountCard2".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountCard2();
      }else if("amountCard3".equals(fieldName)){
          value = ((ArRepReceiptPrintData)data.get(index)).getAmountCard3();





      }

      return(value);
   }
}
