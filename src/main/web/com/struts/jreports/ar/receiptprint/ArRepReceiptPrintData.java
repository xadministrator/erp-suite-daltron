package com.struts.jreports.ar.receiptprint;

import java.util.Comparator;

import com.util.ArRepSalesDetails;

public class ArRepReceiptPrintData implements java.io.Serializable {

   private String customerName = null;
   private String address = null;
   private String description = null;
   private Double amount = null;
   private Double amountASD = null;
   private Double amountRPT = null;
   private Double amountPD = null;
   private Double amountWTR = null;
   private Double amountOTH = null;
   private String amountInWords = null;
   private String date = null;
   private String receiptNumber = null;
   private String referenceNumber = null;

   private Boolean showDuplicate = null;

   private String itemName = null;
   private String itemDescription = null;

   private String itemPropertyCode = null;
   private String itemSerialNumber = null;
   private String itemSpecs = null;
   private String itemCustodian = null;
   private String itemExpiryDate = null;

   private Double quantity = null;
   private String unitOfMeasureShortName = null;
   private Double unitPrice = null;
   private Double itemAmount = null;
   private String customerCode = null;
   private String invoiceNumber = null;
   private Double applyAmount = null;
   private String salesperson = null;
   private String customerCity = null;
   private String bankName = null;
   private String paymentMethod = null;
   private String checkNo = null;

   private Double totalTax = null;
   private Double taxRate = null;
   private Double unitPriceWoVat = null;
   private Double discount1 = null;
   private Double discount2 = null;
   private Double discount3 = null;
   private Double discount4 = null;
   private String discount = null;

   private String customerTin = null;
   private Double totalDiscount = null;

   private Double itemTax = null;
   private String itemCategory = null;

	private String chequeNumber = null;
	private String voucherNumber = null;
	private String cardNumber1 = null;
	private String cardNumber2 = null;
	private String cardNumber3 = null;


   private Double amountCash = null;
	private Double amountCheque = null;
	private Double amountVoucher = null;
	private Double amountCard1 = null;
	private Double amountCard2 = null;
	private Double amountCard3 = null;

   public ArRepReceiptPrintData(String customerName, String address, String description,
		   Double amount, Double amountASD, Double amountRPT, Double amountPD, Double amountWTR, Double amountOTH,
        String amountInWords, String date, String receiptNumber, String referenceNumber, Boolean showDuplicate,
		String itemName, String itemDescription,
		String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,

		Double quantity, String unitOfMeasureShortName, Double unitPrice,
		Double itemAmount, String customerCode, String invoiceNumber, Double applyAmount, String salesperson,
		String customerCity, String bankName, String paymentMethod, String checkNo,  Double totalTax, Double taxRate, Double unitPriceWoVat,
		Double discount1, Double discount2, Double discount3, Double discount4, String discount,
		String customerTin, Double totalDiscount, Double itemTax, String itemCategory,

		String chequeNumber, String voucherNumber, String cardNumber1, String cardNumber2, String cardNumber3,
		Double amountCash, Double amountCheque, Double amountVoucher, Double amountCard1, Double amountCard2, Double amountCard3
		   ) {

      	this.customerName = customerName;
      	this.address = address;
      	this.description = description;
      	this.amount = amount;
      	this.amountASD = amountASD;
      	this.amountRPT = amountRPT;
      	this.amountPD = amountPD;
      	this.amountWTR = amountWTR;
      	this.amountOTH = amountOTH;
      	this.amountInWords = amountInWords;
      	this.date = date;
      	this.receiptNumber = receiptNumber;
      	this.referenceNumber = referenceNumber;
      	this.showDuplicate = showDuplicate;

      	this.itemName = itemName;
      	this.itemDescription = itemDescription;
      	this.itemPropertyCode = itemPropertyCode;
      	this.itemSerialNumber = itemSerialNumber;
      	this.itemSpecs = itemSpecs;
      	this.itemCustodian = itemCustodian;
      	this.itemExpiryDate = itemExpiryDate;
      	this.quantity = quantity;
      	this.unitOfMeasureShortName = unitOfMeasureShortName;
      	this.unitPrice = unitPrice;
      	this.itemAmount = itemAmount;

      	this.customerCode = customerCode;
      	this.invoiceNumber = invoiceNumber;
      	this.applyAmount = applyAmount;
      	this.salesperson = salesperson;
      	this.customerCity = customerCity;
      	this.bankName = bankName;
      	this.paymentMethod = paymentMethod;
      	this.checkNo = checkNo;

      	this.totalTax = totalTax;
      	this.taxRate = taxRate;
      	this.unitPriceWoVat = unitPriceWoVat;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.discount = discount;
		this.customerTin = customerTin;
		this.totalDiscount = totalDiscount;
		this.itemTax = itemTax;
		this.itemCategory = itemCategory;

		this.chequeNumber = chequeNumber;
		this.voucherNumber = voucherNumber;
		this.cardNumber1 = cardNumber1;
		this.cardNumber2 = cardNumber2;
		this.cardNumber3 = cardNumber3;

		this.amountCash = amountCash;
		this.amountCheque = amountCheque;
		this.amountVoucher = amountVoucher;
		this.amountCard1 = amountCard1;
		this.amountCard2 = amountCard2;
		this.amountCard3 = amountCard3;

   }

	public String getCustomerName() {
		return (this.customerName);
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return (this.address);
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return (this.description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAmount() {
		return (this.amount);
	}



	public Double getAmountASD() {
		return (this.amountASD);
	}

	public Double getAmountRPT() {
		return (this.amountRPT);
	}

	public Double getAmountPD() {
		return (this.amountPD);
	}

	public Double getAmountWTR() {
		return (this.amountWTR);
	}
	public Double getAmountOTH() {
		return (this.amountOTH);
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAmountInWords() {
		return (this.amountInWords);
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public String getDate() {
		return (this.date);
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReceiptNumber() {
		return (this.receiptNumber);
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getReferenceNumber() {
		return (this.referenceNumber);
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Boolean getShowDuplicate() {

   	  return showDuplicate;

   }

	public String getItemName() {

		return itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}


	public String getItemPropertyCode() {

		return itemPropertyCode;

	}

	public String getItemSerialNumber() {
		return itemSerialNumber;
	}

	public String getItemSpecs() {
		return itemSpecs;
	}

	public String getItemCustodian() {
		return itemCustodian;
	}

	public String getItemExpiryDate() {
		return itemExpiryDate;
	}

	public Double getQuantity() {

		return quantity;

	}

	public String getUnitOfMeasureShortName() {

		return unitOfMeasureShortName;

	}

	public Double getUnitPrice() {

		return unitPrice;

	}

	public Double getItemAmount() {

		return itemAmount;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getInvoiceNumber() {

		return invoiceNumber;

	}

	public Double getApplyAmount() {

		return applyAmount;

	}

	public String getSalesperson() {

		return salesperson;

	}

	public String getCustomerCity() {

		return customerCity;

	}

	public String getBankName() {

		return bankName;

	}

	public String getPaymentMethod() {

		return paymentMethod;

	}

	public String getCheckNo() {

		return checkNo;

	}


	public Double getTotalTax() {

		return totalTax;
	}

	public Double getTaxRate() {

		return taxRate;

	}

	public Double getUnitPriceWoVat() {

		return unitPriceWoVat;

	}

	public Double getDiscount1() {

		return discount1;

	}

	public Double getDiscount2() {

		return discount2;

	}

	public Double getDiscount3() {

		return discount3;

	}

	public Double getDiscount4() {

		return discount4;

	}

	public String getDiscount() {

		return discount;

	}

	public String getCustomerTin() {

		return customerTin;

	}

	public Double getTotalDiscount() {

		return totalDiscount;

	}

	public Double getItemTax() {

		return itemTax;

	}

	public String getItemCategory() {

		return itemCategory;

	}





public String getChequeNumber() {

		return chequeNumber;

	}

	public String getVoucherNumber() {

		return voucherNumber;

	}

	public String getCardNumber1() {

		return cardNumber1;

	}

	public String getCardNumber2() {

		return cardNumber2;

	}

	public String getCardNumber3() {

	return cardNumber3;

	}



public Double getAmountCash() {

		return amountCash;

	}

public Double getAmountCheque() {

	return amountCheque;

}

public Double getAmountVoucher() {

	return amountVoucher;

}

public Double getAmountCard1() {

	return amountCard1;

}

public Double getAmountCard2() {

	return amountCard2;

}

public Double getAmountCard3() {

	return amountCard3;

}


}
