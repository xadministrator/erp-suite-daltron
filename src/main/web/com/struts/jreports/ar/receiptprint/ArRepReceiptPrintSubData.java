package com.struts.jreports.ar.receiptprint;

import java.util.Comparator;

import com.struts.jreports.gl.journalprint.GlRepJournalPrintData;
import com.util.ArRepSalesDetails;

public class ArRepReceiptPrintSubData implements java.io.Serializable {
	
	private String accountNumber = null;
	private String accountDescription = null;
	private Double debitAmount = null;
	private Double creditAmount = null;	
	private Boolean showDuplicate = null;
	private String category = null;
	private String source = null;			
	private String drclass = null;
		
	
	public ArRepReceiptPrintSubData(String accountNumber, String accountDescription, Double debitAmount,
			Double creditAmount, Boolean showDuplicate, String category, String source, String drclass) {
		
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;		
		this.showDuplicate = showDuplicate;
		this.category = category;
		this.source = source;				
		this.drclass = drclass;
		
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	
	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	
	public String getDrclass() {
		return drclass;
	}

	public void setDrclass(String drclass) {
		this.drclass = drclass;
	}
	
	public Boolean getShowDuplicate() {
		return showDuplicate;
	}

	public void setShowDuplicate(Boolean showDuplicate) {
		this.showDuplicate = showDuplicate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

		
}
