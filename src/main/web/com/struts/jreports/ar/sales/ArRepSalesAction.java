package com.struts.jreports.ar.sales;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepSalesController;
import com.ejb.txn.ArRepSalesControllerHome;
import com.struts.jreports.ar.receipteditlist.ArRepReceiptEditListSubDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepSalesAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepSalesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepSalesForm actionForm = (ArRepSalesForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_SALES_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepSales");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepSalesController EJB
*******************************************************/

         ArRepSalesControllerHome homeSLS = null;
         ArRepSalesController ejbSLS = null;       

         try {
         	
            homeSLS = (ArRepSalesControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepSalesControllerEJB", ArRepSalesControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepSalesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSLS = homeSLS.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepSalesAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR SLS Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            ArrayList list2 = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getItemName())) {

	        		criteria.put("itemName", actionForm.getItemName());

	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {

	        		criteria.put("category", actionForm.getCategory());

	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	
	        	}

	        	if (!Common.validateRequired(actionForm.getNumberFrom())) {

	        		criteria.put("numberFrom", actionForm.getNumberFrom());

	        	}

	        	if (!Common.validateRequired(actionForm.getNumberTo())) {

	        		criteria.put("numberTo", actionForm.getNumberTo());

	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	
	        	}
	        	
	        	if (actionForm.getIncludeUnservedSO()){

	        		criteria.put("includeUnservedSO", "YES");

	        	} else {

	        		criteria.put("includeUnservedSO", "NO");

	        	}
	        	
	        	if (actionForm.getIncludeUnposted()){
					
					criteria.put("includeUnposted", "YES");
					
				} else {
					
					criteria.put("includeUnposted", "NO");
					
				}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	           }
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {

	        		criteria.put("customerCode", actionForm.getCustomerCode());

	        	}

	        	if (!Common.validateRequired(actionForm.getSalesperson())) {

	        		criteria.put("salesperson", actionForm.getSalesperson());

	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getRegion())) {

	        		criteria.put("region", actionForm.getRegion());

	        	}
	        	
	        	if (actionForm.getIncludeZeroes()){

	        		criteria.put("includeZeroes", "YES");

	        	} else {

	        		criteria.put("includeZeroes", "NO");

	        	}
	        	
	        	
	        	
	        		
                	criteria.put("orderStatus", actionForm.getOrderStatus());
                	criteria.put("typeStatus", actionForm.getTypeStatus());
                	
	     
                
                System.out.println("actionForm.getOrderStatus() : " + actionForm.getOrderStatus());
                System.out.println("actionForm.getTypeStatus() : " + actionForm.getTypeStatus());
	        	
	        	

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbSLS.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrSlsListSize(); i++) {
	           	
	           		ArRepBranchSalesList brSlsList = (ArRepBranchSalesList)actionForm.getArRepBrSlsListByIndex(i);
	           	
	           		if(brSlsList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brSlsList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbSLS.executeArRepSales(actionForm.getCriteria(), branchList, actionForm.getGroupBy(), actionForm.getOrderBy(), actionForm.getIncludedCreditMemos(), actionForm.getIncludedInvoices(),actionForm.getIncludedMiscReceipts(),actionForm.getIncludedCollections(),actionForm.getShowEntries(), user.getCmpCode());

		       try {

		    	   list2 = ejbSLS.executeArRepSalesSub(actionForm.getCriteria(), branchList, user.getCmpCode());

		       } catch (GlobalNoRecordFoundException ex) {

		       }
		      
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("sales.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepSalesAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepSales");

            }		    

		    // fill report parameters, fill report to pdf and set report session
            
            String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesSub.jasper";
		       
	        if (!new java.io.File(subreportFilename).exists()) {
	       		    		    
	        	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepSalesSub.jasper");
		    
	        }
   
            JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);
		    
            
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDateTo());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
			parameters.put("viewType", actionForm.getViewType());
			parameters.put("groupBy", actionForm.getGroupBy());
			parameters.put("itemName", actionForm.getItemName());
			parameters.put("orderBy", actionForm.getOrderBy());
			parameters.put("customer", actionForm.getCustomerCode());
			parameters.put("salesperson", actionForm.getSalesperson());
			parameters.put("region", actionForm.getRegion());
			parameters.put("orderStatus", actionForm.getOrderStatus());
			parameters.put("typeStatus", actionForm.getOrderStatus());
			
			if (actionForm.getIncludeZeroes()){
				
				parameters.put("includeZeroes", "YES");
				
			} else {
				
				parameters.put("includeZeroes", "NO");
				
			}
			
			if (actionForm.getIncludeUnposted()){
				
				parameters.put("includeUnposted", "YES");
				
			} else {
				
				parameters.put("includeUnposted", "NO");
				
			}
			
			if (actionForm.getSummarize()) {

				parameters.put("summarize", "YES");

			} else {

				parameters.put("summarize", "NO");

			}

			String branchMap = null;
			boolean first = true;
			for(int j=0; j < actionForm.getArRepBrSlsListSize(); j++) {

				ArRepBranchSalesList brList = (ArRepBranchSalesList)actionForm.getArRepBrSlsListByIndex(j);

				if(brList.getBranchCheckbox() == true) {
					if(first) {
						branchMap = brList.getBranchName();
						first = false;
					} else {
						branchMap = branchMap + ", " + brList.getBranchName();
					}
				}

			}
			parameters.put("branchMap", branchMap);
			parameters.put("arRepSalesSub", subreport);
            parameters.put("arRepSalesSubDS", new ArRepSalesSubDS(list2));
            String filename = null;
           System.out.println("actionForm.getReportType()="+actionForm.getReportType());
		    if(actionForm.getReportType().equals("Van Load Report")){
		    	System.out.println("Van Load");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepVanLoad.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepVanLoad.jasper");
			    	
			    }
			    
		    } else if(actionForm.getReportType().equals("Unit Sales SI Report")){ 
		    	System.out.println("Unit Sales SI");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepUnitSalesSI.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepUnitSalesSI.jasper");
			    	
			    }
			    
		    } else if(actionForm.getReportType().equals("Unit Sales CS Report")){ 
		    	System.out.println("Unit Sales CS");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepUnitSalesCS.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepUnitSalesCS.jasper");
			    	
			    }
			    
		    } else if(actionForm.getReportType().equals("Sales Book Summary")){ 
		    	System.out.println("Sales Book Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesBookSummary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepSalesBookSummary.jasper");
			    	
			    }	
			    
		    } else if(actionForm.getReportType().equals("Sales Invoice Summary")){ 
		    	System.out.println("Sales Invoice Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesInvoiceSummary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepSalesInvoiceSummary.jasper");
			    	
			    }	
		    } else if(actionForm.getReportType().equals("Credit Memo Summary")){ 
		    	System.out.println("Credit Memo Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepCreditMemoSummary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepCreditMemoSummary.jasper");
			    	
			    }
		    } else if(actionForm.getReportType().equals("Cash Sales Summary")){ 
		    	System.out.println("Cash Sales Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepCashSalesSummary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepCashSalesSummary.jasper");
			    	
			    }
		    } else if(actionForm.getReportType().equals("Detailed Sales Summary")){ 
		    	System.out.println("Detailed Sales Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDetailedSalesSummary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepDetailedSalesSummary.jasper");
			    	
			    }
			    
		    } else if(actionForm.getReportType().equals("Detailed Sales 2 Summary")){ 
		    	System.out.println("Detailed Sales 2 Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDetailedSales2Summary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepDetailedSales2Summary.jasper");
			    	
			    }
			    
		    } else if(actionForm.getReportType().equals("Detailed Sales 3 Summary")){ 
		    	System.out.println("Detailed Sales 3 Summary");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepDetailedSales3Summary.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepDetailedSales3Summary.jasper");
			    	
			    }
			    
		    } else {
		    	System.out.println("NOT EQUAL VAN LOAD");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSales.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/ArRepSales.jasper");
			    	
			    }
         	}
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepSalesDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepSalesDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepSalesDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepSalesAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AR SLS Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AR SLS Customer Enter Action --
*******************************************************/

		     } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

		    	 return(mapping.findForward("arRepSales"));	
		          
/*******************************************************
   -- AR SLS Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.reset(mapping, request);
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCategoryList();           	
	            	
	            	list = ejbSLS.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearCustomerBatchList();           	
	         		
	         		list = ejbSLS.getAdLvCustomerBatchAll(user.getCmpCode());
	         		
	         		if (list == null || list.size() == 0) {
	         			
	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
	         			
	         		} else {
	         			
	         			i = list.iterator();
	         			
	         			while (i.hasNext()) {
	         				
	         				actionForm.setCustomerBatchList((String)i.next());
	         				
	         			}
	         			
	         		}
	            	
	            	actionForm.clearLocationList();           	
	            	
	            	list = ejbSLS.getInvLocAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setLocationList((String)i.next());
	            			
	            		}
	            		            		
	            	}   
	            	
	            	actionForm.clearArRepBrSlsList();
	            	
	            	//int ctr = 1;
	         		
	         		list = ejbSLS.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchSalesList arRepBrSlsList = new ArRepBranchSalesList(actionForm, 
	         					details.getBrBranchCode(),  details.getBrName(), details.getBrCode());

	         			if (details.getBrHeadQuarter() == 1) arRepBrSlsList.setBranchCheckbox(true);
	         			//ctr++;

	         			actionForm.saveArRepBrSlsList(arRepBrSlsList);

	         		}

	         		actionForm.clearSalespersonList();           	

	         		list = ejbSLS.getArSlpAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setSalespersonList((String)i.next());

	         			}

	         		}   

	         		actionForm.clearRegionList();           	

	         		list = ejbSLS.getAdLvArRegionAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setRegionList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				String region = (String)i.next();

	         				actionForm.setRegionList(region);

	         			}

	         		}
	         		
	         		
	         		actionForm.clearReportTypeList();
			        
			        list = ejbSLS.getAdLvReportTypeAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            	
	            		actionForm.setReportTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setReportTypeList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	

				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepSalesAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepSales"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepSalesAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
