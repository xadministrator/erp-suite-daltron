package com.struts.jreports.ar.sales;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepSalesDetails;

public class ArRepSalesDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepSalesDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepSalesDetails details = (ArRepSalesDetails)i.next();

         String group = new String("");

         if (groupBy.equals("CUSTOMER")) {
        	 group = details.getSlsCustomerName();
         } else if (groupBy.equals("CATEGORY")) {
        	 group = details.getSlsItemAdLvCategory();
         } else if (groupBy.equals("ITEM")) {
        	 group = details.getSlsItemName() + " - " + details.getSlsItemDescription();
         }
                  
	     ArRepSalesData argData = new ArRepSalesData(details.getSlsItemName(), details.getSlsItemDescription(),
	     		Common.convertSQLDateToString(details.getSlsDate()), details.getSlsDocumentNumber(),details.getSlsShowEntries(), details.getSlsSource(), details.getSlsAdBranch(),
				new Double(details.getSlsQuantitySold()), new Double(details.getSlsUnitPrice()), 
				new Double(details.getSlsAmount()), new Double(details.getSlsOutputVat()),
				details.getSlsCustomerCode(), details.getSlsUnit(), details.getSlsCustomerName(),
				new Double(details.getSlsGrossUnitPrice()), new Double(details.getSlsDiscount()), group, details.getSlsItemAdLvCategory(),
				details.getSlsCustomerAddress(), details.getSlsCmInvoiceNumber(), details.getSlsSalespersonCode(), details.getSlsSalespersonName() ,details.getSlsSalesOrderNumber(), 
				Common.convertSQLDateToString(details.getSlsSalesOrderDate()), new Double(details.getSlsSalesOrderQuantity()),
				new Double(details.getSlsSalesOrderAmount()), details.getSlsCustomerRegion(), details.getSlsCustomerStateProvince(), 
				details.getSlsCustomerClass(), Common.convertSQLDateToString(details.getSlsEffectivityDate()), details.getSlsDescription(),
				details.getSlsOrderStatus(), details.getSlsReferenceNumber(), new Double(details.getSlsSalesOrderSalesPrice()), new Double(details.getSlsDefaultUnitPrice()));
	      
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("itemName".equals(fieldName)){
         value = ((ArRepSalesData)data.get(index)).getItemName();       
      }else if("itemDescription".equals(fieldName)){
         value = ((ArRepSalesData)data.get(index)).getItemDescription();
      }else if("date".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getDate();
      }else if("documentNumber".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getDocumentNumber();
      }else if("showEntries".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getShowEntries();
          System.out.println("value showEntries--"+value);
      }else if("source".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getSource();
          System.out.println("value showEntries--"+value);
      }else if("adBranch".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getAdBranch();
      }else if("quantitySold".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getQuantitySold();
      }else if("unitPrice".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getUnitPrice();  
      }else if("amount".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getAmount(); 
      }else if("outputVat".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getOutputVat();  
      }else if("customerCode".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getCustomerCode();  
      }else if("unit".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getUnit();  
      }else if("customerName".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getCustomerName();  
      }else if("grossUnitPrice".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getGrossUnitPrice();  
      }else if("discount".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getDiscount();  
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getGroupBy();  
      }else if("itemCategory".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getItemCategory();  
      }else if("customerAddress".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getCustomerAddress();  
      }else if("cmInvoiceNumber".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getCmInvoiceNumber();
      }else if("salespersonCode".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalespersonCode();  
      }else if("salespersonName".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalespersonName();  
      }else if("salesOrderNumber".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalesOrderNumber();  
      }else if("salesOrderDate".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalesOrderDate();  
      }else if("salesOrderQuantity".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalesOrderQuantity();  
      }else if("salesOrderAmount".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalesOrderAmount();  
      }else if("salesOrderSalesPrice".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getSalesOrderSalesPrice();  
      }else if("customerStateProvince".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getCustomerStateProvince();  
      }else if("customerRegion".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getCustomerRegion();  
      }else if("customerClass".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getCustomerClass();
      }else if("effectivityDate".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getEffectivityDate();   
      }else if("salesDescription".equals(fieldName)){
          value = ((ArRepSalesData)data.get(index)).getSalesDescription();   
      }else if("orderStatus".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getOrderStatus();  
      }else if("referenceNumber".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getReferenceNumber();  
        //System.out.println("value: " +value);
      }else if("defaultUnitPrice".equals(fieldName)){
        value = ((ArRepSalesData)data.get(index)).getDefaultUnitPrice();  
      }

      return(value);
   }
}
