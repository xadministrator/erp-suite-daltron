package com.struts.jreports.ar.sales;


public class ArRepSalesData implements java.io.Serializable {

   private String itemName = null;
   private String itemDescription = null;
   private String date = null;
   private String documentNumber = null;
   private boolean showEntries = false;
   private String source = null;
   private Integer adBranch = 0;
   private Double quantitySold = null;
   private Double unitPrice = null;
   private Double amount = null;
   private Double outputVat = null;
   private String customerCode = null;
   private String unit = null;
   private String customerName = null;
   private Double grossUnitPrice = null;
   private Double discount = null;
   private String groupBy = null;
   private String itemCategory = null;
   private String customerAddress = null;
   private String cmInvoiceNumber = null;
   private String salespersonCode = null;
   private String salespersonName = null;
   private String salesOrderNumber = null;
   private String salesOrderDate = null;
   private Double salesOrderQuantity = null;
   private Double salesOrderAmount = null;
   private Double salesOrderSalesPrice = null;
   private Double defaultUnitPrice = null;
   
   private String customerRegion = null;   
   private String customerStateProvince = null;   
   private String customerClass = null; 
   private String effectivityDate = null;
   private String salesDescription = null;
   
   private String orderStatus = null;
   private String referenceNumber = null;
   
   public ArRepSalesData(String itemName, String itemDescription, String date, String documentNumber, boolean showEntries, String source, Integer adBranch,
   		Double quantitySold, Double unitPrice, Double amount, Double outputVat, String customerCode,
		String unit, String customerName, Double grossUnitPrice, Double discount, String groupBy,
		String itemCategory, String customerAddress, String cmInvoiceNumber, String salespersonCode, String salespersonName, String salesOrderNumber,
		String salesOrderDate, Double salesOrderQuantity, Double salesOrderAmount, String customerRegion, String customerStateProvince, 
		String customerClass, String effectivityDate, String salesDescription,
		String orderStatus, String referenceNumber, Double salesOrderSalesPrice, Double defaultUnitPrice) {
      	
      	this.itemName = itemName;
      	this.itemDescription = itemDescription;
      	this.date = date;
      	this.documentNumber = documentNumber;
      	this.showEntries = showEntries;
      	this.source = source;
      	this.adBranch = adBranch;
      	this.quantitySold = quantitySold;
      	this.unitPrice = unitPrice;
      	this.amount = amount;
      	this.outputVat = outputVat;
      	this.customerCode = customerCode;
      	this.unit = unit;
      	this.customerName = customerName;
      	this.grossUnitPrice = grossUnitPrice;
      	this.discount = discount;
      	this.groupBy = groupBy;
      	this.itemCategory = itemCategory;
      	this.customerAddress = customerAddress;
      	this.cmInvoiceNumber = cmInvoiceNumber;
      	this.salespersonCode = salespersonCode;
      	this.salespersonName = salespersonName;
      	this.salesOrderNumber = salesOrderNumber;
      	this.salesOrderDate = salesOrderDate;
      	this.salesOrderQuantity = salesOrderQuantity;
      	this.salesOrderAmount = salesOrderAmount;
      	this.customerRegion = customerRegion;
      	this.customerStateProvince = customerStateProvince;
      	this.customerClass = customerClass;
      	this.effectivityDate = effectivityDate;
      	this.salesDescription = salesDescription;
      	this.orderStatus = orderStatus;
      	this.referenceNumber = referenceNumber;      	
      	this.salesOrderSalesPrice = salesOrderSalesPrice;
      	this.defaultUnitPrice = defaultUnitPrice;
   }
 
   public Double getAmount() {
   	
   	 return amount;
   
   }
   
   public String getDate() {
   
   	 return date;
   
   }
   
   public String getDocumentNumber() {
   
   	 return documentNumber;
   
   }
   
   public boolean getShowEntries() {
	   
   	 return showEntries;
   
   }
   
   public String getSource() {
	   
	   	 return source;
	   	 
   }

   public Integer getAdBranch(){
	   
	   return adBranch;
   }
   
   public String getItemDescription() {
   
   	 return itemDescription;
   
   }
   
   public String getItemName() {
   
   	 return itemName;
   
   }
   
   public Double getOutputVat() {
   
   	 return outputVat;
   
   }
   
   public Double getQuantitySold() {
   
   	 return quantitySold;
   
   }
   
   public Double getUnitPrice() {
   
   	 return unitPrice;
   
   }
   
   public String getCustomerCode() {
    
      return customerCode;
    
   }
   
   public String getUnit() {
   	
   	  return unit;
   	
   }
   
   public String getCustomerName() {
	   
	   return customerName;
	   
   }
   
   public Double getGrossUnitPrice() {
	   
	   return grossUnitPrice;
	   
   }
   
   public Double getDiscount() {
	   
	   return discount;
	   
   }
   
   public String getGroupBy() {
	   
	   return groupBy;
	   
   }
   
   public String getItemCategory() {
	   
	   return itemCategory;
	   
   }
   
   public String getCustomerAddress() {
	   
	   return customerAddress;
	   
   }
   
   public String getCmInvoiceNumber() {
	   
	   return cmInvoiceNumber;
	   
   }
   
   public String getSalespersonCode() {
	   
	   return salespersonCode;
	   
   }
   
   public String getSalespersonName() {
	   
	   return salespersonName;
	   
   }
   
   public String getSalesOrderNumber() {
	   
	   return salesOrderNumber;
	   
   }
   
   public String getSalesOrderDate() {
	   
	   return salesOrderDate;
	   
   }
   
   public Double getSalesOrderQuantity() {

	   return salesOrderQuantity;

   }
   
   public Double getSalesOrderSalesPrice() {
	   
	   return salesOrderSalesPrice;
	   
   }

   public Double getSalesOrderAmount() {

	   return salesOrderAmount;

   }
   
   public String getCustomerStateProvince() {
	   
	   return customerStateProvince;
	   
   }
   
   public String getCustomerClass() {
	   
	   return customerClass;
	   
   }
   
   public String getEffectivityDate() {
	   
	   return effectivityDate;
	   
   }
   
   public String getSalesDescription() {
	   
	   return salesDescription;
	   
   }
   
   public String getCustomerRegion() {
	   
	   return customerRegion;
	   
   }
   
   public String getOrderStatus() {
	   
	   return orderStatus;
	   
   }
   
   public String getReferenceNumber(){
	   
	   return referenceNumber;
	   
   }
   
   public double getDefaultUnitPrice(){
	   return defaultUnitPrice;
   }
}
