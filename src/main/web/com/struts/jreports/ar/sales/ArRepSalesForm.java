package com.struts.jreports.ar.sales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepSalesForm extends ActionForm implements Serializable{

	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String customerBatch = null;
	private ArrayList customerBatchList = new ArrayList();
	private String date = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String numberFrom = null;
	private String numberTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	private String itemName = null;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private boolean includeUnposted = false;
	private boolean summarize = false;
	private String customerCode = null;
	private String salesperson = null;
	private ArrayList salespersonList = new ArrayList();
	private String region = null;
	private ArrayList regionList = new ArrayList();
	private boolean includeUnservedSO = false;
	private boolean includeZeroes = false;
	
	
	private boolean includedInvoices = false;
	private boolean includedCreditMemos = false;
	private boolean includedMiscReceipts = false;
	private boolean includedCollections = false;
	private boolean showEntries = false;
	
	
	private String orderStatus = null;
	private ArrayList orderStatusList = new ArrayList();
	
	private String typeStatus = null;
	private ArrayList typeStatusList = new ArrayList();
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();
	
	private HashMap criteria = new HashMap();
	private ArrayList arRepBrSlsList = new ArrayList();

	private String userPermission = new String();

	public HashMap getCriteria() {

		return criteria;

	}

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public void setGoButton(String goButton) {

		this.goButton = goButton;

	}

	public void setCloseButton(String closeButton) {

		this.closeButton = closeButton;

	}

	public String getDateFrom() {

		return (dateFrom);

	}

	public void setDateFrom(String dateFrom) {

		this.dateFrom = dateFrom;

	}

	public String getDateTo() {

		return (dateTo);

	}

	public void setDateTo(String dateTo) {

		this.dateTo = dateTo;

	}

	public String getNumberFrom() {

		return (numberFrom);

	}

	public void setNumberFrom(String numberFrom) {

		this.numberFrom = numberFrom;

	}

	public String getNumberTo() {

		return (numberTo);

	}

	public void setNumberTo(String numberTo) {

		this.numberTo = numberTo;

	}
	
	public String getCategory() {

		return(category);

	}

	public void setCategory(String category) {

		this.category = category;

	}

	public ArrayList getCategoryList() {

		return(categoryList);

	}

	public void setCategoryList(String category) {

		categoryList.add(category);

	}

	public void clearCategoryList() {

		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);

	}

	public String getLocation() {

		return(location);

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return(locationList);

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getItemClass() {

		return itemClass;

	}

	public void setItemClass(String itemClass) {

		this.itemClass = itemClass;

	}

	public ArrayList getItemClassList() {

		return itemClassList;

	}
	
	
	public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }

	public String getViewType() {

		return(viewType);

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getUserPermission() {

		return(userPermission);

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public Object[] getArRepBrSlsList(){

		return arRepBrSlsList.toArray();

	}

	public ArRepBranchSalesList getArRepBrSlsListByIndex(int index){

		return ((ArRepBranchSalesList)arRepBrSlsList.get(index));

	}

	public int getArRepBrSlsListSize(){

		return(arRepBrSlsList.size());

	}

	public void saveArRepBrSlsList(Object newArRepBrSlsList){

		arRepBrSlsList.add(newArRepBrSlsList);   	  

	}

	public void clearArRepBrSlsList(){

		arRepBrSlsList.clear();

	}

	public String getGroupBy() {

		return(groupBy);

	}

	public void setGroupBy(String groupBy) {

		this.groupBy = groupBy;

	}

	public ArrayList getGroupByList() {

		return groupByList;

	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
		
	}
	
	public String getOrderBy() {

		return(orderBy);

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}
	
	public boolean getIncludeUnposted() {
		
		return(includeUnposted);
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public boolean getSummarize() {

		return summarize;

	}

	public void setSummarize(boolean summarize) {

		this.summarize = summarize;

	}
	
	public String getCustomerCode() {
		
		return customerCode;
		
	}
	
	public void setCustomerCode(String customerCode) {
		
		this.customerCode = customerCode;
		
	}
	
	public String getSalesperson(){
		
		return salesperson;
		
	}
	
	public void setSalesperson(String salesperson){
		
		this.salesperson = salesperson;
		
	}
	
	public ArrayList getSalespersonList(){
		
		return salespersonList;
		
	}
	
	public void setSalespersonList(String salesperson){
		
		salespersonList.add(salesperson);
		
	}
	
	public void clearSalespersonList(){
		
		salespersonList.clear();
		salespersonList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getRegion() {
		
		return region;
		
	}
	
	public void setRegion(String region) {
		
		this.region = region;
		
	}

	public ArrayList getRegionList(){
		
		return regionList;
		
	}

	public void setRegionList(String region){
		
		regionList.add(region);
		
	}

	public void clearRegionList(){
		
		regionList.clear();
		regionList.add(Constants.GLOBAL_BLANK);
		
	} 
	

	
	public boolean getIncludeUnservedSO() {
		
		return includeUnservedSO;
		
	}
	
	public void setIncludeUnservedSO(boolean includeUnservedSO) {
		
		this.includeUnservedSO = includeUnservedSO;
		
	}
	
	public boolean getIncludeZeroes() {
		
		return includeZeroes;
		
	}
	
	public void setIncludeZeroes(boolean includeZeroes) {
		
		this.includeZeroes = includeZeroes;
		
	}
	
	public boolean getIncludedInvoices() {

		return includedInvoices;

	}

	public void setIncludedInvoices(boolean includedInvoices) {

		this.includedInvoices = includedInvoices;

	}

	public boolean getIncludedCreditMemos() {

		return includedCreditMemos;

	}

	public void setIncludedCreditMemos(boolean includedCreditMemos) {

		this.includedCreditMemos = includedCreditMemos;

	}

	public boolean getIncludedMiscReceipts() {

		return includedMiscReceipts;

	}

	public void setIncludedMiscReceipts(boolean includedMiscReceipts) {

		this.includedMiscReceipts = includedMiscReceipts;

	}
	
	public boolean getIncludedCollections() {

		return includedCollections;

	}

	public void setIncludedCollections(boolean includedCollections) {

		this.includedCollections = includedCollections;

	}
	
	public boolean getShowEntries() {

		return showEntries;

	}

	public void setShowEntries(boolean showEntries) {

		this.showEntries = showEntries;

	}
	
	
	public String getOrderStatus(){
		return(orderStatus);
	}

	public void setOrderStatus(String orderStatus){
		this.orderStatus = orderStatus;
	}

	public ArrayList getOrderStatusList(){
		return(orderStatusList);
	}
	
	public String getTypeStatus(){
		return(typeStatus);
	}

	public void setTypeStatus(String typeStatus){
		this.typeStatus = typeStatus;
	}

	public ArrayList getTypeStatusList(){
		return(typeStatusList);
	}
	
	
	

	
	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<arRepBrSlsList.size(); i++) {
			
			ArRepBranchSalesList actionList = (ArRepBranchSalesList)arRepBrSlsList.get(i);
			actionList.setBranchCheckbox(false);
			
		}
		
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		category = Constants.GLOBAL_BLANK;
		location = Constants.GLOBAL_BLANK;
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;
		dateFrom = Common.convertSQLDateToString(new Date());
		dateTo = Common.convertSQLDateToString(new Date());
		numberFrom = null;
		numberTo = null;
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add("ITEM");
		groupByList.add("CUSTOMER");
		groupByList.add("CATEGORY");
		groupByList.add("SALES ORDER");
		groupByList.add("UNIT");
		groupByList.add("USR-SI");
		groupByList.add("USR-CS");
		groupByList.add("SIS");
		groupByList.add("SBS");
		groupByList.add("CMS");
		groupByList.add("CSS");
		groupByList.add("DSS");
		groupByList.add("DSS2");
		groupByList.add("DSS3");
		groupBy = "ITEM";
		itemName = null;
		orderByList.clear();
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add("CUSTOMER");
		orderByList.add("DATE");
		orderByList.add("BRANCH");
		orderBy = "";
		includeUnposted = false;
		summarize = false;
		customerCode = null;
		salesperson = null;
		region = null;
		includeUnservedSO = false;
		includeZeroes = false;
		
		includedInvoices = false;
		includedMiscReceipts = false;
		includedCreditMemos = false;
		includedCollections = false;
		showEntries = false;
		
		orderStatusList.clear();
		orderStatusList.add("");
		orderStatusList.add("Bad");
		orderStatusList.add("Good");
		orderStatus = "";
		
		typeStatusList.clear();
		typeStatusList.add("");
		typeStatusList.add("Bad Stock");
		typeStatusList.add("Good Stock");
		typeStatusList.add("Cancelled Invoice");
		typeStatus = "";

		/*reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);
		reportTypeList.add("Unit Sales SI Report");
		reportTypeList.add("Unit Sales CS Report");
		reportTypeList.add("Van Load Report");
		reportTypeList.add("Sales Invoice Summary");
		reportTypeList.add("Sales Book Summary");
		reportTypeList.add("Credit Memo Summary");
		reportTypeList.add("Cash Sales Summary");
		reportTypeList.add("Detailed Sales Summary");
		reportTypeList.add("Detailed Sales 2 Summary");
		reportTypeList.add("Detailed Sales 3 Summary");
		reportType = "";*/
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("sales.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("sales.error.dateToInvalid"));
				
			}

		}
		
		return(errors);
		
	}
	
}

