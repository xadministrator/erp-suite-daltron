package com.struts.jreports.ar.sales;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepSalesDetails;

public class ArRepSalesSubDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepSalesSubDS(ArrayList list) {

	   Iterator i = list.iterator();

	   while (i.hasNext()) {

		   ArRepSalesDetails details = (ArRepSalesDetails)i.next();

		   ArRepSalesSubData argData = new ArRepSalesSubData(details.getSlsCustomerCode(), details.getSlsCustomerName(), 
				   details.getSlsItemAdLvCategory(), details.getSlsItemName(), details.getSlsItemDescription(), details.getSlsUnit(), 
				   new Double(details.getSlsQuantitySold()), new Double(details.getSlsUnitPrice()), 
				   new Double(details.getSlsAmount()), new Double(details.getSlsOutputVat()),
				   new Double(details.getSlsGrossUnitPrice()), new Double(details.getSlsDiscount()), 
				   new Double(details.getSlsSalesOrderQuantity()),new Double(details.getSlsDefaultUnitPrice()));

		   data.add(argData);
	   }

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
	   Object value = null;

	   String fieldName = field.getName();

	   if("customerCode".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getCustomerCode();  
	   }else if("customerName".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getCustomerName();  
	   }else if("itemCategory".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getItemCategory();       
	   }else if("itemName".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getItemName();       
	   }else if("itemDescription".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getItemDescription();
	   }else if("quantitySold".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getQuantitySold();
	   }else if("unit".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getUnit();  
	   }else if("unitPrice".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getUnitPrice();  
	   }else if("amount".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getAmount(); 
	   }else if("outputVat".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getOutputVat();  
	   }else if("grossUnitPrice".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getGrossUnitPrice();  
	   }else if("discount".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getDiscount();  
	   }else if("soQuantity".equals(fieldName)){
		   value = ((ArRepSalesSubData)data.get(index)).getSoQuantity();  
	   }else if("defaultUnitPrice".equals(fieldName)){
		   value = ((ArRepSalesData)data.get(index)).getDefaultUnitPrice();  
	   }

	   return(value);
   }
}
