package com.struts.jreports.ar.sales;


public class ArRepSalesSubData implements java.io.Serializable {

	private String customerCode = null;
	private String customerName = null;
	private String itemCategory = null;
	private String itemName = null;
	private String itemDescription = null;
	private String unit = null;
	private Double quantitySold = null;
	private Double unitPrice = null;
	private Double amount = null;
	private Double outputVat = null;
	private Double grossUnitPrice = null;
	private Double discount = null;
	private Double soQuantity = null;
	private Double defaultUnitPrice = null;
	
	public ArRepSalesSubData(String customerCode, String customerName, String itemCategory,  
			String itemName, String itemDescription, String unit, Double quantitySold, 
			Double unitPrice, Double amount, Double outputVat, Double grossUnitPrice, Double discount, 
			Double soQuantity, Double defaultUnitPrice) {

		this.customerCode = customerCode;
		this.customerName = customerName;
		this.itemCategory = itemCategory;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.unit = unit;
		this.quantitySold = quantitySold;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.outputVat = outputVat;
		this.grossUnitPrice = grossUnitPrice;
		this.discount = discount;
		this.soQuantity = soQuantity;
		this.defaultUnitPrice = defaultUnitPrice;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getCustomerName() {

		return customerName;

	}
	
	public String getItemCategory() {
		
		return itemCategory;
		
	}

	public String getItemName() {

		return itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public String getUnit() {

		return unit;

	}

	public Double getQuantitySold() {

		return quantitySold;

	}

	public Double getUnitPrice() {

		return unitPrice;

	}

	public Double getAmount() {

		return amount;

	}

	public Double getOutputVat() {

		return outputVat;

	}

	public Double getGrossUnitPrice() {

		return grossUnitPrice;

	}

	public Double getDiscount() {

		return discount;

	}

	public Double getSoQuantity() {

		return soQuantity;

	}	
	
	public Double getDefaultUnitPrice() {

		return defaultUnitPrice;

	}	

}
