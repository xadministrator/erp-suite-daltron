package com.struts.jreports.ar.salesorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepSalesOrderController;
import com.ejb.txn.ArRepSalesOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepSalesOrderAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepSalesOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepSalesOrderForm actionForm = (ArRepSalesOrderForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_SALES_ORDER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepSalesOrder");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepSalesOrderController EJB
*******************************************************/

         ArRepSalesOrderControllerHome homeSO = null;
         ArRepSalesOrderController ejbSO = null;       

         try {
         	
            homeSO = (ArRepSalesOrderControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepSalesOrderControllerEJB", ArRepSalesOrderControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepSalesOrderAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSO = homeSO.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepSalesOrderAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR SO Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerType())) {
	        		
	        		criteria.put("customerType", actionForm.getCustomerType());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	           }

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumberFrom())) {
	        		
	        		criteria.put("referenceNumberFrom", actionForm.getReferenceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("referenceNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
                
                if (actionForm.getIncludedUnposted()) {
	        		
	        		criteria.put("includedUnposted", "YES");
	        		
	        	} else {
	        		
	        		criteria.put("includedUnposted", "NO");
	        		
	        	}
                	 
                if (!Common.validateRequired(actionForm.getSalesperson())) {
	        		
	        		criteria.put("salesperson", actionForm.getSalesperson());
	        		
	        	}
                
                criteria.put("approvalStatus", actionForm.getApprovalStatus());
                
                criteria.put("orderStatus", actionForm.getOrderStatus());
                
                // save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbSO.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrSrListSize(); i++) {
	           	
	           		ArRepBranchSalesOrderList brSrList = (ArRepBranchSalesOrderList)actionForm.getArRepBrSrListByIndex(i);
	           	
	           		if(brSrList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brSrList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbSO.executeArRepSalesOrder(actionForm.getCriteria(), branchList, actionForm.getInvoiceStatus(),
            	    actionForm.getOrderBy(), actionForm.getGroupBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("salesOrder.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepSalesOrderAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepSalesOrder");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("orderBy", actionForm.getOrderBy());
			parameters.put("groupBy", actionForm.getGroupBy());
			parameters.put("approvalStatus", actionForm.getApprovalStatus());
			parameters.put("viewType", actionForm.getViewType());
			//parameters.put("orderStatus", actionForm.getOrderStatus());
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getCustomerType() != null) {
			
				parameters.put("customerType", actionForm.getCustomerType());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", actionForm.getDateFrom());				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", actionForm.getDateTo());	    	
			
		    }
	 
			parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
		    parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
		    
            if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }

		    if (actionForm.getSalesperson() != null) {
				
				parameters.put("salesperson", actionForm.getSalesperson());	
		    
		    }
		    
		    parameters.put("source", "Sales Order");
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepBrSrListSize(); j++) {

		    	ArRepBranchSalesOrderList brList = (ArRepBranchSalesOrderList)actionForm.getArRepBrSrListByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBranchName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBranchName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesOrder.jasper";
			       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	        	filename = servlet.getServletContext().getRealPath("jreports/ArRepSalesOrder.jasper");
		    
	        }
  	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepSalesOrderDS(list, actionForm.getGroupBy())));   
				   
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepSalesOrderDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepSalesOrderDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepSalesOrderAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        ex.printStackTrace();
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		   
/*******************************************************
   -- AR SO Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		     	  
		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AR SO Customer Enter Action --
 *******************************************************/
		          
		     } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {
		     	
		     	return(mapping.findForward("arRepSalesOrder"));		          
		          
/*******************************************************
   -- AR SO Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.reset(mapping, request);
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearCustomerTypeList();           	
	            	
	            	list = ejbSO.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
	            	actionForm.clearCustomerBatchList();           	
	         		
	         		list = ejbSO.getAdLvCustomerBatchAll(user.getCmpCode());
	         		
	         		if (list == null || list.size() == 0) {
	         			
	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
	         			
	         		} else {
	         			
	         			i = list.iterator();
	         			
	         			while (i.hasNext()) {
	         				
	         				actionForm.setCustomerBatchList((String)i.next());
	         				
	         			}
	         			
	         		}
	
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbSO.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
	            	
	            	actionForm.clearArRepBrSrList();
	         		
	         		list = ejbSO.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchSalesOrderList arRepBrSrList = new ArRepBranchSalesOrderList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			arRepBrSrList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrSrList(arRepBrSrList);
	         			
	         		}
	         		
	         		actionForm.clearSalespersonList();           	
	            	
	            	list = ejbSO.getArSlpAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSalespersonList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepSalesOrderAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepSalesOrder"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepSalesOrderAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
