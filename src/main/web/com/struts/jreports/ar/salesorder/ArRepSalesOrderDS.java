package com.struts.jreports.ar.salesorder;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepSalesOrderDetails;

public class ArRepSalesOrderDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepSalesOrderDS(ArrayList list, String groupBy) {
   	
   	  Iterator i = list.iterator();
   	
	  int ctr=0;
	  
      while (i.hasNext()) {
      	
         ArRepSalesOrderDetails details = (ArRepSalesOrderDetails)i.next();
         
         String group = null;
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getSoCstCustomerCode();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
         	
         	group = details.getSoCstCustomerType();
         	
         } else if (groupBy.equals("CUSTOMER CLASS")) {
         	
         	group = details.getSoCstCustomerClass();
         	
         }else if (groupBy.equals("SO NUMBER")) {
       	
       	   group = details.getSoDocumentNumber();
       	
         }
                  
	     ArRepSalesOrderData argData = new ArRepSalesOrderData(details.getSoDate(), 
	     details.getSoCstCustomerCode(), details.getSoDescription(),
	     details.getSoDocumentNumber(), details.getSoReferenceNumber(), 
	     new Double(details.getSoOrderQty()), new Double(details.getSoInvoiceQty()),
	     new Double(details.getSoAmount()), new Double(details.getSoTaxAmount()), group, details.getSoSlsSalespersonCode(), details.getSoSlsName(),
		 details.getSoCstName(), details.getSoCstCustomerCode2(), details.getSoOrderStatus(), details.getSolIIName(),
		 details.getSoApprovalStatus(), details.getSoApprovedRejectedBy(), details.getSoInvoiceNumbers());
		    
	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("salesOrderDate".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getSalesOrderDate();       
      }else if("customerCode".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getCustomerCode();
      }else if("description".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getDescription();   
      }else if("documentNumber".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getDocumentNumber();   
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getReferenceNumber();
      }else if("orderQty".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getOrderQty();
      }else if("invoiceQty".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getInvoiceQty();
      }else if("amount".equals(fieldName)){
         value = ((ArRepSalesOrderData)data.get(index)).getAmount();
      }else if("taxAmount".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getTaxAmount();
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getGroupBy();
      }else if("salespersonCode".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getSalespersonCode();
      }else if("salespersonName".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getSalespersonName();
      }else if("customerName".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getCustomerName();
      }else if("customerCode2".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getCustomerCode2();               
      }else if("SolIiName".equals(fieldName)){
        value =((ArRepSalesOrderData)data.get(index)).getSolIiName();                                                                                            
      }else if("orderStatus".equals(fieldName)){
        value = ((ArRepSalesOrderData)data.get(index)).getOrderStatus();
      }else if("approvalStatus".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getApprovalStatus();
      }else if("approvedRejectedBy".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getApprovedRejectedBy();
      }else if("invoiceNumbers".equals(fieldName)){
          value = ((ArRepSalesOrderData)data.get(index)).getInvoiceNumbers();
      }

      return(value);
   }
}
