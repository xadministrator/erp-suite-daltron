package com.struts.jreports.ar.salesorder;


public class ArRepSalesOrderData implements java.io.Serializable {

   private java.util.Date salesOrderDate = null;
   private String customerCode = null;
   private String description = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private Double orderQty = null;
   private Double invoiceQty = null;
   private Double amount = null;
   private Double taxAmount = null;
   private String groupBy = null;
   private String salespersonCode = null;
   private String salespersonName = null;
   private String customerName = null;
   private String customerCode2 = null;
   private String SOL_II_NM = null;
   private String orderStatus = null;
   private String approvalStatus = null;
   private String approvedRejectedBy = null;
   private String invoiceNumbers = null;
   
   public ArRepSalesOrderData(java.util.Date salesOrderDate, String customerCode,
   String description, String documentNumber, String referenceNumber,
   Double orderQty, Double invoiceQty,
   Double amount, Double taxAmount, String groupBy, String salespersonCode, 
   String salespersonName, String customerName, String customerCode2, String orderStatus, String SOL_II_NM,
   String approvalStatus, String approvedRejectedBy, String invoiceNumbers) {
      	
      	this.salesOrderDate = salesOrderDate;      	
      	this.customerCode = customerCode;
      	this.description = description;
      	this.documentNumber = documentNumber;
      	this.referenceNumber = referenceNumber;
      	this.orderQty = orderQty;
      	this.invoiceQty = invoiceQty;
      	this.amount = amount;
      	this.taxAmount = taxAmount;
      	this.groupBy = groupBy;
      	this.salespersonCode = salespersonCode;
      	this.salespersonName = salespersonName;
      	this.customerName = customerName;
      	this.customerCode2 = customerCode2;
      	this.SOL_II_NM = SOL_II_NM;
      	this.orderStatus = orderStatus;
      	this.approvalStatus = approvalStatus;
      	this.approvedRejectedBy = approvedRejectedBy;
      	this.invoiceNumbers = invoiceNumbers;

   }

   public java.util.Date getSalesOrderDate() {
   	
      return salesOrderDate;
      
   }

   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;

   }
   
   public Double getOrderQty() {
	   	
   	  return orderQty;
   	  
   }
   
   public Double getInvoiceQty() {
	   	
   	  return invoiceQty;
   	  
   }
   
      
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public Double getTaxAmount() {
	   	
   	  return taxAmount;
   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getSalespersonCode() {
   	
   	  return salespersonCode;
   	
   }
   
   public String getSalespersonName() {
   	
   	  return salespersonName;
   	
   }
   
   public String getCustomerName() {
   	
   	  return customerName;
   	
   }

   public String getCustomerCode2() {
   	
   	  return customerCode2;
   	  
   }
   
   public String getOrderStatus() {
	   	
	   	  return orderStatus;
	   	  
   }
   public String getSolIiName() {
	   	
	  return SOL_II_NM;
	   	  
   }
   
   public String getApprovalStatus() {
	   
	   return approvalStatus;
	   
   }
   
   public String getApprovedRejectedBy() {
	   
	   return approvedRejectedBy;
	   
   }
   
   public String getInvoiceNumbers() {
	   
	   return invoiceNumbers;
	   
   }
}
