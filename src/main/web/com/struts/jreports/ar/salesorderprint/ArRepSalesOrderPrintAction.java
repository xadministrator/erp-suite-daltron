package com.struts.jreports.ar.salesorderprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepSalesOrderPrintController;
import com.ejb.txn.ArRepSalesOrderPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.ReportParameter;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArRepSalesOrderPrintDetails;

public final class ArRepSalesOrderPrintAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

 /*******************************************************
      	 Check if user has a session
*******************************************************/

      	User user = (User) session.getAttribute(Constants.USER_KEY);

      	if (user != null) {

      		if (log.isInfoEnabled()) {

      			log.info("ArRepSalesOrderPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());

      		}

      	} else {

      		if (log.isInfoEnabled()) {

      			log.info("User is not logged on in session" + session.getId());

      		}

      		return(mapping.findForward("adLogon"));

      	}

      	ArRepSalesOrderPrintForm actionForm = (ArRepSalesOrderPrintForm)form;

      	String frParam = Common.getUserPermission(user, Constants.AR_REP_SALES_ORDER_PRINT_ID);

      	if (frParam != null) {

      		actionForm.setUserPermission(frParam.trim());

      	} else {

      		actionForm.setUserPermission(Constants.NO_ACCESS);

      	}
      	/*******************************************************
      	 Initialize ArRepSalesOrderPrintController EJB
      	 *******************************************************/

      	ArRepSalesOrderPrintControllerHome homeSP = null;
      	ArRepSalesOrderPrintController ejbSP = null;

      	try {

      		homeSP = (ArRepSalesOrderPrintControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/ArRepSalesOrderPrintControllerEJB", ArRepSalesOrderPrintControllerHome.class);

      	} catch(NamingException e) {

      		if(log.isInfoEnabled()) {

      			log.info("NamingException caught in ArRepSalesOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());

      		}

      		return(mapping.findForward("cmnErrorPage"));

      	}

      	try {

      		ejbSP = homeSP.create();

      	} catch(CreateException e) {

      		if(log.isInfoEnabled()) {

      			log.info("CreateException caught in ArRepSalesOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());

      		}

      		return(mapping.findForward("cmnErrorPage"));

      	}

      	ActionErrors errors = new ActionErrors();

      	/*** get report session and if not null set it to null **/

      	Report reportSession =
      		(Report)session.getAttribute(Constants.REPORT_KEY);

      	if(reportSession != null) {

      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);

      	}

/*******************************************************
    --  Go Action --
*******************************************************/

      	if(frParam != null) {

      		AdCompanyDetails adCmpDetails = null;
      		ArRepSalesOrderPrintDetails details = null;
      		int soCode;
      		ArrayList reportParameters = new ArrayList();
      		String reportType = "";

      		ArrayList list = null;

      		try {

      			ArrayList soCodeList = new ArrayList();

      			reportType = request.getParameter("reportType");

      			soCode = new Integer(request.getParameter("salesOrderCode"));
      			if (request.getParameter("forward") != null) {

      				soCodeList.add(soCode);

      			}



      			list = ejbSP.executeArRepSalesOrderPrint(soCodeList, user.getCmpCode());

      			// get company

      			adCmpDetails = ejbSP.getAdCompany(user.getCmpCode());

      			// get parameters

      			Iterator i = list.iterator();

      			while (i.hasNext()) {

      				details = (ArRepSalesOrderPrintDetails)i.next();

      				reportParameters = Common.convertStringToReportParameters(details.getSpReportParameter(), new ArrayList());
      			}





      		} catch (GlobalNoRecordFoundException ex) {

      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("salesOrderPrint.error.noRecordFound"));

      		} catch(EJBException ex) {

      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in ArRepSalesOrderPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());

      			}

      			return(mapping.findForward("cmnErrorPage"));

      		}

      		if(!errors.isEmpty()) {

      			saveErrors(request, new ActionMessages(errors));
      			return(mapping.findForward("arRepSalesOrderPrint"));

      		}




      		// fill report parameters, fill report to pdf and set report session

      		Map parameters = new HashMap();
      		parameters.put("company", adCmpDetails.getCmpName());
      		parameters.put("createdBy", details.getSpSoCreatedBy());
      		parameters.put("approvedBy", details.getSpSoApprovedRejectedBy());


      		//report parameters

      		for(int x=0;x<reportParameters.size();x++) {
      			ReportParameter reportParameter = (ReportParameter)reportParameters.get(x);
      			parameters.put(reportParameter.getParameterName(), reportParameter.getParameterValue());
      		}

      		String jasperFileName = "ArRepSalesOrderPrint.jasper";

      		jasperFileName = Common.getLookUpJasperFile("SALES_ORDER_PRINT_LOOK_UP", reportType , user.getCompany());

	    	if(jasperFileName.equals("")) {
	    		jasperFileName = "ArRepSalesOrderPrint.jasper";
	    	}


      		String filename = "/opt/ofs-resources/" + user.getCompany() + "/" + jasperFileName;

      		if (!new java.io.File(filename).exists()) {

      			filename = servlet.getServletContext().getRealPath("jreports/ArRepSalesOrderPrint.jasper");

      		}

      		try {

      			Report report = new Report();

      			report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
      			report.setBytes(
      					JasperRunManager.runReportToPdf(filename, parameters,
      							new ArRepSalesOrderPrintDS(list)));

      			session.setAttribute(Constants.REPORT_KEY, report);


      		} catch(Exception ex) {

      			if(log.isInfoEnabled()) {

      				log.info("Exception caught in ArRepSalesOrderPrintAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());

      			}

      			return(mapping.findForward("cmnErrorPage"));

      		}

      		return(mapping.findForward("arRepSalesOrderPrint"));

      	} else {

      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));

      		return(mapping.findForward("arRepSalesOrderPrintForm"));

      	}

      } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
      	  e.printStackTrace();
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in ArRepSalesOrderPrintAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
