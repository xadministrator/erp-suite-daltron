package com.struts.jreports.ar.salesorderprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintData;
import com.struts.util.Common;
import com.util.ArRepSalesOrderPrintDetails;

public class ArRepSalesOrderPrintDS implements JRDataSource{

	private ArrayList data = new ArrayList();

	private int index = -1;

	public ArRepSalesOrderPrintDS(ArrayList list) {

		Iterator i = list.iterator();

		while (i.hasNext()) {

			ArRepSalesOrderPrintDetails details = (ArRepSalesOrderPrintDetails)i.next();

			ArRepSalesOrderPrintData soData = new ArRepSalesOrderPrintData(Common.convertSQLDateToString(details.getSpSoDate()),
			        details.getSpSoReferenceNumber(), details.getSpSoDocumentNumber(), details.getSpSoTransactionType(),
					details.getSpSoDescription(), details.getSpSoBillTo(), details.getSpSoShipTo(),
					new Double(details.getSpSolQuantity()), details.getSpSolUom(),
			        details.getSpSolIiCode(), details.getSpSolLocName(), new Double(details.getSpSolUnitPrice()),
			        new Double(details.getSpSolAmount()), details.getSpSoCstCustomerCode(), details.getSpSoCstName(),
					details.getSpSoCstAddress(), new Double(details.getSpSoCstCreditLimit()),

					details.getSpSoCstContactPerson(), details.getSpSoCstPhoneNumber(), details.getSpSoCstMobileNumber(), details.getSpSoCstFax(), details.getSpSoCstEmail(),
					details.getSpSolIiDescription(),
					details.getSpSolPropertyCode(), details.getSpSolSerialNumber(), details.getSpSolSpecs(), details.getSpSolCustodian(), details.getSpSolExpiryDate(),
					new String (details.getSpSoFcSymbol() + ""), new Double(details.getSpSoCstCbBalance()),
					new Double(details.getSpSoCstPendingOrder()), new Double(details.getSpSoAmount()), details.getSpSolIiStockStatus(),
					new Double(details.getSpSoCstLastReceiptAmount()), details.getSpSoCstLastReceiptDate(), details.getSpSoPaymentTerm(),
					new Double(details.getSpSoAgBucket0()), new Double(details.getSpSoAgBucket1()), new Double(details.getSpSoAgBucket2()),
					new Double(details.getSpSoAgBucket3()), new Double(details.getSpSoAgBucket4()), new Double(details.getSpSoAgBucket5()),
					details.getSpSoCstSlsSalespersonCode(), details.getSpSoCstSlsName(), details.getSpSoCstCity(),
					new Double(details.getSpSolTotalDiscount()), new Double(details.getSpSolDiscount1()),
					new Double(details.getSpSolDiscount2()), new Double(details.getSpSolDiscount3()),
					new Double(details.getSpSolDiscount4()), new Double(details.getSpSoAdvanceAmount()), new Double(details.getSpSolTaxAmount()),
					new Double(details.getSpSoTaxRate()), new Double(details.getSpSolUnitPriceWoTax()),
					details.getSpSoTaxType(), new Double(details.getSpSolQuantityOnHand()), details.getSpIlCategory(), details.getSpSoCustomerDealPrice(),
					details.getSpSoMemo()
					);

			data.add(soData);

		}

	}

	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}

	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;

		String fieldName = field.getName();

		if("date".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getDate();
		} else if("referenceNumber".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getReferenceNumber();
		} else if("documentNumber".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getDocumentNumber();
		} else if("transactionType".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getTransactionType();
		} else if("description".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getDescription();
		} else if("quantity".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getQuantity();
		} else if("unit".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getUnit();
		} else if("itemName".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemName();
		} else if("itemLocation".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemLocation();
		} else if("unitPrice".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getUnitPrice();
		} else if("amount".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAmount();
		} else if("customerCode".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerCode();
		} else if("customerName".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerName();
		} else if("customerAddress".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerAddress();
		} else if("customerCreditLimit".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerCreditLimit();
		} else if("customerContactPerson".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerContactPerson();
		} else if("customerPhoneNumber".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerPhoneNumber();
		} else if("customerMobileNumber".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerMobileNumber();
		} else if("customerFax".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerFax();
		} else if("customerEmail".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerEmail();
		} else if("itemDescription".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemDescription();
		} else if("itemPropertyCode".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemPropertyCode();
		} else if("itemSerialNumber".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemSerialNumber();
		} else if("itemSpecs".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemSpecs();
		} else if("itemCustodian".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemCustodian();
		} else if("itemExpiryDate".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemExpiryDate();
		} else if("currencySymbol".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCurrencySymbol();
		} else if("customerBalance".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerBalance();
		} else if("pendingOrder".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getPendingOrder();
		} else if("salesOrderAmount".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getSalesOrderAmount();
		} else if("stockStatus".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getStockStatus();
		} else if("lastReceiptAmount".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getLastReceiptAmount();
		} else if("lastReceiptDate".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getLastReceiptDate();
		} else if("agingBucket0".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket0();
		} else if("agingBucket1".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket1();
		} else if("agingBucket2".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket2();
		} else if("agingBucket3".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket3();
		} else if("agingBucket4".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket4();
		} else if("agingBucket5".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getAgingBucket5();
		} else if("paymentTerm".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getPaymentTerm();
		} else if("salespersonCode".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getSalespersonCode();
		} else if("salespersonName".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getSalespersonName();
		} else if("customerCity".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getCustomerCity();
		} else if("itemTotalDiscount".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemTotalDiscount();
		} else if("itemDiscount1".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemDiscount1();
		} else if("itemDiscount2".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemDiscount2();
		} else if("itemDiscount3".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemDiscount3();
		} else if("itemDiscount4".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getItemDiscount4();
		} else if("soAdvanceAmount".equals(fieldName)) {
		    value = ((ArRepSalesOrderPrintData)data.get(index)).getSoAdvanceAmount();
		} else if("taxAmount".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getTaxAmount();
		} else if("taxRate".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getTaxRate();
		} else if("unitPriceWoTax".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getUnitPriceWoTax();
		} else if("taxType".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getTaxType();
		} else if("quantityOnHand".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getQuantityOnHand();
		} else if("itemCategory".equals(fieldName)) {
			value = ((ArRepSalesOrderPrintData)data.get(index)).getItemCategory();
		}else if("dealPrice".equals(fieldName)){
			value = ((ArRepSalesOrderPrintData)data.get(index)).getDealPrice();
		}else if("memo".equals(fieldName)){
			value = ((ArRepSalesOrderPrintData)data.get(index)).getMemo();
		}else if("billTo".equals(fieldName)){
			value = ((ArRepSalesOrderPrintData)data.get(index)).getBillTo();
		}else if("shipTo".equals(fieldName)){
			value = ((ArRepSalesOrderPrintData)data.get(index)).getShipTo();
		}

		return(value);

	}

}
