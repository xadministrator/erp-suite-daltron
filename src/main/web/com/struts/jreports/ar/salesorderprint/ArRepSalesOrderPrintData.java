package com.struts.jreports.ar.salesorderprint;

import java.util.Date;

public class ArRepSalesOrderPrintData implements java.io.Serializable {

	private String date = null;
	private String referenceNumber = null;
	private String documentNumber = null;
	private String transactionType = null;
	private String description = null;
	private String billTo = null;
	private String shipTo = null;
	private Double quantity = null;
	private String unit = null;
	private String itemName = null;
	private String itemLocation = null;
	private Double unitPrice = null;
	private Double amount = null;
	private String customerCode = null;
	private String customerName = null;
	private String customerAddress = null;
	private Double customerCreditLimit = null;

	private String customerContactPerson = null;
	private String customerPhoneNumber = null;
	private String customerMobileNumber = null;
	private String customerFax = null;
	private String customerEmail = null;


	private String itemDescription = null;
	private String itemPropertyCode = null;
	private String itemSerialNumber = null;
	private String itemSpecs = null;
	private String itemCustodian = null;
	private String itemExpiryDate = null;
	private String currencySymbol = null;
	private Double customerBalance = null;
	private Double pendingOrder = null;
	private Double salesOrderAmount = null;
	private String stockStatus = null;
	private Double lastReceiptAmount = null;
	private Date lastReceiptDate = null;
	private String paymentTerm = null;
	private Double agingBucket0 = null;
	private Double agingBucket1 = null;
	private Double agingBucket2 = null;
	private Double agingBucket3 = null;
	private Double agingBucket4 = null;
	private Double agingBucket5 = null;
	private String salespersonCode = null;
	private String salespersonName = null;
	private String customerCity = null;
	private Double itemTotalDiscount = null;
	private Double itemDiscount1 = null;
	private Double itemDiscount2 = null;
	private Double itemDiscount3 = null;
	private Double itemDiscount4 = null;
	private Double soAdvanceAmount = null;
	private Double taxAmount = null;
	private Double taxRate = null;
	private Double unitPriceWoTax = null;
	private String taxType = null;
	private Double quantityOnHand = null;

	private String itemCategory = null;
	private String dealPrice = null;
	private String memo = null;

	public ArRepSalesOrderPrintData(String date, String referenceNumber, String documentNumber, String transactionType, String description,
			String billTo, String shipTo,
	        Double quantity, String unit, String itemName, String itemLocation, Double unitPrice,
	        Double amount, String customerCode, String customerName, String customerAddress,
			Double customerCreditLimit,
			String customerContactPerson, String customerPhoneNumber, String customerMobileNumber, String customerFax, String customerEmail,

			String itemDescription ,
			String itemPropertyCode, String itemSerialNumber, String itemSpecs, String itemCustodian, String itemExpiryDate,
			String currencySymbol, Double customerBalance,
			Double pendingOrder, Double salesOrderAmount, String stockStatus, Double lastReceiptAmount,
			Date lastReceiptDate, String paymentTerm, Double agingBucket0, Double agingBucket1, Double agingBucket2,
			Double agingBucket3, Double agingBucket4, Double agingBucket5, String salespersonCode, String salespersonName,
			String customerCity, Double itemTotalDiscount, Double itemDiscount1, Double itemDiscount2,
			Double itemDiscount3 , Double itemDiscount4, Double soAdvanceAmount, Double taxAmount, Double taxRate, Double unitPriceWoTax,
			String taxType, Double quantityOnHand, String itemCategory, String dealPrice, String memo) {

			this.date = date;
			this.referenceNumber = referenceNumber;
			this.documentNumber = documentNumber;
			this.transactionType = transactionType;
			this.description = description;
			this.billTo = billTo;
			this.shipTo = shipTo;
			this.quantity =  quantity;
			this.unit = unit;
			this.itemName = itemName;
			this.itemLocation = itemLocation;
			this.description = description;
			this.unitPrice = unitPrice;
			this.amount = amount;
			this.customerCode = customerCode;
			this.customerName = customerName;
			this.customerAddress = customerAddress;
			this.customerCreditLimit = customerCreditLimit;
			this.customerContactPerson = customerContactPerson;
			this.customerPhoneNumber = customerPhoneNumber;
			this.customerMobileNumber = customerMobileNumber;
			this.customerFax = customerFax;
			this.customerEmail = customerEmail;
			this.itemDescription = itemDescription;
			this.itemPropertyCode = itemPropertyCode;
			this.itemSerialNumber = itemSerialNumber;
			this.itemSpecs = itemSpecs;
			this.itemCustodian = itemCustodian;
			this.itemExpiryDate = itemExpiryDate;
			this.currencySymbol = currencySymbol;
			this.customerBalance = customerBalance;
			this.pendingOrder = pendingOrder;
			this.salesOrderAmount = salesOrderAmount;
			this.stockStatus = stockStatus;
			this.lastReceiptAmount = lastReceiptAmount;
			this.lastReceiptDate = lastReceiptDate;
			this.paymentTerm = paymentTerm;
			this.agingBucket0 = agingBucket0;
			this.agingBucket1 = agingBucket1;
			this.agingBucket2 = agingBucket2;
			this.agingBucket3 = agingBucket3;
			this.agingBucket4 = agingBucket4;
			this.agingBucket5 = agingBucket5;
			this.salespersonCode = salespersonCode;
			this.salespersonName = salespersonName;
			this.customerCity = customerCity;
			this.itemTotalDiscount = itemTotalDiscount;
			this.itemDiscount1 = itemDiscount1;
			this.itemDiscount2 = itemDiscount2;
			this.itemDiscount3 = itemDiscount3;
			this.itemDiscount4 = itemDiscount4;
			this.soAdvanceAmount = soAdvanceAmount;
			this.taxAmount = taxAmount;
			this.taxRate = taxRate;
			this.unitPriceWoTax = unitPriceWoTax;
			this.taxType = taxType;
			this.quantityOnHand = quantityOnHand;
			this.itemCategory = itemCategory;
			this.dealPrice = dealPrice;
			this.memo = memo;
	}

	public String getDate() {

		return date;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getDescription() {

		return description;

	}

	public String getBillTo() {

		return billTo;

	}

	public String getShipTo() {

		return shipTo;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getTransactionType() {

		return transactionType;

	}

	public Double getQuantity() {

		return quantity;

	}

	public String getUnit() {

		return unit;

	}

	public String getItemName() {

		return itemName;

	}

	public String getItemLocation() {

		return itemLocation;

	}



	public Double getUnitPrice() {

		return unitPrice;

	}

	public Double getAmount() {

		return amount;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getCustomerName() {

		return customerName;

	}

	public String getCustomerAddress() {

		return customerAddress;

	}

	public Double getCustomerCreditLimit() {

		return customerCreditLimit;

	}

	public String getCustomerContactPerson() {

		return customerContactPerson;

	}


	public String getCustomerPhoneNumber() {

		return customerPhoneNumber;

	}

	public String getCustomerMobileNumber() {

		return customerMobileNumber;

	}

	public String getCustomerFax() {

		return customerFax;

	}

	public String getCustomerEmail() {

		return customerEmail;

	}




	public String getItemDescription() {

		return itemDescription;

	}

	public String getItemPropertyCode() {

		return itemPropertyCode;

	}

	public String getItemSerialNumber() {

		return itemSerialNumber;

	}

	public String getItemSpecs() {

		return itemSpecs;

	}

	public String getItemCustodian() {

		return itemCustodian;

	}


	public String getItemExpiryDate() {

		return itemExpiryDate;

	}
	public String getCurrencySymbol() {

		return currencySymbol;

	}

	public Double getCustomerBalance() {

		return customerBalance;

	}

	public Double getPendingOrder() {

		return pendingOrder;

	}

	public Double getSalesOrderAmount() {

		return salesOrderAmount;

	}

	public String getStockStatus() {

		return stockStatus;

	}

	public Double getLastReceiptAmount() {

		return lastReceiptAmount;

	}

	public Date getLastReceiptDate() {

		return lastReceiptDate;

	}

	public String getPaymentTerm() {

		return paymentTerm;

	}

	public Double getAgingBucket0() {

		return agingBucket0;

	}

	public Double getAgingBucket1() {

		return agingBucket1;

	}

	public Double getAgingBucket2() {

		return agingBucket2;

	}

	public Double getAgingBucket3() {

		return agingBucket3;

	}

	public Double getAgingBucket4() {

		return agingBucket4;

	}

	public Double getAgingBucket5() {

		return agingBucket5;

	}

	public String getSalespersonCode() {

		return salespersonCode;

	}

	public String getSalespersonName() {

		return salespersonName;

	}

	public String getCustomerCity() {

		return customerCity;

	}

	public Double getItemTotalDiscount() {

		return itemTotalDiscount;

	}

	public Double getItemDiscount1() {

		return itemDiscount1;

	}

	public Double getItemDiscount2() {

		return itemDiscount2;

	}

	public Double getItemDiscount3() {

		return itemDiscount3;

	}

	public Double getItemDiscount4() {

		return itemDiscount4;

	}

	public Double getSoAdvanceAmount() {

		return soAdvanceAmount;

	}

	public Double getTaxAmount() {

		return taxAmount;

	}

	public Double getTaxRate() {

		return taxRate;

	}

	public Double getUnitPriceWoTax() {

		return unitPriceWoTax;

	}

	public String getTaxType() {

		return taxType;

	}

	public Double getQuantityOnHand() {

		return quantityOnHand;

	}

	public String getItemCategory() {

		return itemCategory;

	}

	public String getDealPrice() {

		return dealPrice;

	}

	public String getMemo() {

		return memo;

	}

}
