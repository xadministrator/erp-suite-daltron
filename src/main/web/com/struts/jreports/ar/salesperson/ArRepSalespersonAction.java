package com.struts.jreports.ar.salesperson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepSalespersonController;
import com.ejb.txn.ArRepSalespersonControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepSalespersonAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepSalespersonAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepSalespersonForm actionForm = (ArRepSalespersonForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_SALES_REGISTER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepSalesperson");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArRepSalesRegisterController EJB
*******************************************************/

         ArRepSalespersonControllerHome homeSLP = null;
         ArRepSalespersonController ejbSLP = null;       

         try {
         	
            homeSLP = (ArRepSalespersonControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepSalespersonControllerEJB", ArRepSalespersonControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepSalespersonAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSLP = homeSLP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepSalespersonAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AR SR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getSalespersonCode())) {
	        		
	        		criteria.put("salespersonCode", actionForm.getSalespersonCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	    

	        	if (!Common.validateRequired(actionForm.getCustomerType())) {

	        		criteria.put("customerType", actionForm.getCustomerType());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {
	        		
	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
	        		
	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());
	        		
	        	}
	        	
                if (actionForm.getIncludedMiscReceipts()) {	        	
                	
                	criteria.put("includedMiscReceipts", "YES");
                	
                }
                
                if (!actionForm.getIncludedMiscReceipts()) {
                	
                	criteria.put("includedMiscReceipts", "NO");
                	
                }
                
                if (!Common.validateRequired(actionForm.getPaymentStatus())) {
	        		
	        		criteria.put("paymentStatus", actionForm.getPaymentStatus());
	        		
	        	}
                	 
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbSLP.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrSlpListSize(); i++) {
	           	
	           		ArRepBranchSalespersonList brSrList = (ArRepBranchSalespersonList)actionForm.getArRepBrSlpListByIndex(i);
	           	
	           		if(brSrList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brSrList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbSLP.executeArRepSalesperson(actionForm.getCriteria(), branchList, 
		       		actionForm.getGroupBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("salesRegister.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepSalespersonAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arRepSalesperson");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("groupBy", actionForm.getGroupBy());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }

			if (actionForm.getCustomerType() != null) {

				parameters.put("customerType", actionForm.getCustomerType());	

			}
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", actionForm.getDateFrom());				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", actionForm.getDateTo());	    	
			
		    }
	 
			parameters.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
		    parameters.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());
		    
		    if (actionForm.getIncludedMiscReceipts()) {
		    	
		    	parameters.put("includedMiscReceipts", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedMiscReceipts", "NO");
		    	
		    }
		    
		    parameters.put("paymentStatus", actionForm.getPaymentStatus());
		    
		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepBrSlpListSize(); j++) {

		    	ArRepBranchSalespersonList brList = (ArRepBranchSalespersonList)actionForm.getArRepBrSlpListByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBranchName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBranchName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesperson.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	        	filename = servlet.getServletContext().getRealPath("jreports/ArRepSalesperson.jasper");
		    
	        }
    		    	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepSalespersonDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new ArRepSalespersonDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new ArRepSalespersonDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepSalespersonAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        ex.printStackTrace();
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		   
/*******************************************************
   -- AR SR Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		     	  
		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AR SR Customer Enter Action --
 *******************************************************/
		          
		     } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {
		     	
		     	return(mapping.findForward("arRepSalesperson"));		          
		          
/*******************************************************
   -- AR SR Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.reset(mapping, request);
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbSLP.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   

	            	actionForm.clearCustomerTypeList();           	

	            	list = ejbSLP.getArCtAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            			actionForm.setCustomerTypeList((String)i.next());

	            		}

	            	}  
	            	
	            	actionForm.clearArRepBrSlpList();
	         		
	         		list = ejbSLP.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			ArRepBranchSalespersonList arRepBrSrList = new ArRepBranchSalespersonList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			arRepBrSrList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrSlpList(arRepBrSrList);
	         			
	         		}
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepSalespersonAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	            return(mapping.findForward("arRepSalesperson"));
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in ArRepSalespersonAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
