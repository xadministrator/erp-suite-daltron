package com.struts.jreports.ar.salesperson;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepSalespersonDetails;

public class ArRepSalespersonDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepSalespersonDS(ArrayList list, String groupBy) {
   	
   	  Iterator i = list.iterator();
   	
      while (i.hasNext()) {
      	
         ArRepSalespersonDetails details = (ArRepSalespersonDetails)i.next();
         
         String group = null;

         if (groupBy.equals("SALESPERSON CODE")) {
         	
         	group = details.getSlpSalespersonName();
         	
         } else if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getSlpCstCustomerCode() + "-" + details.getSlpCstCustomerName();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
          	
          	group = details.getSlpCstCustomerType();
          	
          }
         
	     ArRepSalespersonData argData = new ArRepSalespersonData(details.getSlpSalespersonCode(), 
	     details.getSlpCstCustomerCode(), details.getSlpCstCustomerName(), new Double(details.getSlpCommissionAmount()),
	     new Double(details.getSlpNonCommissionAmount()), new Double(details.getSlpGrandTotalSales()), 
	     group, details.getSlpSalespersonName(), details.getSlpPaymentStatus(), details.getSlpCstCustomerType(),
	     details.getSlpCollectionDate(), details.getSlpInvoiceNumber(), new Double(details.getSlpInvcAmountPd()), 
	     details.getSlpRctCustomerDeposit(), details.getSlpDate(), details.getSlpType(), new Double(details.getSlpAmount()));
		    
	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("salespersonCode".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getSalespersonCode();       
      }else if("customerCode".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getCustomerCode();
      }else if("commAmnt".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getCommAmnt();   
      }else if("nonCommAmnt".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getNonCommAmnt();   
      }else if("totalSalesAmount".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getTotalSalesAmount();
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepSalespersonData)data.get(index)).getGroupBy();
      }else if("salespersonName".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getSalespersonName();       
      }else if("paymentStatus".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getPaymentStatus();       
      }else if("customerType".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getCustomerType();       
      }else if("customerName".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getCustomerName();       
      }else if("collectionDate".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getCollectionDate();       
      }else if("invoiceNumber".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getInvoiceNumber();       
      }else if("isCustomerDeposit".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getRctCustomerDeposit();       
      }else if("invoiceAmountPaid".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getInvAmountPaid();       
      }else if("invoiceDate".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getInvDate();       
      }else if("slpType".equals(fieldName)){
         value = ((ArRepSalespersonData)data.get(index)).getSlpType();       
      }else if("slpAmount".equals(fieldName)){ 
         value = ((ArRepSalespersonData)data.get(index)).getSlpAmount();       
      }
      

      return(value);
   }
}
