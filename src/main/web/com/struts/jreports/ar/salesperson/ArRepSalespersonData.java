package com.struts.jreports.ar.salesperson;

import java.util.Date;


public class ArRepSalespersonData implements java.io.Serializable {
	
	private String salespersonCode = null;
	private String customerCode = null;
	private String customerName = null;
	private Double commAmnt = null;
	private Double nonCommAmnt = null;	
	private Double totalSalesAmount = null;
	private String groupBy = null;
	private String salespersonName = null;
	private String paymentStatus = null;
	private String customerType = null;
	private Date collectionDate = null;
	private String invoiceNumber = null;
	private Double invoiceAmountPaid = null;
	private Boolean isCustomerDeposit = null;
	private Date invoiceDate = null;
	private String slpType = null;
	private Double slpAmount = null;
	
	public ArRepSalespersonData(String salespersonCode, String customerCode, String customerName, Double commAmnt, Double nonCommAmnt,
			Double totalSalesAmount, String groupBy, String salespersonName, String paymentStatus, String customerType, 
			Date collectionDate, String invoiceNumber, Double invoiceAmountPaid, Boolean isCustomerDeposit, Date invoiceDate, 
			String slpType, Double slpAmount) {
		
		this.salespersonCode = salespersonCode;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.commAmnt = commAmnt;
		this.nonCommAmnt = nonCommAmnt;
		this.totalSalesAmount = totalSalesAmount;
		this.groupBy = groupBy;
		this.salespersonName = salespersonName;
		this.paymentStatus = paymentStatus;
		this.customerType = customerType;
		this.collectionDate = collectionDate;
		this.invoiceNumber = invoiceNumber;
		this.invoiceAmountPaid = invoiceAmountPaid;
		this.isCustomerDeposit = isCustomerDeposit;
		this.invoiceDate = invoiceDate;
		this.slpType = slpType;
		this.slpAmount = slpAmount;
   }

   public String getSalespersonCode() {
   	
   	  return salespersonCode;
   	  
   }

   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   
   public String getCustomerName() {
	   
	   return customerName;
	   
   }
   
   public Double getCommAmnt() {
   	
   	  return commAmnt;
   	  
   }

   public Double getNonCommAmnt() {
   	
   	  return nonCommAmnt;
   	  
   }
   
   public Double getTotalSalesAmount() {
   	
   	  return totalSalesAmount;
   	  
   }
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getSalespersonName() {

	   return salespersonName;

   }
   
   public String getPaymentStatus() {
	   
	   return paymentStatus;
	   
   }
   
   public String getCustomerType() {
	   
	   return customerType;
	   
   }
   
   public Date getCollectionDate() {
	   
	   return collectionDate;
	   
   }
   
   public String getInvoiceNumber() {
	   
	   return invoiceNumber;
	   
   }
   
   public Boolean getRctCustomerDeposit() {
	   
	   return isCustomerDeposit;
	   
   }
   
   public Double getInvAmountPaid() {
	   
	   return invoiceAmountPaid;
	   
   }
   
   public Date getInvDate(){
	   
	   return invoiceDate;
	   
   }
   
   public String getSlpType() {
	   
	   return slpType;
	   
   }
   
   public Double getSlpAmount() {
	   
	   return slpAmount;
	   
   }
   
   
}
