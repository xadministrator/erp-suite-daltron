package com.struts.jreports.ar.salesperson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepSalespersonForm extends ActionForm implements Serializable{

   private String customerCode = null;
   private String salespersonCode = null;
   private String customerClass = null;
   private ArrayList customerClassList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String invoiceNumberFrom = null;
   private String invoiceNumberTo = null;
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();
   private boolean includedMiscReceipts = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String isCustomerEntered = null;
   private String customerType = null;
   private ArrayList customerTypeList = new ArrayList();
   
   private ArrayList arRepBrSlpList = new ArrayList();

   private HashMap criteria = new HashMap();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSalespersonCode() {
	   
	   return(salespersonCode);
	   
   }
   
   public void setSalespersonCode(String salespersonCode) {
	
	   this.salespersonCode = salespersonCode;
	      
   }
   
   public String getCustomerCode(){
   	
      return(customerCode);
      
   }

   public void setCustomerCode(String customerCode){
   	
      this.customerCode = customerCode;
      
   }

   public String getDateFrom(){
   	
      return(dateFrom);
      
   }

   public void setDateFrom(String dateFrom){
   	
      this.dateFrom = dateFrom;
      
   }

   public String getDateTo(){
   	
      return(dateTo);
      
   }

   public void setDateTo(String dateTo){
   	
      this.dateTo = dateTo;
      
   }

   public String getInvoiceNumberFrom(){
   	
      return(invoiceNumberFrom);
      
   }

   public void setInvoiceNumberFrom(String invoiceNumberFrom){
   	
      this.invoiceNumberFrom = invoiceNumberFrom;
      
   }

   public String getInvoiceNumberTo(){
   	
      return(invoiceNumberTo);
      
   }

   public void setInvoiceNumberTo(String invoiceNumberTo){
   	
      this.invoiceNumberTo = invoiceNumberTo;
      
   }

   public void setGoButton(String goButton){
   	
      this.goButton = goButton;
      
   }

   public void setCloseButton(String closeButton){
   	
      this.closeButton = closeButton;
      
   }
   
   public String getIsCustomerEntered() {
   	
   	  return isCustomerEntered;
   	  
   }
   
   public void setIsCustomerEntered(String isCustomerEntered) {
   	
   	  this.isCustomerEntered = isCustomerEntered;
   	  
   }

   public String getCustomerClass(){
   	
      return(customerClass);
      
   }

   public void setCustomerClass(String customerClass){
   	
      this.customerClass = customerClass;
      
   }

   public ArrayList getCustomerClassList(){
   	
      return(customerClassList);
      
   }

   public void setCustomerClassList(String customerClass){
   	
      customerClassList.add(customerClass);
      
   }

   public void clearCustomerClassList(){
   	
      customerClassList.clear();
      customerClassList.add(Constants.GLOBAL_BLANK);
      
   } 

   public String getGroupBy(){
   	
   	return(groupBy);   	
   	
   }
   
   public void setGroupBy(String groupBy){
   	
   	this.groupBy = groupBy;
   	
   }
   
   public ArrayList getGroupByList(){
   	
   	return groupByList;
   	
   }
   
   public String getViewType(){
   	
   	  return(viewType);   	
   	  
   }
   
   public void setViewType(String viewType){
   	
   	  this.viewType = viewType;
   	  
   }
   
   public ArrayList getViewTypeList(){
   	
   	  return viewTypeList;
   	  
   }
   
   public boolean getIncludedMiscReceipts() {
   	
   	  return includedMiscReceipts;
   	
   }
   
   public void setIncludedMiscReceipts(boolean includedMiscReceipts) {
   	
   	  this.includedMiscReceipts = includedMiscReceipts;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
   	
      return(userPermission);
      
   }

   public void setUserPermission(String userPermission){
   	
      this.userPermission = userPermission;
      
   }
   
   public String getPaymentStatus() {
   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }
   
   public Object[] getArRepBrSlpList(){
   	
   	  return arRepBrSlpList.toArray();
   	
   }
   
   public ArRepBranchSalespersonList getArRepBrSlpListByIndex(int index){
   	
   	  return ((ArRepBranchSalespersonList)arRepBrSlpList.get(index));
   	
   }
   
   public int getArRepBrSlpListSize(){
   	
   	  return(arRepBrSlpList.size());
   	
   }
   
   public void saveArRepBrSlpList(Object newArRepBrSlpList){
   	
   	  arRepBrSlpList.add(newArRepBrSlpList);   	  
   	
   }
   
   public void clearArRepBrSlpList(){
   	
   	  arRepBrSlpList.clear();
   	
   }

   public String getCustomerType() {

	   return customerType;

   }

   public void setCustomerType(String customerType) {

	   this.customerType = customerType;

   }

   public ArrayList getCustomerTypeList() {

	   return customerTypeList;

   }

   public void setCustomerTypeList(String customerType) {

	   customerTypeList.add(customerType);

   }

   public void clearCustomerTypeList() {

	   customerTypeList.clear();
	   customerTypeList.add(Constants.GLOBAL_BLANK);

   }

   public void reset(ActionMapping mapping, HttpServletRequest request){      
   	
   	  for (int i=0; i<arRepBrSlpList.size(); i++) {
	  	
   	  	  ArRepBranchSalespersonList actionList = (ArRepBranchSalespersonList)arRepBrSlpList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
   	
      goButton = null;
      closeButton = null;
      salespersonCode = null;
      customerCode = null;
      customerClass = Constants.GLOBAL_BLANK; 
      customerType = Constants.GLOBAL_BLANK;
      dateFrom = null;
      dateTo = null;
      invoiceNumberFrom = null;
      invoiceNumberTo = null;
      isCustomerEntered = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_PAID);
      paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_UNPAID);
      paymentStatus = Constants.GLOBAL_BLANK;
      groupByList.clear();
      groupByList.add(Constants.AR_REP_SALESPERSON_ORDER_BY);
      groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
      groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
      groupBy = Constants.GLOBAL_BLANK;   
      includedMiscReceipts = false;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("salesRegister.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("salesRegister.error.dateToInvalid"));
		 }
		 if(!Common.validateStringExists(customerClassList, customerClass)){
	            errors.add("customerClass", new ActionMessage("salesRegister.error.customerClassInvalid"));
		 }	 
      }
      return(errors);
   }
}
