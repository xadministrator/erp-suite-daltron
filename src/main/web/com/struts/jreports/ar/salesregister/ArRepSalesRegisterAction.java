package com.struts.jreports.ar.salesregister;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepSalesRegisterController;
import com.ejb.txn.ArRepSalesRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class ArRepSalesRegisterAction extends Action {

  private org.apache.commons.logging.Log log =
      org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

  public ActionForward execute(
      ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response
  ) throws Exception {

    HttpSession session = request.getSession();

    try {

      /*******************************************************
       * Check if user has a session
       *******************************************************/

      User user = (User) session.getAttribute(Constants.USER_KEY);

      if (user != null) {

        if (log.isInfoEnabled()) {

          log.info("ArRepSalesRegisterAction: Company '" + user.getCompany() + "' User '"
              + user.getUserName() + "' performed this action on session " + session.getId());

        }

      } else {

        if (log.isInfoEnabled()) {

          log.info("User is not logged on in session" + session.getId());

        }

        return (mapping.findForward("adLogon"));

      }

      ArRepSalesRegisterForm actionForm = (ArRepSalesRegisterForm) form;

      // reset report to null
      actionForm.setReport(null);

      String frParam = Common.getUserPermission(user, Constants.AR_REP_SALES_REGISTER_ID);

      if (frParam != null) {

        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

          ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
          if (!fieldErrors.isEmpty()) {

            saveErrors(request, new ActionMessages(fieldErrors));

            return mapping.findForward("arRepSalesRegister");
          }

        }

        actionForm.setUserPermission(frParam.trim());

      } else {

        actionForm.setUserPermission(Constants.NO_ACCESS);

      }
      /*******************************************************
       * Initialize ArRepSalesRegisterController EJB
       *******************************************************/

      ArRepSalesRegisterControllerHome homeSR = null;
      ArRepSalesRegisterController ejbSR = null;

      try {

        homeSR = (ArRepSalesRegisterControllerHome) com.util.EJBHomeFactory.lookUpHome(
            "ejb/ArRepSalesRegisterControllerEJB", ArRepSalesRegisterControllerHome.class);

      } catch (NamingException e) {

        if (log.isInfoEnabled()) {

          log.info("NamingException caught in ArRepSalesRegisterAction.execute(): " + e.getMessage()
              + " session: " + session.getId());

        }

        return (mapping.findForward("cmnErrorPage"));

      }

      try {

        ejbSR = homeSR.create();

      } catch (CreateException e) {

        if (log.isInfoEnabled()) {

          log.info("CreateException caught in ArRepSalesRegisterAction.execute(): " + e.getMessage()
              + " session: " + session.getId());

        }

        return (mapping.findForward("cmnErrorPage"));

      }

      ActionErrors errors = new ActionErrors();

      /*** get report session and if not null set it to null **/

      Report reportSession = (Report) session.getAttribute(Constants.REPORT_KEY);

      if (reportSession != null) {

        reportSession.setBytes(null);
        session.setAttribute(Constants.REPORT_KEY, reportSession);

      }

      /*******************************************************
       * -- AR SR Go Action --
       *******************************************************/

      if (request.getParameter("goButton") != null
          && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


        Date DATE_FROM = null;
        Date DATE_TO = null;
        String CUSTOMER_CODE = "";
        String INVOICE_BATCH = "";
        String CUSTOMER_BATCH = "";
        String CUSTOMER_TYPE = "";
        String CUSTOMER_CLASS = "";
        String SALESPERSON = "";
        String DOCUMENT_NUMBER_FROM = "";
        String DOCUMENT_NUMBER_TO = "";
        String PAYMENT_STATUS = "";
        String REGION = "";
        byte INCLUDE_INVOICES = 1;
        byte INCLUDE_COLLECTIONS = 0;
        byte INCLUDE_UNPOSTED = 0;
        byte INCLUDE_UNPOSTED_ONLY = 0;
        byte INCLUDE_CREDIT_MEMO = 0;
        String BRANCH_CODES = "";
        int CMP_CODE = 1;
        String ORDER_BY = "";
        String GROUP_BY = "";
        boolean isStoredProceedure = false;
        String storedProcName =
            "{ call ArRepSalesRegisterReportGenerator(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        String jasperFileNameSp =
            "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesRegisterSp.jasper";

        if (!new java.io.File(jasperFileNameSp).exists()) {

          jasperFileNameSp =
              servlet.getServletContext().getRealPath("jreports/ArRepSalesRegisterSp.jasper");

        }

        String filename = "";

        ArrayList list = new ArrayList();

        String company = null;

        // create criteria

        if (request.getParameter("goButton") != null) {

          HashMap criteria = new HashMap();

          if (!Common.validateRequired(actionForm.getCustomerCode())) {

            CUSTOMER_CODE = actionForm.getCustomerCode();
            criteria.put("customerCode", actionForm.getCustomerCode());

          }

          if (!Common.validateRequired(actionForm.getInvoiceBatchName())) {

            INVOICE_BATCH = actionForm.getInvoiceBatchName();
            criteria.put("invoiceBatchName", actionForm.getInvoiceBatchName());
          }

          /*
           * if (!Common.validateRequired(actionForm.getReceiptBatchName())) {
           * 
           * criteria.put("receiptBatchName", actionForm.getReceiptBatchName()); }
           */
          if (!Common.validateRequired(actionForm.getCustomerType())) {
            CUSTOMER_TYPE = actionForm.getCustomerType();
            criteria.put("customerType", actionForm.getCustomerType());
          }

          if (!Common.validateRequired(actionForm.getCustomerClass())) {
            CUSTOMER_CLASS = actionForm.getCustomerClass();
            criteria.put("customerClass", actionForm.getCustomerClass());
          }

          if (!Common.validateRequired(actionForm.getCustomerBatch())) {
            CUSTOMER_BATCH = actionForm.getCustomerBatch();
            criteria.put("customerBatch", actionForm.getCustomerBatch());

          }

          if (!Common.validateRequired(actionForm.getDateFrom())) {
            DATE_FROM = Common.convertStringToSQLDate(actionForm.getDateFrom());
            criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

          }

          if (!Common.validateRequired(actionForm.getDateTo())) {
            DATE_TO = Common.convertStringToSQLDate(actionForm.getDateTo());
            criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

          }

          if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {
            DOCUMENT_NUMBER_FROM = actionForm.getInvoiceNumberFrom();
            criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());

          }

          if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
            DOCUMENT_NUMBER_TO = actionForm.getInvoiceNumberTo();
            criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());

          }

          if (!Common.validateRequired(actionForm.getSalesperson())) {
            SALESPERSON = actionForm.getSalesperson();
            criteria.put("salesperson", actionForm.getSalesperson());

          }

          if (actionForm.getunpostedOnly()) {

            INCLUDE_UNPOSTED_ONLY = Common.convertBooleanToByte(true);
            criteria.put("unpostedOnly", "YES");

          }

          if (!actionForm.getunpostedOnly()) {
            INCLUDE_UNPOSTED_ONLY = Common.convertBooleanToByte(false);
            criteria.put("unpostedOnly", "NO");

          }

          if (actionForm.getIncludedUnposted()) {
            INCLUDE_UNPOSTED = Common.convertBooleanToByte(true);
            criteria.put("includedUnposted", "YES");

          }

          if (!actionForm.getIncludedUnposted()) {
            INCLUDE_UNPOSTED = Common.convertBooleanToByte(false);
            criteria.put("includedUnposted", "NO");

          }


          if (actionForm.getIncludedInvoices()) {
            INCLUDE_INVOICES = Common.convertBooleanToByte(true);
            criteria.put("includedInvoices", "YES");

          }

          if (!actionForm.getIncludedInvoices()) {
            INCLUDE_INVOICES = Common.convertBooleanToByte(false);
            criteria.put("includedInvoices", "NO");

          }

          if (actionForm.getIncludedMiscReceipts()) {

            criteria.put("includedMiscReceipts", "YES");

          }

          if (!actionForm.getIncludedMiscReceipts()) {

            criteria.put("includedMiscReceipts", "NO");

          }

          if (actionForm.getIncludedCreditMemos()) {
            INCLUDE_CREDIT_MEMO = Common.convertBooleanToByte(true);
            criteria.put("includedCreditMemos", "YES");

          }

          if (!actionForm.getIncludedCreditMemos()) {
            INCLUDE_CREDIT_MEMO = Common.convertBooleanToByte(false);
            criteria.put("includedCreditMemos", "NO");

          }

          if (actionForm.getIncludedDebitMemos()) {

            criteria.put("includedDebitMemos", "YES");

          }

          if (!actionForm.getIncludedDebitMemos()) {

            criteria.put("includedDebitMemos", "NO");

          }

          if (!Common.validateRequired(actionForm.getPaymentStatus())) {
            PAYMENT_STATUS = actionForm.getPaymentStatus();
            criteria.put("paymentStatus", actionForm.getPaymentStatus());

          }

          if (!actionForm.getIncludedPriorDues()) {

            criteria.put("includedPriorDues", "NO");

          }

          if (actionForm.getIncludedPriorDues()) {

            criteria.put("includedPriorDues", "YES");

          }

          if (!actionForm.getIncludedCollections()) {
            INCLUDE_COLLECTIONS = Common.convertBooleanToByte(false);
            criteria.put("includedCollections", "NO");

          }

          if (actionForm.getIncludedCollections()) {
            INCLUDE_COLLECTIONS = Common.convertBooleanToByte(true);
            criteria.put("includedCollections", "YES");

          }

          if (!actionForm.getSummary()) {

            criteria.put("summary", "NO");

          }

          if (actionForm.getSummary()) {

            criteria.put("summary", "YES");

          }
          if (!actionForm.getdetailedSales()) {

            criteria.put("detailedSales", "NO");

          }

          if (actionForm.getdetailedSales()) {

            criteria.put("detailedSales", "YES");

          }

          if (!Common.validateRequired(actionForm.getRegion())) {
            REGION = actionForm.getRegion();
            criteria.put("region", actionForm.getRegion());

          }


          GROUP_BY = actionForm.getGroupBy();
          // save criteria

          actionForm.setCriteria(criteria);

        }


        try {

          // get company

          AdCompanyDetails adCmpDetails = ejbSR.getAdCompany(user.getCmpCode());
          company = adCmpDetails.getCmpName();
          CMP_CODE = user.getCmpCode();

          ArrayList branchList = new ArrayList();

          for (int i = 0; i < actionForm.getArRepBrSrListSize(); i++) {

            ArRepBranchSalesRegisterList brSrList =
                (ArRepBranchSalesRegisterList) actionForm.getArRepBrSrListByIndex(i);

            if (brSrList.getBranchCheckbox() == true) {

              AdBranchDetails brDetails = new AdBranchDetails();
              brDetails.setBrCode(brSrList.getBranchCode());
              BRANCH_CODES += brSrList.getBranchCode() + ",";
              branchList.add(brDetails);
            }

          }

          // execute report


          if (new java.io.File(jasperFileNameSp).exists()) {
            // If jasper for stored proceedure exist. Run Stored Procedure process
            isStoredProceedure = true;
            filename = jasperFileNameSp;

            System.out.println("sp proceed");
            /* stored procedure section */
            DataSource dataSource =
                (DataSource) servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
            Connection conn = null;
            CallableStatement stmt = null;

            DATE_FROM = Common.convertStringToSQLDate(actionForm.getDateFrom());
            DATE_TO = Common.convertStringToSQLDate(actionForm.getDateTo());

            try {
        
              conn = dataSource.getConnection();

              stmt = (CallableStatement) conn.prepareCall(storedProcName);
              stmt.setDate(1 /* "DATE_FROM" */, DATE_FROM==null?null:new java.sql.Date(DATE_FROM.getTime()));
              stmt.setDate(2 /* "DATE_TO" */, DATE_TO==null?null:new java.sql.Date(DATE_TO.getTime()));
              stmt.setString(3 /* "CUSTOMER_CODE" */, CUSTOMER_CODE);
              stmt.setString(4 /* "INVOICE_BATCH" */, INVOICE_BATCH);
              stmt.setString(5 /* "CUSTOMER_BATCH" */, CUSTOMER_BATCH);
              stmt.setString(6 /* "CUSTOMER_TYPE" */, CUSTOMER_TYPE);
              stmt.setString(7 /* "CUSTOMER_CLASS" */, CUSTOMER_CLASS);
              stmt.setString(8 /* "SALESPERSON" */, SALESPERSON);
              stmt.setString(9 /* "DOCUMENT_NUMBER_FROM" */, DOCUMENT_NUMBER_FROM);
              stmt.setString(10 /* "DOCUMENT_NUMBER_TO" */, DOCUMENT_NUMBER_TO);
              stmt.setString(11 /* "PAYMENT_STATUS" */, PAYMENT_STATUS);
              stmt.setString(12 /* "REGION" */, REGION);
              stmt.setByte(13 /* "INCLUDE_INVOICES" */, INCLUDE_INVOICES);
              stmt.setByte(14 /* "INCLUDE_COLLECTIONS" */, INCLUDE_COLLECTIONS);
              stmt.setByte(15 /* "INCLUDE_UNPOSTED" */, INCLUDE_UNPOSTED);
              stmt.setByte(16 /* "INCLUDE_UNPOSTED_ONLY" */, INCLUDE_UNPOSTED_ONLY);
              stmt.setByte(17 /* "INCLUDE_CREDIT_MEMO" */, INCLUDE_CREDIT_MEMO);
              stmt.setString(18 /* "BRANCH_CODES" */, BRANCH_CODES);
              stmt.setInt(19 /* "CMP_CODE" */, CMP_CODE);
              stmt.setString(20 /* "ORDER_BY" */, ORDER_BY);
              stmt.setString(21 /* "GROUP_BY" */, GROUP_BY);
              stmt.registerOutParameter(22, java.sql.Types.INTEGER);

              stmt.executeQuery();
          
              int countResult = stmt.getInt(22);


              if (countResult <= 0) {
                throw new GlobalNoRecordFoundException();
              }

            } catch (SQLException ex) {
              System.out.println("sql error 1: " + ex.toString());

              try {
                if (stmt != null)
                  stmt.close();
              } catch (SQLException f) {
                System.out.println("sql error 1.1: " + f.toString());
              }
              stmt = null;
              try {
                if (conn != null)
                  conn.close();
              } catch (SQLException f) {
                System.out.println("sql error 1.2 : " + f.toString());
              }
              conn = null;

            } finally {

              try {
                if (stmt != null)
                  stmt.close();
              } catch (SQLException f) {
                System.out.println("sql error 2.1 : " + f.toString());;
              }
              stmt = null;
              try {
                if (conn != null)
                  conn.close();
              } catch (SQLException f) {
                System.out.println("sql error 2.2 : " + f.toString());;
              }
              conn = null;
            }
          } else {
            // GENERATE REPORT USING EJB

            if (actionForm.getGroupBy().equals("MEMO LINE")
                || actionForm.getOrderBy().equals("MEMO LINE")) {
              list = ejbSR.executeArRepSalesRegisterMemoLine(actionForm.getCriteria(), branchList,
                  actionForm.getOrderBy(), actionForm.getGroupBy(),
                  actionForm.getprintSalesRelatedOnly(), actionForm.getShowEntries(),
                  actionForm.getSummarize(), actionForm.getSummary(), actionForm.getdetailedSales(),
                  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } else {
              list = ejbSR.executeArRepSalesRegister(actionForm.getCriteria(), branchList,
                  actionForm.getOrderBy(), actionForm.getGroupBy(),
                  actionForm.getprintSalesRelatedOnly(), actionForm.getShowEntries(),
                  actionForm.getSummarize(), actionForm.getdetailedSales(), actionForm.getSummary(),
                  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            }


          }



        } catch (GlobalNoRecordFoundException ex) {

          errors.add(ActionMessages.GLOBAL_MESSAGE,
              new ActionMessage("salesRegister.error.noRecordFound"));

        } catch (EJBException ex) {

          if (log.isInfoEnabled()) {
            log.info("EJBException caught in ArRepSalesRegisterAction.execute(): " + ex.getMessage()
                + " session: " + session.getId());

          }

          return (mapping.findForward("cmnErrorPage"));

        }

        if (!errors.isEmpty()) {

          saveErrors(request, new ActionMessages(errors));
          return mapping.findForward("arRepSalesRegister");

        }

        // fill report parameters, fill report to pdf and set report session

        Map parameters = new HashMap();
        parameters.put("company", company);
        parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
        parameters.put("printedBy", user.getUserName());
        parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
        parameters.put("orderBy", actionForm.getOrderBy());
        parameters.put("groupBy", actionForm.getGroupBy());
        parameters.put("viewType", actionForm.getViewType());


        if (actionForm.getCustomerCode() != null) {

          parameters.put("customerCode", actionForm.getCustomerCode());

        }

        if (actionForm.getInvoiceBatchName() != null) {

          parameters.put("invoiceBatchName", actionForm.getInvoiceBatchName());

        }

        /*
         * if (actionForm.getReceiptBatchName() != null) {
         * 
         * parameters.put("receiptBatchName", actionForm.getReceiptBatchName());
         * 
         * }
         */
        if (actionForm.getCustomerType() != null) {

          parameters.put("customerType", actionForm.getCustomerType());

        }

        if (actionForm.getCustomerClass() != null) {

          parameters.put("customerClass", actionForm.getCustomerClass());

        }

        if (actionForm.getDateFrom() != null) {

          parameters.put("dateFrom", actionForm.getDateFrom());

        }

        if (actionForm.getDateTo() != null) {

          parameters.put("dateTo", actionForm.getDateTo());

        }

        parameters.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
        parameters.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());

        if (actionForm.getSalesperson() != null) {

          parameters.put("salesperson", actionForm.getSalesperson());

        }

        if (actionForm.getunpostedOnly()) {

          parameters.put("unpostedOnly", "YES");
          parameters.put("posted", "NO");

        } else {
          parameters.put("unpostedOnly", "NO");
          parameters.put("posted", "YES");

        }

        if (actionForm.getIncludedUnposted() && !actionForm.getunpostedOnly()) {

          parameters.put("includedUnposted", "YES");

        } else {

          parameters.put("includedUnposted", "NO");
          parameters.put("posted", "YES");

        }

        if (actionForm.getIncludedInvoices()) {

          parameters.put("includedInvoices", "YES");

        } else {

          parameters.put("includedInvoices", "NO");

        }

        if (actionForm.getSummary()) {

          parameters.put("summary", "YES");

        } else {

          parameters.put("summary", "NO");

        }
        
        if (actionForm.getIncludedCollections()) {

          parameters.put("includedCollections", "YES");

        } else {

          parameters.put("includedCollections", "NO");

        }

        if (actionForm.getIncludedMiscReceipts()) {

          parameters.put("includedMiscReceipts", "YES");

        } else {

          parameters.put("includedMiscReceipts", "NO");

        }

        if (actionForm.getIncludedCreditMemos()) {

          parameters.put("includedCreditMemos", "YES");

        } else {

          parameters.put("includedCreditMemos", "NO");

        }

        if (actionForm.getIncludedDebitMemos()) {

          parameters.put("includedDebitMemos", "YES");

        } else {

          parameters.put("includedDebitMemos", "NO");

        }

        if (actionForm.getIncludedPriorDues()) {

          parameters.put("includedPriorDues", "YES");

        } else {

          parameters.put("includedPriorDues", "NO");

        }

        if (actionForm.getIncludedCollections()) {

          parameters.put("includedCollections", "YES");

        } else {

          parameters.put("includedCollections", "NO");

        }

        parameters.put("paymentStatus", actionForm.getPaymentStatus());
        parameters.put("showEntries", actionForm.getShowEntries() == true ? "YES" : "NO");

        if (actionForm.getRegion() != null) {

          parameters.put("customerRegion", actionForm.getRegion());

        }

        String source = "";
        boolean first = true;

        if (actionForm.getIncludedInvoices() == true) {

          source += "Invoice";
          first = false;

        }

        if (actionForm.getIncludedMiscReceipts() == true) {

          if (first == false)
            source += ", ";

          source += "Misc Receipts";
          first = false;

        }

        if (actionForm.getIncludedCreditMemos() == true) {

          if (first == false)
            source += ", ";

          source += "Credit Memo";
          first = false;

        }

        if (actionForm.getIncludedDebitMemos() == true) {

          if (first == false)
            source += ", ";

          source += "Debit Memo";

        }

        if (actionForm.getIncludedPriorDues() == true) {

          if (first == false)
            source += ", ";

          source += "Prior Dues";

        }

        if (actionForm.getIncludedCollections() == true) {

          if (first == false)
            source += ", ";

          source += "Collections";

        }

        parameters.put("source", source);
        parameters.put("paymentStatus", actionForm.getPaymentStatus());

        String branchMap = null;
        first = true;
        for (int j = 0; j < actionForm.getArRepBrSrListSize(); j++) {

          ArRepBranchSalesRegisterList brList =
              (ArRepBranchSalesRegisterList) actionForm.getArRepBrSrListByIndex(j);

          if (brList.getBranchCheckbox() == true) {
            if (first) {
              branchMap = brList.getBranchName();
              first = false;
            } else {
              branchMap = branchMap + ", " + brList.getBranchName();
            }
          }

        }
        parameters.put("branchMap", branchMap);


        if (!isStoredProceedure) {


          if (actionForm.getShowEntries()) {

            if (actionForm.getSummarize()) {

              filename = servlet.getServletContext()
                  .getRealPath("jreports/ArRepSalesRegisterSummary.jasper");

            } else {

              filename =
                  "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesRegisterEntries.jasper";

              if (!new java.io.File(filename).exists()) {

                filename = servlet.getServletContext()
                    .getRealPath("jreports/ArRepSalesRegisterEntries.jasper");

              }

            }

          } else {

            if (actionForm.getSummary()) {

              filename = "/opt/ofs-resources/" + user.getCompany() + "/xArRepSalesRegister.jasper";

              if (!new java.io.File(filename).exists()) {

                filename =
                    servlet.getServletContext().getRealPath("jreports/ArRepSalesRegister.jasper");

              }
            } else if (actionForm.getdetailedSales()) {
              filename = "/opt/ofs-resources/" + user.getCompany() + "/yArRepSalesRegister.jasper";

              if (!new java.io.File(filename).exists()) {

                filename =
                    servlet.getServletContext().getRealPath("jreports/ArRepSalesRegister.jasper");

              }
            } else {

              filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepSalesRegister.jasper";

              if (!new java.io.File(filename).exists()) {

                filename =
                    servlet.getServletContext().getRealPath("jreports/ArRepSalesRegister.jasper");

              }
            }

          }
        }

        try {
            
          DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
          Connection conn = null;  

          Report report = new Report();

          if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
            report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
            if(isStoredProceedure){
                conn = dataSource.getConnection(); 
                report.setBytes(JasperRunManager.runReportToPdf(filename, parameters,conn));
                conn.close();
            }else{
              
                report.setBytes(JasperRunManager.runReportToPdf(filename, parameters,
                new ArRepSalesRegisterDS(list, actionForm.getGroupBy())));

            }
           
          } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)) {

             if(isStoredProceedure){
               report.setViewType(Constants.REPORT_VIEW_TYPE_CSV_SP); 
                  conn = dataSource.getConnection();
                   JasperPrint jrprint = JasperFillManager.fillReport(filename, parameters,
                                conn);
                   report.setJasperPrint(jrprint);
                   conn.close(); 
             }else{
                report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
               report.setBytes(JasperRunManagerExt.runReportToXls(filename, parameters,
                new ArRepSalesRegisterDS(list, actionForm.getGroupBy())));
             }
            

          } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)) {

            report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
              if(isStoredProceedure){
                conn = dataSource.getConnection();
                       JasperPrint jrprint = JasperFillManager.fillReport(filename, parameters,
                                    conn);
                       report.setJasperPrint(jrprint);
                       conn.close();
              }else{
                  report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
                new ArRepSalesRegisterDS(list, actionForm.getGroupBy())));
              
              }
            
          

          }

          session.setAttribute(Constants.REPORT_KEY, report);
          actionForm.setReport(Constants.STATUS_SUCCESS);

        } catch (Exception ex) {

          if (log.isInfoEnabled()) {
            log.info("Exception caught in ArRepSalesRegisterAction.execute(): " + ex.getMessage()
                + " session: " + session.getId());

          }
          ex.printStackTrace();

          return (mapping.findForward("cmnErrorPage"));

        }

        /*******************************************************
         * -- AR SR Close Action --
         *******************************************************/

      } else if (request.getParameter("closeButton") != null) {

        return (mapping.findForward("cmnMain"));

        /*******************************************************
         * -- AR SR Customer Enter Action --
         *******************************************************/

      } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

        return (mapping.findForward("arRepSalesRegister"));

        /*******************************************************
         * -- AR SR Load Action --
         *******************************************************/

      }

      if (frParam != null) {

        try {

          actionForm.reset(mapping, request);

          ArrayList list = null;
          Iterator i = null;

          actionForm.clearInvoiceBatchNameList();

          list =
              ejbSR.getArIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setInvoiceBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setInvoiceBatchNameList((String) i.next());

            }

          }


          /*
           * actionForm.clearReceiptBatchNameList();
           * 
           * list = ejbSR.getArRbAll(new Integer(user.getCurrentBranch().getBrCode()),
           * user.getCmpCode());
           * 
           * if (list == null || list.size() == 0) {
           * 
           * actionForm.setReceiptBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
           * 
           * } else {
           * 
           * i = list.iterator();
           * 
           * while (i.hasNext()) {
           * 
           * actionForm.setReceiptBatchNameList((String)i.next());
           * 
           * }
           * 
           * }
           */

          actionForm.clearCustomerTypeList();

          list = ejbSR.getArCtAll(user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setCustomerTypeList((String) i.next());

            }

          }

          actionForm.clearCustomerClassList();

          list = ejbSR.getArCcAll(user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setCustomerClassList((String) i.next());

            }

          }

          actionForm.clearCustomerBatchList();

          list = ejbSR.getAdLvCustomerBatchAll(user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setCustomerBatchList((String) i.next());

            }

          }

          actionForm.clearArRepBrSrList();

          list = ejbSR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

          i = list.iterator();

          while (i.hasNext()) {

            AdBranchDetails details = (AdBranchDetails) i.next();

            ArRepBranchSalesRegisterList arRepBrSrList = new ArRepBranchSalesRegisterList(
                actionForm, details.getBrBranchCode(), details.getBrName(), details.getBrCode());
            arRepBrSrList.setBranchCheckbox(true);

            actionForm.saveArRepBrSrList(arRepBrSrList);

          }

          actionForm.clearRegionList();

          list = ejbSR.getAdLvArRegionAll(user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setRegionList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              String region = (String) i.next();

              actionForm.setRegionList(region);

            }

          }

          actionForm.clearSalespersonList();

          list = ejbSR.getArSlpAll(user.getCmpCode());

          if (list == null || list.size() == 0) {

            actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);

          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setSalespersonList((String) i.next());

            }

          }

          actionForm.clearReportTypeList();

          list = ejbSR.getAdLvReportTypeAll(user.getCmpCode());

          if (list == null || list.size() == 0) {


            actionForm.setReportTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
          } else {

            i = list.iterator();

            while (i.hasNext()) {

              actionForm.setReportTypeList((String) i.next());

            }

          }

        } catch (EJBException ex) {

          if (log.isInfoEnabled()) {
            log.info("EJBException caught in ArRepSalesRegisterAction.execute(): " + ex.getMessage()
                + " session: " + session.getId());

          }

          return (mapping.findForward("cmnErrorPage"));

        }

        return (mapping.findForward("arRepSalesRegister"));

      } else {

        errors.add(ActionMessages.GLOBAL_MESSAGE,
            new ActionMessage("errors.responsibilityAccessNotAllowed"));
        saveErrors(request, new ActionMessages(errors));

        return (mapping.findForward("cmnMain"));

      }

    } catch (Exception e) {


      /*******************************************************
       * System Failed: Forward to error page
       *******************************************************/
      if (log.isInfoEnabled()) {

        log.info("Exception caught in ArRepSalesRegisterAction.execute(): " + e.getMessage()
            + " session: " + session.getId());

      }

      return (mapping.findForward("cmnErrorPage"));

    }
  }
}
