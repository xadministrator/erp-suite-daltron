package com.struts.jreports.ar.salesregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepSalesRegisterDetails;

public class ArRepSalesRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepSalesRegisterDS(ArrayList list, String groupBy) {
   	
   	  Iterator i = list.iterator();
   	
      while (i.hasNext()) {
      	
         ArRepSalesRegisterDetails details = (ArRepSalesRegisterDetails)i.next();
         
         String group = null;     
         String groupCoa = null;
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getSrCstCustomerCode();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
         	
         	group = details.getSrCstCustomerType();
         	
         } else if (groupBy.equals("CUSTOMER CLASS")) {
         	
         	group = details.getSrCstCustomerClass();
         	
         } else if (groupBy.equals("SALESPERSON")) {
          	
          	group = details.getSrSlsSalespersonCode();
          	
         } else if (groupBy.equals("PRODUCT")) {
           	
           	group = details.getSrDescription();
           	
         } else if (groupBy.equals("MEMO LINE")) {
          	
          	group = details.getSrMemoName();
          	
          }
         
         groupCoa = details.getSrDrGlAccountNumber();
                  System.out.println("-------------- account: "+ details.getSrDrGlAccountNumber());
                  
	     ArRepSalesRegisterData argData = new ArRepSalesRegisterData(details.getSrDate(), 
	     details.getSrCstCustomerCode(), details.getSrDescription(),
	     details.getSrInvoiceNumber(), details.getSrOrNumber(), details.getSrOrAmount(), details.getSrReferenceNumber(), details.getSrIiPercentMarkup(),
	     new Double(details.getSrRevenueAmount()), new Double(details.getSrAmount()), new Double(details.getSrServiceAmount()), group,  new Double(details.getSrGrossAmount()),  new Double(details.getSrNetAmount()), 
		 new Double(details.getSrTaxAmount()), details.getSrSlsSalespersonCode(), details.getSrSlsName(),
		 details.getSrDrGlAccountNumber(), details.getSrDrGlAccountDescription(), new Double(details.getSrDrDebitAmount()), 
		 new Double(details.getSrDrCreditAmount()), new Boolean(details.getSrInvCreditMemo()), new Double(details.getSrInvTotalNetAmount()),
		 new Double(details.getSrInvTotalTaxAmount()), new Double(details.getSrInvTotalAmount()), details.getSrCstName(),
		 details.getSrCstCustomerCode2(), details.getSrRctType(), new Double(details.getSrDiscountAmount()), 
		 details.getSrInvReceiptNumbers(), details.getSrInvReceiptDates(), groupCoa, new Byte(details.getPosted()), details.getCmInvoiceNumber(),
		 new Double(details.getSrAiApplyAmount()), new Double(details.getSrAiCreditableWTax()), 
		 new Double(details.getSrAiDiscountAmount()), new Double(details.getSrAiAppliedDeposit()), details.getSrMemoName(), 
		 new Double(details.getSrQuantity()), new Double(details.getSrLineAmount()), new Double(details.getSrLineSAmount()), 
		 new Double(details.getSrLineTaxAmount()), new Double(details.getSrCpf()), new Double (details.getSrCstBalance()), new Double (details.getSrBalance()));

	     data.add(argData);
	     
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("invoiceDate".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getInvoiceDate();       
      }else if("customerCode".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getCustomerCode();
      }else if("description".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getDescription();   
      }else if("invoiceNumber".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getInvoiceNumber();
       }else if("orNumber".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getOrNumber();
      }else if("orAmount".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getOrAmount();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getReferenceNumber();
      }else if("percentMarkup".equals(fieldName)){
         value = ((ArRepSalesRegisterData)data.get(index)).getPercentMarkup();
      }else if("amount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAmount();
      }else if("serviceAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getserviceAmount();
      }else if("groupBy".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getGroupBy();
      }else if("grossAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getGrossAmount();
      }else if("revenueAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getRevenueAmount();
      }else if("netAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getNetAmount();
      }else if("taxAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getTaxAmount();
      }else if("salespersonCode".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getSalespersonCode();
      }else if("salespersonName".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getSalespersonName();
      }else if("accountNumber".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAccountDescription();
      }else if("debitAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getDebitAmount();
      }else if("creditAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getCreditAmount();
      }else if("isCreditMemo".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getIsCreditMemo();
      }else if("totalNetAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getTotalNetAmount();
      }else if("totalTaxAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getTotalTaxAmount();
      }else if("totalAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getTotalAmount();
      }else if("customerName".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getCustomerName();
      }else if("customerCode2".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getCustomerCode2();        
      }else if("receiptType".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getReceiptType();        
      }else if("discountAmount".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getDiscountAmount();        
      }else if("invoiceReceiptNumbers".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getInvoiceReceiptNumbers();        
      }else if("invoiceReceiptDates".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getInvoiceReceiptDates();        
      }else if("groupCoa".equals(fieldName)){
        value = ((ArRepSalesRegisterData)data.get(index)).getGroupCoa();  
      }else if("posted".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getPosted();
      }else if("cmInvoiceNumber".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getCmInvoiceNumber();
      }else if("aiApplyAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAiApplyAmount();
      }else if("aiCreditableWTax".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAiCreditableWTax();
      }else if("aiDiscountAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAiDiscountAmount();
      }else if("aiAppliedDeposit".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getAiAppliedDeposit();
      }else if("memoName".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getMemoName();
      }else if("quantity".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getQuantity();
          System.out.println("QTY "+value);
      }else if("lineAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getLineAmount();
          System.out.println("lineAmount "+value);
      }else if("lineSAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getLineSAmount();
          System.out.println("lineSAmount "+value);
      }else if("lineTaxAmount".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getLineTaxAmount();
          System.out.println("lineTaxAmount "+value);
      }else if("cpf".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getCpf();
          System.out.println("cpf "+value);
      }else if("customerBalance".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getCustomerBalance();
      }else if("balance".equals(fieldName)){
          value = ((ArRepSalesRegisterData)data.get(index)).getBalance();
      }
      
      return(value);
   }
}
