package com.struts.jreports.ar.salesregister;


public class ArRepSalesRegisterData implements java.io.Serializable {
	
	private java.util.Date invoiceDate = null;
	private String customerCode = null;
	private String description = null;
	private String invoiceNumber = null;
        private String orNumber = null;
        private Double orAmount = null;
	private String referenceNumber = null;
	private Double percentMarkup = null;
	private Double amount = null;
	private String groupBy = null;
	private Double grossAmount = null;
	private Double revenueAmount = null;
	private Double netAmount = null;
	private Double taxAmount = null;
	private Double lineAmount = null;
	private Double lineSAmount = null;
	private Double lineTaxAmount = null;
	private String salespersonCode = null;
	private String salespersonName = null;
	private String accountNumber = null;
	private String accountDescription = null;
	private Double debitAmount = null;
	private Double creditAmount = null;
	private Boolean isCreditMemo = null;
	private Double totalNetAmount = null;
	private Double totalTaxAmount = null;
	private Double totalAmount = null;
	private String customerName = null;
	private String customerCode2 = null;
	private String receiptType = null;
	private Double discountAmount = null;
	private String invoiceReceiptNumbers = null;
	private String invoiceReceiptDates = null;
	private String groupCoa = null;
	private Byte posted;
	private String cmInvoiceNumber;
	private String memoName;
	private Double qty;
	
	private Double aiApplyAmount = null;
	private Double aiCreditableWTax = null;
	private Double aiDiscountAmount = null;
	private Double aiAppliedDeposit = null;
	private Double cpf;
	private Double serviceAmount = null;
	private Double customerBalance = null;
	private Double balance = null;
	
	public ArRepSalesRegisterData(java.util.Date invoiceDate, String customerCode,
			String description, String invoiceNumber, String orNumber, Double orAmount, String referenceNumber, Double percentMarkup,
			Double revenueAmount, Double amount, Double serviceAmount, String groupBy, Double grossAmount, Double netAmount, Double taxAmount,
			String salespersonCode, String salespersonName, String accountNumber, String accountDescription,
			Double debitAmount, Double creditAmount, Boolean isCreditMemo, Double totalNetAmount, Double totalTaxAmount,
			Double totalAmount, String customerName, String customerCode2, String receiptType, Double discountAmount,
			String invoiceReceiptNumbers, String invoiceReceiptDates, String groupCoa, Byte posted, String cmInvoiceNumber,
			Double aiApplyAmount, Double aiCreditableWTax, Double aiDiscountAmount, Double aiAppliedDeposit, String memoName, Double qty, 
			Double lineAmount, Double lineSAmount, Double lineTaxAmount, Double cpf, Double customerBalance, Double balance) {
		
		this.invoiceDate = invoiceDate;      	
		this.customerCode = customerCode;
		this.description = description;
		this.invoiceNumber = invoiceNumber;
                this.orNumber = orNumber;   
                this.orAmount = orAmount;
                this.percentMarkup = percentMarkup;
		this.referenceNumber = referenceNumber;
		this.revenueAmount = revenueAmount;
		this.amount = amount;
		this.serviceAmount = serviceAmount;
		this.groupBy = groupBy;
		this.grossAmount = grossAmount;
		this.netAmount = netAmount;
		this.taxAmount = taxAmount;
		this.salespersonCode = salespersonCode;
		this.salespersonName = salespersonName;
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.isCreditMemo = isCreditMemo;
		this.totalNetAmount = totalNetAmount;
		this.totalTaxAmount = totalTaxAmount;
		this.totalAmount = totalAmount;
		this.customerName = customerName;
		this.customerCode2 = customerCode2;
		this.receiptType = receiptType;
		this.discountAmount = discountAmount;
		this.invoiceReceiptNumbers = invoiceReceiptNumbers;
		this.invoiceReceiptDates = invoiceReceiptDates;
		this.groupCoa = groupCoa;
		this.posted = posted;
		this.cmInvoiceNumber = cmInvoiceNumber;
		this.aiApplyAmount = aiApplyAmount;
		this.aiCreditableWTax = aiCreditableWTax;
		this.aiDiscountAmount = aiDiscountAmount;
		this.aiAppliedDeposit = aiAppliedDeposit;
		this.memoName = memoName;
		this.qty = qty;
		this.lineAmount = lineAmount;
		this.lineSAmount = lineSAmount;
		this.lineTaxAmount = lineTaxAmount;
		this.cpf = cpf;
		//this.serviceAmount = serviceAmount;
		this.customerBalance = customerBalance;
		this.balance = balance;
	}
	
	public java.util.Date getInvoiceDate() {
		
		return invoiceDate;
		
	}
	
	public String getCustomerCode() {
		
		return customerCode;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getInvoiceNumber() {
		
		return invoiceNumber;
		
	}
        
        public String getOrNumber() {
		
		return orNumber;
		
	}
        
        public Double getOrAmount() {
		
            return orAmount;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public Double getPercentMarkup() {
		
		return percentMarkup;
		
	}
	

	public Double getRevenueAmount() {
		
		return revenueAmount;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
	
	public Double getserviceAmount() {
		
		return serviceAmount;
		
	}
	
	public String getGroupBy() {
		
		return groupBy;
		
	}
	
	
	public Double getGrossAmount() {
		
		return grossAmount;
		
	}
	
	public Double getNetAmount() {
		
		return netAmount;
		
	}
	
	public Double getTaxAmount() {
		
		return taxAmount;
		
	}
	
	public String getSalespersonCode() {
		
		return salespersonCode;
		
	}
	
	public String getSalespersonName() {
		
		return salespersonName;
		
	}
	
	public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public String getAccountDescription() {
		
		return accountDescription;
		
	}
	
	public Double getDebitAmount() {
		
		return debitAmount;
		
	}
	
	public Double getCreditAmount() {
		
		return creditAmount;
		
	}
	
	public Boolean getIsCreditMemo() {
		
		return isCreditMemo;
		
	}
	
	public Double getTotalNetAmount() {
		
		return totalNetAmount;
		
	}
	
	public Double getTotalTaxAmount() {
		
		return totalTaxAmount;
		
	}
	
	public Double getTotalAmount() {
		
		return totalAmount;
		
	}
	
	public String getCustomerName() {
		
		return customerName;
		
	}
	
	public String getCustomerCode2() {
		
		return customerCode2;
		
	}
	
	public String getReceiptType() {
		
		return receiptType;
		
	}
	
	public Double getDiscountAmount() {
		
		return discountAmount;
		
	}
	
	public String getInvoiceReceiptNumbers() {
		
		return invoiceReceiptNumbers;
		
	}
	
	public String getInvoiceReceiptDates() {
		
		return invoiceReceiptDates;
		
	} 
	
	public String getGroupCoa() {
		
		return groupCoa;
		
	} 
	
	public Byte getPosted() {
		
		return posted;
		
	} 
	
	public String getCmInvoiceNumber() {
		
		return cmInvoiceNumber;
		
	} 
	
	public Double getAiApplyAmount() {
		
		return aiApplyAmount;
		
	}
	public Double getAiCreditableWTax() {
		
		return aiCreditableWTax;
		
	}
	public Double getAiDiscountAmount() {
		
		return aiDiscountAmount;
		
	}
	public Double getAiAppliedDeposit() {
		
		return aiAppliedDeposit;
		
	}
	
	public String getMemoName(){

		return memoName;
	}

	public Double getQuantity() {

		return qty;

	}

	public Double getLineAmount() {

		return lineAmount;

	}
	
	public Double getLineSAmount() {

		return lineSAmount;

	}
	
	public Double getLineTaxAmount() {

		return lineTaxAmount;

	}

	public Double getCpf() {

		return cpf;

	}
	public Double getCustomerBalance() {

		return customerBalance;

	}
	public Double getBalance() {

		return balance;

	}
}
