package com.struts.jreports.ar.salesregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepSalesRegisterForm extends ActionForm implements Serializable{

	private String customerCode = null;
	private String customerType = null;
	private ArrayList customerTypeList = new ArrayList();
	private String customerClass = null;
	private ArrayList customerClassList = new ArrayList();
	private String customerBatch = null;
	private ArrayList customerBatchList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;
	private String invoiceNumberFrom = null;
	private String invoiceNumberTo = null;
	private String salesperson = null;
	private ArrayList salespersonList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String paymentStatus = null;
	private ArrayList paymentStatusList = new ArrayList();
	private boolean includedUnposted = false;
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private String isCustomerEntered = null;
	private boolean includedInvoices = false;
	private boolean includedCreditMemos = false;
	private boolean includedMiscReceipts = false;
	private boolean includedDebitMemos = false;
	private boolean showEntries = false;
	private boolean printSalesRelatedOnly = false;
	private boolean unpostedOnly = false;
	private boolean includedPriorDues = false;
	private boolean includedCollections = false;
	private boolean summary = false;
	private boolean detailedSales = false;
	private String region = null;
	private ArrayList regionList = new ArrayList();
	private boolean summarize = false;
	private String invoiceBatchName = null;
	private ArrayList invoiceBatchNameList = new ArrayList();
	private String receiptBatchName = null;
	private ArrayList receiptBatchNameList = new ArrayList();
	private byte posted;
	
	private ArrayList arRepBrSrList = new ArrayList();

	private HashMap criteria = new HashMap();

	private String userPermission = new String();
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();

	public HashMap getCriteria() {

		return criteria;

	}

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public String getCustomerCode(){
		return(customerCode);
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getDateFrom(){
		return(dateFrom);
	}

	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}

	public String getDateTo(){
		return(dateTo);
	}

	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}

	public String getInvoiceNumberFrom(){
		return(invoiceNumberFrom);
	}

	public void setInvoiceNumberFrom(String invoiceNumberFrom){
		this.invoiceNumberFrom = invoiceNumberFrom;
	}

	public String getInvoiceNumberTo(){
		return(invoiceNumberTo);
	}

	public void setInvoiceNumberTo(String invoiceNumberTo){
		this.invoiceNumberTo = invoiceNumberTo;
	}

	public void setGoButton(String goButton){
		this.goButton = goButton;
	}

	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}

	public String getIsCustomerEntered() {
		return isCustomerEntered;
	}

	public void setIsCustomerEntered(String isCustomerEntered) {
		this.isCustomerEntered = isCustomerEntered;
	}

	public String getCustomerType(){
		return(customerType);
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public ArrayList getCustomerTypeList(){
		return(customerTypeList);
	}

	public void setCustomerTypeList(String customerType){
		customerTypeList.add(customerType);
	}

	public void clearCustomerTypeList(){
		customerTypeList.clear();
		customerTypeList.add(Constants.GLOBAL_BLANK);
	}

	public String getCustomerClass(){
		return(customerClass);
	}

	public void setCustomerClass(String customerClass){
		this.customerClass = customerClass;
	}

	public ArrayList getCustomerClassList(){
		return(customerClassList);
	}

	public void setCustomerClassList(String customerClass){
		customerClassList.add(customerClass);
	}

	public void clearCustomerClassList(){
		customerClassList.clear();
		customerClassList.add(Constants.GLOBAL_BLANK);
	}
	
	
	public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }

	public String getSalesperson(){
		return(salesperson);
	}

	public void setSalesperson(String salesperson){
		this.salesperson = salesperson;
	}

	public ArrayList getSalespersonList(){
		return(salespersonList);
	}

	public void setSalespersonList(String salesperson){
		salespersonList.add(salesperson);
	}

	public void clearSalespersonList(){
		salespersonList.clear();
		salespersonList.add(Constants.GLOBAL_BLANK);
	}

	public String getOrderBy(){
		return(orderBy);   	
	}

	public void setOrderBy(String orderBy){
		this.orderBy = orderBy;
	}

	public ArrayList getOrderByList(){
		return orderByList;
	}

	public String getGroupBy(){
		return(groupBy);   	
	}

	public void setGroupBy(String groupBy){
		this.groupBy = groupBy;
	}

	public ArrayList getGroupByList(){
		return groupByList;
	}

	public String getViewType(){
		return(viewType);   	
	}

	public void setViewType(String viewType){
		this.viewType = viewType;
	}

	public ArrayList getViewTypeList(){
		return viewTypeList;
	}

	public boolean getIncludedUnposted() {

		return includedUnposted;

	}

	public void setIncludedUnposted(boolean includedUnposted) {

		this.includedUnposted = includedUnposted;

	}

	public boolean getIncludedInvoices() {

		return includedInvoices;

	}

	public void setIncludedInvoices(boolean includedInvoices) {

		this.includedInvoices = includedInvoices;

	}

	public boolean getIncludedCreditMemos() {

		return includedCreditMemos;

	}

	public void setIncludedCreditMemos(boolean includedCreditMemos) {

		this.includedCreditMemos = includedCreditMemos;

	}

	public boolean getIncludedMiscReceipts() {

		return includedMiscReceipts;

	}

	public void setIncludedMiscReceipts(boolean includedMiscReceipts) {

		this.includedMiscReceipts = includedMiscReceipts;

	}

	public boolean getIncludedDebitMemos() {

		return includedDebitMemos;

	}

	public void setIncludedDebitMemos(boolean includedDebitMemos) {

		this.includedDebitMemos = includedDebitMemos;

	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}

	public String getPaymentStatus() {

		return paymentStatus;

	}

	public void setPaymentStatus(String paymentStatus) {

		this.paymentStatus = paymentStatus;

	}

	public ArrayList getPaymentStatusList() {

		return paymentStatusList;

	}

	public boolean getShowEntries() {

		return showEntries;

	}

	public void setShowEntries(boolean showEntries) {

		this.showEntries = showEntries;

	}
	public boolean getprintSalesRelatedOnly() {

		return printSalesRelatedOnly;

	}

	public void setprintSalesRelatedOnly(boolean printSalesRelatedOnly) {

		this.printSalesRelatedOnly = printSalesRelatedOnly;

	}
	
	public boolean getunpostedOnly() {

		return unpostedOnly;

	}

	public void setunpostedOnly(boolean unpostedOnly) {

		this.unpostedOnly = unpostedOnly;

	}


	public boolean getIncludedPriorDues() {

		return includedPriorDues;

	}

	public void setIncludedPriorDues(boolean includedPriorDues) {

		this.includedPriorDues = includedPriorDues;

	}

	public boolean getIncludedCollections() {

		return includedCollections;

	}

	public void setIncludedCollections(boolean includedCollections) {

		this.includedCollections = includedCollections;

	}

	public Object[] getArRepBrSrList(){

		return arRepBrSrList.toArray();

	}

	public ArRepBranchSalesRegisterList getArRepBrSrListByIndex(int index){

		return ((ArRepBranchSalesRegisterList)arRepBrSrList.get(index));

	}

	public int getArRepBrSrListSize(){

		return(arRepBrSrList.size());

	}

	public void saveArRepBrSrList(Object newArRepBrSrList){

		arRepBrSrList.add(newArRepBrSrList);   	  

	}

	public void clearArRepBrSrList(){

		arRepBrSrList.clear();

	}

	public String getRegion(){
		return(region);
	}

	public void setRegion(String region){
		this.region = region;
	}

	public ArrayList getRegionList(){
		return(regionList);
	}

	public void setRegionList(String region){
		regionList.add(region);
	}

	public void clearRegionList(){
		regionList.clear();
		regionList.add(Constants.GLOBAL_BLANK);
	} 

	public boolean getSummarize() {

		return summarize;

	}

	public void setSummarize(boolean summarize) {

		this.summarize = summarize;

	}
	
	public boolean getSummary() {

		return summary;

	}

	public void setSummary(boolean summary) {

		this.summary = summary;

	}	
	
	public boolean getdetailedSales() {

		return detailedSales;

	}

	public void setdetailedSales(boolean detailedSales) {

		this.detailedSales = detailedSales;

	}	

	public String getInvoiceBatchName() {

		return invoiceBatchName;

	}

	public void setInvoiceBatchName(String invoiceBatchName) {

		this.invoiceBatchName = invoiceBatchName;

	}

	public ArrayList getInvoiceBatchNameList() {

		return invoiceBatchNameList;

	}

	public void setInvoiceBatchNameList(String invoiceBatchName) {

		invoiceBatchNameList.add(invoiceBatchName);

	}

	public void clearInvoiceBatchNameList() {   	 

		invoiceBatchNameList.clear();
		invoiceBatchNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getReceiptBatchName() {

		return receiptBatchName;

	}

	public void setReceiptBatchName(String receiptBatchName) {

		this.receiptBatchName = receiptBatchName;

	}

	public ArrayList getReceiptBatchNameList() {

		return receiptBatchNameList;

	}

	public void setReceiptBatchNameList(String receiptBatchName) {

		receiptBatchNameList.add(receiptBatchName);

	}

	public void clearReceiptBatchNameList() {   	 

		receiptBatchNameList.clear();
		receiptBatchNameList.add(Constants.GLOBAL_BLANK);

	}

	
	public byte getPosted(){
		return posted;
	}

	public void setPosted(byte posted){
		this.posted = posted;
		System.out.println("ArRepSalesRegisterForm "+this.posted);
	}
	
	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}




	public void reset(ActionMapping mapping, HttpServletRequest request){      

		for (int i=0; i<arRepBrSrList.size(); i++) {

			ArRepBranchSalesRegisterList actionList = (ArRepBranchSalesRegisterList)arRepBrSrList.get(i);
			actionList.setBranchCheckbox(false);

		}

		goButton = null;
		closeButton = null;
		customerCode = null;
		customerClass = Constants.GLOBAL_BLANK; 
		customerType = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = null;
		invoiceNumberFrom = null;
		invoiceNumberTo = null;
		isCustomerEntered = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		paymentStatusList.clear();
		paymentStatusList.add(Constants.GLOBAL_BLANK);
		paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_PAID);
		paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_UNPAID);
		paymentStatus = Constants.GLOBAL_BLANK;
		orderByList.clear();
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_NONE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_DATE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_CUSTOMER_CODE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_CUSTOMER_TYPE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_INVOICE_NUMBER);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_SALESPERSON);
		orderByList.add(Constants.AR_CL_ORDER_BY_MEMO_LINE);
		orderBy = Constants.AR_SALES_REGISTER_ORDER_BY_DATE;   
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
		groupByList.add(Constants.AR_CL_ORDER_BY_SALESPERSON);
		groupByList.add(Constants.AR_CL_ORDER_BY_MEMO_LINE);
		groupByList.add(Constants.AR_CL_ORDER_BY_PRODUCT);
		groupBy = Constants.AR_CL_ORDER_BY_CUSTOMER_CODE;   
		includedUnposted = false;
		includedInvoices = false;
		includedMiscReceipts = false;
		includedCreditMemos = false;
		includedDebitMemos = false;
		showEntries = false;
		summary = false;
		detailedSales = false;
		includedPriorDues = false;
		includedCollections = false;
		region = Constants.GLOBAL_BLANK;
		summarize = false;
		printSalesRelatedOnly = false;
		unpostedOnly = false;
		posted = 1;
	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if(!Common.validateDateFormat(dateFrom)){
				errors.add("dateFrom", new ActionMessage("salesRegister.error.dateFromInvalid"));
			}
			if(!Common.validateDateFormat(dateTo)){
				errors.add("dateTo", new ActionMessage("salesRegister.error.dateToInvalid"));
			}
			if(!Common.validateStringExists(customerTypeList, customerType)){
				errors.add("customerType", new ActionMessage("salesRegister.error.customerTypeInvalid"));
			}
			if(!Common.validateStringExists(customerClassList, customerClass)){
				errors.add("customerClass", new ActionMessage("salesRegister.error.customerClassInvalid"));
			}	 
			if(includedInvoices == false && includedMiscReceipts == false
					&& includedCreditMemos == false && includedDebitMemos == false) {
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("salesRegister.error.sourceMustBeEntered"));
			}
			if(includedPriorDues == true && Common.validateRequired(dateFrom)) {
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("salesRegister.error.dateFromRequiredForPriorDues"));
			}
		}
		return(errors);
	}

}
