package com.struts.jreports.ar.sendemailstatement;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;










import com.sun.mail.smtp.SMTPTransport;

import java.security.Security;









import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;











import javax.mail.internet.MimeMultipart;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.commons.io.FileUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;



import com.struts.ar.invoiceentry.ArInvoiceLineItemList;
import com.struts.jreports.ar.statement.ArRepStatementDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;




public class ArRepSendEmailStatementAction extends Action{
	
	 private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	 public ActionForward execute(ActionMapping mapping,  ActionForm form,
		      HttpServletRequest request, HttpServletResponse response)
		      throws Exception {

		      HttpSession session = request.getSession();
		      
		      
		      try{
		    	  
		    	  User user = (User) session.getAttribute(Constants.USER_KEY);

		          if (user != null) {

		             if (log.isInfoEnabled()) {

		                 log.info("ArRepSendEmailStatement: Company '" + user.getCompany() + "' User '" + user.getUserName() +
		                 "' performed this action on session " + session.getId());
		                 
		                 
		                 
		             }
		             
		             
		             
		             

		          } else {

		             if (log.isInfoEnabled()) {

		                log.info("User is not logged on in session" + session.getId());

		             }

		             return(mapping.findForward("adLogon"));

		          }
		          
		          
		          ArRepSendEmailStatementForm actionForm = (ArRepSendEmailStatementForm)form;
		          
		          ArrayList cstmrListtest = actionForm.getArCtmrList();
	      	      
	      	      
	      	      for(int x=0;x <cstmrListtest.size();x++){
	      	    	  
	      	    	  ArRepSendEmailStatementList data = (ArRepSendEmailStatementList)cstmrListtest.get(x);
	      	    	  
	      	    	  System.out.println("status " + x + ":" + data.getSelectedMail());
	      	      }
	      	      
	      	      
	      	    
		          
		          
		          // reset report to null
		          actionForm.setReport(null);

		         String frParam = Common.getUserPermission(user, Constants.AR_REP_STATEMENT_ID);

		         if (frParam != null) {

			      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

			         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
		               if (!fieldErrors.isEmpty()) {

		                  saveErrors(request, new ActionMessages(fieldErrors));

		                  return mapping.findForward("	");
		               }

		            }

		            actionForm.setUserPermission(frParam.trim());

		           
		            
		            
		            
		         } else {

		            actionForm.setUserPermission(Constants.NO_ACCESS);

		         }


		        
		        
		         
		         
		         ActionErrors errors = new ActionErrors();
		         
		    	 /*** get report session and if not null set it to null **/
		    	 
		    	 Report reportSession = 
		    	    (Report)session.getAttribute(Constants.REPORT_KEY);
		    	    
		    	 if(reportSession != null) {
		    	 	
		    	    reportSession.setBytes(null);
		    	    session.setAttribute(Constants.REPORT_KEY, reportSession);
		    	    
		    	 }

/******************************************************* 	   
 * 		Send Button 
*******************************************************/		    	  
		      	 else if (request.getParameter("sendButton") != null &&
		                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
		      		 
		      		 String cc1 = "";
		      		 String cc2 = "";
		      		 
		      	      if(actionForm.getSaveInfo()){
		      	    	  System.out.println("save info");
		      	    	File file = new File("c:/opt/ofs-statements/"+ user.getCompany() + "/email/send_email_info.txt");
		      	    	FileWriter fw = new FileWriter(file.getAbsoluteFile());
						BufferedWriter bw = new BufferedWriter(fw);
						
						bw.write(actionForm.getEmailHost());
						bw.newLine();
						bw.write(actionForm.getEmailPort());
						bw.newLine();
						bw.write(actionForm.getEmailUsername());
						bw.newLine();
						bw.write(actionForm.getEmailPassword());
						bw.newLine();
						if(actionForm.getCc1().trim().equals("")){
							bw.write("none");
							cc1 = "none";
						}else{
							bw.write(actionForm.getCc1());
							cc1 = actionForm.getCc1();
						}
						bw.newLine();
						if(actionForm.getCc2().trim().equals("")){
							bw.write("none");
							cc2 = "none";
						}else{
							bw.write(actionForm.getCc2());
							cc2 = actionForm.getCc2();
						}
						bw.newLine();
						bw.write(actionForm.getEmailSubject());
						bw.newLine();
						bw.write(actionForm.getEmailMessage());
						bw.close();	 
		      	      }
		        	 
		      	      /*
		      	      String host = "mail.gmail.com";
		      		  String username = "ruben.lamberte@omegabci.com";
		      	      String password = "123P@ssw0rd";
		      	      String recipient = "ruben.lamberte@gmail.com";
		      	      String title = actionForm.getEmailSubject();
		      	      String message = actionForm.getEmailMessage();
		      	      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		      	      
		      	      */
		      	      
		      	      String host = actionForm.getEmailHost();
		      	      String username = actionForm.getEmailUsername();
		      	     String password = actionForm.getEmailPassword();
		      	   // String username = "ruben.lamberte@omegabci.com";
		      	    //  String password = "123P@ssw0rd";
		      	      
		      	      String title = actionForm.getEmailSubject();
		      	      String message = actionForm.getEmailMessage();
		      	      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		      	      String port_num = actionForm.getEmailPort();
		      	   
		      	      
		      	      Session mailSession;
		      	      MimeMessage msg = null;
		      	      ArrayList cstmrList = new ArrayList();
		      	      boolean IfNoSelected = true;
		      	      
		      	   
		      	      
		      	      
		      	      
		      	      cstmrList = actionForm.getArCtmrList();
		      	      
		      	      
		      	      for(int x=0;x <cstmrList.size();x++){
		      	    	  
		      	    	  ArRepSendEmailStatementList data = (ArRepSendEmailStatementList)cstmrList.get(x);
		      	    	  
		      	    	  if(data.getSelectedMail()){
		      	    		IfNoSelected = false;
		      	    		System.out.println("the customer is " + data.getCustomerCode() + " and bool is: " + data.getSelectedMail());
		      	    		  
		      	    		 // String sendTo = data.getCustomerEmail();
		      	    		 String sendTo = data.getCustomerEmail();
		      	    		  try{
		      	    			Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());	
		      	    			
		      	    	        // Get a Properties object
		      	    	        Properties props = System.getProperties();
		      	    	        props.setProperty("mail.smtps.host", host);
		      	    	        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		      	    	        props.setProperty("mail.smtp.socketFactory.fallback", "false");
		      	    	        props.setProperty("mail.smtp.port", port_num);
		      	    	        props.setProperty("mail.smtp.socketFactory.port", "465");
		      	    	        props.setProperty("mail.smtps.auth", "true");
		      	    	        props.setProperty("mail.smtp.starttls.enable", "true");
		      	    	        props.put("mail.smtps.quitwait", "false");
		      	    	        Session sessionMail;
		      	    	        SMTPTransport t ;
		      	    	        try{
		      	    	        	sessionMail = Session.getInstance(props, null);
			      	    	        t = (SMTPTransport)sessionMail.getTransport("smtp");
				      	            t.connect(host, username, password);
		      	    	        }catch(Exception ex){
		      	    	        	System.out.println("error 1: " + ex.toString());
		      	    	        	throw new MessagingException();
		      	    	        }
		      	    	        msg = new MimeMessage(sessionMail);
		      	    	        MimeBodyPart attachBodyPart = new MimeBodyPart();
		      	    	        MimeBodyPart messageBodyPart = new MimeBodyPart();

		      	                Multipart multipart = new MimeMultipart();
		      	                
		      	                attachBodyPart = new MimeBodyPart();
		      	                DataSource source = new FileDataSource(data.getFileLocation());
		      	                
		      	              
		      	                attachBodyPart.setDataHandler(new DataHandler(source));
		      	                attachBodyPart.setFileName(data.getCustomerCode()+ ".pdf");
		      	                
		      	                
		      	                messageBodyPart.setText(actionForm.getEmailMessage());
		      	                
		      	                
		      	                
		      	                
		      	                multipart.addBodyPart(attachBodyPart);
		      	                multipart.addBodyPart(messageBodyPart);
		      	              
		      	             
			      	              
			      	         
		      	    	      
		      	    	        
		      	    	        msg.setFrom(new InternetAddress(username));
			      	            msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(sendTo, false));
			      	            
			      	            if(!cc1.equals("none")){
			      	            	 msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc1,false));
					      	           
			      	            }
			      	            
			      	            if(!cc2.equals("none")){
			      	            	msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc2,false));
				      	            
			      	            }
			      	            
			      	            msg.setSubject(title);
			      	            msg.setText(message, "utf-8");
			      	            msg.setSentDate(new Date());
			      	            msg.setContent(multipart);
			      	           try{
			      	        	 t.sendMessage(msg, msg.getAllRecipients());      
				      	           
			      	           }catch(Exception ex){
			      	        	   throw new SendFailedException();
			      	           }finally{
			      	        	  t.close();
			      	           }
			      	            
				      	      
			      	         
		      	    		  
			      	            
		      	    		  }catch(SendFailedException e){
		      	    			//still continue 
			      	            System.out.println("send fail exception: " +e.toString());
			      	            errors.add(ActionMessages.GLOBAL_MESSAGE,
		      	                       new ActionMessage("sendEmailStatement.error.sendEmailError",data.getCustomerCode()));
			      	            
		      	    		
				      	      }catch(MessagingException e){
				      	    	//must stop send
				      	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
			      	                       new ActionMessage("sendEmailStatement.error.sendMessageError",data.getCustomerCode()));
				      	    	System.out.println("messaging" + e.toString() );
				      	    	
				      	    	
				      	    	 saveErrors(request, new ActionMessages(errors));
					 		     return(mapping.findForward("arRepSendEmailStatement"));
				      	      }catch(Exception e){
				      	    	//must stop send
				      	    	
				      	    	
				      	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
			      	                       new ActionMessage("sendEmailStatement.error.sendMessageError",data.getCustomerCode()));
				      	    	System.out.println("messaging" + e.toString() );
				      	    	
				      	    	saveErrors(request, new ActionMessages(errors));
				      	      }
				      		 		 
		      	    		  
		      	    		  
		      	    		  
		      	    		  
		      	    	  }
		      	    	  
		      	    	  
		      	    	  
		      	      }
		      	      
		      	      if(IfNoSelected){
		      	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
	      	                       new ActionMessage("sendEmailStatement.error.noCustomerSelected"));
		      	    
		      	    	  
		      	      }else{
		      	    	System.out.println("success");
				        actionForm.setTxnStatus(Constants.STATUS_SUCCESS);    
			      	      
		      	      }
		      	      
		      	    if(!errors.isEmpty()) {
				    	
		 		       saveErrors(request, new ActionMessages(errors));
		 		       return(mapping.findForward("arRepSendEmailStatement"));
		 		       
		 		    }	
		      		 		      		 
		      		 
		      		 
		         }
		         
/******************************************************* 	   
 * 		View Button 
*******************************************************/		    	  
		      	 else if (request.getParameter("arCtmrList[" + actionForm.getRowSelected() + "].viewPDFButton") != null &&
		         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
		         		        
		      		 
		      		 
		      		    ArRepSendEmailStatementList data = (ArRepSendEmailStatementList)actionForm.getArCtmrList().get(actionForm.getRowSelected());
		      		    
		      		  File file = new File(data.getFileLocation());
		      	    	byte data_byte[];
		      		try {
		         		
		         		FileInputStream fis = new FileInputStream(file);
		         		
		         		 data_byte= new byte[fis.available()];
		         		
		         		int ctr = 0;
		         		int c = 0;
		         		
		         		while ((c = fis.read()) != -1) {
		         			
		         			data_byte[ctr] = (byte)c;
		         			ctr++;
		         			
		         		}
		         		
		         		response.setContentType("application/pdf");
		         		response.setContentLength(data_byte.length);	
		         		ServletOutputStream outputStream = response.getOutputStream();
		         		outputStream.write(data_byte, 0, data_byte.length);
		         		outputStream.flush();
		         		outputStream.close();
		         		
		         	} catch(Exception ex) {
		         		
		         		ex.printStackTrace();
		         		
		         		if(log.isInfoEnabled()) {
		         			
		         			log.info("Exception caught in AdRepApprovalListAction.execute(): " + ex.getMessage() +
		         					" session: " + session.getId());
		         			
		         		}
		         		
		         		
		         		return(mapping.findForward("cmnErrorPage"));
		         		
		         	}
		      		
		      		Report report = new Report();
		      		report.setBytes(data_byte);

				 //      session.setAttribute(Constants.REPORT_KEY, report);
				       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		         	 System.out.print(" the numbe is: " + actionForm.getRowSelected() );     	
		      		     return(mapping.findForward("arRepSendEmailStatement"));
		      		 
/******************************************************* 	   
* 		Load pdf data
*******************************************************/	
		      		     
		      		     
		         }else if (request.getParameter("loadpdf") != null &&
		                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
		        	System.out.println("loaded----------------------------");
		        	
		        	 
		         }
		    	 
		    	 actionForm.reset(mapping, request);
		    
		    		 
		    		
		    		 
		    		 System.out.println("reading----------------------");
		      	    	actionForm.clearArCtmrList();
						   
				         BufferedReader b_reader = null;
				         String cr_line;
				         b_reader = new BufferedReader(new FileReader("c:/opt/ofs-statements/" + user.getCompany() + "/email/soa_list.txt/"));
				         
				         
				         
				         
						while ((cr_line = b_reader.readLine()) != null) {
								String[] data = cr_line.split("#");
								System.out.println(data[1] + " " + data[2] + " " + data[3] + " " + data[4]);
								
								boolean IsEnable = true;
								if(data[3].toString().equals("NONE")||data[3].toString().equals("")){
									IsEnable = false;
								}	
								ArRepSendEmailStatementList arRpSndEmailStmntList = new ArRepSendEmailStatementList(actionForm,data[1],data[2],data[3],IsEnable,data[4]);
								System.out.println("file loc:" +data[3]);
							
							
							
								arRpSndEmailStmntList.setSelectedMail(IsEnable);
							
							
								actionForm.saveArCtmrList(arRpSndEmailStmntList);
							
						}
						b_reader.close();
			            
		      	 
		    	 
		    	 
	        	 
		    	 
		    	 
		    	 
		         
		    	 File file = new File("c:/opt/ofs-statements/" + user.getCompany() + "/email/send_email_info.txt");
   
				// if file doesnt exists, then create it
				if (!file.exists()) {
						
					file.createNewFile();
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					
					
					 bw.write("mail.domain.com");
					 bw.newLine();
					 bw.write("465");
					 bw.newLine();
					 bw.write("input username");
					 bw.newLine();
					 bw.write("input password");
					 bw.newLine();
					 bw.write("none");
					 bw.newLine();
					 bw.write("none");
					 bw.newLine();
					 bw.write("input subject/title");
					 bw.newLine();
					 bw.write("input message");
					 bw.close();
					
					 
					 actionForm.setEmailMessage("test");
			         actionForm.setEmailSubject("");
			         actionForm.setEmailHost("mail.domain.com");
			         actionForm.setEmailPort("465");
			         actionForm.setEmailUsername("input username");
			         actionForm.setEmailPassword("input password");
			         actionForm.setCc1("");
			         actionForm.setCc2("");
			         
			         
					 
					 
					
				}else{
					BufferedReader br = null;
			         String sCurrentLine;
			         br = new BufferedReader(new FileReader("c:/opt/ofs-statements/" + user.getCompany() + "/email/send_email_info.txt/"));
			         actionForm.setEmailHost(br.readLine());
			         actionForm.setEmailPort(br.readLine());
			         actionForm.setEmailUsername(br.readLine());
			         actionForm.setEmailPassword(br.readLine());
			         
			         String cc_string = br.readLine();
			         
			         if(cc_string.trim().equals("none")){
			        	 actionForm.setCc1("");
			         }else{
			        	 actionForm.setCc1(cc_string);
			         }
			         
			         cc_string = br.readLine();
			         
			         if(cc_string.trim().equals("none")){
			        	 actionForm.setCc2("");
			         }else{
			        	 actionForm.setCc2(cc_string);
			         }
			         
			         actionForm.setEmailSubject(br.readLine());
			         actionForm.setEmailMessage(br.readLine());
			         
			         

				}
		    	 
		    
		      	         		         
				
		       //temporary
			     return(mapping.findForward("arRepSendEmailStatement"));
		      }catch(Exception e){
/******************************************************* 	   
 * 		System Failed: Forward to error page 
*******************************************************/
		    	  System.out.print(e.toString());
		    	 
		    	      if (log.isInfoEnabled()) {

		    	             log.info("Exception caught in ArSendEmailStatement.execute(): " + e.getMessage()
		    	                + " session: " + session.getId());
		    	         }

		    	      e.printStackTrace();
		    	      return mapping.findForward("cmnErrorPage");

		    	      
		    	  
		      }
		      
		      
		      
		      
		  
		      
	 }
}
