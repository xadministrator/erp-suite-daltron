package com.struts.jreports.ar.sendemailstatement;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.salesorderentry.ArSalesOrderEntryList;
import com.struts.ar.salesorderentry.ArSalesOrderLineItemList;
import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepSendEmailStatementForm extends ActionForm implements Serializable{

	private String userPermission = new String();
	private String emailSubject = new String();
	private String emailMessage = new String();
	private ArrayList arCtmrList = new ArrayList();
	private String tableType = null;
	private String emailUsername = new String();
	private String emailPassword = new String();
	private String emailHost = new String();
	private boolean saveInfo = true;
	private String cc1 = new String();
	private String cc2 = new String();
	private String emailPort=new String();
	

	private String txnStatus = new String();
	private int rowSelected = 0;
	private String report = null;

	
	public int getRowSelected() {

	      return rowSelected;

	   }
	
	public void setRowSelected(Object selectedAdRSList ) {

	      this.rowSelected = arCtmrList.indexOf(selectedAdRSList);
	     
	}
	
	
	public void clearArCtmrList(){
		arCtmrList.clear();
	}
	

	
	public void saveArCtmrList(Object newArCtmrData){
		arCtmrList.add(newArCtmrData);
	}
	
	
	public String getEmailSubject(){
		return emailSubject;
	}
	
	public void setEmailSubject(String emailSubject){
		this.emailSubject = emailSubject;
	}
	
	
	
	
	public String getEmailMessage(){
		return emailMessage;
	}
	
	public void setEmailMessage(String emailMessage){
		this.emailMessage = emailMessage;
	}
	
	
	

	
	
	public ArrayList getArCtmrList(){
		return arCtmrList;
	}
	
	public void setArCtmrList(ArrayList arCtmrList){
		this.arCtmrList = arCtmrList;
	}
	
	
	 public String getTableType() {
	    	
	      return(tableType);
	    	
	   }
	    
	   public void setTableType(String tableType) {
	    	
	      this.tableType = tableType;
	    	
	   }     
	   
	
	
	  public String getTxnStatus() {

	      String passTxnStatus = txnStatus;
	      txnStatus = Constants.GLOBAL_BLANK;
	      return passTxnStatus;
	   }

	   public void setTxnStatus(String txnStatus) {

	      this.txnStatus = txnStatus;

	   }
	
	
	public String getUserPermission() {
		   	
	      return userPermission;
	   
	}

	public void setUserPermission(String userPermission) {
	   	
	      this.userPermission = userPermission;
	   
	}
	
	public String getReport() {
	   	
	   	  return report;
	   	
	}
	   
	public void setReport(String report) {
	   	
	   	this.report = report;
	   	
	}
	
	public String getEmailUsername(){
		return emailUsername;
	}
	
	public void setEmailUsername(String emailUsername){
		this.emailUsername = emailUsername;
	}
	
	public String getEmailPassword(){
		return emailPassword;
	}
	
	public void setEmailPassword(String emailPassword){
		this.emailPassword = emailPassword;
	}
	
	public String getEmailHost(){
		return emailHost;
	}
	
	public void setEmailHost(String emailHost){
		this.emailHost = emailHost;
	}
	
	
	public boolean getSaveInfo(){
		return saveInfo;
	}
	
	public void setSaveInfo(boolean saveInfo){
		this.saveInfo =saveInfo;
	}
	
	public String getCc1(){
		return cc1;
	}
	
	public void setCc1(String cc1){
		this.cc1 = cc1;
	}
	
	public String getCc2(){
		return cc2;
	}
	
	public void setCc2(String cc2){
		this.cc2 = cc2;
	}
	
	
	public String getEmailPort(){
		return emailPort;
	}
	
	public void setEmailPort(String emailPort){
		this.emailPort =  emailPort;
	}
	
	
	
	
	
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		   	
		      ActionErrors errors = new ActionErrors();
		      
		      if (request.getParameter("sendButton") != null) {
		    	  
		    	  
		    	  if(Common.validateRequired(emailHost)||Common.validateRequired(emailPort)) {
		             	
		    		    errors.add("host", new ActionMessage("sendEmailStatement.error.hostError"));
		    		    
		          }
		    	  
		    	  
		    	  
		    	  
		    	  if(Common.validateRequired(emailUsername) || Common.validateRequired(emailPassword)) {
		             	
		    		    errors.add("host", new ActionMessage("sendEmailStatement.error.userpassError"));
		    		    
		          }
		    	 
  	  
		    	  
		      }
		      
		      
		      
		      return errors;
		      
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){ 
	   	
       
		
		for (int i=0; i<arCtmrList.size(); i++) {
			
			ArRepSendEmailStatementList actionList = (ArRepSendEmailStatementList)arCtmrList.get(i);
			actionList.setSelectedMail(false);
			
		}

	 
   }
	
	
	
}
