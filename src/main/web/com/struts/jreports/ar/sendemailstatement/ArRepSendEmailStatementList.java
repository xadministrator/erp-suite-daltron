package com.struts.jreports.ar.sendemailstatement;

import java.io.Serializable;

public class ArRepSendEmailStatementList implements Serializable {

	private String customerCode=null;
	private String customerName=null;
	private String customerEmail=null;
	private boolean selectedMail=false;
	private String fileLocation=null;
	private String viewPDFButton = null;
	
	
	
	private ArRepSendEmailStatementForm parentBean;
	public ArRepSendEmailStatementList (){
		
	}
	public ArRepSendEmailStatementList (ArRepSendEmailStatementForm parentBean,String customerCode,String customerName, String customerEmail,boolean selectedMail, String fileLocation){
		this.parentBean = parentBean;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.selectedMail = selectedMail;
		this.fileLocation = fileLocation;	
		
		
	}
	
	
	public String getCustomerCode(){
		return customerCode;
	}
	
	public void setCustomerCode(String customerCode){
		
		this.customerCode = customerCode;
	}
	
	public String getCustomerEmail(){
		return customerEmail;
	}
	
	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}
	
	
	public String getCustomerName(){
		return customerName;
	}
	
	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}
	
	public boolean getSelectedMail(){
		return selectedMail;
	}
	
	public void setSelectedMail(boolean selectedMail){
		this.selectedMail = selectedMail;
	}
	
	public String getFileLocation(){
		return fileLocation;
	}
	
	public void setFileLocation(String fileLocation){
		this.fileLocation = fileLocation;
	}

	 public void setViewPDFButton(String viewPDFButton) {
		   	
	   	  parentBean.setRowSelected(this);
	   	  
	 }
	
}
