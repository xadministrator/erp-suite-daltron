package com.struts.jreports.ar.standardmemolinelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepStandardMemoLineListDetails;

public class ArRepStandardMemoLineListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepStandardMemoLineListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepStandardMemoLineListDetails details = (ArRepStandardMemoLineListDetails)i.next();
                  
	     ArRepStandardMemoLineListData argData = new ArRepStandardMemoLineListData(details.getSllSmlName(), 
	     		details.getSllSmlDescription(),details.getSllSmlType(), new Double(details.getSllSmlUnitPrice()), 
				details.getSllCoaGlAccountNumber(), details.getSllCoaGlAccountDescription(),
				details.getSllTaxable() == 1 ? "YES" : "NO", details.getSllEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("standardMemoLineName".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getStandardMemoLineName();
      }else if("description".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getDescription();
      }else if("type".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getType();
      }else if("unitPrice".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getUnitPrice();
      }else if("accountNumber".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getAccountNumber();         
      }else if("accountDescription".equals(fieldName)){
         value = ((ArRepStandardMemoLineListData)data.get(index)).getAccountDescription(); 
      }else if("taxable".equals(fieldName)){
        value = ((ArRepStandardMemoLineListData)data.get(index)).getTaxable();   
      }else if("enable".equals(fieldName)){
        value = ((ArRepStandardMemoLineListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
