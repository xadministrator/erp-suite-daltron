package com.struts.jreports.ar.standardmemolinelist;


public class ArRepStandardMemoLineListData implements java.io.Serializable {
	
   private String standardMemoLineName = null;
   private String description = null;
   private String type = null;
   private Double unitPrice = null;
   private String accountNumber = null;
   private String accountDescription = null;
   private String taxable = null;
   private String enable = null;

   public ArRepStandardMemoLineListData(String standardMemoLineName, String description, String type, Double unitPrice,
   		String accountNumber, String accountDescription, String taxable, String enable){
      	
      this.standardMemoLineName = standardMemoLineName;
      this.description = description;
      this.type = type;
      this.unitPrice = unitPrice;
      this.accountNumber = accountNumber;
      this.accountDescription = accountDescription;
      this.taxable = taxable;
      this.enable = enable;
      
   }

   public String getAccountDescription() {
   	
     return accountDescription;
   
   }
   
   public void setAccountDescription(String accountDescription) {
   
     this.accountDescription = accountDescription;
   
   }
   
   public String getAccountNumber() {
   
   	 return accountNumber;
   
   }
   
   public void setAccountNumber(String accountNumber) {
   
   	 this.accountNumber = accountNumber;
   
   }

   public String getTaxable() {
    
    	 return taxable;
    
   }
    
   public void setTaxable(String taxable) {
    
    	 this.taxable = taxable;
    
   }
   
   public String getEnable() {
   
   	 return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	 this.enable = enable;
   
   }
   
   public String getDescription() {
   
   	 return description;
   
   }
   
   public void setDescription(String description) {
   
   	 this.description = description;
   
   }
  
   public String getStandardMemoLineName() {
   
   	 return standardMemoLineName;
   
   }
   
   public void setStandardMemoLineName(String standardMemoLineName) {
   
   	 this.standardMemoLineName = standardMemoLineName;
   
   }
   
   public Double getUnitPrice() {
   
   	 return unitPrice;
   
   }
   
   public void setUnitPrice(Double unitPrice) {
   
     this.unitPrice = unitPrice;
   
   }
   
   public String getType() {
   
   	 return type;
   
   }
   
   public void setType(String type) {
   
   	 this.type = type;
   
   }
   
}
