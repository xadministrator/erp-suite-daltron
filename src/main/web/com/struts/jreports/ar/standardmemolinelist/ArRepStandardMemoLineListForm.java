package com.struts.jreports.ar.standardmemolinelist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class ArRepStandardMemoLineListForm extends ActionForm implements Serializable{

   private String standardMemoLineName = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;

   private HashMap criteria = new HashMap();
   private ArrayList arRepBrSmlList = new ArrayList();

   private String userPermission = new String();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getStandardMemoLineName() {
   
   	   return standardMemoLineName;
   
   }

   public void setStandardMemoLineName(String standardMemoLineName) {
   	
      this.standardMemoLineName = standardMemoLineName;
   
   }

   public void setGoButton(String goButton) {
   	
      this.goButton = goButton;
   
   }

   public void setCloseButton(String closeButton) {
   	
      this.closeButton = closeButton;
   
   }

   public String getType() {
   	
      return type;
   
   }

   public void setType(String type) {
   	
      this.type = type;
   
   }

   public ArrayList getTypeList() {
   	
      return typeList;
   
   }

   public String getOrderBy() {
   	
   	  return orderBy;   	
   
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   
   }
   
   public String getViewType() {
   	
   	  return viewType;   	
   
   }
   
   public void setViewType(String viewType) {
   	
   	  this.viewType = viewType;
   
   }
   
   public ArrayList getViewTypeList() {
   	
   	  return viewTypeList;
   
   }

   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission() {
   	
      return userPermission;
   
   }

   public void setUserPermission(String userPermission) {
   	
      this.userPermission = userPermission;
   
   }
   
   public Object[] getArRepBrSmlList(){
   	
   	  return arRepBrSmlList.toArray();
   	
   }
   
   public ArRepBranchStandardMemoLineList getArRepBrSmlListByIndex(int index){
   	
   	  return ((ArRepBranchStandardMemoLineList)arRepBrSmlList.get(index));
   	
   }
   
   public int getArRepBrSmlListSize(){
   	
   	  return(arRepBrSmlList.size());
   	
   }
   
   public void saveArRepBrSmlList(Object newArRepBrSmlList){
   	
   	  arRepBrSmlList.add(newArRepBrSmlList);   	  
   	
   }
   
   public void clearArRepBrSmlList(){
   	
   	  arRepBrSmlList.clear();
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {  
   	
   	  for (int i=0; i<arRepBrSmlList.size(); i++) {
	  	
   	  	  ArRepBranchStandardMemoLineList actionList = (ArRepBranchStandardMemoLineList)arRepBrSmlList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
   	
      goButton = null;
      closeButton = null;
      standardMemoLineName = null;
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK); 
      typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_LINE);
      typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_TAX);
      typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_FREIGHT);
      type = Constants.GLOBAL_BLANK; 
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      
      if (orderByList.isEmpty()) {
      	
		  orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("MEMO LINE NAME");
	      orderByList.add("MEMO LINE TYPE");
	      orderBy = "MEMO LINE NAME";   
	  
	  }    
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
      }
      return(errors);
   }
}
