package com.struts.jreports.ar.statement;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.Base64;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdStoredProcedureSetupController;
import com.ejb.txn.AdStoredProcedureSetupControllerHome;
import com.ejb.txn.ArRepStatementController;
import com.ejb.txn.ArRepStatementControllerHome;
import com.ejb.txn.GlRepDetailTrialBalanceController;
import com.ejb.txn.GlRepDetailTrialBalanceControllerHome;
import com.struts.jreports.ar.aging.ArRepAgingDS;
import com.struts.jreports.ar.sales.ArRepSalesSubDS;
import com.struts.jreports.ar.standardmemolinelist.ArRepStandardMemoLineListDS;
import com.struts.jreports.gl.generalledger.GlRepGeneralLedgerDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.AdStoredProcedureDetails;
import com.util.ArRepStatementDetails;
import com.util.GlModAccountingCalendarValueDetails;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.struts.Globals;

public final class ArRepStatementAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

   	  HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArRepStatementAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ArRepStatementForm actionForm = (ArRepStatementForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.AR_REP_STATEMENT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arRepStatement");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

         ActionMessages messages = new ActionMessages();
/*******************************************************
   Initialize ArRepStatementController EJB
*******************************************************/

         ArRepStatementControllerHome homeSMT = null;
         ArRepStatementController ejbSMT = null;
        AdStoredProcedureSetupControllerHome homeSP = null;


         GlRepDetailTrialBalanceControllerHome homeDTB = null;
         GlRepDetailTrialBalanceController ejbDTB = null;
         AdStoredProcedureSetupController ejbSP = null;


         try {

            homeSMT = (ArRepStatementControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepStatementControllerEJB", ArRepStatementControllerHome.class);

            homeDTB = (GlRepDetailTrialBalanceControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlRepDetailTrialBalanceControllerEJB", GlRepDetailTrialBalanceControllerHome.class);

            homeSP = (AdStoredProcedureSetupControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdStoredProcedureSetupControllerEJB", AdStoredProcedureSetupControllerHome.class);


         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in ArRepStatementAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbDTB = homeDTB.create();

            ejbSMT = homeSMT.create();

            ejbSP = homeSP.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in ArRepStatementAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/******************************************************
   Call ArFindReceiptController EJB
   getAdPrfArUseCustomerPulldown
*******************************************************/

	 boolean useCustomerPulldown = true;

	 try {

	 	useCustomerPulldown = Common.convertByteToBoolean(ejbSMT.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
	 	actionForm.setUseCustomerPulldown(useCustomerPulldown);

	 } catch (EJBException ex) {

        if (log.isInfoEnabled()) {

           log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
           " session: " + session.getId());

        }

        return(mapping.findForward("cmnErrorPage"));
     }

/*******************************************************
   -- AR SMT Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArrayList list = new ArrayList();
            ArrayList list2 = new ArrayList();
            ArrayList list3 = new ArrayList();
            ArRepStatementDetails details = null;
            AdCompanyDetails adCmpDetails = null;
            AdBranchDetails adBranchDetails = null;
            AdStoredProcedureDetails adStoredProcedureDetails = null;
            boolean noCollections = true;
            boolean noCreditMemoes = true;
            String storedProcedureName = null;

            boolean enableStoredProcedure = false;

            int agingBucket = 0;


            String customerCode = "";
            boolean includeUnpostedTransaction = false;
            boolean includeAdvance = false;
            boolean includeAdvanceOnly = false;
            boolean showUnearnedInterestTransactionOnly = false;
            boolean includeCreditMemo = false;

		    try {

		       // get company

		       adCmpDetails = ejbSMT.getAdCompany(user.getCmpCode());
		        adStoredProcedureDetails = ejbSP.getAdSp(user.getCmpCode());


		       // get preference
		       AdPreferenceDetails adPreferenceDetails = ejbSMT.getAdPreference(user.getCmpCode());
		       agingBucket = adPreferenceDetails.getPrfArAgingBucket();

               storedProcedureName = adStoredProcedureDetails.getSpNameArStatementOfAccountReport();

		       enableStoredProcedure = Common.convertByteToBoolean(adStoredProcedureDetails.getSpEnableArStatementOfAccountReport()) ;

		       // execute report

		       HashMap criteria = new HashMap();

		       System.out.println("go code in: " + actionForm.getCustomerCode());

		       /*
                if (!Common.validateRequired(actionForm.getCustomerCode())) {

	        		criteria.put("customerCode", actionForm.getCustomerCode());

	           }
		        */

		       customerCode = actionForm.getCustomerCode();


	           if (!Common.validateRequired(actionForm.getCustomerType())) {

	        		criteria.put("customerType", actionForm.getCustomerType());

	           }

	           if (!Common.validateRequired(actionForm.getCustomerClass())) {

	        		criteria.put("customerClass", actionForm.getCustomerClass());

	           }

	           if (!Common.validateRequired(actionForm.getDateFrom())) {

	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

	           }

	           if (!Common.validateRequired(actionForm.getDateTo())) {

	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

	           }

	           if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {

	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());

	           }

	           if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {

	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());

	           }
	           if (!Common.validateRequired(actionForm.getInvoiceBatchName())) {

	        		criteria.put("invoiceBatchName", actionForm.getInvoiceBatchName());

	           }


	           /*
	           if (actionForm.getIncludeAdvance()) {

			    	criteria.put("includeAdvance", "YES");

			   } else {

			    	criteria.put("includeAdvance", "NO");

			   }


	           if (actionForm.getIncludeAdvanceOnly()) {

			    	criteria.put("includeAdvanceOnly", "YES");

			   } else {

			    	criteria.put("includeAdvanceOnly", "NO");

			   }


	           if (actionForm.getIncludeUnpostedTransaction()) {

			    	criteria.put("includeUnpostedTransaction", "YES");

			   } else {

			    	criteria.put("includeUnpostedTransaction", "NO");

			   }
			   */


	           if (actionForm.getIncludeAdvance()) {

	        	   includeAdvance = true;

			   } else {

				   includeAdvance = false;

			   }


	           if (actionForm.getIncludeAdvanceOnly()) {

	        	   includeAdvanceOnly = true;

			   } else {

				   includeAdvanceOnly = false;

			   }


	           if (actionForm.getIncludeUnpostedTransaction()) {

	        	   includeUnpostedTransaction = true;

			   } else {

				   includeUnpostedTransaction = false;

			   }


	           if (actionForm.getIncludeCreditMemo()) {

	        	   includeCreditMemo = true;

			   } else {

				   includeCreditMemo = false;

			   }





	           if(actionForm.getShowUnearnedInterestTransactionOnly()) {
	        	   showUnearnedInterestTransactionOnly = true;
			    } else {
			    	showUnearnedInterestTransactionOnly = false;
			    }




	           ArrayList branchList = new ArrayList();
                   ArrayList branchList_array = new ArrayList();

	           for(int i=0; i<actionForm.getArRepBrStListSize(); i++) {

                        ArRepBranchStatementList brStList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(i);

                        if(brStList.getBranchCheckbox() == true) {

                            AdBranchDetails brDetails = new AdBranchDetails();
                            brDetails.setBrCode(brStList.getBranchCode());

                            branchList.add(brDetails);
                            branchList_array.add(brStList.getBrBranchCode());
                        }

	           }

	        // get selected customer batch list
           	ArrayList customerBatchSelectedList = new ArrayList();

       		for (int i=0; i < actionForm.getCustomerBatchSelectedList().length; i++) {

       			customerBatchSelectedList.add(actionForm.getCustomerBatchSelectedList()[i]);

       		}

       		ArrayList customerDepartmentSelectedList = new ArrayList();

       		for (int i=0; i < actionForm.getCustomerDepartmentSelectedList().length; i++) {

       			customerBatchSelectedList.add(actionForm.getCustomerDepartmentSelectedList()[i]);

       		}

	        //   System.out.println("show all="+ actionForm.getShowAll() + " include zero=" +actionForm.getIncludeZero() + "include unposted = "+ actionForm.getIncludeUnpostedInvoice() + "include tansaction = " +  actionForm.getSortByTransaction() + " water =" + actionForm.getSortByItem() + " order by = " + actionForm.getOrderBy() + " branchlist= " + branchList.size() + " cmpny=" + user.getCmpCode());
                DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
                Connection conn = null;
                CallableStatement stmt = null;

                ResultSet rs = null;


                if(enableStoredProcedure){

                    try{
                        conn = dataSource.getConnection();
                        stmt = (CallableStatement) conn.prepareCall(storedProcedureName);

                        Date dateFrom = null;
                        Date dateTo =null;
                        if (!Common.validateRequired(actionForm.getDateFrom())) {

                            dateFrom = Common.convertStringToSQLDate(actionForm.getDateFrom());

                        }

                        if (!Common.validateRequired(actionForm.getDateTo())) {

                           dateTo = Common.convertStringToSQLDate(actionForm.getDateTo());

                        }
                        System.out.println("BRANCH LIST ARRAY: " + branchList_array.size() );
                        System.out.println("customer Batch LIST ARRAY: " + customerBatchSelectedList.size());
                        System.out.println("custoemr department ARRAY: " + customerDepartmentSelectedList.size());

                        stmt.setString(1, Common.storedProcedureAlignINvalue(branchList_array));
                        stmt.setString(2, Common.storedProcedureAlignINvalue(customerBatchSelectedList));
                        stmt.setString(3, Common.storedProcedureAlignINvalue(customerDepartmentSelectedList));


                        if (!Common.validateRequired(actionForm.getCustomerCode())) {
                            System.out.println("CUSTOMER CODE: " + actionForm.getCustomerCode());
                            stmt.setString(4,actionForm.getCustomerCode());


                        }else{
                            stmt.setString(4,"");


                        }

                        if (!Common.validateRequired(actionForm.getCustomerType())) {
                            System.out.println("CUSTOMER TYPE: " + actionForm.getCustomerType());
                            stmt.setString(5,actionForm.getCustomerType());

                        }else{
                            stmt.setString(5,"");

                        }

                        if (!Common.validateRequired(actionForm.getCustomerClass())) {
                            System.out.println("CUSTOMER CLASS: " + actionForm.getCustomerClass());
                            stmt.setString(6,actionForm.getCustomerClass());


                        }else{
                            stmt.setString(6,"");

                        }


                        if (!Common.validateRequired(actionForm.getDateFrom())) {

                            stmt.setDate(7, new java.sql.Date(dateFrom.getTime()));


                        }else{

                            stmt.setNull(7, java.sql.Types.DATE);

                        }

                        if (!Common.validateRequired(actionForm.getDateTo())) {

                            stmt.setDate(8, new java.sql.Date(dateTo.getTime()));


                        }else{

                            stmt.setNull(8, java.sql.Types.DATE);

                        }

                        if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {

                            System.out.println("INVOICE FROM: " + actionForm.getInvoiceNumberFrom());
                            stmt.setString(9,actionForm.getInvoiceNumberFrom());

                        }else{
                            stmt.setString(9,"");
                        }

                        if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
                             System.out.println("INVOICE TO: " + actionForm.getInvoiceNumberTo());
                            stmt.setString(10,actionForm.getInvoiceNumberTo());

                        }else{
                            stmt.setString(10,"");
                        }

                        if (!Common.validateRequired(actionForm.getInvoiceBatchName())) {
                             System.out.println("INVOICE BATCH NAME: " + actionForm.getInvoiceBatchName());
                             stmt.setString(11,actionForm.getInvoiceBatchName());

                        }else{
                             stmt.setString(11,"");
                        }

                        if(actionForm.getIncludeAdvance()) {
                            System.out.println("unearned interest: " + actionForm.getIncludeAdvance());
                            stmt.setBoolean(12,true);
                        } else {
                            stmt.setBoolean(12,false);
                        }

                        if(actionForm.getIncludeAdvanceOnly()) {
                            System.out.println("unearned interest: " + actionForm.getIncludeAdvanceOnly());
                            stmt.setBoolean(13,true);
                        } else {
                            stmt.setBoolean(13,false);
                        }

                         if(actionForm.getIncludeCollections()) {
                            System.out.println("unearned interest: " + actionForm.getIncludeCollections());
                            stmt.setBoolean(14,true);
                        } else {
                            stmt.setBoolean(14,false);
                        }

                        if(actionForm.getShowUnearnedInterestTransactionOnly()) {
                            System.out.println("unearned interest: " + actionForm.getShowUnearnedInterestTransactionOnly());
                            stmt.setBoolean(15,true);
                        } else {
                            stmt.setBoolean(15,false);
                        }

                        if (actionForm.getIncludeUnpostedTransaction()) {
                             System.out.println("unposted: " + actionForm.getIncludeUnpostedTransaction());

                            stmt.setBoolean(16,true);


                        } else {

                            stmt.setBoolean(16,false);

                        }

                        if (actionForm.getShowAll()) {
                             System.out.println("show all: " + actionForm.getShowAll());

                            stmt.setBoolean(17,true);


                        } else {

                            stmt.setBoolean(17,false);

                        }

                        if (actionForm.getIncludeZero()) {
                            System.out.println("include zero: " + actionForm.getIncludeZero());

                            stmt.setBoolean(18,true);


                        } else {

                            stmt.setBoolean(18,false);

                        }

                        System.out.println("company: " + user.getCmpCode());

                        stmt.setInt(19, user.getCmpCode());


                        rs = stmt.executeQuery();


                        list = ejbSMT.executeSpArRepStatement(
                                rs,
                                dateTo,
                                actionForm.getMemoLine(),
                                actionForm.getShowAll(),
                                actionForm.getIncludeZero(),
                                actionForm.getIncludeCollections(),
                                actionForm.getOrderBy(),
                                actionForm.getGroupBy(),
                                actionForm.getReportType(),
                                user.getCmpCode());

                    }catch(SQLException qx){
                        System.out.println(qx.toString());

                    }catch(GlobalNoRecordFoundException ex){
                        throw ex;
                    }catch(Exception ex){
                        System.out.println("ERROR: " + ex.toString());
                    }finally{
                        try {
                            if(rs != null) rs.close();
                        }catch(SQLException f){
                            System.out.println("close rs error: " + f.toString());
                        }

                        rs = null;
                        try {
                            if(stmt != null) stmt.close();
                        }catch(SQLException f){
                            System.out.println("close stmt error: " + f.toString());
                        }
                        stmt = null;
                        try{ if(conn != null) conn.close(); }catch(SQLException f){ System.out.println("close conn error: " + f.toString());}
                        conn = null;
                    }

                } else if(actionForm.getReportType().equals("AGING")) {


                	try{
                        conn = dataSource.getConnection();
                        stmt = (CallableStatement) conn.prepareCall("{ call sp_ArRepAging(?,?,?) }");


                        Date dateTo =null;

                        if (!Common.validateRequired(actionForm.getDateTo())) {

                           dateTo = Common.convertStringToSQLDate(actionForm.getDateTo());

                        }

                        System.out.println("customer Batch LIST ARRAY: " + customerBatchSelectedList.size());

                        stmt.setString(1, Common.storedProcedureAlignINvalue(customerBatchSelectedList));


                        if (!Common.validateRequired(actionForm.getDateTo())) {

                            stmt.setDate(2, new java.sql.Date(dateTo.getTime()));

                        }else{

                            stmt.setNull(2, java.sql.Types.DATE);

                        }


                        if (!Common.validateRequired(actionForm.getCustomerCode())) {
                            System.out.println("CUSTOMER CODE: " + actionForm.getCustomerCode());
                            stmt.setString(3,actionForm.getCustomerCode());

                        }else{

                            stmt.setString(3,"");

                        }



                        rs = stmt.executeQuery();


                        list = ejbSMT.executeSpArRepAging(
                                rs,
                                user.getCmpCode());

                    }catch(SQLException qx){
                        System.out.println(qx.toString());

                    }catch(GlobalNoRecordFoundException ex){
                        throw ex;
                    }catch(Exception ex){
                        System.out.println("ERROR: " + ex.toString());
                    }finally{
                        try {
                            if(rs != null) rs.close();
                        }catch(SQLException f){
                            System.out.println("close rs error: " + f.toString());
                        }

                        rs = null;
                        try {
                            if(stmt != null) stmt.close();
                        }catch(SQLException f){
                            System.out.println("close stmt error: " + f.toString());
                        }
                        stmt = null;
                        try{ if(conn != null) conn.close(); }catch(SQLException f){ System.out.println("close conn error: " + f.toString());}
                        conn = null;
                    }




                }else{



       //             list = ejbSMT.executeArRepStatement2(criteria,actionForm.getMemoLine(), customerCode, includeUnpostedTransaction, includeAdvance, showUnearnedInterestTransactionOnly, includeAdvanceOnly,actionForm.getShowAll(), actionForm.getIncludeZero(), actionForm.getIncludeCollections(), actionForm.getIncludeUnpostedInvoice(), actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(),customerBatchSelectedList, customerDepartmentSelectedList , branchList, user.getCmpCode());
                     // list = ejbSMT.executeArRepStatement2(criteria,actionForm.getMemoLine(), actionForm.getShowAll(), actionForm.getIncludeZero(), actionForm.getIncludeCollections(), actionForm.getIncludeUnpostedInvoice(), actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(),customerBatchSelectedList, customerDepartmentSelectedList , branchList, user.getCmpCode());
                      list = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), customerCode, includeUnpostedTransaction, showUnearnedInterestTransactionOnly, includeAdvance, includeAdvanceOnly,actionForm.getShowAll(), actionForm.getIncludeZero(), actionForm.getIncludeCollections(), actionForm.getIncludeUnpostedInvoice(), actionForm.getIncludeCreditMemo(), actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(),customerBatchSelectedList, customerDepartmentSelectedList , branchList, user.getCmpCode());

                }

                System.out.println("LIST SIZE IS : " + list.size());

	          /*
	           if(actionForm.getIncludeCollections()) {

	        	   list2 = ejbSMT.executeArRepStatementReceiptSub(criteria , actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(), branchList, user.getCmpCode());

	        	   if(list2.size() > 0) noCollections = false;

	        	   list3 = ejbSMT.executeArRepStatementCreditMemoSub(criteria, actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(), branchList, user.getCmpCode());

	        	   if(list3.size() > 0) noCreditMemoes = false;

	           }*/

		    } catch (GlobalNoRecordFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("statement.error.noRecordFound"));

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("arRepStatement"));

		    }

		    // fill report parameters, fill report to pdf and set report session

		    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

		    Map parameters = new HashMap();

		    parameters.put("company", adCmpDetails.getCmpName());
		    parameters.put("branchName", user.getCurrentBranch().getBrName());
		    parameters.put("date", actionForm.getDateTo());
		    parameters.put("date2", Common.convertStringToSQLDate(actionForm.getDateTo()));
		    Calendar calendar = new GregorianCalendar();
		    calendar.setTime(Common.convertStringToSQLDate(actionForm.getDateTo()));
		    calendar.add(Calendar.DAY_OF_MONTH, 10);
		    parameters.put("dueDate", Common.convertSQLDateToString(calendar.getTime()));
		    parameters.put("customerMessage", actionForm.getCustomerMessage());
		    parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());
		    parameters.put("faxNumber", adCmpDetails.getCmpFax());
		    parameters.put("email", adCmpDetails.getCmpEmail());
		    parameters.put("tinNumber", adCmpDetails.getCmpTin());
		    parameters.put("collectionHead", actionForm.getCollectionHead());
		    parameters.put("companyAddress", companyAddress);
		    parameters.put("invoiceBatchName", actionForm.getInvoiceBatchName());
		    parameters.put("waterOnly", actionForm.getWaterOnly());

		    parameters.put("viewType", actionForm.getViewType());

		    Date firstDayOfPeriod = null;
		    Calendar dtCal = new GregorianCalendar();
		    dtCal.setTime(Common.convertStringToSQLDate(actionForm.getDateTo()));

			 //dtCal.add(Calendar.MONTH, -1);
			 //dtCal.add(Calendar.DAY_OF_MONTH, 1);
			 dtCal.set(Calendar.DATE,1);

			 firstDayOfPeriod = dtCal.getTime();

		  	System.out.println("firstDayOfPeriod="+firstDayOfPeriod);
		    parameters.put("firstDayOfPeriod", Common.convertSQLDateToString(firstDayOfPeriod));



		    if (agingBucket > 1) {

		    	   parameters.put("agingBucket1", "1-" + agingBucket + " days");
				   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
				   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
				   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
				   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");

		       } else {

				   parameters.put("agingBucket1", "1 day");
				   parameters.put("agingBucket2", "2 days");
				   parameters.put("agingBucket3", "3 days");
				   parameters.put("agingBucket4", "4 days");
				   parameters.put("agingBucket5", "Over 4 days");

			   }

		    if(actionForm.getIncludeCollections()) {
		    	parameters.put("includeCollections", "YES");
		    } else {
		    	parameters.put("includeCollections", "NO");
		    }

		    if(actionForm.getIncludeCollections() && noCollections) {
		    	parameters.put("noCollections", "YES");
		    } else {
		    	parameters.put("noCollections", "NO");
		    }

		    if(actionForm.getShowEntries()) {
		    	parameters.put("showEntries", "YES");
		    } else {
		    	parameters.put("showEntries", "NO");
		    }

		    if(actionForm.getShowUnearnedInterestTransactionOnly()) {
		    	parameters.put("showUnearnedInterestTransactionOnly", "YES");
		    } else {
		    	parameters.put("showUnearnedInterestTransactionOnly", "NO");
		    }

		    if(actionForm.getIncludeUnpostedTransaction()) {
		    	parameters.put("includeUnpostedTransaction", "YES");
		    } else {
		    	parameters.put("includeUnpostedTransaction", "NO");
		    }
		    
		    if(actionForm.getIncludeCreditMemo()) {
		    	parameters.put("includeCreditMemo", "YES");
		    } else {
		    	parameters.put("includeCreditMemo", "NO");
		    }

		    if(actionForm.getIncludeZero()) {
		    	parameters.put("includeZero", "YES");
		    } else {
		    	parameters.put("includeZero", "NO");
		    }

		    if(actionForm.getIncludeAdvance()) {
		    	parameters.put("includeAdvance", "YES");
		    } else {
		    	parameters.put("includeAdvance", "NO");
		    }

		    if(actionForm.getIncludeCollections() && noCreditMemoes) {
		    	parameters.put("noCreditMemoes", "YES");
		    } else {
		    	parameters.put("noCreditMemoes", "NO");
		    }

		    String branchMap = null;
		    boolean first = true;
		    for(int j=0; j < actionForm.getArRepBrStListSize(); j++) {

		    	ArRepBranchStatementList brList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(j);

		    	if(brList.getBranchCheckbox() == true) {
		    		if(first) {
		    			branchMap = brList.getBranchName();
		    			first = false;
		    		} else {
		    			branchMap = branchMap + ", " + brList.getBranchName();
		    		}
		    	}

		    }
		    parameters.put("branchMap", branchMap);

		    // collections

		   /* String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementReceiptSub.jasper";

		    if (!new java.io.File(subreportFilename).exists()) {

		    	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementReceiptSub.jasper");

		    }

		    JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

		    parameters.put("arRepStatementReceiptSub", subreport);
            parameters.put("arRepStatementReceiptSubDS", new ArRepStatementSubDS(list2));

            subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCreditMemoSub.jasper";

            if (!new java.io.File(subreportFilename).exists()) {

            	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementCreditMemoSub.jasper");

            }

            subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

            parameters.put("arRepStatementCreditMemoSub", subreport);
            parameters.put("arRepStatementCreditMemoSubDS", new ArRepStatementSubDS(list3));*/

		    String filename = null;


		    if (actionForm.getReportType().equals("AGING")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementAging.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementAging.jasper");

		        }


			} else if (actionForm.getReportType().equals("DISCONNECTION NOTICE")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementDisconnection.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementDisconnection.jasper");

		        }

		    } else if (actionForm.getReportType().equals("CUSTOMER LEDGER")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCustomerLedger.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementCustomerLedger.jasper");

		        }
		    } else if (actionForm.getReportType().equals("AGING SUMMARY")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementAgingSummary.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementAgingSummary.jasper");

		        }

		    } else if (actionForm.getReportType().equals("UNEARNED INTEREST")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementUnearnedInterest.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementUnearnedInterest.jasper");

		        }



		    } else if (actionForm.getReportType().equals("SUBSIDIARY LEDGER")) {

		    	if(actionForm.getMemoLine().contains("Water")){
		    	    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementSubsidiaryLedgerWTR.jasper";

				    if (!new java.io.File(filename).exists()) {

			        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementSubsidiaryLedgerWTR.jasper");

			        }



		    	}else{
		    	    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementSubsidiaryLedgerASD.jasper";

				    if (!new java.io.File(filename).exists()) {

			        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementSubsidiaryLedgerASD.jasper");

			        }

		    	}



		    } else if (actionForm.getReportType().equals("ACCOUNT SUMMARY")) {

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementAccountSummary.jasper";

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatementAccountSummary.jasper");

		        }



		    } else {

		    	String jasperFileName = Common.getLookUpJasperFile("STATEMENT_OF_ACCOUNT_LOOKUP", actionForm.getReportType(), user.getCompany());

		    	if(jasperFileName.equals("")) {
		    		jasperFileName = "ArRepStatement.jasper";
		    	}

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/" + jasperFileName;
			    System.out.println("jasper path is: " + filename);

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/ArRepStatement.jasper");

		        }


		    }

		    try {
		  


		    	Report report = new Report();


		    	Collections.sort(list,  new GroupBySorter(new sortSoaTransactionType(), new sortSoaDate()  ,new sortSoaCustomerCode() ));


		    	if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		    		report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);

			        report.setBytes(
					          JasperRunManager.runReportToPdf(filename, parameters,
						        new ArRepStatementDS(list)));



		    	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){


			        report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		               report.setBytes(
						   JasperRunManagerExt.runReportToXls(filename, parameters,
						        new ArRepStatementDS(list)));


		    	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){


			        report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);

					   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
					       new ArRepStatementDS(list)));


		    	}


		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }



/*******************************************************
	 -- AR SMT Export Invoice Action --
*******************************************************/

	}else if (request.getParameter("exportInvoice") != null) {

	/*
		ArrayList exportDataList = new ArrayList();


		ArrayList csvCustomerList = new ArrayList();
    	String monthStr = "";
    	int listAdded = 0;
    	 ArrayList customerList = new ArrayList();
	 		AdCompanyDetails adCmpDetails = null;
	 		if(actionForm.getCustomerCodeMail().equals("")){
	 			 customerList =  ejbSMT.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	 		}else{

	 			customerList.add(actionForm.getCustomerCodeMail());
	 		}

	 	//ArrayList customerList = new ArrayList();
	 		 int agingBucket = 0;
	 		//check if customer exist
            try{
	            if(actionForm.getCustomerCodeList().size()<=0){
	            	throw new GlobalNoRecordFoundException();
	            }
            } catch (GlobalNoRecordFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("statement.error.noCustomerListFound"));

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());

				}
		         System.out.println(ex.toString());
				return(mapping.findForward("cmnErrorPage"));

		    }



    		String pathFolder = "c:/opt/ofs-statements";

	        //create ofs-statement dir if not exist
	        File dirLoc = new File(Constants.OFS_STATEMENT_PATH);

	        if (!dirLoc.exists()) {
	            if (dirLoc.mkdir()) {
	                System.out.println("Directory ofs-statements is created!");
	            } else {
	                System.out.println("Failed to create directory!");
	            }
			}



            if (!dirLoc.exists()) {
		    	 if (dirLoc.mkdir()) {
                     System.out.println("Directory export is created!");
                     for(int x=0;x<customerList.size();x++){

                    	 String cstNameFldr = customerList.get(x).toString();
                    	 System.out.println("customer: " + cstNameFldr);

                    	 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);
	                     dirLoc_child.mkdir();
                     }
                 } else {
                     System.out.println("Failed to create directory!");
                 }
		    }else{

		    	 for(int x=0;x<customerList.size();x++){
		    		 String cstNameFldr = customerList.get(x).toString();
		    		 System.out.println("customer: " + cstNameFldr);
		    		 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);

		    		 if (!dirLoc_child.exists()) {
		    			 dirLoc_child.mkdir();
		    		 }else{
		    			 System.out.println("Failed to create foder: export/" + cstNameFldr);
		    		 }

		    	 }
		    }


            try{
                PrintWriter writer = new PrintWriter(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/subject_string.txt", "UTF-8");
                writer.println(actionForm.getEmailSubject());

                writer.close();
            } catch (IOException e) {
            	System.out.println("Error writing subject");
            }



           try{



        	   SimpleDateFormat frm_dt = new SimpleDateFormat("MMM-yyyy");
        	   SimpleDateFormat mnt_dt = new SimpleDateFormat("MMMM");


        	   Calendar cal = Calendar.getInstance();

        	   cal.setTime(frm_dt.parse(actionForm.getPeriodMail()));
        	   cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));
        	   Date input_date = cal.getTime();
        	   monthStr = mnt_dt.format(input_date);
        	   for(Integer x=0;x<customerList.size();x++){

        		   String customer_code_data = customerList.get(x).toString();

        		   ArrayList wateronly_list = new ArrayList();
        		   ArrayList soa_list = new ArrayList();
        		   ArrayList ledger = new ArrayList();


			    	 // get company

				       adCmpDetails = ejbSMT.getAdCompany(user.getCmpCode());


				       // get preference
				       AdPreferenceDetails adPreferenceDetails = ejbSMT.getAdPreference(user.getCmpCode());
				       agingBucket = adPreferenceDetails.getPrfArAgingBucket();


			    	System.out.println("enter customer loop  " + customer_code_data);
			    	 HashMap criteria = new HashMap();


			    	 if (!Common.validateRequired(customer_code_data)) {

			        		criteria.put("customerCode", customer_code_data);

			        }
		           if (!Common.validateRequired(actionForm.getPeriodMail())) {

		        		criteria.put("dateTo",input_date);

		           }

				   criteria.put("includeAdvance", "YES");

				   criteria.put("includeUnpostedTransaction", "YES");


		          System.out.println("date -----------" +new Date() );

		           System.out.print("flag 1");
		           ArrayList branchList = new ArrayList();

		           for(int i=0; i<actionForm.getArRepBrStListSize(); i++) {

		           		ArRepBranchStatementList brStList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(i);

		           	//	if(brStList.getBranchCheckbox() == true) {

		           			AdBranchDetails brDetails = new AdBranchDetails();
		    	           	brDetails.setBrCode(brStList.getBranchCode());

		           			branchList.add(brDetails);
		           	//	}

		           }

		           System.out.println("show all="+ actionForm.getShowAll() + " include zero=" +actionForm.getIncludeZero() + "include unposted = "+ actionForm.getIncludeUnpostedInvoice() + "include tansaction = " +  actionForm.getSortByTransaction() + " water =" + actionForm.getSortByItem() + " order by = " + actionForm.getOrderBy() + " branchlist= " + branchList.size() + " cmpny=" + user.getCmpCode());



		           try{

		        	   ArrayList customerBatchSelectedList = new ArrayList();

		        	   soa_list = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), false, true, true,"", "", "",customerBatchSelectedList, branchList,  user.getCmpCode());
		        	   ledger = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), false, true, true,"", "", "CUSTOMER LEDGER",customerBatchSelectedList, branchList,  user.getCmpCode());


		        	   if(ledger.size()<=0){
		        		   continue;
		        	   }




		           }catch(GlobalNoRecordFoundException gx){
		        	   continue;
		           }



		           for(int i=0; i<soa_list.size(); i++) {

		           		ArRepStatementDetails stateDetails = (ArRepStatementDetails)soa_list.get(i);

		           		System.out.println(stateDetails.getSmtReceiptNumber());

		           }


		           	ArRepStatementDetails SOAdetails = new ArRepStatementDetails();

		           SOAdetails = (ArRepStatementDetails)soa_list.get(0);



				    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

				    Map parameters = new HashMap();
				    parameters.put("company", adCmpDetails.getCmpName());
				    parameters.put("branchName", user.getCurrentBranch().getBrName());
				    parameters.put("date", actionForm.getSoaDateExport());
				    Calendar calendar = new GregorianCalendar();
				    calendar.setTime(Common.convertStringToSQLDate(actionForm.getSoaDateExport()));
				    calendar.add(Calendar.DAY_OF_MONTH, 10);
				    parameters.put("dueDate", Common.convertSQLDateToString(calendar.getTime()));
				    parameters.put("customerMessage", "");
				    parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());
				    parameters.put("faxNumber", adCmpDetails.getCmpFax());
				    parameters.put("email", adCmpDetails.getCmpEmail());
				    parameters.put("tinNumber", adCmpDetails.getCmpTin());
				    parameters.put("collectionHead", "");
				    parameters.put("companyAddress", companyAddress);
				    parameters.put("invoiceBatchName", "");

				    Date firstDayOfPeriod = null;
				    Calendar dtCal = new GregorianCalendar();
				    dtCal.setTime(new Date());

					 //dtCal.add(Calendar.MONTH, -1);
					 //dtCal.add(Calendar.DAY_OF_MONTH, 1);
					 dtCal.set(Calendar.DATE,1);

					 firstDayOfPeriod = dtCal.getTime();

				  	System.out.println("firstDayOfPeriod="+firstDayOfPeriod);
				    parameters.put("firstDayOfPeriod", Common.convertSQLDateToString(firstDayOfPeriod));



				    if (agingBucket > 1) {

				    	   parameters.put("agingBucket1", "1-" + agingBucket + " days");
						   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
						   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
						   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
						   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");

				       } else {

						   parameters.put("agingBucket1", "1 day");
						   parameters.put("agingBucket2", "2 days");
						   parameters.put("agingBucket3", "3 days");
						   parameters.put("agingBucket4", "4 days");
						   parameters.put("agingBucket5", "Over 4 days");

					   }



				    parameters.put("includeCollections", "NO");
				    parameters.put("noCollections", "NO");
				    parameters.put("noCreditMemoes", "NO");

				    System.out.print("flag 4");
				    String branchMap = null;
				    boolean first = true;
				    for(int j=0; j < actionForm.getArRepBrStListSize(); j++) {

				    	ArRepBranchStatementList brList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(j);

				    	if(brList.getBranchCheckbox() == true) {
				    		if(first) {
				    			branchMap = brList.getBranchName();
				    			first = false;
				    		} else {
				    			branchMap = branchMap + ", " + brList.getBranchName();
				    		}
				    	}

				    }

				    parameters.put("branchMap", branchMap);

				    System.out.print("flag 5");

				    String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementReceiptSub.jasper";

				    if (!new java.io.File(subreportFilename).exists()) {

				    	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementReceiptSub.jasper");

				    }

				    JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

				    parameters.put("arRepStatementReceiptSub", subreport);
		            parameters.put("arRepStatementReceiptSubDS", new ArRepStatementSubDS(new ArrayList()));

		            subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCreditMemoSub.jasper";

		            if (!new java.io.File(subreportFilename).exists()) {

		            	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementCreditMemoSub.jasper");

		            }

		            subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

		            parameters.put("arRepStatementCreditMemoSub", subreport);
		            parameters.put("arRepStatementCreditMemoSubDS", new ArRepStatementSubDS(new ArrayList()));

		            // jasper reports for SOA and WATER
				    String filename_soawater = null;

				    filename_soawater = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatement.jasper";

				    if (!new java.io.File(filename_soawater).exists()) {

				    	filename_soawater = servlet.getServletContext().getRealPath("jreports/ArRepStatement.jasper");

			        }


				    //jasper reports for LEDGER
				    String filename_ledger = null;

				    filename_ledger = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCustomerLedger.jasper";

				    if (!new java.io.File(filename_ledger).exists()) {

				    	filename_ledger = servlet.getServletContext().getRealPath("jreports/ArRepStatementCustomerLedger.jasper");

			        }
					String export_dir = Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + customer_code_data + "/" + actionForm.getPeriodMail();
				    try{


				    	dirLoc = new File(export_dir);

			            if (!dirLoc.exists()) {
					    	if (dirLoc.mkdir()) {
			                     System.out.println("Directory export is created!");


			                 } else {
			                     System.out.println("Failed to create directory!");
			                 }
					    }else{
					    	//delete contents of water
					    	File child_dir =  new File(export_dir);

				    		if(child_dir.list().length>0 && x == 0){

				    		   //list all the directory contents
				        	   String files[] = child_dir.list();

				        	   for (String temp : files) {
				        	      //construct the file structure
				        	      File fileDelete = new File(child_dir, temp);

				        	      //recursive delete
				        	      fileDelete.delete();
				        	   }
				    		}
					    }

				    	System.out.println("----------------report water created");
				    	//create water report

				    	//customer_code_data
				    	//input_date remove dash






				    	String pdfName = actionForm.getPeriodMail().replaceAll("-", "") + "-" + customer_code_data;

				    	String periodDate = actionForm.getPeriodMail();
				    	String customerEmail = SOAdetails.getSmtCstEML();
				    	String subjectText = actionForm.getEmailSubject();
				    	String messageText = actionForm.getEmailMessage();
				    	//String pathWater = export_dir +  "/WATER-" + pdfName + ".pdf";
				    	String pathSoa = export_dir +  "/SOA-" + pdfName + ".pdf";
				    	String pathLedger = export_dir +  "/LEDGER-" + pdfName + ".pdf";



				    	if(!subjectText.trim().equals("")){

				    		subjectText = subjectText.replaceAll("<month>", monthStr);
				    		subjectText = subjectText.replaceAll("<customer>", customer_code_data);

				    	}



				    	UploadInvoiceDetails uploadIvoiceDetails  = new UploadInvoiceDetails(parameters,soa_list);

				    	exportDataList.add(uploadIvoiceDetails);



				    } catch(Exception ex) {

				        if(log.isInfoEnabled()) {
				           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
					   " session: " + session.getId());

				        }

					return(mapping.findForward("cmnErrorPage"));

				    }



        	   }



        	   if(actionForm.getUploadInvoiceType().equals("WooCommerce 1")){
        		   if(user.getCompany().equals("TPE")){
        			   WooCommerce1UploadClass uploadClass = new WooCommerce1UploadClass(exportDataList);
        		   }
        	   }



           }catch(Exception ex){

        	   if(log.isInfoEnabled()) {
		           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

        	   return(mapping.findForward("cmnErrorPage"));

           }



            System.out.print("end");
            System.out.println("customer folder create done");
            if(!errors.isEmpty()) {

			       saveErrors(request, new ActionMessages(errors));
			       return(mapping.findForward("arRepStatement"));

			}


            else{

				messages.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("app.csvMail", listAdded));
			}


            saveMessages(request, messages);

     */
/*******************************************************
	 -- AR SMT Export Action --
*******************************************************/

	}else if (request.getParameter("exportButton") != null) {

	 		   AdCompanyDetails adCmpDetails = null;
		 	   ArrayList customerBatchSelectedListExport = new ArrayList();
		 	   ArrayList customerDepartmentSelectedListExport = new ArrayList();

         		for (int i=0; i < actionForm.getCustomerBatchSelectedListExport().length; i++) {

         			customerBatchSelectedListExport.add(actionForm.getCustomerBatchSelectedListExport()[i]);

         		}

		 		ArrayList customerList =  ejbSMT.getArCstAll(customerBatchSelectedListExport,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		 	//ArrayList customerList = new ArrayList();
		 		 int agingBucket = 0;
		 		//check if customer exist
	            try{
		            if(actionForm.getCustomerCodeList().size()<=0){
		            	throw new GlobalNoRecordFoundException();
		            }
	            } catch (GlobalNoRecordFoundException ex) {

		              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("statement.error.noCustomerListFound"));

			    } catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}
			         System.out.println(ex.toString());
					return(mapping.findForward("cmnErrorPage"));

			    }

	            /*
	            System.out.println("print all");
	    		for(int x=1;x<actionForm.getCustomerCodeList().size();x++){
	    			String data = actionForm.getCustomerCodeList().get(x).toString();

	    			if (!data.equals("") || !data.equals(null)){
	    				customerList.add(data);
	    			}
	    		}
		 		*/
	    		String pathFolder = "c:/opt/ofs-statements";

		        //create ofs-statement dir if not exist
		        File dirLoc = new File(Constants.OFS_STATEMENT_PATH);

		        if (!dirLoc.exists()) {
		            if (dirLoc.mkdir()) {
		                System.out.println("Directory ofs-statements is created!");
		            } else {
		                System.out.println("Failed to create directory!");
		            }
				}


	            //create export folder
	            dirLoc = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export");

	            if (!dirLoc.exists()) {
			    	 if (dirLoc.mkdir()) {
	                     System.out.println("Directory export is created!");
	                     for(int x=0;x<customerList.size();x++){

	                    	 String cstNameFldr = customerList.get(x).toString();
	                    	 System.out.println("customer: " + cstNameFldr);

	                    	 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);
		                     dirLoc_child.mkdir();
	                     }
	                 } else {
	                     System.out.println("Failed to create directory!");
	                 }
			    }else{

			    	 for(int x=0;x<customerList.size();x++){
			    		 String cstNameFldr = customerList.get(x).toString();
			    		 System.out.println("customer: " + cstNameFldr);
			    		 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);

			    		 if (!dirLoc_child.exists()) {
			    			 dirLoc_child.mkdir();
			    		 }else{
			    			 System.out.println("Failed to create foder: export/" + cstNameFldr);
			    		 }

			    	 }
			    }


	           try{



	        	   SimpleDateFormat frm_dt = new SimpleDateFormat("MMM-yyyy");

	        	   Date soaDate =  Common.convertStringToSQLDate(actionForm.getSoaDateExport());

	        	   String folderSoaDate =  frm_dt.format(soaDate).toUpperCase();


	        //	   Calendar cal = Calendar.getInstance();

	        	//   cal.setTime(frm_dt.parse(actionForm.getPeriod()));

	        //	   cal.setTime(frm_dt.parse(actionForm.getSoaDateExport()));



	        	//   cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));
	       // 	   Date input_date = cal.getTime();

	        	   for(int x=0;x<customerList.size();x++){

	        		   String customer_code_data = customerList.get(x).toString();

	        		   ArrayList wateronly_list = new ArrayList();
	        		   ArrayList soa_list = new ArrayList();
	        		   ArrayList ledger = new ArrayList();


				    	 // get company

					       adCmpDetails = ejbSMT.getAdCompany(user.getCmpCode());


					       // get preference
					       AdPreferenceDetails adPreferenceDetails = ejbSMT.getAdPreference(user.getCmpCode());
					       agingBucket = adPreferenceDetails.getPrfArAgingBucket();


				    	System.out.println("enter customer loop  " + customer_code_data);
				    	 HashMap criteria = new HashMap();



			           if (!Common.validateRequired(actionForm.getSoaDateExport())) {

			        		criteria.put("dateTo", soaDate);

			           }


			          System.out.println("date -----------" +new Date() );

			           System.out.print("flag 1");
			           ArrayList branchList = new ArrayList();

			           for(int i=0; i<actionForm.getArRepBrStListSize(); i++) {

			           		ArRepBranchStatementList brStList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(i);

			           	//	if(brStList.getBranchCheckbox() == true) {

			           			AdBranchDetails brDetails = new AdBranchDetails();
			    	           	brDetails.setBrCode(brStList.getBranchCode());

			           			branchList.add(brDetails);
			           	//	}

			           }

			           System.out.println("show all="+ actionForm.getShowAll() + " include zero=" +actionForm.getIncludeZero() + "include unposted = "+ actionForm.getIncludeUnpostedInvoice() + "include tansaction = " +  actionForm.getSortByTransaction() + " water =" + actionForm.getSortByItem() + " order by = " + actionForm.getOrderBy() + " branchlist= " + branchList.size() + " cmpny=" + user.getCmpCode());



			           try{
			        	 //  list = ejbSMT.executeArRepStatement(criteria, actionForm.getShowAll(), actionForm.getIncludeZero(), actionForm.getIncludeUnpostedInvoice(), actionForm.getSortByTransaction(), actionForm.getSortByItem(), actionForm.getWaterOnly(), actionForm.getIncludeWater(), actionForm.getOrderBy(), actionForm.getGroupBy(), branchList, user.getCmpCode());

			        	   //get water list

			        	   System.out.println("data : " +  branchList.size() + " and " +  user.getCmpCode() + "----------");
			        	 //  wateronly_list = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), false, false, true, false, true, true, false, "", "", branchList,  user.getCmpCode());
			        	  // if(wateronly_list.size()<=0){
			        	//	   continue;
			        	 //  }
			        	  // ejbSMT.executeArRepStatement(criteria, showAll, includeZero, includeUnpostedInvoice, sortByTransaction, sortByItem, waterOnly, INCLD_WTR, ORDER_BY, GROUP_BY, branchList, AD_CMPNY)
			        	   //get water list


			        	   boolean includeUnpostedTransaction  = true;
			        	   boolean showUnearnedInterestTransactionOnly =false;
			        	   boolean includeAdvance = true;
			        	   boolean includeAdvanceOnly =false;
			        	   boolean showAll = false;
			        	   boolean includeZero =false ;
			        	   boolean includeCollection = false;
			        	   boolean includeUnpostedInvoice = false;
			        	   boolean includeCreditMemo = false;




			        	   soa_list = ejbSMT.executeArRepStatement(criteria,"",customer_code_data,includeUnpostedTransaction,showUnearnedInterestTransactionOnly,includeAdvance,includeAdvanceOnly,showAll, includeZero, includeCollection, includeUnpostedInvoice,includeCreditMemo,"", "", "", customerBatchSelectedListExport, customerDepartmentSelectedListExport, branchList,  user.getCmpCode());
			        	   if(soa_list.size()<=0){
			        		   continue;
			        	   }



			           }catch(GlobalNoRecordFoundException gx){
			        	   continue;
			           }






			           	ArRepStatementDetails SOAdetails = new ArRepStatementDetails();

			           SOAdetails = (ArRepStatementDetails)soa_list.get(0);



					    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();
					    System.out.print("flag 3");
					    Map parameters = new HashMap();

					    parameters.put("company", adCmpDetails.getCmpName());
					    parameters.put("branchName", user.getCurrentBranch().getBrName());
					  //  parameters.put("date", Common.convertSQLDateToString(new java.util.Date()));

					    System.out.println("actionForm.getSoaDateExport()="+actionForm.getSoaDateExport());
					    parameters.put("date", actionForm.getSoaDateExport());
					    Calendar calendar = new GregorianCalendar();
					    calendar.setTime(Common.convertStringToSQLDate(actionForm.getSoaDateExport()));
					    calendar.add(Calendar.DAY_OF_MONTH, 10);
					    parameters.put("dueDate", Common.convertSQLDateToString(calendar.getTime()));



					    parameters.put("customerMessage", "");
					    parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());
					    parameters.put("faxNumber", adCmpDetails.getCmpFax());
					    parameters.put("email", adCmpDetails.getCmpEmail());
					    parameters.put("tinNumber", adCmpDetails.getCmpTin());
					    parameters.put("collectionHead", "");
					    parameters.put("companyAddress", companyAddress);
					    parameters.put("invoiceBatchName", "");

					    Date firstDayOfPeriod = null;
					    Calendar dtCal = new GregorianCalendar();
					    dtCal.setTime(new Date());

						 //dtCal.add(Calendar.MONTH, -1);
						 //dtCal.add(Calendar.DAY_OF_MONTH, 1);
						 dtCal.set(Calendar.DATE,1);

						 firstDayOfPeriod = dtCal.getTime();

					  	System.out.println("firstDayOfPeriod="+firstDayOfPeriod);
					    parameters.put("firstDayOfPeriod", Common.convertSQLDateToString(firstDayOfPeriod));



					    if (agingBucket > 1) {

					    	   parameters.put("agingBucket1", "1-" + agingBucket + " days");
							   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
							   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
							   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
							   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");

					       } else {

							   parameters.put("agingBucket1", "1 day");
							   parameters.put("agingBucket2", "2 days");
							   parameters.put("agingBucket3", "3 days");
							   parameters.put("agingBucket4", "4 days");
							   parameters.put("agingBucket5", "Over 4 days");

						   }



					    parameters.put("includeCollections", "NO");
					    parameters.put("noCollections", "NO");
					    parameters.put("noCreditMemoes", "NO");

					    System.out.print("flag 4");
					    String branchMap = null;
					    boolean first = true;
					    for(int j=0; j < actionForm.getArRepBrStListSize(); j++) {

					    	ArRepBranchStatementList brList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(j);

					    	if(brList.getBranchCheckbox() == true) {
					    		if(first) {
					    			branchMap = brList.getBranchName();
					    			first = false;
					    		} else {
					    			branchMap = branchMap + ", " + brList.getBranchName();
					    		}
					    	}

					    }

					    parameters.put("branchMap", branchMap);

					    System.out.print("flag 5");

					    String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementReceiptSub.jasper";

					    if (!new java.io.File(subreportFilename).exists()) {

					    	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementReceiptSub.jasper");

					    }

					    JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

					    parameters.put("arRepStatementReceiptSub", subreport);
			            parameters.put("arRepStatementReceiptSubDS", new ArRepStatementSubDS(new ArrayList()));

			            subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCreditMemoSub.jasper";

			            if (!new java.io.File(subreportFilename).exists()) {

			            	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementCreditMemoSub.jasper");

			            }

			            subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

			            parameters.put("arRepStatementCreditMemoSub", subreport);
			            parameters.put("arRepStatementCreditMemoSubDS", new ArRepStatementSubDS(new ArrayList()));

			            // jasper reports for SOA and WATER
					    String filename_soawater = null;

					    filename_soawater = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatement.jasper";

					    if (!new java.io.File(filename_soawater).exists()) {

					    	filename_soawater = servlet.getServletContext().getRealPath("jreports/ArRepStatement.jasper");

				        }


					    //jasper reports for LEDGER
					    String filename_ledger = null;

					    filename_ledger = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCustomerLedger.jasper";

					    if (!new java.io.File(filename_ledger).exists()) {

					    	filename_ledger = servlet.getServletContext().getRealPath("jreports/ArRepStatementCustomerLedger.jasper");

				        }
						String export_dir = Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + customer_code_data + "/" + folderSoaDate;
					    try{


					    	dirLoc = new File(export_dir);

				            if (!dirLoc.exists()) {
						    	if (dirLoc.mkdir()) {
				                     System.out.println("Directory export is created!");


				                 } else {
				                     System.out.println("Failed to create directory!");
				                 }
						    }else{
						    	//delete contents of water
						    	File child_dir =  new File(export_dir);

					    		if(child_dir.list().length>0 && x == 0){

					    		   //list all the directory contents
					        	   String files[] = child_dir.list();

					        	   for (String temp : files) {
					        	      //construct the file structure
					        	      File fileDelete = new File(child_dir, temp);

					        	      //recursive delete
					        	      fileDelete.delete();
					        	   }
					    		}
						    }

					    	System.out.println("----------------report water created");
					    	//create water report

					    	//customer_code_data
					    	//input_date remove dash
					    	String pdfName = folderSoaDate.replaceAll("-", "") + "-" + customer_code_data;


					    	// parameters.put("waterOnly", true);
					    	//JasperPrint jsprPrint = JasperFillManager.fillReport(filename_soawater, parameters, new ArRepStatementDS(wateronly_list));
					    	//JasperExportManager.exportReportToPdfFile(jsprPrint,export_dir + "/WATER-" + pdfName + ".pdf");


					    	System.out.println("----------------report soa created");
					    	parameters.put("waterOnly", false);
					    	// create soa report
					    	JasperPrint jsprPrint = JasperFillManager.fillReport(filename_soawater, parameters, new ArRepStatementDS(soa_list));
					    	JasperExportManager.exportReportToPdfFile(jsprPrint,export_dir + "/SOA-" + pdfName + ".pdf");


					    	System.out.println("----------------report ledger created");
					    	// create ledger report
					    	parameters.put("waterOnly", false);
					    	jsprPrint = JasperFillManager.fillReport(filename_ledger, parameters, new ArRepStatementDS(soa_list));
					    	JasperExportManager.exportReportToPdfFile(jsprPrint,export_dir + "/LEDGER-" + pdfName + ".pdf");


					    } catch(Exception ex) {

					        if(log.isInfoEnabled()) {
					           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
						   " session: " + session.getId());

					        }

						return(mapping.findForward("cmnErrorPage"));

					    }

					//    if(x==2){
					  //  	break;
					   // }

	        	   }





	           }catch(Exception ex){

	        	   if(log.isInfoEnabled()) {
			           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
				   " session: " + session.getId());

			        }

	        	   return(mapping.findForward("cmnErrorPage"));

	           }



	            System.out.print("end");
	            System.out.println("customer folder create done");
	            if(!errors.isEmpty()) {

				       saveErrors(request, new ActionMessages(errors));
				       return(mapping.findForward("arRepStatement"));

				}else{

					messages.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("app.export",messages));
				}


	            saveMessages(request, messages);




/*******************************************************
   -- AR SMT Create CSV for Email --
*******************************************************/

		     } else if (request.getParameter("emailButton") != null) {

		    	 ArrayList csvCustomerList = new ArrayList();
		    	 ArrayList customerBatchSelectedListMail = new ArrayList();
		    	 ArrayList customerDepartmentSelectedListMail = new ArrayList();

		    	String monthStr = "";
		    	int listAdded = 0;
		        int noOfHaveEmail = 0;
	             int noOfNoEmail = 0;
		    	 ArrayList customerList = new ArrayList();

		 		 AdCompanyDetails adCmpDetails = null;
		 		 if(actionForm.getCustomerCodeMail().equals("")){

        		     for (int i=0; i < actionForm.getCustomerBatchSelectedListMail().length; i++) {

	          			customerBatchSelectedListMail.add(actionForm.getCustomerBatchSelectedListMail()[i]);

	          		 }

		 			  customerList =  ejbSMT.getArCstAll(customerBatchSelectedListMail,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


	 		      }else{

		 			   customerList.add(actionForm.getCustomerCodeMail());
	 		      }

			 	//ArrayList customerList = new ArrayList();
			 		 int agingBucket = 0;
			 		//check if customer exist
		            try{
			            if(actionForm.getCustomerCodeList().size()<=0){
			            	throw new GlobalNoRecordFoundException();
			            }
		            } catch (GlobalNoRecordFoundException ex) {

			              errors.add(ActionMessages.GLOBAL_MESSAGE,
		                     new ActionMessage("statement.error.noCustomerListFound"));

				    } catch(EJBException ex) {

				         if(log.isInfoEnabled()) {
						    log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
						    " session: " + session.getId());

						}
				         System.out.println(ex.toString());
						return(mapping.findForward("cmnErrorPage"));

				    }


		            /*
		            System.out.println("print all");
		    		for(int x=1;x<actionForm.getCustomerCodeList().size();x++){
		    			String data = actionForm.getCustomerCodeList().get(x).toString();

		    			if (!data.equals("") || !data.equals(null)){
		    				customerList.add(data);
		    			}
		    		}
			 		*/
		    		String pathFolder = "c:/opt/ofs-statements";

			        //create ofs-statement dir if not exist
			        File dirLoc = new File(Constants.OFS_STATEMENT_PATH);

			        if (!dirLoc.exists()) {
			            if (dirLoc.mkdir()) {
			                System.out.println("Directory ofs-statements is created!");
			            } else {
			                System.out.println("Failed to create directory!");
			            }
					}


		            //create export folder
		            dirLoc = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export");

		            if (!dirLoc.exists()) {
				    	 if (dirLoc.mkdir()) {
		                     System.out.println("Directory export is created!");
		                     for(int x=0;x<customerList.size();x++){

		                    	 String cstNameFldr = customerList.get(x).toString();
		                    	 System.out.println("customer: " + cstNameFldr);

		                    	 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);
			                     dirLoc_child.mkdir();
		                     }
		                 } else {
		                     System.out.println("Failed to create directory!");
		                 }
				    }else{

				    	 for(int x=0;x<customerList.size();x++){
				    		 String cstNameFldr = customerList.get(x).toString();
				    		 System.out.println("customer: " + cstNameFldr);
				    		 File dirLoc_child = new File(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/export/" + cstNameFldr);

				    		 if (!dirLoc_child.exists()) {
				    			 dirLoc_child.mkdir();
				    		 }else{
				    			 System.out.println("Failed to create foder: export/" + cstNameFldr);
				    		 }

				    	 }
				    }


		            try{
		                PrintWriter writer = new PrintWriter(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/subject_string.txt", "UTF-8");
		                writer.println(actionForm.getEmailSubject());

		                writer.close();
		            } catch (IOException e) {
		            	System.out.println("Error writing subject");
		            }



		           try{



		        	   SimpleDateFormat frm_dt = new SimpleDateFormat("MMM-yyyy");
		        	   SimpleDateFormat mnt_dt = new SimpleDateFormat("MMMM");

		        	   Date soaDate = Common.convertStringToSQLDate(actionForm.getSoaDateMail());
		        	   String folderSoaDate = frm_dt.format(soaDate).toUpperCase();

		        	   //Calendar cal = Calendar.getInstance();

		        	   //cal.setTime(frm_dt.parse(actionForm.getPeriodMail()));
		        	 //  cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));
		        	//   Date input_date = cal.getTime();
		        	   monthStr = mnt_dt.format(soaDate);

		        	   for(Integer x=0;x<customerList.size();x++){

		        		  // if(x==5) {
		        		//	   break;
		        		//   }
		        		   String customer_code_data = customerList.get(x).toString();

		        		   ArrayList wateronly_list = new ArrayList();
		        		   ArrayList soa_list = new ArrayList();
		        		   ArrayList ledger = new ArrayList();


					    	 // get company

						       adCmpDetails = ejbSMT.getAdCompany(user.getCmpCode());


						       // get preference
						       AdPreferenceDetails adPreferenceDetails = ejbSMT.getAdPreference(user.getCmpCode());
						       agingBucket = adPreferenceDetails.getPrfArAgingBucket();


					    	System.out.println("enter customer loop  " + customer_code_data);
					    	 HashMap criteria = new HashMap();



				           if (!Common.validateRequired(actionForm.getSoaDateMail())) {

				        		criteria.put("dateTo",soaDate);

				           }

						//   criteria.put("includeAdvance", "YES");
						//   criteria.put("includeAdvanceOnly", "NO");

						//   criteria.put("includeUnpostedTransaction", "YES");


				          System.out.println("date -----------" +soaDate);

				           System.out.print("flag 1");
				           ArrayList branchList = new ArrayList();

				           for(int i=0; i<actionForm.getArRepBrStListSize(); i++) {

				           		ArRepBranchStatementList brStList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(i);

				           	//	if(brStList.getBranchCheckbox() == true) {

				           			AdBranchDetails brDetails = new AdBranchDetails();
				    	           	brDetails.setBrCode(brStList.getBranchCode());

				           			branchList.add(brDetails);
				           	//	}

				           }

				           System.out.println("show all="+ actionForm.getShowAll() + " include zero=" +actionForm.getIncludeZero() + "include unposted = "+ actionForm.getIncludeUnpostedInvoice() + "include tansaction = " +  actionForm.getSortByTransaction() + " water =" + actionForm.getSortByItem() + " order by = " + actionForm.getOrderBy() + " branchlist= " + branchList.size() + " cmpny=" + user.getCmpCode());



				           try{



						        //	   list =      ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), customerCode, includeUnpostedTransaction, includeAdvance, showUnearnedInterestTransactionOnly, includeAdvanceOnly,actionForm.getShowAll(), actionForm.getIncludeZero(), actionForm.getIncludeCollections(), actionForm.getIncludeUnpostedInvoice(), actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getReportType(),customerBatchSelectedList, customerDepartmentSelectedList , branchList, user.getCmpCode());

				        	   boolean includeUnpostedTransaction  = true;
				        	   boolean showUnearnedInterestTransactionOnly =false;
				        	   boolean includeAdvance = true;
				        	   boolean includeAdvanceOnly =false;
				        	   boolean showAll = false;
				        	   boolean includeZero =false ;
				        	   boolean includeCollection = false;
				        	   boolean includeUnpostedInvoice = false;
				        	   boolean includeCreditMemo = false;

				        	   soa_list = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(),customer_code_data, includeUnpostedTransaction,showUnearnedInterestTransactionOnly,includeAdvance,includeAdvanceOnly,showAll, includeZero, includeCollection, includeUnpostedInvoice,includeCreditMemo,"", "", "", customerBatchSelectedListMail, customerDepartmentSelectedListMail,branchList,  user.getCmpCode());
				        	//   ledger = ejbSMT.executeArRepStatement(criteria,actionForm.getMemoLine(), false, true, false,"", "", "CUSTOMER LEDGER",customerBatchSelectedListMail, branchList,  user.getCmpCode());

				           }catch(GlobalNoRecordFoundException gx){
				        	   System.out.println("no customer found - XXXX");
				        	   continue;
				           }






				           	ArRepStatementDetails SOAdetails = new ArRepStatementDetails();

				           SOAdetails = (ArRepStatementDetails)soa_list.get(0);



						    String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();
						    System.out.print("flag 3");
						    Map parameters = new HashMap();

						    parameters.put("company", adCmpDetails.getCmpName());
						    parameters.put("branchName", user.getCurrentBranch().getBrName());

						    parameters.put("date", actionForm.getSoaDateMail());
						    Calendar calendar = new GregorianCalendar();
						    calendar.setTime(Common.convertStringToSQLDate(actionForm.getSoaDateMail()));
						    calendar.add(Calendar.DAY_OF_MONTH, 10);
						    parameters.put("dueDate", Common.convertSQLDateToString(calendar.getTime()));



						    parameters.put("customerMessage", "");
						    parameters.put("telephoneNumber", adCmpDetails.getCmpPhone());
						    parameters.put("faxNumber", adCmpDetails.getCmpFax());
						    parameters.put("email", adCmpDetails.getCmpEmail());
						    parameters.put("tinNumber", adCmpDetails.getCmpTin());
						    parameters.put("collectionHead", "");
						    parameters.put("companyAddress", companyAddress);
						    parameters.put("invoiceBatchName", "");

						    Date firstDayOfPeriod = null;
						    Calendar dtCal = new GregorianCalendar();
						    dtCal.setTime(new Date());

							 //dtCal.add(Calendar.MONTH, -1);
							 //dtCal.add(Calendar.DAY_OF_MONTH, 1);
							 dtCal.set(Calendar.DATE,1);

							 firstDayOfPeriod = dtCal.getTime();

						  	System.out.println("firstDayOfPeriod="+firstDayOfPeriod);
						    parameters.put("firstDayOfPeriod", Common.convertSQLDateToString(firstDayOfPeriod));



						    if (agingBucket > 1) {

						    	   parameters.put("agingBucket1", "1-" + agingBucket + " days");
								   parameters.put("agingBucket2", (agingBucket + 1) + "-" + (agingBucket * 2) + " days");
								   parameters.put("agingBucket3", ((agingBucket * 2) + 1) + "-" + (agingBucket * 3) + " days");
								   parameters.put("agingBucket4", ((agingBucket * 3) + 1) + "-" + (agingBucket * 4) + " days");
								   parameters.put("agingBucket5", "Over " + (agingBucket * 4) + " days");

						       } else {

								   parameters.put("agingBucket1", "1 day");
								   parameters.put("agingBucket2", "2 days");
								   parameters.put("agingBucket3", "3 days");
								   parameters.put("agingBucket4", "4 days");
								   parameters.put("agingBucket5", "Over 4 days");

							   }



						    parameters.put("includeCollections", "NO");
						    parameters.put("noCollections", "NO");
						    parameters.put("noCreditMemoes", "NO");

						    System.out.print("flag 4");
						    String branchMap = null;
						    boolean first = true;
						    for(int j=0; j < actionForm.getArRepBrStListSize(); j++) {

						    	ArRepBranchStatementList brList = (ArRepBranchStatementList)actionForm.getArRepBrStListByIndex(j);

						    	if(brList.getBranchCheckbox() == true) {
						    		if(first) {
						    			branchMap = brList.getBranchName();
						    			first = false;
						    		} else {
						    			branchMap = branchMap + ", " + brList.getBranchName();
						    		}
						    	}

						    }

						    parameters.put("branchMap", branchMap);

						    System.out.print("flag 5");

						    String subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementReceiptSub.jasper";

						    if (!new java.io.File(subreportFilename).exists()) {

						    	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementReceiptSub.jasper");

						    }

						    JasperReport subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

						    parameters.put("arRepStatementReceiptSub", subreport);
				            parameters.put("arRepStatementReceiptSubDS", new ArRepStatementSubDS(new ArrayList()));

				            subreportFilename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCreditMemoSub.jasper";

				            if (!new java.io.File(subreportFilename).exists()) {

				            	subreportFilename = servlet.getServletContext().getRealPath("jreports/ArRepStatementCreditMemoSub.jasper");

				            }

				            subreport = (JasperReport)JRLoader.loadObject(subreportFilename);

				            parameters.put("arRepStatementCreditMemoSub", subreport);
				            parameters.put("arRepStatementCreditMemoSubDS", new ArRepStatementSubDS(new ArrayList()));

				            // jasper reports for SOA and WATER
						    String filename_soawater = null;

						    filename_soawater = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatement.jasper";

						    if (!new java.io.File(filename_soawater).exists()) {

						    	filename_soawater = servlet.getServletContext().getRealPath("jreports/ArRepStatement.jasper");

					        }


						    //jasper reports for LEDGER
						    String filename_ledger = null;

						    filename_ledger = "/opt/ofs-resources/" + user.getCompany() + "/ArRepStatementCustomerLedger.jasper";

						    if (!new java.io.File(filename_ledger).exists()) {

						    	filename_ledger = servlet.getServletContext().getRealPath("jreports/ArRepStatementCustomerLedger.jasper");

					        }
							String export_dir = Constants.OFS_STATEMENT_PATH +  user.getCompany() + "/export/" + customer_code_data + "/" + folderSoaDate;
						    try{


						    	dirLoc = new File(export_dir);

					            if (!dirLoc.exists()) {
							    	if (dirLoc.mkdir()) {
					                     System.out.println("Directory export is created!");


					                 } else {
					                     System.out.println("Failed to create directory!");
					                 }
							    }else{
							    	//delete contents of water
							    	File child_dir =  new File(export_dir);

						    		if(child_dir.list().length>0 && x == 0){

						    		   //list all the directory contents
						        	   String files[] = child_dir.list();

						        	   for (String temp : files) {
						        	      //construct the file structure
						        	      File fileDelete = new File(child_dir, temp);

						        	      //recursive delete
						        	      fileDelete.delete();
						        	   }
						    		}
							    }

						    	System.out.println("----------------report water created");
						    	//create water report

						    	//customer_code_data
						    	//input_date remove dash






						    	String pdfName = folderSoaDate.replaceAll("-", "") + "-" + customer_code_data;

						    	String periodDate = folderSoaDate;
						    	String customerEmail = SOAdetails.getSmtCstEML();
						    	String subjectText = actionForm.getEmailSubject();
						    	String messageText = actionForm.getEmailMessage();
						    	//String pathWater = export_dir +  "/WATER-" + pdfName + ".pdf";
						    	String pathSoa = export_dir +  "/SOA-" + pdfName + ".pdf";
						    	String pathLedger = export_dir +  "/LEDGER-" + pdfName + ".pdf";



						    	if(!subjectText.trim().equals("")){

						    		subjectText = subjectText.replaceAll("<month>", monthStr);
						    		subjectText = subjectText.replaceAll("<customer>", customer_code_data);

						    	}

						    //	parameters.put("waterOnly", true);
						    //	JasperPrint jsprPrint = JasperFillManager.fillReport(filename_soawater, parameters, new ArRepStatementDS(wateronly_list));
						   // 	JasperExportManager.exportReportToPdfFile(jsprPrint,export_dir + "/WATER-" + pdfName + ".pdf");


						    	System.out.println("----------------report soa created");
						    	parameters.put("waterOnly", false);
						    	// create soa report
						    	JasperPrint jsprPrint = JasperFillManager.fillReport(filename_soawater, parameters, new ArRepStatementDS(soa_list));
						    	JasperExportManager.exportReportToPdfFile(jsprPrint,export_dir +  "/SOA-" + pdfName + ".pdf");




						    	//write all results in csv
						    	/*
						    		"ID",
						    		"Customer ID",
						    		"Email",
						    		"Subject",
						    		"Message",
						    		"Ledger File",
						    		"SOA File",
						    		"Water File"
						    	 */



						    	System.out.println(customer_code_data + " adding");
						    	System.out.println ("WRITING ---------------------------------------");
						    	Map<String,String> customerData = new HashMap();
						    	customerData.put("Customer ID", customer_code_data);
						    	customerData.put("Email",customerEmail);
						    	customerData.put("Subject",subjectText);
						    	customerData.put("Message",messageText);
						    	//customerData.put("Customer Batch", arg1);

						    	customerData.put("Ledger File",pathLedger);
						    	customerData.put("SOA File", pathSoa);
						    	customerData.put("Water File", "");
						    	csvCustomerList.add(customerData);

						    	System.out.println ("WRITING --add-------------------------------------");


						    } catch(Exception ex) {

						        if(log.isInfoEnabled()) {
						           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
							   " session: " + session.getId());

						        }

							return(mapping.findForward("cmnErrorPage"));

						    }
							/*limit loop
						   	if(x==2){
						  		break;
						   	}
						     */


		        	   }

		        	   StringBuilder strB = new StringBuilder();
		        	   String csvName = actionForm.getCsvFilename();
		        	   String columnLine= "ID,Customer ID,Email,Subject,Message,Ledger File,SOA File,Water File";

		        	   strB.append(columnLine);


		        	   String[] columnArr = columnLine.split(",");

		        	   try {
			        	   System.out.println("start writing csv");
		 				 // File file = new File(Constants.OFS_STATEMENT_PATH + "/" +  user.getCompany() + "/"  + csvName + ".csv");
		 			//	  FileOutputStream is = new FileOutputStream(file);
		 		      //    OutputStreamWriter osw = new OutputStreamWriter(is);
		 		     //     Writer w = new BufferedWriter(osw);
		 		   //       w.write(columnLine);




		 		          for(Integer cnt=0;cnt<csvCustomerList.size();cnt++){

		 		        	if( customerBatchSelectedListMail.size()>0){

		 		        	 }


		 		        	   Map<String,String> data = (Map<String,String>)csvCustomerList.get(cnt);
		 		        	  Integer seqNum = cnt + 1;
		 		        	  String line_data = seqNum.toString();
		 		        	   System.out.println(actionForm.getIncludeNoEmail() + " ---------------");
		 		        	  if(!actionForm.getIncludeNoEmail()){
		 		        		  if(!data.get("Email").trim().equals("")){
		 		        			 noOfHaveEmail++;

		 		        			  System.out.println(" email is: " + data.get("Email")+ "----------------");
		 		        			  for(int cnt2 = 1;cnt2 < columnArr.length;cnt2++){
				 		        		   line_data +=  "," + data.get(columnArr[cnt2]);
				 		        	   }
		 		        			 strB.append(System.getProperty("line.separator"));
		 		        			 strB.append(line_data);
				 		        //	   w.write("\n");
				 		        	//   w.write(line_data);
				 		        	  listAdded++;
		 		        		  }  else{
		 		        			 noOfNoEmail++;
		 		        			  continue;
		 		        		  }
		 		        	  }else{
		 		        		 if(!data.get("Email").trim().equals("")){
		 		        			noOfHaveEmail++;
		 		        		 }else{
		 		        			noOfNoEmail++;
		 		        		 }



		 		        		  for(int cnt2 = 1;cnt2 < columnArr.length;cnt2++){
			 		        		   line_data +=  "," + data.get(columnArr[cnt2]);
			 		        	   }

			 		        	//   w.write("\n");
			 		        	//   w.write(line_data);
		 		        		 strB.append(System.getProperty("line.separator"));
	 		        			 strB.append(line_data);
			 		        	  listAdded++;
		 		        	  }

		 		        	 /*
		 		        	  //for test
		 		        	  for(int x=0;x<250;x++) {
		 		        		  strB.append(System.getProperty("line.separator"));
		 		        		  strB.append(line_data);
		 		        	  }
		 		        	  */

		 		        	 PrintWriter writer = new PrintWriter(Constants.OFS_STATEMENT_PATH + "/" +  user.getCompany() + "/"  + csvName + ".csv", "UTF-8");
		 		        	 writer.print(strB.toString());
		 		        	 writer.close();


		 		           }

		 		         Report report = new Report();
		 		         report.setViewType(Constants.REPORT_EMAIL_BLAST_CSV);
		                 report.setBytes(strB.toString().getBytes());
		 		       //   w.close();

		                 session.setAttribute(Constants.REPORT_KEY, report);
		                 session.setAttribute("csvFileName", csvName);
		  		         actionForm.setReport(Constants.STATUS_SUCCESS);
		 			 }catch(Exception e){
		 				 System.err.println("Problem writing to the file statsTest.txt");
		 			 }





		           }catch(Exception ex){

		        	   if(log.isInfoEnabled()) {
				           log.info("Exception caught in ArRepStatementAction.execute(): " + ex.getMessage() +
					   " session: " + session.getId());

				        }

		        	   return(mapping.findForward("cmnErrorPage"));

		           }



		            System.out.print("end");
		            System.out.println("customer folder create done");
		            if(!errors.isEmpty()) {

					       saveErrors(request, new ActionMessages(errors));
					       return(mapping.findForward("arRepStatement"));

					}


		            else{
		            	StringBuilder strBMessage = new StringBuilder();

		            	String statusMessage = "";
		            	if(listAdded > 0 ){
		            		statusMessage = "Have email: " + noOfHaveEmail + "  No email: " + noOfNoEmail + "  Lines Created : " + listAdded;

		            		strBMessage.append("Total Line Cread: " + listAdded);
			            	strBMessage.append(System.getProperty("line.separator"));
			            	strBMessage.append("Have email: " + noOfHaveEmail);
			            	strBMessage.append(System.getProperty("line.separator"));
			            	strBMessage.append("No email: " + noOfNoEmail);
		            	}else{
		            		statusMessage = "Lines Created : " + listAdded;

		            		strBMessage.append("Total Line Cread: " + listAdded);
			            	strBMessage.append(System.getProperty("line.separator"));
		            	}
						messages.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("app.csvMail", strBMessage.toString()));
					}


		            saveMessages(request, messages);



/*******************************************************
	-- AR SMT Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		    	 return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AR SMT Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	         	try {


	         			String pathFolder = "c:/opt/ofs-statements";

	         			//creating ofs-statements and company folder
			            File dirLoc = new File("C:/opt/ofs-statements");
			            if (!dirLoc.exists()) {
			            	 if (dirLoc.mkdir()) {
			                     System.out.println("Directory ofs-statements is created!");
			                 } else {
			                     System.out.println("Failed to create directory!");
			                 }
					    }


			            dirLoc = new File("C:/opt/ofs-statements/" + user.getCompany());
			            if (!dirLoc.exists()) {
			            	 if (dirLoc.mkdir()) {
			                     System.out.println("Directory ofs-statements/" +  user.getCompany() +" is created!");
			                 } else {
			                     System.out.println("Failed to create ofs-statements/<company name> directory!");
			                 }
					    }


		            actionForm.reset(mapping, request);

	         		ArrayList list = null;
	         		Iterator i = null;



	            	actionForm.clearMemoLineList();

	                 list = ejbSMT.getArSmlAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	                 if (list == null || list.size() == 0) {

	                     actionForm.setMemoLineList(Constants.GLOBAL_NO_RECORD_FOUND);

	                 } else {

	                     i = list.iterator();

	                     while (i.hasNext()) {

	                         actionForm.setMemoLineList((String)i.next());

	                     }

	                 }



	         		actionForm.clearCustomerTypeList();

	         		list = ejbSMT.getArCtAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setCustomerTypeList((String)i.next());

	         			}

	         		}

	         		actionForm.clearReportTypeList();

			        list = ejbSMT.getAdLvReportTypeAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setReportTypeList((String)i.next());

	            		}

	            	}


	         		actionForm.clearCustomerBatchList();

	         		list = ejbSMT.getAdLvCustomerBatchAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setCustomerBatchList((String)i.next());

	         			}

	         		}


	         		actionForm.clearCustomerDepartmentList();

	         		list = ejbSMT.getAdLvCustomerDepartmentAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setCustomerDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setCustomerDepartmentList((String)i.next());

	         			}

	         		}

	         		actionForm.clearCustomerClassList();

	         		list = ejbSMT.getArCcAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setCustomerClassList((String)i.next());

	         			}

	         		}

	         		if(actionForm.getUseCustomerPulldown()) {

	         			actionForm.clearCustomerCodeList();

	         			list = ejbSMT.getArCstAll(new ArrayList(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	         			if (list == null || list.size() == 0) {

	         				actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

	         			} else {


	         				i = list.iterator();

	         				while (i.hasNext()) {

	         					actionForm.setCustomerCodeList((String)i.next());

	         				}

	         			}

	            	}

	         		actionForm.clearArRepBrStList();

	         		list = ejbSMT.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

	         		i = list.iterator();

	         		while(i.hasNext()) {

	         			AdBranchDetails details = (AdBranchDetails)i.next();

	         			ArRepBranchStatementList arRepBrStList = new ArRepBranchStatementList(actionForm,
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			arRepBrStList.setBranchCheckbox(true);

	         			actionForm.saveArRepBrStList(arRepBrStList);

	         		}

	         	 actionForm.clearInvoiceBatchNameList();

                 list = ejbSMT.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setInvoiceBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setInvoiceBatchNameList((String)i.next());

                     }

                 }

				} catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepStatementAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

			    }


	         	Calendar calendar = new GregorianCalendar();
	 		    calendar.setTime(new Date());
	 		    calendar.set(Calendar.DAY_OF_MONTH, 5);

	            actionForm.setDateTo(Common.convertSQLDateToString(new Date()));
	            actionForm.setSoaDateExport(Common.convertSQLDateToString(calendar.getTime()));
	            actionForm.setSoaDateMail(Common.convertSQLDateToString(calendar.getTime()));


	            BufferedReader br = null;
	    		FileReader fr = null;

	    		try {

	    			fr = new FileReader(Constants.OFS_STATEMENT_PATH + "/" + user.getCompany() + "/subject_string.txt");
	    			br = new BufferedReader(fr);

	    			String sCurrentLine =br.readLine();

	    			actionForm.setEmailSubject(sCurrentLine);

	    		} catch (IOException e) {

	    			actionForm.setEmailSubject("");
	    		}
	            return(mapping.findForward("arRepStatement"));

			 } else {

			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));

			    return(mapping.findForward("cmnMain"));

			 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in ArRepStatementAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }


	  private String SignCertificate(String forSign) throws NoSuchAlgorithmException
			{
				MessageDigest md = MessageDigest.getInstance("SHA-512");
				md.reset();
				byte[] buffer = forSign.getBytes();
				md.update(buffer);
				byte[] digest = md.digest();

				String hexStr = "";
				for (int i = 0; i < digest.length; i++) {
					hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
				}
				String strToken=hexStr.replace("-", "").toLowerCase();
				return strToken;	
			}

}

 class sortSoaTransactionType implements Comparator<ArRepStatementDetails> {
	public int compare(ArRepStatementDetails a , ArRepStatementDetails b) {
		return a.getSmtTransactionType().compareTo(b.getSmtTransactionType());
	}
}
 
 class sortSoaCustomerCode implements Comparator<ArRepStatementDetails> {
	public int compare(ArRepStatementDetails a , ArRepStatementDetails b) {
		return a.getSmtCustomerCode().compareTo(b.getSmtCustomerCode());
	}
}
 
 class sortSoaDate implements Comparator<ArRepStatementDetails> {
	public int compare(ArRepStatementDetails a , ArRepStatementDetails b) {
		return a.getSmtDate().compareTo(b.getSmtDate());
	}
}
 
 class GroupBySorter implements Comparator<ArRepStatementDetails> {
	 
	    private List<Comparator<ArRepStatementDetails>> listComparators;
	 
	    public GroupBySorter(Comparator<ArRepStatementDetails>... comparators) {
	        this.listComparators = Arrays.asList(comparators);
	    }
	 
	    public int compare(ArRepStatementDetails a, ArRepStatementDetails b) {
	        for (Comparator<ArRepStatementDetails> comparator : listComparators) {
	            int result = comparator.compare(a, b);
	            if (result != 0) {
	                return result;
	            }
	        }
	        return 0;
	    }
	}
