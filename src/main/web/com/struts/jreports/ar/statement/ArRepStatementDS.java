package com.struts.jreports.ar.statement;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ar.agingsummary.ArRepAgingSummaryData;
import com.struts.util.Common;
import com.util.ArRepStatementDetails;

public class ArRepStatementDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepStatementDS(ArrayList list) {

      Iterator i = list.iterator();

      while (i.hasNext()) {

         ArRepStatementDetails details = (ArRepStatementDetails)i.next();

	     ArRepStatementData smtData = new ArRepStatementData(


	  		 details.getSmtPmProjectCode(),
	  		details.getSmtPmProjectType(),
	  		 details.getSmtPmProjectName(),
	  		 details.getSmtPmContractPrice(),
	  		details.getSmtPmContractTermName(),
	  		details.getSmtPmContractValue(),


	    	details.getSmtCustomerCode(),
	    	details.getSmtCstCustomerID(),
	    	details.getSmtCstEmployeeID(),
	    	details.getSmtCstAccountNumber(),

	    	details.getSmtCustomerBatch(),
	    	details.getSmtCustomerDepartment(),
	    	Common.convertSQLDateToString(details.getSmtCustomerEntryDate()),

	         details.getSmtCustomerName(),
	         details.getSmtCustomerAddress(),
	         details.getSmtCstContact(),
	         details.getSmtCstTin(),
	         details.getSmtCstSquareMeter(),
	         details.getSmtCstParkingID(),
	         details.getSmtCstBillingHeader(),
	         details.getSmtCstBillingFooter(),
	         details.getSmtCstBillingSignatory(),
			 details.getSmtCstSignatoryTitle(),
			 Common.convertSQLDateToString(details.getSmtDate()),

	         Common.convertSQLDateToString(details.getSmtDueDate()),
	         details.getSmtDueDate(),
	         Common.convertSQLDateToString(details.getSmtLastDueDate()),
	         new Double(details.getSmtPaymentTerm()),
	         details.getSmtPaymentTermName(),
	         details.getSmtTransaction(),
	         details.getSmtTransactionType(),
	         details.getSmtPosted() == (byte)1 ? "YES" : "NO",
	         details.getSmtOrNumber(),
	         details.getSmtInvoiceNumber(),
	         Common.convertSQLDateToString(details.getSmtLastOrDate()),
	         new Double(details.getSmtInvoiceAmountDue()),
	         new Double(details.getSmtInvoiceDownPayment()),
	         new Double(details.getSmtInvoiceUnearnedInterest()),


	         new Double(details.getSmtInvoicePenaltyDue()),
	         new Double(details.getSmtNumber()),
	         new Double(details.getSmtAmountDue()),
	         new Double(details.getSmtIpsAmountDue()),
	         new Double(details.getSmtAmountPaid()),
	         new Double(details.getSmtInterestDue()),
	         new Double(details.getSmtAmountDetails()),
	         new Double(details.getSmtDebit()),
	         new Double(details.getSmtCredit()),
	         new Double(details.getSmtCreditAmount()),
	         details.getSmtBatchName(),
	         details.getSmtDescription(),

	         details.getSmtItemDescription(),

	         new Double(details.getSmtPreviousReading()),
	         new Double(details.getSmtPresentReading()),
	         new Double(details.getSmtQuantity()),
	         new Double(details.getSmtUnitPrice()),
	         new Double(details.getSmtAmount()),
	         new Double(details.getSmtPenalty()),
	         new Double(details.getSmtTaxAmount()),
	         new Double(details.getSmtWithholdingTaxAmount()),
	         details.getSmtTaxCode(),

	         details.getSmtRemainingBalance(),
	         details.getSmtBucket0() != 0d ? new Double(details.getSmtBucket0()) : null,
	         details.getSmtBucket1() != 0d ? new Double(details.getSmtBucket1()) : null,
	         details.getSmtBucket2() != 0d ? new Double(details.getSmtBucket2()) : null,
	         details.getSmtBucket3() != 0d ? new Double(details.getSmtBucket3()) : null,
	         details.getSmtBucket4() != 0d ? new Double(details.getSmtBucket4()) : null,
	         details.getSmtBucket5() != 0d ? new Double(details.getSmtBucket5()) : null,

    		 details.getSmtBucketASD0() != 0d ? new Double(details.getSmtBucketASD0()) : null,
    		         details.getSmtBucketASD1() != 0d ? new Double(details.getSmtBucketASD1()) : null,
    		         details.getSmtBucketASD2() != 0d ? new Double(details.getSmtBucketASD2()) : null,
    		         details.getSmtBucketASD3() != 0d ? new Double(details.getSmtBucketASD3()) : null,
    		         details.getSmtBucketASD4() != 0d ? new Double(details.getSmtBucketASD4()) : null,
    		         details.getSmtBucketASD5() != 0d ? new Double(details.getSmtBucketASD5()) : null,

    		        		 details.getSmtBucketRPT0() != 0d ? new Double(details.getSmtBucketRPT0()) : null,
    		        		         details.getSmtBucketRPT1() != 0d ? new Double(details.getSmtBucketRPT1()) : null,
    		        		         details.getSmtBucketRPT2() != 0d ? new Double(details.getSmtBucketRPT2()) : null,
    		        		         details.getSmtBucketRPT3() != 0d ? new Double(details.getSmtBucketRPT3()) : null,
    		        		         details.getSmtBucketRPT4() != 0d ? new Double(details.getSmtBucketRPT4()) : null,
    		        		         details.getSmtBucketRPT5() != 0d ? new Double(details.getSmtBucketRPT5()) : null,

    		        		        		 details.getSmtBucketPD0() != 0d ? new Double(details.getSmtBucketPD0()) : null,
    		        		        		         details.getSmtBucketPD1() != 0d ? new Double(details.getSmtBucketPD1()) : null,
    		        		        		         details.getSmtBucketPD2() != 0d ? new Double(details.getSmtBucketPD2()) : null,
    		        		        		         details.getSmtBucketPD3() != 0d ? new Double(details.getSmtBucketPD3()) : null,
    		        		        		         details.getSmtBucketPD4() != 0d ? new Double(details.getSmtBucketPD4()) : null,
    		        		        		         details.getSmtBucketPD5() != 0d ? new Double(details.getSmtBucketPD5()) : null,

    		        		        		        		 details.getSmtBucketWTR0() != 0d ? new Double(details.getSmtBucketWTR0()) : null,
    		        		        		        		         details.getSmtBucketWTR1() != 0d ? new Double(details.getSmtBucketWTR1()) : null,
    		        		        		        		         details.getSmtBucketWTR2() != 0d ? new Double(details.getSmtBucketWTR2()) : null,
    		        		        		        		         details.getSmtBucketWTR3() != 0d ? new Double(details.getSmtBucketWTR3()) : null,
    		        		        		        		         details.getSmtBucketWTR4() != 0d ? new Double(details.getSmtBucketWTR4()) : null,
    		        		        		        		         details.getSmtBucketWTR5() != 0d ? new Double(details.getSmtBucketWTR5()) : null,

    		        		        		        		        		 details.getSmtBucketMISC0() != 0d ? new Double(details.getSmtBucketMISC0()) : null,
    		        		        		        		        		         details.getSmtBucketMISC1() != 0d ? new Double(details.getSmtBucketMISC1()) : null,
    		        		        		        		        		         details.getSmtBucketMISC2() != 0d ? new Double(details.getSmtBucketMISC2()) : null,
    		        		        		        		        		         details.getSmtBucketMISC3() != 0d ? new Double(details.getSmtBucketMISC3()) : null,
    		        		        		        		        		         details.getSmtBucketMISC4() != 0d ? new Double(details.getSmtBucketMISC4()) : null,
    		        		        		        		        		         details.getSmtBucketMISC5() != 0d ? new Double(details.getSmtBucketMISC5()) : null,

    		        		        		        		        		        		 details.getSmtBucketADV0() != 0d ? new Double(details.getSmtBucketADV0()) : null,
    		        		        		        		        		        		         details.getSmtBucketADV1() != 0d ? new Double(details.getSmtBucketADV1()) : null,
    		        		        		        		        		        		         details.getSmtBucketADV2() != 0d ? new Double(details.getSmtBucketADV2()) : null,
    		        		        		        		        		        		         details.getSmtBucketADV3() != 0d ? new Double(details.getSmtBucketADV3()) : null,
    		        		        		        		        		        		         details.getSmtBucketADV4() != 0d ? new Double(details.getSmtBucketADV4()) : null,
    		        		        		        		        		        		         details.getSmtBucketADV5() != 0d ? new Double(details.getSmtBucketADV5()) : null,


	        details.getSmtReferenceNumber(), details.getSmtInvoiceBatchName(),

	        details.getSmtNumberDays(),
	        details.getSmtExclusiveAdvance(),
	        details.getSmtInclusiveAdvance()

	    		 );

         data.add(smtData);
      }

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();


	  if("pmProjectCode".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getPmProjectCode();
	  }else if("pmProjectType".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPmProjectType();
	  }else if("pmProjectName".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPmProjectName();
	  }else if("pmContractPrice".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPmContractPrice();
	  }else if("pmContractTermName".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPmContractTermName();
	  }else if("pmContractTermValue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPmContractTermValue();



	  }else if("customerCode".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCustomerCode();

	  }else if("customerID".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getCustomerID();
	  }else if("employeeID".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getEmployeeID();
	  }else if("accountNumber".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getAccountNumber();
	  }else if("invoiceBatchName".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoiceBatchName();
	  }else if("customerBatch".equals(fieldName)){
	         value = ((ArRepStatementData)data.get(index)).getCustomerBatch();
	  }else if("customerDepartment".equals(fieldName)){
	         value = ((ArRepStatementData)data.get(index)).getCustomerDepartment();
	  }else if("customerEntryDate".equals(fieldName)){
	         value = ((ArRepStatementData)data.get(index)).getCustomerEntryDate();
      }else if("customerName".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getCustomerName();
      }else if("customerAddress".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getCustomerAddress();
      }else if("customerContact".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getCustomerContact();
      }else if("customerTin".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCustomerTin();
      }else if("customerSquareMeter".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCustomerSquareMeter();
      }else if("customerParkingID".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCustomerParkingID();

      }else if("billingHeader".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBillingHeader();
      }else if("billingFooter".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBillingFooter();
      }else if("billingSignatory".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBillingSignatory();
      }else if("signatoryTitle".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getSignatoryTitle();
      }else if("date".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getDate();

      }else if("dueDate".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getDueDate();
      }else if("dueDate2".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getDueDate2();
      }else if("lastDueDate".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getLastDueDate();
      }else if("paymentTerm".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPaymentTerm();
      }else if("paymentTermName".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPaymentTermName();
      }else if("transaction".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getTransaction();
      }else if("transactionType".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getTransactionType();
      }else if("posted".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPosted();
      }else if("orNumber".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getOrNumber();
      }else if("invoiceNumber".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoiceNumber();
      }else if("lastOrDate".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getLastOrDate();
      }else if("invoiceAmountDue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoiceAmountDue();
      }else if("invoiceDownPayment".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoiceDownPayment();
      }else if("invoiceUnearnedInterest".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoiceUnearnedInterest();


      }else if("invoicePenaltyDue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInvoicePenaltyDue();

      }else if("number".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getNumber();
      }else if("amountDue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getAmountDue();
      }else if("ipsAmountDue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getIpsAmountDue();
      }else if("amountPaid".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getAmountPaid();
      }else if("interestDue".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInterestDue();

      }else if("amountDetails".equals(fieldName)) {
    	  value = ((ArRepStatementData)data.get(index)).getAmountDetails();
      }else if("debit".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getDebit();

      }else if("credit".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCredit();


      }else if("creditAmount".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getCreditAmount();

      }else if("batchName".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBatchName();


      }else if("description".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getDescription();
      }else if("itemDescription".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getItemDescription();

      }else if("previousReading".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPreviousReading();
      }else if("presentReading".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPresentReading();

      }else if("quantity".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getQuantity();
      }else if("unitPrice".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getUnitPrice();

      }else if("amount".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getAmount();

          System.out.println("amount="+value);
      }else if("penalty".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getPenalty();

      }else if("taxAmount".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getTaxAmount();
          System.out.println("taxAmount="+value);
      }else if("wtaxAmount".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getWtaxAmount();
      }else if("taxCode".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getTaxCode();

      }else if("remainingBalance".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getRemainingBalance();
      }else if("bucket0".equals(fieldName)) {
      	 value = ((ArRepStatementData)data.get(index)).getBucket0();
      }else if("bucket1".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getBucket1();
      }else if("bucket2".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getBucket2();
      }else if("bucket3".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getBucket3();
      }else if("bucket4".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getBucket4();
      }else if("bucket5".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getBucket5();

      }else if("bucketASD0".equals(fieldName)) {
       	 value = ((ArRepStatementData)data.get(index)).getBucketASD0();
       }else if("bucketASD1".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBucketASD1();
       }else if("bucketASD2".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBucketASD2();
       }else if("bucketASD3".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBucketASD3();
       }else if("bucketASD4".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBucketASD4();
       }else if("bucketASD5".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getBucketASD5();

       }else if("bucketRPT0".equals(fieldName)) {
        	 value = ((ArRepStatementData)data.get(index)).getBucketRPT0();
        }else if("bucketRPT1".equals(fieldName)){
           value = ((ArRepStatementData)data.get(index)).getBucketRPT1();
        }else if("bucketRPT2".equals(fieldName)){
           value = ((ArRepStatementData)data.get(index)).getBucketRPT2();
        }else if("bucketRPT3".equals(fieldName)){
           value = ((ArRepStatementData)data.get(index)).getBucketRPT3();
        }else if("bucketRPT4".equals(fieldName)){
           value = ((ArRepStatementData)data.get(index)).getBucketRPT4();
        }else if("bucketRPT5".equals(fieldName)){
           value = ((ArRepStatementData)data.get(index)).getBucketRPT5();

        }else if("bucketPD0".equals(fieldName)) {
         	 value = ((ArRepStatementData)data.get(index)).getBucketPD0();
         }else if("bucketPD1".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBucketPD1();
         }else if("bucketPD2".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBucketPD2();
         }else if("bucketPD3".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBucketPD3();
         }else if("bucketPD4".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBucketPD4();
         }else if("bucketPD5".equals(fieldName)){
            value = ((ArRepStatementData)data.get(index)).getBucketPD5();

         }else if("bucketWTR0".equals(fieldName)) {
          	 value = ((ArRepStatementData)data.get(index)).getBucketWTR0();
          }else if("bucketWTR1".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getBucketWTR1();
          }else if("bucketWTR2".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getBucketWTR2();
          }else if("bucketWTR3".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getBucketWTR3();
          }else if("bucketWTR4".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getBucketWTR4();
          }else if("bucketWTR5".equals(fieldName)){
             value = ((ArRepStatementData)data.get(index)).getBucketWTR5();

          }else if("bucketMISC0".equals(fieldName)) {
           	 value = ((ArRepStatementData)data.get(index)).getBucketMISC0();
           }else if("bucketMISC1".equals(fieldName)){
              value = ((ArRepStatementData)data.get(index)).getBucketMISC1();
           }else if("bucketMISC2".equals(fieldName)){
              value = ((ArRepStatementData)data.get(index)).getBucketMISC2();
           }else if("bucketMISC3".equals(fieldName)){
              value = ((ArRepStatementData)data.get(index)).getBucketMISC3();
           }else if("bucketMISC4".equals(fieldName)){
              value = ((ArRepStatementData)data.get(index)).getBucketMISC4();
           }else if("bucketMISC5".equals(fieldName)){
              value = ((ArRepStatementData)data.get(index)).getBucketMISC5();

           }else if("bucketADV0".equals(fieldName)) {
             	 value = ((ArRepStatementData)data.get(index)).getBucketADV0();
     }else if("bucketADV1".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBucketADV1();
     }else if("bucketADV2".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBucketADV2();
     }else if("bucketADV3".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBucketADV3();
     }else if("bucketADV4".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBucketADV4();
     }else if("bucketADV5".equals(fieldName)){
        value = ((ArRepStatementData)data.get(index)).getBucketADV5();

      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepStatementData)data.get(index)).getReferenceNumber();

      }else if("numberDays".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getNumberDays();
      }else if("exclusiveAdvance".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getExclusiveAdvance();
       }else if("inclusiveAdvance".equals(fieldName)){
          value = ((ArRepStatementData)data.get(index)).getInclusiveAdvance();
      }

      return(value);
   }
}
