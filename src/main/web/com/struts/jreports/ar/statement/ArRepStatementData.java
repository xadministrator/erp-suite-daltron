package com.struts.jreports.ar.statement;

import java.util.Date;

public class ArRepStatementData implements java.io.Serializable{

	//PMA
	private String pmProjectCode;
	private String pmProjectType;
	private String pmProjectName;
	private Double pmContractPrice;
	private String pmContractTermName;
	private Double pmContractTermvalue;


   private String customerCode;
   private String customerID;
   private String employeeID;
   private String accountNumber;
   private String customerBatch;
   private String customerDepartment;
   private String customerEntryDate;
   private String customerName;
   private String customerAddress;
   private String customerContact;
   private String customerTin;
   private Double customerSquareMeter;
   private String customerParkingID;
   private String billingHeader;
   private String billingFooter;
   private String billingSignatory;
   private String signatoryTitle;
   private String date;
   private String dueDate;
   private Date dueDate2;
   private String lastDueDate;
   private Double paymentTerm;
   private String paymentTermName;
   private String transaction;
   private String transactionType;
   private String posted;

   private String orNumber;
   private String invoiceNumber;
   private String lastOrDate;
   private Double invoiceAmountDue;
   private Double invoiceDownPayment;
   private Double invoiceUnearnedInterest;
   private Double invoicePenaltyDue;

   private Double number;
   private Double amountDue;
   private Double ipsAmountDue;
   private Double amountPaid;
   private Double interestDue;
   private Double amountDetails;
   private Double debit;
   private Double credit;
   private Double creditAmount;
   private String batchName;
   private String description;
   private String itemDescription;

   private Double previousReading;
   private Double presentReading;
   private Double quantity;
   private Double unitPrice;
   private Double amount;
   private Double penalty;
   private Double taxAmount;
   private Double wtaxAmount;
   private String taxCode;

   private Double remainingBalance;
   private Double bucket0 = null;
   private Double bucket1 = null;
   private Double bucket2 = null;
   private Double bucket3 = null;
   private Double bucket4 = null;
   private Double bucket5 = null;

   private Double bucketASD0 = 0d;
   private Double bucketASD1 = 0d;
   private Double bucketASD2 = 0d;
   private Double bucketASD3 = 0d;
   private Double bucketASD4 = 0d;
   private Double bucketASD5 = 0d;

   private Double bucketRPT0 = null;
   private Double bucketRPT1 = null;
   private Double bucketRPT2 = null;
   private Double bucketRPT3 = null;
   private Double bucketRPT4 = null;
   private Double bucketRPT5 = null;

   private Double bucketPD0 = null;
   private Double bucketPD1 = null;
   private Double bucketPD2 = null;
   private Double bucketPD3 = null;
   private Double bucketPD4 = null;
   private Double bucketPD5 = null;

   private Double bucketWTR0 = null;
   private Double bucketWTR1 = null;
   private Double bucketWTR2 = null;
   private Double bucketWTR3 = null;
   private Double bucketWTR4 = null;
   private Double bucketWTR5 = null;

   private Double bucketMISC0 = null;
   private Double bucketMISC1 = null;
   private Double bucketMISC2 = null;
   private Double bucketMISC3 = null;
   private Double bucketMISC4 = null;
   private Double bucketMISC5 = null;

   private Double bucketADV0 = null;
   private Double bucketADV1 = null;
   private Double bucketADV2 = null;
   private Double bucketADV3 = null;
   private Double bucketADV4 = null;
   private Double bucketADV5 = null;

   private String referenceNumber;
   private String invoiceBatchName;
   private String numberDays;
   private Double exclusiveAdvance = null;
   private Double inclusiveAdvance = null;

   public ArRepStatementData(

		   String pmProjectCode,
		   String pmProjectType,
		   String pmProjectName,
		   Double pmContractPrice,
		   String pmContractTermName,
		   Double pmContractTermvalue,



		   String customerCode, String customerID, String employeeID, String accountNumber, String customerBatch, String customerDepartment, String customerEntryDate, String customerName,
       String customerAddress, String customerContact, String customerTin, Double customerSquareMeter, String customerParkingID , String billingHeader,
       String billingFooter, String billingSignatory, String signatoryTitle,
	   String date, String dueDate, Date dueDate2, String lastDueDate,
	   Double paymentTerm, String paymentTermName,
	   String transaction, String transactionType,
	   String posted,
	   String orNumber,
	   String invoiceNumber,
	   String lastOrDate,
	   Double invoiceAmountDue,
	   Double invoiceDownPayment,
	   Double invoiceUnearnedInterest,
	   Double invoicePenaltyDue,
	   Double number,
	   Double amountDue,
	   Double ipsAmountDue,
	   Double amountPaid,
	   Double interestDue,
	   Double amountDetails,
	   Double debit,
	   Double credit,

	   Double creditAmount,
	   String batchName, String description, String itemDescription,
	   Double previousReading,
	   Double presentReading,
	   Double quantity,
	   Double unitPrice,
	   Double amount,
	   Double penalty,
	   Double taxAmount,
	   Double wtaxAmount,
	   String taxCode,
	   Double remainingBalance,
	   Double bucket0,
	   Double bucket1,
	   Double bucket2,
	   Double bucket3,
	   Double bucket4,
	   Double bucket5,

	   Double bucketASD0,
	   Double bucketASD1,
	   Double bucketASD2,
	   Double bucketASD3,
	   Double bucketASD4,
	   Double bucketASD5,

	   Double bucketRPT0,
	   Double bucketRPT1,
	   Double bucketRPT2,
	   Double bucketRPT3,
	   Double bucketRPT4,
	   Double bucketRPT5,

	   Double bucketPD0,
	   Double bucketPD1,
	   Double bucketPD2,
	   Double bucketPD3,
	   Double bucketPD4,
	   Double bucketPD5,

	   Double bucketWTR0,
	   Double bucketWTR1,
	   Double bucketWTR2,
	   Double bucketWTR3,
	   Double bucketWTR4,
	   Double bucketWTR5,

	   Double bucketMISC0,
	   Double bucketMISC1,
	   Double bucketMISC2,
	   Double bucketMISC3,
	   Double bucketMISC4,
	   Double bucketMISC5,

	   Double bucketADV0,
	   Double bucketADV1,
	   Double bucketADV2,
	   Double bucketADV3,
	   Double bucketADV4,
	   Double bucketADV5,

	   String referenceNumber, String invoiceBatchName,
	   String numberDays,
	   Double exclusiveAdvance,
	   Double inclusiveAdvance
		   ){

	   this.pmProjectCode = pmProjectCode;
	   this.pmProjectType = pmProjectType;
	   this.pmProjectName = pmProjectName;
	   this.pmContractPrice = pmContractPrice;
	   this.pmContractTermName = pmContractTermName;
	   this.pmContractTermvalue = pmContractTermvalue;

       this.customerCode = customerCode;
       this.customerID = customerID;
       this.employeeID = employeeID;
       this.accountNumber = accountNumber;

       this.customerBatch = customerBatch;
       this.customerDepartment = customerDepartment;
       this.customerEntryDate = customerEntryDate;
       this.customerName = customerName;
       this.customerAddress = customerAddress;
       this.customerContact = customerContact;
       this.customerTin = customerTin;
       this.customerSquareMeter = customerSquareMeter;
       this.customerParkingID = customerParkingID;
       this.billingHeader = billingHeader;
       this.billingFooter = billingFooter;
       this.billingSignatory = billingSignatory;
       this.signatoryTitle = signatoryTitle;


       this.date = date;
       this.dueDate = dueDate;
       this.dueDate2 = dueDate2;
       this.lastDueDate = lastDueDate;
       this.paymentTerm = paymentTerm;
       this.paymentTermName = paymentTermName;
       this.transaction = transaction;
       this.transactionType = transactionType;
       this.posted = posted;
       this.orNumber = orNumber;
       this.invoiceNumber = invoiceNumber;
       this.lastOrDate = lastOrDate;
       this.invoiceAmountDue = invoiceAmountDue;
       this.invoiceDownPayment= invoiceDownPayment;
       this.invoiceUnearnedInterest= invoiceUnearnedInterest;

       this.invoicePenaltyDue = invoicePenaltyDue;
       this.number = number;
       this.amountDue = amountDue;
       this.ipsAmountDue = ipsAmountDue;
       this.amountPaid = amountPaid;
       this.interestDue = interestDue;
       this.amountDetails = amountDetails;
       this.debit = debit;
       this.credit = credit;

       this.creditAmount = creditAmount;
       this.batchName = batchName;
       this.description = description;
       this.itemDescription = itemDescription;

       this.previousReading = previousReading;
       this.presentReading = presentReading;

       this.quantity = quantity;
       this.unitPrice = unitPrice;
       this.amount = amount;
       this.penalty = penalty;
       this.taxAmount = taxAmount;
       this.wtaxAmount = wtaxAmount;
       this.taxCode = taxCode;

       this.remainingBalance = remainingBalance;
       this.bucket0 = bucket0;
       this.bucket1 = bucket1;
       this.bucket2 = bucket2;
       this.bucket3 = bucket3;
       this.bucket4 = bucket4;
       this.bucket5 = bucket5;

       this.bucketASD0 = bucketASD0;
       this.bucketASD1 = bucketASD1;
       this.bucketASD2 = bucketASD2;
       this.bucketASD3 = bucketASD3;
       this.bucketASD4 = bucketASD4;
       this.bucketASD5 = bucketASD5;

       this.bucketRPT0 = bucketRPT0;
       this.bucketRPT1 = bucketRPT1;
       this.bucketRPT2 = bucketRPT2;
       this.bucketRPT3 = bucketRPT3;
       this.bucketRPT4 = bucketRPT4;
       this.bucketRPT5 = bucketRPT5;

       this.bucketPD0 = bucketPD0;
       this.bucketPD1 = bucketPD1;
       this.bucketPD2 = bucketPD2;
       this.bucketPD3 = bucketPD3;
       this.bucketPD4 = bucketPD4;
       this.bucketPD5 = bucketPD5;

       this.bucketWTR0 = bucketWTR0;
       this.bucketWTR1 = bucketWTR1;
       this.bucketWTR2 = bucketWTR2;
       this.bucketWTR3 = bucketWTR3;
       this.bucketWTR4 = bucketWTR4;
       this.bucketWTR5 = bucketWTR5;

       this.bucketMISC0 = bucketMISC0;
       this.bucketMISC1 = bucketMISC1;
       this.bucketMISC2 = bucketMISC2;
       this.bucketMISC3 = bucketMISC3;
       this.bucketMISC4 = bucketMISC4;
       this.bucketMISC5 = bucketMISC5;

       this.bucketADV0 = bucketADV0;
       this.bucketADV1 = bucketADV1;
       this.bucketADV2 = bucketADV2;
       this.bucketADV3 = bucketADV3;
       this.bucketADV4 = bucketADV4;
       this.bucketADV5 = bucketADV5;

       this.referenceNumber = referenceNumber;
       this.invoiceBatchName = invoiceBatchName;
       this.numberDays = numberDays;
       this.exclusiveAdvance = exclusiveAdvance;
       this.inclusiveAdvance = inclusiveAdvance;

   }


   public String getPmProjectCode() {

   	   return pmProjectCode;

   }

   public String getPmProjectType() {

   	   return pmProjectType;

   }

   public String getPmProjectName() {

   	   return pmProjectName;

   }

   public Double getPmContractPrice() {

   	   return pmContractPrice;

   }

   public String getPmContractTermName() {

   	   return pmContractTermName;

   }

   public Double getPmContractTermValue() {

   	   return pmContractTermvalue;

   }




   public String getCustomerCode() {

   	   return customerCode;

   }

   public String getCustomerID() {

   	   return customerID;

   }


   public String getEmployeeID() {

   	   return employeeID;

   }

   public String getAccountNumber() {

   	   return accountNumber;

   }

   public String getCustomerBatch() {

   	   return customerBatch;

   }

   public String getCustomerDepartment() {

   	   return customerDepartment;

   }

   public String getCustomerName() {

   	   return customerName;

   }

   public String getCustomerEntryDate() {

   	   return customerEntryDate;

   }

   public String getCustomerAddress() {

   	   return customerAddress;

   }

   public String getCustomerContact() {

   	   return customerContact;

   }

   public String getCustomerTin() {

   	   return customerTin;

   }

   public Double getCustomerSquareMeter() {

   	   return customerSquareMeter;

   }

   public String getCustomerParkingID() {

   	   return customerParkingID;

   }

   public String getDate() {

   	   return date;

   }

   public String getDueDate() {

   	   return dueDate;

   }

   public Date getDueDate2() {

   	   return dueDate2;

   }

   public String getLastDueDate() {

   	   return lastDueDate;

   }

   public Double getPaymentTerm() {

   	   return paymentTerm;

   }

   public String getPaymentTermName() {

   	   return paymentTermName;

   }

   public String getTransaction() {

   	   return transaction;

   }

public String getTransactionType() {

   	   return transactionType;

   }

public String getPosted() {

   	   return posted;

   }
   public String getOrNumber() {

   	   return orNumber;

   }

  public String getInvoiceNumber() {

   	   return invoiceNumber;

   }

public String getLastOrDate() {

   	   return lastOrDate;

   }

public Double getNumber() {

   	   return number;

   }


public Double getInvoiceAmountDue() {

   	   return invoiceAmountDue;

   }



public Double getInvoiceDownPayment() {

	   return invoiceDownPayment;

}

public Double getInvoiceUnearnedInterest() {

	   return invoiceUnearnedInterest;

}



public Double getInvoicePenaltyDue() {

	   return invoicePenaltyDue;

}


   public Double getAmountDue() {

   	   return amountDue;

   }

public Double getIpsAmountDue() {

   	   return ipsAmountDue;

   }

   public Double getAmountPaid() {

   	   return amountPaid;

   }

public Double getInterestDue() {

   	   return interestDue;

   }

public Double getAmountDetails() {

	   return amountDetails;

}

	public Double getDebit() {

	   	   return debit;

	   }

	public Double getCredit() {

		   return credit;

	}



public Double getCreditAmount() {

   	   return creditAmount;

   }

public String getBatchName() {

   	   return batchName;

   }

   public String getDescription() {

   	   return description;

   }

   public String getItemDescription() {

   	   return itemDescription;

   }

   public Double getPreviousReading() {

   	   return previousReading;

   }

   public Double getPresentReading() {

   	   return presentReading;

   }

   public Double getQuantity() {

   	   return quantity;

   }

   public Double getUnitPrice() {

   	   return unitPrice;

   }


   public Double getAmount() {

   	   return amount;

   }

   public Double getPenalty() {

   	   return penalty;

   }

   public Double getTaxAmount() {

   	   return taxAmount;

   }

   public Double getWtaxAmount() {

   	   return wtaxAmount;

   }

   public String getTaxCode() {

   	   return taxCode;

   }


   public Double getRemainingBalance() {

	   	  return remainingBalance;

	   }

   public Double getBucket0() {

   	  return bucket0;

   }

   public Double getBucket1() {

   	  return bucket1;

   }

   public Double getBucket2() {

   	  return bucket2;

   }

   public Double getBucket3() {

   	  return bucket3;

   }

   public Double getBucket4() {

   	  return bucket4;

   }

   public Double getBucket5() {

   	  return bucket5;

   }

   public Double getBucketASD0() {

	   	  return bucketASD0;

	   }

	   public Double getBucketASD1() {

	   	  return bucketASD1;

	   }

	   public Double getBucketASD2() {

	   	  return bucketASD2;

	   }

	   public Double getBucketASD3() {

	   	  return bucketASD3;

	   }

	   public Double getBucketASD4() {

	   	  return bucketASD4;

	   }

	   public Double getBucketASD5() {

	   	  return bucketASD5;

	   }


   public Double getBucketRPT0() {

   	  return bucketRPT0;

   }

   public Double getBucketRPT1() {

   	  return bucketRPT1;

   }

   public Double getBucketRPT2() {

   	  return bucketRPT2;

   }

   public Double getBucketRPT3() {

   	  return bucketRPT3;

   }

   public Double getBucketRPT4() {

   	  return bucketRPT4;

   }

   public Double getBucketRPT5() {

   	  return bucketRPT5;

   }

   public Double getBucketPD0() {

   	  return bucketPD0;

   }

   public Double getBucketPD1() {

   	  return bucketPD1;

   }

   public Double getBucketPD2() {

   	  return bucketPD2;

   }

   public Double getBucketPD3() {

   	  return bucketPD3;

   }

   public Double getBucketPD4() {

   	  return bucketPD4;

   }

   public Double getBucketPD5() {

   	  return bucketPD5;

   }

   public Double getBucketWTR0() {

   	  return bucketWTR0;

	   }

	   public Double getBucketWTR1() {

	   	  return bucketWTR1;

	   }

	   public Double getBucketWTR2() {

	   	  return bucketWTR2;

	   }

	   public Double getBucketWTR3() {

	   	  return bucketWTR3;

	   }

	   public Double getBucketWTR4() {

	   	  return bucketWTR4;

	   }

	   public Double getBucketWTR5() {

	   	  return bucketWTR5;

	   }



	   public Double getBucketMISC0() {

	   	  return bucketMISC0;

		   }

		   public Double getBucketMISC1() {

		   	  return bucketMISC1;

		   }

		   public Double getBucketMISC2() {

		   	  return bucketMISC2;

		   }

		   public Double getBucketMISC3() {

		   	  return bucketMISC3;

		   }

		   public Double getBucketMISC4() {

		   	  return bucketMISC4;

		   }

		   public Double getBucketMISC5() {

		   	  return bucketMISC5;

		   }

		   public Double getBucketADV0() {

		   	  return bucketADV0;

		   }

		   public Double getBucketADV1() {

		   	  return bucketADV1;

		   }

		   public Double getBucketADV2() {

		   	  return bucketADV2;

		   }

		   public Double getBucketADV3() {

		   	  return bucketADV3;

		   }

		   public Double getBucketADV4() {

		   	  return bucketADV4;

		   }

		   public Double getBucketADV5() {

		   	  return bucketADV5;

		   }

   public String getBillingHeader() {

   	   return billingHeader;

   }

   public String getBillingFooter() {

   	   return billingFooter;

   }

   public String getBillingSignatory() {

   	   return billingSignatory;

   }

   public String getSignatoryTitle() {

   	   return signatoryTitle;

   }

   public String getReferenceNumber() {

	   return referenceNumber;

   }

   public String getInvoiceBatchName(){

	   return invoiceBatchName;

   }

   public String getNumberDays(){

	   return numberDays;

   }

   public Double getExclusiveAdvance(){

	   return exclusiveAdvance;

   }

   public Double getInclusiveAdvance(){

	   return inclusiveAdvance;

   }

}
