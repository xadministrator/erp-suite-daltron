package com.struts.jreports.ar.statement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArRepStatementForm extends ActionForm implements Serializable{

   private String customerCode = null;
   private String customerCodeMail = null;
   private ArrayList customerCodeList = new ArrayList();
   private String customerType = null;
   private ArrayList customerTypeList = new ArrayList();

   private ArrayList customerBatchList = new ArrayList();
   private String[] customerBatchSelectedList = new String[0];
   private ArrayList customerDepartmentList = new ArrayList();
   private String[] customerDepartmentSelectedList = new String[0];
   
   private String[] customerBatchSelectedListExport = new String[0];
   private String[] customerBatchSelectedListMail = new String[0];
   
   
   private String customerClass = null;
   private ArrayList customerClassList = new ArrayList();
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String reportType = null;
   private ArrayList reportTypeList = new ArrayList();
   
   private String uploadInvoiceType = null;
   private ArrayList uploadInvoiceTypeList = new ArrayList();
   
   private String memoLine = null;
   private ArrayList memoLineList = new ArrayList();
   
   private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
   
   private String csvFilename = null;
   private String emailSubject = null;
   private String emailMessage = null;
   private boolean includeNoEmail = false;
   private String customerMessage = null;
   private String collectionHead = null;
   private String report = null;
   private boolean useCustomerPulldown = true;
   private boolean includeCollections = false;
   private boolean waterOnly = false;
   private Date firstDayOfPeriod = null;
   private boolean includeAdvance = false;
   private boolean includeAdvanceOnly = false;
   private boolean includeUnpostedTransaction = false;
   private boolean sendByEmail = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String invoiceNumberFrom = null;
   private String invoiceNumberTo = null;
   private boolean showAll = false;
   private boolean includeZero = false;
   private boolean includeUnpostedInvoice = false;
   private boolean showEntries = false;
   private boolean showUnearnedInterestTransactionOnly = false;
   private boolean sortByTransaction = false;
   private boolean sortByItem = false;
   private boolean includeCreditMemo = false;
   
   
 
   
   
   private String invoiceBatchName = null;
   private ArrayList invoiceBatchNameList = new ArrayList();
   
   private String soaDateExport = null;
   private String soaDateMail = null;
   
   private ArrayList arRepBrStList = new ArrayList();

   private String userPermission = new String();

   public String getInvoiceBatchName() {
	   	
   	   return invoiceBatchName;
   	
   }
   public ArrayList getInvoiceBatchNameList() {
	   	
   	   return invoiceBatchNameList;
   	
   }
   
   public void setInvoiceBatchNameList(String invoiceBatchName) {
   	
	   invoiceBatchNameList.add(invoiceBatchName);
   	
   }
   
   public void setInvoiceBatchName(String invoiceBatchName) {
   	
   	   this.invoiceBatchName = invoiceBatchName;
   }
   

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   public String[] getCustomerBatchSelectedList() {

			return customerBatchSelectedList;

		}

	public void setCustomerBatchSelectedList(String[] customerBatchSelectedList) {

		this.customerBatchSelectedList = customerBatchSelectedList;

	}
	
	
	
	
	
	
	
	
	public ArrayList getCustomerDepartmentList() {
	   	
	      return customerDepartmentList;
	      
	   }

	   public void setCustomerDepartmentList(String customerDepartment) {
	   	
	      customerDepartmentList.add(customerDepartment);
	      
	   }
	   
	   public void clearCustomerDepartmentList() {
	   	
		   customerDepartmentList.clear();
		   customerDepartmentList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   public String[] getCustomerDepartmentSelectedList() {

			return customerDepartmentSelectedList;

		}

	public void setCustomerDepartmentSelectedList(String[] customerDepartmentSelectedList) {

		this.customerDepartmentSelectedList = customerDepartmentSelectedList;

	}
	
	
	public String[] getCustomerBatchSelectedListExport() {

		return customerBatchSelectedListExport;

	}

	public void setCustomerBatchSelectedListExport(String[] customerBatchSelectedListExport) {

		this.customerBatchSelectedListExport = customerBatchSelectedListExport;

	}
	
	public String[] getCustomerBatchSelectedListMail() {

		return customerBatchSelectedListMail;

	}

	public void setCustomerBatchSelectedListMail(String[] customerBatchSelectedListMail) {

		this.customerBatchSelectedListMail = customerBatchSelectedListMail;

	}
	   
	   
   
   public String getCustomerCode() {
   	
   	  return customerCode;
   	
   }
   
   public void setCustomerCode(String customerCode) {
   	
   	  this.customerCode = customerCode;
   	
   }
   
   public String getCustomerCodeMail() {
	   	
	   	  return customerCodeMail;
	   	
	   }
	   
   public void setCustomerCodeMail(String customerCodeMail) {
	   	
	   this.customerCodeMail = customerCodeMail;
	   	
	}
   
   public ArrayList getCustomerCodeList() {
   	
   	   return customerCodeList;
   	
   }
   
   public void setCustomerCodeList(String customerCode) {
   	
   	   customerCodeList.add(customerCode);
   	
   }
   
   public void clearCustomerCodeList() {
   	
   	   customerCodeList.clear();
   	   customerCodeList.add(Constants.GLOBAL_BLANK);
   	
   }   
      
   public String getCustomerType() {
   	
      return customerType;
      
   }

   public void setCustomerType(String customerType) {
   	
      this.customerType = customerType;
      
   }

   public ArrayList getCustomerTypeList() {
   	
      return customerTypeList;
      
   }

   public void setCustomerTypeList(String customerType) {
   	
      customerTypeList.add(customerType);
      
   }

   public void clearInvoiceBatchNameList(){
	   invoiceBatchNameList.clear();
	   invoiceBatchNameList.add(Constants.GLOBAL_BLANK);
	   
   }
   
   public void clearCustomerTypeList() {
   	
      customerTypeList.clear();
      customerTypeList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getCustomerClass() {
   	
      return customerClass;
      
   }

   public void setCustomerClass(String customerClass) {
   	
      this.customerClass = customerClass;
      
   }

   public ArrayList getCustomerClassList() {
   	
      return customerClassList;
      
   }

   public void setCustomerClassList(String customerClass) {
   	
      customerClassList.add(customerClass);
      
   }

   public void clearCustomerClassList() {
   	
      customerClassList.clear();
      customerClassList.add(Constants.GLOBAL_BLANK);
      
   }
   
  
   public String getGroupBy() {

		return groupBy;

	}

	public void setGroupBy(String groupBy) {

		this.groupBy = groupBy;

	}

	public ArrayList getGroupByList() {

		return groupByList;

	}

	public void setGroupByList(ArrayList groupByList) {

		this.groupByList = groupByList;

	}

	public String getOrderBy() {

		return orderBy;

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}

	public void setOrderByList(ArrayList orderByList) {

		this.orderByList = orderByList;

	}
   
   
   public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	
	
	
	
	
	
	
	
	 public String getUploadInvoiceType() {

			return(uploadInvoiceType);

		}

		public void setUploadInvoiceType(String uploadInvoiceType) {

			this.uploadInvoiceType = uploadInvoiceType;

		}

		public ArrayList getUploadInvoiceTypeList() {

			return(uploadInvoiceTypeList);

		}

		public void setUploadInvoiceTypeList(String uploadInvoiceType) {

			uploadInvoiceTypeList.add(uploadInvoiceType);

		}

		public void clearUploadInvoiceType() {

			uploadInvoiceTypeList.clear();
			uploadInvoiceTypeList.add(Constants.GLOBAL_BLANK);

		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	public String getMemoLine() {

		return(memoLine);

	}

	public void setMemoLine(String memoLine) {

		this.memoLine = memoLine;

	}

	public ArrayList getMemoLineList() {

		return(memoLineList);

	}

	public void setMemoLineList(String memoLine) {

		memoLineList.add(memoLine);

	}

	public void clearMemoLineList() {

		memoLineList.clear();
		memoLineList.add(Constants.GLOBAL_BLANK);

	}
	
	
	
	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
   
   public String getCustomerMessage() {
   	
   	  return customerMessage;
   	
   }
   
   public void setCustomerMessage(String customerMessage) {
   	
   	  this.customerMessage = customerMessage;
   	
   }
   
   public String getCollectionHead() {
   	
   	  return collectionHead;
   	  
   }
   
   public void setCollectionHead(String collectionHead) {
   	
   	  this.collectionHead = collectionHead;
   	  
   }
         
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   } 
   
   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public boolean getUseCustomerPulldown() {
   	
   	  return useCustomerPulldown;
   	  
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	 this.useCustomerPulldown = useCustomerPulldown;
   	 
   }
   
   public Object[] getArRepBrStList(){
   	
   	  return arRepBrStList.toArray();
   	
   }
   
   public ArRepBranchStatementList getArRepBrStListByIndex(int index){
   	
   	  return ((ArRepBranchStatementList)arRepBrStList.get(index));
   	
   }
   
   public int getArRepBrStListSize(){
   	
   	  return(arRepBrStList.size());
   	
   }
   
   public void saveArRepBrStList(Object newArRepBrStList){
   	
   	  arRepBrStList.add(newArRepBrStList);   	  
   	
   }
   
   public void clearArRepBrStList(){
   	
   	  arRepBrStList.clear();
   	
   }
   
   public boolean getIncludeCollections() {
	   
	   return includeCollections;
	   
   }
   
   public void setIncludeCollections(boolean includeCollections) {
	   
	   this.includeCollections = includeCollections;
	   
   }
   
   public boolean getWaterOnly() {
	   
	   return waterOnly;
	   
   }
   
   public void setWaterOnly(boolean waterOnly) {
	   
	   this.waterOnly = waterOnly;
	   
   }
   
   
   public void setFirstDayOfPeriod(Date firstDayOfPeriod) {
	   
	   this.firstDayOfPeriod = firstDayOfPeriod;
	   
   }
   
   public Date getFirstDayOfPeriod() {
	   
	   return firstDayOfPeriod;
	   
   }
   
   
   public boolean getIncludeAdvance() {
	   
	   return includeAdvance;
	   
   }
   
   public void setIncludeAdvance(boolean includeAdvance) {
	   
	   this.includeAdvance = includeAdvance;
	   
   }
   
public boolean getIncludeAdvanceOnly() {
	   
	   return includeAdvanceOnly;
	   
   }
   
   public void setIncludeAdvanceOnly(boolean includeAdvanceOnly) {
	   
	   this.includeAdvanceOnly = includeAdvanceOnly;
	   
   }
   
public boolean getIncludeUnpostedTransaction() {
	   
	   return includeUnpostedTransaction;
	   
   }
   
   public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
	   
	   this.includeUnpostedTransaction = includeUnpostedTransaction;
	   
   }
   
   public boolean getIncludeUnpostedInvoice() {
	   
	   return includeUnpostedInvoice;
	   
   }
   
   
public void setShowEntries(boolean showEntries) {
	   
	   this.showEntries = showEntries;
	   
   }
   
   public boolean getShowEntries() {
	   
	   return showEntries;
	   
   }
   
   
public void setShowUnearnedInterestTransactionOnly(boolean showUnearnedInterestTransactionOnly) {
	   
	   this.showUnearnedInterestTransactionOnly = showUnearnedInterestTransactionOnly;
	   
   }
   
   public boolean getShowUnearnedInterestTransactionOnly() {
	   
	   return showUnearnedInterestTransactionOnly;
	   
   }
   
   
   public boolean getSendByEmail(){
	   return sendByEmail;
   }
   
   public void setSendByEmail(boolean sendByEmail){
	   this.sendByEmail = sendByEmail;
   }
   
   public void setIncludeUnpostedInvoice(boolean includeUnpostedInvoice) {
	   
	   this.includeUnpostedInvoice = includeUnpostedInvoice;
	   
   }
   
   public boolean getSortByTransaction() {
	   
	   return sortByTransaction;
	   
   }
   
   public void setSortByTransaction(boolean sortByTransaction) {
	   
	   this.sortByTransaction = sortByTransaction;
	   
   }
   
   
   
   public boolean getSortByItem() {
	   
	   return sortByItem;
	   
   }
   
   public void setSortByItem(boolean sortByItem) {
	   
	   this.sortByItem = sortByItem;
	   
   }
   
  public boolean getIncludeCreditMemo() {
	   
	   return includeCreditMemo;
	   
   }
   
   public void setIncludeCreditMemo(boolean includeCreditMemo) {
	   
	   this.includeCreditMemo = includeCreditMemo;
	   
   }
   
   public String getDateFrom() {
	   
	   return dateFrom;
	   
   }
   
   public void setDateFrom(String dateFrom) {
	   
	   this.dateFrom = dateFrom;
	   
   }
   
   public String getDateTo() {

	   return dateTo;

   }

   public void setDateTo(String dateTo) {

	   this.dateTo = dateTo;

   }
   
   
   public void setSoaDateExport(String soaDateExport){
	   
	   this.soaDateExport = soaDateExport;
   }
   
   public String getSoaDateExport(){
	   
	   return soaDateExport;	
	}
   

   
   
  public void setSoaDateMail(String soaDateMail){
	   
	   this.soaDateMail = soaDateMail;
   }
   
   public String getSoaDateMail(){
	   
	   return soaDateMail;	
	}

   public String getInvoiceNumberFrom() {

	   return invoiceNumberFrom;

   }

   public void setInvoiceNumberFrom(String invoiceNumberFrom) {

	   this.invoiceNumberFrom = invoiceNumberFrom;

   }

   public String getInvoiceNumberTo() {

	   return invoiceNumberTo;

   }

   public void setInvoiceNumberTo(String invoiceNumberTo) {

	   this.invoiceNumberTo = invoiceNumberTo;

   }
   
   public boolean getShowAll() {
	   
	   return showAll;
	   
   }
   
   public void setShowAll(boolean showAll) {
	   
	   this.showAll = showAll;
	   
   }
   
   public boolean getIncludeZero() {
	   
	   return includeZero;
	   
   }
   
   public void setIncludeZero(boolean includeZero) {
	   
	   this.includeZero = includeZero;
	   
   }
   
   
  
	public String getCsvFilename(){
		return csvFilename;
	
	}
	
	public void setCsvFilename(String csvFilename){
		this.csvFilename = csvFilename;
	}
	
	
	public String getEmailSubject(){
		return emailSubject;
	
	}
	
	public void setEmailSubject(String emailSubject){
		this.emailSubject = emailSubject;
	}
	
	public String getEmailMessage(){
		return emailMessage;
	
	}
	
	public void setEmailMessage(String emailMessage){
		this.emailMessage = emailMessage;
	}
   
   
   public boolean getIncludeNoEmail(){
	   
   	return includeNoEmail;
   }	
           
   public void setIncludeNoEmail(boolean includeNoEmail){
	   
	   this.includeNoEmail = includeNoEmail;
   }
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {      
   	
   	  for (int i=0; i<arRepBrStList.size(); i++) {
	  	
   	  	  ArRepBranchStatementList actionList = (ArRepBranchStatementList)arRepBrStList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
       
      customerCode = Constants.GLOBAL_BLANK;
      customerType = Constants.GLOBAL_BLANK;
      customerClass = Constants.GLOBAL_BLANK;
      viewTypeList.clear();
	  viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
	  viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
	  
	  customerBatchSelectedList = new String[0];
	  customerDepartmentSelectedList = new String[0];
		
	  viewType = Constants.REPORT_VIEW_TYPE_EXCEL;
      customerMessage = null;
      collectionHead = null;
      includeCollections = false;
      waterOnly = false;
      firstDayOfPeriod = null;
      includeAdvance = false;
      includeAdvanceOnly = false;
      includeUnpostedTransaction= false;
      includeUnpostedInvoice = false;
      showEntries = false;
      showUnearnedInterestTransactionOnly = false;
      sendByEmail= false;
      sortByTransaction = false;
      dateFrom = null;
      dateTo = null;
      invoiceNumberFrom = null;
      invoiceNumberTo = null;
      showAll = false;
      includeZero = false;
      invoiceBatchName = Constants.GLOBAL_BLANK;
      soaDateExport = null;
      soaDateMail = null;
      includeNoEmail = false;
      includeCreditMemo = false;
      viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
      
      
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CODE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_NAME);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_TYPE);
		groupByList.add(Constants.AR_CL_ORDER_BY_CUSTOMER_CLASS);
		groupBy = Constants.GLOBAL_BLANK;
		
		
		uploadInvoiceTypeList.clear();
		uploadInvoiceTypeList.add(Constants.GLOBAL_BLANK);
		uploadInvoiceTypeList.add("WooCommerce 1");
		uploadInvoiceType = Constants.GLOBAL_BLANK;

		orderByList.clear();  
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add("TRANSACTION");
		orderByList.add("BATCH_NAME");
		orderByList.add("DUE DATE");
	
		
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_DATE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_CUSTOMER_CODE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_CUSTOMER_NAME);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_CUSTOMER_TYPE);
		orderByList.add(Constants.AR_SALES_REGISTER_ORDER_BY_INVOICE_NUMBER);
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null) {
      	
    	 /* if(Common.validateRequired(customerCode)) {
           	
  		    errors.add("customerCode", new ActionMessage("statement.error.customerCodeRequired"));
  		    
  		 }*/
    	  
    	  if(Common.validateRequired(dateTo)) {
           	
  		    errors.add("dateTo", new ActionMessage("statement.error.dateToRequired"));
  		    
  		 }
  		 
  		 if(!Common.validateDateFormat(dateTo)) {
           	
  		    errors.add("dateTo", new ActionMessage("statement.error.dateToInvalid"));
  		    
  		 }
		 
		 if(!Common.validateDateFormat(dateFrom)) {

			 errors.add("dateFrom", new ActionMessage("statement.error.dateFromInvalid"));

		 }
		 
		 /*if(includeCollections == true) {
			 
			 if(Common.validateRequired(dateFrom)) {

				 errors.add("dateFrom", new ActionMessage("statement.error.dateFromRequired"));

			 }

			 
			 
		 }*/
		 	 
      }
      
      if (request.getParameter("exportButton") != null) {
    	  
    	  	
			
			if(!Common.validateDateFormat(soaDateExport)){
				errors.add("soaDateExport", new ActionMessage("statement.error.dateFormatInvalid"));
				
			}
      }
      
      
      if (request.getParameter("emailButton") != null){
    	 
    	  
    	  if(Common.validateRequired(csvFilename)) {
             	
    		    errors.add("dateTo", new ActionMessage("statement.error.csvFileNameRequired"));
    		    	
    	  }
    	  
    	 
    	  
    	  if(!Common.validateDateFormat(soaDateExport)){
				errors.add("soaDateMail", new ActionMessage("statement.error.dateFormatInvalid"));
				
    	  }
    
      }
      
      return errors;
      
   }
}
