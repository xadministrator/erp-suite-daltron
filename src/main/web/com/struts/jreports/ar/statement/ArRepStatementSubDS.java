package com.struts.jreports.ar.statement;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepStatementDetails;

public class ArRepStatementSubDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepStatementSubDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepStatementDetails details = (ArRepStatementDetails)i.next();
         
         String group = details.getSmtTransaction() + " - " + Common.convertSQLDateToString(details.getSmtDueDate());
                                    
	     ArRepStatementSubData smtSubData = new ArRepStatementSubData(
	         Common.convertSQLDateToString(details.getSmtDate()), 
	         Common.convertSQLDateToString(details.getSmtDueDate()), details.getSmtTransaction(),
	         new Double(details.getSmtAmount()), details.getSmtReceiptNumber(),
	         Common.convertSQLDateToString(details.getSmtReceiptDate()),
	         new Double(details.getSmtReceiptApplyAmount()), group, details.getSmtReferenceNumber(),
	         details.getSmtCreditMemoNumber(), Common.convertSQLDateToString(details.getSmtCreditMemoDate()),
	         new Double(details.getSmtCreditMemoAmount()));
		    
         data.add(smtSubData);
      }     
      
   }

   public boolean next() throws JRException{
	   index++;
	   return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

	  if("date".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getDate();
      }else if("dueDate".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getDueDate();
      }else if("transaction".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getTransaction();
      }else if("amount".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getAmount();
      }else if("receiptNumber".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getReceiptNumber();
      }else if("receiptDate".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getReceiptDate();
      }else if("receiptApplyAmount".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getReceiptApplyAmount();
      }else if("group".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getGroup();
      }else if("referenceNumber".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getReferenceNumber();
      }else if("creditMemoNumber".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getCreditMemoNumber();
      }else if("creditMemoDate".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getCreditMemoDate();
      }else if("creditMemoAmount".equals(fieldName)){
         value = ((ArRepStatementSubData)data.get(index)).getCreditMemoAmount();
      }
      return(value);
   }
}
