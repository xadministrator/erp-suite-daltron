package com.struts.jreports.ar.statement;

public class ArRepStatementSubData implements java.io.Serializable{
   
   private String date;
   private String dueDate;
   private String transaction;
   private Double amount;
   private String receiptNumber;
   private String receiptDate;
   private Double receiptApplyAmount;
   private String group;
   private String referenceNumber;
   private String creditMemoNumber;
   private String creditMemoDate;
   private Double creditMemoAmount;

   public ArRepStatementSubData(String date, String dueDate, String transaction, Double amount, 
		   String receiptNumber, String receiptDate, Double receiptApplyAmount, String group, 
		   String referenceNumber, String creditMemoNumber, String creditMemoDate, Double creditMemoAmount){

	   this.date = date;
	   this.dueDate = dueDate;
	   this.transaction = transaction;
	   this.amount = amount;
	   this.receiptNumber = receiptNumber;
	   this.receiptDate = receiptDate;
	   this.receiptApplyAmount = receiptApplyAmount;
	   this.group = group;
	   this.referenceNumber = referenceNumber;
	   this.creditMemoNumber = creditMemoNumber;
	   this.creditMemoDate = creditMemoDate;
	   this.creditMemoAmount = creditMemoAmount;
	   
   }

   public String getDate() {
   	
   	   return date;
   	
   }
   
   public String getDueDate() {
   	
   	   return dueDate;
   	
   }
   
   public String getTransaction() {
   	   
   	   return transaction;
   	
   }
   
   public Double getAmount() {
   	
   	   return amount;
   	
   }
   
   public String getReceiptNumber() {
	   
	   return receiptNumber;
	   
   }
   
   public String getReceiptDate() {
	   
	   return receiptDate;
	   
   }
   
   public Double getReceiptApplyAmount() {
	   
	   return receiptApplyAmount;
	   
   }
   
   public String getGroup() {
	   
	   return group;
	   
   }
   
   public String getReferenceNumber() {
	   
	   return referenceNumber;
	   
   }
   
   public String getCreditMemoNumber() {
	   
	   return creditMemoNumber;
	   
   }
   
   public String getCreditMemoDate() {
	   
	   return creditMemoDate;
	   
   }
   
   public Double getCreditMemoAmount() {
	   
	   return creditMemoAmount;
	   
   }

}
