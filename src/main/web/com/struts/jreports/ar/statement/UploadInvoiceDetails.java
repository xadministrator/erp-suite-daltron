package com.struts.jreports.ar.statement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.struts.util.Common;
import com.util.ArRepStatementDetails;





public class UploadInvoiceDetails {

	 ArrayList data = new ArrayList();
	 Map parameters ;

	 public UploadInvoiceDetails(Map parameters,ArrayList soa_list){

		 this.parameters = parameters;
		 setSoaList(soa_list);


	 }

	 private void setSoaList(ArrayList list){
		 Iterator i = list.iterator();

	      while (i.hasNext()) {

	         ArRepStatementDetails details = (ArRepStatementDetails)i.next();

		     ArRepStatementData smtData = new ArRepStatementData(


	    		 details.getSmtPmProjectCode(),
	    		 details.getSmtPmProjectType(),
		  		 details.getSmtPmProjectName(),
		  		 details.getSmtPmContractPrice(),
		  		details.getSmtPmContractTermName(),
		  		details.getSmtPmContractValue(),


		    	details.getSmtCustomerCode(),

		    	details.getSmtCstCustomerID(),
		    	details.getSmtCstEmployeeID(),
		    	details.getSmtCstAccountNumber(),

		    	details.getSmtCustomerBatch(),
		    	details.getSmtCustomerDepartment(),
		    	Common.convertSQLDateToString(details.getSmtCustomerEntryDate()),

		         details.getSmtCustomerName(),
		         details.getSmtCustomerAddress(),
		         details.getSmtCstContact(),
		         details.getSmtCstTin(),
		         details.getSmtCstSquareMeter(),
		         details.getSmtCstParkingID(),
		         details.getSmtCstBillingHeader(),
		         details.getSmtCstBillingFooter(),
		         details.getSmtCstBillingSignatory(),
				 details.getSmtCstSignatoryTitle(),


				 Common.convertSQLDateToString(details.getSmtDate()),
		         Common.convertSQLDateToString(details.getSmtDueDate()),
		         details.getSmtDueDate(),
		         Common.convertSQLDateToString(details.getSmtLastDueDate()),
		         new Double(details.getSmtPaymentTerm()),
		         details.getSmtPaymentTermName(),
		         details.getSmtTransaction(),
		         details.getSmtTransactionType(),
		         details.getSmtPosted() == (byte)1 ? "YES" : "NO",

		         details.getSmtOrNumber(),
		         details.getSmtInvoiceNumber(),
		         Common.convertSQLDateToString(details.getSmtLastOrDate()),
		         new Double(details.getSmtInvoiceAmountDue()),
		         new Double(details.getSmtInvoiceDownPayment()),
		         new Double(details.getSmtInvoiceUnearnedInterest()),

		         new Double(details.getSmtInvoicePenaltyDue()),
		         new Double(details.getSmtNumber()),
		         new Double(details.getSmtAmountDue()),
		         new Double(details.getSmtIpsAmountDue()),
		         new Double(details.getSmtAmountPaid()),
		         new Double(details.getSmtInterestDue()),
		         new Double(details.getSmtAmountDetails()),
		         new Double(details.getSmtDebit()),
		         new Double(details.getSmtCredit()),
		         new Double(details.getSmtCreditAmount()),
		         details.getSmtBatchName(),
		         details.getSmtDescription(),

		         details.getSmtItemDescription(),

		         new Double(details.getSmtPreviousReading()),
		         new Double(details.getSmtPresentReading()),
		         new Double(details.getSmtQuantity()),
		         new Double(details.getSmtUnitPrice()),
		         new Double(details.getSmtAmount()),
		         new Double(details.getSmtPenalty()),
		         new Double(details.getSmtTaxAmount()),
                         new Double(details.getSmtWithholdingTaxAmount()),
		         details.getSmtTaxCode(),

		         details.getSmtRemainingBalance(),
		         details.getSmtBucket0() != 0d ? new Double(details.getSmtBucket0()) : null,
		         details.getSmtBucket1() != 0d ? new Double(details.getSmtBucket1()) : null,
		         details.getSmtBucket2() != 0d ? new Double(details.getSmtBucket2()) : null,
		         details.getSmtBucket3() != 0d ? new Double(details.getSmtBucket3()) : null,
		         details.getSmtBucket4() != 0d ? new Double(details.getSmtBucket4()) : null,
		         details.getSmtBucket5() != 0d ? new Double(details.getSmtBucket5()) : null,

	    		 details.getSmtBucketASD0() != 0d ? new Double(details.getSmtBucketASD0()) : null,
	    		         details.getSmtBucketASD1() != 0d ? new Double(details.getSmtBucketASD1()) : null,
	    		         details.getSmtBucketASD2() != 0d ? new Double(details.getSmtBucketASD2()) : null,
	    		         details.getSmtBucketASD3() != 0d ? new Double(details.getSmtBucketASD3()) : null,
	    		         details.getSmtBucketASD4() != 0d ? new Double(details.getSmtBucketASD4()) : null,
	    		         details.getSmtBucketASD5() != 0d ? new Double(details.getSmtBucketASD5()) : null,

	    		        		 details.getSmtBucketRPT0() != 0d ? new Double(details.getSmtBucketRPT0()) : null,
	    		        		         details.getSmtBucketRPT1() != 0d ? new Double(details.getSmtBucketRPT1()) : null,
	    		        		         details.getSmtBucketRPT2() != 0d ? new Double(details.getSmtBucketRPT2()) : null,
	    		        		         details.getSmtBucketRPT3() != 0d ? new Double(details.getSmtBucketRPT3()) : null,
	    		        		         details.getSmtBucketRPT4() != 0d ? new Double(details.getSmtBucketRPT4()) : null,
	    		        		         details.getSmtBucketRPT5() != 0d ? new Double(details.getSmtBucketRPT5()) : null,

	    		        		        		 details.getSmtBucketPD0() != 0d ? new Double(details.getSmtBucketPD0()) : null,
	    		        		        		         details.getSmtBucketPD1() != 0d ? new Double(details.getSmtBucketPD1()) : null,
	    		        		        		         details.getSmtBucketPD2() != 0d ? new Double(details.getSmtBucketPD2()) : null,
	    		        		        		         details.getSmtBucketPD3() != 0d ? new Double(details.getSmtBucketPD3()) : null,
	    		        		        		         details.getSmtBucketPD4() != 0d ? new Double(details.getSmtBucketPD4()) : null,
	    		        		        		         details.getSmtBucketPD5() != 0d ? new Double(details.getSmtBucketPD5()) : null,

	    		        		        		        		 details.getSmtBucketWTR0() != 0d ? new Double(details.getSmtBucketWTR0()) : null,
	    		        		        		        		         details.getSmtBucketWTR1() != 0d ? new Double(details.getSmtBucketWTR1()) : null,
	    		        		        		        		         details.getSmtBucketWTR2() != 0d ? new Double(details.getSmtBucketWTR2()) : null,
	    		        		        		        		         details.getSmtBucketWTR3() != 0d ? new Double(details.getSmtBucketWTR3()) : null,
	    		        		        		        		         details.getSmtBucketWTR4() != 0d ? new Double(details.getSmtBucketWTR4()) : null,
	    		        		        		        		         details.getSmtBucketWTR5() != 0d ? new Double(details.getSmtBucketWTR5()) : null,

	    		        		        		        		        		 details.getSmtBucketMISC0() != 0d ? new Double(details.getSmtBucketMISC0()) : null,
	    		        		        		        		        		         details.getSmtBucketMISC1() != 0d ? new Double(details.getSmtBucketMISC1()) : null,
	    		        		        		        		        		         details.getSmtBucketMISC2() != 0d ? new Double(details.getSmtBucketMISC2()) : null,
	    		        		        		        		        		         details.getSmtBucketMISC3() != 0d ? new Double(details.getSmtBucketMISC3()) : null,
	    		        		        		        		        		         details.getSmtBucketMISC4() != 0d ? new Double(details.getSmtBucketMISC4()) : null,
	    		        		        		        		        		         details.getSmtBucketMISC5() != 0d ? new Double(details.getSmtBucketMISC5()) : null,

	    		        		        		        		        		        		 details.getSmtBucketADV0() != 0d ? new Double(details.getSmtBucketADV0()) : null,
	    		        		        		        		        		        		         details.getSmtBucketADV1() != 0d ? new Double(details.getSmtBucketADV1()) : null,
	    		        		        		        		        		        		         details.getSmtBucketADV2() != 0d ? new Double(details.getSmtBucketADV2()) : null,
	    		        		        		        		        		        		         details.getSmtBucketADV3() != 0d ? new Double(details.getSmtBucketADV3()) : null,
	    		        		        		        		        		        		         details.getSmtBucketADV4() != 0d ? new Double(details.getSmtBucketADV4()) : null,
	    		        		        		        		        		        		         details.getSmtBucketADV5() != 0d ? new Double(details.getSmtBucketADV5()) : null,


		        details.getSmtReferenceNumber(), details.getSmtInvoiceBatchName(),
		        details.getSmtNumberDays(), details.getSmtExclusiveAdvance(), details.getSmtInclusiveAdvance()
		    		 );

	         data.add(smtData);
	      }
	 }




}
