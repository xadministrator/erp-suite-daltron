package com.struts.jreports.ar.taxcodelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepTaxCodeListDetails;

public class ArRepTaxCodeListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepTaxCodeListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         ArRepTaxCodeListDetails details = (ArRepTaxCodeListDetails)i.next();
                  
	     ArRepTaxCodeListData argData = new ArRepTaxCodeListData(details.getTclTaxName(), 
	     		details.getTclTaxDescription(),details.getTclTaxType(), new Double(details.getTclTaxRate()), 
				details.getTclCoaGlTaxAccountNumber(), details.getTclCoaGlTaxAccountDescription(), 
				details.getTclCoaGlInterimAccountNumber(), details.getTclCoaGlInterimAccountDescription(), 
				details.getTclEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("taxName".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxName();
      }else if("taxDescription".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxDescription();
      }else if("taxType".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxType();
      }else if("taxRate".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxRate();
      }else if("taxAccountNumber".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxAccountNumber();         
      }else if("taxAccountDescription".equals(fieldName)){
         value = ((ArRepTaxCodeListData)data.get(index)).getTaxAccountDescription();
      }else if("interimAccountNumber".equals(fieldName)){
        value = ((ArRepTaxCodeListData)data.get(index)).getInterimAccountNumber();         
      }else if("interimAccountDescription".equals(fieldName)){
        value = ((ArRepTaxCodeListData)data.get(index)).getInterimAccountDescription();     
      }else if("enable".equals(fieldName)){
        value = ((ArRepTaxCodeListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
