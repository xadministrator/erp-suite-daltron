package com.struts.jreports.ar.taxcodelist;


public class ArRepTaxCodeListData implements java.io.Serializable {
	
   private String taxName = null;
   private String taxDescription = null;
   private String taxType = null;
   private Double taxRate = null;
   private String taxAccountNumber = null;
   private String taxAccountDescription = null;
   private String interimAccountNumber = null;
   private String interimAccountDescription = null;
   private String enable = null;

   public ArRepTaxCodeListData(String taxName, String taxDescription, String taxType, Double taxRate,
   		String taxAccountNumber, String taxAccountDescription, String interimAccountNumber, 
		String interimAccountDescription, String enable){
      	
      this.taxName = taxName;
      this.taxDescription = taxDescription;
      this.taxType = taxType;
      this.taxRate = taxRate;
      this.taxAccountNumber = taxAccountNumber;
      this.taxAccountDescription = taxAccountDescription;
      this.interimAccountNumber = interimAccountNumber;
      this.interimAccountDescription = interimAccountDescription;
      this.enable = enable;
      
   }

   public String getTaxAccountDescription() {
   	
     return taxAccountDescription;
   
   }
   
   public void setTaxAccountDescription(String taxAccountDescription) {
   
     this.taxAccountDescription = taxAccountDescription;
   
   }
   
   public String getTaxAccountNumber() {
   
   	 return taxAccountNumber;
   
   }
   
   public void setTaxAccountNumber(String taxAccountNumber) {
   
   	 this.taxAccountNumber = taxAccountNumber;
   
   }
   
   public String getInterimAccountDescription() {
   	
     return interimAccountDescription;
   
   }
   
   public void setInterimAccountDescription(String interimAccountDescription) {
   
     this.interimAccountDescription = interimAccountDescription;
   
   }
   
   public String getInterimAccountNumber() {
   
   	 return interimAccountNumber;
   
   }
   
   public void setInterimAccountNumber(String interimAccountNumber) {
   
   	 this.interimAccountNumber = interimAccountNumber;
   
   }
   
   public String getEnable() {
   
   	 return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	 this.enable = enable;
   
   }
   
   public String getTaxDescription() {
   
   	 return taxDescription;
   
   }
   
   public void setTaxDescription(String taxDescription) {
   
   	 this.taxDescription = taxDescription;
   
   }
  
   public String getTaxName() {
   
   	 return taxName;
   
   }
   
   public void setTaxName(String taxName) {
   
   	 this.taxName = taxName;
   
   }
   
   public Double getTaxRate() {
   
   	 return taxRate;
   
   }
   
   public void setTaxRate(Double taxRate) {
   
     this.taxRate = taxRate;
   
   }
   
   public String getTaxType() {
   
   	 return taxType;
   
   }
   
   public void setTaxType(String taxType) {
   
   	 this.taxType = taxType;
   
   }
   
}
