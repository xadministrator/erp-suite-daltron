package com.struts.jreports.ar.transmittaldocumentprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArRepInvoicePrintController;
import com.ejb.txn.ArRepInvoicePrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoicePrintDetails;

public final class ArRepTransmittalDocumentPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	   

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArRepTransmittalDocumentPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArRepTransmittalDocumentPrintForm actionForm = (ArRepTransmittalDocumentPrintForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.AR_REP_TRANSMITTAL_DOCUMENT_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize ArRepInvoicePrintController EJB
*******************************************************/

         ArRepInvoicePrintControllerHome homeTDP = null;
         ArRepInvoicePrintController ejbTDP = null;       

         try {
         	
            homeTDP = (ArRepInvoicePrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArRepInvoicePrintControllerEJB", ArRepInvoicePrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in ArRepTransmittalDocumentPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbTDP = homeTDP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in ArRepTransmittalDocumentPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	       
/*******************************************************
   -- TO DO executeArRepInvoicePrint() --
*******************************************************/       

		if (frParam != null) {

			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list = null;  
			    ArRepInvoicePrintDetails details = null;     
			    ArrayList invCodeList = new ArrayList();
			    
				try {
					
					if (request.getParameter("forward") != null) {
												
	            		invCodeList.add(new Integer(request.getParameter("invoiceCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("invoiceCode[" + i + "]") != null) {
								
								invCodeList.add(new Integer(request.getParameter("invoiceCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbTDP.executeArRepInvoicePrint(invCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbTDP.getAdCompany(user.getCmpCode()); 

			        Iterator i = list.iterator();
			        
			        while (i.hasNext()) {
			        	
			        	details = (ArRepInvoicePrintDetails)i.next();
			        	
			            int num = (int)Math.floor(details.getIpInvAmount());
					
						String str = Common.convertDoubleToStringMoney(details.getIpInvAmount(), (short)2);
						str = str.substring(str.indexOf('.') + 1, str.length());
						
			        }
           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("transmittalDocumentPrint.error.invoiceAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArRepTransmittalDocumentPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           }
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("arRepTransmittalDocumentPrintForm");

	             }
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				
				String companyAddress = adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();	            	
				
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("companyAddress", companyAddress);
				parameters.put("phoneNumber", adCmpDetails.getCmpPhone());
				parameters.put("faxNumber", adCmpDetails.getCmpFax());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/ArRepTransmittalDocumentPrint.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/ArRepTransmittalDocumentPrint.jasper");
			    
		        }
				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new ArRepTransmittalDocumentPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in ArRepTransmittalDocumentPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				    
				    ex.printStackTrace();
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("arRepTransmittalDocumentPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("arRepTransmittalDocumentPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in ArRepTransmittalDocumentPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          
          e.printStackTrace();
          return(mapping.findForward("cmnErrorPage"));        
   
     }

   }

}
