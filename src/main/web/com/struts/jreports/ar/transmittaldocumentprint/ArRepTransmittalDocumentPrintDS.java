/*
 * opt/jreport/ArRepTransmittalDocumentPrintDS.java
 *
 * Created on May 11, 2006, 4:25 PM
 *
 * @author  Neville P. Tagle
 */

package com.struts.jreports.ar.transmittaldocumentprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.ArRepInvoicePrintDetails;

public class ArRepTransmittalDocumentPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepTransmittalDocumentPrintDS(ArrayList list) {
	   int lineNumber = 0;
   	
   	  Iterator i = list.iterator();
   	  
   	  while(i.hasNext()) {

        ArRepInvoicePrintDetails details = (ArRepInvoicePrintDetails)i.next();
        
        String customerAddress = details.getIpInvCustomerAddress() + " " + details.getIpInvCustomerCity() + " " + details.getIpInvCustomerCountry();        
        
        ArRepTransmittalDocumentPrintData arRepTDPData = new ArRepTransmittalDocumentPrintData(details.getIpInvCustomerName(),
    	  		customerAddress, Common.convertSQLDateToString(details.getIpInvDate()), details.getIpInvDate(), 
    			details.getIpInvNumber(), details.getIpInvReferenceNumber(), new Integer(++lineNumber),
    			details.getIpIlDescription(), Common.convertDoubleToStringMoney(details.getIpIlQuantity(), (short)2), 
    			
    			new Double(details.getIpIlUnitPrice()), 
    			
    			new Double(details.getIpIlAmount()), 
    			Common.convertSQLDateToString(details.getIpInvDueDate()),Common.convertSQLDateToString(details.getIpInvEffectivityDate()), details.getIpInvCurrency(),
    			Common.convertCharToString(details.getIpInvCurrencySymbol()), details.getIpInvCurrencyDescription(), 
    			details.getIpInvAmountInWords(),
    			details.getIpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
    			details.getIpInvBillingHeader(), details.getIpInvBillingFooter(), details.getIpInvBillingHeader2(), 
    			details.getIpInvBillingFooter2(), details.getIpInvBillingHeader3(), 
    			details.getIpInvBillingFooter3(), details.getIpInvBillingSignatory(),
    			details.getIpInvSignatoryTitle(), details.getIpInvBillToAddress(), details.getIpInvBillToContact(),
    			details.getIpInvBillToAltContact(), details.getIpInvBillToPhone(), details.getIpInvShipToAddress(), 
    			details.getIpInvShipToContact(), details.getIpInvShipToAltContact(), details.getIpInvShipToPhone(),
    			new Double(details.getIpInvTotalTax()), new Double(details.getIpIlAmountWoVat()),
    			new Double(details.getIpInvDiscountAmount()), details.getIpInvDiscountDescription(),
    			details.getIpInvIlSmlUnitOfMeasure(), details.getIpInvPytName(), details.getIpInvPytDescription(),
    			details.getIpIlCategory(), details.getIpInvReferenceNumber(), details.getIpInvCstCustomerCode(),
    			details.getIpIlName(), details.getIpIlUomShortName(), details.getIpSlpSalespersonCode(),
    			details.getIpInvDescription(), new Double(details.getIpInvAmount()), new Double(details.getIpInvAmountUnearnedInterest()) , details.getIpSlpName(), 
    			details.getIpInvCustomerDescription(), details.getIpInvCustomerTin(), details.getIpInvSoNumber(), 
    			
    			new Double (details.getIpIlUnitPriceWoVat()), 
    			
    			new Double (details.getIpIliDiscount1()),
    			new Double (details.getIpIliDiscount2()), new Double (details.getIpIliDiscount3()),
    			new Double (details.getIpIliDiscount4()), details.getIpIliClientPo(),  new Double (details.getIpInvTcRate()), details.getIpInvTaxCode(), details.getIpInvWithholdingTaxCode(), 
				new Double (details.getIpInvWithholdingTaxAmount()), new Double (details.getIpIlQuantity()), new Double (details.getIpIliTotalDiscount()),
				details.getIpInvCreatedBy(), details.getIpInvCreatedByDesc(),details.getIpInvCheckByDescription(), details.getIpInvApprovedRejectedByDescription(),
				details.getIpInvBranchCode(), details.getIpInvBranchName(), details.getIpInvPartNumber(),details.getIpInvSoReferenceNumber(), details.getIpInvApprovalStatus(), details.getIpInvCustomerDealPrice());
  	
	  	data.add(arRepTDPData);
	  	  	
   	  }
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("customer".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCustomer();       
       }else if("address".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getAddress();
       }else if("date".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDate();   
       }else if("date2".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDate2();
       }else if("invoiceNumber".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getInvoiceNumber();
       }else if("number".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getNumber();       
       }else if("lineNumber".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getLineNumber();       
       }else if("description".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDescription();
       }else if("quantity".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getQuantity();  
       }else if("unitPrice".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getUnitPrice();
       }else if("unitPriceWoVat".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getUnitPriceWoVat();   
       }else if("amount".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getAmount();
       }else if("dueDate".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDueDate();
       }else if("effectivityDate".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getEffectivityDate();
           System.out.println("effectivityDate="+value);
       }else if("currency".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCurrency();
       }else if("currencySymbol".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCurrencySymbol();
       }else if("currencyDescription".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCurrencyDescription();
       }else if("amountInWords".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getAmountInWords();
       }else if("showDuplicate".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getShowDuplicate();   
       }else if("billingHeader".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingHeader();
       }else if("billingFooter".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingFooter();
       }else if("billingHeader2".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingHeader2();
       }else if("billingFooter2".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingFooter2();       
       }else if("billingHeader3".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingHeader3();
       }else if("billingFooter3".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingFooter3();
       }else if("billingSignatory".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillingSignatory();
       }else if("signatoryTitle".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getSignatoryTitle();
       }else if("billToAddress".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillToAddress();
       }else if("billToContact".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillToContact();
       }else if("billToAltContact".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillToAltContact();
       }else if("billToPhone".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBillToPhone();
       }else if("shipToAddress".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getShipToAddress();
       }else if("shipToContact".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getShipToContact();
       }else if("shipToAltContact".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getShipToAltContact();
       }else if("shipToPhone".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getShipToPhone(); 
       }else if("taxAmount".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getTaxAmont();
       }else if("amountWoVat".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getAmountWoVat();
       }else if("discountAmount".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscountAmount();
       }else if("discountDescription".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscountDescription();
       }else if("unitOfMeasure".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getUnitOfMeasure();
       }else if("paymentTermName".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getPaymentTermName();
       }else if("paymentTermDescription".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getPaymentTermDescription();
       }else if("itemCategory".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getItemCategory();
       }else if("referenceNumber".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getReferenceNumber();
       }else if("customerCode".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCustomerCode();
       }else if("itemName".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getItemName();
       }else if("unitOfMeasureShortName".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getUnitOfMeasureShortName();
       }else if("salespersonCode".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getSalespersonCode();
       }else if("invoiceDescription".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getInvoiceDescription();
       }else if("invoiceAmount".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getInvoiceAmount();
       }else if("invoiceAmountUnearnedInterest".equals(fieldName)){
        	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getInvoiceAmountUnearnedInterest();
       }else if("salespersonName".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getSalespersonName();
       }else if("customerDescription".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCustomerDescription();
       }else if("customerTin".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCustomerTin();
       }else if("soNumber".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getSoNumber();
       }else if("discount1".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscount1();
       }else if("discount2".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscount2();
       }else if("discount3".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscount3();
       }else if("discount4".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDiscount4();
       }else if("clientPo".equals(fieldName)){
        	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getClientPo();
       }else if("taxRate".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getTaxRate();
       }else if("ipsDueDate".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getIpsDueDate();
       }else if("amountDue".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getAmountDue();
       }else if("createdBy".equals(fieldName)){
       	value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCreatedBy();
       }else if("taxCode".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getTaxCode();
       }else if("withholdingTaxCode".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getWithholdingTaxCode();
       }else if("withholdingTaxAmount".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getWithholdingTaxAmount();
       }else if("quantity2".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getQuantity2();
       }else if("totalDiscount".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getTotalDiscount();
       }else if("createdByDesc".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCreatedByDesc();
       }else if("checkedByDescription".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getCheckedByDescription();
          System.out.println("value: "+value);
       }else if("approvedRejectedByDescription".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getApprovedRejectedByDescription();
       }else if("branchCode".equals(fieldName)){
          value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBranchCode();
       }else if("branchName".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getBranchName();
       }else if("partNumber".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getPartNumber();
       }else if("soReferenceNumber".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getSoReference();
       }else if("approvalStatus".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getApprovalStatus();
       }else if("dealPrice".equals(fieldName)){
           value = ((ArRepTransmittalDocumentPrintData)data.get(index)).getDealPrice();
       }
      return(value);
   }
}
