package com.struts.jreports.ar.withholdingtaxcodelist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.ArRepWithholdingTaxCodeListDetails;

public class ArRepWithholdingTaxCodeListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public ArRepWithholdingTaxCodeListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	 ArRepWithholdingTaxCodeListDetails details = (ArRepWithholdingTaxCodeListDetails)i.next();
                  
	     ArRepWithholdingTaxCodeListData argData = new ArRepWithholdingTaxCodeListData(details.getWtlTaxName(), 
	     		details.getWtlTaxDescription(), new Double(details.getWtlTaxRate()), details.getWtlCoaGlTaxAccountNumber(),
				details.getWtlCoaGlTaxAccountDescription(), details.getWtlEnable() == 1 ? "YES" : "NO");
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("taxName".equals(fieldName)){
         value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getTaxName();
      }else if("taxDescription".equals(fieldName)){
         value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getTaxDescription();
      }else if("taxRate".equals(fieldName)){
         value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getTaxRate();
      }else if("accountNumber".equals(fieldName)){
         value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getAccountNumber();         
      }else if("accountDescription".equals(fieldName)){
         value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getAccountDescription();
      }else if("enable".equals(fieldName)){
        value = ((ArRepWithholdingTaxCodeListData)data.get(index)).getEnable();   
      }

      return(value);
   }
}
