package com.struts.jreports.ar.withholdingtaxcodelist;


public class ArRepWithholdingTaxCodeListData implements java.io.Serializable {
	
   private String taxName = null;
   private String taxDescription = null;
   private Double taxRate = null;
   private String accountNumber = null;
   private String accountDescription = null;
   private String enable = null;

   public ArRepWithholdingTaxCodeListData(String taxName, String taxDescription, Double taxRate,
   		String accountNumber, String accountDescription, String enable){
      	
      this.taxName = taxName;
      this.taxDescription = taxDescription;
      this.taxRate = taxRate;
      this.accountNumber = accountNumber;
      this.accountDescription = accountDescription;
      this.enable = enable;
      
   }

   public String getAccountDescription() {
   	
     return accountDescription;
   
   }
   
   public void setAccountDescription(String accountDescription) {
   
     this.accountDescription = accountDescription;
   
   }
   
   public String getAccountNumber() {
   
   	 return accountNumber;
   
   }
   
   public void setAccountNumber(String accountNumber) {
   
   	 this.accountNumber = accountNumber;
   
   }
   
   public String getEnable() {
   
   	 return enable;
   
   }
   
   public void setEnable(String enable) {
   
   	 this.enable = enable;
   
   }
   
   public String getTaxDescription() {
   
   	 return taxDescription;
   
   }
   
   public void setTaxDescription(String taxDescription) {
   
   	 this.taxDescription = taxDescription;
   
   }
  
   public String getTaxName() {
   
   	 return taxName;
   
   }
   
   public void setTaxName(String taxName) {
   
   	 this.taxName = taxName;
   
   }
   
   public Double getTaxRate() {
   
   	 return taxRate;
   
   }
   
   public void setTaxRate(Double taxRate) {
   
     this.taxRate = taxRate;
   
   }
   
}
