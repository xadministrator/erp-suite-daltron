package com.struts.jreports.cm.adjustmentprint;

import java.util.ArrayList;
import java.util.Iterator;

import com.struts.jreports.ar.receiptprint.ArRepReceiptPrintData;
import com.struts.util.Common;
import com.util.CmModAdjustmentDetails;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class CmRepAdjustmentPrintDS implements JRDataSource {

	
	//public CmModAdjustmentDetails sd;
	private ArrayList data = new ArrayList();

	private int index = -1;
	
	public CmRepAdjustmentPrintDS(ArrayList list){
		
		Iterator i = list.iterator();
	   	  while(i.hasNext()) {
	   		CmModAdjustmentDetails mdetails = (CmModAdjustmentDetails)i.next();
	   		
	   		CmRepAdjustmentPrintData cmRepAdjData = new CmRepAdjustmentPrintData(mdetails.getAdjType(),mdetails.getAdjCustomerCode(),mdetails.getAdjCustomerName(),Common.convertSQLDateToString(mdetails.getAdjDate()),mdetails.getAdjDocumentNumber(),   				
	   				mdetails.getAdjBaName(),mdetails.getAdjReferenceNumber(),mdetails.getAdjAmount(),mdetails.getAdjAmountApplied(),
	   				mdetails.getAdjMemo(),mdetails.getAdjTotalDebit(),mdetails.getAdjTotalCredit(),mdetails.getAdjCreatedBy(),mdetails.getAdjPostedBy());
	   		/*
	   		 * 
	   		 * String adjustmentType, String customerCode,
			String date, String documentNumber, String bankAccount,
			String referenceNumber, Double amount, Double amountApplied,
			String description, Double totalDebit, Double totalCredit,
			String createdBy, String postedBy
	   		 */
	   		data.add(cmRepAdjData);
	   		
	   	  }
		
	}
	   
   public boolean next() throws JRException {
		   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
	   	Object value = null;

      String fieldName = field.getName();
	   
       if("type".equals(fieldName)){
          value = ((CmRepAdjustmentPrintData)data.get(index)).getAdjustmentType(); 
       }else if("date".equals(fieldName)){
          value = ((CmRepAdjustmentPrintData)data.get(index)).getDate();
       }else if("customerCode".equals(fieldName)){
          value = ((CmRepAdjustmentPrintData)data.get(index)).getCustomerCode();
       }else if("customerName".equals(fieldName)){
           value = ((CmRepAdjustmentPrintData)data.get(index)).getCustomerName();
        }else if("documentNumber".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getDocumentNumber();
        }else if("bankAccount".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getBankAccount();
        }else if("referenceNumber".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getReferenceNumber();
        }else if("amount".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getAmount();
        }else if("amountApplied".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getAmountApplied();
        }else if("description".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getDescription();
        }else if("totalDebit".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getTotalDebit();
        }else if("totalCredit".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getTotalCredit();
        }else if("customerCode".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getCustomerCode();
        }else if("createdBy".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getCreatedBy();
        }else if("postedBy".equals(fieldName)){
            value = ((CmRepAdjustmentPrintData)data.get(index)).getPostedBy();
        }
       
       
	      
       return(value);
   }
	   
	   
}
