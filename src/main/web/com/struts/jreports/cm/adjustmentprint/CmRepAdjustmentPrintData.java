package com.struts.jreports.cm.adjustmentprint;

public class CmRepAdjustmentPrintData implements java.io.Serializable{

	
	private String adjustmentType = null;
	private String customerCode = null;
	private String customerName = null;
	private String date = null;
	private String documentNumber = null;
	private String bankAccount = null;
	private String referenceNumber = null;
	private Double amount = null;
	private Double amountApplied = null;
	private String description = null;
	private Double totalDebit = null;
	private Double totalCredit = null;
	private String createdBy = null;
	private String postedBy = null;
	
	
	public CmRepAdjustmentPrintData(String adjustmentType, String customerCode,String customerName,
			String date, String documentNumber, String bankAccount,
			String referenceNumber, Double amount, Double amountApplied,
			String description, Double totalDebit, Double totalCredit,
			String createdBy, String postedBy) {
	
		this.adjustmentType = adjustmentType;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.date = date;
		this.documentNumber = documentNumber;
		this.bankAccount = bankAccount;
		this.referenceNumber = referenceNumber;
		this.amount = amount;
		this.amountApplied = amountApplied;
		this.description = description;
		this.totalDebit = totalDebit;
		this.totalCredit = totalCredit;
		this.createdBy = createdBy;
		this.postedBy = postedBy;
	}
	
	
	
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmountApplied() {
		return amountApplied;
	}
	public void setAmountApplied(Double amountApplied) {
		this.amountApplied = amountApplied;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getTotalDebit() {
		return totalDebit;
	}
	public void setTotalDebit(Double totalDebit) {
		this.totalDebit = totalDebit;
	}
	public Double getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(Double totalCredit) {
		this.totalCredit = totalCredit;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	
			
			
	
	
	
	
}
