package com.struts.jreports.cm.bankreconciliation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmRepBankReconciliationController;
import com.ejb.txn.CmRepBankReconciliationControllerHome;
import com.struts.jreports.ad.bankaccountlist.AdRepBankAccountListDS;
import com.struts.jreports.ad.bankaccountlist.AdRepBranchBankAccountList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.CmRepBankReconciliationDetails;

public final class CmRepBankReconciliationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CmRepBankReconciliationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         CmRepBankReconciliationForm actionForm = (CmRepBankReconciliationForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.CM_REP_BANK_RECONCILIATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmRepBankReconciliation");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize AdRepBankAccountListController EJB
*******************************************************/

         CmRepBankReconciliationControllerHome homeBR = null;
         CmRepBankReconciliationController ejbBR = null;       

         try {
         	
        	 homeBR = (CmRepBankReconciliationControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/CmRepBankReconciliationControllerEJB", CmRepBankReconciliationControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in CmRepBankReconciliationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbBR = homeBR.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in CmRepBankReconciliationAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AD BAL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            CmRepBankReconciliationDetails mdetails = null;
            
            String company = null;

            Integer branchCode = null;
	     	
	     	for(int i=0; i<actionForm.getCmRepBrBankReconciliationListSize(); i++) {
	     	    
	     	    CmRepBranchBankReconciliationList cmBrList = (CmRepBranchBankReconciliationList)actionForm.getCmRepBrBankReconciliationListByIndex(i);
	     	    
	     	    if(cmBrList.getBranchCheckbox() == true) {                                          
	     	        
	     	    	branchCode = cmBrList.getBrCode();                    
	     	        
	     	    }
	     	    
	     	}
	     	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbBR.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    	
		       System.out.print(actionForm.getViewIsDraftBCBD() + " bool");
		       mdetails = ejbBR.executeCmRepBankReconciliation(actionForm.getBankAccountName(),
            	    Common.convertStringToSQLDate(actionForm.getDateFrom()), Common.convertStringToSQLDate(actionForm.getDateTo()),
            	    branchCode, user.getCmpCode(),actionForm.getViewIsDraftBCBD());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("bankReconciliationRep.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in CmRepBankReconciliationAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmRepBankReconciliation");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getBankAccountName() != null) {
				
				parameters.put("accountName", actionForm.getBankAccountName());
					
			}
			
			if (actionForm.getDateFrom() != null) {
				
				parameters.put("dateFrom", actionForm.getDateFrom());
					
			}

			if (actionForm.getDateTo() != null) {

				parameters.put("dateTo", actionForm.getDateTo());

			}
			
			String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getCmRepBrBankReconciliationListSize(); j++) {

      			CmRepBranchBankReconciliationList brList = (CmRepBranchBankReconciliationList)actionForm.getCmRepBrBankReconciliationListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);

			String filename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepBankReconciliation.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/CmRepBankReconciliation.jasper");
		    
	        }
	
	        try {

	        	Report report = new Report();

	        	if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

	        		report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	        		report.setBytes(
	        				JasperRunManager.runReportToPdf(filename, parameters, 
	        						new CmRepBankReconciliationDS(mdetails)));   

	        	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	        		report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	        		report.setBytes(
	        				JasperRunManagerExt.runReportToXls(filename, parameters, 
	        						new CmRepBankReconciliationDS(mdetails)));   

	        	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

	        		report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	        		report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	        				new CmRepBankReconciliationDS(mdetails)));												    

	        	}

	        	session.setAttribute(Constants.REPORT_KEY, report);
	        	actionForm.setReport(Constants.STATUS_SUCCESS);		    	

	        } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in CmRepBankReconciliationAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AD BAL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AD BAL Load Action --
*******************************************************/

             }
         
         if(frParam != null) {

        	 try {

        	 } catch(EJBException ex) {

        		 if(log.isInfoEnabled()) {
        			 log.info("EJBException caught in CmRepBankReconciliationAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }

        	 actionForm.reset(mapping, request);

        	 ArrayList list = new ArrayList();
        	 
        	 // populate branch list

        	 actionForm.clearCmRepBrBankReconciliationList();

        	 list = ejbBR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

        	 Iterator i = list.iterator();
        	 //int ctr = 1;
        	 while(i.hasNext()) {

        		 AdBranchDetails details = (AdBranchDetails)i.next();

        		 CmRepBranchBankReconciliationList adBrList = new CmRepBranchBankReconciliationList(actionForm,
        				 details.getBrBranchCode(), details.getBrName(),
        				 details.getBrCode());

        		 if (details.getBrHeadQuarter() == 1) adBrList.setBranchCheckbox(true);
        		 //ctr++;
        		 actionForm.saveCmRepBrBankReconciliationList(adBrList);

        	 }

        	 // populate bank account list
        	 
        	 actionForm.clearBankAccountNameList();

        	 list = ejbBR.getAdBaAll(user.getCmpCode()); 

        	 if (list == null || list.size() == 0) {

        		 actionForm.setBankAccountNameList(Constants.GLOBAL_NO_RECORD_FOUND);

        	 } else {

        		 i = list.iterator();

        		 while (i.hasNext()) {

        			 actionForm.setBankAccountNameList((String)i.next());

        		 }

        	 }

        	 return(mapping.findForward("cmRepBankReconciliation"));		          

         } else {
	         	
	         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	         	saveErrors(request, new ActionMessages(errors));
	         	
	         	return(mapping.findForward("cmnMain"));
	         	
	         }
	 
         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in CmRepBankReconciliationAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
