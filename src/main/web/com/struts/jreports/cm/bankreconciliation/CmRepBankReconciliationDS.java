package com.struts.jreports.cm.bankreconciliation;


import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepBankReconciliationDetails;

public class CmRepBankReconciliationDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepBankReconciliationDS(CmRepBankReconciliationDetails mdetails) {

	   CmRepBankReconciliationData argData = new CmRepBankReconciliationData(mdetails.getBrBankAccount(), 
			   mdetails.getBrDateFrom(), mdetails.getBrDateTo(), new Double(mdetails.getBrBeginningBalancePerBook()),
			   new Double(mdetails.getBrBeginningBalancePerBank()), new Double(mdetails.getBrDepositAmountPerBook()),
			   new Double(mdetails.getBrDepositAmountPerBank()), new Double(mdetails.getBrDisbursementAmountPerBook()),
			   new Double(mdetails.getBrDisbursementAmountPerBank()), new Double(mdetails.getBrDepositInTransitAmount()),
			   new Double(mdetails.getBrOutstandingChecksAmount()),new Double(mdetails.getBrBankCreditAmount()),new Double(mdetails.getBrBankDebitAmount()));

	   data.add(argData);

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("accountName".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getAccountName();
      }else if("dateFrom".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDateFrom();
      }else if("dateTo".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDateTo();
      }else if("beginningBalPerBook".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getBeginningBalancePerBook();
      }else if("beginningBalPerBank".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getBeginningBalancePerBank();
      }else if("depositAmntPerBook".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDepositAmountPerBook();
      }else if("depositAmntPerBank".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDepositAmountPerBank();
      }else if("disbursementAmntPerBook".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDisbursementAmountPerBook();
      }else if("disbursementAmntPerBank".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDisbursementAmountPerBank();
      }else if("depositInTransit".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getDepositInTransitAmount();
      }else if("outstandingChecks".equals(fieldName)){
         value = ((CmRepBankReconciliationData)data.get(index)).getOutstandingCheckAmount();
      }else if("bankCreditMemo".equals(fieldName)){
          value = ((CmRepBankReconciliationData)data.get(index)).getBankCreditAmount();
      }else if("bankDebitMemo".equals(fieldName)){
          value = ((CmRepBankReconciliationData)data.get(index)).getBankDebitAmount();
      }
      

      return(value);
   }
}
