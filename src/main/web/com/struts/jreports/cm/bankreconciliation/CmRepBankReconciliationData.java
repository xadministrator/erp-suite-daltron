package com.struts.jreports.cm.bankreconciliation;

import java.util.Date;

public class CmRepBankReconciliationData implements java.io.Serializable {
	
   private String accountName = null;
   private String dateFrom = null;
   private String dateTo = null;
   
   private Double beginningBalancePerBook = null;
   private Double beginningBalancePerBank = null;
   private Double depositAmountPerBook = null;
   private Double depositAmountPerBank = null;
   private Double disbursementAmountPerBook = null;
   private Double disbursementAmountPerBank = null;
   private Double depositInTransitAmount = null;
   private Double outstandingChecksAmount = null;
   private Double bankCreditAmount = null;
   private Double bankDebitAmount = null;

   public CmRepBankReconciliationData(String accountName, String dateFrom, String dateTo, 
		   Double beginningBalancePerBook, Double beginningBalancePerBank, Double depositAmountPerBook, 
		   Double depositAmountPerBank, Double disbursementAmountPerBook, Double disbursementAmountPerBank, 
		   Double depositInTransitAmount, Double outstandingChecksAmount, Double bankCreditAmount, Double bankDebitAmount){
      	
      this.accountName = accountName;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;
      this.beginningBalancePerBook = beginningBalancePerBook;
      this.beginningBalancePerBank = beginningBalancePerBank;
      this.depositAmountPerBook = depositAmountPerBook;
      this.depositAmountPerBank = depositAmountPerBank;
      this.disbursementAmountPerBook = disbursementAmountPerBook;
      this.disbursementAmountPerBank = disbursementAmountPerBank;
      this.depositInTransitAmount = depositInTransitAmount;
      this.outstandingChecksAmount = outstandingChecksAmount;
      this.bankCreditAmount = bankCreditAmount;
      this.bankDebitAmount = bankDebitAmount;
      
   }

   public String getAccountName() {
   
	   return accountName;
   
   }
   
   public String getDateFrom() {
	   
	   return dateFrom;
	   
   }
   
   public String getDateTo() {
	   
	   return dateTo;
	   
   }
   
   public Double getBeginningBalancePerBook() {
	   
	   return beginningBalancePerBook;
	   
   }
   
   public Double getBeginningBalancePerBank() {
	   
	   return beginningBalancePerBank;
	   
   }
   
   public Double getDepositAmountPerBook() {
	   
	   return depositAmountPerBook;
	   
   }

   public Double getDepositAmountPerBank() {

	   return depositAmountPerBank;

   }
   
   public Double getDisbursementAmountPerBook() {
	   
	   return disbursementAmountPerBook;
	   
   }

   public Double getDisbursementAmountPerBank() {

	   return disbursementAmountPerBank;

   }
   
   public Double getDepositInTransitAmount() {
	   
	   return depositInTransitAmount;
	   
   }
   
   public Double getOutstandingCheckAmount() {
	   
	   return outstandingChecksAmount;
	   
   }
   
   public Double getBankCreditAmount() {
	   
	   return bankCreditAmount;
	   
   }
   
   public Double getBankDebitAmount() {
	   
	   return bankDebitAmount;
	   
   }
   
}
