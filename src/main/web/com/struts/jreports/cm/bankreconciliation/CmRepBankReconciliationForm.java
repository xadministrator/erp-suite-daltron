package com.struts.jreports.cm.bankreconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class CmRepBankReconciliationForm extends ActionForm implements Serializable{

	private String bankAccountName = null;
	private ArrayList bankAccountNameList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;

	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String viewIsDraftBCBD = null;
	private ArrayList viewIsDraftBCBDList = new ArrayList();
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;

	private String userPermission = new String();

	private ArrayList cmRepBrBankReconciliationList = new ArrayList();

	public String getBankAccountName() {

		return bankAccountName;

	}

	public void setBankAccountName(String bankAccountName) {

		this.bankAccountName = bankAccountName;

	}

	public ArrayList getBankAccountNameList() {

		return bankAccountNameList;

	}

	public void setBankAccountNameList(String bankAccountName) {

		bankAccountNameList.add(bankAccountName);

	}

	public void clearBankAccountNameList() {

		bankAccountNameList.clear();
		bankAccountNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getDateFrom() {

		return dateFrom;

	}

	public void setDateFrom(String dateFrom) {

		this.dateFrom = dateFrom;

	}

	public String getDateTo() {

		return dateTo;

	}

	public void setDateTo(String dateTo) {

		this.dateTo = dateTo;

	}

	public void setGoButton(String goButton) {

		this.goButton = goButton;

	}

	public void setCloseButton(String closeButton) {

		this.closeButton = closeButton;

	}

	public String getViewType() {

		return viewType;   	

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}
	
	
	
	public String getViewIsDraftBCBD() {

		return viewIsDraftBCBD;   	

	}

	public void setViewIsDraftBCBD(String viewIsDraftBCBD) {

		this.viewIsDraftBCBD = viewIsDraftBCBD;

	}

	public ArrayList getViewIsDraftBCBDList() {

		return viewIsDraftBCBDList;

	}
	
	
	

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getUserPermission() {

		return userPermission;

	}
	

	
	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public Object[] getCmRepBrBankReconciliationList(){

		return cmRepBrBankReconciliationList.toArray();

	}

	public CmRepBranchBankReconciliationList getCmRepBrBankReconciliationListByIndex(int index){

		return ((CmRepBranchBankReconciliationList)cmRepBrBankReconciliationList.get(index));

	}

	public int getCmRepBrBankReconciliationListSize(){

		return(cmRepBrBankReconciliationList.size());

	}

	public void saveCmRepBrBankReconciliationList(Object newCmRepBrBankReconciliationList){

		cmRepBrBankReconciliationList.add(newCmRepBrBankReconciliationList);   	  

	}

	public void clearCmRepBrBankReconciliationList(){

		cmRepBrBankReconciliationList.clear();

	}

	public void setCmBrRepBankReconciliationList(ArrayList cmRepBrBankReconciliationList) {

		this.cmRepBrBankReconciliationList = cmRepBrBankReconciliationList;

	}
	
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {   

		for (int i=0; i<cmRepBrBankReconciliationList.size(); i++) {

			CmRepBranchBankReconciliationList list = (CmRepBranchBankReconciliationList)cmRepBrBankReconciliationList.get(i);
			list.setBranchCheckbox(false);	       

		}  

		bankAccountName = Constants.GLOBAL_BLANK;	
		dateFrom = Common.convertSQLDateToString(new Date());
		dateTo = Common.convertSQLDateToString(new Date());
		
		goButton = null;
		closeButton = null;

		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		
		
		viewIsDraftBCBDList.clear();
		viewIsDraftBCBDList.add("Yes");
		viewIsDraftBCBDList.add("No");
		viewType ="No";
		
		
		
		

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if (Common.validateRequired(bankAccountName)) {

				errors.add("bankAccountName",
						new ActionMessage("bankReconciliationRep.error.bankAccountNameRequired"));

			}

			if (!Common.validateDateFormat(dateFrom)) {

				errors.add("dateFrom",
						new ActionMessage("bankReconciliationRep.error.dateFromInvalid"));

			}

			if (!Common.validateDateFormat(dateTo)) {

				errors.add("dateTo",
						new ActionMessage("bankReconciliationRep.error.dateToInvalid"));

			}

		}
		return(errors);
	}
}
