package com.struts.jreports.cm.cashposition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.CmRepCashPositionDetails;
import com.ejb.txn.CmRepCashPositionController;
import com.ejb.txn.CmRepCashPositionControllerHome;

public final class CmRepCashPositionAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CmRepCashPositionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         CmRepCashPositionForm actionForm = (CmRepCashPositionForm)form;
      
	     //reset report to null
         actionForm.setReport(null);
	      
         String frParam = Common.getUserPermission(user, Constants.CM_REP_CASH_POSITION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmRepCashPosition");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize CmRepCashPositionController EJB
*******************************************************/

         CmRepCashPositionControllerHome homeCP = null;
         CmRepCashPositionController ejbCP = null;       

         try {
         	
            homeCP = (CmRepCashPositionControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/CmRepCashPositionControllerEJB", CmRepCashPositionControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in CmRepCashPositionAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCP = homeCP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in CmRepCashPositionAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- CM DL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());

	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (actionForm.getIncludedUnposted()) {	        	
	        		
	        		criteria.put("includedUnposted", "YES");
	        		
	        	}
	        	
	        	if (!actionForm.getIncludedUnposted()) {
	        		
	        		criteria.put("includedUnposted", "NO");
	        		
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
		    try {
		    	
		       // get company
		       AdCompanyDetails adCmpDetails = ejbCP.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();

		       // execute report
		       list = ejbCP.executeCmRepCashPosition(actionForm.getCriteria(), user.getCmpCode());
	           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("cmRepCashPosition.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in CmRepCashPositionAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmRepCashPosition");

            }

            // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    
		    parameters.put("company", company);
		    parameters.put("bankAccount", actionForm.getBankAccount());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			parameters.put("showEntries", actionForm.getShowEntries());
			
			if (actionForm.getIncludedUnposted()){
				parameters.put("includeUnposted", "YES");
			} else {
				parameters.put("includeUnposted", "NO");
			}
			
			
        	ArrayList baList = ejbCP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        	Iterator baIter = baList.iterator();
        	
        	int baCount = baList.size();
        	int ctr=0;
        	String bankAccount[] = new String[baCount];
        	
        	while(baIter.hasNext()) {
        		bankAccount[ctr] = (String)baIter.next();
        		ctr++;
        	}
        	
    		String bankParameters[] = new String[baCount];

    		Iterator i = list.iterator();
    		while(i.hasNext()) {
    			
    			CmRepCashPositionDetails details = (CmRepCashPositionDetails)i.next();
    			
    			for(int x=0; x<bankAccount.length; x++) {
    				
    				if((bankAccount[x].equals(details.getCpAccount()))) {
    					bankParameters[x] = "bankAccountBalance" + (x+1);
		    			parameters.put(bankParameters[x], new Double(details.getCpBegBal()));
    					break;
    				}
    			}
    		}

		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepCashPosition.jasper";
		    
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/CmRepCashPosition.jasper");
		    
	        }    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new CmRepCashPositionDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new CmRepCashPositionDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new CmRepCashPositionDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);		
		       actionForm.setReport(Constants.STATUS_SUCCESS);
		        		       
		    } catch(Exception ex) {
		    	
				ex.printStackTrace();
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in CmRepCashPositionAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- CM DL Close Action --
*******************************************************/

	     } else if (request.getParameter("closeButton") != null) {
	
	          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- CM DL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
		            
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        actionForm.clearBankAccountList();           	
	            	
	            	list = ejbCP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}

				} catch(EJBException ex) {
			    	
					ex.printStackTrace(); 
					
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in CmRepCashPositionAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
				actionForm.setDateFrom(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setDateTo(Common.convertSQLDateToString(new java.util.Date()));
	            return(mapping.findForward("cmRepCashPosition"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	e.printStackTrace(); 
         	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in CmRepCashPositionAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
