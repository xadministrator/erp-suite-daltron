package com.struts.jreports.cm.cashposition;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepCashPositionDetails;

public class CmRepCashPositionDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepCashPositionDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while(i.hasNext()) {
			
			CmRepCashPositionDetails details = (CmRepCashPositionDetails)i.next();
		
			CmRepCashPositionData argData = new CmRepCashPositionData(
					details.getCpDate(),
					details.getCpType(),
					details.getCpAccount(),
					details.getCpDesc(),
					details.getCpNum(),
					details.getCpRefNum(),
					new Double(details.getCpBegBal()),
					new Double(details.getCpAmount()),
					details.getCpDateFrom(),
					details.getCpDateTo());
			
			data.add(argData);
		}
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("date".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpDate();
			
		} else if("type".equals(fieldName)){ 
			
			value = ((CmRepCashPositionData)data.get(index)).getCpType();
			
		} else if("account".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpAccnt();
			
		} else if("desc".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpDesc();
			
		} else if("num".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpNum();
			
		} else if("refNum".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpRefNum();
			
		} else if("begBal".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpBegBal();
			
		} else if("amount".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpAmount();
			
		} else if("dateFrom".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpDateFrom();
			
		} else if("dateTo".equals(fieldName)){
			
			value = ((CmRepCashPositionData)data.get(index)).getCpDateTo();
			
		}
		return(value);
		
	}
	
}
