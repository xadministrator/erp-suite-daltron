package com.struts.jreports.cm.cashposition;

import java.util.Date;


public class CmRepCashPositionData implements java.io.Serializable {

	private Date cpDate;
	private String cpType;
	private String cpAccnt;	
	private String cpDesc;
	private String cpNum;
	private String cpRefNum;
	private Double cpBegBal;
	private Double cpAmount;
	private Date cpDateFrom;
	private Date cpDateTo;
	
		
	public CmRepCashPositionData(
		java.util.Date cpDate,
		String cpType,
		String cpAccnt,
		String cpDesc,
		String cpNum,
		String cpRefNum,
		Double cpBegBal,
		Double cpAmount,
		java.util.Date cpDateFrom,
		java.util.Date cpDateTo) {
		
		this.cpDate = cpDate;
		this.cpType = cpType;
		this.cpAccnt = cpAccnt;
		this.cpDesc = cpDesc;
		this.cpNum = cpNum;
		this.cpRefNum = cpRefNum;
		this.cpBegBal = cpBegBal;
		this.cpAmount = cpAmount;
		this.cpDateFrom = cpDateFrom;
		this.cpDateTo = cpDateTo;
	}
	

	public Date getCpDate(){
		
		return cpDate;
		
	}
	
	public void setCpDate(Date cpDate){
		
		this.cpDate = cpDate;
		
	}
	
	public String getCpType(){
		
		return cpType;
		
	}
	
	public void setCpType(String cpType){
		
		this.cpType = cpType;
		
	}
	
	public String getCpAccnt(){
		
		return cpAccnt;
		
	}
	
	public void setCpAccnt(String cpAccnt){
		
		this.cpAccnt = cpAccnt;
		
	}
	
	public String getCpDesc(){
		
		return cpDesc;
		
	}
	
	public void setCpDesc(String cpDesc){
		
		this.cpDesc = cpDesc;
		
	}
	
	public String getCpNum(){
		
		return cpNum;
		
	}
	
	public void setCpNum(String cpNum){
		
		this.cpNum = cpNum;
		
	}
	
	public String getCpRefNum(){
		
		return cpRefNum;
		
	}
	
	public void setCpRefNum(String cpRefNum){
		
		this.cpRefNum = cpRefNum;
		
	}
	
	public Double getCpBegBal(){
		
		return cpBegBal;
		
	}
	
	public void setCpBegBal(Double cpBegBal){
		
		this.cpBegBal = cpBegBal;
		
	}
	
	public Double getCpAmount(){
		
		return cpAmount;
		
	}
	
	public void setCpAmount(Double CpAmount){
		
		this.cpAmount = cpAmount;
		
	}
	
	public Date getCpDateFrom(){
	
	return cpDateFrom;
	
	}
	
	public void setCpDateFrom(Date cpDateFrom){
		
		this.cpDateFrom = cpDateFrom;
		
	}
	
	public Date getCpDateTo(){
		
		return cpDateTo;
		
	}
	
	public void setCpDateTo(Date cpDateTo){
		
		this.cpDateTo = cpDateTo;
		
	}
}
