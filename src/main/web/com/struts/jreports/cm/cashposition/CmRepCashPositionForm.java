package com.struts.jreports.cm.cashposition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class CmRepCashPositionForm extends ActionForm implements Serializable{
	
	
	private String bankAccount = null;
	private ArrayList bankAccountList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;

	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private boolean includedUnposted = false;
	private boolean showEntries = false;
	
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private HashMap criteria = new HashMap();
	private String userPermission = new String();
	
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getViewType(){
		
		return(viewType);   	
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}	
	
	public String getBankAccount(){
		
		return(bankAccount);
		
	}
	
	public void setBankAccount(String bankAccount){
		
		this.bankAccount = bankAccount;
		
	}
	
	public ArrayList getBankAccountList(){
		
		return(bankAccountList);
		
	}
	
	public void setBankAccountList(String bankAccount){
		
		bankAccountList.add(bankAccount);
		
	}
	
	public void clearBankAccountList(){
		
		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);
		
	}	

	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport (String report) {
		
		this.report = report;
		
	}
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
		
	public boolean getIncludedUnposted() {
		
		return includedUnposted;
		
	}
	
	public void setIncludedUnposted( boolean includedUnposted) {
		
		this.includedUnposted = includedUnposted;
		
	}
	
	public boolean getShowEntries() {
		
		return showEntries;
		
	}
	
	public void setShowEntries( boolean showEntries) {
		
		this.showEntries = showEntries;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){    
		
		goButton = null;
		closeButton = null;
		
		bankAccount = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = null;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		includedUnposted = false;
		showEntries = false;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("cmRepCashPosition.error.dateFromInvalid"));
				
			}
			
			if(Common.validateRequired(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("cmRepCashPosition.error.dateFromRequired"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)){
				
				errors.add("dateTo", new ActionMessage("cmRepCashPosition.error.dateToInvalid"));
				
			}
			
			if(Common.validateRequired(dateTo)){
				
				errors.add("dateTo", new ActionMessage("cmRepCashPosition.error.dateToRequired"));
				
			}
		}
		
		return(errors);
	}
	
}