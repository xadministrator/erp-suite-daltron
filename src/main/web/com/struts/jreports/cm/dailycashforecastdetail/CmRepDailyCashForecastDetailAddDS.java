package com.struts.jreports.cm.dailycashforecastdetail;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashForecastDetailAddDetails;

public class CmRepDailyCashForecastDetailAddDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepDailyCashForecastDetailAddDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	CmRepDailyCashForecastDetailAddDetails details = (CmRepDailyCashForecastDetailAddDetails)i.next();

         CmRepDailyCashForecastDetailAddData argData = new CmRepDailyCashForecastDetailAddData(details.getDcfdaCustomer(),
			details.getDcfdaDescription(),
			details.getDcfdaInvoiceNumber(),
			details.getDcfdaDateTransaction(),
			new Double (details.getDcfdaAmount1()),
			new Double (details.getDcfdaAmount2()),
			new Double (details.getDcfdaAmount3()),
			new Double (details.getDcfdaAmount4()),
			new Double (details.getDcfdaAmount5()),
			new Double (details.getDcfdaAmount6()),
			new Double (details.getDcfdaAmount7()));

         data.add(argData);
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

		if("customer".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getCustomer();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getDescription();
			
		} else if("invoiceNumber".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getInvoiceNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailAddData)data.get(index)).getAmount7();
			
		}

      return(value);
      
   }
}
