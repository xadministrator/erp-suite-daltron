package com.struts.jreports.cm.dailycashforecastdetail;

import java.util.Date;


public class CmRepDailyCashForecastDetailData implements java.io.Serializable {

	private Date date1;
	private Date date2;
	private Date date3;
	private Date date4;
	private Date date5;
	private Date date6;
	private Date date7;

	private Double beginningBalance1;
	private Double beginningBalance2;
	private Double beginningBalance3;
	private Double beginningBalance4;
	private Double beginningBalance5;
	private Double beginningBalance6;
	private Double beginningBalance7;
	
	private Double availableCashBalance1;
	private Double availableCashBalance2;
	private Double availableCashBalance3;
	private Double availableCashBalance4;
	private Double availableCashBalance5;
	private Double availableCashBalance6;
	private Double availableCashBalance7;

	public CmRepDailyCashForecastDetailData(java.util.Date date1,
		java.util.Date date2,
		java.util.Date date3,
		java.util.Date date4,
		java.util.Date date5,
		java.util.Date date6,
		java.util.Date date7,
		Double beginningBalance1,
		Double beginningBalance2,
		Double beginningBalance3,
		Double beginningBalance4,
		Double beginningBalance5,
		Double beginningBalance6,
		Double beginningBalance7,
		Double availableCashBalance1,
		Double availableCashBalance2,
		Double availableCashBalance3,
		Double availableCashBalance4,
		Double availableCashBalance5,
		Double availableCashBalance6,
		Double availableCashBalance7) {
		
		this.date1 = date1;
		this.date2 = date2;
		this.date3 = date3;
		this.date4 = date4;
		this.date5 = date5;
		this.date6 = date6;
		this.date7 = date7;
		this.beginningBalance1 = beginningBalance1;
		this.beginningBalance2 = beginningBalance2;
		this.beginningBalance3 = beginningBalance3;
		this.beginningBalance4 = beginningBalance4;
		this.beginningBalance5 = beginningBalance5;
		this.beginningBalance6 = beginningBalance6;
		this.beginningBalance7 = beginningBalance7;
		this.availableCashBalance1 = availableCashBalance1;
		this.availableCashBalance2 = availableCashBalance2;
		this.availableCashBalance3 = availableCashBalance3;
		this.availableCashBalance4 = availableCashBalance4;
		this.availableCashBalance5 = availableCashBalance5;
		this.availableCashBalance6 = availableCashBalance6;
		this.availableCashBalance7 = availableCashBalance7;	
		
	}
	
	public Date getDate1(){
		
		return date1;
		
	}
	
	public void setDate1(Date date1){
		
		this.date1 = date1;
		
	}
	
	public Date getDate2(){
		
		return date2;
		
	}
	
	public void setDate2(Date date2){
		
		this.date2 = date2;
		
	}
	
	public Date getDate3(){
		
		return date3;

	}
	
	public void setDate3(Date date3){
		
		this.date3 = date3;

	}

	public Date getDate4(){
		
		return date4;		
	}
	
	public void setDate4(Date date4){
		
		this.date4 = date4;		
	}

	public Date getDate5(){

		return date5;

	}

	public void setDate5(Date date5){

		this.date5 = date5;

	}
	
	public Date getDate6(){
		

		return date6;
		
	}

	public void setDate6(Date date6){

		this.date6 = date6;
		
	}
	
	public Date getDate7(){
		return date7;
		
	}

	public void setDate7(Date date7){
		
		this.date7 = date7;
		
	}
	
	public Double getBeginningBalance1(){
		
		return beginningBalance1;
		
	}
	
	public void setBeginningBalance1(Double beginningBalance1){
		
		this.beginningBalance1 = beginningBalance1;
		
	}
	
	public Double getBeginningBalance2(){
		
		return beginningBalance2;
		
	}
	
	public void setBeginningBalance2(Double beginningBalance2){
		
		this.beginningBalance2 = beginningBalance2;
		
	}
	
	public Double getBeginningBalance3(){
		
		return beginningBalance3;

	}
	
	public void setBeginningBalance3(Double beginningBalance3){
		
		this.beginningBalance3 = beginningBalance3;

	}

	public Double getBeginningBalance4(){
		
		return beginningBalance4;		
	}
	
	public void setBeginningBalance4(Double beginningBalance4){
		
		this.beginningBalance4 = beginningBalance4;		
	}

	public Double getBeginningBalance5(){

		return beginningBalance5;

	}

	public void setBeginningBalance5(Double beginningBalance5){

		this.beginningBalance5 = beginningBalance5;

	}
	
	public Double getBeginningBalance6(){
		

		return beginningBalance6;
		
	}

	public void setBeginningBalance6(Double beginningBalance6){

		this.beginningBalance6 = beginningBalance6;
		
	}
	
	public Double getBeginningBalance7(){
		return beginningBalance7;
		
	}

	public void setBeginningBalance7(Double beginningBalance7){
		
		this.beginningBalance7 = beginningBalance7;
		
	}

	public Double getAvailableCashBalance1(){
		
		return availableCashBalance1;
		
	}
	
	public void setAvailableCashBalance1(Double availableCashBalance1){
		
		this.availableCashBalance1 = availableCashBalance1;
		
	}
	
	public Double getAvailableCashBalance2(){
		
		return availableCashBalance2;
		
	}
	
	public void setAvailableCashBalance2(Double availableCashBalance2){
		
		this.availableCashBalance2 = availableCashBalance2;
		
	}
	
	public Double getAvailableCashBalance3(){
		
		return availableCashBalance3;

	}
	
	public void setAvailableCashBalance3(Double availableCashBalance3){
		
		this.availableCashBalance3 = availableCashBalance3;

	}

	public Double getAvailableCashBalance4(){
		
		return availableCashBalance4;		
	}
	
	public void setAvailableCashBalance4(Double availableCashBalance4){
		
		this.availableCashBalance4 = availableCashBalance4;		
	}

	public Double getAvailableCashBalance5(){

		return availableCashBalance5;

	}

	public void setAvailableCashBalance5(Double availableCashBalance5){

		this.availableCashBalance5 = availableCashBalance5;

	}
	
	public Double getAvailableCashBalance6(){
		

		return availableCashBalance6;
		
	}

	public void setAvailableCashBalance6(Double availableCashBalance6){

		this.availableCashBalance6 = availableCashBalance6;
		
	}
	
	public Double getAvailableCashBalance7(){
		
		return availableCashBalance7;
		
	}

	public void setAvailableCashBalance7(Double availableCashBalance7){
		
		this.availableCashBalance7 = availableCashBalance7;
		
	}

}
