package com.struts.jreports.cm.dailycashforecastdetail;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashForecastDetailLessDetails;

public class CmRepDailyCashForecastDetailLessDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepDailyCashForecastDetailLessDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			CmRepDailyCashForecastDetailLessDetails details = (CmRepDailyCashForecastDetailLessDetails)i.next();
			
			CmRepDailyCashForecastDetailLessData argData = new CmRepDailyCashForecastDetailLessData(details.getDcfdlSupplier(),
					details.getDcfdlDescription(),
					details.getDcfdlVoucherNumber(),
					details.getDcfdlDateTransaction(),
					new Double (details.getDcfdlAmount1()),
					new Double (details.getDcfdlAmount2()),
					new Double (details.getDcfdlAmount3()),
					new Double (details.getDcfdlAmount4()),
					new Double (details.getDcfdlAmount5()),
					new Double (details.getDcfdlAmount6()),
					new Double (details.getDcfdlAmount7()));
			
			data.add(argData);
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("supplier".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getSupplier();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getDescription();
			
		} else if("voucherNumber".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getVoucherNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastDetailLessData)data.get(index)).getAmount7();
			
		}
		
		return(value);
		
	}
}
