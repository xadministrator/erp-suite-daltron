package com.struts.jreports.cm.dailycashforecastsummary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmRepDailyCashForecastSummaryController;
import com.ejb.txn.CmRepDailyCashForecastSummaryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.CmRepDailyCashForecastSummaryDetails;

public final class CmRepDailyCashForecastSummaryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CmRepDailyCashForecastSummaryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         CmRepDailyCashForecastSummaryForm actionForm = (CmRepDailyCashForecastSummaryForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.CM_REP_DAILY_CASH_FORECAST_SUMMARY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmRepDailyCashForecastSummary");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize CmRepDailyCashForecastSummaryController EJB
*******************************************************/

         CmRepDailyCashForecastSummaryControllerHome homeDCP = null;
         CmRepDailyCashForecastSummaryController ejbDCP = null;       

         try {
         	
            homeDCP = (CmRepDailyCashForecastSummaryControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/CmRepDailyCashForecastSummaryControllerEJB", CmRepDailyCashForecastSummaryControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in CmRepDailyCashForecastSummaryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDCP = homeDCP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in CmRepDailyCashForecastSummaryAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- CM DL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            CmRepDailyCashForecastSummaryDetails details = new CmRepDailyCashForecastSummaryDetails();
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (actionForm.getSelectedBankAccount().length != 0) {
	        		
	        		criteria.put("selectedBankAccount", actionForm.getSelectedBankAccount());

	        	}

	        	if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReportType())) {
	        		
	        		criteria.put("reportType", actionForm.getReportType());

	        	}

	        	if (actionForm.getIncludedUnposted()) {	        	
	        		
	        		criteria.put("includedUnposted", "YES");
	        		
	        	}
	        	
	        	if (!actionForm.getIncludedUnposted()) {
	        		
	        		criteria.put("includedUnposted", "NO");
	        		
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbDCP.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       // execute report

	           details = ejbDCP.executeCmRepDailyCashForecastSummary(actionForm.getCriteria(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("cmRepDailyCashForecastSummary.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in CmRepDailyCashForecastSummaryAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmRepDailyCashForecastSummary");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getSelectedBankAccount() != null) {
				
				String bankAccounts = "";
				
				for (int i =0; i< actionForm.getSelectedBankAccount().length; i++) {
					
					if (actionForm.getSelectedBankAccount()[i] != null) {
						
						bankAccounts = bankAccounts +  actionForm.getSelectedBankAccount()[i] + " ";
						
					}
					
				}
				
				parameters.put("bankAccount", bankAccounts);
				
			}
			
		    if (actionForm.getDate() != null)  {
		    	
				parameters.put("dateEntered", Common.convertStringToSQLDate(actionForm.getDate()));				
		    
		    }
		    
		    if (actionForm.getReportType() != null) {
		    
				parameters.put("reportType", actionForm.getReportType());	    	
			
		    }

		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includeUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includeUnposted", "NO");
		    	
		    }
		    
		    //assign add subreport
		    
		    String subreportAddFilename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashForecastSummaryAddSubreport.jasper";
		    
		    if (!new java.io.File(subreportAddFilename).exists()) {
		    	
		    	subreportAddFilename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashForecastSummaryAddSubreport.jasper");
		    	
		    }
		    
		    JasperReport subreportAdd = (JasperReport)JRLoader.loadObject(subreportAddFilename);
		    
		    ArrayList list = details.getDcfsAddList();
		    
			parameters.put("cmRepDailyCashForecastSummaryAddData", subreportAdd);
            parameters.put("cmRepDailyCashForecastSummaryAddDS", new CmRepDailyCashForecastSummaryAddDS(list));

		    String subreportLessFilename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashForecastSummaryLessSubreport.jasper";
		    
		    if (!new java.io.File(subreportLessFilename).exists()) {
		    	
		    	subreportLessFilename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashForecastSummaryLessSubreport.jasper");
		    	
		    }
		    
		    //assign less subreport
		    
		    JasperReport subreportLess = (JasperReport)JRLoader.loadObject(subreportLessFilename);
		    
		    list = details.getDcfsLessList();
		    
			parameters.put("cmRepDailyCashForecastSummaryLessData", subreportLess);
            parameters.put("cmRepDailyCashForecastSummaryLessDS", new CmRepDailyCashForecastSummaryLessDS(list));

		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashForecastSummary.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashForecastSummary.jasper");
		    
	        }    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new CmRepDailyCashForecastSummaryDS(details)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new CmRepDailyCashForecastSummaryDS(details)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new CmRepDailyCashForecastSummaryDS(details)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
				ex.printStackTrace();
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in CmRepDailyCashForecastSummaryAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- CM DL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- CM DL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
		            
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        actionForm.clearBankAccountList();           	
	            	
	            	list = ejbDCP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}

				} catch(EJBException ex) {
			    	
					ex.printStackTrace(); 
					
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in CmRepDailyCashForecastSummaryAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
	            return(mapping.findForward("cmRepDailyCashForecastSummary"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	e.printStackTrace(); 
         	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in CmRepDailyCashForecastSummaryAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
