package com.struts.jreports.cm.dailycashforecastsummary;


import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashForecastSummaryDetails;

public class CmRepDailyCashForecastSummaryDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepDailyCashForecastSummaryDS(CmRepDailyCashForecastSummaryDetails details) {
		
		CmRepDailyCashForecastSummaryData argData = new CmRepDailyCashForecastSummaryData(details.getDcfsDate1(),
				details.getDcfsDate2(),
				details.getDcfsDate3(),
				details.getDcfsDate4(),
				details.getDcfsDate5(),
				details.getDcfsDate6(),
				details.getDcfsDate7(),
				new Double(details.getDcfsBeginningBalance1()),
				new Double(details.getDcfsBeginningBalance2()),
				new Double(details.getDcfsBeginningBalance3()),
				new Double(details.getDcfsBeginningBalance4()),
				new Double(details.getDcfsBeginningBalance5()),
				new Double(details.getDcfsBeginningBalance6()),
				new Double(details.getDcfsBeginningBalance7()),
				new Double(details.getDcfsAvailableCashBalance1()),
				new Double(details.getDcfsAvailableCashBalance2()),
				new Double(details.getDcfsAvailableCashBalance3()),
				new Double(details.getDcfsAvailableCashBalance4()),
				new Double(details.getDcfsAvailableCashBalance5()),
				new Double(details.getDcfsAvailableCashBalance6()),
				new Double(details.getDcfsAvailableCashBalance7()));
		
		data.add(argData);
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("date1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate1();
			
		} else if("date2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate2();
			
		} else if("date3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate3();
			
		} else if("date4".equals(fieldName)){ 
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate4();
			
		} else if("date5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate5();
			
		} else if("date6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate6();
			
		} else if("date7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getDate7();
			
		} else if("beginningBalance1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance1();
			
		} else if("beginningBalance2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance2();
			
		} else if("beginningBalance3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance3();
			
		} else if("beginningBalance4".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance4();
			
		} else if("beginningBalance5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance5();
			
		} else if("beginningBalance6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance6();
			
		} else if("beginningBalance7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getBeginningBalance7();
			
		} else if("availableCashBalance1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance1();
			
		} else if("availableCashBalance2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance2();
			
		} else if("availableCashBalance3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance3();
			
		} else if("availableCashBalance4".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance4();
			
		} else if("availableCashBalance5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance5();
			
		} else if("availableCashBalance6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance6();
			
		} else if("availableCashBalance7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryData)data.get(index)).getAvailableCashBalance7();
			
		}

		return(value);
		
	}
	
}
