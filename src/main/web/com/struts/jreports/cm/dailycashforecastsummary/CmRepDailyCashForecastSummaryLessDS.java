package com.struts.jreports.cm.dailycashforecastsummary;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashForecastSummaryLessDetails;

public class CmRepDailyCashForecastSummaryLessDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepDailyCashForecastSummaryLessDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			CmRepDailyCashForecastSummaryLessDetails details = (CmRepDailyCashForecastSummaryLessDetails)i.next();
			
			CmRepDailyCashForecastSummaryLessData argData = new CmRepDailyCashForecastSummaryLessData(details.getDcfslDescription(),
					new Double (details.getDcfslAmount1()),
					new Double (details.getDcfslAmount2()),
					new Double (details.getDcfslAmount3()),
					new Double (details.getDcfslAmount4()),
					new Double (details.getDcfslAmount5()),
					new Double (details.getDcfslAmount6()),
					new Double (details.getDcfslAmount7()));
			
			data.add(argData);
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("description".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getDescription();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepDailyCashForecastSummaryLessData)data.get(index)).getAmount7();
			
		}
		
		return(value);
		
	}
	
}
