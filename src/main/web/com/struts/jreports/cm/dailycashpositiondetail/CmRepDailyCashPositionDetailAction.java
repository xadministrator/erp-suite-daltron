package com.struts.jreports.cm.dailycashpositiondetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmRepDailyCashPositionDetailController;
import com.ejb.txn.CmRepDailyCashPositionDetailControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.CmRepDailyCashPositionDetailDetails;

public final class CmRepDailyCashPositionDetailAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CmRepDailyCashPositionDetailAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         CmRepDailyCashPositionDetailForm actionForm = (CmRepDailyCashPositionDetailForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.CM_REP_DAILY_CASH_POSITION_DETAIL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmRepDailyCashPositionDetail");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize CmRepDailyCashPositionDetailController EJB
*******************************************************/

         CmRepDailyCashPositionDetailControllerHome homeDCP = null;
         CmRepDailyCashPositionDetailController ejbDCP = null;       

         try {
         	
            homeDCP = (CmRepDailyCashPositionDetailControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/CmRepDailyCashPositionDetailControllerEJB", CmRepDailyCashPositionDetailControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in CmRepDailyCashPositionDetailAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDCP = homeDCP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in CmRepDailyCashPositionDetailAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- CM DL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            CmRepDailyCashPositionDetailDetails details = new CmRepDailyCashPositionDetailDetails();
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());

	        	}

	        	if (!Common.validateRequired(actionForm.getDate())) {
	        		
	        		criteria.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReportType())) {
	        		
	        		criteria.put("reportType", actionForm.getReportType());

	        	}

	        	if (actionForm.getIncludedUnposted()) {	        	
	        		
	        		criteria.put("includedUnposted", "YES");
	        		
	        	}
	        	
	        	if (!actionForm.getIncludedUnposted()) {
	        		
	        		criteria.put("includedUnposted", "NO");
	        		
	        	}

	        	/*if (actionForm.getIncludedDirectChecks()) {	        	

	        		criteria.put("includedDirectChecks", "YES");

	        	}

	        	if (!actionForm.getIncludedDirectChecks()) {

	        		criteria.put("includedDirectChecks", "NO");

	        	}*/

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbDCP.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       // execute report

	           details = ejbDCP.executeCmRepDailyCashPositionDetail(actionForm.getCriteria(), actionForm.getIncludedDirectChecks(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("cmRepDailyCashPositionDetail.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in CmRepDailyCashPositionDetailAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmRepDailyCashPositionDetail");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getBankAccount() != null) {
			
				parameters.put("bankAccount", actionForm.getBankAccount());
				
			}
			
		    if (actionForm.getDate() != null)  {
		    	
				parameters.put("dateEntered", Common.convertStringToSQLDate(actionForm.getDate()));				
		    
		    }
		    
		    if (actionForm.getReportType() != null) {
		    
				parameters.put("reportType", actionForm.getReportType());	    	
			
		    }

		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");

		    }

		    if (actionForm.getIncludedDirectChecks()) {

		    	parameters.put("includedDirectChecks", "YES");

		    } else {

		    	parameters.put("includedDirectChecks", "NO");

		    }
		    
		    //assign add subreport
		    
		    String subreportAddFilename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashPositionDetailAddSubreport.jasper";
		    
		    if (!new java.io.File(subreportAddFilename).exists()) {
		    	
		    	subreportAddFilename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashPositionDetailAddSubreport.jasper");
		    	
		    }
		    
		    JasperReport subreportAdd = (JasperReport)JRLoader.loadObject(subreportAddFilename);
		    
		    ArrayList list = details.getDcpdAddList();
		    
			parameters.put("cmRepDailyCashPositionDetailAddData", subreportAdd);
            parameters.put("cmRepDailyCashPositionDetailAddDS", new CmRepDailyCashPositionDetailAddDS(list));

		    String subreportLessFilename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashPositionDetailLessSubreport.jasper";
		    
		    if (!new java.io.File(subreportLessFilename).exists()) {
		    	
		    	subreportLessFilename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashPositionDetailLessSubreport.jasper");
		    	
		    }
		    
		    //assign less subreport
		    
		    JasperReport subreportLess = (JasperReport)JRLoader.loadObject(subreportLessFilename);
		    
		    list = details.getDcpdLessList();
		    
			parameters.put("cmRepDailyCashPositionDetailLessData", subreportLess);
            parameters.put("cmRepDailyCashPositionDetailLessDS", new CmRepDailyCashPositionDetailLessDS(list));

		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDailyCashPositionDetail.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/CmRepDailyCashPositionDetail.jasper");
		    
	        }    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new CmRepDailyCashPositionDetailDS(details)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new CmRepDailyCashPositionDetailDS(details)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new CmRepDailyCashPositionDetailDS(details)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
				ex.printStackTrace();
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in CmRepDailyCashPositionDetailAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- CM DL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- CM DL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
		            
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        actionForm.clearBankAccountList();           	
	            	
	            	list = ejbDCP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}

				} catch(EJBException ex) {
			    	
					ex.printStackTrace(); 
					
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in CmRepDailyCashPositionDetailAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
	            return(mapping.findForward("cmRepDailyCashPositionDetail"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	e.printStackTrace(); 
         	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in CmRepDailyCashPositionDetailAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
