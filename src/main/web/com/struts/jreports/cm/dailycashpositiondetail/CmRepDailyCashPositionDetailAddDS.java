package com.struts.jreports.cm.dailycashpositiondetail;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashPositionDetailAddDetails;

public class CmRepDailyCashPositionDetailAddDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepDailyCashPositionDetailAddDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	CmRepDailyCashPositionDetailAddDetails details = (CmRepDailyCashPositionDetailAddDetails)i.next();

         CmRepDailyCashPositionDetailAddData argData = new CmRepDailyCashPositionDetailAddData(details.getDcpdaCustomer(),
			details.getDcpdaDescription(),
			details.getDcpdaReceiptNumber(),
			details.getDcpdaDateTransaction(),
			new Double (details.getDcpdaAmount1()),
			new Double (details.getDcpdaAmount2()),
			new Double (details.getDcpdaAmount3()),
			new Double (details.getDcpdaAmount4()),
			new Double (details.getDcpdaAmount5()),
			new Double (details.getDcpdaAmount6()),
			new Double (details.getDcpdaAmount7()));

         data.add(argData);
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

		if("customer".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getCustomer();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getDescription();
			
		} else if("receiptNumber".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getReceiptNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailAddData)data.get(index)).getAmount7();
			
		}

      return(value);
      
   }
}
