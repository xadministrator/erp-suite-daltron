package com.struts.jreports.cm.dailycashpositiondetail;

import java.util.Date;


public class CmRepDailyCashPositionDetailAddData implements java.io.Serializable {

	private String customer;
	private String description;
	private String receiptNumber;
	private Date dateTransaction;

	private Double amount1;
	private Double amount2;
	private Double amount3;
	private Double amount4;
	private Double amount5;
	private Double amount6;
	private Double amount7;
	
	public CmRepDailyCashPositionDetailAddData(String customer,
		String description,
		String receiptNumber,
		Date dateTransaction,
		Double amount1,
		Double amount2,
		Double amount3,
		Double amount4,
		Double amount5,
		Double amount6,
		Double amount7) {
		
		this.customer = customer;
		this.description = description;
		this.receiptNumber = receiptNumber;
		this.dateTransaction = dateTransaction;
		this.amount1 = amount1;
		this.amount2 = amount2;
		this.amount3 = amount3;
		this.amount4 = amount4;
		this.amount5 = amount5;
		this.amount6 = amount6;
		this.amount7 = amount7;
		
	}
	
	public Date getDateTransaction(){
		
		return dateTransaction;
		
	}
	
	public void setDateTransaction(Date dateTransaction){
		
		this.dateTransaction = dateTransaction;
		
	}
	
	public String getCustomer(){
		
		return customer;
		
	}
	
	public void setCustomer(String customer){
		
		this.customer = customer;
		
	}
	
	public String getDescription(){
		
		return description;

	}
	
	public void setDescription(String description){
		
		this.description = description;

	}

	public String getReceiptNumber(){
		
		return receiptNumber;		
	}
	
	public void setReceiptNumber(String receiptNumber){
		
		this.receiptNumber = receiptNumber;		
	}

	public Double getAmount1(){
		
		return amount1;
		
	}
	
	public void setAmount1(Double amount1){
		
		this.amount1 = amount1;
		
	}
	
	public Double getAmount2(){
		
		return amount2;
		
	}
	
	public void setAmount2(Double amount2){
		
		this.amount2 = amount2;
		
	}
	
	public Double getAmount3(){
		
		return amount3;

	}
	
	public void setAmount3(Double amount3){
		
		this.amount3 = amount3;

	}

	public Double getAmount4(){
		
		return amount4;		
	}
	
	public void setAmount4(Double amount4){
		
		this.amount4 = amount4;		
	}

	public Double getAmount5(){

		return amount5;

	}

	public void setAmount5(Double amount5){

		this.amount5 = amount5;

	}
	
	public Double getAmount6(){
		

		return amount6;
		
	}

	public void setAmount6(Double amount6){

		this.amount6 = amount6;
		
	}
	
	public Double getAmount7(){
		return amount7;
		
	}

	public void setAmount7(Double amount7){
		
		this.amount7 = amount7;
		
	}

}
