package com.struts.jreports.cm.dailycashpositiondetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class CmRepDailyCashPositionDetailForm extends ActionForm implements Serializable{
	
	private String bankAccount = null;
	private ArrayList bankAccountList = new ArrayList();
	private String date = null;
	private String reportType = null;
	
	private ArrayList reportTypeList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private boolean includedUnposted = false;
	private boolean includedDirectChecks = false;
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getReportType() {
		
		return reportType;
		
	}
	
	public void setReportType(String reportType) {
		
		this.reportType = reportType;
		
	}
	
	public ArrayList getReportTypeList(){
		
		return reportTypeList;
		
	}
	
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getBankAccount(){
		
		return(bankAccount);
		
	}
	
	public void setBankAccount(String bankAccount){
		
		this.bankAccount = bankAccount;
		
	}
	
	public ArrayList getBankAccountList(){
		
		return(bankAccountList);
		
	}
	
	public void setBankAccountList(String bankAccount){
		
		bankAccountList.add(bankAccount);
		
	}
	
	public void clearBankAccountList(){
		
		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	public String getViewType(){
		
		return(viewType);   	
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public boolean getIncludedUnposted() {
		
		return includedUnposted;
		
	}
	
	public void setIncludedUnposted( boolean includedUnposted) {

		this.includedUnposted = includedUnposted;

	}

	public boolean getIncludedDirectChecks() {

		return includedDirectChecks;

	}

	public void setIncludedDirectChecks( boolean includedDirectChecks) {

		this.includedDirectChecks = includedDirectChecks;

	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){    
		
		goButton = null;
		closeButton = null;
		
		bankAccount = Constants.GLOBAL_BLANK;
		date = null;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		includedUnposted = false;
		includedDirectChecks = false;
		
		reportTypeList.clear();
		reportTypeList.add("ONE DAY");
		reportTypeList.add("FIVE DAYS");
		reportTypeList.add("SEVEN DAYS");
		reportType = "ONE DAY";
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(date)){
				
				errors.add("date", new ActionMessage("cmRepDailyCashPositionDetail.error.dateInvalid"));
				
			}
			
			if(Common.validateRequired(date)){
				
				errors.add("date", new ActionMessage("cmRepDailyCashPositionDetail.error.dateRequired"));
				
			}
			
			if(!Common.validateStringExists(bankAccountList, bankAccount)){
				
				errors.add("bankAccount", new ActionMessage("cmRepDailyCashPositionDetail.error.bankAccountInvalid"));
				
			}
			
			if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				
				errors.add("bankAccount", new ActionMessage("cmRepDailyCashPositionDetail.error.bankAccountRequired"));
				
			}
			
		}
		
		return(errors);
	}
	
}