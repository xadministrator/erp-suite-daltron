package com.struts.jreports.cm.dailycashpositiondetail;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashPositionDetailLessDetails;

public class CmRepDailyCashPositionDetailLessDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepDailyCashPositionDetailLessDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			CmRepDailyCashPositionDetailLessDetails details = (CmRepDailyCashPositionDetailLessDetails)i.next();
			
			CmRepDailyCashPositionDetailLessData argData = new CmRepDailyCashPositionDetailLessData(details.getDcpdlSupplier(),
					details.getDcpdlDescription(),
					details.getDcpdlCheckNumber(),
					details.getDcpdlDateTransaction(),
					new Double (details.getDcpdlAmount1()),
					new Double (details.getDcpdlAmount2()),
					new Double (details.getDcpdlAmount3()),
					new Double (details.getDcpdlAmount4()),
					new Double (details.getDcpdlAmount5()),
					new Double (details.getDcpdlAmount6()),
					new Double (details.getDcpdlAmount7()));
			
			data.add(argData);
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("supplier".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getSupplier();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getDescription();
			
		} else if("checkNumber".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getCheckNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionDetailLessData)data.get(index)).getAmount7();
			
		}
		
		return(value);
		
	}
}
