package com.struts.jreports.cm.dailycashpositionsummary;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashPositionSummaryAddDetails;

public class CmRepDailyCashPositionSummaryAddDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepDailyCashPositionSummaryAddDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	CmRepDailyCashPositionSummaryAddDetails details = (CmRepDailyCashPositionSummaryAddDetails)i.next();

         CmRepDailyCashPositionSummaryAddData argData = new CmRepDailyCashPositionSummaryAddData(details.getDcpsaDescription(),
			new Double (details.getDcpsaAmount1()),
			new Double (details.getDcpsaAmount2()),
			new Double (details.getDcpsaAmount3()),
			new Double (details.getDcpsaAmount4()),
			new Double (details.getDcpsaAmount5()),
			new Double (details.getDcpsaAmount6()),
			new Double (details.getDcpsaAmount7()));

         data.add(argData);
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("description".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getDescription();
      	
      } else if("amount1".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount1();
      	
      } else if("amount2".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount2();
      	
      } else if("amount3".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount3();
      	
      } else if("amount4".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount4();
      	
      } else if("amount5".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount5();
      	
      } else if("amount6".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount6();
      	
      } else if("amount7".equals(fieldName)){
      	
      	value = ((CmRepDailyCashPositionSummaryAddData)data.get(index)).getAmount7();
      	
      }
      
      return(value);
      
   }
   
}
