package com.struts.jreports.cm.dailycashpositionsummary;


import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDailyCashPositionSummaryDetails;

public class CmRepDailyCashPositionSummaryDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepDailyCashPositionSummaryDS(CmRepDailyCashPositionSummaryDetails details) {
		
		CmRepDailyCashPositionSummaryData argData = new CmRepDailyCashPositionSummaryData(details.getDcpsDate1(),
				details.getDcpsDate2(),
				details.getDcpsDate3(),
				details.getDcpsDate4(),
				details.getDcpsDate5(),
				details.getDcpsDate6(),
				details.getDcpsDate7(),
				new Double(details.getDcpsBeginningBalance1()),
				new Double(details.getDcpsBeginningBalance2()),
				new Double(details.getDcpsBeginningBalance3()),
				new Double(details.getDcpsBeginningBalance4()),
				new Double(details.getDcpsBeginningBalance5()),
				new Double(details.getDcpsBeginningBalance6()),
				new Double(details.getDcpsBeginningBalance7()),
				new Double(details.getDcpsAvailableCashBalance1()),
				new Double(details.getDcpsAvailableCashBalance2()),
				new Double(details.getDcpsAvailableCashBalance3()),
				new Double(details.getDcpsAvailableCashBalance4()),
				new Double(details.getDcpsAvailableCashBalance5()),
				new Double(details.getDcpsAvailableCashBalance6()),
				new Double(details.getDcpsAvailableCashBalance7()));
		
		data.add(argData);
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("date1".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate1();
			
		} else if("date2".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate2();
			
		} else if("date3".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate3();
			
		} else if("date4".equals(fieldName)){ 
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate4();
			
		} else if("date5".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate5();
			
		} else if("date6".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate6();
			
		} else if("date7".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getDate7();
			
		} else if("beginningBalance1".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance1();
			
		} else if("beginningBalance2".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance2();
			
		} else if("beginningBalance3".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance3();
			
		} else if("beginningBalance4".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance4();
			
		} else if("beginningBalance5".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance5();
			
		} else if("beginningBalance6".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance6();
			
		} else if("beginningBalance7".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getBeginningBalance7();
			
		} else if("availableCashBalance1".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance1();
			
		} else if("availableCashBalance2".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance2();
			
		} else if("availableCashBalance3".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance3();
			
		} else if("availableCashBalance4".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance4();
			
		} else if("availableCashBalance5".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance5();
			
		} else if("availableCashBalance6".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance6();
			
		} else if("availableCashBalance7".equals(fieldName)){
			
			value = ((CmRepDailyCashPositionSummaryData)data.get(index)).getAvailableCashBalance7();
			
		}

		return(value);
		
	}
	
}
