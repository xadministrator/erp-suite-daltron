package com.struts.jreports.cm.depositlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmRepDepositListController;
import com.ejb.txn.CmRepDepositListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class CmRepDepositListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CmRepDepositListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         CmRepDepositListForm actionForm = (CmRepDepositListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.CM_REP_DEPOSIT_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmRepDepositList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize CmRepDepositListController EJB
*******************************************************/

         CmRepDepositListControllerHome homeDL = null;
         CmRepDepositListController ejbDL = null;       

         try {
         	
            homeDL = (CmRepDepositListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/CmRepDepositListControllerEJB", CmRepDepositListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in CmRepDepositListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDL = homeDL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in CmRepDepositListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- CM DL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerType())) {
	        		
	        		criteria.put("customerType", actionForm.getCustomerType());
	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptType())) {
	        		
	        		criteria.put("receiptType", actionForm.getReceiptType());
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberFrom())) {
	        		
	        		criteria.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberTo())) {
	        		
	        		criteria.put("receiptNumberTo", actionForm.getReceiptNumberTo());
	        		
	        	}
	        	
	        	if (actionForm.getIncludedUnposted()) {	        	
	        		
	        		criteria.put("includedUnposted", "YES");
	        		
	        	}
	        	
	        	if (!actionForm.getIncludedUnposted()) {
	        		
	        		criteria.put("includedUnposted", "NO");
	        		
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
		    try {
		    	
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbDL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       ArrayList branchList = new ArrayList();
	           
	           for(int i=0; i<actionForm.getArRepBrOrListSize(); i++) {
	           	
	           		CmRepBranchDepositList brOrList = (CmRepBranchDepositList)actionForm.getArRepBrOrListByIndex(i);
	           	
	           		if(brOrList.getBranchCheckbox() == true) {
	           			
	           			AdBranchDetails brDetails = new AdBranchDetails();
	    	           	brDetails.setBrCode(brOrList.getBranchCode());
	           			
	           			branchList.add(brDetails);
	           		}
	           		
	           }
	           
		       // execute report
		    		    
		       list = ejbDL.executeCmRepDepositList(actionForm.getCriteria(), branchList, actionForm.getOrderBy(),
		       		actionForm.getGroupBy(), actionForm.getStatus(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("depositList.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in CmRepDepositListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmRepDepositList");

            }		    

		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getCustomerCode() != null) {
			
				parameters.put("customerCode", actionForm.getCustomerCode());
				
			}
			
			if (actionForm.getBankAccount() != null) {
			
				parameters.put("bankAccount", actionForm.getBankAccount());
				
			}
			
			if (actionForm.getCustomerType() != null) {
			
				parameters.put("customerType", actionForm.getCustomerType());
				
			}
			
			if (actionForm.getCustomerClass() != null) {
				
				parameters.put("customerClass", actionForm.getCustomerClass());	
		    
		    }
		    
		    if (actionForm.getDateFrom() != null)  {
		    	
				parameters.put("dateFrom", actionForm.getDateFrom());				
		    
		    }
		    
		    if (actionForm.getDateTo() != null) {
		    
				parameters.put("dateTo", actionForm.getDateTo());	    	
			
		    }
		    
		    if (actionForm.getReceiptType() != null) {
		    	
		    	parameters.put("receiptType", actionForm.getReceiptType());
		    	
		    }
	 
			parameters.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
		    parameters.put("receiptNumberTo", actionForm.getReceiptNumberTo());
		    parameters.put("status", actionForm.getStatus());
		    
		    if (actionForm.getIncludedUnposted()) {
		    	
		    	parameters.put("includedUnposted", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includedUnposted", "NO");
		    	
		    }
		    
		    parameters.put("groupBy", actionForm.getGroupBy());
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getArRepBrOrListSize(); j++) {

      			CmRepBranchDepositList brList = (CmRepBranchDepositList)actionForm.getArRepBrOrListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/CmRepDepositList.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/CmRepDepositList.jasper");
		    
	        }    		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new CmRepDepositListDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new CmRepDepositListDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new CmRepDepositListDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in CmRepDepositListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- CM DL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- CM DL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
		            
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        actionForm.clearBankAccountList();           	
	            	
	            	list = ejbDL.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setBankAccountList((String)i.next());
	            			
	            		}
	            		
	            	}
			       
	            	actionForm.clearCustomerTypeList();           	
	            	
	            	list = ejbDL.getArCtAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            			actionForm.setCustomerTypeList((String)i.next());
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearCustomerClassList();           	
	            	
	            	list = ejbDL.getArCcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCustomerClassList((String)i.next());
	            			
	            		}
	            		
	            	}   
			       
	            	actionForm.clearArRepBrOrList();
	         		
	         		list = ejbDL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
	         		
	         		i = list.iterator();
	         		
	         		while(i.hasNext()) {
	         			
	         			AdBranchDetails details = (AdBranchDetails)i.next();
	         			
	         			CmRepBranchDepositList arRepBrOrList = new CmRepBranchDepositList(actionForm, 
	         					details.getBrBranchCode(), details.getBrName(), details.getBrCode());
	         			
	         			arRepBrOrList.setBranchCheckbox(true);
	         			
	         			actionForm.saveArRepBrOrList(arRepBrOrList);
	         			
	         		}
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in CmRepDepositListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("cmRepDepositList"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in CmRepDepositListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
