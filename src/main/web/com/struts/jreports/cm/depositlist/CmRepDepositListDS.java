package com.struts.jreports.cm.depositlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepDepositListDetails;

public class CmRepDepositListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepDepositListDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         CmRepDepositListDetails details = (CmRepDepositListDetails)i.next();
         
         String group = null;
         
         if (groupBy.equals("CUSTOMER CODE")) {
         	
         	group = details.getDlCstCustomerCode();
         	
         } else if (groupBy.equals("CUSTOMER TYPE")) {
         	
         	group = details.getDlCstCustomerType();
         	
         } else if (groupBy.equals("CUSTOMER CLASS")) {
         	
         	group = details.getDlCstCustomerClass();
         	
         }
                  
	     CmRepDepositListData argData = new CmRepDepositListData(details.getDlDate(), 
	     details.getDlCstCustomerCode(), details.getDlDescription(), 
	     details.getDlReceiptNumber(), details.getDlReferenceNumber(),
		 new Double(details.getDlAmount()), new Double(details.getDlAmountDeposited()), group);
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("receiptDate".equals(fieldName)){
         value = ((CmRepDepositListData)data.get(index)).getReceiptDate();
      }else if("customerCode".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getCustomerCode();
      }else if("description".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getDescription();     
      }else if("receiptNumber".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getReceiptNumber();                 
      }else if("referenceNumber".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getReferenceNumber();                 
      }else if("amount".equals(fieldName)){
         value = ((CmRepDepositListData)data.get(index)).getAmount();
      }else if("amountDeposited".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getAmountDeposited();     
      }else if("groupBy".equals(fieldName)){
        value = ((CmRepDepositListData)data.get(index)).getGroupBy();
      }

      return(value);
   }
}
