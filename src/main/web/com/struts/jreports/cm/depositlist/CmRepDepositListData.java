package com.struts.jreports.cm.depositlist;


public class CmRepDepositListData implements java.io.Serializable {

   private java.util.Date receiptDate = null;
   private String customerCode = null;
   private String description = null;
   private String receiptNumber = null;
   private String referenceNumber = null;
   private Double amount = null;
   private Double amountDeposited = null;
   private String groupBy = null;
   
   public CmRepDepositListData(java.util.Date receiptDate, String customerCode,
   String description, String receiptNumber,  String referenceNumber, 
   Double amount, Double amountDeposited, String groupBy) {
      	
      	this.receiptDate = receiptDate;
      	this.customerCode = customerCode;
      	this.description = description;
      	this.receiptNumber = receiptNumber;
      	this.referenceNumber = referenceNumber;
      	this.amount = amount;
      	this.amountDeposited = amountDeposited;
      	this.groupBy = groupBy;
      	
   }

   public java.util.Date getReceiptDate() {
   	
      return receiptDate;
      
   }

   public String getCustomerCode() {
   	
   	  return customerCode;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }

   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   public Double getAmountDeposited() {
   	
   	  return amountDeposited;
   	  
   }

      
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
    
}
