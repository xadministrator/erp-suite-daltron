package com.struts.jreports.cm.releasedchecks;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepReleasedChecksDetails;

public class CmRepReleasedChecksDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepReleasedChecksDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         CmRepReleasedChecksDetails details = (CmRepReleasedChecksDetails)i.next();
             
         String group = null;
         
         if (groupBy.equals("SUPPLIER CODE")) {
         	
         	group = details.getRrcSplSupplierCode();
         	
         } else if (groupBy.equals("SUPPLIER TYPE")) {
         	
         	group = details.getRrcSplSupplierType();
         	
         } else if (groupBy.equals("SUPPLIER CLASS")) {
         	
         	group = details.getRrcSplSupplierClass();
         	
         }
         
	     CmRepReleasedChecksData argData = new CmRepReleasedChecksData(details.getRrcDate(), 
	     details.getRrcSplSupplierCode(), details.getRrcDescription(), 
		 details.getRrcCheckNumber(), details.getRrcReferenceNumber(), 
		 new Double(details.getRrcAmount()), group, details.getRrcChkReleased(),
		 details.getRrcChkDate(),details.getRrcChkDateReleased(), details.getRrcVouDate(),
		 details.getRrcVouReferenceNumber(), details.getRrcVpsDueDate(), details.getRrcMemo(), 
		 details.getVouDueDate());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("checkDate".equals(fieldName)){
         value = ((CmRepReleasedChecksData)data.get(index)).getCheckDate();       
      }else if("supplierCode".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getSupplierCode();
      }else if("description".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getDescription();
      }else if("checkNumber".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getCheckNumber();
      }else if("referenceNumber".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getReferenceNumber();
      }else if("amount".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getAmount();  
      }else if("groupBy".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getGroupBy();
      }else if("status".equals(fieldName)){
        value = ((CmRepReleasedChecksData)data.get(index)).getReleaseStatus();
      }else if("rrcCheckDate".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getRrcCheckDate();        
      }else if("checkReleaseDate".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getCheckReleaseDate();        
      }else if("vouDate".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getRrcVouDate();   
      }else if("vouReferenceNumber".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getVouReferenceNumber();
      }else if("vpsDueDate".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getVpsDueDate();
      }else if("memo".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getMemo();        
      }else if("vouDueDate".equals(fieldName)){
          value = ((CmRepReleasedChecksData)data.get(index)).getVouDueDate();        
      }

      return(value);
   }
}
