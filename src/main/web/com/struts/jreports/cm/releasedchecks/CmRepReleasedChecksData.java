package com.struts.jreports.cm.releasedchecks;


public class CmRepReleasedChecksData implements java.io.Serializable{
	
   private java.util.Date checkDate = null;
   private String supplierCode = null;
   private String description = null;
   private String checkNumber = null;
   private String referenceNumber = null;
   private Double amount = null;
   private String groupBy = null;
   private String releaseStatus = null;
   
   private java.util.Date rrcCheckDate = null;
   private java.util.Date checkReleaseDate = null;
   private java.util.Date vouDate = null;
   private String vouReferenceNumber = null;
   private java.util.Date vpsDueDate = null;
   private String memo = null;
   private java.util.Date vouDueDate = null;
      
   public CmRepReleasedChecksData(java.util.Date checkDate,  String supplierCode, String description,
   String checkNumber, String referenceNumber,  Double amount, String groupBy, String releaseStatus,
   java.util.Date rrcCheckDate, java.util.Date checkReleaseDate, java.util.Date rrcVouDate, 
   String vouReferenceNumber, java.util.Date vpsDueDate, String memo, java.util.Date vouDueDate) {
      	
      	this.checkDate = checkDate;
      	this.supplierCode = supplierCode;
      	this.description = description;
      	this.checkNumber = checkNumber;
      	this.referenceNumber = referenceNumber;
      	this.amount = amount;
      	this.groupBy = groupBy;
      	this.releaseStatus = releaseStatus;
      	this.rrcCheckDate = rrcCheckDate;
      	this.checkReleaseDate = checkReleaseDate;
      	this.vouDate = rrcVouDate;
      	this.vouReferenceNumber = vouReferenceNumber;
      	this.vpsDueDate = vpsDueDate;
      	this.memo = memo;
      	this.vouDueDate = vouDueDate;
      	      	
   }

   public java.util.Date getCheckDate() {
   	
      return checkDate;
      
   }
   
   public String getSupplierCode() {
   	
   	  return supplierCode;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;

   }
      
   public Double getAmount() {
   	
   	  return amount;
   	  
   }
   
   
   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public String getReleaseStatus() {
   	
   	  return releaseStatus;
   	  
   }

   
   public java.util.Date getRrcCheckDate() {
   	
   	  return rrcCheckDate;
   	  
   }
  
   public java.util.Date getCheckReleaseDate() {
   	
   	  return checkReleaseDate;
   	  
   }
   
   public java.util.Date getRrcVouDate() {
	   	
	   	  return vouDate;
	   	  
   }

   public String getVouReferenceNumber() {
	   	
	   	  return vouReferenceNumber;
	   	  
   }
   
   public java.util.Date getVpsDueDate() {
	   	
	   	  return vpsDueDate;
	   	  
   }
   
   public String getMemo() {
	   	
	   	  return memo;
	   	  
   }
   
   public java.util.Date getVouDueDate() {
	   
	   return vouDueDate;
	   
   }

}
