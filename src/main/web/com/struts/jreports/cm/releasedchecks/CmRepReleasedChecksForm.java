package com.struts.jreports.cm.releasedchecks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class CmRepReleasedChecksForm extends ActionForm implements Serializable{

   private String supplierCode = null;
   private String supplierType = null;
   private ArrayList supplierTypeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String checkNumberFrom = null;
   private String checkNumberTo = null;   
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private boolean includedUnposted = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String groupBy = null;
   private ArrayList groupByList = new ArrayList();
   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();   
   private String checkType = null;
   private ArrayList checkTypeList = new ArrayList();

   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList apRepBrChckRgstrList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getSupplierCode(){
      return(supplierCode);
   }

   public void setSupplierCode(String supplierCode){
      this.supplierCode = supplierCode;
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }

   public String getCheckNumberFrom(){
      return(checkNumberFrom);
   }

   public void setCheckNumberFrom(String checkNumberFrom){
      this.checkNumberFrom = checkNumberFrom;
   }

   public String getCheckNumberTo(){
      return(checkNumberTo);
   }

   public void setCheckNumberTo(String checkNumberTo){
      this.checkNumberTo = checkNumberTo;
   }

   public String getDocumentNumberFrom(){
      return(documentNumberFrom);
   }

   public void setDocumentNumberFrom(String documentNumberFrom){
      this.documentNumberFrom = documentNumberFrom;
   }

   public String getDocumentNumberTo(){
      return(documentNumberTo);
   }

   public void setDocumentNumberTo(String documentNumberTo){
      this.documentNumberTo = documentNumberTo;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getBankAccount(){
      return(bankAccount);
   }

   public void setBankAccount(String bankAccount){
      this.bankAccount = bankAccount;
   }

   public ArrayList getBankAccountList(){
      return(bankAccountList);
   }

   public void setBankAccountList(String bankAccount){
      bankAccountList.add(bankAccount);
   }

   public void clearBankAccountList(){
      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
   }

   public String getSupplierType(){
      return(supplierType);
   }

   public void setSupplierType(String supplierType){
      this.supplierType = supplierType;
   }

   public ArrayList getSupplierTypeList(){
      return(supplierTypeList);
   }

   public void setSupplierTypeList(String supplierType){
   	supplierTypeList.add(supplierType);
   }

   public void clearSupplierTypeList(){
   	supplierTypeList.clear();
   	supplierTypeList.add(Constants.GLOBAL_BLANK);
   }
 
   public String getSupplierClass(){
      return(supplierClass);
   }

   public void setSupplierClass(String supplierClass){
      this.supplierClass = supplierClass;
   }

   public ArrayList getSupplierClassList(){
      return(supplierClassList);
   }

   public void setSupplierClassList(String supplierClass){
      supplierClassList.add(supplierClass);
   }

   public void clearSupplierClassList(){
      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
   } 

   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
  
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }

   public Object[] getCmRepBrRlsngChckList(){
       
       return apRepBrChckRgstrList.toArray();
       
   }
   
   public CmRepBranchReleasedChecksList getCmRepBrRlsngChckByIndex(int index){
       
       return ((CmRepBranchReleasedChecksList)apRepBrChckRgstrList.get(index));
       
   }
   
   public int getCmRepBrRlsngChckListSize(){
       
       return(apRepBrChckRgstrList.size());
       
   }
   
   public void saveCmRepBrRlsngChckList(Object newCmRepBrRlsngChckList){
       
       apRepBrChckRgstrList.add(newCmRepBrRlsngChckList);   	  
       
   }
   
   public void clearCmRepBrRlsngChckList(){
       
       apRepBrChckRgstrList.clear();
       
   }
   
   public void setCmRepBrRlsngChckList(ArrayList apRepBrChckRgstrList) {
       
       this.apRepBrChckRgstrList = apRepBrChckRgstrList;
       
   }

   public String getStatus() {
   	
   	  return status;
   	
   }
   
   public void setStatus(String status) {
   	
   	  this.status = status;
   	
   }
   
   public ArrayList getStatusList() {
   	
   	  return statusList;
   	
   }
   
   public void setStatusList(ArrayList statusList) {
   	
   	  this.statusList = statusList;
   	
   }
   
   public String getCheckType(){
   	
   	  return(checkType);
   	  
   }
   
   public void setCheckType(String checkType){
   	
   	  this.checkType = checkType;
   	  
   }
   
   public ArrayList getCheckTypeList(){
   	
   	  return(checkTypeList);
   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<apRepBrChckRgstrList.size(); i++) {
           
           CmRepBranchReleasedChecksList  list = (CmRepBranchReleasedChecksList)apRepBrChckRgstrList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
      goButton = null;
      closeButton = null;
      supplierCode = null;
      supplierClass = Constants.GLOBAL_BLANK; 
      supplierType = Constants.GLOBAL_BLANK;
      bankAccount = Constants.GLOBAL_BLANK;
      checkTypeList.clear();
      checkTypeList.add(Constants.GLOBAL_BLANK);
      checkTypeList.add(Constants.AP_FR_CHECK_TYPE_PAYMENT);
      checkTypeList.add(Constants.AP_FR_CHECK_TYPE_DIRECT);
      checkType = Constants.GLOBAL_BLANK; 
      dateFrom = null;
      dateTo = null;
      checkNumberFrom = null;
      checkNumberTo = null;      
      documentNumberFrom = null;
      documentNumberTo = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      orderByList.clear();
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_DATE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_BANK_ACCOUNT);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_CODE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_TYPE);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_CHECK_NUMBER);
      orderByList.add(Constants.AP_CHECK_REGISTER_ORDER_BY_DOCUMENT_NUMBER);
      orderBy = Constants.AP_CHECK_REGISTER_ORDER_BY_DATE;
      groupByList.clear();
      groupByList.add(Constants.GLOBAL_BLANK);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CODE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_TYPE);
      groupByList.add(Constants.AP_SL_ORDER_BY_SUPPLIER_CLASS);
	  includedUnposted = false;

	  statusList.clear();
	  statusList.add(Constants.GLOBAL_BLANK);
	  statusList.add("UNRELEASED");
	  statusList.add("RELEASED");
	  status = Constants.GLOBAL_BLANK;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("apCheckRegister.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("apCheckRegister.error.dateToInvalid"));
		 }
		 if(!Common.validateStringExists(bankAccountList, bankAccount)){
	            errors.add("bankAccount", new ActionMessage("apCheckRegister.error.bankAccountInvalid"));
		 }		 		 		 
		 if(!Common.validateStringExists(supplierTypeList, supplierType)){
	            errors.add("supplierType", new ActionMessage("apCheckRegister.error.supplierTypeInvalid"));
		 }
		 if(!Common.validateStringExists(supplierClassList, supplierClass)){
	            errors.add("supplierClass", new ActionMessage("apCheckRegister.error.supplierClassInvalid"));
		 }	 
      }
      return(errors);
   }
}
