package com.struts.jreports.cm.weeklycashforecastdetail;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepWeeklyCashForecastDetailAddDetails;

public class CmRepWeeklyCashForecastDetailAddDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepWeeklyCashForecastDetailAddDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	CmRepWeeklyCashForecastDetailAddDetails details = (CmRepWeeklyCashForecastDetailAddDetails)i.next();

         CmRepWeeklyCashForecastDetailAddData argData = new CmRepWeeklyCashForecastDetailAddData(details.getWcfdaCustomer(),
			details.getWcfdaDescription(),
			details.getWcfdaInvoiceNumber(),
			details.getWcfdaDateTransaction(),
			new Double (details.getWcfdaAmount1()),
			new Double (details.getWcfdaAmount2()),
			new Double (details.getWcfdaAmount3()),
			new Double (details.getWcfdaAmount4()),
			new Double (details.getWcfdaAmount5()),
			new Double (details.getWcfdaAmount6()),
			new Double (details.getWcfdaAmount7()),
			new Double (details.getWcfdaAmount8()),
			new Double (details.getWcfdaAmount9()),
			new Double (details.getWcfdaAmount10()),
			new Double (details.getWcfdaAmount11()),
			new Double (details.getWcfdaAmount12()));

         data.add(argData);
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

		if("customer".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getCustomer();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getDescription();
			
		} else if("invoiceNumber".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getInvoiceNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount7();
			
		} else if("amount8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount8();
			
		} else if("amount9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount9();
			
		} else if("amount10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount10();
			
		} else if("amount11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount11();
			
		} else if("amount12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailAddData)data.get(index)).getAmount12();

		}

      return(value);
      
   }
}
