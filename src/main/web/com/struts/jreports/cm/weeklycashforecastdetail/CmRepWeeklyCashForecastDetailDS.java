package com.struts.jreports.cm.weeklycashforecastdetail;


import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepWeeklyCashForecastDetailDetails;

public class CmRepWeeklyCashForecastDetailDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepWeeklyCashForecastDetailDS(CmRepWeeklyCashForecastDetailDetails details) {
		
		CmRepWeeklyCashForecastDetailData argData = new CmRepWeeklyCashForecastDetailData(new Double(details.getWcfdBeginningBalance1()),
				new Double(details.getWcfdBeginningBalance2()),
				new Double(details.getWcfdBeginningBalance3()),
				new Double(details.getWcfdBeginningBalance4()),
				new Double(details.getWcfdBeginningBalance5()),
				new Double(details.getWcfdBeginningBalance6()),
				new Double(details.getWcfdBeginningBalance7()),
				new Double(details.getWcfdBeginningBalance8()),
				new Double(details.getWcfdBeginningBalance9()),
				new Double(details.getWcfdBeginningBalance10()),
				new Double(details.getWcfdBeginningBalance11()),
				new Double(details.getWcfdBeginningBalance12()),
				new Double(details.getWcfdAvailableCashBalance1()),
				new Double(details.getWcfdAvailableCashBalance2()),
				new Double(details.getWcfdAvailableCashBalance3()),
				new Double(details.getWcfdAvailableCashBalance4()),
				new Double(details.getWcfdAvailableCashBalance5()),
				new Double(details.getWcfdAvailableCashBalance6()),
				new Double(details.getWcfdAvailableCashBalance7()),
				new Double(details.getWcfdAvailableCashBalance8()),
				new Double(details.getWcfdAvailableCashBalance9()),
				new Double(details.getWcfdAvailableCashBalance10()),
				new Double(details.getWcfdAvailableCashBalance11()),
				new Double(details.getWcfdAvailableCashBalance12()));
		
		data.add(argData);
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("beginningBalance1".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance1();
			
		} else if("beginningBalance2".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance2();
			
		} else if("beginningBalance3".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance3();
			
		} else if("beginningBalance4".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance4();
			
		} else if("beginningBalance5".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance5();
			
		} else if("beginningBalance6".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance6();
			
		} else if("beginningBalance7".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance7();
			
		} else if("beginningBalance8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance8();
			
		} else if("beginningBalance9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance9();
			
		} else if("beginningBalance10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance10();
			
		} else if("beginningBalance11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance11();
			
		} else if("beginningBalance12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getBeginningBalance12();
			
		} else if("availableCashBalance1".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance1();
			
		} else if("availableCashBalance2".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance2();
			
		} else if("availableCashBalance3".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance3();
			
		} else if("availableCashBalance4".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance4();
			
		} else if("availableCashBalance5".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance5();
			
		} else if("availableCashBalance6".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance6();
			
		} else if("availableCashBalance7".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance7();
			
		} else if("availableCashBalance8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance8();
			
		} else if("availableCashBalance9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance9();
			
		} else if("availableCashBalance10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance10();
			
		} else if("availableCashBalance11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance11();
			
		} else if("availableCashBalance12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailData)data.get(index)).getAvailableCashBalance12();
			
		}

		return(value);
		
	}
	
}
