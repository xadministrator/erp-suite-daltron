package com.struts.jreports.cm.weeklycashforecastdetail;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepWeeklyCashForecastDetailLessDetails;

public class CmRepWeeklyCashForecastDetailLessDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepWeeklyCashForecastDetailLessDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			CmRepWeeklyCashForecastDetailLessDetails details = (CmRepWeeklyCashForecastDetailLessDetails)i.next();
			
			CmRepWeeklyCashForecastDetailLessData argData = new CmRepWeeklyCashForecastDetailLessData(details.getWcfdlSupplier(),
					details.getWcfdlDescription(),
					details.getWcfdlVoucherNumber(),
					details.getWcfdlDateTransaction(),
					new Double (details.getWcfdlAmount1()),
					new Double (details.getWcfdlAmount2()),
					new Double (details.getWcfdlAmount3()),
					new Double (details.getWcfdlAmount4()),
					new Double (details.getWcfdlAmount5()),
					new Double (details.getWcfdlAmount6()),
					new Double (details.getWcfdlAmount7()),
					new Double (details.getWcfdlAmount8()),
					new Double (details.getWcfdlAmount9()),
					new Double (details.getWcfdlAmount10()),
					new Double (details.getWcfdlAmount11()),
					new Double (details.getWcfdlAmount12()));
			
			data.add(argData);
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("supplier".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getSupplier();
			
		} else if("description".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getDescription();
			
		} else if("voucherNumber".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getVoucherNumber();
			
		} else if("transactionDate".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getDateTransaction();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount7();
			
		} else if("amount8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount8();
			
		} else if("amount9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount9();
			
		} else if("amount10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount10();
			
		} else if("amount11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount11();
			
		} else if("amount12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastDetailLessData)data.get(index)).getAmount12();
			
		}
		
		return(value);
		
	}
}
