package com.struts.jreports.cm.weeklycashforecastsummary;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.CmRepWeeklyCashForecastSummaryAddDetails;

public class CmRepWeeklyCashForecastSummaryAddDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public CmRepWeeklyCashForecastSummaryAddDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	CmRepWeeklyCashForecastSummaryAddDetails details = (CmRepWeeklyCashForecastSummaryAddDetails)i.next();

         CmRepWeeklyCashForecastSummaryAddData argData = new CmRepWeeklyCashForecastSummaryAddData(details.getWcfsaDescription(),
			new Double (details.getWcfsaAmount1()),
			new Double (details.getWcfsaAmount2()),
			new Double (details.getWcfsaAmount3()),
			new Double (details.getWcfsaAmount4()),
			new Double (details.getWcfsaAmount5()),
			new Double (details.getWcfsaAmount6()),
			new Double (details.getWcfsaAmount7()),
			new Double (details.getWcfsaAmount8()),
			new Double (details.getWcfsaAmount9()),
			new Double (details.getWcfsaAmount10()),
			new Double (details.getWcfsaAmount11()),
			new Double (details.getWcfsaAmount12()));

         data.add(argData);
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;

      if (index == data.size()) {

          index = -1;
          return false;

      } else return true;

      
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("description".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getDescription();
      	
      } else if("amount1".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount1();
      	
      } else if("amount2".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount2();
      	
      } else if("amount3".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount3();
      	
      } else if("amount4".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount4();
      	
      } else if("amount5".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount5();
      	
      } else if("amount6".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount6();
      	
      } else if("amount7".equals(fieldName)){
      	
      	value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount7();
		} else if("amount8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount8();
			
		} else if("amount9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount9();
			
		} else if("amount10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount10();
			
		} else if("amount11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount11();
			
		} else if("amount12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryAddData)data.get(index)).getAmount12();

      }
      
      return(value);
      
   }
   
}
