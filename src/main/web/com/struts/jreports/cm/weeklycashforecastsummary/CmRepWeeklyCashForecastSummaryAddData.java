package com.struts.jreports.cm.weeklycashforecastsummary;



public class CmRepWeeklyCashForecastSummaryAddData implements java.io.Serializable {

	private String description;

	private Double amount1;
	private Double amount2;
	private Double amount3;
	private Double amount4;
	private Double amount5;
	private Double amount6;
	private Double amount7;
	private Double amount8;
	private Double amount9;
	private Double amount10;
	private Double amount11;
	private Double amount12;
	
	public CmRepWeeklyCashForecastSummaryAddData(String description,
		Double amount1,
		Double amount2,
		Double amount3,
		Double amount4,
		Double amount5,
		Double amount6,
		Double amount7,
		Double amount8,
		Double amount9,
		Double amount10,
		Double amount11,
		Double amount12) {
		
		this.description = description;
		this.amount1 = amount1;
		this.amount2 = amount2;
		this.amount3 = amount3;
		this.amount4 = amount4;
		this.amount5 = amount5;
		this.amount6 = amount6;
		this.amount7 = amount7;
		this.amount8 = amount8;
		this.amount9 = amount9;
		this.amount10 = amount10;
		this.amount11 = amount11;
		this.amount12 = amount12;
		
	}
	
	public String getDescription(){
		
		return description;

	}
	
	public void setDescription(String description){
		
		this.description = description;

	}


	public Double getAmount1(){
		
		return amount1;
		
	}
	
	public void setAmount1(Double amount1){
		
		this.amount1 = amount1;
		
	}
	
	public Double getAmount2(){
		
		return amount2;
		
	}
	
	public void setAmount2(Double amount2){
		
		this.amount2 = amount2;
		
	}
	
	public Double getAmount3(){
		
		return amount3;

	}
	
	public void setAmount3(Double amount3){
		
		this.amount3 = amount3;

	}

	public Double getAmount4(){
		
		return amount4;		
	}
	
	public void setAmount4(Double amount4){
		
		this.amount4 = amount4;		
	}

	public Double getAmount5(){

		return amount5;

	}

	public void setAmount5(Double amount5){

		this.amount5 = amount5;

	}
	
	public Double getAmount6(){
		

		return amount6;
		
	}

	public void setAmount6(Double amount6){

		this.amount6 = amount6;
		
	}
	
	public Double getAmount7(){
		return amount7;
		
	}

	public void setAmount7(Double amount7){
		
		this.amount7 = amount7;
		
	}

	public Double getAmount8(){
		
		return amount8;
		
	}
	
	public void setAmount8(Double amount8){
		
		this.amount8 = amount8;
		
	}
	
	public Double getAmount9(){
		
		return amount9;
		
	}
	
	public void setAmount9(Double amount9){
		
		this.amount9 = amount9;
		
	}
	
	public Double getAmount10(){
		
		return amount10;

	}
	
	public void setAmount10(Double amount10){
		
		this.amount10 = amount10;

	}

	public Double getAmount11(){
		
		return amount11;		
	}
	
	public void setAmount11(Double amount11){
		
		this.amount11 = amount11;		
	}

	public Double getAmount12(){

		return amount12;

	}

	public void setAmount12(Double amount12){

		this.amount12 = amount12;

	}

}
