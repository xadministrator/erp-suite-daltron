package com.struts.jreports.cm.weeklycashforecastsummary;

public class CmRepWeeklyCashForecastSummaryData implements java.io.Serializable {

	private Double beginningBalance1;
	private Double beginningBalance2;
	private Double beginningBalance3;
	private Double beginningBalance4;
	private Double beginningBalance5;
	private Double beginningBalance6;
	private Double beginningBalance7;
	private Double beginningBalance8;
	private Double beginningBalance9;
	private Double beginningBalance10;
	private Double beginningBalance11;
	private Double beginningBalance12;
	
	private Double availableCashBalance1;
	private Double availableCashBalance2;
	private Double availableCashBalance3;
	private Double availableCashBalance4;
	private Double availableCashBalance5;
	private Double availableCashBalance6;
	private Double availableCashBalance7;
	private Double availableCashBalance8;
	private Double availableCashBalance9;
	private Double availableCashBalance10;
	private Double availableCashBalance11;
	private Double availableCashBalance12;
	
	public CmRepWeeklyCashForecastSummaryData(Double beginningBalance1,
		Double beginningBalance2,
		Double beginningBalance3,
		Double beginningBalance4,
		Double beginningBalance5,
		Double beginningBalance6,
		Double beginningBalance7,
		Double beginningBalance8,
		Double beginningBalance9,
		Double beginningBalance10,
		Double beginningBalance11,
		Double beginningBalance12,
		Double availableCashBalance1,
		Double availableCashBalance2,
		Double availableCashBalance3,
		Double availableCashBalance4,
		Double availableCashBalance5,
		Double availableCashBalance6,
		Double availableCashBalance7,
		Double availableCashBalance8,
		Double availableCashBalance9,
		Double availableCashBalance10,
		Double availableCashBalance11,
		Double availableCashBalance12) {
		
		this.beginningBalance1 = beginningBalance1;
		this.beginningBalance2 = beginningBalance2;
		this.beginningBalance3 = beginningBalance3;
		this.beginningBalance4 = beginningBalance4;
		this.beginningBalance5 = beginningBalance5;
		this.beginningBalance6 = beginningBalance6;
		this.beginningBalance7 = beginningBalance7;
		this.beginningBalance8 = beginningBalance8;
		this.beginningBalance9 = beginningBalance9;
		this.beginningBalance10 = beginningBalance10;
		this.beginningBalance11 = beginningBalance11;
		this.beginningBalance12 = beginningBalance12;
		this.availableCashBalance1 = availableCashBalance1;
		this.availableCashBalance2 = availableCashBalance2;
		this.availableCashBalance3 = availableCashBalance3;
		this.availableCashBalance4 = availableCashBalance4;
		this.availableCashBalance5 = availableCashBalance5;
		this.availableCashBalance6 = availableCashBalance6;
		this.availableCashBalance7 = availableCashBalance7;	
		this.availableCashBalance8 = availableCashBalance8;
		this.availableCashBalance9 = availableCashBalance9;
		this.availableCashBalance10 = availableCashBalance10;
		this.availableCashBalance11 = availableCashBalance11;
		this.availableCashBalance12 = availableCashBalance12;
		
	}
	
	public Double getBeginningBalance1(){
		
		return beginningBalance1;
		
	}
	
	public void setBeginningBalance1(Double beginningBalance1){
		
		this.beginningBalance1 = beginningBalance1;
		
	}
	
	public Double getBeginningBalance2(){
		
		return beginningBalance2;
		
	}
	
	public void setBeginningBalance2(Double beginningBalance2){
		
		this.beginningBalance2 = beginningBalance2;
		
	}
	
	public Double getBeginningBalance3(){
		
		return beginningBalance3;

	}
	
	public void setBeginningBalance3(Double beginningBalance3){
		
		this.beginningBalance3 = beginningBalance3;

	}

	public Double getBeginningBalance4(){
		
		return beginningBalance4;		
	}
	
	public void setBeginningBalance4(Double beginningBalance4){
		
		this.beginningBalance4 = beginningBalance4;		
	}

	public Double getBeginningBalance5(){

		return beginningBalance5;

	}

	public void setBeginningBalance5(Double beginningBalance5){

		this.beginningBalance5 = beginningBalance5;

	}
	
	public Double getBeginningBalance6(){
		

		return beginningBalance6;
		
	}

	public void setBeginningBalance6(Double beginningBalance6){

		this.beginningBalance6 = beginningBalance6;
		
	}
	
	public Double getBeginningBalance7(){
		return beginningBalance7;
		
	}

	public void setBeginningBalance7(Double beginningBalance7){
		
		this.beginningBalance7 = beginningBalance7;
		
	}

	public Double getBeginningBalance8(){
		
		return beginningBalance8;
		
	}
	
	public void setBeginningBalance8(Double beginningBalance8){
		
		this.beginningBalance8 = beginningBalance8;
		
	}
	
	public Double getBeginningBalance9(){
		
		return beginningBalance9;
		
	}
	
	public void setBeginningBalance9(Double beginningBalance9){
		
		this.beginningBalance9 = beginningBalance9;
		
	}
	
	public Double getBeginningBalance10(){
		
		return beginningBalance10;

	}
	
	public void setBeginningBalance10(Double beginningBalance10){
		
		this.beginningBalance10 = beginningBalance10;

	}

	public Double getBeginningBalance11(){
		
		return beginningBalance11;
		
	}
	
	public void setBeginningBalance11(Double beginningBalance11){
		
		this.beginningBalance11= beginningBalance11;
		
	}

	public Double getBeginningBalance12(){

		return beginningBalance12;

	}

	public void setBeginningBalance12(Double beginningBalance12){

		this.beginningBalance12 = beginningBalance12;

	}
	
	public Double getAvailableCashBalance1(){
		
		return availableCashBalance1;
		
	}
	
	public void setAvailableCashBalance1(Double availableCashBalance1){
		
		this.availableCashBalance1 = availableCashBalance1;
		
	}
	
	public Double getAvailableCashBalance2(){
		
		return availableCashBalance2;
		
	}
	
	public void setAvailableCashBalance2(Double availableCashBalance2){
		
		this.availableCashBalance2 = availableCashBalance2;
		
	}
	
	public Double getAvailableCashBalance3(){
		
		return availableCashBalance3;

	}
	
	public void setAvailableCashBalance3(Double availableCashBalance3){
		
		this.availableCashBalance3 = availableCashBalance3;

	}

	public Double getAvailableCashBalance4(){
		
		return availableCashBalance4;		
	}
	
	public void setAvailableCashBalance4(Double availableCashBalance4){
		
		this.availableCashBalance4 = availableCashBalance4;		
	}

	public Double getAvailableCashBalance5(){

		return availableCashBalance5;

	}

	public void setAvailableCashBalance5(Double availableCashBalance5){

		this.availableCashBalance5 = availableCashBalance5;

	}
	
	public Double getAvailableCashBalance6(){
		

		return availableCashBalance6;
		
	}

	public void setAvailableCashBalance6(Double availableCashBalance6){

		this.availableCashBalance6 = availableCashBalance6;
		
	}
	
	public Double getAvailableCashBalance7(){
		
		return availableCashBalance7;
		
	}

	public void setAvailableCashBalance7(Double availableCashBalance7){
		
		this.availableCashBalance7 = availableCashBalance7;
		
	}

	public Double getAvailableCashBalance8(){
		
		return availableCashBalance8;
		
	}
	
	public void setAvailableCashBalance8(Double availableCashBalance8){
		
		this.availableCashBalance8 = availableCashBalance8;
		
	}
	
	public Double getAvailableCashBalance9(){
		
		return availableCashBalance9;
		
	}
	
	public void setAvailableCashBalance9(Double availableCashBalance9){
		
		this.availableCashBalance9 = availableCashBalance9;
		
	}
	
	public Double getAvailableCashBalance10(){
		
		return availableCashBalance10;

	}
	
	public void setAvailableCashBalance10(Double availableCashBalance10){
		
		this.availableCashBalance10 = availableCashBalance10;

	}

	public Double getAvailableCashBalance11(){
		
		return availableCashBalance11;		
	}
	
	public void setAvailableCashBalance11(Double availableCashBalance11){
		
		this.availableCashBalance11 = availableCashBalance11;
		
	}

	public Double getAvailableCashBalance12(){

		return availableCashBalance12;

	}

	public void setAvailableCashBalance12(Double availableCashBalance12){

		this.availableCashBalance12 = availableCashBalance12;

	}
	
}
