package com.struts.jreports.cm.weeklycashforecastsummary;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.cm.weeklycashforecastsummary.CmRepWeeklyCashForecastSummaryLessData;
import com.util.CmRepWeeklyCashForecastSummaryLessDetails;

public class CmRepWeeklyCashForecastSummaryLessDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public CmRepWeeklyCashForecastSummaryLessDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			CmRepWeeklyCashForecastSummaryLessDetails details = (CmRepWeeklyCashForecastSummaryLessDetails)i.next();
			
			CmRepWeeklyCashForecastSummaryLessData argData = new CmRepWeeklyCashForecastSummaryLessData(details.getWcfslDescription(),
					new Double (details.getWcfslAmount1()),
					new Double (details.getWcfslAmount2()),
					new Double (details.getWcfslAmount3()),
					new Double (details.getWcfslAmount4()),
					new Double (details.getWcfslAmount5()),
					new Double (details.getWcfslAmount6()),
					new Double (details.getWcfslAmount7()),
					new Double (details.getWcfslAmount8()),
					new Double (details.getWcfslAmount9()),
					new Double (details.getWcfslAmount10()),
					new Double (details.getWcfslAmount11()),
					new Double (details.getWcfslAmount12()));
			
			data.add(argData);
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		
		if (index == data.size()) {
			
			index = -1;
			return false;
			
		} else return true;
		
		
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("description".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getDescription();
			
		} else if("amount1".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount1();
			
		} else if("amount2".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount2();
			
		} else if("amount3".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount3();
			
		} else if("amount4".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount4();
			
		} else if("amount5".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount5();
			
		} else if("amount6".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount6();
			
		} else if("amount7".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount7();
			
		} else if("amount8".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount8();
			
		} else if("amount9".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount9();
			
		} else if("amount10".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount10();
			
		} else if("amount11".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount11();
			
		} else if("amount12".equals(fieldName)){
			
			value = ((CmRepWeeklyCashForecastSummaryLessData)data.get(index)).getAmount12();
		
		}
		
		return(value);
		
	}
	
}
