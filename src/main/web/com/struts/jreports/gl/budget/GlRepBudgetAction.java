package com.struts.jreports.gl.budget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlRepBGTPeriodOutOfRangeException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepBudgetController;
import com.ejb.txn.GlRepBudgetControllerHome;
import com.struts.jreports.gl.budget.GlRepBranchBudgetList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlModBudgetDetails;

public final class GlRepBudgetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepBudgetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepBudgetForm actionForm = (GlRepBudgetForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_BUDGET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepBudget");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepBudgetController EJB
*******************************************************/

         GlRepBudgetControllerHome homeBGT = null;
         GlRepBudgetController ejbBGT = null;       

         try {
         	
            homeBGT = (GlRepBudgetControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepBudgetControllerEJB", GlRepBudgetControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepBudgetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbBGT = homeBGT.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepBudgetAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- GL BGT Go Action --
*******************************************************/

     if(request.getParameter("goButton") != null &&
        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
     	
     	 ArrayList list = null;
     	 String company = null;
     	
     	 try {
     	 	
     		 
     		ArrayList branchList = new ArrayList();
	     	
	     	for(int i=0; i<actionForm.getGlRepBrDetailIncomeStatementListSize(); i++) {
	     	    
	     	    GlRepBranchBudgetList adBdisList = (GlRepBranchBudgetList)actionForm.getGlRepBrBudgetListByIndex(i);
	     	    
	     	    if(adBdisList.getBranchCheckbox() == true) {                                          
	     	        
	     	        AdBranchDetails brDetails = new AdBranchDetails();                                          
	     	        
	     	        brDetails.setBrAdCompany(user.getCmpCode());	     	        
	     	        brDetails.setBrCode(adBdisList.getBrCode());                    
	     	        
	     	        branchList.add(brDetails);
	     	        
	     	    }
	     	}
	     	
            // get company
		       
		    AdCompanyDetails adCmpDetails = ejbBGT.getAdCompany(user.getCmpCode());
		    company = adCmpDetails.getCmpName();
     	
            // execute report
		    
	     	list = ejbBGT.executeGlRepBudget(
	     			actionForm.getBudgetOrganization(), 
					actionForm.getBudgetName(),
					actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')),
			        Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
			        actionForm.getAmountType(),actionForm.getIncludeUnpostedTransaction(), actionForm.getIncludeUnpostedSlTransaction(), 
			        actionForm.getShowZeroes(),
			        branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	     		     	
	     } catch (GlobalNoRecordFoundException ex) {
		  	
		      errors.add(ActionMessages.GLOBAL_MESSAGE,
		         new ActionMessage("budget.error.noRecordFound"));  
		              				              
	     } catch (GlRepBGTPeriodOutOfRangeException ex) {
	    	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budget.error.periodOutOfRange"));
		   
		} catch(EJBException ex) {
			
		   if(log.isInfoEnabled()) {
		   	
		      log.info("EJBException caught in GlRepBudgetAction.execute(): " + ex.getMessage() +
			     " session: " + session.getId());
			     
		   }
		   
		   return(mapping.findForward("cmnErrorPage"));
		}
		
		if(!errors.isEmpty()) {
	    	
	       saveErrors(request, new ActionMessages(errors));
	       return(mapping.findForward("glRepBudget"));
	       
	    }
		
        // fill report parameters, fill report to pdf and set report session
	    
	    Map parameters = new HashMap();
	    parameters.put("company", company);
	    parameters.put("budgetOrganization", actionForm.getBudgetOrganization());
	    parameters.put("budgetName", actionForm.getBudgetName());
	    parameters.put("period", actionForm.getPeriod());
	    parameters.put("amountType", actionForm.getAmountType());
	    parameters.put("viewType", actionForm.getViewType());
	    parameters.put("showEntries", actionForm.getShowEntries());
	    parameters.put("showZeroes", actionForm.getShowZeroes());
	    
	    
	    if (actionForm.getIncludeUnpostedTransaction()) {
	    	
	    	parameters.put("includeUnpostedTransaction", "YES");
	    	
	    } else {
	    	
	    	parameters.put("includeUnpostedTransaction", "NO");
	    	
	    }
	    
	    if (actionForm.getIncludeUnpostedSlTransaction()) {

	    	parameters.put("includeUnpostedSlTransaction", "YES");

	    } else {

	    	parameters.put("includeUnpostedSlTransaction", "NO");

	    }
	    
	    String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepBudget.jasper";
	       
	    if (!new java.io.File(filename).exists()) {
	    		    		    
	       filename = servlet.getServletContext().getRealPath("jreports/GlRepBudget.jasper");
		    
	    }
	    
	    try {
	    	
	       Report report = new Report();
       
	       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	       	
	       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		       report.setBytes(
		          JasperRunManager.runReportToPdf(filename, parameters, 
			        new GlRepBudgetDS(list)));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
               
               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
               report.setBytes(
				   JasperRunManagerExt.runReportToXls(filename, parameters, 
				        new GlRepBudgetDS(list)));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
			   
			   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
			   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
			       new GlRepBudgetDS(list)));												    
			        
		   }
		   
	       session.setAttribute(Constants.REPORT_KEY, report);
	       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	        		       
	    } catch(Exception ex) {
	    	
	        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepBudgetAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
	        }
	        
	        return(mapping.findForward("cmnErrorPage"));
		
	    }
		
	          
/*******************************************************
   -- GL BGT Close Action --
*******************************************************/

     } else if (request.getParameter("closeButton") != null) {

          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- GL BGT Load Action --
*******************************************************/

         }
     
         if(frParam != null) {
         	
         	try {
		    	
		        ArrayList list = null;			       			       
		        Iterator i = null;
		        
		        actionForm.clearPeriodList();
            	
            	list = ejbBGT.getGlAcvAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModAccountingCalendarValueDetails mdetails = (GlModAccountingCalendarValueDetails)i.next();

            			actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() + "-" + mdetails.getAcvYear());
            			
            			if (mdetails.getAcvCurrent()) {
            		    	
            		    	actionForm.setPeriod(mdetails.getAcvPeriodPrefix() + "-" + 
            		            mdetails.getAcvYear());
            		    	
            		    }

            	    }
            		
                }
		       
		        actionForm.clearBudgetOrganizationList();
            	
            	list = ejbBGT.getGlBoAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetOrganizationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		actionForm.setBudgetOrganizationList("ALL");
            		actionForm.setBudgetOrganization("ALL");
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {

            			actionForm.setBudgetOrganizationList((String)i.next());

            	    }
            		
                }
	            	
            	actionForm.clearBudgetNameList();
            	
            	list = ejbBGT.getGlBgtAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();            		            		
            		
            		while (i.hasNext()) {
            			
            			GlModBudgetDetails mdetails = (GlModBudgetDetails)i.next();
            			
            			actionForm.setBudgetNameList(mdetails.getBgtName());
            			
            			if(mdetails.getBgtIsDefault() == true) {
   
            				actionForm.setBudgetName(mdetails.getBgtName());
            			
            			}

            	    }
            		            		            		
                }   
	
			} catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepBudgetAction.execute(): " + ex.getMessage() +
				    		" session: " + session.getId());
		    
			}
		
			return(mapping.findForward("cmnErrorPage"));
		
	    }

	    actionForm.reset(mapping, request);
	    
	    ArrayList brList = new ArrayList();
 		actionForm.clearGlRepBrBudgetList();
        
 		brList = ejbBGT.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
        
        Iterator k = brList.iterator();
        
        while(k.hasNext()) {
        
            AdBranchDetails brDetails = (AdBranchDetails)k.next();
        
            GlRepBranchBudgetList adBdisList = new GlRepBranchBudgetList(actionForm,
                brDetails.getBrBranchCode(), brDetails.getBrName(),
                brDetails.getBrCode());
            adBdisList.setBranchCheckbox(true);
                         
            actionForm.saveGlRepBrBudgetList(adBdisList);
            
        }	
        
        
	    return(mapping.findForward("glRepBudget"));		          
            
	 } else {
	 	
	    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	    saveErrors(request, new ActionMessages(errors));
	
	    return(mapping.findForward("cmnMain"));
			
			 }
 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in GlRepBudgetAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
