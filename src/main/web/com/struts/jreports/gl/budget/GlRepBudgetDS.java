package com.struts.jreports.gl.budget;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepBudgetDetails;

public class GlRepBudgetDS implements JRDataSource{
	
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepBudgetDS(ArrayList list){
   	
      Iterator i = list.iterator();
      
      

      String accountNumber = "";
      while (i.hasNext()) {
    	 Double groupAmount = 0d;
         GlRepBudgetDetails details = (GlRepBudgetDetails)i.next();
         
         
         if(!accountNumber.equals(details.getBgtAccountNumber())){
        	 groupAmount = details.getBgtAmount();
        	 accountNumber = details.getBgtAccountNumber();
         }
         
        
	     GlRepBudgetData disData = new GlRepBudgetData(
	    	new Double(groupAmount),
	     	details.getBgtAccountNumber(), details.getBgtAccountDescription(), details.getBgtDescription(),
	     	details.getBgtDocumentNumber(), details.getBgtDate(),
	     	details.getBgtMisc1(), details.getBgtMisc2(), details.getBgtMisc3(),
	     	details.getBgtMisc4(), details.getBgtMisc5(), details.getBgtMisc6(),
	     	
			new Double(details.getBgtAmount()), 
			new Double(details.getBgtRulePercentage1()), 
			new Double(details.getBgtRulePercentage2()), 
			
			new Double(details.getBgtAccountBalance()));
		    
         data.add(disData);
      }     
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("account".equals(fieldName)){
         value = ((GlRepBudgetData)data.get(index)).getAccount();
      }else if("groupAmount".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getGroupAmount();
          
      }else if("accountDescription".equals(fieldName)){
         value = ((GlRepBudgetData)data.get(index)).getAccountDescription();
      }else if("description".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getDescription();
      }else if("documentNumber".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getDocumentNumber();
      }else if("date".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getDate();
      }else if("misc1".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc1(); 
      }else if("misc2".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc2(); 
      }else if("misc3".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc3(); 
      }else if("misc4".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc4(); 
      }else if("misc5".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc5(); 
      }else if("misc6".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getMisc6(); 
      }else if("amount".equals(fieldName)){
         value = ((GlRepBudgetData)data.get(index)).getAmount();
      }else if("rulePercentage1".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getRulePercentage1();
      }else if("rulePercentage2".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getRulePercentage2();    
         
         
      }else if("balance".equals(fieldName)){
          value = ((GlRepBudgetData)data.get(index)).getAccountBalance();
       }

      return(value);
   }
}
