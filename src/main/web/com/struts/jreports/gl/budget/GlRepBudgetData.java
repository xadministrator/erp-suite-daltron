package com.struts.jreports.gl.budget;

import java.util.Date;

public class GlRepBudgetData implements java.io.Serializable{
	private Double groupAmount = null;
   private String account = null;
   private String accountDescription = null;
   private String description = null;
   private String documentNumber = null;
   private Date date = null;
   private String misc1 = null;
   private String misc2 = null;
   private String misc3 = null;
   private String misc4 = null;
   private String misc5 = null;
   private String misc6 = null;
   private Double amount = null;
   private Double rulePercentage1 = null;
   private Double rulePercentage2 = null;
   private Double accountBalance = null;

   public GlRepBudgetData(Double groupAmount, String account, String accountDescription, String description, 
		   String documentNumber, Date date, String misc1, String misc2, String misc3, String misc4, String misc5, String misc6,
		   Double amount, Double rulePercentage1, Double rulePercentage2 ,Double accountBalance){
	   
	   this.groupAmount = groupAmount;
      this.account = account;
      this.accountDescription = accountDescription;
      this.description = description;
      this.documentNumber = documentNumber;
      this.date = date;
      this.misc1 = misc1;
      this.misc2 = misc2;
      this.misc3 = misc3;
      this.misc4 = misc4;
      this.misc5 = misc5;
      this.misc6 = misc6;
      
      this.amount = amount;
      this.rulePercentage1 = rulePercentage1;
      this.rulePercentage2 = rulePercentage2;
      this.accountBalance = accountBalance;
   }
   
   public Double getGroupAmount(){
	      return(groupAmount);
	   }
   

   public String getAccount(){
      return(account);
   }

   public String getAccountDescription(){
      return(accountDescription);
   }
   
   public String getDescription(){
      return(description);
   }
   
   
   public String getDocumentNumber(){
      return(documentNumber);
   }
   
   public Date getDate(){
      return(date);
   }
   
   public String getMisc1(){
      return(misc1);
   }
   
   public String getMisc2(){
      return(misc2);
   }
   
   public String getMisc3(){
      return(misc3);
   }
   
   public String getMisc4(){
      return(misc4);
   }
   
   public String getMisc5(){
	      return(misc5);
	   }
   
   public String getMisc6(){
	      return(misc6);
	   }

   public Double getAmount(){
      return(amount);
   }
   
   public Double getRulePercentage1(){
	      return(rulePercentage1);
	   }
   
   public Double getRulePercentage2(){
	      return(rulePercentage2);
	   }
   
   
   public Double getAccountBalance(){
	      return(accountBalance);
	   }

}
