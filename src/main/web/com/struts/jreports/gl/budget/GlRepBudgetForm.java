package com.struts.jreports.gl.budget;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.jreports.gl.detailincomestatement.GlRepBranchDetailIncomeStatementList;
import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepBudgetForm extends ActionForm implements Serializable{

   private String budgetOrganization = null;
   private ArrayList budgetOrganizationList = new ArrayList();
   private String budgetName = null;
   private ArrayList budgetNameList = new ArrayList();
   private String period = null;
   private ArrayList periodList = new ArrayList();
   private String amountType = null;
   private ArrayList amountTypeList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private boolean includeUnpostedTransaction = false;
   private boolean includeUnpostedSlTransaction = false;
   private boolean showEntries = false;
   private boolean showZeroes = false;


   private String userPermission = new String();
   
   private ArrayList glRepBrBudgetList = new ArrayList();

   
   public String getBudgetOrganization() {

       return budgetOrganization;

   }

   public void setBudgetOrganization(String budgetOrganization) {
	
	   this.budgetOrganization = budgetOrganization;
	
   }
	 
   public ArrayList getBudgetOrganizationList() {
 	
 	   return budgetOrganizationList;
 	  
   }
 
   public void setBudgetOrganizationList(String budgetOrganization) {
 	
       budgetOrganizationList.add(budgetOrganization);
 	  
   }
 
   public void clearBudgetOrganizationList() {
 	
 	   budgetOrganizationList.clear();
       budgetOrganizationList.add(Constants.GLOBAL_BLANK);
    
   }
 
   public String getBudgetName() {

       return budgetName;

   }

   public void setBudgetName(String budgetName) {

       this.budgetName = budgetName;

   } 
 
   public ArrayList getBudgetNameList() {
 	
 	   return budgetNameList;
 	
   }
 
   public void setBudgetNameList(String budgetName) {
 	
 	   budgetNameList.add(budgetName);
 	  
   }
 
   public void clearBudgetNameList() {
 	
 	  budgetNameList.clear();
      budgetNameList.add(Constants.GLOBAL_BLANK);
 	
   }
   
   public String getPeriod(){
      return(period);
   }

   public void setPeriod(String period){
      this.period = period;
   }

   public ArrayList getPeriodList(){
      return(periodList);
   }

   public void setPeriodList(String period){
      periodList.add(period);
   }

   public void clearPeriodList(){
      periodList.clear();
      periodList.add(Constants.GLOBAL_BLANK);
   }

   public String getAmountType(){
      return(amountType);
   }

   public void setAmountType(String amountType){
      this.amountType = amountType;
   }

   public ArrayList getAmountTypeList(){
      return(amountTypeList);
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }
   
   
   public boolean getIncludeUnpostedTransaction() {
		
		return includeUnpostedTransaction;
		
	}
	
	public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
		
		this.includeUnpostedTransaction = includeUnpostedTransaction;
		
	}
	
	
	public boolean getIncludeUnpostedSlTransaction() {

		return includeUnpostedSlTransaction;

	}

	public void setIncludeUnpostedSlTransaction(boolean includeUnpostedSlTransaction) {

		this.includeUnpostedSlTransaction = includeUnpostedSlTransaction;

	}
	
	public boolean getShowEntries() {

		return showEntries;

	}

	public void setShowEntries(boolean showEntries) {

		this.showEntries = showEntries;

	}
	
	public boolean getShowZeroes() {
		
		return showZeroes;
		
	}
	
	public void setShowZeroes(boolean showZeroes) {
		
		this.showZeroes = showZeroes;
		
	}
   
	

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   
   public Object[] getGlRepBrBudgetList(){
		
		return glRepBrBudgetList.toArray();
		
	}
	
	public GlRepBranchBudgetList getGlRepBrBudgetListByIndex(int index){
		
		return ((GlRepBranchBudgetList)glRepBrBudgetList.get(index));
		
	}
	
	public int getGlRepBrDetailIncomeStatementListSize(){
		
		return(glRepBrBudgetList.size());
		
	}
	
	public void saveGlRepBrBudgetList(Object newGlRepBrBudgetList){
		
		glRepBrBudgetList.add(newGlRepBrBudgetList);   	  
		
	}
	
	public void clearGlRepBrBudgetList(){
		
		glRepBrBudgetList.clear();
		
	}
	
	public void setGlRepBrBudgetList(ArrayList glRepBrBudgetList) {
		
		this.glRepBrBudgetList = glRepBrBudgetList;
		
	}
	

   public void reset(ActionMapping mapping, HttpServletRequest request){      
	   
	   for (int i=0; i<glRepBrBudgetList.size(); i++) {
			
		   GlRepBranchBudgetList list = (GlRepBranchBudgetList)glRepBrBudgetList.get(i);
			list.setBranchCheckbox(false);	 
			
		}  
   	
	  amountTypeList.clear();
	  amountTypeList.add("PTD");
	  amountTypeList.add("QTD");
	  amountTypeList.add("YTD");
	  amountType = "PTD";
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      includeUnpostedTransaction = false;
	  includeUnpostedSlTransaction = false;
	  showEntries = false;
	  showZeroes = false;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("goButton") != null){
         if(Common.validateRequired(budgetOrganization) || budgetOrganization.equals(Constants.GLOBAL_NO_RECORD_FOUND) ){
	            errors.add("budgetOrganzanition", new ActionMessage("budget.error.budgetOrganizationRequired"));
		 }
		 if(Common.validateRequired(budgetName) || budgetName.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	            errors.add("budgetName", new ActionMessage("budget.error.budgetNameRequired"));
		 }
		 if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("period", new ActionMessage("budget.error.periodRequired"));
	     }
		 if(Common.validateRequired(amountType)){
            errors.add("amountType", new ActionMessage("budget.error.amountTypeRequired"));
         }	
		
      }
      return(errors);
   }
}
