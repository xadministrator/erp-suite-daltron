package com.struts.jreports.gl.chartofaccountlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepChartOfAccountListController;
import com.ejb.txn.GlRepChartOfAccountListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.GenModSegmentDetails;
import com.util.GlRepChartOfAccountListDetails;

public final class GlRepChartOfAccountListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepChartOfAccountListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepChartOfAccountListForm actionForm = (GlRepChartOfAccountListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.GL_REP_CHART_OF_ACCOUNT_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepChartOfAccountList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepChartOfAccountListController EJB
*******************************************************/

         GlRepChartOfAccountListControllerHome homeCOA = null;
         GlRepChartOfAccountListController ejbCOA = null;       

         try {
         	
            homeCOA = (GlRepChartOfAccountListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepChartOfAccountListControllerEJB", GlRepChartOfAccountListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepChartOfAccountListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCOA = homeCOA.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepChartOfAccountListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- GL COA Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            GlRepChartOfAccountListDetails details = null;
            
            String company = null;
            	
            	
		    try {

		    	ArrayList branchList = new ArrayList();
		     	
		     	for(int i=0; i<actionForm.getGlRepBrChartOfAccountListSize(); i++) {
		     	    
		     	    GlRepBranchChartOfAccountList adBcoalList = (GlRepBranchChartOfAccountList)actionForm.getGlRepBrChartOfAccountListByIndex(i);
		     	    
		     	    if(adBcoalList.getBranchCheckbox() == true) {                                          
		     	        
		     	        AdBranchDetails brDetails = new AdBranchDetails();                                          
		     	        
		     	        brDetails.setBrAdCompany(user.getCmpCode());	     	        
		     	        brDetails.setBrCode(adBcoalList.getBrCode());                    
		     	        
		     	        branchList.add(brDetails);
		     	        
		     	    }
		     	}
		    	
		     	// get company
		       
		       AdCompanyDetails adCmpDetails = ejbCOA.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbCOA.executeGlRepChartOfAccountList(
		       	   actionForm.getAccountFrom(),
		       	   actionForm.getAccountTo(),
		       	   actionForm.getEnable(),
		       	   actionForm.getDisable(), branchList, user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("chartOfAccountList.error.noRecordFound"));
                     
            } catch (GlobalAccountNumberInvalidException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("chartOfAccountList.error.accountNumberInvalid"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepChartOfAccountListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepChartOfAccountList"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("accountFrom", actionForm.getAccountFrom());
		    parameters.put("accountTo", actionForm.getAccountTo());
		    parameters.put("viewType", actionForm.getViewType());
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getGlRepBrChartOfAccountListSize(); j++) {

      			GlRepBranchChartOfAccountList brList = (GlRepBranchChartOfAccountList)actionForm.getGlRepBrChartOfAccountListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepChartOfAccountList.jasper";
		       
		    if (!new java.io.File(filename).exists()) {
		    		    		    
		       filename = servlet.getServletContext().getRealPath("jreports/GlRepChartOfAccountList.jasper");
			    
		    }		    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new GlRepChartOfAccountListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new GlRepChartOfAccountListDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new GlRepChartOfAccountListDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepChartOfAccountListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- GL COA Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- GL COA Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       			        
	            	list = ejbCOA.getGenSgAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            			            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		actionForm.setAccountFrom("");
	            		actionForm.setAccountTo("");
	            		
	            		while (i.hasNext()) {
	            			
	            		    GenModSegmentDetails mdetails = 
	            		        (GenModSegmentDetails)i.next();
	            		        
	            		    for (int j=0; j<mdetails.getSgMaxSize(); j++) {
	            		    	
	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + "0");
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + "Z");
	            		    	
	            		    }
	            		    
	            		    if (i.hasNext()) {
	            		    	
	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + String.valueOf(mdetails.getSgFlSegmentSeparator()));
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + String.valueOf(mdetails.getSgFlSegmentSeparator()));
	            		    	
	            		    }
	            			
	            		}
	            			            		
	            	}   
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepChartOfAccountListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            actionForm.reset(mapping, request);

                // populate AdRepBranchBankAccount
         		
         		ArrayList brList = new ArrayList();
         		actionForm.clearGlRepBrChartOfAccountList();
                
         		brList = ejbCOA.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                Iterator k = brList.iterator();
                
                while(k.hasNext()) {
                
                    AdBranchDetails brDetails = (AdBranchDetails)k.next();
                
                    GlRepBranchChartOfAccountList adBcoalList = new GlRepBranchChartOfAccountList(actionForm,
                        brDetails.getBrBranchCode(), brDetails.getBrName(),
                        brDetails.getBrCode());
                    adBcoalList.setBranchCheckbox(true);
                                 
                    actionForm.saveGlRepBrChartOfAccountList(adBcoalList);
                    
                }
	            
	            return(mapping.findForward("glRepChartOfAccountList"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in GlRepChartOfAccountListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
