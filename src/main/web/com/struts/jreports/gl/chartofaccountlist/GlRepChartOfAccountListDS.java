package com.struts.jreports.gl.chartofaccountlist;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepChartOfAccountListDetails;

public class GlRepChartOfAccountListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepChartOfAccountListDS(ArrayList list) {
      
      Iterator i = list.iterator();
      while(i.hasNext()) {
      
         GlRepChartOfAccountListDetails details = (GlRepChartOfAccountListDetails)i.next();
         
         GlRepChartOfAccountListData coaData = new GlRepChartOfAccountListData(
		    details.getCoaAccountNumber(), details.getCoaDescription(),
		    details.getCoaAccountType(), details.getCoaEnable() ? "YES" : "NO");
		    
         data.add(coaData);
      
      }
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("accountNumber".equals(fieldName)) {
      	
          value = ((GlRepChartOfAccountListData)data.get(index)).getAccountNumber();
      
      } else if("description".equals(fieldName)) {
      
          value = ((GlRepChartOfAccountListData)data.get(index)).getDescription();
      
      } else if("accountType".equals(fieldName)){
      
          value = ((GlRepChartOfAccountListData)data.get(index)).getAccountType();
      
      } else if("enable".equals(fieldName)){
      
          value = ((GlRepChartOfAccountListData)data.get(index)).getEnable();
      
      }

      return(value);
   }
}
