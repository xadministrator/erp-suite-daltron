package com.struts.jreports.gl.chartofaccountlist;

public class GlRepChartOfAccountListData implements java.io.Serializable{
   
   private String accountNumber = null;
   private String description = null;
   private String accountType = null;
   private String enable = null;
   
   public GlRepChartOfAccountListData(String accountNumber, String description, 
      String accountType, String enable) {
      	
      this.accountNumber = accountNumber;
      this.description = description;
      this.accountType = accountType;
      this.enable = enable;
      
   }

   public String getAccountNumber() {
   	 
      return(accountNumber);
   
   }

   public String getDescription() {
   
      return(description);
   
   } 
   
   public String getAccountType() {
   
      return(accountType);
   
   }
   
   public String getEnable() {
   	
   	  return(enable);
   	  
   }
}
