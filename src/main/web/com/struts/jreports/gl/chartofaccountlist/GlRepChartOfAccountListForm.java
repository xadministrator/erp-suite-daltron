package com.struts.jreports.gl.chartofaccountlist;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepChartOfAccountListForm extends ActionForm implements Serializable{
	
	private String accountFrom = null;
	private String accountTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private boolean enable = false;
	private boolean disable = false;
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private String userPermission = new String();
	
	private ArrayList glRepBrChartOfAccountList = new ArrayList();
	
	public String getAccountFrom() {
		
		return(accountFrom);
		
	}
	
	public void setAccountFrom(String accountFrom) {
		
		this.accountFrom = accountFrom;
		
	}
	
	public String getAccountTo() {
		
		return(accountTo);
		
	}
	
	public void setAccountTo(String accountTo) {
		
		this.accountTo = accountTo;
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		
	}
	
	public boolean getDisable() {
		
		return disable;
		
	}
	
	public void setDisable(boolean disable) {
		
		this.disable = disable;
		
	}
	
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getViewType() {
		
		return(viewType);   	
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getGlRepBrChartOfAccountList(){
		
		return glRepBrChartOfAccountList.toArray();
		
	}
	
	public GlRepBranchChartOfAccountList getGlRepBrChartOfAccountListByIndex(int index){
		
		return ((GlRepBranchChartOfAccountList)glRepBrChartOfAccountList.get(index));
		
	}
	
	public int getGlRepBrChartOfAccountListSize(){
		
		return(glRepBrChartOfAccountList.size());
		
	}
	
	public void saveGlRepBrChartOfAccountList(Object newGlRepBrChartOfAccountList){
		
		glRepBrChartOfAccountList.add(newGlRepBrChartOfAccountList);   	  
		
	}
	
	public void clearGlRepBrChartOfAccountList(){
		
		glRepBrChartOfAccountList.clear();
		
	}
	
	public void setGlRepBrChartOfAccountList(ArrayList glRepBrChartOfAccountList) {
		
		this.glRepBrChartOfAccountList = glRepBrChartOfAccountList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<glRepBrChartOfAccountList.size(); i++) {
			
			GlRepBranchChartOfAccountList list = (GlRepBranchChartOfAccountList)glRepBrChartOfAccountList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		enable = false;
		disable = false;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("goButton") != null){
			
			if(Common.validateRequired(accountFrom)) {
				errors.add("accountFrom", new ActionMessage("chartOfAccountList.error.accountFromRequired"));
				
			}
			
			if(Common.validateRequired(accountTo)) {
				errors.add("accountTo", new ActionMessage("chartOfAccountList.error.accountToRequired"));
				
			}
			
		}
		return(errors);
	}
}
