package com.struts.jreports.gl.csvquarterlyvatreturn;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVPrinter;
import com.ejb.txn.GlRepCsvQuarterlyVatReturnController;
import com.ejb.txn.GlRepCsvQuarterlyVatReturnControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.ApModSupplierDetails;
import com.util.ArModCustomerDetails;

public final class GlRepCsvQuarterlyVatReturnAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
         
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepCsvQuarterlyVatReturnAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepCsvQuarterlyVatReturnForm actionForm = (GlRepCsvQuarterlyVatReturnForm)form;
      
	     // reset report to null
         actionForm.setReport(null);
         
         String frParam = Common.getUserPermission(user, Constants.GL_REP_CSV_QUARTERLY_VAT_RETURN_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepCsvQuarterlyVatReturn");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepCsvQuarterlyVatReturnController EJB
*******************************************************/

         GlRepCsvQuarterlyVatReturnControllerHome homeCSV = null;
         GlRepCsvQuarterlyVatReturnController ejbCSV = null;       

         try {
         	
            homeCSV = (GlRepCsvQuarterlyVatReturnControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepCsvQuarterlyVatReturnControllerEJB", GlRepCsvQuarterlyVatReturnControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCSV = homeCSV.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }
         
         short journalLineNumber = 0;
         
         try {
         	
         	journalLineNumber = ejbCSV.getAdPrfApJournalLineNumber(user.getCmpCode());
         	
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();   

/*******************************************************
   -- Gl CSV Validate & Save Action --
*******************************************************/

	    if(request.getParameter("validateGenerateButton") != null &&
	     	actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	     	try {	
	     		 
	     		// validate all the fields for sales
	     		
	     		if (actionForm.getRecordType().equals("SALES")) {

	     			actionForm.setRecordType("H");
	     			actionForm.setTransactionType("S");
	     			actionForm.setTinOwner(actionForm.getTinOwner());
	     			actionForm.setRegisteredName(actionForm.getRegisteredName().replaceAll(",", "").trim());
	     			actionForm.setLastName(actionForm.getLastName().replaceAll(",", "").trim());
	     			actionForm.setFirstName(actionForm.getFirstName().replaceAll(",", "").trim());
	     			actionForm.setMiddleName(actionForm.getMiddleName().replaceAll(",", "").trim());
	     			actionForm.setTradeName(actionForm.getTradeName().replaceAll(",", "").trim());
	     			actionForm.setAddress1(actionForm.getAddress1().replaceAll(",", "").trim());
	     			actionForm.setAddress2(actionForm.getAddress2().replaceAll(",", "").trim());     		    			
	     			actionForm.setTotalExemptSales(actionForm.getTotalExemptSales());
	     			actionForm.setTotalZeroRatedSales(actionForm.getTotalZeroRatedSales());
	     			actionForm.setTotalTaxableSales(actionForm.getTotalTaxableSales());
	     			actionForm.setTotalOutputTax(actionForm.getTotalOutputTax());
	     			actionForm.setRdoCode(actionForm.getRdoCode());
	     			actionForm.setTaxableMonth(Common.formatDate(actionForm.getTaxableMonth()));
	     			actionForm.setFiscalYearEnding(actionForm.getFiscalYearEnding());

	     			ByteArrayOutputStream baos = new ByteArrayOutputStream();
	     			CSVPrinter printer = new CSVPrinter(baos);
	     			
	     			String[][] str = new String[actionForm.getCsvSLListSize() + 1][17];     	
	     			
	     			// main header   			
	     			str[0][0] = actionForm.getRecordType();
	     			str[0][1] = actionForm.getTransactionType();
	     			str[0][2] = actionForm.getTinOwner();
	     			str[0][3] = actionForm.getRegisteredName();
	     			str[0][4] = actionForm.getLastName();
	     			str[0][5] = actionForm.getFirstName();
	     			str[0][6] = actionForm.getMiddleName();
	     			str[0][7] = actionForm.getTradeName();
	     			str[0][8] = actionForm.getAddress1();
	     			str[0][9] = actionForm.getAddress2();     		

	     			// sales - header (DETAILS OF SALES)	     			
	     			str[0][10] = actionForm.getTotalExemptSales();
	     			str[0][11] = actionForm.getTotalZeroRatedSales();
	     			str[0][12] = actionForm.getTotalTaxableSales();
	     			str[0][13] = actionForm.getTotalOutputTax();
	     			str[0][14] = actionForm.getRdoCode();
	     			str[0][15] = actionForm.getTaxableMonth();
	     			str[0][16] = actionForm.getFiscalYearEnding();

	     			for (int i = 0; i<actionForm.getCsvSLListSize(); i++) {
	     				
	     				// sales - details
	     				
	     				GlRepCsvQuarterlyVatReturnSalesList glCsvSLSList = actionForm.getGlCSVSalesByIndex(i);
	     				
	     				glCsvSLSList.setRecordType("D");
	     				glCsvSLSList.setTransactionType("S");
	     				glCsvSLSList.setTinOfCustomer(glCsvSLSList.getTinOfCustomer());
	     				glCsvSLSList.setRegisteredNameSales(glCsvSLSList.getRegisteredNameSales().replaceAll(",", "").trim());
	     				glCsvSLSList.setLastNameSales(glCsvSLSList.getLastNameSales().replaceAll(",", "").trim());
	     				glCsvSLSList.setFirstNameSales(glCsvSLSList.getFirstNameSales().replaceAll(",", "").trim()); 
	     				glCsvSLSList.setMiddleNameSales(glCsvSLSList.getMiddleNameSales().replaceAll(",", "").trim()); 
	     				glCsvSLSList.setAddress1Sales(glCsvSLSList.getAddress1Sales().replaceAll(",", "").trim()); 
	     				glCsvSLSList.setAddress2Sales(glCsvSLSList.getAddress2Sales().replaceAll(",", "").trim()); 
	     				glCsvSLSList.setExemptSales(glCsvSLSList.getExemptSales()); 
	     				glCsvSLSList.setZeroRatedSales(glCsvSLSList.getZeroRatedSales()); 
	     				glCsvSLSList.setTaxableSales(glCsvSLSList.getTaxableSales()); 
	     				glCsvSLSList.setOutputTax(glCsvSLSList.getOutputTax());
	     				glCsvSLSList.setTinOwnerSales(glCsvSLSList.getTinOwnerSales());
	     				glCsvSLSList.setTaxableMonthSales(Common.formatDate(glCsvSLSList.getTaxableMonthSales()));
	     				
	     				str[i+1][0] = glCsvSLSList.getRecordType();
	     				str[i+1][1] = glCsvSLSList.getTransactionType();
	     				str[i+1][2] = glCsvSLSList.getTinOfCustomer();
	     				str[i+1][3] = glCsvSLSList.getRegisteredNameSales();
	     				str[i+1][4] = glCsvSLSList.getLastNameSales();
	     				str[i+1][5] = glCsvSLSList.getFirstNameSales(); 
	     				str[i+1][6] = glCsvSLSList.getMiddleNameSales(); 
	     				str[i+1][7] = glCsvSLSList.getAddress1Sales(); 
	     				str[i+1][8] = glCsvSLSList.getAddress2Sales(); 
	     				str[i+1][9] = glCsvSLSList.getExemptSales(); 
	     				str[i+1][10] = glCsvSLSList.getZeroRatedSales(); 
	     				str[i+1][11] = glCsvSLSList.getTaxableSales(); 
	     				str[i+1][12] = glCsvSLSList.getOutputTax(); 
	     				str[i+1][13] = glCsvSLSList.getTinOwnerSales(); 
	     				str[i+1][14] = glCsvSLSList.getTaxableMonthSales(); 
	     				
	     			}

	     			printer.println(str);
	     				     			
	     			Report report = new Report();
	     				     				     			
	     			report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
				    report.setBytes(baos.toByteArray());			    
				    session.setAttribute(Constants.REPORT_KEY, report);
				    actionForm.setReport(Constants.STATUS_SUCCESS);

				    return mapping.findForward("glRepCsvQuarterlyVatReturn");
	     		
	     		} else if (actionForm.getRecordType().equals("PURCHASES")) {

	     			actionForm.setRecordType("H");
	     			actionForm.setTransactionType("P");
	     			actionForm.setTinOwner(actionForm.getTinOwner());
	     			actionForm.setRegisteredName(actionForm.getRegisteredName().replaceAll(",", "").trim());
	     			actionForm.setLastName(actionForm.getLastName().replaceAll(",", "").trim());
	     			actionForm.setFirstName(actionForm.getFirstName().replaceAll(",", "").trim());
	     			actionForm.setMiddleName(actionForm.getMiddleName().replaceAll(",", "").trim());
	     			actionForm.setTradeName(actionForm.getTradeName().replaceAll(",", "").trim());
	     			actionForm.setAddress1(actionForm.getAddress1().replaceAll(",", "").trim());
	     			actionForm.setAddress2(actionForm.getAddress2().replaceAll(",", "").trim());     		
	     			actionForm.setTotalExemptPurchases(actionForm.getTotalExemptPurchases());
	     			actionForm.setTotalZeroRatedPurchases(actionForm.getTotalZeroRatedPurchases());
	     			actionForm.setTotalServices(actionForm.getTotalServices());
	     			actionForm.setTotalCapitalGoods(actionForm.getTotalCapitalGoods());
	     			actionForm.setTotalGoodsOtherThanCp(actionForm.getTotalGoodsOtherThanCp());
	     			actionForm.setTotalInputTax(actionForm.getTotalInputTax());
	     			actionForm.setCreditable(actionForm.getCreditable());
	     			actionForm.setNonCreditable(actionForm.getNonCreditable());
	     			actionForm.setRdoCode(actionForm.getRdoCode());
	     			actionForm.setTaxableMonth(Common.formatDate(actionForm.getTaxableMonth()));
	     			actionForm.setFiscalYearEnding(actionForm.getFiscalYearEnding());
	     			
	     			ByteArrayOutputStream baos = new ByteArrayOutputStream();
	     			CSVPrinter printer = new CSVPrinter(baos);
	     			
	     			String[][] str = new String[actionForm.getCsvSLListSize() + 1][21];     	
	     			
	     			// main header
	     			str[0][0] = actionForm.getRecordType();
	     			str[0][1] = actionForm.getTransactionType();
	     			str[0][2] = actionForm.getTinOwner();
	     			str[0][3] = actionForm.getRegisteredName();
	     			str[0][4] = actionForm.getLastName();
	     			str[0][5] = actionForm.getFirstName();
	     			str[0][6] = actionForm.getMiddleName();
	     			str[0][7] = actionForm.getTradeName();
	     			str[0][8] = actionForm.getAddress1();
	     			str[0][9] = actionForm.getAddress2();     		

	     			// purchases - header (DETAILS OF PURCHASES)
	     			str[0][10] = actionForm.getTotalExemptPurchases();
	     			str[0][11] = actionForm.getTotalZeroRatedPurchases();
	     			
	     			// purchases - header (TAXABLE PURCHASES ON)
	     			str[0][12] = actionForm.getTotalServices();
	     			str[0][13] = actionForm.getTotalCapitalGoods();
	     			str[0][14] = actionForm.getTotalGoodsOtherThanCp();
	     			str[0][15] = actionForm.getTotalInputTax();
	     			
	     			// purchases - header (ALLOCATION OF INPUT TAX)
	     			str[0][16] = actionForm.getCreditable();
	     			str[0][17] = actionForm.getNonCreditable();
	     			str[0][18] = actionForm.getRdoCode();
	     			str[0][19] = actionForm.getTaxableMonth();
	     			str[0][20] = actionForm.getFiscalYearEnding();
	     			
	     			for (int i = 0; i<actionForm.getCsvPRListSize(); i++) {
	     				
	     				// purchases - details
	     				
	     				GlRepCsvQuarterlyVatReturnPurchasesList glCsvPURList = actionForm.getGlCSVPurchasesByIndex(i);
	     				
	     				glCsvPURList.setRecordType("D");
	     				glCsvPURList.setTransactionType("P");
	     				glCsvPURList.setTinOfSupplier(glCsvPURList.getTinOfSupplier());
	     				glCsvPURList.setRegisteredNamePurchases(glCsvPURList.getRegisteredNamePurchases().replaceAll(",", "").trim());
	     				glCsvPURList.setLastNamePurchases(glCsvPURList.getLastNamePurchases().replaceAll(",", "").trim());
	     				glCsvPURList.setFirstNamePurchases(glCsvPURList.getFirstNamePurchases().replaceAll(",", "").trim()); 
	     				glCsvPURList.setMiddleNamePurchases(glCsvPURList.getMiddleNamePurchases().replaceAll(",", "").trim()); 
	     				glCsvPURList.setAddress1Purchases(glCsvPURList.getAddress1Purchases().replaceAll(",", "").trim()); 
	     				glCsvPURList.setAddress2Purchases(glCsvPURList.getAddress2Purchases().replaceAll(",", "").trim()); 
	     				glCsvPURList.setExemptPurchases(glCsvPURList.getExemptPurchases()); 
	     				glCsvPURList.setZeroRatedPurchases(glCsvPURList.getZeroRatedPurchases()); 
	     				glCsvPURList.setServices(glCsvPURList.getServices()); 
	     				glCsvPURList.setCapitalGoods(glCsvPURList.getCapitalGoods());
	     				glCsvPURList.setGoodsOtherThanCp(glCsvPURList.getGoodsOtherThanCp());
	     				glCsvPURList.setInputTax(glCsvPURList.getInputTax());
	     				glCsvPURList.setTinOwnerPurchases(glCsvPURList.getTinOwnerPurchases()); 
	     				glCsvPURList.setTaxableMonthPurchases(Common.formatDate(glCsvPURList.getTaxableMonthPurchases())); 
	     				
	     				str[i+1][0] = glCsvPURList.getRecordType();
	     				str[i+1][1] = glCsvPURList.getTransactionType();
	     				str[i+1][2] = glCsvPURList.getTinOfSupplier();
	     				str[i+1][3] = glCsvPURList.getRegisteredNamePurchases();
	     				str[i+1][4] = glCsvPURList.getLastNamePurchases();
	     				str[i+1][5] = glCsvPURList.getFirstNamePurchases(); 
	     				str[i+1][6] = glCsvPURList.getMiddleNamePurchases();
	     				str[i+1][7] = glCsvPURList.getAddress1Purchases(); 
	     				str[i+1][8] = glCsvPURList.getAddress2Purchases(); 
	     				str[i+1][9] = glCsvPURList.getExemptPurchases(); 
	     				str[i+1][10] = glCsvPURList.getZeroRatedPurchases(); 
	     				str[i+1][11] = glCsvPURList.getServices(); 
	     				str[i+1][12] = glCsvPURList.getCapitalGoods(); 
	     				str[i+1][13] = glCsvPURList.getGoodsOtherThanCp(); 
	     				str[i+1][14] = glCsvPURList.getInputTax(); 
	     				str[i+1][15] = glCsvPURList.getTinOwnerPurchases(); 
	     				str[i+1][16] = glCsvPURList.getTaxableMonthPurchases(); 
	     				
	     			}

	     			printer.println(str);
		     			
					Report report = new Report();									  
					report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
					report.setBytes(baos.toByteArray());			    
					session.setAttribute(Constants.REPORT_KEY, report);
					actionForm.setReport(Constants.STATUS_SUCCESS);
				    
					return mapping.findForward("glRepCsvQuarterlyVatReturn");

	     		}

		    } catch (EJBException ex) {
	     		
	     		if (log.isInfoEnabled()) {
	     			
	     			log.info("EJBException caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + ex.getMessage() +
	     					" session: " + session.getId());
	     			return mapping.findForward("cmnErrorPage");
	     			
	     		}
	     	}
	     	
	     	return mapping.findForward("glRepCsvQuarterlyVatReturn");

/*******************************************************
 -- Gl CSV Go Action --
*******************************************************/
	     	
	     } else if(request.getParameter("goButton") != null &&
	     		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {	 
	     	
	     	try {
	     		
	     		AdCompanyDetails adCmpDetails = ejbCSV.getAdCompany(user.getCmpCode());

	     		ArrayList list = null;
	     		Iterator i = null;
	     		
                // get all sales for each customers
	     		
	     		actionForm.clearCsvSLList();
	     		
	     		list = ejbCSV.getCstSalesByDateRange(Common.convertStringToSQLDate(actionForm.getDateFrom()),
	     				Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode()); 

	     		i = list.iterator();
	     		
	     		Date currentDate = new Date();

	     		int lineNumber = 0;
	     		
	     		while (i.hasNext()) {
	     			
	     			ArModCustomerDetails mdetails = (ArModCustomerDetails)i.next();

	     			actionForm.setTotalExemptSales(Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstTotalExemptSales()));
	     			actionForm.setTotalZeroRatedSales(Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstTotalZeroRatedSales()));
	     			actionForm.setTotalTaxableSales(Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstTotalTaxableSales()));
	     			actionForm.setTotalOutputTax(Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstTotalOutputTax()));

	     			GlRepCsvQuarterlyVatReturnSalesList csvSLList = new GlRepCsvQuarterlyVatReturnSalesList(actionForm, 
	     					mdetails.getCstTin(),							
							mdetails.getCstName().replaceAll(",", "").trim(),							 
							actionForm.getLastNameSales(),
							actionForm.getFirstNameSales(),
							actionForm.getMiddleNameSales(),
							mdetails.getCstAddress(),
							(mdetails.getCstCity() + " " + mdetails.getCstStateProvince()).trim(),
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstExemptSales()),
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstZeroRatedSales()),
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstTaxableSales()),
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getCstOutputTax()),
							adCmpDetails.getCmpTin(),
							Common.convertSQLDateToString(currentDate),
							String.valueOf(++lineNumber));
	     			
	     			actionForm.saveCsvSLList(csvSLList);

	     		}
	     		
	     		//	get all purchases for each suppliers
	     		
	     		actionForm.clearCsvPRList();
	     		
	     		list = ejbCSV.getSplPurchasesByDateRange(Common.convertStringToSQLDate(actionForm.getDateFrom()),
	     				Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());
	     		
	     		i = list.iterator();
	     			     		
	     		currentDate = new Date();
	     		
	     		String SPL_TIN = null;
	     		
	     		lineNumber = 0;
	     		
	     		while (i.hasNext()) {
	     			
	     			ApModSupplierDetails mdetails = (ApModSupplierDetails)i.next();

	     			actionForm.setTotalExemptPurchases(Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplTotalExemptPurchases()));
	     			actionForm.setTotalZeroRatedPurchases(Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplTotalZeroRatedPurchase()));
	     			actionForm.setTotalInputTax(Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplTotalInputTax()));
	     			
	     			GlRepCsvQuarterlyVatReturnPurchasesList csvPRList = new GlRepCsvQuarterlyVatReturnPurchasesList(actionForm, 
	     					mdetails.getSplTin(),							
							mdetails.getSplName().replaceAll(",", "").trim(),  							 
							actionForm.getLastNamePurchases(),
							actionForm.getFirstNamePurchases(),
							actionForm.getMiddleNamePurchases(),
							mdetails.getSplAddress(),							
							(mdetails.getSplCity() + " " + mdetails.getSplStateProvince()).trim(), 									
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplExemptPurchases()),
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplZeroRatedPurchase()),
							"0.00",
							"0.00",
							"0.00",
							Common.convertDoubleToStringTaxCsvMoney(mdetails.getSplInputTax()),
							adCmpDetails.getCmpTin(),
							Common.convertSQLDateToString(currentDate),
							String.valueOf(++lineNumber));
	     			
	     			actionForm.saveCsvPRList(csvPRList);
	     			
	     		}

	     	} catch (EJBException ex) {
	     		
	     		if (log.isInfoEnabled()) {
	     			
	     			log.info("EJBException caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + ex.getMessage() +
	     					" session: " + session.getId());
	     			return mapping.findForward("cmnErrorPage");
	     			
	     		}
	     	}
	     	
	     	return(mapping.findForward("glRepCsvQuarterlyVatReturn"));
	     		     	
/*******************************************************
    -- Ap CSV Add Lines Action --
*******************************************************/
	     
	     } else if (request.getParameter("addLinesButton") != null && 
	     		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {	     
	     	
	     	int listSize = actionForm.getCsvIMListSize();
	     	
	     	for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	     		GlRepCsvQuarterlyVatReturnImportationsList glCSVList = new GlRepCsvQuarterlyVatReturnImportationsList(actionForm,
	     						null, null, null, null, null, "0.00", "0.00", "0.00", "0.00", "0.00", null, null, null, null, String.valueOf(x));	        	    	        	
	        	
	        	actionForm.saveCsvIMList(glCSVList);
	        	
	        }	        
	        
	        return(mapping.findForward("glRepCsvQuarterlyVatReturn"));
	        
/*******************************************************
    -- Ap VOU Delete Lines Action --
 *******************************************************/
	
	  } else if(request.getParameter("deleteLinesButton") != null && 
	  		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {	
	  	
	  	for (int i = 0; i<actionForm.getCsvIMListSize(); i++) {
	    	
	  		   GlRepCsvQuarterlyVatReturnImportationsList glCSVList = actionForm.getGlCSVImByIndex(i);
	    	   
	    	   if (glCSVList.getDeleteCheckbox()) {
	    	   	
	    	   	   actionForm.deleteGLCSVImList(i);
	    	   	   i--;
	    	   }
	    	   
	     }	     	 
	    
	    return(mapping.findForward("glRepCsvQuarterlyVatReturn"));	
	     	
/*******************************************************
   -- Close Action --
*******************************************************/

	     } else if (request.getParameter("closeButton") != null) {
	     	
	     	return(mapping.findForward("cmnMain"));
	     		     	
/*******************************************************
   -- Gl CSV Load Action --
*******************************************************/

         }
     
         if(frParam != null) {
         	
         	if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glRepCsvQuarterlyVatReturn");
               
            }
            
            if (request.getParameter("forward") == null &&
				actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
				saveErrors(request, new ActionMessages(errors));
				
				return mapping.findForward("cmnMain");	
				
			}
			
			actionForm.reset(mapping, request);

         	try {
         		
         		// clear all lines
         		
         		actionForm.clearCsvSLList();
         		actionForm.clearCsvPRList();
         		actionForm.clearCsvIMList();

		        // get company details
			       
	            AdCompanyDetails adCmpDetails = ejbCSV.getAdCompany(user.getCmpCode());
	            
	            String REGISTER_ADDRSS =  adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

	            actionForm.setTinOwner(adCmpDetails.getCmpTin());
	            actionForm.setRegisteredName(adCmpDetails.getCmpName());		            	           		
		        actionForm.setAddress1(adCmpDetails.getCmpAddress());
		        actionForm.setAddress2(REGISTER_ADDRSS);

			} catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				 return(mapping.findForward("cmnErrorPage"));
				    
				}

		    }
		    
		    if (!errors.isEmpty()) {
					
				saveErrors(request, new ActionMessages(errors));
					
			} else {
				
				if ((request.getParameter("validateGenerateButton") != null &&
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
			}	         	

			actionForm.reset(mapping, request);         
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            // reset for sales
            
            actionForm.setTotalExemptSales("0.00");
            actionForm.setTotalZeroRatedSales("0.00");
            actionForm.setTotalTaxableSales("0.00");
            actionForm.setTotalOutputTax("0.00");
            
            // reset for purchases
            
            actionForm.setTotalExemptPurchases("0.00");
            actionForm.setTotalZeroRatedPurchases("0.00");
            actionForm.setTotalServices("0.00");
            actionForm.setTotalCapitalGoods("0.00");
            actionForm.setTotalGoodsOtherThanCp("0.00");
            actionForm.setTotalInputTax("0.00");
            actionForm.setCreditable("0.00");
            actionForm.setNonCreditable("0.00");
            
            // reset for importations
            
            actionForm.setTotalDutiableValue("0.00");
            actionForm.setTotalOtherCharges("0.00");
            actionForm.setTotalExemptImportations("0.00");
            actionForm.setTotalTaxableImportations("0.00");
			actionForm.setTotalVatPaid("0.00");
            
            return(mapping.findForward("glRepCsvQuarterlyVatReturn"));		          
		            
		 } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("cmnMain"));
		
		 }
 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in GlRepCsvQuarterlyVatReturnAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
