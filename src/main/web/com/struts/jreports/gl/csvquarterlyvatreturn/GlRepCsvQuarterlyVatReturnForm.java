package com.struts.jreports.gl.csvquarterlyvatreturn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlRepCsvQuarterlyVatReturnForm extends ActionForm implements Serializable {

   private String dateFrom = null;
   private String dateTo = null;
   private String recordType = null;
   private String transactionType = null;
   private String tinOwner = null;
   private String registeredName = null;
   private String lastName = null;
   private String firstName = null;
   private String middleName = null;
   private String tradeName = null;
   private String address1 = null;
   private String address2 = null;
   private String rdoCode = null;
   private String taxableMonth = null;
   private String fiscalYearEnding = null;
   private String reportType = null;
   private ArrayList reportTypeList = new ArrayList();
   private String report = null;

   // group sales
   private String registeredNameSales = null;
   private String lastNameSales = null;
   private String firstNameSales = null;
   private String middleNameSales = null;
   private String tradeNameSales = null;
   private String address1Sales = null;
   private String address2Sales = null;
   private String totalExemptSales = null;
   private String totalZeroRatedSales = null;
   private String totalTaxableSales = null;
   private String totalOutputTax = null;
   
   
   // group purchases
   private String registeredNamePurchases = null;
   private String lastNamePurchases = null;
   private String firstNamePurchases = null;
   private String middleNamePurchases = null;
   private String tradeNamePurchases = null;
   private String address1Purchases = null;
   private String address2Purchases = null;
   private String totalExemptPurchases = null;
   private String totalZeroRatedPurchases = null;
   private String totalServices = null;
   private String totalCapitalGoods = null;
   private String totalGoodsOtherThanCp = null;
   private String totalInputTax = null;
   private String creditable = null;
   private String nonCreditable = null;

   // group importation
   private String registeredNameImportations = null;
   private String lastNameImportations = null;
   private String firstNameImportations = null;
   private String middleNameImportations = null;
   private String tradeNameImportations = null;
   private String address1Importations = null;
   private String address2Importations = null;
   private String totalDutiableValue = null;
   private String totalOtherCharges = null;
   private String totalExemptImportations = null;
   private String totalTaxableImportations = null;
   private String totalVatPaid = null;
   private boolean includeImportations = false;
   
   private String pageState = new String();
   private ArrayList csvSLList = new ArrayList();
   private ArrayList csvPRList = new ArrayList();
   private ArrayList csvIMList = new ArrayList();
   private int rowSelected = 0;
   private String txnStatus = new String();
   private String validateSaveButton = null;
   private String closeButton = null;

   private String userPermission = new String();
   
   
   public GlRepCsvQuarterlyVatReturnSalesList getGlCSVSalesByIndex(int index) {
   	
   	return((GlRepCsvQuarterlyVatReturnSalesList)csvSLList.get(index));
   
   }
   
   public GlRepCsvQuarterlyVatReturnPurchasesList getGlCSVPurchasesByIndex(int index) {
   	
   	return((GlRepCsvQuarterlyVatReturnPurchasesList)csvPRList.get(index));
   
   }
   
   public GlRepCsvQuarterlyVatReturnImportationsList getGlCSVImportationsByIndex(int index) {
   	
   	return((GlRepCsvQuarterlyVatReturnImportationsList)csvIMList.get(index));
   
   }
   
   public Object[] getCsvSLList() {

    return csvSLList.toArray();

   }
 
   public Object[] getCsvPRList() {

    return csvPRList.toArray();

   }
   
   public Object[] getCsvIMList() {

    return csvIMList.toArray();

   }
   
   public int getCsvSLListSize() {

    return csvSLList.size();

   }
 
   public int getCsvPRListSize() {

    return csvPRList.size();

   }
   
   public int getCsvIMListSize() {

    return csvIMList.size();

   }
   
   public void saveCsvSLList(Object newCsvSLList) {

   	csvSLList.add(newCsvSLList);

   }
 
   public void saveCsvPRList(Object newCsvPRList) {

   	csvPRList.add(newCsvPRList);

   }
   
   public void saveCsvIMList(Object newCsvIMList) {

   	csvIMList.add(newCsvIMList);

   }
   
   public void clearCsvSLList() {
  
   	csvSLList.clear();
  
   }
 
 
   public void clearCsvPRList() {
   
   	csvPRList.clear();
 
   }
 
 
   public void clearCsvIMList() {
   
   	csvIMList.clear();
 
   }
   
   public void deleteGLCSVImList(int rowCSVSelected){
   	
    csvIMList.remove(rowCSVSelected);
    
   }
   
   public GlRepCsvQuarterlyVatReturnImportationsList getGlCSVImByIndex(int index){
   	
   	return((GlRepCsvQuarterlyVatReturnImportationsList)csvIMList.get(index));
    
   }
   
   public void setPageState(String pageState) {

    this.pageState = pageState;

   }

   public String getPageState() {

    return pageState;

   }
   
   public String getTxnStatus() {

    String passTxnStatus = txnStatus;
    txnStatus = Constants.GLOBAL_BLANK;
    return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

    this.txnStatus = txnStatus;

   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getDateFrom() {
   	
   	  return(dateFrom);
   
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return(dateTo);
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getRecordType() {
   	
   	  return(recordType);
   
   }
   
   public void setRecordType(String recordType) {
   	
   	  this.recordType = recordType;
   
   }
   
   public String getTransactionType() {
   	
   	  return(transactionType);
   	  
   }
   
   public void setTransactionType(String transactionType) {
   	
   	  this.transactionType = transactionType;
   	  
   }

   public String setCloseButton() {
   	
   	  return closeButton;
   	
   }
      
   public String getUserPermission() {

      return userPermission;

   } 

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getAddress1Importations() {
	
   	return address1Importations;
   
   }
   
   public void setAddress1Importations(String address1Importations) {
	
   	this.address1Importations = address1Importations;
	
   }
	
   public String getAddress1Purchases() {
	
   	return address1Purchases;
	
   }
	
   public void setAddress1Purchases(String address1Purchases) {
	
   	this.address1Purchases = address1Purchases;
	
   }
	
   public String getAddress1Sales() {
	
   	return address1Sales;
	
   }
	
   public void setAddress1Sales(String address1Sales) {
	
   	this.address1Sales = address1Sales;
	
   }
   
   public String getAddress1() {
	
   	return address1;
	
   }
	
   public void setAddress1(String address1) {
	
   	this.address1 = address1;
	
   }
	
   public String getAddress2Importations() {
	
   	return address2Importations;
	
   }
	
   public void setAddress2Importations(String address2Importations) {
	
   	this.address2Importations = address2Importations;
	
   }
	
   public String getAddress2Purchases() {
	
   	return address2Purchases;
	
   }
	
   public void setAddress2Purchases(String address2Purchases) {
	
   	this.address2Purchases = address2Purchases;
	
   }
	
   public String getAddress2Sales() {
	
   	return address2Sales;
	
   }
	
   public void setAddress2Sales(String address2Sales) {
	
   	this.address2Sales = address2Sales;
	
   }
   
   public String getAddress2() {
	
   	return address2;
	
   }
	
   public void setAddress2(String address2) {
	
   	this.address2 = address2;
	
   }
	
   public String getTotalCapitalGoods() {
	
   	return totalCapitalGoods;
	
   }
	
   public void setTotalCapitalGoods(String totalCapitalGoods) {
	
   	this.totalCapitalGoods = totalCapitalGoods;
	
   }
	
   public String getCreditable() {
	
   	return creditable;
	
   }
	
   public void setCreditable(String creditable) {
	
   	this.creditable = creditable;
	
   }
	
   public String getTotalDutiableValue() {
	
   	return totalDutiableValue;
	
   }
	
   public void setTotalDutiableValue(String totalDutiableValue) {
	
   	this.totalDutiableValue = totalDutiableValue;
	
   }
	
   public String getTotalExemptImportations() {
	
   	return totalExemptImportations;
	
   }
	
   public void setTotalExemptImportations(String totalExemptImportations) {
	
   	this.totalExemptImportations = totalExemptImportations;
	
   }
	
   public String getTotalExemptPurchases() {
	
   	return totalExemptPurchases;
	
   }
	
   public void setTotalExemptPurchases(String totalExemptPurchases) {
	
   	this.totalExemptPurchases = totalExemptPurchases;
	
   }
	
   public String getTotalExemptSales() {
	
   	return totalExemptSales;
	
   }
	
   public void setTotalExemptSales(String totalExemptSales) {
	
   	this.totalExemptSales = totalExemptSales;
	
   }
	
   public String getFirstNameImportations() {
	
   	return firstNameImportations;
	
   }
	
   public void setFirstNameImportations(String firstNameImportations) {
	
   	this.firstNameImportations = firstNameImportations;
	
   }
	
   public String getFirstNamePurchases() {
	
   	return firstNamePurchases;
	
   }
	
   public void setFirstNamePurchases(String firstNamePurchases) {
	
   	this.firstNamePurchases = firstNamePurchases;
	
   }
	
   public String getFirstNameSales() {
	
   	return firstNameSales;
	
   }
	
   public void setFirstNameSales(String firstNameSales) {
	
   	this.firstNameSales = firstNameSales;
	
   }
   
   public String getFirstName() {
	
   	return firstName;
	
   }
	
   public void setFirstName(String firstName) {
	
   	this.firstName = firstName;
	
   }
	
   public String getFiscalYearEnding() {
	
   	return fiscalYearEnding;
	
   }
	
   public void setFiscalYearEnding(String fiscalYearEnding) {
	
   	this.fiscalYearEnding = fiscalYearEnding;
	
   }
	
   public String getTotalGoodsOtherThanCp() {
	
   	return totalGoodsOtherThanCp;
	
   }
	
   public void setTotalGoodsOtherThanCp(String totalGoodsOtherThanCp) {
	
   	this.totalGoodsOtherThanCp = totalGoodsOtherThanCp;
	
   }
	
   public String getTotalInputTax() {
	
   	return totalInputTax;
	
   }
	
   public void setTotalInputTax(String totalInputTax) {
	
   	this.totalInputTax = totalInputTax;
	
   }
	
   public String getLastNameImportations() {
	
   	return lastNameImportations;
	
   }
	
   public void setLastNameImportations(String lastNameImportations) {
	
   	this.lastNameImportations = lastNameImportations;
	
   }
	
   public String getLastNamePurchases() {
	
   	return lastNamePurchases;
	
   }
	
   public void setLastNamePurchases(String lastNamePurchases) {
	
   	this.lastNamePurchases = lastNamePurchases;
	
   }
	
   public String getLastNameSales() {
	
   	return lastNameSales;
	
   }
	
   public void setLastNameSales(String lastNameSales) {
	
   	this.lastNameSales = lastNameSales;
	
   }
   
   public String getLastName() {
	
   	return lastName;
	
   }
	
   public void setLastName(String lastName) {
	
   	this.lastName = lastName;
	
   }
	
   public String getMiddleNameImportations() {
	
   	return middleNameImportations;
	
   }
	
   public void setMiddleNameImportations(String middleNameImportations) {
	
   	this.middleNameImportations = middleNameImportations;
	
   }
	
   public String getMiddleNamePurchases() {
	
   	return middleNamePurchases;
	
   }
	
   public void setMiddleNamePurchases(String middleNamePurchases) {
	
   	this.middleNamePurchases = middleNamePurchases;
	
   }
	
   public String getMiddleNameSales() {
	
   	return middleNameSales;
	
   }
	
   public void setMiddleNameSales(String middleNameSales) {
	
   	this.middleNameSales = middleNameSales;
	
   }
   
   public String getMiddleName() {
	
   	return middleName;
	
   }
	
   public void setMiddleName(String middleName) {
	
   	this.middleName = middleName;
	
   }
	
   public String getNonCreditable() {
	
   	return nonCreditable;
	
   }
	
   public void setNonCreditable(String nonCreditable) {
	
   	this.nonCreditable = nonCreditable;
	
   }
	
   public String getTotalOtherCharges() {
	
   	return totalOtherCharges;
	
   }
	
   public void setTotalOtherCharges(String totalOtherCharges) {
	
   	this.totalOtherCharges = totalOtherCharges;
	
   }
	
   public String getTotalOutputTax() {
	
   	return totalOutputTax;
	
   }
	
   public void setTotalOutputTax(String totalOutputTax) {
	
   	this.totalOutputTax = totalOutputTax;
	
   }
	
   public String getRdoCode() {
	
   	return rdoCode;
	
   }
	
   public void setRdoCode(String rdoCode) {
	
   	this.rdoCode = rdoCode;
	
   }
	
   public String getRegisteredNameImportations() {
	
   	return registeredNameImportations;
	
   }
	
   
   public void setRegisteredNameImportations(String registeredNameImportations) {
	
   	this.registeredNameImportations = registeredNameImportations;
	
   }
	
   public String getRegisteredNamePurchases() {
	
   	return registeredNamePurchases;
	
   }
	
   public void setRegisteredNamePurchases(String registeredNamePurchases) {
	
   	this.registeredNamePurchases = registeredNamePurchases;
	
   }
	
   public String getRegisteredNameSales() {
	
   	return registeredNameSales;
	
   }
	
   public void setRegisteredNameSales(String registeredNameSales) {
	
   	this.registeredNameSales = registeredNameSales;
	
   }
   
   public String getRegisteredName() {
	
   	return registeredName;
	
   }
	
   public void setRegisteredName(String registeredName) {
	
   	this.registeredName = registeredName;
	
   }
	
   public String getTotalServices() {
	
   	return totalServices;
	
   }
	
   public void setTotalServices(String totalServices) {
	
   	this.totalServices = totalServices;
	
   }
	
   public String getTotalTaxableImportations() {
	
   	return totalTaxableImportations;
	
   }
	
   public void setTotalTaxableImportations(String totalTaxableImportations) {
	
   	this.totalTaxableImportations = totalTaxableImportations;
	
   }
	
   public String getTaxableMonth() {
	
   	return taxableMonth;
	
   }
	
   public void setTaxableMonth(String taxableMonth) {
	
   	this.taxableMonth = taxableMonth;
	
   }
	
   public String getTotalTaxableSales() {
	
   	return totalTaxableSales;
	
   }
	
   public void setTotalTaxableSales(String totalTaxableSales) {
	
   	this.totalTaxableSales = totalTaxableSales;
	
   }

   public String getTinOwner() {
	
   	return tinOwner;
	
   }
	
   public void setTinOwner(String tinOwner) {
	
   	this.tinOwner = tinOwner;
	
   }
	
   public String getTotalVatPaid() {
	
   	return totalVatPaid;
	
   }
	
   public void setTotalVatPaid(String totalVatPaid) {
	
   	this.totalVatPaid = totalVatPaid;
	
   }
   
   public boolean getIncludeImportations() {
	
   	return includeImportations;
	
   }
	
   public void setIncludeImportations(boolean includeImportations) {
	
   	this.includeImportations = includeImportations;
	
   }
	
   public String getTotalZeroRatedPurchases() {
	
   	return totalZeroRatedPurchases;
	
   }
	
   public void setTotalZeroRatedPurchases(String totalZeroRatedPurchases) {
	
   	this.totalZeroRatedPurchases = totalZeroRatedPurchases;
	
   }
	
   public String getTotalZeroRatedSales() {
	
   	return totalZeroRatedSales;
	
   }
	
   public void setTotalZeroRatedSales(String totalZeroRatedSales) {
	
   	this.totalZeroRatedSales = totalZeroRatedSales;
	
   }
   
   public String getTradeNameSales() {
   	
   	  return tradeNameSales;   	
   
   }
   
   public void setTradeNameSales(String tradeNameSales) {
   	
   	  this.tradeNameSales = tradeNameSales;
   
   }
   
   public String getTradeNamePurchases() {
   	
   	  return tradeNamePurchases;   	
   
   }
   
   public void setTradeNamePurchases(String tradeNamePurchases) {
   	
   	  this.tradeNamePurchases = tradeNamePurchases;
   
   }
   
   public String getTradeNameImportations() {
   	
   	  return tradeNameImportations;   	
   
   }
   
   public void setTradeNameImportations(String tradeNameImportations) {
   	
   	  this.tradeNameImportations = tradeNameImportations;
   
   }
   
   public String getTradeName() {
   	
   	  return tradeName;   	
   
   }
   
   public void setTradeName(String tradeName) {
   	
   	  this.tradeName = tradeName;
   
   }
   
   public String getReportType() {
   	
   	  return reportType;
   	
   }
   
   public void setReportType(String reportType) {
   	
   	 this.recordType = reportType;
   	
   }
   
   public ArrayList getReportTypeList() {
   	
   	  return reportTypeList;
   	  
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
		dateFrom = null;
		dateTo = null;
		tradeName = null;
		lastName = null;
	    firstName = null;
	    middleName = null;
		rdoCode = null;
		taxableMonth = null;
		fiscalYearEnding = null;
		reportTypeList.clear();
		reportTypeList.add("SALES");
		reportTypeList.add("PURCHASES");
		reportType = "SALES";
		
				
        // group sales
		registeredNameSales = null;
		lastNameSales = null;
		firstNameSales = null;
		middleNameSales = null;
		address1Sales = null;
		address2Sales = null;
		totalExemptSales = null;
		totalZeroRatedSales = null;
		totalTaxableSales = null;
		totalOutputTax = null;
		

	    // group purchases
	    registeredNamePurchases = null;
	    lastNamePurchases = null;
	    firstNamePurchases = null;
	    middleNamePurchases = null;
	    address1Purchases = null;
	    address2Purchases = null;
	    totalExemptPurchases = null;
	    totalZeroRatedPurchases = null;
        totalServices = null;
	    totalCapitalGoods = null;
	    totalGoodsOtherThanCp = null;
	    totalInputTax = null;
	    creditable = null;
	    nonCreditable = null;

	    
	    // group importation
	    registeredNameImportations = null;
	    lastNameImportations = null;
	    firstNameImportations = null;
	    middleNameImportations = null;
	    address1Importations = null;
	    address2Importations = null;
	    totalDutiableValue = null;
	    totalOtherCharges = null;
	    totalExemptImportations = null;
	    totalTaxableImportations = null;
	    totalVatPaid = null;  
	    includeImportations = true;

   }
   
   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = new ActionErrors();

      if(request.getParameter("goButton") != null) { 
      	
      	if(Common.validateRequired(dateFrom)) {
      		
      		errors.add("dateFrom", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateFromRequired"));
      		
      	}
      	
      	if(!Common.validateDateFormat(dateFrom)) {
      		
      		errors.add("dateFrom", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateFromInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(dateTo)) {
      		
      		errors.add("dateTo", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateToRequired"));
      		
      	}
      	
      	if(!Common.validateDateFormat(dateTo)) {
      		
      		errors.add("dateTo", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateToInvalid"));
      		
      	}
      	
      }

      if(request.getParameter("validateGenerateButton") != null) {
      	
      	if(Common.validateRequired(dateFrom)) {
      		
      		errors.add("dateFrom", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateFromRequired"));
      		
      	}
      	
      	if(!Common.validateDateFormat(dateFrom)) {
      		
      		errors.add("dateFrom", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateFromInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(dateTo)) {
      		
      		errors.add("dateTo", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateToRequired"));
      		
      	}
      	
      	if(!Common.validateDateFormat(dateTo)) {
      		
      		errors.add("dateTo", 
      				new ActionMessage("csvQuarterlyVatReturn.error.dateToInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(tinOwner)) {
      		
      		errors.add("tinOwner", 
      				new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerRequired"));
      		
      	}
      	
      	if (tinOwner.length() != 9) {
      		
      		errors.add("tinOwner", 
      				new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(tradeName)) {
      		
      		errors.add("tradeName", 
      				new ActionMessage("csvQuarterlyVatReturn.error.tradeNameRequired"));
      		
      	}

      	if (Common.validateRequired(address1)) {
      		
      		errors.add("address1", 
      				new ActionMessage("csvQuarterlyVatReturn.error.address1Required"));
      		
      	}
      	
      	if (address1.length() > 30) {
      		
      		errors.add("address1", 
      				new ActionMessage("csvQuarterlyVatReturn.error.address1Invalid"));
      		
      	}

      	if (address2.length() > 30 && address2 != null) {
      		
      		errors.add("address2", 
      				new ActionMessage("csvQuarterlyVatReturn.error.address2Invalid"));
      		
      	}

      	if (Common.validateRequired(rdoCode)) {
      		
      		errors.add("rdoCode", 
      				new ActionMessage("csvQuarterlyVatReturn.error.rdoCodeRequired"));
      		
      	}
      	
      	if (!Common.validateNumberFormat(rdoCode)) {
      		
      		errors.add("rdoCode", 
      				new ActionMessage("csvQuarterlyVatReturn.error.rdoCodeInvalid"));
      		
      	}
      	
      	if(rdoCode.length() > 3) {
      		
      		errors.add("rdoCode", 
      				new ActionMessage("csvQuarterlyVatReturn.error.rdoCodeInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(taxableMonth)) {
      		
      		errors.add("taxableMonth", 
      				new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthRequired"));
      		
      	}

      	if(!Common.validateDateFormat(taxableMonth)) {
      		
      		errors.add("taxableMonth", 
      				new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(fiscalYearEnding)) {
      		
      		errors.add("fiscalYearEnding", 
      				new ActionMessage("csvQuarterlyVatReturn.error.fiscalYearEndingRequired"));
      		
      	}
      	
      	if(fiscalYearEnding.length() > 2) {
      		
      		errors.add("fiscalYearEnding", 
      				new ActionMessage("csvQuarterlyVatReturn.error.fiscalYearEndingInvalid"));
			
      	}

      	if(Common.validateRequired(reportType) || reportType.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
      		
           errors.add("reportType", 
           		   new ActionMessage("csvQuarterlyVatReturn.error.reportTypeRequired"));
	    
      	}
      	
      	// FIELDS FOR SALES
      	
      	if (Common.validateRequired(totalExemptSales)) {
      		
      		errors.add("totalExemptSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.exemptSalesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalExemptSales) || Common.validateTaxCsvMoneyDecimalFormat(totalExemptSales)) {
      		
      		errors.add("totalExemptSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.exemptSalesInvalid"));
      		
      	}

      	if (Common.validateRequired(totalZeroRatedSales)) {
      		
      		errors.add("zeroRatedSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedSalesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalZeroRatedSales) || Common.validateTaxCsvMoneyDecimalFormat(totalZeroRatedSales)) {
      		
      		errors.add("zeroRatedSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedSalesInvalid"));
      		
      	}

      	if (Common.validateRequired(totalTaxableSales)) {
      		
      		errors.add("taxableSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.taxableSalesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalTaxableSales) || Common.validateTaxCsvMoneyDecimalFormat(totalTaxableSales)) {
      		
      		errors.add("taxableSales", 
      				new ActionMessage("csvQuarterlyVatReturn.error.taxableSalesInvalid"));
      		
      	}

      	if (Common.validateRequired(totalOutputTax)) {
      		
      		errors.add("outputTax", 
      				new ActionMessage("csvQuarterlyVatReturn.error.outputTaxRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalOutputTax) || Common.validateTaxCsvMoneyDecimalFormat(totalOutputTax)) {
      		
      		errors.add("outputTax", 
      				new ActionMessage("csvQuarterlyVatReturn.error.outputTaxInvalid"));
      		
      	}
      	
      	int salesLines = 0;
      	
      	Iterator i = csvSLList.iterator();      	 
      	
      	while (i.hasNext()) {
      		
      		GlRepCsvQuarterlyVatReturnSalesList slsList = (GlRepCsvQuarterlyVatReturnSalesList)i.next();      	 	 
      		
      		if (Common.validateRequired(slsList.getTinOfCustomer()) &&
      				Common.validateRequired(slsList.getTinOwnerSales()) &&
					Common.validateRequired(slsList.getAddress1Sales()) &&
					Common.validateRequired(slsList.getExemptSales()) &&
					Common.validateRequired(slsList.getZeroRatedSales())&&
					Common.validateRequired(slsList.getOutputTax())&&
					Common.validateRequired(slsList.getTaxableMonthSales())&&
					Common.validateRequired(slsList.getTaxableSales())) continue;
      		
      		salesLines++;
      		
      		if(Common.validateRequired(slsList.getTinOfCustomer())){
      			errors.add("tinOfCustomer",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOfCustomerListRequired", slsList.getLineNumber()));
      		}
      		
      		if(slsList.getTinOfCustomer().length() != 9){
      			errors.add("tinOfCustomer",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOfCustomerListInvalid", slsList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(slsList.getTinOwnerSales())){
      			errors.add("tinOwnerSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerSalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(slsList.getTinOwnerSales().length() != 9){
      			errors.add("tinOwnerSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerSalesListInvalid", slsList.getLineNumber()));
      		}
      		
      		if(slsList.getRegisteredNameSales().length() > 50){
      			errors.add("registeredNameSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.registeredNameSalesListInvalid", slsList.getLineNumber()));
      		}

      		if(Common.validateRequired(slsList.getAddress1Sales())){
      			errors.add("address1Sales",
      					new ActionMessage("csvQuarterlyVatReturn.error.address1SalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(slsList.getAddress1Sales().length() > 30){
      			errors.add("address1Sales",
      					new ActionMessage("csvQuarterlyVatReturn.error.address1SalesListInvalid", slsList.getLineNumber()));
      		}

      		if(slsList.getAddress2Sales().length() > 30 && slsList.getAddress2Sales() != null){
      			errors.add("address2Sales",
      					new ActionMessage("csvQuarterlyVatReturn.error.address2SalesListInvalid", slsList.getLineNumber()));
      		}

      		if(Common.validateRequired(slsList.getExemptSales())){
      			errors.add("exemptSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.exemptSalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(slsList.getExemptSales()) || Common.validateTaxCsvMoneyDecimalFormat(slsList.getExemptSales())){
      			errors.add("exemptSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.exemptSalesListInvalid", slsList.getLineNumber()));
      		}

      		if(Common.validateRequired(slsList.getZeroRatedSales())){
      			errors.add("zeroRatedSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedSalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(slsList.getZeroRatedSales()) || Common.validateTaxCsvMoneyDecimalFormat(slsList.getZeroRatedSales())){
      			errors.add("zeroRatedSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedSalesListInvalid", slsList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(slsList.getOutputTax())){
      			errors.add("outputTax",
      					new ActionMessage("csvQuarterlyVatReturn.error.outputTaxListRequired", slsList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(slsList.getOutputTax()) || Common.validateTaxCsvMoneyDecimalFormat(slsList.getOutputTax())){
      			errors.add("outputTax",
      					new ActionMessage("csvQuarterlyVatReturn.error.outputTaxListInvalid", slsList.getLineNumber()));
      		}

      		if(Common.validateRequired(slsList.getTaxableMonthSales())){
      			errors.add("taxableMonthSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthSalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(!Common.validateDateFormat(slsList.getTaxableMonthSales())){
      			
      			errors.add("taxableMonthSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthSalesListInvalid", slsList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(slsList.getTaxableSales())){
      			errors.add("taxableSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableSalesListRequired", slsList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(slsList.getTaxableSales()) || Common.validateTaxCsvMoneyDecimalFormat(slsList.getTaxableSales())){
      			errors.add("taxableSales",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableSalesListInvalid", slsList.getLineNumber()));
      		}
      		
      	}
      	
      	
      	// FIELDS FOR PURCHASES
      	
      	if (Common.validateRequired(totalExemptPurchases)) {
      		
      		errors.add("totalExemptPurchases", 
      				new ActionMessage("csvQuarterlyVatReturn.error.exemptPurchasesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalExemptPurchases) || Common.validateTaxCsvMoneyDecimalFormat(totalExemptPurchases)) {
      		
      		errors.add("totalExemptPurchases", 
      				new ActionMessage("csvQuarterlyVatReturn.error.exemptPurchasesInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(totalZeroRatedPurchases)) {
      		
      		errors.add("totalZeroRatedPurchases", 
      				new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedPurchasesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalZeroRatedPurchases) || Common.validateTaxCsvMoneyDecimalFormat(totalZeroRatedPurchases)) {
      		
      		errors.add("totalZeroRatedPurchases", 
      				new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedPurchasesInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(totalServices)) {
      		
      		errors.add("totalServices", 
      				new ActionMessage("csvQuarterlyVatReturn.error.servicesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalServices) || Common.validateTaxCsvMoneyDecimalFormat(totalServices)) {
      		
      		errors.add("totalServices", 
      				new ActionMessage("csvQuarterlyVatReturn.error.servicesInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(totalCapitalGoods)) {
      		
      		errors.add("totalCapitalGoods", 
      				new ActionMessage("csvQuarterlyVatReturn.error.capitalGoodsRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalCapitalGoods) || Common.validateTaxCsvMoneyDecimalFormat(totalCapitalGoods)) {
      		
      		errors.add("totalCapitalGoods", 
      				new ActionMessage("csvQuarterlyVatReturn.error.capitalGoodsInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(totalGoodsOtherThanCp)) {
      		
      		errors.add("totalGoodsOtherThanCp", 
      				new ActionMessage("csvQuarterlyVatReturn.error.goodsOtherThanCpRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalGoodsOtherThanCp) || Common.validateTaxCsvMoneyDecimalFormat(totalGoodsOtherThanCp)) {
      		
      		errors.add("totalGoodsOtherThanCp", 
      				new ActionMessage("csvQuarterlyVatReturn.error.goodsOtherThanCpInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(totalInputTax)) {
      		
      		errors.add("totalInputTax", 
      				new ActionMessage("csvQuarterlyVatReturn.error.inputTaxRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalInputTax) || Common.validateTaxCsvMoneyDecimalFormat(totalInputTax)) {
      		
      		errors.add("totalInputTax", 
      				new ActionMessage("csvQuarterlyVatReturn.error.inputTaxInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(creditable)) {
      		
      		errors.add("creditable", 
      				new ActionMessage("csvQuarterlyVatReturn.error.creditableRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(creditable) || Common.validateTaxCsvMoneyDecimalFormat(creditable)) {
      		
      		errors.add("creditable", 
      				new ActionMessage("csvQuarterlyVatReturn.error.creditableInvalid"));
      		
      	}
      	
      	if (Common.validateRequired(nonCreditable)) {
      		
      		errors.add("nonCreditable", 
      				new ActionMessage("csvQuarterlyVatReturn.error.nonCreditableRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(nonCreditable) || Common.validateTaxCsvMoneyDecimalFormat(nonCreditable)) {
      		
      		errors.add("nonCreditable", 
      				new ActionMessage("csvQuarterlyVatReturn.error.nonCreditableInvalid"));
      		
      	}
      	
      	int purchasesLines = 0;
      	
      	Iterator j = csvPRList.iterator();      	 
      	
      	while (j.hasNext()) {
      		
      		GlRepCsvQuarterlyVatReturnPurchasesList purList = (GlRepCsvQuarterlyVatReturnPurchasesList)j.next();      	 	 
      		
      		if (Common.validateRequired(purList.getTinOfSupplier()) &&
      				Common.validateRequired(purList.getTinOwnerPurchases()) &&
					Common.validateRequired(purList.getAddress1Purchases()) &&
					Common.validateRequired(purList.getExemptPurchases()) &&
					Common.validateRequired(purList.getZeroRatedPurchases())&&
					Common.validateRequired(purList.getServices())&&
					Common.validateRequired(purList.getInputTax())&&
					Common.validateRequired(purList.getCapitalGoods())&&
					Common.validateRequired(purList.getGoodsOtherThanCp())&&
					Common.validateRequired(purList.getTaxableMonthPurchases())) continue;
      		
      		purchasesLines++;
      		
      		if(Common.validateRequired(purList.getTinOfSupplier())){
      			errors.add("tinOfSupplier",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOfSupplierListRequired", purList.getLineNumber()));
      		}
      		
      		if(purList.getTinOfSupplier().length() != 9){
      			errors.add("tinOfSupplier",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOfSupplierListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getTinOwnerPurchases())){
      			errors.add("tinOwnerPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerPurchasesListRequired", purList.getLineNumber()));
      		}
      		
      		if(purList.getTinOwnerPurchases().length() != 9){
      			errors.add("tinOwnerPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.tinOwnerPurchasesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(purList.getRegisteredNamePurchases().length() > 50){
      			errors.add("registeredNamePurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.registeredNamePurchasesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getAddress1Purchases())){
      			errors.add("address1Purchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.address1PurchasesListRequired", purList.getLineNumber()));
      		}
      		
      		if(purList.getAddress1Purchases().length() > 30){
      			errors.add("address1Purchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.address1PurchasesListInvalid", purList.getLineNumber()));
      		}

      		if(purList.getAddress2Purchases().length() > 30 && purList.getAddress2Purchases() != null){
      			errors.add("address2Purchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.address2PurchasesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getExemptPurchases())){
      			errors.add("exemptPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.exemptPurchasesListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getExemptPurchases()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getExemptPurchases())){
      			errors.add("exemptPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.exemptPurchasesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getZeroRatedPurchases())){
      			errors.add("zeroRatedPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedPurchasesListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getZeroRatedPurchases()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getZeroRatedPurchases())){
      			errors.add("zeroRatedPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.zeroRatedPurchasesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getInputTax())){
      			errors.add("inputTax",
      					new ActionMessage("csvQuarterlyVatReturn.error.inputTaxListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getInputTax()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getInputTax())){
      			errors.add("inputTax",
      					new ActionMessage("csvQuarterlyVatReturn.error.inputTaxListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getServices())){
      			errors.add("services",
      					new ActionMessage("csvQuarterlyVatReturn.error.servicesListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getServices()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getServices())){
      			errors.add("services",
      					new ActionMessage("csvQuarterlyVatReturn.error.servicesListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getCapitalGoods())){
      			errors.add("capitalGoods",
      					new ActionMessage("csvQuarterlyVatReturn.error.capitalGoodsListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getCapitalGoods()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getCapitalGoods())){
      			errors.add("capitalGoods",
      					new ActionMessage("csvQuarterlyVatReturn.error.capitalGoodsListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getGoodsOtherThanCp())){
      			errors.add("goodsOtherThanCp",
      					new ActionMessage("csvQuarterlyVatReturn.error.goodsOtherThanCpListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateTaxCsvMoneyFormat(purList.getGoodsOtherThanCp()) || Common.validateTaxCsvMoneyDecimalFormat(purList.getGoodsOtherThanCp())){
      			errors.add("goodsOtherThanCp",
      					new ActionMessage("csvQuarterlyVatReturn.error.goodsOtherThanCpListInvalid", purList.getLineNumber()));
      		}
      		
      		if(Common.validateRequired(purList.getTaxableMonthPurchases())){
      			errors.add("taxableMonthPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthPurchasesListRequired", purList.getLineNumber()));
      		}
      		
      		if(!Common.validateDateFormat(purList.getTaxableMonthPurchases())){
      			errors.add("taxableMonthPurchases",
      					new ActionMessage("csvQuarterlyVatReturn.error.taxableMonthPurchasesListInvalid", purList.getLineNumber()));
      		}
      		      		      		
      	}
      	
      	//FIELDS FOR IMPORTATION
      	
      	if(Common.validateRequired(totalDutiableValue)) {
      		
      		errors.add("totalDutiableValue", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalDutiableValueRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalDutiableValue) || Common.validateTaxCsvMoneyDecimalFormat(totalDutiableValue)) {
      		
      		errors.add("totalDutiableValue", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalDutiableValueInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(totalOtherCharges)) {
      		
      		errors.add("totalOtherCharges", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalOtherChargesRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalOtherCharges) || Common.validateTaxCsvMoneyDecimalFormat(totalOtherCharges)) {
      		
      		errors.add("totalOtherCharges", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalOtherChargesInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(totalExemptImportations)) {
      		
      		errors.add("totalExemptImportations", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalExemptImportationsRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalExemptImportations) || Common.validateTaxCsvMoneyDecimalFormat(totalExemptImportations)) {
      		
      		errors.add("totalExemptImportations", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalExemptImportationsInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(totalTaxableImportations)) {
      		
      		errors.add("totalTaxableImportations", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalTaxableImportationsRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalTaxableImportations) || Common.validateTaxCsvMoneyDecimalFormat(totalTaxableImportations)) {
      		
      		errors.add("totalTaxableImportations", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalTaxableImportationsInvalid"));
      		
      	}
      	
      	if(Common.validateRequired(totalVatPaid)) {
      		
      		errors.add("totalVatPaid", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalVatPaidRequired"));
      		
      	}
      	
      	if (!Common.validateTaxCsvMoneyFormat(totalVatPaid) || Common.validateTaxCsvMoneyDecimalFormat(totalVatPaid)) {
      		
      		errors.add("totalVatPaid", 
      				new ActionMessage("csvQuarterlyVatReturn.error.totalVatPaidInvalid"));
      		
      	} 
      	
      	Iterator k = csvIMList.iterator();
      	
      	while(k.hasNext()) {
      		
      		GlRepCsvQuarterlyVatReturnImportationsList impList =  (GlRepCsvQuarterlyVatReturnImportationsList) k.next();
      		
      		if(impList.getImportEntry().length() > 10 && impList.getImportEntry() != null) {
      			errors.add("importEntry", 
      					new ActionMessage("csvQuarterlyVatReturn.error.importEntryImportationsListInvalid", impList.getLineNumber()));
      		}
      		
      		if(!Common.validateDateFormat(impList.getAssessmentReleaseDate()) && impList.getAssessmentReleaseDate() != null){
      			errors.add("assessmentReleaseDate",
      					new ActionMessage("csvQuarterlyVatReturn.error.assessmentReleaseDateInvalid", impList.getLineNumber()));
      		}
      		
      		if(impList.getNameOfSeller().length() > 50 && impList.getNameOfSeller() != null) {
      			errors.add("nameOfSeller", 
      					new ActionMessage("csvQuarterlyVatReturn.error.nameOfSellerInvalid", impList.getLineNumber()));
      		}
      		
      		if(!Common.validateDateFormat(impList.getDateOfImportation()) && impList.getDateOfImportation() != null){
      			errors.add("dateOfImportation",
      					new ActionMessage("csvQuarterlyVatReturn.error.dateOfImportationInvalid", impList.getLineNumber()));
      		}
      		
      		if(impList.getCountryOfOrigin().length() > 30 && impList.getCountryOfOrigin() != null) {
      			errors.add("countryOfOrigin", 
      					new ActionMessage("csvQuarterlyVatReturn.error.countryOfOriginInvalid", impList.getLineNumber()));
      		}
      		
      		if (!Common.validateTaxCsvMoneyFormat(impList.getDutiableValue()) || Common.validateTaxCsvMoneyDecimalFormat(impList.getDutiableValue()) && 
      					impList.getDutiableValue() != null ) {          	
          		errors.add("dutiableValue", 
          				new ActionMessage("csvQuarterlyVatReturn.error.dutiableValueInvalid", impList.getLineNumber()));          		
          	} 
      		
      		if (!Common.validateTaxCsvMoneyFormat(impList.getOtherCharges()) || Common.validateTaxCsvMoneyDecimalFormat(impList.getOtherCharges()) && 
  					impList.getOtherCharges() != null ) {      		
      			errors.add("otherCharges", 
      				new ActionMessage("csvQuarterlyVatReturn.error.otherChargesInvalid", impList.getLineNumber()));      		
      		}
      		
      		if (!Common.validateTaxCsvMoneyFormat(impList.getExemptImportation()) || Common.validateTaxCsvMoneyDecimalFormat(impList.getExemptImportation()) && 
  					impList.getExemptImportation() != null ) {      		
      			errors.add("exemptImportation", 
      				new ActionMessage("csvQuarterlyVatReturn.error.exemptImportationInvalid", impList.getLineNumber()));      		
      		}
      		
      		if (!Common.validateTaxCsvMoneyFormat(impList.getTaxableImportation()) || Common.validateTaxCsvMoneyDecimalFormat(impList.getTaxableImportation()) && 
  					impList.getTaxableImportation() != null ) {      		
      			errors.add("taxableImportation", 
      				new ActionMessage("csvQuarterlyVatReturn.error.taxableImportationInvalid", impList.getLineNumber()));      		
      		}
      		
      		if (!Common.validateTaxCsvMoneyFormat(impList.getVatPaid()) || Common.validateTaxCsvMoneyDecimalFormat(impList.getVatPaid()) && 
  					impList.getVatPaid() != null ) {      		
      			errors.add("vatPaid", 
      				new ActionMessage("csvQuarterlyVatReturn.error.vatPaidInvalid", impList.getLineNumber()));      		
      		}
      		
      		if(impList.getOrNumber().length() > 15 && impList.getOrNumber() != null) {
      			errors.add("orNumber", 
      					new ActionMessage("csvQuarterlyVatReturn.error.orNumberInvalid", impList.getOrNumber()));
      		}
      		
      		if(!Common.validateDateFormat(impList.getDateOfVatPayment()) && impList.getDateOfVatPayment() != null){
      			errors.add("dateOfVatPayment",
      					new ActionMessage("csvQuarterlyVatReturn.error.dateOfVatPaymentInvalid", impList.getLineNumber()));
      		}
      	}
      	
      }
      
      return(errors);
      
   }             
   
}