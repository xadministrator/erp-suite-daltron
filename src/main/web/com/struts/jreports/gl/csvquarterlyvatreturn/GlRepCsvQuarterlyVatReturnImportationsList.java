package com.struts.jreports.gl.csvquarterlyvatreturn;

import java.io.Serializable;

public class GlRepCsvQuarterlyVatReturnImportationsList implements Serializable {

   private String recordType = null;
   private String transactionType = null;
   private String importEntry = null;
   private String assessmentReleaseDate = null;
   private String nameOfSeller = null;
   private String dateOfImportation = null;
   private String countryOfOrigin = null;
   private String dutiableValue = null;
   private String otherCharges = null;
   private String exemptImportation = null;
   private String taxableImportation = null;
   private String vatPaid = null;
   private String orNumber = null;
   private String dateOfVatPayment = null;
   private String tinOwnerImportations = null;
   private String taxableMonthImportation = null;
   private String lineNumber = null;

   private boolean deleteCheckbox = false;
   
   private GlRepCsvQuarterlyVatReturnForm parentBean;
    
   public GlRepCsvQuarterlyVatReturnImportationsList(GlRepCsvQuarterlyVatReturnForm parentBean,
		String importEntry,
		String assessmentReleaseDate,
		String nameOfSeller,
		String dateOfImportation,
		String countryOfOrigin,
		String dutiableValue,
		String otherCharges,
		String exemptImportation,
		String taxableImportation,
		String vatPaid,
		String orNumber,
		String dateOfVatPayment,
		String tinOwnerImportations,
		String taxableMonthImportation, 
		String lineNumber) {

   	  this.parentBean = parentBean; 
      this.importEntry = importEntry;
      this.assessmentReleaseDate = assessmentReleaseDate;
      this.nameOfSeller = nameOfSeller;
      this.dateOfImportation = dateOfImportation;
      this.countryOfOrigin = countryOfOrigin;
      this.dutiableValue = dutiableValue;
      this.otherCharges = otherCharges;
      this.exemptImportation = exemptImportation;
      this.taxableImportation = taxableImportation;
      this.vatPaid = vatPaid;
      this.orNumber = orNumber;
      this.dateOfVatPayment = dateOfVatPayment;
      this.tinOwnerImportations = tinOwnerImportations;
      this.taxableMonthImportation = taxableMonthImportation;
      this.lineNumber = lineNumber;
      
   }

   public String getAssessmentReleaseDate() {
   	return assessmentReleaseDate;
   }
   public void setAssessmentReleaseDate(String assessmentReleaseDate) {
   	this.assessmentReleaseDate = assessmentReleaseDate;
   }
   public String getCountryOfOrigin() {
   	return countryOfOrigin;
   }
   public void setCountryOfOrigin(String countryOfOrigin) {
   	this.countryOfOrigin = countryOfOrigin;
   }
   public String getDateOfImportation() {
   	return dateOfImportation;
   }
   public void setDateOfImportation(String dateOfImportation) {
   	this.dateOfImportation = dateOfImportation;
   }
   public String getDateOfVatPayment() {
   	return dateOfVatPayment;
   }
   public void setDateOfVatPayment(String dateOfVatPayment) {
   	this.dateOfVatPayment = dateOfVatPayment;
   }
   public String getDutiableValue() {
   	return dutiableValue;
   }
   public void setDutiableValue(String dutiableValue) {
   	this.dutiableValue = dutiableValue;
   }
   public String getExemptImportation() {
   	return exemptImportation;
   }
   public void setExemptImportation(String exemptImportation) {
   	this.exemptImportation = exemptImportation;
   }
   public String getImportEntry() {
   	return importEntry;
   }
   public void setImportEntry(String importEntry) {
   	this.importEntry = importEntry;
   }
   public String getNameOfSeller() {
   	return nameOfSeller;
   }
   public void setNameOfSeller(String nameOfSeller) {
   	this.nameOfSeller = nameOfSeller;
   }
   public String getOrNumber() {
   	return orNumber;
   }
   public void setOrNumber(String orNumber) {
   	this.orNumber = orNumber;
   }
   public String getOtherCharges() {
   	return otherCharges;
   }
   public void setOtherCharges(String otherCharges) {
   	this.otherCharges = otherCharges;
   }
   public String getRecordType() {
   	return recordType;
   }
   public void setRecordType(String recordType) {
   	this.recordType = recordType;
   }
   public String getTaxableImportation() {
   	return taxableImportation;
   }
   public void setTaxableImportation(String taxableImportation) {
   	this.taxableImportation = taxableImportation;
   }
   public String getTaxableMonthImportation() {
   	return taxableMonthImportation;
   }
   public void setTaxableMonthImportation(String taxableMonthImportation) {
   	this.taxableMonthImportation = taxableMonthImportation;
   }
   public String getTinOwnerImportations() {
   	return tinOwnerImportations;
   }
   public void setTinOwnerImportations(String tinOwnerImportations) {
   	this.tinOwnerImportations = tinOwnerImportations;
   }
   public String getTransactionType() {
   	return transactionType;
   }
   public void setTransactionType(String transactionType) {
   	this.transactionType = transactionType;
   }
   public String getVatPaid() {
   	return vatPaid;
   }
   public void setVatPaid(String vatPaid) {
   	this.vatPaid = vatPaid;
   }   
   public boolean getDeleteCheckbox() {	
   	  return deleteCheckbox;	
   }	
   public void setDeleteCheckbox(boolean deleteCheckbox) {		
   	  this.deleteCheckbox = deleteCheckbox;		
   }
   public String getLineNumber() {
   	  return lineNumber;
   }
   public void setLineNumber(String lineNumber) {
   	  this.lineNumber = lineNumber;
   }
   
}

