package com.struts.jreports.gl.csvquarterlyvatreturn;

import java.io.Serializable;

public class GlRepCsvQuarterlyVatReturnPurchasesList implements Serializable {

   private String recordType = null;
   private String transactionType = null;
   private String tinOfSupplier = null;
   private String registeredNamePurchases = null;
   private String lastNamePurchases = null;
   private String firstNamePurchases = null;
   private String middleNamePurchases = null;
   private String address1Purchases = null;
   private String address2Purchases = null;
   private String exemptPurchases = null;
   private String zeroRatedPurchases = null;
   private String services = null;
   private String capitalGoods = null;
   private String goodsOtherThanCp = null;
   private String inputTax = null;
   private String tinOwnerPurchases = null;
   private String taxableMonthPurchases = null;
   
   private String lineNumber = null;
   
   private GlRepCsvQuarterlyVatReturnForm parentBean;
    
   public GlRepCsvQuarterlyVatReturnPurchasesList(GlRepCsvQuarterlyVatReturnForm parentBean,
   	  String tinOfSupplier,
	  String registeredNamePurchases,
      String lastNamePurchases,
      String firstNamePurchases,
      String middleNamePurchases,
      String address1Purchases,
      String address2Purchases,
	  String exemptPurchases,
      String zeroRatedPurchases,
      String services,
      String capitalGoods,
      String goodsOtherThanCp,
      String inputTax,
      String tinOwnerPurchases,
      String taxableMonthPurchases,
	  String lineNumber) {

   	this.parentBean = parentBean;
   	this.tinOfSupplier = tinOfSupplier;
    this.registeredNamePurchases = registeredNamePurchases;
    this.lastNamePurchases = lastNamePurchases;
    this.firstNamePurchases = firstNamePurchases;
    this.middleNamePurchases = middleNamePurchases;
    this.address1Purchases = address1Purchases;
    this.address2Purchases = address2Purchases;
    this.exemptPurchases = exemptPurchases;
    this.zeroRatedPurchases = zeroRatedPurchases;
    this.services = services;
    this.capitalGoods = capitalGoods;
    this.goodsOtherThanCp = goodsOtherThanCp;
    this.inputTax = inputTax;
    this.tinOwnerPurchases = tinOwnerPurchases;
    this.taxableMonthPurchases = taxableMonthPurchases;
    this.lineNumber = lineNumber;

   }

   public String getLineNumber() {  	
   	return lineNumber;  	
   }  
   public void setLineNumber(String lineNumber) {  	
   	this.lineNumber = lineNumber;	
   }
   public String getAddress1Purchases() {
   	return address1Purchases;
   }
   public void setAddress1Purchases(String address1Purchases) {
   	this.address1Purchases = address1Purchases;
   }
   public String getAddress2Purchases() {
   	return address2Purchases;
   }
   public void setAddress2Purchases(String address2Purchases) {
   	this.address2Purchases = address2Purchases;
   }
   public String getCapitalGoods() {
   	return capitalGoods;
   }
   public void setCapitalGoods(String capitalGoods) {
   	this.capitalGoods = capitalGoods;
   }
   public String getExemptPurchases() {
   	return exemptPurchases;
   }
   public void setExemptPurchases(String exemptPurchases) {
   	this.exemptPurchases = exemptPurchases;
   }
   public String getFirstNamePurchases() {
   	return firstNamePurchases;
   }
   public void setFirstNamePurchases(String firstNamePurchases) {
   	this.firstNamePurchases = firstNamePurchases;
   }
   public String getGoodsOtherThanCp() {
   	return goodsOtherThanCp;
   }
   public void setGoodsOtherThanCp(String goodsOtherThanCp) {
   	this.goodsOtherThanCp = goodsOtherThanCp;
   }
   public String getInputTax() {
   	return inputTax;
   }
   public void setInputTax(String inputTax) {
   	this.inputTax = inputTax;
   }
   public String getLastNamePurchases() {
   	return lastNamePurchases;
   }
   public void setLastNamePurchases(String lastNamePurchases) {
   	this.lastNamePurchases = lastNamePurchases;
   }
   public String getMiddleNamePurchases() {
   	return middleNamePurchases;
   }
   public void setMiddleNamePurchases(String middleNamePurchases) {
   	this.middleNamePurchases = middleNamePurchases;
   }
   public String getRecordType() {
   	return recordType;
   }
   public void setRecordType(String recordType) {
   	this.recordType = recordType;
   }
   public String getRegisteredNamePurchases() {
   	return registeredNamePurchases;
   }
   public void setRegisteredNamePurchases(String registeredNamePurchases) {
   	this.registeredNamePurchases = registeredNamePurchases;
   }
   public String getServices() {
   	return services;
   }
   public void setServices(String services) {
   	this.services = services;
   }
   public String getTaxableMonthPurchases() {
   	return taxableMonthPurchases;
   }
   public void setTaxableMonthPurchases(String taxableMonthPurchases) {
   	this.taxableMonthPurchases = taxableMonthPurchases;
   }
   public String getTinOfSupplier() {
   	return tinOfSupplier;
   }
   public void setTinOfSupplier(String tinOfSupplier) {
   	this.tinOfSupplier = tinOfSupplier;
   }
   public String getTinOwnerPurchases() {
   	return tinOwnerPurchases;
   }
   public void setTinOwnerPurchases(String tinOwnerPurchases) {
   	this.tinOwnerPurchases = tinOwnerPurchases;
   }
   public String getTransactionType() {
   	return transactionType;
   }
   public void setTransactionType(String transactionType) {
   	this.transactionType = transactionType;
   }
   public String getZeroRatedPurchases() {
   	return zeroRatedPurchases;
   }
   public void setZeroRatedPurchases(String zeroRatedPurchases) {
   	this.zeroRatedPurchases = zeroRatedPurchases;
   }
   public GlRepCsvQuarterlyVatReturnForm getParentBean() {
   	return parentBean;
   }

}

