package com.struts.jreports.gl.csvquarterlyvatreturn;

import java.io.Serializable;

public class GlRepCsvQuarterlyVatReturnSalesList implements Serializable {

   private String recordType = null;
   private String transactionType = null;
   private String tinOfCustomer = null;
   private String registeredNameSales = null;
   private String lastNameSales = null;
   private String firstNameSales = null;
   private String middleNameSales = null;
   private String address1Sales = null;
   private String address2Sales = null;
   private String exemptSales = null;
   private String zeroRatedSales = null;
   private String taxableSales = null;
   private String outputTax = null;
   private String tinOwnerSales = null;
   private String taxableMonthSales = null;
   
   private String lineNumber = null;

   private GlRepCsvQuarterlyVatReturnForm parentBean;

   public GlRepCsvQuarterlyVatReturnSalesList(GlRepCsvQuarterlyVatReturnForm parentBean,
      String tinOfCustomer,
	  String registeredNameSales,
      String lastNameSales,
      String firstNameSales,
      String middleNameSales,
      String address1Sales,
      String address2Sales,
      String exemptSales,
      String zeroRatedSales,
      String taxableSales,
      String outputTax,
      String tinOwnerSales,
      String taxableMonthSales,
	  String lineNumber) {

      this.parentBean = parentBean;   	
      this.tinOfCustomer = tinOfCustomer;
      this.registeredNameSales = registeredNameSales;
      this.lastNameSales = lastNameSales;
      this.firstNameSales = firstNameSales;
      this.middleNameSales = middleNameSales;
      this.address1Sales = address1Sales;
      this.address2Sales = address2Sales;
      this.exemptSales = exemptSales;
      this.zeroRatedSales = zeroRatedSales;
      this.taxableSales = taxableSales;
      this.outputTax = outputTax;
      this.tinOwnerSales = tinOwnerSales;
      this.taxableMonthSales = taxableMonthSales;
      this.lineNumber = lineNumber;

   }
   
   public String getLineNumber() {  	
   	  return lineNumber;  	
   }  
   public void setLineNumber(String lineNumber) {  	
   	  this.lineNumber = lineNumber;	
   }
   public String getAddress1Sales() {
   	return address1Sales;
   }
   public void setAddress1Sales(String address1Sales) {
   	this.address1Sales = address1Sales;
   }
   public String getAddress2Sales() {
   	return address2Sales;
   }
   public void setAddress2Sales(String address2Sales) {
   	this.address2Sales = address2Sales;
   }
   public String getExemptSales() {
   	return exemptSales;
   }
   public void setExemptSales(String exemptSales) {
   	this.exemptSales = exemptSales;
   }
   public String getFirstNameSales() {
   	return firstNameSales;
   }
   public void setFirstNameSales(String firstNameSales) {
   	this.firstNameSales = firstNameSales;
   }
   public String getLastNameSales() {
   	return lastNameSales;
   }
   public void setLastNameSales(String lastNameSales) {
   	this.lastNameSales = lastNameSales;
   }
   public String getMiddleNameSales() {
   	return middleNameSales;
   }
   public void setMiddleNameSales(String middleNameSales) {
   	this.middleNameSales = middleNameSales;
   }
   public String getOutputTax() {
   	return outputTax;
   }
   public void setOutputTax(String outputTax) {
   	this.outputTax = outputTax;
   }
   public String getRecordType() {
   	return recordType;
   }
   public void setRecordType(String recordType) {
   	this.recordType = recordType;
   }
   public String getRegisteredNameSales() {
   	return registeredNameSales;
   }
   public void setRegisteredNameSales(String registeredNameSales) {
   	this.registeredNameSales = registeredNameSales;
   }
   public String getTaxableMonthSales() {
   	return taxableMonthSales;
   }
   public void setTaxableMonthSales(String taxableMonthSales) {
   	this.taxableMonthSales = taxableMonthSales;
   }
   public String getTaxableSales() {
   	return taxableSales;
   }
   public void setTaxableSales(String taxableSales) {
   	this.taxableSales = taxableSales;
   }
   public String getTinOfCustomer() {
   	return tinOfCustomer;
   }
   public void setTinOfCustomer(String tinOfCustomer) {
   	this.tinOfCustomer = tinOfCustomer;
   }
   public String getTinOwnerSales() {
   	return tinOwnerSales;
   }
   public void setTinOwnerSales(String tinOwnerSales) {
   	this.tinOwnerSales = tinOwnerSales;
   }
   public String getTransactionType() {
   	return transactionType;
   }
   public void setTransactionType(String transactionType) {
   	this.transactionType = transactionType;
   }
   public String getZeroRatedSales() {
   	return zeroRatedSales;
   }
   public void setZeroRatedSales(String zeroRatedSales) {
   	this.zeroRatedSales = zeroRatedSales;
   }  
}

