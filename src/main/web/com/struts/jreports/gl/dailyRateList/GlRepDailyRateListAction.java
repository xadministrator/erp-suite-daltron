package com.struts.jreports.gl.dailyRateList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepDailyRateListController;
import com.ejb.txn.GlRepDailyRateListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;

public final class GlRepDailyRateListAction extends Action{
	
	private org.apache.commons.logging.Log log =
		org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
			/*******************************************************
			 Check if user has a session
			 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("GlRepDailyListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			GlRepDailyRateListForm actionForm = (GlRepDailyRateListForm) form;
			
			// set report to null
			
			actionForm.setReport(null);
			
			String frParam = Common.getUserPermission(user, Constants.GL_REP_DAILY_RATE_LIST_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("glRepDailyRateList");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize GlRepDailyRateListController EJB
*******************************************************/
			
			GlRepDailyRateListControllerHome homeDR = null;
			GlRepDailyRateListController ejbDR = null;
			
			try {
				
				homeDR = (GlRepDailyRateListControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/GlRepDailyRateListControllerEJB", GlRepDailyRateListControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in GlRepDailyRateListAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbDR = homeDR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in GlRepDailyRateListAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();	
			
			 /*** get report session and if not null set it to null **/
			 
			 Report reportSession = 
			    (Report)session.getAttribute(Constants.REPORT_KEY);
			    
			 if(reportSession != null) {
			 	
			    reportSession.setBytes(null);
			    session.setAttribute(Constants.REPORT_KEY, reportSession);
			    
			 }
			
/*******************************************************
   -- GL DR Go Action --
*******************************************************/			 
			 if(request.getParameter("goButton") != null &&
			 		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			 	
	            ArrayList list = null;
	            
	            String company = null;

	             // create criteria 
	            if (request.getParameter("goButton") != null) {

		        	HashMap criteria = new HashMap();            	
	                
		        	if (!Common.validateRequired(actionForm.getDateFrom())) {
		        		
		        		criteria.put("dateFrom", actionForm.getDateFrom());
		        		
		        	}

		        	if (!Common.validateRequired(actionForm.getDateTo())) {
		        		
		        		criteria.put("dateTo", actionForm.getDateTo());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getCurrency())) {
		        		
		        		criteria.put("currency", actionForm.getCurrency());
		        		
		        	}

		        	// save criteria

		        	actionForm.setCriteria(criteria);
		        	
		     	}

			    try {
			    	
			       // get company
			       
			       AdCompanyDetails adCmpDetails = ejbDR.getAdCompany(user.getCmpCode());
			       company = adCmpDetails.getCmpName();
			       
			       // execute report
			    		    
			       list = ejbDR.executeDailyRateList(actionForm.getCriteria(), user.getCmpCode());
			           
			    } catch (GlobalNoRecordFoundException ex) {
			    	
		              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("dailyRateList.error.noRecordFound"));
	               
			    } catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepDailyRateListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }			 	
			 	
			    if (!errors.isEmpty()) {
			    	
			    	saveErrors(request, new ActionMessages(errors));
			    	return mapping.findForward("glRepDailyRateList");
			    	
			    }
			    
			    // fill report parameters, fill report to pdf and set report session
			    
			    Map parameters = new HashMap();
			    parameters.put("company", company);
			    parameters.put("printedBy", user.getUserName());
			    parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			    parameters.put("viewType", actionForm.getViewType());
			    
			    if (actionForm.getCurrency() != null) {
			    	
			    	parameters.put("currency", actionForm.getCurrency());
			    	
			    }
			    
			    if (actionForm.getDateFrom() != null) {
			    	
			    	parameters.put("dateFrom", actionForm.getDateFrom());
			    	
			    }
			    
			    if (actionForm.getDateTo() != null) {
			    	
			    	parameters.put("dateTo", actionForm.getDateTo());
			    	
			    }
			    
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepDailyRateList.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/GlRepDailyRateList.jasper");
			    
		        }

		        try {
		        	
		        	Report report = new Report();
		        	
		        	if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		        		
		        		report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		        		report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
		        						new GlRepDailyRateListDS(list)));   
		        		
		        	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
		        		
		        		report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		        		report.setBytes(JasperRunManagerExt.runReportToXls(filename, parameters, 
		        						new GlRepDailyRateListDS(list)));   
		        		
		        	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
		        		
		        		report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
		        		report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
		        				new GlRepDailyRateListDS(list)));												    
		        		
		        	}
		        	
		        	session.setAttribute(Constants.REPORT_KEY, report);
		        	actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        	
		        } catch(Exception ex) {
		        	
		        	if(log.isInfoEnabled()) {
		        		log.info("Exception caught in GlRepDailyRateListAction.execute(): " + ex.getMessage() +
		        				" session: " + session.getId());
		        		
		        	}
		        	
		        	return(mapping.findForward("cmnErrorPage"));
		        	
		        }
		        
/*******************************************************
   -- GL DR Close Action --
*******************************************************/		        

			 } else if (request.getParameter("closeButton") != null) {
			 	
			 	return(mapping.findForward("cmnMain"));

/*******************************************************
   -- GL DR Load Action --
*******************************************************/			 
			 }

		     ArrayList list = null;
	         Iterator i = null;
			 
	         if(frParam != null) {
	         	
	         	try {
	         		
	         		actionForm.clearCurrencyList();
	         		
	         		list = ejbDR.getGlFcAll(user.getCmpCode());
	         		
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            		
	            			String currency = (String)i.next();
	            			
	      		          if (!"USD".equals(currency.trim())) {
	      		          	
	      		          	actionForm.setCurrencyList(currency);

	    			  	  }
	            		    
	            			
	            		}
	            		
	            	}

	         		
	         	} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepDailyRateListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	         	actionForm.reset(mapping, request);
	         	return(mapping.findForward("glRepDailyRateList"));	
	         	
	         } else {
	         	
	         	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	         	saveErrors(request, new ActionMessages(errors));
	         	
	         	return(mapping.findForward("cmnMain"));
	         	
	         }
	         
		} catch(Exception e) { 
			
			e.printStackTrace();
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in GlRepDailyRateListAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}   
			
			return(mapping.findForward("cmnErrorPage"));   
			
		}
		
		
	}
	
}
