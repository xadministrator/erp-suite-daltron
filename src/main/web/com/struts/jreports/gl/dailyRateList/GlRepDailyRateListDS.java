package com.struts.jreports.gl.dailyRateList;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepDailyRateListDetails;

public class GlRepDailyRateListDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public GlRepDailyRateListDS(ArrayList list){
		
		Iterator i = list.iterator();
		
		while (i.hasNext()){
			
			GlRepDailyRateListDetails details = (GlRepDailyRateListDetails) i.next();
			
			GlRepDailyRateListData argData = new GlRepDailyRateListData(details.getDrFrFunctionalCurrencyName(),
				details.getDrFrDate(), new Double (details.getDrFrConversionToUSD()),
				new Double(details.getDrInverseRate()));
			
			data.add(argData);
			
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		
		Object value = null;
		
		String fieldName = field.getName();
		
		if("currency".equals(fieldName)){
			value = ((GlRepDailyRateListData)data.get(index)).getCurrency();
		}else if("date".equals(fieldName)){
			value = ((GlRepDailyRateListData)data.get(index)).getDate();
		}else if("conversionRateToUSD".equals(fieldName)){
			value = ((GlRepDailyRateListData)data.get(index)).getConversionRateToUSD();
		}else if("inverseRate".equals(fieldName)){
			value = ((GlRepDailyRateListData)data.get(index)).getInverseRate();
		}

		
		return(value);	  
		
		
	}
	   
}
