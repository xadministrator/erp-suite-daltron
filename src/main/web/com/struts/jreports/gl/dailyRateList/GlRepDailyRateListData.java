package com.struts.jreports.gl.dailyRateList;

import java.io.Serializable;
import java.util.Date;

import org.apache.struts.action.ActionForm;

public class GlRepDailyRateListData extends ActionForm implements Serializable{
	
	private String currency = null;
	private Date date = null;
	private Double conversionRateToUSD = null;
	private Double inverseRate = null;

	public GlRepDailyRateListData(String currency, Date date, Double conversionRateToUSD, Double inverseRate){
		
		this.currency = currency;
		this.date = date;
		this.conversionRateToUSD = conversionRateToUSD;
		this.inverseRate = inverseRate;
		
	}
	
	public String getCurrency() {
		
		return currency;
		
	}

	public Date getDate() {
		
		return date;
		
	}

	public Double getConversionRateToUSD() {
		
		return conversionRateToUSD;
		
	}

	public Double getInverseRate() {
		
		return inverseRate;
		
	}

}
