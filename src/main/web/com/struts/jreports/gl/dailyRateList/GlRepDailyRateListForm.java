package com.struts.jreports.gl.dailyRateList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepDailyRateListForm extends ActionForm implements Serializable{
	
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String userPermission = null;
	private HashMap criteria = new HashMap();
	
	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public ArrayList getCurrencyList(){
		
		return currencyList;
		
	}
	
	public void setCurrencyList(String currency){
		
		currencyList.add(currency);
		
	}
	
	public void clearCurrencyList(){
		
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public String getViewType() {
		
		return viewType;
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){

		currency = null;
		dateFrom = null;
		dateTo = null;
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("goButton") != null) {
			
			if(dateFrom != null && !Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("dailyRateList.error.dateFromInvalid"));
				
			}
			
			if(dateTo != null && !Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("dailyRateList.error.dateToInvalid"));
				
			}
			
		}
		
		return errors;
		
	}
	
}
