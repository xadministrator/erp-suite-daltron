package com.struts.jreports.gl.detailbalancesheet;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.joda.time.LocalDate;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdStoredProcedureSetupController;
import com.ejb.txn.AdStoredProcedureSetupControllerHome;
import com.ejb.txn.GlRepDetailBalanceSheetController;
import com.ejb.txn.GlRepDetailBalanceSheetControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdStoredProcedureDetails;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlRepDetailBalanceSheetDetails;

public final class GlRepDetailBalanceSheetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepDetailBalanceSheetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepDetailBalanceSheetForm actionForm = (GlRepDetailBalanceSheetForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_DETAIL_BALANCE_SHEET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepDetailBalanceSheet");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepDetailBalanceSheetController EJB
*******************************************************/

         GlRepDetailBalanceSheetControllerHome homeDBS = null;
         GlRepDetailBalanceSheetController ejbDBS = null;
         
         AdStoredProcedureSetupControllerHome homeSP = null;
         AdStoredProcedureSetupController ejbSP = null;

         try {
         	
            homeDBS = (GlRepDetailBalanceSheetControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepDetailBalanceSheetControllerEJB", GlRepDetailBalanceSheetControllerHome.class);
            
            homeSP = (AdStoredProcedureSetupControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdStoredProcedureSetupControllerEJB", AdStoredProcedureSetupControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepDetailBalanceSheetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbDBS = homeDBS.create();
            ejbSP = homeSP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepDetailBalanceSheetAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- GL DBS Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            GlRepDetailBalanceSheetDetails details = null;
            
            String company = null;
            String storedProcedureName = null;
            AdStoredProcedureDetails adStoredProcedureDetails = null;
            boolean enableStoredProcedure = false;	
            	
		    try {
		    	
		    	ArrayList branchList = new ArrayList();
		    	String branchCodes = "";
		     	
		     	for(int i=0; i<actionForm.getGlRepBrDetailBalanceSheetListSize(); i++) {
		     	    
		     	    GlRepBranchDetailBalanceSheetList adBdbsList = (GlRepBranchDetailBalanceSheetList)actionForm.getGlRepBrDetailBalanceSheetListByIndex(i);
		     	    
		     	    if(adBdbsList.getBranchCheckbox() == true) {                                          
		     	        
		     	        AdBranchDetails brDetails = new AdBranchDetails();                                          
		     	        
		     	        brDetails.setBrAdCompany(user.getCmpCode());	     	        
		     	        brDetails.setBrCode(adBdbsList.getBrCode());                    
		     	        branchCodes += brDetails.getBrCode()+ ",";
		     	        branchList.add(brDetails);
		     	        
		     	    }
		     	}		    	
		     	
		     	// get company
		       
		       AdCompanyDetails adCmpDetails = ejbDBS.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       adStoredProcedureDetails = ejbSP.getAdSp(user.getCmpCode());
		       storedProcedureName = adStoredProcedureDetails.getSpNameGlBalanceSheetReport();
		       enableStoredProcedure = Common.convertByteToBoolean(adStoredProcedureDetails.getSpEnableGlBalanceSheetReport()) ;
		       
		       // execute report
		       
		       DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
				Connection conn = null;
				CallableStatement stmt = null;
	
				ResultSet rs = null;
				
				// execute report
				
			    if(enableStoredProcedure){
			    	
			    	
			    	SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
			    	Date date = sdf.parse(actionForm.getPeriod());
			    	LocalDate today = new LocalDate(date);

			    	
			    	Date dateFrom = new Date();
			    	Date dateTo = today.dayOfMonth().withMaximumValue().toDate();
			    	System.out.println("actionForm.getPeriod()="+actionForm.getPeriod());
			    	System.out.println("today="+today);  
				    System.out.println("actionForm.getAmountType()="+actionForm.getAmountType());
				    if(actionForm.getAmountType().equals("PTD")){

				    	dateFrom = today.dayOfMonth().withMinimumValue().toDate();
				    	
				    } else {
				    	
				    	dateFrom = today.withDayOfYear(1).toDate();
				    }
				    
				    
				    
			    	
			    	try{
			    		   System.out.println("ACC FRM: " + actionForm.getAccountFrom());
			    		   System.out.println("ACC FRM: " + actionForm.getAccountTo());
						   System.out.println("storedProcedureName="+storedProcedureName);
						   System.out.println("dateFrom="+dateFrom);
						   System.out.println("dateTo="+dateTo);
						   System.out.println("branchCodes="+branchCodes);
						   System.out.println("actionForm.getIncludeUnpostedTransaction()="+actionForm.getIncludeUnpostedTransaction());
						   System.out.println("actionForm.getIncludeUnpostedSlTransaction()="+actionForm.getIncludeUnpostedSlTransaction());
						   
						   conn = dataSource.getConnection();
						   stmt = (CallableStatement) conn.prepareCall(storedProcedureName);

						   stmt.setString(1, actionForm.getAccountFrom());
						   stmt.setString(2, actionForm.getAccountTo());
						   stmt.setDate(3, new java.sql.Date(dateFrom.getTime()));
						   stmt.setDate(4, new java.sql.Date(dateTo.getTime()));
						   stmt.setString(5, branchCodes);
						   stmt.setByte(6, Common.convertBooleanToByte(actionForm.getIncludeUnpostedTransaction()));
						   stmt.setByte(7, Common.convertBooleanToByte(actionForm.getIncludeUnpostedSlTransaction()));

						   
						   
						   
						   
						   
						   rs = stmt.executeQuery();

						   list = ejbDBS.executeSpGlRepDetailBalanceSheet(rs, actionForm.getAmountType(),
						       	   actionForm.getAccountFrom(),
						       	   actionForm.getAccountTo(),
						       	   actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')),
						           Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
						           actionForm.getIncludeUnpostedTransaction(), actionForm.getIncludeUnpostedSlTransaction(),
						           actionForm.getShowZeroes(), branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						  
						
						} catch(SQLException ex){
				          
						}finally{
					           try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
				                   rs = null;
				                   try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
				                   stmt = null;
				                   try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
				                   conn = null;
						}
			    	
			    } else {
			    	
			    	list = ejbDBS.executeGlRepDetailBalanceSheet(
					       	   actionForm.getAccountFrom(),
					       	   actionForm.getAccountTo(),
					       	   actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')),
					           Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
					           actionForm.getIncludeUnpostedTransaction(), actionForm.getIncludeUnpostedSlTransaction(),
					           actionForm.getShowZeroes(), branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					        
			    	
			    }
		    		    
		          
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("detailBalanceSheet.error.noRecordFound"));
                     
            } catch (GlobalAccountNumberInvalidException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("detailBalanceSheet.error.accountNumberInvalid"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepDetailBalanceSheetAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepDetailBalanceSheet"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("period", actionForm.getPeriod());
		    parameters.put("accountFrom", actionForm.getAccountFrom());
		    parameters.put("accountTo", actionForm.getAccountTo());
		    parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
		    
		    if (actionForm.getIncludeUnpostedTransaction()) {
		    	
		    	parameters.put("includeUnpostedTransaction", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includeUnpostedTransaction", "NO");
		    	
		    }
		    
		    if (actionForm.getShowZeroes()) {
		    	
		    	parameters.put("showZeroes", "YES");
		    	
		    } else {
		    	
		    	parameters.put("showZeroes", "NO");
		    	
		    }
		    
		    if (actionForm.getIncludeUnpostedSlTransaction()) {

		    	parameters.put("includeUnpostedSlTransaction", "YES");

		    } else {

		    	parameters.put("includeUnpostedSlTransaction", "NO");

		    }
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getGlRepBrDetailBalanceSheetListSize(); j++) {

      			GlRepBranchDetailBalanceSheetList brList = (GlRepBranchDetailBalanceSheetList)actionForm.getGlRepBrDetailBalanceSheetListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepDetailBalanceSheet.jasper";
		       
		    if (!new java.io.File(filename).exists()) {
		    		    		    
		       filename = servlet.getServletContext().getRealPath("jreports/GlRepDetailBalanceSheet.jasper");
			    
		    }		    		    	    
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new GlRepDetailBalanceSheetDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new GlRepDetailBalanceSheetDS(list)));
	               
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new GlRepDetailBalanceSheetDS(list)));
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new GlRepDetailBalanceSheetDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepDetailBalanceSheetAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- GL DBS Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- GL DBS Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
	         		
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearPeriodList();           	
            	
	            	list = ejbDBS.getGlReportableAcvAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    GlModAccountingCalendarValueDetails mdetails = 
	            		        (GlModAccountingCalendarValueDetails)i.next();
	            		        
	            		    actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() + "-" + 
	            		        mdetails.getAcvYear());
	            		        
	            		    if (mdetails.getAcvCurrent()) {
	            		    	
	            		    	actionForm.setPeriod(mdetails.getAcvPeriodPrefix() + "-" + 
	            		            mdetails.getAcvYear());
	            		    	
	            		    }
	            			
	            		}
	            			            		
	            	}    
	            	
	            	
	            	list = ejbDBS.getGenSgAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            			            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		actionForm.setAccountFrom("");
	            		actionForm.setAccountTo("");
	            		System.out.println("list="+list.size());
	            		while (i.hasNext()) {
	            			
	            		    GenModSegmentDetails mdetails = 
	            		        (GenModSegmentDetails)i.next();
	            		    System.out.println("mdetails.getSgMaxSize()="+mdetails.getSgMaxSize());
	            		    System.out.println("mdetails.getSgName()="+mdetails.getSgName());
	            		    for (int j=0; j<mdetails.getSgMaxSize(); j++) {
	            		    	
	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + "0");
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + "Z");
	            		    	
	            		    }
	            		    
	            		    if (i.hasNext()) {
	            		    	
	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + String.valueOf(mdetails.getSgFlSegmentSeparator()));
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + String.valueOf(mdetails.getSgFlSegmentSeparator()));
	            		    	
	            		    }
	            			
	            		}
	            			            		
	            	}   
			       
			       System.out.println("actionForm.setAccountFrom="+actionForm.getAccountFrom());
			       System.out.println("actionForm.setAccountTo="+actionForm.getAccountTo());
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepDetailBalanceSheetAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            actionForm.reset(mapping, request);

                // populate AdRepBranchBankAccountListList
         		
         		ArrayList brList = new ArrayList();
         		actionForm.clearGlRepBrDetailBalanceSheetList();
                
         		brList = ejbDBS.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                Iterator k = brList.iterator();
                
                while(k.hasNext()) {
                
                    AdBranchDetails brDetails = (AdBranchDetails)k.next();
                
                    GlRepBranchDetailBalanceSheetList adBdbsList = new GlRepBranchDetailBalanceSheetList(actionForm,
                        brDetails.getBrBranchCode(), brDetails.getBrName(),
                        brDetails.getBrCode());
                    adBdbsList.setBranchCheckbox(true);
                                 
                    actionForm.saveGlRepBrDetailBalanceSheetList(adBdbsList);
                    
                }	         		
	            
	            return(mapping.findForward("glRepDetailBalanceSheet"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in GlRepDetailBalanceSheetAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
