package com.struts.jreports.gl.detailbalancesheet;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepDetailBalanceSheetDetails;

public class GlRepDetailBalanceSheetDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepDetailBalanceSheetDS(ArrayList list){
      
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         GlRepDetailBalanceSheetDetails details = (GlRepDetailBalanceSheetDetails)i.next();

         String bsAccountType = null;
         
         if (details.getDbsAccountType().equals("ASSET")) {
         	
         	bsAccountType = "ASSETS";
         	
         } else if (details.getDbsAccountType().equals("LIABILITY")) {
         	
         	bsAccountType = "LIABILITIES AND EQUITY";
         	
         } else if (details.getDbsAccountType().equals("OWNERS EQUITY")) {
         	
         	bsAccountType = "LIABILITIES AND EQUITY";
         	
         }
                  
	     GlRepDetailBalanceSheetData dbsData = new GlRepDetailBalanceSheetData(
	     	details.getDbsAccountNumber(), details.getDbsAccountDescription(),
	     	details.getDbsAccountType(), bsAccountType, new Double(details.getDbsBalance()),
	     	new Double(details.getDbsPreviousBalance()), details.getDbsPreviousPeriod(),
	     	new Double(details.getDbsBalanceAdvances()));
		    
         data.add(dbsData);
      }


   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("account".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getAccount();
      }else if("accountDescription".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getAccountDescription();
      }else if("accountType".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getAccountType();
      }else if("bsAccountType".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getBsAccountType();
      }else if("balance".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getBalance();
      }else if("previousBalance".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getPreviousBalance();
      }else if("previousPeriod".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getPreviousPeriod();
      }else if("advances1".equals(fieldName)){
         value = ((GlRepDetailBalanceSheetData)data.get(index)).getAdvances1();
      }

      return(value);
   }
}
