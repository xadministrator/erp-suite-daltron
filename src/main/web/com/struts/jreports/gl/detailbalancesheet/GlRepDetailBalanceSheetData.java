package com.struts.jreports.gl.detailbalancesheet;

public class GlRepDetailBalanceSheetData implements java.io.Serializable{
   private String account = null;
   private String accountDescription = null;
   private String accountType = null;
   private String bsAccountType = null;
   private Double balance = null;
   private Double previousBalance = null;
   private String previousPeriod = null;
   private Double advances1 = null;

   public GlRepDetailBalanceSheetData(String account, String accountDescription,
      String accountType, String bsAccountType, Double balance, Double previousBalance,
      String previousPeriod, Double advances1){
      this.account = account;
      this.accountDescription = accountDescription;
      this.accountType = accountType ;
      this.bsAccountType = bsAccountType;
      this.balance = balance ;
      this.previousBalance = previousBalance;
      this.previousPeriod = previousPeriod;
      this.advances1 = advances1;
   }

   public String getAccount(){
      return(account);
   }

   public String getAccountDescription(){
      return(accountDescription);
   }
   
   public String getAccountType(){
      return(accountType);
   }

   public String getBsAccountType(){
      return(bsAccountType);
   }

   public Double getBalance(){
      return(balance);
   }

   public Double getPreviousBalance(){
      return(previousBalance);
   }

   public String getPreviousPeriod(){
      return(previousPeriod);
   }
   
   public Double getAdvances1(){
	      return(advances1);
	   }
}
