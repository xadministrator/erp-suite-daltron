package com.struts.jreports.gl.detailincomestatement;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepDetailIncomeStatementDetails;

public class GlRepDetailIncomeStatementDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepDetailIncomeStatementDS(ArrayList list){
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         GlRepDetailIncomeStatementDetails details = (GlRepDetailIncomeStatementDetails)i.next();
                                    
	     GlRepDetailIncomeStatementData disData = new GlRepDetailIncomeStatementData(
	     	details.getDisAccountNumber(), 
	     	details.getDisAccountDescription(),
	     	details.getDisAccountType(), 
	     	new Double(details.getDisBalance()));
		    
         data.add(disData);
      }     
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("account".equals(fieldName)){
         value = ((GlRepDetailIncomeStatementData)data.get(index)).getAccount();
      }else if("accountDescription".equals(fieldName)){
         value = ((GlRepDetailIncomeStatementData)data.get(index)).getAccountDescription();
      }else if("accountType".equals(fieldName)){
         value = ((GlRepDetailIncomeStatementData)data.get(index)).getAccountType();
      }else if("balance".equals(fieldName)){
         value = ((GlRepDetailIncomeStatementData)data.get(index)).getBalance();
      }

      return(value);
   }
}
