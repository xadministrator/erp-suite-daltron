package com.struts.jreports.gl.detailincomestatement;

public class GlRepDetailIncomeStatementData implements java.io.Serializable{
   private String account = null;
   private String accountDescription = null;
   private String accountType = null;
   private Double balance = null;

   public GlRepDetailIncomeStatementData(String account, String accountDescription,
      String accountType, Double balance){
      this.account = account;
      this.accountDescription = accountDescription;
      this.accountType = accountType ;
      this.balance = balance ;
   }

   public String getAccount(){
      return(account);
   }

   public String getAccountDescription(){
      return(accountDescription);
   }
   
   public String getAccountType(){
      return(accountType);
   }
   
   public Double getBalance(){
      return(balance);
   }

}
