package com.struts.jreports.gl.detailincomestatement;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepDetailIncomeStatementForm extends ActionForm implements Serializable{
	
	private String accountFrom = null;
	private String accountTo = null;
	private String period = null;
	private ArrayList periodList = new ArrayList();
	private String amountType = null;
	private ArrayList amountTypeList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String formatType = null;
	private ArrayList formatTypeList = new ArrayList();
	private boolean includeUnpostedTransaction = false;
	private boolean includeUnpostedSlTransaction = false;
	private boolean showZeroes = false;
	private String report = null;
	
	
	private String goButton = null;
	private String closeButton = null;
	
	private String userPermission = new String();
	
	private ArrayList glRepBrDetailIncomeStatementList = new ArrayList();
	
	public String getAccountFrom(){
		return(accountFrom);
	}
	
	public void setAccountFrom(String accountFrom){
		this.accountFrom = accountFrom;
	}
	
	public String getAccountTo(){
		return(accountTo);
	}
	
	public void setAccountTo(String accountTo){
		this.accountTo = accountTo;
	}
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}
	
	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}
	
	public String getPeriod(){
		return(period);
	}
	
	public void setPeriod(String period){
		this.period = period;
	}
	
	public ArrayList getPeriodList(){
		return(periodList);
	}
	
	public void setPeriodList(String period){
		periodList.add(period);
	}
	
	public void clearPeriodList(){
		periodList.clear();
		periodList.add(Constants.GLOBAL_BLANK);
	}
	
	public String getAmountType(){
		return(amountType);
	}
	
	public void setAmountType(String amountType){
		this.amountType = amountType;
	}
	
	public ArrayList getAmountTypeList(){
		return(amountTypeList);
	}
	
	public ArrayList getformatTypeList(){
		return (formatTypeList);
	}
	
	public void setFormatType(String FormatType){
		this.formatType= FormatType;
	}
	
	public String getFormatType(){
		return(formatType);   	
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	

	
	
	
	
	public boolean getIncludeUnpostedTransaction() {
		
		return includeUnpostedTransaction;
		
	}
	
	public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
		
		this.includeUnpostedTransaction = includeUnpostedTransaction;
		
	}
	
	public boolean getShowZeroes() {
		
		return showZeroes;
		
	}
	
	public void setShowZeroes(boolean showZeroes) {
		
		this.showZeroes = showZeroes;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Object[] getGlRepBrDetailIncomeStatementList(){
		
		return glRepBrDetailIncomeStatementList.toArray();
		
	}
	
	public GlRepBranchDetailIncomeStatementList getGlRepBrDetailIncomeStatementListByIndex(int index){
		
		return ((GlRepBranchDetailIncomeStatementList)glRepBrDetailIncomeStatementList.get(index));
		
	}
	
	public int getGlRepBrDetailIncomeStatementListSize(){
		
		return(glRepBrDetailIncomeStatementList.size());
		
	}
	
	public void saveGlRepBrDetailIncomeStatementList(Object newGlRepBrDetailIncomeStatementList){
		
		glRepBrDetailIncomeStatementList.add(newGlRepBrDetailIncomeStatementList);   	  
		
	}
	
	public void clearGlRepBrDetailIncomeStatementList(){
		
		glRepBrDetailIncomeStatementList.clear();
		
	}
	
	public void setGlRepBrDetailIncomeStatementList(ArrayList glRepBrDetailIncomeStatementList) {
		
		this.glRepBrDetailIncomeStatementList = glRepBrDetailIncomeStatementList;
		
	}
	
	public boolean getIncludeUnpostedSlTransaction() {

		return includeUnpostedSlTransaction;

	}

	public void setIncludeUnpostedSlTransaction(boolean includeUnpostedSlTransaction) {

		this.includeUnpostedSlTransaction = includeUnpostedSlTransaction;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<glRepBrDetailIncomeStatementList.size(); i++) {
			
			GlRepBranchDetailIncomeStatementList list = (GlRepBranchDetailIncomeStatementList)glRepBrDetailIncomeStatementList.get(i);
			list.setBranchCheckbox(false);	 
			
		}  
		
		goButton = null;
		closeButton = null;
		amountTypeList.clear();
		amountTypeList.add("PTD");
		amountTypeList.add("QTD");
		amountTypeList.add("YTD");
		
		formatTypeList.clear();
		formatTypeList.add("");
		formatTypeList.add("SUMMARY");
		formatTypeList.add("DIVISION");
		formatTypeList.add("HEADOFFICE");
		formatTypeList.add("BRANCHES");
		amountType = "PTD";
		viewTypeList.clear();
		formatType = "";
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_CSV);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		showZeroes = false;
		includeUnpostedTransaction = false;
		includeUnpostedSlTransaction = false;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("goButton") != null){
			if(Common.validateRequired(accountFrom)){
				errors.add("accountFrom", new ActionMessage("detailIncomeStatement.error.accountFromRequired"));
			}
			if(Common.validateRequired(accountTo)){
				errors.add("accountTo", new ActionMessage("detailIncomeStatement.error.accountToRequired"));
			}
			if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("period", new ActionMessage("detailIncomeStatement.error.periodRequired"));
			}
			if(!Common.validateStringExists(periodList, period)){
				errors.add("period", new ActionMessage("detailIncomeStatement.error.periodInvalid"));
			}
			if(Common.validateRequired(amountType)){
				errors.add("amountType", new ActionMessage("detailIncomeStatement.error.amountTypeRequired"));
			}
			
		}
		return(errors);
	}
}
