package com.struts.jreports.gl.detailtrialbalance;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepDetailTrialBalanceDetails;

public class GlRepDetailTrialBalanceDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepDetailTrialBalanceDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      String groupAccount = "";
      double groupNetDebit= 0d;
      double groupNetCredit= 0d;
      
      double debitBalance = 0d;
      double creditBalance = 0d;
      
      while (i.hasNext()) {
      	
         GlRepDetailTrialBalanceDetails details = (GlRepDetailTrialBalanceDetails)i.next();

         Double grossDebit = details.getDtbDebit() + details.getDtbDebitBeg();
         Double grossCredit = details.getDtbCredit() + details.getDtbCreditBeg();
        		 
	     GlRepDetailTrialBalanceData dtbData = new GlRepDetailTrialBalanceData(
		    details.getDtbAccountNumber(), 
		    details.getDtbAccountSegment1(),
		    details.getDtbAccountDescription(),
		    details.getDtbDebit(),
		    details.getDtbCredit(),
		    
		    grossDebit > grossCredit ? grossDebit - grossCredit : null,
		    grossCredit > grossDebit ? grossCredit - grossDebit : null,

		    details.getDtbDebitBeg(), 
		    details.getDtbCreditBeg(),
		    
		    details.getDtbTotalDebit(), 
		    details.getDtbTotalCredit());
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("account".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getAccount();
      }else if("accountSegment1".equals(fieldName)){
          value = ((GlRepDetailTrialBalanceData)data.get(index)).getAccountSegment1();
      }else if("accountDescription".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getAccountDescription();
      }else if("debit".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getDebit();
      }else if("credit".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getCredit();
         
      }else if("grossDebit".equals(fieldName)){
          value = ((GlRepDetailTrialBalanceData)data.get(index)).getGrossDebit();
      }else if("grossCredit".equals(fieldName)){
          value = ((GlRepDetailTrialBalanceData)data.get(index)).getGrossCredit();
 
      }else if("debitBeg".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getDebitBeg();
      }else if("creditBeg".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getCreditBeg();
      }else if("debitTotal".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getDebitTotal();
      }else if("creditTotal".equals(fieldName)){
         value = ((GlRepDetailTrialBalanceData)data.get(index)).getCreditTotal();
      }
      

      return(value);
   }
}
