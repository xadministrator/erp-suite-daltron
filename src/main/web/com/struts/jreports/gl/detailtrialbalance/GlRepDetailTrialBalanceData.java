package com.struts.jreports.gl.detailtrialbalance;

public class GlRepDetailTrialBalanceData implements java.io.Serializable{
   private String account = null;
   private String accountSegment1 = null;
   private String accountDescription = null;
   private Double debit = null;
   private Double credit = null;
   private Double grossDebit = null;
   private Double grossCredit = null;
   private Double debitBeg = null;
   private Double creditBeg = null;
   private Double debitTotal = null;
   private Double creditTotal = null;
   

   public GlRepDetailTrialBalanceData(String account, String accountSegment1, String accountDescription,
      Double debit, Double credit,
      Double grossDebit, Double grossCredit,
      Double debitBeg, Double creditBeg,
      Double debitTotal, Double creditTotal)
   {
      this.account = account;
      this.accountSegment1 = accountSegment1;
      this.accountDescription = accountDescription;
      this.debit = debit;
      this.credit = credit;
      this.grossDebit = grossDebit;
      this.grossCredit = grossCredit;
      this.debitBeg = debitBeg;
      this.creditBeg = creditBeg;
      this.debitTotal = debitTotal;
      this.creditTotal = creditTotal;
   }

   public String getAccount(){
      return(account);
   }
   
   
   public String getAccountSegment1(){
	      return(accountSegment1);
	   }

   public String getAccountDescription(){
      return(accountDescription);
   }
   
   public Double getDebit(){
      return(debit);
   }

   public Double getCredit(){
      return(credit);
   }
   
   public Double getGrossDebit(){
      return(grossDebit);
   }

   public Double getGrossCredit(){
      return(grossCredit);
   }
   
   
   
   public Double getDebitBeg() {
	   return (debitBeg);
   }
   
   public Double getCreditBeg() {
	   return (creditBeg);
   }
   
   public Double getDebitTotal() {
	   return (debitTotal);
   }
   
   public Double getCreditTotal() {
	   return (creditTotal);
   }
   

}
