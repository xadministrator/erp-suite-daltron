package com.struts.jreports.gl.detailtrialbalance;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepDetailTrialBalanceForm extends ActionForm implements Serializable{
	
	private String amountType = null;
	private ArrayList amountTypeList = new ArrayList();
	
	private String accountFrom = null;
	private String accountTo = null;
	private String period = null;
	private ArrayList periodList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private boolean includeUnpostedTransaction = false;
	private boolean showZeroes = false;
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private boolean includeUnpostedSlTransaction = false;
	private boolean groupSegment1 = false;
	private boolean showEntries = false;
	
	private boolean showRecomputeCoaBalanceButton = false;
	
	
	private String userPermission = new String();
	
	private ArrayList glRepBrDetailTrialBalanceList = new ArrayList();
	
	
	public String getAmountType(){
		return(amountType);
	}
	
	public void setAmountType(String amountType){
		this.amountType = amountType;
	}
	
	public ArrayList getAmountTypeList() {
		
		return(amountTypeList);
		
	}
	
	
	public String getAccountFrom(){
		return(accountFrom);
	}
	
	public void setAccountFrom(String accountFrom){
		this.accountFrom = accountFrom;
	}
	
	public String getAccountTo(){
		return(accountTo);
	}
	
	public void setAccountTo(String accountTo){
		this.accountTo = accountTo;
	}
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}
	
	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}
	
	public String getPeriod(){
		return(period);
	}
	
	public void setPeriod(String period){
		this.period = period;
	}
	
	public ArrayList getPeriodList(){
		return(periodList);
	}
	
	public void setPeriodList(String period){
		periodList.add(period);
	}
	
	public void clearPeriodList(){
		periodList.clear();
		periodList.add(Constants.GLOBAL_BLANK);
	}
	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
	public boolean getIncludeUnpostedTransaction() {
		
		return includeUnpostedTransaction;
		
	}
	
	public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
		
		this.includeUnpostedTransaction = includeUnpostedTransaction;
		
	}
	
	public boolean getShowZeroes() {
		
		return showZeroes;
		
	}
	
	public void setShowZeroes(boolean showZeroes) {
		
		this.showZeroes = showZeroes;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Object[] getGlRepBrDetailTrialBalanceList(){
		
		return glRepBrDetailTrialBalanceList.toArray();
		
	}
	
	public GlRepBranchDetailTrialBalanceList getGlRepBrDetailTrialBalanceListByIndex(int index){
		
		return ((GlRepBranchDetailTrialBalanceList)glRepBrDetailTrialBalanceList.get(index));
		
	}
	
	public int getGlRepBrDetailTrialBalanceListSize(){
		
		return(glRepBrDetailTrialBalanceList.size());
		
	}
	
	public void saveGlRepBrDetailTrialBalanceList(Object newGlRepBrDetailTrialBalanceList){
		
		glRepBrDetailTrialBalanceList.add(newGlRepBrDetailTrialBalanceList);   	  
		
	}
	
	public void clearGlRepBrDetailTrialBalanceList(){
		
		glRepBrDetailTrialBalanceList.clear();
		
	}
	
	public void setGlRepBrDetailTrialBalanceList(ArrayList glRepBrDetailTrialBalanceList) {
		
		this.glRepBrDetailTrialBalanceList = glRepBrDetailTrialBalanceList;
		
	}
	
	public boolean getIncludeUnpostedSlTransaction() {

		return includeUnpostedSlTransaction;

	}

	public void setIncludeUnpostedSlTransaction(boolean includeUnpostedSlTransaction) {

		this.includeUnpostedSlTransaction = includeUnpostedSlTransaction;

	}
	
	public boolean getGroupSegment1() {

		return groupSegment1;

	}

	public void setGroupSegment1(boolean groupSegment1) {

		this.groupSegment1 = groupSegment1;

	}
	
	public boolean getShowEntries() {

		return showEntries;

	}

	public void setShowEntries(boolean showEntries) {

		this.showEntries = showEntries;

	}
	
	
	
	
	
	public boolean getShowRecomputeCoaBalanceButton() {

		return showRecomputeCoaBalanceButton;

	}

	public void setShowRecomputeCoaBalanceButton(boolean showRecomputeCoaBalanceButton) {

		this.showRecomputeCoaBalanceButton = showRecomputeCoaBalanceButton;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i < getGlRepBrDetailTrialBalanceListSize(); i++) {
			
			GlRepBranchDetailTrialBalanceList list = (GlRepBranchDetailTrialBalanceList)glRepBrDetailTrialBalanceList.get(i);
			list.setBranchCheckbox(false);
			
		}
		
		goButton = null;
		closeButton = null;
		
		amountTypeList.clear();
		amountTypeList.add("PTD");
		amountTypeList.add("QTD");
		amountTypeList.add("YTD");
		amountType = "PTD";
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_CSV);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		includeUnpostedTransaction = false;
		showZeroes = false;
		includeUnpostedSlTransaction = false;
		groupSegment1 = false;
		showEntries = false;
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if(Common.validateRequired(accountFrom)){
				errors.add("accountFrom", new ActionMessage("detailTrialBalance.error.accountFromRequired"));
			}
			if(Common.validateRequired(accountTo)){
				errors.add("accountTo", new ActionMessage("detailTrialBalance.error.accountToRequired"));
			}
			if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("period", new ActionMessage("detailTrialBalance.error.periodRequired"));
			}
			if(!Common.validateStringExists(periodList, period)){
				errors.add("period", new ActionMessage("detailTrialBalance.error.periodInvalid"));
			}
			
		}
		return(errors);
	}
}
