package com.struts.jreports.gl.financialreportrun;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRTextElement;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignLine;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JasperDesign;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlRepFinancialReportRunController;
import com.ejb.txn.GlRepFinancialReportRunControllerHome;
import com.struts.ar.invoiceentry.ArInvoiceLineItemList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlRepFinancialReportDetails;


public final class GlRepFinancialReportRunAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepFinancialReportRunAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepFinancialReportRunForm actionForm = (GlRepFinancialReportRunForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
  
         String frParam = Common.getUserPermission(user, Constants.GL_REP_FINANCIAL_REPORT_RUN_ID);
         
         if (frParam != null) {
         	
	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {
               	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glRepFinancialReportRun"));
                  
               }
            }
            
            actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlRepFinancialReportRunController EJB
*******************************************************/

         GlRepFinancialReportRunControllerHome homeFRR = null;
         GlRepFinancialReportRunController ejbFRR = null;

         try{
            
            homeFRR = (GlRepFinancialReportRunControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlRepFinancialReportRunControllerEJB", GlRepFinancialReportRunControllerHome.class);
            
         } catch(NamingException e) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepFinancialReportRunAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbFRR = homeFRR.create();
         } catch (CreateException e) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlRepFinancialReportRunAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
                
            } 
            
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
	 
		 // get report session and if not null set it to null 
		 Report reportSession = 
		    (Report)session.getAttribute(Constants.REPORT_KEY);
		    
		 if (reportSession != null){
		 	
		    reportSession.setBytes(null);
		    session.setAttribute(Constants.REPORT_KEY, reportSession);
		    
		 }

/*******************************************************
   -- Gl  FRR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            GlRepFinancialReportDetails details = null;
            ArrayList columnHeadingList = null;
            ArrayList reportDetailList = null;

            try {
            	
            	details = ejbFRR.getGlRepFrParameters(
            		actionForm.getFinancialReport(), actionForm.getPeriod().substring(0, 
					  actionForm.getPeriod().indexOf('-')),
				    Common.convertStringToInt(actionForm.getPeriod().substring(
					  actionForm.getPeriod().indexOf('-') + 1)),
					Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
					
			    columnHeadingList = ejbFRR.getGlRepFrColumnHeadings(actionForm.getFinancialReport(), user.getCmpCode());
			    
			    reportDetailList = ejbFRR.getGlRepFrDetails(actionForm.getFinancialReport(), actionForm.getPeriod().substring(0, 
					  actionForm.getPeriod().indexOf('-')),
				    Common.convertStringToInt(actionForm.getPeriod().substring(
					  actionForm.getPeriod().indexOf('-') + 1)),
					Common.convertStringToSQLDate(actionForm.getDate()), 
					Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode());
				
		    
		    } catch (EJBException ex) {
		    	
		        if (log.isInfoEnabled()) {
		        	
			   		log.info("EJBException caught in GlRepFinancialReportRunAction.execute(): " + ex.getMessage() +
			   		" session: " + session.getId());
			   		
			    }
			    
			    return(mapping.findForward("cmnErrorPage"));
		    }
		    
		    // create jasper design report properties
		    
		    // jasper design
		    
			JasperDesign jasperDesign = new JasperDesign();
			jasperDesign.setName("FinancialReport");
			
			
			if (details.getFrWidth() > 595) {
				
				jasperDesign.setPageWidth(details.getFrWidth());
			    jasperDesign.setPageHeight(595);
			    
			} else {
				
				jasperDesign.setPageWidth(595);
				jasperDesign.setPageHeight(842);
				
			}
			
			jasperDesign.setColumnWidth(545);
			jasperDesign.setColumnSpacing(0);
			jasperDesign.setLeftMargin(30);
			jasperDesign.setRightMargin(20);
			jasperDesign.setTopMargin(50);
			jasperDesign.setBottomMargin(50);
			
			// fonts
			
			JRDesignReportFont normalFont = new JRDesignReportFont();
			normalFont.setName("Arial_Normal");
			normalFont.setDefault(true);
			normalFont.setFontName("Arial Narrow");
			normalFont.setSize(details.getFrFontSize());
			normalFont.setPdfFontName(details.getFrFontStyle());
			normalFont.setPdfEncoding("Cp1252");
			normalFont.setPdfEmbedded(false);
			jasperDesign.addFont(normalFont);
	
			JRDesignReportFont boldFont = new JRDesignReportFont();
			boldFont.setName("Arial_Bold");
			boldFont.setDefault(false);
			boldFont.setFontName("Arial Narrow");
			boldFont.setSize(details.getFrFontSize());
			boldFont.setBold(true);
			boldFont.setPdfFontName(details.getFrFontStyle().equals("Times-Roman") ? "Times-Bold" :details.getFrFontStyle()+"-Bold");
			boldFont.setPdfEncoding("Cp1252");
			boldFont.setPdfEmbedded(false);
			jasperDesign.addFont(boldFont);
	
			JRDesignReportFont italicFont = new JRDesignReportFont();
			italicFont.setName("Arial_Italic");
			italicFont.setDefault(false);
			italicFont.setFontName("Arial Narrow");
			italicFont.setSize(details.getFrFontSize());
			italicFont.setItalic(true);
			italicFont.setPdfFontName(details.getFrFontStyle().equals("Times-Roman") ? "Times-Italic" : details.getFrFontStyle()+"-Oblique");
			italicFont.setPdfEncoding("Cp1252");
			italicFont.setPdfEmbedded(false);
			jasperDesign.addFont(italicFont);
			
			JRDesignBand band = new JRDesignBand();
			JRDesignStaticText staticText = new JRDesignStaticText();	
			
			// groups
			
			// rows
			
			int rowWidth = 0;
			
			Iterator i = reportDetailList.iterator();
			
			int j = 0;
			int first=0;
			int toPrint=0;
			while (i.hasNext()) {
				
				j++;
				
				GlRepFinancialReportDetails reportDetails = (GlRepFinancialReportDetails)i.next();
				
				// check if row is hidden
				
				if (reportDetails.getFrRowHideRow() == (byte)1)
					continue;
			    
				// get row width
				
				if (rowWidth == 0) 
					rowWidth = 75 + details.getFrFontSize() + reportDetails.getFrLastColPosition();
			    
				// line to skip before 
			    
			    for (int l = 0; l < reportDetails.getFrRowLineToSkipBefore(); l++) {
			    	
			    	band = new JRDesignBand();
					band.setHeight(3 + details.getFrFontSize());
					JRDesignGroup lineToSkipGroup = new JRDesignGroup();
					lineToSkipGroup.setName("GROUP_NAME" + j);
					lineToSkipGroup.setGroupHeader(band);
					jasperDesign.addGroup(lineToSkipGroup);
					
					j++;
					
			    }
			    
			    // group
			    
				band = new JRDesignBand();
				band.setHeight(7 + details.getFrFontSize() + reportDetails.getFrRowUnderlineCharacterAfter());
				JRDesignGroup group = new JRDesignGroup();
				group.setName("GROUP_NAME" + j);
				
				// row
				
				staticText = new JRDesignStaticText();
				
				if(reportDetails.getFrColumnList().size() == 0) {
					
					staticText.setX(5);
					staticText.setY(4);
					staticText.setWidth(rowWidth);
						
				} else {
					
					staticText.setX(5 + reportDetails.getFrRowIndent());
					staticText.setY(4);
					staticText.setWidth(reportDetails.getFrFirstColPosition() - reportDetails.getFrRowIndent() - 5);
					
				}
									
				if (reportDetails.getFrRowName().length() > 50) {
				
			    	staticText.setHeight((3 + details.getFrFontSize()) * 3);
			    
			    } else {
			    
			        staticText.setHeight(3 + details.getFrFontSize());
			     
			    }
																	
				if (reportDetails.getFrRowFontStyle().equals("NORMAL")) {
					
				    staticText.setFont(normalFont);
				    
				} else if (reportDetails.getFrRowFontStyle().equals("BOLD")) {
					
					staticText.setFont(boldFont);
					
			    } else if (reportDetails.getFrRowFontStyle().equals("ITALIC")) {
			    	
			    	staticText.setFont(italicFont);
			    }
			    
			    if (reportDetails.getFrRowHorizontalAlign().equals("Left")) {
					
					staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
					
				} else if (reportDetails.getFrRowHorizontalAlign().equals("Center")) {
					
					staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
					
				} else if (reportDetails.getFrRowHorizontalAlign().equals("Right")) {
					
					staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
					
				} else if (reportDetails.getFrRowHorizontalAlign().equals("Justified")) {
					
					staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_JUSTIFIED);
					
				} 
				
				staticText.setText(reportDetails.getFrRowName());
				System.out.println("getFrRowName "+reportDetails.getFrRowName());
				
				Iterator ix = reportDetails.getFrColumnList().iterator();
				double x =0d;
				while (ix.hasNext()) {
					GlRepFinancialReportDetails reportDetails1 = (GlRepFinancialReportDetails)ix.next();
					System.out.println("reportDetails.getFrColAmount():"+reportDetails1.getFrColAmount());
					try{
					x=reportDetails1.getFrColAmount();
					}
					catch(Exception exx)
					{
						
					}
				}
				System.out.println("x="+x);
				if(reportDetails.getFrRowName().equals("GRAND TOTAL"))
				{
					System.out.println("toPrint na");
					toPrint=1;
				}

				first++;
				if(x>0){
				band.addElement(staticText);
				}
				else{
					if(first<2)
					{
						band.addElement(staticText);
						
					}
					else if(toPrint==1)
					{
						band.addElement(staticText);
					}
					else	
					continue;
				}
				// columns
				
				ArrayList columnList = reportDetails.getFrColumnList();
				
				Iterator k = columnList.iterator();
				
				while (k.hasNext()) {
					
				    GlRepFinancialReportDetails columnDetails = (GlRepFinancialReportDetails)k.next();	
				    
				    JRDesignLine line = null;
				    
				    // underline character before
				    
				    for (int l = 0; l < reportDetails.getFrRowUnderlineCharacterBefore(); l++) {
				    	
				    	line = new JRDesignLine();
						line.setX(columnDetails.getFrColPosition());
						line.setY(l);
						line.setWidth(75 + details.getFrFontSize());
						line.setHeight(0);
						band.addElement(line);
				    	
				    }
				    
				    // underline character after
				    
				    for (int l = 0; l < reportDetails.getFrRowUnderlineCharacterAfter(); l++) {
				    	
				    	line = new JRDesignLine();
						line.setX(columnDetails.getFrColPosition());
						line.setY(l + 7 + details.getFrFontSize());
						line.setWidth(75 + details.getFrFontSize());
						line.setHeight(0);
						band.addElement(line);
				    	
				    }
				        
				    // column
				    
				    staticText = new JRDesignStaticText();
					staticText.setX(columnDetails.getFrColPosition());
					staticText.setY(4);
					staticText.setWidth(75 + details.getFrFontSize());
					staticText.setHeight(3 + details.getFrFontSize());
					if (reportDetails.getFrRowFontStyle().equals("NORMAL")) {
					
				    staticText.setFont(normalFont);
				    
					} else if (reportDetails.getFrRowFontStyle().equals("BOLD")) {
						
						staticText.setFont(boldFont);
						
				    } else if (reportDetails.getFrRowFontStyle().equals("ITALIC")) {
				    	
				    	staticText.setFont(italicFont);
				    }
				    
					staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
					
					Double COLUMN_AMOUNT = columnDetails.getFrColAmount();
					
					if(COLUMN_AMOUNT!=null){
						
						double columnAmount = COLUMN_AMOUNT.doubleValue();
						
						if (columnDetails.getFrColFactor().equals("BILLIONS")) {
							
							columnAmount = columnAmount / 1000000000;
							
						} else if (columnDetails.getFrColFactor().equals("MILLIONS")) {
							
							columnAmount = columnAmount / 1000000;
							
						} else if (columnDetails.getFrColFactor().equals("THOUSANDS")) {
							
							columnAmount = columnAmount / 1000;
							
						} else if (columnDetails.getFrColFactor().equals("UNITS")) {
							
							
						} else if (columnDetails.getFrColFactor().equals("PERCENTILES")) {
							
							columnAmount = columnAmount * 100;
							
						}
						
						DecimalFormat dcf = new DecimalFormat(columnDetails.getFrColFormatMask());
		
						staticText.setText(dcf.format(columnAmount));
						band.addElement(staticText);
					}else{
						
						staticText.setText("");
						band.addElement(staticText);
						
					}
					
				}			
												
				group.setGroupHeader(band);
				jasperDesign.addGroup(group);
				
				// line to skip after
				
			    for (int l = 0; l < reportDetails.getFrRowLineToSkipAfter(); l++) {
			    	
			    	j++;
			    	
			    	band = new JRDesignBand();
					band.setHeight(3 + details.getFrFontSize());
					JRDesignGroup lineToSkipGroup = new JRDesignGroup();
					lineToSkipGroup.setName("GROUP_NAME" + j);
					lineToSkipGroup.setGroupHeader(band);
					jasperDesign.addGroup(lineToSkipGroup);
															
			    }
				
			}
			
				
			// title band
			
			band = new JRDesignBand();
			band.setHeight(80 + details.getFrFontSize());
			
			// company name
			
			staticText = new JRDesignStaticText();
			staticText.setX(5);
			staticText.setY(5);
			staticText.setWidth(rowWidth);
			staticText.setHeight(15 + details.getFrFontSize());
			staticText.setText(user.getCompanyName());
			
			boldFont.setSize(details.getFrFontSize() + 1);
			staticText.setFont(boldFont);
			
			if (details.getFrHorizontalAlign().equals("Left")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
			} else if (details.getFrHorizontalAlign().equals("Center")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
			} else if (details.getFrHorizontalAlign().equals("Right")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
			} else if (details.getFrHorizontalAlign().equals("Justified")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_JUSTIFIED);
			} 
			
			band.addElement(staticText);
			
			// title
			
			staticText = new JRDesignStaticText();
			staticText.setX(5);
			staticText.setY(25 + details.getFrFontSize());
			staticText.setWidth(rowWidth);
			staticText.setHeight(5 + details.getFrFontSize());
			staticText.setText(details.getFrTitle());
			
			boldFont.setSize(details.getFrFontSize() + 1);
			staticText.setFont(boldFont);
			
			if (details.getFrHorizontalAlign().equals("Left")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
			} else if (details.getFrHorizontalAlign().equals("Center")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
			} else if (details.getFrHorizontalAlign().equals("Right")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
			} else if (details.getFrHorizontalAlign().equals("Justified")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_JUSTIFIED);
			} 
			
			band.addElement(staticText);
			
			// period
			
			staticText = new JRDesignStaticText();
			staticText.setX(5);
			staticText.setY(43 + details.getFrFontSize());
			staticText.setWidth(rowWidth);
			staticText.setHeight(5 + details.getFrFontSize());
			staticText.setFont(normalFont);
			staticText.setText("Period: " + details.getFrPeriod() + "-" + details.getFrYear());
			
			if (details.getFrHorizontalAlign().equals("Left")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
			} else if (details.getFrHorizontalAlign().equals("Center")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
			} else if (details.getFrHorizontalAlign().equals("Right")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
			} else if (details.getFrHorizontalAlign().equals("Justified")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_JUSTIFIED);
			} 
			
			band.addElement(staticText);
			
			// currency
			
			staticText = new JRDesignStaticText();
			staticText.setX(5);
			staticText.setY(58 + details.getFrFontSize());
			staticText.setWidth(rowWidth);
			staticText.setHeight(5 + details.getFrFontSize());
			staticText.setFont(normalFont);
			//staticText.setText("(in " + details.getFrFcName() + " currency)");
			System.out.println("details.getFrTitle():"+details.getFrTitle());
			if(details.getFrTitle().equals("SUMMARY BREAKDOWN"))
			{
				
			}
			else
			staticText.setText("LBP - Office Supplies");
			if (details.getFrHorizontalAlign().equals("Left")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
			} else if (details.getFrHorizontalAlign().equals("Center")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
			} else if (details.getFrHorizontalAlign().equals("Right")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_RIGHT);
			} else if (details.getFrHorizontalAlign().equals("Justified")) {
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_JUSTIFIED);
			} 
			
			band.addElement(staticText);
			
			jasperDesign.setTitle(band);
			
			// page header band
			
			band = new JRDesignBand();
			jasperDesign.setPageHeader(band);
	
			// column header band
			
			band = new JRDesignBand();
			band.setHeight(10 + details.getFrFontSize());
			
			// column headers
						
			i = columnHeadingList.iterator();
			
			while (i.hasNext()) {
				
				GlRepFinancialReportDetails columnHeadingDetails = 
				    (GlRepFinancialReportDetails)i.next();
				
				staticText = new JRDesignStaticText();
				staticText.setX(columnHeadingDetails.getFrColPosition());
				staticText.setY(5);
				staticText.setWidth(75 + details.getFrFontSize());
				staticText.setHeight(5 + details.getFrFontSize());
				staticText.setFont(boldFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_CENTER);
				staticText.setText(columnHeadingDetails.getFrColName());
				band.addElement(staticText);
				
			}
			
			jasperDesign.setColumnHeader(band);
	
			// detail band
			
			band = new JRDesignBand();
			jasperDesign.setDetail(band);
	
			// column footer band
			
			band = new JRDesignBand();
			jasperDesign.setColumnFooter(band);
	
			// page footer band
			
			band = new JRDesignBand();
			jasperDesign.setPageFooter(band);
	
			// summary band
			
			band = new JRDesignBand();
			jasperDesign.setSummary(band);
	
		                	    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    		    
		    Report report = new Report();

	       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	       	
	       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		       report.setBytes(
		          JasperRunManager.runReportToPdf(JasperCompileManager.compileReport(jasperDesign), parameters, 
			        new GlRepFinancialReportRunDS()));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
               
               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
               report.setBytes(
				   JasperRunManagerExt.runReportToXls(JasperCompileManager.compileReport(jasperDesign), parameters, 
				        new GlRepFinancialReportRunDS()));   
			        
		   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
			   
			   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
			   report.setJasperPrint(JasperFillManager.fillReport(JasperCompileManager.compileReport(jasperDesign), parameters, 
			       new GlRepFinancialReportRunDS()));												    
			        
		   }
		   
	       session.setAttribute(Constants.REPORT_KEY, report);
	       actionForm.setReport(Constants.STATUS_SUCCESS); 	       
		    
/*******************************************************
   -- Gl  FRR Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl  FRR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

             // get close and permanently close periods 
            
		    		    
		    Iterator i = null;
		    ArrayList list = null;
		    
		    try {
		    	
		        actionForm.clearPeriodList();           	
            	
	        	list = ejbFRR.getGlReportableAcvAll(user.getCmpCode());
	        	
	        	if (list == null || list.size() == 0) {
	        		
	        		actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
	        		
	        	} else {
	        		           		            		
	        		i = list.iterator();
	        		
	        		while (i.hasNext()) {
	        			
	        		    GlModAccountingCalendarValueDetails mdetails = 
	        		        (GlModAccountingCalendarValueDetails)i.next();
	        		        
	        		    actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() + "-" + 
	        		        mdetails.getAcvYear());
	        		        
	        		    if (mdetails.getAcvCurrent()) {
	        		    	
	        		    	actionForm.setPeriod(mdetails.getAcvPeriodPrefix() + "-" + 
	        		            mdetails.getAcvYear());
	        		    	
	        		    }
	        			
	        		}
	        			            		
	        	}    
		       
		       		        
	         } catch(EJBException ex) {
	         	
		        if (log.isInfoEnabled()) {
		        	
				   log.info("EJBException caught in GlRepFinancialReportRunAction.execute(): " + ex.getMessage() +
				   " session: " + session.getId());
				   
				}
				return(mapping.findForward("cmnErrorPage"));
				
              }
              
              try {
              	 
              	 actionForm.clearFinancialReportList();
              	 
              	 list = ejbFRR.getGlFrgFrAll(user.getCmpCode());
              	 
              	 i = list.iterator();
              	 
              	 while (i.hasNext()) {
              	 	
              	 	actionForm.setFinancialReportList((String)i.next());
              	 	
              	 }
              	  
              	
              } catch (EJBException ex) {
	         	
		        if (log.isInfoEnabled()) {
		        	
				   log.info("EJBException caught in GlRepFinancialReportRunAction.execute(): " + ex.getMessage() +
				   " session: " + session.getId());
				   
				}
				return(mapping.findForward("cmnErrorPage"));
				
              }

              actionForm.reset(mapping, request);
              return(mapping.findForward("glRepFinancialReportRun"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
         
      } catch (Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if (log.isInfoEnabled()) {
          	
             log.info("Exception caught in GlRepFinancialReportRunAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }
          
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}

