package com.struts.jreports.gl.financialreportrun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepFinancialReportRunForm extends ActionForm implements Serializable{

   private String date = null;
   private String period = null;
   private ArrayList periodList = new ArrayList();
   private String financialReport = null;
   private ArrayList financialReportList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String mainButton = null;
   private String conversionDate = null;


   private String userPermission = new String();

   public String getDate(){
      return(date);
   }

   public void setDate(String date){
      this.date = date;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setMainButton(String mainButton){
      this.mainButton = mainButton;
   }

   public String getPeriod(){
      return(period);
   }

   public void setPeriod(String period){
      this.period = period;
   }

   public ArrayList getPeriodList(){
      return(periodList);
   }

   public void setPeriodList(String period){
      periodList.add(period);
   }

   public void clearPeriodList(){
      periodList.clear();
      periodList.add(Constants.GLOBAL_BLANK);
   }

   public String getFinancialReport(){
      return(financialReport);
   }

   public void setFinancialReport(String financialReport){
      this.financialReport = financialReport;
   }

   public ArrayList getFinancialReportList(){
      return(financialReportList);
   }
   
   public void setFinancialReportList(String financialReport) {
   	
   	  financialReportList.add(financialReport);
   	
   }
   
   public void clearFinancialReportList(){
      financialReportList.clear();
      financialReportList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }
   
   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getConversionDate() {
	   
	   return conversionDate;
	   
   }
   
   public void setConversionDate(String conversionDate) {
	   
	   this.conversionDate = conversionDate;
	   
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
	   
      date = Common.convertSQLDateToString(new Date());
      goButton = null;
      mainButton = null;
      financialReport = Constants.GLOBAL_BLANK;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      conversionDate = Common.convertSQLDateToString(new Date());
            
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("goButton") != null){
         if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
		    errors.add("period", new ActionMessage("financialReportRun.error.periodRequired"));
		 }
		 if(Common.validateRequired(date)){
	        errors.add("date", new ActionMessage("financialReportRun.error.dateRequired"));
		 }
		 if(!Common.validateDateFormat(date)){
		 	errors.add("date", new ActionMessage("financialReportRun.error.dateInvalid"));
		 }
		 if(Common.validateRequired(financialReport)){
	            errors.add("financialReport", new ActionMessage("financialReportRun.error.financialReportRequired"));
	     }

		 if(Common.validateRequired(conversionDate)){
			 errors.add("conversionDate", new ActionMessage("financialReportRun.error.conversionDateRequired"));
		 }
		 if(!Common.validateDateFormat(conversionDate)){
			 errors.add("conversionDate", new ActionMessage("financialReportRun.error.conversionDateInvalid"));
		 }
      }
      return(errors);
   }
}
