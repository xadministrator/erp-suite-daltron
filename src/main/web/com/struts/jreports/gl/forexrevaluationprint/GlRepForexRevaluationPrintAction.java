package com.struts.jreports.gl.forexrevaluationprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepForexRevaluationPrintController;
import com.ejb.txn.GlRepForexRevaluationPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.GlChartOfAccountDetails;

public final class GlRepForexRevaluationPrintAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("GlRepForexRevaluationPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			GlRepForexRevaluationPrintForm actionForm = (GlRepForexRevaluationPrintForm)form; 
			
			String frParam= Common.getUserPermission(user, Constants.GL_REP_FOREX_REVALUATION_PRINT_ID);
			
			if (frParam != null) {
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
			}
			
/*******************************************************
 Initialize GlRepForexRevaluationPrintController EJB
*******************************************************/
			
			GlRepForexRevaluationPrintControllerHome homeFXP = null;
			GlRepForexRevaluationPrintController ejbFXP = null;       
			
			try {
				
				homeFXP = (GlRepForexRevaluationPrintControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/GlRepForexRevaluationPrintControllerEJB", GlRepForexRevaluationPrintControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in GlRepForexRevaluationPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbFXP = homeFXP.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in GlRepForexRevaluationPrintAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			short precisionUnit = ejbFXP.getGlFcPrecisionUnit(user.getCmpCode());
			
			/*** get report session and if not null set it to null **/
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}	           
			
/*******************************************************
 -- TO DO executeRepForexRevaluationPrint() --
*******************************************************/       
			
			if (frParam != null) {
				
				AdCompanyDetails adCmpDetails = new AdCompanyDetails();
				GlChartOfAccountDetails forexCoaDetails = new GlChartOfAccountDetails();
				ArrayList list = new ArrayList();           
				
				try {
					
					// get company
					
					adCmpDetails = ejbFXP.getAdCompany(user.getCmpCode());
					
					// get forex coa details
					
					forexCoaDetails = ejbFXP.getGlChartOfAccount(request.getParameter("unrealizedGainLossAccount"),
						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					actionForm.setAccountFrom(request.getParameter("accountFrom"));
					actionForm.setAccountTo(request.getParameter("accountTo"));
					actionForm.setConversionRate(request.getParameter("conversionRate"));
					actionForm.setCurrency(request.getParameter("currency"));
					actionForm.setPeriod(request.getParameter("period"));

					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getAccountFrom())) {
						
						criteria.put("accountNumberFrom", actionForm.getAccountFrom());
						
					}
					
					if (!Common.validateRequired(actionForm.getAccountTo())) {
						
						criteria.put("accountNumberTo", actionForm.getAccountTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getCurrency())) {
						
						criteria.put("functionalCurrency", actionForm.getCurrency());
						
					}

					list = ejbFXP.executeRepForexRevaluationPrint(criteria,
						Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit),
						Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
						actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')), user.getUserName(),
						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.noRecordFound"));
					
				} catch (GlJREffectiveDateNoPeriodExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.effectiveDateNoPeriodExist", ex.getMessage()));
					
				} catch (GlJREffectiveDateViolationException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.effectiveDateViolation", ex.getMessage()));	
					
				} catch (GlFCNoFunctionalCurrencyFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.currencyNotFound"));
					
				} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.defaultCompanyCurrencySelected"));
					
				} catch (GlCOANoChartOfAccountFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.chartOfAccountNotFound", ex.getMessage()));
					
				} catch (GlobalAccountNumberInvalidException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluationPrint.error.accountNumberInvalid"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlRepForexRevaluationPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return (mapping.findForward("glRepForexRevaluationPrintForm"));
					
				} 
				
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
				
				Map parameters = new HashMap();
				
				parameters.put("printedBy", user.getUserName());
				parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
				parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("accountFrom", actionForm.getAccountFrom());
				parameters.put("accountTo", actionForm.getAccountTo());
				parameters.put("conversionRate", actionForm.getConversionRate());
				parameters.put("currency", actionForm.getCurrency());
				parameters.put("period", actionForm.getPeriod());
				parameters.put("unrealizedGainLossAccount", forexCoaDetails.getCoaAccountNumber());
				parameters.put("unrealizedGainLossAccountDesc", forexCoaDetails.getCoaAccountDescription());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepForexRevaluationPrint.jasper";
				
				if (!new java.io.File(filename).exists()) {
					
					filename = servlet.getServletContext().getRealPath("jreports/GlRepForexRevaluationPrint.jasper");
					
				}
				
				try {
					
					Report report = new Report();
					
					report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
					report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, 
						new GlRepForexRevaluationPrintDS(list)));  
					
					session.setAttribute(Constants.REPORT_KEY, report);
					
				} catch(Exception ex) {
					
					ex.printStackTrace();
					
					if(log.isInfoEnabled()) {

						log.info("Exception caught in GlRepForexRevaluationPrintAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					} 
					
					return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("glRepForexRevaluationPrint"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("glRepForexRevaluationPrintForm"));
				
			}
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
/*******************************************************
 System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in GlRepForexRevaluationPrintAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}
			
			return(mapping.findForward("cmnErrorPage"));
			
		}
	}
}
