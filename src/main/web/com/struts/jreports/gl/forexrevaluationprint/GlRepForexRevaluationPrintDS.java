package com.struts.jreports.gl.forexrevaluationprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepForexRevaluationPrintDetails;


public class GlRepForexRevaluationPrintDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public GlRepForexRevaluationPrintDS(ArrayList list) {
		
		Iterator i = list.iterator();
		while(i.hasNext()) {
			
			GlRepForexRevaluationPrintDetails mdetails = (GlRepForexRevaluationPrintDetails)i.next();
			
			GlRepForexRevaluationPrintData glRepFXPData = new GlRepForexRevaluationPrintData(
				mdetails.getFxpFrlCoaAccountNumber(), mdetails.getFxpFrlCoaAccountDescription(),
				mdetails.getFxpFrlCoaAccountType(), new Double(mdetails.getFxpFrlRate()),
				new Double(mdetails.getFxpFrlForexAmount()), new Double(mdetails.getFxpForexGainLoss()),
				new Double (mdetails.getFxpForexRemainingAmount()));
			
			data.add(glRepFXPData);
			
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		return (index < data.size());
		
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		
		Object value = null;
		
		String fieldName = field.getName();
		if("accountNumber".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getAccountNumber();       
		}else if("accountDescription".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getAccountDescription();
		}else if("accountType".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getAccountType();			
		}else if("rate".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getRate();
		}else if("forexAmount".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getForexAmount();
		}else if("forexGainLoss".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getForexGainLoss();
		}else if("forexRemainingAmount".equals(fieldName)){
			value = ((GlRepForexRevaluationPrintData)data.get(index)).getForexRemainingAmount();
		}
		
		return(value);
	}
}
