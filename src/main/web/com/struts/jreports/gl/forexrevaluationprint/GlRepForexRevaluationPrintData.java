package com.struts.jreports.gl.forexrevaluationprint;


public class GlRepForexRevaluationPrintData implements java.io.Serializable {
	
	private String accountNumber = null;
	private String accountDescription = null;
	private String accountType = null;
	private Double rate = null;
	private Double forexAmount = null;
	private Double forexGainLoss = null;
	private Double forexRemainingAmount = null;
	
	public GlRepForexRevaluationPrintData(String accountNumber, String accountDescription, String accountType,
			Double rate, Double forexAmount, Double forexGainLoss, Double forexRemainingAmount) {
		
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.accountType = accountType;
		this.rate = rate;
		this.forexAmount = forexAmount;
		this.forexGainLoss = forexGainLoss;
		this.forexRemainingAmount = forexRemainingAmount; 
		
	}
	public String getAccountNumber() {
		
		return(accountNumber);
		
	}
	
	public String getAccountDescription() {
		
		return(accountDescription);
		
	}
	
	public String getAccountType() {
		
		return(accountType);
		
	}
	
	public Double getRate() {
		
		return(rate);
		
	}
	
	public Double getForexAmount() {
		
		return(forexAmount);
		
	}
	
	public Double getForexGainLoss() {
		
		return(forexGainLoss);
		
	}
	
	public Double getForexRemainingAmount() {
		
		return(forexRemainingAmount);
		
	}
		
}
