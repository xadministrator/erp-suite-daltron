package com.struts.jreports.gl.forexrevaluationprint;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class GlRepForexRevaluationPrintForm extends ActionForm implements Serializable {

	private String currency = null;
	private String conversionRate = null;	
	private String accountFrom = null;
	private String accountTo = null;
	private String period = null;

	private String userPermission = new String();
	
	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public String getConversionRate() {
		
		return conversionRate;
		
	}
	
	public void setConversionRate(String conversionRate) {
		
		this.conversionRate = conversionRate;
		
	}
	
	public String getAccountFrom() {
		
		return accountFrom;
		
	}
	
	public void setAccountFrom(String accountFrom) {
		
		this.accountFrom = accountFrom;
		
	}   
	public String getAccountTo() {
		
		return accountTo;
		
	}
	
	public void setAccountTo(String accountTo) {
		
		this.accountTo = accountTo;
		
	}   

	public String getPeriod() {
		
		return period;
		
	}
	
	public void setPeriod(String period) {
		
		this.period = period;
		
	}

	public String getUserPermission() {
		
		return userPermission;
		
	} 
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
	}       
}