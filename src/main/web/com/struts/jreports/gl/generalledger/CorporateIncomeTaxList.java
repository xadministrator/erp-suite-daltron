package com.struts.jreports.gl.generalledger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.util.GlRepGeneralLedgerDetails;

public class CorporateIncomeTaxList {

	
	
	
	private Map<String, Double> map = new HashMap<String, Double>();
	

	
	public CorporateIncomeTaxList(){
		GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
		String[] varList = details.getCITNameList();
		for(int x=0;x<varList.length;x++){
			map.put(varList[x], 0d);
		}
	}
	

	

	public Double getCIT(String key){
		return map.get(key);
	}
	
	public void setCIT(String key, Double value){
		map.put(key, value);
	}
	
	public void SubtractValue(String key, Double value){
		map.put(key, map.get(key) - value);
	}
	
	public void AddValue(String key, Double value){
		map.put(key, map.get(key) + value);
	}
	
}
