package com.struts.jreports.gl.generalledger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;




import javax.sql.DataSource;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRCsvExporter;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.joda.time.LocalDate;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdStoredProcedureSetupController;
import com.ejb.txn.AdStoredProcedureSetupControllerHome;
import com.ejb.txn.GlRepGeneralLedgerController;
import com.ejb.txn.GlRepGeneralLedgerControllerHome;
import com.struts.jreports.ar.statement.ArRepStatementDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdStoredProcedureDetails;
import com.util.EJBCommon;
import com.util.GenModSegmentDetails;
import com.util.GenModValueSetValueDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlRepGeneralLedgerDetails;

public final class GlRepGeneralLedgerAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRepGeneralLedgerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         GlRepGeneralLedgerForm actionForm = (GlRepGeneralLedgerForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_GENERAL_LEDGER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepGeneralLedger");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepGeneralLedgerController EJB
*******************************************************/

         GlRepGeneralLedgerControllerHome homeGL = null;
         GlRepGeneralLedgerController ejbGL = null;

         AdStoredProcedureSetupControllerHome homeSP = null;
         AdStoredProcedureSetupController ejbSP = null;
         try {

            homeGL = (GlRepGeneralLedgerControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepGeneralLedgerControllerEJB", GlRepGeneralLedgerControllerHome.class);

            homeSP = (AdStoredProcedureSetupControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdStoredProcedureSetupControllerEJB", AdStoredProcedureSetupControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in GlRepGeneralLedgerAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbGL = homeGL.create();
            ejbSP = homeSP.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in GlRepGeneralLedgerAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- GL GL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArrayList list = null;
            GlRepGeneralLedgerDetails details = null;
            AdCompanyDetails adCmpDetails = null;
            AdStoredProcedureDetails adStoredProcedureDetails = null;
            String company = null;
            String dateAppointment = null;
            String storedProcedureName = null;
            boolean enableStoredProcedure = false;



		    try {

		    	ArrayList branchList = new ArrayList();
		    	String branchCodes = "";


		     	for(int i=0; i<actionForm.getGlRepBrGeneralLedgerListSize(); i++) {

		     	    GlRepBranchGeneralLedgerList adBglList = (GlRepBranchGeneralLedgerList)actionForm.getGlRepBrGeneralLedgerListByIndex(i);

		     	    if(adBglList.getBranchCheckbox() == true) {

		     	        AdBranchDetails brDetails = new AdBranchDetails();

		     	        brDetails.setBrAdCompany(user.getCmpCode());
		     	        brDetails.setBrCode(adBglList.getBrCode());
		     	        branchCodes += adBglList.getBrCode()+ ",";
		     	        branchList.add(brDetails);

		     	    }
		     	}


		     	// get company

		       adCmpDetails = ejbGL.getAdCompany(user.getCmpCode());
		       adStoredProcedureDetails = ejbSP.getAdSp(user.getCmpCode());


		       company = adCmpDetails.getCmpName();
		       dateAppointment = adCmpDetails.getCmpDateAppointment();
		       storedProcedureName = adStoredProcedureDetails.getSpNameGlGeneralLedgerReport();
		       enableStoredProcedure = Common.convertByteToBoolean(adStoredProcedureDetails.getSpEnableGlGeneralLedgerReport()) ;

		       ArrayList vsvDescList = new ArrayList();

			   ArrayList genVsList = actionForm.getGenVsList();
			   Iterator i = genVsList.iterator();


			   while (i.hasNext()) {

			    	GlRepGeneralLedgerVsList valueSetValue = (GlRepGeneralLedgerVsList)i.next();

			    	if (Common.validateRequired(valueSetValue.getValueSetValueDescription())) continue;

			    	GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
			    	mdetails.setVsvVsName(valueSetValue.getValueSetName());
			    	mdetails.setVsvDescription(valueSetValue.getValueSetValueDescription());

			    	vsvDescList.add(mdetails);

			   }

			   System.out.println("------------------------------------->");
			   // get selected account type
			   String accountTypes = "";
	           	ArrayList qualifierSelectedList = new ArrayList();

	       		for (int x=0; x < actionForm.getQualifierSelectedList().length; x++) {

	       			qualifierSelectedList.add(actionForm.getQualifierSelectedList()[x]);
	       			accountTypes += actionForm.getQualifierSelectedList()[x]+ ",";

	       		}


	       		DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
				Connection conn = null;
				CallableStatement stmt = null;

				ResultSet rs = null;

				// execute report

			    if(enableStoredProcedure){

			    	LocalDate today = new LocalDate(Common.convertStringToSQLDate(actionForm.getDate()));

			    	try{

			    		System.out.println("pasok1");
			    		conn = dataSource.getConnection();
			    		 stmt = (CallableStatement) conn.prepareCall(storedProcedureName);
			    		 Date dateFrom = today.withDayOfYear(1).toDate();
                         Date dateTo = Common.convertStringToSQLDate(actionForm.getDate());
                         stmt.setString(1, actionForm.getAccountFrom());
                         stmt.setString(2, actionForm.getAccountTo());
                         stmt.setDate(3, new java.sql.Date(dateFrom.getTime()));
                         stmt.setDate(4, new java.sql.Date(dateTo.getTime()));
                         stmt.setString(5, branchCodes);
                         stmt.setString(6, accountTypes);
                         stmt.setByte(7, Common.convertBooleanToByte(actionForm.getIncludeUnpostedTransaction()));
                         stmt.setByte(8, Common.convertBooleanToByte(actionForm.getIncludeUnpostedSlTransaction()));

			    		 stmt.executeQuery();
			    		 /*
                                    conn = dataSource.getConnection();
                                    stmt = (CallableStatement) conn.prepareCall(storedProcedureName);
                                    Date dateFrom = today.withDayOfYear(1).toDate();
                                    Date dateTo = Common.convertStringToSQLDate(actionForm.getDate());
                                    stmt.setString(1, actionForm.getAccountFrom());
                                    stmt.setString(2, actionForm.getAccountTo());
                                    stmt.setDate(3, new java.sql.Date(dateFrom.getTime()));
                                    stmt.setDate(4, new java.sql.Date(dateTo.getTime()));
                                    stmt.setString(5, branchCodes);
                                    stmt.setString(6, accountTypes);
                                    stmt.setByte(7, Common.convertBooleanToByte(actionForm.getIncludeUnpostedTransaction()));
                                    stmt.setByte(8, Common.convertBooleanToByte(actionForm.getIncludeUnpostedSlTransaction()));


                                    rs = stmt.executeQuery();

                                    list = ejbGL.executeSpGlRepGeneralLedger(rs,
                                            actionForm.getAccountFrom(),
                                            actionForm.getAccountTo(),
                                            vsvDescList,
                                            Common.convertStringToSQLDate(actionForm.getDate()),
                                            actionForm.getAmountType(), actionForm.getJournalSource().equals("") ? null : actionForm.getJournalSource(),
                                            actionForm.getIncludeUnpostedTransaction(), actionForm.getShowZeroesTxn(),
                                            actionForm.getShowZeroesAll(), actionForm.getIncludeUnpostedSlTransaction(), actionForm.getVatRelief(),
                                            actionForm.getCurrency(), actionForm.getShowEntries(), actionForm.getReportType(), actionForm.getOrderBy(), qualifierSelectedList, branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


									*/
			    		 		    System.out.println("pasok2");
                                 } catch(SQLException ex){
                                	 System.out.println("sql error: " + ex.toString());
                                	 
                                	  try { 
                                         if(rs != null) rs.close(); 
                                      }catch(SQLException f){ ; }
                                      rs = null;
                                      try { 
                                          if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
                                      stmt = null;
                                      try{ 
                                          if(conn != null) conn.close(); }catch(SQLException f){ ; }
                                      conn = null;
                                 }finally{
                                    try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
                                    rs = null;
                                    try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
                                    stmt = null;
                                    try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
                                    conn = null;
                                 }

			    } else {


			    	list = ejbGL.executeGlRepGeneralLedger(
					       	   actionForm.getAccountFrom(),
					       	   actionForm.getAccountTo(),
					       	   vsvDescList,
					       	   Common.convertStringToSQLDate(actionForm.getDate()),
					       	   actionForm.getAmountType(), actionForm.getJournalSource().equals("") ? null : actionForm.getJournalSource(),
					           actionForm.getIncludeUnpostedTransaction(), actionForm.getShowZeroesTxn(),
					           actionForm.getShowZeroesAll(), actionForm.getIncludeUnpostedSlTransaction(), actionForm.getVatRelief(),
					           actionForm.getCurrency(), actionForm.getShowEntries(), actionForm.getReportType(), actionForm.getOrderBy(), qualifierSelectedList, branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());



			    }



		    } catch (GlobalNoRecordFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("generalLedger.error.noRecordFound"));

           } catch (GlobalAccountNumberInvalidException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("generalLedger.error.accountNumberInvalid"));

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepGeneralLedgerAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepGeneralLedger"));

		    }

		    // fill report parameters, fill report to pdf and set report session

		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("tin", adCmpDetails.getCmpTin());

		    parameters.put("taxPayerName", adCmpDetails.getCmpTaxPayerName());
		    parameters.put("contacts", adCmpDetails.getCmpContact());

		    parameters.put("phoneNumber", adCmpDetails.getCmpPhone());
		    parameters.put("email", adCmpDetails.getCmpEmail());



		    parameters.put("salaryWagesMailSectionNo", actionForm.getSalaryWagesMailSectionNo());
		    parameters.put("salaryWagesMailLotNo", actionForm.getSalaryWagesMailLotNo());
		    parameters.put("salaryWagesMailStreet", actionForm.getSalaryWagesMailStreet());
		    parameters.put("salaryWagesMailPoBox", actionForm.getSalaryWagesMailPoBox());
		    parameters.put("salaryWagesMailCountry", actionForm.getSalaryWagesMailCountry());
		    parameters.put("salaryWagesMailProvince", actionForm.getSalaryWagesMailProvince());
		    parameters.put("salaryWagesMailPostOffice", actionForm.getSalaryWagesMailPostOffice());
		    parameters.put("salaryWagesMailCareOff", actionForm.getSalaryWagesMailCareOff());
		    parameters.put("salaryWagesTaxPeriodFrom", actionForm.getSalaryWagesTaxPeriodFrom());
		    parameters.put("salaryWagesTaxPeriodTo", actionForm.getSalaryWagesTaxPeriodTo());
		    parameters.put("salaryWagesPublicOfficeName", actionForm.getSalaryWagesPublicOfficeName());
		    parameters.put("salaryWagesDateAppointment", actionForm.getSalaryWagesDateAppointment());




		    parameters.put("sawParam2", new Double(actionForm.getSalaryWagesField2()));
		    parameters.put("sawParam4", new Double(actionForm.getSalaryWagesField4()));
		    parameters.put("sawParam5", new Double(actionForm.getSalaryWagesField5()));
		    parameters.put("sawParam6", new Double(actionForm.getSalaryWagesField6()));


		    parameters.put("corporateIncomeTaxMailSectionNo", actionForm.getCorporateIncomeTaxMailSectionNo());
		    parameters.put("corporateIncomeTaxMailLotNo", actionForm.getCorporateIncomeTaxMailLotNo());
		    parameters.put("corporateIncomeTaxMailStreet", actionForm.getCorporateIncomeTaxMailStreet());
		    parameters.put("corporateIncomeTaxMailPoBox", actionForm.getCorporateIncomeTaxMailPoBox());
		    parameters.put("corporateIncomeTaxMailCountry", actionForm.getCorporateIncomeTaxMailCountry());
		    parameters.put("corporateIncomeTaxMailProvince", actionForm.getCorporateIncomeTaxMailProvince());
		    parameters.put("corporateIncomeTaxMailPostOffice", actionForm.getCorporateIncomeTaxMailPostOffice());
		    parameters.put("corporateIncomeTaxMailCareOff", actionForm.getCorporateIncomeTaxMailCareOff());
		    parameters.put("corporateIncomeTaxTaxPeriodFrom", actionForm.getCorporateIncomeTaxTaxPeriodFrom());
		    parameters.put("corporateIncomeTaxTaxPeriodTo", actionForm.getCorporateIncomeTaxTaxPeriodTo());
		    parameters.put("corporateIncomeTaxPublicOfficeName", actionForm.getCorporateIncomeTaxPublicOfficeName());
		    parameters.put("corporateIncomeTaxDateAppointment", actionForm.getCorporateIncomeTaxDateAppointment());


		    parameters.put("individualIncomeTaxMailSectionNo", actionForm.getIndividualIncomeTaxMailSectionNo());
		    parameters.put("individualIncomeTaxMailLotNo", actionForm.getIndividualIncomeTaxMailLotNo());
		    parameters.put("individualIncomeTaxMailStreet", actionForm.getIndividualIncomeTaxMailStreet());
		    parameters.put("individualIncomeTaxMailPoBox", actionForm.getIndividualIncomeTaxMailPoBox());
		    parameters.put("individualIncomeTaxMailCountry", actionForm.getIndividualIncomeTaxMailCountry());
		    parameters.put("individualIncomeTaxMailProvince", actionForm.getIndividualIncomeTaxMailProvince());
		    parameters.put("individualIncomeTaxMailPostOffice", actionForm.getIndividualIncomeTaxMailPostOffice());
		    parameters.put("individualIncomeTaxMailCareOff", actionForm.getIndividualIncomeTaxMailCareOff());
		    parameters.put("individualIncomeTaxTaxPeriodFrom", actionForm.getIndividualIncomeTaxTaxPeriodFrom());
		    parameters.put("individualIncomeTaxTaxPeriodTo", actionForm.getIndividualIncomeTaxTaxPeriodTo());







		    parameters.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
		    parameters.put("accountFrom", actionForm.getAccountFrom());
		    parameters.put("accountTo", actionForm.getAccountTo());
            parameters.put("amountType", actionForm.getAmountType());
            if (actionForm.getShowEntries()) {

            	parameters.put("showEntries", "YES");

		    } else {

		    	parameters.put("showEntries", "NO");

		    }

            //parameters.put("firstDayOfPeriod", adCmpDetails.getCmpEmail());

		    LocalDate today = new LocalDate(Common.convertStringToSQLDate(actionForm.getDate()));

		    LocalDate firstDayOfPeriod = new LocalDate();


		    if(actionForm.getAmountType().equals("PTD")){

		    	firstDayOfPeriod = today.withDayOfMonth(1);


		    } else {

		    	firstDayOfPeriod = today.withDayOfYear(1);
		    }





		  	System.out.println(actionForm.getAmountType()+"= firstDayOfPeriod="+firstDayOfPeriod);
		    parameters.put("firstDayOfPeriod", firstDayOfPeriod.toDate());
            parameters.put("viewType", actionForm.getViewType());
            parameters.put("journalSource", actionForm.getJournalSource());
            parameters.put("currency", actionForm.getCurrency());
            parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());

		    if (actionForm.getIncludeUnpostedTransaction()) {

		    	parameters.put("includeUnpostedTransaction", "YES");

		    } else {

		    	parameters.put("includeUnpostedTransaction", "NO");

		    }

		    if (actionForm.getShowZeroesTxn()) {

		    	parameters.put("showZeroesTxn", "YES");

		    } else {

		    	parameters.put("showZeroesTxn", "NO");

		    }

		    if (actionForm.getShowZeroesAll()) {

		    	parameters.put("showZeroesAll", "YES");

		    } else {

		    	parameters.put("showZeroesAll", "NO");

		    }

		    if (actionForm.getIncludeUnpostedSlTransaction()) {

		    	parameters.put("includeUnpostedSlTransaction", "YES");

		    } else {

		    	parameters.put("includeUnpostedSlTransaction", "NO");

		    }

		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getGlRepBrGeneralLedgerListSize(); j++) {

      			GlRepBranchGeneralLedgerList brList = (GlRepBranchGeneralLedgerList)actionForm.getGlRepBrGeneralLedgerListByIndex(j);

      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}

      		}
      		parameters.put("branchMap", branchMap);

		    String filename = null;



		    if (actionForm.getReportType().equals("RELIEF SALES")) {
		    	System.out.println("REPORT RELIEF SALES");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerReliefAR.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerReliefAR.jasper");

			    }
		    } else if (actionForm.getReportType().equals("RELIEF PURCHASES")) {
		    	System.out.println("REPORT RELIEF PURCHASES");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerReliefAP.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerReliefAP.jasper");

			    }

		    } else if (actionForm.getReportType().equals("RELIEF IMPORTATION")) {
		    	System.out.println("REPORT RELIEF IMPORTATION");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerReliefImportation.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerReliefImportation.jasper");

			    }


		    } else if (actionForm.getReportType().equals("CIT RETURN")) {
		    	System.out.println("CIT RETURN");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerCITReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerCITReturn.jasper");

			    }


		    } else if (actionForm.getReportType().equals("SALARY WAGES")) {
		    	System.out.println("SALARY WAGES");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerSalaryWages.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerSalaryWages.jasper");

			    }

		    } else if (actionForm.getReportType().equals("IIT RETURN")) {
		    	System.out.println("IIT RETURN");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerIncomeTaxReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerIncomeTaxReturn.jasper");

			    }

		    } else if (actionForm.getReportType().equals("GST RETURN")) {
		    	System.out.println("GST RETURN");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerGSTReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedgerGSTReturn.jasper");

			    }
         	} else {


         		System.out.println("REPORT GENERAL LEDGER");
         		filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedger.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGeneralLedger.jasper");

			    }

		    }

		    try {

		    	Report report = new Report();

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {


		    	   if(enableStoredProcedure) {
		    		   filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerReportTable.jasper";
		    		   	DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
						Connection conn = null;

						conn = dataSource.getConnection();

			       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    //   report.setBytes(
				     //     JasperRunManager.runReportToPdf(filename, parameters,
					 //       new GlRepGeneralLedgerDS(list)));


		    		   report.setBytes( JasperRunManager.runReportToPdf(filename, parameters, conn));


						  	conn.close();
		    	   }else {

			       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				       report.setBytes(
				          JasperRunManager.runReportToPdf(filename, parameters,
					        new GlRepGeneralLedgerDS(list)));

		    	   }






		       } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)){
		    	   /*
		    	   report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
	               report.setBytes(
					   JasperRunManagerExt.runReportToCsv(filename, parameters,
					        new GlRepGeneralLedgerDS(list)));
		    	    */

		    	   if(enableStoredProcedure) {
		    		   filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGeneralLedgerReportTable.jasper";
		    		   	DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
						Connection conn = null;

						conn = dataSource.getConnection();

			       	   	report.setViewType(Constants.REPORT_VIEW_TYPE_CSV_SP);

			       	   	JasperPrint jrprint = JasperFillManager.fillReport(filename, parameters,
							        conn);

			       	   	report.setJasperPrint(jrprint);
			       	   	
			       	   	conn.close();
			       	   	


		    	   }else {
		    		    report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
			               report.setBytes(
							   JasperRunManagerExt.runReportToCsv(filename, parameters,
							        new GlRepGeneralLedgerDS(list)));
		    	   }






			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters,
					        new GlRepGeneralLedgerDS(list)));



			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);

				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
				       new GlRepGeneralLedgerDS(list)));

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);


		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepGeneralLedgerAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }


/*******************************************************
   -- GL GL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- GL GL Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	         	try {

			        ArrayList list = null;
			        Iterator i = null;

			        actionForm.clearGenVsList();

	            	list = ejbGL.getGenVsAll(user.getCmpCode());

	        		i = list.iterator();

	        		while (i.hasNext()) {

	        		    GlRepGeneralLedgerVsList valueSetValue = new GlRepGeneralLedgerVsList(actionForm,
	        		        (String)i.next(), null);


	        			actionForm.saveGenVsList(valueSetValue);

	        		}

	            	list = ejbGL.getGenSgAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            	} else {

	            		i = list.iterator();

	            		actionForm.setAccountFrom("");
	            		actionForm.setAccountTo("");

	            		while (i.hasNext()) {

	            		    GenModSegmentDetails mdetails =
	            		        (GenModSegmentDetails)i.next();

	            		    for (int j=0; j<mdetails.getSgMaxSize(); j++) {

	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + "0");
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + "Z");

	            		    }

	            		    if (i.hasNext()) {

	            		    	actionForm.setAccountFrom(actionForm.getAccountFrom() + String.valueOf(mdetails.getSgFlSegmentSeparator()));
	            		    	actionForm.setAccountTo(actionForm.getAccountTo() + String.valueOf(mdetails.getSgFlSegmentSeparator()));

	            		    }

	            		}

	            	}




	            	actionForm.clearQualifierList();

	            	list = ejbGL.getGenQlfrAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setQualifierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setQualifierList((String)i.next());

	            		}

	            	}


	            	actionForm.clearJournalSourceList();

		        	list = ejbGL.getGlJsAll(user.getCmpCode());

		        	if (list == null || list.size() == 0) {

		        		actionForm.setJournalSourceList(Constants.GLOBAL_NO_RECORD_FOUND);

		        	} else {

		        		i = list.iterator();

		        		while (i.hasNext()) {

		        		    actionForm.setJournalSourceList((String)i.next());

		        		}

		        	}


		        	actionForm.clearCurrencyList();

	            	list = ejbGL.getGlFcAllWithDefault(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		actionForm.setCurrencyList(Constants.GLOBAL_BLANK);

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

	            			actionForm.setCurrencyList(mFcDetails.getFcName());

	            			/*if (mFcDetails.getFcSob() == 1) {

	            				actionForm.setCurrency(mFcDetails.getFcName());
	            			}*/
	            		}
	            	}


	            	actionForm.clearReportTypeList();

			        list = ejbGL.getAdLvReportTypeAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setReportTypeList((String)i.next());

	            		}

	            	}



				} catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepGeneralLedgerAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

			    }


	            actionForm.reset(mapping, request);

                // populate AdRepBranchBankAccountListList

         		ArrayList brList = new ArrayList();
         		actionForm.clearGlRepBrGeneralLedgerList();

         		brList = ejbGL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

                Iterator k = brList.iterator();

                while(k.hasNext()) {

                    AdBranchDetails brDetails = (AdBranchDetails)k.next();

                    GlRepBranchGeneralLedgerList adBglList = new GlRepBranchGeneralLedgerList(actionForm,
                        brDetails.getBrBranchCode(), brDetails.getBrName(),
                        brDetails.getBrCode());
                    adBglList.setBranchCheckbox(true);

                    actionForm.saveGlRepBrGeneralLedgerList(adBglList);

                }



             // get company details

	            AdCompanyDetails adCmpDetails = ejbGL.getAdCompany(user.getCmpCode());

	            actionForm.setSalaryWagesMailSectionNo(adCmpDetails.getCmpMailSectionNo());
	            actionForm.setSalaryWagesMailLotNo(adCmpDetails.getCmpMailLotNo());
	            actionForm.setSalaryWagesMailStreet(adCmpDetails.getCmpMailStreet());
	            actionForm.setSalaryWagesMailPoBox(adCmpDetails.getCmpMailPoBox());
	            actionForm.setSalaryWagesMailCountry(adCmpDetails.getCmpMailCountry());
	            actionForm.setSalaryWagesMailProvince(adCmpDetails.getCmpMailProvince());
	            actionForm.setSalaryWagesMailPostOffice(adCmpDetails.getCmpMailPostOffice());
	            actionForm.setSalaryWagesMailCareOff(adCmpDetails.getCmpMailCareOff());
	            actionForm.setSalaryWagesTaxPeriodFrom(adCmpDetails.getCmpTaxPeriodFrom());
	            actionForm.setSalaryWagesTaxPeriodTo(adCmpDetails.getCmpTaxPeriodTo());
	            actionForm.setSalaryWagesPublicOfficeName(adCmpDetails.getCmpPublicOfficeName());
	            actionForm.setSalaryWagesDateAppointment(adCmpDetails.getCmpDateAppointment());

	            actionForm.setSalaryWagesField2(Common.convertDoubleToStringMoney(0d,(short)2));
	            actionForm.setSalaryWagesField4(Common.convertDoubleToStringMoney(0d,(short)2));
	            actionForm.setSalaryWagesField5(Common.convertDoubleToStringMoney(0d,(short)2));
	            actionForm.setSalaryWagesField6(Common.convertDoubleToStringMoney(0d,(short)2));

	            actionForm.setCorporateIncomeTaxMailSectionNo(adCmpDetails.getCmpMailSectionNo());
	            actionForm.setCorporateIncomeTaxMailLotNo(adCmpDetails.getCmpMailLotNo());
	            actionForm.setCorporateIncomeTaxMailStreet(adCmpDetails.getCmpMailStreet());
	            actionForm.setCorporateIncomeTaxMailPoBox(adCmpDetails.getCmpMailPoBox());
	            actionForm.setCorporateIncomeTaxMailCountry(adCmpDetails.getCmpMailCountry());
	            actionForm.setCorporateIncomeTaxMailProvince(adCmpDetails.getCmpMailProvince());
	            actionForm.setCorporateIncomeTaxMailPostOffice(adCmpDetails.getCmpMailPostOffice());
	            actionForm.setCorporateIncomeTaxMailCareOff(adCmpDetails.getCmpMailCareOff());
	            actionForm.setCorporateIncomeTaxTaxPeriodFrom(adCmpDetails.getCmpTaxPeriodFrom());
	            actionForm.setCorporateIncomeTaxTaxPeriodTo(adCmpDetails.getCmpTaxPeriodTo());
	            actionForm.setCorporateIncomeTaxPublicOfficeName(adCmpDetails.getCmpPublicOfficeName());
	            actionForm.setCorporateIncomeTaxDateAppointment(adCmpDetails.getCmpDateAppointment());


	            actionForm.setIndividualIncomeTaxMailSectionNo(adCmpDetails.getCmpMailSectionNo());
	            actionForm.setIndividualIncomeTaxMailLotNo(adCmpDetails.getCmpMailLotNo());
	            actionForm.setIndividualIncomeTaxMailStreet(adCmpDetails.getCmpMailStreet());
	            actionForm.setIndividualIncomeTaxMailPoBox(adCmpDetails.getCmpMailPoBox());
	            actionForm.setIndividualIncomeTaxMailCountry(adCmpDetails.getCmpMailCountry());
	            actionForm.setIndividualIncomeTaxMailProvince(adCmpDetails.getCmpMailProvince());
	            actionForm.setIndividualIncomeTaxMailPostOffice(adCmpDetails.getCmpMailPostOffice());
	            actionForm.setIndividualIncomeTaxMailCareOff(adCmpDetails.getCmpMailCareOff());
	            actionForm.setIndividualIncomeTaxTaxPeriodFrom(adCmpDetails.getCmpTaxPeriodFrom());
	            actionForm.setIndividualIncomeTaxTaxPeriodTo(adCmpDetails.getCmpTaxPeriodTo());

	            return(mapping.findForward("glRepGeneralLedger"));

				 } else {

				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));

				    return(mapping.findForward("cmnMain"));

				 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in GlRepGeneralLedgerAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
