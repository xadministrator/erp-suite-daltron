package com.struts.jreports.gl.generalledger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Date;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.GlRepGeneralLedgerDetails;


public class GlRepGeneralLedgerDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;
   private Double debit = 0d;
   private Double credit = 0d;
   
   
   double asdf = 0;
   double asdf2 = 0;
   double balance = 0;
   
   IndividualIncomeTaxList iitList = new IndividualIncomeTaxList();
   CorporateIncomeTaxList citList = new CorporateIncomeTaxList();
   SalaryAndWagesList sawList = new SalaryAndWagesList();
   GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   String[] citNames = details.getCITNameList();
   String[] sawNames = details.getSAWNameList();
   String[] iitNames = details.getIITNameList();
  

   public GlRepGeneralLedgerDS(ArrayList list){
   	
   	Date currentDate = new Date();
   	
    double [] variable = new double[40];
 
    
    int a = 0;
      Iterator i = list.iterator();
      
      String accountNumber = "";
      
      while (i.hasNext()) {
      	a++;
         GlRepGeneralLedgerDetails details = (GlRepGeneralLedgerDetails)i.next();       
         
         double vatNet = 0;
         double journalAmount = 0d;

         if (accountNumber.equals(details.getGlAccountNumber()))  System.out.println("details.getGlAccountNumber()="+details.getGlAccountNumber());
         accountNumber = details.getGlAccountNumber();
         if((details.getGlAccountType().equals("ASSET")  || details.getGlAccountType().equals("EXPENSE") )&& details.getGlJlDebit() == (short)1){//DEBIT
    
        	 
        	 //CIT Category
        	 if(details.getGlCitCategory() != null)
	        	 for (int y=0;y<citNames.length;y++){
	        		
	        		 String citNameVal = citNames[y];
	        		 
	        		 
	        		 if(details.getGlCitCategory() != null  && details.getGlCitCategory().equals(citNames[y])){
	        	
	        			 citList.AddValue(citNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 
        	 //SAW Category
        	 if(details.getGlSawCategory() != null)
	        	 for (int y=0;y<sawNames.length;y++){
	        		
	        		 String sawNameVal = sawNames[y];
	        		 
	        		 
	        		 if(details.getGlSawCategory() != null  && details.getGlSawCategory().equals(sawNames[y])){
	
	        			 sawList.AddValue(sawNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 
        	 
        	 
        	 
        	 
	        //IIT Category
        	 if(details.getGlIitCategory() != null)
	        	 for(int x=0;x<iitNames.length;x++){
	        		 
	        		 String iitNameVal = iitNames[x];
	        		 if(details.getGlIitCategory() != null  && details.getGlIitCategory().equals(iitNames[x])){
	        			 
	        			 iitList.AddValue(iitNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	
        	 
        	 
        	 
         } else if((details.getGlAccountType().equals("ASSET")  || details.getGlAccountType().equals("EXPENSE")) && details.getGlJlDebit() == (short)0){//CREDIT
        	 

        	 //IIT Category
        	 if(details.getGlIitCategory() != null)
	        	 for(int x=0;x<iitNames.length;x++){
	        		 
	        		 String iitNameVal = iitNames[x];
	        		 if(details.getGlIitCategory() != null  && details.getGlIitCategory().equals(iitNames[x])){
	        			 iitList.SubtractValue(iitNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 //CIT Category
        	 if(details.getGlCitCategory() != null)
	        	 for (int x=0;x<citNames.length;x++){
	        		 
	        		 String citNameVal = citNames[x];
	        		 if(details.getGlCitCategory() != null  && details.getGlCitCategory().equals(citNames[x])){
	        			 citList.SubtractValue(citNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 
        	 //SAW Category
        	 if(details.getGlSawCategory() != null)
	        	 for (int x=0;x<sawNames.length;x++){
	        		 
	        		 String sawNameVal = sawNames[x];
	        		 if(details.getGlSawCategory() != null  && details.getGlSawCategory().equals(sawNames[x])){
	        			 sawList.SubtractValue(sawNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	  
        	 
        	 ///
         } else if((details.getGlAccountType().equals("LIABILITY") || details.getGlAccountType().equals("OWNERS EQUITY") || details.getGlAccountType().equals("REVENUE"))  && details.getGlJlDebit() == (short)1){//DEBIT

        	 //IIT Category
        	 if(details.getGlIitCategory() != null)
	        	 for(int x=0;x<iitNames.length;x++){
	        		 
	        		 String iitNameVal = iitNames[x];
	        		 if(details.getGlIitCategory() != null  && details.getGlIitCategory().equals(iitNames[x])){
	        			 iitList.SubtractValue(iitNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 //CIT Category
        	 if(details.getGlCitCategory() != null)
	        	 for (int x=0;x<citNames.length;x++){
	        		 
	        		 String citNameVal = citNames[x];
	        		 if(details.getGlCitCategory() != null  && details.getGlCitCategory().equals(citNames[x])){
	        			 citList.SubtractValue(citNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 
        	//SAW Category
        	 if(details.getGlSawCategory() != null)
	        	 for (int x=0;x<sawNames.length;x++){
	        		 
	        		 String sawNameVal = sawNames[x];
	        		 if(details.getGlSawCategory() != null  && details.getGlSawCategory().equals(sawNames[x])){
	        			 sawList.SubtractValue(sawNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	

         } else if((details.getGlAccountType().equals("LIABILITY") || details.getGlAccountType().equals("OWNERS EQUITY") || details.getGlAccountType().equals("REVENUE"))  && details.getGlJlDebit() == (short)0){//DEBIT
             	
        	 //IIT Category
        	 if(details.getGlIitCategory() != null)
	        	 for(int x=0;x<iitNames.length;x++){
	        		 
	        		 String iitNameVal = iitNames[x];
	        		 if(details.getGlIitCategory() != null  && details.getGlIitCategory().equals(iitNames[x])){
	        			 iitList.AddValue(iitNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	 //CIT Category
        	 if(details.getGlCitCategory() != null)
	        	 for (int x=0;x<citNames.length;x++){
	        		 
	        		 String citNameVal = citNames[x];
	        		 if(details.getGlCitCategory() != null  && details.getGlCitCategory().equals(citNames[x])){
	        			 citList.AddValue(citNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	//SAW Category
        	 if(details.getGlSawCategory() != null)
	        	 for (int x=0;x<sawNames.length;x++){
	        		 
	        		 String sawNameVal = sawNames[x];
	        		 if(details.getGlSawCategory() != null  && details.getGlSawCategory().equals(sawNames[x])){
	        			 sawList.AddValue(sawNameVal, details.getGlJlAmount());
	        		 }
	        	 }
        	 
        	
         }
         
         if ((details.getGlAccountType().equals("ASSET") || details.getGlAccountType().equals("EXPENSE")) && details.getGlJlDebit() == (short)1) {
        	 vatNet = details.getGlJlAmount();
        	 journalAmount = details.getGlJlAmount();
        	 asdf = asdf +details.getGlJlAmount();

         } else if ((details.getGlAccountType().equals("ASSET") || details.getGlAccountType().equals("EXPENSE")) && details.getGlJlDebit() == (short)0) {
        	 vatNet = details.getGlJlAmount() * -1;
        	 journalAmount = details.getGlJlAmount() * -1;
        	 asdf2 = asdf2 +details.getGlJlAmount();
        	 
         } else if ((!details.getGlAccountType().equals("ASSET") && !details.getGlAccountType().equals("EXPENSE")) && details.getGlJlDebit() == (short)1) {
        	 vatNet = details.getGlJlAmount() * -1;
        	 journalAmount = details.getGlJlAmount() * -1;
        	 //asdf = asdf +(details.getGlJlAmount() * -1);
  	 
         } else if ((!details.getGlAccountType().equals("ASSET") && !details.getGlAccountType().equals("EXPENSE")) && details.getGlJlDebit() == (short)0) {
        	 vatNet = details.getGlJlAmount();
        	 journalAmount = details.getGlJlAmount();
 
         }        
         balance=details.getGlBalance();

	     GlRepGeneralLedgerData glData = new GlRepGeneralLedgerData(
	    	 details.getGlAccountNumber(), 
	    	 details.getGlCitCategory(), details.getGlSawCategory(), details.getGlIitCategory(),
	         details.getGlAccountDescription(), 
	         new Double(details.getGlBeginningBalance()),
	         Common.convertSQLDateToString(details.getGlJrEffectiveDate()),
	         details.getGlJrEffectiveDate(),
	         details.getGlJrName(), 
	         details.getGlJrDescription(), 
	         details.getGlJsName(),
	         details.getGlJrTin(), 
	         details.getGlJrDocumentNumber(), 
	         details.getGlJrCheckNumber(), 
	         details.getGlJlDebit() == (short)1 ? new Double(details.getGlJlAmount()) : 0d, 
	         details.getGlJlDebit() == (short)0 ? new Double(details.getGlJlAmount()) : 0d,
	         new Double(journalAmount),
	        		 
	         details.getGlAccountType(), new Double(details.getGlBalance()),
			 details.getGlJrPosted() == (byte)1 ? "YES" : "NO", details.getGlJrSubLedger(),
			 details.getGlJrReferenceNumber(), Common.convertSQLDateToString(currentDate), 
			 details.getGlVatTin(), details.getGlVatSubLedgerName(), details.getGlVatAddress1(), details.getGlVatAddress2(), 
			 new Double(vatNet),
			 new Double(details.getGlVatAmountDue()), details.getGlVatTaxCode(),
			 new Double(details.getGlConversionRate()),
			 details.getGlShowEntries(),
			 details.getGlCompanyName(), details.getGlCompanyTin(), details.getGlCompanyAddress(), details.getGlCompanyCity(), details.getGlCompanyRevenueOffice(), details.getGlCompanyFiscalYearEnding(),
			 citList,iitList
	    		 );

         data.add(glData);
      }
      

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();
    
      if("account".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getAccount();
     }else if("accountDescription".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getAccountDescription();
     }else if("beginningBalance".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getBeginningBalance();
     }else if("date".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getDate();
     }else if("date2".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getDate2();
     }else if("name".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getName();
     }else if("description".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getDescription();
     }else if("source".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getSource();
     }else if("tin".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getTin();
	}else if("documentNumber".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getDocumentNumber();
	}else if("checkNumber".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getCheckNumber();
     }else if("debit".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getDebit();
     }else if("credit".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getCredit();   
     }else if("journalAmount".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getJournalAmount();  
          
     }else if("accountType".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getAccountType();  
     }else if("balance".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getBalance();
     }else if("posted".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getPosted();
     }else if("subLedger".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getSubLedger();
     }else if("referenceNumber".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getReferenceNumber();
     }else if("dateCreated".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getDateCreated(); 
     }else if("vatTin".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getVatTin();   
     }else if("vatSubLedgerName".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getVatSubLedgerName();   
     }else if("vatAddress1".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getVatAddress1(); 
     }else if("vatAddress2".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getVatAddress2(); 
     }else if("vatNet".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getVatNet(); 
     }else if("amountDue".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getAmountDue(); 
     }else if("taxCode".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getTaxCode();   
     }else if("conversionRate".equals(fieldName)){
        value = ((GlRepGeneralLedgerData)data.get(index)).getConversionRate();    
     }else if("showEntries".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getShowEntries(); 
  
     }else if("companyName".equals(fieldName)){
         value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyName();
	  }else if("companyTin".equals(fieldName)){
	      value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyTin().replace("-", "");
	  }else if("companyAddress".equals(fieldName)){
	      value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyAddress();    
	  }else if("companyCity".equals(fieldName)){
	      value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyCity();
	  }else if("companyRevenueOffice".equals(fieldName)){
	      value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyRevenueOffice();  
	  }else if("companyFiscalYearEnding".equals(fieldName)){
	      value = ((GlRepGeneralLedgerData)data.get(index)).getCompanyFiscalYearEnding();  
	  }else{

		 
		//IIT Category 
		  for(int x=0;x<iitNames.length;x++){
     		 
     		 String iitNameVal = iitNames[x];
     		
     		 if(fieldName.equals(iitNameVal)){
     			 Double valueData = iitList.getIIT(iitNameVal);
     			 value = valueData >= 0 ? valueData : valueData * -1;
     		 }
     	 }
		  
		
		  
     	 
     	 //CIT Category
     	 for (int x=0;x<citNames.length;x++){
     		 
     		String citNameVal = citNames[x];
     		 if(fieldName.equals(citNameVal)){
     			 Double valueData = citList.getCIT(citNameVal);
     			 value = valueData >= 0 ? valueData : valueData * -1;
     		 }
     	 }
     	 
     	 //SAW Category
     	 for (int x=0;x<sawNames.length;x++){
     		 
     		String sawNameVal = sawNames[x];
     		 if(fieldName.equals(sawNameVal)){
     			 Double valueData = sawList.getSAW(sawNameVal);
     			 value = valueData >= 0 ? valueData : valueData * -1;
     		 }
     	 }
     	 
     	 
	  }

      return(value);
   }
}
