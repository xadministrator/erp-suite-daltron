package com.struts.jreports.gl.generalledger;

import java.util.Date;

public class GlRepGeneralLedgerData implements java.io.Serializable{
	   private String account = null;
	   private String citCategory = null;
	   private String sawCategory = null;
	   private String iitCategory = null;
	   private String accountDescription = null;
	   private Double beginningBalance = 	null;
	   private String date = null;
	   private Date date2 = null;
	   private String name = null;
	   private String description = null;
	   private String source = null;
	   private String tin = null;
	   private String documentNumber = null;
	   private String checkNumber = null;
	   private Double debit = null;
	   private Double credit = null;
	   private Double journalAmount = null;
	   private String accountType = null;
	   private Double balance = null;
	   private String posted = null;
	   private String subLedger = null;
	   private String referenceNumber = null;
	   private String dateCreated = null;
	   private String vatTin = null;
	   private String vatSubLedgerName = null;
	   private String vatAddress1 = null;
	   private String vatAddress2 = null;
	   private Double vatNet = null;
	   private Double amountDue = null;
	   private String taxCode = null;
	   private Double conversionRate = null;
	   private Double totalDebit = null;
	   private Double totalCredit = null;
	   private String companyName = null;
	   private String companyTin = null;
	   private String companyAddress = null;
	   private String companyCity = null;
	   private String companyRevenueOffice = null;
	   private String companyFiscalYearEnding = null;
	   private boolean showEntries = false;
	   
	   private CorporateIncomeTaxList citList;
	   private IndividualIncomeTaxList iitList;
   
	   public GlRepGeneralLedgerData(String account, String citCategory, String sawCategory, String iitCategory, String accountDescription, Double beginningBalance,
	      String date, Date date2, String name, String description, String source, 
	      String tin, String documentNumber, String checkNumber, Double debit, Double credit, Double journalAmount, String accountType, Double balance, 
		  String posted, String subLedger, String referenceNumber, String dateCreated, 
		  String vatTin, String vatSubLedgerName, String vatAddress1, String vatAddress2, Double vatNet, 
		  Double amountDue, String taxCode,
		  Double conversionRate, 
		  boolean showEntries,String companyName, String companyTin, String companyAddress, String companyCity, String companyRevenueOffice, String companyFiscalYearEnding,
		  CorporateIncomeTaxList citList, IndividualIncomeTaxList iitList
		  ){
		   
	      this.account = account;
	      this.citCategory = citCategory;
	      this.sawCategory = sawCategory;
	      this.iitCategory = iitCategory;
	      this.accountDescription = accountDescription;
	      this.beginningBalance = beginningBalance;
	      this.date = date;
	      this.date2 = date2;
	      this.name = name;
	      this.description = description;
	      this.source = source;
	      this.tin = tin;
	      this.documentNumber = documentNumber;
	      this.checkNumber = checkNumber;
	      this.debit = debit;
	      this.credit = credit;
	      this.journalAmount = journalAmount;
	      this.accountType = accountType;
	      this.balance = balance;
	      this.posted = posted;
	      this.subLedger = subLedger;
	      this.referenceNumber = referenceNumber;
	      this.dateCreated = dateCreated;
	      this.vatTin = vatTin;
	      this.vatSubLedgerName = vatSubLedgerName;
	      this.vatAddress1 = vatAddress1;
	      this.vatAddress2 = vatAddress2;
	      this.vatNet = vatNet;
	      this.amountDue = amountDue;
	      this.taxCode = taxCode;
	      this.conversionRate = conversionRate;
	      this.showEntries = showEntries;
	      this.citList = citList;
	      this.iitList = iitList;
	      
	     
	   }
	   
	   
	   
	   
	   
	   
	   public String getAccount() {
	   	
	   	  return account;
	   	
	   }
	   
	   public String getCitCategory() {
		   	
		   	  return citCategory;
		   	
		   }
	   
	   public String getSawCategory() {
		   	
		   	  return sawCategory;
		   	
		   }
	   
	   public String getIitCategory1() {
		   	
		   	  return iitCategory;
		   	
		   }
	   
	   public String getAccountDescription() {
	   	
	   	  return accountDescription;
	   	
	   }
	   
	   public Double getBeginningBalance() {
	   	
	   	  return beginningBalance;
	   	
	   }
	   
	   public String getDate() {
	   	
	   	  return date;
	   	
	   }
	   
	   public Date getDate2() {
		   	
	   	  return date2;
	   	
	   }
	   
	   public String getName() {
	   	
	   	  return name;
	   	
	   }
	   
	   public String getDescription() {
	   	
	   	  return description;
	   	
	   }
	   
	   public String getSource() {
	   	
	   	  return source;
	   	
	   }
	   
	   public String getTin() {
	   	
	   	  return tin;
	   	
	   }

	   public String getDocumentNumber() {

	      return documentNumber;

	   }
	   
	   public String getCheckNumber() {

	      return checkNumber;

	   }
	   
	   public Double getDebit() {
	   	
	   	  return debit;
	   	
	   }
	   
	   public Double getCredit() {
	   	
	   	  return credit;
	   	
	   }
	   
	   public Double getJournalAmount() {
		   	
	   	  return journalAmount;
	   	
	   }
	   
	   
	   public String getAccountType() {
	   	
	   	  return accountType;
	   	
	   }

	   public Double getBalance() {

	      return balance;

	   }
	   
	   public String getPosted() {
	   	
	   	  return posted;
	   	
	   }
	   
	   public String getSubLedger() {
	   	
	   	  return subLedger;
	   	
	   }
	   
	   public String getReferenceNumber() {
	   	
	   	  return referenceNumber;
	   	
	   }
	   
	   public String getDateCreated() {
	   	
	   	  return dateCreated; 
	   	
	   }
	   
	   public String getVatTin() {
		   	
	   	  return vatTin; 
	   	
	   }
	   
	   public String getVatSubLedgerName() {
		   	
	   	  return vatSubLedgerName; 
	   	
	   }
	   
	   public String getVatAddress1() {
		   	
	   	  return vatAddress1;
	   	
	   }
	   
	   public String getVatAddress2() {
		   	
	   	  return vatAddress2; 
	   	
	   }
	   
	   public Double getVatNet() {
		
		   return vatNet;		  
	   
	   }
	   
	   public Double getAmountDue() {

		   return amountDue;

	   }
	   
	   public String getTaxCode() {
		   	
	   	  return taxCode; 
	   	
	   }
	   
	   
	   public Double getConversionRate() {

		   return conversionRate;

	   }
	   
	   public boolean getShowEntries() {

		   return showEntries;

	   }
	   
	   
	   public Double getCitValue(String key){
		   return citList.getCIT(key);
	   }
	   
	   public Double getIitValue(String key){
		   return iitList.getIIT(key);
	   }
	  
	   public Double getTotalDebit() {

		   return debit;

	   }

	   public Double getTotalCredit() {

		   return credit;

	   }
	   
	   
	   public String getCompanyName() {
		   	
	   	  return companyName; 
	   	
	   }
	   
	   public String getCompanyTin() {
		   	
	   	  return companyTin;
	   	
	   }
	   
	   public String getCompanyAddress() {
		   	
	   	  return companyAddress;
	   	
	   }
	   
	   public String getCompanyCity() {
		   	
	   	  return companyCity;
	   	
	   }
	   
	   public String getCompanyRevenueOffice() {
		   	
	   	  return companyRevenueOffice;
	   	
	   }
	   
	   public String getCompanyFiscalYearEnding() {
		   	
	   	  return companyFiscalYearEnding;
	   	
	   }
	   
	   
	   public CorporateIncomeTaxList getCitList() {
			return citList;
		}

		public void setCitList(CorporateIncomeTaxList citList) {
			this.citList = citList;
		}

		public IndividualIncomeTaxList getIitList() {
			return iitList;
		}

		public void setIitList(IndividualIncomeTaxList iitList) {
			this.iitList = iitList;
		}
	   
	}
