package com.struts.jreports.gl.generalledger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepGeneralLedgerForm extends ActionForm implements Serializable{
	
	private String date = null;
	private String amountType = null;
	private ArrayList amountTypeList = new ArrayList();
	private String accountFrom = null;
	private String accountTo = null;
	private ArrayList genVsList = new ArrayList();
	private boolean showZeroesTxn = false;
	private boolean showZeroesAll = false;
	private boolean includeUnpostedTransaction = false;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private String journalSource = null;
	private ArrayList journalSourceList = new ArrayList();   
	private boolean includeUnpostedSlTransaction = false;
	private boolean vatRelief = false;
	private boolean showEntries = false;
	private ArrayList qualifierList = new ArrayList();
	private String[] qualifierSelectedList = new String[0];

	
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	
	private String userPermission = new String();
	
	private ArrayList glRepBrGeneralLedgerList = new ArrayList();
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();
	
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	

	
	private String salaryWagesMailSectionNo = null;
	private String salaryWagesMailLotNo = null;
	private String salaryWagesMailStreet = null;
	private String salaryWagesMailPoBox = null;
	private String salaryWagesMailCountry = null;
	private String salaryWagesMailProvince = null;
	private String salaryWagesMailPostOffice = null;
	private String salaryWagesMailCareOff = null;
	private String salaryWagesTaxPeriodFrom = null;
	private String salaryWagesTaxPeriodTo = null;
	private String salaryWagesPublicOfficeName = null;
	private String salaryWagesDateAppointment = null;
	
	private String salaryWagesField2 = null;
	private String salaryWagesField4 = null;
	private String salaryWagesField5 = null;
	private String salaryWagesField6 = null;


	private String corporateIncomeTaxMailSectionNo = null;
	private String corporateIncomeTaxMailLotNo = null;
	private String corporateIncomeTaxMailStreet = null;
	private String corporateIncomeTaxMailPoBox = null;
	private String corporateIncomeTaxMailCountry = null;
	private String corporateIncomeTaxMailProvince = null;
	private String corporateIncomeTaxMailPostOffice = null;
	private String corporateIncomeTaxMailCareOff = null;
	private String corporateIncomeTaxTaxPeriodFrom = null;
	private String corporateIncomeTaxTaxPeriodTo = null;
	private String corporateIncomeTaxPublicOfficeName = null;
	private String corporateIncomeTaxDateAppointment = null;
	
	
	private String individualIncomeTaxMailSectionNo = null;
	private String individualIncomeTaxMailLotNo = null;
	private String individualIncomeTaxMailStreet = null;
	private String individualIncomeTaxMailPoBox = null;
	private String individualIncomeTaxMailCountry = null;
	private String individualIncomeTaxMailProvince = null;
	private String individualIncomeTaxMailPostOffice = null;
	private String individualIncomeTaxMailCareOff = null;
	private String individualIncomeTaxTaxPeriodFrom = null;
	private String individualIncomeTaxTaxPeriodTo = null;




	
	public String getIndividualIncomeTaxMailSectionNo() {
		return individualIncomeTaxMailSectionNo;
	}

	public void setIndividualIncomeTaxMailSectionNo(
			String individualIncomeTaxMailSectionNo) {
		this.individualIncomeTaxMailSectionNo = individualIncomeTaxMailSectionNo;
	}

	public String getIndividualIncomeTaxMailLotNo() {
		return individualIncomeTaxMailLotNo;
	}

	public void setIndividualIncomeTaxMailLotNo(String individualIncomeTaxMailLotNo) {
		this.individualIncomeTaxMailLotNo = individualIncomeTaxMailLotNo;
	}

	public String getIndividualIncomeTaxMailStreet() {
		return individualIncomeTaxMailStreet;
	}

	public void setIndividualIncomeTaxMailStreet(
			String individualIncomeTaxMailStreet) {
		this.individualIncomeTaxMailStreet = individualIncomeTaxMailStreet;
	}

	public String getIndividualIncomeTaxMailPoBox() {
		return individualIncomeTaxMailPoBox;
	}

	public void setIndividualIncomeTaxMailPoBox(String individualIncomeTaxMailPoBox) {
		this.individualIncomeTaxMailPoBox = individualIncomeTaxMailPoBox;
	}

	public String getIndividualIncomeTaxMailCountry() {
		return individualIncomeTaxMailCountry;
	}

	public void setIndividualIncomeTaxMailCountry(
			String individualIncomeTaxMailCountry) {
		this.individualIncomeTaxMailCountry = individualIncomeTaxMailCountry;
	}

	public String getIndividualIncomeTaxMailProvince() {
		return individualIncomeTaxMailProvince;
	}

	public void setIndividualIncomeTaxMailProvince(
			String individualIncomeTaxMailProvince) {
		this.individualIncomeTaxMailProvince = individualIncomeTaxMailProvince;
	}

	public String getIndividualIncomeTaxMailPostOffice() {
		return individualIncomeTaxMailPostOffice;
	}

	public void setIndividualIncomeTaxMailPostOffice(
			String individualIncomeTaxMailPostOffice) {
		this.individualIncomeTaxMailPostOffice = individualIncomeTaxMailPostOffice;
	}

	public String getIndividualIncomeTaxMailCareOff() {
		return individualIncomeTaxMailCareOff;
	}

	public void setIndividualIncomeTaxMailCareOff(
			String individualIncomeTaxMailCareOff) {
		this.individualIncomeTaxMailCareOff = individualIncomeTaxMailCareOff;
	}

	public String getIndividualIncomeTaxTaxPeriodFrom() {
		return individualIncomeTaxTaxPeriodFrom;
	}

	public void setIndividualIncomeTaxTaxPeriodFrom(
			String individualIncomeTaxTaxPeriodFrom) {
		this.individualIncomeTaxTaxPeriodFrom = individualIncomeTaxTaxPeriodFrom;
	}

	public String getIndividualIncomeTaxTaxPeriodTo() {
		return individualIncomeTaxTaxPeriodTo;
	}

	public void setIndividualIncomeTaxTaxPeriodTo(
			String individualIncomeTaxTaxPeriodTo) {
		this.individualIncomeTaxTaxPeriodTo = individualIncomeTaxTaxPeriodTo;
	}

	public String getDate() {
		return(date);
	}
	
	public void setDate(String date){
		this.date = date;
	}
	
	public String getAmountType(){
		return(amountType);
	}
	
	public void setAmountType(String amountType){
		this.amountType = amountType;
	}
	
	public ArrayList getAmountTypeList() {
		
		return(amountTypeList);
		
	}
	
	public boolean getShowZeroesTxn() {
		
		return showZeroesTxn;
		
	}
	
	public void setShowZeroesTxn(boolean showZeroesTxn) {
		
		this.showZeroesTxn = showZeroesTxn;
		
	}
	
	
	public boolean getShowZeroesAll() {
		
		return showZeroesAll;
		
	}
	
	public void setShowZeroesAll(boolean showZeroesAll) {
		
		this.showZeroesAll = showZeroesAll;
		
	}
	
	public boolean getIncludeUnpostedTransaction() {
		
		return includeUnpostedTransaction;
		
	}
	
	public void setIncludeUnpostedTransaction(boolean includeUnpostedTransaction) {
		
		this.includeUnpostedTransaction = includeUnpostedTransaction;
		
	}

	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getAccountFrom(){
		return(accountFrom);
	}
	
	public void setAccountFrom(String accountFrom){
		this.accountFrom = accountFrom;
	}
	
	public String getAccountTo(){
		return(accountTo);
	}
	
	public void setAccountTo(String accountTo){
		this.accountTo = accountTo;
	}
	
	public ArrayList getGenVsList() {
		
		return genVsList;
		
	}
	
	public void saveGenVsList(Object genValueSet) {
		
		genVsList.add(genValueSet);
	}
	
	public void clearGenVsList() {
		
		genVsList.clear();
		
	}
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}
	
	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public String getJournalSource() {
		
		return journalSource;
		
	}
	
	public void setJournalSource(String journalSource) {
		
		this.journalSource = journalSource;
		
	}
	
	public ArrayList getJournalSourceList() {
		
		return journalSourceList;
		
	}
	
	public void setJournalSourceList(String journalSource) {
		
		journalSourceList.add(journalSource);
		
	}
	
	public void clearJournalSourceList() {
		
		journalSourceList.clear();
		journalSourceList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public Object[] getGlRepBrGeneralLedgerList(){
		
		return glRepBrGeneralLedgerList.toArray();
		
	}
	
	public GlRepBranchGeneralLedgerList getGlRepBrGeneralLedgerListByIndex(int index){
		
		return ((GlRepBranchGeneralLedgerList)glRepBrGeneralLedgerList.get(index));
		
	}
	
	public int getGlRepBrGeneralLedgerListSize(){
		
		return(glRepBrGeneralLedgerList.size());
		
	}
	
	public void saveGlRepBrGeneralLedgerList(Object newGlRepBrGeneralLedgerList){
		
		glRepBrGeneralLedgerList.add(newGlRepBrGeneralLedgerList);   	  
		
	}
	
	public void clearGlRepBrGeneralLedgerList(){
		
		glRepBrGeneralLedgerList.clear();
		
	}
	
	public void setGlRepBrGeneralLedgerList(ArrayList glRepBrGeneralLedgerList) {
		
		this.glRepBrGeneralLedgerList = glRepBrGeneralLedgerList;
		
	}
	
	public boolean getIncludeUnpostedSlTransaction() {
		
		return includeUnpostedSlTransaction;
		
	}
	
	public void setIncludeUnpostedSlTransaction(boolean includeUnpostedSlTransaction) {
		
		this.includeUnpostedSlTransaction = includeUnpostedSlTransaction;
		
	}
	
	public boolean getVatRelief() {
		
		return vatRelief;
		
	}
	
	public void setVatRelief(boolean vatRelief) {
		
		this.vatRelief = vatRelief;
		
	}
	
	
	public boolean getShowEntries() {

		return showEntries;

	}

	public void setShowEntries(boolean showEntries) {

		this.showEntries = showEntries;

	}
	
	
	public ArrayList getQualifierList() {
	   	
	      return qualifierList;
	      
	   }

	   public void setQualifierList(String qualifier) {
	   	
	      qualifierList.add(qualifier);
	      
	   }
	   
	   public void clearQualifierList() {
	   	
		   qualifierList.clear();
		   qualifierList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   
	   public String[] getQualifierSelectedList() {

			return qualifierSelectedList;

		}

	public void setQualifierSelectedList(String[] qualifierSelectedList) {

		this.qualifierSelectedList = qualifierSelectedList;

	}

	
	public String getCurrency() {
	   	
   	   	return currency;
   	
   	}
   
   	public void setCurrency(String currency) {
   	
   	   	this.currency = currency;	
   	
   	}
   
   	public ArrayList getCurrencyList() {
   	
   	   	return currencyList;
   	
   	}
   
   	public void setCurrencyList(String currency) {
   	
	   	currencyList.add(currency);
   	
   	}	
   
   	public void clearCurrencyList() {
   	
   		currencyList.clear();
   	
   	}
   	
   	
   	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	
	public String getOrderBy() {

		return(orderBy);

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}
	
	
	
	public String getSalaryWagesMailSectionNo(){ return(salaryWagesMailSectionNo);}	public void setSalaryWagesMailSectionNo(String salaryWagesMailSectionNo){ this.salaryWagesMailSectionNo=salaryWagesMailSectionNo;}
	public String getSalaryWagesMailLotNo(){ return(salaryWagesMailLotNo);}	public void setSalaryWagesMailLotNo(String salaryWagesMailLotNo){ this.salaryWagesMailLotNo=salaryWagesMailLotNo;}
	public String getSalaryWagesMailStreet(){ return(salaryWagesMailStreet);}	public void setSalaryWagesMailStreet(String salaryWagesMailStreet){ this.salaryWagesMailStreet=salaryWagesMailStreet;}
	public String getSalaryWagesMailPoBox(){ return(salaryWagesMailPoBox);}	public void setSalaryWagesMailPoBox(String salaryWagesMailPoBox){ this.salaryWagesMailPoBox=salaryWagesMailPoBox;}
	public String getSalaryWagesMailCountry(){ return(salaryWagesMailCountry);}	public void setSalaryWagesMailCountry(String salaryWagesMailCountry){ this.salaryWagesMailCountry=salaryWagesMailCountry;}
	public String getSalaryWagesMailProvince(){ return(salaryWagesMailProvince);}	public void setSalaryWagesMailProvince(String salaryWagesMailProvince){ this.salaryWagesMailProvince=salaryWagesMailProvince;}
	public String getSalaryWagesMailPostOffice(){ return(salaryWagesMailPostOffice);}	public void setSalaryWagesMailPostOffice(String salaryWagesMailPostOffice){ this.salaryWagesMailPostOffice=salaryWagesMailPostOffice;}
	public String getSalaryWagesMailCareOff(){ return(salaryWagesMailCareOff);}	public void setSalaryWagesMailCareOff(String salaryWagesMailCareOff){ this.salaryWagesMailCareOff=salaryWagesMailCareOff;}
	public String getSalaryWagesTaxPeriodFrom(){ return(salaryWagesTaxPeriodFrom);}	public void setSalaryWagesTaxPeriodFrom(String salaryWagesTaxPeriodFrom){ this.salaryWagesTaxPeriodFrom=salaryWagesTaxPeriodFrom;}
	public String getSalaryWagesTaxPeriodTo(){ return(salaryWagesTaxPeriodTo);}	public void setSalaryWagesTaxPeriodTo(String salaryWagesTaxPeriodTo){ this.salaryWagesTaxPeriodTo=salaryWagesTaxPeriodTo;}
	public String getSalaryWagesPublicOfficeName(){ return(salaryWagesPublicOfficeName);}	public void setSalaryWagesPublicOfficeName(String salaryWagesPublicOfficeName){ this.salaryWagesPublicOfficeName=salaryWagesPublicOfficeName;}
	public String getSalaryWagesDateAppointment(){ return(salaryWagesDateAppointment);}	public void setSalaryWagesDateAppointment(String salaryWagesDateAppointment){ this.salaryWagesDateAppointment=salaryWagesDateAppointment;}
	
	public String getSalaryWagesField2(){ return(salaryWagesField2);}	public void setSalaryWagesField2(String salaryWagesField2){ this.salaryWagesField2=salaryWagesField2;}
	public String getSalaryWagesField4(){ return(salaryWagesField4);}	public void setSalaryWagesField4(String salaryWagesField4){ this.salaryWagesField4=salaryWagesField4;}
	public String getSalaryWagesField5(){ return(salaryWagesField5);}	public void setSalaryWagesField5(String salaryWagesField5){ this.salaryWagesField5=salaryWagesField5;}
	public String getSalaryWagesField6(){ return(salaryWagesField6);}	public void setSalaryWagesField6(String salaryWagesField6){ this.salaryWagesField6=salaryWagesField6;}


	public String getCorporateIncomeTaxMailSectionNo(){ return(corporateIncomeTaxMailSectionNo);}	public void setCorporateIncomeTaxMailSectionNo(String corporateIncomeTaxMailSectionNo){ this.corporateIncomeTaxMailSectionNo=corporateIncomeTaxMailSectionNo;}
	public String getCorporateIncomeTaxMailLotNo(){ return(corporateIncomeTaxMailLotNo);}	public void setCorporateIncomeTaxMailLotNo(String corporateIncomeTaxMailLotNo){ this.corporateIncomeTaxMailLotNo=corporateIncomeTaxMailLotNo;}
	public String getCorporateIncomeTaxMailStreet(){ return(corporateIncomeTaxMailStreet);}	public void setCorporateIncomeTaxMailStreet(String corporateIncomeTaxMailStreet){ this.corporateIncomeTaxMailStreet=corporateIncomeTaxMailStreet;}
	public String getCorporateIncomeTaxMailPoBox(){ return(corporateIncomeTaxMailPoBox);}	public void setCorporateIncomeTaxMailPoBox(String corporateIncomeTaxMailPoBox){ this.corporateIncomeTaxMailPoBox=corporateIncomeTaxMailPoBox;}
	public String getCorporateIncomeTaxMailCountry(){ return(corporateIncomeTaxMailCountry);}	public void setCorporateIncomeTaxMailCountry(String corporateIncomeTaxMailCountry){ this.corporateIncomeTaxMailCountry=corporateIncomeTaxMailCountry;}
	public String getCorporateIncomeTaxMailProvince(){ return(corporateIncomeTaxMailProvince);}	public void setCorporateIncomeTaxMailProvince(String corporateIncomeTaxMailProvince){ this.corporateIncomeTaxMailProvince=corporateIncomeTaxMailProvince;}
	public String getCorporateIncomeTaxMailPostOffice(){ return(corporateIncomeTaxMailPostOffice);}	public void setCorporateIncomeTaxMailPostOffice(String corporateIncomeTaxMailPostOffice){ this.corporateIncomeTaxMailPostOffice=corporateIncomeTaxMailPostOffice;}
	public String getCorporateIncomeTaxMailCareOff(){ return(corporateIncomeTaxMailCareOff);}	public void setCorporateIncomeTaxMailCareOff(String corporateIncomeTaxMailCareOff){ this.corporateIncomeTaxMailCareOff=corporateIncomeTaxMailCareOff;}
	public String getCorporateIncomeTaxTaxPeriodFrom(){ return(corporateIncomeTaxTaxPeriodFrom);}	public void setCorporateIncomeTaxTaxPeriodFrom(String corporateIncomeTaxTaxPeriodFrom){ this.corporateIncomeTaxTaxPeriodFrom=corporateIncomeTaxTaxPeriodFrom;}
	public String getCorporateIncomeTaxTaxPeriodTo(){ return(corporateIncomeTaxTaxPeriodTo);}	public void setCorporateIncomeTaxTaxPeriodTo(String corporateIncomeTaxTaxPeriodTo){ this.corporateIncomeTaxTaxPeriodTo=corporateIncomeTaxTaxPeriodTo;}
	public String getCorporateIncomeTaxPublicOfficeName(){ return(corporateIncomeTaxPublicOfficeName);}	public void setCorporateIncomeTaxPublicOfficeName(String corporateIncomeTaxPublicOfficeName){ this.corporateIncomeTaxPublicOfficeName=corporateIncomeTaxPublicOfficeName;}
	public String getCorporateIncomeTaxDateAppointment(){ return(corporateIncomeTaxDateAppointment);}	public void setCorporateIncomeTaxDateAppointment(String corporateIncomeTaxDateAppointment){ this.corporateIncomeTaxDateAppointment=corporateIncomeTaxDateAppointment;}

	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<glRepBrGeneralLedgerList.size(); i++) {
			
			GlRepBranchGeneralLedgerList list = (GlRepBranchGeneralLedgerList)glRepBrGeneralLedgerList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		date = Common.convertSQLDateToString(new Date());
		
		Iterator i = genVsList.iterator();
		
		while (i.hasNext()) {
			
			GlRepGeneralLedgerVsList list = (GlRepGeneralLedgerVsList)i.next();
			
			list.setValueSetValueDescription(null);
			
		}
		amountTypeList.clear();
		amountTypeList.add("PTD");
		amountTypeList.add("QTD");
		amountTypeList.add("YTD");
		amountType = "PTD";
		
		goButton = null;
		closeButton = null;
		showZeroesTxn = false;
		showZeroesAll = false;
		includeUnpostedTransaction = false;
		includeUnpostedSlTransaction = false;
		journalSource = Constants.GLOBAL_BLANK;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_CSV);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		vatRelief = false;
		
		reportTypeList.add("RELIEF IMPORTATION");
		reportTypeList.add("RELIEF PURCHASES");
		reportTypeList.add("RELIEF SALES");
		
		orderByList.clear();
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add("DATE");
		orderByList.add("CHECK NUMBER");
		orderByList.add("DOCUMENT NUMBER");
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("goButton") != null){
			if(Common.validateRequired(accountFrom)){
				errors.add("accountFrom", new ActionMessage("generalLedger.error.accountFromRequired"));
			}
			if(Common.validateRequired(accountTo)){
				errors.add("accountTo", new ActionMessage("generalLedger.error.accountToRequired"));
			}
			
			if(Common.validateRequired(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateRequired"));
			}
			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateInvalid"));
			}
			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateInvalid"));
			}
			
		}
		
		return(errors);
	}
}
