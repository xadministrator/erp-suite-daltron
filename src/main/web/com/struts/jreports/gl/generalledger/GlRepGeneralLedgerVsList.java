package com.struts.jreports.gl.generalledger;

import java.io.Serializable;

public class GlRepGeneralLedgerVsList implements Serializable {

    private String valueSetName = null;
    private String valueSetValueDescription = null;
		
	private GlRepGeneralLedgerForm parentBean;

	public GlRepGeneralLedgerVsList(GlRepGeneralLedgerForm parentBean, String valueSetName,
	   String valueSetValueDescription){
	   this.parentBean = parentBean;
	   this.valueSetName = valueSetName;
	   this.valueSetValueDescription = valueSetValueDescription;
	}

	public void setParentBean(GlRepGeneralLedgerForm parentBean){
	   this.parentBean = parentBean;
	}

	public String getValueSetName() {
		
		return valueSetName;		
		
	}
	
	public void setValueSetName(String valueSetName) {
		
		this.valueSetName = valueSetName;
		
	}
	
	public String getValueSetValueDescription() {
		
		return valueSetValueDescription;
		
	}
	
	public void setValueSetValueDescription(String valueSetValueDescription) {
		
		this.valueSetValueDescription = valueSetValueDescription;
		
	}

}
