package com.struts.jreports.gl.generalledger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.util.GlRepGeneralLedgerDetails;

public class SalaryAndWagesList {


	private Map<String, Double> map = new HashMap<String, Double>();
	

	
	public SalaryAndWagesList(){
		GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
		String[] varList = details.getSAWNameList();
		for(int x=0;x<varList.length;x++){
			map.put(varList[x], 0d);
		}
	}
	
	

	public Double getSAW(String key){
		return map.get(key);
	}
	
	public void setSAW(String key, Double value){
		map.put(key, value);
	}
	
	public void SubtractValue(String key, Double value){
		map.put(key, map.get(key) - value);
	}
	
	public void AddValue(String key, Double value){
		map.put(key, map.get(key) + value);
	}
	
	

	
	
}
