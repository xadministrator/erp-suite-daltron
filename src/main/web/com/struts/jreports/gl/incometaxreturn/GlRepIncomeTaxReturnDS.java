/*
 * opt/jreport/GlRepMonthlyVatDeclarationDS.java
 *
 * Created on March 29, 2004, 11:30 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.gl.incometaxreturn;

import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepIncomeTaxReturnDetails;
import com.util.GlRepMonthlyVatDeclarationDetails;


public class GlRepIncomeTaxReturnDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepIncomeTaxReturnDS(GlRepIncomeTaxReturnDetails mdetails) {

	GlRepIncomeTaxReturnDetails details = new GlRepIncomeTaxReturnDetails();
	GlRepIncomeTaxReturnData glRepMVDData = new GlRepIncomeTaxReturnData(
	new Double(mdetails.getGrossSalesReceipt()), new Double(mdetails.getTaxDue()),
	new Double(mdetails.getOnTaxableGoods()),
	new Double(mdetails.getOutputTax()), new Double(mdetails.getInputTax()), new Double(mdetails.getSalesAmount()),
	new Double(mdetails.getRevenue()),
	new Double(mdetails.getSalesAmountExempt()),
	new Double(mdetails.getSaleAmountZeroRated()),
	new Double(mdetails.getTotalSalesExemptZero()),
	new Double(mdetails.getRevenueAmountExempt()),
	new Double(mdetails.getRevenueAmountZeroRated()),
	new Double(mdetails.getTotalRevenueExemptZero())
	);

	data.add(glRepMVDData);

   }

   public boolean next() throws JRException {

      index++;
      return (index < data.size());

   }

   public Object getFieldValue(JRField field) throws JRException {

      Object value = null;

      String fieldName = field.getName();

      if("grossSalesReceipt".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getGrossSalesReceipt();

      } else if("taxDue".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getTaxDue();

      } else if("onTaxableGoodsServices".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getOnTaxableGoodsServices();
      } else if("outputTax".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getOutputTax();
      } else if("inputTax".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getInputTax();

      } else if("salesAmount".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getSalesAmount();

      } else if("revenue".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getRevenue();

      } else if("salesAmountExempt".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getSalesAmountExempt();

      } else if("salesAmountZeroRated".equals(fieldName)) {

          value = ((GlRepIncomeTaxReturnData)data.get(index)).getSalesAmountZeroRated();

      } else if("totalSalesExemptZero".equals(fieldName)) {

	       value = ((GlRepIncomeTaxReturnData)data.get(index)).getTotalSalesExemptZero();

	  } else if("revenueAmountExempt".equals(fieldName)) {

		       value = ((GlRepIncomeTaxReturnData)data.get(index)).getRevenueAmountExempt();

	  } else if("revenueAmountZeroRated".equals(fieldName)) {

	       value = ((GlRepIncomeTaxReturnData)data.get(index)).getRevenueAmountZeroRated();

	  } else if("totalRevenueExemptZero".equals(fieldName)) {

	       value = ((GlRepIncomeTaxReturnData)data.get(index)).getTotalRevenueExemptZero();

	  }
      return(value);
   }
}
