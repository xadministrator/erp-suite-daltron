package com.struts.jreports.gl.incometaxreturn;

import java.io.Serializable;


/**
 *
 * @author Ruben P. Lamberte
 * Created: 9/15/2018 11:43 AM
 *
 */
public class GlRepIncomeTaxReturnList implements Serializable {


	private Integer rvCode = null;
    private String lineNumber = null;
	private String rvType = null;
	private String rvParameter = null;
	private String rvDescription = null;
	private String rvDefaultValue = null;
	private String accountNumber = null;
	private String accountNumberDescription = null;

    private String deleteButton = null;
    private String isCoaAccountEntered = null;
	private GlRepIncomeTaxReturnForm parentBean;



	public GlRepIncomeTaxReturnList(GlRepIncomeTaxReturnForm parentBean, Integer rvCode, String lineNumber , String rvType, String rvParameter, String rvDescription, String rvDefaultValue,
			String accountNumber, String accountNumberDescription ) {
		this.parentBean = parentBean;
		this.rvCode = rvCode;
		this.lineNumber = lineNumber;
		this.rvType = rvType;
		this.rvParameter = rvParameter;
		this.rvDescription = rvDescription;
		this.rvDefaultValue = rvDefaultValue;
		this.accountNumber = accountNumber;
		this.accountNumberDescription = accountNumberDescription;

	}


	public Integer getRvCode() {
		return rvCode;
	}

	public void setRvCode(Integer rvCode) {
		this.rvCode = rvCode;
	}

	public String getLineNumber(){
        return lineNumber;
    }

    public void setLineNumber(String lineNumber){
        this.lineNumber = lineNumber;
    }





	public String getRvType() {
		return rvType;
	}

	public void setRvType(String rvType) {
		this.rvType = rvType;
	}

	public String getRvParameter() {
		return rvParameter;
	}

	public void setRvParameter(String rvParameter) {
		this.rvParameter = rvParameter;
	}

	public String getRvDescription() {
		return rvDescription;
	}

	public void setRvDescription(String rvDescription) {
		this.rvDescription = rvDescription;
	}

	public String getRvDefaultValue() {
		return rvDefaultValue;
	}

	public void setRvDefaultValue(String rvDefaultValue) {
		this.rvDefaultValue = rvDefaultValue;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNumberDescription() {
		return accountNumberDescription;
	}

	public void setAccountNumberDescription(String accountNumberDescription) {
		this.accountNumberDescription = accountNumberDescription;
	}

	public void setDeleteButton(String deleteButton) {

        parentBean.setRowSelected(this, true);

    }
	public String getIsCoaAccountEntered() {

   	   return isCoaAccountEntered;

    }

    public void setIsMemoLineEntered(String isCoaAccountEntered) {
        if (isCoaAccountEntered != null && isCoaAccountEntered.equals("true")) {

            parentBean.setRowSelected(this, false);

        }
    }


}

