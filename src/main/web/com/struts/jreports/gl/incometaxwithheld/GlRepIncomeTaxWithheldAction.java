package com.struts.jreports.gl.incometaxwithheld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlRepIncomeTaxWithheldController;
import com.ejb.txn.GlRepIncomeTaxWithheldControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.GlRepIncomeTaxWithheldDetails;

public final class GlRepIncomeTaxWithheldAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRepIncomeTaxWithheldAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         GlRepIncomeTaxWithheldForm actionForm = (GlRepIncomeTaxWithheldForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_INCOME_TAX_WITHHELD_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepIncomeTaxWithheld");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepIncomeTaxWithheldController EJB
*******************************************************/

         GlRepIncomeTaxWithheldControllerHome homeITW = null;
         GlRepIncomeTaxWithheldController ejbITW = null;

         try {

            homeITW = (GlRepIncomeTaxWithheldControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepIncomeTaxWithheldControllerEJB", GlRepIncomeTaxWithheldControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in GlRepIncomeTaxWithheldAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbITW = homeITW.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in GlRepIncomeTaxWithheldAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- AP ITW Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            	ArrayList glRepITWList = null;

		    try {

		       glRepITWList = ejbITW.executeGlRepIncomeTaxWithheld(
		       	 Common.convertStringToSQLDate(actionForm.getDateFrom()),
		       	 Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
			    log.info("EJBException caught in GlRepIncomeTaxWithheldAction.execute(): " + ex.getMessage() +
			    " session: " + session.getId());

			}

			return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepIncomeTaxWithheld"));

		    }

		    // get for the month

		    String forTheMonth = actionForm.getForTheMonth();
		    String month = null;
		    String year = null;
		    String monthYear = null;

		    StringTokenizer st = new StringTokenizer(forTheMonth, "/");

	        month = st.nextToken();

	        if (month != null && month.length() == 2) {

	    	    month = " " + month.substring(0, 1) + "  " + month.substring(1, 2);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.forTheMonthInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

	        year = st.nextToken();

	        if (year != null && year.length() == 4) {

	            year = year.substring(0, 1) + "  " + year.substring(1, 2) + "   " + year.substring(2, 3) + "  " + year.substring(3, 4);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.forTheMonthInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

	        monthYear = month + "   " + year;

	        // get tin number

	        String tinNumber = actionForm.getTinNumber();

	        if (tinNumber != null && tinNumber.length() == 12) {

		        tinNumber = " " + tinNumber.substring(0, 1) + "  " + tinNumber.substring(1, 2) + "   " + tinNumber.substring(2, 3) + "       " + tinNumber.substring(3, 4)
		                   + "  " + tinNumber.substring(4, 5) + "  " + tinNumber.substring(5, 6) + "        " + tinNumber.substring(6, 7)
		                   + "  " + tinNumber.substring(7, 8) + "  " + tinNumber.substring(8, 9) + "        " + tinNumber.substring(9, 10)
		                   + "  " + tinNumber.substring(10, 11) + "   " + tinNumber.substring(11, 12);

		    } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.tinNumberInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

		    }

	        // get telephone number

	        String telephoneNumber = actionForm.getTelephoneNumber();

	        if (telephoneNumber != null && telephoneNumber.length() == 7) {

		        telephoneNumber = " " + telephoneNumber.substring(0, 1) + "  " + telephoneNumber.substring(1, 2) + "  " + telephoneNumber.substring(2, 3)
		                         + "  " + telephoneNumber.substring(3, 4) + " " + telephoneNumber.substring(4, 5) + "  " + telephoneNumber.substring(5, 6)
		                         + "  " + telephoneNumber.substring(6, 7);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.telephoneNumberInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

	        // get zip code

	        String zipCode = actionForm.getZipCode();

	        if (zipCode != null && zipCode.length() == 4) {

	        	zipCode = " " + zipCode.substring(0, 1) + "   " + zipCode.substring(1, 2) + "    " + zipCode.substring(2, 3) + "   " + zipCode.substring(3, 4);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.zipCodeInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

	        // get rdo code

	        String rdoCode = actionForm.getRdoCode();

	        if (rdoCode != null && rdoCode.length() == 3) {

	        	rdoCode = " " + rdoCode.substring(0, 1) + "  " + rdoCode.substring(1, 2) + "  " + rdoCode.substring(2, 3);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.rdoCodeInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

	        // get no of sheet attached

	        String noOfSheetAttached = actionForm.getNoOfSheetAttached();

	        if (noOfSheetAttached != null && noOfSheetAttached.length() == 2) {

	        	noOfSheetAttached = " " + noOfSheetAttached.substring(0, 1) + "    " + noOfSheetAttached.substring(1, 2);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("incomeTaxWithheld.error.noOfSheetAttachedInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepIncomeTaxWithheld"));

	        }

		    /*** fill report parameters, fill report to pdf and set report session **/

		    Map parameters = new HashMap();
		    parameters.put("BirDir", servlet.getServletContext().getRealPath("images/bir.gif"));
		    parameters.put("ArrowDir", servlet.getServletContext().getRealPath("images/arrow.gif"));
		    parameters.put("forTheMonth", monthYear);
		    parameters.put("forTheYear", year);
		    parameters.put("tinNumber", tinNumber);
		    parameters.put("rdoCode", rdoCode);
		    parameters.put("lineOfBusiness", actionForm.getLineOfBusiness());
		    parameters.put("withholdingAgentName", actionForm.getAgentName());
		    parameters.put("telephoneNumber", telephoneNumber);
		    parameters.put("registeredAddress", actionForm.getRegisteredAddress());
		    parameters.put("zipCode", zipCode);
		    parameters.put("reliefDescription", actionForm.getReliefDescription());
		    parameters.put("noOfSheetAttached", noOfSheetAttached);

		    // for computation details

		    parameters.put("taxRemitted", new Double(Common.convertStringMoneyToDouble(actionForm.getTaxRemitted(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("surcharge", new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("interest", new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("compromise", new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION)));




		    // for payment details

		    parameters.put("checkBank", actionForm.getCheckBank());
		    parameters.put("otherBank", actionForm.getOtherBank());
		    parameters.put("checkNumber", actionForm.getCheckNumber());
		    parameters.put("otherNumber", actionForm.getOtherNumber());

		    String checkDate = actionForm.getCheckDate();
		    String mnth = null;
		    String day = null;
		    String yr = null;

		    if (!checkDate.equals(null) && checkDate.length() != 0 ) {

			    st = new StringTokenizer(checkDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = " " + mnth.substring(0, 1) + "  " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = day.substring(0, 1) + "   " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "  " + yr.substring(1, 2) + "  " + yr.substring(2,3) +
					     "  " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        checkDate = mnth + "   " + day + "   " + yr;
		    }

		    parameters.put("checkDate", checkDate);

		    String otherDate = actionForm.getOtherDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!otherDate.equals(null) && otherDate.length() != 0 ) {

			    st = new StringTokenizer(otherDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = " " + mnth.substring(0, 1) + "  " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = day.substring(0, 1) + "   " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "  " + yr.substring(1, 2) + "  " + yr.substring(2,3) +
				     "  " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("incomeTaxWithheld.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepIncomeTaxWithheld"));

		        }

		        otherDate = mnth + "   " + day + "   " + yr;

		    }

		    parameters.put("otherDate", otherDate);
		    parameters.put("cashBankAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCashBankAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("checkAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCheckAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("otherAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getOtherAmount(), Constants.MONEY_RATE_PRECISION)));

		    // for amended

		    if (actionForm.getAmended().equals("YES")) {

		        parameters.put("amendedYes", "X");

		    } else if (actionForm.getAmended().equals("NO")) {

		        parameters.put("amendedNo", "X");

		    } else {

		    	parameters.put("amendedYes", "");
		    	parameters.put("amendedNo", "");

		    }

		    // for withheld

		    if (actionForm.getWithheld().equals("YES")) {

		        parameters.put("withheldYes", "X");

		    } else if (actionForm.getWithheld().equals("NO")) {

		        parameters.put("withheldNo", "X");

		    } else {

		    	parameters.put("withheldYes", "");
		    	parameters.put("withheldNo", "");

		    }

		    // for category

		    if (actionForm.getCategory().equals("PRIVATE")) {

		        parameters.put("private", "X");

		    } else if (actionForm.getCategory().equals("GOVERNMENT")) {

		        parameters.put("government", "X");

		    } else {

		    	parameters.put("government", "");
		    	parameters.put("private", "");

		    }

		    // for relief

		    if (actionForm.getRelief().equals("YES")) {

		        parameters.put("reliefYes", "X");
		        parameters.put("reliefDescription", actionForm.getReliefDescription());

		    } else if (actionForm.getRelief().equals("NO")) {

		        parameters.put("reliefNo", "X");

		    } else {

		    	parameters.put("reliefYes", "");
		    	parameters.put("reliefNo", "");

		    }

		    // for signatories details

		    parameters.put("presidentSignature", actionForm.getPresidentSignature());
		    parameters.put("treasurerSignature", actionForm.getTreasurerSignature());
		    parameters.put("titleOfSignatory1", actionForm.getTitleOfSignatory1());
		    parameters.put("titleOfSignatory2", actionForm.getTitleOfSignatory2());
		    parameters.put("tinOfTaxAgent", actionForm.getTinOfTaxAgent());
		    parameters.put("taxAgentAccreditation", actionForm.getTaxAgentAccreditation());

		    StringBuilder strLineNumber = new StringBuilder();
		    StringBuilder strAtcCode = new StringBuilder();
		    StringBuilder strNatureOfIncomePayment = new StringBuilder();
		    StringBuilder strTaxBase = new StringBuilder();
		    StringBuilder strTaxRate = new StringBuilder();
		    StringBuilder strTaxWithheld = new StringBuilder();
		    double totalTaxWithheld = 0d;

		    for(int x=0;x<glRepITWList.size();x++) {


		    	GlRepIncomeTaxWithheldDetails glItWdetails = (GlRepIncomeTaxWithheldDetails)glRepITWList.get(x);

		    	strLineNumber.append( (x+1) + System.getProperty("line.separator"));
		    	strAtcCode.append( glItWdetails.getAtcCode() + System.getProperty("line.separator"));
		    	strNatureOfIncomePayment.append( glItWdetails.getNatureOfIncomePayment() + System.getProperty("line.separator"));
		    	strTaxBase.append( glItWdetails.getTaxBase() + System.getProperty("line.separator"));
		    	strTaxRate.append( glItWdetails.getTaxRate() + System.getProperty("line.separator"));
		    	strTaxWithheld.append( glItWdetails.getTaxRequiredWithheld() + System.getProperty("line.separator"));

		    	totalTaxWithheld+= glItWdetails.getTaxRequiredWithheld();

		    }


		    parameters.put("lineNumberList", strLineNumber.toString());
		    parameters.put("atcCodeList", strAtcCode.toString());
		    parameters.put("natureOfIncomePaymentList", strNatureOfIncomePayment.toString());
		    parameters.put("taxBaseList", strTaxBase.toString());
		    parameters.put("taxRateList", strTaxRate.toString());
		    parameters.put("taxWithheldList", strTaxWithheld.toString());
		    parameters.put("totalTaxWithheld", totalTaxWithheld);

		    //total penalties
		    double surcharge = new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION));
		    double interest= new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION));
		    double compromise = new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION));

		    double totalPenalties = surcharge + interest + compromise;
		    parameters.put("totalPenalties", totalPenalties);

		    double taxRemitted = new Double(Common.convertStringMoneyToDouble(actionForm.getTaxRemitted(), Constants.MONEY_RATE_PRECISION));
		    double taxStillDue = totalTaxWithheld -taxRemitted;

		    parameters.put("taxStillDue", taxStillDue);

		    double totalAmountStillDue = taxStillDue + totalPenalties;
		    parameters.put("totalAmountStillDue", totalAmountStillDue);

		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepIncomeTaxWithheld.jasper";

		    if(!actionForm.getReportType().equals(Constants.GLOBAL_BLANK) || !actionForm.getReportType().equals("")) {

		    	String jasperFileName = Common.getLookUpJasperFile("INCOME_TAX_WITHHELD_LOOK_UP", actionForm.getReportType(), user.getCompany());


		    	if(jasperFileName.equals("")) {
		    		jasperFileName = "GlRepIncomeTaxWithheld.jasper";
		    	}

		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/" + jasperFileName;
			    System.out.println("jasper path is: " + filename);

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/GlRepIncomeTaxWithheld.jasper");

		        }

		    }



		    if (!new java.io.File(filename).exists()) {

		       filename = servlet.getServletContext().getRealPath("jreports/GlRepIncomeTaxWithheld.jasper");

		    }


		    try {

		    	Report report = new Report();

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);

			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters,
				        new GlRepIncomeTaxWithheldDS(glRepITWList)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters,
					        new GlRepIncomeTaxWithheldDS(glRepITWList)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
				       new GlRepIncomeTaxWithheldDS(glRepITWList)));

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepIncomeTaxWithheldAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }

/*******************************************************
   -- Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AP ITW Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	             ArrayList list = null;
	        	 actionForm.clearReportTypeList();
	        	 Iterator i = null;
		        list = ejbITW.getAdLvReportTypeAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {


            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
            	} else {

            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
            		i = list.iterator();

            		while (i.hasNext()) {

            			String rprtType = (String)i.next();
            			System.out.println(rprtType);
            		    actionForm.setReportTypeList(rprtType);

            		}

            	}


	             try {

	            	AdCompanyDetails details = ejbITW.getArCmp(user.getCmpCode());

	            	String address = details.getCmpAddress() + " " + details.getCmpCity() + ", " + details.getCmpCountry();

					actionForm.setAgentName(details.getCmpName());
		            actionForm.setRegisteredAddress(address);
		            actionForm.setZipCode(details.getCmpZip());
		            actionForm.setTelephoneNumber(details.getCmpPhone());
		            actionForm.setTinNumber(details.getCmpTin());
		            actionForm.setLineOfBusiness(details.getCmpIndustry());

		        } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in AdCompanyAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

	          //  actionForm.reset(mapping, request);
	            return(mapping.findForward("glRepIncomeTaxWithheld"));

			 } else {

			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));

			    return(mapping.findForward("cmnMain"));

			 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in GlRepIncomeTaxWithheldAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
