/*
 * opt/jreport/GlRepIncomeTaxWithheldDS.java
 *
 * Created on March 29, 2004, 1:28 PM
 *
 * @author  Enrico C. Yap
 */

package com.struts.jreports.gl.incometaxwithheld;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepIncomeTaxWithheldDetails;


public class GlRepIncomeTaxWithheldDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepIncomeTaxWithheldDS(ArrayList glRepITWList) {
   	
   	if (glRepITWList.isEmpty()) {
   		
   	  	GlRepIncomeTaxWithheldData glRepITWData = new GlRepIncomeTaxWithheldData(
   	    null, null, 
   	    new Double(0d), new Double(0d),
   	    new Double(0d));
   	    
   	    data.add(glRepITWData);
   	    
   	} else {   
   	
   	  Iterator i = glRepITWList.iterator();
   	  while(i.hasNext()) {
   	  	GlRepIncomeTaxWithheldDetails details = (GlRepIncomeTaxWithheldDetails)i.next();
   	  	GlRepIncomeTaxWithheldData glRepITWData = new GlRepIncomeTaxWithheldData(
   	    details.getNatureOfIncomePayment(), details.getAtcCode(), 
   	    new Double(details.getTaxBase()), new Double(details.getTaxRate()),
   	    new Double(details.getTaxRequiredWithheld()));
   	  	
   	  	data.add(glRepITWData);
   	  	
   	  }
   	  
    }
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("natureOfIncomePayment".equals(fieldName)) {
      	
          value = ((GlRepIncomeTaxWithheldData)data.get(index)).getNatureOfIncomePayment();
      
      } else if("atcCode".equals(fieldName)) {
      
          value = ((GlRepIncomeTaxWithheldData)data.get(index)).getAtcCode();

      } else if("taxBase".equals(fieldName)) {
      
          value = ((GlRepIncomeTaxWithheldData)data.get(index)).getTaxBase();
          
      } else if("taxRate".equals(fieldName)) {
      
          value = ((GlRepIncomeTaxWithheldData)data.get(index)).getTaxRate();

      } else if("taxWithheld".equals(fieldName)) {
      
          value = ((GlRepIncomeTaxWithheldData)data.get(index)).getTaxWithheld();
          
      }

      return(value);
   }
}
