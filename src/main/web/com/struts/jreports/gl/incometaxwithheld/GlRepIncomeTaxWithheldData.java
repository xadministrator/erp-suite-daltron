/*
 * opt/jreport/GlRepIncomeTaxWithheldData.java
 *
 * Created on March 29, 2004 1:31 PM
 *
 * @author  Enrico C. Yap
 */
package com.struts.jreports.gl.incometaxwithheld; 


public class GlRepIncomeTaxWithheldData implements java.io.Serializable {

    private String natureOfIncomePayment = null;
    private String atcCode = null;
    private Double taxBase = null;
    private Double taxRate = null;
    private Double taxWithheld = null;

    public GlRepIncomeTaxWithheldData(String natureOfIncomePayment, String atcCode,
        Double taxBase, Double taxRate, Double taxWithheld) {

        this.natureOfIncomePayment = natureOfIncomePayment;
        this.atcCode = atcCode;
        this.taxBase = taxBase;
        this.taxRate = taxRate;
        this.taxWithheld = taxWithheld;

    }

    public String getNatureOfIncomePayment() {

        return natureOfIncomePayment;

    }
    
    public String getAtcCode() {
    	
    	return atcCode;
    	
    }
    
    public Double getTaxBase() {
    	
    	return taxBase;
    	
    }
    
    public Double getTaxRate() {
    	
    	return taxRate;
    	
    }
    
    public Double getTaxWithheld() {
    	
    	return taxWithheld;
    	
    }

} 