package com.struts.jreports.gl.incometaxwithheld;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlRepIncomeTaxWithheldForm extends ActionForm implements Serializable {

   private String dateFrom = null;
   private String dateTo = null;
   private String forTheMonth = null;
   private String amended = null;
   private ArrayList amendedList = new ArrayList();
   private String noOfSheetAttached = null;
   private String withheld = null;
   private ArrayList withheldList = new ArrayList();
   private String tinNumber = null;
   private String rdoCode = null;
   private String lineOfBusiness = null;
   private String agentName = null;
   private String telephoneNumber = null;
   private String registeredAddress = null;
   private String zipCode = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String relief = null;
   private ArrayList reliefList = new ArrayList();
   private String reliefDescription = null;
   private String taxRemitted = null;
   private String surcharge = null;
   private String interest = null;
   private String compromise = null;
   private String checkBank = null;
   private String otherBank = null;
   private String checkNumber = null;
   private String otherNumber = null;
   private String checkDate = null;
   private String otherDate = null;
   private String cashBankAmount = null;
   private String checkAmount = null;
   private String otherAmount = null;
   private String presidentSignature = null;
   private String treasurerSignature = null;
   private String titleOfSignatory1 = null;
   private String titleOfSignatory2 = null;
   private String tinOfTaxAgent = null;
   private String taxAgentAccreditation = null;
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private ArrayList reportTypeList = new ArrayList();
   private String reportType;


   private String userPermission = new String();

   public String getDateFrom() {

   	  return(dateFrom);

   }

   public void setDateFrom(String dateFrom) {

   	  this.dateFrom = dateFrom;

   }

   public String getDateTo() {

   	  return(dateTo);

   }

   public void setDateTo(String dateTo) {

   	  this.dateTo = dateTo;

   }

   public String getForTheMonth() {

   	  return forTheMonth;

   }

   public void setForTheMonth(String forTheMonth) {

   	  this.forTheMonth = forTheMonth;

   }

   public String getAmended() {

   	  return amended;

   }

   public void setAmended(String amended) {

   	  this.amended = amended;

   }

   public ArrayList getAmendedList() {

   	  return amendedList;

   }

   public String getNoOfSheetAttached() {

   	  return noOfSheetAttached;

   }

   public void setNoOfSheetAttached(String noOfSheetAttached) {

   	  this.noOfSheetAttached = noOfSheetAttached;

   }

   public String getWithheld() {

   	  return withheld;

   }

   public void setWithheld(String withheld) {

   	  this.withheld = withheld;

   }

   public ArrayList getWithheldList() {

   	  return withheldList;

   }

   public String getTinNumber() {

   	  return tinNumber;

   }

   public void setTinNumber(String tinNumber) {

   	  this.tinNumber = tinNumber;

   }

   public String getRdoCode() {

   	  return rdoCode;

   }

   public void setRdoCode(String rdoCode) {

   	  this.rdoCode = rdoCode;

   }

   public String getLineOfBusiness() {

   	  return lineOfBusiness;

   }

   public void setLineOfBusiness(String lineOfBusiness) {

   	  this.lineOfBusiness = lineOfBusiness;

   }

   public String getAgentName() {

   	  return agentName;

   }

   public void setAgentName(String agentName) {

   	  this.agentName = agentName;

   }

   public String getTelephoneNumber() {

   	  return telephoneNumber;

   }

   public void setTelephoneNumber(String telephoneNumber) {

   	  this.telephoneNumber = telephoneNumber;

   }

   public String getRegisteredAddress() {

   	  return registeredAddress;

   }

   public void setRegisteredAddress(String registeredAddress) {

   	  this.registeredAddress = registeredAddress;

   }

   public String getZipCode() {

   	  return zipCode;

   }

   public void setZipCode(String zipCode) {

   	  this.zipCode = zipCode;

   }

   public String getCategory() {

   	  return category;

   }

   public void setCategory(String category) {

   	  this.category = category;

   }

   public ArrayList getCategoryList() {

   	  return categoryList;

   }

   public String getRelief() {

   	  return relief;

   }

   public void setRelief(String relief) {

   	  this.relief = relief;

   }

   public ArrayList getReliefList() {

   	  return reliefList;

   }

   public String getReliefDescription() {

   	  return reliefDescription;

   }

   public void setReliefDescription(String reliefDescription) {

   	  this.reliefDescription = reliefDescription;

   }

   public String setCloseButton() {

   	  return closeButton;

   }

   public String getViewType(){
   	  return(viewType);
   }

   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }

   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }


   public ArrayList getReportTypeList(){
   	  return reportTypeList;
   }

   public void setReportTypeList(String reportType){
	   reportTypeList.add(reportType);
   }

   public void clearReportTypeList() {
	   reportTypeList.clear();

   }

   public String getReportType() {

   	  return reportType;

   }

   public void setReportType(String reportType) {

   	  this.reportType = reportType;

   }



   public String getReport() {

   	  return report;

   }

   public void setReport(String report) {

   	  this.report = report;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getTaxRemitted() {

   	  return taxRemitted;

   }

   public void setTaxRemitted(String taxRemitted) {

     this.taxRemitted = taxRemitted;

   }

   public String getSurcharge() {

   	  return surcharge;

   }

   public void setSurcharge(String surcharge) {

   	  this.surcharge = surcharge;

   }

   public String getInterest() {

   	  return interest;

   }

   public void setInterest(String interest) {

   	  this.interest = interest;

   }

   public String getCompromise() {

   	  return compromise;

   }

   public void setCompromise(String compromise) {

   	  this.compromise = compromise;

   }

   public String getCheckBank() {

   	  return checkBank;

   }

   public void setCheckBank(String checkBank) {

   	 this.checkBank = checkBank;

   }

   public String getOtherBank() {

   	  return otherBank;

   }

   public void setOtherBank(String otherBank) {

   	  this.otherBank = otherBank;

   }

   public String getCheckNumber() {

   	  return checkNumber;

   }

   public void setCheckNumber(String checkNumber) {

   	  this.checkNumber = checkNumber;

   }

   public String getOtherNumber() {

   	  return otherNumber;

   }

   public void setOtherNumber(String otherNumber) {

   	  this.otherNumber = otherNumber;

   }

   public String getCheckDate() {

   	  return checkDate;

   }

   public void setCheckDate(String checkDate) {

   	  this.checkDate = checkDate;

   }

   public String getOtherDate() {

   	  return otherDate;

   }

   public void setOtherDate(String otherDate) {

   	  this.otherDate = otherDate;

   }

   public String getCashBankAmount() {

   	  return cashBankAmount;

   }

   public void setCashBankAmount(String cashBankAmount) {

   	  this.cashBankAmount = cashBankAmount;

   }

   public String getCheckAmount() {

   	  return checkAmount;

   }

   public void setCheckAmount(String checkAmount) {

   	  this.checkAmount = checkAmount;

   }

   public String getOtherAmount() {

   	  return otherAmount;

   }

   public void setOtherAmount(String otherAmount) {

   	  this.otherAmount = otherAmount;

   }

   public String getPresidentSignature() {

   	  return presidentSignature;

   }

   public void setPresidentSignature(String presidentSignature) {

   	  this.presidentSignature = presidentSignature;

   }

   public String getTreasurerSignature() {

   	  return treasurerSignature;

   }

   public void setTreasurerSignature(String treasurerSignature) {

   	  this.treasurerSignature = treasurerSignature;

   }

   public String getTitleOfSignatory1() {

   	  return titleOfSignatory1;

   }

   public void setTitleOfSignatory1(String titleOfSignatory1) {

   	  this.titleOfSignatory1 = titleOfSignatory1;

   }

   public String getTitleOfSignatory2() {

   	  return titleOfSignatory2;

   }

   public void setTitleOfSignatory2(String titleOfSignatory2) {

   	  this.titleOfSignatory2 = titleOfSignatory2;

   }

   public String getTinOfTaxAgent() {

   	  return tinOfTaxAgent;

   }

   public void setTinOfTaxAgent(String tinOfTaxAgent) {

   	  this.tinOfTaxAgent = tinOfTaxAgent;

   }

   public String getTaxAgentAccreditation() {

   	  return taxAgentAccreditation;

   }

   public void setTaxAgentAccreditation(String taxAgentAccreditation) {

   	  this.taxAgentAccreditation = taxAgentAccreditation;

   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

		dateFrom = null;
		dateTo = null;
		forTheMonth = null;
		amendedList.clear();
		amendedList.add(Constants.GLOBAL_BLANK);
		amendedList.add("YES");
		amendedList.add("NO");
		amended = Constants.GLOBAL_BLANK;
		noOfSheetAttached = null;
		withheldList.clear();
		withheldList.add(Constants.GLOBAL_BLANK);
		withheldList.add("YES");
		withheldList.add("NO");
		withheld = Constants.GLOBAL_BLANK;
		rdoCode = null;
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		categoryList.add("PRIVATE");
		categoryList.add("GOVERNMENT");
		category = Constants.GLOBAL_BLANK;
		reliefList.clear();
		reliefList.add(Constants.GLOBAL_BLANK);
		reliefList.add("YES");
		reliefList.add("NO");
		relief = Constants.GLOBAL_BLANK;
		reliefDescription = null;
		taxRemitted = null;
		surcharge = null;
		interest = null;
		compromise = null;
		checkBank = null;
		otherBank = null;
		checkNumber = null;
		otherNumber = null;
		checkDate = null;
		otherDate = null;
		cashBankAmount = null;
		checkAmount = null;
		otherAmount = null;
		presidentSignature = null;
		treasurerSignature = null;
		titleOfSignatory1 = null;
		titleOfSignatory2 = null;
		tinOfTaxAgent = null;
		taxAgentAccreditation = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		goButton = null;
		closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = new ActionErrors();

      if(request.getParameter("goButton") != null) {

		 if(Common.validateRequired(dateFrom)) {

	            errors.add("dateFrom", new ActionMessage("incomeTaxWithheld.error.dateFromRequired"));

		 }

		 if(!Common.validateDateFormat(dateFrom)) {

	            errors.add("dateFrom", new ActionMessage("incomeTaxWithheld.error.dateFromInvalid"));

		 }

		 if(Common.validateRequired(dateTo)) {

	            errors.add("dateTo", new ActionMessage("incomeTaxWithheld.error.dateToRequired"));

	     }

	     if(!Common.validateDateFormat(dateTo)) {

	            errors.add("dateTo", new ActionMessage("incomeTaxWithheld.error.dateToInvalid"));

	     }

		 if(Common.validateRequired(forTheMonth)) {

	            errors.add("forTheMonth", new ActionMessage("incomeTaxWithheld.error.forTheMonthRequired"));

		 }

	     if(!Common.validateTaxDateFormat(forTheMonth)) {

	            errors.add("forTheMonth", new ActionMessage("incomeTaxWithheld.error.forTheMonthInvalid"));

	     }

	     System.out.println("TIN: " + tinNumber);
		 if(Common.validateRequired(tinNumber)) {

	            errors.add("tinNumber", new ActionMessage("incomeTaxWithheld.error.tinNumberRequired"));

		 }

		 if(Common.validateRequired(telephoneNumber)) {

	            errors.add("telephoneNumber", new ActionMessage("incomeTaxWithheld.error.telephoneNumberRequired"));

		 }

		 if(Common.validateRequired(zipCode)) {

	            errors.add("zipCode", new ActionMessage("incomeTaxWithheld.error.zipCodeRequired"));

		 }


		 if(Common.validateRequired(rdoCode)) {

	            errors.add("rdoCode", new ActionMessage("incomeTaxWithheld.error.rdoCodeRequired"));

		 }

	     if(!Common.validateNumberFormat(rdoCode)) {

	            errors.add("rdoCode", new ActionMessage("incomeTaxWithheld.error.rdoCodeInvalid"));

	     }

	     if(!Common.validateNumberFormat(noOfSheetAttached)) {

	            errors.add("noOfSheetAttached", new ActionMessage("incomeTaxWithheld.error.noOfSheetAttachedInvalid"));

	     }

		 if((relief.equals("YES")) && Common.validateRequired(reliefDescription)) {

	            errors.add("reliefDescription", new ActionMessage("incomeTaxWithheld.error.reliefDescriptionRequired"));

		 }

         if (!Common.validateMoneyFormat(taxRemitted)) {

            errors.add("taxRemitted",
               new ActionMessage("incomeTaxWithheld.error.taxRemittedInvalid"));

         }

         if (!Common.validateMoneyFormat(surcharge)) {

            errors.add("surcharge",
               new ActionMessage("incomeTaxWithheld.error.surchargeInvalid"));

         }

         if (!Common.validateMoneyFormat(interest)) {

            errors.add("interest",
               new ActionMessage("incomeTaxWithheld.error.interestInvalid"));

         }

         if (!Common.validateMoneyFormat(compromise)) {

            errors.add("compromise",
               new ActionMessage("incomeTaxWithheld.error.compromiseInvalid"));

         }

         if (!Common.validateMoneyFormat(cashBankAmount)) {

            errors.add("cashBankAmount",
               new ActionMessage("incomeTaxWithheld.error.cashBankAmountInvalid"));

         }

         if (!Common.validateMoneyFormat(checkAmount)) {

            errors.add("checkAmount",
               new ActionMessage("incomeTaxWithheld.error.checkAmountInvalid"));

         }

         if (!Common.validateMoneyFormat(otherAmount)) {

            errors.add("otherAmount",
               new ActionMessage("incomeTaxWithheld.error.otherAmountInvalid"));

         }

	     if(!Common.validateNumberFormat(checkNumber)) {

	        errors.add("checkNumber",
	           new ActionMessage("incomeTaxWithheld.error.checkNumberInvalid"));

	     }

	     if(!Common.validateNumberFormat(otherNumber)) {

	        errors.add("otherNumber",
	           new ActionMessage("incomeTaxWithheld.error.otherNumberInvalid"));

	     }

		 if(!Common.validateDateFormat(checkDate)) {

	        errors.add("checkDate",
	           new ActionMessage("incomeTaxWithheld.error.checkDateInvalid"));

		 }

		 if(!Common.validateDateFormat(otherDate)) {

	        errors.add("otherDate",
	           new ActionMessage("incomeTaxWithheld.error.otherDateInvalid"));

		 }

		 if(Common.validateRequired(noOfSheetAttached)) {

	            errors.add("noOfSheetAttached", new ActionMessage("incomeTaxWithheld.error.noOfSheetAttachedRequired"));

		 }

      }

      return(errors);

   }
}