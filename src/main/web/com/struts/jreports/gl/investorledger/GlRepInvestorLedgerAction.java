package com.struts.jreports.gl.investorledger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;









import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.joda.time.LocalDate;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepInvestorLedgerController;
import com.ejb.txn.GlRepInvestorLedgerControllerHome;
import com.struts.ap.voucherentry.ApVoucherLineItemList;
import com.struts.ap.voucherentry.ApVoucherPurchaseOrderLineList;
import com.struts.jreports.ar.statement.ArRepStatementDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.GenModSegmentDetails;
import com.util.GenModValueSetValueDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class GlRepInvestorLedgerAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepInvestorLedgerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepInvestorLedgerForm actionForm = (GlRepInvestorLedgerForm)form;
	      
	      // reset report to null
	      actionForm.setReport(null);
 
         String frParam = Common.getUserPermission(user, Constants.GL_REP_INVESTOR_LEDGER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepInvestorLedger");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepInvestorLedgerController EJB
*******************************************************/

         
         GlRepInvestorLedgerControllerHome homeIL = null;
         GlRepInvestorLedgerController ejbIL = null;       

         try {
         	
        	 homeIL = (GlRepInvestorLedgerControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepInvestorLedgerControllerEJB", GlRepInvestorLedgerControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepInvestorLedgerAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
        	 ejbIL = homeIL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepInvestorLedgerAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
     
 /*******************************************************
     Call GlRepInvestorLedgerControllerEJB EJB
     getGlFcPrecisionUnit
  *******************************************************/
   
   
   short precisionUnit = 0;
   short journalLineNumber = 0;
   boolean enableVoucherBatch = false; 
   boolean isInitialPrinting = false;
   boolean useSupplierPulldown = true;
    
   try {
   	
      
      useSupplierPulldown = Common.convertByteToBoolean(ejbIL.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
      actionForm.setUseSupplierPulldown(useSupplierPulldown);          

      
      
   
   } catch(EJBException ex) {
   	
      if (log.isInfoEnabled()) {
      	
         log.info("EJBException caught in GlRepInvestorLedgerAction.execute(): " + ex.getMessage() +
        " session: " + session.getId());
  }
  
  return(mapping.findForward("cmnErrorPage"));
      
   }
   
   
         		        

   
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- GL GL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            AdCompanyDetails adCmpDetails = null;
            String company = null;
            
           
            	
            	
		    try {

		    	ArrayList branchList = new ArrayList();
		     	
		     	for(int i=0; i<actionForm.getGlRepBrInvestorLedgerListSize(); i++) {
		     	    
		     	    GlRepBranchInvestorLedgerList adBglList = (GlRepBranchInvestorLedgerList)actionForm.getGlRepBrInvestorLedgerListByIndex(i);
		     	    
		     	    if(adBglList.getBranchCheckbox() == true) {                                          
		     	        
		     	        AdBranchDetails brDetails = new AdBranchDetails();                                          
		     	        
		     	        brDetails.setBrAdCompany(user.getCmpCode());	     	        
		     	        brDetails.setBrCode(adBglList.getBrCode());                    
		     	        
		     	        branchList.add(brDetails);
		     	        
		     	    }
		     	}
		    	
		     	// get company
		       
		       adCmpDetails = ejbIL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		      

		       
		       
			   
			   // execute report

		       list = ejbIL.executeGlRepInvestorLedger(
			       	   actionForm.getSupplier(),
			       	   Common.convertStringToSQLDate(actionForm.getDate()),
			           actionForm.getIncludeUnpostedSlTransaction(), 
			           actionForm.getOrderBy(), 
			           branchList, 
			           new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			       
		       
		       
		       
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("investorLedger.error.noRecordFound"));
                     
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in GlRepInvestorLedgerAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepInvestorLedger"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("investor", actionForm.getSupplier());
		    parameters.put("date", Common.convertStringToSQLDate(actionForm.getDate()));
		    parameters.put("amountType", actionForm.getAmountType());
		    
		    LocalDate today = new LocalDate(Common.convertStringToSQLDate(actionForm.getDate()));
		    
		    LocalDate firstDayOfPeriod = new LocalDate();
		    
		    
		    if(actionForm.getAmountType().equals("PTD")){
		    	
		    	firstDayOfPeriod = today.withDayOfMonth(1); 
		    
		    	
		    } else {
		    	
		    	firstDayOfPeriod = today.withDayOfYear(1);
		    }

		  	System.out.println(actionForm.getAmountType()+"= firstDayOfPeriod="+firstDayOfPeriod); 
		    parameters.put("firstDayOfPeriod", firstDayOfPeriod.toDate());
            parameters.put("amountType", actionForm.getAmountType());
            parameters.put("viewType", actionForm.getViewType());
            parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());

		    if (actionForm.getIncludeUnpostedSlTransaction()) {
		    	
		    	parameters.put("includeUnpostedSlTransaction", "YES");
		    	
		    } else {
		    	
		    	parameters.put("includeUnpostedSlTransaction", "NO");
		    	
		    }
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getGlRepBrInvestorLedgerListSize(); j++) {

      			GlRepBranchInvestorLedgerList brList = (GlRepBranchInvestorLedgerList)actionForm.getGlRepBrInvestorLedgerListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
		    
      		String filename = null;
      		
      		
      		if (actionForm.getReportType().equals("SUMMARY")) {
		    	System.out.println("SUMMARY");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepInvestorLedgerSummary.jasper";
			       
			    if (!new java.io.File(filename).exists()) {
			    		    		    
			       filename = servlet.getServletContext().getRealPath("jreports/GlRepInvestorLedgerSummary.jasper");
				    
			    }
			
      		} else   if (actionForm.getReportType().equals("STATEMENT")) {
		    	System.out.println("STATEMENT");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepInvestorLedgerStatement.jasper";
			       
			    if (!new java.io.File(filename).exists()) {
			    		    		    
			       filename = servlet.getServletContext().getRealPath("jreports/GlRepInvestorLedgerStatement.jasper");
				    
			    }
			    
			    
         	} else {
         		System.out.println("DEFAULT REPORT");
         		filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepInvestorLedger.jasper";
 		       
    		    if (!new java.io.File(filename).exists()) {
    		    		    		    
    		       filename = servlet.getServletContext().getRealPath("jreports/GlRepInvestorLedger.jasper");
    			    
    		    }		    	
		    	
		    }
      		
		    
		    
		    
		     
		    
		    try {
		    	
		    	Report report = new Report();
		    	
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		 
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new GlRepInvestorLedgerDS(list)));   
				        
			      
			       
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
				   
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new GlRepInvestorLedgerDS(list)));   
	               
	      
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)){
				   
	               report.setViewType(Constants.REPORT_VIEW_TYPE_CSV);
	               report.setBytes(
					   JasperRunManagerExt.runReportToCsv(filename, parameters, 
					        new GlRepInvestorLedgerDS(list)));   
	               
	      
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	 
				   
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new GlRepInvestorLedgerDS(list)));												    
				       
			   }
		     
		       session.setAttribute(Constants.REPORT_KEY, report);
		       
		     
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepInvestorLedgerAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
/*******************************************************
    -- Ap VOU Supplier Enter Action --
 *******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
		 		    	
	    	System.out.println("GlRepInvestorLedgerAction isSupplierEntered");
	    	
	    	try {	
         		
         		ApModSupplierDetails spldetails = ejbIL.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

         		actionForm.setSupplierName(spldetails.getSplName());
         	
         		System.out.println("spldetails.getSplName()="+spldetails.getSplName());
         		
         	} catch (GlobalNoRecordFoundException ex) {
             	
             	
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("investorLedger.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlRepInvestorLedgerAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
	        	  
	        	  
	    	return(mapping.findForward("glRepInvestorLedger"));	    
		    
		    
	          
/*******************************************************
   -- GL GL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- GL GL Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;			
			      

	            	actionForm.clearReportTypeList();
			        
			        list = ejbIL.getAdLvReportTypeAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            	
	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setReportTypeList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	
	            	
	            	
	            	if(actionForm.getUseSupplierPulldown()) {
	            		
		            	actionForm.clearSupplierList();           	
		            	
		            	list = ejbIL.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		            	
		            	if (list == null || list.size() == 0) {
		            		
		            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
		            		
		            	} else {
		            		           		            		
		            		i = list.iterator();
		            		
		            		while (i.hasNext()) {
		            			
		            		    actionForm.setSupplierList((String)i.next());
		            			
		            		}
		            		            		
		            	}
		            	
	            	}
	            	

			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepInvestorLedgerAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            actionForm.reset(mapping, request);
	            
                // populate AdRepBranchBankAccountListList
         		
         		ArrayList brList = new ArrayList();
         		actionForm.clearGlRepBrInvestorLedgerList();
                
         		brList = ejbIL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                Iterator k = brList.iterator();
                
                while(k.hasNext()) {
                	
                    AdBranchDetails brDetails = (AdBranchDetails)k.next();
                
                    GlRepBranchInvestorLedgerList adBglList = new GlRepBranchInvestorLedgerList(actionForm,
                        brDetails.getBrBranchCode(), brDetails.getBrName(),
                        brDetails.getBrCode());
                    adBglList.setBranchCheckbox(true);
                                 
                    actionForm.saveGlRepBrInvestorLedgerList(adBglList);
                    
                }
                
                
                
             // get company details
			       
	            AdCompanyDetails adCmpDetails = ejbIL.getAdCompany(user.getCmpCode());
	            
	          
	            
	            return(mapping.findForward("glRepInvestorLedger"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in GlRepInvestorLedgerAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
