package com.struts.jreports.gl.investorledger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Date;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.GlRepInvestorLedgerDetails;

public class GlRepInvestorLedgerDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;


   GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();


   public GlRepInvestorLedgerDS(ArrayList list){
   	

      Iterator i = list.iterator();
      
      while (i.hasNext()) {

    	  GlRepInvestorLedgerDetails details = (GlRepInvestorLedgerDetails)i.next();       
    	  System.out.println("details.getIlBeginningBalance()="+details.getIlBeginningBalance());
	     GlRepInvestorLedgerData glData = new GlRepInvestorLedgerData(
	    		 details.getIlInvestorCode(),
	    		 details.getIlInvestorName(),
	    		 details.getIlInvestorContact(),
	    		 details.getIlInvestorAddress(),
	    		 
	    		 
	    		 Common.convertSQLDateToString(details.getIlEffectiveDate()),
	    		 details.getIlEffectiveDate() ,
	    		 details.getIlDocumentNumber(),
	    		 details.getIlReferenceNumber(),
	    		 details.getIlSourceName(),
	    		 details.getIlDescription(),
	    		 details.getIlType(),
	    		 details.getIlDebit() == (short)1 ? new Double(details.getIlAmount()) : 0d,
	    		 details.getIlDebit() == (short)0 ? new Double(details.getIlAmount()) : 0d,
	    		 details.getIlDebit() == (short)1 ? new Double(details.getIlAmount()) : null,
	    		 details.getIlDebit() == (short)0 ? new Double(details.getIlAmount()) : null,
	    			    				 
	    		 details.getIlAmount(),
	    		 details.getIlBeginningBalance(),
	    		 details.getIlPosted() == (byte)1 ? "YES" : "NO",
	    		new Double(details.getIlConversionRate())
	    	 );

         data.add(glData);
      }
      

   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();
    
      if("investorCode".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getInvestorCode();
     }else if("investorName".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getInvestorName();
     }else if("investorContact".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getInvestorContact();
     }else if("investorAddress".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getInvestorAddress();
        
     }else if("date".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getDate();
      }else if("date2".equals(fieldName)){
          value = ((GlRepInvestorLedgerData)data.get(index)).getDate2();
     }else if("documentNumber".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getDocumentNumber();
     }else if("referenceNumber".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getReferenceNumber();
     }else if("sourceName".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getSourceName();
     }else if("description".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getDescription();
     
     }else if("type".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getType();
         
     }else if("beginningBalance".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getBeginningBalance();
         System.out.println("beginningBalance"+value);

     }else if("debit".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getDebit();

     }else if("credit".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getCredit();  

     }else if("debit2".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getDebit2();
         System.out.println("debit2="+value);
      }else if("credit2".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getCredit2();  
         System.out.println("credit2="+value);
     }else if("amount".equals(fieldName)){
         value = ((GlRepInvestorLedgerData)data.get(index)).getAmount();   
     
     }else if("posted".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getPosted();

     }else if("conversionRate".equals(fieldName)){
        value = ((GlRepInvestorLedgerData)data.get(index)).getConversionRate();    
     
	  }

      return(value);
   }
}
