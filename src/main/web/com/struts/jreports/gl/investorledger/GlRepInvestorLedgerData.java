package com.struts.jreports.gl.investorledger;

import java.util.Date;

public class GlRepInvestorLedgerData implements java.io.Serializable{
	
	   private String investorCode = null;
	   private String investorName = null;
	   private String investorContact= null;
	   private String investorAddress= null;

	   private String date = null;
	   private Date date2 = null;
	   private String documentNumber = null;
	   private String referenceNumber = null;
	   private String sourceName = null;
	   private String description = null;
	   private String type = null;

	   private Double debit = null;
	   private Double credit = null;
	   private Double debit2 = null;
	   private Double credit2 = null;
	   private Double amount = null;
	   private Double beginningBalance = null;
	   private String posted = null;
	   private Double conversionRate = null;


	   public GlRepInvestorLedgerData(
			   String investorCode, 
			   String investorName,
			   String investorContact,
			   String investorAddress,
			   
			   String date, 
			   Date date2, 
			   String documentNumber, 
			   String referenceNumber, 
			   String sourceName,
			   String description,
			   String type,
			   
			   Double debit, Double credit, 
			   Double debit2, Double credit2,
			   Double amount,
			   Double beginningBalance,
			   String posted,
			   Double conversionRate
		  
		  ){
		   
	      this.investorCode = investorCode;
	      this.investorName = investorName;
	      this.investorContact = investorContact;
	      this.investorAddress = investorAddress;
	      
	      this.date = date;
	      this.date2 = date2;
	      this.documentNumber = documentNumber;
	      this.referenceNumber = referenceNumber;
	      this.sourceName = sourceName;
	      this.description = description;
	      this.type = type;

	      this.debit = debit;
	      this.credit = credit;
	      this.debit2 = debit2;
	      this.credit2 = credit2;
	      this.amount = amount;
	      this.beginningBalance = beginningBalance;
	      this.posted = posted;
	      this.conversionRate = conversionRate;

	   }
	   

	   public String getInvestorCode() {
	   	
	   	  return investorCode;
	   	
	   }
	   
	 
	   public String getInvestorName() {
	   	
	   	  return investorName;
	   	
	   }
	   
	   public String getInvestorContact() {
		   	
	   	  return investorContact;
	   	
	   }
	   
	   public String getInvestorAddress() {
		   	
	   	  return investorAddress;
	   	
	   }
	   
	   
	   
	
	   public String getDate() {
	   	
	   	  return date;
	   	
	   }
	   
	   
	   public Date getDate2() {
		   	
	   	  return date2;
	   	
	   }
	   
	   public String getDocumentNumber() {

	      return documentNumber;

	   }
	   
	   public String getReferenceNumber() {
		   	
	   	  return referenceNumber;
	   	
	   }
	   
	   
	   public String getSourceName() {
	   	
	   	  return sourceName;
	   	
	   }
	   
	   public String getDescription() {
	   	
	   	  return description;
	   	
	   }
	   
	
	   public String getType() {
		   	
	   	  return type;
	   	
	   }

	   
	   
	  
	   
	   public Double getDebit() {
	   	
	   	  return debit;
	   	
	   }
	   
	   public Double getCredit() {
	   	
	   	  return credit;
	   	
	   }
	   
	   
	   public Double getDebit2() {
		   	
	   	  return debit2;
	   	
	   }
	   
	   public Double getCredit2() {
	   	
	   	  return credit2;
	   	
	   }
	   
	   public Double getAmount() {
		   	
	   	  return amount;
	   	
	   }
	   
	   public Double getBeginningBalance() {
		   	
	   	  return beginningBalance;
	   	
	   }

	   public String getPosted() {
	   	
	   	  return posted;
	   	
	   }
	   
	   public Double getConversionRate() {

		   return conversionRate;

	   }
	   

	
	}
