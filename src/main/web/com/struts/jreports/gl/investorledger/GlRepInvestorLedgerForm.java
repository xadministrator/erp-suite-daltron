package com.struts.jreports.gl.investorledger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepInvestorLedgerForm extends ActionForm implements Serializable{
	
	private String supplier = null;
	private String supplierName = null;
	private ArrayList supplierList = new ArrayList();
	private boolean useSupplierPulldown = true;
	
	private String date = null;
	private boolean includeUnpostedSlTransaction = false;

	
	
	
	private String amountType = null;
	private ArrayList amountTypeList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	
	
	private ArrayList glRepBrInvestorLedgerList = new ArrayList();
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();
	
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private String userPermission = new String();
	
	private String isSupplierEntered  = null;
	
	
	public String getIsSupplierEntered() {
	   	
   	   return isSupplierEntered;
   	
   }
	
	
public String getSupplier() {   	   	  
   	  	
   	  	return supplier;
   	     	     	
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   	
   }
   
   public String getSupplierName() {
	   	
	   	return supplierName;
	   	
	   }
	   
	   public void setSupplierName(String supplierName) {
	   	
	   	this.supplierName = supplierName;
	   	
	   }
            
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(String supplier) {
   	
   	  supplierList.add(supplier);
   	
   }
   public void clearSupplierList() {
   	
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   	
   }




public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }
   
	public String getDate() {
		return(date);
	}
	
	public void setDate(String date){
		this.date = date;
	}
	
	public String getAmountType(){
		return(amountType);
	}
	
	public void setAmountType(String amountType){
		this.amountType = amountType;
	}
	
	public ArrayList getAmountTypeList() {
		
		return(amountTypeList);
		
	}
	
	
	
	public String getViewType(){
		return(viewType);   	
	}
	
	public void setViewType(String viewType){
		this.viewType = viewType;
	}
	
	public ArrayList getViewTypeList(){
		return viewTypeList;
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	
	
	public void setGoButton(String goButton){
		this.goButton = goButton;
	}
	
	public void setCloseButton(String closeButton){
		this.closeButton = closeButton;
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	
	
	public Object[] getGlRepBrInvestorLedgerList(){
		
		return glRepBrInvestorLedgerList.toArray();
		
	}
	
	public GlRepBranchInvestorLedgerList getGlRepBrInvestorLedgerListByIndex(int index){
		
		return ((GlRepBranchInvestorLedgerList)glRepBrInvestorLedgerList.get(index));
		
	}
	
	public int getGlRepBrInvestorLedgerListSize(){
		
		return(glRepBrInvestorLedgerList.size());
		
	}
	
	public void saveGlRepBrInvestorLedgerList(Object newGlRepBrInvestorLedgerList){
		
		glRepBrInvestorLedgerList.add(newGlRepBrInvestorLedgerList);   	  
		
	}
	
	public void clearGlRepBrInvestorLedgerList(){
		
		glRepBrInvestorLedgerList.clear();
		
	}
	
	public void setGlRepBrInvestorLedgerList(ArrayList glRepBrInvestorLedgerList) {
		
		this.glRepBrInvestorLedgerList = glRepBrInvestorLedgerList;
		
	}
	
	public boolean getIncludeUnpostedSlTransaction() {
		
		return includeUnpostedSlTransaction;
		
	}
	
	public void setIncludeUnpostedSlTransaction(boolean includeUnpostedSlTransaction) {
		
		this.includeUnpostedSlTransaction = includeUnpostedSlTransaction;
		
	}
	

	
   	
   	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	
	public String getOrderBy() {

		return(orderBy);

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}
	
	
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<glRepBrInvestorLedgerList.size(); i++) {
			
			GlRepBranchInvestorLedgerList list = (GlRepBranchInvestorLedgerList)glRepBrInvestorLedgerList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		date = Common.convertSQLDateToString(new Date());
		
		supplier = Constants.GLOBAL_BLANK;
		supplierName = Constants.GLOBAL_BLANK;
		
		amountTypeList.clear();
		amountTypeList.add("PTD");
		amountTypeList.add("QTD");
		amountTypeList.add("YTD");
		amountType = "PTD";
		goButton = null;
		closeButton = null;

		isSupplierEntered = null;
		
		includeUnpostedSlTransaction = false;

		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_CSV);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;

		orderByList.clear();
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add("INVESTOR NAME");

		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("goButton") != null){

			
			if(Common.validateRequired(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateRequired"));
			}
			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateInvalid"));
			}
			if(!Common.validateDateFormat(date)){
				errors.add("date", new ActionMessage("generalLedger.error.dateInvalid"));
			}
			
		}
		
		return(errors);
	}
}
