package com.struts.jreports.gl.journaleditlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepJournalEditListController;
import com.ejb.txn.GlRepJournalEditListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;

public final class GlRepJournalEditListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRepJournalPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRepJournalEditListForm actionForm = (GlRepJournalEditListForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.GL_REP_JOURNAL_EDIT_LIST_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlRepJournalEditListController EJB
*******************************************************/

         GlRepJournalEditListControllerHome homeVP = null;
         GlRepJournalEditListController ejbVP = null;       

         try {
         	
            homeVP = (GlRepJournalEditListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepJournalEditListControllerEJB", GlRepJournalEditListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in GlRepJournalEditListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbVP = homeVP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in GlRepJournalEditListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getGlJrByJrCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    ArrayList list = null;           
				    
				try {
					
					ArrayList jrCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
						jrCodeList.add(new Integer(request.getParameter("journalCode")));
						
					} else {
						
						int i = 0;
																	
						while (true) {
							
							if (request.getParameter("journalCode[" + i + "]") != null) {
								
								jrCodeList.add(new Integer(request.getParameter("journalCode[" + i + "]")));
								
								i++;
								
							} else {
								
								break;
								
							}
							
						}												
												
					}
	            	
	            	list = ejbVP.executeGlRepJournalEditList(jrCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbVP.getAdCompany(user.getCmpCode());	            	
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("journalEditList.error.journalAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in GlRepJournalEditListAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
				
				/** fill report parameters, fill report to pdf and set report session **/ 	    	               
	   
				Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepJournalEditList.jasper";
			       
			    if (!new java.io.File(filename).exists()) {
			    		    		    
			       filename = servlet.getServletContext().getRealPath("jreports/GlRepJournalEditList.jasper");
				    
			    }
				 						    	    	    	        	    
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new GlRepJournalEditListDS(list, user.getUserName())));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in GlRepJournalEditListAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("glRepJournalEditList"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("glRepJournalEditListForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in GlRepJournalEditListAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
