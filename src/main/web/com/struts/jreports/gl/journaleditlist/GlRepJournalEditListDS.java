package com.struts.jreports.gl.journaleditlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.GlRepJournalEditListDetails;


public class GlRepJournalEditListDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepJournalEditListDS(ArrayList glRepJELList, String userName) {
   	
   	  Date currentDate = new Date();

   	  Iterator i = glRepJELList.iterator();
   	     	     	  
   	  while(i.hasNext()) {
   	  	
   	    GlRepJournalEditListDetails details = (GlRepJournalEditListDetails)i.next();
   	    
   	    if(details.getJelJlDebit() == 1) {
   	    
	   	  	GlRepJournalEditListData glRepJELData = new GlRepJournalEditListData(details.getJelJrBatchName(),
	   	  			details.getJelJrBatchDescription(), Common.convertIntegerToString(new Integer(details.getJelJrTransactionTotal())), 
					Common.convertSQLDateToString(currentDate), 
					Common.convertSQLDateTimeToString(currentDate), userName,
					Common.convertSQLDateToString(details.getJelJrDate()), details.getJelJrDocumentNumber(),
					details.getJelJrReferenceNumber(), details.getJelJrDescription(), details.getJelJlAccountNumber(),
					details.getJelJlAccountDescription(), new Double(details.getJelJlAmount()), null, details.getJelJlDescription());
	   	  	

             data.add(glRepJELData);
	   	  	
	    } else {
	   	
	    	GlRepJournalEditListData glRepJELData = new GlRepJournalEditListData(details.getJelJrBatchName(),
	   	  			details.getJelJrBatchDescription(), Common.convertIntegerToString(new Integer(details.getJelJrTransactionTotal())), 
					Common.convertSQLDateToString(currentDate), 
					Common.convertSQLDateTimeToString(currentDate), userName,
					Common.convertSQLDateToString(details.getJelJrDate()), details.getJelJrDocumentNumber(),
					details.getJelJrReferenceNumber(), details.getJelJrDescription(), details.getJelJlAccountNumber(),
					details.getJelJlAccountDescription(), null, new Double(details.getJelJlAmount()), details.getJelJlDescription());
    	
   	  	    data.add(glRepJELData);
   	    }	   	   
   	  	
   	  	
   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

	  if("batchName".equals(fieldName)){
	    value = ((GlRepJournalEditListData)data.get(index)).getBatchName(); 
	  }else if("batchDescription".equals(fieldName)){
	    value = ((GlRepJournalEditListData)data.get(index)).getBatchDescription();       
	  }else if("transactionTotal".equals(fieldName)){
	    value = ((GlRepJournalEditListData)data.get(index)).getTransactionTotal(); 
	  }else if("dateCreated".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getDateCreated();  
      }else if("timeCreated".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getTimeCreated();  
      }else if("createdBy".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getCreatedBy();
      }else if("date".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getDate();     
      }else if("documentNumber".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getDocumentNumber();  
      }else if("referenceNumber".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getReferenceNumber(); 
      }else if("description".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getDescription();   
      }else if("accountNumber".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getAccountNumber();   
      }else if("accountDescription".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getAccountDescription();  
      }else if("debitAmount".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getDebitAmount();   
      }else if("creditAmount".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getCreditAmount();                   
      }else if("lineDescription".equals(fieldName)){
        value = ((GlRepJournalEditListData)data.get(index)).getLineDescription();   
      }

      return(value);
   }
}
