package com.struts.jreports.gl.journaleditlist;

public class GlRepJournalEditListData implements java.io.Serializable {

	   private String batchName = null;
	   private String batchDescription = null;
	   private String transactionTotal = null;
	   private String dateCreated = null;
	   private String timeCreated = null;
	   private String createdBy = null;
	   private String date = null;
	   private String documentNumber = null;
	   private String referenceNumber = null;
	   private String description = null;

	   private String accountNumber = null;
	   private String accountDescription = null;
	   private Double debitAmount = null;
	   private Double creditAmount = null;
	   private String lineDescription = null;
	   
	   public GlRepJournalEditListData(String batchName, String batchDescription, String transactionTotal, 
	    String dateCreated, String timeCreated, String createdBy, String date, String documentNumber, 
		String referenceNumber, String description, String accountNumber, String accountDescription, 
		Double debitAmount, Double creditAmount, String lineDescription) {
	      
	      this.batchName = batchName;
	      this.batchDescription = batchDescription;
	      this.transactionTotal = transactionTotal;
	      this.dateCreated = dateCreated;
	      this.timeCreated = timeCreated;
	      this.createdBy = createdBy;
	      this.date = date;
	      this.documentNumber = documentNumber;
	      this.referenceNumber = referenceNumber;
	      this.description = description;
	      this.accountNumber = accountNumber;
	      this.accountDescription = accountDescription;
	      this.debitAmount = debitAmount;
	      this.creditAmount = creditAmount;
	      this.lineDescription = lineDescription;
	      	
	   }
	   
	   public String getBatchName() {
	   	
	   	  return batchName;
	   	 
	   }
	   
	   public String getBatchDescription() {
	   	
	   	  return batchDescription;
	   	
	   }
	   
	   public String getTransactionTotal() {
	   	
	   	  return transactionTotal;
	   	
	   }

	   public String getDateCreated() {

	      return dateCreated;

	   }

	   public String getTimeCreated() {

	      return timeCreated;

	   }

	   public String getCreatedBy() {

	      return createdBy;

	   }

	   public String getDate() {

	      return date;

	   }

	   public String getDocumentNumber() {
	   
	      return documentNumber;

	   }

	   public String getReferenceNumber() {

	      return referenceNumber;

	   }
	   
	   public String getDescription() {

	      return description;

	   }

	   public String getAccountNumber() {

	      return accountNumber;

	   }

	   public String getAccountDescription() {

	      return accountDescription;

	   }

	   public Double getDebitAmount() {

	      return debitAmount;

	   }

	   public Double getCreditAmount() {
	    
	      return creditAmount;

	   } 
	   
	   public String getLineDescription() {
		   
		   return lineDescription;
		   
	   }
	   
}

