package com.struts.jreports.gl.journalprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.GlRepJournalPrintDetails;


public class GlRepJournalPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepJournalPrintDS(ArrayList list) {

   	  Iterator i = list.iterator();
   	  while(i.hasNext()) {
   	  	
   	    GlRepJournalPrintDetails mdetails = (GlRepJournalPrintDetails)i.next();
   	    
   	    if(mdetails.getJpJlDebit() == 1) {
   	    
	   	  	GlRepJournalPrintData glRepJPData = new GlRepJournalPrintData(mdetails.getJpJrDocumentNumber(),
	   	  	    Common.convertSQLDateToString(mdetails.getJpJrEffectiveDate()),
	   	  	    mdetails.getJpJrCreatedBy(),
	   	  	    mdetails.getJpJrApprovedRejectedBy(),
	   	  	    mdetails.getJpJlCoaAccountNumber(),
	   	  	    mdetails.getJpJlCoaAccountDescription(),
	   	  	    new Double(mdetails.getJpJlAmount()),
	   	  	    null, 
	   	  		mdetails.getJpJrApprovalStatus(),
				mdetails.getJpJrPosted() == (byte)1 ? "YES" : "NO",
				mdetails.getJpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				mdetails.getJpJrDescription(),
				mdetails.getJpJrName(),
				mdetails.getJpJrDateReversal(),
	   	  		mdetails.getJpJrReversed() == (byte)1 ? "YES" : "NO",
	   	  		mdetails.getJpJrPostedBy(),
				mdetails.getJpJrJcName(),
				mdetails.getJpJrJsName(),
				mdetails.getJpJrFcName(),
				Common.convertCharToString(mdetails.getJpJrFcSymbol()),
				mdetails.getJpJrJbName(),
				mdetails.getJpJrCreatedByDescription(),
				mdetails.getBrBranchCode(),
				mdetails.getBrName(),
				mdetails.getJpJlDescription(),
				mdetails.getJpJrApprovedByDescription());

             data.add(glRepJPData);
	   	  	
	    } else {
	   	
	   	    GlRepJournalPrintData glRepJPData = new GlRepJournalPrintData(mdetails.getJpJrDocumentNumber(),
	   	  	    Common.convertSQLDateToString(mdetails.getJpJrEffectiveDate()),
	   	  	    mdetails.getJpJrCreatedBy(),
	   	  	    mdetails.getJpJrApprovedRejectedBy(),
	   	  	    mdetails.getJpJlCoaAccountNumber(),
	   	  	    mdetails.getJpJlCoaAccountDescription(),
	   	  	    null,
	   	  	    new Double(mdetails.getJpJlAmount()),
	   	  	    mdetails.getJpJrApprovalStatus(),
			    mdetails.getJpJrPosted() == (byte)1 ? "YES" : "NO",
				mdetails.getJpShowDuplicate() == (byte)1 ? new Boolean(true) : new Boolean(false),
				mdetails.getJpJrDescription(),
				mdetails.getJpJrName(),
				mdetails.getJpJrDateReversal(),
	   	  		mdetails.getJpJrReversed() == (byte)1 ? "YES" : "NO",
	   	  		mdetails.getJpJrPostedBy(),
				mdetails.getJpJrJcName(),
				mdetails.getJpJrJsName(),
				mdetails.getJpJrFcName(),
				Common.convertCharToString(mdetails.getJpJrFcSymbol()),
				mdetails.getJpJrJbName(),
				mdetails.getJpJrCreatedByDescription(),
				mdetails.getBrBranchCode(),
				mdetails.getBrName(),
				mdetails.getJpJlDescription(),
				mdetails.getJpJrApprovedByDescription());
    	
   	  	    data.add(glRepJPData);
   	  	    
   	    }
   	   
   	  }
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      if("journalNumber".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getJournalNumber();
      }else if("date".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getDate();       
      }else if("createdBy".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getCreatedBy();           
      }else if("approvedBy".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getApprovedBy();              
      }else if("accountNumber".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getAccountNumber();       
      }else if("accountDescription".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getAccountDescription();
      }else if("debitAmount".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getDebitAmount();
      }else if("creditAmount".equals(fieldName)){
         value = ((GlRepJournalPrintData)data.get(index)).getCreditAmount();
      }else if("approvalStatus".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getApprovalStatus();
      }else if("posted".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getPosted();   
      }else if("showDuplicate".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getShowDuplicate();
      }else if("category".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getJournalCategory();
      }else if("source".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getJournalSource();
      }else if("description".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getDescription();        
      }else if("createdByDescription".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getCreatedByDescription();              
      }else if("referenceNumber".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getReferenceNumber();              
      }else if("currency".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getCurrency();              
      }else if("currencySymbol".equals(fieldName)){
          value = ((GlRepJournalPrintData)data.get(index)).getCurrencySymbol();              
      }else if("branchCode".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getBranchCode();              
      }else if("branchName".equals(fieldName)){
        value = ((GlRepJournalPrintData)data.get(index)).getBranchName();              
      }else if("lineDescription".equals(fieldName)){
       value = ((GlRepJournalPrintData)data.get(index)).getLineDescription();              
     }else if("approvedByDescription".equals(fieldName)){
       value = ((GlRepJournalPrintData)data.get(index)).getApprovedByDescription();              
     }
      
      return(value);
   }
}
