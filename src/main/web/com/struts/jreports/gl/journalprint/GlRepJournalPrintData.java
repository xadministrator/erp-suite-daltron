package com.struts.jreports.gl.journalprint;

import java.util.Date;

public class GlRepJournalPrintData implements java.io.Serializable {

   private String journalNumber = null;
   private String date = null;
   private String createdBy = null;
   private String approvedBy = null;
   private String accountNumber = null;
   private String accountDescription = null;
   private Double debitAmount = null;
   private Double creditAmount = null;
   
   // added fields
   private String approvalStatus = null;
   private String posted = null;
   
   private String description = null;
   private String referenceNumber = null;
   private Date dateReversal = null;
   private String reversed = null;
   private String postedBy = null;
   private String journalCategory = null;
   private String journalSource = null;
   private String currency = null;
   private String batchName = null;
   private String createdByDescription = null;
   private Boolean showDuplicate = null;
   private String branchCode = null;
   private String branchName = null;
   private String lineDescription = null;
   private String approvedByDescription = null;
   private String currencySymbol = null;
   
   public GlRepJournalPrintData(String journalNumber, 
        String date, String createdBy, String approvedBy, String accountNumber, String accountDescription, 
        Double debitAmount, Double creditAmount, String approvalStatus, String posted, Boolean showDuplicate,
        String description, String referenceNumber,
		Date dateReversal, String reversed, String postedBy, String journalCategory, String journalSource,
		String currency, String currencySymbol, String batchName, String createdByDescription, String branchCode, String branchName, String lineDescription,
		String approvedByDescription) {
      	
      	this.journalNumber = journalNumber;
      	this.date = date;
      	this.createdBy = createdBy;
      	this.approvedBy = approvedBy;
      	this.accountNumber = accountNumber;
      	this.accountDescription = accountDescription;
      	this.debitAmount = debitAmount;
      	this.creditAmount = creditAmount;
      	this.approvalStatus = approvalStatus;
      	this.posted = posted;
      	this.showDuplicate = showDuplicate;
      	      	
      	this.referenceNumber = referenceNumber;
      	this.dateReversal = dateReversal;
      	this.reversed = reversed;
		this.postedBy = postedBy;
		this.currency = currency;
		this.currencySymbol = currencySymbol;
		this.batchName = batchName;
		this.description = description;
		this.journalCategory = journalCategory;
		this.journalSource = journalSource;
		this.createdByDescription = createdByDescription;
		
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.lineDescription = lineDescription;
		this.approvedByDescription = approvedByDescription;
   }
   
   public String getJournalNumber() {
   	
      return(journalNumber);
      
   }
   
   public String getDate() {
   	
      return(date);
      
   }
   
   public String getCreatedBy() {
   	
      return(createdBy);
      
   }
   
   public String getApprovedBy() {
   	
      return(approvedBy);
      
   }

   public String getAccountNumber() {
   	
      return(accountNumber);
      
   }
   
   public String getAccountDescription() {
   	
   	  return(accountDescription);
   	  
   }
   
   public Double getDebitAmount() {
   	
   	  return(debitAmount);
   	  
   }
   
   public Double getCreditAmount() {
   	
   	  return(creditAmount);
   	  
   }
   
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   	
   }
   
   public String getPosted() {
   	
   	  return posted;
   	
   }
   
   public Boolean getShowDuplicate() {
   	
   	  return(showDuplicate);
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public Date getDateReversal() {
   	
   	  return dateReversal;
   	
   }
   
   public String getReversed() {
   	
   	  return reversed;
   	
   }
   
   public String getPostedBy() {
   	
   	  return postedBy;
   	
   }
   
   public String getJournalCategory() {
   	
   	  return journalCategory;
   	
   }

   public String getJournalSource() {

	   return journalSource;

   }

   public String getCurrency() {

	   return currency;

   }

   public String getCurrencySymbol() {

	   return currencySymbol;

   }
   
   public String getBatchName() {
   	
   	  return batchName;
   	
   }
   
   public String getCreatedByDescription() {
   	
   	  return createdByDescription;
   	
   }
   
   public String getBranchCode() {
	   
	   return branchCode;
	   
   }
   
   public String getBranchName() {
	   
	   return branchName;
	   
   }

   public String getLineDescription() {

	   return lineDescription;

   } 

   public String getApprovedByDescription() {

	   return approvedByDescription;

   }

}
