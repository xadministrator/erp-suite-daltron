package com.struts.jreports.gl.journalregister;

import java.io.Serializable;

public class GlRepBranchJournalRegisterList implements Serializable {
	
	private String brName = null;
	private String brBranchCode = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	
	private GlRepJournalRegisterForm parentBean;
	
	public GlRepBranchJournalRegisterList(GlRepJournalRegisterForm parentBean, 
			String brBranchCode, String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		this.brBranchCode = brBranchCode;
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void setBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}

