package com.struts.jreports.gl.journalregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepJournalRegisterController;
import com.ejb.txn.GlRepJournalRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.GlRepJournalRegisterDetails;

public final class GlRepJournalRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {
      	
/*******************************************************
 Check if user has a session
 *******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("GlRepJournalRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	GlRepJournalRegisterForm actionForm = (GlRepJournalRegisterForm)form;
      	
      	// reset report to null
      	actionForm.setReport(null);

      	String frParam = Common.getUserPermission(user, Constants.GL_REP_JOURNAL_REGISTER_ID);

      	if (frParam != null) {
      	
      		if (frParam.trim().equals(Constants.FULL_ACCESS)) {
      			
      			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
      			if (!fieldErrors.isEmpty()) {
      				
      				saveErrors(request, new ActionMessages(fieldErrors));
      				
      				return mapping.findForward("glRepJournalRegister");
      			}
      			
      		}
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	
/*******************************************************
 Initialize GlRepJournalRegisterController EJB
 *******************************************************/
      	
      	GlRepJournalRegisterControllerHome homeJR = null;
      	GlRepJournalRegisterController ejbJR = null;       
      	
      	try {
      		
      		homeJR = (GlRepJournalRegisterControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/GlRepJournalRegisterControllerEJB", GlRepJournalRegisterControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in GlRepJournalRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbJR = homeJR.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in GlRepJournalRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
/*******************************************************
 -- GL JR Go Action --
 *******************************************************/
      	
      	if(request.getParameter("goButton") != null &&
      			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      		
      		ArrayList list = null;
      		GlRepJournalRegisterDetails details = null;
      		
      		String company = null;
      		
      		if(request.getParameter("goButton") != null) {
      			
      			HashMap criteria = new HashMap();
      			
      			if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        	}
      			
      			if (!Common.validateRequired(actionForm.getJournalNumberFrom())) {
	        		
	        		criteria.put("journalNumberFrom", actionForm.getJournalNumberFrom());
	        	}
      			
      			if (!Common.validateRequired(actionForm.getJournalNumberTo())) {
	        		
	        		criteria.put("journalNumberTo", actionForm.getJournalNumberTo());
	        	}
      			
      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	}
      			
      			if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	}
      			
      			if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        	}
      			
      			if (!Common.validateRequired(actionForm.getSource())) {
	        		
	        		criteria.put("source", actionForm.getSource());
	        	}
      			
      			if (actionForm.getIncludeUnposted()) {	        	
      				
      				criteria.put("includeUnposted", "YES");
      				
      			}
      			
      			if (!actionForm.getIncludeUnposted()) {
      				
      				criteria.put("includeUnposted", "NO");

      			}
      			
      			//save criteria
      			
      			actionForm.setCriteria(criteria);
      			
      		}
      		
      		try {
      			
      			ArrayList branchList = new ArrayList();
      			
      			for(int i=0; i<actionForm.getGlRepBrJournalRegisterListSize(); i++) {
      				
      				GlRepBranchJournalRegisterList adBjrList = (GlRepBranchJournalRegisterList)actionForm.getGlRepBrJournalRegisterListByIndex(i);
      				
      				if(adBjrList.getBranchCheckbox() == true) {                                          
      					
      					AdBranchDetails brDetails = new AdBranchDetails();                                          
      					
      					brDetails.setBrAdCompany(user.getCmpCode());	     	        
      					brDetails.setBrCode(adBjrList.getBrCode());                    
      					
      					branchList.add(brDetails);
      					
      				}
      			}
      			
      			// get company
      			
      			AdCompanyDetails adCmpDetails = ejbJR.getAdCompany(user.getCmpCode());
      			company = adCmpDetails.getCmpName();
      			
      			// execute report
      			
      			list = ejbJR.executeGlRepJournalRegister(actionForm.getCriteria(), actionForm.getOrderBy(), 
      					actionForm.getGroupBy(), branchList, actionForm.getShowEntries(), actionForm.getSummarize(),
      					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			
      		} catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("journalRegister.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in GlRepJournalRegisterAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}
      		
      		if(!errors.isEmpty()) {
      			
      			saveErrors(request, new ActionMessages(errors));
      			return(mapping.findForward("glRepJournalRegister"));
      			
      		}	
      		
      		// fill report parameters, fill report to pdf and set report session
      		
      		Map parameters = new HashMap();
      		parameters.put("company", company);
      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
      		parameters.put("printedBy", user.getUserName());
      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
      		parameters.put("batchName", actionForm.getBatchName());
      		parameters.put("journalNumberFrom", actionForm.getJournalNumberFrom());
      		parameters.put("journalNumberTo", actionForm.getJournalNumberTo());
      		parameters.put("dateFrom", actionForm.getDateFrom());
      		parameters.put("dateTo", actionForm.getDateTo());
      		parameters.put("source", actionForm.getSource());
      		parameters.put("category", actionForm.getCategory());
      		parameters.put("groupBy", actionForm.getGroupBy());
      		parameters.put("orderBy", actionForm.getOrderBy());
      		parameters.put("showEntries", actionForm.getShowEntries() == true ? "YES" : "NO");
      		parameters.put("includeUnposted", actionForm.getIncludeUnposted() == true ? "YES" : "NO");
      		parameters.put("dateTo2", Common.convertStringToSQLDate(actionForm.getDateTo()));
      		
      		String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getGlRepBrJournalRegisterListSize(); j++) {

      			GlRepBranchJournalRegisterList brList = (GlRepBranchJournalRegisterList)actionForm.getGlRepBrJournalRegisterListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBrName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBrName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
      		
      		String filename = null;
      		
      		if(!actionForm.getShowEntries()) {
      			
      			filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepJournalRegister.jasper";
          		
          		if (!new java.io.File(filename).exists()) {
          			
          			filename = servlet.getServletContext().getRealPath("jreports/GlRepJournalRegister.jasper");
          			
          		}
      			
      		} else {
      			
      			if (actionForm.getSummarize()) {
      			
      				filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepJournalRegisterSummary.jasper";

      				if (!new java.io.File(filename).exists()) {

      					filename = servlet.getServletContext().getRealPath("jreports/GlRepJournalRegisterSummary.jasper");

      				}

      			} else {
      				
      				filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepJournalRegisterEntries.jasper";
              		
              		if (!new java.io.File(filename).exists()) {
              			
              			filename = servlet.getServletContext().getRealPath("jreports/GlRepJournalRegisterEntries.jasper");
              			
              		}
          			
      			}
      				
      		}
      		
      		try {
      			
      			Report report = new Report();
      			
      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
      				report.setBytes(
      						JasperRunManager.runReportToPdf(filename, parameters, 
      								new GlRepJournalRegisterDS(list, actionForm.getGroupBy())));   
      				
      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
      				report.setBytes(
      						JasperRunManagerExt.runReportToXls(filename, parameters, 
      								new GlRepJournalRegisterDS(list, actionForm.getGroupBy())));   
      				
      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
      						new GlRepJournalRegisterDS(list, actionForm.getGroupBy())));												    
      				
      			}
      			
      			session.setAttribute(Constants.REPORT_KEY, report);
      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
      			
      		} catch(Exception ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("Exception caught in GlRepJournalRegisterAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			ex.printStackTrace();
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}		    	    
      		
/*******************************************************
 -- GL JR Close Action --
 *******************************************************/
      		
      	} else if (request.getParameter("closeButton") != null) {
      		
      		return(mapping.findForward("cmnMain"));
      		
/*******************************************************
 -- GL JR Load Action --
 *******************************************************/
      		
      	}
      	
      	if(frParam != null) {
      	
      		try {
      			
      			ArrayList list = null;			       			       
      			Iterator i = null;			
      			
      			actionForm.clearSourceList();           	
      			
      			list = ejbJR.getGlJsAll(user.getCmpCode());
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setSourceList((String)i.next());
      					
      				}
      				
      			} 
      			
      			actionForm.clearCategoryList();
      			
      			list = ejbJR.getGlJcAll(user.getCmpCode());
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setCategoryList((String)i.next());
      					
      				}
      				
      			}
      			
      			actionForm.clearBatchNameList();
      			
      			list = ejbJR.getGlJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setBatchNameList((String)i.next());
      					
      				}
      				
      			} 
      			
      		} catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in GlRepJournalRegisterAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}
      		
      		actionForm.reset(mapping, request);
      		
      		// populate GlRepBranchJournalRegisterList
      		
      		ArrayList brList = new ArrayList();
      		actionForm.clearGlRepBrJournalRegisterList();
      		
      		brList = ejbJR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
      		
      		Iterator k = brList.iterator();
      		
      		while(k.hasNext()) {
      			
      			AdBranchDetails brDetails = (AdBranchDetails)k.next();
      			
      			GlRepBranchJournalRegisterList adBjrList = new GlRepBranchJournalRegisterList(actionForm,
      					brDetails.getBrBranchCode(), brDetails.getBrName(),
						brDetails.getBrCode());
      			adBjrList.setBranchCheckbox(true);
      			
      			actionForm.saveGlRepBrJournalRegisterList(adBjrList);
      			
      		}	         		
      		
      		return(mapping.findForward("glRepJournalRegister"));		          
      		
      	} else {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));
      		
      		return(mapping.findForward("cmnMain"));
      		
      	}
      	
      } catch(Exception e) {
      	
      	
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
      	if(log.isInfoEnabled()) {
      		
      		log.info("Exception caught in GlRepJournalRegisterAction.execute(): " + e.getMessage()
      				+ " session: " + session.getId());
      		
      		e.printStackTrace();
      	}   
      	
      	return(mapping.findForward("cmnErrorPage"));   
      	
      }
      
    }
   
}
