package com.struts.jreports.gl.journalregister;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepJournalRegisterDetails;

public class GlRepJournalRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public GlRepJournalRegisterDS(ArrayList list, String groupBy){
   	
   	  Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         GlRepJournalRegisterDetails details = (GlRepJournalRegisterDetails)i.next();
         
         String group = null;
         
         if (groupBy.equals("SOURCE")) {
         	
         	group = details.getJrJrSource();
         	
         } else if (groupBy.equals("CATEGORY")) {
         	
         	group = details.getJrJrCategory();
         	
         } else if (groupBy.equals("BATCH")) {
         	
         	group = details.getJrJrBatch();
         	
         }
         
	     GlRepJournalRegisterData jrData = new GlRepJournalRegisterData(details.getJrJlCoaAccountNumber(),
	     		details.getJrJlCoaAccountDescription(), details.getJrJrDate(), details.getJrJrDescription(), details.getJrJrDocumentNumber(),
				new Double(details.getJrJlDebit()), new Double(details.getJrJlCredit()), group, details.getJrJrReferenceNumber(),
				new Double(details.getJrJrAmount()));
		    
         data.add(jrData);
         
      }
         
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

     if("accountNumber".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getAccountNumber();
     }else if("accountDescription".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getAccountDescription();
     }else if("date".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getDate();
     }else if("description".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getDescription();
     }else if("documentNumber".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getDocumentNumber();
     }else if("debit".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getDebit();
     }else if("credit".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getCredit();      
     }else if("groupBy".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getGroupBy();      
     }else if("referenceNumber".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getReferenceNumber();      
     }else if("amount".equals(fieldName)){
        value = ((GlRepJournalRegisterData)data.get(index)).getAmount();      
     }
     
     return(value);
   }
}
