package com.struts.jreports.gl.journalregister;

import java.util.Date;

public class GlRepJournalRegisterData implements java.io.Serializable {
	
	private String accountNumber = null;
	private String accountDescription = null;
	private Date date = null;
	private String description = null;
	private String documentNumber = null;
	private Double debit = null;
	private Double credit = null;
	private String groupBy = null;
	private String referenceNumber = null;
	private Double amount = null;
	
	public GlRepJournalRegisterData(String accountNumber, String accountDescription, Date date, String description, 
			String documentNumber, Double debit, Double credit, String groupBy, String referenceNumber, Double amount){
		
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.date = date;
		this.description = description;
		this.documentNumber = documentNumber;
		this.debit = debit;
		this.credit = credit;
		this.groupBy = groupBy;
		this.referenceNumber = referenceNumber;
		this.amount = amount;
		
	}
	
	public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public String getAccountDescription() {
		
		return accountDescription;
		
	}
	
	public Date getDate() {
		
		return date;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public Double getDebit() {
		
		return debit;
		
	}
	
	public Double getCredit() {
		
		return credit;
		
	}   
	
	public String getGroupBy() {
		
		return groupBy;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
		
}
