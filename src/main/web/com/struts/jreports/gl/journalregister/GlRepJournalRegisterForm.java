package com.struts.jreports.gl.journalregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlRepJournalRegisterForm extends ActionForm implements Serializable {
	
	private String journalNumberFrom = null;
	private String journalNumberTo = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList(); 
	private String source = null;
	private ArrayList sourceList = new ArrayList();
	private boolean includeUnposted = false;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private boolean showEntries = false;
	private boolean summarize = false;
	
	private String userPermission = new String();
	private ArrayList glRepBrJournalRegisterList = new ArrayList();
	private HashMap criteria = new HashMap();
	
	public String getJournalNumberFrom() {
		
		return journalNumberFrom;
		
	}
	public void setJournalNumberFrom(String journalNumberFrom) {
		
		this.journalNumberFrom = journalNumberFrom;
		
	}
	
	public String getJournalNumberTo() {
		
		return journalNumberTo;
		
	}
	
	public void setJournalNumberTo(String journalNumberTo) {
		
		this.journalNumberTo = journalNumberTo;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getBatchName() {
		
		return batchName;
		
	}
	
	public void setBatchName(String batchName) {
		
		this.batchName = batchName;
		
	}
	
	public ArrayList getBatchNameList() {
		
		return batchNameList;
		
	}
	
	public void setBatchNameList(String batchName) {
		
		batchNameList.add(batchName);
		
	}
	
	public void clearBatchNameList() {
		
		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getSource() {
		
		return source;
		
	}
	
	public void setSource(String source) {
		
		this.source = source;
		
	}
	
	public ArrayList getSourceList() {
		
		return sourceList;
		
	}
	
	public void setSourceList(String source) {
		
		sourceList.add(source);
		
	}
	
	public void clearSourceList() {
		
		sourceList.clear();
		sourceList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getIncludeUnposted() {
		
		return includeUnposted;
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public String getGroupBy() {
		
		return groupBy;
		
	}
	
	public void setGroupBy(String groupBy) {
		
		this.groupBy = groupBy;
		
	}
	
	public ArrayList getGroupByList() {
		
		return groupByList;
		
	}
	
	public String getCategory() {
		
		return category;
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return categoryList;
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
										
	public String getViewType() {
		
		return viewType;
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public boolean getShowEntries() {
		
		return showEntries;
		
	}
	
	public void setShowEntries(boolean showEntries) {
		
		this.showEntries = showEntries;
		
	}
	
	public String getUserPermission(){
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getGlRepBrJournalRegisterList(){
		
		return glRepBrJournalRegisterList.toArray();
		
	}
	
	public GlRepBranchJournalRegisterList getGlRepBrJournalRegisterListByIndex(int index){
		
		return ((GlRepBranchJournalRegisterList)glRepBrJournalRegisterList.get(index));
		
	}
	
	public int getGlRepBrJournalRegisterListSize(){
		
		return(glRepBrJournalRegisterList.size());
	}
	
	public void saveGlRepBrJournalRegisterList(Object newGlRepBrJournalRegisterList){
		
		glRepBrJournalRegisterList.add(newGlRepBrJournalRegisterList);   	  
		
	}
	
	public void clearGlRepBrJournalRegisterList(){
		
		glRepBrJournalRegisterList.clear();
		
	}
	
	public void setGlRepBrJournalRegisterList(ArrayList glRepBrJournalRegisterList) {
		
		this.glRepBrJournalRegisterList = glRepBrJournalRegisterList;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}

	public boolean getSummarize() {

		return summarize;

	}

	public void setSummarize(boolean summarize) {

		this.summarize = summarize;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<glRepBrJournalRegisterList.size(); i++) {
			
			GlRepBranchJournalRegisterList list = (GlRepBranchJournalRegisterList)glRepBrJournalRegisterList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		journalNumberFrom = null;
		journalNumberTo = null;
		dateFrom = null;
		dateTo = null;
		batchName = null;
		source = null;
		includeUnposted = false;
		orderBy = null;
		groupBy = null;
		category = null;
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		showEntries = false;
		summarize = false;
		
		orderByList.clear();
		orderByList.add("DATE");
		orderByList.add("JOURNAL NUMBER");
		
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add("SOURCE");
		groupByList.add("CATEGORY");
		groupByList.add("BATCH");
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("goButton") != null) {
			
			if(dateFrom != null && !Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("journalRegister.error.dateFromInvalid"));
				
			}
			
			if(dateTo != null && !Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("journalRegister.error.dateToInvalid"));
				
			}
			
		}
		
		return errors;
		
	}
	
}