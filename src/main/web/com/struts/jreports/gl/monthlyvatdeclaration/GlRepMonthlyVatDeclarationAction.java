package com.struts.jreports.gl.monthlyvatdeclaration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.joda.time.LocalDate;

import com.ejb.txn.GlRepMonthlyVatDeclarationController;
import com.ejb.txn.GlRepMonthlyVatDeclarationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.GlRepMonthlyVatDeclarationDetails;

public final class GlRepMonthlyVatDeclarationAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRepMonthlyVatDeclarationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         GlRepMonthlyVatDeclarationForm actionForm = (GlRepMonthlyVatDeclarationForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_MONTHLY_VAT_DECLARATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepMonthlyVatDeclaration");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepMonthlyVatDeclarationController EJB
*******************************************************/

         GlRepMonthlyVatDeclarationControllerHome homeMVD = null;
         GlRepMonthlyVatDeclarationController ejbMVD = null;

         try {

            homeMVD = (GlRepMonthlyVatDeclarationControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepMonthlyVatDeclarationControllerEJB", GlRepMonthlyVatDeclarationControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in GlRepMonthlyVatDeclarationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbMVD = homeMVD.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in GlRepMonthlyVatDeclarationAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- GL MVD Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlRepMonthlyVatDeclarationDetails details = null;

         // get company details

            AdCompanyDetails adCmpDetails = ejbMVD.getAdCompany(user.getCmpCode());
            StringBuilder strB = null;

		    try {

		    	if(actionForm.getViewType().equals("RELIEF PURCHASES")){

		    		strB = ejbMVD.executeGlRepReliefPurchases(
		    					Common.convertStringToSQLDate(actionForm.getDateFrom()),
				    		   Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());

		    	} else if(actionForm.getViewType().equals("RELIEF SALES")){

		    		strB = ejbMVD.executeGlRepReliefSales(
		    					Common.convertStringToSQLDate(actionForm.getDateFrom()),
				    		   Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());

		    	} else if(actionForm.getViewType().equals("RELIEF IMPORTATIONS")){

		    		strB = ejbMVD.executeGlRepReliefPurchases(
		    					Common.convertStringToSQLDate(actionForm.getDateFrom()),
				    		   Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());

		    	} else {

		    		details = ejbMVD.executeGlRepMonthlyVatDeclaration(
				    		   Common.convertStringToSQLDate(actionForm.getDateFrom()),
				    		   Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());


		    	}

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
			    log.info("EJBException caught in GlRepMonthlyVatDeclarationAction.execute(): " + ex.getMessage() +
			    " session: " + session.getId());

			}

			return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepMonthlyVatDeclaration"));

		    }
		    StringTokenizer st = null;

		    //get for the month



		    String month = null;
		    String year = null;
		    String monthYear = null;

		    String forTheMonth = actionForm.getForTheMonth();
		    /*if(!forTheMonth.equals(null) && forTheMonth.equals("")){
			    st = new StringTokenizer(forTheMonth, "/");

		        month = st.nextToken();

		        if (month != null && month.length() == 2) {

		        	month = " " + month.substring(0, 1) + "  " + month.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.forTheMonthInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

		        }

		        year = st.nextToken();

		        if (year != null && year.length() == 4) {

		    	    year = " "  + year.substring(0, 1) + "    " + year.substring(1, 2) + "      " + year.substring(2, 3) + "    " + year.substring(3, 4);

		        } else {

		    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.forTheMonthInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

		        }

		        monthYear = month + "   " + year;
		    }*/

		    // get number of sheets attached

	        String numberOfSheetsAttached = actionForm.getNumberOfSheetsAttached();
	        /*System.out.println("----------->numberOfSheetsAttached="+numberOfSheetsAttached);
	        if(!numberOfSheetsAttached.equals(null) && numberOfSheetsAttached.equals("")){
	        	System.out.println("1");
		        if(numberOfSheetsAttached != null){
			        if (numberOfSheetsAttached != null && numberOfSheetsAttached.length() == 2) {
			        	System.out.println("1");
			        	numberOfSheetsAttached = "  " + numberOfSheetsAttached.substring(0, 1) + "    " + numberOfSheetsAttached.substring(1, 2);

		    		} else {

			    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			          		new ActionMessage("monthlyVatDeclaration.error.numberOfSheetsAttachedInvalid"));
			            saveErrors(request, new ActionMessages(errors));

			            return(mapping.findForward("glRepMonthlyVatDeclaration"));

			        }
		        }
	        }*/
		    // get tin number

		    String tinNumber = actionForm.getTinNumber();
		   /* if(!tinNumber.equals(null) && tinNumber.equals("")){
			    if (tinNumber != null && tinNumber.length() == 12) {

				    tinNumber = tinNumber.substring(0, 1) + "  " + tinNumber.substring(1, 2) + "  " + tinNumber.substring(2, 3) + "        " +
				                tinNumber.substring(3, 4) + "   " + tinNumber.substring(4, 5) + "   " + tinNumber.substring(5, 6) + "         " +
						        tinNumber.substring(6, 7) + "  " + tinNumber.substring(7, 8) + "  " + tinNumber.substring(8, 9) + "        " +
					            tinNumber.substring(9, 10) + "  " + tinNumber.substring(10, 11) + "  " + tinNumber.substring(11, 12);

	            } else {

		    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.tinNumberInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

			    }
		    } */
	        // get rdo code

	        String rdoCode = actionForm.getRdoCode();
	        /*if(!rdoCode.equals(null) && rdoCode.equals("")){
		        if (rdoCode != null && rdoCode.length() == 3) {

		        	rdoCode = "" + rdoCode.substring(0, 1) + "   " + rdoCode.substring(1, 2) + "   " + rdoCode.substring(2, 3);

			    } else {

		    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.rdoCodeInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

		        }
	        }*/
		     // get telephone number

	        String telephoneNumber = actionForm.getTelephoneNumber();
	        /*if(!telephoneNumber.equals(null) && telephoneNumber.equals("")){
		        if (telephoneNumber != null && telephoneNumber.length() == 7) {

			        telephoneNumber = telephoneNumber.substring(0, 1) + "  " + telephoneNumber.substring(1, 2) + "  " + telephoneNumber.substring(2, 3) + "  " +
			                          telephoneNumber.substring(3, 4) + "   " + telephoneNumber.substring(4, 5) + "  " +
			                          telephoneNumber.substring(5, 6) + "  " + telephoneNumber.substring(6, 7);

				} else {

		    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.telephoneNumberInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

		        }
	        }  */
		    // get zip code

	        String zipCode = actionForm.getZipCode();
	        /*if(!zipCode.equals(null) && zipCode.equals("")){
		        if (zipCode != null && zipCode.length() == 4) {

		        	zipCode = " " + zipCode.substring(0, 1) + "   " + zipCode.substring(1, 2) + "   " + zipCode.substring(2, 3) + "   " + zipCode.substring(3, 4);

			    } else {

		    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatDeclaration.error.zipCodeInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatDeclaration"));

		        }
	        }*/
		    /*** fill report parameters, fill report to pdf and set report session **/

		    Map parameters = new HashMap();
		    parameters.put("BirDir", servlet.getServletContext().getRealPath("images/bir.gif"));
		    parameters.put("ArrowDir", servlet.getServletContext().getRealPath("images/arrow.gif"));
		    parameters.put("forTheMonth", monthYear);

		    // for amended

		    if (actionForm.getAmended().equals("YES")) {

		        parameters.put("amendedYes", "X");

		    } else if (actionForm.getAmended().equals("NO")) {

		        parameters.put("amendedNo", "X");

		    } else {

		    	parameters.put("amendedYes", "");
		    	parameters.put("amendedNo", "");

		    }

		    parameters.put("numberOfSheetsAttached", numberOfSheetsAttached);
		    parameters.put("tinNumber", tinNumber);
		    parameters.put("rdoCode", rdoCode);
		    parameters.put("telephoneNumber", telephoneNumber);
		    parameters.put("zipCode", zipCode);
		    parameters.put("lineOfBusiness", actionForm.getLineOfBusiness());
		    parameters.put("taxpayersName", actionForm.getTaxpayersName());
		    parameters.put("registeredAddress", actionForm.getRegisteredAddress());


		    // for relief

		    if (actionForm.getRelief().equals("YES")) {

		        parameters.put("reliefYes", "X");
		        parameters.put("reliefDescription", actionForm.getReliefDescription());

		    } else if (actionForm.getRelief().equals("NO")) {

		        parameters.put("reliefNo", "X");

		    } else {

		    	parameters.put("reliefYes", "");
		    	parameters.put("reliefNo", "");

		    }


		    // for computation details
		    double transitionalPresumptiveInputTax = new Double(Common.convertStringMoneyToDouble(actionForm.getTransitionalPresumptiveInputTax(), Constants.MONEY_RATE_PRECISION));
		    double carriedOverFromPreviousReturnPeriod = new Double(Common.convertStringMoneyToDouble(actionForm.getCarriedOverReturn(), Constants.MONEY_RATE_PRECISION));
		    double onTaxableGoodsServices = details.getOnTaxableGoods();

		    double totalAvailableInputTaxes =transitionalPresumptiveInputTax + carriedOverFromPreviousReturnPeriod + onTaxableGoodsServices;

		    parameters.put("totalAvailableInputTaxes", totalAvailableInputTaxes);

		    double advancePayments = new Double(Common.convertStringMoneyToDouble(actionForm.getAdvancePayments(), Constants.MONEY_RATE_PRECISION));
			double creditableValueAddedTaxWithheld = new Double(Common.convertStringMoneyToDouble(actionForm.getCreditableVatW(), Constants.MONEY_RATE_PRECISION));
			double vatPaidInReturn = new Double(Common.convertStringMoneyToDouble(actionForm.getVatPaidInReturn(), Constants.MONEY_RATE_PRECISION));



		    double totalTaxCreditsPayments = advancePayments + creditableValueAddedTaxWithheld + vatPaidInReturn;

		    parameters.put("totalTaxCreditsPayments", totalTaxCreditsPayments);




		    double surcharge = new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION));
		    double interest = new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION));
		    double compromise = new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION));



		    double totalPenalties = surcharge + interest + compromise;

		    parameters.put("totalPenalties", totalPenalties);


		    double totalAllowableInputTax = totalAvailableInputTaxes - (new Double(Common.convertStringMoneyToDouble(actionForm.getAnyRefund(), Constants.MONEY_RATE_PRECISION)));
		    System.out.println("totalAllowableInputTax: " + totalAllowableInputTax);
		    parameters.put("totalAllowableInputTax", totalAllowableInputTax);



		    double netVatPayable = details.getTaxDue() - totalAllowableInputTax;

		    parameters.put("netVatPayable", netVatPayable);




		    double taxPayableOverpayment = netVatPayable - totalTaxCreditsPayments;
		    parameters.put("taxPayableOverpayment", taxPayableOverpayment);

		    double totalAmountPayable = totalPenalties + taxPayableOverpayment;
		    parameters.put("totalAmountPayable", totalAmountPayable);

			parameters.put("industryClassification", actionForm.getIndustryClassification());
			parameters.put("atcCode", actionForm.getAtcCode());
			parameters.put("transitionalPresumptiveInputTax", new Double(Common.convertStringMoneyToDouble(actionForm.getTransitionalPresumptiveInputTax(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("carriedOverFromPreviousReturnPeriod", new Double(Common.convertStringMoneyToDouble(actionForm.getCarriedOverReturn(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("anyRefundTccClaimed", new Double(Common.convertStringMoneyToDouble(actionForm.getAnyRefund(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("advancePayments", new Double(Common.convertStringMoneyToDouble(actionForm.getAdvancePayments(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("creditableValueAddedTaxWithheld", new Double(Common.convertStringMoneyToDouble(actionForm.getCreditableVatW(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("vatPaidInReturn", new Double(Common.convertStringMoneyToDouble(actionForm.getVatPaidInReturn(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("surcharge", new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("interest", new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("compromise", new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION)));

		    // for payment details

		    parameters.put("cashBankAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCashBankAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("draweeBankAgencyCheck", actionForm.getCheckBank());
		    parameters.put("checkNumber", actionForm.getCheckNumber());

		    String checkDate = actionForm.getCheckDate();
		    String mnth = null;
		    String day = null;
		    String yr = null;

		    if (!checkDate.equals(null) && checkDate.length() != 0 ) {

			    st = new StringTokenizer(checkDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        checkDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("checkDate", checkDate);
		    parameters.put("checkAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCheckAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("taxDebitNumber", actionForm.getTaxDebitNumber());
		    parameters.put("taxDebitAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getTaxDebitAmount(), Constants.MONEY_RATE_PRECISION)));

		    String taxDebitDate = actionForm.getTaxDebitDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!taxDebitDate.equals(null) && taxDebitDate.length() != 0 ) {

			    st = new StringTokenizer(taxDebitDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        taxDebitDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("taxDebitDate", taxDebitDate);
		    parameters.put("draweeBankAgencyOther", actionForm.getOtherBank());
		    parameters.put("otherNumber", actionForm.getOtherNumber());

		    String otherDate = actionForm.getOtherDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!otherDate.equals(null) && otherDate.length() != 0 ) {

			    st = new StringTokenizer(otherDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("monthlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepMonthlyVatReturn"));

		        }

		        otherDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("otherDate", otherDate);
		    parameters.put("otherAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getOtherAmount(), Constants.MONEY_RATE_PRECISION)));

		    // for signatories details

		    parameters.put("presidentSignature", actionForm.getPresidentSignature());
		    parameters.put("treasurerSignature", actionForm.getTreasurerSignature());
		    parameters.put("titleOfSignatory1", actionForm.getTitleOfSignatory1());
		    parameters.put("titleOfSignatory2", actionForm.getTitleOfSignatory2());
		    parameters.put("tinOfTaxAgent", actionForm.getTinOfTaxAgent());
		    parameters.put("taxAgentAccreditation", actionForm.getTaxAgentAccreditation());
		    parameters.put("province", actionForm.getProvince());

		    // IRC details


		    parameters.put("tin", adCmpDetails.getCmpTin());
		    parameters.put("taxPayerName", adCmpDetails.getCmpTaxPayerName());
		    parameters.put("contacts", adCmpDetails.getCmpContact());
		    parameters.put("phoneNumber", adCmpDetails.getCmpPhone());
		    parameters.put("email", adCmpDetails.getCmpEmail());

		    parameters.put("mailSectionNo", adCmpDetails.getCmpMailSectionNo());
		    parameters.put("mailLotNo", adCmpDetails.getCmpMailLotNo());
		    parameters.put("mailStreet", adCmpDetails.getCmpMailStreet());
		    parameters.put("mailPoBox", adCmpDetails.getCmpMailPoBox());
		    parameters.put("mailCountry", adCmpDetails.getCmpMailCountry());
		    parameters.put("mailProvince", adCmpDetails.getCmpMailProvince());
		    parameters.put("mailPostOffice", adCmpDetails.getCmpMailPostOffice());
		    parameters.put("mailCareOff", adCmpDetails.getCmpMailCareOff());
		    parameters.put("taxPeriodFrom", adCmpDetails.getCmpTaxPeriodFrom());
		    parameters.put("taxPeriodTo", adCmpDetails.getCmpTaxPeriodTo());
		    parameters.put("publicOfficeName", adCmpDetails.getCmpPublicOfficeName());
		    parameters.put("dateAppointment", adCmpDetails.getCmpDateAppointment());

		    String filename = null;

		    if (actionForm.getReportType().equals("GST RETURN")) {
		    	System.out.println("GST RETURN");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepGSTReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepGSTReturn.jasper");

			    }
		    } else {

		    	String jasperFileName = Common.getLookUpJasperFile("MONTHLY_VAT_DECLARATION_LOOK_UP", actionForm.getReportType(), user.getCompany());

		    	if(jasperFileName.equals("")) {
		    		jasperFileName = "GlRepMonthlyVatDeclaration.jasper";
		    	}

			    filename = "/opt/ofs-resources/" + user.getCompany() + "/" + jasperFileName;
			    System.out.println("jasper path is: " + filename);

			    if (!new java.io.File(filename).exists()) {

		        	filename = servlet.getServletContext().getRealPath("jreports/GlRepMonthlyVatDeclaration.jasper");

		        }


		    }

		    try {

		    	Report report = new Report();
		    	report.setBytes("THIS IS A TEST".getBytes());
                report.setViewType(Constants.REPORT_VIEW_TYPE_TEXT);

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters,
				        new GlRepMonthlyVatDeclarationDS(details)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters,
					        new GlRepMonthlyVatDeclarationDS(details)));
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_PURCHASES)){

                   report.setViewType(Constants.REPORT_VIEW_TYPE_RELIEF_PURCHASES);
                   report.setBytes(strB.toString().getBytes());

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_SALES)){

                   report.setViewType(Constants.REPORT_VIEW_TYPE_RELIEF_SALES);
                   report.setBytes(strB.toString().getBytes());

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_IMPORTATIONS)){

                   report.setViewType(Constants.REPORT_VIEW_TYPE_RELIEF_IMPORTATIONS);
                   report.setBytes(strB.toString().getBytes());

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
				       new GlRepMonthlyVatDeclarationDS(details)));

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepMonthlyVatDeclarationAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }

/*******************************************************
   -- Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- GL MVD Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	         	try {
	         		ArrayList list = null;
			        Iterator i = null;

			        actionForm.clearProvinceList();

			        list = ejbMVD.getAdLvProvincesAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setProvinceList(Constants.GLOBAL_BLANK);
	            	} else {

	            		actionForm.setProvinceList(Constants.GLOBAL_BLANK);
	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setProvinceList((String)i.next());

	            		}

	            	}



	            	actionForm.clearReportTypeList();

			        list = ejbMVD.getAdLvReportTypeAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {

	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            		i = list.iterator();

	            		while (i.hasNext()) {


	            		    actionForm.setReportTypeList((String)i.next());

	            		}

	            	}



				} catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepOutputTaxAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

			    }

	            actionForm.reset(mapping, request);



	         // get company details

	            AdCompanyDetails adCmpDetails = ejbMVD.getAdCompany(user.getCmpCode());

	            String REGISTER_ADDRSS =  adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();


	            LocalDate today = new LocalDate(new Date());


			    actionForm.setDateFrom(Common.convertSQLDateToString(today.withDayOfMonth(1).toDate()));
			    actionForm.setDateTo(Common.convertSQLDateToString(today.dayOfMonth().withMaximumValue().toDate()));
	            actionForm.setTinNumber(adCmpDetails.getCmpTin());
	            actionForm.setTaxpayersName(adCmpDetails.getCmpName());
		        actionForm.setTelephoneNumber(adCmpDetails.getCmpPhone());
		        actionForm.setRegisteredAddress(REGISTER_ADDRSS);
	            actionForm.setZipCode(adCmpDetails.getCmpZip());
	            actionForm.setLineOfBusiness(adCmpDetails.getCmpIndustry());
	            actionForm.setRdoCode(adCmpDetails.getCmpRevenueOffice());


	            return(mapping.findForward("glRepMonthlyVatDeclaration"));

			 } else {

			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));

			    return(mapping.findForward("cmnMain"));

			 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in GlRepMonthlyVatDeclarationAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
