/*
 * opt/jreport/GlRepMonthlyVatDeclarationData.java
 *
 * Created on March 29, 2004, 11:30 AM
 *
 * @author  Neil Andrew M. Ajero
 */
package com.struts.jreports.gl.monthlyvatdeclaration; 


public class GlRepMonthlyVatDeclarationData implements java.io.Serializable {

    private Double grossSalesReceipt = null;
    private Double taxDue = null;
    private Double onTaxableGoodsServices = null;
    private Double outputTax = null;
    private Double inputTax = null;
    private Double salesAmount = null;
    private Double revenue = null;
    private Double salesAmountExempt = null;
    private Double salesAmountZeroRated = null;
    private Double totalSalesExemptZero = null;
    private Double revenueAmountExempt = null;
    private Double revenueAmountZeroRated = null;
    private Double totalRevenueExemptZero = null;
    

    public GlRepMonthlyVatDeclarationData(Double grossSalesReceipt, Double taxDue,
    Double onTaxableGoodsServices, Double outputTax, Double inputTax, Double salesAmount, Double revenue,Double salesAmountExempt, Double salesAmountZeroRated , Double totalSalesExemptZero , Double revenueAmountExempt, Double revenueAmountZeroRated, Double totalRevenueExemptRated ) {

        this.grossSalesReceipt = grossSalesReceipt;
        this.taxDue = taxDue;
        this.onTaxableGoodsServices = onTaxableGoodsServices;
        this.outputTax = outputTax;
        this.inputTax = inputTax;
        this.salesAmount = salesAmount;
        this.revenue = revenue;
        this.salesAmountExempt = salesAmountExempt;
        this.salesAmountZeroRated = salesAmountZeroRated;
        this.totalSalesExemptZero = totalSalesExemptZero;
        this.revenueAmountExempt = revenueAmountExempt;
        this.revenueAmountZeroRated = revenueAmountZeroRated;
        this.totalRevenueExemptZero = totalRevenueExemptRated;
        
    }

    public Double getGrossSalesReceipt() {

        return grossSalesReceipt;

    }
    
    public Double getTaxDue() {
    	
    	return taxDue;
    	
    }
    
    public Double getOnTaxableGoodsServices() {
    	
    	return onTaxableGoodsServices;
    	
    }
    
    public Double getOutputTax() {
    	
    	return outputTax;
    	
    }
    
    public Double getInputTax() {
    	
    	return inputTax;
    	
    }
    
    public Double getSalesAmount() {
    	
    	return salesAmount;
    	
    }
    
    public Double getRevenue() {
    	
    	return revenue;
    	
    }
    
    public Double getSalesAmountExempt() {
    	
    	return salesAmountExempt;
    	
    }
    
    public Double getSalesAmountZeroRated() {
    	
    	return salesAmountZeroRated;
    	
    }
    

    public Double getTotalSalesExemptZero(){
    	return totalSalesExemptZero;
    
    }
    
    public Double getRevenueAmountExempt(){
    	return revenueAmountExempt;
    }
    
    public Double getRevenueAmountZeroRated(){
    	return revenueAmountZeroRated;
    }
    
    public Double getTotalRevenueExemptZero(){
    	return totalRevenueExemptZero;
    
    }

} 