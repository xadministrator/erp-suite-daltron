package com.struts.jreports.gl.monthlyvatdeclaration;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlRepMonthlyVatDeclarationForm extends ActionForm implements Serializable {

   private String dateFrom = null;
   private String dateTo = null;
   private String forTheMonth = null;
   private String amended = null;
   private ArrayList amendedList = new ArrayList();
   private String numberOfSheetsAttached = null;
   private String tinNumber = null;
   private String rdoCode = null;
   private String lineOfBusiness = null;
   private String taxpayersName = null;
   private String telephoneNumber = null;
   private String registeredAddress = null;
   private String zipCode = null;
   private String relief = null;
   private ArrayList reliefList = new ArrayList();
   private String reliefDescription = null;
   private String industryClassification = null;
   private String atcCode = null;
   private String transitionalPresumptiveInputTax = null;
   private String carriedOverReturn = null;
   private String anyRefund = null;
   private String advancePayments = null;
   private String creditableVatW = null;
   private String vatPaidInReturn = null;
   private String surcharge = null;
   private String interest = null;
   private String compromise = null;
   private String checkBank = null;
   private String otherBank = null;
   private String checkNumber = null;
   private String taxDebitNumber = null;
   private String otherNumber = null;
   private String checkDate = null;
   private String taxDebitDate = null;
   private String otherDate = null;
   private String cashBankAmount = null;
   private String checkAmount = null;
   private String taxDebitAmount = null;
   private String otherAmount = null; 
   private String presidentSignature = null;
   private String treasurerSignature = null;
   private String titleOfSignatory1 = null;
   private String titleOfSignatory2 = null;
   private String tinOfTaxAgent = null;
   private String taxAgentAccreditation = null;
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   
   private String province = null;
   private ArrayList provinceList = new ArrayList();
   
   private String reportType = null;
   private ArrayList reportTypeList = new ArrayList();

   private String userPermission = new String();

   public String getDateFrom() {
   	
   	  return(dateFrom);
   
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return(dateTo);
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getForTheMonth() {
   	
   	  return forTheMonth;
   	  
   }
   
   public void setForTheMonth(String forTheMonth) {
   	
   	  this.forTheMonth = forTheMonth;
   	  
   }
   
   public String getAmended() {
   	
   	  return amended;   	
   
   }
   
   public void setAmended(String amended) {
   	
   	  this.amended = amended;
   
   }
   
   public ArrayList getAmendedList() {
   	
   	  return amendedList;
   
   }   
    
   public String getNumberOfSheetsAttached() {
   	
   	  return numberOfSheetsAttached;
   	  
   }
   
   public void setNumberOfSheetsAttached(String numberOfSheetsAttached) {
   	
   	  this.numberOfSheetsAttached = numberOfSheetsAttached;
   	  
   }
   
   public String getTinNumber() {
   	
   	  return tinNumber;
   	  
   }
   
   public void setTinNumber(String tinNumber) {
   	
   	  this.tinNumber = tinNumber;
   	  
   }
   
   public String getRdoCode() {
   	
   	  return rdoCode;
   	  
   }
   
   public void setRdoCode(String rdoCode) {
   	
   	  this.rdoCode = rdoCode;
   	  
   }
   
   public String getLineOfBusiness() {
   	
   	  return lineOfBusiness;
   	  
   }
   
   public void setLineOfBusiness(String lineOfBusiness) {
   	
   	  this.lineOfBusiness = lineOfBusiness;
   	  
   }
   
   public String getTaxpayersName() {
   	
   	  return taxpayersName;
   	  
   }
   
   public void setTaxpayersName(String taxpayersName) {
   	
   	  this.taxpayersName = taxpayersName;
   	  
   }
   
   public String getTelephoneNumber() {
   	
   	  return telephoneNumber;
   	  
   }
   
   public void setTelephoneNumber(String telephoneNumber) {
   	
   	  this.telephoneNumber = telephoneNumber;
   	  
   }
   
   public String getRegisteredAddress() {
   	
   	  return registeredAddress;
   	  
   }
   
   public void setRegisteredAddress(String registeredAddress) {
   	
   	  this.registeredAddress = registeredAddress;
   	  
   }
   
   public String getZipCode() {
   	
   	  return zipCode;
   	  
   }
   
   public void setZipCode(String zipCode) {
   	
   	  this.zipCode = zipCode;
   	  
   }
   
   public String getRelief() {
   	
   	  return relief;   	
   
   }
   
   public void setRelief(String relief) {
   	
   	  this.relief = relief;
   
   }
   
   public ArrayList getReliefList() {
   	
   	  return reliefList;
   
   } 
   
   public String getReliefDescription() {
   	
   	  return reliefDescription;
   	  
   }
   
   public void setReliefDescription(String reliefDescription) {
   	
   	  this.reliefDescription = reliefDescription;
   	  
   }

   public String setCloseButton() {
   	
   	  return closeButton;
   	
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission() {

      return userPermission;

   } 

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getIndustryClassification() {
   	
   	  return industryClassification;
   	  
   }
   
   public void setIndustryClassification(String industryClassification) {
   	
   	  this.industryClassification = industryClassification;
   	  
   }
   
   public String getAtcCode() {
   	
   	  return atcCode;
   	  
   }
   
   public void setAtcCode(String atcCode) {
   	
   	  this.atcCode = atcCode;
   	  
   }
   
   public String getTransitionalPresumptiveInputTax() {
   	   
   	  return transitionalPresumptiveInputTax;
   	  
   }
   
   public void setTransitionalPresumptiveInputTax(String transitionalPresumptiveInputTax) {
   	
   	  this.transitionalPresumptiveInputTax = transitionalPresumptiveInputTax;
   	  
   }
   
   public String getCarriedOverReturn() {
   	
   	  return carriedOverReturn;
   	  
   }
   
   public void setCarriedOverReturn(String carriedOverReturn) {
   	
   	  this.carriedOverReturn = carriedOverReturn;
   	  
   }
   
   public String getAnyRefund() {
   	
   	  return anyRefund;
   	  
   }
   
   public void setAnyRefund(String anyRefund) {
   	
   	  this.anyRefund = anyRefund;
   	  
   }
   
   public String getAdvancePayments() {
   	
   	  return advancePayments;
   
   }
   
   public void setAdvancePayments(String advancePayments) {
   	
   	  this.advancePayments = advancePayments;
   	  
   }
   
   public String getCreditableVatW() {
   	
   	  return creditableVatW;
   	  
   }
   
   public void setCreditableVatW(String creditableVatW) {
   	
   	  this.creditableVatW = creditableVatW;
   	  
   }
   
   public String getVatPaidInReturn() {
   	
   	  return vatPaidInReturn;
   	  
   }   
   
   public void setVatPaidInReturn(String vatPaidInReturn) {
   	
   	  this.vatPaidInReturn = vatPaidInReturn;
   	  
   }
   
   public String getSurcharge() {
   	
   	  return surcharge;
   	  
   }
   
   public void setSurcharge(String surcharge) {
   	
   	  this.surcharge = surcharge;
   	  
   }
   
   public String getInterest() {
   	
   	  return interest;
   	  
   }
   
   public void setInterest(String interest) {
   	
   	  this.interest = interest;
   	  
   }
   
   public String getCompromise() {
   	  
   	  return compromise;
   	  
   }
   
   public void setCompromise(String compromise) {
   	
   	  this.compromise = compromise;
   	  
   }
   
   public String getCheckBank() {
   	
   	  return checkBank;
   	    
   }
   
   public void setCheckBank(String checkBank) {
   	
   	 this.checkBank = checkBank;
   	 
   }
   
   public String getOtherBank() {
   	
   	  return otherBank;
   	  
   }
   
   public void setOtherBank(String otherBank) {
   	
   	  this.otherBank = otherBank;
   	  
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	  
   }
   
   public void setCheckNumber(String checkNumber) {
   	
   	  this.checkNumber = checkNumber;
   	  
   }
   
   public String getTaxDebitNumber() {
   	
   	  return taxDebitNumber;
   	  
   }
   
   public void setTaxDebitNumber(String taxDebitNumber) {
   	
   	  this.taxDebitNumber = taxDebitNumber;
   	  
   }
   
   public String getOtherNumber() {
   	  
   	  return otherNumber;
   	  
   }
   
   public void setOtherNumber(String otherNumber) {
   	
   	  this.otherNumber = otherNumber;
   	  
   }
   
   public String getCheckDate() {
   	  
   	  return checkDate;
   	  
   }
   
   public void setCheckDate(String checkDate) {
   	
   	  this.checkDate = checkDate;
   	  
   }
   
   public String getTaxDebitDate() {
   	
   	  return taxDebitDate;
   	  
   }
   
   public void setTaxDebitDate(String taxDebitDate) {
   	
   	  this.taxDebitDate = taxDebitDate;
   	  
   }
   
   public String getOtherDate() {
   	
   	  return otherDate;
   	  
   }
   
   public void setOtherDate(String otherDate) {
   	
   	  this.otherDate = otherDate;
   	  
   }
   
   public String getCashBankAmount() {
   	
   	  return cashBankAmount;
   	  
   }
   
   public void setCashBankAmount(String cashBankAmount) {
   	
   	  this.cashBankAmount = cashBankAmount;
   	  
   }
   
   public String getCheckAmount() {
   	
   	  return checkAmount;
   	  
   }
   
   public void setCheckAmount(String checkAmount) {
   	  
   	  this.checkAmount = checkAmount;
   	  
   }
   
   public String getTaxDebitAmount() {
   	
   	  return taxDebitAmount;
   	  
   }
   
   public void setTaxDebitAmount(String taxDebitAmount) {
   	
   	  this.taxDebitAmount = taxDebitAmount;
   	  
   }
   
   public String getOtherAmount() {
   	
   	  return otherAmount;
   	  
   }
   
   public void setOtherAmount(String otherAmount) {
   	
   	  this.otherAmount = otherAmount;
   	  
   }
   
   public String getPresidentSignature() {
   	
   	  return presidentSignature;
   	  
   }
   
   public void setPresidentSignature(String presidentSignature) {
   	
   	  this.presidentSignature = presidentSignature;
   	  
   }
   
   public String getTreasurerSignature() {
   	
   	  return treasurerSignature;
   	  
   }
   
   public void setTreasurerSignature(String treasurerSignature) {
   	
   	  this.treasurerSignature = treasurerSignature;
   	  
   }
   
   public String getTitleOfSignatory1() {
   	
   	  return titleOfSignatory1;
   	  
   }
   
   public void setTitleOfSignatory1(String titleOfSignatory1) {
   	
   	  this.titleOfSignatory1 = titleOfSignatory1;
   	  
   }
   
   public String getTitleOfSignatory2() {
   	
   	  return titleOfSignatory2;
   	  
   }
   
   public void setTitleOfSignatory2(String titleOfSignatory2) {
   	
   	  this.titleOfSignatory2 = titleOfSignatory2;
   	  
   }
   
   public String getTinOfTaxAgent() {
   	
   	  return tinOfTaxAgent;
   	  
   }
   
   public void setTinOfTaxAgent(String tinOfTaxAgent) {
   	
   	  this.tinOfTaxAgent = tinOfTaxAgent;
   	  
   }
   
   public String getTaxAgentAccreditation() {
   	
   	  return taxAgentAccreditation;
   	  
   }
   
   public void setTaxAgentAccreditation(String taxAgentAccreditation) {
   	
   	  this.taxAgentAccreditation = taxAgentAccreditation;
   	  
   }
   
   public String getProvince(){
	   
   	  return(province);
   	  
   }
   
   public void setProvince(String province){
	   
   	  this.province = province;
   	  
   }
   
   public ArrayList getProvinceList(){
	   
   	  return provinceList;
   	  
   }
   
   
   public void setProvinceList(String province) {

		provinceList.add(province);

	}

	public void clearProvinceList() {

		provinceList.clear();

	}
   
   
   
   public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();

	}
   
   
   
   

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
		dateFrom = null;
		dateTo = null;
		forTheMonth = null;
		amendedList.clear();
		amendedList.add(Constants.GLOBAL_BLANK);
		amendedList.add("YES");
		amendedList.add("NO");
		amended = Constants.GLOBAL_BLANK;
		numberOfSheetsAttached = null;
		rdoCode = null;
		reliefList.clear();
		reliefList.add(Constants.GLOBAL_BLANK);
		reliefList.add("YES");
		reliefList.add("NO");
		relief = Constants.GLOBAL_BLANK;
		reliefDescription = null;
		industryClassification = null;
		atcCode = null;
		transitionalPresumptiveInputTax = null;
		carriedOverReturn = null;
		anyRefund = null;
		advancePayments = null;
		creditableVatW = null;
		vatPaidInReturn = null;
		surcharge = null;
		interest = null;
		compromise = null;
		checkBank = null;
		otherBank = null;
		checkNumber = null;
		taxDebitNumber = null;
		otherNumber = null;
		checkDate = null;
		taxDebitDate = null;
		otherDate = null;
		cashBankAmount = null;
		checkAmount = null;
		taxDebitAmount = null;
		otherAmount = null;
		presidentSignature = null;
		treasurerSignature = null;
		titleOfSignatory1 = null;
		titleOfSignatory2 = null;
		tinOfTaxAgent = null;
		taxAgentAccreditation = null;	  
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_RELIEF_PURCHASES);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_RELIEF_SALES);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_RELIEF_IMPORTATIONS);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		goButton = null;
		closeButton = null;

   }
   
   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = new ActionErrors();
      
      if(request.getParameter("goButton") != null) { 
      
		 if(Common.validateRequired(dateFrom)) {
		 	
	        errors.add("dateFrom", 
	           new ActionMessage("monthlyVatDeclaration.error.dateFromRequired"));
	            
		 }
		 
		 if(!Common.validateDateFormat(dateFrom)) {
		 	
	        errors.add("dateFrom", 
	           new ActionMessage("monthlyVatDeclaration.error.dateFromInvalid"));
	            
		 }
		 
		 if(Common.validateRequired(dateTo)) {
		 	
	        errors.add("dateTo", 
	           new ActionMessage("monthlyVatDeclaration.error.dateToRequired"));
	            
	     }
	     	 
	     if(!Common.validateDateFormat(dateTo)) {
	     	
	        errors.add("dateTo", 
	           new ActionMessage("monthlyVatDeclaration.error.dateToInvalid"));
	            
	     }

	     /*if(!Common.validateTaxDateFormat(forTheMonth)) {
	     	
	        errors.add("forTheMonth", 
	           new ActionMessage("monthlyVatDeclaration.error.forTheMonthInvalid"));
	            
	     }
	     
	     if(!Common.validateNumberFormat(numberOfSheetsAttached)) {
	     	
	        errors.add("numberOfSheetsAttached", 
	           new ActionMessage("monthlyVatDeclaration.error.numberOfSheetsAttachedInvalid"));
	            
	     }   
	     
	     if(!Common.validateNumberFormat(rdoCode)) {
	     	
	        errors.add("rdoCode",
	           new ActionMessage("monthlyVatDeclaration.error.rdoCodeInvalid"));
	            
	     }
		 
		 if((relief.equals("YES")) && Common.validateRequired(reliefDescription)) {
		 	
	        errors.add("reliefDescription", 
	           new ActionMessage("monthlyVatDeclaration.error.reliefDescriptionRequired"));
	            
		 }
		 
         if (!Common.validateMoneyFormat(transitionalPresumptiveInputTax)) {

            errors.add("transitionalPresumptiveInputTax",
               new ActionMessage("monthlyVatDeclaration.error.transitionalPresumptiveInputTaxInvalid"));

         }
         
         if (!Common.validateMoneyFormat(carriedOverReturn)) {

            errors.add("carriedOverReturn",
               new ActionMessage("monthlyVatDeclaration.error.carriedOverReturnInvalid"));

         }    
         
         if (!Common.validateMoneyFormat(anyRefund)) {

            errors.add("anyRefund",
               new ActionMessage("monthlyVatDeclaration.error.anyRefundInvalid"));

         }     
         
         if (!Common.validateMoneyFormat(advancePayments)) {

            errors.add("advancePayments",
               new ActionMessage("monthlyVatDeclaration.error.advancePaymentsInvalid"));

         }    
         
         if (!Common.validateMoneyFormat(creditableVatW)) {

            errors.add("creditableVatW",
               new ActionMessage("monthlyVatDeclaration.error.creditableVatWInvalid"));

         }   
         
         if (!Common.validateMoneyFormat(vatPaidInReturn)) {

            errors.add("vatPaidInReturn",
               new ActionMessage("monthlyVatDeclaration.error.vatPaidInReturnInvalid"));

         }    
         
         if (!Common.validateMoneyFormat(surcharge)) {

            errors.add("surcharge",
               new ActionMessage("monthlyVatDeclaration.error.surchargeInvalid"));

         }  
         
         if (!Common.validateMoneyFormat(interest)) {

            errors.add("interest",
               new ActionMessage("monthlyVatDeclaration.error.interestInvalid"));

         }  
         
         if (!Common.validateMoneyFormat(compromise)) {

            errors.add("compromise",
               new ActionMessage("monthlyVatDeclaration.error.compromiseInvalid"));

         }  
         
         if (!Common.validateMoneyFormat(cashBankAmount)) {

            errors.add("cashBankAmount",
               new ActionMessage("monthlyVatDeclaration.error.cashBankAmountInvalid"));

         }    
         
         if (!Common.validateMoneyFormat(checkAmount)) {

            errors.add("checkAmount",
               new ActionMessage("monthlyVatDeclaration.error.checkAmountInvalid"));

         }  
         
         if (!Common.validateMoneyFormat(taxDebitAmount)) {

            errors.add("taxDebitAmount",
               new ActionMessage("monthlyVatDeclaration.error.taxDebitAmountInvalid"));

         }           
         
         if (!Common.validateMoneyFormat(otherAmount)) {

            errors.add("otherAmount",
               new ActionMessage("monthlyVatDeclaration.error.otherAmountInvalid"));

         }
         
	     if(!Common.validateNumberFormat(checkNumber)) {
	     	
	        errors.add("checkNumber", 
	           new ActionMessage("monthlyVatDeclaration.error.checkNumberInvalid"));
	            
	     }  
	     
	     if(!Common.validateNumberFormat(taxDebitNumber)) {
	     	
	        errors.add("taxDebitNumber", 
	           new ActionMessage("monthlyVatDeclaration.error.taxDebitNumberInvalid"));
	            
	     }
	     
	     if(!Common.validateNumberFormat(otherNumber)) {
	     	
	        errors.add("otherNumber", 
	           new ActionMessage("monthlyVatDeclaration.error.otherNumberInvalid"));
	            
	     }
	     
		 if(!Common.validateDateFormat(checkDate)) {
		 	
	        errors.add("checkDate", 
	           new ActionMessage("monthlyVatDeclaration.error.checkDateInvalid"));
	            
		 }	
		 
		 if(!Common.validateDateFormat(taxDebitDate)) {
		 	
	        errors.add("taxDebitDate", 
	           new ActionMessage("monthlyVatDeclaration.error.taxDebitDateInvalid"));
	            
		 }	
		 
		 if(!Common.validateDateFormat(otherDate)) {
		 	
	        errors.add("otherDate", 
	           new ActionMessage("monthlyVatDeclaration.error.otherDateInvalid"));
	            
		 }
		 
		 // added validations
		 
		 if(Common.validateRequired(forTheMonth)) {
		 	
	            errors.add("forTheMonth", new ActionMessage("monthlyVatDeclaration.error.forTheMonthRequired"));
	            
		 }
		 
		 if(Common.validateRequired(tinNumber)) {
		 	
	            errors.add("tinNumber", new ActionMessage("monthlyVatDeclaration.error.tinNumberRequired"));
	            
		 }		 			 	 	 	     	
		 
		 if(Common.validateRequired(rdoCode)) {
		 	
	            errors.add("rdoCode", new ActionMessage("monthlyVatDeclaration.error.rdoCodeRequired"));
	            
		 }
		 
		 if(Common.validateRequired(telephoneNumber)) {
		 	
	            errors.add("telephoneNumber", new ActionMessage("monthlyVatDeclaration.error.telephoneNumberRequired"));
	            
		 }		 			 	 	 	     	
		 
		 if(Common.validateRequired(zipCode)) {
		 	
	            errors.add("zipCode", new ActionMessage("monthlyVatDeclaration.error.zipCodeRequired"));
	            
		 }		 
		 
		 if(Common.validateRequired(numberOfSheetsAttached)) {
		 	
	            errors.add("numberOfSheetsAttached", new ActionMessage("monthlyVatDeclaration.error.numberOfSheetsAttachedRequired"));
	            
		 }		 */	      	     	                                                                                                		 		 				 	 			 	 	 	     	     	     
	     	
      }
      
      return(errors);
      
   }               
}