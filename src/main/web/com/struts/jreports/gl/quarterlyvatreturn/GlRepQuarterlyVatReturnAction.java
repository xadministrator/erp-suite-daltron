package com.struts.jreports.gl.quarterlyvatreturn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlRepQuarterlyVatReturnController;
import com.ejb.txn.GlRepQuarterlyVatReturnControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.GlRepQuarterlyVatReturnDetails;

public final class GlRepQuarterlyVatReturnAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRepQuarterlyVatReturnAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         GlRepQuarterlyVatReturnForm actionForm = (GlRepQuarterlyVatReturnForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.GL_REP_QUARTERLY_VAT_RETURN_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepQuarterlyVatReturn");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize GlRepQuarterlyVatReturnController EJB
*******************************************************/

         GlRepQuarterlyVatReturnControllerHome homeQVR = null;
         GlRepQuarterlyVatReturnController ejbQVR = null;

         try {

            homeQVR = (GlRepQuarterlyVatReturnControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/GlRepQuarterlyVatReturnControllerEJB", GlRepQuarterlyVatReturnControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in GlRepQuarterlyVatReturnAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbQVR = homeQVR.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in GlRepQuarterlyVatReturnAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- Gl QVR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlRepQuarterlyVatReturnDetails details = null;

		    try {

		       details = ejbQVR.executeGlRepQuarterlyVatReturn(
		       	 Common.convertStringToSQLDate(actionForm.getDateFrom()),
		       	 Common.convertStringToSQLDate(actionForm.getDateTo()), user.getCmpCode());

		    } catch(EJBException ex) {
		    	System.out.println("ERROR----------------->");

		         if(log.isInfoEnabled()) {
			    log.info("EJBException caught in GlRepQuarterlyVatReturnAction.execute(): " + ex.getMessage() +
			    " session: " + session.getId());

			}

			return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glRepQuarterlyVatReturn"));

		    }

		    //get for the month

		    String forTheMonth = actionForm.getForTheMonth();
		    String month = null;
		    String year = null;
		    String monthYear = null;
		    /*
		    StringTokenizer st = new StringTokenizer(forTheMonth, "/");

	        month = st.nextToken();

	        if (month != null && month.length() == 2) {

	        	month = " " + month.substring(0, 1) + "  " + month.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.forTheMonthInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        year = st.nextToken();

	        if (year != null && year.length() == 4) {

	    	    year = " "  + year.substring(0, 1) + "    " + year.substring(1, 2) + "      " + year.substring(2, 3) + "    " + year.substring(3, 4);

	        } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.forTheMonthInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        monthYear = month + "   " + year;
	        */

		    // get number of sheets attached

	        String numberOfSheetsAttached = actionForm.getNumberOfSheetsAttached();

	        /*if (numberOfSheetsAttached != null && numberOfSheetsAttached.length() == 2) {

	        	numberOfSheetsAttached = numberOfSheetsAttached.substring(0, 1) + "  " + numberOfSheetsAttached.substring(1, 2);

    		} else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.numberOfSheetsAttachedInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }*/

		    // get tin number

		    String tinNumber = actionForm.getTinNumber();

		    /*if (tinNumber != null && tinNumber.length() == 12) {

			    tinNumber = tinNumber.substring(0, 1) + "  " + tinNumber.substring(1, 2) + " " + tinNumber.substring(2, 3) + "       " +
			                tinNumber.substring(3, 4) + "  " + tinNumber.substring(4, 5) + "  " + tinNumber.substring(5, 6) + "      " +
					        tinNumber.substring(6, 7) + "  " + tinNumber.substring(7, 8) + "  " + tinNumber.substring(8, 9) + "      " +
				            tinNumber.substring(9, 10) + " " + tinNumber.substring(10, 11) + " " + tinNumber.substring(11, 12);

            } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("monthlyVatDeclaration.error.tinNumberInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

		    }
              */
	        // get rdo code

	        String rdoCode = actionForm.getRdoCode();

	        /*if (rdoCode != null && rdoCode.length() == 3) {

	        	rdoCode = "" + rdoCode.substring(0, 1) + "  " + rdoCode.substring(1, 2) + "  " + rdoCode.substring(2, 3);

		    } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.rdoCodeInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }*/

		     // get telephone number

	        String telephoneNumber = actionForm.getTelephoneNumber();

	        /*if (telephoneNumber != null && telephoneNumber.length() == 7) {

		        telephoneNumber = telephoneNumber.substring(0, 1) + "  " + telephoneNumber.substring(1, 2) + "  " +
		                          telephoneNumber.substring(2, 3) + "  " + telephoneNumber.substring(3, 4) + "   " +
		                          telephoneNumber.substring(4, 5) + "   " + telephoneNumber.substring(5, 6) + "   " +
					              telephoneNumber.substring(6, 7);

			} else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.telephoneNumberInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }*/

		    // get zip code

	        String zipCode = actionForm.getZipCode();

	        /*if (zipCode != null && zipCode.length() == 4) {

	        	zipCode = " " + zipCode.substring(0, 1) + "   " + zipCode.substring(1, 2) + "   " + zipCode.substring(2, 3) + "   " + zipCode.substring(3, 4);

		    } else {

	    	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.zipCodeInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }	*/

		    /*** fill report parameters, fill report to pdf and set report session **/

		    Map parameters = new HashMap();
		    parameters.put("BirDir", servlet.getServletContext().getRealPath("images/bir.gif"));
		    parameters.put("ArrowDir", servlet.getServletContext().getRealPath("images/arrow.gif"));
		    parameters.put("forTheMonth", monthYear);

		    // calender year

		    if (actionForm.getAccountingPeriod().equals("CALENDAR")) {

		    	parameters.put("calendarField", "X");

		    } else if (actionForm.getAccountingPeriod().equals("FISCAL")) {

		    	parameters.put("fiscalField", "X");

		    }

		    // get and set date from
		    String dateFrom = actionForm.getDateFrom();
		    String mnth = null;
		    String day = null;
		    String yr = null;
		    StringTokenizer st = new StringTokenizer(dateFrom, "/");

		    /*mnth = st.nextToken();

	        if (mnth != null && mnth.length() == 2) {

	        	mnth = " " + mnth.substring(0, 1) + "   " + mnth.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateFromInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        day = st.nextToken();

	        if (day != null && day.length() == 2) {

	        	day = " " + day.substring(0, 1) + "   " + day.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateFromInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        yr = st.nextToken();

	        if (yr != null && yr.length() == 2) {

	        	yr = yr.substring(0, 1) + "   " + yr.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateFromInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        dateFrom = mnth + "   " + day + "    " + yr;*/

		    parameters.put("dateFrom", dateFrom);

		    //get and set date to
		    String dateTo = actionForm.getDateTo();
		    /*mnth = null;
		    day = null;
		    yr = null;
		    st = new StringTokenizer(dateTo, "/");

		    mnth = st.nextToken();

		    if (mnth != null && mnth.length() == 2) {

	        	mnth = " " + mnth.substring(0, 1) + "   " + mnth.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateToInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        day = st.nextToken();

	        if (day != null && day.length() == 2) {

	        	day = " " + day.substring(0, 1) + "   " + day.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateToInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        yr = st.nextToken();

	        if (yr != null && yr.length() == 2) {

	        	yr = yr.substring(0, 1) + "   " + yr.substring(1, 2);

	        } else {

	        	errors.add(ActionMessages.GLOBAL_MESSAGE,
	          		new ActionMessage("quarterlyVatReturn.error.dateToInvalid"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("glRepQuarterlyVatReturn"));

	        }

	        dateTo = mnth + "   " + day + "    " + yr;*/

		    parameters.put("dateTo", dateTo);

		    // quarter

		    if (actionForm.getQuarter().equals("1st")) {

		    	parameters.put("firstQuarter", "X");

		    } else if (actionForm.getQuarter().equals("2nd")) {

		    	parameters.put("secondQuarter", "X");

		    } else if (actionForm.getQuarter().equals("3rd")) {

		    	parameters.put("thirdQuarter", "X");

		    } else if (actionForm.getQuarter().equals("4th")) {

		    	parameters.put("fourthQuarter", "X");

		    }

		    // for amended

		    if (actionForm.getAmended().equals("YES")) {

		        parameters.put("amendedYes", "X");

		    } else if (actionForm.getAmended().equals("NO")) {

		        parameters.put("amendedNo", "X");

		    } else {

		    	parameters.put("amendedYes", "");
		    	parameters.put("amendedNo", "");

		    }

		    parameters.put("numberOfSheetsAttached", numberOfSheetsAttached);
		    parameters.put("tinNumber", tinNumber);
		    parameters.put("rdoCode", rdoCode);
		    parameters.put("lineOfBusiness", actionForm.getLineOfBusiness());
		    parameters.put("taxpayersName", actionForm.getTaxpayersName());
		    parameters.put("telephoneNumber", telephoneNumber);
		    parameters.put("registeredAddress", actionForm.getRegisteredAddress());
		    parameters.put("zipCode", zipCode);

		    // for relief

		    if (actionForm.getRelief().equals("YES")) {

		        parameters.put("reliefYes", "X");
		        parameters.put("reliefDescription", actionForm.getReliefDescription());

		    } else if (actionForm.getRelief().equals("NO")) {

		        parameters.put("reliefNo", "X");

		    } else {

		    	parameters.put("reliefYes", "");
		    	parameters.put("reliefNo", "");

		    }

		    // for computation of tax

			parameters.put("industryClassification", actionForm.getIndustryClassification());
			parameters.put("atcCode", actionForm.getAtcCode());
			parameters.put("zeroRatedSalesReceipts", new Double(Common.convertStringMoneyToDouble(actionForm.getZeroRatedSalesReceipts(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("exemptSalesReceipts", new Double(Common.convertStringMoneyToDouble(actionForm.getExemptSalesReceipts(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("transitionalInputTax", new Double(Common.convertStringMoneyToDouble(actionForm.getTransitionalInputTax(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("presumptiveInputTax", new Double(Common.convertStringMoneyToDouble(actionForm.getPresumptiveInputTax(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("inputTaxCarriedOver", new Double(Common.convertStringMoneyToDouble(actionForm.getInputTaxCarriedOver(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("servicesRenderedByNonResident", new Double(Common.convertStringMoneyToDouble(actionForm.getServicesRenderedByNonResident(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("importationsCapitalGoods", new Double(Common.convertStringMoneyToDouble(actionForm.getImportationsCapitalGoods(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("importationsGoods", new Double(Common.convertStringMoneyToDouble(actionForm.getImportationsGoods(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("purchasesNotQualified", new Double(Common.convertStringMoneyToDouble(actionForm.getPurchasesNotQualified(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("vatRefund", new Double(Common.convertStringMoneyToDouble(actionForm.getVatRefund(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("excessInputTax", new Double(Common.convertStringMoneyToDouble(actionForm.getExcessInputTax(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("excessPayments", new Double(Common.convertStringMoneyToDouble(actionForm.getExcessPayments(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("monthlyVatPayments", new Double(Common.convertStringMoneyToDouble(actionForm.getMonthlyVatPayments(), Constants.MONEY_RATE_PRECISION)));
			parameters.put("creditableVatWithheld", new Double(Common.convertStringMoneyToDouble(actionForm.getCreditableVatWithheld(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("advancePayments", new Double(Common.convertStringMoneyToDouble(actionForm.getAdvancePayments(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("vatPaidInReturn", new Double(Common.convertStringMoneyToDouble(actionForm.getVatPaidInReturn(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("surcharge", new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("interest", new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("compromise", new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION)));

		    parameters.put("dpGoods", new Double(Common.convertStringMoneyToDouble(actionForm.getDpGoods(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("dpServices", new Double(Common.convertStringMoneyToDouble(actionForm.getDpServices(), Constants.MONEY_RATE_PRECISION)));

		    // input taxes

		    parameters.put("dpGoodsInputTax", new Double((Common.convertStringMoneyToDouble(actionForm.getDpGoods(), Constants.MONEY_RATE_PRECISION))));
		    parameters.put("dpServicesInputTax", new Double((Common.convertStringMoneyToDouble(actionForm.getDpServices(), Constants.MONEY_RATE_PRECISION))));
		    parameters.put("servicesRenderedInputTax", new Double((Common.convertStringMoneyToDouble(actionForm.getServicesRenderedByNonResident(), Constants.MONEY_RATE_PRECISION))));
		    parameters.put("importationsCpInputTax", new Double((Common.convertStringMoneyToDouble(actionForm.getImportationsCapitalGoods(), Constants.MONEY_RATE_PRECISION))));
		    parameters.put("importationsGoodsInputTax", new Double((Common.convertStringMoneyToDouble(actionForm.getImportationsGoods(), Constants.MONEY_RATE_PRECISION))));

		    // for payment details

		    parameters.put("cashBankAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCashBankAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("checkBank", actionForm.getCheckBank());
		    parameters.put("checkNumber", actionForm.getCheckNumber());


		    double zeroRatedSalesReceipts = new Double(Common.convertStringMoneyToDouble(actionForm.getZeroRatedSalesReceipts(), Constants.MONEY_RATE_PRECISION));
		    double exemptSalesReceipts = new Double(Common.convertStringMoneyToDouble(actionForm.getExemptSalesReceipts(), Constants.MONEY_RATE_PRECISION));


		    double totalSalesReceiptsNet = details.getSalesReceiptForQuarter() + zeroRatedSalesReceipts + exemptSalesReceipts;
		    parameters.put("totalSalesReceiptsNet", totalSalesReceiptsNet);

		    double dpGoods = new Double(Common.convertStringMoneyToDouble(actionForm.getDpGoods(), Constants.MONEY_RATE_PRECISION));
		    double dpServices = new Double(Common.convertStringMoneyToDouble(actionForm.getDpServices(), Constants.MONEY_RATE_PRECISION));
		    double servicesRenderedByNonResident = new Double(Common.convertStringMoneyToDouble(actionForm.getServicesRenderedByNonResident(), Constants.MONEY_RATE_PRECISION));
		    double importationsCapitalGoods = new Double(Common.convertStringMoneyToDouble(actionForm.getImportationsCapitalGoods(), Constants.MONEY_RATE_PRECISION));
		    double importationsGoods = new Double(Common.convertStringMoneyToDouble(actionForm.getImportationsGoods(), Constants.MONEY_RATE_PRECISION));
		    double totalPurchases = details.getNetPurchasesQuarter() + dpGoods + dpServices + servicesRenderedByNonResident + importationsCapitalGoods + importationsGoods;

		    parameters.put("totalPurchases", totalPurchases);

		    double transitionalInputTax = new Double(Common.convertStringMoneyToDouble(actionForm.getTransitionalInputTax(), Constants.MONEY_RATE_PRECISION));
		    double presumptiveInputTax = new Double(Common.convertStringMoneyToDouble(actionForm.getPresumptiveInputTax(), Constants.MONEY_RATE_PRECISION));
		    double inputTaxCarriedOver = new Double(Common.convertStringMoneyToDouble(actionForm.getInputTaxCarriedOver(), Constants.MONEY_RATE_PRECISION));
		    double dpGoodsInputTax = new Double((Common.convertStringMoneyToDouble(actionForm.getDpGoods(), Constants.MONEY_RATE_PRECISION)));
		    double dpServicesInputTax = new Double(Common.convertStringMoneyToDouble(actionForm.getDpServices(), Constants.MONEY_RATE_PRECISION));


		    double totalAvailableInputTax = details.getInputTaxQuarter() + transitionalInputTax + presumptiveInputTax + inputTaxCarriedOver + dpGoodsInputTax + dpServicesInputTax;
		    parameters.put("totalAvailableInputTax", totalAvailableInputTax);

		    double vatRefund = new Double(Common.convertStringMoneyToDouble(actionForm.getVatRefund(), Constants.MONEY_RATE_PRECISION));
		    double excessInputTax = new Double(Common.convertStringMoneyToDouble(actionForm.getExcessInputTax(), Constants.MONEY_RATE_PRECISION));

		    double totalDeductions = vatRefund +  excessInputTax;
		    parameters.put("totalDeductions", totalDeductions);

		    double netCreditableInputTax = totalAvailableInputTax - totalDeductions;
		    parameters.put("netCreditableInputTax", netCreditableInputTax);

		    double vatPayable = details.getTaxOutputQuarter() - netCreditableInputTax;
		    parameters.put("vatPayable", vatPayable);


		    double excessPayments = new Double(Common.convertStringMoneyToDouble(actionForm.getExcessPayments(), Constants.MONEY_RATE_PRECISION));
		    double monthlyVatPayments = new Double(Common.convertStringMoneyToDouble(actionForm.getMonthlyVatPayments(), Constants.MONEY_RATE_PRECISION));
		    double creditableVatWithheld = new Double(Common.convertStringMoneyToDouble(actionForm.getCreditableVatWithheld(), Constants.MONEY_RATE_PRECISION));
		    double advancePayments = new Double(Common.convertStringMoneyToDouble(actionForm.getAdvancePayments(), Constants.MONEY_RATE_PRECISION));
		    double vatPaidInReturn = new Double(Common.convertStringMoneyToDouble(actionForm.getVatPaidInReturn(), Constants.MONEY_RATE_PRECISION));


		    double totalTaxCreditsPayments = excessPayments + monthlyVatPayments + creditableVatWithheld+ advancePayments + vatPaidInReturn;
		    parameters.put("totalTaxCreditsPayments", totalTaxCreditsPayments);


		    double surcharge = new Double(Common.convertStringMoneyToDouble(actionForm.getSurcharge(), Constants.MONEY_RATE_PRECISION));
		    double interest = new Double(Common.convertStringMoneyToDouble(actionForm.getInterest(), Constants.MONEY_RATE_PRECISION));
		    double  compromise= new Double(Common.convertStringMoneyToDouble(actionForm.getCompromise(), Constants.MONEY_RATE_PRECISION));

		    double totalPenalties = surcharge + interest + compromise;
		    parameters.put("totalPenalties", totalPenalties);


		    double taxPayable = vatPayable - totalTaxCreditsPayments;

		    parameters.put("taxPayable", taxPayable);

		    double totalAmountPayable = taxPayable + totalPenalties;
		    parameters.put("totalAmountPayable", totalAmountPayable);

		    String checkDate = actionForm.getCheckDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!checkDate.equals(null) && checkDate.length() != 0 ) {

			    st = new StringTokenizer(checkDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.checkDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        checkDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("checkDate", checkDate);
		    parameters.put("checkAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getCheckAmount(), Constants.MONEY_RATE_PRECISION)));
		    parameters.put("taxDebitNumber", actionForm.getTaxDebitNumber());
		    parameters.put("taxDebitAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getTaxDebitAmount(), Constants.MONEY_RATE_PRECISION)));

		    String taxDebitDate = actionForm.getTaxDebitDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!taxDebitDate.equals(null) && taxDebitDate.length() != 0 ) {

			    st = new StringTokenizer(taxDebitDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.taxDebitDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        taxDebitDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("taxDebitDate", taxDebitDate);
		    parameters.put("otherBank", actionForm.getOtherBank());
		    parameters.put("otherNumber", actionForm.getOtherNumber());

		    String otherDate = actionForm.getOtherDate();
		    mnth = null;
		    day = null;
		    yr = null;

		    if (!otherDate.equals(null) && otherDate.length() != 0 ) {

			    st = new StringTokenizer(otherDate, "/");

			    mnth = st.nextToken();

			    if (mnth != null && mnth.length() == 2) {

		        	mnth = "  " + mnth.substring(0, 1) + "    " + mnth.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        day = st.nextToken();

		        if (day != null && day.length() == 2) {

		        	day = " " + day.substring(0, 1) + "    " + day.substring(1, 2);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        yr = st.nextToken();

		        if (yr != null && yr.length() == 4) {

		        	yr = yr.substring(0, 1) + "    " + yr.substring(1, 2) + "    " + yr.substring(2,3) +
					     "    " + yr.substring(3,4);

		        } else {

		        	errors.add(ActionMessages.GLOBAL_MESSAGE,
		          		new ActionMessage("quarterlyVatReturn.error.otherDateInvalid"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("glRepQuarterlyVatReturn"));

		        }

		        otherDate = mnth + "   " + day + "    " + yr;
		    }

		    parameters.put("otherDate", otherDate);
		    parameters.put("otherAmount", new Double(Common.convertStringMoneyToDouble(actionForm.getOtherAmount(), Constants.MONEY_RATE_PRECISION)));

		    // for signatories details

		    parameters.put("presidentSignature", actionForm.getPresidentSignature());
		    parameters.put("treasurerSignature", actionForm.getTreasurerSignature());
		    parameters.put("titleOfSignatory1", actionForm.getTitleOfSignatory1());
		    parameters.put("titleOfSignatory2", actionForm.getTitleOfSignatory2());
		    parameters.put("tinOfTaxAgent", actionForm.getTinOfTaxAgent());
		    parameters.put("taxAgentAccreditation", actionForm.getTaxAgentAccreditation());




		    String filename = null;

		    if (actionForm.getReportType().equals("CIT RETURN")) {
		    	System.out.println("CIT RETURN");
			    filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepCITReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepCITReturn.jasper");

			    }
		    } else {

		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/GlRepQuarterlyVatReturn.jasper";

			    if (!new java.io.File(filename).exists()) {

			       filename = servlet.getServletContext().getRealPath("jreports/GlRepQuarterlyVatReturn.jasper");

			    }

		    }

		    try {

		    	Report report = new Report();

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters,
				        new GlRepQuarterlyVatReturnDS(details)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters,
					        new GlRepQuarterlyVatReturnDS(details)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
				       new GlRepQuarterlyVatReturnDS(details)));

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in GlRepQuarterlyVatReturnAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }

/*******************************************************
   -- Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl QVR Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	         	try {
	         		ArrayList list = null;
			        Iterator i = null;

	         		actionForm.clearReportTypeList();

			        list = ejbQVR.getAdLvReportTypeAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {

	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            		i = list.iterator();

	            		while (i.hasNext()) {


	            		    actionForm.setReportTypeList((String)i.next());

	            		}

	            	}


			        // get company details

		            AdCompanyDetails adCmpDetails = ejbQVR.getAdCompany(user.getCmpCode());

		            String REGISTER_ADDRSS =  adCmpDetails.getCmpAddress() + " " + adCmpDetails.getCmpCity() + " " + adCmpDetails.getCmpCountry();

		            actionForm.setTinNumber(adCmpDetails.getCmpTin());
		            actionForm.setTaxpayersName(adCmpDetails.getCmpName());
			        actionForm.setTelephoneNumber(adCmpDetails.getCmpPhone());
			        actionForm.setRegisteredAddress(REGISTER_ADDRSS);
		            actionForm.setZipCode(adCmpDetails.getCmpZip());
		            actionForm.setLineOfBusiness(adCmpDetails.getCmpIndustry());

				} catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in ArRepOutputTaxAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

			    }

	            actionForm.reset(mapping, request);
	            return(mapping.findForward("glRepQuarterlyVatReturn"));

			 } else {

			    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			    saveErrors(request, new ActionMessages(errors));

			    return(mapping.findForward("cmnMain"));

			 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in GlRepQuarterlyVatReturnAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
