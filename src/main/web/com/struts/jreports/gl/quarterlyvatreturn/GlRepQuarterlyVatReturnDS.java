/*
 * opt/jreport/GlRepQuarterlyVatReturnDS.java
 *
 * Created on March 29, 2004, 5:00 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.struts.jreports.gl.quarterlyvatreturn;

import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.GlRepQuarterlyVatReturnDetails;


public class GlRepQuarterlyVatReturnDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;
   	 
   public GlRepQuarterlyVatReturnDS(GlRepQuarterlyVatReturnDetails mdetails) {   	 
   	  	
	GlRepQuarterlyVatReturnDetails details = new GlRepQuarterlyVatReturnDetails();
	GlRepQuarterlyVatReturnData glRepQVRData = new GlRepQuarterlyVatReturnData(
	new Double(mdetails.getSalesReceiptForQuarter()), new Double(mdetails.getTaxOutputQuarter()),
	new Double(mdetails.getNetPurchasesQuarter()), new Double(mdetails.getInputTaxQuarter()));
	
	data.add(glRepQVRData);
	
   }
   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("salesReceiptsForQuarter".equals(fieldName)) {
      	
          value = ((GlRepQuarterlyVatReturnData)data.get(index)).getSalesReceiptsForQuarter();
      
      } else if("outputTaxDueForQuarter".equals(fieldName)) {
      
          value = ((GlRepQuarterlyVatReturnData)data.get(index)).getOutputTaxDueForQuarter();

      } else if("dpCapitalGoods".equals(fieldName)) {
      
          value = ((GlRepQuarterlyVatReturnData)data.get(index)).getDpCapitalGoods();
          
      } else if("inputTaxForQuarter".equals(fieldName)) {
      
          value = ((GlRepQuarterlyVatReturnData)data.get(index)).getInputTaxForQuarter();    
          
      }
      return(value);
   }
}
