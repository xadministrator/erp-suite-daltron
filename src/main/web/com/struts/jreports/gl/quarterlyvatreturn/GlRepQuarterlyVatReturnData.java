/*
 * opt/jreport/GlRepQuarterlyVatReturnData.java
 *
 * Created on March 29, 2004, 5:00 PM
 *
 * @author  Neil Andrew M. Ajero
 */
package com.struts.jreports.gl.quarterlyvatreturn; 


public class GlRepQuarterlyVatReturnData implements java.io.Serializable {

    private Double salesReceiptsForQuarter = null;
    private Double outputTaxDueForQuarter = null;
    private Double dpCapitalGoods = null;
    private Double inputTaxForQuarter = null;

    public GlRepQuarterlyVatReturnData(Double salesReceiptsForQuarter, Double outputTaxDueForQuarter,
    Double dpCapitalGoods, Double inputTaxForQuarter) {

        this.salesReceiptsForQuarter = salesReceiptsForQuarter;
        this.outputTaxDueForQuarter = outputTaxDueForQuarter;
        this.dpCapitalGoods = dpCapitalGoods;
        this.inputTaxForQuarter = inputTaxForQuarter;

    }

    public Double getSalesReceiptsForQuarter() {

        return salesReceiptsForQuarter;

    }
    
    public Double getOutputTaxDueForQuarter() {
    	
    	return outputTaxDueForQuarter;
    	
    }
    
    public Double getDpCapitalGoods() {
    	
    	return dpCapitalGoods;
    	
    }
    
    public Double getInputTaxForQuarter() {
    	
    	return inputTaxForQuarter;
    	
    }
} 