package com.struts.jreports.gl.staticreport;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlRepStaticReportController;
import com.ejb.txn.GlRepStaticReportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModUserStaticReportDetails;

public final class GlRepStaticReportAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRepStaticReportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlRepStaticReportForm actionForm = (GlRepStaticReportForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_REP_STATIC_REPORT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRepStaticReport");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

//         actionForm.setStaticReport(null);
         
/*******************************************************
   Initialize GlRepStaticReportController EJB
*******************************************************/

         GlRepStaticReportControllerHome homeUSTR = null;
         GlRepStaticReportController ejbUSTR = null;

         try {

            homeUSTR = (GlRepStaticReportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlRepStaticReportControllerEJB", GlRepStaticReportControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlRepStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbUSTR = homeUSTR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlRepStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl Rep SR Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Rep SR View Action --
*******************************************************/

         } else if (request.getParameter("view")!=null) {
         	
         	try {
         		
         		FileInputStream fis = new FileInputStream(request.getParameter("fileName"));
         		
         		byte data[] = new byte[fis.available()];
         		
         		int ctr = 0;
         		int c = 0;
         		
         		while ((c = fis.read()) != -1) {
         			
         			data[ctr] = (byte)c;
         			ctr++;
         			
         		}
         		
         		response.setContentType("application/pdf");
         		response.setContentLength(data.length);	
         		ServletOutputStream outputStream = response.getOutputStream();
         		outputStream.write(data, 0, data.length);
         		outputStream.flush();
         		outputStream.close();
         		
         	} catch(Exception ex) {
         		
         		ex.printStackTrace();
         		
         		if(log.isInfoEnabled()) {
         			
         			log.info("Exception caught in AdRepApprovalListAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	}

            return mapping.findForward("glRepStaticReport");

         	
         }
            
/*******************************************************
   -- Gl Rep SR Load Action --
*******************************************************/

         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glRepStaticReport");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearGlRUSTRList();	        
	           
	           list = ejbUSTR.getGlUstrByUsrCode(new Integer(user.getUserCode()), user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              GlModUserStaticReportDetails details = (GlModUserStaticReportDetails)i.next();

	              if (details.getUstrSrDateTo()!=null){

	              }
	              
	              if ((new java.io.File(details.getUstrSrFlName()).exists()) &&
	              	((details.getUstrSrDateTo() == null) || 
	              			(Common.getGcCurrentDateWoTime().getTime().compareTo(details.getUstrSrDateTo()) >= 0 ))) {
	            	  System.out.println("TRUE");
	            	
	              	GlRepStaticReportList glRUSTRList = new GlRepStaticReportList(actionForm, details.getUstrSrCode(),
						details.getUstrSrName(),
						details.getUstrSrFlName());

	              	actionForm.saveGlRUSTRList(glRUSTRList);
	              	
	              }
	              System.out.println(new java.io.File(details.getUstrSrFlName()).exists());
	              System.out.println(details.getUstrSrDateTo() +"-----"+ Common.getGcCurrentDateWoTime().getTime());
	              try{System.out.println(details.getUstrSrDateTo().compareTo(Common.getGcCurrentDateWoTime().getTime()));
	              } catch (Exception e) {
					// TODO: handle exception
				}
	              
	              System.out.println("FALSE");
	           }
	           

	        } catch (GlobalNoRecordFoundException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("repStaticReport.error.noRecordFound"));
	            saveErrors(request, new ActionMessages(errors));

	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlRepStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("glRepStaticReport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      		e.printStackTrace();
         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlRepStaticReportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}