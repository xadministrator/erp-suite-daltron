package com.struts.jreports.gl.staticreport;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class GlRepStaticReportForm extends ActionForm implements Serializable {
	
	private ArrayList glRUSTRList = new ArrayList();
	private String userPermission = new String();
	private String txnStatus = new String();

	public GlRepStaticReportList getGlRUSTRByIndex(int index) {
		
		return((GlRepStaticReportList)glRUSTRList.get(index));
		
	}
	
	public Object[] getGlRUSTRList() {
		
		return glRUSTRList.toArray();
		
	}
	
	public int getGlRUSTRListSize() {
		
		return glRUSTRList.size();
		
	}
	
	public void saveGlRUSTRList(Object newGlRUSTRList) {
		
		glRUSTRList.add(newGlRUSTRList);
		
	}
	
	public void clearGlRUSTRList() {
		
		glRUSTRList.clear();
		
	}

	public void updateGlRUSTRRow(int rowSelected, Object newGlRUSTRList) {
		
		glRUSTRList.set(rowSelected, newGlRUSTRList);
		
	}
	
	public void deleteGlRUSTRList(int rowSelected) {
		
		glRUSTRList.remove(rowSelected);
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		this.glRUSTRList = new ArrayList();
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		return errors;
		
	}
	
}