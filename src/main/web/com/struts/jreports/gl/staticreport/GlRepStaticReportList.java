package com.struts.jreports.gl.staticreport;

import java.io.Serializable;

public class GlRepStaticReportList implements Serializable {
	
	private Integer staticReportCode = null;
	private String staticReportName = null;
	private String fileName = null;

	private GlRepStaticReportForm parentBean;
	
	public GlRepStaticReportList(GlRepStaticReportForm parentBean,
			Integer staticReportCode,
			String staticReportName,
			String fileName) {
		
		this.parentBean = parentBean;
		this.staticReportCode = staticReportCode;
		this.staticReportName = staticReportName;
		this.fileName = fileName;
		
	}

	public Integer getStaticReportCode() {
		
		return staticReportCode;
		
	}
	
	public String getStaticReportName() {
		
		return staticReportName;
		
	}
	
	public String getFileName() {
		
		return fileName;
		
	}
	
}