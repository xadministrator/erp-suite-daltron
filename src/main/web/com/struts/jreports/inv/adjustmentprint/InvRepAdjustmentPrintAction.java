package com.struts.jreports.inv.adjustmentprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.txn.InvRepAdjustmentPrintController;
import com.ejb.txn.InvRepAdjustmentPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.EJBHomeFactory;
import com.util.InvRepAdjustmentPrintDetails;
import com.ejb.txn.AdPreferenceControllerHome;
import com.ejb.txn.AdPreferenceController;

public final class InvRepAdjustmentPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepAdjustmentPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvRepAdjustmentPrintForm actionForm = (InvRepAdjustmentPrintForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.INV_REP_ADJUSTMENT_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize InvRepAdjustmenPrintController EJB
*******************************************************/

         InvRepAdjustmentPrintControllerHome homeADJ = null;
         InvRepAdjustmentPrintController ejbADJ = null; 
         LocalAdPreferenceHome adPreferenceHome = null;
         LocalAdPreference adPreference = null;
         try {
         	
             homeADJ = (InvRepAdjustmentPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepAdjustmentPrintControllerEJB", InvRepAdjustmentPrintControllerHome.class);
             adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepAdjustmentPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
             ejbADJ = homeADJ.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepAdjustmentPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getInvRepAdjustmentEntryCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    InvRepAdjustmentPrintDetails details = null; 
				
			    ArrayList list = null;   
			    //boolean isService = false; 
	      		boolean isInventoriable = false;
				try {
					
					ArrayList atrCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		atrCodeList.add(new Integer(request.getParameter("adjustmentCode")));
				
					}
	            	
	            	list = ejbADJ.executeInvRepAdjustmentPrint(atrCodeList, user.getCmpCode());
	            
	            
			        // get company
			       
			        adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());	
			       
			        // get parameters
	      			
	      			Iterator i = list.iterator();
	      			
	      			while (i.hasNext()) {
	      				
	      				details = (InvRepAdjustmentPrintDetails)i.next();
	      				//isService = details.getApAlIsService();
	      				isInventoriable = details.getApAlIsInventoriable();
	      				System.out.println(details.getApAlAdjustQuantity() + "<== adjusted quantity");
	      			}
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentPrint.error.adjustmentEntryAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvAdjustmentPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("invRepAdjustmentPrint");

	             }
				
				// fill report parameters, fill report to pdf and set report session  	    	               
			    boolean isTrackMisc = ejbADJ.getInvTraceMisc(details.getApAlIiName(), user.getCmpCode());
			    Map parameters = new HashMap();
			    System.out.print("HERE IS THE PRINTING");
			    parameters.put("company", adCmpDetails.getCmpName());
			    System.out.print("CMP CODE : "+user.getCmpCode());
				parameters.put("createdBy", details.getApAdjCreatedBy());
				parameters.put("Doc", details.getApAdjDocumentNumber());
				parameters.put("Doc", details.getApAdjDocumentNumber());
				
				try{
					
				adPreference = adPreferenceHome.findByPrfAdCompany(user.getCmpCode());
	
				 System.out.print("A");
				//parameters.put("NotedBy",  adPreference.getPrfApDefaultChecker());
				
				String sNotedBy=details.getApAdjNotedBy();
				if(sNotedBy.trim().length()<1)
				{
					
					sNotedBy=adPreference.getPrfApDefaultChecker();
					System.out.print("ad pref NotedBy: "+adPreference.getPrfApDefaultChecker());
				}
				
					
				 int Seperator=0;
				 try{
					 Seperator= sNotedBy.indexOf("-");
				 }
				 catch(Exception ex)
				 {
					 
				 }
					
					if(Seperator>2)
						{
						parameters.put("NotedBy", sNotedBy.substring(0, Seperator));
					parameters.put("Position", sNotedBy.substring(Seperator+1));
					}
					else
					{
						parameters.put("NotedBy", sNotedBy);
						parameters.put("Position", "");
					}
					System.out.print("NotedBy: "+details.getApAdjNotedBy());
				 System.out.print("B");
				}
				catch(Exception ex)
				{
				
				}
				String filename = "";
				
				if (details.getApAdjType().equals("ISSUANCE")){
					filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrintWasteMaterials.jasper";
					
					//temporary filename
					if(user.getCompany().equals("LBP")){
						filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
						}
					
					
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
				    
			        }
				}else if (details.getApAdjType().equals("GENERAL") && isTrackMisc == true){
					/*
					*/
					if (details.getApAlAdjustQuantity()>0){
						if(isInventoriable){
		          			System.out.println("isInventoriable");
		          			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrintAre.jasper";
		          			 
		          			 //temporary filename     
		          			if(user.getCompany().equals("LBP")){
								filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
								}
		          			
		              		if (!new java.io.File(filename).exists()) {
	
		          				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
	
		          			}
		          		}else {
		          				
		          			System.out.println("is not inventoriable");
		          			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrintCustodian.jasper";
		          			
		          			//temporary filename
		          			if(user.getCompany().equals("LBP")){
								filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
								}
		              		if (!new java.io.File(filename).exists()) {
	
		          				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
	
		          			}
		          		}
					}else{
						if(isInventoriable){
		          			System.out.println("isInventoriable");
		          			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrintAreNegativeAdjust.jasper";
		          			
		          			//temporary filename
		          			if(user.getCompany().equals("LBP")){
								filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
								}		
		              		if (!new java.io.File(filename).exists()) {
	
		          				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
	
		          			}
		          		}else {
		          				
		          			System.out.println("is not inventoriable");
		          			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrintCustodianNegativeAdjust.jasper";
		          			
		          			//temporary filename
		          			if(user.getCompany().equals("LBP")){
								filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
								}
		              		if (!new java.io.File(filename).exists()) {
	
		          				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
	
		          			}
		          		}
					}
						
				}else{
					filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
					
					//temporary filename
					if(user.getCompany().equals("LBP")){
						filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentPrint.jasper";
						}
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentPrint.jasper");
				    
			        }
				}
				
						    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepAdjustmentPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					ex.printStackTrace();
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in InvRepAdjustmentPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("invRepAdjustmentPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("invRepAdjustmentPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in InvRepAdjustmentPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
