package com.struts.jreports.inv.adjustmentprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.receivingreportprint.ApRepReceivingReportPrintData;
import com.struts.jreports.inv.adjustmentrequestprint.InvRepAdjustmentRequestPrintData;
import com.struts.util.Common;
import com.util.InvRepAdjustmentPrintDetails;

public class InvRepAdjustmentPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentPrintDS(ArrayList list) {
       
       Iterator i = list.iterator();
       
       while(i.hasNext()) {
           
           InvRepAdjustmentPrintDetails details = (InvRepAdjustmentPrintDetails)i.next();
           
           InvRepAdjustmentPrintData invRepADJData = new InvRepAdjustmentPrintData(
        		   details.getApAdjType(), Common.convertSQLDateToString(details.getApAdjDate()),details.getApAdjSupplierName(),details.getApAdjSupplierAddress(), details.getApAdjDocumentNumber(), details.getApAdjReferenceNumber(),
                   details.getApAdjGlCoaAccount(), details.getApAdjGlCoaAccountDesc(), details.getApAdjDescription(),details.getApAdjApprovedBy(),details.getApAdjCreatedBy(),details.getApAdjCreatedByPosition(),
                   details.getApAlIiName(), details.getApAlIiDescription(), details.getApAlUomName(), details.getApAlLocName(), 
                   new Double(details.getApAlUnitCost()),new Double(details.getApAlActualQuantity()), new Double(details.getApAlAdjustQuantity()), new Double(details.getApAlAveCost()),new Double(details.getApAlBranchCode()),details.getApAdjPostedBy(),
                   Common.convertSQLDateToString(details.getApTgExpiryDate()),
                   details.getApTgCustodian(), details.getApTgCustodianPosition(), details.getApTgPropertyCode(), details.getApTgSpecs(), details.getApTgSerialNumber(),
                   details.getApTgDocumentNumber());
           
           data.add(invRepADJData);
           
       }   	
       
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      
      if("type".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getType();
      }else if("date".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getDate();
      }else if("supplierName".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getSupplierName();
      }else if("supplierAddress".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getSupplierAddress();
          
          
          
      }else if("documentNumber".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getDocumentNumber();
          System.out.print(value +"DocNum");
      }else if("referenceNumber".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getReferenceNumber();
      }else if("adjustmentAccount".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getAccountDescription();
      }else if("description".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getDescription();
      } else if("approvedBy".equals(fieldName)){
         value = ((InvRepAdjustmentPrintData)data.get(index)).getApprovedBy();  
       }else if("createdBy".equals(fieldName)){
           value = ((InvRepAdjustmentPrintData)data.get(index)).getCreatedBy();
       }else if("createdByPosition".equals(fieldName)){
           value = ((InvRepAdjustmentPrintData)data.get(index)).getCreatedByPosition();    
           
      }else if("itemName".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getItemName();       
      }else if("itemDescription".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getItemDescription();
      }else if("unitMeasureShortName".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getUnitMeasureShortName();
      }else if("location".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getLocationName();
      }else if("unitCost".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getUnitCost();
      }else if("adjustQuantity".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getAdjustQuantity();
      }else if("aveCost".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getAveCost();
      } else if("branchCode".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getBranchCode();
          System.out.print(value +"valueBranch");
      }else if("postedBy".equals(fieldName)){
          value = ((InvRepAdjustmentPrintData)data.get(index)).getPostedBy();
          System.out.print(value +"valueBranch");
          
      }else if("expiryDate".equals(fieldName)) {
			value = ((InvRepAdjustmentPrintData)data.get(index)).getExpiryDate();
      }else if("custodian".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getCustodian();
      }else if("custodianPosition".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getCustodianPosition();
      }else if("propertyCode".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getPropertyCode();
      }else if("specs".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getSpecs();
      }else if("serialNumber".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getSerialNumber();
      }else if("tgDocumentNumber".equals(fieldName)) {
    	  value = ((InvRepAdjustmentPrintData)data.get(index)).getTgDocumentNumber();
      }
      
      return(value);
   }
}
