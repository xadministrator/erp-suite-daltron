package com.struts.jreports.inv.adjustmentprint;

public class InvRepAdjustmentPrintData implements java.io.Serializable {
    
	private String type = null;
    private String date = null;
    private String supplierName = null;
    private String supplierAddress = null;
    private String documentNumber = null;
    private String referenceNumber = null;
    private String accountNumber = null;
    private String accountDescription = null;
    private String description = null;
    
    
    private String approvedBy = null;
    private String createdBy = null;
    private String createdByPosition = null;
    private String itemName = null;
    private String itemDescription = null;
    private String unitMeasureShortName = null;
    private String locationName = null;
    private Double unitCost = null;
    private Double actualQuantity = null;
    private Double adjustQuantity = null;
    private Double aveCost = null;
    private Double branchCode = null;
    private String postedBy = null;
    
    private String expiryDate=null;
	private String custodian=null;
	private String custodianPosition=null;
	private String propertyCode=null;
	private String specs = null;
	private String serialNumber =null;
	private String tgDocumentNumber =null;
	    
	    public InvRepAdjustmentPrintData(String type, String date, String supplierName, String supplierAddress, String documentNumber, String referenceNumber, 
	            String accountNumber, String accountDescription, String description,String approvedBy,String createdBy, String createdByPosition,
	            String itemName, String itemDescription, String unitMeasureShortName, String locationName,
	            Double unitCost,Double actualQuantity ,Double adjustQuantity, Double aveCost,Double branchCode,String postedBy,
	            String expiryDate, String custodian, String custodianPosition, String propertyCode, String specs, String serialNumber, String tgDocumentNumber){
	                
	        this.type = type;
	        this.date = date;
	        this.supplierName = supplierName;
	        this.supplierAddress = supplierAddress;
	        this.documentNumber = documentNumber;
	        this.referenceNumber = referenceNumber;
	        this.accountNumber = accountNumber;
	        this.accountDescription = accountDescription;
	        this.description = description;
	        this.approvedBy=approvedBy;
	        this.createdBy=createdBy;
	        this.createdByPosition=createdByPosition;
	        this.itemName = itemName;
	        this.itemDescription = itemDescription;
	        this.unitMeasureShortName = unitMeasureShortName;
	        this.locationName = locationName;
	        this.unitCost = unitCost;
	        this.actualQuantity=actualQuantity;
	        this.adjustQuantity = adjustQuantity;
	        this.aveCost = aveCost;
	        this.branchCode = branchCode;
	        this.postedBy=postedBy;
	        
	        this.expiryDate=expiryDate;
			this.custodian=custodian;
			this.custodianPosition = custodianPosition;
			this.propertyCode = propertyCode;
			this.serialNumber = serialNumber;
			this.specs = specs;
			this.tgDocumentNumber = tgDocumentNumber;
	    }
	    
	    public String getExpiryDate() {
			return expiryDate;
		}

		public String getCustodian() {
			return custodian;
		}
		
		public String getCustodianPosition() {
			return custodianPosition;
		}

		public String getPropertyCode() {
			return propertyCode;
		}

		public String getSpecs() {
			return specs;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public String getTgDocumentNumber() {
			return tgDocumentNumber;
		}
	    public String getType() {
	        
	        return type;
	        
	    }
	    
	    public String getDate() {
	        
	        return date;
	        
	    }
	  
	    public String getSupplierName() {
	        
	        return supplierName;
	        
	    }

	    public String getSupplierAddress() {
    
	    	return supplierAddress;
    
	    }
	    
	    public String getDocumentNumber() {
	        
	        return documentNumber;
	        
	    }
	    
	    public String getReferenceNumber() {
	        
	        return referenceNumber;
	        
	    }
	    
	    public String getAccountNumber() {
	        
	        return accountNumber;
	        
	    }
	    
	    public String getAccountDescription() {
	        
	        return accountDescription;
	        
	    }
	    
	    public String getDescription() {
	        
	        return description;
	        
	    }
	    
	    
	  public String getApprovedBy() {
	        
	        return approvedBy;
	        
	    }
	  public String getCreatedBy() {
	      
	      return createdBy;
	      
	  }
	  
	  public String getCreatedByPosition() {
	      
	      return createdByPosition;
	      
	  }
	  
	    
	    public String getItemName() {
	        
	        return itemName;
	        
	    }
	    
	    public String getItemDescription() {
	        
	        return itemDescription;
	        
	    }
	    
	    public String getUnitMeasureShortName() {
	        
	        return unitMeasureShortName;
	        
	    }
	    
	    public String getLocationName() {
	        
	        return locationName;
	        
	    }

	    public Double getUnitCost() {
	        
	        return unitCost;
	        
	    }
	    
	    public Double getActualQuantity() {
	        
	        return actualQuantity;
	        
	    }
	    
	    public Double getAdjustQuantity() {
	        
	        return adjustQuantity;
	        
	    }

	    public Double getAveCost() {
	        
	        return aveCost;
	        
	    }
	  public Double getBranchCode() {
	        
	        return branchCode;
	        
	    }
	  public String getPostedBy() {
	      
	      return postedBy;
	      
	  }

}
