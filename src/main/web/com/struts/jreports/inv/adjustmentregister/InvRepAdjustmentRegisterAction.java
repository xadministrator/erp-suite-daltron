package com.struts.jreports.inv.adjustmentregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepAdjustmentRegisterController;
import com.ejb.txn.InvRepAdjustmentRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepAdjustmentRegisterAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("InvRepAdjustmentRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	InvRepAdjustmentRegisterForm actionForm = (InvRepAdjustmentRegisterForm)form;
      	
      	// reset report to null
      	actionForm.setReport(null);
      	
      	String frParam = Common.getUserPermission(user, Constants.INV_REP_ADJUSTMENT_REGISTER_ID);
      	
      	if (frParam != null) {
      		
      		if (frParam.trim().equals(Constants.FULL_ACCESS)) {
      			
      			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
      			if (!fieldErrors.isEmpty()) {
      				
      				saveErrors(request, new ActionMessages(fieldErrors));
      				
      				return mapping.findForward("invRepAdjustmentRegister");
      			}
      			
      		}
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	
/*******************************************************
   Initialize InvRepAdjustmentController EJB
*******************************************************/
      	
      	InvRepAdjustmentRegisterControllerHome homeADJ = null;
      	InvRepAdjustmentRegisterController ejbADJ = null;       
      	
      	try {
      		System.out.println("HEY");
      		homeADJ = (InvRepAdjustmentRegisterControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/InvRepAdjustmentRegisterControllerEJB", InvRepAdjustmentRegisterControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in InvRepAdjustmentRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbADJ = homeADJ.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in InvRepAdjustmentRegisterAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
/*******************************************************
   -- INV ADJ Go Action --
*******************************************************/

      	if(request.getParameter("goButton") != null &&
      			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      		
      		
      		if	(actionForm.getReportType().equals("Prod List")){
      			System.out.println("Prod List");
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescriptionTo())) {
	      				criteria.put("accountDescriptionTo", actionForm.getAccountDescriptionTo());
	      			}
	      			

	      			
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"PROD_LIST", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		System.out.println("4");
	      		parameters.put("type", "ISSUANCE");
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6");
	      		if(actionForm.getReportType() == "Prod List"){
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegisterProdList.jasper";
	      		}else{
	      			filename= "";
	      		}
      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegisterProdList.jasper");
      			}
      			
      			try {
      				System.out.println("7");
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				System.out.println("waaaaaaaaaa");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSProdList(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(
	      						JasperRunManagerExt.runReportToXls(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSProdList(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSProdList(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		
      		}else if(actionForm.getReportType().equals("Daily Balance Sheet")){
      			System.out.println("Daily Balance Sheet Action");
      			
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"Daily Balance Sheet", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		String xMonth="";
	      		String xYear="";
	      		if(actionForm.getDateFrom().length()>0){
	      		xMonth = actionForm.getDateFrom().substring(0, actionForm.getDateFrom().indexOf('/'));
	      		xYear = actionForm.getDateFrom().substring(actionForm.getDateFrom().lastIndexOf('/')+1);
	      		int x = Integer.parseInt(xMonth);
	      		if(x==1)
	      			xMonth ="January";
	      		else if(x==2)
	      			xMonth ="February";
	      		else if(x==3)
	      			xMonth ="March";
	      		else if(x==4)
	      			xMonth ="April";
	      		else if(x==5)
	      			xMonth ="May";
	      		else if(x==6)
	      			xMonth ="June";
	      		else if(x==7)
	      			xMonth ="July";
	      		else if(x==8)
	      			xMonth ="August";
	      		else if(x==9)
	      			xMonth ="September";
	      		else if(x==10)
	      			xMonth ="October";
	      		else if(x==11)
	      			xMonth ="November";
	      		else 
	      			xMonth ="December";}
	      		parameters.put("date", xMonth +" "+xYear);
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		parameters.put("InitialBalance", ejbADJ.getforwardedBal());
	      		System.out.println("ejbADJ.getforwardedBal(): "+ejbADJ.getforwardedBal());
	      		System.out.println("4");
	      		parameters.put("type", "ISSUANCE");
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6" + actionForm.getReportType());
	      		System.out.println("6" + user.getCompany());
	      		
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBalanceSheet.jasper";
	      			
	      		
      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepBalanceSheet.jasper");
      			}
      			try {
      				System.out.println("7");
      				System.out.println("filename "+filename);
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				
	      				
	      				System.out.println("wwwwwwwwwwww");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSBalanceSheet(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				System.out.println("ms-excel= ");
	      				System.out.println("filename= "+filename);
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(JasperRunManagerExt.runReportToXls(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSBalanceSheet(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
	      				
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSBalanceSheet(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
      		}
      		else if(actionForm.getReportType().equals("Posted Deliveries")){
      			System.out.println("Posted Deliveries Action");
      			
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"Posted Deliveries", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		String xMonth="";
	      		String xYear="";
	      		if(actionForm.getDateFrom().length()>0){
	      		xMonth = actionForm.getDateFrom().substring(0, actionForm.getDateFrom().indexOf('/'));
	      		xYear = actionForm.getDateFrom().substring(actionForm.getDateFrom().lastIndexOf('/')+1);
	      		int x = Integer.parseInt(xMonth);
	      		if(x==1)
	      			xMonth ="January";
	      		else if(x==2)
	      			xMonth ="February";
	      		else if(x==3)
	      			xMonth ="March";
	      		else if(x==4)
	      			xMonth ="April";
	      		else if(x==5)
	      			xMonth ="May";
	      		else if(x==6)
	      			xMonth ="June";
	      		else if(x==7)
	      			xMonth ="July";
	      		else if(x==8)
	      			xMonth ="August";
	      		else if(x==9)
	      			xMonth ="September";
	      		else if(x==10)
	      			xMonth ="October";
	      		else if(x==11)
	      			xMonth ="November";
	      		else 
	      			xMonth ="December";}
	      		//parameters.put("date", xMonth +" "+xYear);
	      		//parameters.put("printedBy", user.getUserName());
	      		//parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		//parameters.put("viewType", actionForm.getViewType());
	      		//parameters.put("InitialBalance", ejbADJ.getforwardedBal());
	      		//System.out.println("ejbADJ.getforwardedBal(): "+ejbADJ.getforwardedBal());
	      		System.out.println("4");
	      		//parameters.put("type", "ISSUANCE");
	      		
	      		//parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		//parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		//if (actionForm.getIncludedUnposted()) {
	      		//	parameters.put("includedUnposted", "YES");
	      		//} else {
	      		//	parameters.put("includedUnposted", "NO");
	      		//}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		parameters.put("asOfDate", actionForm.getDateFrom() +" to "+actionForm.getDateTo());
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		//parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6" + actionForm.getReportType());
	      		System.out.println("6" + user.getCompany());
	      	
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepPostedDeliveries.jasper";

      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepPostedDeliveries.jasper");
      			}
      			try {
      				System.out.println("7");
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				
	      				
	      				System.out.println("wwwwwwwwwwww");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSPostedDeliveries(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				System.out.println("ms-excel ");
	      				System.out.println("filename= "+filename);
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(
	      						JasperRunManagerExt.runReportToXls(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSPostedDeliveries(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSPostedDeliveries(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
      		}
      		else if(actionForm.getReportType().equals("Summary of Issuance")){
      			System.out.println("Summary of Issuance");
      			
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      				System.out.println("dateFrom::"+actionForm.getDateFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      				System.out.println("dateTo::"+actionForm.getDateTo());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			
	      			if (!Common.validateRequired(actionForm.getItemFrom())) {
	      				criteria.put("itemFrom", actionForm.getItemFrom());
	      			}
	      			
	      			if (!Common.validateRequired(actionForm.getItemTo())) {
	      				criteria.put("itemTo", actionForm.getItemTo());
	      			}
	      			
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"Summary of Issuance", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		//parameters.put("company", company);
	      		String xMonth="";
	      		String xYear="";
	      		if(actionForm.getDateFrom().length()>0){
	      		xMonth = actionForm.getDateFrom().substring(0, actionForm.getDateFrom().indexOf('/'));
	      		xYear = actionForm.getDateFrom().substring(actionForm.getDateFrom().lastIndexOf('/')+1);
	      		int x = Integer.parseInt(xMonth);
	      		if(x==1)
	      			xMonth ="JANUARY";
	      		else if(x==2)
	      			xMonth ="FEBRUARY";
	      		else if(x==3)
	      			xMonth ="MARCH";
	      		else if(x==4)
	      			xMonth ="APRIL";
	      		else if(x==5)
	      			xMonth ="MAY";
	      		else if(x==6)
	      			xMonth ="JUNE";
	      		else if(x==7)
	      			xMonth ="JULY";
	      		else if(x==8)
	      			xMonth ="AUGUST";
	      		else if(x==9)
	      			xMonth ="SEPTEMBER";
	      		else if(x==10)
	      			xMonth ="OCTOBER";
	      		else if(x==11)
	      			xMonth ="NOVEMBER";
	      		else 
	      			xMonth ="DECEMBER";}
	      		parameters.put("date","FOR THE MONTH OF "+ xMonth +" "+xYear);
	      		//parameters.put("printedBy", user.getUserName());
	      		//parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		//parameters.put("viewType", actionForm.getViewType());
	      		//parameters.put("InitialBalance", ejbADJ.getforwardedBal());
	      		System.out.println("ejbADJ.getforwardedBal(): "+ejbADJ.getforwardedBal());
	      		System.out.println("4");
	      		System.out.println("xMonth"+xMonth);
	      		System.out.println("xYear"+xYear);
	      		//parameters.put("type", "ISSUANCE");
	      		if (actionForm.getDateFrom() != null)  {
	      			//parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		
	      		if (actionForm.getDateTo() != null) {
	      			//parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		//parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		//parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			//parameters.put("includedUnposted", "YES");
	      		} else {
	      			//parameters.put("includedUnposted", "NO");
	      		}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		//parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6" + actionForm.getReportType());
	      		System.out.println("6" + user.getCompany());
	      	
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepSummaryOfIssuance.jasper";
	      			System.out.println("7 "+filename);
	      	
      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepSummaryOfIssuance.jasper");
      			}
      			try {
      				System.out.println("7");
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				
	      				
	      				System.out.println("wwwwwwwwwwww");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSSummaryOfIssuance(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(
	      						JasperRunManagerExt.runReportToXls(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSSummaryOfIssuance(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSSummaryOfIssuance(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
      		} 
      		else if(actionForm.getReportType().equals("RM Used")){
      			System.out.println("RM Used");
      			
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"RM_USED", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		System.out.println("4");
	      		parameters.put("type", "ISSUANCE");
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6");
	      		if(actionForm.getReportType() == "RM Used"){
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegisterRmUsed.jasper";
	      		}else{
	      			filename= "";
	      		}
      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegisterRmUsed.jasper");
      			}
      			try {
      				System.out.println("7");
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				System.out.println("wwwwwwwwwwww");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSRmUsed(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(
	      						JasperRunManagerExt.runReportToXls(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSRmUsed(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSRmUsed(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
      		} else if(actionForm.getReportType().equals("RM Variance")){
      			System.out.println("RM Variance");
      			

      			System.out.println("RM Used");
      			
      			ArrayList list = null;
	      		String company = null;
	      		
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			
	      				criteria.put("type", "ISSUANCE");
	      			
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"RM_VAR", user.getCmpCode());
	      			
	      			
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		System.out.println("4");
	      		parameters.put("type", "ISSUANCE");
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		//parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		//if (actionForm.getShowLineItems()) {
	      		//	parameters.put("showLineItems", "YES");
	      		//} else {
	      		//	parameters.put("showLineItems", "NO");
	      		//}
	      		
	      		//parameters.put("orderBy", actionForm.getOrderBy());
	      		//parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
	      		System.out.println("6");
	      		if(actionForm.getReportType() == "RM Used"){
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegisterRmVar.jasper";
	      		}else{
	      			filename= "";
	      		}
      			if (!new java.io.File(filename).exists()) {
      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegisterRmVar.jasper");
      			}
      			try {
      				System.out.println("7");
      				System.out.println("GROUP BY"+actionForm.getGroupBy());
	      			Report report = new Report();
	      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
	      				System.out.println("8");
	      				System.out.println("filename= "+filename);
	      				System.out.println("parameters= "+parameters);
	      				System.out.println("list= "+list.size());
	      				System.out.println("wwwwwwwwwwww");
	      				report.setBytes(
	      						
	      						JasperRunManager.runReportToPdf(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSRmVar(list,actionForm.getGroupBy())));
	      				System.out.println("9");  
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	      				report.setBytes(
	      						JasperRunManagerExt.runReportToXls(filename, parameters, 
	      								new InvRepAdjustmentRegisterDSRmVar(list, actionForm.getGroupBy())));   
	      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
	      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
	      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
	      						new InvRepAdjustmentRegisterDSRmVar(list, actionForm.getGroupBy())));												    
	      			}
	      			System.out.println("8");
	      			session.setAttribute(Constants.REPORT_KEY, report);
	      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
	      		} catch(Exception ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
      		
      			
      			
      		}else{
      			System.out.println("Report Type NULL");
      			ArrayList list = null;
	      		String company = null;
	      		// create criteria 
	      		if (request.getParameter("goButton") != null) {
	      			HashMap criteria = new HashMap(); 
	      			if (!Common.validateRequired(actionForm.getType())) {
	      				criteria.put("type", actionForm.getType());
	      			}
	      			if (!Common.validateRequired(actionForm.getAccountDescription())) {
	      				criteria.put("accountDescription", actionForm.getAccountDescription());
	      			}
	      			if (!Common.validateRequired(actionForm.getDateFrom())) {
	      				criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDateTo())) {
	      				criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			if (actionForm.getIncludedUnposted()) {	        	
	      				criteria.put("includedUnposted", "YES");
	      			}
	      			if (!actionForm.getIncludedUnposted()) {
	      				criteria.put("includedUnposted", "NO");
	      			}
	      			if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	      				criteria.put("referenceNumber", actionForm.getReferenceNumber());
	      			}
	      			// save criteria
	      			actionForm.setCriteria(criteria);
	      		}
	      		// branch map
	      		ArrayList adBrnchList = new ArrayList();
	      		// ArrayList adBrnchList2 = new ArrayList();
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(invRepAdjstmntBrList.getBranchCheckbox() == true) {
	      				AdBranchDetails mdetails = new AdBranchDetails();	                    
	      				mdetails.setBrCode(invRepAdjstmntBrList.getBrCode());
	      				adBrnchList.add(mdetails);
	      			}
	      		}
	      		try {
	      			// get company
	      			AdCompanyDetails adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());
	      			company = adCmpDetails.getCmpName();
	      			// execute report
	      			list = ejbADJ.executeInvRepAdjustmentRegister(actionForm.getCriteria(),
	      					actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getShowLineItems(),actionForm.getDateFrom(),actionForm.getDateTo(), adBrnchList,"BLANK", user.getCmpCode());
	      		} catch (GlobalNoRecordFoundException ex) {
	      			errors.add(ActionMessages.GLOBAL_MESSAGE,
	      					new ActionMessage("adjustmentRegister.error.noRecordFound"));
	      		} catch(EJBException ex) {
	      			if(log.isInfoEnabled()) {
	      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
	      						" session: " + session.getId());
	      			}
	      			return(mapping.findForward("cmnErrorPage"));
	      		}
	      		if (!errors.isEmpty()) {
	      			saveErrors(request, new ActionMessages(errors));
	      			return mapping.findForward("invRepAdjustmentRegister");
	      		}
	      		// fill report parameters, fill report to pdf and set report session
	      		Map parameters = new HashMap();
	      		parameters.put("company", company);
	      		parameters.put("date", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("printedBy", user.getUserName());
	      		parameters.put("datePrinted", Common.getGcCurrentDateWoTime().getTime());
	      		parameters.put("viewType", actionForm.getViewType());
	      		System.out.println("4");
	      		parameters.put("type", actionForm.getType());
	      		if (actionForm.getDateFrom() != null)  {
	      			parameters.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));				
	      		}
	      		if (actionForm.getDateTo() != null) {
	      			parameters.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));	    	
	      		}
	      		parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      		parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      		parameters.put("referenceNumber", actionForm.getReferenceNumber());
	      		if (actionForm.getIncludedUnposted()) {
	      			parameters.put("includedUnposted", "YES");
	      		} else {
	      			parameters.put("includedUnposted", "NO");
	      		}
	      		if (actionForm.getShowLineItems()) {
	      			parameters.put("showLineItems", "YES");
	      		} else {
	      			parameters.put("showLineItems", "NO");
	      		}
	      		
	      		parameters.put("orderBy", actionForm.getOrderBy());
	      		parameters.put("groupBy", actionForm.getGroupBy());
	      		System.out.println("5");
	      		String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrArListSize(); j++) {
	      			InvRepAdjustmentRegisterBranchList brList = (InvRepAdjustmentRegisterBranchList)actionForm.getInvRepBrArByIndex(j);
	      			if(brList.getBranchCheckbox() == true) {
	      				if(first) {
	      					branchMap = brList.getBrName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBrName();
	      				}
	      			}
	      		}
	      		parameters.put("branchMap", branchMap);
	      		String filename = null;
				      		if (actionForm.getShowLineItems() && !actionForm.getGroupBy().equals("ITEM NAME")) {
				      			filename = "C:opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegisterLineItems.jasper";
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegisterLineItems.jasper");
				      			}
				      		}  else  if (actionForm.getShowLineItems() && actionForm.getGroupBy().equals("ITEM NAME")){
				      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegisterLineItemsGroupByItems.jasper";
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegisterLineItemsGroupByItems.jasper");
				      			}
				      		} else {
				      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRegister.jasper";
				      			if (!new java.io.File(filename).exists()) {
				      				filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRegister.jasper");
				      			}
				      		}

				      		try {
				      			Report report = new Report();
				      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				      				report.setBytes(
				      						JasperRunManager.runReportToPdf(filename, parameters, 
				      								new InvRepAdjustmentRegisterDS(list,actionForm.getGroupBy())));   
				      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
				      				report.setBytes(
				      						JasperRunManagerExt.runReportToXls(filename, parameters, 
				      								new InvRepAdjustmentRegisterDS(list, actionForm.getGroupBy())));   
				      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				      						new InvRepAdjustmentRegisterDS(list, actionForm.getGroupBy())));												    
				      			}
				      			session.setAttribute(Constants.REPORT_KEY, report);
				      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
				      		} catch(Exception ex) {
				      			if(log.isInfoEnabled()) {
				      				log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
				      						" session: " + session.getId());
				      			}
				      			return(mapping.findForward("cmnErrorPage"));
				      		}
      		
								
      		
      		
      		}    		        	          
/*******************************************************
   -- INV ADJ Close Action --
*******************************************************/
		    
      	} else if (request.getParameter("closeButton") != null) {
      		
      		return(mapping.findForward("cmnMain"));
         	
/*******************************************************
   -- INV ADJ Load Action --
*******************************************************/
      		
      	}
      	
      	if(frParam != null) {
      		
      		try {
      			
      			actionForm.reset(mapping, request);
      			
      			ArrayList list = null;
      			Iterator i = null;
      			
      			actionForm.clearInvRepBrArList();
      			
      			list = ejbADJ.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
      			
      			i = list.iterator();
      			//int ctr = 1;
      			while(i.hasNext()) {
      				
      				AdBranchDetails details = (AdBranchDetails)i.next();
      				
      				InvRepAdjustmentRegisterBranchList invRepAdjstmntBrList = new InvRepAdjustmentRegisterBranchList(actionForm,
      						details.getBrCode(),
							details.getBrBranchCode(), details.getBrName());
      				if (details.getBrHeadQuarter() == 1) invRepAdjstmntBrList.setBranchCheckbox(true);
      				//ctr++;
      				
      				actionForm.saveInvRepBrArList(invRepAdjstmntBrList);
      				
      			}
      			
      		} catch (GlobalNoRecordFoundException ex) {
      			
      		} catch (EJBException ex) {
      			
      			if (log.isInfoEnabled()) {
      				
      				log.info("EJBException caught in InvRepAdjustmentRegisterAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				return mapping.findForward("cmnErrorPage"); 
      				
      			}
      			
      		} 
      		
      		return(mapping.findForward("invRepAdjustmentRegister"));		          
      		
      	} else {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      		saveErrors(request, new ActionMessages(errors));
      		
      		return(mapping.findForward("cmnMain"));
      		
      	}
      	
      } catch(Exception e) { 
      	
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	if(log.isInfoEnabled()) {
      		
      		log.info("Exception caught in InvRepAdjustmentRegisterAction.execute(): " + e.getMessage()
      				+ " session: " + session.getId());
      		
      	}   
      	
      	return(mapping.findForward("cmnErrorPage"));   
      	
      }
   }
}
