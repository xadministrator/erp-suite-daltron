package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepAdjustmentRegisterDetails;

public class InvRepAdjustmentRegisterDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRegisterDS(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	InvRepAdjustmentRegisterDetails details = (InvRepAdjustmentRegisterDetails)i.next();
             
         String group = new String("");
         
         if (groupBy.equals("ITEM NAME")) {
         	
         	group = details.getAdjItemName();
         	
         } else if (groupBy.equals("DATE")) {
         	
         	group = details.getAdjDate().toString();
         	
         } 
         
         InvRepAdjustmentRegisterData argData = new InvRepAdjustmentRegisterData(details.getAdjDate(), 
	   	     details.getAdjDescription(), details.getAdjReferenceNumber(), 
	   		 new Double(details.getAdjAmount()), group, details.getAdjDocumentNumber(), details.getAdjType(),details.getAdjAccountDescription(),
			 details.getAdjItemName(), details.getAdjItemDesc(), details.getAdjLocation(),
			 new Double (details.getAdjQuantity()), details.getAdjUnit(), new Double(details.getAdjUnitCost()));
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getDate();       
   	}else if("description".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getDescription();
   	}else if("referenceNumber".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getReferenceNumber();
   	}else if("amount".equals(fieldName)){
   		 
   		value = Math.abs(((InvRepAdjustmentRegisterData)data.get(index)).getAmount()); 
   	}else if("groupBy".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getGroupBy();
   	}else if("documentNumber".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getDocumentNumber();
   	}else if("type".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getType();
   	}else if("itemName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getItemName();
   		System.out.print(value+"item");
   	}else if("itemDesc".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getItemDesc();
   	}else if("location".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getLocation();
   	}else if("quantity".equals(fieldName)){
   		value = Math.abs(((InvRepAdjustmentRegisterData)data.get(index)).getQuantity()); 
   		 
   	}else if("unit".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getUnit();
   	}else if("unitCost".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getUnitCost();
   	}else if("accountDescription".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterData)data.get(index)).getAccountDescription();
   		System.out.print(value+"Account description");
   	}
   	return(value);
   }
}
