package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


import com.util.InvRepAdjustmentRegisterBalanceSheetDetails;
import com.util.InvRepAdjustmentRegisterRmVarDetails;

public class InvRepAdjustmentRegisterDSBalanceSheet implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRegisterDSBalanceSheet(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	InvRepAdjustmentRegisterBalanceSheetDetails details = (InvRepAdjustmentRegisterBalanceSheetDetails)i.next();
             
      	InvRepAdjustmentRegisterDataBalanceSheet argData = new InvRepAdjustmentRegisterDataBalanceSheet(details.getIlDate(),details.getIlForwardedBalance(),details.getIlDeliveries(),
        		 details.getIlDocNumber(),details.getIlAjustmentCost(),details.getIlReferenceNumber(),details.getIlWithdrawals(),
        		 details.getIlEndingBalance());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getDate();       
   	}else if("forwardedBalance".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getforwardedBalance();
   	}else if("deliveries".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getdeliveries();
   	}else if("docNum".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getDocNum();
   	}else if("adjustmentCost".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getadjCost();
   	}else if("refNum".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getRefNum();
   	}else if("withdrawals".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getWithdrawals();
   		//System.out.println("WITHDRAWALS " +value);
   	}else if("endingBalance".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataBalanceSheet)data.get(index)).getEndingBalance();
   		//System.out.println(value);
   	}
   	return(value);
   	
}
}
