package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepAdjustmentRegisterPostedDeliveriesDetails;
import com.util.InvRepAdjustmentRegisterRmVarDetails;

public class InvRepAdjustmentRegisterDSPostedDeliveries implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRegisterDSPostedDeliveries(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
    	  InvRepAdjustmentRegisterPostedDeliveriesDetails details = (InvRepAdjustmentRegisterPostedDeliveriesDetails)i.next();
             
      	InvRepAdjustmentRegisterDataPostedDeliveries argData = new InvRepAdjustmentRegisterDataPostedDeliveries(details.getIL_DOC_NUM(),details.getIL_ITEM_DESC(),details.getIL_ITEM_UNIT(),
        		 details.getIL_ITEM_CODE(),details.getIL_ITEM_QUANTITY(),details.getIL_ITEM_COST());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getDate();       
   	}else if("documentNumber".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getDocNum();
   	}else if("itemDescription".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getItemDesc();
   	}else if("unit".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getItemUnit();
   	}else if("itemName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getItemCode();
   	}else if("quantity".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getItemQuantity();
   	}else if("unitCost".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataPostedDeliveries)data.get(index)).getItemCost();
   	}
   	return(value);
   	
}
}
