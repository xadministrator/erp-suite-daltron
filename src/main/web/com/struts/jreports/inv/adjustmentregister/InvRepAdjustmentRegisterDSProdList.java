package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


import com.util.InvRepAdjustmentRegisterProdListDetails;

public class InvRepAdjustmentRegisterDSProdList implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRegisterDSProdList(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	InvRepAdjustmentRegisterProdListDetails details = (InvRepAdjustmentRegisterProdListDetails)i.next();
             
         InvRepAdjustmentRegisterDataProdList argData = new InvRepAdjustmentRegisterDataProdList(details.getIlIiName(),details.getIlIiDescription(), details.getIlIiYield(),details.getIlIiUomName(),
        		 details.getIlIiMonth1(),details.getIlIiMonth2(),details.getIlIiMonth3(),details.getIlIiMonth4(),
        		 details.getIlIiMonth5(),details.getIlIiMonth6(),details.getIlIiMonth7(),details.getIlIiMonth8(),
        		 details.getIlIiMonth9(),details.getIlIiMonth10(),details.getIlIiMonth11(),details.getIlIiMonth12());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getItemName();  
   		System.out.println(value);
   	}else if("description".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getDescription();
   	}else if("yield".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getYield();
   	}else if("uomName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getUomName();
   	}else if("month1".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth1();
   	}else if("month2".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth2();
   	}else if("month3".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth3();
   	}else if("month4".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth4();
   	}else if("month5".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth5();
   	}else if("month6".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth6();
   	}else if("month7".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth7();
   	}else if("month8".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth8();
   	}else if("month9".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth9();
   	}else if("month10".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth10();
   	}else if("month11".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth11();
   	}else if("month12".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataProdList)data.get(index)).getMonth12();
    }
   	return(value);
   	
}
}
