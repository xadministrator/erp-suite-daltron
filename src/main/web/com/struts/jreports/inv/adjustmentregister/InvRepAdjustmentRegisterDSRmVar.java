package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


import com.util.InvRepAdjustmentRegisterRmVarDetails;

public class InvRepAdjustmentRegisterDSRmVar implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRegisterDSRmVar(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
      	InvRepAdjustmentRegisterRmVarDetails details = (InvRepAdjustmentRegisterRmVarDetails)i.next();
             
      	InvRepAdjustmentRegisterDataRmVar argData = new InvRepAdjustmentRegisterDataRmVar(details.getIlIiName(),details.getIlIiDescription(),details.getIlIiUomName(),
        		 details.getIlIiStdUnitCost(),details.getIlIiActualQtyUsed(),details.getIlIiStdQty(),details.getIlIiVarianceQty(),
        		 details.getIlIiActualAmnt(),details.getIlIiStdAmnt(),details.getIlIiVarianceAmnt());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getItemName();       
   	}else if("description".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getDescription();
   	}else if("uomName".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getUomName();
   	}else if("stdUnitCost".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getStdUnitCost();
   	}else if("actualQtyUsed".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getActualQtyUsed();
   	}else if("stdQty".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getStdQty();
   	}else if("varianceQty".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getVarianceQty();
   	}else if("actualAmnt".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getActualAmnt();
   	}else if("stdAmnt".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getStdAmnt();
   	}else if("varianceAmnt".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataRmVar)data.get(index)).getVarianceAmnt();
   		
    }
   	return(value);
   	
}
}
