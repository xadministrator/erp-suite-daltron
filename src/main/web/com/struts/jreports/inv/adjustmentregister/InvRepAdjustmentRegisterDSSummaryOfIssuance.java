package com.struts.jreports.inv.adjustmentregister;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


import com.util.InvRepAdjustmentRegisterSummaryOfIssuanceDetails;
import com.util.InvRepAdjustmentRegisterRmVarDetails;

public class InvRepAdjustmentRegisterDSSummaryOfIssuance implements JRDataSource{

   private ArrayList data = new ArrayList();
  
   private int index = -1;

   public InvRepAdjustmentRegisterDSSummaryOfIssuance(ArrayList list, String groupBy) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
    	  InvRepAdjustmentRegisterSummaryOfIssuanceDetails details = (InvRepAdjustmentRegisterSummaryOfIssuanceDetails)i.next();
             
    	  InvRepAdjustmentRegisterDataSummaryOfIssuance argData = new InvRepAdjustmentRegisterDataSummaryOfIssuance(details.getIlName(),details.getIlQuantity(),details.getIlTotalCost(),
        		 details.getIlDescription(),details.getIlDepartment(),details.getOrderBy(),details.getIL_REFERENCE(),details.getDATE());
		    
         data.add(argData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemCode".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getitemCode(); 
    }else if("quantity".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getQuantity();
   	}else if("totalCost".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getTotalCost();
   	}else if("groupBy".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getGroupBy();
   	}else if("description".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getDescription();
   	}else if("reference".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getReference();
   	}else if("date".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getIlDate();
   	}else if("department".equals(fieldName)){
   		value = ((InvRepAdjustmentRegisterDataSummaryOfIssuance)data.get(index)).getDepartment();
   	}
   	return(value);
   	
}
}
