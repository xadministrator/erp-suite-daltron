package com.struts.jreports.inv.adjustmentregister;


public class InvRepAdjustmentRegisterDataBalanceSheet implements java.io.Serializable{
	
	   private String Date = null;
	   private Double forwardedBalance = null;
	   private Double deliveries = null;
	   private String DocNum = null;
	   private Double adjCost = null;
	   private String RefNum = null;
	   private Double withdrawals = null;
	   private Double EndingBalance = null;

	   
	   public InvRepAdjustmentRegisterDataBalanceSheet(String Date, Double forwardedBalance,Double deliveries, 
			   String DocNum, Double adjCost, String RefNum, Double withdrawals, Double EndingBalance) {

	      	this.Date = Date;
	       	this.forwardedBalance = forwardedBalance;
	       	this.deliveries = deliveries;
	       	this.DocNum = DocNum;
	       	this.adjCost = adjCost;
	       	this.RefNum = RefNum;
	       	this.withdrawals = withdrawals;
	       	this.EndingBalance = EndingBalance;

      		      	
	   }

	   public String getDate() {
		   	
		   	  return Date;
		   	  
		}
	   
	   public Double getforwardedBalance() {
	   	
	   	  return forwardedBalance;
	   	  
	   }
	   
	   public Double getdeliveries() {
		   	
		   	  return deliveries;
		   	  
	  }
	   
	   public String getDocNum() {
		   	
		   	  return DocNum;
		   	  
	  }
	   public Double getadjCost() {
		   	
		   	  return adjCost;
		   	  
	  }
	   public String getRefNum() {
		   	
		   	  return RefNum;
		   	  
	  }
	   public Double getWithdrawals() {
		   	
		   	  return withdrawals;
		   	  
	  }
	   public Double getEndingBalance() {
		   	
		   	  return EndingBalance;
		   	  
	  }

	  
}
