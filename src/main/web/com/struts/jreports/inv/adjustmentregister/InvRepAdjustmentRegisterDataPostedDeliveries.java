package com.struts.jreports.inv.adjustmentregister;


public class InvRepAdjustmentRegisterDataPostedDeliveries implements java.io.Serializable{
	
	   private String Date = null;
	   private String DocNum = null;
	   private String ItemDesc = null;
	   private String ItemUnit = null;
	   private String ItemCode = null;
	   private Double ItemQuantity = null;
	   private Double ItemCost = null;
	   
	   public InvRepAdjustmentRegisterDataPostedDeliveries(String DocNum,String ItemDesc, 
			   String ItemUnit, String ItemCode, Double ItemQuantity, Double ItemCost) {
	      	
	       	this.DocNum = DocNum;
	       	this.ItemDesc = ItemDesc;
	       	this.ItemUnit=ItemUnit;
	       	this.ItemCode=ItemCode;
	       	this.ItemQuantity=ItemQuantity;
	       	this.ItemCost=ItemCost;     		      	
	   }

	   public String getDate() {		   	
		   	  return Date;		   	  
		}
	   
	   public String getItemCode() {		   	
		   	  return ItemCode;		   	  
		}
	   
	   public String getItemDesc() {		   	
		   	  return ItemDesc;		   	  
		}
	   
	   public String getItemUnit() {		   	
		   	  return ItemUnit;		   	  
		}
	   
	   public Double getItemQuantity() {		   	
		   	  return ItemQuantity;		   	  
		}
	   
	   public Double getItemCost() {		   	
		   	  return ItemCost;	   	  
		}

	   
	   public String getDocNum() {		   	
		   	  return DocNum;		   	  
	  }



	  
}
