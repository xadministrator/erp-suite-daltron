package com.struts.jreports.inv.adjustmentregister;


public class InvRepAdjustmentRegisterDataRmUsed implements java.io.Serializable{
	
	   private String itemName = null;
	   private String description = null;
	   private String uomName = null;
	   private Double month1 = 0d;
	   private Double month2 = 0d;
	   private Double month3 = 0d;
	   private Double month4 = 0d;
	   private Double month5 = 0d;
	   private Double month6 = 0d;
	   private Double month7 = 0d;
	   private Double month8 = 0d;
	   private Double month9 = 0d;
	   private Double month10 = 0d;
	   private Double month11 = 0d;
	   private Double month12 = 0d;

	   
	   public InvRepAdjustmentRegisterDataRmUsed(String itemName, String description,String uomName, 
			   Double month1, Double month2, Double month3, Double month4, 
			   Double month5, Double month6, Double month7, Double month8,
			   Double month9, Double month10, Double month11, Double month12) {

	      	this.itemName = itemName;
	       	this.description = description;
	       	this.uomName = uomName;
	       	this.month1 = month1;
	       	this.month2 = month2;
	       	this.month3 = month3;
	       	this.month4 = month4;
	       	this.month5 = month5;
	       	this.month6 = month6;
	       	this.month7 = month7;
	       	this.month8 = month8;
	       	this.month9 = month9;
	       	this.month10 = month10;
	       	this.month11 = month11;
	       	this.month12 = month12;
	       	
	
	      		      	
	   }

	   public String getItemName() {
		   	
		   	  return itemName;
		   	  
		}
	   
	   public String getDescription() {
	   	
	   	  return description;
	   	  
	   }
	   
	   public String getUomName() {
		   	
		   	  return uomName;
		   	  
	  }
	   
	   public Double getMonth1() {
		   	
		   	  return month1;
		   	  
		}
	   
	   public Double getMonth2() {
		   	
		   	  return month2;
		   	  
		}
	   
	   public Double getMonth3() {
		   	
		   	  return month3;
		   	  
		}
	   
	   public Double getMonth4() {
		   	
		   	  return month4;
		   	  
		}
	   
	   public Double getMonth5() {
		   	
		   	  return month5;
		   	  
		}
	   
	   public Double getMonth6() {
		   	
		   	  return month6;
		   	  
		}
	   
	   public Double getMonth7() {
		   	
		   	  return month7;
		   	  
		}
	   
	   public Double getMonth8() {
		   	
		   	  return month8;
		   	  
		}
	   
	   public Double getMonth9() {
		   	
		   	  return month9;
		   	  
		}
	   
	   public Double getMonth10() {
		   	
		   	  return month10;
		   	  
		}
	   
	   public Double getMonth11() {
		   	
		   	  return month11;
		   	  
		}
	   public Double getMonth12() {
		   	
		   	  return month12;
		   	  
		}
	  
	   
	  
	 
}
