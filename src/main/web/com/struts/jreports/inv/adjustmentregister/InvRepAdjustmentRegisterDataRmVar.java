package com.struts.jreports.inv.adjustmentregister;


public class InvRepAdjustmentRegisterDataRmVar implements java.io.Serializable{
	
	   private String itemName = null;
	   private String description = null;
	   private String uomName = null;
	   private Double stdUnitCost = null;
	   private Double actualQtyUsed = null;
	   private Double stdQty = null;
	   private Double varianceQty = null;
	   private Double actualAmnt = null;
	   private Double stdAmnt = null;
	   private Double varianceAmnt = null;
	   
	   public InvRepAdjustmentRegisterDataRmVar(String itemName, String description,String uomName, 
			   Double stdUnitCost, Double actualQtyUsed, Double stdQty, Double varianceQty, Double actualAmnt, Double stdAmnt, Double varianceAmnt) {

	      	this.itemName = itemName;
	       	this.description = description;
	       	this.uomName = uomName;
	       	this.stdUnitCost = stdUnitCost;
	       	this.actualQtyUsed = actualQtyUsed;
	       	this.stdQty = stdQty;
	       	this.varianceQty = varianceQty;
	       	this.actualAmnt = actualAmnt;
	       	this.stdAmnt = stdAmnt;
	       	this.varianceAmnt = varianceAmnt;
      		      	
	   }

	   public String getItemName() {
		   	
		   	  return itemName;
		   	  
		}
	   
	   public String getDescription() {
	   	
	   	  return description;
	   	  
	   }
	   
	   public String getUomName() {
		   	
		   	  return uomName;
		   	  
	  }
	   
	   public Double getStdUnitCost() {
		   	
		   	  return stdUnitCost;
		   	  
	  }
	   public Double getActualQtyUsed() {
		   	
		   	  return actualQtyUsed;
		   	  
	  }
	   public Double getStdQty() {
		   	
		   	  return stdQty;
		   	  
	  }
	   public Double getVarianceQty() {
		   	
		   	  return varianceQty;
		   	  
	  }
	   public Double getActualAmnt() {
		   	
		   	  return actualAmnt;
		   	  
	  }
	   public Double getStdAmnt() {
		   	
		   	  return stdAmnt;
		   	  
	  }
	   public Double getVarianceAmnt() {
		   	
		   	  return varianceAmnt;
		   	  
	  }
	  
}
