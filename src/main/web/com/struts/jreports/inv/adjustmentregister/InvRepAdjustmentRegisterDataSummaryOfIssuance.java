package com.struts.jreports.inv.adjustmentregister;

import java.util.Date;


public class InvRepAdjustmentRegisterDataSummaryOfIssuance implements java.io.Serializable{
	
	   private String itemCode = null;
	   private Double Quantity = null;
	   private Double TotalCost = null;
	   private String Decription = null;
	   private String Department = null;
	   private String Reference = null;
	   private Date ilDate = null;

	   private String groupBy = null;
	   
	   public InvRepAdjustmentRegisterDataSummaryOfIssuance(String itemCode, Double Quantity,Double TotalCost, 
			   String Decription,  String Department ,String groupBy,String Reference,Date ilDate) {

	      	this.itemCode = itemCode;
	       	this.Quantity = Quantity;
	       	this.TotalCost = TotalCost;
	       	this.Decription = Decription;
	       	this.Department = Department;
	       	this.groupBy = groupBy;
	       	this.Reference = Reference;
	       	this.ilDate = ilDate;

      		      	
	   }

	   public String getReference() {
		return Reference;
	}

	public void setReference(String reference) {
		Reference = reference;
	}

	public Date getIlDate() {
		return ilDate;
	}

	public void setIlDate(Date ilDate) {
		this.ilDate = ilDate;
	}

	public String getitemCode() {
		   	
		   	  return itemCode;
		   	  
		}
	   
	   public Double getQuantity() {
	   	
	   	  return Quantity;
	   	  
	   }
	   
	   public Double getTotalCost() {
		   	
		   	  return TotalCost;
		   	  
	  }
	   
	   public String getGroupBy() {
		   	
		   	  return groupBy;
		   	  
		   }
	   
	   public String getDescription() {
		   	
		   	  return Decription;
		   	  
	  }
	 
	   public String getDepartment() {
		   	
		   	  return Department;
		   	  
	  }
	

	  
}
