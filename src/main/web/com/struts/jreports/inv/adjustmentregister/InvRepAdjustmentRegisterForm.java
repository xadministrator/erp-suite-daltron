package com.struts.jreports.inv.adjustmentregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepAdjustmentRegisterForm extends ActionForm implements Serializable{

   private String type = null;
 
   private ArrayList typeList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String itemFrom = null;
   private String itemTo = null;
   private String accountDescription = null;
   private String accountDescriptionTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String referenceNumber = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   

   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String reportType = null;
   private ArrayList reportTypeList = new ArrayList();
   private boolean includedUnposted = false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String groupBy = null;
   private boolean showLineItems = false;
   private ArrayList groupByList = new ArrayList();
   private boolean showLineItemsChecked = false;
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   private ArrayList invRepBrArList = new ArrayList();

   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public String getDateFrom(){
   		return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }
   
   public String getItemTo(){
  		return(itemTo);
  }

  public void setItemTo(String itemTo){
     this.itemTo = itemTo;
  }
  
  public String getItemFrom(){
 		return(itemFrom);
 }

 public void setItemFrom(String itemFrom){
    this.itemFrom = itemFrom;
 }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }
   
   
   public String getAccountDescription(){
	      return(accountDescription);
	   }

	   public void setAccountDescription(String accountDescription){
	      this.accountDescription = accountDescription;
	   }
	   
	   public String getAccountDescriptionTo(){
		      return(accountDescriptionTo);
		   }

		   public void setAccountDescriptionTo(String accountDescriptionTo){
		      this.accountDescriptionTo = accountDescriptionTo;
		   }
   
   public String getDocumentNumberFrom(){
      return(documentNumberFrom);
   }

   public void setDocumentNumberFrom(String documentNumberFrom){
      this.documentNumberFrom = documentNumberFrom;
   }

   public String getDocumentNumberTo(){
      return(documentNumberTo);
   }

   public void setDocumentNumberTo(String documentNumberTo){
      this.documentNumberTo = documentNumberTo;
   }

   public String getReferenceNumber(){
    return(referenceNumber);
   }

   public void setReferenceNumber(String referenceNumber){
    this.referenceNumber = referenceNumber;
   }
 
   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getType(){
      return(type);
   }

   public void setType(String type){
      this.type = type;
   }
 

   
   public ArrayList getTypeList(){
      return(typeList);
   }

   public void setTypeList(String type){
      typeList.add(type);
   }

   public void clearTypeList(){
      typeList.clear();
   }
 
   public String getOrderBy(){
   	  return(orderBy);   	
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return orderByList;
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReportType(){
   	  return(reportType);   	
   }
   
   public void setReportType(String reportType){
   	  this.reportType = reportType;
   }
   
   public ArrayList getReportTypeList(){
   	  return reportTypeList;
   }

   public boolean getIncludedUnposted() {
   	
   	  return includedUnposted;
   	
   }
   
   public void setIncludedUnposted(boolean includedUnposted) {
   	
   	  this.includedUnposted = includedUnposted;
   	
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getGroupBy() {
   	
   	  return groupBy;
   	  
   }
   
   public void setGroupBy(String groupBy) {
   	
   	  this.groupBy = groupBy;
   	  
   }
  
   public ArrayList getGroupByList() {
   	
   	  return groupByList;
   	  
   }
 
   public Object[] getInvRepBrArList(){
       
       return invRepBrArList.toArray();
       
   }
   
   public InvRepAdjustmentRegisterBranchList getInvRepBrArByIndex(int index){
       
       return ((InvRepAdjustmentRegisterBranchList)invRepBrArList.get(index));
       
   }
   
   public int getInvRepBrArListSize(){
       
       return(invRepBrArList.size());
       
   }
   
   public void saveInvRepBrArList(Object newInvRepBrArList){
       
   	invRepBrArList.add(newInvRepBrArList);   	  
       
   }
   
   public void clearInvRepBrArList(){
       
   	invRepBrArList.clear();
       
   }
   
   public void setInvRepBrArList(ArrayList invRepBrArList) {
       
       this.invRepBrArList = invRepBrArList;
       
   }
   
   public boolean getShowLineItems() {
   	
   	  return showLineItems;
   	
   }
   
   public void setShowLineItems(boolean showLineItems) {
   	
   	  this.showLineItems = showLineItems;
   	
   }
   
   public boolean getShowLineItemsChecked() {
   	
   	  return showLineItemsChecked;
   	
   }
   
   public void setShowLineItemsChecked(boolean showLineItemsChecked) {
   	
   	  this.showLineItemsChecked = showLineItemsChecked;
   	
   }
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<invRepBrArList.size(); i++) {
           
           InvRepAdjustmentRegisterBranchList  list = (InvRepAdjustmentRegisterBranchList)invRepBrArList.get(i);
           list.setBranchCheckbox(false);	       
           
       }  
       
      goButton = null;
      closeButton = null;
      type = Constants.GLOBAL_BLANK;
      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
      typeList.add("GENERAL");
      typeList.add("WASTAGE");
      typeList.add("VARIANCE");
      typeList.add("REQUEST");
      typeList.add("ISSUANCE");
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      
      reportTypeList.clear();
      reportTypeList.add(Constants.GLOBAL_BLANK);
      reportTypeList.add("Prod List");
      reportTypeList.add("RM Used");
      reportTypeList.add("RM Variance");
      reportTypeList.add("Daily Balance Sheet");
      reportTypeList.add("Summary of Issuance");
      reportTypeList.add("Posted Deliveries");
      reportType = Constants.GLOBAL_BLANK;
      
      orderByList.clear();
      orderByList.add(Constants.INV_REP_ADJUSTMENT_REGISTER_ORDER_BY_DATE);
      orderByList.add(Constants.INV_REP_ADJUSTMENT_REGISTER_ORDER_BY_DOC_NUM);
      orderBy = Constants.INV_REP_ADJUSTMENT_REGISTER_ORDER_BY_DATE;
      groupByList.clear();
      groupByList.add(Constants.GLOBAL_BLANK);
      groupByList.add(Constants.INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_ITEM_NAME);
      groupByList.add(Constants.INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_DATE);
      groupBy = Constants.GLOBAL_BLANK;
      includedUnposted = false;
	  showLineItems = false;
      showLineItemsChecked = true;
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(!Common.validateDateFormat(dateFrom)){
		    errors.add("dateFrom", new ActionMessage("adjustmentRegister.error.dateFromInvalid"));
		 }
		 if(!Common.validateDateFormat(dateTo)){
	            errors.add("dateTo", new ActionMessage("adjustmentRegister.error.dateToInvalid"));
		 }
		 if(!Common.validateStringExists(typeList, type)){
	            errors.add("type", new ActionMessage("adjustmentRegister.error.typeInvalid"));
		 }
		 if(!showLineItems && (groupBy.equals(Constants.INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_ITEM_NAME) ||
		 		groupBy.equals(Constants.INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_DATE))) {
		 		errors.add("groupBy", new ActionMessage("adjustmentRegister.error.groupByInvalid"));
		 }
		 if (type != null && (type.equals("Prod List") || type.equals("RM Used")) && !Common.validateRequired(dateTo)) {
			 errors.add("dateTo", new ActionMessage("adjustmentRegister.error.dateToRequired"));
			 
		 }
      }
      return(errors);
   }
}
