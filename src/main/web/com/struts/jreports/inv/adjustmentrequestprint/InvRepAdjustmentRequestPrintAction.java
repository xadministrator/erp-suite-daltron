package com.struts.jreports.inv.adjustmentrequestprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepAdjustmentRequestPrintController;
import com.ejb.txn.InvRepAdjustmentRequestPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepAdjustmentRequestPrintDetails;

public final class InvRepAdjustmentRequestPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepAdjustmentRequestPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvRepAdjustmentRequestPrintForm actionForm = (InvRepAdjustmentRequestPrintForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.INV_REP_ADJUSTMENT_REQUEST_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize InvRepAdjustmenRequestPrintController EJB
*******************************************************/

         InvRepAdjustmentRequestPrintControllerHome homeADJ = null;
         InvRepAdjustmentRequestPrintController ejbADJ = null;       

         try {
         	
             homeADJ = (InvRepAdjustmentRequestPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepAdjustmentRequestPrintControllerEJB", InvRepAdjustmentRequestPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepAdjustmentRequestPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
             ejbADJ = homeADJ.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepAdjustmentRequestPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getInvRepAdjustmentRequestCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    InvRepAdjustmentRequestPrintDetails details = null; 
				
			    ArrayList list = null;   
			    
				try {
					
					ArrayList atrCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		atrCodeList.add(new Integer(request.getParameter("adjustmentCode")));
				
					}
	            	
	            	list = ejbADJ.executeInvRepAdjustmentRequestPrint(atrCodeList, user.getCmpCode());
	            
			        // get company
			       
			        adCmpDetails = ejbADJ.getAdCompany(user.getCmpCode());	
			       
			        // get parameters
	      			
	      			Iterator i = list.iterator();
	      			
	      			while (i.hasNext()) {
	      				
	      				details = (InvRepAdjustmentRequestPrintDetails)i.next();

	      			}
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentRequestPrint.error.adjustmentRequestAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvAdjustmentRequestPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("invRepAdjustmentRequestPrint");

	             }
				
				// fill report parameters, fill report to pdf and set report session  	    	               
			    
			    Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("createdBy", details.getApAdjCreatedBy());
				
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepAdjustmentRequestPrint.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/InvRepAdjustmentRequestPrint.jasper");
			    
		        }
				 			    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepAdjustmentRequestPrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					ex.printStackTrace();
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in InvRepAdjustmentRequestPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("invRepAdjustmentRequestPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("invRepAdjustmentRequestPrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in InvRepAdjustmentRequestPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
