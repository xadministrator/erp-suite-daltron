package com.struts.jreports.inv.adjustmentrequestprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepAdjustmentRequestPrintDetails;

public class InvRepAdjustmentRequestPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAdjustmentRequestPrintDS(ArrayList list) {
       
       Iterator i = list.iterator();
       
       while(i.hasNext()) {
           
           InvRepAdjustmentRequestPrintDetails details = (InvRepAdjustmentRequestPrintDetails)i.next();
           
           InvRepAdjustmentRequestPrintData invRepADJData = new InvRepAdjustmentRequestPrintData(
                   details.getApAdjType(), Common.convertSQLDateToString(details.getApAdjDate()), details.getApAdjDocumentNumber(), details.getApAdjReferenceNumber(),
                   details.getApAdjGlCoaAccount(), details.getApAdjGlCoaAccountDesc(), details.getApAdjDescription(),details.getApAdjApprovedBy(),details.getApAdjCreatedBy(),
                   details.getApAlIiName(), details.getApAlIiDescription(), details.getApAlUomName(), details.getApAlLocName(), 
                   new Double(details.getApAlUnitCost()),new Double(details.getApAlActualQuantity()), new Double(details.getApAlAdjustQuantity()), new Double(details.getApAlAveCost()),new Double(details.getApAlBranchCode()));
           
           data.add(invRepADJData);
           
       }   	
       
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      
      if("type".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getType();
      }else if("date".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getDate();
      }else if("documentNumber".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getDocumentNumber();
      }else if("referenceNumber".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getReferenceNumber();
      }else if("adjustmentAccount".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getAccountDescription();
      }else if("description".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getDescription();
      }else if("approvedBy".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getApprovedBy();  
      }else if("createdBy".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getCreatedBy();         
      }else if("itemName".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getItemName();       
      }else if("itemDescription".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getItemDescription();
      }else if("unitMeasureShortName".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getUnitMeasureShortName();
      }else if("location".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getLocationName();
      }else if("unitCost".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getUnitCost();
      }else if("actualQuantity".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getActualQuantity();    
      }else if("adjustQuantity".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getAdjustQuantity();
      }else if("aveCost".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getAveCost();
      }else if("branchCode".equals(fieldName)){
          value = ((InvRepAdjustmentRequestPrintData)data.get(index)).getBranchCode();
          System.out.print(value +"valueBranch");
      }
      
      
      return(value);
   }
}
