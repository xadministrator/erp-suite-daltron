package com.struts.jreports.inv.adjustmentrequestprint;

public class InvRepAdjustmentRequestPrintData implements java.io.Serializable {
    
    private String type = null;
    private String date = null;
    private String documentNumber = null;
    private String referenceNumber = null;
    private String accountNumber = null;
    private String accountDescription = null;
    private String description = null;
    
    
    private String approvedBy = null;
    private String createdBy = null;
    
    private String itemName = null;
    private String itemDescription = null;
    private String unitMeasureShortName = null;
    private String locationName = null;
    private Double unitCost = null;
    private Double actualQuantity = null;
    private Double adjustQuantity = null;
    private Double aveCost = null;
    private Double branchCode = null;
   
    
    public InvRepAdjustmentRequestPrintData(String type, String date, String documentNumber, String referenceNumber, 
            String accountNumber, String accountDescription, String description,String approvedBy,String createdBy,
            String itemName, String itemDescription, String unitMeasureShortName, String locationName,
            Double unitCost,Double actualQuantity ,Double adjustQuantity, Double aveCost,Double branchCode){
                
        this.type = type;
        this.date = date;
        this.documentNumber = documentNumber;
        this.referenceNumber = referenceNumber;
        this.accountNumber = accountNumber;
        this.accountDescription = accountDescription;
        this.description = description;
        this.approvedBy=approvedBy;
        this.createdBy=createdBy;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.unitMeasureShortName = unitMeasureShortName;
        this.locationName = locationName;
        this.unitCost = unitCost;
        this.actualQuantity=actualQuantity;
        this.adjustQuantity = adjustQuantity;
        this.aveCost = aveCost;
        this.branchCode = branchCode;
       
    }
    
    public String getType() {
        
        return type;
        
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public String getDocumentNumber() {
        
        return documentNumber;
        
    }
    
    public String getReferenceNumber() {
        
        return referenceNumber;
        
    }
    
    public String getAccountNumber() {
        
        return accountNumber;
        
    }
    
    public String getAccountDescription() {
        
        return accountDescription;
        
    }
    
    public String getDescription() {
        
        return description;
        
    }
    
    
  public String getApprovedBy() {
        
        return approvedBy;
        
    }
  public String getCreatedBy() {
      
      return createdBy;
      
  }
  
    
    public String getItemName() {
        
        return itemName;
        
    }
    
    public String getItemDescription() {
        
        return itemDescription;
        
    }
    
    public String getUnitMeasureShortName() {
        
        return unitMeasureShortName;
        
    }
    
    public String getLocationName() {
        
        return locationName;
        
    }

    public Double getUnitCost() {
        
        return unitCost;
        
    }
    
    public Double getActualQuantity() {
        
        return actualQuantity;
        
    }
    
    public Double getAdjustQuantity() {
        
        return adjustQuantity;
        
    }

    public Double getAveCost() {
        
        return aveCost;
        
    }
  public Double getBranchCode() {
        
        return branchCode;
        
    }


}
