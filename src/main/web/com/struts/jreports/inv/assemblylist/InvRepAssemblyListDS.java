package com.struts.jreports.inv.assemblylist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepAssemblyListDetails;

public class InvRepAssemblyListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAssemblyListDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         InvRepAssemblyListDetails details = (InvRepAssemblyListDetails)i.next();
         
	     InvRepAssemblyListData dtbData = new InvRepAssemblyListData(
	     		details.getAlIiName(), details.getAlIiDescription(), details.getAlIiUomName(),
				details.getAlComponentName(), details.getAlComponentDescription(),
				new Double(details.getAlQuantityNeeded()), 
				details.getAlComponentUomName(), details.getAlIiPartNumber(), new Double(details.getAlIiUnitCost()));
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("itemName".equals(fieldName)) {
      	
          value = ((InvRepAssemblyListData)data.get(index)).getItemName();
      
      } else if("itemDescription".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getItemDescription();
      
      } else if("unit".equals(fieldName)) {
            
                value = ((InvRepAssemblyListData)data.get(index)).getUnit();

      } else if("componentName".equals(fieldName)) {
      	
          value = ((InvRepAssemblyListData)data.get(index)).getComponentName();
      
      } else if("componentDescription".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getComponentDescription();

      } else if("quantityNeeded".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getQuantityNeeded();

      } else if("componentUnit".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getComponentUnit();
      
      } else if("partNumber".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getPartNumber();
      
      } else if("unitCost".equals(fieldName)) {
      
          value = ((InvRepAssemblyListData)data.get(index)).getUnitCost();
      
      }

      return(value);
   }
}
