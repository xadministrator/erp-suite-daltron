package com.struts.jreports.inv.assemblylist;

public class InvRepAssemblyListData implements java.io.Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String unit = null;
	private String componentName = null;
	private String componentDescription = null;
	private String componentUnit = null;
	private Double quantityNeeded = null;
	private String enable = null;
	private String partNumber = null;
	private Double unitCost = null;
	public InvRepAssemblyListData(String itemName, String itemDescription, String unit, String componentName, 
			String componentDescription, Double quantityNeeded, String componentUnit, String partNumber, Double unitCost) {
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.componentName = componentName;
		this.unit = unit;
		this.componentDescription = componentDescription;
		this.quantityNeeded = quantityNeeded;
		this.componentUnit = componentUnit;
		this.partNumber = partNumber;
		this.unitCost = unitCost;
		
	}
	
	public String getItemName() {
		
		return(itemName);
		
	}
	
	public String getItemDescription() {
		
		return(itemDescription);
		
	}
	
	public String getComponentName() {
		
		return(componentName);
		
	}
	
	public String getComponentDescription() {
		
		return(componentDescription);
		
	}
	
	public String getUnit() {
		
		return(unit);
		
	}
	
	public Double getQuantityNeeded() {
		
		return(quantityNeeded);
		
	}
	
	public String getComponentUnit() {
		
		return(componentUnit);
		
	}
	
	public String getPartNumber() {
		
		return(partNumber);
		
	}
	
	public Double getUnitCost() {
		
		return(unitCost);
		
	}
	
}
