package com.struts.jreports.inv.assemblytransferprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepAssemblyTransferPrintDetails;

public class InvRepAssemblyTransferPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepAssemblyTransferPrintDS(ArrayList invRepATPList) {

   	  Iterator i = invRepATPList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	    InvRepAssemblyTransferPrintDetails details = (InvRepAssemblyTransferPrintDetails)i.next();

   	    InvRepAssemblyTransferPrintData invRepATPData = new InvRepAssemblyTransferPrintData(
   	    		Common.convertSQLDateToString(details.getAtpAtrDate()), details.getAtpAtrDocumentNumber(), details.getAtpAtrReferenceNumber(),
				details.getAtpAtrDescription(),details.getAtpAtlBolIlIiName(),details.getAtpAtlBolIlIiDescription(), details.getAtpAtlBolIlLocName(), details.getAtpAtlBolIlIiUomName(),
				details.getAtpAtlBolBorDocumentNumber(), new Double(details.getAtpAtlBolQtyRequired()), new Double(details.getAtpAtlAssembleQuantity()), 
				new Double(details.getAtpAtlAssembleCost()));
   	    
   	    data.add(invRepATPData);
   	    }	   	   
   	
   	  }   	   	
                                                                     


   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("date".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getDate();       
      }else if("documentNumber".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getDocumentNumber();
      }else if("referenceNumber".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getReferenceNumber();
      }else if("description".equals(fieldName)){
        value = ((InvRepAssemblyTransferPrintData)data.get(index)).getDescription();
      }else if("itemName".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getItemName();
      }else if("itemDescription".equals(fieldName)){
        value = ((InvRepAssemblyTransferPrintData)data.get(index)).getItemDescription();
      }else if("itemLocation".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getItemLocation();
      }else if("unit".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getUnit();
      }else if("buildOrder".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getBuildOrder();
      }else if("quantityRequired".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getQuantityRequired();
      }else if("assembleQuantity".equals(fieldName)){
         value = ((InvRepAssemblyTransferPrintData)data.get(index)).getAssembleQuantity();
      }else if("assembleCost".equals(fieldName)){
          value = ((InvRepAssemblyTransferPrintData)data.get(index)).getAssembleCost();
         
      }
      
      return(value);
   }
}
