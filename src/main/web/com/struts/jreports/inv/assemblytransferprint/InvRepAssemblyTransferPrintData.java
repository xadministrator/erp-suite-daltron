package com.struts.jreports.inv.assemblytransferprint;

public class InvRepAssemblyTransferPrintData implements java.io.Serializable {
    
    private String date = null;
    private String documentNumber = null;
    private String referenceNumber = null;
    private String description = null;
    private String itemName = null;
    private String itemDescription = null;
    private String itemLocation = null;
    private String unit = null;
    private String buildOrder = null;
    private Double quantityRequired = null;
    private Double assembleQuantity = null;
    private Double assembleCost = null;
    
    public InvRepAssemblyTransferPrintData(String date, 
            String documentNumber, 
            String referenceNumber,
            String description,
            String itemName,
            String itemDescription,
            String itemLocation,
            String unitOfMeasure,
            String buildOrder,
            Double quantityRequired,
            Double assembleQuantity,
            Double assembleCost) {
        
        this.date = date; 
        this.documentNumber = documentNumber; 
        this.referenceNumber = referenceNumber;
        this.description = description;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemLocation = itemLocation;
        this.unit = unitOfMeasure;
        this.buildOrder = buildOrder;
        this.quantityRequired = quantityRequired;
        this.assembleQuantity = assembleQuantity;
        this.assembleCost = assembleCost;
    }
    
    public Double getAssembleQuantity() {
        
        return assembleQuantity;
        
    }
    
    public void setAssembleQuantity(Double assembleQuantity) {
        
        this.assembleQuantity = assembleQuantity;
        
    }
    
    public String getBuildOrder() {
        
        return buildOrder;
        
    }
    
    public void setBuildOrder(String buildOrder) {
        
        this.buildOrder = buildOrder;
        
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public void setDate(String date) {
        
        this.date = date;
        
    }
    
    public String getDescription() {
        
        return description;
        
    }
    
    public void setDescription(String description) {
        
        this.description = description;
        
    }
    
    public String getDocumentNumber() {
        
        return documentNumber;
        
    }
    
    public void setDocumentNumber(String documentNumber) {
        
        this.documentNumber = documentNumber;
        
    }
    
    public String getItemLocation() {
        
        return itemLocation;
        
    }
    
    public void setItemLocation(String itemLocation) {
        
        this.itemLocation = itemLocation;
        
    }
    
    public String getItemName() {
        
        return itemName;
        
    }
    
    public void setItemName(String itemName) {
        
        this.itemName = itemName;
        
    }
    
    public String getItemDescription() {
        
        return itemDescription;
        
    }
    
    public void setItemDescription(String itemDescription) {
        
        this.itemDescription = itemDescription;
        
    }
    
    public Double getQuantityRequired() {
        
        return quantityRequired;
        
    }
    
    public void setQuantityRequired(Double quantityRequired) {
        
        this.quantityRequired = quantityRequired;
        
    }
    
    public String getReferenceNumber() {
        
        return referenceNumber;
        
    }
    
    public void setReferenceNumber(String referenceNumber) {
        
        this.referenceNumber = referenceNumber;
        
    }
    
    public String getUnit() {
        
        return unit;
        
    }
    
    public void setUnit(String unit) {
        
        this.unit = unit;
        
    }
    
    public Double getAssembleCost() {
        
        return assembleCost;
        
    }
    
    public void setAssembleCost(Double assembleCost) {
        
        this.assembleCost = assembleCost;
        
    }
    
    
}
