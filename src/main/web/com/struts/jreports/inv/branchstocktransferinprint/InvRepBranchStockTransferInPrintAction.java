package com.struts.jreports.inv.branchstocktransferinprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepBranchStockTransferInPrintController;
import com.ejb.txn.InvRepBranchStockTransferInPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepBranchStockTransferInPrintDetails;

public class InvRepBranchStockTransferInPrintAction extends Action {
    
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
       HttpServletRequest request, HttpServletResponse response)    
       throws Exception {

       HttpSession session = request.getSession();
           
       try {	

 /*******************************************************
    Check if user has a session
 *******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          
          if (user != null) {
          	
             if (log.isInfoEnabled()) {
             	
                 log.info("InvRepBranchStockTransferInPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                 "' performed this action on session " + session.getId());
                 
             }
             
          } else {
          	
              if (log.isInfoEnabled()) {
              	
                 log.info("User is not logged on in session" + session.getId());
                
             }
             
             return(mapping.findForward("adLogon"));
             
          }
          
          InvRepBranchStockTransferInPrintForm actionForm = (InvRepBranchStockTransferInPrintForm)form; 
          
          String frParam= Common.getUserPermission(user, Constants.INV_REP_BRANCH_STOCK_TRANSFER_IN_PRINT_ID);
          
          if (frParam != null) {
             
              actionForm.setUserPermission(frParam.trim());
             
          } else {
          	
              actionForm.setUserPermission(Constants.NO_ACCESS);
          }

 /*******************************************************
    Initialize InvRepBranchStockTransferInPrintController EJB
 *******************************************************/

          InvRepBranchStockTransferInPrintControllerHome homeBTP = null;
          InvRepBranchStockTransferInPrintController ejbBTP = null;       
          
          try {
          	
          	homeBTP = (InvRepBranchStockTransferInPrintControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/InvRepBranchStockTransferInPrintControllerEJB", InvRepBranchStockTransferInPrintControllerHome.class);
          	
          } catch(NamingException e) {
          	
          	if(log.isInfoEnabled()) {
          		
          		log.info("NamingException caught in InvRepBranchStockTransferInPrintAction.execute(): " + e.getMessage() +
          				" session: " + session.getId());
          		
          	}
          	
          	return(mapping.findForward("cmnErrorPage"));
          	
          }
          
          try {
          	
          	ejbBTP = homeBTP.create();
          	
          } catch(CreateException e) {
          	
          	if(log.isInfoEnabled()) {
          		
          		log.info("CreateException caught in InvRepBranchStockTransferInPrintAction.execute(): " + e.getMessage() +
          				" session: " + session.getId());
          		
          	}
          	
          	return(mapping.findForward("cmnErrorPage"));
          	
          }
          
          ActionErrors errors = new ActionErrors();
          
          /*** get report session and if not null set it to null **/
          
          Report reportSession = 
          	(Report)session.getAttribute(Constants.REPORT_KEY);
          
          if(reportSession != null) {
          	
          	reportSession.setBytes(null);
          	session.setAttribute(Constants.REPORT_KEY, reportSession);
          	
          }          

 /*******************************************************
    -- INV Rep Branch Stock Transfer-In Print  --
 *******************************************************/       

 		if (frParam != null) {
 	        
 			    AdCompanyDetails adCmpDetails = null;
 			    InvRepBranchStockTransferInPrintDetails details = null; 
 			    
 			    ArrayList list = null;   
 			    
 				try {
 					
 					ArrayList bstCodeList = new ArrayList();
 					
 					if (request.getParameter("forward") != null &&
 	 						request.getParameter("branchStockTransferCode") != null) {
 												
 	            		bstCodeList.add(new Integer(request.getParameter("branchStockTransferCode")));
 						
 					}
 	            	
 	            	list = ejbBTP.executeInvRepBranchStockTransferInPrint(bstCodeList, user.getCmpCode());
 	            	
 			        // get company
 			       
 			        adCmpDetails = ejbBTP.getAdCompany(user.getCmpCode());	
 			        
 			        // get parameters
 	      			
 	      			Iterator i = list.iterator();
 	      			int line = 0;
 	      			while (i.hasNext()) {
 	      				
 	      				details = (InvRepBranchStockTransferInPrintDetails)i.next();
 	      																												
 	      			}
 	            			 	      			 	      			
 	            } catch(GlobalNoRecordFoundException ex) {
 	            	
 	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                    new ActionMessage("branchStockTransferInPrint.error.branchStockTransferInAlreadyDeleted"));
 	            		
 	            } catch(EJBException ex) {
 		     	
 	               if (log.isInfoEnabled()) {
 	               	
 	                  log.info("EJBException caught in InvBranchStockTransferInPrintAction.execute(): " + ex.getMessage() +
 	                           " session: " + session.getId());
 	               }
 	               
 	               return(mapping.findForward("cmnErrorPage"));
 	               
 	           } 
 	            
 	            if (!errors.isEmpty()) {

 	                saveErrors(request, new ActionMessages(errors));
 	                return mapping.findForward("InvRepBranchStockTransferInPrint");

 	            }
 				
 				// fill report parameters, fill report to pdf and set report session  	    	               
 			     	             	             	            
 			    Map parameters = new HashMap();
 				parameters.put("company", adCmpDetails.getCmpName());
 				parameters.put("branch", user.getCurrentBranch().getBrName());
 				parameters.put("createdBy", details.getBstpBstCreatedBy());
 				parameters.put("approvedBy", details.getBstpBstApprovedRejectedBy());
 				
 				String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBranchStockTransferInPrint.jasper";
 			       
 		        if (!new java.io.File(filename).exists()) {
 		       		    		    
 		           filename = servlet.getServletContext().getRealPath("jreports/InvRepBranchStockTransferInPrint.jasper");
 			    
 		        }
 				 						    	    	    	        	    				
 				try {
 					   
 				    Report report = new Report();
 				    
 				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
 				    report.setBytes(
 				            JasperRunManager.runReportToPdf(filename, parameters, 
 				            new InvRepBranchStockTransferInPrintDS(list)));  

 				    session.setAttribute(Constants.REPORT_KEY, report);
 				    
 				} catch(Exception ex) {
 					
 				    if(log.isInfoEnabled()) {
 				   	
 				      log.info("Exception caught in InvRepBranchStockTransferInPrintAction.execute(): " + ex.getMessage() +
 				               " session: " + session.getId());
 					      
 				   } 
 				   
 				    return(mapping.findForward("cmnErrorPage"));
 					
 				}              
 				
 				return(mapping.findForward("invRepBranchStockTransferInPrint"));
 	   
 		  } else {
 		 	
 		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
 		    saveErrors(request, new ActionMessages(errors));
 		
 		    return(mapping.findForward("invRepBranchStockTransferInPrintForm"));
 		
 		 }
 	 
      } catch(Exception e) {
      	   	
          	 
 /*******************************************************
    System Failed: Forward to error page 
 *******************************************************/
           if(log.isInfoEnabled()) {
           	
              log.info("Exception caught in InvRepBranchStockTransferInPrintAction.execute(): " + e.getMessage()
                 + " session: " + session.getId());
                 
           }        
           return(mapping.findForward("cmnErrorPage"));        
        }
     }


}
