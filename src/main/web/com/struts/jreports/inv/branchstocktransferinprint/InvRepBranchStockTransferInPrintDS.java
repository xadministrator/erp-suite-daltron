package com.struts.jreports.inv.branchstocktransferinprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.inv.branchstocktransferoutprint.InvRepBranchStockTransferOutPrintData;
import com.struts.util.Common;
import com.util.InvRepBranchStockTransferInPrintDetails;

public class InvRepBranchStockTransferInPrintDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepBranchStockTransferInPrintDS(ArrayList invRepBSTPList) {
		
		Iterator i = invRepBSTPList.iterator();
		
		while(i.hasNext()) {
			
			InvRepBranchStockTransferInPrintDetails details = (InvRepBranchStockTransferInPrintDetails)i.next();
			
			InvRepBranchStockTransferInPrintData invRepSTPData = new InvRepBranchStockTransferInPrintData(
					Common.convertSQLDateToString(details.getBstpBstDate()),
					details.getBstpBstNumber(),
					details.getBstpBstDescription(),
					details.getBstpBstTransferOutNumber(),
					details.getBstpBstTransitLocation(),
					details.getBstpBstBranch(),
					details.getBstpBstCreatedBy(),
					details.getBstpBstApprovedRejectedBy(),
					new Double(details.getBstpBslQuantity()),
					new Double(details.getBstpBslQuantityReceived()),
					details.getBstpBslUnitOfMeasure(),
					details.getBstpBslItemName(),
					details.getBstpBslItemDescription(),
					details.getBstpBslItemLocation(),
					new Double(details.getBstpBslUnitPrice()),
					new Double(details.getBstpBslAmount()),
					new Double(details.getBstpBslSlsPrc()),
					details.getBstpBslMscItm());
			
			data.add(invRepSTPData);
			
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		return (index < data.size());
		
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		
		Object value = null;
		String rem = null;
		int ctr1 = 0;
		String fieldName = field.getName();
		
		if("date".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getDate();       
		}else if("number".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getNumber();
		}else if("description".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getDescription();
		}else if("transferOutNumber".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getTransferOutNumber();
		}else if("createdBy".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getCreatedBy();
		}else if("transitLocation".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getTransitLocation();
		}else if("approvedBy".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getApprovedRejectedBy();
		}else if("branch".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getBranch();
		}else if("quantity".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getQuantity();
		}else if("quantityReceived".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getQuantityReceived();
		}else if("unit".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getUnitOfMeasure();
		}else if("itemName".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getItemName();
		}else if("itemDescription".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getItemDescription();
		}else if("itemLocation".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getItemLocation();
		}else if("unitPrice".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getUnitPrice();
		}else if("amount".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getAmount();
		}else if("salesPrice".equals(fieldName)){
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getSalesPrice();
		}else if("miscItem".equals(fieldName)){
			 rem = ((InvRepBranchStockTransferInPrintData)data.get(index)).getMiscItem();
			if(rem.equals("null") || rem.equals(" ")){					
			value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getMiscItem();
			}
			else{
				int last1 = rem.length();
				ctr1 = rem.indexOf('$',1);
				if(ctr1 == -1){
				value = ((InvRepBranchStockTransferInPrintData)data.get(index)).getMiscItem();
				}else{
				
				value = rem.substring(ctr1+1, last1-1).replace('$','/');
				}
				}
		}
			return(value);
			}
		
	
}
