package com.struts.jreports.inv.branchstocktransferinprint;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class InvRepBranchStockTransferInPrintForm extends ActionForm implements java.io.Serializable {
    
    private String userPermission = new String();
    private Integer branchStockTransferCode = null;

    public String getUserPermission() {

       return userPermission;

    } 

    public void setUserPermission(String userPermission) {

       this.userPermission = userPermission;

    }   

    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	  
    }       

}
