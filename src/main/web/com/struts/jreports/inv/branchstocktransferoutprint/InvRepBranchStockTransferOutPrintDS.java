package com.struts.jreports.inv.branchstocktransferoutprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepBranchStockTransferOutPrintDetails;

public class InvRepBranchStockTransferOutPrintDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepBranchStockTransferOutPrintDS(ArrayList invRepBSTPList) {
		
		Iterator i = invRepBSTPList.iterator();
		
		while(i.hasNext()) {
			
			InvRepBranchStockTransferOutPrintDetails details = (InvRepBranchStockTransferOutPrintDetails)i.next();
			
			InvRepBranchStockTransferOutPrintData invRepSTPData = new InvRepBranchStockTransferOutPrintData(
					Common.convertSQLDateToString(details.getBstpBstDate()),
					details.getBstpBstNumber(),
					details.getBstpBstDescription(),
					details.getBstpBstTransitLocation(),
					details.getBstpBstBranch(),
					details.getBstpBstCreatedBy(),
					details.getBstpBstApprovedRejectedBy(),
					new Double(details.getBstpBslQuantity()),
					new Double(details.getBstpBslOrderedQuantity()),
					details.getBstpBslUnitOfMeasure(),
					details.getBstpBslItemName(),
					details.getBstpBslItemDescription(),
					new Double(details.getBstpBslUnitPrice()),
					new Double(details.getBstpBslAmount()),
					new Double(details.getBstpBslSlsPrc()),
					details.getBstpBslMscItm());
			
			data.add(invRepSTPData);
			
		}
		
	}
	
	public boolean next() throws JRException {
		
		index++;
		return (index < data.size());
		
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		
		Object value = null;
		String rem = null;
		int ctr1 = 0;
		
		String fieldName = field.getName();
		
		if("date".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getDate();       
		}else if("number".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getNumber();
		}else if("description".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getDescription();
		}else if("createdBy".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getCreatedBy();
		}else if("approvedBy".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getApprovedRejectedBy();
		}else if("transitLocation".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getTransitLocation();
		}else if("branch".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getBranch();
		}else if("quantity".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getQuantity();
		}else if("orderedQuantity".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getOrderedQuantity();
		}else if("unit".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getUnitOfMeasure();
		}else if("itemName".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getItemName();
		}else if("itemDescription".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getItemDescription();
		}else if("unitPrice".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getUnitPrice();
		}else if("amount".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getAmount();
		}else if("salesPrice".equals(fieldName)){
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getSalesPrice();
		}else if("miscItem".equals(fieldName)){
			rem = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getMiscItem();
			if(rem.equals("null") || rem.equals(" ")){					
			value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getMiscItem();
			}
			else{
				int last1 = rem.length();
				ctr1 = rem.indexOf('$',1);
				if(ctr1 == -1){
				value = ((InvRepBranchStockTransferOutPrintData)data.get(index)).getMiscItem();
				}else{
				
				value = rem.substring(ctr1+1, last1-1).replace('$','/');
				}
				}
			}
		
		return(value);
	}
	
}
