package com.struts.jreports.inv.branchstocktransferoutprint;

public class InvRepBranchStockTransferOutPrintData implements java.io.Serializable {
    
    private String date = null;
    private String number = null;
    private String description = null;
    private String transitLocation = null;
    private String branch = null;
    private String createdBy = null;
    private String approvedRejectedBy = null;
    private Double quantity = null;
    private Double orderedQuantity = null;
    private Double amount = null;
    private String unitOfMeasure = null;
    private String itemName = null;
    private String itemDescription = null;
    private Double unitPrice = null;
    private Double salesPrice = null;
    private String miscItem = null;

    
    public InvRepBranchStockTransferOutPrintData(
            String date,
            String number,
			String description,
            String transitLocation,
			String branch,
            String createdBy,
            String approvedRejectedBy,
			Double quantity,
			Double orderedQuantity,
            String unitOfMeasure,
            String itemName,
            String itemDescription,
            Double unitPrice,
            Double amount,
            Double salesPrice,
            String miscItem){
        
        this.date = date;
        this.number = number;
        this.description = description;
        this.transitLocation = transitLocation;
        this.branch = branch;
        this.createdBy = createdBy;
        this.approvedRejectedBy = approvedRejectedBy;
        this.quantity = quantity;
        this.orderedQuantity = orderedQuantity;
        this.unitOfMeasure = unitOfMeasure;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.unitPrice = unitPrice;
        this.amount = amount;
        this.salesPrice = salesPrice;
        this.miscItem = miscItem;
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public void setDate(String date) {
        
        this.date = date;
                
    }
    
    public String getNumber() {
        
        return number;
        
    }
    
    public void setNumber(String number) {
        
        this.number = number;
        
    }
    
    public String getDescription() {
        
        return description;
        
    }
    
    public void setDescription(String description) {
        
        this.description = description;
        
    }
    
    public String getCreatedBy() {
        
        return createdBy;
        
    }
    
    public void setCreatedBy(String createdBy) {
        
        this.createdBy = createdBy;
        
    }
    
    public String getApprovedRejectedBy() {
        
        return approvedRejectedBy;
        
    }
    
    public void setApprovedRejectedBy(String approvedRejectedBy) {
        
        this.approvedRejectedBy = approvedRejectedBy;
        
    }
    
    public String getTransitLocation() {
        
        return transitLocation;
        
    }
    
    public void setTransitLocation(String transitLocation) {
        
        this.transitLocation = transitLocation;
        
    }
    
    public String getBranch() {
        
        return branch;
        
    }
    
    public void setBranch(String branch) {
        
        this.branch = branch;
        
    }
    
    public Double getQuantity() {
        
        return quantity;
        
    }
    
    public void setQuantity(Double quantity) {
        
        this.quantity = quantity;
        
    }
    
    public Double getOrderedQuantity() {
        
        return orderedQuantity;
        
    }
    
    public void setOrderedQuantity(Double orderedQuantity) {
        
        this.orderedQuantity = orderedQuantity;
        
    }
    
    public String getUnitOfMeasure() {
        
        return unitOfMeasure;
        
    }
    
    public void setUnitOfMeasure(String unitOfMeasure) {
        
        this.unitOfMeasure = unitOfMeasure;
        
    }
    
    public String getItemName() {
        
        return itemName;
        
    }
    
    public void setItemName(String itemName) {
        
        this.itemName = itemName;
        
    }
    
    public String getItemDescription() {
        
        return itemDescription;
        
    }
    
    public void setItemDescription(String itemDescription) {
        
        this.itemName = itemDescription;
        
    }
    
    public Double getUnitPrice() {
        
        return unitPrice;
        
    }
    
    public void setUnitPrice(Double unitPrice) {
        
        this.unitPrice = unitPrice;
        
    }

    public Double getAmount() {
        
        return amount;
        
    }
    
    public void setAmount(Double amount) {

    	this.amount = amount;

    }

    public Double getSalesPrice() {

    	return salesPrice;

    }

    public void setSalesPrice(Double salesPrice) {

    	this.salesPrice = salesPrice;

    }
    
public String getMiscItem() {
        
        return miscItem;
        
    }

public void setMiscItem(String miscItem) {
    
    this.miscItem = miscItem;
    
}
    
}
