package com.struts.jreports.inv.branchstocktransferregister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepBranchStockTransferRegisterController;
import com.ejb.txn.InvRepBranchStockTransferRegisterControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepBranchStockTransferRegisterAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {

		HttpSession session = request.getSession();

		try {

/*******************************************************
 Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("InvRepBranchStockTransferRegisterAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());

				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			InvRepBranchStockTransferRegisterForm actionForm = (InvRepBranchStockTransferRegisterForm)form;

			// reset report to null
			actionForm.setReport(null);

			String frParam = Common.getUserPermission(user, Constants.INV_REP_BRANCH_STOCK_TRANSFER_REGISTER_ID);

			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));

						return mapping.findForward("invRepBranchStockTransferRegister");
					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}
			
/***********************************************************
 Initialize InvRepBranchStockTransferRegisterController EJB
***********************************************************/

			InvRepBranchStockTransferRegisterControllerHome homeBSTR = null;
			InvRepBranchStockTransferRegisterController ejbBSTR = null;       

			try {

				homeBSTR = (InvRepBranchStockTransferRegisterControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvRepBranchStockTransferRegisterControllerEJB", InvRepBranchStockTransferRegisterControllerHome.class);

			} catch(NamingException e) {

				if(log.isInfoEnabled()) {

					log.info("NamingException caught in InvRepBranchStockTransferRegisterAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			try {

				ejbBSTR = homeBSTR.create();

			} catch(CreateException e) {

				if(log.isInfoEnabled()) {

					log.info("CreateException caught in InvRepBranchStockTransferRegisterAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

			}

			ActionErrors errors = new ActionErrors();

			/*** get report session and if not null set it to null **/

			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);

			if(reportSession != null) {

				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);

			}

/*******************************************************
 -- INV BSTR Go Action --
*******************************************************/

			if(request.getParameter("goButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArrayList list = null;

				String company = null;
				String branchName = null;

				// create criteria 

				if (request.getParameter("goButton") != null) {

					HashMap criteria = new HashMap();            	

					if (!Common.validateRequired(actionForm.getDateFrom())) {

						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

					}
					
					if (!Common.validateRequired(actionForm.getCategory())) {
		        		
		        		criteria.put("itemCategory", (actionForm.getCategory()));
		        		
		        	}
					
					if (!Common.validateRequired(actionForm.getDateTo())) {

						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

					}
					
					if (!Common.validateRequired(actionForm.getTransferOutNumberFrom())) {

						criteria.put("transferOutNumberFrom", actionForm.getTransferOutNumberFrom());

					}
					
					if (!Common.validateRequired(actionForm.getTransferOutNumberTo())) {

						criteria.put("transferOutNumberTo", actionForm.getTransferOutNumberTo());

					}
					
					if (!Common.validateRequired(actionForm.getTransferInNumberFrom())) {

						criteria.put("transferInNumberFrom", actionForm.getTransferInNumberFrom());

					}
					
					if (!Common.validateRequired(actionForm.getTransferInNumberTo())) {

						criteria.put("transferInNumberTo", actionForm.getTransferInNumberTo());

					}
					if (!Common.validateRequired(actionForm.getTransferOrderNumberFrom())) {

						criteria.put("transferOrderNumberFrom", actionForm.getTransferOrderNumberFrom());

					}
					
					if (!Common.validateRequired(actionForm.getTransferOrderNumberTo())) {

						criteria.put("transferOrderNumberTo", actionForm.getTransferOrderNumberTo());

					}
			//		
					if (actionForm.getIncludeTransferOut()){

						criteria.put("includeTransferOut", "YES");

					} else {

						criteria.put("includeTransferOut", "NO");

					}

					if (!Common.validateRequired(actionForm.getBranchTo())) {

						criteria.put("branchTo", actionForm.getBranchTo());

					}
				//	
					if (actionForm.getIncludeTransferIn()){

						criteria.put("includeTransferIn", "YES");

					} else {

						criteria.put("includeTransferIn", "NO");

					}
					
					if (!Common.validateRequired(actionForm.getBranchFrom())) {

						criteria.put("branchFrom", actionForm.getBranchFrom());

					}	
					if (actionForm.getIncludeTransferOrder()){

						criteria.put("includeTransferOrder", "YES");

					} else {

						criteria.put("includeTransferOrder", "NO");

					}

					if (!Common.validateRequired(actionForm.getReferenceNumber())) {

						criteria.put("referenceNumber", actionForm.getReferenceNumber());

					}
					
					if (actionForm.getIncludeIncoming()){

						criteria.put("includeIncoming", "YES");

					} else {

						criteria.put("includeIncoming", "NO");

					}
					
					if (actionForm.getIncludeUnposted()){

						criteria.put("includeUnposted", "YES");

					} else {

						criteria.put("includeUnposted", "NO");

					}

					// save criteria

					actionForm.setCriteria(criteria);

				}
				
				try {

					// get company

					AdCompanyDetails adCmpDetails = ejbBSTR.getAdCompany(user.getCmpCode());
					company = adCmpDetails.getCmpName();
					
					// get selected branch

					Integer branchCode = new Integer(0);
					
					ArrayList branchList = new ArrayList();
					for(int i=0; i<actionForm.getInvRepBrBstrListSize(); i++) {
                       
						InvRepBranchStockTransferRegisterBranchList brList = (InvRepBranchStockTransferRegisterBranchList)actionForm.getInvRepBrBstrListByIndex(i);
						
						if(brList.getBranchCheckbox() == true ) {
							branchCode = brList.getBranchCode();
							branchName = brList.getBranchName();
							//parameters.put("branch", branchName);
							break;
						}

					}

					// execute report

					list = ejbBSTR.executeInvRepBranchStockTransferRegister(actionForm.getCriteria(), actionForm.getOrderBy(), branchCode, user.getCmpCode());

				} catch (GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("branchStockTransferRegister.error.noRecordFound"));

				} catch(EJBException ex) {

					if(log.isInfoEnabled()) {
						log.info("EJBException caught in InvRepBranchStockTransferRegisterAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invRepBranchStockTransferRegister");

				}


				// fill report parameters, fill report to pdf and set report session

				Map parameters = new HashMap();
				parameters.put("company", company);
				parameters.put("branch", branchName);
				parameters.put("dateToday", new java.util.Date());
				parameters.put("printedBy", user.getUserName());
				parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));

				if (actionForm.getDateFrom() != null) {

					parameters.put("dateFrom", actionForm.getDateFrom());
					
				}

				if (actionForm.getDateTo() != null) {

					parameters.put("dateTo", actionForm.getDateTo());
					
				}	
				
				if (actionForm.getCategory() != null) {
	        		
	        		parameters.put("itemCategory", (actionForm.getCategory()));
	        		
	        	}
				
				if (actionForm.getTransferOutNumberFrom() != null) {

					parameters.put("transferOutNumberFrom", actionForm.getTransferOutNumberFrom());
					
				}	
				
				if (actionForm.getTransferOutNumberTo() != null) {

					parameters.put("transferOutNumberTo", actionForm.getTransferOutNumberTo());
					
				}
				
				if (actionForm.getTransferInNumberFrom() != null) {

					parameters.put("transferInNumberFrom", actionForm.getTransferInNumberFrom());
					
				}	
				
				if (actionForm.getTransferInNumberTo() != null) {

					parameters.put("transferInNumberTo", actionForm.getTransferInNumberTo());
					
				}
				if (actionForm.getTransferOrderNumberFrom() != null) {

					parameters.put("transferOrderNumberFrom", actionForm.getTransferOrderNumberFrom());
					
				}	
				
				if (actionForm.getTransferOrderNumberTo() != null) {

					parameters.put("transferOrderNumberTo", actionForm.getTransferOrderNumberTo());
					
				}
				
				if (actionForm.getIncludeTransferOut()){

					parameters.put("includeTransferOut", "YES");

				} else {

					parameters.put("includeTransferOut", "NO");

				}
				
				
				if (actionForm.getBranchTo() != null) {

					parameters.put("branchTo", actionForm.getBranchTo());
				}
				
				if (actionForm.getIncludeTransferIn()){

					parameters.put("includeTransferIn", "YES");

				} else {

					parameters.put("includeTransferIn", "NO");

				}
				
				if (actionForm.getBranchFrom() != null) {

					parameters.put("branchFrom", actionForm.getBranchFrom());
				}
				
				if (actionForm.getIncludeTransferOrder()){

					parameters.put("includeTransferOrder", "YES");

				} else {

					parameters.put("includeTransferOrder", "NO");

				}
				
				if (actionForm.getReferenceNumber() != null) {

					parameters.put("referenceNumber", actionForm.getReferenceNumber());
				}
				
				if (actionForm.getIncludeIncoming()){

					parameters.put("includeIncoming", "YES");

				} else {

					parameters.put("includeIncoming", "NO");

				}
				
				if (actionForm.getIncludeUnposted()){

					parameters.put("includeUnposted", "YES");

				} else {

					parameters.put("includeUnposted", "NO");

				}

			    parameters.put("orderBy", actionForm.getOrderBy());
			    
			    String branchMap = null;
	      		boolean first = true;
	      		for(int j=0; j < actionForm.getInvRepBrBstrListSize(); j++) {

	      			InvRepBranchStockTransferRegisterBranchList brList = (InvRepBranchStockTransferRegisterBranchList)actionForm.getInvRepBrBstrListByIndex(j);
	      		   
	      			if(brList.getBranchCheckbox() == true)
	      			
	      			{
	      				if(first) {
	      					branchMap = brList.getBranchName();
	      					first = false;
	      				} else {
	      					branchMap = branchMap + ", " + brList.getBranchName();
	      				}
	      			}
	      		
	      		}
	      		
	      		parameters.put("branchMap", branchMap);
	      		String filename = "";
	      		
	      		
	      		
	      		if (actionForm.getIncludeTransferOrder() == true && actionForm.getShowOrderList() == true){
	      			filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBranchTransferOrderRegister.jasper";
	      		}else {
	      			filename = "";
	      		}
				//filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBranchTransferRegister.jasper";

				if (!new java.io.File(filename).exists()) {
					//if (actionForm.getIncludeTransferOrder() == true){
						//filename = servlet.getServletContext().getRealPath("jreports/InvRepBranchTransferOrderRegister.jasper");
		      		//}else {
		      			filename = servlet.getServletContext().getRealPath("jreports/InvRepBranchTransferRegister.jasper");
		      		//} 
					

				}	    	

				try {

					Report report = new Report();

					if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(
								JasperRunManager.runReportToPdf(filename, parameters, 
										new InvRepBranchStockTransferRegisterDS(list)));   

					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

						report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
						report.setBytes(
								JasperRunManagerExt.runReportToXls(filename, parameters, 
										new InvRepBranchStockTransferRegisterDS(list)));   

					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

						report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
						report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
								new InvRepBranchStockTransferRegisterDS(list)));												    

					}

					session.setAttribute(Constants.REPORT_KEY, report);
					actionForm.setReport(Constants.STATUS_SUCCESS);		    	

				} catch(Exception ex) {

					if(log.isInfoEnabled()) {
						log.info("Exception caught in InvRepBranchStockTransferRegisterAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}		    	    

/*******************************************************
 -- INV BSTR Close Action --
*******************************************************/

			} else if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

/*******************************************************
 -- INV BSTR Load Action --
*******************************************************/

			}

			if(frParam != null) {


				try {

					actionForm.reset(mapping, request);

					ArrayList list = null;			       			       
					Iterator i = null;

					actionForm.clearBranchFromList();

					list = ejbBSTR.getAdBrAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setBranchFromList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setBranchFromList((String)i.next());

						}

					}
					
					actionForm.clearCategoryList();           	
	         		
	         		list = ejbBSTR.getAdLvInvItemCategoryAll(user.getCmpCode());
	         		
	         		if (list == null || list.size() == 0) {
	         			
	         			actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	         			
	         		} else {
	         			
	         			i = list.iterator();
	         			
	         			while (i.hasNext()) {
	         				
	         				actionForm.setCategoryList((String)i.next());
	         				
	         			}
	         			
	         		}
	         		
					actionForm.clearBranchToList();

					list = ejbBSTR.getAdBrAll(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setBranchToList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setBranchToList((String)i.next());

						}

					}
					
					actionForm.clearInvRepBrBstrList();
					
					//int ctr = 1;

					list = ejbBSTR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 

					i = list.iterator();

					while(i.hasNext()) {

						AdBranchDetails details = (AdBranchDetails)i.next();

						InvRepBranchStockTransferRegisterBranchList invBrBstrList = new InvRepBranchStockTransferRegisterBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						
						if (details.getBrHeadQuarter() == 1) invBrBstrList.setBranchCheckbox(true);
						//ctr++;
						
						actionForm.saveInvRepBrBstrList(invBrBstrList);

					}						

				} catch(EJBException ex) {

					if(log.isInfoEnabled()) {
						log.info("EJBException caught in InvRepBranchStockTransferRegisterAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}

				return(mapping.findForward("invRepBranchStockTransferRegister"));	

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

		} catch(Exception e) { 


/*******************************************************
 System Failed: Forward to error page 
*******************************************************/

			if(log.isInfoEnabled()) {

				log.info("Exception caught in InvRepBranchStockTransferRegisterAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());

			}   

			return(mapping.findForward("cmnErrorPage"));   

		}
	}
}
