package com.struts.jreports.inv.branchstocktransferregister;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepBranchStockTransferRegisterDetails;

public class InvRepBranchStockTransferRegisterDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepBranchStockTransferRegisterDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         InvRepBranchStockTransferRegisterDetails details = (InvRepBranchStockTransferRegisterDetails)i.next();
	         
	         InvRepBranchStockTransferRegisterData argData = new InvRepBranchStockTransferRegisterData(
	        		details.getBstrBstType(), details.getBstrBstStatus(), details.getBstrBstDocumentNumber(), details.getBstrBstDate(),
	        		details.getBstrBstTransferOutNumber(), details.getBstrBstBranch(), details.getBstrBstTransitLocation(), details.getBstrBstCreatedBy(),
	        		details.getBstrBstlItemName(), details.getBstrBstlItemDescription(), details.getBstrBstlLocation(), details.getBstrBstlUnit(),
	        		new Double(details.getBstrBstlQuantity()), new Double(details.getBstrBstlUnitCost()), new Double(details.getBstrBstlAmount()),
	        		new Double(details.getBstrBstlQuantityTransfered()));
	         
	         data.add(argData);
	         
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("type".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getType();
   	} else if("status".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getStatus();
   	} else if("documentNumber".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getDocumentNumber();
   	} else if("date".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getDate();
   	} else if("referenceNumber".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getReferenceNumber();
   	} else if("branch".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getBranch();
   	} else if("transitLocation".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getTransitLocation();
   	} else if("createdBy".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getCreatedBy();
   	} else if("itemName".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getItemName();
   	} else if("itemDescription".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getItemDescription();
   	} else if("location".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getLocation();
   	} else if("unit".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getUnit();
   	} else if("quantity".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getQuantity();
   	} else if("unitCost".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getUnitCost();
   	} else if("amount".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getAmount();
   	} else if("quantityTransfered".equals(fieldName)) {
   		value = ((InvRepBranchStockTransferRegisterData)data.get(index)).getQuantityTransfered();
   	}
   		
   	return(value);
   	
   }
   
}
