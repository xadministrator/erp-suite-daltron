package com.struts.jreports.inv.branchstocktransferregister;

import java.util.Date;

public class InvRepBranchStockTransferRegisterData implements java.io.Serializable {

	private String type;
	private String status;
	private String documentNumber;
	private Date date;
	private String referenceNumber;
	private String branch;
	private String transitLocation;
	private String createdBy;
	
	private String itemName;
	private String itemDescription;
	private String location;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;
	
	private Double quantityTransfered;

	public InvRepBranchStockTransferRegisterData(String type, String status, String documentNumber,	Date date, 
			String referenceNumber, String branch, String transitLocation, String createdBy, String itemName, 
			String itemDescription, String location, String unit, Double quantity, Double unitCost, Double amount,
			Double quantityTransfered) {
		
		this.type = type;
		this.status = status;
		this.documentNumber = documentNumber;
		this.date = date;
		this.referenceNumber = referenceNumber;
		this.branch = branch;
		this.transitLocation = transitLocation;
		this.createdBy = createdBy;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.quantityTransfered = quantityTransfered;
		
	}

	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public String getStatus() {
		
		return status;
		
	}
	
	public void setStatus(String status) {
		
		this.status = status;
		
	}
	
	public String getDocumentNumber() {

		return documentNumber;

	}

	public void setDocumentNumber(String documentNumber) {

		this.documentNumber = documentNumber;

	}

	public Date getDate() {
		
		return date;
		
	}
	
	public void setDate(Date date) {
		
		this.date = date;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getBranch() {
		
		return branch;
		
	}
	
	public void setBranch(String branch) {
		
		this.branch = branch;
		
	}
	
	public String getTransitLocation() {
		
		return transitLocation;
		
	}
	
	public void setTransitLocation(String transitLocation) {
		
		this.transitLocation = transitLocation;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getItemName() {
		
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDesc) {
	
		this.itemDescription = itemDesc;
	
	}
	
	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}
	
	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public void setUnitCost(Double unitCost) {
	
		this.unitCost = unitCost;
	
	}
	
	public Double getAmount() {

		return amount;

	}

	public void setAmount(Double amount) {

		this.amount = amount;

	}
	
	public Double getQuantityTransfered() {
		
		return quantityTransfered;
		
	}
	
	public void setQuantityTransfered(Double quantityTransfered) {
		
		this.quantityTransfered = quantityTransfered;
		
	}

} // InvRepBranchStockTransferRegisterData class
