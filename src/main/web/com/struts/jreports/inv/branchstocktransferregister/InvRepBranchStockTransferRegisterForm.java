package com.struts.jreports.inv.branchstocktransferregister;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepBranchStockTransferRegisterForm extends ActionForm implements Serializable{

	private String dateFrom = null;
	private String dateTo = null; 
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String transferOutNumberFrom = null;
	private String transferOutNumberTo = null;
	private String transferInNumberFrom = null;
	private String transferInNumberTo = null;
	private String transferOrderNumberFrom = null;
	private String transferOrderNumberTo = null;
	private boolean includeTransferOut = false;
	private String branchTo = null;
	private ArrayList branchToList = new ArrayList();
	private boolean includeTransferIn = false;
	private String branchFrom = null;	
	private boolean includeTransferOrder = false;
	private boolean showOrderList = false;
	private boolean enableOrderListCheckBox = false;
	private ArrayList branchFromList = new ArrayList();
	private String referenceNumber = null;
	private boolean includeIncoming = false;
	private boolean includeUnposted = false;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private ArrayList invRepBrBstrList = new ArrayList(); 

	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}

	public String getUserPermission() {

		return(userPermission);

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}
	
	public String getReport() {

		return report;

	}
	
	public String getCategory (){
		
		return(category );
		
	}
	
	public void setCategory (String category ){
		
		this.category  = category ;
		
	}
	
	public ArrayList getCategoryList(){
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category){
		
		categoryList.add(category);
		
	}
	public void clearCategoryList(){
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}

	public void setReport(String report) {

		this.report = report;

	}

	public void setGoButton(String goButton) {

		this.goButton = goButton;

	}

	public void setCloseButton(String closeButton) {

		this.closeButton = closeButton;

	}
	
	public String getDateFrom() {
		
		return(dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return(dateTo);
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getTransferOutNumberFrom() {
		
		return(transferOutNumberFrom);
		
	}
	
	public void setTransferOutNumberFrom(String transferOutNumberFrom) {
		
		this.transferOutNumberFrom = transferOutNumberFrom;
		
	}
	
	public String getTransferOutNumberTo() {

		return(transferOutNumberTo);

	}

	public void setTransferOutNumberTo(String transferOutNumberTo) {

		this.transferOutNumberTo = transferOutNumberTo;

	}
	
	public String getTransferInNumberFrom() {
		
		return(transferInNumberFrom);
		
	}
	
	public void setTransferInNumberFrom(String transferInNumberFrom) {
		
		this.transferInNumberFrom = transferInNumberFrom;
		
	}

	
	public String getTransferInNumberTo() {

		return(transferInNumberTo);

	}

	public void setTransferInNumberTo(String transferInNumberTo) {

		this.transferInNumberTo = transferInNumberTo;

	}
	
	public String getTransferOrderNumberFrom() {
		
		return(transferOrderNumberFrom);
		
	}
	
	public void setTransferOrderNumberFrom(String transferOrderNumberFrom) {
		
		this.transferOrderNumberFrom = transferOrderNumberFrom;
		
	}
	public String getTransferOrderNumberTo() {
		
		return(transferOrderNumberTo);
		
	}
	
	public void setTransferOrderNumberTo(String transferOrderNumberTo) {
		
		this.transferOrderNumberTo = transferOrderNumberTo;
		
	}
	public boolean getIncludeTransferOut() {

		return(includeTransferOut);

	}

	public void setIncludeTransferOut(boolean includeTransferOut) {

		this.includeTransferOut = includeTransferOut;

	}
	public String getBranchTo(){
		
		return(branchTo);
		
	}
	
	public void setBranchTo(String branchTo){
		
		this.branchTo = branchTo ;
		
	}
	
	public ArrayList getBranchToList(){
		
		return(branchToList);
		
	}
	
	public void setBranchToList(String branchTo){
		
		branchToList.add(branchTo);
		
	}
	
	public void clearBranchToList(){
		
		branchToList.clear();
		branchToList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getIncludeTransferIn() {

		return(includeTransferIn);

	}

	public void setIncludeTransferIn(boolean includeTransferIn) {

		this.includeTransferIn = includeTransferIn;

	}
	
	public String getBranchFrom(){
		
		return(branchFrom);
		
	}
	
	public void setBranchFrom(String branchFrom){
		
		this.branchFrom = branchFrom ;
		
	}
	
	public boolean getShowOrderList() {

		return(showOrderList);

	}

	public void setShowOrderList(boolean showOrderList) {

		this.showOrderList = showOrderList;

	}
	public boolean getIncludeTransferOrder() {

		return(includeTransferOrder);

	}

	public boolean isEnableOrderListCheckBox() {
		
		return enableOrderListCheckBox;
		
	}
	
	public void setEnableOrderListCheckBox(boolean enableOrderListCheckBox) {
		
		this.enableOrderListCheckBox = enableOrderListCheckBox;
		
	}
	
	public void setIncludeTransferOrder(boolean includeTransferOrder) {

		this.includeTransferOrder = includeTransferOrder;

	}

	
	public ArrayList getBranchFromList(){
		
		return(branchFromList);
		
	}
	
	public void setBranchFromList(String branchFrom){
		
		branchFromList.add(branchFrom);
		
	}
	
	public void clearBranchFromList(){
		
		branchFromList.clear();
		branchFromList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getReferenceNumber(){

		return(referenceNumber);

	}

	public void setReferenceNumber(String referenceNumber){

		this.referenceNumber = referenceNumber ;

	}
	
	public boolean getIncludeIncoming(){

		return(includeIncoming);

	}

	public void setIncludeIncoming(boolean includeIncoming) {

		this.includeIncoming = includeIncoming;

	}
	
	public boolean getIncludeUnposted(){

		return(includeUnposted);

	}

	public void setIncludeUnposted(boolean includeUnposted) {

		this.includeUnposted = includeUnposted;

	}
	
	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public String getViewType(){
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public Object[] getInvRepBrBstrList(){
		
		return invRepBrBstrList.toArray();
		
	}

	public InvRepBranchStockTransferRegisterBranchList getInvRepBrBstrListByIndex(int index){
		
		return ((InvRepBranchStockTransferRegisterBranchList)invRepBrBstrList.get(index));
		
	}
	
	public int getInvRepBrBstrListSize(){
		
		return(invRepBrBstrList.size());
		
	}
	
	public void saveInvRepBrBstrList(Object newInvRepBrBstrList){
		
		invRepBrBstrList.add(newInvRepBrBstrList);   	  
		
	}
	
	public void clearInvRepBrBstrList(){
		
		invRepBrBstrList.clear();
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<invRepBrBstrList.size(); i++) {
			
			InvRepBranchStockTransferRegisterBranchList list = (InvRepBranchStockTransferRegisterBranchList)invRepBrBstrList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  

		goButton = null;
		closeButton = null;
		category = null;
		dateFrom = Common.convertSQLDateToString(new java.util.Date());
		dateTo = Common.convertSQLDateToString(new java.util.Date());
		transferOutNumberFrom = null;
		transferOutNumberTo = null;
		transferInNumberFrom = null;
		transferInNumberTo = null;
		transferOrderNumberFrom = null;
		transferOrderNumberTo = null;
		includeTransferOut = false;
		branchTo = null;
		includeTransferIn = false;
		branchFrom = null;
		includeTransferOrder = false;
		showOrderList = false;
		referenceNumber = null;
		includeIncoming = false;
		includeUnposted = false;
		
		orderByList.clear();
		orderByList.add("DATE");
		orderByList.add("DOCUMENT NUMBER");
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("branchStockTransferRegister.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)){
				
				errors.add("dateTo", new ActionMessage("branchStockTransferRegister.error.dateToInvalid"));
				
			}	
			
			if(includeTransferOut == false && includeTransferIn == false && includeTransferOrder == false && includeIncoming == false ) {
			 	
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("branchStockTransferRegister.error.sourceMustBeEntered"));
			
			}
						
		}

		return(errors);
		
	}
	
}
