package com.struts.jreports.inv.buildorderprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepBuildOrderPrintController;
import com.ejb.txn.InvRepBuildOrderPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepBuildOrderPrintDetails;

public final class InvRepBuildOrderPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {
      	
 /*******************************************************
      	 Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("InvRepBuildOrderPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	InvRepBuildOrderPrintForm actionForm = (InvRepBuildOrderPrintForm)form;
      	
      	String frParam = Common.getUserPermission(user, Constants.INV_REP_BUILD_ORDER_PRINT_ID);
      	
      	if (frParam != null) {
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}
      	
/*******************************************************
  	 Initialize InvRepBuildOrderPrintController EJB
*******************************************************/
      	
      	InvRepBuildOrderPrintControllerHome homeBOP = null;
      	InvRepBuildOrderPrintController ejbBOP = null;   
      	
      	try {
      		
      		homeBOP = (InvRepBuildOrderPrintControllerHome)com.util.EJBHomeFactory.
      		lookUpHome("ejb/InvRepBuildOrderPrintControllerEJB", InvRepBuildOrderPrintControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in InvRepBuildOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbBOP = homeBOP.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in InvRepBuildOrderPrintAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
  
/*******************************************************
  	 -- INV Rep Build Order Print --
*******************************************************/
      	
      	if(frParam != null) {
      	    
      	    AdCompanyDetails adCmpDetails = null;
      	    InvRepBuildOrderPrintDetails details = null;
      	    
      	    ArrayList list = null;           
      	    
      	    try {
      	        
      	        ArrayList borCodeList = new ArrayList();
      	        
      	        if (request.getParameter("forward") != null) {
      	            
      	            borCodeList.add(new Integer(request.getParameter("buildOrderCode")));
      	            
      	        }
      	        
      	        list = ejbBOP.executeInvRepBuildOrderPrint(borCodeList, user.getCmpCode());
      	        
      	        // get company
      	        
      	        adCmpDetails = ejbBOP.getAdCompany(user.getCmpCode());
      	        
      	        // get parameters
      	        
      	        Iterator i = list.iterator();
      	        
      	        while (i.hasNext()) {
      	            
      	            details = (InvRepBuildOrderPrintDetails)i.next();
      	            
      	        }
      	        
      	    } catch (GlobalNoRecordFoundException ex) {
      	        
      	        errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                new ActionMessage("buildOrderPrint.error.noRecordFound"));
      	        
      	    } catch(EJBException ex) {
      	        
      	        if(log.isInfoEnabled()) {
      	            log.info("EJBException caught in InvRepBuildOrderPrintAction.execute(): " + ex.getMessage() +
      	                    " session: " + session.getId());
      	            
      	        }
      	        
      	        return(mapping.findForward("cmnErrorPage"));
      	        
      	    }
      	    
      	    if(!errors.isEmpty()) {
      	        
      	        saveErrors(request, new ActionMessages(errors));
      	        return(mapping.findForward("invRepBuildOrderPrint"));
      	        
      	    }	
      	    
      	    // fill report parameters, fill report to pdf and set report session
      	    
      	    Map parameters = new HashMap();
      	    parameters.put("company", adCmpDetails.getCmpName());
      	    parameters.put("createdBy", details.getBopBorCreatedBy());
      	    parameters.put("approvedBy", details.getBopBorApprovedRejectedBy());
      	    
      	    String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildOrderPrint.jasper";
      	    
      	    if (!new java.io.File(filename).exists()) {
      	        
      	        filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildOrderPrint.jasper");
      	        
      	    }
      	    
      	    try {
      	        
      	        Report report = new Report();
      	        
      	        report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
      	        report.setBytes(
      	                JasperRunManager.runReportToPdf(filename, parameters, 
      	                        new InvRepBuildOrderPrintDS(list)));  
      	        
      	        session.setAttribute(Constants.REPORT_KEY, report);
      	        
      	        
      	    } catch(Exception ex) {
      	        
      	        if(log.isInfoEnabled()) {
      	            
      	            log.info("Exception caught in InvRepBuildOrderPrintAction.execute(): " + ex.getMessage() +
      	                    " session: " + session.getId());
      	            
      	        } 
      	        
      	        return(mapping.findForward("cmnErrorPage"));
      	        
      	    }              
      	    
      	    return(mapping.findForward("invRepBuildOrderPrint"));
      	    
      	} else {
      	    
      	    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
      	    saveErrors(request, new ActionMessages(errors));
      	    
      	    return(mapping.findForward("invRepBuildOrderPrintForm"));
      	    
      	}
      	
      } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepBuildOrderAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
