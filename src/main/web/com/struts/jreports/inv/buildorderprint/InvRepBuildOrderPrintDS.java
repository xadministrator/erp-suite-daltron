package com.struts.jreports.inv.buildorderprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepBuildOrderPrintDetails;

public class InvRepBuildOrderPrintDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepBuildOrderPrintDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			InvRepBuildOrderPrintDetails details = (InvRepBuildOrderPrintDetails)i.next();
			
			InvRepBuildOrderPrintData dtbData = new InvRepBuildOrderPrintData(Common.convertSQLDateToString(details.getBopBorDate()), details.getBopBorDocumentNumber(),
					details.getBopBorReferenceNumber(), details.getBopBorDescription(), details.getBopBolIiName(), 
					details.getBopBolIiDescription(), details.getBopBolLocName(), new Double(details.getBopBolQuantityRequired()),
					details.getBopBolUomName(), new Double(details.getBopBolUnitCost()));
			
			data.add(dtbData);
			
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("date".equals(fieldName)) {
			value = ((InvRepBuildOrderPrintData)data.get(index)).getDate();
		} else if("documentNumber".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getDocumentNumber();
		} else if("referenceNumber".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getReferenceNumber();
		} else if("description".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getDescription();	
		} else if("itemName".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getItemName();
		} else if("itemDescription".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getItemDescription();
		} else if("itemLocation".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getLocationName();
		} else if("quantityRequired".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getQuantityRequired();	
		} else if("unit".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getUnit();
		} else if("unitCost".equals(fieldName)) {	
			value = ((InvRepBuildOrderPrintData)data.get(index)).getUnitCost();	
		}
		
		return(value);
		
	}
	
}
