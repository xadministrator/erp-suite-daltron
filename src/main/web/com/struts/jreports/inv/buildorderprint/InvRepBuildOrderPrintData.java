package com.struts.jreports.inv.buildorderprint;

public class InvRepBuildOrderPrintData implements java.io.Serializable {
	
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String itemName = null;
	private String itemDescription = null;
	private String locationName = null;
	private Double quantityRequired = null;
	private String unit = null;
	private String description = null;
	private Double unitCost = null;
	
	public InvRepBuildOrderPrintData(String date, String documentNumber, String referenceNumber, String description, String itemName,
			String itemDescription, String locationName, Double quantityRequired, String unit, Double unitCost) {
			
			this.date = date;
			this.documentNumber = documentNumber;
			this.referenceNumber = referenceNumber;
			this.itemName = itemName;
			this.itemDescription = itemDescription;
			this.locationName = locationName;
			this.quantityRequired = quantityRequired;
			this.unit = unit;
			this.description = description;
			this.unitCost = unitCost;
			
	}
	
	public String getDate() {
	
		return date;
	
	}
	
	public String getDocumentNumber() {
	
		return documentNumber;
	
	}
	
	public String getReferenceNumber() {
		
			return referenceNumber;
		
		}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public String getItemDescription() {
		
		return itemDescription;
	
	}
	
	public String getLocationName() {
		
			return locationName;
		
		}
	
	public Double getQuantityRequired() {
	
		return quantityRequired;
	
	}
	
	public String getUnit() {
	
		return unit;
	
	}
	
	
	public String getDescription(){
		
		return description;
	}
	
	public Double getUnitCost(){
		
		return unitCost;
	}


}
