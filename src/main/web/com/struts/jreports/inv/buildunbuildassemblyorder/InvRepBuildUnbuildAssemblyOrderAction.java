package com.struts.jreports.inv.buildunbuildassemblyorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepBuildUnbuildAssemblyOrderController;
import com.ejb.txn.InvRepBuildUnbuildAssemblyOrderController;
import com.ejb.txn.InvRepBuildUnbuildAssemblyOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepBuildUnbuildAssemblyOrderAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)    
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
			Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("InvRepBuildUnbuildAssemblyOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			InvRepBuildUnbuildAssemblyOrderForm actionForm = (InvRepBuildUnbuildAssemblyOrderForm)form;
			
			// reset report to null
			actionForm.setReport(null);
			
			String frParam = Common.getUserPermission(user, Constants.INV_REP_BUILD_UNBUILD_ASSEMBLY_ORDER_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("invRepBuildUnbuildAssemblyOrder");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
/*******************************************************
 			Initialize InvRepBuildUnbuildAssemblyOrderController EJB
*******************************************************/
			
			InvRepBuildUnbuildAssemblyOrderControllerHome homeRR = null;
			InvRepBuildUnbuildAssemblyOrderController ejbRR = null;       
			
			try {
				
				homeRR = (InvRepBuildUnbuildAssemblyOrderControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/InvRepBuildUnbuildAssemblyOrderControllerEJB", InvRepBuildUnbuildAssemblyOrderControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in InvRepBuildUnbuildAssemblyOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbRR = homeRR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in InvRepBuildUnbuildAssemblyOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			/*** get report session and if not null set it to null **/
			
			Report reportSession = 
				(Report)session.getAttribute(Constants.REPORT_KEY);
			
			if(reportSession != null) {
				
				reportSession.setBytes(null);
				session.setAttribute(Constants.REPORT_KEY, reportSession);
				
			}
			
/*******************************************************
			-- AP RR Go Action --
*******************************************************/
			
			if(request.getParameter("goButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArrayList list = null;
				
				String company = null;
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();            	
					
					if (!Common.validateRequired(actionForm.getCustomerCode())) {
						
						criteria.put("customerCode", actionForm.getCustomerCode());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}	
					
					
					if (!Common.validateRequired(actionForm.getDueDateFrom())) {
						
						criteria.put("dueDateFrom", Common.convertStringToSQLDate(actionForm.getDueDateFrom()));
						
					}
					
					if (!Common.validateRequired(actionForm.getDueDateTo())) {
						
						criteria.put("dueDateTo", Common.convertStringToSQLDate(actionForm.getDueDateTo()));
						
					}
					
					
					
					if (!Common.validateRequired(actionForm.getItemName())) {
						
	      				criteria.put("itemName", actionForm.getItemName());
	      			}
	      			
	      			
	      			if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	      				criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	      			}
	      			if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	      				criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	      			}
	      			
	      			
					
					if (!Common.validateRequired(actionForm.getCategory())) {
						
						criteria.put("category", actionForm.getCategory());
						
					}
					
					if (!Common.validateRequired(actionForm.getLocation())) {
						
						criteria.put("location", actionForm.getLocation());
						
					}
					
					if (actionForm.getIncludeUnposted()){
						
						criteria.put("includeUnposted", "yes");
						
					} else {
						
						criteria.put("includeUnposted", "no");
						
					}
					
					if (actionForm.getIncludeCancelled()){
						
						criteria.put("includeCancelled", "yes");
						
					} else {
						
						criteria.put("includeCancelled", "no");
						
					}

					// save criteria
					
					actionForm.setCriteria(criteria);
					
				}
				
				// branch map
				
				ArrayList adBrnchList = new ArrayList();
				// ArrayList adBrnchList2 = new ArrayList();
				
				for(int j=0; j < actionForm.getInvRepBrBldUnbldAssmblyOrdrListSize(); j++) {
				    
				    InvRepBranchBuildUnbuildAssemblyOrderList invRepBrBldUnbldAssemblyOrdrList = (InvRepBranchBuildUnbuildAssemblyOrderList)actionForm.getInvRepBrBldUnbldAssmblyOrdrByIndex(j);
				    
				    if(invRepBrBldUnbldAssemblyOrdrList.getBranchCheckbox() == true) {
				        
				        AdBranchDetails mdetails = new AdBranchDetails();	                    
				        mdetails.setBrCode(invRepBrBldUnbldAssemblyOrdrList.getBrCode());
				        adBrnchList.add(mdetails);
				        
				    }
				    
				}
				
				try {
					
					// get company
					
					AdCompanyDetails adCmpDetails = ejbRR.getAdCompany(user.getCmpCode());
					company = adCmpDetails.getCmpName();
					
					// execute report
					
					list = ejbRR.executeInvRepBuildUnbuildAssemblyOrder(actionForm.getCriteria(),actionForm.getOrderBy(), actionForm.getSummarize(), adBrnchList, user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("purchaseOrderRep.error.noRecordFound"));
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						
						log.info("EJBException caught in InvRepBuildUnbuildAssemblyOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("invRepBuildUnbuildAssemblyOrder");
					
				}
				
				
				// fill report parameters, fill report to pdf and set report session
				
				Map parameters = new HashMap();
				parameters.put("company", company);
				parameters.put("branchName", user.getCurrentBranch().getBrName());
				parameters.put("dateToday", new java.util.Date());
				parameters.put("printedBy", user.getUserName());
				parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
				
				if (actionForm.getIncludeUnposted()){
					
					parameters.put("includeUnposted", "yes");
					
				} else {
					
					parameters.put("includeUnposted", "no");
					
				}
				
				
				if (actionForm.getIncludeCancelled()){
					
					parameters.put("includeCancelled", "yes");
					
				} else {
					
					parameters.put("includeCancelled", "no");
					
				}
				
				if (actionForm.getCustomerCode() != null) {
					
					parameters.put("customerCode", actionForm.getCustomerCode());
					
				}
				
				if (actionForm.getDateFrom() != null) {
					
					parameters.put("dateFrom", actionForm.getDateFrom());
				}
				
				if (actionForm.getDateTo() != null) {
					
					parameters.put("dateTo", actionForm.getDateTo());
				}	    		    	
				
			
				if (actionForm.getDueDateFrom() != null) {
					
					parameters.put("dueDateFrom", actionForm.getDueDateFrom());
				}
				
				if (actionForm.getDueDateTo() != null) {
					
					parameters.put("dueDateTo", actionForm.getDueDateTo());
				}
				
				
				if (actionForm.getItemName() != null) {
					
					parameters.put("itemName", actionForm.getItemName());
					
				}
				
				
				
				
				if (actionForm.getDocumentNumberFrom() != null) {
					
					parameters.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
					
				}
				
				if (actionForm.getDocumentNumberTo() != null) {
					
					parameters.put("documentNumberTo", actionForm.getDocumentNumberTo());
					
				}
				
				
			
      			
				
				
				if (actionForm.getCategory() != null) {
					
					parameters.put("category", actionForm.getCategory());
					
				}
				
				if (actionForm.getLocation() != null) {
					
					parameters.put("location", actionForm.getLocation());
					
				}
				
				String branchMap = null;
				boolean first = true;
				for(int j=0; j < actionForm.getInvRepBrBldUnbldAssmblyOrdrListSize(); j++) {

					InvRepBranchBuildUnbuildAssemblyOrderList brList = (InvRepBranchBuildUnbuildAssemblyOrderList)actionForm.getInvRepBrBldUnbldAssmblyOrdrByIndex(j);

					if(brList.getBranchCheckbox() == true) {
						if(first) {
							branchMap = brList.getBrName();
							first = false;
						} else {
							branchMap = branchMap + ", " + brList.getBrName();
						}
					}

				}
				parameters.put("branchMap", branchMap);
				
				String filename = null;
				if (actionForm.getReportType().equals("MANUFACTURING ORDER")) {
					
					filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildAssemblyOrderMO.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildAssemblyOrderMO.jasper");

					}
				
				} else if(actionForm.getReportType().equals("EXPLOSION")){
					
					filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildAssemblyOrderMOExplosion.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildAssemblyOrderMOExplosion.jasper");

					}
			
				} else if(actionForm.getReportType().equals("PACKAGING ORDER")){
					
					filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildAssemblyOrderPO.jasper";
					
					if (!new java.io.File(filename).exists()) {
						
						filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildAssemblyOrderPO.jasper");

					}
					
					
				} else {
				
				filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildAssemblyOrder.jasper";

				if (!new java.io.File(filename).exists()) {
					
					filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildAssemblyOrder.jasper");

				}	
				}
				
				try {
					
					Report report = new Report();
					
					if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
						report.setBytes(
								JasperRunManager.runReportToPdf(filename, parameters, 
										new InvRepBuildUnbuildAssemblyOrderDS(list)));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
						report.setBytes(
								JasperRunManagerExt.runReportToXls(filename, parameters, 
										new InvRepBuildUnbuildAssemblyOrderDS(list)));   
						
					} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
						
						report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
						report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
								new InvRepBuildUnbuildAssemblyOrderDS(list)));												    
						
					}
					
					session.setAttribute(Constants.REPORT_KEY, report);
					actionForm.setReport(Constants.STATUS_SUCCESS);		    	
					
				} catch(Exception ex) {
					
					if(log.isInfoEnabled()) {
						log.info("Exception caught in InvRepBuildUnbuildAssemblyOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}		    	    
				
/*******************************************************
			-- AP RR Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
			-- INV RR Load Action --
*******************************************************/
				
			}
			
			if(frParam != null) {
				
				
				
				try {
					ArrayList list = null;			       			       
			        Iterator i = null;			
					
					actionForm.clearReportTypeList();
			        
			        list = ejbRR.getAdLvReportTypeAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            	
	            		actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setReportTypeList((String)i.next());
	            			
	            		}
	            		            		
	            	}
					
					
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in GlRepGeneralLedgerAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
			    
			    try {
			    	
			    	actionForm.reset(mapping, request);
			        
			        ArrayList list = null;
			        Iterator i = null;
			        
			        actionForm.clearInvRepBrBldUnbldAssmblyOrdrList();
			        
			        list = ejbRR.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
			        
			        i = list.iterator();
			        
			        while(i.hasNext()) {
			            
			            AdBranchDetails details = (AdBranchDetails)i.next();
			            
			            InvRepBranchBuildUnbuildAssemblyOrderList invRepBrBldUnbldAssemblyOrdrList = new InvRepBranchBuildUnbuildAssemblyOrderList(actionForm,
			                    details.getBrCode(),
			                    details.getBrBranchCode(), details.getBrName());
			            invRepBrBldUnbldAssemblyOrdrList.setBranchCheckbox(true);
			            
			            actionForm.saveInvRepBrBldUnbldAssmblyOrdrList(invRepBrBldUnbldAssemblyOrdrList);
			            
			        }
			        
			    } catch (GlobalNoRecordFoundException ex) {
			        
			    } catch (EJBException ex) {
			        
			        if (log.isInfoEnabled()) {
			            
			            log.info("EJBException caught in InvRepBuildUnbuildAssemblyOrderAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			            return mapping.findForward("cmnErrorPage"); 
			            
			        }
			        
			    } 
			    
			    try {
			        
			        ArrayList list = null;			       			       
			        Iterator i = null;
					
					actionForm.clearCategoryList();           	
					
					list = ejbRR.getAdLvInvItemCategoryAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setCategoryList((String)i.next());
							
						}
						
					}
					
					actionForm.clearLocationList();           	
					
					list = ejbRR.getInvLocAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					
				} catch(EJBException ex) {
					
					if(log.isInfoEnabled()) {
						log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}

				return(mapping.findForward("invRepBuildUnbuildAssemblyOrder"));		          
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			
/*******************************************************
			 System Failed: Forward to error page 
*******************************************************/
			if(log.isInfoEnabled()) {
				
				log.info("Exception caught in InvRepItemCostingAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}   
			
			return(mapping.findForward("cmnErrorPage"));   
			
		}
		
	}
}
