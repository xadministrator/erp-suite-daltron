package com.struts.jreports.inv.buildunbuildassemblyorder;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


import com.util.ApRepPurchaseOrderDetails;
import com.util.InvRepBuildUnbuildAssemblyOrderDetails;

public class InvRepBuildUnbuildAssemblyOrderDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepBuildUnbuildAssemblyOrderDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
    	  		 
    	  InvRepBuildUnbuildAssemblyOrderDetails details = (InvRepBuildUnbuildAssemblyOrderDetails)i.next();
	         
	         InvRepBuildUnbuildAssemblyOrderData argData = new InvRepBuildUnbuildAssemblyOrderData( details.getBoDate(), details.getBoBranchCode(),
	         		details.getBoItemName(), details.getBoItemDescription(), details.getBoLocation(),
					details.getBoDocNo(), details.getBoNumber(), details.getBoCustomerName(), details.getBoUnit(),
					new Double(details.getBoQuantity()), new Double(details.getBoSpecificGravity()), new Double(details.getBoStandardFillSize()), 
					
					new Double(details.getBoUnitCost()),
					new Double(details.getBoAmount()), new Double(details.getBoReceived()), new Double(details.getBoRemaining()),
					details.getBoVoid(), details.getBoCreatedBy(), details.getBoApprovedRejectedBy(),
					details.getBoDescription(), details.getBoReferenceNumber(), details.getBoPaymentTermName(), details.getBoCustomerAddress(), details.getBoCustomerPhoneNumber(),
					details.getBoCustomerFaxNumber(),
					
					details.getBoBmIiCode(), details.getBoBmIiName(), details.getBoBmUomName(), new Double(details.getBoBmQuantityNeeded()), new Double(details.getBoBmQuantityOnHand())  , new Double(details.getBoBmCost()), details.getBoBmCategory()
	        		    
	        		 );

	         data.add(argData);
	         
      }
      
   }
   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getDate();
   		
   	} else if("branchCode".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBoBranchCode();	
	
   	} else if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getLocation();
   		
   	} else if("documentNumber".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBoDocNo();
   		
   	} else if("boNumber".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBoNumber();
   		
   	} else if("supplierName".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getCustomerName();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getUnit();
   		
   	}else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getQuantity();
   		
   	}else if("specificGravity".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getSpecificGravity();
   		
   	}else if("standardFillSize".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getStandardFillSize();
   		
   	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getUnitCost();
   		
   	} else if("amount".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getAmount();
   		
   	} else if("received".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getReceived();
   		
   	} else if("remaining".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getRemaining();
   		
   		
   		
   	} else if("boVoid".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBoVoid();
   		
   	} else if("createdBy".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getCreatedBy();
   		
   	} else if("approvedRejectedBy".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getApprovedRejectedBy();
   		
   	} else if("description".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getDescription();
   		
   	} else if("referenceNumber".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getReferenceNumber();
   		
   	} else if("paymentTermName".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getReferenceNumber();
   		
   	} else if("supplierAddress".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getCustomerAddress();
   		
   	} else if("supplierPhoneNumber".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getCustomerPhoneNumber();
   		
   	} else if("supplierFaxNumber".equals(fieldName)) {
   		
   		value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getCustomerFaxNumber();
   		
   	
   		
   	}else if("bomItemCode".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomItemCode();
   	}else if("bomItemName".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomItemName();     
   	}else if("bomQuantityNeeded".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomQuantityNeeded();
    }else if("bomQuantityOnHand".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomQuantityOnHand();
    }else if("bomCost".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomCost();
    }else if("bomCategory".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomCategory();
    }else if("bomUOM".equals(fieldName)){
        value = ((InvRepBuildUnbuildAssemblyOrderData)data.get(index)).getBomUOM();  
    }
   	
   	return(value);
   	
   }
   
}
