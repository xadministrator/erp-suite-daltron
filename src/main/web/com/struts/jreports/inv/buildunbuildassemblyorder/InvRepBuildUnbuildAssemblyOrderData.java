package com.struts.jreports.inv.buildunbuildassemblyorder;

import java.util.Date;

public class InvRepBuildUnbuildAssemblyOrderData implements java.io.Serializable {

	private Date date;
	private String branchCode;
	private String itemName;
	private String itemDescription;
	private String location;
	private String documentNumber;
	private String boNumber;
	private String customerName;
	private String unit;
	private Double quantity;
	private Double specificGravity;
	private Double standardFillSize;
	private Double unitCost;
	private Double amount;
	private Double received;
	private Double remaining;
	
	
	private String boVoid;
	private String createdBy;
	private String approvedRejectedBy;
	private String description;
	private String referenceNumber;
	private String paymentTermName;
	private String customerAddress;
	private String customerPhoneNumber;
	private String customerFaxNumber;
	
	private String bomItemCode = null;
    private String bomItemName = null;
    private String bomUOM = null;   
    private Double bomQuantityNeeded = null;
    private Double bomQuantityOnHand = null;
    private Double bomCost = null; 
    private String bomCategory = null;

	public InvRepBuildUnbuildAssemblyOrderData(	Date date, String branchCode, String itemName, String itemDescription, String location,
			String documentNumber, String boNumber, String customerName, String unit, Double quantity, Double specificGravity, Double standardFillSize,
			Double unitCost, 
			Double amount, Double received, Double remaining, String boVoid, String createdBy, String approvedRejectedBy,
			String description, String referenceNumber, String paymentTermName, String customerAddress, String customerPhoneNumber, String customerFaxNumber,
			String bomItemCode, String bomItemName, String bomUOM, double bomQuantityNeeded, double bomQuantityOnHand, double bomCost, String bomCategory
    		
			) {
		
		this.date = date;
		this.branchCode = branchCode;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.documentNumber = documentNumber;
		this.boNumber = boNumber;
		this.customerName = customerName;
		this.unit = unit;
		this.quantity = quantity;
		this.received = received;
		this.remaining = remaining;
		this.specificGravity = specificGravity;
		this.unitCost = unitCost;
		this.amount = amount;

		this.standardFillSize = standardFillSize;
		this.boVoid = boVoid;
		this.createdBy = createdBy;
		this.approvedRejectedBy = approvedRejectedBy;
		this.description = description;
		this.referenceNumber = referenceNumber;		
		this.paymentTermName = paymentTermName;
		this.customerAddress = customerAddress;
		this.customerPhoneNumber = customerPhoneNumber;
		this.customerFaxNumber = customerFaxNumber;
		
		this.bomItemCode = bomItemCode;
        this.bomItemName = bomItemName;
        this.bomUOM = bomUOM;
        this.bomQuantityNeeded = bomQuantityNeeded;
        this.bomQuantityOnHand = bomQuantityOnHand;
        this.bomCost = bomCost;
        this.bomCategory = bomCategory;
	}

	
	public Date getDate() {
		
		return date;
		
	}
	
	public void setDate(Date date) {
		
		this.date = date;
		
	}
	
	public String getBoBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBoBranchCode(String branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public String getBoDocNo() {
		
		return documentNumber;
		
	}
	
	public void setBoDocNo(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	
	public String getBoNumber() {
		
		return boNumber;
		
	}
	
	public void setBoNumber(String boNumber) {
		
		this.boNumber = boNumber;
		
	}
	
	public Double getAmount() {
		
		return amount;
	
	}
	
	public void setAmount(Double amount) {
	
		this.amount = amount;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDescription) {
	
		this.itemDescription = itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
public Double getSpecificGravity() {
		
		return specificGravity;
	
	}
	
	public void setSpecificGravity(Double specificGravity) {
		
		this.specificGravity = specificGravity;
		
	}
	
	public Double getStandardFillSize() {
		
		return standardFillSize;
	
	}
	
	public void setStandardFillSize(Double standardFillSize) {
		
		this.standardFillSize = standardFillSize;
		
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public void setLocation(String location) {
	
		this.location = location;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public void setUnitCost(Double unitCost) {
	
		this.unitCost = unitCost;
	
	}
	
	public String getCustomerName() {
		
		return customerName;

	}
	
	public void setCustomerName(String customerName) {
		
		this.customerName = customerName;
		
	}
	
	public String getUnit() {
		
		return unit;

	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public Double getReceived() {
		
			return received;
		
		}
		
	public void setReceived(Double received) {
		
		this.received = received;
		
	}
	
	
	public Double getRemaining() {
		
		return remaining;
	
	}
	
	public void setRemaining(Double remaining) {
		
		this.remaining = remaining;
		
	}
	
	public String getBoVoid() {
		
		return boVoid;
	}
	
	public void setBoVoid(String boVoid) {
		
		this.boVoid = boVoid;
	}
	
	public String getCreatedBy() {
		
		return createdBy;
	}
	
	public void setCreatedBY(String createdBy) {
		
		this.createdBy = createdBy;
	}
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getDescription() {
		
		return description;
	}
	
	public void setDescription(String description) {
		
		this.description = description;
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
	}
	
	public String getPaymentTermName() {
		
		return referenceNumber;
	}
	
	public void setPaymentTermName(String paymentTermName) {
		
		this.paymentTermName = paymentTermName;
	}
	
	public void setCustomerAddress(String customerAddress) {
		
		this.customerAddress = customerAddress;
	}
	public String getCustomerAddress() {
		
		return customerAddress;
	}
	
	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		
		this.customerPhoneNumber = customerPhoneNumber;
	}
	public String getCustomerPhoneNumber() {
		
		return customerPhoneNumber;
	}
	
	public void setCustomerFaxNumber(String customerFaxNumber) {
		
		this.customerFaxNumber = customerFaxNumber;
	}
	public String getCustomerFaxNumber() {
		
		return customerFaxNumber;
	}
	
	
	public String getBomItemCode() {
        
        return bomItemCode;
        
    }
    
    public String getBomItemName() {
        
        return bomItemName;
        
    }
    
    public Double getBomQuantityNeeded() {
        
        return bomQuantityNeeded;
        
    }
    
    public Double getBomQuantityOnHand() {
        
        return bomQuantityOnHand;
        
    }
    public Double getBomCost() {
        
        return bomCost;
        
    }
    
    public String getBomUOM() {
        
        return bomUOM;
        
    }

    public String getBomCategory() {
        
        return bomCategory;
        
    }
} // ApRepPurchaseOrderData class
