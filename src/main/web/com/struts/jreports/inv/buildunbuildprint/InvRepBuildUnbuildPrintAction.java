package com.struts.jreports.inv.buildunbuildprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepBuildUnbuildPrintController;
import com.ejb.txn.InvRepBuildUnbuildPrintControllerHome;
import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.InvRepBuildUnbuildPrintDetails;

public final class InvRepBuildUnbuildPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepBuildUnbuildPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvRepBuildUnbuildPrintForm actionForm = (InvRepBuildUnbuildPrintForm)form; 
         
         String frParam= Common.getUserPermission(user, Constants.INV_REP_BUILD_UNBUILD_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
             
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize InvRepBuildUnbuildPrintController EJB
*******************************************************/

         InvRepBuildUnbuildPrintControllerHome homeBUA = null;
         InvRepBuildUnbuildPrintController ejbBUA = null;       

         try {
         	
        	 homeBUA = (InvRepBuildUnbuildPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepBuildUnbuildPrintControllerEJB", InvRepBuildUnbuildPrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepBuildUnbuildPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
        	 ejbBUA = homeBUA.create();
            
         } catch(CreateException e) {
        	 
        	 Debug.printStackTrace(e);
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepBuildUnbuildPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO getInvRepBuildUnbuildEntryCode() --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    InvRepBuildUnbuildPrintDetails details = null; 
			    String VIEW_TYP = "";
			    ArrayList list = null;   
			    
				try {
					
					ArrayList atrCodeList = new ArrayList();
					
					
					if (request.getParameter("forward") != null) {
						
						System.out.println("FORWARDED------------->");
								try{
									atrCodeList.add(new Integer(request.getParameter("buildUnbuildAssemblyCode")));					
									actionForm.setReportType(request.getParameter("reportType"));
									VIEW_TYP = request.getParameter("viewType");
									
								}
								catch (Exception h){
									
									System.out.println ("UYYYYYYYYYYYYYYYYYYYYYYYYYYy");
									h.printStackTrace();
									
								}
				
					}
	            	
	            	list = ejbBUA.executeInvRepBuildUnbuildPrint(atrCodeList, actionForm.getReportType(), user.getCmpCode());
	            
			        // get company
			       
			        adCmpDetails = ejbBUA.getAdCompany(user.getCmpCode());	
			       
			        // get parameters
	      			
	      			Iterator i = list.iterator();
	      			
	      			while (i.hasNext()) {
	      				
	      				details = (InvRepBuildUnbuildPrintDetails)i.next();

	      			}
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("buildUnbuildPrint.error.buildUnbuildEntryAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
	            	
	            	Debug.printStackTrace(ex);
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvBuildUnbuildPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("invRepBuildUnbuildPrint");

	             }
				
				// fill report parameters, fill report to pdf and set report session  	    	               
			    
			    Map parameters = new HashMap();
				parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("createdBy", details.getBupBuaCreatedBy());
				
				
		        
		        String filename = null;
			
			    System.out.println("REPORT TYPE="+actionForm.getReportType());
			    if (actionForm.getReportType().equals("PACKAGING ORDER REPORT")) {

				    filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildPackagingPrint.jasper";
				       
				    if (!new java.io.File(filename).exists()) {
				    		    		    
				       filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildPackagingPrint.jasper");
					    
				    }
			    } else if (actionForm.getReportType().equals("MANUFACTURING ORDER REPORT")){
				    	
			
				    filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildManufacturingPrint.jasper";
				       
				    if (!new java.io.File(filename).exists()) {
				    		    		    
				       filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildManufacturingPrint.jasper");
					    
				    }	
				    	
			    } else if (actionForm.getReportType().equals("EXPLOSION REPORT")){
			    	
					
				    filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildExplosionPrint.jasper";
				       
				    if (!new java.io.File(filename).exists()) {
				    		    		    
				       filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildExplosionPrint.jasper");
					    
				    }	
				    	
				     	
				  
	         	} else {
	         		 filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepBuildUnbuildPrint.jasper";
				       
			        if (!new java.io.File(filename).exists()) {
			       		    		    
			           filename = servlet.getServletContext().getRealPath("jreports/InvRepBuildUnbuildPrint.jasper");
				    
			        }	    	
			    	
			    }
				 			    	    	    	        	    				
				try {
				    
			    Report report = new Report();
			    
			    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {
			       	
			       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				       report.setBytes(
				          JasperRunManager.runReportToPdf(filename, parameters, 
				        		  new InvRepBuildUnbuildPrintDS(list)));    
					        
				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
		               
		               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		               report.setBytes(
						   JasperRunManagerExt.runReportToXls(filename, parameters, 
								   new InvRepBuildUnbuildPrintDS(list)));  
					        
				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){
					   
					   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
					   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
							   new InvRepBuildUnbuildPrintDS(list)));  												    
					        
				   }
				   
			       session.setAttribute(Constants.REPORT_KEY, report);
			       //actionForm.setReport(Constants.STATUS_SUCCESS);	
				   
				} catch(Exception ex) {
					ex.printStackTrace();
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in InvRepBuildUnbuildPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("invRepBuildUnbuildPrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("invRepBuildUnbuildPrintForm"));
		
		 }
	 
     } catch(Exception e) {
    	 
    	 Debug.printStackTrace(e);
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in InvRepBuildUnbuildPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       }
    }
}
