package com.struts.jreports.inv.buildunbuildprint;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepBuildUnbuildPrintDetails;

public class InvRepBuildUnbuildPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepBuildUnbuildPrintDS(ArrayList list) {
       
       Iterator i = list.iterator();
       
       while(i.hasNext()) {
           
           InvRepBuildUnbuildPrintDetails details = (InvRepBuildUnbuildPrintDetails)i.next();
           
           InvRepBuildUnbuildPrintData invRepBUAData = new InvRepBuildUnbuildPrintData(
                   Common.convertSQLDateToString(details.getBupBuaDate()), details.getBupBuaDocumentNumber(), details.getBupBuaCustomerName(), details.getBupBuaReferenceNumber(),
                   details.getBupBuaDescription(), details.getBupBuaCreatedBy(), details.getBupBlIiName(), details.getBupBlIiDesc(), details.getBupBlIiCategory(),
                   details.getBupBlIiNameArray(), details.getBupBlIiDescArray(), 
                   details.getBupBlLocName(), details.getBupBlUomName(), 
                   new Double(details.getBupBlBuildQuantity()), details.getBupBlBuildQuantityArray(), 
                   new Double(details.getBupBlRemaining()), new Double(details.getBupBlReceived()), 
                   new Double(details.getBupBlBuildSpecificGravity()),new Double(details.getBupBlBuildStandardFillSize()),
                   
        		   details.getBupBlBmIiCode(), details.getBupBlBmIiName(), details.getBupBlBmUomName(), 
        		   new Double(details.getBupBlBmQuantityNeeded()), new Double(details.getBupBlBmQuantityOnHand()) , new Double( details.getBupBlBmUnpostedQuantity()),
        		   new Double(details.getBupBlBmCost()), details.getBupBlBmCategory(),
        		   
        		   details.getBupBlBmQualityControl1(), details.getBupBlBmQualityControl2(), details.getBupBlBmQualityControlOthers(),
        		   details.getBupBlBmQualityControl1Remaining(), details.getBupBlBmQualityControl2Remaining(), details.getBupBlBmQualityControlOthersRemaining()
        		   
        		   );
           
           data.add(invRepBUAData);
           
       }   	
       
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      
      if("date".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getDate();
      }else if("documentNumber".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getDocumentNumber();   
      }else if("customerName".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getCustomerName();
      }else if("referenceNumber".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getReferenceNumber();          
      }else if("description".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getDescription();          
      }else if("createdBy".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getCreatedBy();
      }else if("itemName".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getItemName();     
      }else if("itemDesc".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getItemDesc();
      }else if("itemCategory".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getItemCategory();
          
      }else if("itemNameArray".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getItemNameArray(); 
          System.out.println("itemNameArray="+value);
      }else if("itemDescArray".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getItemDescArray();
          System.out.println("itemDescArray="+value);
      }else if("locationName".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getLocationName();
      }else if("unitMeasureShortName".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getUnitMeasureShortName();
      }else if("buildQuantity".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBuildQuantity();
      }else if("buildQuantityArray".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBuildQuantityArray();
      }else if("remaining".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getRemaining();
      }else if("received".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getReceived();
         
          
      }else if("specificGravity".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBuildSpecificGravity();
      }else if("standardFillSize".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBuildStandardFillSize();
          
      }else if("bomQuantity".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQuantityNeeded();
      }else if("bomQuantityOnHand".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQuantityOnHand();
      }else if("bomUnpostedQuantity".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomUnpostedQuantity();
      
      }else if("bomCost".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomCost();
      }else if("bomCategory".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomCategory();
      }else if("bomItemName".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomItemName();
      }else if("bomItemCode".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomItemCode();
      }else if("bomUOM".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomUOM();  
      }else if("qualityControl1".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControl1();
      }else if("qualityControl2".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControl2();
      }else if("qualityControlOthers".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControlOthers();
          
      }else if("qualityControl1Remaining".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControl1Remaining();  
      }else if("qualityControl2Remaining".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControl2Remaining();
      }else if("qualityControlOthersRemaining".equals(fieldName)){
          value = ((InvRepBuildUnbuildPrintData)data.get(index)).getBomQualityControlOthersRemaining();
      }
      
      return(value);
   }
}
