package com.struts.jreports.inv.buildunbuildprint;


public class InvRepBuildUnbuildPrintData implements java.io.Serializable {
    
    private String date = null;
    private String documentNumber = null;
    private String customerName = null;
    private String referenceNumber = null;
    private String description = null;
    private String createdBy = null;
    
    private String itemName = null;
    private String itemDesc = null;
    private String itemCategory = null;
    private String itemNameArray = null;
    private String itemDescArray = null;
    private String locationName = null;
    private String unitMeasureShortName = null;    
    private Double buildQuantity = null;
    private String buildQuantityArray = null;
    private Double remaining = null;
    private Double received = null;
    private Double buildSpecificGravity = null;
    private Double buildStandardFillSize = null;
    
    private String bomItemCode = null;
    private String bomItemName = null;
    private String bomUOM = null;   
    private Double bomQuantityNeeded = null;
    private Double bomQuantityOnHand = null;
    private Double bomUnpostedQuantity = null;
    private Double bomCost = null; 
    private String bomCategory = null;
    
    
    private String bomQualityControl1 = null;
    private String bomQualityControl2 = null;
    private String bomQualityControlOthers = null;
    
    private Double bomQualityControl1Remaining = null;
    private Double bomQualityControl2Remaining = null;
    private Double bomQualityControlOthersRemaining = null;
    
    public InvRepBuildUnbuildPrintData(String date, String documentNumber, String customerName, String referenceNumber, 
            String description, String createdBy, String itemName, String itemDesc, String itemCategory, String itemNameArray, String itemDescArray, String locationName,
            String unitMeasureShortName, Double buildQuantity, String buildQuantityArray, Double remaining, Double received,
            Double buildSpecificGravity , Double buildStandardFillSize,
            
            String bomItemCode, String bomItemName, String bomUOM, double bomQuantityNeeded, double bomQuantityOnHand, double bomUnpostedQuantity, double bomCost, String bomCategory,
            String bomQualityControl1 , String bomQualityControl2 , String bomQualityControlOthers ,
            double bomQualityControl1Remaining , double bomQualityControl2Remaining , double bomQualityControlOthersRemaining
    		
    		
    		){
                
        this.date = date;
        this.documentNumber = documentNumber;
        this.customerName = customerName;
        this.referenceNumber = referenceNumber;
        this.description = description;
        this.createdBy = createdBy;
        this.itemName = itemName;
        this.itemDesc = itemDesc;
        this.itemCategory = itemCategory;
        this.itemNameArray = itemNameArray;
        this.itemDescArray = itemDescArray;
        this.locationName = locationName;
        this.unitMeasureShortName = unitMeasureShortName;
        this.buildQuantity = buildQuantity;
        this.buildQuantityArray = buildQuantityArray;
        this.remaining = remaining;
        this.received = received;
        this.buildSpecificGravity = buildSpecificGravity;
        this.buildStandardFillSize = buildStandardFillSize;
        
        this.bomItemCode = bomItemCode;
        this.bomItemName = bomItemName;
        this.bomUOM = bomUOM;
        this.bomQuantityNeeded = bomQuantityNeeded;
        this.bomQuantityOnHand = bomQuantityOnHand;
        this.bomUnpostedQuantity = bomUnpostedQuantity;
        this.bomCost = bomCost;
        this.bomCategory = bomCategory;
        
        this.bomQualityControl1 = bomQualityControl1;
        this.bomQualityControl2 = bomQualityControl2;
        this.bomQualityControlOthers = bomQualityControlOthers;
        this.bomQualityControl1Remaining = bomQualityControl1Remaining;
        this.bomQualityControl2Remaining = bomQualityControl2Remaining;
        this.bomQualityControlOthersRemaining = bomQualityControlOthersRemaining;
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public String getDocumentNumber() {
        
        return documentNumber;
        
    }
    
    public String getCustomerName() {
        
        return customerName;
        
    }
    public String getReferenceNumber() {
        
        return referenceNumber;
        
    }
    public String getDescription() {
        
        return description;
        
    }
   public String getCreatedBy() {
        
        return createdBy;
        
    }
   public String getItemName() {
       
       return itemName;
       
   }
   public String getItemDesc() {
       
       return itemDesc;
       
   }
   
   public String getItemCategory() {
       
       return itemCategory;
       
   }
   
   public String getItemNameArray() {
       
       return itemNameArray;
       
   }
   public String getItemDescArray() {
       
       return itemDescArray;
       
   }
   public String getLocationName() {
       
       return locationName;
       
   }
   public String getUnitMeasureShortName() {
       
       return unitMeasureShortName;
       
   }

    public Double getBuildQuantity() {
        
        return buildQuantity;
        
    }
    
    public String getBuildQuantityArray() {
        
        return buildQuantityArray;
        
    }
    
	public Double getRemaining() {
	        
        return remaining;
        
    }
	
	public Double getReceived() {
	    
	    return received;
	    
	}
    
    public Double getBuildSpecificGravity() {
        
        return buildSpecificGravity;
        
    }

	public Double getBuildStandardFillSize() {
	    
	    return buildStandardFillSize;
	    
	}
    
    public String getBomItemCode() {
        
        return bomItemCode;
        
    }
    
    public String getBomItemName() {
        
        return bomItemName;
        
    }
    
    public Double getBomQuantityNeeded() {
        
        return bomQuantityNeeded;
        
    }
    
    public Double getBomQuantityOnHand() {
        
        return bomQuantityOnHand;
        
    }
    
public Double getBomUnpostedQuantity() {
        
        return bomUnpostedQuantity;
        
    }
    public Double getBomCost() {
        
        return bomCost;
        
    }
    
    public String getBomUOM() {
        
        return bomUOM;
        
    }

    public String getBomCategory() {
        
        return bomCategory;
        
    }
    
   public String getBomQualityControl1() {
        
        return bomQualityControl1;
        
    }
    
   public String getBomQualityControl2() {
        
        return bomQualityControl2;
        
    }
   
   public String getBomQualityControlOthers() {
       
       return bomQualityControlOthers;
       
   }

   public Double getBomQualityControl1Remaining() {
    
	   return bomQualityControl1Remaining;
    
   }

   public Double getBomQualityControl2Remaining() {
    
	   return bomQualityControl2Remaining;
    
   }

   public Double getBomQualityControlOthersRemaining() {
	    
	   return bomQualityControlOthersRemaining;
    
   }

    
  
}
