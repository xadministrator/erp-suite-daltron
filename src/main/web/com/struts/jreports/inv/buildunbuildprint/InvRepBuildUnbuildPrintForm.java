package com.struts.jreports.inv.buildunbuildprint;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class InvRepBuildUnbuildPrintForm extends ActionForm implements Serializable {

   private String userPermission = new String();
   
   
   
   private String reportType = "";

	
	
   public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}


    
   public String getUserPermission() {

      return userPermission;

   } 

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }   

   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  
   }       
}