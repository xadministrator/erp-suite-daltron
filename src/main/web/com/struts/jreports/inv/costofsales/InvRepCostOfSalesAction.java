package com.struts.jreports.inv.costofsales;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepCostOfSalesController;
import com.ejb.txn.InvRepCostOfSalesControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepCostOfSalesAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepCostOfSalesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         InvRepCostOfSalesForm actionForm = (InvRepCostOfSalesForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.INV_REP_COST_OF_SALES_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepCostOfSales");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepCostOfSalesController EJB
*******************************************************/

         InvRepCostOfSalesControllerHome homeCS = null;
         InvRepCostOfSalesController ejbCS = null;       

         try {
         	
            homeCS = (InvRepCostOfSalesControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepCostOfSalesControllerEJB", InvRepCostOfSalesControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepCostOfSalesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbCS = homeCS.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepCostOfSalesAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- INV RIC Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
         	ArrayList list = null;
            
            String company = null;                        

            // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getSalesperson())) {
	        		
	        		criteria.put("salesperson", actionForm.getSalesperson());
	        	}
       
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
			//get all InvRepCostOfSalesBranchLists
			
			ArrayList branchList = new ArrayList();
			
			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {
				
				InvRepCostOfSalesBranchList brList = (InvRepCostOfSalesBranchList)actionForm.getInvBrIlListByIndex(i);
				
				if(brList.getBranchCheckbox() == true) {
				
					AdBranchDetails mdetails = new AdBranchDetails();
					mdetails.setBrAdCompany(user.getCmpCode());
					mdetails.setBrCode(brList.getBranchCode());
					branchList.add(mdetails);
					
				}
				
			}
			
			// get selected categories
			ArrayList categorySelectedList = new ArrayList();
			System.out.println("get selected price levels");
			for (int i=0; i < actionForm.getCategorySelectedList().length; i++) {
				
				System.out.println("actionForm.getCategorySelectedList()[i]" + actionForm.getCategorySelectedList()[i]);
				categorySelectedList.add(actionForm.getCategorySelectedList()[i]);
				
			}
                        	
			try {
				
				// get company
				
				AdCompanyDetails adCmpDetails = ejbCS.getAdCompany(user.getCmpCode());
				company = adCmpDetails.getCmpName();
				
				
				
				// execute report
				list = ejbCS.executeInvRepCostOfSales(actionForm.getCriteria(), actionForm.getCostingMethod(), categorySelectedList, branchList, user.getCmpCode());
				
			} catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("costOfSales.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepCostOfSalesAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepCostOfSales"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDateTo());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));		    
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("salesperson", actionForm.getSalesperson());
		    
		    System.out.println("actionForm.getShowEntries()="+actionForm.getShowEntries());
		    
		    if (actionForm.getShowEntries()) {
      			parameters.put("showEntries", "YES");
      		} else {
      			parameters.put("showEntries", "NO");
      		}
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepCostOfSalesBranchList brList = (InvRepCostOfSalesBranchList)actionForm.getInvBrIlListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);

		    String filename = null;

		    if(actionForm.getReportType().equals("SALES MARGIN")){
 		    	
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepCostOfSalesMargin.jasper"; 
			    
			    if (!new java.io.File(filename).exists()) {
			    	
			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepCostOfSalesMargin.jasper");
			    	
			    }

		    } else {
		    	
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepCostOfSales.jasper";
			       
			    if (!new java.io.File(filename).exists()) {
			    		    		    
			       filename = servlet.getServletContext().getRealPath("jreports/InvRepCostOfSales.jasper");
				    
			    }

			}
		    
		    
		    
		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepCostOfSalesDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new InvRepCostOfSalesDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new InvRepCostOfSalesDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepCostOfSalesAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV RIC Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV RIC Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
	         		
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCategoryList();           	
	            	
	            	list = ejbCS.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearReportTypeList();
			        
			        list = ejbCS.getAdLvReportTypeAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            	
	            		actionForm.setReportTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setReportTypeList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearLocationList();           	
	            	
	            	list = ejbCS.getInvLocAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setLocationList((String)i.next());
	            			
	            		}
	            		            		
	            	}

					actionForm.clearInvBrIlList();
					
					list = ejbCS.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 
					//boolean isFirst = true;
					i = list.iterator();
					
					while(i.hasNext()) {
						
						AdBranchDetails details = (AdBranchDetails)i.next();
						
						InvRepCostOfSalesBranchList invBrIlList = new InvRepCostOfSalesBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						
						if (details.getBrHeadQuarter() == 1) {
							invBrIlList.setBranchCheckbox(true);
							//isFirst = false;
						}
						
						actionForm.saveInvBrIlList(invBrIlList);
						
					}	
					
					
					actionForm.clearSalespersonList();           	
	            	
	            	list = ejbCS.getArSlpAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSalespersonList((String)i.next());
	            			
	            		}
	            		
	            	}
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in InvRepCostOfSalesAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("invRepCostOfSales"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepCostOfSalesAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
