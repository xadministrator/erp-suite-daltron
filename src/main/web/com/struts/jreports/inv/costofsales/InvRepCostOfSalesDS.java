package com.struts.jreports.inv.costofsales;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepCostOfSaleDetails;

public class InvRepCostOfSalesDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	int ctr=1;
	
	public InvRepCostOfSalesDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			InvRepCostOfSaleDetails details = (InvRepCostOfSaleDetails)i.next();
			
			InvRepCostOfSalesData dtbData = new InvRepCostOfSalesData(details.getCsItemName(), details.getCsItemDescription(),
					Common.convertSQLDateToString(details.getCsDate()), details.getCsDocumentNumber(),
				    new Double(details.getCsQuantitySold()), new Double(details.getCsUnitCost()),
					new Double(details.getCsCostOfSales()), details.getCsUnit(), details.getCsCustomerCode(),
					details.getCsBranchCode(), new Double(details.getCsStockOnHand()), details.getCsSalespersonName(),
					new Double(details.getCsSalesAmount()), new Double(details.getCsGrossProfit()),
					new Double(details.getCsPercentProfit())
					);
			
			
			System.out.println("===============");
			System.out.println("Detail # : " + ctr);
			System.out.println("Doc Num  : " + details.getCsDocumentNumber());
			System.out.println("Qty Sold : " + details.getCsQuantitySold());
			System.out.println("Unit Cost : " + details.getCsUnitCost());
			System.out.println("Cost of Sales : " + details.getCsCostOfSales());
			System.out.println("Sales Amount : " + details.getCsSalesAmount());
			System.out.println("Gross Profit : " + details.getCsGrossProfit());
			System.out.println("Percent Profit : " + details.getCsPercentProfit());
			
			ctr++;
			
			data.add(dtbData);
			
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("itemName".equals(fieldName)) {
			value = ((InvRepCostOfSalesData)data.get(index)).getItemName();
		} else if("itemDescription".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getItemDescription();
		} else if("date".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getDate();	
		} else if("documentNumber".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getDocumentNumber();
		} else if("quantitySold".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getQuantitySold();
		} else if("unitCost".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getUnitCost();	
		} else if("costOfSales".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getCostOfSales();		
		} else if("unit".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getUnit();
		} else if("customerCode".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getCustomerCode();
		} else if("branchCode".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getBranchCode();
		} else if("stockOnHand".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getStockOnHand();
		} else if("salesperson".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getSalesperson();
		} else if("salesAmount".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getSalesAmount();
		} else if("grossProfit".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getGrossProfit();
		} else if("percentProfit".equals(fieldName)) {	
			value = ((InvRepCostOfSalesData)data.get(index)).getPercentProfit();
		}
		return(value);
		
	}
	
}
