package com.struts.jreports.inv.costofsales;

public class InvRepCostOfSalesData implements java.io.Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String date = null;
	private String documentNumber = null;
	private Double quantitySold = null;
	private Double unitCost = null;
	private Double costOfSales = null;
	private String unit = null;
	private String customerCode = null;
	private String branchCode = null;
	private Double stockOnHand = null;
	private String salesperson = null;
	
	private Double salesAmount = null;
	private Double grossProfit = null;
	private Double percentProfit = null;

	
	public InvRepCostOfSalesData(String itemName, String itemDescription, String date, String documentNumber,
			Double quantitySold, Double unitCost, Double costOfSales, String unit, String customerCode,
			String branchCode, Double stockOnHand, String salesperson,
			Double salesAmount, Double grossProfit, Double percentProfit) {
			
			this.itemName = itemName;
			this.itemDescription = itemDescription;
			this.date = date;
			this.documentNumber = documentNumber;
			this.quantitySold = quantitySold;
			this.unitCost = unitCost;
			this.costOfSales = costOfSales;
			this.unit = unit;
			this.customerCode = customerCode;
			this.branchCode = branchCode;
			this.stockOnHand = stockOnHand;
			this.salesperson = salesperson;
			this.salesAmount = salesAmount;
			this.grossProfit = grossProfit;
			this.percentProfit = percentProfit;
			
	}
	
	public Double getCostOfSales() {
		
		return costOfSales;
	
	}
	
	public String getDate() {
	
		return date;
	
	}
	
	public String getDocumentNumber() {
	
		return documentNumber;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public Double getQuantitySold() {
	
		return quantitySold;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public String getUnit() {
		
		return unit;
	
	}
	
	public String getCustomerCode() {
		
		return customerCode;
	
	}
	
	public String getBranchCode() {
		
		return branchCode;
	
	}
	
	public Double getStockOnHand() {
		
		return stockOnHand;
	
	}
	
	
	public String getSalesperson() {
		
		return salesperson;
	
	}
	
	public Double getSalesAmount() {
		
		return salesAmount;
		
	}
	
	public Double getGrossProfit() {
		
		return grossProfit;
		
	}
	
	public Double getPercentProfit() {
		
		return percentProfit;
		
	}

}
