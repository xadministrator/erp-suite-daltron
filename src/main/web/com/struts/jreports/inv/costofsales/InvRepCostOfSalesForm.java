package com.struts.jreports.inv.costofsales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepCostOfSalesForm extends ActionForm implements Serializable{
	
	private String itemName = null;
	private String[] categorySelectedList = null;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String costingMethod = null;
	private ArrayList costingMethodList = new ArrayList();
	private String salesperson = null;
	private ArrayList salespersonList = new ArrayList();
	private String date = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private ArrayList invBrIlList = new ArrayList();
	private boolean showEntries = false;
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public boolean getShowEntries() {
	   	
	   	  return showEntries;
	   	
	   }
	   
	   public void setShowEntries(boolean showEntries) {
	   	
	   	  this.showEntries = showEntries;
	   	
	   }
	   
	   public String getReportType() {

			return(reportType);

		}

		public void setReportType(String reportType) {

			this.reportType = reportType;

		}

		public ArrayList getReportTypeList() {

			return(reportTypeList);

		}

		public void setReportTypeList(String reportType) {

			reportTypeList.add(reportType);

		}

		public void clearReportTypeList() {

			reportTypeList.clear();
			reportTypeList.add(Constants.GLOBAL_BLANK);

		}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName (String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getDateFrom() {
		
		return (dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return (dateTo);
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String[] getCategorySelectedList() {
		
		return(categorySelectedList);
		
	}
	
	public void setCategorySelectedList(String[] categorySelectedList) {
		
		this.categorySelectedList = categorySelectedList;
		
	}
	
	public ArrayList getCategoryList() {
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation() {
		
		return(location);
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return(locationList);
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public ArrayList getItemClassList() {
		
		return itemClassList;
		
	}
	
	public String getCostingMethod() {
		
		return costingMethod;
		
	}
	
	public void setCostingMethod(String costingMethod) {
		
		this.costingMethod = costingMethod;
		
	}
	
	public ArrayList getCostingMethodList() {
		
		return costingMethodList;
		
	}
	
	public String getViewType() {
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepCostOfSalesBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepCostOfSalesBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	public String getSalesperson(){
		return(salesperson);
	}

	public void setSalesperson(String salesperson){
		this.salesperson = salesperson;
	}

	public ArrayList getSalespersonList(){
		return(salespersonList);
	}

	public void setSalespersonList(String salesperson){
		salespersonList.add(salesperson);
	}

	public void clearSalespersonList(){
		salespersonList.clear();
		salespersonList.add(Constants.GLOBAL_BLANK);
	}

	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepCostOfSalesBranchList list = (InvRepCostOfSalesBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		categorySelectedList = new String[0];
		location = Constants.GLOBAL_BLANK;
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;
		costingMethodList.clear();		
		costingMethodList.add("Average");
		costingMethodList.add("Standard");
		costingMethod = "Average";
		dateFrom = Common.convertSQLDateToString(new Date());
		dateTo = Common.convertSQLDateToString(new Date());
		itemName = null;
		showEntries = false;
		salesperson = null;

	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
						
			if(!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("costOfSales.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("costOfSales.error.dateToInvalid"));
				
			}
			
		}
		
		return(errors);
		
	}
	
}
