package com.struts.jreports.inv.countvariance;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepCountVarianceDetails;

public class InvRepCountVarianceDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepCountVarianceDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         InvRepCountVarianceDetails details = (InvRepCountVarianceDetails)i.next();
                  
	     InvRepCountVarianceData dtbData = new InvRepCountVarianceData(
		    details.getCvItemName(), new Double(details.getCvBegInventory()),
		    new Double(details.getCvDeliveries()), new Double(details.getCvAdjustQuantity()), 
		    new Double(details.getCvStandardUsage()), new Double(details.getCvEndInventory()),
			new Double(details.getCvPhysicalInventory()), new Double(details.getCvWastage()),
			new Double(details.getCvVariance()));
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("rawMaterial".equals(fieldName)) {
      	
          value = ((InvRepCountVarianceData)data.get(index)).getRawMaterial();
      
      } else if("begInventory".equals(fieldName)) {
      
          value = ((InvRepCountVarianceData)data.get(index)).getBegInventory();
      
      } else if("deliveries".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getDeliveries();
          
      } else if("adjustQuantity".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getAdjustQuantity();
          
      } else if("standard".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getStandard();
      
      } else if("endInventory".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getEndInventory();

	  } else if("physicalInventory".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getPhysicalInventory();

	  } else if("wastage".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getWastage();

	  } else if("countVariance".equals(fieldName)){
      
          value = ((InvRepCountVarianceData)data.get(index)).getCountVariance();
      
      }

      return(value);
   }
}
