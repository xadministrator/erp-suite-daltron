package com.struts.jreports.inv.countvariance;

public class InvRepCountVarianceData implements java.io.Serializable {
	
   private String rawMaterial = null;
   private Double begInventory = null;
   private Double deliveries = null;
   private Double adjustQuantity = null;
   private Double standard = null;
   private Double endInventory = null;
   private Double physicalInventory = null;
   private Double wastage = null;  
   private Double countVariance = null;   
   
   public InvRepCountVarianceData(String rawMaterial, Double begInventory, Double deliveries, Double adjustQuantity, 
   	  Double standard, Double endInventory, Double physicalInventory, Double wastage, Double countVariance) {
      	
      this.rawMaterial = rawMaterial;
      this.begInventory = begInventory;
      this.deliveries = deliveries;
      this.adjustQuantity = adjustQuantity;
      this.standard = standard;
      this.endInventory = endInventory;
      this.physicalInventory = physicalInventory; 
      this.wastage = wastage;
      this.countVariance = countVariance;
      
   }

   public String getRawMaterial() {
   	 
      return(rawMaterial);
   
   }

   public Double getBegInventory() {

      return begInventory;

   }

   public Double getDeliveries() {

      return deliveries;

   }
   
   public Double getAdjustQuantity() {

      return adjustQuantity;

   }
   
   public Double getStandard() {

      return standard;

   }

   public Double getEndInventory() {

      return endInventory;

   }

   public Double getPhysicalInventory() {

      return physicalInventory;

   }
   
   public Double getWastage() {
   	
   	  return wastage;
   	
   }
   
   public Double getCountVariance() {

      return countVariance;

   }

}
