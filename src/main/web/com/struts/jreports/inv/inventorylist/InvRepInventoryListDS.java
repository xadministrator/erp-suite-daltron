package com.struts.jreports.inv.inventorylist;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvPriceLevelDetails;
import com.util.InvRepInventoryListDetails;

public class InvRepInventoryListDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepInventoryListDS(ArrayList list, boolean includePriceLevel) {
	   System.out.println("mark");
	  Iterator i = list.iterator();
      
      while (i.hasNext()) {
    	   System.out.println("enter 1");
	         InvRepInventoryListDetails details = (InvRepInventoryListDetails)i.next();
	    //     if(details.getRilDlItemCategory() == null) {  System.out.println("really?"); continue;}
	         if(!includePriceLevel) {
	        	 System.out.println("enter 2");
	        	 InvRepInventoryListData argData = new InvRepInventoryListData(details.getRilItemName(),
		 	         		details.getRilItemDescription(), details.getRilLocation(), details.getRilItemClass(), details.getRilUnit(),
		 					new Double(details.getRilQuantity()), new Double(details.getRilUnitCost()), new Double(details.getRilAmount()),
		 					new Double(details.getRilSalesPrice()), new Double(details.getRilAverageCost()), details.getRilIiPartNumber(), 
		 					details.getRilItemCategory(), details.getRilExpiryDate(), details.getRilPropertyCode(), details.getRilSerialNumber(),
		 					details.getRilSpecs(), details.getRilCustodian(), 
		 					details.getRilDlItemCategory(), details.getRilDlItemName(), 
		 					details.getRilDlPropertyCode(), details.getRilDlSerialNumber(), details.getRilDlSpecs(), details.getRilDlCustodian(),
		 					details.getRilDlDate(), details.getRilDlAcquisitionCost(), details.getRilDlAmount(), details.getRilDlMonthLifeSpan(), details.getRilDlCurrentBalance()
	        			 );
	        	 
	        	 data.add(argData);
	        	 
	         } else {
	        	 System.out.println("enter other 1");
	        	 Iterator iter = details.getRilPriceLevels().iterator();
	        	 
	        	 while(iter.hasNext()){
	        		 System.out.println("enter other 2");
		        	 InvPriceLevelDetails pDetails = (InvPriceLevelDetails)iter.next();
		        	 
		        	 InvRepInventoryListData argData = new InvRepInventoryListData(details.getRilItemName(),
		 	         		details.getRilItemDescription(), details.getRilLocation(), details.getRilItemClass(), details.getRilUnit(),
		 					new Double(details.getRilQuantity()), new Double(details.getRilUnitCost()), new Double(details.getRilAmount()),
		 					new Double(details.getRilSalesPrice()), new Double(details.getRilAverageCost()), details.getRilIiPartNumber(), 
		 					details.getRilItemCategory(), details.getRilExpiryDate(), details.getRilPropertyCode(), details.getRilSerialNumber(),
		 					details.getRilSpecs(), details.getRilCustodian(), 
		 					details.getRilDlItemCategory(), details.getRilDlItemName(), 
		 					details.getRilDlPropertyCode(), details.getRilDlSerialNumber(), details.getRilDlSpecs(), details.getRilDlCustodian(),
		 					details.getRilDlDate(), details.getRilDlAcquisitionCost(), details.getRilDlAmount(), details.getRilDlMonthLifeSpan(), details.getRilDlCurrentBalance());
		 	         
		        	 argData.setPriceLevelName(pDetails.getPlAdLvPriceLevel());
		        	 argData.setPriceLevelAmount(new Double(pDetails.getPlAmount()));
		        	 
		 	         data.add(argData);
		        	 
		         }
	        	 
	         }
	         
	         
	         
	         
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getItemName();
   		System.out.println("value="+value);
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getLocation();
   		
   	} else if("itemClass".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getItemClass();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getUnit();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getQuantity();
   		
	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getUnitCost();
   		
	} else if("amount".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getAmount();
   		
	} else if("salesPrice".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getSalesPrice();
   		
	} else if("aveCost".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getAverageCost();
   		
	} else if("partNumber".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getPartNumber();
   		
	} else if("priceLevelName".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getPriceLevelName();
   		
	} else if("priceLevelAmount".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getPriceLevelAmount();
   		
	} else if("itemCategory".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getItemCategory();
   		System.out.println("itemCategory="+value);
   		
	}else if("expiryDate".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getExpiryDate();
   		
	}else if("propertyCode".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getPropertyCode();
   		
	}else if("serialNumber".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getSerialNumber();
   		
	}else if("specs".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getSpecs();
   		
	}else if("custodian".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getCustodian();
   		
	} else if("depItemCategory".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepItemCategory();
   		
   		
	} else if("depItemName".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepItemName();
   		
	} else if("depPropertyCode".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepPropertyCode();
   		
	} else if("depSerialNumber".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepSerialNumber();
   		
	} else if("depSpecs".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepSpecs();
   		
	} else if("depCustodian".equals(fieldName)) {
		
		value = ((InvRepInventoryListData)data.get(index)).getDepCustodian();
   		
   		

	}else if("depreciationDate".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepreciationDate();
   		System.out.println("depreciationDate="+value);
   		
	}else if("acquisitionCost".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getAcquisitionCost();
   		
	}else if("depreciationAmount".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getDepreciationAmount();
   		
	}else if("monthLifeSpan".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getMonthLifeSpan();
   		
	}else if("currentBalance".equals(fieldName)) {
   		
   		value = ((InvRepInventoryListData)data.get(index)).getCurrentBalance();
   		
	}
   	
   	
   	
   	
   	
   	
   	
   	
   	
   	
   	
   	return(value);
   	
   }
   
}
