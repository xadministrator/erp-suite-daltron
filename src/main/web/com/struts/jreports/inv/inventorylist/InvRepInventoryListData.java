package com.struts.jreports.inv.inventorylist;

import java.util.ArrayList;


public class InvRepInventoryListData implements java.io.Serializable {

	private String itemName;
	private String itemDescription;
	private String location;
	private String itemClass;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;
	private Double salesPrice;
	private Double averageCost;
	private String partNumber;	
	private String priceLevelName;
	private Double priceLevelAmount;
	private String itemCategory;
	private String expiryDate;
	
	private String propertyCode;
	private String serialNumber;
	private String specs;
	private String custodian;
	
	private String depItemCategory;
	private String depItemName;
	private String depPropertyCode;
	private String depSerialNumber;
	private String depSpecs;
	private String depCustodian;
	private String depreciationDate;
	private Double acquisitionCost;
	private Double depreciationAmount;
	private Double monthLifeSpan;
	private Double currentBalance;
	
	
	public InvRepInventoryListData( String itemName, String itemDescription, String location,
			String itemClass, String unit, Double quantity, Double unitCost, Double amount, 
			Double salesPrice, Double averageCost, String partNumber, String itemCategory, String expiryDate,
			String propertyCode, String serialNumber, String specs, String custodian, 
			String depItemCategory, String depItemName, 
			String depPropertyCode, String depSerialNumber, String depSpecs, String depCustodian,
			String depreciationDate, Double acquisitionCost, Double depreciationAmount, Double monthLifeSpan, Double currentBalance
			
			) {
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.itemClass = itemClass;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.salesPrice = salesPrice;
		this.averageCost = averageCost;
		this.partNumber = partNumber;
		this.itemCategory = itemCategory;
		this.expiryDate = expiryDate;
		this.propertyCode = propertyCode;
		this.serialNumber = serialNumber;
		this.specs = specs;
		this.custodian = custodian;
		
		this.depItemCategory = depItemCategory;
		this.depItemName = depItemName;
		this.depPropertyCode = depPropertyCode;
		this.depSerialNumber = depSerialNumber;
		this.depSpecs = depSpecs;
		this.depCustodian = depCustodian;
		this.depreciationDate = depreciationDate;
		this.acquisitionCost = acquisitionCost;
		this.depreciationAmount = depreciationAmount;
		this.monthLifeSpan = monthLifeSpan;
		this.currentBalance = currentBalance;
		
		
	}
	
	
	public String getDepreciationDate() {
		
		return depreciationDate;
		
	}
	
	public void setDepreciationDate(String depreciationDate) {
		
		this.depreciationDate = depreciationDate;
		
	}
	
	public Double getAcquisitionCost() {
		
		return acquisitionCost;
	
	}
	
	public void setAcquisitionCost(Double acquisitionCost) {
	
		this.acquisitionCost = acquisitionCost;
	
	}
	
	public Double getDepreciationAmount() {
		
		return depreciationAmount;
	
	}
	
	public void setDepreciationAmount(Double depreciationAmount) {
	
		this.depreciationAmount = depreciationAmount;
	
	}
	
	
	public Double getMonthLifeSpan() {
		
		return monthLifeSpan;
	
	}
	
	public void setMonthLifeSpan(Double monthLifeSpan) {
	
		this.monthLifeSpan = monthLifeSpan;
	
	}
	
	public Double getCurrentBalance() {
		
		return currentBalance;
	
	}
	
	public void setCurrentBalance(Double currentBalance) {
	
		this.currentBalance = currentBalance;
	
	}
	

	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}

	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDescription) {
	
		this.itemDescription = itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public String getUnit() {
		
		return unit;
	
	}
	
	public void setUnit(String unit) {
	
		this.unit = unit;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public void setLocation(String location) {
	
		this.location = location;
	
	}
	
	public Double getUnitCost() {
		
		return unitCost;
		
	}
		
	public void setUnitCost(Double unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
		
	public void setAmount(Double amount) {
		
		this.amount = amount;
		
	}
	
	public Double getSalesPrice() {
		
		return salesPrice;
		
	}
	
	public void setSalesPrice(Double salesPrice) {
		
		this.salesPrice = salesPrice;
		
	}
	
	public Double getAverageCost() {
		
		return averageCost;
		
	}
	
	public void setAverageCost(Double averageCost) {
		
		this.averageCost = averageCost;
		
	}
	
	public String getPartNumber() {
		
		return partNumber;
		
	}
	
	public void setPartNumber(String partNumber) {
		
		this.partNumber = partNumber;
		
	}
	
	public String getPriceLevelName(){
	
		return priceLevelName;
		
	}
	
	public void setPriceLevelName(String priceLevelName){
	
		this.priceLevelName = priceLevelName;
		
	}
	
	public Double getPriceLevelAmount(){
		
		return priceLevelAmount;
		
	}
	
	public void setPriceLevelAmount(Double priceLevelAmount){
	
		this.priceLevelAmount = priceLevelAmount;
		
	}

	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
		System.out.println("expiryDate1: " + expiryDate);
	}

	public String getPropertyCode() {
		return propertyCode;
	}

	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	public String getCustodian() {
		return custodian;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}
	
	
	
	public String getDepItemCategory() {
		return depItemCategory;
	}

	public void setDepItemCategory(String depItemCategory) {
		this.depItemCategory = depItemCategory;
	}


	public String getDepItemName() {
		
		return depItemName;
		
	}

	public void setDepItemName(String depItemName) {
		
		this.depItemName = depItemName;
		
	}
	
	
	public String getDepPropertyCode() {
		
		return depPropertyCode;
		
	}

	public void setDepPropertyCode(String depPropertyCode) {
		
		this.depPropertyCode = depPropertyCode;
		
	}
	
	public String getDepSerialNumber() {
		
		return depSerialNumber;
		
	}

	public void setDepSerialNumber(String depSerialNumber) {
		
		this.depSerialNumber = depSerialNumber;
		
	}
	
	
	public String getDepSpecs() {
		
		return depSpecs;
		
	}

	public void setDepSpecs(String depSpecs) {
		
		this.depSpecs = depSpecs;
		
	}
	
	
	public String getDepCustodian() {
		
		return depCustodian;
		
	}

	public void setDepCustodian(String depCustodian) {
		
		this.depCustodian = depCustodian;
		
	}
} // InvRepInventoryListData class
