package com.struts.jreports.inv.inventoryprofitability;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepInventoryProfitabilityController;
import com.ejb.txn.InvRepInventoryProfitabilityControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepInventoryProfitabilityAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepInventoryProfitabilityAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         InvRepInventoryProfitabilityForm actionForm = (InvRepInventoryProfitabilityForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.INV_REP_INVENTORY_PROFITABILITY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepInventoryProfitability");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepInventoryProfitabilityController EJB
*******************************************************/

         InvRepInventoryProfitabilityControllerHome homeIP = null;
         InvRepInventoryProfitabilityController ejbIP = null;       

         try {
         	
            homeIP = (InvRepInventoryProfitabilityControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepInventoryProfitabilityControllerEJB", InvRepInventoryProfitabilityControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepInventoryProfitabilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbIP = homeIP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepInventoryProfitabilityAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
	  Check enabled shift
*******************************************************/
	 
	 boolean enableShift = false;
	 
	 try {
	 	
	 	enableShift = Common.convertByteToBoolean(ejbIP.getAdPrfEnableInvShift(user.getCmpCode()));
	 	
	 } catch(EJBException ex) {
	 	
	 	if (log.isInfoEnabled()) {
	 		
	 		log.info("EJBException caught in InvRepInventoryProfitabilityAction.execute(): " + ex.getMessage() +
	 				" session: " + session.getId());
	 	}
	 	
	 	return(mapping.findForward("cmnErrorPage"));
	 	
	 }
	 
/*******************************************************
   -- INV PROFIT Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
         	ArrayList list = null;
            
            String company = null;                        

            // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();   
	        	
	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}
       
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            ArrayList branchList = new ArrayList();
			
			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {
				
				InvRepInventoryProfitabilityBranchList brList = (InvRepInventoryProfitabilityBranchList)actionForm.getInvBrIlListByIndex(i);
				
				if(brList.getBranchCheckbox() == true) {
				
					AdBranchDetails mdetails = new AdBranchDetails();
					mdetails.setBrAdCompany(user.getCmpCode());
					mdetails.setBrCode(brList.getBranchCode());
					branchList.add(mdetails);
					
				}
				
			}
                        	
		    try {
		    	
		       // get company
			       
			   AdCompanyDetails adCmpDetails = ejbIP.getAdCompany(user.getCmpCode());
			   company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbIP.executeInvRepInventoryProfitability(actionForm.getCriteria(), actionForm.getCostingMethod(), branchList, actionForm.getGroupBy(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("inventoryProfitability.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepInventoryProfitabilityAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepInventoryProfitability"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDateTo());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("costingMethod", actionForm.getCostingMethod());
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("groupBy", actionForm.getGroupBy());
		   
		    if (actionForm.getSummarize()) {

		    	parameters.put("summarize", "YES");

		    } else {

		    	parameters.put("summarize", "NO");

		    }
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j<actionForm.getInvBrIlListSize(); j++) {
				
				InvRepInventoryProfitabilityBranchList brList = (InvRepInventoryProfitabilityBranchList)actionForm.getInvBrIlListByIndex(j);
				
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
	    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepInventoryProfitability.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/InvRepInventoryProfitability.jasper");
	           
	        }	 
		    
		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepInventoryProfitabilityDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new InvRepInventoryProfitabilityDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new InvRepInventoryProfitabilityDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepInventoryProfitabilityAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV PROFIT Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV PROFIT Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        actionForm.clearCategoryList();           	
	            	
	            	list = ejbIP.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearLocationList();           	
	            	
	            	list = ejbIP.getInvLocAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setLocationList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearInvBrIlList();
					
					list = ejbIP.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
					
					//boolean isFirst = true;
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						AdBranchDetails details = (AdBranchDetails)i.next();
						
						InvRepInventoryProfitabilityBranchList invBrIlList = new InvRepInventoryProfitabilityBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						if (details.getBrHeadQuarter() == 1 ) {
							invBrIlList.setBranchCheckbox(true);
							//isFirst = false;
						}
						
						actionForm.saveInvBrIlList(invBrIlList);
						
					}	
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in InvRepInventoryProfitabilityAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	            
	            return(mapping.findForward("invRepInventoryProfitability"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepInventoryProfitabilityAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
