package com.struts.jreports.inv.inventoryprofitability;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepInventoryProfitabilityDetails;

public class InvRepInventoryProfitabilityDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepInventoryProfitabilityDS(ArrayList list, String groupBy) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			InvRepInventoryProfitabilityDetails details = (InvRepInventoryProfitabilityDetails)i.next();
						
			String group = new String("");

			if (groupBy.equals("CATEGORY")) {
				group = details.getIpItemAdLvCategory();
			}  
	         
			InvRepInventoryProfitabilityData dtbData = new InvRepInventoryProfitabilityData(details.getIpItemName(), 
					details.getIpItemDescription(), details.getIpUnitOfMeasure(), 
					new Double(details.getIpQuantitySold()), new Double(details.getIpUnitPriceCost()),
					new Double(details.getIpAmount()), details.getIpType(), details.getIpItemAdLvCategory(), group);
			
			data.add(dtbData);
									
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("itemName".equals(fieldName)) {
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getItemName();
		} else if("itemDescription".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getItemDescription();
		} else if("unitOfMeasure".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getUnitOfMeasure();
		} else if("quantitySold".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getQuantitySold();
		} else if("unitPriceCost".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getUnitPriceCost();	
		} else if("amount".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getAmount();
		} else if("type".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getType();		
		} else if("itemCategory".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getItemCategory();		
		} else if("groupBy".equals(fieldName)) {	
			value = ((InvRepInventoryProfitabilityData)data.get(index)).getGroupBy();		
		} 
		
		return(value);
		
	}
	
}
