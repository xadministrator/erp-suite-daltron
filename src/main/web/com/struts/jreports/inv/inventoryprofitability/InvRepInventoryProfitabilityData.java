package com.struts.jreports.inv.inventoryprofitability;

public class InvRepInventoryProfitabilityData implements java.io.Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String unitOfMeasure = null;
	private Double quantitySold = null;
	private Double unitPriceCost = null;
	private Double amount = null;
	private String type = null;
	private String itemCategory = null;
	private String groupBy = null;
	
	public InvRepInventoryProfitabilityData(String itemName, String itemDescription, String unitOfMeasure,
		Double quantitySold, Double unitPriceCost, Double amount, String type, String itemCategory,
		String groupBy) {
			
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.unitOfMeasure = unitOfMeasure;
		this.quantitySold = quantitySold;
		this.unitPriceCost = unitPriceCost;
		this.amount = amount;
		this.type = type;
		this.itemCategory = itemCategory;
		this.groupBy = groupBy;
			
	}

	public Double getAmount() {

		return amount;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public Double getQuantitySold() {
	
		return quantitySold;
	
	}
	
	public String getUnitOfMeasure() {
	
		return unitOfMeasure;
	
	}
	
	public Double getUnitPriceCost() {
	
		return unitPriceCost;
	
	}
	
	public String getType() {
		
		return type;
		
	}

	public String getItemCategory() {

		return itemCategory;

	}

	public String getGroupBy() {

		return groupBy;

	}
	
}
