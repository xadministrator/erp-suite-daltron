package com.struts.jreports.inv.itemcosting;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepItemCostingController;
import com.ejb.txn.InvRepItemCostingControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;

public final class InvRepItemCostingAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepItemCostingAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         InvRepItemCostingForm actionForm = (InvRepItemCostingForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.INV_REP_ITEM_COSTING_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepItemCosting");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepItemCostingController EJB
*******************************************************/

         InvRepItemCostingControllerHome homeRIC = null;
         InvRepItemCostingController ejbRIC = null;       

         try {
         	
            homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepItemCostingAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbRIC = homeRIC.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepItemCostingAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- INV RIC Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;                        

            
         
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();
	        	

	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}
	        		        
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}

			//get all InvRepItemCostingBranchList
			
			ArrayList branchList = new ArrayList();
			
			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {
				
				InvRepItemCostingBranchList brList = (InvRepItemCostingBranchList)actionForm.getInvBrIlListByIndex(i);
				
				if(brList.getBranchCheckbox() == true) {
				
					AdBranchDetails mdetails = new AdBranchDetails();
					mdetails.setBrAdCompany(user.getCmpCode());
					mdetails.setBrCode(brList.getBranchCode());
					branchList.add(mdetails);
					
				}
				
			}            
            
		    try {
		       
	    	   if(actionForm.getIncludeFixCosting()){
	            	
	            	ejbRIC.executeInvFixItemCosting(actionForm.getCriteria(), branchList, user.getCmpCode());
	            	
	            }
		    	
		    	
		    	
		       // execute report
		    		    
		       list = ejbRIC.executeInvRepItemCosting(actionForm.getCriteria(), actionForm.getLocation(), actionForm.getCategory(), actionForm.getShowCommittedQuantity(), 
		    		   actionForm.getIncludeUnposted(), actionForm.getShowZeroes(), branchList, user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("itemCosting.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepItemCosting"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("date", new Date());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    parameters.put("viewType", actionForm.getViewType());
		    	    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepItemCostingBranchList brList = (InvRepItemCostingBranchList)actionForm.getInvBrIlListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
		    
			if (actionForm.getShowCommittedQuantity()){
				
				parameters.put("showCommittedQuantity", "yes");
				
			} else {
				
				parameters.put("showCommittedQuantity", "no");
				
			}
			
			if (actionForm.getIncludeUnposted()){
				
				parameters.put("includeUnposted", "yes");
				
			} else {
				
				parameters.put("includeUnposted", "no");
				
			}
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepItemCosting.jasper";
		       
		    if (!new java.io.File(filename).exists()) {
		    		    		    
		       filename = servlet.getServletContext().getRealPath("jreports/InvRepItemCosting.jasper");
			    
		    }
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepItemCostingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new InvRepItemCostingDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new InvRepItemCostingDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV RIC Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV RIC Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
	         		actionForm.reset(mapping, request);
	         		
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCategoryList();           	
	            	
	            	list = ejbRIC.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearLocationList();           	
	            	
	            	list = ejbRIC.getInvLocAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setLocationList((String)i.next());
	            			
	            		}
	            		            		
	            	}
			       
					actionForm.clearInvBrIlList();
					
					list = ejbRIC.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 
					
					i = list.iterator();
					//int ctr = 1;
					while(i.hasNext()) {
						
						AdBranchDetails details = (AdBranchDetails)i.next();
						
						InvRepItemCostingBranchList invBrIlList = new InvRepItemCostingBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						if (details.getBrHeadQuarter() == 1) invBrIlList.setBranchCheckbox(true);
						//ctr++;
						
						actionForm.saveInvBrIlList(invBrIlList);
					
					}

				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            return(mapping.findForward("invRepItemCosting"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepItemCostingAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
