package com.struts.jreports.inv.itemcosting;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepItemCostingDetails;

public class InvRepItemCostingDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepItemCostingDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			InvRepItemCostingDetails details = (InvRepItemCostingDetails)i.next();
			
			InvRepItemCostingData dtbData = new InvRepItemCostingData(
					details.getIcItemName(), Common.convertSQLDateToString(details.getIcDate()), new Double(details.getIcQuantityReceived()),
					new Double(details.getIcItemCost()), new Double(details.getIcActualCost()), 
					new Double(details.getIcAdjustQuantity()), new Double(details.getIcAdjustCost()), 
					new Double(details.getIcAssemblyQuantity()), new Double(details.getIcAssemblyCost()),
					new Double(details.getIcQuantitySold()), new Double(details.getIcCostOfSales()),
					new Double(details.getIcRemainingQuantity()), new Double(details.getIcRemainingValue()),
					details.getIcSourceDocument(), details.getIcSourceDocumentNumber(),
					new Double(details.getIcBeginningQuantity()), new Double(details.getIcBeginningValue()),
					details.getIcItemCategory(), details.getIcItemUnitOfMeasure(), details.getIcReferenceNumber(),
					new Double(details.getIcBeginningUnitCost()), new Double(details.getIcOutQuantity()),
					new Double(details.getIcOutUnitCost()), new Double(details.getIcOutAmount()),
					new Double(details.getIcInQuantity()), new Double(details.getIcInUnitCost()),
					new Double(details.getIcInAmount()), details.getIcItemLocation(), details.getIcItemLocation1(),
					details.getIcItemCategory1());
			
			data.add(dtbData);
			
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("itemName".equals(fieldName)) {
			
			value = ((InvRepItemCostingData)data.get(index)).getItemName();
			
		} else if("date".equals(fieldName)) {
			
			value = ((InvRepItemCostingData)data.get(index)).getDate();
			
		} else if("quantityReceived".equals(fieldName)) {
			
			value = ((InvRepItemCostingData)data.get(index)).getQuantityReceived();
			
		} else if("itemCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getItemCost();
			
		} else if("actualCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getActualCost();
			
		} else if("adjustQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getAdjustQuantity();
			
		} else if("adjustCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getAdjustCost();
			
		} else if("assemblyQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getAssemblyQuantity();
			
		} else if("assemblyCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getAssemblyCost();
			
		} else if("quantitySold".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getQuantitySold();
			
		} else if("costOfSales".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getCostOfSales();
			
		} else if("remainingQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getRemainingQuantity();
			
		} else if("remainingValue".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getRemainingValue();
			
		} else if("sourceDocument".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getSourceDocument();
			
		} else if("sourceDocumentNumber".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getSourceDocumentNumber();
			
		} else if("beginningQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getBeginningQuantity();
			
		} else if("beginningValue".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getBeginningValue();
			
		} else if("itemCategory".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getItemCategory();
			
		} else if("itemCategory1".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getItemCategory1();
			
		} else if("uom".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getUnitOfMeasure();
			
		} else if("referenceNumber".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getReferenceNumber();
			
		} else if("beginningUnitCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getBeginningUnitCost();
			
		} else if("outQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getOutQuantity();
			
		} else if("outUnitCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getOutUnitCost();
			
		} else if("outAmount".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getOutAmount();
			
		} else if("inQuantity".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getInQuantity();
			
		} else if("inUnitCost".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getInUnitCost();
			
		} else if("inAmount".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getInAmount();
			
		} else if("location".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getLocation();
			
		} else if("location1".equals(fieldName)){
			
			value = ((InvRepItemCostingData)data.get(index)).getLocation1();
			
		}
		
		return(value);
		
	}
	
}
