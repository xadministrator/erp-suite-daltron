package com.struts.jreports.inv.itemcosting;

public class InvRepItemCostingData implements java.io.Serializable {
	
	private String itemName = null;
	private String date = null;
	private Double quantityReceived = null;
	private Double itemCost = null;
	private Double actualCost = null;
	private Double adjustQuantity = null;
	private Double adjustCost = null;
	private Double assemblyQuantity = null;
	private Double assemblyCost = null;
	private Double quantitySold = null;
	private Double costOfSales = null;
	private Double remainingQuantity = null;
	private Double remainingValue = null;
	private String sourceDocument = null;
	private String sourceDocumentNumber = null;
	private Double beginningQuantity = null;
	private Double beginningValue = null;
	private Double beginningUnitCost = null;
	private String itemCategory = null;
	private String itemCategory1 = null;
	private String uom = null;
	private String referenceNumber = null;
	private Double outQuantity = null;
	private Double outUnitCost = null;
	private Double outAmount = null;
	private Double inQuantity = null;
	private Double inUnitCost = null;
	private Double inAmount = null;
	private String location = null;
	private String location1 = null;
	
	public InvRepItemCostingData(String itemName, String date, Double quantityReceived, Double itemCost, Double actualCost,
		Double adjustQuantity, Double adjustCost, Double assemblyQuantity, Double assemblyCost, Double quantitySold,
		Double costOfSales, Double remainingQuantity, Double remainingValue, String sourceDocument, String sourceDocumentNumber,
		Double beginningQuantity, Double beginningValue, String itemCategory, String uom, String referenceNumber, Double beginningUnitCost,
		Double outQuantity, Double outUnitCost, Double outAmount, Double inQuantity, Double inUnitCost, Double inAmount, String location,
		String location1, String itemCategory1) {
		
		this.itemName = itemName;
		this.date = date;
		this.quantityReceived = quantityReceived;
		this.itemCost = itemCost;
		this.actualCost = actualCost;
		this.adjustQuantity = adjustQuantity;
		this.adjustCost = adjustCost;
		this.assemblyQuantity = assemblyQuantity;
		this.assemblyCost = assemblyCost;
		this.quantitySold = quantitySold;
		this.costOfSales = costOfSales;
		this.remainingQuantity = remainingQuantity;
		this.remainingValue = remainingValue;
		this.sourceDocument = sourceDocument;
		this.sourceDocumentNumber = sourceDocumentNumber;
		this.beginningQuantity = beginningQuantity;
		this.beginningValue = beginningValue;
		this.itemCategory = itemCategory;
		this.itemCategory1 = itemCategory1;
		this.uom = uom;
		this.referenceNumber = referenceNumber;
		this.beginningUnitCost = beginningUnitCost;
		this.outQuantity = outQuantity;
		this.outUnitCost = outUnitCost;
		this.outAmount = outAmount;
		this.inQuantity = inQuantity;
		this.inUnitCost = inUnitCost;
		this.inAmount = inAmount;
		this.location = location;
		this.location1 = location1;
	}
	
	public Double getActualCost() {
		
		return actualCost;
		
	}
	
	public Double getAdjustCost() {
		
		return adjustCost;
		
	}
	
	public Double getAdjustQuantity() {
		
		return adjustQuantity;
		
	}
	
	public Double getAssemblyCost() {
		
		return assemblyCost;
		
	}
	
	public Double getAssemblyQuantity() {
		
		return assemblyQuantity;
		
	}
	
	public Double getCostOfSales() {
		
		return costOfSales;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public Double getItemCost() {
		
		return itemCost;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public Double getQuantityReceived() {
		
		return quantityReceived;
		
	}
	
	public Double getQuantitySold() {
		
		return quantitySold;
		
	}
	
	public Double getRemainingQuantity() {
		
		return remainingQuantity;
		
	}
	
	public Double getRemainingValue() {
		
		return remainingValue;
		
	}
	
	public String getSourceDocument() {
		
		return sourceDocument;
		
	}
	
	public String getSourceDocumentNumber() {
		
		return sourceDocumentNumber;
		
	}
	
	public Double getBeginningQuantity() {
		
		return beginningQuantity;
		
	}
	
	public Double getBeginningValue() {
		
		return beginningValue;
		
	}
	
	public String getItemCategory() {
		
		return itemCategory;
		
	}
	
	public String getItemCategory1() {
		
		return itemCategory1;
		
	}
	
	public String getUnitOfMeasure() {
		
		return uom;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public Double getBeginningUnitCost() {
		
		return beginningUnitCost;
		
	}
	
	public Double getOutQuantity() {
		
		return outQuantity;
		
	}
	
	public Double getOutUnitCost() {
	
		return outUnitCost;
		
	}
	
	public Double getOutAmount() {
		
		return outAmount;
		
	}

	public Double getInQuantity() {
		
		return inQuantity;
		
	}

	public Double getInUnitCost() {
		
		return inUnitCost;
		
	}

	public Double getInAmount() {
		
		return inAmount;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public String getLocation1() {
		
		return location1;
		
	}
}
