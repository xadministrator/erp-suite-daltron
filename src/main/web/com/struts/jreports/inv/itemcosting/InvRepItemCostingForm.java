package com.struts.jreports.inv.itemcosting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepItemCostingForm extends ActionForm implements Serializable{
	
	private String itemName = null;
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String date = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private ArrayList invBrIlList = new ArrayList(); 
	private boolean showCommittedQuantity = false;	
	private boolean includeUnposted = false;
	
	private boolean showZeroes = false;
	private boolean includeFixCosting = false;
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getItemName() {
		
		return (itemName);
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;		
		
	}
	
	public String getDateFrom() {
		
		return (dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return (dateTo);
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getCategory() {
		
		return(category);
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation() {
		
		return(location);
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return(locationList);
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public ArrayList getItemClassList() {
		
		return itemClassList;
		
	}
	
	public String getViewType() {
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}

	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepItemCostingBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepItemCostingBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	public boolean getShowCommittedQuantity(){
		
		return(showCommittedQuantity);
		
	}
	
	public void setShowCommittedQuantity(boolean showCommittedQuantity){
		
		this.showCommittedQuantity = showCommittedQuantity;
		
	}
	
	public boolean getIncludeUnposted() {
		
		return includeUnposted;
	}
	
	public void setIncludeUnposted (boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public boolean getShowZeroes() {
		
		return showZeroes;
	}
	
	public void setShowZeroes (boolean showZeroes) {
		
		this.showZeroes = showZeroes;
		
	}
	
	public boolean getIncludeFixCosting() {
		
		return includeFixCosting;
	}
	
	public void setIncludeFixCosting (boolean includeFixCosting) {
		
		this.includeFixCosting = includeFixCosting;
		
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepItemCostingBranchList list = (InvRepItemCostingBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		category = Constants.GLOBAL_BLANK;
		location = Constants.GLOBAL_BLANK;
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = null;	
		itemName = null;
		showCommittedQuantity = false;
		includeUnposted = false;
		includeFixCosting = false;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("itemCosting.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("itemCosting.error.dateToInvalid"));
				
			}
			
			if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("location", new ActionMessage("itemCosting.error.locationRequired"));
				
			}
			
		}
		
		return(errors);
		
	}
	
}
