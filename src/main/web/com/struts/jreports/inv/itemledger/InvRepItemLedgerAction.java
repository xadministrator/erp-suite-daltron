package com.struts.jreports.inv.itemledger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepItemLedgerController;
import com.ejb.txn.InvRepItemLedgerControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepItemLedgerAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvRepItemLedgerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         InvRepItemLedgerForm actionForm = (InvRepItemLedgerForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.INV_REP_ITEM_LEDGER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepItemLedger");

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepItemLedgerController EJB
*******************************************************/

         InvRepItemLedgerControllerHome homeRSC = null;
         InvRepItemLedgerController ejbRIL = null;

         try {

            homeRSC = (InvRepItemLedgerControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepItemLedgerControllerEJB", InvRepItemLedgerControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in InvRepItemLedgerAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbRIL= homeRSC.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in InvRepItemLedgerAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- INV RIL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArrayList list = null;

            String company = null;

            // create criteria

            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();

	        	if (!Common.validateRequired(actionForm.getItemName())) {

	        		criteria.put("itemName", actionForm.getItemName());

	        	}
	        	if (!Common.validateRequired(actionForm.getItemNameTo())) {

	        		criteria.put("itemNameTo", actionForm.getItemNameTo());

	        	}

	        	if (!Common.validateRequired(actionForm.getCustomer())) {

	        		criteria.put("customerCode", actionForm.getCustomer());

	        	}

	        	if (!Common.validateRequired(actionForm.getSupplier())) {

	        		criteria.put("supplierCode", actionForm.getSupplier());

	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {

	        		criteria.put("category", actionForm.getCategory());

	        	}

	        	if (!Common.validateRequired(actionForm.getLocation())) {

	        		criteria.put("location", actionForm.getLocation());

	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {

	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	}

	        	if (!Common.validateRequired(actionForm.getDateTo())) {

	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	}

	        	if (!Common.validateRequired(actionForm.getItemClass())) {

	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}

	        	if (!Common.validateRequired(actionForm.getPropertyCode())) {

	        		criteria.put("propertyCode", actionForm.getPropertyCode());
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);

	     	}

			//get all InvRepItemLedgerBranchList

			ArrayList branchList = new ArrayList();

			for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {

				InvRepItemLedgerBranchList brList = (InvRepItemLedgerBranchList)actionForm.getInvBrIlListByIndex(i);

				if(brList.getBranchCheckbox() == true) {

					AdBranchDetails mdetails = new AdBranchDetails();
					mdetails.setBrAdCompany(user.getCmpCode());
					mdetails.setBrCode(brList.getBranchCode());
					branchList.add(mdetails);

				}

			}

		    try {

		       // get company

			       AdCompanyDetails adCmpDetails = ejbRIL.getAdCompany(user.getCmpCode());
			       company = adCmpDetails.getCmpName();

		       // execute report

		       list = ejbRIL.executeInvRepItemLedger(actionForm.getCriteria(), actionForm.getShowCommittedQuantity(), actionForm.getReferenceNumber(),
		    		   actionForm.getIncludeUnposted(), branchList, user.getCmpCode());

		    } catch (GlobalNoRecordFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("itemLedger.error.noRecordFound"));

		    } catch(EJBException ex) {

		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepItemLedgerAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

		    }

		    if(!errors.isEmpty()) {

		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepItemLedger"));

		    }

		    // fill report parameters, fill report to pdf and set report session

		    Map parameters = new HashMap();





		    for(int x=0;x<user.getResCount();x++) {
		    	Responsibility ds = (Responsibility)user.getRes(x);

		    	System.out.println("resp: " + ds.getRSName());
		    	parameters.put(ds.getRSName(), "YES");

		    }






		    parameters.put("date", new Date());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("company", company);

		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepItemLedgerBranchList brList = (InvRepItemLedgerBranchList)actionForm.getInvBrIlListByIndex(j);

      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}

      		}
      		parameters.put("branchMap", branchMap);

			if (actionForm.getShowCommittedQuantity()){

				parameters.put("showCommittedQuantity", "yes");

			} else {

				parameters.put("showCommittedQuantity", "no");

			}

			if (actionForm.getIncludeUnposted()){

				parameters.put("includeUnposted", "yes");

			} else {

				parameters.put("includeUnposted", "no");

			}



			String filename = "";

			 if(actionForm.getReportType().equals("DSOD")){

				 filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepItemLedgerDSOD.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepItemLedgerDSOD.jasper");

			    }

		    } else {

		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepItemLedger.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepItemLedger.jasper");

			    }
		    }


		    try {

		    	Report report = new Report();

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters,
				        new InvRepItemLedgerDS(list)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters,
					        new InvRepItemLedgerDS(list)));

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
				       new InvRepItemLedgerDS(list)));

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepItemLedgerAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }

/*******************************************************
   -- INV RIL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- INV RIL Load Action --
*******************************************************/

             }

	         if(frParam != null) {

	         	try {

	         		actionForm.reset(mapping, request);

			        ArrayList list = null;
			        Iterator i = null;

			        actionForm.clearCategoryList();

	            	list = ejbRIL.getAdLvInvItemCategoryAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCategoryList((String)i.next());

	            		}

	            	}

	            	actionForm.clearLocationList();

	            	list = ejbRIL.getInvLocAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {
	            		actionForm.setLocationList("ALL");
	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setLocationList((String)i.next());

	            		}

	            	}

	            	actionForm.clearReportTypeList();

			        list = ejbRIL.getAdLvReportTypeAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {


	            		actionForm.setReportTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setReportTypeList((String)i.next());

	            		}

	            	}

	            	actionForm.clearCustomerList();

	            	list = ejbRIL.getArCstAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCustomerList((String)i.next());

	            		}

	            	}


	            	actionForm.clearSupplierList();

	            	list = ejbRIL.getApSplAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setSupplierList((String)i.next());

	            		}

	            	}

					actionForm.clearInvBrIlList();

					list = ejbRIL.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

					i = list.iterator();
					//int ctr = 1;
					while(i.hasNext()) {

						AdBranchDetails details = (AdBranchDetails)i.next();

						InvRepItemLedgerBranchList invBrIlList = new InvRepItemLedgerBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						if (details.getBrHeadQuarter() == 1) invBrIlList.setBranchCheckbox(true);
						//ctr++;

						actionForm.saveInvBrIlList(invBrIlList);

					}

				} catch(EJBException ex) {

			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in InvRepItemLedgerAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

			    }


	            return(mapping.findForward("invRepItemLedger"));

				 } else {

				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));

				    return(mapping.findForward("cmnMain"));

				 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in InvRepItemLedgerAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
