package com.struts.jreports.inv.itemledger;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepItemLedgerDetails;

public class InvRepItemLedgerDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	private int start = 1;
	private Double rmnngVl = 1d;
	private Double rmnngQty = 1d;
	public InvRepItemLedgerDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			InvRepItemLedgerDetails details = (InvRepItemLedgerDetails)i.next();
			
			if(start==1){
				rmnngVl = (((Math.abs(details.getRilBeginningAmount())+Math.abs(details.getRilInAmount()-details.getRilOutAmount())) / 
				(Math.abs(details.getRilBeginningQuantity())+Math.abs(details.getRilInQuantity()-details.getRilOutQuantity()))) *
				(details.getRilBeginningQuantity()+(details.getRilInQuantity()-details.getRilOutQuantity())));
				rmnngQty=details.getRilRemainingQuantity();
			}else{
				
				rmnngVl = (((Math.abs(rmnngVl)+Math.abs(details.getRilInAmount()-details.getRilOutAmount())) / 
						(Math.abs(rmnngQty)+Math.abs(details.getRilInQuantity()-details.getRilOutQuantity()))) *
						(rmnngQty+(details.getRilInQuantity()-details.getRilOutQuantity())));
				
				
				rmnngQty=details.getRilRemainingQuantity();
				
			}
			
			InvRepItemLedgerData dtbData = new InvRepItemLedgerData(details.getRilItemName(), details.getRilItemDesc(), details.getRilItemCategory(), details.getRilUnit(), Common.convertSQLDateToString(details.getRilDate()), 
					details.getRilDocumentNumber(), details.getRilReferenceNumber(), details.getRilReferenceNumber1(), details.getRilSource(), 
					
					details.getRilQcNumber(), details.getRilLocationName(), details.getRilSupplierName(),
					new Double(details.getRilInQuantity()),
					new Double(details.getRilInUnitCost()), new Double(details.getRilInAmount()), new Double(details.getRilOutQuantity()), new Double(details.getRilOutUnitCost()),new Double(details.getRilOutAmount()), new Double(details.getRilRemainingQuantity()),
					new Double(details.getRilRemainingUnitCost()), new Double(details.getRilRemainingAmount()), new Double(details.getRilBeginningQuantity()), new Double(details.getRilBeginningUnitCost()), new Double(details.getRilBeginningAmount()),
					new Double(rmnngVl), details.getRilAccount(), details.getRilPropertyCode(), details.getRilCustodianSource(), details.getRilCustodian());

			data.add(dtbData);
		
			start++;
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("itemName".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getItemName();
			
		} else if("itemDesc".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getItemDescription();
			
		} else if("itemCategory".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getItemCategory();
			
		} else if("unit".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getUnit();
			
		} else if("date".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getDate();
			
		} else if("documentNumber".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getDocumentNumber();
			
		} else if("referenceNumber".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getReferenceNumber();
		
		} else if("referenceNumber1".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getReferenceNumber1();
		
		} else if("source".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getSource();
			
		} else if("qcNumber".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getQcNumber();
			
		} else if("locationName".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getLocationName();
			
		} else if("supplierName".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getSupplierName();
						
						
		} else if("inQuantity".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getInQuantity();
			
		} else if("inUnitCost".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getInUnitCost();
			
		} else if("inAmount".equals(fieldName)) {
			
			value = ((InvRepItemLedgerData)data.get(index)).getInAmount();
			
		} else if("outQuantity".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getOutQuantity();
			
		} else if("outUnitCost".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getOutUnitCost();
			
		} else if("outAmount".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getOutAmount();
			
		} else if("remainingQuantity".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getRemainingQuantity();
			//System.out.println("----------------------remainingQuantity: "+value);
		} else if("remainingUnitCost".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getRemainingUnitCost();
			
		} else if("remainingAmount".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getRemainingAmount();
			//System.out.println("----------------------remainingAmount: "+value);
		} else if("beginningQuantity".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getBeginningQuantity();
			
		} else if("beginningUnitCost".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getBeginningUnitCost();
			
		} else if("beginningAmount".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getBeginningAmount();
			
		} else if("remainingAmount2".equals(fieldName)){
			
			value = ((InvRepItemLedgerData)data.get(index)).getRemainingAmount2();
			//System.out.println("RemAmnt2: "+value);
		} else if ("account".equals(fieldName)) {
			value = ((InvRepItemLedgerData)data.get(index)).getAccount();
			
		} else if ("propertyCode".equals(fieldName)) {
			value = ((InvRepItemLedgerData)data.get(index)).getPropertyCode();
		} else if ("custodian".equals(fieldName)) {
			value = ((InvRepItemLedgerData)data.get(index)).getCustodian();
		}else if ("custodianSource".equals(fieldName)) {
			value = ((InvRepItemLedgerData)data.get(index)).getCustodianSource();
		}
		
		return(value);
		
	}
	
}
