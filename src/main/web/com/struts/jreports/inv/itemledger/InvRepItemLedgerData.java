package com.struts.jreports.inv.itemledger;

public class InvRepItemLedgerData implements java.io.Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String itemCategory = null;
	private String unit = null;
	private String date = null;
	private String documentNumber= null;
	private String referenceNumber = null;
	private String referenceNumber1 = null;
	private String source = null;
	private String qcNumber = null;
	private String locationName = null;
	private String supplierName = null;
	private Double inQuantity = null;
	private Double inUnitCost = null;
	private Double inAmount = null;
	private Double outQuantity = null;
	private Double outUnitCost = null;
	private Double outAmount = null;
	private Double remainingQuantity = null;
	private Double remainingUnitCost = null;
	private Double remainingAmount = null;
	private Double beginningQuantity = null;
	private Double beginningUnitCost = null;
	private Double beginningAmount = null;
	private Double remainingAmount2 = null;
	private String account = null;
	private String custodianSource = null;
	private String custodian = null;
	private String propertyCode = null;
	
	public InvRepItemLedgerData(String itemName, String itemDescription, String itemCategory, String unit, String date, String documentNumber, String referenceNumber, String referenceNumber1, String source, 
			String qcNumber, String locationName, String supplierName,
			Double inQuantity, Double inUnitCost, Double inAmount, Double outQuantity, Double outUnitCost, Double outAmount, Double remainingQuantity, Double remainingUnitCost,
			Double remainingAmount, Double beginningQuantity, Double beginningUnitCost, Double beginningAmount, Double remainingAmount2, String account,
			String propertyCode, String custodianSource, String custodian) {
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemCategory = itemCategory;
		this.unit = unit;
		this.date = date;
		this.documentNumber= documentNumber;
		this.referenceNumber = referenceNumber;
		this.referenceNumber1 = referenceNumber1;
		this.source = source;
		this.qcNumber = qcNumber;
		this.locationName = locationName;
		this.supplierName = supplierName;
		this.inQuantity = inQuantity;
		this.inUnitCost = inUnitCost;
		this.inAmount = inAmount;
		this.outQuantity = outQuantity;
		this.outUnitCost = outUnitCost;
		this.outAmount = outAmount;
		this.remainingQuantity = remainingQuantity;
		this.remainingUnitCost = remainingUnitCost;
		this.remainingAmount = remainingAmount;
		this.beginningQuantity = beginningQuantity;
		this.beginningUnitCost = beginningUnitCost;
		this.beginningAmount = beginningAmount;
		this.remainingAmount2 = remainingAmount2;
		this.account = account;
		
		this.propertyCode = propertyCode;
		this.custodian = custodian;
		this.custodianSource = custodian;
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}

	public String getItemCategory() {
		
		return itemCategory;
		
	}
	
	public String getPropertyCode() {
		
		return propertyCode;
		
	}
	
	public String getCustodianSource() {
		
		return custodianSource;
		
	}

	public String getCustodian() {
		
		return custodian;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}

	public String getDate() {
		
		return date;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}

	public String getReferenceNumber1() {
		
		return referenceNumber1;
		
	}
	
	public String getSource() {
		
		return source;
		
	}
	
	
	public String getQcNumber() {
		
		return qcNumber;
		
	}
	
	public String getLocationName() {
		
		return locationName;
		
	}
	
	
	public String getSupplierName() {
		
		return supplierName;
		
	}
	
	public Double getInQuantity() {
		
		return inQuantity;
		
	}
	
	public Double getInUnitCost() {
		
		return inUnitCost;
		
	}
	
	public Double getInAmount() {
		
		return inAmount;
		
	}	
	
	public Double getOutQuantity() {
		
		return outQuantity;
		
	}
	
	public Double getOutUnitCost() {
		
		return outUnitCost;
		
	}
	
	public Double getOutAmount() {
		
		return outAmount;
		
	}
	
	public Double getRemainingQuantity() {
		
		return remainingQuantity;
		
	}
	
	public Double getRemainingUnitCost() {
		
		return remainingUnitCost;
		
	}
	
	public Double getRemainingAmount() {
	
		return remainingAmount;
	
	}

	public Double getBeginningQuantity() {
		
		return beginningQuantity;
		
	}
	
	public Double getBeginningUnitCost() {
		
		return beginningUnitCost;
		
	}
	
	public Double getBeginningAmount() {
	
		return beginningAmount;
	
	}
	
	public Double getRemainingAmount2() {
		
		return remainingAmount2;
	
	}
	
	public String getAccount() {
		
		return account;
	}

}
