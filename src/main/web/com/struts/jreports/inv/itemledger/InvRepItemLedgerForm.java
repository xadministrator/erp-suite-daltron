package com.struts.jreports.inv.itemledger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepItemLedgerForm extends ActionForm implements Serializable{
	
	private String category = null;
	private String itemName = null;
	private String itemNameTo = null;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String date = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private ArrayList invBrIlList = new ArrayList();
	private boolean showCommittedQuantity = false;	
	private boolean includeUnposted = false;
	private String referenceNumber = null;
	private String propertyCode = null;
	
	private String reportType = null;
	private ArrayList reportTypeList = new ArrayList();
	
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();
	
	private String customer = null;
	private ArrayList customerList = new ArrayList();
	

	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getPropertyCode() {
		
		return (propertyCode);
		
	}
	
	public void setPropertyCode(String propertyCode) {
		
		this.propertyCode = propertyCode;
		
	}
	
	public String getReportType() {

		return(reportType);

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {

		return(reportTypeList);

	}

	public void setReportTypeList(String reportType) {

		reportTypeList.add(reportType);

	}

	public void clearReportTypeList() {

		reportTypeList.clear();
		reportTypeList.add(Constants.GLOBAL_BLANK);

	}
	
	
public String getSupplier() {
		
		return supplier;
		
	}
	
	public void setSupplier(String supplier) {
		
		this.supplier = supplier;
		
	}
	
	public ArrayList getSupplierList() {
		
		return supplierList;
		
	}
	
	public void setSupplierList(String supplier) {
		
		supplierList.add(supplier);
		
	}
	
	public void clearSupplierList() {
		
		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
public String getCustomer() {
		
		return customer;
		
	}
	
	public void setCustomer(String customer) {
		
		this.customer = customer;
		
	}
	
	public ArrayList getCustomerList() {
		
		return customerList;
		
	}
	
	public void setCustomerList(String customer) {
		
		customerList.add(customer);
		
	}
	
	public void clearCustomerList() {
		
		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	public String getDateFrom() {
		
		return (dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}

	public String getItemName() {
		
		return (itemName);
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;		
		
	}
	
	public String getItemNameTo() {
		
		return (itemNameTo);
		
	}
	
	public void setItemNameTo(String itemNameTo) {
		
		this.itemNameTo = itemNameTo;		
		
	}
	
	public String getReferenceNumber() {
		
		return (referenceNumber);
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;		
		
	}
	
	public String getDateTo() {
		
		return (dateTo);
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getCategory() {
		
		return(category);
		
	}
	
	public void setCategory(String category) {
		
		this.category = category;
		
	}
	
	public ArrayList getCategoryList() {
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation() {
		
		return(location);
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return(locationList);
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public ArrayList getItemClassList() {
		
		return itemClassList;
		
	}
	
	public String getViewType() {
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}

	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepItemLedgerBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepItemLedgerBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}

	public boolean getShowCommittedQuantity(){
		
		return(showCommittedQuantity);
		
	}
	
	public void setShowCommittedQuantity(boolean showCommittedQuantity){
		
		this.showCommittedQuantity = showCommittedQuantity;
		
	}
	
	public boolean getIncludeUnposted() {
		
		return includeUnposted;
	}
	
	public void setIncludeUnposted (boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepItemLedgerBranchList list = (InvRepItemLedgerBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		category = Constants.GLOBAL_BLANK;
		location = Constants.GLOBAL_BLANK;
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = null;
		itemName = null;
		showCommittedQuantity = false;
		includeUnposted = false;
		propertyCode = null;

	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("itemLedger.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("itemLedger.error.dateToInvalid"));
				
			}
			
			if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("location", new ActionMessage("itemLedger.error.locationRequired"));
				
			}
			
		}
		
		return(errors);
		
	}
	
}
