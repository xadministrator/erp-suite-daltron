package com.struts.jreports.inv.itemlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepItemListController;
import com.ejb.txn.InvRepItemListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepItemListDetails;

public final class InvRepItemListAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepItemListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         InvRepItemListForm actionForm = (InvRepItemListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.INV_REP_ITEM_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepItemList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepItemListController EJB
*******************************************************/

         InvRepItemListControllerHome homeIL = null;
         InvRepItemListController ejbIL = null;       

         try {
         	
            homeIL = (InvRepItemListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepItemListControllerEJB", InvRepItemListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepItemListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbIL = homeIL.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepItemListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- INV RUV Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            InvRepItemListDetails details = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("itemCategory", actionForm.getCategory());
	        	}	        	
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        	}
	        	
	        	if (actionForm.getEnable()) {
	        			        	   		        		
	        		criteria.put("enable", new Byte((byte)1));
	        	
	            }
	            
	        	if (actionForm.getDisable()) {
	        			            
	        		criteria.put("disable", new Byte((byte)1));
 
                }
                
                if  (!actionForm.getEnable() && !actionForm.getDisable()) {
                	
                	criteria.put("enable", new Byte((byte)1));
                	criteria.put("disable", new Byte((byte)1));
                	
                }
                
                if (actionForm.getSupplier()) {	        		
	        		criteria.put("supplier", new Byte((byte)1));
	        	} else {
	        		criteria.put("supplier", new Byte((byte)0));
	        	}
                
                if (actionForm.getFixedAsset()) {	        		
	        		criteria.put("fixedAsset", new Byte((byte)1));
	        	} else {
	        		criteria.put("fixedAsset", new Byte((byte)0));
	        	}


	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
		    try {
		    
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbIL.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
			    // get selected price levels
	           	ArrayList priceLevelSelectedList = new ArrayList();
	       		
	       		for (int i=0; i < actionForm.getPriceLevelSelectedList().length; i++) {
	
	       			priceLevelSelectedList.add(actionForm.getPriceLevelSelectedList()[i]);
	
	       		}
		       
		       // execute report
		    		    
		       list = ejbIL.executeInvRepItemList(actionForm.getCriteria(),
            	    actionForm.getOrderBy(), priceLevelSelectedList , user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("itemList.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepItemListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepItemList"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("company", company);
		    parameters.put("itemName", actionForm.getItemName());
		    parameters.put("itemCategory", actionForm.getCategory());
		    parameters.put("itemClass", actionForm.getItemClass());
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("location", actionForm.getLocation());		    	    
		    
		    if(actionForm.getEnable() == true && actionForm.getDisable() == false) {
		    
		    	parameters.put("enable", "YES");
		    	parameters.put("disable", "NO");
		    
		    } else if(actionForm.getEnable() == false && actionForm.getDisable() == false) {
		    
		    	parameters.put("enable", "NO");
		    	parameters.put("disable", "NO");
		    	
		    } else {
		    
		    	parameters.put("enable", "YES");
		    	parameters.put("disable", "YES"); 	
		    
		    }
		    
		    if(actionForm.getSupplier()==true)
		    	parameters.put("supplier", "YES");
		    else
		    	parameters.put("supplier", "NO");
		    
		    String filename ="";
		    if(actionForm.getFixedAsset()){
		    	 filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepItemListFixedAsset.jasper";
			       
				    if (!new java.io.File(filename).exists()) {
				    		    		    
				       filename = servlet.getServletContext().getRealPath("jreports/InvRepItemListFixedAsset.jasper");
					    
				    }
		    }else{
		    	 filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepItemList.jasper";
			       
				    if (!new java.io.File(filename).exists()) {
				    		    		    
				       filename = servlet.getServletContext().getRealPath("jreports/InvRepItemList.jasper");
					    
				    }
		    }
		   
	
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepItemListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new InvRepItemListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new InvRepItemListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepItemListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV RUV Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV RUV Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			       
			        actionForm.clearCategoryList();           	
	            	
	            	list = ejbIL.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCategoryList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearLocationList();           	
					
					list = ejbIL.getAdLvInvLocationAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLocationList((String)i.next());
							
						}
						
					}
					
					
					actionForm.clearPriceLevelList();           	

	         		list = ejbIL.getAdLvInvPriceLevelAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setPriceLevelList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setPriceLevelList((String)i.next());

	         			}

	         		}

				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in InvRepItemListAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	         	
	
	            actionForm.reset(mapping, request);
	            
	            return(mapping.findForward("invRepItemList"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepItemListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
