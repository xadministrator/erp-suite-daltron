package com.struts.jreports.inv.itemlist;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.inv.markuplist.InvRepMarkupListData;
import com.struts.util.Common;
import com.util.InvPriceLevelDetails;
import com.util.InvRepItemListDetails;

public class InvRepItemListDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepItemListDS(ArrayList list, boolean includePriceLevel) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         InvRepItemListDetails details = (InvRepItemListDetails)i.next();
         
         
         
         if(!includePriceLevel) {
        	 
        	 InvRepItemListData dtbData = new InvRepItemListData(
       	     		details.getIlIiName(), details.getIlIiDescription(), details.getIlIiClass(),
       				Common.convertByteToBoolean(details.getIlIiEnable()) ? "YES" : "NO", details.getIlIiUomName(),
       				new Double(details.getIlIiUnitCost()), new Double(details.getIlIiPrice()), details.getIlIiCostMethod(),
       				details.getIlIiSupplierName(), details.getIlIiPartNumber(), details.getIlIiLctn(), details.getIlIiCategoryName(), 
       				Common.convertSQLDateToString(details.getIiDateAcquired()), details.getIlIiBrnch(), details.getIlIiDprtmnt());
       		  
         	 	data.add(dtbData);
        	 
         } else {
		       // Selected Price Level	        	 
        	 Iterator iter = details.getIiPriceLevels().iterator();
        	 
        	 while(iter.hasNext()){
	        	 
	        	 InvPriceLevelDetails pDetails = (InvPriceLevelDetails)iter.next();
	        	 
	        	 InvRepItemListData dtbData = new InvRepItemListData(
	         	     		details.getIlIiName(), details.getIlIiDescription(), details.getIlIiClass(),
	         				Common.convertByteToBoolean(details.getIlIiEnable()) ? "YES" : "NO", details.getIlIiUomName(),
	         				new Double(details.getIlIiUnitCost()), new Double(details.getIlIiPrice()), details.getIlIiCostMethod(),
	         				details.getIlIiSupplierName(), details.getIlIiPartNumber(), details.getIlIiLctn(), details.getIlIiCategoryName(), 
	         				Common.convertSQLDateToString(details.getIiDateAcquired()), details.getIlIiBrnch(), details.getIlIiDprtmnt());
	         		  
	            	 System.out.println("pDetails.getPlAdLvPriceLevel()="+pDetails.getPlAdLvPriceLevel());
	            	 dtbData.setPriceLevelName(pDetails.getPlAdLvPriceLevel());
	         	     
	         	     dtbData.setPriceLevelAmount(new Double(pDetails.getPlAmount()));
	         	     dtbData.setPriceLevelMarkupPercent(new Double(pDetails.getPlPercentMarkup()));
	         	     dtbData.setPriceLevelShippingCost(new Double(pDetails.getPlShippingCost()));
	             	
	         	     data.add(dtbData);
	        	 
	         }
        	 
         }
         
   
         
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("itemName".equals(fieldName)) {
      	
          value = ((InvRepItemListData)data.get(index)).getItemName();
      
      } else if("itemDescription".equals(fieldName)) {
      
          value = ((InvRepItemListData)data.get(index)).getItemDescription();
      
      } else if("itemClass".equals(fieldName)){
      
          value = ((InvRepItemListData)data.get(index)).getItemClass();
      
      } else if("enable".equals(fieldName)){
      
          value = ((InvRepItemListData)data.get(index)).getEnable();

      } else if("unitMeasure".equals(fieldName)){
      	
      	value = ((InvRepItemListData)data.get(index)).getUnitMeasure();
      	
      } else if("unitCost".equals(fieldName)){
      	
      	value = ((InvRepItemListData)data.get(index)).getUnitCost();
      	
      } else if("price".equals(fieldName)){
      	
      	value = ((InvRepItemListData)data.get(index)).getPrice();
      	
      } else if("costMethod".equals(fieldName)){
      	
      	value = ((InvRepItemListData)data.get(index)).getCostMethod();
      	
      } else if("supplierName".equals(fieldName)){
      	
      	value = ((InvRepItemListData)data.get(index)).getSupplierName();
      	
      } else if("partNumber".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getPartNumber();

      }else if("itemLocation".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getItemLocation();

      }else if("categoryName".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getCategoryName();

      }else if("dateAcquired".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getDateAcquired();

      }else if("locationBranch".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getLocationBranch();

      }else if("locationDepartment".equals(fieldName)){

    	  value = ((InvRepItemListData)data.get(index)).getLocationDepartment();
    	  
      }else if("priceLevelName".equals(fieldName)) {
     		
     		value = ((InvRepItemListData)data.get(index)).getPriceLevelName();
     		
	  	} else if("priceLevelAmount".equals(fieldName)) {
	     		
	     		value = ((InvRepItemListData)data.get(index)).getPriceLevelAmount();
	     		
	  	} else if("priceLevelShippingCost".equals(fieldName)) {
	  		
	  		value = ((InvRepItemListData)data.get(index)).getPriceLevelShippingCost();
	  		
	  	} else if("priceLevelMarkupPercent".equals(fieldName)){
	  		
	  		value = ((InvRepItemListData)data.get(index)).getPriceLevelMarkupPercent();
	  	

      }

      return(value);
   }
}
