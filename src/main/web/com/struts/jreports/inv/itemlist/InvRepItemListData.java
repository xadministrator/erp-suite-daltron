package com.struts.jreports.inv.itemlist;

public class InvRepItemListData implements java.io.Serializable {

	private String itemName = null;
	private String itemDescription = null;
	private String itemClass = null;
	private String enable = null;
	private String unitMeasure = null;
	private Double unitCost = null;
	private Double price = null;
	private String costMethod = null;
	private String supplierName = null;
	private String partNumber = null;
	private String location = null;
	private String categoryName=null;
	private String dateAcquired = null;
	private String locationBranch=null;
	private String locationDepartment = null;
	private String priceLevelName;
	private Double priceLevelAmount;
	private Double priceLevelShippingCost;
	private Double priceLevelMarkupPercent;
	
	public InvRepItemListData(String itemName, String itemDescription, String itemClass, String enable, String unitMeasure,
			Double unitCost, Double price, String costMethod, String supplierName, String partNumber, String location, String categoryName,
			String dateAcquired, String locationBranch, String locationDepartment) {

		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemClass = itemClass;
		this.enable = enable;
		this.unitMeasure = unitMeasure;
		this.unitCost = unitCost;
		this.price = price;
		this.costMethod = costMethod;
		this.supplierName = supplierName;
		this.partNumber = partNumber;
		this.location = location;
		this.categoryName = categoryName;
		this.dateAcquired = dateAcquired;
		this.locationBranch = locationBranch;
		this.locationDepartment = locationDepartment;
	}

	public String getItemName() {

		return(itemName);

	}

	public String getItemDescription() {

		return(itemDescription);

	}

	public String getItemClass() {

		return(itemClass);

	}

	public String getEnable() {

		return(enable);

	}

	public String getUnitMeasure() {

		return(unitMeasure);

	}

	public Double getUnitCost() {

		return(unitCost);

	}

	public Double getPrice() {

		return(price);

	}

	public String getCostMethod() {

		return(costMethod);

	}

	public String getSupplierName() {

		return(supplierName);

	}

	public String getPartNumber() {

		return(partNumber);

	}

	public String getItemLocation() {

		return(location);

	}

	public String getCategoryName() {

		return(categoryName);

	}

	public String getDateAcquired() {

		return dateAcquired;

	}
	
	public String getLocationBranch() {

		return locationBranch;

	}
	
	public String getLocationDepartment() {

		return locationDepartment;

	}
	
	
	public String getPriceLevelName(){
		
		return priceLevelName;
		
	}
	
	public void setPriceLevelName(String priceLevelName){
	
		this.priceLevelName = priceLevelName;
		
	}
	
	public Double getPriceLevelAmount(){
		
		return priceLevelAmount;
		
	}
	
	public void setPriceLevelAmount(Double priceLevelAmount){
	
		this.priceLevelAmount = priceLevelAmount;
		
	}
	
	public Double getPriceLevelShippingCost(){
		
		return priceLevelShippingCost;
		
	}
	
	public void setPriceLevelShippingCost(Double priceLevelShippingCost){
	
		this.priceLevelShippingCost = priceLevelShippingCost;
		
	}
	
	public Double getPriceLevelMarkupPercent(){
		
		return priceLevelMarkupPercent;
		
	}
	
	public void setPriceLevelMarkupPercent(Double priceLevelMarkupPercent){
	
		this.priceLevelMarkupPercent = priceLevelMarkupPercent;
		
	}

}
