package com.struts.jreports.inv.itemlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class InvRepItemListForm extends ActionForm implements Serializable{

   private String itemName = null;
   private String itemClass = null;
   private ArrayList itemClassList = new ArrayList();
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private ArrayList priceLevelList = new ArrayList();
	private String[] priceLevelSelectedList = new String[0];
   private boolean enable = false;
   private boolean disable = false;
   private boolean fixedAsset= false;
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private String location = null;
   
   private boolean supplier = false;   
   
   private ArrayList locationList = new ArrayList();
   
   private HashMap criteria = new HashMap();

   private String userPermission = new String();
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public String getItemName(){
      return (itemName);
   }
   
   public void setItemName(String itemName){
      this.itemName = itemName;
   }

   public String getCategory(){
      return(category);
   }

   public void setCategory(String category){
      this.category = category;
   }

   public ArrayList getCategoryList(){
      return(categoryList);
   }

   public void setCategoryList(String category){
   	  categoryList.add(category);
   }

   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getItemClass(){
      return(itemClass);
   }

   public void setItemClass(String itemClass){
      this.itemClass = itemClass;
   }

   public ArrayList getItemClassList(){
      return(itemClassList);
   }

   public String getOrderBy(){
 	  return(orderBy);   	
   }
 
   public void setOrderBy(String orderBy){
 	  this.orderBy = orderBy;
   }
 
   public ArrayList getOrderByList(){
 	  return orderByList;
   }
   
   public ArrayList getPriceLevelList(){

		return(priceLevelList);

	}

	public void setPriceLevelList(String priceLevel){

		priceLevelList.add(priceLevel);

	}

	public void clearPriceLevelList(){

		priceLevelList.clear();

	}
	
	public String[] getPriceLevelSelectedList() {

		return priceLevelSelectedList;

	}

	public void setPriceLevelSelectedList(String[] priceLevelSelectedList) {

		this.priceLevelSelectedList = priceLevelSelectedList;

	}
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
   
   public void setEnable(boolean enable) {
   	
   	  this.enable = enable;
   	  
   }
   
   public boolean getDisable() {

	   return disable;

   }
   
   public void setDisable(boolean disable) {

	   this.disable = disable;

   }

   public boolean getFixedAsset() {

	   return fixedAsset;

   }

   public void setFixedAsset(boolean fixedAsset) {

	   this.fixedAsset = fixedAsset;

   }
   
   public String getLocation(){
       return(location);
    }

    public void setSupplier(boolean supplier){
       this.supplier = supplier;
    }
    
    public boolean getSupplier(){
        return(supplier);
     }

     public void setLocation(String location){
        this.location = location;
     }

    public ArrayList getLocationList(){
       return(locationList);
    }

    public void setLocationList(String location){
    	  locationList.add(location);
    }

    public void clearLocationList(){
    	  locationList.clear();
    	  locationList.add(Constants.GLOBAL_BLANK);
    }
  
   
   public void reset(ActionMapping mapping, HttpServletRequest request){      
      goButton = null;
      closeButton = null;
      viewTypeList.clear();
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
      viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
      viewType = Constants.REPORT_VIEW_TYPE_PDF;
      category = Constants.GLOBAL_BLANK;
      itemClassList.clear();
	  itemClassList.add(Constants.GLOBAL_BLANK);
	  itemClassList.add("Stock");
	  itemClassList.add("Assembly");		
	  itemClass = Constants.GLOBAL_BLANK;	
      itemName = null;
      enable = false;
      disable = false;
      fixedAsset =false;
      supplier = false;
      
      if (orderByList.isEmpty()) {
      	
		  orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("ITEM NAME");
	      orderByList.add("ITEM CATEGORY");
	      orderBy = "ITEM CATEGORY";   
	  
	  }
      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();

      return(errors);
   }
}
