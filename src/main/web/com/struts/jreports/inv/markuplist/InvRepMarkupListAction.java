package com.struts.jreports.inv.markuplist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepMarkupListController;
import com.ejb.txn.InvRepMarkupListControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepMarkupListAction extends Action {
	
   private org.apache.commons.logging.Log log =
   	org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepMarkupListAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvRepMarkupListForm actionForm = (InvRepMarkupListForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.INV_REP_MARKUP_LIST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepMarkupList");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepmarkupListController EJB
*******************************************************/

         InvRepMarkupListControllerHome homeSH = null;
         InvRepMarkupListController ejbSH = null;       

         try {
         	
            homeSH = (InvRepMarkupListControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepMarkupListControllerEJB", InvRepMarkupListControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepMarkupListAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSH = homeSH.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepMarkupListAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- AP RIL Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getItemName())) {
	        		
	        		criteria.put("itemName", actionForm.getItemName());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("itemCategory", (actionForm.getCategory()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", (actionForm.getItemClass()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", (actionForm.getLocation()));
	        		
	        	}	        		        
	 
	        	if (!Common.validateRequired(actionForm.getLocationType())) {
	        		
	        		criteria.put("locationType", (actionForm.getLocationType()));
	        		
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            	
            	
            try {

            	// get company

            	AdCompanyDetails adCmpDetails = ejbSH.getAdCompany(user.getCmpCode());
            	company = adCmpDetails.getCmpName();

            	//get all InvUsageVarianceBranchLists

            	ArrayList branchList = new ArrayList();

            	for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {

            		InvRepMarkupListBranchList brList = (InvRepMarkupListBranchList)actionForm.getInvBrIlListByIndex(i);

            		if(brList.getBranchCheckbox() == true) {

            			AdBranchDetails mdetails = new AdBranchDetails();
            			mdetails.setBrAdCompany(user.getCmpCode());
            			mdetails.setBrCode(brList.getBranchCode());
            			branchList.add(mdetails);

            		}

            	}

            	// get selected price levels
            	ArrayList priceLevelSelectedList = new ArrayList();
        		
        		for (int i=0; i < actionForm.getPriceLevelSelectedList().length; i++) {

        			priceLevelSelectedList.add(actionForm.getPriceLevelSelectedList()[i]);

        		}

            	// execute report

            	list = ejbSH.executeInvRepMarkupList(actionForm.getCriteria(), actionForm.getUnit(), actionForm.getIncludeZeroes(), actionForm.getRecalcMarkup(),
            			actionForm.getOrderBy(), priceLevelSelectedList, branchList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("inventoryList.error.noRecordFound"));
               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepMarkupListAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invRepMarkupList");

            }


		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", new java.util.Date());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
			parameters.put("viewType", actionForm.getViewType());
			
			if (actionForm.getIncludeZeroes()){
				
				parameters.put("includeZeroes", "yes");
				
			} else {
				
				parameters.put("includeZeroes", "no");
				
			}
			
			if (actionForm.getPriceLevelSelectedList().length > 0){
				
				parameters.put("includePriceLevel", "yes");
				
			} else {
				
				parameters.put("includePriceLevel", "no");
				
			}

        	if (actionForm.getItemName() != null) {
        		
        		parameters.put("itemName", actionForm.getItemName());
        		
        	}

        	if (actionForm.getCategory() != null) {
        		
        		parameters.put("itemCategory", actionForm.getCategory());
        		
        	}
        	
        	if (actionForm.getItemClass() != null) {
        		
        		parameters.put("itemClass", actionForm.getItemClass());
        		
        	}

        	if (actionForm.getLocation() != null) {
        		
        		parameters.put("location", actionForm.getLocation());
        		
        	}	        		        
 
        	if (actionForm.getLocationType() != null) {
        		
        		parameters.put("locationType", actionForm.getLocationType());
        		
        	}	    		 

        	if (actionForm.getUnit() != null) {

        		parameters.put("unit", actionForm.getUnit());

        	}
	    		        
        	String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepMarkupListBranchList brList = (InvRepMarkupListBranchList)actionForm.getInvBrIlListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
        	
      		String ReportType="InvRepMarkupList.jasper";
      		
      		if(actionForm.getPriceLevelSelectedList().length > 0){
      			ReportType="InvRepMarkupListPriceLevel.jasper";
    		} 
      		
	    	String filename = "/opt/ofs-resources/" + user.getCompany() + "/" + ReportType;
		       
	    	if (!new java.io.File(filename).exists()) {
	    			
	    			filename = servlet.getServletContext().getRealPath("jreports/" + ReportType);
	    		
	    	}	    	
	
		    try {
		    	
		    	Report report = new Report();
		    	
		    	if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		    		
		    		report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		    		report.setBytes(
		    				JasperRunManager.runReportToPdf(filename, parameters, 
		    						new InvRepMarkupListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));   
		    		
		    	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
		    		
		    		report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		    		report.setBytes(
		    				JasperRunManagerExt.runReportToXls(filename, parameters, 
		    						new InvRepMarkupListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));   
		    		
		    	} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
		    		
		    		report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
		    		report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
		    				new InvRepMarkupListDS(list, actionForm.getPriceLevelSelectedList().length > 0 ? true : false)));												    
		    		
		    	}
		    	
		    	session.setAttribute(Constants.REPORT_KEY, report);
		    	actionForm.setReport(Constants.STATUS_SUCCESS);
		    		
		    } catch(Exception ex) {
		    	ex.printStackTrace();
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepMarkupListAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- AP RIL Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- AP RIL Load Action --
*******************************************************/

             }
         
         if(frParam != null) {
         	
         	
         	try {

         		// populate branch list

             	actionForm.reset(mapping, request);
             	
         		actionForm.clearInvBrIlList();
         		ArrayList brList = new ArrayList();
         		
				brList = ejbSH.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 
				
				Iterator k = brList.iterator();
				//int ctr = 1;
				while(k.hasNext()) {

					AdBranchDetails details = (AdBranchDetails)k.next();
					
					InvRepMarkupListBranchList invBrIlList = new InvRepMarkupListBranchList(actionForm,
							details.getBrBranchCode(),details.getBrName(),
							details.getBrCode());
					if (details.getBrHeadQuarter() == 1) invBrIlList.setBranchCheckbox(true);
					//ctr++;
					actionForm.saveInvBrIlList(invBrIlList);
					
				}

         		ArrayList list = null;			       			       
         		Iterator i = null;
         		
         		actionForm.clearCategoryList();           	
         		
         		list = ejbSH.getAdLvInvItemCategoryAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCategoryList((String)i.next());
         				
         			}
         			
         		}
         		
         		actionForm.clearLocationTypeList();           	
         		
         		list = ejbSH.getAdLvInvLocationTypeAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setLocationTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setLocationTypeList((String)i.next());
         				
         			}
         			
         		}
         		
         		actionForm.clearLocationList();           	
         		
         		list = ejbSH.getInvLocAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setLocationList((String)i.next());
         				
         			}
         			
         		}
         		
         		actionForm.clearUnitList();
         		
         		list = ejbSH.getInvUomAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setUnitList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setUnitList((String)i.next());
         				
         			}
         			
         		}
         		
         		actionForm.clearPriceLevelList();           	

         		list = ejbSH.getAdLvInvPriceLevelAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setPriceLevelList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setPriceLevelList((String)i.next());

         			}

         		}
         		
         	} catch(EJBException ex) {
         		
         		if(log.isInfoEnabled()) {
         			log.info("EJBException caught in InvRepMarkupListAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	}
         	
         	
         	return(mapping.findForward("invRepMarkupList"));
         	
         } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE,
				    		new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }

         } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
        	 e.printStackTrace();
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepMarkupListAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
