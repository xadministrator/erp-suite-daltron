package com.struts.jreports.inv.markuplist;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvPriceLevelDetails;
import com.util.InvRepMarkupListDetails;

public class InvRepMarkupListDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepMarkupListDS(ArrayList list, boolean includePriceLevel) {
	   
	  Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         InvRepMarkupListDetails details = (InvRepMarkupListDetails)i.next();
	               
	         
	         if(!includePriceLevel) {
	        	 
	        	 InvRepMarkupListData argData = new InvRepMarkupListData(details.getMlItemName(),
		 	         		details.getMlItemDescription(), details.getMlLocation(), details.getMlItemClass(), details.getMlUnit(),
		 					new Double(details.getMlQuantity()), new Double(details.getMlUnitCost()), new Double(details.getMlAmount()),
		 					new Double(details.getMlSalesPrice()), new Double(details.getMlAverageCost()), details.getMlIiPartNumber(),
		 					new Double(details.getMlShippingCost()), new Double(details.getMlMarkupPercent()), new Double(details.getMlGrossProfit()));
	        	        	 
	        	 data.add(argData);
	        	 
	         } else {
			       // Selected Price Level	        	 
	        	 Iterator iter = details.getMlPriceLevels().iterator();
	        	 
	        	 while(iter.hasNext()){
		        	 
		        	 InvPriceLevelDetails pDetails = (InvPriceLevelDetails)iter.next();
		        	 
		        	 InvRepMarkupListData argData = new InvRepMarkupListData(details.getMlItemName(),
		 	         		details.getMlItemDescription(), details.getMlLocation(), details.getMlItemClass(), details.getMlUnit(),
		 					new Double(details.getMlQuantity()), new Double(details.getMlUnitCost()), new Double(details.getMlAmount()),
		 					new Double(details.getMlSalesPrice()), new Double(details.getMlAverageCost()), details.getMlIiPartNumber(),
		 					new Double(details.getMlShippingCost()), new Double(details.getMlMarkupPercent()), new Double(details.getMlGrossProfit()));
		 	     
		        	 

		        	 argData.setPriceLevelName(pDetails.getPlAdLvPriceLevel());
		        	 argData.setPriceLevelAmount(new Double(pDetails.getPlAmount()));
		        	 argData.setPriceLevelMarkupPercent(new Double(pDetails.getPlPercentMarkup()));
		        	 argData.setPriceLevelShippingCost(new Double(pDetails.getPlShippingCost()));
		        	 	        	 
		 	         data.add(argData);
		        	 
		         }
	        	 
	         }
	         
	         
	         
	         
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getLocation();
   		
   	} else if("itemClass".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getItemClass();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getUnit();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getQuantity();
   		
	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getUnitCost();
   		
	} else if("amount".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getAmount();
   		
	} else if("salesPrice".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getSalesPrice();
   		
	} else if("aveCost".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getAverageCost();
   		
	} else if("partNumber".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getPartNumber();
   		
	} else if("shippingCost".equals(fieldName)) {
		
		value = ((InvRepMarkupListData)data.get(index)).getShippingCost();
		
	} else if("markupPercent".equals(fieldName)) {
		
		value = ((InvRepMarkupListData)data.get(index)).getMarkupPercent();
		
	} else if("grossProfit".equals(fieldName)) {
		
		value = ((InvRepMarkupListData)data.get(index)).getGrossProfit();
		
	}else if("priceLevelName".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getPriceLevelName();
   		
	} else if("priceLevelAmount".equals(fieldName)) {
   		
   		value = ((InvRepMarkupListData)data.get(index)).getPriceLevelAmount();
   		
	} else if("priceLevelShippingCost".equals(fieldName)) {
		
		value = ((InvRepMarkupListData)data.get(index)).getPriceLevelShippingCost();
		
	} else if("priceLevelMarkupPercent".equals(fieldName)){
		
		value = ((InvRepMarkupListData)data.get(index)).getPriceLevelMarkupPercent();
		
	
	}
   	
   	return(value);
   	
   }
   
}
