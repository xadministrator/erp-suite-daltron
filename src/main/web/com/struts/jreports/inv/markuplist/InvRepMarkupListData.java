package com.struts.jreports.inv.markuplist;

import java.util.ArrayList;


public class InvRepMarkupListData implements java.io.Serializable {

	private String itemName;
	private String itemDescription;
	private String location;
	private String itemClass;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;
	private Double salesPrice;	
	private Double averageCost;
	private String partNumber;
	private Double shippingCost;
	private Double markupPercent;
	private Double grossProfit;
	private String priceLevelName;
	private Double priceLevelAmount;
	private Double priceLevelShippingCost;
	private Double priceLevelMarkupPercent;
	
	
	
	public InvRepMarkupListData( String itemName, String itemDescription, String location,
			String itemClass, String unit, Double quantity, Double unitCost, Double amount, 
			Double salesPrice, Double averageCost, String partNumber, Double shippingCost, 
			Double markupPercent, Double grossProfit) {
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.itemClass = itemClass;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;
		this.salesPrice = salesPrice;
		this.averageCost = averageCost;
		this.partNumber = partNumber;
		this.shippingCost = shippingCost;
		this.markupPercent = markupPercent;
		this.grossProfit = grossProfit;
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}

	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDescription) {
	
		this.itemDescription = itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public String getUnit() {
		
		return unit;
	
	}
	
	public void setUnit(String unit) {
	
		this.unit = unit;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public void setLocation(String location) {
	
		this.location = location;
	
	}
	
	public Double getUnitCost() {
		
		return unitCost;
		
	}
		
	public void setUnitCost(Double unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public Double getAmount() {
		
		return amount;
		
	}
		
	public void setAmount(Double amount) {
		
		this.amount = amount;
		
	}
	
	public Double getSalesPrice() {
		
		return salesPrice;
		
	}
	
	public void setSalesPrice(Double salesPrice) {
		
		this.salesPrice = salesPrice;
		
	}
	
	public Double getAverageCost() {
		
		return averageCost;
		
	}
	
	public void setAverageCost(Double averageCost) {
		
		this.averageCost = averageCost;
		
	}
	
	public String getPartNumber() {
		
		return partNumber;
		
	}
	
	public void setPartNumber(String partNumber) {
		
		this.partNumber = partNumber;
		
	}
	
	public Double getShippingCost (){
		
		return shippingCost;
		
	}
	
	public void setShippingCost(Double shippingCost) {
		
		this.shippingCost = shippingCost;
		
	}
	
	public Double getMarkupPercent() {
		
		return markupPercent;
				
	}
	
	public void serMarkupPercent (Double markupPercent) {
		
		this.markupPercent = markupPercent;
		System.out.println("Data(getMarkup): " + markupPercent);
				
	}
	
	public Double getGrossProfit () {
		
		return grossProfit;
		
	}
	
	public void setGrossProfit (Double grossProfit) {
		
		this.grossProfit = grossProfit;
	}
	
	public String getPriceLevelName(){
	
		return priceLevelName;
		
	}
	
	public void setPriceLevelName(String priceLevelName){
	
		this.priceLevelName = priceLevelName;
		
	}
	
	public Double getPriceLevelAmount(){
		
		return priceLevelAmount;
		
	}
	
	public void setPriceLevelAmount(Double priceLevelAmount){
	
		this.priceLevelAmount = priceLevelAmount;
		
	}
	
	public Double getPriceLevelShippingCost(){
		
		return priceLevelShippingCost;
		
	}
	
	public void setPriceLevelShippingCost(Double priceLevelShippingCost){
	
		this.priceLevelShippingCost = priceLevelShippingCost;
		
	}
	
	public Double getPriceLevelMarkupPercent(){
		
		return priceLevelMarkupPercent;
		
	}
	
	public void setPriceLevelMarkupPercent(Double priceLevelMarkupPercent){
	
		this.priceLevelMarkupPercent = priceLevelMarkupPercent;
		
	}

	
} // InvRepMarkupListData class
