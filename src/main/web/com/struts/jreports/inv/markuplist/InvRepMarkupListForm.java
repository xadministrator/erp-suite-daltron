package com.struts.jreports.inv.markuplist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class InvRepMarkupListForm extends ActionForm implements Serializable{
	
	
	private String itemName = null;
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String locationType = null;
	private ArrayList locationTypeList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private boolean includeZeroes = false;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private ArrayList invBrIlList = new ArrayList(); 
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private ArrayList priceLevelList = new ArrayList();
	private String[] priceLevelSelectedList = new String[0];
	private boolean recalcMarkup = false;
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;

	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public boolean getIncludeZeroes(){
		
		return(includeZeroes);
		
	}
	
	public void setIncludeZeroes(boolean includeZeroes){
		
		this.includeZeroes = includeZeroes;
		
	}
	
	public boolean getRecalcMarkup(){
		
		return(recalcMarkup);
		
	}
	
	public void setRecalcMarkup(boolean recalcMarkup){
		
		this.recalcMarkup = recalcMarkup;
		
	}
	
	public String getItemName(){
		
		return(itemName);
		
	}
	
	public void setItemName(String itemName){
		
		this.itemName = itemName;
		
	}
	
	public String getCategory (){
		
		return(category );
		
	}
	
	public void setCategory (String category ){
		
		this.category  = category ;
		
	}
	
	public ArrayList getCategoryList(){
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category){
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList(){
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}

	public String getItemClass (){
		
		return(itemClass );
		
	}
	
	public void setItemClass (String itemClass ){
		
		this.itemClass  = itemClass ;
		
	}
	
	public ArrayList getItemClassList(){
		
		return(itemClassList);
		
	}
	
	public void setItemClassList(String itemClass){
		
		itemClassList.add(itemClass);
		
	}
	
	public void clearItemClassList(){
		
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocation (){
		
		return(location );
		
	}
	
	public void setLocation (String location ){
		
		this.location  = location ;
		
	}
	
	public ArrayList getLocationList(){
		
		return(locationList);
		
	}
	
	public void setLocationList(String location){
		
		locationList.add(location);
		
	}
	
	public void clearLocationList(){
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}	

	public String getLocationType (){
		
		return(locationType );
		
	}
	
	public void setLocationType (String locationType ){
		
		this.locationType  = locationType ;
		
	}
	
	public ArrayList getLocationTypeList(){
		
		return(locationTypeList);
		
	}
	
	public void setLocationTypeList(String locationType){
		
		locationTypeList.add(locationType);
		
	}
	
	public void clearLocationTypeList(){
		
		locationTypeList.clear();
		locationTypeList.add(Constants.GLOBAL_BLANK);
		
	}	

	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getViewType(){
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepMarkupListBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepMarkupListBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	public String getUnit (){
		
		return(unit);
		
	}
	
	public void setUnit (String unit){
		
		this.unit = unit ;
		
	}
	
	public ArrayList getUnitList(){
		
		return(unitList);
		
	}
	
	public void setUnitList(String unit){
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList(){
		
		unitList.clear();
		unitList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public ArrayList getPriceLevelList(){

		return(priceLevelList);

	}

	public void setPriceLevelList(String priceLevel){

		priceLevelList.add(priceLevel);

	}

	public void clearPriceLevelList(){

		priceLevelList.clear();

	}
	
	public String[] getPriceLevelSelectedList() {

		return priceLevelSelectedList;

	}

	public void setPriceLevelSelectedList(String[] priceLevelSelectedList) {

		this.priceLevelSelectedList = priceLevelSelectedList;

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepMarkupListBranchList list = (InvRepMarkupListBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;
		
		category = null;
		itemName = null;
		location = null;
		locationType = null;
		includeZeroes = false;
		recalcMarkup = false;
		unit = null;
		priceLevelSelectedList = new String[0];	
		
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add(Constants.INV_REP_STOCK_ON_HAND_ITEM_CLASS_STOCK);
		itemClassList.add(Constants.INV_REP_STOCK_ON_HAND_ITEM_CLASS_ASSEMBLY);
		itemClass = null;
		
		orderByList.clear();
		orderByList.add(Constants.INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_CLASS);
		orderBy = Constants.INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_NAME;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();

		return(errors);
		
	}
}
