package com.struts.jreports.inv.physicalinventoryprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepPhysicalInventoryPrintController;
import com.ejb.txn.InvRepPhysicalInventoryPrintControllerHome;
import com.struts.jreports.ar.invoiceprint.ArRepInvoicePrintDS;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepPhysicalInventoryPrintDetails;


public final class InvRepPhysicalInventoryPrintAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvRepPhysicalInventoryPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         InvRepPhysicalInventoryPrintForm actionForm = (InvRepPhysicalInventoryPrintForm)form;

         String frParam= Common.getUserPermission(user, Constants.INV_REP_PHYSICAL_INVENTORY_PRINT_ID);

         if (frParam != null) {

             actionForm.setUserPermission(frParam.trim());

         } else {

             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize InvRepAdjustmenPrintController EJB
*******************************************************/

         InvRepPhysicalInventoryPrintControllerHome homePI = null;
         InvRepPhysicalInventoryPrintController ejbPI = null;

         try {

        	 homePI = (InvRepPhysicalInventoryPrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepPhysicalInventoryPrintControllerEJB", InvRepPhysicalInventoryPrintControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in InvRepPhysicalInventoryPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

        	 ejbPI = homePI.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in InvRepPhysicalInventoryPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- TO DO getInvRepPhysicalInventoryEntryCode() --
*******************************************************/
	
		if (frParam != null) {
			 System.out.print("enter jasper print");
			    AdCompanyDetails adCmpDetails = null;
			    InvRepPhysicalInventoryPrintDetails details = null;
			    String VIEW_TYP = "";
			    ArrayList list = null;
			
				try {

					ArrayList atrCodeList = new ArrayList();

					if (request.getParameter("forward") != null) {

						 VIEW_TYP = request.getParameter("viewType");
	            		atrCodeList.add(new Integer(request.getParameter("physicalInventoryCode")));

					}

	            	list = ejbPI.executeInvRepPhysicalInventoryPrint(atrCodeList, user.getCmpCode());


			        // get company

			        adCmpDetails = ejbPI.getAdCompany(user.getCmpCode());

			        // get parameters

	      			Iterator i = list.iterator();

	      			while (i.hasNext()) {

	      				details = (InvRepPhysicalInventoryPrintDetails)i.next();

	      			}

	            } catch(GlobalNoRecordFoundException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentPrint.error.adjustmentEntryAlreadyDeleted"));

	            } catch(EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in InvPhysicalInventoryPrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }

	               return(mapping.findForward("cmnErrorPage"));

	           }

	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("invRepPhysicalInventoryPrint");

	             }

				// fill report parameters, fill report to pdf and set report session

			    Map parameters = new HashMap();
			    parameters.put("company", adCmpDetails.getCmpName());
				parameters.put("createdBy", details.getPipPiCreatedBy());

		
				String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepPhysicalInventoryPrint.jasper";

          		if (!new java.io.File(filename).exists()) {

      				filename = servlet.getServletContext().getRealPath("jreports/InvRepPhysicalInventoryPrint.jasper");

      			}


				try {
					System.out.println("path jasper :" + filename);

				    Report report = new Report();

				   
				    
				    
				    if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_PDF)) {

			       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				       report.setBytes(
				          JasperRunManager.runReportToPdf(filename, parameters,
				        		  new InvRepPhysicalInventoryPrintDS(list)));

				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_EXCEL)){

		               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
		               report.setBytes(
						   JasperRunManagerExt.runReportToXls(filename, parameters,
								   new InvRepPhysicalInventoryPrintDS(list)));

				   } else if (VIEW_TYP.equals(Constants.REPORT_VIEW_TYPE_HTML)){

					   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);
					   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters,
							   new InvRepPhysicalInventoryPrintDS(list)));

				   } else {
					   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				       report.setBytes(
				          JasperRunManager.runReportToPdf(filename, parameters,
				        		  new InvRepPhysicalInventoryPrintDS(list)));

				   }

				    
				

				    
				    session.setAttribute(Constants.REPORT_KEY, report);
				} catch(Exception ex) {
					ex.printStackTrace();
				    if(log.isInfoEnabled()) {

				      log.info("Exception caught in InvRepPhysicalInventoryPrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());

				   }

				    return(mapping.findForward("cmnErrorPage"));

				}

				return(mapping.findForward("invRepPhysicalInventoryPrint"));

		  } else {

		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));

		    return(mapping.findForward("invRepPhysicalInventoryPrint"));

		 }

     } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
          if(log.isInfoEnabled()) {

             log.info("Exception caught in InvRepPhysicalInventoryPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());

          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
