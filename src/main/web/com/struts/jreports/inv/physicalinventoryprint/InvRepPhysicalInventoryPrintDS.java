package com.struts.jreports.inv.physicalinventoryprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.jreports.ap.receivingreportprint.ApRepReceivingReportPrintData;
import com.struts.jreports.inv.adjustmentrequestprint.InvRepAdjustmentRequestPrintData;
import com.struts.util.Common;
import com.util.InvRepPhysicalInventoryPrintDetails;

public class InvRepPhysicalInventoryPrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepPhysicalInventoryPrintDS(ArrayList list) {
       
       Iterator i = list.iterator();
       
       while(i.hasNext()) {
           
           InvRepPhysicalInventoryPrintDetails details = (InvRepPhysicalInventoryPrintDetails)i.next();
           
           InvRepPhysicalInventoryPrintData invRepADJData = new InvRepPhysicalInventoryPrintData(
        		   Common.convertSQLDateToString(details.getPipPiDate()), details.getPipPiReferenceNumber(),
                   details.getPipPiGlCoaAccount(), details.getPipPiGlCoaAccountDesc(), details.getPipPiDescription(),details.getPipPiCreatedBy(),
                   details.getPipPlIiName(), details.getPipPlIiDescription(), details.getPipPlUomName(), details.getPipPlLocName(), 
                   new Double(details.getPipPlEndingInventory()), new Double(details.getPipPlWastage()), new Double(details.getPipPlVariance()), new Double(details.getPipPlUnitCost())
                   );
           
           data.add(invRepADJData);
           
       }   	
       
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();
      
      if("date".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getDate();
      }else if("referenceNumber".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getReferenceNumber();
      }else if("adjustmentAccount".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getAccountNumber();
      }else if("accountDescription".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getAccountDescription();
      }else if("description".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getDescription(); 
       }else if("createdBy".equals(fieldName)){
           value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getCreatedBy(); 
      }else if("itemName".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getItemName();       
      }else if("itemDescription".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getItemDescription();
      }else if("unitMeasureShortName".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getUnitMeasureShortName();
      }else if("location".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getLocationName();
      }else if("endingInventory".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getEndingInventory();
      }else if("wastage".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getWastage();
      }else if("variance".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getVariance();
 
      }else if("unitCost".equals(fieldName)){
          value = ((InvRepPhysicalInventoryPrintData)data.get(index)).getUnitCost();
      
      }
      
      return(value);
   }
}
