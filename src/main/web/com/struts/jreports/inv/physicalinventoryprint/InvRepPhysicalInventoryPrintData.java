package com.struts.jreports.inv.physicalinventoryprint;

public class InvRepPhysicalInventoryPrintData implements java.io.Serializable {
    
    private String date = null;
    private String referenceNumber = null;
    private String accountNumber = null;
    private String accountDescription = null;
    private String description = null;

    private String createdBy = null;
    private String itemName = null;
    private String itemDescription = null;
    private String unitMeasureShortName = null;
    private String locationName = null;
    private Double endingInventory = null;
    private Double wastage = null;
    private Double variance = null;
    private Double unitCost = null;

	    
    public InvRepPhysicalInventoryPrintData(String date, String referenceNumber, 
            String accountNumber, String accountDescription, String description,String createdBy,
            String itemName, String itemDescription, String unitMeasureShortName, String locationName,
            Double endingInventory, Double wastage, Double variance, Double unitCost){
                

        this.date = date;
        this.referenceNumber = referenceNumber;
        this.accountNumber = accountNumber;
        this.accountDescription = accountDescription;
        this.description = description;
        this.createdBy = createdBy;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.unitMeasureShortName = unitMeasureShortName;
        this.locationName = locationName;
        
        this.endingInventory = endingInventory;
        this.wastage = wastage;
        this.variance = variance;
        this.unitCost = unitCost;

    }
    
    
    public String getDate() {
        
        return date;
        
    }

    public String getReferenceNumber() {
        
        return referenceNumber;
        
    }
    
    public String getAccountNumber() {
        
        return accountNumber;
        
    }
    
    public String getAccountDescription() {
        
        return accountDescription;
        
    }
    
    public String getDescription() {
        
        return description;
        
    }

  public String getCreatedBy() {
      
      return createdBy;
      
  }
  

    public String getItemName() {
        
        return itemName;
        
    }
    
    public String getItemDescription() {
        
        return itemDescription;
        
    }
    
    public String getUnitMeasureShortName() {
        
        return unitMeasureShortName;
        
    }
    
    public String getLocationName() {
        
        return locationName;
        
    }

    public Double getEndingInventory() {
        
        return endingInventory;
        
    }
    
    public Double getWastage() {
        
        return wastage;
        
    }
    
    public Double getVariance() {
        
        return variance;
        
    }
    
    
    public Double getUnitCost() {
        
        return unitCost;
        
    }

	   
}
