package com.struts.jreports.inv.physicalinventoryworksheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepPhysicalInventoryWorksheetController;
import com.ejb.txn.InvRepPhysicalInventoryWorksheetControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepItemListDetails;

public final class InvRepPhysicalInventoryWorksheetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          
          if (user != null) {
              
              if (log.isInfoEnabled()) {
                  
                  log.info("InvRepPhysicalInventoryWorksheetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                          "' performed this action on session " + session.getId());
                  
              }
              
          } else {
              
              if (log.isInfoEnabled()) {
                  
                  log.info("User is not logged on in session" + session.getId());
                  
              }
              
              return(mapping.findForward("adLogon"));
              
          }
          
          InvRepPhysicalInventoryWorksheetForm actionForm = (InvRepPhysicalInventoryWorksheetForm)form;
          
          // reset report to null
          actionForm.setReport(null);
          
          String frParam = Common.getUserPermission(user, Constants.INV_REP_PHYSICAL_INVENTORY_WORKSHEET_ID);
          
          if (frParam != null) {
              
              if (frParam.trim().equals(Constants.FULL_ACCESS)) {
                  
                  ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
                  if (!fieldErrors.isEmpty()) {
                      
                      saveErrors(request, new ActionMessages(fieldErrors));
                      
                      return mapping.findForward("invRepPhysicalInventoryWorksheet");
                  }
                  
              }
              
              actionForm.setUserPermission(frParam.trim());
              
          } else {
              
              actionForm.setUserPermission(Constants.NO_ACCESS);
              
          }
          
/*******************************************************
   Initialize InvRepPhysicalInventoryWorksheetController EJB
*******************************************************/

         InvRepPhysicalInventoryWorksheetControllerHome homePIW = null;
         InvRepPhysicalInventoryWorksheetController ejbPIW = null;       

         try {
         	
            homePIW = (InvRepPhysicalInventoryWorksheetControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepPhysicalInventoryWorksheetControllerEJB", InvRepPhysicalInventoryWorksheetControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepInvRepPhysicalInventoryWorksheetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbPIW = homePIW.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepInvRepPhysicalInventoryWorksheetAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 
/*******************************************************
   -- INV PIW Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            ArrayList list = null;
            InvRepItemListDetails details = null;
            
            String company = null;

             // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("itemCategory", actionForm.getCategory());
	        	}
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getLocation())) {
	        		
	        		criteria.put("location", actionForm.getLocation());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBranch())) {
	        		
	        		criteria.put("branch", actionForm.getBranch());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        		
	        		criteria.put("itemClass", actionForm.getItemClass());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomer())) {
	        		
	        		criteria.put("customer", actionForm.getCustomer());
	        	}

	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
			try {
		    
		       // get company
		       
		       AdCompanyDetails adCmpDetails = ejbPIW.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       
		       // execute report
		       System.out.println("getIncludeIntransit(): "+actionForm.getIncludeIntransit());
		    		    
		       list = ejbPIW.executeInvRepPhysicalInventoryWorksheet(actionForm.getCriteria(),
            	    Common.convertStringToSQLDate(actionForm.getDate()), actionForm.getIncludeUnposted(), 
            	    actionForm.getIncludeEncoded(), user.getCmpCode(), actionForm.getIncludeIntransit());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("physicalInventoryWorksheet.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepInvRepPhysicalInventoryWorksheetAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("invRepPhysicalInventoryWorksheet"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", Common.convertSQLDateToString(new Date()));
		    parameters.put("category", actionForm.getCategory());
		    parameters.put("location", actionForm.getLocation());
		    parameters.put("branch", actionForm.getBranch());
		    parameters.put("date", actionForm.getDate());
		    parameters.put("company", company);
		    parameters.put("viewType", actionForm.getViewType());
		    parameters.put("itemClass", actionForm.getItemClass());
		    
			if (actionForm.getIncludeUnposted()){
				
				parameters.put("includeUnposted", "yes");
				
			} else {
				
				parameters.put("includeUnposted", "no");
				
			}
			
			if (actionForm.getIncludeEncoded()){

				parameters.put("includeEncoded", "yes");

			} else {

				parameters.put("includeEncoded", "no");

			}
			
			if (actionForm.getIncludeIntransit()){

				parameters.put("includeIntransit", "yes");

			} else {

				parameters.put("includeIntransit", "no");

			}
		    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepPhysicalInventory.jasper";
		       
		    if (!new java.io.File(filename).exists()) {
		    		    		    
		       filename = servlet.getServletContext().getRealPath("jreports/InvRepPhysicalInventory.jasper");
			    
		    }
		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		    
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepPhysicalInventoryWorksheetDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
				   System.out.println("filename-" + filename);
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new InvRepPhysicalInventoryWorksheetDS(list)));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
			       report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new InvRepPhysicalInventoryWorksheetDS(list)));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	ex.printStackTrace();
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepPhysicalInventoryWorksheetAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV PIW Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV PIW Load Action --
*******************************************************/
		          
		     }
		     
         if(frParam != null) {
             
             try {
                 
                 ArrayList list = null;			       			       
                 Iterator i = null;
                 
                 actionForm.clearCategoryList();           	
                 
                 list = ejbPIW.getAdLvInvItemCategoryAll(user.getCmpCode());
                 
                 if (list == null || list.size() == 0) {
                     
                     actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
                     
                 } else {
                     
                     i = list.iterator();
                     
                     while (i.hasNext()) {
                         
                         actionForm.setCategoryList((String)i.next());
                         
                     }
                     
                 }

                 actionForm.clearCustomerList();           	

                 list = ejbPIW.getInvCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                	 actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                	 i = list.iterator();

                	 while (i.hasNext()) {

                		 actionForm.setCustomerList((String)i.next());

                	 }

                 }
                 
                 actionForm.clearLocationList();           	
                 
                 list = ejbPIW.getInvLocAll(user.getCmpCode());
                 
                 if (list == null || list.size() == 0) {
                     
                     actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
                     
                 } else {
                	 actionForm.setLocationList("ALL");   
                     i = list.iterator();
                     
                     while (i.hasNext()) {
                         
                         actionForm.setLocationList((String)i.next());
                         
                     }
                     
                 }
                 
                 actionForm.clearBranchList();           	
                 
                 list = ejbPIW.getAdBrAll(user.getCmpCode());
                 
                 if (list == null || list.size() == 0) {
                     
                     actionForm.setBranchList(Constants.GLOBAL_NO_RECORD_FOUND);
                     
                 } else {
                     
                     i = list.iterator();
                     
                     while (i.hasNext()) {
                         
                         actionForm.setBranchList((String)i.next());
                         
                     }
                     
                 }
                 
             } catch(EJBException ex) {
                 
                 if(log.isInfoEnabled()) {
                     log.info("EJBException caught in InvRepPhysicalInventoryPrintAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     
                 }
                 
                 return(mapping.findForward("cmnErrorPage"));
                 
             }
             
             actionForm.reset(mapping, request);
             actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
             return(mapping.findForward("invRepPhysicalInventoryWorksheet"));		          
             
         } else {
             
             errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
             saveErrors(request, new ActionMessages(errors));
             
             return(mapping.findForward("cmnMain"));
             
         }
         
      } catch(Exception e) {
          System.out.println("STACKKKKKKK");
          e.printStackTrace(); 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepPhysicalInventoryPrintAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
