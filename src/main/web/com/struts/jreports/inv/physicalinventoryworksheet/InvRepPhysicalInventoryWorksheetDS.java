package com.struts.jreports.inv.physicalinventoryworksheet;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepPhysicalInventoryWorksheetDetails;

public class InvRepPhysicalInventoryWorksheetDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepPhysicalInventoryWorksheetDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         InvRepPhysicalInventoryWorksheetDetails details = (InvRepPhysicalInventoryWorksheetDetails)i.next();
                  
         System.out.println("details.getPiwPilActualQty() "+details.getPiwPilActualQty());
         System.out.println("details.getPiwPilEndingInv() "+details.getPiwPilEndingInv());
	     InvRepPhysicalInventoryWorksheetData dtbData = new InvRepPhysicalInventoryWorksheetData(
	     		details.getPiwPilItemName(), details.getPiwPilItemDescription(), details.getPiwPilItemCategory(), details.getPiwPilUnitMeasureName(),
				new Double(details.getPiwPilActualQty()), new Double(details.getPiwPilEndingInv()), 
				new Double(details.getPiwPilWastage()), new Double(details.getPiwPilVariance()), 
				new Double(details.getPiwPilActualCost()), new Double(details.getPiwPilSalesPrice()));
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("itemName".equals(fieldName)) {

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getItemName();

      } else if("itemDescription".equals(fieldName)) {

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getItemDescription();
    	  
      } else if("itemCategory".equals(fieldName)) {

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getItemCategory();

      } else if("unitMeasure".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getUnitMeasure();

      } else if("actualQty".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getActualQty();

      } else if("endingInventory".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getEndingInventory();
    	  

      }else if("wastage".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getWastage();
    	  

      }else if("variance".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getVariance();
    	  

      }else if("actualCost".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getActualCost();
    	  
      }else if("salesPrice".equals(fieldName)){

    	  value = ((InvRepPhysicalInventoryWorksheetData)data.get(index)).getSalesPrice();
    	  
      }

      return(value);
   }
}
