package com.struts.jreports.inv.physicalinventoryworksheet;

public class InvRepPhysicalInventoryWorksheetData implements java.io.Serializable {
	
	private String itemName = null;
	private String itemDescription = null;
	private String itemCategory = null;
	private String unitMeasure = null;
	private Double actualQty = null;
	private Double endingInventory = null;
	private Double wastage = null;
	private Double variance = null;
	private Double actualCost = null;
	private Double salesPrice = null;
	
	
	private String enable = null;
	
	public InvRepPhysicalInventoryWorksheetData(String itemName, String itemDescription, String itemCategory, String unitMeasure, Double actualQty,
			Double endingInventory, Double wastage, Double variance, Double actualCost, Double salesPrice) {
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemCategory = itemCategory;
		this.unitMeasure = unitMeasure;
		this.actualQty = actualQty;
		this.endingInventory = endingInventory;
		this.wastage = wastage;
		this.variance = variance;
		this.actualCost = actualCost;
		this.salesPrice = salesPrice;
		
	}
	
	public String getItemName() {
		
		return(itemName);
		
	}
	
	public String getItemDescription() {
		
		return(itemDescription);
		
	}
	
	public String getItemCategory() {
		
		return(itemCategory);
		
	}
	
	public String getUnitMeasure() {
		
		return(unitMeasure);
		
	}
	
	public Double getActualQty() {
		
		return(actualQty);
		
	}
	
	public Double getEndingInventory() {

		return(endingInventory);

	}
	public Double getWastage() {

		return(wastage);

	}
	public Double getVariance() {

		return(variance);

	}
	
	public Double getActualCost() {

		return(actualCost);

	}
	
	public Double getSalesPrice() {

		return(salesPrice);

	}

}
