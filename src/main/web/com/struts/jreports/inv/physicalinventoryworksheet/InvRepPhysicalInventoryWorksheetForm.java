package com.struts.jreports.inv.physicalinventoryworksheet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepPhysicalInventoryWorksheetForm extends ActionForm implements Serializable{
    
    private String date = null;
    private String category = null;
    private ArrayList categoryList = new ArrayList();
    private String location = null;
    private ArrayList locationList = new ArrayList();
    private String branch = null;
    private ArrayList branchList = new ArrayList();
    private String viewType = null;
    private ArrayList viewTypeList = new ArrayList();
    private String customer = null;
    private ArrayList customerList = new ArrayList();
    
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private boolean includeUnposted = false;
	private boolean includeEncoded = false;
	private boolean includeIntransit = false;
	
    private String report = null;
    private String goButton = null;
    private String closeButton = null;
    
    private HashMap criteria = new HashMap();
    
    private String userPermission = new String();
    
    public HashMap getCriteria() {
        
        return criteria;
        
    }
    
    public void setCriteria(HashMap criteria) {
        
        this.criteria = criteria;
        
    }
    
    public void setGoButton(String goButton){
        this.goButton = goButton;
    }
    
    public void setCloseButton(String closeButton){
        this.closeButton = closeButton;
    }
    
    public String getCategory(){
        return(category);
    }
    
    public void setCategory(String category){
        this.category = category;
    }
    
    public ArrayList getCategoryList(){
        return(categoryList);
    }
    
    public void setCategoryList(String category){
        categoryList.add(category);
    }
    
    public void clearCategoryList(){
        categoryList.clear();
        categoryList.add(Constants.GLOBAL_BLANK);
    }
    
    public String getViewType(){
        return(viewType);   	
    }
    
    public void setViewType(String viewType){
        this.viewType = viewType;
    }
    
    public ArrayList getViewTypeList(){
        return viewTypeList;
    }
    
    public String getCustomer(){
        return(customer);
    }
    
    public void setCustomer(String customer) {

    	this.customer = customer;

    }

    public ArrayList getCustomerList() {

    	return customerList;

    }

    public void setCustomerList(String customer) {

    	customerList.add(customer);

    }

    public void clearCustomerList() {

    	customerList.clear();
    	customerList.add(Constants.GLOBAL_BLANK);

    }
    
    public String getReport() {
        
        return report;
        
    }
    
    public void setReport(String report) {
        
        this.report = report;
        
    }
    
    public String getUserPermission(){
        return(userPermission);
    }
    
    public void setUserPermission(String userPermission){
        this.userPermission = userPermission;
    }
    
    public String getLocation(){
        return(location);
    }
    
    public void setLocation(String location){
        this.location = location;
    }
    
    public ArrayList getLocationList(){
        return(locationList);
    }
    
    public void setLocationList(String location){
        locationList.add(location);
    }
    
    public void clearLocationList(){
        locationList.clear();
        locationList.add(Constants.GLOBAL_BLANK);
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public void setDate(String date) {
        
        this.date = date;
        
    }
    
    public String getBranch(){
        return(branch);
    }
    
    public void setBranch(String branch){
        this.branch = branch;
    }
    
    public ArrayList getBranchList(){
        return(branchList);
    }
    
    public void setBranchList(String branch){
        branchList.add(branch);
    }
    
    public void clearBranchList(){
        branchList.clear();
        branchList.add(Constants.GLOBAL_BLANK);
    }
    
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public ArrayList getItemClassList() {
		
		return itemClassList;
		
	}
	
	public boolean getIncludeUnposted() {
		
		return includeUnposted;
	}
	
	public void setIncludeUnposted (boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}
	
	public boolean getIncludeEncoded() {
		
		return includeEncoded;
	}
	
	public void setIncludeEncoded (boolean includeEncoded) {
		
		this.includeEncoded = includeEncoded;
		
	}
	
	public boolean getIncludeIntransit() {
		
		return includeIntransit;
	}
	
	public void setIncludeIntransit (boolean includeIntransit) {
		
		this.includeIntransit = includeIntransit;
		
	}

    public void reset(ActionMapping mapping, HttpServletRequest request){      
     
        category = Constants.GLOBAL_BLANK;
        location = Constants.GLOBAL_BLANK;
        branch = Constants.GLOBAL_BLANK;
        date = null;
        
        customer = Constants.GLOBAL_BLANK;
        goButton = null;
        closeButton = null;
        viewTypeList.clear();
        viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
        viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
        viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
        viewType = Constants.REPORT_VIEW_TYPE_PDF;
        
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add("Stock");
		itemClassList.add("Assembly");
		itemClass = Constants.GLOBAL_BLANK;

		includeUnposted = false;
		includeIntransit = false;
    }
    
    public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
        ActionErrors errors = new ActionErrors();
        
        if (request.getParameter("goButton") != null) {
            
            if(Common.validateRequired(date)) {
                
                errors.add("date", new ActionMessage("physicalInventoryWorksheet.error.dateRequired"));
                
            }
            
            if(!Common.validateDateFormat(date)) {
                
                errors.add("date", new ActionMessage("physicalInventoryWorksheet.error.dateInvalid"));
                
            }
            
            if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
                
                errors.add("location", new ActionMessage("physicalInventoryWorksheet.error.locationRequired"));
                
            }
            
           
            if(Common.validateRequired(branch) || branch.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
                
                errors.add("branch", new ActionMessage("physicalInventoryWorksheet.error.branchRequired"));
                
            }
                        
        }
        
        return(errors);
    }
}
