package com.struts.jreports.inv.reorderitems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepReorderItemsController;
import com.ejb.txn.InvRepReorderItemsControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepReorderItemsAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {
      	
/*******************************************************
      	 Check if user has a session
*******************************************************/
      	
      	User user = (User) session.getAttribute(Constants.USER_KEY);
      	
      	if (user != null) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("InvRepReorderItemsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
      					"' performed this action on session " + session.getId());
      			
      		}
      		
      	} else {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("User is not logged on in session" + session.getId());
      			
      		}
      		
      		return(mapping.findForward("adLogon"));
      		
      	}
      	
      	InvRepReorderItemsForm actionForm = (InvRepReorderItemsForm)form;
      	
      	// reset report to null
      	actionForm.setReport(null);
      	
      	String frParam = Common.getUserPermission(user, Constants.INV_REP_REORDER_ITEMS_ID);
      	
      	if (frParam != null) {
      		
      		if (frParam.trim().equals(Constants.FULL_ACCESS)) {
      			
      			ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
      			if (!fieldErrors.isEmpty()) {
      				
      				saveErrors(request, new ActionMessages(fieldErrors));
      				
      				return mapping.findForward("invRepReorderItems");
      			}
      			
      		}
      		
      		actionForm.setUserPermission(frParam.trim());
      		
      	} else {
      		
      		actionForm.setUserPermission(Constants.NO_ACCESS);
      		
      	}

/*******************************************************
      	 Initialize InvRepReorderItemsController EJB
*******************************************************/
      	
      	InvRepReorderItemsControllerHome homeRI = null;
      	InvRepReorderItemsController ejbRI = null;       
      	
      	try {
      		
      		homeRI = (InvRepReorderItemsControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/InvRepReorderItemsControllerEJB", InvRepReorderItemsControllerHome.class);
      		
      	} catch(NamingException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("NamingException caught in InvRepReorderItemsAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	try {
      		
      		ejbRI = homeRI.create();
      		
      	} catch(CreateException e) {
      		
      		if(log.isInfoEnabled()) {
      			
      			log.info("CreateException caught in InvRepReorderItemsAction.execute(): " + e.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	}
      	
      	ActionErrors errors = new ActionErrors();
      	
      	/*** get report session and if not null set it to null **/
      	
      	Report reportSession = 
      		(Report)session.getAttribute(Constants.REPORT_KEY);
      	
      	if(reportSession != null) {
      		
      		reportSession.setBytes(null);
      		session.setAttribute(Constants.REPORT_KEY, reportSession);
      		
      	}
      	
/*******************************************************
      	 -- AP RI Go Action --
*******************************************************/
      	
      	if(request.getParameter("goButton") != null &&
      			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      		
      		ArrayList list = null;
      		
      		String company = null;
      		
      		// create criteria 
      		
      		if (request.getParameter("goButton") != null) {
      			
      			HashMap criteria = new HashMap();      

      			if (!Common.validateRequired(actionForm.getItemName())) {
      				
      				criteria.put("itemName", actionForm.getItemName());
      				
      			}
      			
      			if (!Common.validateRequired(actionForm.getSupplier())) {
      				
      				criteria.put("supplier", actionForm.getSupplier());
      				
      			}
      			
      			if (!Common.validateRequired(actionForm.getCategory())) {
      				
      				criteria.put("itemCategory", (actionForm.getCategory()));
      				
      			}
      			
      			if (!Common.validateRequired(actionForm.getItemClass())) {
      				
      				criteria.put("itemClass", (actionForm.getItemClass()));
      				
      			}
      			
      			if (!Common.validateRequired(actionForm.getLocation())) {
      				
      				criteria.put("location", (actionForm.getLocation()));
      				
      			}	        		        
      			
      			if (!Common.validateRequired(actionForm.getLocationType())) {
      				
      				criteria.put("locationType", (actionForm.getLocationType()));
      				
      			}
      			
      			// save criteria
      			
      			actionForm.setCriteria(criteria);
      			
      		}
      		
      		
      		try {
      			
      			// get company
      			
      			AdCompanyDetails adCmpDetails = ejbRI.getAdCompany(user.getCmpCode());
      			company = adCmpDetails.getCmpName();
      			
				//get all InvUsageVarianceBranchLists
				
				ArrayList branchList = new ArrayList();
				
				for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {
					
					InvRepReorderItemsBranchList brList = (InvRepReorderItemsBranchList)actionForm.getInvBrIlListByIndex(i);
					
					if(brList.getBranchCheckbox() == true) {
					
						AdBranchDetails mdetails = new AdBranchDetails();
						mdetails.setBrAdCompany(user.getCmpCode());
						mdetails.setBrCode(brList.getBranchCode());
						branchList.add(mdetails);
						
					}
					
				}
				
				// execute report
      			
      			list = ejbRI.executeInvRepReorderItems(actionForm.getCriteria(),actionForm.getOrderBy(), actionForm.getGroupBy(), actionForm.getType(), branchList, new Integer(user.getCurrentBranch().getBrCode()),  user.getCmpCode());
      			
      		} catch (GlobalNoRecordFoundException ex) {
      			
      			errors.add(ActionMessages.GLOBAL_MESSAGE,
      					new ActionMessage("reorderItem.error.noRecordFound"));
      			
      		} catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in InvRepReorderItemsAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}
      		
      		if (!errors.isEmpty()) {
      			
      			saveErrors(request, new ActionMessages(errors));
      			return mapping.findForward("invRepReorderItems");
      			
      		}
      		
      		
      		// fill report parameters, fill report to pdf and set report session
      		
      		Map parameters = new HashMap();
      		parameters.put("company", company);
      		parameters.put("dateToday", new java.util.Date());
      		parameters.put("printedBy", user.getUserName());
      		parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));
      		parameters.put("viewType", actionForm.getViewType());
      		
      		if (actionForm.getItemName() != null) {
      			
      			parameters.put("itemName", actionForm.getItemName());
      			
      		}
      		
      		if (actionForm.getCategory() != null) {
      			
      			parameters.put("itemCategory", actionForm.getCategory());
      			
      		}
      		
      		if (actionForm.getItemClass() != null) {
      			
      			parameters.put("itemClass", actionForm.getItemClass());
      			
      		}
      		
      		if (actionForm.getLocation() != null) {
      			
      			parameters.put("location", actionForm.getLocation());
      			
      		}	        		        
      		
      		if (actionForm.getLocationType() != null) {
      			
      			parameters.put("locationType", actionForm.getLocationType());
      			
      		}	 
      		
      		if (actionForm.getType() != null) {

      			parameters.put("type", actionForm.getType());

      		}
      		
      		if (actionForm.getOrderBy() != null) {

      			parameters.put("orderBy", actionForm.getOrderBy());

      		}
      		
      		if (actionForm.getGroupBy() != null) {

      			parameters.put("groupBy", actionForm.getGroupBy());

      		}
      		
      		if (actionForm.getSupplier() != null) {
      			
      			parameters.put("supplier", actionForm.getSupplier());
      			
      		}
      		
      		String branchMap = null;
      		boolean first = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepReorderItemsBranchList brList = (InvRepReorderItemsBranchList)actionForm.getInvBrIlListByIndex(j);
      		
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
      		
      		String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepReorderItems.jasper";
      		
      		if (!new java.io.File(filename).exists()) {
      			
      			filename = servlet.getServletContext().getRealPath("jreports/InvRepReorderItems.jasper");
      			
      		}	    	
      		
      		try {
      			
      			Report report = new Report();
      			
      			if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
      				report.setBytes(
      						JasperRunManager.runReportToPdf(filename, parameters, 
      								new InvRepReorderItemsDS(list, actionForm.getGroupBy())));   
      				
      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
      				report.setBytes(
      						JasperRunManagerExt.runReportToXls(filename, parameters, 
      								new InvRepReorderItemsDS(list, actionForm.getGroupBy())));   
      				
      			} else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
      				
      				report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
      				report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
      						new InvRepReorderItemsDS(list, actionForm.getGroupBy())));												    
      				
      			}
      			
      			session.setAttribute(Constants.REPORT_KEY, report);
      			actionForm.setReport(Constants.STATUS_SUCCESS);		    	
      			
      		} catch(Exception ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("Exception caught in InvRepReorderItemsAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}		    	    
      		
/*******************************************************
      		 -- AP RI Close Action --
*******************************************************/
      		
      	} else if (request.getParameter("closeButton") != null) {
      		
      		return(mapping.findForward("cmnMain"));
      		
/*******************************************************
      		 -- AP RI Load Action --
*******************************************************/
      		
      	}
      	
      	if(frParam != null) {
      		
      		
      		try {
      			
      			actionForm.reset(mapping, request);
      			
      			ArrayList list = null;			       			       
      			Iterator i = null;
      			
      			actionForm.clearCategoryList();           	
      			
      			list = ejbRI.getAdLvInvItemCategoryAll(user.getCmpCode());
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setCategoryList((String)i.next());
      					
      				}
      				
      			}
      			
      			actionForm.clearLocationTypeList();           	
      			
      			list = ejbRI.getAdLvInvLocationTypeAll(user.getCmpCode());
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setLocationTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setLocationTypeList((String)i.next());
      					
      				}
      				
      			}
      			
      			actionForm.clearLocationList();           	
      			
      			list = ejbRI.getInvLocAll(user.getCmpCode());
      			
      			
      			if (list == null || list.size() == 0) {
      				
      				actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
      				
      			} else {
      				
      				i = list.iterator();
      				
      				while (i.hasNext()) {
      					
      					actionForm.setLocationList((String)i.next());

      				}
      				
      			}
				actionForm.clearInvBrIlList();
				
				list = ejbRI.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode()); 
				
				i = list.iterator();
				
				while(i.hasNext()) {
					
					AdBranchDetails details = (AdBranchDetails)i.next();
					
					InvRepReorderItemsBranchList invBrIlList = new InvRepReorderItemsBranchList(actionForm,
							details.getBrBranchCode(), details.getBrName(), details.getBrCode());
					invBrIlList.setBranchCheckbox(false);
					
					actionForm.saveInvBrIlList(invBrIlList);
					
				}			       

      		} catch(EJBException ex) {
      			
      			if(log.isInfoEnabled()) {
      				log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
      						" session: " + session.getId());
      				
      			}
      			
      			return(mapping.findForward("cmnErrorPage"));
      			
      		}

          	          	
          	return(mapping.findForward("invRepReorderItems"));	
      		
      	} else {

      		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("cmnMain"));
		
		 }

      } catch(Exception e) { 
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in InvRepReorderItemsAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
