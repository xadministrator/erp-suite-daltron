package com.struts.jreports.inv.reorderitems;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepReorderItemsDetails;

public class InvRepReorderItemsDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepReorderItemsDS(ArrayList list, String groupBy) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         InvRepReorderItemsDetails details = (InvRepReorderItemsDetails)i.next();
	         
	         String group = new String("");

	         if (groupBy.equals("ITEM NAME")) {

	        	 group = details.getRiItemName();

	         } 
	         
	         InvRepReorderItemsData argData = new InvRepReorderItemsData( details.getRiItemName(),
	         		details.getRiItemDescription(), details.getRiLocation(), details.getRiUomName(), details.getRiItemClass(),
					new Double(details.getRiQuantity()), new Double(details.getRiReorderPoint()),
					new Double(details.getRiReorderQuantity()), group, details.getRiSupplierCode(),details.getRiBranch());

	         data.add(argData);
	         
      }
      
   }
   
   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getLocation();
   		
   	} else if("itemClass".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getItemClass();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getQuantity();
   		
   	} else if("itemReorderPoint".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getItemReorderPoint();
   		
   	} else if("itemReorderQuantity".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getItemReorderQuantity();
   		
   	} else if("groupBy".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getGroupBy();
   		
   	} else if("uom".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getUom();
   		
   	} else if("supplier".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getSupplier();
   		
   	} else if("branch".equals(fieldName)) {
   		
   		value = ((InvRepReorderItemsData)data.get(index)).getBranch();
   		
   	}
   	
   	return(value);
   	
   }
   
}
