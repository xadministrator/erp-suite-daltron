package com.struts.jreports.inv.reorderitems;


public class InvRepReorderItemsData implements java.io.Serializable {

	private String itemName;
	private String itemDescription;
	private String itemClass;	
	private String location;
	private String uom;
	private Double quantity;
	private Double itemReorderPoint;
	private Double itemReorderQuantity;
	private String groupBy;
	private String supplier;
	private String branch;

	public InvRepReorderItemsData( String itemName, String itemDescription, String location, String uom,
			String itemClass, Double quantity, Double itemReorderPoint, 
			Double itemReorderQuantity, String groupBy, String supplier, String branch) {

		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.location = location;
		this.uom = uom;
		this.itemClass = itemClass;
		this.quantity = quantity;
		this.itemReorderPoint = itemReorderPoint;
		this.itemReorderQuantity = itemReorderQuantity;	
		this.groupBy = groupBy;
		this.supplier = supplier;
		this.branch = branch;
	}

	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public Double getItemReorderQuantity() {
		
		return itemReorderQuantity;
	
	}
	
	public void setAmount(Double itemReorderQuantity) {
	
		this.itemReorderQuantity = itemReorderQuantity;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDescription) {
	
		this.itemDescription = itemDescription;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public String getLocation() {
	
		return location;
	
	}
	
	public void setLocation(String location) {
	
		this.location = location;
	
	}
	
	public String getUom() {
		
		return uom;
	
	}
	
	public void setUom(String uom) {
	
		this.uom = uom;
	
	}
	
	public Double getItemReorderPoint() {
	
		return itemReorderPoint;
	
	}
	
	public void setItemReorderPoint(Double itemReorderPoint) {
	
		this.itemReorderPoint = itemReorderPoint;
	
	}
	
	public String getGroupBy() {
		
		return groupBy;
		
	}
	
	public void setGroupBy(String groupBy) {
		
		this.groupBy = groupBy;
		
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
} // InvRepReorderItemsData class
