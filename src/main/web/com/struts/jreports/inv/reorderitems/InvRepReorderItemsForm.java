package com.struts.jreports.inv.reorderitems;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class InvRepReorderItemsForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String supplier = null;
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String locationType = null;
	private ArrayList locationTypeList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private ArrayList invBrIlList = new ArrayList(); 
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private HashMap criteria = new HashMap();
	private String userPermission = new String();


	public HashMap getCriteria() {

		return criteria;

	}

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public String getItemName() {
		
		return (itemName);
		
	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	public String getCategory() {

		return (category);

	}

	public void setCategory(String category) {

		this.category = category;

	}

	public ArrayList getCategoryList() {
		
		return (categoryList);
		
	}

	public void setCategoryList(String category) {
		
		categoryList.add(category);
		
	}

	public void clearCategoryList() {
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}

	public String getItemClass() {

		return (itemClass);

	}

	public void setItemClass(String itemClass) {

		this.itemClass = itemClass;

	}

	public ArrayList getItemClassList() {

		return (itemClassList);

	}

	public void setItemClassList(String itemClass) {

		itemClassList.add(itemClass);

	}

	public void clearItemClassList() {

		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);

	}

	public String getLocationType() {

		return (locationType);

	}

	public void setLocationType(String locationType) {

		this.locationType = locationType;

	}

	public ArrayList getLocationTypeList() {

		return (locationTypeList);

	}

	public void setLocationTypeList(String locationType) {

		locationTypeList.add(locationType);

	}

	public void clearLocationTypeList() {

		locationTypeList.clear();
		locationTypeList.add(Constants.GLOBAL_BLANK);

	}

	public String getLocation() {
		
		return (location);
		
	}

	public void setLocation(String location) {
		
		this.location = location;
		
	}

	public ArrayList getLocationList() {
		
		return (locationList);
		
	}

	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}

	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}

	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}

	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}

	public String getViewType() {
		
		return (viewType);
		
	}

	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}

	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getUserPermission() {
		
		return (userPermission);
		
	}

	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}

	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepReorderItemsBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepReorderItemsBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public ArrayList getTypeList() {

		return (typeList);

	}

	public void setTypeList(String type) {

		typeList.add(type);

	}

	public String getGroupBy (){

		return(groupBy );

	}

	public void setGroupBy (String groupBy ){

		this.groupBy  = groupBy ;

	}

	public ArrayList getGroupByList(){

		return(groupByList);

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepReorderItemsBranchList list = (InvRepReorderItemsBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		goButton = null;
		closeButton = null;

		category = null;
		locationType = null;
		location = null;
		itemName=null;
		supplier=null;

		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add(Constants.INV_REP_REORDER_ITEMS_ITEM_CLASS_STOCK);
		itemClassList.add(Constants.INV_REP_REORDER_ITEMS_ITEM_CLASS_ASSEMBLY);
		itemClass = null;
				
		orderByList.clear();
		orderByList.add(Constants.INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_CLASS);
		orderBy = Constants.INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_NAME;
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;

		typeList.clear();
		typeList.add("ALL");
		typeList.add("SELLING ITEMS");
		typeList.add("NON-SELLING ITEMS");
		typeList.add("NO ACTIVITY");
		type = "ALL";
		
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add("ITEM NAME");
		groupBy = "ITEM NAME";
		
	
	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return (errors);

	}

}
