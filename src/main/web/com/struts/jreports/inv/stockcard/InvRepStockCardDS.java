package com.struts.jreports.inv.stockcard;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepStockCardDetails;

public class InvRepStockCardDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepStockCardDS(ArrayList list) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
                    InvRepStockCardDetails details = (InvRepStockCardDetails)i.next();

                    InvRepStockCardData dtbData = new InvRepStockCardData(details.getRscItemName(), details.getRscUnit(), Common.convertSQLDateToString(details.getRscDate()), 
                                    details.getRscDocumentNumber(), details.getRscReferenceNumber(), details.getRscSource(), details.getRscLocation(), new Double(details.getRscInQuantity()),
                                    new Double(details.getRscOutQuantity()), new Double(details.getRscRemainingQuantity()), new Double(details.getRscBeginningQuantity()), details.getRscAccount());

                    data.add(dtbData);
			
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("itemName".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getItemName();
			
		} else if("unit".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getUnit();
			
		} else if("date".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getDate();
			
		} else if("documentNumber".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getDocumentNumber();
			
		} else if("referenceNumber".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getReferenceNumber();
		
		} else if("source".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getSource();
                        
                } else if("location".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getLocation();
						
		} else if("inQuantity".equals(fieldName)) {
			
			value = ((InvRepStockCardData)data.get(index)).getInQuantity();
			
		} else if("outQuantity".equals(fieldName)){
			
			value = ((InvRepStockCardData)data.get(index)).getOutQuantity();
			
		} else if("remainingQuantity".equals(fieldName)){
			
			value = ((InvRepStockCardData)data.get(index)).getRemainingQuantity();
			
		} else if("beginningQuantity".equals(fieldName)){
			
			value = ((InvRepStockCardData)data.get(index)).getBeginningQuantity();
			
		} else if("account".equals(fieldName)){
			
			value = ((InvRepStockCardData)data.get(index)).getAccount();
			
		} 
		
		return(value);
		
	}
	
}
