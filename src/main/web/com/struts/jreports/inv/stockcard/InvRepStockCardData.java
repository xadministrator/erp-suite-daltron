package com.struts.jreports.inv.stockcard;

public class InvRepStockCardData implements java.io.Serializable {
	
	private String itemName = null;
	private String unit = null;
	private String date = null;
	private String documentNumber= null;
	private String referenceNumber = null;
	private String source = null;
        private String location = null;
	private Double inQuantity = null;
	private Double outQuantity = null;
	private Double remainingQuantity = null;
	private Double beginningQuantity = null;
	private String account = null;
		
	public InvRepStockCardData(String itemName, String unit, String date, String documentNumber, String referenceNumber, String source, String location,
			Double inQuantity, Double outQuantity, 	Double remainingQuantity, Double beginningQuantity, String account) {
		
		this.itemName = itemName;
		this.unit = unit;
		this.date = date;
		this.documentNumber= documentNumber;
		this.referenceNumber = referenceNumber;
		this.source = source;
                this.location = location;
		this.inQuantity = inQuantity;
		this.outQuantity = outQuantity;
		this.remainingQuantity = remainingQuantity;
		this.beginningQuantity = beginningQuantity;
		this.account = account;
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}

	public String getDate() {
		
		return date;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getSource() {
		
		return source;
		
	}
        
        public String getLocation() {
		
		return location;
		
	}
	
	public Double getInQuantity() {
		
		return inQuantity;
		
	}
		
	public Double getOutQuantity() {
		
		return outQuantity;
		
	}
	
	public Double getRemainingQuantity() {
		
		return remainingQuantity;
		
	}
		
	public Double getBeginningQuantity() {
		
		return beginningQuantity;
		
	}
	
	public String getAccount(){
		
		return account;
	}
	
}
