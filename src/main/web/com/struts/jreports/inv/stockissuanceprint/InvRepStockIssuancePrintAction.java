package com.struts.jreports.inv.stockissuanceprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepStockIssuancePrintController;
import com.ejb.txn.InvRepStockIssuancePrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepStockIssuancePrintDetails;

public final class InvRepStockIssuancePrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
          
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("InvRepStockIssuancePrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         InvRepStockIssuancePrintForm actionForm = (InvRepStockIssuancePrintForm)form; 
         
         String frParam = Common.getUserPermission(user, Constants.INV_REP_STOCK_ISSUANCE_PRINT_ID);
         
         if (frParam != null) {
            
             actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
             actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize InvRepStockIssuancePrintController EJB
*******************************************************/

         InvRepStockIssuancePrintControllerHome homeSIP = null;
         InvRepStockIssuancePrintController ejbSIP = null;       

         try {
         	
            homeSIP = (InvRepStockIssuancePrintControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepStockIssuancePrintControllerEJB", InvRepStockIssuancePrintControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in InvRepStockIssuancePrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbSIP = homeSIP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in InvRepStockIssuancePrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }          

/*******************************************************
   -- TO DO  --
*******************************************************/       

		if (frParam != null) {
	        
			    AdCompanyDetails adCmpDetails = null;
			    InvRepStockIssuancePrintDetails  details = null;
			    
			    ArrayList list = null;           
				    
				try {
					
					ArrayList siCodeList = new ArrayList();
					
					if (request.getParameter("forward") != null) {
												
	            		siCodeList.add(new Integer(request.getParameter("stockIssuanceCode")));
						
					}
	            	
	            	list = ejbSIP.executeInvStockIssuancePrint(siCodeList, user.getCmpCode());
	            	
			        // get company
			       
			        adCmpDetails = ejbSIP.getAdCompany(user.getCmpCode());	  
			        
			        //get parameter
			        
			        Iterator i = list.iterator();
			        
			        while (i.hasNext()) {
			        	
			        	details = (InvRepStockIssuancePrintDetails)i.next();
			        	
			        }
	            			            		           			   	
	            } catch(GlobalNoRecordFoundException ex) {
	            	
	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("stockIssuancePrint.error.stockIssuanceAlreadyDeleted"));
	            		
	            } catch(EJBException ex) {
		     	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in InvRepStockIssuancePrintAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	               
	           } 
	            
	            if (!errors.isEmpty()) {

	                saveErrors(request, new ActionMessages(errors));
	                return mapping.findForward("invRepStockIssuancePrintForm");

	             }
				
				// fill report parameters, fill report to pdf and set report session  	    	               
			    
			    
				Map parameters = new HashMap();
				
				parameters.put("createdBy", details.getSipSiCreatedBy());
				parameters.put("company", adCmpDetails.getCmpName());

				String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockIssuancePrint.jasper";
			       
		        if (!new java.io.File(filename).exists()) {
		       		    		    
		           filename = servlet.getServletContext().getRealPath("jreports/InvRepStockIssuancePrint.jasper");
			    
		        }
				 						    	    	    	        	    				
				try {
					    
				    Report report = new Report();
				    
				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
				    report.setBytes(
				      JasperRunManager.runReportToPdf(filename, parameters, 
				        new InvRepStockIssuancePrintDS(list)));  

				    session.setAttribute(Constants.REPORT_KEY, report);
				   
				} catch(Exception ex) {
					
				    if(log.isInfoEnabled()) {
				   	
				      log.info("Exception caught in InvRepStockIssuancePrintAction.execute(): " + ex.getMessage() +
					      " session: " + session.getId());
					      
				   } 
				   
				    return(mapping.findForward("cmnErrorPage"));
					
				}              
				
				return(mapping.findForward("invRepStockIssuancePrint"));
	   
		  } else {
		 	
		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		    saveErrors(request, new ActionMessages(errors));
		
		    return(mapping.findForward("invRepStockIssuancePrintForm"));
		
		 }
	 
     } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()) {
          	
             log.info("Exception caught in InvRepStockIssuancePrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
                
          }        
          return(mapping.findForward("cmnErrorPage"));        
       } 	
         	
    }
}
