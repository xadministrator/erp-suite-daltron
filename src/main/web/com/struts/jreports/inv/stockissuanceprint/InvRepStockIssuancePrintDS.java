package com.struts.jreports.inv.stockissuanceprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepStockIssuancePrintDetails;


public class InvRepStockIssuancePrintDS implements JRDataSource {

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepStockIssuancePrintDS(ArrayList invRepSIPList) {

   	  Iterator i = invRepSIPList.iterator();
   	  
   	  while(i.hasNext()) {
   	  	
   	  	InvRepStockIssuancePrintDetails details = (InvRepStockIssuancePrintDetails)i.next();
   	  	
   	  	InvRepStockIssuancePrintData invRepSIPData = new InvRepStockIssuancePrintData(
   	  			Common.convertSQLDateToString(details.getSipSiDate()),
				details.getSipSiDocumentNumber(),
				details.getSipSiReferenceNumber(),
				details.getSipSiDescription(),
				details.getSilIlIiName(),
				details.getSilIlIiDescription(),
				details.getSilIlLocationName(),
				details.getSilIlIiUomName(),
				new Double(details.getSilBosQuantityRequired()),
				new Double(details.getSilIssueQuantity()), new Double(details.getSilUnitCost()));
   	  	
   	  	data.add(invRepSIPData);

   	  }   	   	
                                                                     
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }

   public Object getFieldValue(JRField field) throws JRException {
   	
      Object value = null;

      String fieldName = field.getName();

      if("date".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getDate();       
      }else if("documentNumber".equals(fieldName)){
        value = ((InvRepStockIssuancePrintData)data.get(index)).getDocumentNumber();
      }else if("referenceNumber".equals(fieldName)){
        value = ((InvRepStockIssuancePrintData)data.get(index)).getReferenceNumber();
      }else if("description".equals(fieldName)){
        value = ((InvRepStockIssuancePrintData)data.get(index)).getDescription(); 
      }else if("itemName".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getItem();
      }else if("itemDescription".equals(fieldName)){
        value = ((InvRepStockIssuancePrintData)data.get(index)).getItemDescription();
      }else if("itemLocation".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getLocation();
      }else if("unit".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getUnit();
      }else if("quantityRequired".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getQuantityRequired();
      }else if("issueQuantity".equals(fieldName)){
         value = ((InvRepStockIssuancePrintData)data.get(index)).getIssueQuantity();
      }else if("unitCost".equals(fieldName)){
          value = ((InvRepStockIssuancePrintData)data.get(index)).getUnitCost();
       
      }

      return(value);
   }
}
