package com.struts.jreports.inv.stockissuanceprint;


public class InvRepStockIssuancePrintData implements java.io.Serializable {

   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String description = null;
   private String item = null;
   private String itemDescription = null;
   private String location = null;
   private String unit = null;
   private Double quantityRequired = null;
   private Double issueQuantity = null;
   private String createdBy = null;
   private String approvedBy = null; 
   private Double unitCost = null;

  
   public InvRepStockIssuancePrintData( String date,
   		String documentNumber,
		String referenceNumber,
		String description,
		String item,
		String itemDescription,
   		String location,
   		String unit,
   		Double quantityRequired,
   		Double issueQuantity,
   		Double unitCost) {
      	
       this.date = date;
       this.documentNumber = documentNumber;
       this.referenceNumber = referenceNumber;
       this.description = description;
       this.item = item;
       this.itemDescription = itemDescription;
       this.location = location;
       this.unit = unit;
       this.quantityRequired = quantityRequired;
       this.issueQuantity = issueQuantity;
       this.unitCost = unitCost;
      	
   }
   
   public String getReferenceNumber() {
   	
      return referenceNumber;
      
   }
   
   public String getDescription() {
   	
      return description;
      
   }
   
   public String getDate() {
   	
      return date;
      
   }
   
   public String getDocumentNumber() {
   	
      return documentNumber;
      
   }
   
   public String getCreatedBy() {
   	
      return createdBy;
      
   }
   
   public String getItem() {
   	
      return item;
      
   }
   
   public String getItemDescription() {
   	
      return itemDescription;
      
   }
   
   public String getApprovedBy() {
   	
      return approvedBy;
      
   }
   
   public String getLocation() {
   	
      return location;
      
   }
   
   public String getUnit() {
   	
      return unit;
      
   }
   
   public Double getQuantityRequired() {
       
       return quantityRequired;
       
   }
   
   public Double getIssueQuantity() {
       
       return issueQuantity;
       
   }
   
   
   public Double getUnitCost() {
       
       return unitCost;
       
   }
   
   
   
}
