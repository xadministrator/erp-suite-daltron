package com.struts.jreports.inv.stockonhand;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.joda.time.LocalDate;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepStockOnHandController;
import com.ejb.txn.InvRepStockOnHandControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class InvRepStockOnHandAction extends Action {

   private org.apache.commons.logging.Log log =
   	org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvRepStockOnHandAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());

            }

         } else {

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         InvRepStockOnHandForm actionForm = (InvRepStockOnHandForm)form;

	      // reset report to null
	      actionForm.setReport(null);

         String frParam = Common.getUserPermission(user, Constants.INV_REP_STOCK_ON_HAND_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("invRepStockOnHand");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize InvRepStockOnHandController EJB
*******************************************************/

         InvRepStockOnHandControllerHome homeSH = null;
         InvRepStockOnHandController ejbSH = null;
         

         try {

            homeSH = (InvRepStockOnHandControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepStockOnHandControllerEJB", InvRepStockOnHandControllerHome.class);

         } catch(NamingException e) {

            if(log.isInfoEnabled()) {

                log.info("NamingException caught in InvRepStockOnHandAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbSH = homeSH.create();

         } catch(CreateException e) {

             if(log.isInfoEnabled()) {

                 log.info("CreateException caught in InvRepStockOnHandAction.execute(): " + e.getMessage() +
                " session: " + session.getId());

             }

             return(mapping.findForward("cmnErrorPage"));

         }

     ActionErrors errors = new ActionErrors();

	 /*** get report session and if not null set it to null **/

	 Report reportSession =
	    (Report)session.getAttribute(Constants.REPORT_KEY);

	 if(reportSession != null) {

	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);

	 }

/*******************************************************
   -- AP SH Go Action --
*******************************************************/
	 String rptType="";
         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            /* stored proc var */
            boolean isStoredProceedure = false;
            
            String itemNameFrom = "";
            String itemNameTo = "";
            String itemCategory = "";
            String itemClass = "";
            String location = "";
            String locationType= "";
            Date asOfDate = null;
            byte  includeUnposted = 0;
            byte showCommitted = 0;
            byte includeZero = 0;
            String branchCodes = "";
            Integer companyCode = 1;
             
            String storedProcName = "{ call InvRepStockOnHandReportGenerate(?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
            String jasperFileNameSp = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHandSp.jasper";
            
            if (!new java.io.File(jasperFileNameSp).exists()) {

               jasperFileNameSp = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHandSp.jasper");

            }   
   
            String filename = "";

            ArrayList list = new ArrayList();

            String company = null;

             // create criteria

            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();

	        	if (!Common.validateRequired(actionForm.getItemName())) {

                    itemNameFrom = actionForm.getItemName();
	        		criteria.put("itemName", actionForm.getItemName());

	        	}

	           if (!Common.validateRequired(actionForm.getItemNameTo())) {
                    itemNameTo = actionForm.getItemNameTo();
	        		criteria.put("itemNameTo", actionForm.getItemNameTo());
	        		System.out.println("ACTION FORM : itemNameTo ");
	        	}

	        	if (!Common.validateRequired(actionForm.getCategory())) {

                    itemCategory = actionForm.getCategory();
	        		criteria.put("itemCategory", (actionForm.getCategory()));

	        	}

	        	if (!Common.validateRequired(actionForm.getItemClass())) {
	        	  
                    itemClass = actionForm.getItemClass();
	        		criteria.put("itemClass", (actionForm.getItemClass()));

	        	}

	        	if (!Common.validateRequired(actionForm.getLocation())) {

                    location = actionForm.getLocation();
	        		criteria.put("location", (actionForm.getLocation()));

	        	}

	        	if (!Common.validateRequired(actionForm.getLocationType())) {
                    locationType = actionForm.getLocationType();
	        		criteria.put("locationType", (actionForm.getLocationType()));

	        	}


	        		//criteria.put("reportType", (actionForm.getReportType()));
	        		rptType=actionForm.getReportType();


	        	// save criteria

	        	actionForm.setCriteria(criteria);

	     	}


		    try {

		       // get company

		       AdCompanyDetails adCmpDetails = ejbSH.getAdCompany(user.getCmpCode());
		       company = adCmpDetails.getCmpName();
		       companyCode = user.getCmpCode();

				//get all InvUsageVarianceBranchLists

				ArrayList branchList = new ArrayList();

				for(int i=0; i<actionForm.getInvBrIlListSize(); i++) {

					InvRepStockOnHandBranchList brList = (InvRepStockOnHandBranchList)actionForm.getInvBrIlListByIndex(i);

					if(actionForm.getReportType().equals("Consolidated Report") || actionForm.getReportType().equals("SRP") || actionForm.getReportType().equals("SKU") || actionForm.getReportType().equals("Quantity")){
							AdBranchDetails mdetails = new AdBranchDetails();
							mdetails.setBrAdCompany(user.getCmpCode());
							mdetails.setBrCode(brList.getBranchCode());
							branchCodes += brList.getBranchCode()+ ",";
							branchList.add(mdetails);
							
							System.out.println("trace01 brList: " + brList.getBranchCode());
					}else{
						if(brList.getBranchCheckbox() == true) {
							AdBranchDetails mdetails = new AdBranchDetails();
							mdetails.setBrAdCompany(user.getCmpCode());
							mdetails.setBrCode(brList.getBranchCode());
							branchList.add(mdetails);
							 branchCodes += brList.getBranchCode()+ ",";
							System.out.println("trace02 brList: " + brList.getBranchCode());
						}
					}

				}
		
				includeUnposted = Common.convertBooleanToByte(actionForm.getIncludeUnposted());
				showCommitted = Common.convertBooleanToByte(actionForm.getShowCommittedQuantity());
				includeZero = Common.convertBooleanToByte(actionForm.getIncludeZeroes());
				
				
				if (new java.io.File(jasperFileNameSp).exists()) {
				   //If jasper for stored proceedure exist. Run Stored Procedure process
                  isStoredProceedure = true;
                  filename = jasperFileNameSp;
                  System.out.println("sp exist");
                     
                  /*stored procedure section */
                  DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
                  Connection conn = null;
                  CallableStatement stmt = null;
                  asOfDate =  Common.convertStringToSQLDate(actionForm.getAsOfDate());
 
                  
                  try{
  
                      conn = dataSource.getConnection();
  
                       stmt = (CallableStatement) conn.prepareCall(storedProcName);
                       stmt.setString(1 /*"ITEM_FROM"*/, itemNameFrom);
                       stmt.setString(2 /*"ITEM_TO"*/, itemNameTo);
                       stmt.setString(3 /*"ITEM_CATEGORY"*/, itemCategory);
                       stmt.setString(4 /*"ITEM_CLASS"*/, itemClass);
                       stmt.setString(5 /*"LOCATION"*/, location);
                       stmt.setString(6 /*"LOCATION_TYPE"*/, locationType);
                       stmt.setDate(7 /*"AS_OF_DATE"*/, new java.sql.Date(asOfDate.getTime()));
                       stmt.setByte(8 /*"INCLUDE_UNPOSTED"*/, includeUnposted);
                       stmt.setByte(9 /*"SHOW_COMMITED"*/, showCommitted);
                       stmt.setByte(10 /*"INCLUDE_ZERO"*/, includeZero);
                       stmt.setString(11 /*"BRANCH_CODES"*/, branchCodes);
                       stmt.setInt(12 /*"CMP_CODE"*/, companyCode);
                       stmt.setString(13 /*"ORDER_BY"*/, actionForm.getOrderBy());
                       stmt.registerOutParameter(14, java.sql.Types.INTEGER);
  
                    
                    
                       stmt.executeQuery();
                       
                       int countResult = stmt.getInt(14);
                       
                       
                      if(countResult <=0){
                          throw new GlobalNoRecordFoundException();
                      }
        
                   } catch(SQLException ex){
                       System.out.println("sql error 1: " +    ex.toString());
                  
                      try { 
                          if(stmt != null) stmt.close(); 
                      }catch(SQLException f){ 
                           System.out.println("sql error 1.1: " +    f.toString());
                      }
                      stmt = null;
                      try{ 
                          if(conn != null) conn.close(); 
                      }
                      catch(SQLException f){
                          System.out.println("sql error 1.2 : " +    f.toString());
                      }
                      conn = null;
                      
                   }finally{
                    
                      try { 
                          if(stmt != null) stmt.close(); }catch(SQLException f){ System.out.println("sql error 2.1 : " +    f.toString()); ; }
                      stmt = null;
                      try{ 
                          if(conn != null) conn.close(); }catch(SQLException f){ System.out.println("sql error 2.2 : " +    f.toString());; }
                      conn = null;
                   } 
  
                  }else{
                    //Process report by ejb
                  
                    // This section execute report by ejb.
                    System.out.println("execute report");
                    list = ejbSH.executeInvRepStockOnHand(actionForm.getCriteria(), actionForm.getIncludeZeroes(),
                        actionForm.getOrderBy(), actionForm.getShowCommittedQuantity(), actionForm.getIncludeUnposted(),
                        actionForm.getIncludeForecast(), actionForm.getlayered(), Common.convertStringToSQLDate(actionForm.getAsOfDate()),
                        actionForm.getUnit(), actionForm.getFixCosting(), new Integer(user.getCurrentBranch().getBrCode()), branchList, user.getCmpCode(), rptType);
                    System.out.println("Order By >> " + actionForm.getOrderBy());
                    System.out.println("List >> " + list);
                    
                    filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHand.jasper";

                    if (!new java.io.File(filename).exists()) {

                       filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHand.jasper");

                    }

             
                  }
 
            } catch (GlobalNoRecordFoundException ex) {

                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("InvRepStockOnHandAction.error.noRecordFound"));
		    } catch(Exception ex) {

		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in InvRepStockOnHandAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());

				}

				return(mapping.findForward("cmnErrorPage"));

		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("invRepStockOnHand");

            }


		    // fill report parameters, fill report to pdf and set report session

		    Map parameters = new HashMap();


		    for(int x=0;x<user.getResCount();x++) {
		    	Responsibility ds = (Responsibility)user.getRes(x);

		    	System.out.println("resp: " + ds.getRSName());
		    	parameters.put(ds.getRSName(), "YES");

		    }



		    parameters.put("userResponsibility","");
		    parameters.put("company", company);
		    parameters.put("date", new java.util.Date());
			parameters.put("printedBy", user.getUserName());
			parameters.put("datePrinted", Common.convertSQLDateToString(new java.util.Date()));

			if (actionForm.getIncludeZeroes()){

				parameters.put("includeZeroes", "yes");

			} else {

				parameters.put("includeZeroes", "no");

			}

			if (actionForm.getlayered()){

				parameters.put("layered", "yes");

			} else {

				parameters.put("layered", "no");

			}


			if (actionForm.getShowCommittedQuantity()){

				parameters.put("showCommittedQuantity", "yes");

			} else {

				parameters.put("showCommittedQuantity", "no");

			}

			if (actionForm.getIncludeUnposted()){

				parameters.put("includeUnposted", "yes");

			} else {

				parameters.put("includeUnposted", "no");

			}

        	if (actionForm.getItemName() != null) {

        		parameters.put("itemName", actionForm.getItemName());

        	}

        	if (actionForm.getCategory() != null) {

        		parameters.put("itemCategory", actionForm.getCategory());
        	}

        	if (actionForm.getItemClass() != null) {

        		parameters.put("itemClass", actionForm.getItemClass());

        	}

        	if (actionForm.getLocation() != null) {

        		parameters.put("location", actionForm.getLocation());

        	}

        	if (actionForm.getLocationType() != null) {

        		parameters.put("locationType", actionForm.getLocationType());

        	}

        	if (actionForm.getAsOfDate() != null) {

        		parameters.put("asOfDate", Common.convertStringToSQLDate(actionForm.getAsOfDate()));

        	}

			if (actionForm.getIncludeForecast()){

				parameters.put("includeForecast", "yes");

			} else {

				parameters.put("includeForecast", "no");

			}

        	String branchMap = null;
      		boolean first2 = true;
      		for(int j=0; j < actionForm.getInvBrIlListSize(); j++) {

      			InvRepStockOnHandBranchList brList = (InvRepStockOnHandBranchList)actionForm.getInvBrIlListByIndex(j);

      			if(brList.getBranchCheckbox() == true) {
      				if(first2) {
      					branchMap = brList.getBranchName();
      					first2 = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}

      		}
      		parameters.put("branchMap", branchMap);

      	   /*

      		if(actionForm.getReportType().equals("Consolidated Report")){
 		    	System.out.println("Consolidated Report");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHandConsolidated.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHandConsolidated.jasper");

			    }

		    } else if(actionForm.getReportType().equals("SRP")){
 		    	System.out.println("SRP");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHandSRP.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHandSRP.jasper");

			    }

		    } else if(actionForm.getReportType().equals("Quantity")){
 		    	System.out.println("Quantity");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHandQTY.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHandQTY.jasper");

			    }

		    }  else if(actionForm.getReportType().equals("SKU")){
 		    	System.out.println("SKU");
		    	filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHandSKU.jasper";

			    if (!new java.io.File(filename).exists()) {

			    	filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHandSKU.jasper");

			    }

		    }else {
                
		    	 filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockOnHand.jasper";

			        if (!new java.io.File(filename).exists()) {

			           filename = servlet.getServletContext().getRealPath("jreports/InvRepStockOnHand.jasper");

			        }


			}
			
			*/

		    try {
		    
		     
              DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
              Connection conn = null;
		    

		       Report report = new Report();

		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {

		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
		       	   
		       	   if(isStoredProceedure){
		       	       conn = dataSource.getConnection(); 
		       	       report.setBytes(
                         JasperRunManager.runReportToPdf(filename, parameters, conn));
		       	       conn.close();
		       	   }else{
		       	      report.setBytes(
                      JasperRunManager.runReportToPdf(filename, parameters,
                        new InvRepStockOnHandDS(list)));
		       	   }
		       	   parameters.put("viewType", "PDF");
			     

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
				  
			      report.setViewType(Constants.REPORT_VIEW_TYPE_CSV_SP);       
				  if(isStoredProceedure){
                       
                       conn = dataSource.getConnection();
                       JasperPrint jrprint = JasperFillManager.fillReport(filename, parameters,
                                    conn);
                       report.setJasperPrint(jrprint);
                       conn.close();
                   }else{

                       report.setBytes(
                           JasperRunManagerExt.runReportToXls(filename, parameters,
                                new InvRepStockOnHandDS(list)));
                   }
                   parameters.put("viewType", "EXCEL");	        
					        
					        

			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){

			
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);    
				   if(isStoredProceedure){
                      
                       conn = dataSource.getConnection();
                       JasperPrint jrprint = JasperFillManager.fillReport(filename, parameters,
                                    conn);
                       report.setJasperPrint(jrprint);
                       conn.close();
                   }else{
                       

                       report.setBytes(
                           JasperRunManagerExt.runReportToXls(filename, parameters,
                                new InvRepStockOnHandDS(list)));
                   }
                     parameters.put("viewType", "HTML");

			   }

		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);

		    } catch(Exception ex) {

		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in InvRepStockOnHandAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());

		        }

			return(mapping.findForward("cmnErrorPage"));

		    }

/*******************************************************
   -- AP SH Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {

		          return(mapping.findForward("cmnMain"));

/*******************************************************
   -- AP SH Load Action --
*******************************************************/

             }

         if(frParam != null) {


         	try {

         		// populate branch list

             	actionForm.reset(mapping, request);

         		actionForm.clearInvBrIlList();
         		ArrayList brList = new ArrayList();

				brList = ejbSH.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());

				Iterator k = brList.iterator();
				//int ctr = 1;
				while(k.hasNext()) {

					AdBranchDetails details = (AdBranchDetails)k.next();

					InvRepStockOnHandBranchList invBrIlList = new InvRepStockOnHandBranchList(actionForm,
							details.getBrBranchCode(), details.getBrName(),
							details.getBrCode());
					if (details.getBrHeadQuarter() == 1 ) invBrIlList.setBranchCheckbox(true);
					//ctr++;
					actionForm.saveInvBrIlList(invBrIlList);

				}

         		ArrayList list = null;
         		Iterator i = null;

         		actionForm.clearCategoryList();

         		list = ejbSH.getAdLvInvItemCategoryAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setCategoryList((String)i.next());

         			}

         		}

         		actionForm.clearReportTypeList();

		        list = ejbSH.getAdLvReportTypeAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {


            		actionForm.setReportTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setReportTypeList((String)i.next());

            		}

            	}




            	actionForm.clearSortByList();

		        list = ejbSH.getAdLvSortByAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {


            		actionForm.setSortByList(Constants.GLOBAL_NO_RECORD_FOUND);
            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setSortByList((String)i.next());

            		}

            	}

         		actionForm.clearLocationTypeList();

         		list = ejbSH.getAdLvInvLocationTypeAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setLocationTypeList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setLocationTypeList((String)i.next());

         			}

         		}

         		actionForm.clearLocationList();
         		actionForm.setlayered(false);

         		list = ejbSH.getInvLocAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setLocationList((String)i.next());

         			}

         		}

         		actionForm.clearUnitList();

         		list = ejbSH.getInvUomAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setUnitList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setUnitList((String)i.next());

         			}

         		}

         	} catch(EJBException ex) {

         		if(log.isInfoEnabled()) {
         			log.info("EJBException caught in InvRepItemCostingAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	actionForm.setAsOfDate(Common.convertSQLDateToString(new Date()));

         	return(mapping.findForward("invRepStockOnHand"));

         } else {

				    errors.add(ActionMessages.GLOBAL_MESSAGE,
				    		new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));

				    return(mapping.findForward("cmnMain"));

				 }

         } catch(Exception e) {


/*******************************************************
   System Failed: Forward to error page
*******************************************************/
	      if(log.isInfoEnabled()) {

	         log.info("Exception caught in InvRepStockOnHandAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());

	       }

		  return(mapping.findForward("cmnErrorPage"));

        }
    }
}
