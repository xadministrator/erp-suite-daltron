package com.struts.jreports.inv.stockonhand;

import java.io.Serializable;

public class InvRepStockOnHandBranchList implements Serializable {
	
	private String brBranchCode = null;
	private String branchName = null;
	private Integer branchCode = null;
	private boolean branchCheckbox = false;
	
	private InvRepStockOnHandForm parentBean;
	
	public InvRepStockOnHandBranchList(InvRepStockOnHandForm parentBean, 
			String brBranchCode, String branchName, Integer branchCode) {
		
		this.parentBean = parentBean;
		this.branchName = branchName;
		this.branchCode = branchCode;
		this.brBranchCode = brBranchCode;
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBrName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
public String getBrBranchCode() {
		
		return brBranchCode;
		
	}
	
	public void setBrBranchCode(String brBranchCode) {
		
		this.brBranchCode = brBranchCode;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}