package com.struts.jreports.inv.stockonhand;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepStockOnHandDetails;

public class InvRepStockOnHandDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepStockOnHandDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         InvRepStockOnHandDetails details = (InvRepStockOnHandDetails)i.next();
	         
	         InvRepStockOnHandData argData = new InvRepStockOnHandData(details.getShItemName(), details.getShItemCategory(),
	         		details.getShItemDescription(), details.getShLocation(), details.getShItemClass(),
					details.getShUnit(), new Double(details.getShQuantity()), new Boolean(details.getShItemForReorder()),
					new Double(details.getShCommittedQuantity()), new Double(details.getShAverageCost()),
					details.getShLastPoDate(), details.getShLastReceiveDate(), new Double(details.getShAgedInMonth()), new Double(details.getShValue()), new Double(details.getShUnpostedQuantity()),
					new Double(details.getShUnservedPo()), new Double(details.getShForecastQuantity()),
					new Double(details.getShUnitCost()), new Double(details.getShSalesPrice()) , details.getShPartNumber(), details.getShCategoryName(), details.getShOrderBy(), details.getShMisc(),
					details.getShBranchCode());
	         		
	         data.add(argData);
	         
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getItemName();
   		
   	} else if("itemCategory".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getItemCategory();

   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getItemDescription();
   		
   	} else if("location".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getLocation();
   		
   	} else if("itemClass".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getItemClass();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getUnit();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getQuantity();
  
	} else if("forReorder".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getForReorder();	

	} else if("committedQuantity".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getCommitedQuantity();	

   	} else if("aveCost".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getAverageCost();	

   	} else if("lastPoDate".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getLastPoDate();
   		
   	} else if("lastReceiveDate".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getLastReceiveDate();
   		
   	} else if("agedInMonth".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getAgedInMonth();	
  
   	} else if("value".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getValue();	


   	} else if("unpostedQuantity".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getUnpostedQuantity();
   		
   	} else if("unservedPo".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getUnservedPo();
   		
   	} else if("forecastQuantity".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getForecastQuantity();
   		
   	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getUnitCost();
   		
   	} else if("salesPrice".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getSalesPrice();


   	}  else if("partNumber".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getPartNumber();
   		
   	}  else if("categoryName".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getCategoryName();
   		
   	} else if("orderBy".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getOrderBy();
   		
   	}  else if("misc".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getMisc();
   		
   	}  else if("branchCode".equals(fieldName)) {
   		
   		value = ((InvRepStockOnHandData)data.get(index)).getBranchCode();
 
   	}
   	
   	
   	return(value);
   	
   }
   
}
