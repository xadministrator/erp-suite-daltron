package com.struts.jreports.inv.stockonhand;

import java.util.Date;


public class InvRepStockOnHandData implements java.io.Serializable {

	private String itemName;
	private String itemCategory;
	private String itemDescription;
	private String location;
	private String itemClass;
	private String unit;
	private Double quantity;
	private Boolean forReorder;
	private Double committedQuantity;
	private Double aveCost;
	private String lastPoDate;
	private Date lastReceiveDate;
	private Double agedInMonth;
	private Double value;
	private Double unpostedQuantity;
	private String partNumber;
	private String categoryName;

	private Double unservedPo;
	private Double forecastQuantity;
	private Double unitCost;
	private Double salesPrice;
	
	private String orderBy;
	private String misc;
	private String branchCode;

	public InvRepStockOnHandData( String itemName, String itemCategory, String itemDescription, String location,
			String itemClass, String unit, Double quantity, Boolean forReorder, Double committedQuantity,
			Double aveCost, String lastPoDate, Date lastReceiveDate, Double agedInMonth, Double value, Double unpostedQuantity, Double unservedPo, 
			Double forecastQuantity, Double unitCost, Double salesPrice, String partNumber, String categoryName, String orderBy, String misc,
			String branchCode) {

		this.itemName = itemName;
		this.itemCategory = itemCategory;
		this.itemDescription = itemDescription;
		this.location = location;
		this.itemClass = itemClass;
		this.unit = unit;
		this.quantity = quantity;
		this.forReorder = forReorder;
		this.committedQuantity = committedQuantity;
		this.aveCost = aveCost;
		this.lastPoDate = lastPoDate;
		this.lastReceiveDate = lastReceiveDate;
		this.agedInMonth = agedInMonth;
		this.value = value;
		this.unpostedQuantity = unpostedQuantity;
		this.unservedPo = unservedPo;
		this.forecastQuantity = forecastQuantity;
		this.unitCost = unitCost;
		this.salesPrice = salesPrice;
		this.partNumber = partNumber;
		this.categoryName = categoryName;
		this.orderBy = orderBy;
		this.misc = misc;
		this.branchCode = branchCode;

	}

	public String getItemClass() {

		return itemClass;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public String getItemName() {

		return itemName;

	}
	
	public String getItemCategory() {

		return itemCategory;

	}

	public String getUnit() {

		return unit;

	}

	public Double getQuantity() {

		return quantity;

	}

	public String getLocation() {

		return location;

	}

	public Boolean getForReorder() {

		return forReorder;

	}

	public Double getCommitedQuantity() {

		return committedQuantity;

	}

	public Double getAverageCost() {

		return aveCost;

	}

	public String getLastPoDate() {

		return lastPoDate;

	}
	
	public Date getLastReceiveDate() {

		return lastReceiveDate;

	}
	
	
	public Double getAgedInMonth() {

		return agedInMonth;

	}
	
	

	public Double getValue() {

		return value;

	}

	public Double getUnpostedQuantity() {

		return unpostedQuantity;
	}

	public Double getUnservedPo() {

		return unservedPo;
	}

	public Double getForecastQuantity() {

		return forecastQuantity;
	}

	public Double getUnitCost() {

		return unitCost;
	}
	
	public Double getSalesPrice() {

		return salesPrice;
	}
	
	public String getCategoryName() {

		return categoryName;

	}
	
	public String getOrderBy() {

		return orderBy;

	}
	
	public String getMisc() {

		return misc;

	}
	
	public String getBranchCode() {

		return branchCode;

	}

} // InvRepStockOnHandData class
