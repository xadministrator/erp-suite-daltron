package com.struts.jreports.inv.stocktransfer;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepStockTransferDetails;

public class InvRepStockTransferDS implements JRDataSource{
	
   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepStockTransferDS(ArrayList list) {
   	   	  
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	       	  
	         InvRepStockTransferDetails details = (InvRepStockTransferDetails)i.next();
	         
	         InvRepStockTransferData argData = new InvRepStockTransferData( details.getStDate(),
	         		details.getStItemName(), details.getStItemDescription(), details.getStItemCategory(), details.getStLocationFrom(), 
	         		details.getStLocationTo(), details.getStDocNumber(), details.getStUnit(), new Double(details.getStQuantity()),
					new Double(details.getStUnitCost()), new Double(details.getStAmount()));

	         data.add(argData);
	         
      }
      
   }

   public boolean next() throws JRException {
   	
      index++;
      return (index < data.size());
      
   }
   
   public Object getFieldValue(JRField field) throws JRException {
   	
   	Object value = null;
   	
   	String fieldName = field.getName();
   	
   	if("date".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getDate();
   		
   	} else if("itemName".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getItemName();
   		
   	} else if("itemDescription".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getItemDescription();
   		
   	} else if("itemCategory".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getItemCategory();
   		
   	} else if("locationFrom".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getLocationFrom();

   	} else if("locationTo".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getLocationTo();
   		
   	} else if("documentNumber".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getDocumentNumber();
   		
   	} else if("unit".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getUnit();
   		
   	} else if("quantity".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getQuantity();

   	} else if("unitCost".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getUnitCost();
   	
   	} else if("amount".equals(fieldName)) {
   		
   		value = ((InvRepStockTransferData)data.get(index)).getAmount();

   	}
   	
   	return(value);
   	
   }
   
}
