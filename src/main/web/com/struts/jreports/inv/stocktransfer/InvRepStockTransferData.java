package com.struts.jreports.inv.stocktransfer;

import java.util.Date;

public class InvRepStockTransferData implements java.io.Serializable {

	private Date date;
	private String itemName;
	private String itemDescription;
	private String itemCategory;
	private String locationFrom;
	private String locationTo;
	private String documentNumber;
	private String unit;
	private Double quantity;
	private Double unitCost;
	private Double amount;

	public InvRepStockTransferData(	Date date, String itemName, String itemDescription, String itemCategory, String locationFrom,
			String locationTo, String documentNumber, String unit, Double quantity, Double unitCost, 
			Double amount) {
		
		this.date = date;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.itemCategory = itemCategory;
		this.locationFrom = locationFrom;
		this.locationTo = locationTo;
		this.documentNumber = documentNumber;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCost = unitCost;
		this.amount = amount;		
	}

	
	public Date getDate() {
		
		return date;
		
	}
	
	public void setDate(Date date) {
		
		this.date = date;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	
	public String getLocationFrom() {
		
		return locationFrom;
		
	}
	
	public void setLocationFrom(String locationFrom) {
		
		this.locationFrom = locationFrom;
		
	}
	
	public Double getAmount() {
		
		return amount;
	
	}
	
	public void setAmount(Double amount) {
	
		this.amount = amount;
	
	}
	
	public String getItemDescription() {
	
		return itemDescription;
	
	}
	
	public void setItemDescription(String itemDesc) {
	
		this.itemDescription = itemDesc;
	
	}
	
	public String getItemName() {
	
		return itemName;
	
	}
	
	public void setItemName(String itemName) {
	
		this.itemName = itemName;
	
	}
	
	public String getItemCategory() {
		
		return itemCategory;
		
	}
	
	public void setItemCategory(String itemCategory) {
		
		this.itemCategory = itemCategory;
		
	}
	
	public Double getQuantity() {
	
		return quantity;
	
	}
	
	public void setQuantity(Double quantity) {
	
		this.quantity = quantity;
	
	}
	
	public String getLocationTo() {
	
		return locationTo;
	
	}
	
	public void setLocationTo(String locationTo) {
	
		this.locationTo = locationTo;
	
	}
	
	public String getUnit() {
		
		return unit;
	
	}
	
	public void setUnit(String unit) {
	
		this.unit = unit;
	
	}
	
	public Double getUnitCost() {
	
		return unitCost;
	
	}
	
	public void setUnitCost(Double unitCost) {
	
		this.unitCost = unitCost;
	
	}

} // InvRepStockTransferData class
