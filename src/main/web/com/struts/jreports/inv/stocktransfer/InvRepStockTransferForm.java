package com.struts.jreports.inv.stocktransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepStockTransferForm extends ActionForm implements Serializable{

	private boolean includeUnposted = false;
	private String dateFrom = null;
	private String dateTo = null; 
	private String itemName = null;
	private String category = null;
	private ArrayList categoryList = new ArrayList();
	private String locationFrom = null;
	private ArrayList locationFromList = new ArrayList();
	private String locationTo = null;
	private ArrayList locationToList = new ArrayList();
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	private ArrayList invBrIlList = new ArrayList(); 

	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public boolean getIncludeUnposted(){
		
		return(includeUnposted);
		
	}
	
	public void setIncludeUnposted(boolean includeUnposted) {
		
		this.includeUnposted = includeUnposted;
		
	}

	public String getDateFrom(){
		
		return(dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return(dateTo);
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public String getItemName(){
		
		return(itemName);
		
	}

	public void setItemName(String itemName){
		
		this.itemName = itemName;
		
	}
	
	public String getCategory (){
		
		return(category );
		
	}

	public void setCategory (String category ){
		
		this.category  = category ;
		
	}
	
	public ArrayList getCategoryList(){
		
		return(categoryList);
		
	}
	
	public void setCategoryList(String category){
		
		categoryList.add(category);
		
	}
	
	public void clearCategoryList(){
		
		categoryList.clear();
		categoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocationFrom (){
		
		return(locationFrom);
		
	}
	
	public void setLocationFrom (String locationFrom ){
		
		this.locationFrom  = locationFrom;
		
	}
	
	public ArrayList getLocationFromList(){
		
		return(locationFromList);
		
	}
	
	public void setLocationFromList(String locationFrom){
		
		locationFromList.add(locationFrom);
		
	}
	
	public void clearLocationFromList(){
		
		locationFromList.clear();
		locationFromList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLocationTo (){
		
		return(locationTo );
		
	}
	
	public void setLocationTo (String locationTo ){
		
		this.locationTo  = locationTo ;
		
	}
	
	public ArrayList getLocationToList(){
		
		return(locationToList);
		
	}
	
	public void setLocationToList(String locationTo){
		
		locationToList.add(locationTo);
		
	}
	
	public void clearLocationToList(){
		
		locationToList.clear();
		locationToList.add(Constants.GLOBAL_BLANK);
		
	}

	public String getItemClass (){
		
		return(itemClass );
		
	}
	
	public void setItemClass (String itemClass ){
		
		this.itemClass  = itemClass ;
		
	}
	
	public ArrayList getItemClassList(){
		
		return(itemClassList);
		
	}

	public String getOrderBy (){
		
		return(orderBy );
		
	}
	
	public void setOrderBy (String orderBy ){
		
		this.orderBy  = orderBy ;
		
	}
	
	public ArrayList getOrderByList(){
		
		return(orderByList);
		
	}
	
	public void setGoButton(String goButton){
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public String getViewType(){
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType){
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList(){
		
		return viewTypeList;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}

	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepStockTransferBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepStockTransferBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		for (int i=0; i<invBrIlList.size(); i++) {
			
			InvRepStockTransferBranchList list = (InvRepStockTransferBranchList)invBrIlList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  

		goButton = null;
		closeButton = null;
		
		dateFrom = Common.convertSQLDateToString(new java.util.Date());
		dateTo = Common.convertSQLDateToString(new java.util.Date());
		
		itemName = null;
		category = null;
		locationFrom = null;
		locationTo = null;
		includeUnposted = false;
		
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add(Constants.INV_REP_STOCK_TRANSFER_ITEM_CLASS_STOCK);
		itemClassList.add(Constants.INV_REP_STOCK_TRANSFER_ITEM_CLASS_ASSEMBLY);
		itemClass = null;
		
		orderByList.clear();
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_DATE);
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_DOCUMENT_NUMBER);
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_LOCATION_FROM);
		orderByList.add(Constants.INV_REP_STOCK_TRANSFER_ORDER_BY_LOCATION_TO);
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;      	  
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
			
			if(Common.validateRequired(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("stockTransferRep.error.dateFromRequired"));
				
			}
			
			if(Common.validateRequired(dateTo)){
				
				errors.add("dateTo", new ActionMessage("stockTransferRep.error.dateToRequired"));
				
			}
			
			if(!Common.validateDateFormat(dateFrom)){
				
				errors.add("dateFrom", new ActionMessage("stockTransferRep.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)){
				
				errors.add("dateTo", new ActionMessage("stockTransferRep.error.dateToInvalid"));
				
			}	
						
		}

		return(errors);
		
	}
	
}
