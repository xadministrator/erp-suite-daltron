package com.struts.jreports.inv.stocktransferprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.InvRepStockTransferPrintController;
import com.ejb.txn.InvRepStockTransferPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.InvRepStockTransferPrintDetails;

public class InvRepStockTransferPrintAction extends Action {
    
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
       HttpServletRequest request, HttpServletResponse response)    
       throws Exception {

       HttpSession session = request.getSession();
           
       try {

 /*******************************************************
    Check if user has a session
 *******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          
          if (user != null) {
          	
             if (log.isInfoEnabled()) {
             	
                 log.info("InvRepStockTransferPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                 "' performed this action on session " + session.getId());
                 
             }
             
          } else {
          	
              if (log.isInfoEnabled()) {
              	
                 log.info("User is not logged on in session" + session.getId());
                
             }
             
             return(mapping.findForward("adLogon"));
             
          }
          
          InvRepStockTransferPrintForm actionForm = (InvRepStockTransferPrintForm)form; 
          
          String frParam= Common.getUserPermission(user, Constants.INV_REP_STOCK_TRANSFER_PRINT_ID);
          
          if (frParam != null) {
             
              actionForm.setUserPermission(frParam.trim());
             
          } else {
          	
              actionForm.setUserPermission(Constants.NO_ACCESS);
          }

 /*******************************************************
    Initialize InvRepStockTransferPrintController EJB
 *******************************************************/

          InvRepStockTransferPrintControllerHome homeSTP = null;
          InvRepStockTransferPrintController ejbSTP = null;       

          try {
          	
              homeSTP = (InvRepStockTransferPrintControllerHome)com.util.EJBHomeFactory.
               lookUpHome("ejb/InvRepStockTransferPrintControllerEJB", InvRepStockTransferPrintControllerHome.class);
             
          } catch(NamingException e) {
          	
             if(log.isInfoEnabled()) {
             	
                 log.info("NamingException caught in InvRepStockTransferPrintAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
             
             return(mapping.findForward("cmnErrorPage"));
             
          }

          try {
          	
              ejbSTP = homeSTP.create();
             
          } catch(CreateException e) {
          	
              if(log.isInfoEnabled()) {
              	
                  log.info("CreateException caught in InvRepStockTransferPrintAction.execute(): " + e.getMessage() +
                           " session: " + session.getId());
                 
              }
             
              return(mapping.findForward("cmnErrorPage"));
             
          }

      ActionErrors errors = new ActionErrors();
          
 	 /*** get report session and if not null set it to null **/
 	 
 	 Report reportSession = 
 	    (Report)session.getAttribute(Constants.REPORT_KEY);
 	    
 	 if(reportSession != null) {
 	 	
 	    reportSession.setBytes(null);
 	    session.setAttribute(Constants.REPORT_KEY, reportSession);
 	    
 	 }          

 /*******************************************************
    -- INV Rep Stock Transfer Print  --
 *******************************************************/       

 		if (frParam != null) {
 	        
 			    AdCompanyDetails adCmpDetails = null;
 			    InvRepStockTransferPrintDetails details = null; 
 				
 			    ArrayList list = null;   
 			    
 				try {
 					
 					ArrayList stCodeList = new ArrayList();
 					
 					if (request.getParameter("forward") != null) {
 												
 	            		stCodeList.add(new Integer(request.getParameter("stockTransferCode")));
 						
 					}
 	            	
 	            	list = ejbSTP.executeInvRepStockTransferPrint(stCodeList, user.getCmpCode());
 	            	
 			        // get company
 			       
 			        adCmpDetails = ejbSTP.getAdCompany(user.getCmpCode());	
 			        
 			        // get parameters
 	      			
 	      			Iterator i = list.iterator();
 	      			
 	      			while (i.hasNext()) {
 	      				
 	      				details = (InvRepStockTransferPrintDetails)i.next();

 	      			}
 	            			 	      			 	      			
 	            } catch(GlobalNoRecordFoundException ex) {
 	            	
 	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                    new ActionMessage("stockTransferPrint.error.stockTransferAlreadyDeleted"));
 	            		
 	            } catch(EJBException ex) {
 		     	
 	               if (log.isInfoEnabled()) {
 	               	
 	                  log.info("EJBException caught in InvStockTransferPrintAction.execute(): " + ex.getMessage() +
 	                           " session: " + session.getId());
 	               }
 	               
 	               return(mapping.findForward("cmnErrorPage"));
 	               
 	           } 
 	            
 	            if (!errors.isEmpty()) {

 	                saveErrors(request, new ActionMessages(errors));
 	                return mapping.findForward("InvRepStockTransferPrint");

 	            }
 				
 				// fill report parameters, fill report to pdf and set report session  	    	               
 			     	             	             	            
 			    Map parameters = new HashMap();
 				parameters.put("company", adCmpDetails.getCmpName());
 				parameters.put("createdBy", details.getStpStCreatedBy());
 				parameters.put("approvedBy", details.getStpStApprovedRejectedBy());
 				
 				String filename = "/opt/ofs-resources/" + user.getCompany() + "/InvRepStockTransferPrint.jasper";
 			       
 		        if (!new java.io.File(filename).exists()) {
 		       		    		    
 		           filename = servlet.getServletContext().getRealPath("jreports/InvRepStockTransferPrint.jasper");
 			    
 		        }
 				 						    	    	    	        	    				
 				try {
 					   
 				    Report report = new Report();
 				    
 				    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
 				    report.setBytes(
 				            JasperRunManager.runReportToPdf(filename, parameters, 
 				            new InvRepStockTransferPrintDS(list)));  

 				    session.setAttribute(Constants.REPORT_KEY, report);
 				    
 				} catch(Exception ex) {
 					
 				    if(log.isInfoEnabled()) {
 				   	
 				      log.info("Exception caught in InvRepStockTransferPrintAction.execute(): " + ex.getMessage() +
 				               " session: " + session.getId());
 					      
 				   } 
 				   
 				    return(mapping.findForward("cmnErrorPage"));
 					
 				}              
 				
 				return(mapping.findForward("invRepStockTransferPrint"));
 	   
 		  } else {
 		 	
 		    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
 		    saveErrors(request, new ActionMessages(errors));
 		
 		    return(mapping.findForward("invRepStockTransferPrintForm"));
 		
 		 }
 	 
      } catch(Exception e) {
      	   	
          	 
 /*******************************************************
    System Failed: Forward to error page 
 *******************************************************/
           if(log.isInfoEnabled()) {
           	
              log.info("Exception caught in InvRepStockTransferPrintAction.execute(): " + e.getMessage()
                 + " session: " + session.getId());
                 
           }        
           return(mapping.findForward("cmnErrorPage"));        
        }
     }


}
