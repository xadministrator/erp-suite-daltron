package com.struts.jreports.inv.stocktransferprint;

import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.struts.util.Common;
import com.util.InvRepStockTransferPrintDetails;

public class InvRepStockTransferPrintDS implements JRDataSource {
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public InvRepStockTransferPrintDS(ArrayList invRepSTPList) {
		
		Iterator i = invRepSTPList.iterator();
		
		while(i.hasNext()) {
			
			InvRepStockTransferPrintDetails details = (InvRepStockTransferPrintDetails)i.next();
			
			InvRepStockTransferPrintData invRepSTPData = new InvRepStockTransferPrintData(
					Common.convertSQLDateToString(details.getStpStDate()), details.getStpStDocumentNumber(), 
					details.getStpStReferenceNumber(), details.getStpStCreatedBy(), details.getStpStApprovedRejectedBy(), 
					details.getStpStDescription(), details.getStpStlLocationTo(), new Double(details.getStpStlQuantityDelivered()),
					details.getStpStlUnit(), details.getStpStlItemName(), details.getStpStlItemDescription(), 
					details.getStpStlLocationFrom(), new Double(details.getStpStlUnitPrice()), new Double(details.getStpStlAmount()),
					details.getStpStlItemCategory());
			
			data.add(invRepSTPData);
			
		}	   	   
		
	}   	   	
	
	
	
	public boolean next() throws JRException {
		
		index++;
		return (index < data.size());
		
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		
		Object value = null;
		
		String fieldName = field.getName();
		
		if("date".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getDate();       
		}else if("documentNumber".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getDocumentNumber();
		}else if("referenceNumber".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getReferenceNumber();
		}else if("createdBy".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getCreatedBy();
		}else if("approvedBy".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getApprovedBy();
		}else if("description".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getDescription();
		}else if("itemLocationTo".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getItemLocationTo();
		}else if("quantityDelivered".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getQuantityDelivered();
		}else if("unit".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getUnit();
		}else if("itemName".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getItemName();
		}else if("itemDescription".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getItemDescription();
		}else if("itemLocationFrom".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getItemLocationFrom();
		}else if("unitPrice".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getUnitPrice();
		}else if("amount".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getAmount();
		}else if("itemCategory".equals(fieldName)){
			value = ((InvRepStockTransferPrintData)data.get(index)).getItemCategory();
		}
		
		return(value);
	}
	
}
