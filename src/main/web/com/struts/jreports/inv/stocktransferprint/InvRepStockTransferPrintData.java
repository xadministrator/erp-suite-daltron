package com.struts.jreports.inv.stocktransferprint;

public class InvRepStockTransferPrintData implements java.io.Serializable {
    
    private String date = null;
    private String documentNumber = null;
    private String referenceNumber = null;
    private String createdBy = null;
    private String approvedBy = null;
    private String description = null;
    private String itemLocationTo = null;
    private Double quantityDelivered = null;
    private String unit = null;
    private String itemName = null;
    private String itemDescription = null;
    private String itemLocationFrom = null;
    private Double unitPrice = null;
    private Double amount = null;
    private String itemCategory = null;
    
    public InvRepStockTransferPrintData(
            String date,
            String documentNumber,
            String referenceNumber,
            String createdBy,
            String approvedBy,
            String description,
            String itemLocationTo,
            Double quantityDelivered,
            String unit,
            String itemName,
            String itemDescription,
            String itemLocationFrom,
            Double unitPrice,
            Double amount,
			String itemCategory) {
        
        this.date = date;
        this.documentNumber = documentNumber;
        this.referenceNumber = referenceNumber;
        this.createdBy = createdBy;
        this.approvedBy = approvedBy;
        this.description = description;
        this.itemLocationTo = itemLocationTo;
        this.quantityDelivered = quantityDelivered;
        this.unit = unit;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemLocationFrom = itemLocationFrom;
        this.unitPrice = unitPrice;
        this.amount = amount;
        this.itemCategory = itemCategory;
        
    }
    
    public String getDate() {
        
        return date;
        
    }
    
    public void setDate(String date) {
        
        this.date = date;
                
    }
    
    public String getDocumentNumber() {
        
        return documentNumber;
        
    }
    
    public void setDocumentNumber(String documentNumber) {
        
        this.documentNumber = documentNumber;
        
    }
    
    public String getReferenceNumber() {
        
        return referenceNumber;
        
    }
    
    public void setReferenceNumber(String referenceNumber) {
        
        this.referenceNumber = referenceNumber;
        
    }
    
    public String getCreatedBy() {
        
        return createdBy;
        
    }
    
    public void setCreatedBy(String createdBy) {
        
        this.createdBy = createdBy;
        
    }
    
    public String getApprovedBy() {
        
        return approvedBy;
        
    }
    
    public void setApprovedBy(String approvedBy) {
        
        this.approvedBy = approvedBy;
        
    }
    
    public String getDescription() {
        
        return description;
        
    }
    
    public void setDescription(String description) {
        
        this.description = description;
        
    }
    
    public String getItemLocationTo() {
        
        return itemLocationTo;
        
    }
    
    public void setItemLocationTo(String itemLocationTo) {
        
        this.itemLocationTo = itemLocationTo;
        
    }
    
    public Double getQuantityDelivered() {
        
        return quantityDelivered;
        
    }
    
    public void setQuantityDelivered(Double quantityDelivered) {
        
        this.quantityDelivered = quantityDelivered;
        
    }
    
    public String getUnit() {
        
        return unit;
        
    }
    
    public void setUnit(String unit) {
        
        this.unit = unit;
        
    }
    
    public String getItemName() {
        
        return itemName;
        
    }
    
    public void setItemName(String itemName) {
        
        this.itemName = itemName;
        
    }
    
    public String getItemDescription() {
        
        return itemDescription;
        
    }
    
    public void setItemDescription(String itemDescription) {
        
        this.itemName = itemDescription;
        
    }
    
    public String getItemLocationFrom() {
        
        return itemLocationFrom;
        
    }
    
    public void setItemLocationFrom(String itemLocationFrom) {
        
        this.itemLocationFrom = itemLocationFrom;
        
    }
    
    public Double getUnitPrice() {
        
        return unitPrice;
        
    }
    
    public void setUnitPrice(Double unitPrice) {
        
        this.unitPrice = unitPrice;
        
    }

    public Double getAmount() {
        
        return amount;
        
    }
    
    public void setAmount(Double amount) {
        
        this.amount = amount;
        
    }
    
    public String getItemCategory() {
    	
    	return itemCategory;
    	
    }
    
    public void setItemCategory(String itemCategory) {
    	
    	this.itemCategory = itemCategory;
    	
    }

}
