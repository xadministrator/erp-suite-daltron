package com.struts.jreports.inv.stocktransferprint;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class InvRepStockTransferPrintForm extends ActionForm implements java.io.Serializable {
    
    private String userPermission = new String();
    private Integer stockTransferCode = null;

    public String getUserPermission() {

       return userPermission;

    } 

    public void setUserPermission(String userPermission) {

       this.userPermission = userPermission;

    }   

    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	  
    }       

}
