package com.struts.jreports.inv.usagevariance;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.InvRepUsageVarianceDetails;

public class InvRepUsageVarianceDS implements JRDataSource{

   private ArrayList data = new ArrayList();

   private int index = -1;

   public InvRepUsageVarianceDS(ArrayList list) {
   	
      Iterator i = list.iterator();
      
      while (i.hasNext()) {
      	
         InvRepUsageVarianceDetails details = (InvRepUsageVarianceDetails)i.next();
                  
	     InvRepUsageVarianceData dtbData = new InvRepUsageVarianceData(
		    details.getUvItemName(), new Double(details.getUvBegInventory()),
		    new Double(details.getUvDeliveries()), new Double(details.getUvAdjustQuantity()), 
		    new Double(details.getUvEndInventory()), new Double(details.getUvActual()), 
			new Double(details.getUvStandardUsage()), new Double(details.getUvWastage()),
			new Double(details.getUvVariance()));
		    
         data.add(dtbData);
      }
      
   }

   public boolean next() throws JRException{
      index++;
      return (index < data.size());
   }

   public Object getFieldValue(JRField field) throws JRException{
      Object value = null;

      String fieldName = field.getName();

      if("rawMaterial".equals(fieldName)) {
      	
          value = ((InvRepUsageVarianceData)data.get(index)).getRawMaterial();
      
      } else if("begInventory".equals(fieldName)) {
      
          value = ((InvRepUsageVarianceData)data.get(index)).getBegInventory();
      
      } else if("deliveries".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getDeliveries();
          
      } else if("adjustQuantity".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getAdjustQuantity();
      
      } else if("endInventory".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getEndInventory();

	  } else if("actual".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getActual();

	  } else if("standard".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getStandard();
          
	  } else if("wastage".equals(fieldName)){
	      
	      value = ((InvRepUsageVarianceData)data.get(index)).getWastage();

	  } else if("variance".equals(fieldName)){
      
          value = ((InvRepUsageVarianceData)data.get(index)).getVariance();
      
      }

      return(value);
   }
}
