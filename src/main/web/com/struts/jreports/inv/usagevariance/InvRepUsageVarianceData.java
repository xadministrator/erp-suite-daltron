package com.struts.jreports.inv.usagevariance;

public class InvRepUsageVarianceData implements java.io.Serializable {
	
   private String rawMaterial = null;
   private Double begInventory = null;
   private Double deliveries = null;
   private Double adjustQuantity = null;
   private Double endInventory = null;
   private Double actual = null;
   private Double standard = null;
   private Double wastage = null;
   private Double variance = null;
   
   public InvRepUsageVarianceData(String rawMaterial, Double begInventory, Double deliveries, Double adjustQuantity, 
      Double endInventory, Double actual, Double standard, Double wastage, Double variance) {
      	
      this.rawMaterial = rawMaterial;
      this.begInventory = begInventory;
      this.deliveries = deliveries;
      this.adjustQuantity = adjustQuantity;
      this.endInventory = endInventory;
      this.actual = actual;
      this.standard = standard;
      this.wastage = wastage;
      this.variance = variance;
      
   }

   public String getRawMaterial() {
   	 
      return(rawMaterial);
   
   }

   public Double getBegInventory() {

      return begInventory;

   }

   public Double getDeliveries() {

      return deliveries;

   }
   
   public Double getAdjustQuantity() {

      return adjustQuantity;

   }

   public Double getEndInventory() {

      return endInventory;

   }

   public Double getActual() {

      return actual;

   }

   public Double getStandard() {

      return standard;

   }
   
   public Double getWastage() {
   	
   	  return wastage;
   	
   }

   public Double getVariance() {

      return variance;

   }

}
