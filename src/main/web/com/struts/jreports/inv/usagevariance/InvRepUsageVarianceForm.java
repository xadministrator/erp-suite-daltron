package com.struts.jreports.inv.usagevariance;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class InvRepUsageVarianceForm extends ActionForm implements Serializable{

   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String date = null;
   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();
   private String report = null;
   private String goButton = null;
   private String closeButton = null;
   private boolean includeAssemblyItems = false;
   private ArrayList invBrIlList = new ArrayList(); 

   private String userPermission = new String();

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public String getDate(){
      return (date);
   }
   
   public void setDate(String date){
      this.date = date;
   }

   public String getCategory(){
      return(category);
   }

   public void setCategory(String category){
      this.category = category;
   }

   public ArrayList getCategoryList(){
      return(categoryList);
   }

   public void setCategoryList(String category){
   	  categoryList.add(category);
   }

   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getLocation(){
      return(location);
   }

   public void setLocation(String location){
      this.location = location;
   }

   public ArrayList getLocationList(){
      return(locationList);
   }

   public void setLocationList(String location){
   	  locationList.add(location);
   }

   public void clearLocationList(){
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getViewType(){
   	  return(viewType);   	
   }
   
   public void setViewType(String viewType){
   	  this.viewType = viewType;
   }
   
   public ArrayList getViewTypeList(){
   	  return viewTypeList;
   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public boolean getIncludeAssemblyItems() {
       
       return includeAssemblyItems;
       
   }
   
   public void setIncludeAssemblyItems(boolean includeAssemblyItems) {
       
       this.includeAssemblyItems = includeAssemblyItems;
       
   }

	public Object[] getInvBrIlList(){
		
		return invBrIlList.toArray();
		
	}

	public InvRepUsageVarianceBranchList getInvBrIlListByIndex(int index){
		
		return ((InvRepUsageVarianceBranchList)invBrIlList.get(index));
		
	}
	
	public int getInvBrIlListSize(){
		
		return(invBrIlList.size());
		
	}
	
	public void saveInvBrIlList(Object newInvBrIlList){
		
		invBrIlList.add(newInvBrIlList);   	  
		
	}
	
	public void clearInvBrIlList(){
		
		invBrIlList.clear();
		
	}
	
   public void reset(ActionMapping mapping, HttpServletRequest request){      
   	
   	for (int i=0; i<invBrIlList.size(); i++) {
   		
   		InvRepUsageVarianceBranchList list = (InvRepUsageVarianceBranchList)invBrIlList.get(i);
   		list.setBranchCheckbox(false);	       
   		
   	}  
   	
   	goButton = null;
   	closeButton = null;
   	viewTypeList.clear();
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
   	viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
   	viewType = Constants.REPORT_VIEW_TYPE_PDF;
   	category = Constants.GLOBAL_BLANK;
   	location = Constants.GLOBAL_BLANK;      
   	date = null;
   	includeAssemblyItems = false;
   	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if(Common.validateRequired(date)){
		    errors.add("date", new ActionMessage("usageVariance.error.dateRequired"));
		 }
		 if(!Common.validateDateFormat(date)){
	            errors.add("date", new ActionMessage("usageVariance.error.dateInvalid"));
		 }
		 if(Common.validateRequired(location) || location.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
		    errors.add("location", new ActionMessage("usageVariance.error.locationRequired"));
		 }

      }
      return(errors);
   }
}
