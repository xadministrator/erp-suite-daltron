package com.struts.jreports.pm.projectprofitability;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.PmRepProjectProfitabilityController;
import com.ejb.txn.PmRepProjectProfitabilityControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.JasperRunManagerExt;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;

public final class PmRepProjectProfitabilityAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)    
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("PmRepProjectProfitabilityAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
             if (log.isInfoEnabled()) {
             	
                log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }

         PmRepProjectProfitabilityForm actionForm = (PmRepProjectProfitabilityForm)form;
      
	      // reset report to null
	      actionForm.setReport(null);
   
         String frParam = Common.getUserPermission(user, Constants.PM_REP_PROJECT_PROFITABILITY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("pmRepProjectProfitability");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize PmRepProjectProfitabilityController EJB
*******************************************************/

         PmRepProjectProfitabilityControllerHome homePP = null;
         PmRepProjectProfitabilityController ejbPP = null;       

         try {
         	
            homePP = (PmRepProjectProfitabilityControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/PmRepProjectProfitabilityControllerEJB", PmRepProjectProfitabilityControllerHome.class);
            
         } catch(NamingException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("NamingException caught in PmRepProjectProfitabilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         try {
         	
            ejbPP = homePP.create();
            
         } catch(CreateException e) {
         	
             if(log.isInfoEnabled()) {
             	
                 log.info("CreateException caught in PmRepProjectProfitabilityAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
             }
            
             return(mapping.findForward("cmnErrorPage"));
            
         }

     ActionErrors errors = new ActionErrors();
         
	 /*** get report session and if not null set it to null **/
	 
	 Report reportSession = 
	    (Report)session.getAttribute(Constants.REPORT_KEY);
	    
	 if(reportSession != null) {
	 	
	    reportSession.setBytes(null);
	    session.setAttribute(Constants.REPORT_KEY, reportSession);
	    
	 }
	 

	 
/*******************************************************
   -- INV PROFIT Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
         	ArrayList list = null;
            
            String company = null;                        

            // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();   
	        	
	        	if (!Common.validateRequired(actionForm.getProjectName())) {
	        		
	        		criteria.put("projectName", actionForm.getProjectName());
	        		
	        	}
                
	        	
	        	if (!Common.validateRequired(actionForm.getProjectType())) {
	        		
	        		criteria.put("projectType", actionForm.getProjectType());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        	
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        	
	        	}
	        	
	        	
	        	
       
	        	// save criteria

	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            ArrayList branchList = new ArrayList();
			
			for(int i=0; i<actionForm.getPmBrPttListSize(); i++) {
				
				PmRepProjectProfitabilityBranchList brList = (PmRepProjectProfitabilityBranchList)actionForm.getPmBrPttListByIndex(i);
				
				if(brList.getBranchCheckbox() == true) {
				
					AdBranchDetails mdetails = new AdBranchDetails();
					mdetails.setBrAdCompany(user.getCmpCode());
					mdetails.setBrCode(brList.getBranchCode());
					branchList.add(mdetails);
					
				}
				
			}
                        	
		    try {
		    	
		       // get company
			       
			   AdCompanyDetails adCmpDetails = ejbPP.getAdCompany(user.getCmpCode());
			   company = adCmpDetails.getCmpName();
		       
		       // execute report
		    		    
		       list = ejbPP.executePmRepProjectProfitability(actionForm.getCriteria(), branchList, actionForm.getGroupBy(), user.getCmpCode());
		           
		    } catch (GlobalNoRecordFoundException ex) {
		    	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("projectProfitability.error.noRecordFound"));
		       	 		               
		    } catch(EJBException ex) {
		    	
		         if(log.isInfoEnabled()) {
				    log.info("EJBException caught in PmRepProjectProfitabilityAction.execute(): " + ex.getMessage() +
				    " session: " + session.getId());
				    
				}
			
				return(mapping.findForward("cmnErrorPage"));
			
		    }
		    
		    if(!errors.isEmpty()) {
		    	
		       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("pmRepProjectProfitability"));
		       
		    }	
		    
		    // fill report parameters, fill report to pdf and set report session
		    
		    Map parameters = new HashMap();
		    parameters.put("company", company);
		    parameters.put("date", actionForm.getDateTo());
		    parameters.put("printedBy", user.getUserName());
		    parameters.put("datePrinted", new Date());
		    parameters.put("projectType", actionForm.getProjectType());
		    parameters.put("dateFrom", actionForm.getDateFrom());
		    parameters.put("dateTo", actionForm.getDateTo());
		    
		    if(actionForm.getShowEntries()) {
		    	parameters.put("showEntries", "YES");
		    } else {
		    	parameters.put("showEntries", "NO");
		    }
		    
		    parameters.put("viewType", actionForm.getViewType());
		    
		    Date firstDayOfPeriod = null;
		    Calendar dtCal = new GregorianCalendar();
		    dtCal.setTime(Common.convertStringToSQLDate(actionForm.getDateTo()));
		    dtCal.set(Calendar.DATE,1);
		    firstDayOfPeriod = dtCal.getTime();
		  	System.out.println("firstDayOfPeriod="+firstDayOfPeriod);
		    parameters.put("firstDayOfPeriod", firstDayOfPeriod);
		    
		    
		    String branchMap = null;
      		boolean first = true;
      		for(int j=0; j<actionForm.getPmBrPttListSize(); j++) {
				
				PmRepProjectProfitabilityBranchList brList = (PmRepProjectProfitabilityBranchList)actionForm.getPmBrPttListByIndex(j);
				
      			if(brList.getBranchCheckbox() == true) {
      				if(first) {
      					branchMap = brList.getBranchName();
      					first = false;
      				} else {
      					branchMap = branchMap + ", " + brList.getBranchName();
      				}
      			}
      		
      		}
      		parameters.put("branchMap", branchMap);
	    
		    String filename = "/opt/ofs-resources/" + user.getCompany() + "/PmRepProjectProfitability.jasper";
		       
	        if (!new java.io.File(filename).exists()) {
	       		    		    
	           filename = servlet.getServletContext().getRealPath("jreports/PmRepProjectProfitability.jasper");
	           
	        }	 
		    
		    
		    try {
		    	
		    	Report report = new Report();
	       
		       if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
		       	
		       	   report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			       report.setBytes(
			          JasperRunManager.runReportToPdf(filename, parameters, 
				        new PmRepProjectProfitabilityDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)){
	               
	               report.setViewType(Constants.REPORT_VIEW_TYPE_EXCEL);
	               report.setBytes(
					   JasperRunManagerExt.runReportToXls(filename, parameters, 
					        new PmRepProjectProfitabilityDS(list, actionForm.getGroupBy())));   
				        
			   } else if (actionForm.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)){
				   
				   report.setViewType(Constants.REPORT_VIEW_TYPE_HTML);	   						
				   report.setJasperPrint(JasperFillManager.fillReport(filename, parameters, 
				       new PmRepProjectProfitabilityDS(list, actionForm.getGroupBy())));												    
				        
			   }
			   
		       session.setAttribute(Constants.REPORT_KEY, report);
		       actionForm.setReport(Constants.STATUS_SUCCESS);		    	
		        		       
		    } catch(Exception ex) {
		    	
		        if(log.isInfoEnabled()) {
		           log.info("Exception caught in PmRepProjectProfitabilityAction.execute(): " + ex.getMessage() +
			   " session: " + session.getId());
			   
		        }
		        
			return(mapping.findForward("cmnErrorPage"));
			
		    }		    	    
	   	        		        	          
/*******************************************************
   -- INV PROFIT Close Action --
*******************************************************/

		     } else if (request.getParameter("closeButton") != null) {
		
		          return(mapping.findForward("cmnMain"));
		          
/*******************************************************
   -- INV PROFIT Load Action --
*******************************************************/

             }
         
	         if(frParam != null) {
	         	
	         	try {
		    	
			        ArrayList list = null;			       			       
			        Iterator i = null;
			        
			        
	            	
	            	actionForm.clearProjectTypeList();           	
	            	
	            	list = ejbPP.getPmProjTypAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setProjectTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setProjectTypeList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
	            	actionForm.clearPmBrPttList();
					
					list = ejbPP.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
					
					//boolean isFirst = true;
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						AdBranchDetails details = (AdBranchDetails)i.next();
						
						PmRepProjectProfitabilityBranchList pmBrPttList = new PmRepProjectProfitabilityBranchList(actionForm,
								details.getBrBranchCode(), details.getBrName(), details.getBrCode());
						if (details.getBrHeadQuarter() == 1 ) {
							pmBrPttList.setBranchCheckbox(true);
							//isFirst = false;
						}
						
						actionForm.savePmBrPttList(pmBrPttList);
						
					}	
			       
			       
				} catch(EJBException ex) {
			    	
			         if(log.isInfoEnabled()) {
					    log.info("EJBException caught in PmRepProjectProfitabilityAction.execute(): " + ex.getMessage() +
					    " session: " + session.getId());
					    
					}
				
					return(mapping.findForward("cmnErrorPage"));
				
			    }
	            
	            return(mapping.findForward("pmRepProjectProfitability"));		          
			            
				 } else {
				 	
				    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				    saveErrors(request, new ActionMessages(errors));
				
				    return(mapping.findForward("cmnMain"));
				
				 }
	 
         } catch(Exception e) {
     	   	
         	 
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
	      if(log.isInfoEnabled()) {
	      	
	         log.info("Exception caught in PmRepProjectProfitabilityAction.execute(): " + e.getMessage()
	            + " session: " + session.getId());
	            
	       }   
		           
		  return(mapping.findForward("cmnErrorPage"));   
		           
        }
    }
}
