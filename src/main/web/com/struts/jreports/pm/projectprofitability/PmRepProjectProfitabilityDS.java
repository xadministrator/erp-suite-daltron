package com.struts.jreports.pm.projectprofitability;


import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.util.PmRepProjectProfitabilityDetails;

public class PmRepProjectProfitabilityDS implements JRDataSource{
	
	private ArrayList data = new ArrayList();
	
	private int index = -1;
	
	public PmRepProjectProfitabilityDS(ArrayList list, String groupBy) {
		
		Iterator i = list.iterator();
		
		while (i.hasNext()) {
			
			PmRepProjectProfitabilityDetails details = (PmRepProjectProfitabilityDetails)i.next();

	         
			PmRepProjectProfitabilityData dtbData = new PmRepProjectProfitabilityData(
					details.getPpProjectName(), details.getPpProjectDescription(), details.getPpProjectTypeName(), details.getPpProjectTypeDescription(), details.getPpProjectPhaseName(), 
					details.getPpContractAmount(),
					
					details.getPpItemName(), details.getPpItemDescription(),
					details.getPpDate(), details.getPpDocumentNumber(), details.getPpAmount()
					);
			
			data.add(dtbData);
									
		}
		
	}
	
	public boolean next() throws JRException{
		index++;
		return (index < data.size());
	}
	
	public Object getFieldValue(JRField field) throws JRException{
		Object value = null;
		
		String fieldName = field.getName();
		
		if("projectName".equals(fieldName)) {
			value = ((PmRepProjectProfitabilityData)data.get(index)).getProjectName();
		} else if("projectDescription".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getProjectDescription();
		} else if("projectTypeName".equals(fieldName)) {
				value = ((PmRepProjectProfitabilityData)data.get(index)).getProjectTypeName();		
		} else if("projectTypeDescription".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getProjectTypeDescription();
		} else if("projectPhaseName".equals(fieldName)) {
			value = ((PmRepProjectProfitabilityData)data.get(index)).getProjectPhaseName();	
		} else if("contractAmount".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getContractAmount();
			
			
		} else if("itemName".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getItemName();
		} else if("itemDescription".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getItemDescription();
		} else if("date".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getDate();
		} else if("documentNumber".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getDocumentNumber();
		} else if("amount".equals(fieldName)) {	
			value = ((PmRepProjectProfitabilityData)data.get(index)).getAmount();
			
		
			
			
		} 
		
		return(value);
		
	}
	
}
