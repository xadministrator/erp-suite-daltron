package com.struts.jreports.pm.projectprofitability;

import java.util.Date;

public class PmRepProjectProfitabilityData implements java.io.Serializable {
	
	private String projectName = null;
	private String projectDescription = null;
	private String projectTypeName = null;
	private String projectTypeDescription = null;
	private String projectPhaseName = null;
	private String itemName = null;
	private String itemDescription = null;
	private Date date = null;
	private String documentNumber = null;
	private Double amount = null; 
	
	private Double contractAmount = null; 

	
	public PmRepProjectProfitabilityData(
						String projectName, String projectDescription, String projectTypeName, String projectTypeDescription, String projectPhaseName,
						Double contractAmount,
						
						String itemName, String itemDescription,
						Date date, String documentNumber, Double amount
						
						) {
			
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.projectTypeName = projectTypeName;
		this.projectTypeDescription = projectTypeDescription;
		this.contractAmount = contractAmount;
		
		
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.date = date;
		this.documentNumber = documentNumber;
		this.amount = amount;
		
		
			
	}

	public String getProjectName() {
		
		return projectName;
	
	}
	
	public String getProjectDescription() {
	
		return projectDescription;
	
	}
	
	
	public String getProjectTypeName() {
		
		return projectTypeName;
	
	}
	
	public String getProjectTypeDescription() {
	
		return projectTypeDescription;
	
	}
	
	public String getProjectPhaseName() {
		
		return projectPhaseName;
	
	}
	
	
	
	public Double getContractAmount() {
		
		return contractAmount;
	
	}
	
	
	
	public String getItemName() {
		
		return itemName;
	
	}
	
	public String getItemDescription() {
		
		return itemDescription;
	
	}
	
	public Date getDate() {
		
		return date;
	
	}

	public String getDocumentNumber() {
		
		return documentNumber;
	
	}
	
	public Double getAmount() {
		
		return amount;
	
	}
	

	

}
