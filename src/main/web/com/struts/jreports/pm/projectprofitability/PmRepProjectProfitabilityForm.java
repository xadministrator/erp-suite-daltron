package com.struts.jreports.pm.projectprofitability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class PmRepProjectProfitabilityForm extends ActionForm implements Serializable{
	
	private String projectName = null;
	private String projectType = null;
	private ArrayList projectTypeList = new ArrayList();
	private String date = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	
	private String report = null;
	private String goButton = null;
	private String closeButton = null;
	private ArrayList pmBrPttList = new ArrayList();
	private String groupBy = null;
	private ArrayList groupByList = new ArrayList();
	
	private boolean showEntries = false;
	
	private HashMap criteria = new HashMap();
	
	private String userPermission = new String();
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getDateFrom() {
		
		return (dateFrom);
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return (dateTo);
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	public String getProjectName() {
		
		return(projectName);
		
	}
	
	public void setProjectName(String projectName) {
		
		this.projectName = projectName;
		
	}
		
	
	
	public String getProjectType() {
		
		return(projectType);
		
	}
	
	public void setProjectType(String projectType) {
		
		this.projectType = projectType;
		
	}
	
	public ArrayList getProjectTypeList() {
		
		return(projectTypeList);
		
	}
	
	public void setProjectTypeList(String projectType) {
		
		projectTypeList.add(projectType);
		
	}
	
	public void clearProjectTypeList() {
		
		projectTypeList.clear();
		projectTypeList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getViewType() {
		
		return(viewType);
		
	}
	
	public void setViewType(String viewType) {
		
		this.viewType = viewType;
		
	}
	
	public ArrayList getViewTypeList() {
		
		return viewTypeList;
		
	}
	
	
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Object[] getPmBrPttList(){
		
		return pmBrPttList.toArray();
		
	}

	public PmRepProjectProfitabilityBranchList getPmBrPttListByIndex(int index){
		
		return ((PmRepProjectProfitabilityBranchList)pmBrPttList.get(index));
		
	}
	
	public int getPmBrPttListSize(){
		
		return(pmBrPttList.size());
		
	}
	
	public void savePmBrPttList(Object newPmBrPttList){
		
		pmBrPttList.add(newPmBrPttList);   	  
		
	}
	
	public void clearPmBrPttList(){
		
		pmBrPttList.clear();
		
	}
	
	public String getGroupBy() {

		return groupBy;

	}

	public void setGroupBy(String groupBy) {

		this.groupBy = groupBy;

	}

	public ArrayList getGroupByList() {

		return groupByList;

	}
	
	
	
	public void setShowEntries(boolean showEntries) {
		   
	   this.showEntries = showEntries;
	   
   }
   
   public boolean getShowEntries() {
	   
	   return showEntries;
	   
   }
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		for (int i=0; i<pmBrPttList.size(); i++) {
			
			PmRepProjectProfitabilityBranchList list = (PmRepProjectProfitabilityBranchList)pmBrPttList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		goButton = null;
		closeButton = null;
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;

		projectName = null;
		
		projectType = Constants.GLOBAL_BLANK;
		dateFrom = null;
		dateTo = Common.convertSQLDateToString(new Date());
		groupByList.clear();
		groupByList.add(Constants.GLOBAL_BLANK);
		groupByList.add("CATEGORY");
		groupBy = Constants.GLOBAL_BLANK;
		
		showEntries = false;


	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null) {
												
			if(!Common.validateDateFromTo(dateFrom, dateTo)) {
				
				errors.add("dateTo", new ActionMessage("projectProfitability.error.dateFromAndToRangeIsInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom", new ActionMessage("projectProfitability.error.dateFromInvalid"));
				
			}
			
			if(!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo", new ActionMessage("projectProfitability.error.dateToInvalid"));
				
			}			
						
		}
		
		return(errors);
		
	}
	
}
