package com.struts.pm.findproject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.PmFindProjectController;
import com.ejb.txn.PmFindProjectControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.PmModProjectDetails;

public final class PmFindProjectAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("PmFindProjectAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         PmFindProjectForm actionForm = (PmFindProjectForm)form;
         
         String frParam = null;
         

         if (request.getParameter("child") == null) {
         
            frParam = Common.getUserPermission(user, Constants.PM_FIND_PROJECT_ID);
         	
         } else {
         	
         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  
                  if (request.getParameter("child") != null ){
                  	
                  	return mapping.findForward("pmFindProjectChild");
                  	
                  } else {
                  
                  	return mapping.findForward("pmFindProject");
                  	
                  }
                  	
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize PmFindProjectController EJB
*******************************************************/

         PmFindProjectControllerHome homeFP = null;
         PmFindProjectController ejbFP = null;

         try {

            homeFP = (PmFindProjectControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/PmFindProjectControllerEJB", PmFindProjectControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in PmFindProjectAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFP = homeFP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in PmFindProjectAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
/*******************************************************
   Call PmFindProjectController EJB
   getGlFcPrecisionUnit
*******************************************************/         
                  
         short precisionUnit = 0;
         
			try {
				
				precisionUnit = ejbFP.getGlFcPrecisionUnit(user.getCmpCode());           
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in PmFindProjectAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}	         
         
/*******************************************************
   -- Pm FP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	        
            if (request.getParameter("child") != null ){
              	
              	return mapping.findForward("pmFindProjectChild");

              } else {
              
              	return mapping.findForward("pmFindProject");
              	
              }	     
            
/*******************************************************
   -- Pm FP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
            if (request.getParameter("child") != null ){
              	
              	return mapping.findForward("pmFindProjectChild");
              	
           	
              } else {
              
              	return mapping.findForward("pmFindProject");
              	
              }
/*******************************************************
    -- Pm FP First Action --
 *******************************************************/ 

	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	        
/*******************************************************
   -- Pm FP Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Pm FP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         }
         
/*******************************************************
   -- Pm FP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            System.out.println("1");
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	System.out.println("2");
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getProjectName())) {
	        		
	        		criteria.put("projectName", actionForm.getProjectName());
	        		
	        	}
	        	
	        	

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFP.getPmPrjSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearPmFPList();
            	
            	ArrayList list = ejbFP.getPmPrjByCriteria(actionForm.getCriteria(),
            		new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
					actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		PmModProjectDetails mdetails = (PmModProjectDetails)i.next();
      		
            		PmFindProjectList pmFPList = new PmFindProjectList(actionForm,
            		    mdetails.getPrjCode(),
            		    mdetails.getPrjProjectCode(),
            		    mdetails.getPrjName());
            		    
            		actionForm.savePmFPList(pmFPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findProject.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in PmFindProjectAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
               if (request.getParameter("child") != null ){
              	
              	return mapping.findForward("pmFindProjectChild");

              } else {
              
              	return mapping.findForward("pmFindProject");
              	
              }
          }
                        
            //actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            if (request.getParameter("child") != null ){
              	
              	return mapping.findForward("pmFindProjectChild");

              } else {
              
              	return mapping.findForward("pmFindProject");
              	
              }
            
/*******************************************************
   -- Pm FP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Pm FP Open Action --
*******************************************************/

         } else if (request.getParameter("pmFPList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
        	 
             PmFindProjectList pmFpList =
                actionForm.getPmFPByIndex(actionForm.getRowSelected());

             String path = "/pmProjectEntry.do?forward=1" +
    			     "&prjCode=" + pmFpList.getProjectCode();
                
             return(new ActionForward(path));

/*******************************************************
   -- PM FP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearPmFPList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in PmFindProjectAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            System.out.println("actionForm.getTableType()="+actionForm.getTableType());
            
	        if (actionForm.getTableType() == null) {
      		      	
	        	actionForm.setProjectName(null);
	        	actionForm.setProjectDescription(null);
	        	actionForm.setOrderBy("PROJECT NAME");
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	          
	            actionForm.setCriteria(criteria);
	            
	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	
            	try {
                	
                	actionForm.clearPmFPList();
                	
                	ArrayList newList = ejbFP.getPmPrjByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	  newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		PmModProjectDetails mdetails = (PmModProjectDetails)j.next();
          		
                		PmFindProjectList pmFPList = new PmFindProjectList(actionForm,
                		    mdetails.getPrjCode(),
                		    mdetails.getPrjProjectCode(),
                		    mdetails.getPrjName());
                		    
                		actionForm.savePmFPList(pmFPList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findProject.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in PmFindProjectAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
            
	        if (request.getParameter("child") != null ) {
            	// child
                // save criteria
	        	System.out.println("child------------------------------");
	        	try {
	        	
	        	actionForm.setLineCount(0);
	        	
	        	HashMap criteria = new HashMap();
	        	
	        	actionForm.setCriteria(criteria);
	        		                                        
            	actionForm.clearPmFPList();
            	
            	ArrayList newList = ejbFP.getPmPrjByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1),
					actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
	           actionForm.setDisablePreviousButton(true);
	           actionForm.setDisableFirstButton(true);
	            	
	           // check if next should be disabled
	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);	
	           	  
	           	  //remove last record
	           	  newList.remove(newList.size() - 1);
	           	
	           }
	           
               i = newList.iterator();
            	
            	while (i.hasNext()) {
            		
            		PmModProjectDetails mdetails = (PmModProjectDetails)i.next();
      		
            		PmFindProjectList pmFpList = new PmFindProjectList(actionForm,
            		    mdetails.getPrjCode(),
            		    mdetails.getPrjProjectCode(),
            		    mdetails.getPrjName()
            		    );
            		    
            		actionForm.savePmFPList(pmFpList);
            		
            	}

	        	} catch (GlobalNoRecordFoundException ex) {
               
	        		// disable prev next buttons
	        	   actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("findProject.error.noRecordFound"));
	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in PmFindProjectAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }    
	            
	        	actionForm.setSelectedProjectName(request.getParameter("selectedProjectName"));
	            actionForm.setSelectedProjectDescription(request.getParameter("selectedProjectDescription"));
	            actionForm.setSelectedProjectEntered(request.getParameter("selectedProjectEntered"));

	            return(mapping.findForward("pmFindProjectChild"));
            } else {
            	// find item

            	return(mapping.findForward("pmFindProject"));
            	
            }

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in PmFindProjectAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}