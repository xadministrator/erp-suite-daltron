package com.struts.pm.findproject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class PmFindProjectForm extends ActionForm implements Serializable {
	
	private String projectName = null;
	private String projectDescription = null;

	private String selectedProjectName = null;
	private String selectedProjectDescription = null;
	private String selectedProjectEntered = null;
	
	
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList pmFPList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount(){
		return lineCount;   	 
	}
	
	public void setLineCount(int lineCount){  	
		this.lineCount = lineCount;
	}
	
	public PmFindProjectList getPmFPByIndex(int index) {
		
		return((PmFindProjectList)pmFPList.get(index));
		
	}
	
	public Object[] getPmFPList() {
		
		return pmFPList.toArray();
		
	}
	
	public int getPmFPListSize() {
		
		return pmFPList.size();
		
	}
	
	public void savePmFPList(Object newPmFPList) {
		
		pmFPList.add(newPmFPList);
		
	}
	
	public void clearPmFPList() {
		
		pmFPList.clear();
		
	}
	
	public void setRowSelected(Object selectedPmFPList, boolean isEdit) {
		
		this.rowSelected = pmFPList.indexOf(selectedPmFPList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getProjectName() {
	
		return projectName;
		
	}
	
	public void setProjectName(String projectName) {
	
		this.projectName = projectName;
		
	}
	
	public String getProjectDescription() {
		
		return projectDescription;
		
	}
	
	public void setProjectDescription(String projectDescription) {
	
		this.projectDescription = projectDescription;
		
	}
	
	
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public String getSelectedProjectName() {
		
		return selectedProjectName;
		
	}
	
	public void setSelectedProjectName(String selectedProjectName) {
		
		this.selectedProjectName = selectedProjectName;
		
	}
	

	public String getSelectedProjectDescription() {
		
		return selectedProjectDescription;
		
	}
	
	public void setSelectedProjectDescription(String selectedProjectDescription) {
		
		this.selectedProjectDescription = selectedProjectDescription;
		
	}

	public String getSelectedProjectEntered() {
		
		return selectedProjectEntered;
		
	}
	
	public void setSelectedProjectEntered(String selectedProjectEntered) {
		
		this.selectedProjectEntered = selectedProjectEntered;
		
	}
	
	
	public String getOrderBy() {
	   	
		return orderBy;
   	  
	}
   
	public void setOrderBy(String orderBy) {
   	
		this.orderBy = orderBy;
   	  
	}
   
	public ArrayList getOrderByList() {
   	
		return orderByList;
   	  
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		
		showDetailsButton = null;
		hideDetailsButton = null;
		
		if (orderByList.isEmpty()) { 
		    
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("PROJECT NAME");

		} 
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
		}
		
		return errors;
		
	}
}