package com.struts.pm.findproject;

import java.io.Serializable;

public class PmFindProjectList implements Serializable {
	
	private Integer projectCode = null;
	private String projectName = null;
	private String projectDescription = null;
	
	
	private String openButton = null;
	
	private PmFindProjectForm parentBean;
	
	public PmFindProjectList(PmFindProjectForm parentBean,
			Integer projectCode,  
			String projectName,
			String projectDescription 
			) {
		
		this.parentBean = parentBean;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		
		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getProjectCode() {
		
		return projectCode;
		
	}
	
	public String getProjectName() {
		
		return projectName;
		
	}
	
	
	public String getProjectDescription() {
		
		return projectDescription;
		
	}
	
	
	
}