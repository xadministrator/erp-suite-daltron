package com.struts.pm.findprojecttype;

import java.io.Serializable;

public class PmFindProjectTypeList implements Serializable {
	
	private Integer projectTypeCode = null;
	private String projectTypeProjectTypeCode = null;
	private String projectTypeValue = null;
	
	
	private String openButton = null;
	
	private PmFindProjectTypeForm parentBean;
	
	public PmFindProjectTypeList(PmFindProjectTypeForm parentBean,
			Integer projectTypeCode,
			String projectTypeProjectTypeCode,
			String projectTypeValue) {
		
		this.parentBean = parentBean;
		this.projectTypeCode = projectTypeCode;
		this.projectTypeProjectTypeCode = projectTypeProjectTypeCode;
		this.projectTypeValue = projectTypeValue;

		
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getProjectTypeCode() {
		
		return projectTypeCode;
		
	}
	
	public String getProjectTypeProjectTypeCode() {
		
		return projectTypeProjectTypeCode;
		
	}
	
	public String getProjectTypeValue() {
		
		return projectTypeValue;
		
	}
	
	
	
	
}