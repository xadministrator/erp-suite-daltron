package com.struts.pm.synchronizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.PmSynchronizerController;
import com.ejb.txn.PmSynchronizerControllerHome;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

import sun.net.www.protocol.http.HttpURLConnection;

public final class PmSynchronizerAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      
      
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("HrSynchronizerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         PmSynchronizerForm actionForm = (PmSynchronizerForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.PM_SYNCHRONIZER);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("pmSynchronizer");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize hrSynchronizerController EJB
*******************************************************/

         
         PmSynchronizerControllerHome homeSYNC = null;
         PmSynchronizerController ejbSYNC = null;
         

         try {

        	 homeSYNC = (PmSynchronizerControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/PmSynchronizerControllerEJB", PmSynchronizerControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in PmSynchronizeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

        	 ejbSYNC = homeSYNC.create();
        	 
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in PmSynchronizeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad AS Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AS Save Action --
*******************************************************/

         } else if (request.getParameter("synchronizeButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            System.out.println("PM synchronizeButton");

            try {
            	
            	ejbSYNC.setPmProject(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmProjectType(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmContract(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmContractTerm(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmProjectTypeType(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmProjectPhase(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmUser(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmProjectTimeRecord(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmUserAsOmegaUser(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setPmUsersAsCustomerAndSupplier(user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in PmSynchronizeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad AS Approval Document Action --
*******************************************************/

         // forward to approval document module             	
            	
/*         } else if (request.getParameter("approvalDocumentButton") != null) {

			return(new ActionForward("/adApprovalDocument.do"));     */        
            
/*******************************************************
   -- Ad AS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("pmSynchronizer");

            }
                         	                          	            	
	        try {
	        	
            	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in PmSynchronizerAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("pmSynchronizer"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in PmSynchronizerAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}