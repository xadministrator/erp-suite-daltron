package com.struts.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;

import org.apache.struts.upload.FormFile;

public class Attachment {
	
	
	
	
	public static void uploadAttachment(FormFile formFile ,String fileName , String attachmentPath, String fileAttachment){
					

    	String fileExtension = fileName.substring(fileName.lastIndexOf("."));

    	new File(attachmentPath).mkdirs();

    	if(fileExtension==null) {
    		System.out.println("fileExtension is null");
    		fileExtension = ".fed";
    	}
		

		try{
			InputStream is = formFile.getInputStream();
		
			File fileTest = new File(attachmentPath + fileAttachment + ".txt");
        	FileWriter writer = new FileWriter(fileTest);
            writer.write(fileAttachment + fileName);
            writer.close();

    		FileOutputStream fos = new FileOutputStream(attachmentPath + fileAttachment + "-" + fileName);

    		int c;

        	while ((c = is.read()) != -1) {

        		fos.write((byte)c);

        	}
        	is.close();
			fos.close();

		}catch(Exception ex){
		
		}
	}
	            	
}
