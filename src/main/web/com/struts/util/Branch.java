package com.struts.util;

import java.io.Serializable;

public class Branch implements Serializable{
   private int brCode = 0;
   private String brBranchCode = null;
   private String brName = null;

   public int getBrCode(){
   	  return(brCode);
   }

   public void setBrCode(int brCode){
      this.brCode = brCode;
   }
   
   public String getBrBranchCode() {
   	  return brBranchCode;
   }
   
   public void setBrBranchCode(String brBranchCode) {
   	  this.brBranchCode = brBranchCode;
   }

   public String getBrName(){
      return(brName);
   }

   public void setBrName(String brName){
      this.brName = brName;
   }

}
