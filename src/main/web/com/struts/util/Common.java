package com.struts.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.sql.DataSource;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.util.InvModTagListDetails;

public final class Common{

    public static String DATE_FORMAT_INPUT = null;
    public static String DATE_FORMAT_OUTPUT = null;


   public static boolean validateDateEnabled(Date dateFrom, Date dateTo){
      GregorianCalendar gcCurrDate = Common.getGcCurrentDateWoTime();
      GregorianCalendar gcDateFrom = new GregorianCalendar();
      GregorianCalendar gcDateTo = new GregorianCalendar();

      if(dateFrom != null){
         gcDateFrom.setTime(dateFrom);
         if (dateTo != null)
            gcDateTo.setTime(dateTo);
         if(gcCurrDate.getTime().getTime() < gcDateFrom.getTime().getTime() ||
            gcCurrDate.getTime().getTime() > gcDateTo.getTime().getTime() && dateTo != null)
            return(false);
         return(true);
      }else{
         return(false);
      }
   }

   public static boolean validateDateFromTo(String strDateFrom, String strDateTo){
      GregorianCalendar gcDateFrom = new GregorianCalendar();
      GregorianCalendar gcDateTo = new GregorianCalendar();

      if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
          loadDateFormat();
      }

      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
      Date dateFrom = null;
      Date dateTo = null;

      sdf.setLenient(false);
      if(strDateFrom != null || strDateFrom.length() >= 1){
         try{
            dateFrom = sdf.parse(strDateFrom.trim());
         }catch(ParseException e){
            return(true);
         }
         gcDateFrom.setTime(dateFrom);
         if(strDateTo != null || strDateTo.length() >= 1){
            try{
               dateTo = sdf.parse(strDateTo.trim());
            }catch(ParseException e){
               return(true);
            }
            gcDateTo.setTime(dateTo);
            if(gcDateFrom.getTime().getTime() <= gcDateTo.getTime().getTime()){
               return(true);
            }else{
               return(false);
            }
          }else{
             return(true);
          }
       }else{
          return(true);
       }
   }

   public static boolean validateDateGreaterThanCurrent(String strDate){
      GregorianCalendar gcCurrDate = new GregorianCalendar();
      gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
      gcCurrDate.set(Calendar.MILLISECOND, 0);

      if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
          loadDateFormat();
      }
      GregorianCalendar gcDate = new GregorianCalendar();
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
      Date date = null;

      sdf.setLenient(false);
      if(strDate != null){
         try{
	    date = sdf.parse(strDate.trim());
	 }catch(ParseException e){
            return(true);
	 }
	 gcDate.setTime(date);
	 if(gcDate.getTime().getTime() < gcCurrDate.getTime().getTime()){
	    return(false);
	 }else{
	    return(true);
	 }
      }else{
         return(true);
      }

   }

   public static boolean validateDateLessThanCurrent(String strDate){
    GregorianCalendar gcCurrDate = new GregorianCalendar();
    gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
    gcCurrDate.set(Calendar.MILLISECOND, 0);
    if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
        loadDateFormat();
    }

    GregorianCalendar gcDate = new GregorianCalendar();
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
    Date date = null;

    sdf.setLenient(false);
    if(strDate != null){
       try{
	    date = sdf.parse(strDate.trim());
	 }catch(ParseException e){
          return(true);
	 }
	 gcDate.setTime(date);
	 if(gcDate.getTime().getTime() > gcCurrDate.getTime().getTime()){
	    return(false);
	 }else{
	    return(true);
	 }
    }else{
       return(true);
    }

 }


   public static boolean validateRequired(String strRequired){
      if(strRequired == null || strRequired.length() < 1){
         return(true);
      }else{
         return(false);
      }
   }

   public static boolean validateDateFormat(String strDate){
	   if(strDate != null  && strDate.length() >= 1){

                if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
                    loadDateFormat();
                }
		   SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
		   sdf.setLenient(false);
		   try{
			   Date dt = sdf.parse(strDate.trim());
			   return	(true);

		   }catch(ParseException e){

			   return(false);
		   }
	   }else{

		   return(true);
	   }
   }

   public static String formatDate(String strDateInput){
      if(strDateInput != null && strDateInput.length() >= 1){

         if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
             loadDateFormat();
         }
         SimpleDateFormat sdfDateInput = new SimpleDateFormat(DATE_FORMAT_INPUT);
         SimpleDateFormat sdfDateOutput = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
         sdfDateInput.setLenient(false);
         try{
            return(sdfDateOutput.format(sdfDateInput.parse(strDateInput)));
         }catch(ParseException e){
            return(null);
         }
      }
      else{
         return(null);
      }
   }

   public static Date convertStringToSQLDate(String strDateInput){
      if(strDateInput != null && strDateInput.length() >= 1){
           if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
             loadDateFormat();
         }
         SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
         sdf.setLenient(false);
         try{
            return(sdf.parse(strDateInput));
         }catch(ParseException e){
            return(null);
         }
      }
      else{
         return(null);
      }
   }


   public static Date convertStringToSQLDateMMDDYYYY(String strDateInput){
	      if(strDateInput != null && strDateInput.length() >= 1){
	         SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_MMDDYYYY);
	         sdf.setLenient(false);
	         try{
	            return(sdf.parse(strDateInput));
	         }catch(ParseException e){
	            return(null);
	         }
	      }
	      else{
	         return(null);
	      }
	   }


   public static double convertStringToDouble(String strDouble){

	   if(strDouble != null && strDouble.length() >= 1){
		   try{
			   return(Double.parseDouble(strDouble.trim()));
		   }catch(NumberFormatException nfe){
			   return(0);
		   }
	   }else{
		   return(0);
	   }
   }



   public static String convertSQLDateToString(Date dtInput){
      if(dtInput != null){

           if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
             loadDateFormat();
         }
         SimpleDateFormat sdfDateOutput = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
         return(sdfDateOutput.format(dtInput));
      }
      else{
         return(null);
      }
   }


   public static String convertSQLDateTimeToString(Date dtInput){
	      if(dtInput != null){

	           if(DATE_FORMAT_INPUT==null || DATE_FORMAT_OUTPUT==null){
	             loadDateFormat();
	         }
	         SimpleDateFormat sdfDateOutput = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
	         return(sdfDateOutput.format(dtInput));
	      }
	      else{
	         return(null);
	      }
	   }


   public static String convertSQLLocalDateToString(LocalDate dtInput){
	      if(dtInput != null){

	    	  DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");

	         return(dtInput.toString( fmt ));
	      }
	      else{
	         return(null);
	      }
	   }


   public static String convertSQLDateAndTimeToString(Date dtInput){
    if(dtInput != null){
       SimpleDateFormat sdfDateOutput = new SimpleDateFormat(Constants.DATETIME_FORMAT_OUTPUT);
       return(sdfDateOutput.format(dtInput));
    }
    else{
       return(null);
    }
 }

   public static String getUserPermission(User user, int txnID){

   	  FormFunctionRes ffRes = user.getFFResByKey(String.valueOf(txnID) + String.valueOf(user.getCurrentResCode()));

   	  if (ffRes != null) {

   	      return(ffRes.getFrParam().trim());

   	  }

      return(null);
   }

   public static short convertStringToShort(String strShort){
      if(strShort != null && strShort.length() >= 1){
         try{
	    return(Short.parseShort(strShort.trim()));
	 }catch(NumberFormatException nfe){
	    return(0);
	 }
      }else{
         return(0);
      }
   }

   public static int convertStringToInt(String strInt){
      if(strInt != null && strInt.length() >= 1){
         try{
	    return(Integer.parseInt(strInt.trim()));
	 }catch(NumberFormatException nfe){
	    return(0);
	 }
       }else{
	    return(0);
       }
   }

   public static Integer convertStringToInteger(String strInteger){
      if(strInteger != null && strInteger.length() >= 1){
         return(new Integer(strInteger));
      }else{
         return(null);
      }
   }

   public static String convertIntegerToString(Integer integerValue){
      if(integerValue != null){
         return(String.valueOf(integerValue.intValue()));
      }else{
         return(null);
      }
   }

   public static long convertStringToLong(String strLong){
      if(strLong != null && strLong.length() >= 1){
         try{
	    return(Long.parseLong(strLong.trim()));
	 }catch(NumberFormatException nfe){
	    return(0);
	 }
      }
      else{
         return(0);
      }
   }

   public static char convertStringToChar(String strChar){
      if(strChar != null && strChar.length() >= 1){
         return(strChar.charAt(0));
      }else{
         return(' ');
      }
   }

   public static String convertCharToString(char cChar){
      if(cChar == ' '){
         return(null);
      }
      return(new Character(cChar).toString());
   }

   public static String convertLongToString(long lLong){
      return(Long.toString(lLong));
   }

   public static String convertShortToString(short sShort){
      return(Short.toString(sShort));
   }

   public static String getGlYearTypeDescription(char cYearTypeCode){
      switch(cYearTypeCode){
         case Constants.GL_YEAR_TYPE_FY:
	    return(Constants.GL_YEAR_TYPE_FISCAL_YEAR);
	 case Constants.GL_YEAR_TYPE_CY:
	    return(Constants.GL_YEAR_TYPE_CALENDAR_YEAR);
	 default:
	    return(null);
      }
   }

   public static char getGlYearTypeCode(String strYearType){
      if(strYearType.equals(Constants.GL_YEAR_TYPE_CALENDAR_YEAR)){
         return(Constants.GL_YEAR_TYPE_CY);
      }else if(strYearType.equals(Constants.GL_YEAR_TYPE_FISCAL_YEAR)){
         return(Constants.GL_YEAR_TYPE_FY);
      }else{
         return(0);
      }
   }

   public static String getGlReversalMethodDescription(char cReversalMethodCode){
      switch(cReversalMethodCode){
         case Constants.GL_REVERSAL_METHOD_S:
	    return(Constants.GL_REVERSAL_METHOD_SWITCHDRCR);
	 case Constants.GL_REVERSAL_METHOD_C:
	    return(Constants.GL_REVERSAL_METHOD_CHANGESIGN);
	 case Constants.GL_REVERSAL_METHOD_U:
	    return(Constants.GL_REVERSAL_METHOD_USE_DEFAULT);
	 default:
	    return(null);
      }
   }

   public static char getGlReversalMethodCode(String strReversalMethod){
      if(strReversalMethod.equals(Constants.GL_REVERSAL_METHOD_SWITCHDRCR)){
         return(Constants.GL_REVERSAL_METHOD_S);
      }else if(strReversalMethod.equals(Constants.GL_REVERSAL_METHOD_CHANGESIGN)){
         return(Constants.GL_REVERSAL_METHOD_C);
      }else{
         return(Constants.GL_REVERSAL_METHOD_U);
      }
   }

   public static String getGlPeriodStatusDescription(char cPeriodStatus){
      switch(cPeriodStatus){
         case Constants.GL_PERIOD_STATUS_N:
	    return(Constants.GL_PERIOD_STATUS_NEVER_OPENED);
	 case Constants.GL_PERIOD_STATUS_O:
	    return(Constants.GL_PERIOD_STATUS_OPEN);
	 case Constants.GL_PERIOD_STATUS_F:
	    return(Constants.GL_PERIOD_STATUS_FUTURE_ENTRY);
	 case Constants.GL_PERIOD_STATUS_C:
	    return(Constants.GL_PERIOD_STATUS_CLOSED);
	 case Constants.GL_PERIOD_STATUS_P:
	    return(Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED);
	 default:
	    return(null);
      }
   }

   public static char getGlPeriodStatusCode(String strPeriodStatus){
      if(strPeriodStatus.equals(Constants.GL_PERIOD_STATUS_NEVER_OPENED)){
         return(Constants.GL_PERIOD_STATUS_N);
      }else if(strPeriodStatus.equals(Constants.GL_PERIOD_STATUS_OPEN)){
         return(Constants.GL_PERIOD_STATUS_O);
      }else if(strPeriodStatus.equals(Constants.GL_PERIOD_STATUS_CLOSED)){
         return(Constants.GL_PERIOD_STATUS_C);
      }else if(strPeriodStatus.equals(Constants.GL_PERIOD_STATUS_FUTURE_ENTRY)){
         return(Constants.GL_PERIOD_STATUS_F);
      }else{
         return(Constants.GL_PERIOD_STATUS_P);
      }
   }

    public static String getGlEffectiveDateRuleDescription(char cEffectiveDateRuleCode){
       switch(cEffectiveDateRuleCode){
          case Constants.GL_EFFECTIVE_DATE_RULE_F:
             return(Constants.GL_EFFECTIVE_DATE_RULE_FAIL);
          case Constants.GL_EFFECTIVE_DATE_RULE_L:
             return(Constants.GL_EFFECTIVE_DATE_RULE_LEAVE_ALONE);
	  case Constants.GL_EFFECTIVE_DATE_RULE_R:
	     return(Constants.GL_EFFECTIVE_DATE_RULE_ROLL_DATE);
          default:
             return(null);
       }
    }

    public static char getGlEffectiveDateRuleCode(String strEffectiveDateRule){
       if(strEffectiveDateRule.equals(Constants.GL_EFFECTIVE_DATE_RULE_FAIL)){
          return(Constants.GL_EFFECTIVE_DATE_RULE_F);
       }else if(strEffectiveDateRule.equals(Constants.GL_EFFECTIVE_DATE_RULE_LEAVE_ALONE)){
          return(Constants.GL_EFFECTIVE_DATE_RULE_L);
       }else{
          return(Constants.GL_EFFECTIVE_DATE_RULE_R);
       }
    }

   public static String getAdNumberingTypeDescription(char cNumberingTypeCode){
      switch(cNumberingTypeCode){
         case Constants.AD_NUMBERING_TYPE_A:
	    return(Constants.AD_NUMBERING_TYPE_AUTO);
	 case Constants.AD_NUMBERING_TYPE_M:
	    return(Constants.AD_NUMBERING_TYPE_MANUAL);
	 default:
	    return(null);
      }
   }

   public static char getAdNumberingTypeCode(String strNumberingType){
      if(strNumberingType.equals(Constants.AD_NUMBERING_TYPE_AUTO)){
         return(Constants.AD_NUMBERING_TYPE_A);
      }else{
         return(Constants.AD_NUMBERING_TYPE_M);
      }
   }

   public static boolean validateNumberFormat(String strNum){
      NumberFormat nf = NumberFormat.getInstance();
      if(strNum != null && strNum.length() >= 1){
         try{
	    nf.parse(strNum);
	    return(true);
	 }catch(ParseException e){
	    return(false);
	 }
      }else{
         return(true);
      }
   }


   public static boolean validateDecimalExist(String strNum){
   		if(strNum != null && strNum.length() >= 1){
   			try {
   				int index = strNum.indexOf(".");

   				if (index == -1) {
   					return true;

   				} else {

   					boolean hasDecimal = false;

   					for(int i=index + 1; i<strNum.length(); i++){
   						if(!strNum.substring(i, i + 1).equals("0")) {
   							hasDecimal = true;

   							break;
   						}
   					}

   					if(hasDecimal) {
   						return(false);

   					} else {
   						return(true);
   					}
   				}

   			}catch(Exception e){
   				return(true);
   			}
   		}else{
   			return(true);
   		}
   }


   public static boolean validateMoneyFormat(String strMoney){
   	DecimalFormat dcf = new DecimalFormat(Constants.MONEY_FORMAT);
   	DecimalFormat dcf2 = new DecimalFormat(Constants.MONEY_FORMAT_NO_GROUPING);
   	Number num = null;

   	int cnt = 0;

   	//check if number contains invalid characters
   	for(int i=0; i<strMoney.length(); i++) {

   		if(i == 0 && strMoney.charAt(i) == '-')
   			continue;

   		if(strMoney.charAt(i) == ',')
   			continue;

   		if(strMoney.charAt(i) == '.') {
   			cnt++;
   			continue;
   		}

   		if(!Character.isDigit(strMoney.charAt(i))) {
                    System.out.println("char:" + strMoney.charAt(i));
   			return false;
   		}


   	}

   	//check if number contains more than 1 decimal points
   	if(cnt > 1) {
   		return false;
   	}

   	dcf.setParseIntegerOnly(true);
   	if(strMoney != null && strMoney.length() >= 1){
   		try{
   			num = dcf.parse(strMoney);
   		}catch(ParseException e){
   			return(false);
   		}

   		long lNum = num.longValue();
   		String newStrMoney = dcf2.format(lNum);
   		if(newStrMoney.length() > Constants.MAXIMUM_MONEY_INTEGER){
   			return(false);
   		}else{
   			return(true);
   		}
   	}else{
   		return(true);
   	}
   }

   public static boolean validateTaxCsvMoneyDecimalFormat(String strMoney){
	   	if(strMoney != null && strMoney.length() >= 1){
	   		try {
		   		String str1 = strMoney.substring(strMoney.indexOf('.'), strMoney.length());
			   	String str2 = str1.substring(1, str1.length());
			   	if(str2.length() == 2) {
			   		return(false);
			   	}else{
			   		return(true);
			   	}
	   		}catch(Exception e){
	   			return(true);
	   		}
	   	}else{
	   		return(true);
	   	}
	}

	public static boolean validateTaxCsvMoneyFormat(String strMoney){
	   	DecimalFormat dcf = new DecimalFormat(Constants.TAX_CSV_MONEY_FORMAT);
	   	DecimalFormat dcf2 = new DecimalFormat(Constants.TAX_CSV_MONEY_FORMAT_NO_GROUPING);
	   	Number num = null;

	   	dcf.setParseIntegerOnly(true);
	   	if(strMoney != null && strMoney.length() >= 1){
	   		try{
	   			num = dcf.parse(strMoney);
	   		}catch(ParseException e){
	   			return(false);
	   		}
	   		long lNum = num.longValue();
	   		String newStrMoney = dcf2.format(lNum);
	   		if(newStrMoney.length() > Constants.TAX_CSV_MAXIMUM_MONEY_INTEGER){
	   			return(false);
	   		}else{
	   			return(true);
	   		}
	   	}else{
	   		return(true);
	   	}
	}


   public static boolean validateMoneyRateFormat(String strMoney){
      DecimalFormat dcf = new DecimalFormat(Constants.MONEY_RATE_FORMAT);
      DecimalFormat dcf2 = new DecimalFormat(Constants.MONEY_RATE_FORMAT_NO_GROUPING);
      Number num = null;

      dcf.setParseIntegerOnly(true);
      if(strMoney != null && strMoney.length() >= 1){
         try{
            num = dcf.parse(strMoney);
         }catch(ParseException e){
            return(false);
         }
         long lNum = num.longValue();
         String newStrMoney = dcf2.format(lNum);
         if(newStrMoney.length() > Constants.MAXIMUM_MONEY_RATE_INTEGER){
            return(false);  													       }else{  															        return(true);
	 }
      }else{
         return(true);
      }
   }

   public static boolean validateStringNumLessThanEqual(String strLong, long lNumber){
      if(strLong != null && strLong.length() >= 1){
         try{
	    if(new Long(strLong).longValue() > lNumber){
	       return(false);
	    }
	    return(true);
	 }catch(NumberFormatException e){
	    return(true);
	 }
      }else{
         return(true);
      }
   }

   public static boolean validateStringNumGreaterThanEqual(String strLong, long lNumber){
      if(strLong != null && strLong.length() >= 1){
         try{
            if(new Long(strLong).longValue() < lNumber){
               return(false);
            }
            return(true);
         }catch(NumberFormatException e){
            return(true);
         }
      }else{
         return(true);
      }
   }

   public static boolean validateNumGreaterThan(String strLong, double lNumber){
	      if(strLong != null && strLong.length() >= 1){
	          try{
	             if(new Double(strLong).doubleValue() <= lNumber){
	                return(false);
	             }
	             return(true);
	          }catch(NumberFormatException e){
	             return(true);
	          }
	       }else{
	          return(true);
	       }
	   }

   public static boolean validateStringExists(ArrayList al, String str){
      if(str != null && str.length() >= 1){
         Iterator i = al.iterator();
         while(i.hasNext()){
            if(str.trim().equals((String)i.next())){
            	return(true);
	        }
         }
         return(false);
       }else{
          return(true);
       }
   }

   public static String getDayOfWeek(int dayOfWeekNum){
      switch(dayOfWeekNum){
         case 1:
	    return("SUNDAY");
	 case 2:
	    return("MONDAY");
	 case 3:
	    return("TUESDAY");
	 case 4:
	    return("WEDNESDAY");
	 case 5:
	    return("THURSDAY");
	 case 6:
	    return("FRIDAY");
	 case 7:
	    return("SATURDAY");
	 default:
	    return(null);
      }
   }

   public static ArrayList getAdUserAll(DataSource dataSource, Integer cmpCode)
      throws SQLException, Exception{

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      ArrayList userNameList = new ArrayList();

      try{
         conn = dataSource.getConnection();
	 stmt = conn.prepareStatement("SELECT USR_NM FROM AD_USR WHERE USR_AD_CMPNY = ?");
	 stmt.setInt(1, cmpCode.intValue());

	 rs = stmt.executeQuery();

	 while(rs.next()){
	       userNameList.add(rs.getString(1));
	 }

	 return(userNameList);

      }catch(SQLException sqle){
         throw sqle;
      }catch(Exception e){
         throw e;
      }finally{
          try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
          rs = null;
          try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
          stmt = null;
          try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
          conn = null;
      }
   }

   public static ArrayList getAdResAll(DataSource dataSource, Integer cmpCode)
      throws SQLException, Exception{

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      ArrayList resNameList = new ArrayList();

      try{
         conn = dataSource.getConnection();
         stmt = conn.prepareStatement("SELECT RS_NM FROM AD_RSPNSBLTY WHERE RS_AD_CMPNY = ?");

         stmt.setInt(1, cmpCode.intValue());

         rs = stmt.executeQuery();
         while(rs.next()){
            resNameList.add(rs.getString(1));
         }
         return(resNameList);
      }catch(SQLException sqle){
         throw sqle;
      }catch(Exception e){
         throw e;
      }finally{
         try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
         rs = null;
         try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
         stmt = null;
         try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
         conn = null;
      }
   }

   public static String getAdUserByUserCode(DataSource dataSource, Integer userCode, Integer cmpCode)
      throws SQLException, Exception{

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;

      if(userCode != null){
         try{
            conn = dataSource.getConnection();
	    stmt = conn.prepareStatement("SELECT USR_NM FROM AD_USR WHERE USR_CODE = ? AND USR_AD_CMPNY = ?");

	    stmt.setInt(1, userCode.intValue());
	    stmt.setInt(2, cmpCode.intValue());

	    rs = stmt.executeQuery();

	    while(rs.next()){
	       return(rs.getString(1));
	    }
	    throw new SQLException(Constants.GLOBAL_NO_RECORD_FOUND);
         }catch(SQLException sqle){
            throw sqle;
         }catch(Exception e){
            throw e;
         }finally{
            try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
	    rs = null;
	    try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
	    stmt = null;
  	    try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
	    conn = null;
         }
      }else{
         return(null);
      }
   }

   public static String getAdResByResCode(DataSource dataSource, Integer resCode, Integer cmpCode)
      throws SQLException, Exception{

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;

      if(resCode != null){
         try{
            conn = dataSource.getConnection();
            stmt = conn.prepareStatement("SELECT RS_NM FROM AD_RSPNSBLTY WHERE RS_CODE = ? AND RS_AD_CMPNY = ?");
            stmt.setInt(1, resCode.intValue());
            stmt.setInt(2, cmpCode.intValue());

            rs = stmt.executeQuery();
            while(rs.next()){
               return(rs.getString(1));
            }
            throw new SQLException(Constants.GLOBAL_NO_RECORD_FOUND);
          }catch(SQLException sqle){
            throw sqle;
          }catch(Exception e){
            throw e;
          }finally{
            try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
            rs = null;
            try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
            stmt = null;
            try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
	    conn = null;
	  }
       }else{
	  return(null);
       }
   }

   public static Integer getAdUserCodeByUserName(DataSource dataSource, String userName, Integer cmpCode)
      throws SQLException, Exception {

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      if(userName != null && userName.length() >= 1){
         try{
            conn = dataSource.getConnection();
            stmt = conn.prepareStatement("SELECT USR_CODE FROM AD_USR WHERE USR_NM = ? AND USR_CODE = ?");

            stmt.setString(1, userName.trim());
            stmt.setInt(2, cmpCode.intValue());

            rs = stmt.executeQuery();

            while(rs.next()){
  	       return(new Integer(rs.getInt(1)));
	    }

	    throw new SQLException(Constants.GLOBAL_NO_RECORD_FOUND);
         }catch(SQLException sqle){
            throw sqle;
         }catch(Exception e){
            throw e;
         }finally{
            try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
            rs = null;
            try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
            stmt = null;
            try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
            conn = null;
         }
      }else{
         return(null);
      }
   }

   public static Integer getAdResCodeByResName(DataSource dataSource, String resName, Integer cmpCode)
      throws SQLException, Exception {

      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      if(resName != null && resName.length() >= 1){
         try{
            conn = dataSource.getConnection();
            stmt = conn.prepareStatement("SELECT RS_CODE FROM AD_RSPNSBLTY WHERE RS_NM = ? AND RS_AD_CMPNY = ?");
            stmt.setString(1, resName.trim());
            stmt.setInt(2, cmpCode.intValue());
            rs = stmt.executeQuery();

            while(rs.next()){
	       return(new Integer(rs.getInt(1)));
	    }
            throw new SQLException(Constants.GLOBAL_NO_RECORD_FOUND);
          }catch(SQLException sqle){
             throw sqle;
          }catch(Exception e){
             throw e;
          }finally{
             try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
             rs = null;
             try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
             stmt = null;
             try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
             conn = null;
          }
       }else{
          return(null);
       }
   }

   public static GregorianCalendar getGcCurrentDateWoTime(){
       GregorianCalendar gcCurrDate = new GregorianCalendar();
       gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
       gcCurrDate.set(Calendar.MILLISECOND, 0);
       return(gcCurrDate);
   }

   public static String convertDoubleToStringMoney(double number, short precisionUnit){
      String decimalFormat = new String(Constants.MONEY_FORMAT);
      for(int i=0; i<precisionUnit; i++){
         decimalFormat = decimalFormat + "0";
      }
      DecimalFormat dcf = new DecimalFormat(decimalFormat);
      return dcf.format(number);
   }

   public static String convertDoubleToStringTaxCsvMoney(double number){
    String decimalFormat = new String(Constants.TAX_CSV_MONEY_FORMAT_DEC);
    DecimalFormat dcf = new DecimalFormat(decimalFormat);
    return dcf.format(number);
   }

   public static double convertStringMoneyToDouble(String sNumber, short precisionUnit){
      String decimalFormat = new String(Constants.MONEY_FORMAT);
      for(int i=0; i<precisionUnit; i++){
         decimalFormat = decimalFormat + "0";
      }
      DecimalFormat dcf = new DecimalFormat(decimalFormat);
      try{
         return((dcf.parse(sNumber)).doubleValue());
      }catch(Exception e){
         return(0);
      }
   }

   public static String convertFloatToStringMoneyRate(float number, short precisionUnit){
      String decimalFormat = new String(Constants.MONEY_RATE_FORMAT);
      for(int i=0; i<precisionUnit; i++){
         decimalFormat = decimalFormat + "0";
      }
      DecimalFormat dcf = new DecimalFormat(decimalFormat);
      return dcf.format(number);
   }

   public static float convertStringMoneyRateToFloat(String sNumber, short precisionUnit){
      String decimalFormat = new String(Constants.MONEY_RATE_FORMAT);
      for(int i=0; i<precisionUnit; i++){
         decimalFormat = decimalFormat + "0";
      }
      DecimalFormat dcf = new DecimalFormat(decimalFormat);
      try{
         return((dcf.parse(sNumber)).floatValue());
      }catch(ParseException e){
         return(0);
      }
   }

   public static boolean convertByteToBoolean(byte byteVal){
      if(byteVal == 1)
         return(true);

      return(false);
   }

   public static byte convertBooleanToByte(boolean booleanVal){
      if(booleanVal == true)
         return((byte)1);

      return((byte)0);
   }

   public static String convertStringToUpperCase(String strVal) {

   	  if(strVal == null || strVal.length() < 1) {

   	  	  return null;

   	  } else {

   	      return strVal.trim().toUpperCase();

   	  }

   }

   public static String getGlFundStatusDescription(char cFundStatusCode){
      switch(cFundStatusCode){
         case Constants.GL_FUND_STATUS_R:
	    return(Constants.GL_FUND_STATUS_RESERVED);
	 case Constants.GL_FUND_STATUS_U:
	    return(Constants.GL_FUND_STATUS_UNRESERVED);
	 case Constants.GL_FUND_STATUS_N:
	    return(Constants.GL_FUND_STATUS_NOT_APPLICABLE);
	 default:
	    return(null);
      }
   }

   public static double roundIt(double amount, short precision) {

      return Math.round(amount * Math.pow(10, (double)precision)) / Math.pow(10, (double)precision);

   }

   public static boolean validateTaxDateFormat(String strDate){
     if(strDate != null  && strDate.length() >= 1){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
	sdf.setLenient(false);
        try{
	   Date dt = sdf.parse(strDate.trim());
           return(true);
	}catch(ParseException e){
	   return(false);
	}
     }else{
        return(true);
     }
   }

   public static boolean validateTimeFormat(String strTime) {

		if (strTime != null && strTime.length() >= 1) {
			SimpleDateFormat stf = new SimpleDateFormat(
					Constants.TIME_FORMAT_INPUT);
			stf.setLenient(false);
			try {
				Date t = stf.parse(strTime.trim());
				return (true);
			} catch (ParseException e) {
				return (false);
			}
		} else {
			return (true);
		}
	}
	public static boolean validateJournalIntefaceUploadMoneyFormat(
			String strMoney) {
		DecimalFormat dcf = new DecimalFormat(Constants.MONEY_FORMAT);
		DecimalFormat dcf2 = new DecimalFormat(
				Constants.MONEY_FORMAT_NO_GROUPING);
		Number num = null;
		dcf.setParseIntegerOnly(true);
		if (strMoney != null && strMoney.length() >= 1) {
			try {
				new Double(strMoney);
				num = dcf.parse(strMoney);
			} catch (ParseException e) {
				return (false);
			} catch (NumberFormatException ex) {
				return (false);
			}
			long lNum = num.longValue();
			String newStrMoney = dcf2.format(lNum);
			if (newStrMoney.length() > Constants.MAXIMUM_MONEY_INTEGER) {
				return (false);
			} else {
				return (true);
			}
		} else {
			return (true);
		}
	}

    public static String storedProcedureAlignINvalue(ArrayList dataList){
        String valueReturn = "";

        for (int counter = 0; counter < dataList.size(); counter++) {

            String data = dataList.get(counter).toString();

            if(!valueReturn.equals("")){
                valueReturn += ", ";
            }
            valueReturn +=   data  ;

        }
        return valueReturn;
    }

    public static String getDateFormatInput(){
        if(DATE_FORMAT_INPUT==null){
            loadDateFormat();
        }

        return DATE_FORMAT_INPUT;
    }

    public static String getDateFormatOutput(){
        if(DATE_FORMAT_OUTPUT==null){
            loadDateFormat();
        }

        return DATE_FORMAT_OUTPUT;
    }



    public static void loadDateFormat(){
        String dateFormatPath = Constants.OFS_RESOURCES_PATH + "DEFAULT_DATE_FORMAT.dat";
        try{
            if (!new java.io.File(dateFormatPath).exists()) {
                File file = new File(dateFormatPath);
                FileOutputStream is = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(is);
                Writer w = new BufferedWriter(osw);
                w.write("dd/MM/yyyy"); //INPUT
                w.write(System.getProperty("line.separator"));
                w.write("dd/MM/yyyy"); //OUTPUT
                w.close();

                DATE_FORMAT_INPUT = "dd/MM/yyyy";
                DATE_FORMAT_OUTPUT = "dd/MM/yyyy";
            }else{
                System.out.println("read date format");
                FileReader fileReader = new FileReader(dateFormatPath);

                BufferedReader bufferedReader = new BufferedReader(fileReader);



                DATE_FORMAT_INPUT = bufferedReader.readLine().toString();
                System.out.println(DATE_FORMAT_INPUT);
                DATE_FORMAT_OUTPUT = bufferedReader.readLine().toString();
                System.out.println(DATE_FORMAT_OUTPUT);

                System.out.println("read date format end");
                bufferedReader.close();
                fileReader.close();
            }
        }catch(Exception ex){
            System.out.println("Error reading Date Format: use dd/MM/yyyy instead");
            System.out.println(ex.toString());
            DATE_FORMAT_INPUT = "dd/MM/yyyy";
            DATE_FORMAT_OUTPUT = "dd/MM/yyyy";

        }

    }

    public static void setDateFormat(String NEW_DATE_FORMAT_INPUT, String NEW_DATE_FORMAT_OUTPUT){
        String dateFormatPath = Constants.OFS_RESOURCES_PATH + "DEFAULT_DATE_FORMAT.dat";
        try{
         //   if (!new java.io.File(dateFormatPath).exists()) {
                File file = new File(dateFormatPath);
                FileOutputStream is = new FileOutputStream(file,false);
                OutputStreamWriter osw = new OutputStreamWriter(is);
                Writer w = new BufferedWriter(osw);
                w.write(NEW_DATE_FORMAT_INPUT); //INPUT
                w.write(System.getProperty("line.separator"));
                w.write(NEW_DATE_FORMAT_OUTPUT); //OUTPUT
                w.close();
        //    }
        }catch(Exception ex){
            System.out.println("Error updating Date Format:");


        }finally{
            DATE_FORMAT_INPUT = NEW_DATE_FORMAT_INPUT;
            DATE_FORMAT_OUTPUT = NEW_DATE_FORMAT_OUTPUT;

        }
    }


    public static String getLookUpJasperFile(String fileNameList, String lookUpName, String companyName) {

    	String jasperFileName = "";
    	String textListFilePath = "/opt/ofs-resources/" + companyName + "/" + fileNameList + ".txt";

    	if (!new java.io.File(textListFilePath).exists()) {




    		System.out.println("text list not found");
        	return jasperFileName;

        }else {

        	try {
        		System.out.println("read jasperreport list custom format");
                FileReader fileReader = new FileReader(textListFilePath);

                BufferedReader bufferedReader = new BufferedReader(fileReader);

                String line;
                while ((line = bufferedReader.readLine()) != null) {

                	if(line.contains("=")) {
                		String reportType = line.split("=")[0];
                		String fileName = line.split("=")[1];

                		if(reportType.equals(lookUpName)) {

                			System.out.println("found " + lookUpName);
                			return fileName;
                		}

                	}
                }
                System.out.println(" not found " + lookUpName);
                return jasperFileName;

        	}catch(Exception ex) {
        		System.out.println("Error Reading text file: " + fileNameList );
        		return "";
        	}



        }


    }

    public static ArrayList convertStringToReportParameters(String reportParameterString,ArrayList rpListValid) {
    	ArrayList list = new ArrayList();


    	try {

    		String[] rpList = new String[0];
    		if(reportParameterString!=null && !reportParameterString.equals("")) {
    			rpList = reportParameterString.split("\\$");
    		}

    		if(rpListValid.size()>0) {

    			for(int x=0;x<rpListValid.size();x++) {
    				System.out.println("1");
    				String rpNameValid = rpListValid.get(x).toString();
    				ReportParameter reportParameter = new ReportParameter();
    				reportParameter.setParameterName(rpNameValid);


    				System.out.println("size rpList is: " + rpList.length);
    				for(String rpString: rpList) {
    					System.out.println("2 : " + rpString);


    					String[] rpTkn = rpString.split("\\|",2);

    					String rpName = rpTkn[0];
    	    			String rpValue = rpTkn[1];

    	    			System.out.println("3");
    	    			System.out.println("line is: " + rpName + " = " + rpNameValid);
    					if(rpName.equals(rpNameValid)) {
    						reportParameter.setParameterValue(rpValue);
    						break;

    					}
    				}
    				list.add(reportParameter);


    			}


    		}else {
    			System.out.println(rpList.length + "  length");

        		for(int x=0;x<rpList.length;x++) {

        			System.out.println("line is: " + rpList[x]);

        			String rpName = rpList[x].split("\\|")[0];
        			String rpValue = rpList[x].split("\\|")[1];

        			ReportParameter reportParameter = new ReportParameter(rpName,rpValue);

        			list.add(reportParameter);




        		}


    		}


    		return list;


    	}catch(Exception ex) {
    		System.out.println("Error convertStringToReportParameters in : " + reportParameterString);
    		System.out.println(ex.toString());
    		return new ArrayList();
    	}


    }


    public static String convertReportParametersToString(ArrayList list) {

    	String reportParameterString = "";
    	try {

    		for(int x=0;x<list.size();x++) {
    			ReportParameter reportParameter = (ReportParameter)list.get(x);

    			if(reportParameter.getParameterValue().equals("")) {
    				continue;
    			}
    			if(reportParameterString.length()>0) {

    				reportParameterString += "$";
    			}
    			reportParameterString += reportParameter.getParameterName() + "|" + reportParameter.getParameterValue();

    		}

    		return reportParameterString;
    	}catch(Exception ex) {
    		System.out.println("Error convertReportParametersToString in : " + reportParameterString);
    		return "";
    	}


    }




    public static StringBuilder convertStatusDataInHtml(ArrayList data){

        StringBuilder strB = new StringBuilder();
        String[] columnHeaders = data.get(1).toString().split(",");

        strB.append("<b>" + data.get(0).toString() + "</b>");
        strB.append(System.getProperty("line.separator"));
        strB.append("<table border='1' bordercolor='black' style=' style=\"width: 100%;\"'>");
        strB.append("<tr>");
        //header
        for(int x=0;x<columnHeaders.length;x++){

            strB.append("<th>");
            strB.append(columnHeaders[x]);
            strB.append("</th>");

        }
        strB.append("</tr>");
        strB.append(System.getProperty("line.separator"));

        //values
        for(int x=2;x<data.size();x++){
            strB.append("<tr>");
            String[] dataValues = data.get(x).toString().split(",");
            for(int y=0;y<dataValues.length;y++){
                strB.append("<td>");
                strB.append(dataValues[y]);
                strB.append("</td>");
            }
            strB.append("</tr>");
            strB.append(System.getProperty("line.separator"));
        }

        strB.append("</table>");

        return strB;

    }

    
    
    
    
    
    
    
    
    
    
    

    public static ArrayList convertMiscToInvModTagListDetailsList(String misc, String transactionDate, String type) {



    	ArrayList list = new ArrayList();

    	if(misc == null) {
    		return list;
    	}

    	if(misc.equals("")) {
    		return list;
    	}
    	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;

    	System.out.println("misc is: " + misc);
		  String[] arrayMisc = misc.split("_");
				double qty= Common.convertStringToDouble(arrayMisc[0]);
				System.out.println("common conv misctoInv qty " + qty);
				misc = arrayMisc[1];
	      	   	arrayMisc = misc.split("@");
	      	   	propertyCode = arrayMisc[0].split(",");

	      	   	misc = arrayMisc[1];
	      	   	arrayMisc = misc.split("<");
	      	   	serialNumber = arrayMisc[0].split(",");

	      	   	misc = arrayMisc[1];
	      	   	arrayMisc = misc.split(">");
	      	   	specs = arrayMisc[0].split(",");

	      	   	misc = arrayMisc[1];
	      	   	arrayMisc = misc.split(";");
	      	   	custodian = arrayMisc[0].split(",");
	      	   	//expiryDate = arrayMisc[1].split(",");

	      	   	misc = arrayMisc[1];
	      	   	arrayMisc = misc.split("%");
	      	   	expiryDate = arrayMisc[0].split(",");
	      	   	tgDocumentNumber = arrayMisc[1].split(",");
	      	   	//if(apRILList.getTagListSize()>0){
        	   for (int y = 0; y<qty; y++) {

        		   System.out.println("pass:" + y);
        		   //ApReceivingItemEntryTagList apRiLTgList =apRILList.getTagListByIndex(y);
        		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
        		   InvModTagListDetails tgDetails = new InvModTagListDetails();

        		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
	     				   &&serialNumber[y].equals(" ")){
	        			   System.out.println("break");
	        			   break;
        		   }

        		   System.out.println("propertyCode:" + propertyCode[y].trim());
        		   System.out.println("specs:" + specs[y].trim());
        		   System.out.println("serialNumber:" + serialNumber[y].trim());
        		   System.out.println("custodian:" + custodian[y].trim());
        		   System.out.println("expiryDate:"+ expiryDate[y].trim());
        		   if(propertyCode[y].trim().equals("") &&
    				   specs[y].trim().equals("") &&
    				   serialNumber[y].trim().equals("") &&
    				   custodian[y].trim().equals("") &&
    				   expiryDate[y].trim().equals("") ) {
        			   continue;
        		   }


        		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
        		   tgDetails.setTgSpecs(specs[y].trim());
        		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
        		   //tgDetails.setTgDocumentNumber(tgDocumentNumber[y].trim());
        		   tgDetails.setTgCustodian(custodian[y].trim());
        		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
        		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(transactionDate));
        		   tgDetails.setTgType(type);

        		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
        		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
        		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
        		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
        		   list.add(tgDetails);

        	   }



    	return list;
    }
    
    
    
    public static double currencyConversion(double amount, double rate, short precisionUnit){
        
        double convertedAmount  = roundIt(amount * rate, precisionUnit);
        
   
        return convertedAmount;
    
    }
    
    
    


    public static String convertInvModTagListDetailsListToMisc(ArrayList list, String defaultQuantity) {



    	String misc = defaultQuantity + "_";
    	String propertyCode = "";
    	String specs = "";
    	String serialNumber = "";
    	String custodian = "";
    	String expiryDate = "";
    	String tgDocumentNumber = "";

    	if (list.size() == 0 && list.isEmpty()) {
    		defaultQuantity = "20";
			System.out.println("if?");
			for (int x=0; x<=Common.convertStringToDouble(defaultQuantity); x++) {
    			/*
				ApPurchaseOrderEntryTagList apTagList = new ApPurchaseOrderEntryTagList(apPlList, "", "", "", "", "");
    			apPlList.saveTagList(apTagList);*/
				/*InvModTagListDetails tgDetails = new InvModTagListDetails();

       		   	propertyCode = tgDetails.getTgPropertyCode();
       		   	specs = tgDetails.getTgSpecs();
       		   	serialNumber = tgDetails.getTgSerialNumber();
       		   	custodian = tgDetails.getTgCustodian();
       		   	expiryDate = Common.convertSQLDateToString(tgDetails.getTgExpiryDate());
       		   	tgDocumentNumber = tgDetails.getTgDocumentNumber();*/
       		   	if (expiryDate == null){
       		   		expiryDate = "";
       		   	}
       		   	propertyCode += " ,";
       		    specs += " ,";
       		    serialNumber += " ,";
       		    custodian += " ,";
       		    expiryDate += " ,";
       		    tgDocumentNumber += " ,";
			}
			misc = 	defaultQuantity+"_"+
					propertyCode.substring(0,propertyCode.length()-1)+"@"+
			   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
			   		specs.substring(0,specs.length()-1)+">"+
			   		custodian.substring(0,custodian.length()-1)+";"+
			   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
			   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1);
		}else{
			System.out.println("else? list:" + list.size() );
			Double qty = Double.parseDouble(defaultQuantity);
			for(int x=0;x<qty;x++) {

				if(x<list.size()) {
					System.out.println("enterd in");
					InvModTagListDetails tgDetails = (InvModTagListDetails)list.get(x);

					if (tgDetails.getTgPropertyCode().equals("") || tgDetails.getTgPropertyCode()==null){
						propertyCode += " ,";
					}else{
						propertyCode += tgDetails.getTgPropertyCode() + ",";
					}
					if (tgDetails.getTgSerialNumber().equals("") || tgDetails.getTgPropertyCode()==null){
						serialNumber += " ,";
					}else{
						serialNumber += tgDetails.getTgSerialNumber() + ",";
					}
					if (tgDetails.getTgSpecs().equals("") || tgDetails.getTgPropertyCode()==null){
						specs += " ,";
					}else{
						specs += tgDetails.getTgSpecs() + ",";
					}

	       		   	custodian += (tgDetails.getTgCustodian() == "") ? " ," : tgDetails.getTgCustodian() + ",";
	       		 	expiryDate += (tgDetails.getTgExpiryDate() == null) ? " ," : Common.convertSQLDateToString(tgDetails.getTgExpiryDate()) + ",";
	       		 	if (tgDetails.getTgDocumentNumber()==null){
						tgDocumentNumber += " ,";
					}else{
						tgDocumentNumber += tgDetails.getTgDocumentNumber() + ",";
					}


				}else {
					if (expiryDate == null){
	       		   		expiryDate = "";
	       		   	}
	       		   	propertyCode += " ,";
	       		    specs += " ,";
	       		    serialNumber += " ,";
	       		    custodian += " ,";
	       		    expiryDate += " ,";
	       		    tgDocumentNumber += " ,";
				}




			}

			System.out.println(" qty : " + String.valueOf(defaultQuantity));
			System.out.println(" propertyCode : " + propertyCode);
			System.out.println(" serialNumber : " + serialNumber);
			System.out.println(" specs : " + specs);
			System.out.println(" custodian : " + custodian);
			System.out.println(" expiryDate : " + expiryDate);

			System.out.println("raw qty : " + String.valueOf(defaultQuantity));
			System.out.println("raw propertyCode : " + propertyCode.substring(0,propertyCode.length()-1));
			System.out.println("raw serialNumber : " + serialNumber.substring(0,serialNumber.length()-1));
			System.out.println("raw specs : " + specs.substring(0,specs.length()-1));
			System.out.println("raw custodian : " + custodian.substring(0,custodian.length()-1));
			System.out.println("raw expiryDate : " + expiryDate.substring(0,expiryDate.length()-1));

			misc = 	defaultQuantity+"_"+
					propertyCode.substring(0,propertyCode.length()-1)+"@"+
			   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
			   		specs.substring(0,specs.length()-1)+">"+
			   		custodian.substring(0,custodian.length()-1)+";"+
			   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
			   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1);
			/*
			Iterator y = list.iterator();
			while (y.hasNext()){


				InvModTagListDetails tgDetails = (InvModTagListDetails)y.next();

				if (tgDetails.getTgPropertyCode().equals("") || tgDetails.getTgPropertyCode()==null){
					propertyCode += " ,";
				}else{
					propertyCode += tgDetails.getTgPropertyCode() + ",";
				}
				if (tgDetails.getTgSerialNumber().equals("") || tgDetails.getTgPropertyCode()==null){
					serialNumber += " ,";
				}else{
					serialNumber += tgDetails.getTgSerialNumber() + ",";
				}
				if (tgDetails.getTgSpecs().equals("") || tgDetails.getTgPropertyCode()==null){
					specs += " ,";
				}else{
					specs += tgDetails.getTgSpecs() + ",";
				}

       		   	custodian += (tgDetails.getTgCustodian() == "") ? " ," : tgDetails.getTgCustodian() + ",";
       		 	expiryDate += (tgDetails.getTgExpiryDate() == null) ? " ," : Common.convertSQLDateToString(tgDetails.getTgExpiryDate()) + ",";
       		 	if (tgDetails.getTgDocumentNumber()==null){
					tgDocumentNumber += " ,";
				}else{
					tgDocumentNumber += tgDetails.getTgDocumentNumber() + ",";
				}

			}
			misc = 	defaultQuantity+"_"+
					propertyCode.substring(0,propertyCode.length()-1)+"@"+
			   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
			   		specs.substring(0,specs.length()-1)+">"+
			   		custodian.substring(0,custodian.length()-1)+";"+
			   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
			   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1);

			   		*/
		}




    	return misc;
    }
    
    
    public static String incrementStringNumber(String stringNumber) {
            
        String strNumberExtract = stringNumber.replaceFirst(".*\\D(\\d+)\\D*$", "$1");
                
        long num;
        
        try {
            
            num = Long.parseLong(strNumberExtract);
            
        } catch (NumberFormatException ex) {
            
            return strNumberExtract + "1";
            
        }
        
        String strIncrementedNumberExtract = Long.toString(++num);
    
        int diffLen = strNumberExtract.length() - strIncrementedNumberExtract.length(); 
    
        String zeroPadding = "";
    
        if (diffLen > 0) for (int i = 0; i < diffLen; i++) zeroPadding = zeroPadding + "0";
    
        return stringNumber.replaceFirst("\\d+(\\D*)$", zeroPadding + strIncrementedNumberExtract + "$1");
            
   }


}
