package com.struts.util;

public final class Constants{


	// Session Context Keys

	public static final String COMPANY_CODE = "DALTRON";
	public static final String OFS_RESOURCES_PATH = "/opt/ofs-resources/";

	public static final String USER_KEY = "user";
	public static final String CONSOLE_USER_KEY = "consoleuser";
	public static final String SESSIONS_KEY = "sessions";
	public static final String PDF_REPORT_KEY = "pdfReport";
	public static final String DOWNLOAD_REPORT_KEY = "downloadReport";
	public static final String REPORT_KEY = "report";
    public static final String STATUS_REPORT_KEY = "statusReport";
	public static final String IMAGES_MAP_KEY = "imagesMap";
	public static final String IMAGE_KEY = "image";

	// Global Messages
	public static final String GLOBAL_NO_RECORD_FOUND = "NO RECORD FOUND";
	public static final String GLOBAL_BLANK = "";
	public static final String GLOBAL_ZERO = "0";
	public static final String GLOBAL_DISABLED = "DISABLED";
	public static final String GLOBAL_ENABLED = "ENABLED";
	public static final String GLOBAL_HIDE = "HIDDEN";
	public static final String GLOBAL_VISIBLE = "VISIBLE";
	public static final int GLOBAL_MAX_LINES = 20;
	public static final String GLOBAL_DETAILED = "DETAILED";
	public static final String GLOBAL_SUMMARIZED = "SUMMARIZED";
	public static final String GLOBAL_YES ="YES";
	public static final String GLOBAL_NO = "NO";

	// AdminDB Return Types
	public static final int ADMIN_SUCCESS = 0;
	public static final int ADMIN_INVALID_LOGON = 2;
	public static final int ADMIN_EXPIRED_ACCESS = 3;
	public static final int ADMIN_EXPIRED_DAYS = 4;
	public static final int ADMIN_USER_DISABLED = 5;
	public static final int ADMIN_NO_RESPONSIBILITY = 6;
	public static final int ADMIN_USER_RES_DISABLED = 7;
	public static final int ADMIN_USER_ASSIGNED_RES_DISABLED = 8;

	// AdminDB Dates Conversion Rates
	public static final int MILLIS_PER_DAY = 1000 * 60 * 60 * 24;

	// Generic Formats


	//public static final String DATE_FORMAT_INPUT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_INPUT = "dd/MM/yyyy";
	//public static final String DATE_FORMAT_OUTPUT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_OUTPUT = "dd/MM/yyyy";


	public static final String DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
	public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";


	//Path String for ofs-statements
	public static final String OFS_STATEMENT_PATH = "/opt/ofs-statements/";


	public static final String DATETIME_FORMAT_OUTPUT = "dd/MM/yyyy hh:mm:ss a";

	public static final String TIME_FORMAT_OUTPUT = "hh:mm:ss a";
	public static final String TIME_FORMAT_INPUT = "HH:mm:ss";
	public static final String MONEY_FORMAT = "#,###,###,###,###,###,##0.";
	public static final String MONEY_FORMAT_NO_GROUPING = "##############################";
	public static final int MAXIMUM_MONEY_INTEGER = 30;
	public static final short DEFAULT_PRECISION = 2;
	public static final String MONEY_RATE_FORMAT = "###0.";
    public static final String MONEY_RATE_FORMAT_NO_GROUPING = "####";
    public static final int MAXIMUM_MONEY_RATE_INTEGER = 4;
	public static final short MONEY_RATE_PRECISION = 3;
	public static final short CONVERSION_RATE_PRECISION = 4;


	// Tax CSV Formats
	public static final String TAX_CSV_MONEY_FORMAT = "############0.";
	public static final String TAX_CSV_MONEY_FORMAT_DEC = "############0.00";
	public static final String TAX_CSV_MONEY_FORMAT_NO_GROUPING = "###############";
	public static final int TAX_CSV_MAXIMUM_MONEY_INTEGER = 15;

	// Page States
	public static final String PAGE_STATE_SAVE = "save";
	public static final String PAGE_STATE_EDIT = "edit";
	public static final String PAGE_STATE_ADD = "add";
	public static final String PAGE_STATE_VIEW = "view";
	public static final String PAGE_STATE_COPY = "copy";

	// Transaction Success Status
	public static final String STATUS_SUCCESS = "S";

	//Transaction IDs

	//Admin
	public static final int AD_DOCUMENT_SEQUENCES_ID = 1002;
	public static final int AD_DOCUMENT_SEQUENCE_ASSIGNMENTS_ID = 1003;
	public static final int AD_BANK_ID = 1004;
	public static final int AD_BANK_ACCOUNT_ID = 1005;
	public static final int AD_LOOK_UP_VALUES_ID = 1006;
	public static final int AD_DOCUMENT_CATEGORY_ID = 1007;
	public static final int AD_AGING_BUCKET_ID = 1008;
	public static final int AD_AGING_BUCKET_VALUE_ID = 1009;
    public static final int AD_PAYMENT_TERM_ID = 1010;
    public static final int AD_PAYMENT_SCHEDULE_ID = 1011;
    public static final int AD_DISCOUNT_ID = 1012;
	public static final int AD_PREFERENCE_ID = 1013;
	public static final int AD_USER_ID = 1014;
	public static final int AD_USER_RESPONSIBILITY_ID = 1015;
    public static final int AD_RESPONSIBILITY_ID = 1016;
    public static final int AD_FORM_FUNCTION_RESPONSIBILITY_ID = 1017;
	public static final int GEN_VALUE_SET_VALUE_ID = 1018;
	public static final int AD_CHANGE_PASSWORD_ID = 1019;
	public static final int AD_COMPANY_ID = 1020;
	public static final int AD_APPROVAL_SETUP_ID = 1021;
	public static final int AD_APPROVAL_DOCUMENT_ID = 1022;
	public static final int AD_AMOUNT_LIMIT_ID = 1023;
	public static final int AD_APPROVAL_COA_LINE_ID = 1024;
	public static final int AD_APPROVAL_USER_ID = 1025;
	public static final int AD_USER_SESSION_ID = 1026;
	public static final int AD_BRANCH_ID = 1027;
	public static final int AD_TRANSACTION_SYNC_IMPORT_ID = 1028;
	public static final int AD_TRANSACTION_SYNC_EXPORT_ID = 1029;
	public static final int AD_REP_TRANSACTION_LOG_ID = 1501;
	public static final int AD_REP_BANK_ACCOUNT_LIST_ID = 1502;
	public static final int GEN_REP_VALUE_SET_VALUE_LIST_ID = 1503;
	public static final int AD_REP_TRANSACTION_SUMMARY_ID = 1504;
	public static final int AD_REP_RESPONSIBILITY_LIST_ID = 1505;
	public static final int AD_REP_USER_LIST_ID = 1506;
	public static final int AD_REP_APPROVAL_LIST_ID = 1507;
	public static final int AD_STORED_PROCEDURE_SETUP_ID = 1508;
	//General Ledger

    public static final int GL_JOURNAL_ENTRY_ID = 1;
    public static final int GL_FIND_JOURNAL_ID = 2;
    public static final int GL_JOURNAL_POST_ID = 3;
    public static final int GL_JOURNAL_IMPORT_ID = 4;
    public static final int GL_JOURNAL_REVERSAL_ID = 5;
    public static final int GL_RECURRING_JOURNAL_ENTRY_ID = 6;
	public static final int GL_YEAR_END_CLOSING_ID = 7;
	public static final int GL_FIND_RECURRING_JOURNAL_ID = 8;
	public static final int GL_RECURRING_JOURNAL_GENERATION_ID = 9;
	public static final int GL_APPROVAL_ID = 10;
	public static final int GL_JOURNAL_BATCH_PRINT_ID = 11;
	public static final int GL_JOURNAL_BATCH_SUBMIT_ID = 12;
	public static final int GL_JOURNAL_BATCH_ENTRY_ID = 13;
	public static final int GL_FIND_JOURNAL_BATCH_ID = 14;
	public static final int GL_JOURNAL_BATCH_COPY_ID = 15;
	public static final int GL_CHART_OF_ACCOUNTS_ID = 22;
	public static final int GL_PERIOD_TYPES_ID = 23;
	public static final int GL_ACCOUNTING_CALENDAR_ID = 24;
	public static final int GL_ACCOUNTING_CALENDAR_LINES_ID = 25;
	public static final int GL_TRANSACTION_CALENDAR_LINES_ID = 27;
	public static final int GL_JOURNAL_SOURCES_ID = 33;
	public static final int GL_JOURNAL_CATEGORIES_ID = 34;
	public static final int GL_SUSPENSE_ACCOUNTS_ID = 35;
	public static final int GL_OPEN_CLOSE_PERIODS_ID = 37;
	public static final int GL_CURRENCY_MAINTENANCE_ID = 42;
	public static final int GL_DAILY_RATES_ID = 43;
    public static final int GL_ORGANIZATION_ENTRY_ID = 45;
	public static final int GL_ORGANIZATION_ACCOUNT_ASSIGNMENT_ID=46;
	public static final int GL_ORGANIZATION_RESPONSIBILITY_ASSIGNMENT_ID=47;
	public static final int GL_FRG_FINANCIAL_REPORT_ENTRY_ID = 53;
	public static final int GL_FRG_ROW_SET_ID = 54;
	public static final int GL_FRG_COLUMN_SET_ID = 55;
	public static final int GL_FRG_ROW_ENTRY_ID = 56;
	public static final int GL_FRG_COLUMN_ENTRY_ID = 57;
	public static final int GL_FRG_ACCOUNT_ASSIGNMENT_ID = 58;
	public static final int GL_FRG_CALCULATION_ID = 59;
	public static final int GL_FIND_CHART_OF_ACCOUNT_ID = 60;
	public static final int GL_FIND_DAILY_RATE_ID = 61;
	public static final int GL_FIND_JOURNAL_INTERFACE_ID = 62;
	public static final int GL_JOURNAL_INTERFACE_MAINTENANCE_ID = 63;
	public static final int GL_BUDGET_DEFINITION_ID = 64;
	public static final int GL_BUDGET_ORGANIZATION_ID = 65;
	public static final int GL_BUDGET_ACCOUNT_ASSIGNMENT_ID = 66;
	public static final int GL_BUDGET_ENTRY_ID = 67;
	public static final int GL_FIND_BUDGET_ID = 68;
	public static final int GL_JOURNAL_INTERFACE_UPLOAD_ID = 69;
	public static final int GL_COA_GENERATOR_ID = 70;
	public static final int GL_TAX_INTERFACE_RUN_ID = 71;
	public static final int GL_TAX_INTERFACE_MAINTENANCE_ID = 72;
	public static final int GL_STATIC_REPORT_ID = 73;
	public static final int GL_USER_STATIC_REPORT_ID = 74;
	public static final int GL_FIND_SEGMENT_ID = 75;
	public static final int GL_FOREX_REVALUATION_ID = 76;
	public static final int GL_REP_DETAIL_TRIAL_BALANCE_ID = 501;
	public static final int GL_REP_DETAIL_INCOME_STATEMENT_ID = 502;
	public static final int GL_REP_DETAIL_BALANCE_SHEET_ID = 503;
	public static final int GL_REP_GENERAL_LEDGER_ID = 504;
	public static final int GL_REP_FINANCIAL_REPORT_RUN_ID = 505;
	public static final int GL_REP_CHART_OF_ACCOUNT_LIST_ID = 506;
	public static final int GL_REP_JOURNAL_PRINT_ID = 507;
	public static final int GL_REP_INCOME_TAX_WITHHELD_ID = 508;
	public static final int GL_REP_MONTHLY_VAT_DECLARATION_ID = 509;
	public static final int GL_REP_QUARTERLY_VAT_RETURN_ID = 510;
	public static final int GL_REP_JOURNAL_EDIT_LIST_ID = 511;
	public static final int GL_REP_CSV_QUARTERLY_VAT_RETURN_ID = 512;
	public static final int GL_REP_BUDGET_ID = 513;
	public static final int GL_REP_STATIC_REPORT_ID = 515;
	public static final int GL_REP_JOURNAL_REGISTER_ID = 516;
	public static final int GL_REP_FOREX_REVALUATION_PRINT_ID = 517;
	public static final int GL_REP_INCOME_TAX_RETURN_ID = 520;
	public static final int GL_REP_DAILY_RATE_LIST_ID = 518;
	public static final int GL_REP_INVESTOR_LEDGER_ID = 519;


    // Accounts Payables

    public static final int AP_SUPPLIER_ENTRY_ID = 3001;
    public static final int AP_FIND_SUPPLIER_ID = 3002;
    public static final int AP_VOUCHER_ENTRY_ID = 3003;
    public static final int AP_FIND_VOUCHER_ID = 3004;
    public static final int AP_DEBIT_MEMO_ENTRY_ID = 3005;
    public static final int AP_RECURRING_VOUCHER_ENTRY_ID = 3006;
    public static final int AP_RECURRING_VOUCHER_GENERATION_ID = 3007;
    public static final int AP_FIND_RECURRING_VOUCHER_ID = 3008;
    public static final int AP_PAYMENT_ENTRY_ID = 3009;
    public static final int AP_DIRECT_CHECK_ENTRY_ID = 3010;
    public static final int AP_FIND_CHECK_ID = 3011;
    public static final int AP_VOUCHER_POST_ID = 3012;
    public static final int AP_CHECK_POST_ID = 3013;
    public static final int AP_GL_JOURNAL_INTERFACE_ID = 3014;
    public static final int AP_JOURNAL_ID = 3015;
    public static final int AP_APPROVAL_ID = 3016;
    public static final int AP_VOUCHER_BATCH_PRINT_ID = 3017;
    public static final int AP_CHECK_BATCH_PRINT_ID = 3018;
    public static final int AP_VOUCHER_BATCH_SUBMIT_ID = 3019;
    public static final int AP_CHECK_BATCH_SUBMIT_ID = 3020;
    public static final int AP_VOUCHER_BATCH_ENTRY_ID = 3021;
    public static final int AP_CHECK_BATCH_ENTRY_ID = 3022;
    public static final int AP_FIND_VOUCHER_BATCH_ID = 3023;
    public static final int AP_FIND_CHECK_BATCH_ID = 3024;
    public static final int AP_VOUCHER_BATCH_COPY_ID = 3025;
    public static final int AP_CHECK_BATCH_COPY_ID = 3026;
    public static final int AP_PURCHASE_ORDER_ENTRY_ID = 3027;
    public static final int AP_FIND_PURCHASE_ORDER_ID = 3028;
    public static final int AP_RECEIVING_ITEM_ID = 3029;
    public static final int AP_VOUCHER_IMPORT_ID = 3030;
    public static final int AP_CHECK_IMPORT_ID = 3031;
    public static final int AP_GENERATE_PURCHASE_REQUISITION_ID = 3032;
    public static final int AP_PURCHASE_REQUISITION_ENTRY_ID = 3033;
    public static final int AP_FIND_PURCHASE_REQUISITION_ID = 3034;
    public static final int AP_CHECK_PAYMENT_REQUEST_ENTRY_ID = 3035;
    public static final int AP_CANVASS_ID = 3036;
    public static final int AP_ANNUAL_PROCUREMENT_UPLOAD = 3037;
    public static final int AP_TAX_CODE_ID = 3301;
    public static final int AP_SUPPLIER_CLASS_ID = 3302;
    public static final int AP_SUPPLIER_TYPE_ID = 3303;
    public static final int AP_WITHHOLDING_TAX_CODE_ID = 3304;
    public static final int AP_REP_VOUCHER_PRINT_ID = 3501;
    public static final int AP_REP_DEBIT_MEMO_PRINT_ID = 3502;
    public static final int AP_REP_CHECK_VOUCHER_PRINT_ID = 3503;
    public static final int AP_REP_CHECK_PRINT_ID = 3504;
    public static final int AP_REP_AP_REGISTER_ID = 3505;
    public static final int AP_REP_CHECK_REGISTER_ID = 3506;
    public static final int AP_REP_AGING_ID = 3507;
    public static final int AP_REP_INPUT_TAX_ID = 3508;
    public static final int AP_REP_WITHHOLDING_TAX_EXPANDED_ID = 3509;
    public static final int AP_REP_SUPPLIER_LIST_ID = 3510;
    public static final int AP_REP_VOUCHER_EDIT_LIST_ID = 3511;
    public static final int AP_REP_CHECK_EDIT_LIST_ID = 3512;
    public static final int AP_REP_TAX_CODE_LIST_ID = 3513;
    public static final int AP_REP_WITHHOLDING_TAX_CODE_LIST_ID = 3514;
    public static final int AP_REP_SUPPLIER_CLASS_LIST_ID = 3515;
    public static final int AP_REP_SUPPLIER_TYPE_LIST_ID = 3516;
    public static final int AP_REP_PURCHASE_ORDER_PRINT_ID = 3517;
    public static final int AP_REP_RECEIVING_REPORT_PRINT_ID = 3518;
    public static final int AP_REP_AGING_SUMMARY_ID = 3519;
    public static final int AP_REP_RECEIVING_ITEMS_ID = 3520;
    public static final int AP_REP_PURCHASE_ORDER_ID = 3521;
    public static final int AP_REP_PURCHASE_REQUISITION_PRINT_ID = 3522;
    public static final int AP_REP_CANVASS_REPORT_PRINT_ID = 3523;
    public static final int AP_REP_PURCHASE_REQUISITION_REGISTER_ID = 3524;
    public static final int AP_REP_ANNUAL_PROCUREMENT_ID = 3525;
    // Accounts Receivables
    public static final int AR_CUSTOMER_ENTRY_ID = 2001;
    public static final int AR_FIND_CUSTOMER_ID = 2002;
    public static final int AR_INVOICE_ENTRY_ID = 2003;
    public static final int AR_FIND_INVOICE_ID = 2004;
    public static final int AR_CREDIT_MEMO_ENTRY_ID = 2005;
    public static final int AR_FIND_RECEIPT_ID = 2006;
    public static final int AR_MISC_RECEIPT_ENTRY_ID = 2007;
    public static final int AR_RECEIPT_ENTRY_ID = 2008;
    public static final int AR_INVOICE_POST_ID = 2009;
    public static final int AR_RECEIPT_POST_ID = 2010;
    public static final int AR_GL_JOURNAL_INTERFACE_ID = 2011;
    public static final int AR_JOURNAL_ID = 2012;
    public static final int AR_APPROVAL_ID = 2013;
    public static final int AR_PDC_ENTRY_ID = 2014;
    public static final int AR_FIND_PDC_ID = 2015;
    public static final int AR_PDC_INVOICE_GENERATION_ID = 2016;
    public static final int AR_SALES_ORDER_ENTRY_ID = 2017;
    public static final int AR_FIND_SALES_ORDER_ID = 2018;
    public static final int AR_INVOICE_IMPORT_ID = 2019;
    public static final int AR_RECEIPT_IMPORT_ID = 2020;
    public static final int AR_JOB_ORDER_ENTRY_ID = 2021;
    public static final int AR_FIND_JOB_ORDER_ID = 2022;
    public static final int AP_JOB_ORDER_ASSIGNMENT_ID = 2023;
    public static final int AR_FIND_PERSONEL_ID = 2024;
    public static final int AR_TAX_CODE_ID = 2301;
    public static final int AR_WITHHOLDING_TAX_CODE_ID = 2302;
    public static final int AR_CUSTOMER_TYPE_ID = 2303;
    public static final int AR_CUSTOMER_CLASS_ID = 2305;
    public static final int AR_STANDARD_MEMO_LINE_ID = 2306;
    public static final int AR_INVOICE_BATCH_PRINT_ID = 2307;
    public static final int AR_RECEIPT_BATCH_PRINT_ID = 2308;
    public static final int AR_INVOICE_BATCH_SUBMIT_ID = 2309;
    public static final int AR_RECEIPT_BATCH_SUBMIT_ID = 2310;
    public static final int AR_INVOICE_BATCH_ENTRY_ID = 2311;
    public static final int AR_FIND_INVOICE_BATCH_ID = 2312;
    public static final int AR_RECEIPT_BATCH_ENTRY_ID = 2313;
    public static final int AR_FIND_RECEIPT_BATCH_ID = 2314;
    public static final int AR_INVOICE_BATCH_COPY_ID = 2315;
    public static final int AR_RECEIPT_BATCH_COPY_ID = 2316;
    public static final int AR_SALESPERSON_ID = 2317;
    public static final int AR_PERSONEL_ID = 2318;
    public static final int AR_JOB_ORDER_TYPE_ID = 2319;
    public static final int AR_PERSONEL_TYPE_ID = 2320;
    public static final int AR_DELIVERY_ID = 2321;
    public static final int AR_FIND_DELIVERY_ID = 2322;
    public static final int AR_REP_INVOICE_PRINT_ID = 2501;
    public static final int AR_REP_CREDIT_MEMO_PRINT_ID = 2502;
    public static final int AR_REP_RECEIPT_PRINT_ID = 2503;
    public static final int AR_REP_STATEMENT_ID = 2504;
    public static final int AR_REP_AGING_ID = 2505;
    public static final int AR_REP_SALES_REGISTER_ID = 2506;
    public static final int AR_REP_OR_REGISTER_ID = 2507;
    public static final int AR_REP_CUSTOMER_LIST_ID = 2508;
    public static final int AR_REP_OUTPUT_TAX_ID = 2509;
    public static final int AR_REP_CREDITABLE_WITHHOLDING_TAX_ID = 2510;
    public static final int AR_REP_INVOICE_EDIT_LIST_ID = 2511;
    public static final int AR_REP_RECEIPT_EDIT_LIST_ID = 2512;
    public static final int AR_REP_SALES_ID = 2513;
    public static final int AR_REP_TAX_CODE_LIST_ID = 2514;
    public static final int AR_REP_WITHHOLDING_TAX_CODE_LIST_ID = 2515;
    public static final int AR_REP_CUSTOMER_CLASS_LIST_ID = 2516;
    public static final int AR_REP_CUSTOMER_TYPE_LIST_ID = 2517;
    public static final int AR_REP_STANDARD_MEMO_LINE_LIST_ID = 2518;
    public static final int AR_REP_PDC_ID = 2519;
    public static final int AR_REP_AGING_SUMMARY_ID = 2520;
    public static final int AR_REP_SALES_ORDER_PRINT_ID = 2521;
    public static final int AR_REP_DELIVERY_RECEIPT_PRINT_ID = 2522;
    public static final int AR_REP_SALESPERSON_ID = 2523;
    public static final int AR_REP_TRANSMITTAL_DOCUMENT_PRINT_ID = 2524;
    public static final int AR_REP_DELIVERY_RECEIPT_ID = 2525;
    public static final int AR_REP_SALES_ORDER_ID = 2526;
    public static final int AR_REP_PDC_PRINT_ID = 2527;
    public static final int AR_REP_JOB_ORDER_PRINT_ID = 2528;
    public static final int AR_REP_JOB_STATUS_REPORT_ID = 2529;
    public static final int AR_REP_PERSONEL_SUMMARY_ID = 2530;

    // Cash Management
    public static final int CM_BANK_RECONCILIATION_ID = 4001;
    public static final int CM_ADJUSTMENT_ENTRY_ID = 4002;
    public static final int CM_FIND_ADJUSTMENT_ID = 4003;
    public static final int CM_FUND_TRANSFER_ENTRY_ID = 4004;
    public static final int CM_FIND_FUND_TRANSFER_ID = 4005;
    public static final int CM_GL_JOURNAL_INTERFACE_ID = 4006;
    public static final int CM_JOURNAL_ID = 4007;
    public static final int CM_APPROVAL_ID = 4008;
    public static final int CM_ADJUSTMENT_POST_ID = 4009;
    public static final int CM_FUND_TRANSFER_POST_ID = 4010;
    public static final int CM_ADJUSTMENT_BATCH_SUBMIT_ID = 4011;
    public static final int CM_FUND_TRANSFER_BATCH_SUBMIT_ID = 4012;
    public static final int CM_RELEASING_CHECKS_ID = 4013;
    public static final int CM_REP_DEPOSIT_LIST_ID = 4501;
    public static final int CM_REP_RELEASED_CHECKS_ID = 4502;
    public static final int CM_REP_DAILY_CASH_POSITION_DETAIL_ID = 4503;
    public static final int CM_REP_DAILY_CASH_POSITION_SUMMARY_ID = 4504;
    public static final int CM_REP_DAILY_CASH_FORECAST_DETAIL_ID = 4505;
    public static final int CM_REP_DAILY_CASH_FORECAST_SUMMARY_ID = 4506;
    public static final int CM_REP_WEEKLY_CASH_FORECAST_DETAIL_ID = 4507;
    public static final int CM_REP_WEEKLY_CASH_FORECAST_SUMMARY_ID = 4508;
    public static final int CM_REP_BANK_RECONCILIATION_ID = 4509;
    public static final int CM_REP_CASH_POSITION_ID = 4510;
    public static final int CM_REP_ADJUSTMENT_PRINT_ID = 4511;

   	//  Inventory

   	public static final int INV_ADJUSTMENT_ENTRY_ID = 5001;
   	public static final int INV_FIND_ADJUSTMENT_ID = 5002;
   	public static final int INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID = 5003;
   	public static final int INV_BUILD_UNBUILD_ASSEMBLY_ORDER_ENTRY_ID = 5529;
   	public static final int INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID = 5004;
   	public static final int INV_PHYSICAL_INVENTORY_ENTRY_ID = 5005;
   	public static final int INV_FIND_PHYSICAL_INVENTORY_ID = 5006;
   	public static final int INV_APPROVAL_ID = 5007;
   	public static final int INV_ADJUSTMENT_POST_ID = 5008;
   	public static final int INV_GL_JOURNAL_INTERFACE_ID = 5009;
   	public static final int INV_JOURNAL_ID = 5010;
    public static final int INV_OVERHEAD_ENTRY_ID = 5011;
    public static final int INV_FIND_OVERHEAD_ID = 5012;
    public static final int INV_BUILD_ORDER_ENTRY_ID = 5013;
    public static final int INV_FIND_BUILD_ORDER_ID = 5014;
    public static final int INV_STOCK_ISSUANCE_ENTRY_ID = 5015;
    public static final int INV_FIND_STOCK_ISSUANCE_ID = 5016;
    public static final int INV_ASSEMBLY_TRANSFER_ENTRY_ID = 5017;
    public static final int INV_FIND_ASSEMBLY_TRANSFER_ID = 5018;
    public static final int INV_FIND_BUILD_ORDER_LINE_ID  = 5019;
    public static final int INV_STOCK_TRANSFER_ENTRY_ID = 5020;
    public static final int INV_FIND_STOCK_TRANSFER_ID = 5021;
    public static final int INV_BRANCH_STOCK_TRANSFER_OUT_ENTRY_ID = 5022;
    public static final int INV_BRANCH_STOCK_TRANSFER_IN_ENTRY_ID = 5023;
    public static final int INV_FIND_BRANCH_STOCK_TRANSFER_ID = 5024;
    public static final int INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID = 5025;
    public static final int INV_ADJUSTMENT_REQUEST_ID= 5026;
    public static final int INV_TRANSACTIONAL_BUDGET_ID = 5027;
    public static final int INV_ITEM_ENTRY_ID = 5301;
    public static final int INV_FIND_ITEM_ID = 5302;
    public static final int INV_LOCATION_ENTRY_ID = 5303;
    public static final int INV_FIND_LOCATION_ID = 5304;
    public static final int INV_ITEM_LOCATION_ENTRY_ID = 5305;
    public static final int INV_FIND_ITEM_LOCATION_ENTRY_ID = 5306;
    public static final int INV_UNIT_OF_MEASURE_ID = 5307;
    public static final int INV_OVERHEAD_MEMO_LINE_ID = 5308;
    public static final int INV_BUILD_UNBUILD_ASSEMBLY_POST_ID = 5309;
    public static final int INV_ASSEMBLY_ITEM_ENTRY_ID = 5310;
    public static final int INV_UNIT_OF_MEASURE_CONVERSION_ID = 5311;
    public static final int INV_PRICE_LEVEL_ID = 5312;
    public static final int INV_LINE_ITEM_TEMPLATE_ID = 5313;
    public static final int INV_REP_USAGE_VARIANCE_ID = 5501;
    public static final int INV_REP_ITEM_COSTING_ID = 5502;
    public static final int INV_REP_ITEM_LIST_ID = 5503;
    public static final int INV_REP_ASSEMBLY_LIST_ID = 5504;
    public static final int INV_REP_COUNT_VARIANCE_ID = 5505;
    public static final int INV_REP_COST_OF_SALES_ID = 5506;
    public static final int INV_REP_INVENTORY_PROFITABILITY_ID = 5507;
    public static final int INV_REP_BUILD_ORDER_PRINT_ID = 5508;
    public static final int INV_REP_STOCK_ISSUANCE_PRINT_ID = 5509;
    public static final int INV_REP_ASSEMBLY_TRANSFER_PRINT_ID = 5510;
    public static final int INV_REP_STOCK_TRANSFER_ID = 5511;
    public static final int INV_REP_STOCK_ON_HAND_ID = 5512;
    public static final int INV_REP_REORDER_ITEMS_ID = 5513;
    public static final int INV_REP_STOCK_TRANSFER_PRINT_ID = 5514;
    public static final int INV_REP_BRANCH_STOCK_TRANSFER_IN_PRINT_ID = 5515;
    public static final int INV_REP_BRANCH_STOCK_TRANSFER_OUT_PRINT_ID = 5516;
    public static final int INV_REP_PHYSICAL_INVENTORY_WORKSHEET_ID = 5517;
    public static final int INV_REP_ADJUSTMENT_PRINT_ID = 5518;
    public static final int INV_REP_STOCK_CARD_ID = 5519;
    public static final int INV_REP_ITEM_LEDGER_ID = 5520;
    public static final int INV_REP_INVENTORY_LIST_ID = 5521;
    public static final int INV_REP_ADJUSTMENT_REGISTER_ID = 5522;
    public static final int INV_REP_BRANCH_STOCK_TRANSFER_REGISTER_ID = 5523;
    public static final int INV_REP_MARKUP_LIST_ID = 5524;
    public static final int INV_REP_BUILD_UNBUILD_PRINT_ID = 5525;
    public static final int INV_REP_ADJUSTMENT_REQUEST_PRINT_ID = 5526;
    public static final int INV_FIND_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID = 5527;
    public static final int INV_BUILD_UNBUILD_ASSEMBLY_BATCH_ENTRY_ID = 5528;
    public static final int INV_REP_BUILD_UNBUILD_ASSEMBLY_ORDER_ID = 5530;

    public static final int INV_REP_PHYSICAL_INVENTORY_PRINT_ID = 5531;

    //Human Resource

    //--Transaction--

    //--Maintenance--
    public static final int HR_SYNCHRONIZER = 6000;

    //--Report/List--

    //Project Management

    //--Transaction--

    //--Maintenance--
    public static final int PM_SYNCHRONIZER = 7000;

    public static final int PM_PROJECT_ENTRY_ID = 7001;
    public static final int PM_FIND_PROJECT_ID = 7002;
    public static final int PM_PROJECT_TYPE_ENTRY_ID = 7003;
    public static final int PM_FIND_PROJECT_TYPE_ID = 7004;
    public static final int PM_PROJECT_TYPE_TYPE_ENTRY_ID = 7005;
    public static final int PM_FIND_PROJECT_TYPE_TYPE_ENTRY_ID = 7006;

    //--Report/List--
    public static final int PM_REP_PROJECT_PROFITABILITY_ID = 7100;


	// Page Colors
	public static final String TXN_HEADER_BGC = "#92A4B6";
	public static final String TXN_MAIN_BGC = "#F4F6F8";
	public static final String TXN_TABLE_TITLE_BGC = "#92A4B6";
	public static final String TXN_TABLE_HEADER_BGC = "#DDDDEE";
	public static final String TXN_TABLE_BORDER_DARK_BGC = "#FFFFFF";
	public static final String TXN_TABLE_BORDER_BGC = "#74B4E0";
	public static final String TXN_TABLE_ROW_BGC1 = "#DEE9E9";
	public static final String TXN_TABLE_ROW_BGC2 = "#EDF3F3";

    // Report View Type

    public static final String REPORT_VIEW_TYPE_PDF = "PDF";
    public static final String REPORT_VIEW_TYPE_EXCEL = "MS-EXCEL";
    public static final String REPORT_VIEW_TYPE_TEXT = "TEXT";
    public static final String REPORT_VIEW_TYPE_HTML = "HTML";
    public static final String REPORT_VIEW_TYPE_CSV = "CSV";
    public static final String REPORT_VIEW_TYPE_CSV_SP = "CSV_SP";
    public static final String REPORT_VIEW_TYPE_PDF_SP = "PDF_SP";

    public static final String REPORT_VIEW_TYPE_RELIEF_PURCHASES = "RELIEF PURCHASES";
    public static final String REPORT_VIEW_TYPE_RELIEF_SALES = "RELIEF SALES";
    public static final String REPORT_VIEW_TYPE_RELIEF_IMPORTATIONS = "RELIEF IMPORTATIONS";
    public static final String REPORT_EMAIL_BLAST_CSV = "COA EMAIL LIST";

    public static final String REPORT_VIEW_TYPE_DIGIBANK = "DIGIBANK";

	// User Permissions
	public static final String QUERY_ONLY = "Q";
	public static final String FULL_ACCESS = "F";
	public static final String NO_ACCESS= "N";
	public static final String SPECIAL = "S";

	// AD Numbering Types
	public static final char AD_NUMBERING_TYPE_A = 'A';
	public static final char AD_NUMBERING_TYPE_M = 'M';
	public static final String AD_NUMBERING_TYPE_AUTO = "AUTOMATIC";
	public static final String AD_NUMBERING_TYPE_MANUAL = "MANUAL";

    // AD Bank Institution Types

    public static final String AD_BANK_CLEARING_HOUSE = "CLEARING HOUSE";
    public static final String AD_BANK = "BANK";

    // AD Bank Account Use

    public static final String AD_BANK_ACCOUNT_USE_INTERNAL = "INTERNAL";
    public static final String AD_BANK_ACCOUNT_USE_SUPPLIER = "SUPPLIER";

    // AD Aging Bucket Types

    public static final String AD_AB_4_BUCKET_AGING = "4-BUCKET AGING";
    public static final String AD_AB_7_BUCKET_AGING = "7-BUCKET AGING";
    public static final String AD_AB_CREDIT_SNAPSHOT = "CREDIT SNAPSHOT";
    public static final String AD_AB_STATEMENT_AGING = "STATEMENT AGING";

    // AD Aging Bucket Value Types

    public static final String AD_AV_CURRENT = "CURRENT";
    public static final String AD_AV_PAST_DUE = "PAST DUE";
    public static final String AD_AV_DISPUTE_ONLY = "DISPUTE ONLY";
    public static final String AD_AV_PENDING_ADJUSTMENT_ONLY = "PENDING ADJUSTMENT ONLY";
    public static final String AD_AV_DISPUTE_AND_PENDING_ADJUSTMENTS = "DISPUTE AND PENDING ADJUSTMENTS";
    public static final String AD_AV_FUTURE = "FUTURE";

    // AD User Password Expiration Types

    public static final String AD_USR_DAYS = "DAYS";
    public static final String AD_USR_ACCESS = "ACCESS";
    public static final String AD_USR_NONE = "NONE";

    // GL Fund Status
 	public static final char GL_FUND_STATUS_R = 'R';
	public static final char GL_FUND_STATUS_U = 'U';
	public static final char GL_FUND_STATUS_N = 'N';
	public static final String GL_FUND_STATUS_RESERVED = "RESERVED";
	public static final String GL_FUND_STATUS_UNRESERVED = "UNRESERVED";
	public static final String GL_FUND_STATUS_NOT_APPLICABLE = "N/A";

	// GL Year Types
	public static final char GL_YEAR_TYPE_FY = 'F';
	public static final char GL_YEAR_TYPE_CY = 'C';
	public static final String GL_YEAR_TYPE_FISCAL_YEAR = "FISCAL YEAR";
	public static final String GL_YEAR_TYPE_CALENDAR_YEAR = "CALENDAR YEAR";

	// GL Reversal Methods
	public static final char GL_REVERSAL_METHOD_S = 'S';
	public static final char GL_REVERSAL_METHOD_C = 'C';
	public static final char GL_REVERSAL_METHOD_U = 'U';
	public static final String GL_REVERSAL_METHOD_SWITCHDRCR = "SWITCH DR/CR";
	public static final String GL_REVERSAL_METHOD_CHANGESIGN = "CHANGE SIGN";
	public static final String GL_REVERSAL_METHOD_USE_DEFAULT = "USE DEFAULT";

	// GL Effective Date Rules
	public static final char GL_EFFECTIVE_DATE_RULE_F = 'F';
	public static final char GL_EFFECTIVE_DATE_RULE_L = 'L';
	public static final char GL_EFFECTIVE_DATE_RULE_R = 'R';
	public static final String GL_EFFECTIVE_DATE_RULE_FAIL = "FAIL";
	public static final String GL_EFFECTIVE_DATE_RULE_LEAVE_ALONE = "LEAVE ALONE";
	public static final String GL_EFFECTIVE_DATE_RULE_ROLL_DATE = "ROLL DATE";

	// GL Period Status
	public static final char GL_PERIOD_STATUS_O = 'O';
	public static final char GL_PERIOD_STATUS_F = 'F';
	public static final char GL_PERIOD_STATUS_N = 'N';
	public static final char GL_PERIOD_STATUS_C = 'C';
	public static final char GL_PERIOD_STATUS_P = 'P';
	public static final String GL_PERIOD_STATUS_OPEN = "OPEN";
	public static final String GL_PERIOD_STATUS_FUTURE_ENTRY = "FUTURE ENTRY";
	public static final String GL_PERIOD_STATUS_NEVER_OPENED = "NEVER OPENED";
	public static final String GL_PERIOD_STATUS_CLOSED = "CLOSE";
	public static final String GL_PERIOD_STATUS_PERMANENTLY_CLOSED = "PERMANENTLY CLOSE";

	// GL FJB View
	public static final String GL_VIEW_TYPE_POSTED = "POSTED";
	public static final String GL_VIEW_TYPE_UNPOSTED = "UNPOSTED";

    // Gen Qualifier Types

    public static final String GEN_SEGMENT_TYPE_N = "N";

    // Gl Row Entry - Font Style

    public static final String GL_FRG_RE_FONT_BOLD = "BOLD";
    public static final String GL_FRG_RE_FONT_ITALIC = "ITALIC";
    public static final String GL_FRG_RE_FONT_NORMAL = "NORMAL";

    // Gl Column Entry - Factors

    public static final String GL_FRG_CE_FACTOR_BILLIONS = "BILLIONS";
    public static final String GL_FRG_CE_FACTOR_MILLIONS = "MILLIONS";
    public static final String GL_FRG_CE_FACTOR_THOUSANDS = "THOUSANDS";
    public static final String GL_FRG_CE_FACTOR_UNITS = "UNITS";
    public static final String GL_FRG_CE_FACTOR_PERCENTILES = "PERCENTILES";

    // Gl Column Entry - Amount Types

    public static final String GL_FRG_CE_FACTOR_PTD = "PTD";
    public static final String GL_FRG_CE_FACTOR_QTD = "QTD";
    public static final String GL_FRG_CE_FACTOR_YTD = "YTD";

    // Gl Account Assignment - Activity Types

    public static final String GL_FRG_AA_ACTIVITY_TYPE_BEG = "BEG";
    public static final String GL_FRG_AA_ACTIVITY_TYPE_DEBIT = "DR";
    public static final String GL_FRG_AA_ACTIVITY_TYPE_CREDIT = "CR";
    public static final String GL_FRG_AA_ACTIVITY_TYPE_END = "END";
    public static final String GL_FRG_AA_ACTIVITY_TYPE_NET = "NET";

    // Gl Calculation - Operator Types

    public static final String GL_FRG_CAL_OPERATOR_TYPE_ENTER = "ENTER";
    public static final String GL_FRG_CAL_OPERATOR_TYPE_AVERAGE = "AVERAGE";
    public static final String GL_FRG_CAL_OPERATOR_TYPE_SUB = "-";
    public static final String GL_FRG_CAL_OPERATOR_TYPE_ADD = "+";
    public static final String GL_FRG_CAL_OPERATOR_TYPE_DIV = "/";
    public static final String GL_FRG_CAL_OPERATOR_TYPE_MUL = "*";

    // True or False

    public static final String TRUE = "true";
    public static final String FALSE = "false";

    // Cm Adjustment Type

    public static final String CM_AE_INTEREST= "INTEREST";
    public static final String CM_AE_BANK_CHARGE= "BANK CHARGE";
    public static final String CM_AE_CREDIT_MEMO= "CREDIT MEMO";
    public static final String CM_AE_DEBIT_MEMO= "DEBIT MEMO";
    public static final String CM_AE_ADVANCE= "ADVANCE";
    public static final String CM_AE_SPL_ADVANCE= "SUPPLIER ADVANCE";
    public static final String CM_AR_SO_ADVANCE= "SO ADVANCE";

    // Cm Find Fund Transfer

    public static final String CM_CR_ORDER_BY_REFERENCE_NMBER= "REFERENCE NUMBER";

    // Gl Journal Order By

    public static final String GL_JR_ORDER_BY_NAME = "NAME";
    public static final String GL_JR_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String GL_JR_ORDER_BY_CATEGORY = "CATEGORY";
    public static final String GL_JR_ORDER_BY_SOURCE = "SOURCE";

    // Gl Recurring Journal Schedule

    public static final String GL_RJ_SCHEDULE_DAILY = "DAILY";
    public static final String GL_RJ_SCHEDULE_WEEKLY = "WEEKLY";
    public static final String GL_RJ_SCHEDULE_SEMI_MONTHLY = "SEMI MONTHLY";
    public static final String GL_RJ_SCHEDULE_MONTHLY = "MONTHLY";
    public static final String GL_RJ_SCHEDULE_QUARTERLY = "QUARTERLY";
    public static final String GL_RJ_SCHEDULE_SEMI_ANNUALLY = "SEMI ANNUALLY";
    public static final String GL_RJ_SCHEDULE_ANNUALLY = "ANNUALLY";

    // Gl Recurring Journal Order By

    public static final String GL_RJ_ORDER_BY_NAME = "NAME";
    public static final String GL_RJ_ORDER_BY_NEXT_RUN_DATE = "NEXT RUN DATE";
    public static final String GL_RJ_ORDER_BY_CATEGORY = "CATEGORY";

    // Ap Dr Class

    public static final String AP_DR_CLASS_EXPENSE = "EXPENSE";
    public static final String AP_DR_CLASS_TAX = "TAX";
    public static final String AP_DR_CLASS_WITHHOLDING_TAX = "W-TAX";
    public static final String AP_DR_CLASS_OTHER = "OTHER";
    public static final String AP_DR_CLASS_PAYABLE = "PAYABLE";
    public static final String AP_DR_CLASS_CASH = "CASH";

    // Ap Find Supplier Order By

    public static final String AP_FS_ORDER_BY_SUPPLIER_CODE= "SUPPLIER CODE";
    public static final String AP_FS_ORDER_BY_SUPPLIER_NAME= "SUPPLIER NAME";

    // Ap Find Voucher Payment Status

    public static final String AP_FV_PAYMENT_STATUS_PAID = "PAID";
    public static final String AP_FV_PAYMENT_STATUS_UNPAID = "UNPAID";

    // Ap Find Voucher Order By

    public static final String AP_FV_ORDER_BY_SUPPLIER_CODE= "SUPPLIER CODE";
    public static final String AP_FV_ORDER_BY_DOCUMENT_NUMBER= "DOCUMENT NUMBER";

    // Ap Recurring Voucher Schedule

    public static final String AP_RV_SCHEDULE_DAILY = "DAILY";
    public static final String AP_RV_SCHEDULE_WEEKLY = "WEEKLY";
    public static final String AP_RV_SCHEDULE_SEMI_MONTHLY = "SEMI MONTHLY";
    public static final String AP_RV_SCHEDULE_MONTHLY = "MONTHLY";
    public static final String AP_RV_SCHEDULE_QUARTERLY = "QUARTERLY";
    public static final String AP_RV_SCHEDULE_SEMI_ANNUALLY = "SEMI ANNUALLY";
    public static final String AP_RV_SCHEDULE_ANNUALLY = "ANNUALLY";

    // Ap Recurring Voucher Order By

    public static final String AP_RVG_ORDER_BY_NAME = "NAME";
    public static final String AP_RVG_ORDER_BY_NEXT_RUN_DATE = "NEXT RUN DATE";
    public static final String AP_RVG_ORDER_BY_SUPPLIER_CODE = "SUPPLIER CODE";


    // Ap Check Types

    public static final String AP_FC_CHECK_TYPE_PAYMENT = "PAYMENT";
    public static final String AP_FC_CHECK_TYPE_DIRECT = "DIRECT";

    // Ap Find Check Order By

    public static final String AP_FC_ORDER_BY_BANK_ACCOUNT = "BANK ACCOUNT";
    public static final String AP_FC_ORDER_BY_SUPPLIER_CODE = "SUPPLIER CODE";
    public static final String AP_FC_ORDER_BY_CHECK_NUMBER = "CHECK NUMBER";
    public static final String AP_FC_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String AP_FC_ORDER_BY_REFERENCE_NUMBER = "REFERENCE NUMBER";

    // Ap Rep Ap Register Order By

    public static final String AP_REGISTER_ORDER_BY_SUPPLIER_CODE = "SUPPLIER CODE";
    public static final String AP_REGISTER_ORDER_BY_SUPPLIER_TYPE = "SUPPLIER TYPE";
    public static final String AP_REGISTER_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String AP_REGISTER_ORDER_BY_DATE = "DATE";

    // Ap Tax Code Types

    public static final String AP_TAX_CODE_TYPE_NONE = "NONE";
    public static final String AP_TAX_CODE_TYPE_EXEMPT = "EXEMPT";
    public static final String AP_TAX_CODE_TYPE_INCLUSIVE = "INCLUSIVE";
    public static final String AP_TAX_CODE_TYPE_EXCLUSIVE = "EXCLUSIVE";
    public static final String AP_TAX_CODE_TYPE_ZERO_RATED = "ZERO-RATED";

    // Ap Supplier List Order By

    public static final String AP_SL_ORDER_BY_SUPPLIER_CODE = "SUPPLIER CODE";
    public static final String AP_SL_ORDER_BY_SUPPLIER_CLASS = "SUPPLIER CLASS";
    public static final String AP_SL_ORDER_BY_SUPPLIER_TYPE = "SUPPLIER TYPE";

    // Ap Aging By

    public static final String AP_AGING_BY_VOUCHER_DATE = "VOUCHER DATE";
    public static final String AP_AGING_BY_DUE_DATE = "DUE DATE";

    // Ap Check Types

    public static final String AP_FR_CHECK_TYPE_PAYMENT = "PAYMENT";
    public static final String AP_FR_CHECK_TYPE_DIRECT = "DIRECT";

    // Ar Customer Entry Payment Methods

    public static final String AR_CE_PAYMENT_METHOD_CASH = "CASH";
    public static final String AR_CE_PAYMENT_METHOD_CHECK = "CHECK";
    public static final String AR_CE_PAYMENT_METHOD_CREDIT_CARD = "CREDIT CARD";
    public static final String AR_CE_PAYMENT_METHOD_BANK_TRANSFER= "BANK TRANSFER";
    public static final String AR_CE_PAYMENT_METHOD_DIRECT= "DIRECT DEPOSIT";
    public static final String AR_CE_PAYMENT_METHOD_ONLINE_BANK= "ONLINE BANKING PAYMENTS";
    public static final String AR_CE_PAYMENT_METHOD_OFFSETTING= "OFFSETTING";

    // Ar Find Customer Order By

    public static final String AR_FC_ORDER_BY_CUSTOMER_CODE= "CUSTOMER CODE";
    public static final String AR_FC_ORDER_BY_CUSTOMER_NAME= "CUSTOMER NAME";

    
    // Ar Find Personel Order By

    public static final String AR_FP_ORDER_BY_ID_NUMBER= "ID NUMBER";
    public static final String AR_FP_ORDER_BY_PERSONEL_NAME= "PERSONEL NAME";

    // Ar Find Invoice Order By

    public static final String AR_FI_ORDER_BY_CUSTOMER_CODE= "CUSTOMER CODE";
    public static final String AR_FI_ORDER_BY_INVOICE_NUMBER= "INVOICE NUMBER";

    // Ar Find Invoice Payment Status

    public static final String AR_FI_PAYMENT_STATUS_PAID = "PAID";
    public static final String AR_FI_PAYMENT_STATUS_UNPAID = "UNPAID";

    // Ar Find Invoice Type
    public static final String AR_FI_ITEM = "ITEMS";
    public static final String AR_FI_MEMO_LINES = "MEMO LINES";

    // Ar Receipt Types

    public static final String AR_FR_RECEIPT_TYPE_COLLECTION = "COLLECTION";
    public static final String AR_FR_RECEIPT_TYPE_MISC = "MISC";

    // Ar Find Receipt Order By

    public static final String AR_FR_ORDER_BY_BANK_ACCOUNT= "BANK ACCOUNT";
    public static final String AR_FR_ORDER_BY_CUSTOMER_CODE= "CUSTOMER CODE";
    public static final String AR_FR_ORDER_BY_RECEIPT_NUMBER= "RECEIPT NUMBER";

    // Ar Tax Code Types

    public static final String AR_TAX_CODE_TYPE_NONE = "NONE";
    public static final String AR_TAX_CODE_TYPE_EXEMPT = "EXEMPT";
    public static final String AR_TAX_CODE_TYPE_INCLUSIVE = "INCLUSIVE";
    public static final String AR_TAX_CODE_TYPE_EXCLUSIVE = "EXCLUSIVE";
    public static final String AR_TAX_CODE_TYPE_ZERO_RATED = "ZERO-RATED";

    // Ar Standard Memo Line Types

    public static final String AR_STANDARD_MEMO_LINE_TYPE_LINE = "LINE";
    public static final String AR_STANDARD_MEMO_LINE_TYPE_TAX = "TAX";
    public static final String AR_STANDARD_MEMO_LINE_TYPE_FREIGHT = "FREIGHT";
    public static final String AR_STANDARD_MEMO_LINE_TYPE_SERVICE_CHARGE = "SC";

    // Ar Misc Receipt Entry Payment Methods

    public static final String AR_MR_PAYMENT_METHOD_CASH = "CASH";
    public static final String AR_MR_PAYMENT_METHOD_CHECK = "CHECK";
    public static final String AR_MR_PAYMENT_METHOD_CREDIT_CARD = "CREDIT CARD";

    // Ar Aging By

    public static final String AR_AGING_BY_INVOICE_DATE = "INVOICE DATE";
    public static final String AR_AGING_BY_DUE_DATE = "DUE DATE";
    public static final String AR_AGING_BY_EFFECTIVITY_DAYS = "EFFECTIVITY DAYS";
    public static final String AR_AGING_BY_RECEIVED_DATE = "RECEIVED DATE";
    // Ar Sales Register Order By

    public static final String AR_SALES_REGISTER_ORDER_BY_NONE= "";
    public static final String AR_SALES_REGISTER_ORDER_BY_CUSTOMER_CODE= "CUSTOMER CODE";
    public static final String AR_SALES_REGISTER_ORDER_BY_CUSTOMER_NAME= "CUSTOMER NAME";
    public static final String AR_SALES_REGISTER_ORDER_BY_CUSTOMER_TYPE= "CUSTOMER TYPE";
    public static final String AR_SALES_REGISTER_ORDER_BY_INVOICE_NUMBER= "INVOICE NUMBER";
    public static final String AR_SALES_REGISTER_ORDER_BY_SALESPERSON= "SALESPERSON";
    public static final String AR_SALES_REGISTER_ORDER_BY_DATE= "DATE";

    // Ar Or Register Order By

    public static final String AR_OR_REGISTER_ORDER_BY_NONE= "";
    public static final String AR_OR_REGISTER_ORDER_BY_BANK_ACCOUNT= "BANK ACCOUNT";
    public static final String AR_OR_REGISTER_ORDER_BY_CUSTOMER_CODE= "CUSTOMER CODE";
    public static final String AR_OR_REGISTER_ORDER_BY_CUSTOMER_TYPE= "CUSTOMER TYPE";
    public static final String AR_OR_REGISTER_ORDER_BY_RECEIPT_NUMBER= "RECEIPT NUMBER";
    public static final String AR_OR_REGISTER_ORDER_BY_DATE= "DATE";

    // Ar Customer List Order By

    public static final String AR_CL_ORDER_BY_CUSTOMER_CODE = "CUSTOMER CODE";
    public static final String AR_CL_ORDER_BY_CUSTOMER_NAME = "CUSTOMER NAME";
    public static final String AR_CL_ORDER_BY_CUSTOMER_CLASS = "CUSTOMER CLASS";
    public static final String AR_CL_ORDER_BY_CUSTOMER_TYPE = "CUSTOMER TYPE";
    public static final String AR_CL_ORDER_BY_CUSTOMER_REGION = "CUSTOMER REGION";
    public static final String AR_CL_ORDER_BY_MEMO_LINE = "MEMO LINE";
    public static final String AR_CL_ORDER_BY_SALESPERSON = "SALESPERSON";
    public static final String AR_CL_ORDER_BY_PRODUCT = "PRODUCT";
    public static final String AR_CL_ORDER_BY_SO_NUMBER = "SO NUMBER";
    public static final String AR_CL_ORDER_BY_JO_NUMBER = "JO NUMBER";

    // Ar Customer List Group By

    public static final String AR_CL_GROUP_BY_CUSTOMER_CODE = "CUSTOMER CODE";
    public static final String AR_CL_GROUP_BY_SALESPERSON_CODE = "SALESPERSON";

    // Ar Post Dated Check

    public static final String AR_PDCI_ORDER_BY_CUSTOMER = "CUSTOMER";
    public static final String AR_PDCI_ORDER_BY_MATURITY_DATE = "MATURITY DATE";

    // Ad Amount Limit And/Or

    public static final String AD_CAL_AND = "AND";
    public static final String AD_CAL_OR = "OR";

    // Ad Approval User Type

    public static final String AD_AU_APPROVER = "APPROVER";
    public static final String AD_AU_REQUESTER = "REQUESTER";

    // Ad Approval User Level

    public static final String AD_AU_LEVEL1 = "LEVEL 1";
    public static final String AD_AU_LEVEL2 = "LEVEL 2";
    public static final String AD_AU_LEVEL3 = "LEVEL 3";
    public static final String AD_AU_LEVEL4 = "LEVEL 4";
    public static final String AD_AU_LEVEL5 = "LEVEL 5";

    // Ar Output Tax Order By

    public static final String AR_OT_ORDER_BY_CUSTOMER_CODE = "CUSTOMER CODE";
    public static final String AR_OT_ORDER_BY_INVOICE_NUMBER = "INVOICE NUMBER";
    public static final String AR_OT_ORDER_BY_CUSTOMER_CLASS = "CUSTOMER CLASS";
    public static final String AR_OT_ORDER_BY_CUSTOMER_TYPE = "CUSTOMER TYPE";

    // Gl Find Journal Interface Order By

    public static final String GL_FJI_ORDER_BY_NAME = "NAME";
    public static final String GL_FJI_ORDER_BY_DATE = "DATE";

    // Ap Rep Check Register Order By

    public static final String AP_CHECK_REGISTER_ORDER_BY_BANK_ACCOUNT = "BANK ACCOUNT";
    public static final String AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_CODE = "SUPPLIER CODE";
    public static final String AP_CHECK_REGISTER_ORDER_BY_SUPPLIER_TYPE = "SUPPLIER TYPE";
    public static final String AP_CHECK_REGISTER_ORDER_BY_CHECK_NUMBER = "CHECK NUMBER";
    public static final String AP_CHECK_REGISTER_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String AP_CHECK_REGISTER_ORDER_BY_DATE = "DATE";

    // Ap Rep Receiving Items Order By

    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_DATE = "DATE";
    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_LOCATION = "LOCATION";
    public static final String AP_REP_RECEIVING_ITEMS_ORDER_BY_PO_NUMBER = "PO NUMBER";

    // Ap Rep Purchase Order Order By

    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_DATE = "DATE";
    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_LOCATION = "LOCATION";
    public static final String AP_REP_PURCHASE_ORDER_ORDER_BY_PO_NUMBER = "PO NUMBER";



    // Inv Rep Reorder Items Order By

    public static final String INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String INV_REP_REORDER_ITEMS_ORDER_BY_ITEM_CLASS = "ITEM CLASS";

    // Inv Rep Stock On Hand Item Class

    public static final String INV_REP_REORDER_ITEMS_ITEM_CLASS_STOCK = "STOCK";
    public static final String INV_REP_REORDER_ITEMS_ITEM_CLASS_ASSEMBLY = "ASSEMBLY";

    // Inv Rep Stock On Hand Item Class

    public static final String INV_REP_STOCK_ON_HAND_ITEM_CLASS_STOCK = "Stock";
    public static final String INV_REP_STOCK_ON_HAND_ITEM_CLASS_ASSEMBLY = "Assembly";

    // Inv Rep Stock On Hand Order By

    public static final String INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_CLASS = "ITEM CLASS";
    public static final String INV_REP_STOCK_ON_HAND_ORDER_BY_ITEM_CATEGORY = "ITEM CATEGORY";

    // Inv Rep Stock Transfer Order By

    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_DATE = "DATE";
    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_DOCUMENT_NUMBER = "DOCUMENT NUMBER";
    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_LOCATION_TO = "LOCATION TO";
    public static final String INV_REP_STOCK_TRANSFER_ORDER_BY_LOCATION_FROM = "LOCATION FROM";

    // Inv Rep Stock On Hand Item Class

    public static final String INV_REP_STOCK_TRANSFER_ITEM_CLASS_STOCK = "STOCK";
    public static final String INV_REP_STOCK_TRANSFER_ITEM_CLASS_ASSEMBLY = "ASSEMBLY";

    // Cm Fund Transfer Type

    public static final String CM_FUND_TRANSFER_TYPE_TRANSFER = "TRANSFER";
    public static final String CM_FUND_TRANSFER_TYPE_DEPOSIT = "DEPOSIT";

    // Ar Rep Salesperson Order By

   public static final String AR_REP_SALESPERSON_ORDER_BY = "SALESPERSON CODE";

    // Ap Generate Purchase Requisition Item Class

    public static final String AP_GENERATE_PURCHASE_REQUISITION_ITEM_CLASS_STOCK = "STOCK";
    public static final String AP_GENERATE_PURCHASE_REQUISITION_ITEM_CLASS_ASSEMBLY = "ASSEMBLY";

    //  Ap Generate Purchase Requisition Order By

    public static final String AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_NAME = "ITEM NAME";
    public static final String AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_DESCRIPTION = "ITEM DESCRIPTION";
    public static final String AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_CATEGORY = "ITEM CATEGORY";
    public static final String AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_CLASS = "ITEM CLASS";

    //  Ap Rep Receiving Item Group By

   public static final String AP_RI_GROUP_BY_SUPPLIER_CODE = "SUPPLIER CODE";
   public static final String AP_RI_GROUP_BY_ITEM_NAME = "ITEM NAME";
   public static final String AP_RI_GROUP_BY_DATE = "DATE";

   // Inv Rep Adjustment Register Order By

   public static final String INV_REP_ADJUSTMENT_REGISTER_ORDER_BY_DATE = "DATE";
   public static final String INV_REP_ADJUSTMENT_REGISTER_ORDER_BY_DOC_NUM = "DOCUMENT NUMBER";

   // Inv Rep Purchase Requisition Register Order By

   public static final String AP_REP_PURCHASE_REQUISITION_REGISTER_ORDER_BY_DATE = "DATE";
   public static final String AP_REP_PURCHASE_REQUISITION_REGISTER_ORDER_BY_DOC_NUM = "DOCUMENT NUMBER";

   // Inv Rep Purchase Requisition Group By

   public static final String AP_REP_PURCHASE_REQUISITION_REGISTER_GROUP_BY_ITEM_NAME = "ITEM NAME";
   public static final String AP_REP_PURCHASE_REQUISITION_REGISTER_GROUP_BY_DATE = "DATE";


   // Inv Rep Adjustment Register Group By

   public static final String INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_ITEM_NAME = "ITEM NAME";
   public static final String INV_REP_ADJUSTMENT_REGISTER_GROUP_BY_DATE = "DATE";

   // Ar Rep Delivery Receipt Order By

   public static final String AR_REP_DELIVERY_RECEIPT_ORDER_BY_DATE = "DATE";
   public static final String AR_REP_DELIVERY_RECEIPT_ORDER_BY_DOC_NUM = "DOCUMENT NUMBER";

   // Ar Rep Delivery Receipt Group By

   public static final String AR_REP_DELIVERY_RECEIPT_GROUP_BY_ITEM_NAME = "ITEM NAME";
   public static final String AR_REP_DELIVERY_RECEIPT_GROUP_BY_CUSTOMER = "CUSTOMER";


}

