package com.struts.util;

import java.io.Serializable;

public final class FormFunctionRes implements Serializable{

   private int frCode = 0;
   private int frRsCode = 0;
   private int frFfCode = 0;
   private String frParam = null;

   public int getFrCode(){
      return(frCode);
   }

   public void setFrCode(int frCode){
      this.frCode = frCode;
   }

   public int getFrRsCode(){
      return(frRsCode);
   }

   public void setFrRsCode(int frRsCode){
      this.frRsCode = frRsCode;
   }

   public int getFrFfCode(){
      return(frFfCode);
   }

   public void setFrFfCode(int frFfCode){
      this.frFfCode = frFfCode;
   }

   public String getFrParam(){
      return(frParam);
   }

   public void setFrParam(String frParam){
      this.frParam = frParam;
   }

   
   
}
