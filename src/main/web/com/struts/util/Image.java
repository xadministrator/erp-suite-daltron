package com.struts.util;

public class Image implements java.io.Serializable{
   
   private byte[] bytes;	
   private String imageType = null;
   	
   public Image(byte[] bytes, String imageType) {
   	
       this.bytes = bytes;
       this.imageType = imageType;
   	
   }    

   public byte[] getBytes(){
   	
      return(bytes);
      
   }
   
   public String getImageType() {
   	
   	  return imageType;   	
   	  
   }
      
}
