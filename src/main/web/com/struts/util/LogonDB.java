package com.struts.util;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.ejb.EJBException;
import javax.sql.DataSource;

import sun.misc.BASE64Encoder;
//import java.util.Base64;
import com.util.Debug;

public class LogonDB{
      
 private int defaultResCode = 0; 
 public int validateUser(DataSource dataSource, String userName, String password, String company) throws SQLException, Exception{

  Connection conn = null;
  PreparedStatement stmt =  null;
  ResultSet rs = null;
  int cmpCode = 0;
  
  try{
      System.out.println("ds: " + dataSource.toString());
  
  
  
     conn = dataSource.getConnection();
     
     
     
     stmt = conn.prepareStatement("SELECT CMP_CODE FROM AD_CMPNY WHERE CMP_SHRT_NM=?");
     
     stmt.setString(1, company);
     
     rs = stmt.executeQuery();
     
     while(rs.next()) {
     	cmpCode = rs.getInt(1);
     }
     
	 if(cmpCode == 0)
	   return (Constants.ADMIN_INVALID_LOGON);
     
     stmt = conn.prepareStatement("SELECT USR_CODE, USR_NM, USR_PSSWRD, USR_PSSWRD_EXPRTN_CODE, " +
 		"USR_PSSWRD_EXPRTN_DYS, USR_PSSWRD_EXPRTN_ACCSS, USR_PSSWRD_CURR_ACCSS, " +
		"USR_DT_FRM, USR_DT_TO FROM AD_USR WHERE USR_NM = ? AND USR_PSSWRD = ? AND USR_AD_CMPNY = ?");
	
	
	 userName = userName.trim();
	 password = password.trim();
	 	 
	 stmt.setString(1, userName);
	 stmt.setString(2, this.encryptPassword(password));
	 stmt.setInt(3, cmpCode);
     
	 rs = stmt.executeQuery();

	 int userCode = 0;
	 String sUserName = null;
	 String sPassword = null;
	 int userPasswordExpirationCode = 0;
	 int userPasswordExpirationDays = 0;
	 int userPasswordExpirationAccess = 0;;
	 int userPasswordCurrentAccess = 0;
	 Date userDateFrom = null;
	 Date userDateTo = null;

	 while(rs.next()){
	    userCode = rs.getInt(1);
	    sUserName = rs.getString(2);
	    sPassword = rs.getString(3);
	    userPasswordExpirationCode = rs.getInt(4);
	    userPasswordExpirationDays = rs.getInt(5);
	    userPasswordExpirationAccess = rs.getInt(6);
	    userPasswordCurrentAccess = rs.getInt(7);
	    userDateFrom = rs.getDate(8);
	    userDateTo = rs.getDate(9);
	 }
	 
	 //Check if user and password exist.
	 if(userCode == 0 || !sUserName.equals(userName) || !sPassword.equals(this.encryptPassword(password)))
	   return (Constants.ADMIN_INVALID_LOGON);
         
	 //Check if user and password is enabled.
 	 GregorianCalendar gcCurrDate = new GregorianCalendar();
	 GregorianCalendar gcUserDateFrom = new GregorianCalendar();
	 GregorianCalendar gcUserDateTo = new GregorianCalendar();
	 
	 gcUserDateFrom.setTime(userDateFrom);
	 if (userDateTo != null)
 	    gcUserDateTo.setTime(userDateTo);
	 if(gcCurrDate.getTime().getTime() < gcUserDateFrom.getTime().getTime() ||
	    gcCurrDate.getTime().getTime() > gcUserDateTo.getTime().getTime() && userDateTo != null)
	    return(Constants.ADMIN_USER_DISABLED);
	 
	 
	 //Check if user does not exceed the password expiration access.
         if(userPasswordExpirationCode == 1){
	    if(userPasswordCurrentAccess >= userPasswordExpirationAccess)
	       return(Constants.ADMIN_EXPIRED_ACCESS);
	 }
	 //Check if user does not exceed the password expiration days.
	 else if(userPasswordExpirationCode == 0){
	    gcUserDateFrom.setTime(userDateFrom);
	    double daysAcquired = (double)(gcCurrDate.getTime().getTime() - gcUserDateFrom.getTime().getTime()) /
	        Constants.MILLIS_PER_DAY;
	    if(daysAcquired > userPasswordExpirationDays)
	       return(Constants.ADMIN_EXPIRED_DAYS);
             
	 }
         
	 stmt = conn.prepareStatement("SELECT a.UR_CODE, a.UR_RS_CODE, a.UR_DT_FRM, a.UR_DT_TO, b.RS_DT_FRM, b.RS_DT_TO, " +
				 "b.RS_CODE FROM AD_USR_RSPNSBLTY a, AD_RSPNSBLTY b " +
	 			"WHERE a.UR_USR_CODE = ? AND b.RS_CODE = a.UR_RS_CODE AND a.UR_AD_CMPNY = ?");

	 stmt.setInt(1, userCode);
	 stmt.setInt(2, cmpCode);

	 rs = stmt.executeQuery();

	 int urCode = 0;
	 int rsCode = 0;
	 Date urDateFrom = null;
	 Date urDateTo = null;
	 Date rsDateFrom = null;
	 Date rsDateTo = null;
	 boolean urCodeFound = false;
	 boolean urEnabledDateFound = false;
	 boolean rsEnabledDateFound = false;
	 GregorianCalendar gcURDateFrom = new GregorianCalendar();
	 GregorianCalendar gcURDateTo = new GregorianCalendar();
	 GregorianCalendar gcRSDateFrom = new GregorianCalendar();
	 GregorianCalendar gcRSDateTo = new GregorianCalendar();
	
	 while(rs.next()){
	    urCode = rs.getInt(1);
	    urDateFrom = rs.getDate(3);
	    urDateTo = rs.getDate(4);
	    rsDateFrom = rs.getDate(5);
	    rsDateTo = rs.getDate(6);
	    rsCode = rs.getInt(7);
	    
	    if (this.defaultResCode == 0) this.defaultResCode = rsCode;

	    // Find assigned responsibility
	    if(urCode != 0  && urCodeFound == false)
	       	urCodeFound = true;

	    //  Find enabled responsibility   
	    if (urDateFrom != null){
  	      gcURDateFrom.setTime(urDateFrom);
	      if(urDateTo != null)
	         gcURDateTo.setTime(urDateTo);
	      if(gcCurrDate.getTime().getTime() >= gcURDateFrom.getTime().getTime()){
	         if(urDateTo != null){
	            if(gcCurrDate.getTime().getTime() <= gcURDateTo.getTime().getTime())
		       urEnabledDateFound = true;
		 }   
	         else 
	            urEnabledDateFound = true;
	      }
	    } 
	   
	    // Find enabled assigned responsibility
	    if(rsDateFrom != null){
	    	gcRSDateFrom.setTime(rsDateFrom);
	    	if(rsDateTo != null){
	    		gcRSDateTo.setTime(rsDateTo);
	    	}
	    	if(gcCurrDate.getTime().getTime() >= gcRSDateFrom.getTime().getTime()){
	    		if(rsDateTo != null){
	    			if(gcCurrDate.getTime().getTime() <= gcRSDateTo.getTime().getTime()){
	    				rsEnabledDateFound = true;	    				
	    			}
	    		}
	    		else
	    			rsEnabledDateFound = true;
	    	}
	    }
	  }
	 
	 // Check if user has an assigned responsibility
	 if(urCodeFound == false)
	    return(Constants.ADMIN_NO_RESPONSIBILITY);
	    
	 // Check if user has an enabled responsibility
	 else if(urEnabledDateFound == false)
	    return(Constants.ADMIN_USER_RES_DISABLED);
	    
	 // Check if user's assigned responsibility is enabled
	 else if(rsEnabledDateFound == false)
	    return(Constants.ADMIN_USER_ASSIGNED_RES_DISABLED);
	  
	
	 // Increment user current access column in user table
	 if(userPasswordExpirationCode == 1){
	    stmt = conn.prepareStatement("UPDATE AD_USR SET USR_PSSWRD_CURR_ACCSS = ? WHERE USR_CODE = ? AND USR_AD_CMPNY = ?");
	    stmt.setInt(1, userPasswordCurrentAccess + 1);
	    stmt.setInt(2, userCode);
	    stmt.setInt(3, cmpCode);
	    int rowsUpdated = stmt.executeUpdate();
	    conn.commit();
	  }

	 // No errors: return successful
	 return(Constants.ADMIN_SUCCESS);
      }catch(SQLException sqle){
          throw sqle;
       }catch(Exception e){
          throw e;
       }
       finally{
            try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
            rs = null;													    
            try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
            stmt = null;   										                                  
            try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }	
            conn = null;
	}
   } 

   public int getDefaultResCode(){
      return(defaultResCode);
   }
   
   private String encryptPassword(String password) {
   	
   	String encPassword = new String();
   	
   	try {
   		
   		String keyString = "Some things are better left unread.";
   		StringReader key = new StringReader(keyString);
   		key.mark(keyString.length() + 1);
   		
   		StringReader reader = new StringReader(password);
   		byte data[] = new byte[password.getBytes().length];
   		
   		int r, k;
   		int ctr = 0;
   		while ((r = reader.read()) != -1) {
   			
   			if ((k = key.read()) == -1 ) {
   				
   				key.reset();
   				k = key.read();
   				
   			}	        	
   			
   			data[ctr] = (byte)(r ^ k);
   			ctr++;
   		}
   		
   		BASE64Encoder encoder = new BASE64Encoder();
   		
   		
   		encPassword = encoder.encode(data);
   		
   		//new encoder for java 8
   		
   	//	encPassword = Base64.getEncoder().encodeToString(data);
   		
   	} catch (Exception ex) {	
   		
   		Debug.printStackTrace(ex);
   		throw new EJBException(ex.getMessage());
   		
   	}
   	
   	return encPassword;
   	
   }

}
