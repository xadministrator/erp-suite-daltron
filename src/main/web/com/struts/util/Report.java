package com.struts.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sf.jasperreports.engine.JasperPrint;

public class Report implements java.io.Serializable{
   
   public Report() { ; }
   
   private byte[] bytes;
   private String viewType;
   private JasperPrint jasperPrint;

   public byte[] getBytes(){
      return(bytes);
   }

   public void setBytes(byte[] bytes){
      this.bytes = bytes;
   }
   
   public String getViewType() {
      return(viewType);   	     	     	
   }
   
   public void setViewType(String viewType) {
   	  this.viewType = viewType;   	
   }
   
   public JasperPrint getJasperPrint() {
   	  return(jasperPrint);
   }
   
   public void setJasperPrint(JasperPrint jasperPrint){
   	  this.jasperPrint = jasperPrint;   	
   }
   
   public void setFileToByte(File file) throws FileNotFoundException{
	   FileInputStream fis = new FileInputStream(file);
       //System.out.println(file.exists() + "!!");
       //InputStream in = resource.openStream();
       ByteArrayOutputStream bos = new ByteArrayOutputStream();
       byte[] buf = new byte[1024];
       try {
           for (int readNum; (readNum = fis.read(buf)) != -1;) {
               bos.write(buf, 0, readNum); //no doubt here is 0
               //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
               System.out.println("read " + readNum + " bytes,");
           }
       } catch (IOException ex) {
          System.out.println(ex.toString());
       }
       byte[] bytes = bos.toByteArray();
       
       setBytes(bytes);
   }
   
}
