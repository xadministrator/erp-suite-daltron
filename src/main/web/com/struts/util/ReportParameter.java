package com.struts.util;

import java.io.Serializable;

public class ReportParameter  implements Serializable {

	private String parameterName = null;
	private String parameterValue = null;

	public ReportParameter() {


	}

	public ReportParameter(String parameterName, String parameterValue) {
		super();
		this.parameterName = parameterName;
		this.parameterValue = parameterValue;
	}

	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public String getParameterValue() {
		return parameterValue;
	}
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}



}
