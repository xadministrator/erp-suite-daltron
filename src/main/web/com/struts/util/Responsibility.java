package com.struts.util;

import java.io.Serializable;
import java.util.Date;

public class Responsibility implements Serializable{
   private int rsCode = 0;
   private String rsName = null;
   private Date rsDateFrom = null;
   private Date rsDateTo = null;

   public int getRSCode(){
      return(rsCode);
   }

   public void setRSCode(int rsCode){
      this.rsCode = rsCode;
   }

   public String getRSName(){
      return(rsName);
   }

   public void setRSName(String rsName){
      this.rsName = rsName;
   }

   public Date getRSDateFrom(){
      return(rsDateFrom);
   }

   public void setRSDateFrom(Date rsDateFrom){
      this.rsDateFrom = rsDateFrom;
   }

   public Date getRSDateTo(){
      return(rsDateTo);
   }

   public void setRSDateTo(Date rsDateTo){
      this.rsDateTo = rsDateTo;
   }
}
