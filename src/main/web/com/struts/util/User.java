package com.struts.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public final class User implements Serializable {
   private int userCode = 0;
   private String userName = null;
   private String userDescription = null;
   private String userPassword = null;
   private String userDepartment = null;
   private String loginTime = null;
   private int currResCode = 0;
   private int currAppCode = 0;
   private Branch currBrnch = null;
   private Integer cmpCode = null;
   private String company = null;
   private String companyName = null;
   private String welcomeNote = null;

   private ArrayList userRes = null;
   private ArrayList res = null;
   private ArrayList userApps = null;
   private ArrayList branches = null;
   private HashMap ffRes = null;
   private String returnLink = null;
   private String url = null;
   
   
   public User(){
      userRes = new ArrayList();
      res = new ArrayList();
      branches = new ArrayList();
      ffRes = new HashMap();
   }

   public String salesOrderURL(){
	   
	   Calendar currentDate = Calendar.getInstance();
       SimpleDateFormat formatter=  new SimpleDateFormat("MM/dd/yyyy");
       String dateNow = formatter.format(currentDate.getTime());
	   url = "/arFindSalesOrder.do?goButton=1&approvalStatus=APPROVED&posted=&approvalDateFrom=" + dateNow + "&approvalDateTo=" + dateNow + "&orderBy=DOCUMENT NUMBER";      
       
       return(url);  
	          
	   }
   
   public int getUserCode(){
      return(userCode);
   }

   public void setUserCode(int userCode){
      this.userCode = userCode;
   }

   public String getUserName(){
      return(userName);
   }

   public void setUserName(String userName){
      this.userName = userName;
   }
   
   public String getUserDepartment(){
      return(userDepartment);
   }

   public void setUserDepartment(String userDepartment){
      this.userDepartment = userDepartment;
   }

   public String getUserDescription(){
      return(userDescription);
   }

   public void setUserDescription(String userDescription){
      this.userDescription = userDescription;
   }

   public String getUserPassword(){
      return(userPassword);
   }

   public void setUserPassword(String userPassword){
      this.userPassword = userPassword;
   }
   
   public String getLoginTime(){
      return(loginTime);
   }

   public void setLoginTime(String loginTime){
      this.loginTime = loginTime;
   }

   public void setUserRes(Object userRes){
      this.userRes.add(userRes);
   }

   public int getUserResCount(){
     return(userRes.size());
   }

   public UserRes getUserRes(int index){
      return((UserRes)userRes.get(index));
   }
  
   public Responsibility getRes(int index){
      return((Responsibility)res.get(index));
   }
   
   public void setRes(Object res){
      this.res.add(res);
   }
   
   public int getResCount(){
      return(res.size());
   }

   public FormFunctionRes getFFResByKey(Object key){
      return((FormFunctionRes)ffRes.get(key));
   }

   public void setFFRes(Object key, Object ffRes){
      this.ffRes.put(key, ffRes);
   }

   public int getFFResCount(){
      return(ffRes.size());
   }
   

   public int getCurrentResCode(){ 
      return(currResCode);
   }
   
   public void setCurrentResCode(int currResCode){
      this.currResCode = currResCode;
   }
   
   public int getCurrentAppCode(){
   	  return(currAppCode);
   }
   
   public void setCurrentAppCode(int currAppCode){
   	  this.currAppCode = currAppCode;   	
   }
   
   public Integer getCmpCode(){
   	  return(cmpCode);
   }
   
   public void setCmpCode(Integer cmpCode){
   	  this.cmpCode = cmpCode;   	
   }
   
   public String getCompany(){
   	  return(company);
   }
   
   public void setCompany(String company){
   	  this.company = company;   	
   }
   
   public String getCompanyName(){
   	  return(companyName);
   }
   
   public void setCompanyName(String companyName){
   	  this.companyName = companyName;   	
   }
   
   public String getWelcomeNote(){
   	  return(welcomeNote);
   }
   
   public void setWelcomeNote(String welcomeNote){
   	  this.welcomeNote = welcomeNote;   	
   }
   
   public ArrayList getUserApps() {
   	  
   	  return userApps;
   	
   }
   
   public void setUserApps(ArrayList userApps) {
   	
   	  this.userApps = userApps;
   	
   }
   
   public void setBranch(Branch brnch) {
   	this.branches.add(brnch);
   }
   
   public Branch getBranch(int index){
    return((Branch)branches.get(index));
   }

   public void setBranches(ArrayList branches){
    this.branches = branches; 
   }
   
   public void clearBranches() {
   	this.branches.clear();
   }
   
   public int getBrnchCount(){
    return(branches.size());
   }
   
   public void setCurrentBranch(Branch brnch) {
   	this.currBrnch = brnch;
   }
   
   public Branch getCurrentBranch() {
   	return currBrnch;
   }
   
   public void setReturnLink(String returnLink) {
   		this.returnLink = returnLink;
   }
   
   public String getReturnLink() {
   		return returnLink;
   }
}
