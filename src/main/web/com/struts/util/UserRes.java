package com.struts.util;

import java.io.Serializable;
import java.util.Date;

public class UserRes implements Serializable{

   private int urCode = 0;
   private int urUserCode = 0;
   private int urResCode = 0;
   private Date urDateFrom = null;
   private Date urDateTo = null;

   
   public int getURCode(){
      return(urCode);
   }

   public void setURCode(int urCode){
      this.urCode = urCode;
   }
   
   public int getURUserCode(){
      return(urUserCode);
   }
   
   public void setURUserCode(int urUserCode){
      this.urUserCode = urUserCode;
   }

   public int getURResCode(){
      return(urResCode);
   }

   public void setURResCode(int urResCode){
      this.urResCode = urResCode;
   }

   public Date getURDateFrom(){
      return(urDateFrom);
   }

   public void setURDateFrom(Date urDateFrom){
     this.urDateFrom = urDateFrom;
   }

   public Date getURDateTo(){
      return(urDateTo);
   }

   public void setURDateTo(Date urDateTo){
      this.urDateTo = urDateTo;
   }
}
