<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="agingBucketValue.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<html:form action="/adAgingBucketValue.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="agingBucketValue.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adAgingBucketValueForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.agingBucketName"/>
               </td>
		       <td width="147" height="25" class="control">
                  <html:text property="agingBucketName" size="20" maxlength="25" styleClass="text" disabled="true"/>
		       </td>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.agingBucketDescription"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="agingBucketDescription" size="20" maxlength="25" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>            
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.agingBucketType"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="agingBucketType" size="20" maxlength="25" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.enable"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:checkbox property="enable" disabled="true"/>
		       </td>		 
            </tr>	        
	     	<tr>
	           <td width="575" height="50" colspan="4"> <p align="right">
		          <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          </logic:equal>
		          </logic:equal>
		       <td>
            </tr>	     
	        <tr>
               <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
               </td>
            </tr>
	        <tr>
	           <td width="575" height="5" colspan="4">
		    </td>
	        </tr>
 	        <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.sequenceNumber"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="sequenceNumber" styleClass="textRequired"/>
		       </td>		 
            </tr>	        
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.type"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:select property="type" styleClass="comboRequired">
				  <html:options property="typeList"/>
				  </html:select>
			   </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.daysFrom"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="daysFrom" styleClass="textRequired"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.daysTo"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="daysTo" styleClass="textRequired"/>
		       </td>		 
            </tr>  
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="agingBucketValue.prompt.description"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="description" styleClass="textRequired"/>
		       </td>		 
            </tr>                       
	        </logic:equal>
	        <tr>                	        
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	        	                             	         	        
	           <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="185" colspan="4">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="agingBucketValue.gridTitle.AVDetails"/>
	                 </td>
	              </tr>	              	              	              	              	              	              
	            <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="373" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="agingBucketValue.prompt.sequenceNumber"/>
			       </td>
			       <td width="521" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="agingBucketValue.prompt.description"/>
			       </td>
                </tr>			       	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adAVList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="373" height="1" class="gridLabel">
			            <nested:write property="sequenceNumber"/>
			         </td>
                     <td width="372" height="1" class="gridLabel">
                        <nested:write property="description"/>
                     </td>			         
			         <td width="149" align="center" height="1" colspan="2">
			         <div id="buttons">
			         <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      </nested:iterate>
			      </logic:equal>
			      <logic:equal name="adAgingBucketValueForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			      			      
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adAVList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="agingBucketValue.prompt.sequenceNumber"/>
			         </td>
			         <td width="570" height="1" class="gridLabel">
			            <nested:write property="sequenceNumber"/>
			         </td>
			         <td width="149" align="center" height="1" colspan="2">
			         <div id="buttons">
			         <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adAgingBucketValueForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adAgingBucketValueForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="agingBucketValue.prompt.type"/>
			         </td>
			         <td width="719" height="1" class="gridLabel" colspan="3">
			            <nested:write property="type"/>
			         </td>
			      </tr>
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="agingBucketValue.prompt.daysFrom"/>
                     </td>
                     <td width="719" height="1" class="gridLabel" colspan="3">
                        <nested:write property="daysFrom"/>
                     </td>
			      </tr>
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="agingBucketValue.prompt.daysTo"/>
                     </td>
                     <td width="719" height="1" class="gridLabel" colspan="3">
                        <nested:write property="daysTo"/>
                     </td>
			      </tr>	
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="agingBucketValue.prompt.description"/>
                     </td>
                     <td width="719" height="1" class="gridLabel" colspan="3">
                        <nested:write property="description"/>
                     </td>
			      </tr>			      		      
			      </nested:iterate>
			      </logic:equal>			      			      			      
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["sequenceNumber"] != null)
        document.forms[0].elements["sequenceNumber"].focus()
	       // -->
</script>
</body>
</html>
