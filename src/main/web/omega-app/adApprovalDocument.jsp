<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="approvalDocument.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('updateButton'));">
<html:form action="/adApprovalDocument.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="250">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
         <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="approvalDocument.title"/>
		    </td>
	     </tr>
          <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
			   <logic:equal name="adApprovalDocumentForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
	                <bean:message key="app.success"/>
	           </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
	     <tr>
	        <td class="prompt" width="125" height="25">
	           <bean:message key="approvalDocument.prompt.type"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
	           <html:text property="type" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.printOption"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:select property="printOption" styleClass="comboRequired">
	               <html:options property="printOptionList"/>
	           </html:select>
		    </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.allowDuplicate"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="allowDuplicate"/>
		    </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.trackDuplicate"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="trackDuplicate"/>
		    </td>
	     </tr>
	     <logic:equal name="adApprovalDocumentForm" property="showEnableCreditLimitChecking" value="true">
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.enableCreditLimitChecking"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="enableCreditLimitChecking"/>
		    </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>
	     </logic:equal>
	     <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	     <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
	     <tr>
	        <td class="prompt" width="125" height="25">
	           <bean:message key="approvalDocument.prompt.type"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
	           <html:text property="type" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.printOption"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:select property="printOption" styleClass="comboRequired" disabled="true">
	               <html:options property="printOptionList"/>
	           </html:select>
		    </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.allowDuplicate"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="allowDuplicate" disabled="true"/>
		    </td>
	     </tr>
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.trackDuplicate"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="trackDuplicate" disabled="true"/>
		    </td>
	     </tr>
	     <logic:equal name="adApprovalDocumentForm" property="showEnableCreditLimitChecking" value="true">
	     <tr>   
		    <td class="prompt" width="125" height="25">
               <bean:message key="approvalDocument.prompt.enableCreditLimitChecking"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:checkbox property="enableCreditLimitChecking" disabled="true"/>
		    </td>
	     </tr>
	     </logic:equal>
         </logic:equal>
         </logic:equal>
         <tr>                	        
	      <td width="160" height="50">
			<div id="buttons">
			  <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
				<html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					<bean:message key="button.showDetails"/>
				</html:submit>
			  </logic:equal>			     
			  <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
				<html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					<bean:message key="button.hideDetails"/>
				</html:submit>
			  </logic:equal>			     			     
			  </div>
			  <div id="buttonsDisabled" style="display: none;">
			  <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
				<html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			     	<bean:message key="button.showDetails"/>
				</html:submit>
			  </logic:equal>			     
			  <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
				<html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
				   <bean:message key="button.hideDetails"/>
				</html:submit>
		      </logic:equal>			     
			 </div>
	      </td>         
         <td width="415" height="50" colspan="3"> 
	      <div id="buttons">
	      <p align="right">
         <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_ADD%>">
		   <html:submit property="approvalSetup" styleClass="mainButtonBig">
		       <bean:message key="button.approvalSetup"/>
		   </html:submit>
		   </logic:equal>		   
		   <html:submit property="closeButton" styleClass="mainButton">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
           <div id="buttonsDisabled" style="display: none;">
	       <p align="right">
		   <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   <logic:equal name="adApprovalDocumentForm" property="pageState" value="<%=Constants.PAGE_STATE_ADD%>">
		   <html:submit property="approvalSetup" styleClass="mainButtonBig" disabled="true">
		       <bean:message key="button.approvalSetup"/>
		   </html:submit>
		   </logic:equal>
		   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
           </td>		   	     
	     <tr valign="top">
	       <td width="575" height="185" colspan="4">
		    <div align="center">
		    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			 bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
		     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                  <td width="575" height="1" colspan="6" class="gridTitle" 
			        bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	               <bean:message key="approvalDocument.gridTitle.ADCDetails"/>
	              </td>
	            </tr>

	            <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="234" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalDocument.prompt.type"/>
			       </td>
			       <td width="281" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalDocument.prompt.printOption"/>
			       </td>
			       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalDocument.prompt.allowDuplicate"/>
			       </td>
			       <td width="329" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalDocument.prompt.trackDuplicate"/>
			       </td>
                </tr>			       	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adADCList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="234" height="1" class="gridLabel">
			            <nested:write property="type"/>
			         </td>
                     <td width="281" height="1" class="gridLabel">
                        <nested:write property="printOption"/>
                     </td>
                     <td width="50" height="1" class="gridLabel">
                        <nested:write property="allowDuplicate"/>
                     </td>
                     <td width="50" height="1" class="gridLabel">
                        <nested:write property="trackDuplicate"/>
                     </td>		         
			         <td width="279" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit> 	                      
				     </logic:equal>
				     <nested:equal property="type" value="GL JOURNAL"> 
			            <nested:submit property="coaLimitButton" styleClass="gridButtonHuge">
	                       <bean:message key="button.coaLines"/>
  	                    </nested:submit> 	                    
  	                 </nested:equal>
  	                 <nested:notEqual property="type" value="GL JOURNAL">
			            <nested:submit property="amountLimitButton" styleClass="gridButtonHuge">
			               <bean:message key="button.amountLimits"/>
  	                    </nested:submit> 	                    
  	                 </nested:notEqual>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adApprovalDocumentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>  	                      
				     </logic:equal>
				     <nested:equal property="type" value="GL JOURNAL">
			            <nested:submit property="coaLimitButton" styleClass="gridButtonHuge" disabled="true">
	                       <bean:message key="button.coaLines"/>
  	                    </nested:submit> 	                    
  	                 </nested:equal>
  	                 <nested:notEqual property="type" value="GL JOURNAL">
			            <nested:submit property="amountLimitButton" styleClass="gridButtonHuge" disabled="true">
			               <bean:message key="button.amountLimits"/>
  	                    </nested:submit> 	                    
  	                 </nested:notEqual>
				     </div>
			         </td>
			      </tr>
			      </nested:iterate>
			      </logic:equal>	            
			    <logic:equal name="adApprovalDocumentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="adADCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			     <td width="175" height="1" class="gridHeader">
			       <bean:message key="approvalDocument.prompt.type"/>
			     </td>
			     <td width="570" height="1" class="gridLabel">
			       <nested:write property="type"/>
			     </td>
			     <td width="229" align="center" height="1" colspan="2">
			       <div id="buttons">
			        <logic:equal name="adApprovalDocumentForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			         <nested:submit property="editButton" styleClass="gridButton">
	                    <bean:message key="button.edit"/>
  	                 </nested:submit>    
				    </logic:equal>
					<nested:equal property="type" value="GL JOURNAL"> 
			            <nested:submit property="coaLimitButton" styleClass="gridButtonHuge">
	                       <bean:message key="button.coaLines"/>
  	                    </nested:submit> 	                    
  	                 </nested:equal>
  	                 <nested:notEqual property="type" value="GL JOURNAL">
			            <nested:submit property="amountLimitButton" styleClass="gridButtonHuge">
			               <bean:message key="button.amountLimits"/>
  	                    </nested:submit> 	                    
  	                 </nested:notEqual>
				   </div>
				   <div id="buttonsDisabled" style="display: none;">
			        <logic:equal name="adApprovalDocumentForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                  </nested:submit>
				    </logic:equal>
				      <nested:submit property="amountLimitButton" styleClass="gridButtonHuge" disabled="true">
	                       <bean:message key="button.amountLimits"/>
  	                  </nested:submit>    
				   </div>
			     </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="approvalDocument.prompt.printOption"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="printOption"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="approvalDocument.prompt.allowDuplicate"/>
			      </td>
			      <td width="272" height="1" class="gridLabel">
			         <nested:write property="allowDuplicate"/>
			      </td>
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="approvalDocument.prompt.trackDuplicate"/>
			      </td>
			      <td width="272" height="1" class="gridLabel">
			         <nested:write property="trackDuplicate"/>
			      </td>
			    </tr>
			    </nested:iterate>			    
			    </logic:equal>
			 </table>
		    </div>
		  </td>
	    </tr>
	     <tr>
	       <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   </td>
		 </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
</body>
</html>