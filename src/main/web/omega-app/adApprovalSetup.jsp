<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="approvalSetup.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adApprovalSetup.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="700">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
         <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="approvalSetup.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="adApprovalSetupForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="adApprovalSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <tr>
	        <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableGlJournal"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableGlJournal"/>
		    </td>
			<td class="prompt" width="220" height="25">
	           <bean:message key="approvalSetup.prompt.approvalQueueExpiration"/>
	        </td>
	        <td width="128" height="25" class="control">
	           <html:text property="approvalQueueExpiration" size="3" maxlength="3" styleClass="textRequired"/>
	        </td>
		</tr>
		
		 <tr>		    		 
		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableArSalesOrder"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableArSalesOrder"/>
		    </td>

	     </tr>
	     
	     
	      <tr>		    		 
		    
		      <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableArInvoice"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableArInvoice"/>
		    </td>
		    
		    
		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableArCreditMemo"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableArCreditMemo"/>
		    </td>
		    
	     </tr>
		 <tr>

		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableArReceipt"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableArReceipt"/>
		    </td>
		 </tr>  
		
		 <tr>
		 
		 </tr>
		 <tr>		    		 
			<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApPurchaseRequisition"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApPurchaseRequisition"/>
		    </td>
		    
		    
		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApCanvass"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApCanvass"/>
		    </td>
		  
		 </tr>
		 
		  <tr>		    		 
	        <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApPurchaseOrder"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApPurchaseOrder"/>
		    </td>
		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApReceivingItem"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableApReceivingItem"/>
		    </td>
	     </tr>
	     
	     
	     <tr>
	     
	       <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApVoucherDepartment"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApVoucherDepartment"/>
		    </td>
	     
	     </tr>
	     
	     <tr>

		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApVoucher"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApVoucher"/>
		    </td>
		    
		     <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApDebitMemo"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApDebitMemo"/>
		    </td>
		    
		    
		 </tr> 
		 
		 <tr>		    

		     <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableApCheck"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApCheck"/>
		    </td>
		    
		 </tr>
		     
		 
		 <tr>
			<td class="prompt" width="220" height="25">
		    	<bean:message key="approvalSetup.prompt.enableApCheckPaymentRequest"/>
		    </td>
		 	    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApCheckPaymentRequest"/>
		    </td>
		    
		    <td class="prompt" width="220" height="25">
		    	<bean:message key="approvalSetup.prompt.enableApCheckPaymentRequestDepartment"/>
		    </td>
		 	    <td width="128" height="25" class="control">
		       <html:checkbox property="enableApCheckPaymentRequestDepartment"/>
		    </td>
		</tr>  


	     
		 <tr>
		   <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableCmAdjustment"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableCmAdjustment"/>
		    </td>		    
		 	    
	     </tr>
		 <tr>		    		 
	        <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableCmFundTransfer"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableCmFundTransfer"/>
		    </td>
			<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvStockTransfer"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableInvStockTransfer"/>
		    </td>
		 </tr>  
		 
		 <tr>
		 
		   <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvAdjustment"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableInvAdjustment"/>
		    </td>	
	
		 	<td class="prompt" width="220" height="25">
	       <bean:message key="approvalSetup.prompt.enableInvAdjustmentRequest"/>
		   	</td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableInvAdjustmentRequest"/>
		    </td> 
		    
		</tr> 
		
		<tr>		    		 
			<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvBuildUnbuildAssembly"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableInvBuildUnbuildAssembly"/>
		    </td>
	
			
			<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvBuildUnbuildAssemblyOrder"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableInvBuildUnbuildAssembly"/>
		    </td>
		 </tr>
		 

	  	<tr>
	  	
	  		<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvBranchStockTransferOrder"/>
            </td>
		    <td width="128" height="25" class="control">
		       <html:checkbox property="enableInvBranchStockTransferOrder"/>
		    </td>
		    
		    <td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableInvBranchStockTransfer"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableInvBranchStockTransfer"/>
		    </td>
		    
		</tr> 
		
		<tr>
			
			<td class="prompt" width="220" height="25">
               <bean:message key="approvalSetup.prompt.enableArCustomer"/>
            </td>
		    <td width="127" height="25" class="control">
		       <html:checkbox property="enableArCustomer"/>
		    </td>
		
		</tr>
		
		    

         </logic:equal>
         
	        
	     <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
		     <logic:equal name="adApprovalSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton">
			       <bean:message key="button.save"/>
			  	</html:submit>
			  </logic:equal>
			    <html:submit property="approvalDocumentButton" styleClass="mainButtonBig">
			       <bean:message key="button.approvalDocument"/>
			    </html:submit>
			    <html:submit property="closeButton" styleClass="mainButton">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			 <logic:equal name="adApprovalSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.save"/>
			    </html:submit>
			 </logic:equal>
			    <html:submit property="approvalDocumentButton" styleClass="mainButtonBig" disabled="true">
			       <bean:message key="button.approvalDocument"/>
			    </html:submit>
			    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>		         
			 </td>
		 </tr>
	     <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["approvalQueueExpiration"] != null &&
        document.forms[0].elements["approvalQueueExpiration"].disabled == false)
        document.forms[0].elements["approvalQueueExpiration"].focus()
   // -->
</script>
</body>
</html>
