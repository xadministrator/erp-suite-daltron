<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="approvalUser.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function enableUserOr() {

	if(document.forms[0].andOr.value == "AND") {
	
		if (document.forms[0].type == "" || document.forms[0].type.value == "REQUESTER") {
			document.forms[0].or.disabled=true;
			document.forms[0].or.checked=false;
		} else {
			document.forms[0].or.disabled=false;
		}
	
	} else {
	
		document.forms[0].or.disabled=true;
		document.forms[0].or.checked=false;
		
	}
	
	
	
}

//Done Hiding--> 
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adApprovalUser.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="approvalUser.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adApprovalUserForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.department"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="department" size="25" maxlength="200" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>
            
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.amountLimit"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="amountLimit" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
		       </td>		 
            </tr>            
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.andOr"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="andOr" size="25" maxlength="25" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>
	        <tr>                	               	       	      	        	                             	         	        
	           <td width="570" height="25" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          <html:submit property="amountLimitButton" styleClass="mainButtonBig">
		             <bean:message key="button.amountLimits"/>
		          </html:submit>
		       </logic:equal>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          <html:submit property="amountLimitButton" styleClass="mainButtonBig" disabled="true">
		             <bean:message key="button.amountLimits"/>
		          </html:submit>
		       </logic:equal>   
		       </div>
               </td>
	        </tr>            	        	     
	        <tr>
               <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
               </td>
            </tr>
	        <tr>
	           <td width="575" height="5" colspan="4">
		    </td>
	        </tr>
 	        <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.user"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:select property="user" styleClass="comboRequired">
				  <html:options property="userList"/>
				  </html:select>
			   </td>		 
            </tr>
            
            <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.level"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:select property="level" styleClass="comboRequired" onchange="enableUserOr()">
				  <html:options property="levelList"/>
				  </html:select>
			   </td>		 
            </tr>
            	        
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.type"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:select property="type" styleClass="comboRequired" onchange="enableUserOr()">
				  <html:options property="typeList"/>
				  </html:select>
			   </td>		 
            </tr>  
            
            <logic:equal name="adApprovalUserForm" property="andOr" value="AND">    
            <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.or"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:checkbox property="or"/>
			   </td>		 
            </tr>    
            </logic:equal>
            <logic:equal name="adApprovalUserForm" property="andOr" value="OR">    
            <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="approvalUser.prompt.or"/>
               </td>
			   <td width="435" height="25" class="control" colspan="3">
				  <html:checkbox property="or" disabled="true"/>
			   </td>		 
            </tr>    
            </logic:equal>
            
	        </logic:equal>
	        <tr>                	        
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	        	                             	         	        
	           <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="185" colspan="6">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="approvalUser.gridTitle.AUDetails"/>
	                 </td>
	              </tr>	              	              	              	              	              	              
	            <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="372" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalUser.prompt.user"/>
			       </td>
			       <td width="224" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalUser.prompt.level"/>
			       </td>
			       <td width="224" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalUser.prompt.type"/>
			       </td>
			       
			       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="approvalUser.prompt.or"/>
			       </td>
                </tr>			       	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adAUList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="372" height="1" class="gridLabel">
			            <nested:write property="user"/>
			         </td> 	
			         <td width="224" height="1" class="gridLabel">
                        <nested:write property="level"/>
                     </td>  
			         <td width="224" height="1" class="gridLabel">
                        <nested:write property="type"/>
                     </td>
                     <td width="149" height="1" class="gridLabel">
                        <nested:checkbox property="or" disabled="true"/>
                     </td>
			         <td width="149" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal> 
				     </logic:equal>  		  	                 	                                     
				     </div>				     
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>
				     </logic:equal>  				                        
					 </div>                      	                      
			         </td>
			      </tr>
			      </nested:iterate>
			      </logic:equal>
			      <logic:equal name="adApprovalUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			      			      
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adAUList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="approvalUser.prompt.user"/>
			         </td>
			         <td width="570" height="1" class="gridLabel">
			            <nested:write property="user"/>
			         </td>
			         <td width="149" align="center" height="1" colspan="2">
			         <div id="buttons">
			         <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>	                                        
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adApprovalUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adApprovalUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>   	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="approvalUser.prompt.type"/>
			         </td>
			         <td width="395" height="1" class="gridLabel">
			            <nested:write property="type"/>
			         </td>
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="approvalUser.prompt.type"/>
			         </td>
			         <td width="149" height="1" class="gridLabel" colspan="3">
			            <nested:write property="type"/>
			         </td>
			      </tr>
			      </nested:iterate>
			      </logic:equal>			      			      			      
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["type"] != null)
        document.forms[0].elements["type"].focus()
	       // -->
</script>
</body>
</html>
