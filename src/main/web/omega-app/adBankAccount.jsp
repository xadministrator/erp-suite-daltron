<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="bankAccount.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adBankAccount.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="210">
     <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
           <table border="0" cellpadding="0" cellspacing="0" width="585" height="210" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	      <tr>
	         <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    <bean:message key="bankAccount.title"/>
		 </td>
	      </tr>
              <tr>
	         <td width="575" height="44" colspan="4" class="statusBar">
		         <logic:equal name="adBankAccountForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                    <bean:message key="app.success"/>
                 </logic:equal>
		    <html:errors/>	
	         </td>
	      </tr>	      	      
	      <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	      <tr>
	         <td class="prompt" width="140" height="25">
                    <bean:message key="bankAccount.prompt.bankName"/>
             </td>
		     <td width="435" height="25" class="control" colspan="3">
                  <html:select property="bankName" styleClass="comboRequired">
		          <html:options property="bankNameList"/>
                    </html:select>
		     </td>		 
           </tr>
           <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.accountName"/>
	         </td>
	         <td width="147" height="25" class="control" >
	            <html:text property="accountName" styleClass="textRequired"/>	              
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.isCashAccount"/>
	         </td>
	         <td width="147" height="25" class="control" >
				<html:checkbox property="isCashAccount"/>	              	              
	         </td>
	      </tr>
           <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.accountNumber"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="accountNumber" styleClass="textRequired"/>	              
	         </td>           
	         <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.accountType"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="accountType" styleClass="textRequired"/>	              
	         </td>         
	       </tr>
           <tr>	       
              <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.description"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="description" styleClass="text"/>	              
	         </td>	       
	         <td class="prompt" width="140" height="25">
                 <bean:message key="bankAccount.prompt.accountUse"/>
             </td>
		     <td width="148" height="25" class="control">
                 <html:select property="accountUse" styleClass="comboRequired">
		         <html:options property="accountUseList"/>
                 </html:select>
		     </td>        
	       </tr>
	       <tr>	   
	         <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="comboRequired">	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>                  
             <td class="prompt" width="140" height="25">
	            <bean:message key="bankAccount.prompt.enable"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:checkbox property="enable"/>	              
	         </td>
	       </tr>
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="GL Accounts">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		           <tr> 
				      <td class="prompt" width="575" height="25" colspan="4">
		              </td>
		           </tr>
		           <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.cashAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="cashAccount" size="30" maxlength="255" styleClass="textRequired"/>	              
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('cashAccount','cashAccountDescription');"/>
			         </td>         
			       </tr>
			       <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.cashAccountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="cashAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>         
			       </tr>
		           <tr>		           
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.interestAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="interestAccount" size="30" maxlength="255" styleClass="textRequired"/>	       
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('interestAccount','interestAccountDescription');"/>       
			         </td>			         
			       </tr>
			       <tr>		           
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.interestAccountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="interestAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>			         
			       </tr>
		           <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.salesDiscount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="salesDiscountAccount" size="30" maxlength="255" styleClass="textRequired"/>	     
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('salesDiscountAccount','salesDiscountAccountDescription');"/>         
			         </td>	                  
			       </tr>
			       <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.salesDiscountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="salesDiscountAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>	                  
			       </tr>
			       <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.adjustmentAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="adjustmentAccount" size="30" maxlength="255" styleClass="textRequired"/>	  
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('adjustmentAccount','adjustmentAccountDescription');"/>            
			         </td>
			       </tr>
			       <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.adjustmentAccountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="adjustmentAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>
			       </tr>
			       <tr>
  			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.bankChargeAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="bankChargeAccount" size="30" maxlength="255" styleClass="textRequired"/>	     
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('bankChargeAccount','bankChargeAccountDescription');"/>         
			         </td>
			       </tr>
			       <tr>
  			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.bankChargeAccountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="bankChargeAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>
			       </tr>
			       
			       
			       <tr>
  			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.advanceAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="advanceAccount" size="30" maxlength="255" styleClass="textRequired"/>	     
			            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('advanceAccount','advanceAccountDescription');"/>         
			         </td>
			       </tr>
			       <tr>
  			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.advanceAccountDescription"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="advanceAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
			         </td>
			       </tr>
			    </table>
				</div>
				<div class="tabbertab" title="Balances">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				   <tr> 
				     <td class="prompt" width="575" height="25" colspan="4">
		             </td>
		           </tr>
		           <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="bankAccount.prompt.availableBalance"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="availableBalance" styleClass="textAmount" disabled="true"/>	              
			         </td>  	         
			       </tr>
			    </table>
				</div>
				<div class="tabbertab" title="Check">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
				   <tr> 
				      <td class="prompt" width="575" height="25" colspan="6">
		              </td>
		           </tr>
		           <tr>	                    
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.initialCheckNumber"/>
			          </td>
			          <td width="242" height="25" class="control" colspan="3">
			             <html:text property="initialCheckNumber" size="15" maxlength="25" styleClass="textRequired"/>	              
			          </td>  
			          
			       </tr>
			       <tr>
			          
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.fontStyle"/>
			          </td>
			          <td width="53" height="25" class="control" colspan="3">
			          	 <html:select property="fontStyle" styleClass="combo">
			          	 	<html:options property="fontStyleList"/>
			          	 </html:select>
			          </td>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.fontSize"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="fontSize" size="3" maxlength="2" styleClass="textRequired"/>	              
			          </td>       
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNumberShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="accountNumberShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNumberTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="accountNumberTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNumberLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="accountNumberLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNameShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="accountNameShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNameTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="accountNameTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.accountNameLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="accountNameLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.numberShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="numberShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.numberTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="numberTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.numberLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="numberLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.dateShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="dateShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.dateTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="dateTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.dateLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="dateLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.payeeShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="payeeShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.payeeTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="payeeTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.payeeLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="payeeLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.amountShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="amountShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.amountTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="amountTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.amountLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="amountLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.wordAmountShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="wordAmountShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.wordAmountTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="wordAmountTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.wordAmountLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="wordAmountLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.currencyShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="currencyShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.currencyTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="currencyTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.currencyLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="currencyLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.addressShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="addressShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.addressTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="addressTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.addressLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="addressLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.memoShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="memoShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.memoTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="memoTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.memoLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="memoLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.docNumberShow"/>
			          </td>
			          <td width="49" height="25" class="control">
			             <html:checkbox property="docNumberShow"/>	              
			          </td>  
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.docNumberTop"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="docNumberTop" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 
			          <td class="prompt" width="140" height="25">
			             <bean:message key="bankAccount.prompt.docNumberLeft"/>
			          </td>
			          <td width="53" height="25" class="control">
			             <html:text property="docNumberLeft" size="5" maxlength="5" styleClass="text"/>	              
			          </td> 			          
			       </tr>
			       <tr>
			           <td width="575" height="50" colspan="6"> 
			           <div id="buttons">
			               <p align="right">
			               <html:submit property="previewButton" styleClass="mainButton">
				              <bean:message key="button.preview"/>
					       </html:submit>
					   </div>
					   <div id="buttonsDisabled" style="display: none;">
			               <p align="right">
			               <html:submit property="previewButton" styleClass="mainButton" disabled="true">
				              <bean:message key="button.preview"/>
					       </html:submit>
					   </div>
			           </td>
			       </tr>			       
			    </table>
				</div>
				<div class="tabbertab" title="Branch Map">
		 		<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 			<tr> 
		 	  			<td class="prompt" width="575" height="25" colspan="4">
			  			</td>
		 			</tr>
		 			<nested:iterate property="adBBAList">
		 			<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="branchName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
		            
		            
		            
		             <tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="bankAccount.prompt.cashAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchCashAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchCashAccount','branchCashAccountDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="bankAccount.prompt.cashAccountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchCashAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>
					
					
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="bankAccount.prompt.interestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchInterestAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchInterestAccount','branchInterestAccountDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="bankAccount.prompt.interestAccountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchInterestAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>
					
					
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="bankAccount.prompt.salesDiscount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchSalesDiscountAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchSalesDiscountAccount','branchSalesDiscountAccountDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="bankAccount.prompt.salesDiscountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchSalesDiscountAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr> 
					
					
					
						<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="bankAccount.prompt.adjustmentAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchAdjustmentAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchAdjustmentAccount','branchAdjustmentAccountDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="bankAccount.prompt.adjustmentAccountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchAdjustmentAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>
					
					
					
					<tr>    
						<td></td>
						     
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="bankAccount.prompt.bankChargeAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchBankChargeAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchBankChargeAccount','branchBankChargeAccountDescription');"/>
						</td>
						

					</tr>	
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="bankAccount.prompt.bankChargeAccountDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchBankChargeAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>

		            
		            <tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="bankAccount.prompt.advanceAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchAdvanceAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchAdvanceAccount','branchAdvanceAccountDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="bankAccount.prompt.advanceAccountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchAdvanceAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>
		            
		            </nested:iterate>
		        </table>
				</div> 
				</div><script>tabberAutomatic(tabberOptions)</script>
		       </td>
		      </tr>			       
              </logic:equal>
              <tr>
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	                                                              
	         <td width="415" height="50" colspan="3"> 
	         <div id="buttons">
	         <p align="right">
		    <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
                <html:submit property="saveButton" styleClass="mainButton">
	                <bean:message key="button.save"/>
		        </html:submit>
		    </logic:equal>
		    <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
                <html:submit property="updateButton" styleClass="mainButton">
	                <bean:message key="button.update"/>
		        </html:submit>
		        <html:submit property="cancelButton" styleClass="mainButton">
	                <bean:message key="button.cancel"/>
		        </html:submit>
		     </logic:equal>
		    </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		    </div>
		    <div id="buttonsDisabled" style="display: none;">
	         <p align="right">
		    <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
                <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.save"/>
		        </html:submit>
		    </logic:equal>
		    <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
                <html:submit property="updateButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.update"/>
		        </html:submit>
		        <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.cancel"/>
		        </html:submit>
		     </logic:equal>
		    </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		    </div>
                 </td>
	      </tr>
	      <tr valign="top">
	         <td width="575" height="185" colspan="4">
		    <div align="center">
		       <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			  bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			  bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			  <tr>
                             <td width="575" height="1" colspan="6" class="gridTitle" 
			        bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                        <bean:message key="bankAccount.gridTitle.BADetails"/>
	                     </td>
	                  </tr>
	                  
	                  
	                  
	            <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="373" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="bankAccount.prompt.bankName"/>
			       </td>
			       <td width="521" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="bankAccount.prompt.description"/>
			       </td>
                </tr>			     	                  
			  <%
			     int i = 0;	
			     String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
	                     String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
		             String rowBgc = null;
	                    %>
	                  <nested:iterate property="adBAList">
			   <%
			     i++;
			     if((i % 2) != 0){
			         rowBgc = ROW_BGC1;
		             }else{
			         rowBgc = ROW_BGC2;
		             }  
			    %>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="373" height="1" class="gridLabel">
			           <nested:write property="bankName"/>
			        </td>
			        <td width="372" height="1" class="gridLabel">
			           <nested:write property="description"/>
			        </td>			        
			        <td width="149" align="center" height="1" colspan="2">
			        <div id="buttons">
			         <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
  	                  </logic:equal>				     
  	                  </logic:equal>  
  	                 </div>	                  				     				     
  	                 <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
  	                  </logic:equal>				     
  	                  </logic:equal>  
  	                 </div>
			       </td>
			     </tr>
		          </nested:iterate>
		          </logic:equal>		          		          
		      <logic:equal name="adBankAccountForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">            
			  <%
			     int i = 0;	
			     String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
	                     String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
		             String rowBgc = null;
	                    %>
	                  <nested:iterate property="adBAList">
			   <%
			     i++;
			     if((i % 2) != 0){
			         rowBgc = ROW_BGC1;
		             }else{
			         rowBgc = ROW_BGC2;
		             }  
			    %>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.bankName"/>
			        </td>
			        <td width="570" height="1" class="gridLabel" colspan="2">
			           <nested:write property="bankName"/>
			        </td>
			        <td width="149" align="center" height="1">
			        <div id="buttons">
			         <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
  	                  </logic:equal>				     
  	                  </logic:equal>  
  	                 </div>	                  				     				     
  	                 <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adBankAccountForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBankAccountForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
  	                  </logic:equal>				     
  	                  </logic:equal>  
  	                 </div>
			       </td>
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.accountName"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="accountName"/>
			        </td>
                 <tr>
			     <tr bgcolor="<%= rowBgc %>">			     
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.accountNumber"/>
			        </td>
			        <td width="350" height="1" class="gridLabel">
			           <nested:write property="accountNumber"/>
			        </td>			     
			        <td width="220" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.accountType"/>
			        </td>
			        <td width="149" height="1" class="gridLabel">
			           <nested:write property="accountType"/>
			        </td>
			     </tr>			        
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="220" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.accountUse"/>
			        </td>
			        <td width="149" height="1" class="gridLabel">
			           <nested:write property="accountUse"/>
			        </td>			        
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.description"/>
			        </td>
			        <td width="350" height="1" class="gridLabel">
			           <nested:write property="description"/>
		            </td>			           			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.cashAccount"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="cashAccount"/>
			        </td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.cashAccountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="cashAccountDescription"/>
			        </td>
			     <tr>	
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.adjustmentAccount"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="adjustmentAccount"/>
			        </td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.adjustmentAccountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="adjustmentAccountDescription"/>
			        </td>
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.interestAccount"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="interestAccount"/>
			        </td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.interestAccountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="interestAccountDescription"/>
			        </td>
			     </tr>		     			     			   		     			     
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.bankChargeAccount"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="bankChargeAccount"/>
			        </td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.bankChargeAccountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="bankChargeAccountDescription"/>
			        </td>
			     </tr>			    
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.salesDiscount"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="salesDiscountAccount"/>
			        </td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.salesDiscountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="salesDiscountAccountDescription"/>
			        </td>
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.advanceAccountDescription"/>
			        </td>
			        <td width="719" height="1" class="gridLabel" colspan="3">
			           <nested:write property="advanceAccountDescription"/>
			        </td>
			     </tr>   		     					     
			     <tr bgcolor="<%= rowBgc %>">			        
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.availableBalance"/>
			        </td>
			        <td width="350" height="1" class="gridLabelNum" colspan="1">
			           <nested:write property="availableBalance"/>
			        </td>	
			        <td width="369" height="1" class="gridLabel" colspan="2"></td>			        
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">			        
			        <td width="175" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.initialCheckNumber"/>
			        </td>
			        <td width="350" height="1" class="gridLabel">
			           <nested:write property="initialCheckNumber"/>
			        </td>			        
			        <td width="220" height="1" class="gridHeader">
			           <bean:message key="bankAccount.prompt.enable"/>
		                </td>
			        <td width="149" height="1" class="gridLabel">
			           <nested:write property="enable"/>
			        </td>
			     </tr>
		          </nested:iterate>	
		          </logic:equal>	          		          		          		          
	               </table>
		    </div>
		 </td>      	      	      
	      <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		 </td>
	      </tr>
           </table>
        </td>
     </tr>
  </table>
</html:form>	    	      
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["bankName"] != null &&
        document.forms[0].elements["bankName"].disabled == false)
        document.forms[0].elements["bankName"].focus()
	       // -->
</script>
<logic:equal name="adBankAccountForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar");
  //-->
</script>
</logic:equal>
</body>
</html>
