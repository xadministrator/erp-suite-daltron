<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="branch.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adBranch.do" onsubmit="return submitForm();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="branch.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adBranchForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        
	        <tr>
	        	<td width="575" height="10" colspan="4">
                <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
                	<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			        	<tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.brBranchCode"/>
			               </td>
					       <td width="147" height="25" class="control">
			                  <html:text property="brBranchCode" size="25" maxlength="25"  styleClass="textRequired"/>
					       </td>	
					       <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.branchName"/>
			               </td>
					       <td width="148" height="25" class="control">
			                  <html:text property="branchName" size="25" maxlength="100"  styleClass="textRequired"/>
					       </td>
					    </tr>
					    <tr>
					       <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.description"/>
			               </td>
					       <td width="148" height="25" class="control">
			                  <html:text property="description" size="25" maxlength="250"  styleClass="text"/>
					       </td>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.type"/>
			               </td>
					       <td width="147" height="25" class="control">
			                  <html:select property="type" styleClass="combo">
               					<html:options property="typeList"/>
            				  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.contactPerson"/>
			               </td>
					       <td width="148" height="25" class="control">
			                  <html:text property="contactPerson" size="25" maxlength="100" styleClass="text"/>
					       </td>
					       <td class="prompt" width="140" height="25">
						   	  <bean:message key="branch.prompt.address"/>
						   </td>
						   <td width="148" height="25" class="control" rowspan="3">
						      <html:textarea property="address" cols="20" rows="4" styleClass="text"/>
						   </td> 
			            </tr>
			            <tr>
						   <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.contactNumber"/>
			               </td>
					       <td width="148" height="25" class="control" colspan="3">
			                  <html:text property="contactNumber" size="25" maxlength="25" styleClass="text"/>
					       </td>
					    </tr>
					    <tr>
					       <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.headQuarter"/>
			               </td>
					       <td width="147" height="25" class="control" colspan="3">
			                  <html:checkbox property="headQuarter" disabled="true"/>
					       </td>		 
			            </tr>
			            <tr>
			               <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.applyShipping"/>
			               </td>
					       <td width="148" height="25" class="control">
			                  <html:checkbox property="applyShipping"/>
					       </td>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="branch.prompt.percentMarkup"/>
			               </td>
					       <td width="147" height="25" class="control">
			                  <html:text property="percentMarkup" size="15" maxlength="25"  styleClass="textAmount"/>
					       </td>
			            </tr>
			            <tr>
			   			   <td class="prompt" width="140" height="25">
                  			  <bean:message key="branch.prompt.shippingAccount"/>
               			   </td>
		       			   <td width="435" height="25" class="control" colspan="3">
                              <html:text property="shippingAccount" size="25" maxlength="25" styleClass="text"/>
				              <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('shippingAccount','shippingAccountDescription');"/>
		                   </td>		 
                       </tr>
	                   <tr>
	                       <td class="prompt" width="140" height="25">
                              <bean:message key="branch.prompt.shippingAccountDescription"/>
                           </td>
		                   <td width="435" height="25" class="control" colspan="3">
                              <html:text property="shippingAccountDescription" size="50" maxlength="50" styleClass="text" disabled="true"/>
		                   </td>
			           </tr>
					</table>   
		        </logic:equal>
			    </td>    
			</tr>        
	        <tr>	        	        
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	        
	           <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="185" colspan="4">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="branch.gridTitle.BRDetails"/>
	                 </td>
	              </tr>	              
	            <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="branch.prompt.brBranchCode"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="branch.prompt.branchName"/>
			       </td>
			       <td width="494" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="branch.prompt.description"/>
			       </td>
                </tr>			    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adBRList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="200" height="1" class="gridLabel">
			            <nested:write property="brBranchCode"/>
			         </td>
			         <td width="200" height="1" class="gridLabel">
			            <nested:write property="branchName"/>
			         </td>	
			         <td width="250" height="1" class="gridLabel">
			            <nested:write property="description"/>
			         </td>		         
			         <td width="244" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      </nested:iterate>
			      </logic:equal>
			      <logic:equal name="adBranchForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adBRList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="branch.prompt.brBranchCode"/>
			         </td>
			         <td width="570" height="1" class="gridLabel" colspan="2">
			            <nested:write property="brBranchCode"/>
			         </td>
			         <td width="149" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adBranchForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adBranchForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="branch.prompt.branchName"/>
			         </td>
			         <td width="570" height="1" class="gridLabel">
			            <nested:write property="branchName"/>
			         </td>
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.type"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="type"/>
                     </td>
			      </tr>
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.description"/>
                     </td>
                     <td width="570" height="1" class="gridLabel">
                        <nested:write property="description"/>
                     </td>
                     <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.headQuarter"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="headQuarter"/>
                     </td>
				  </tr>
				  <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.contactPerson"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="contactPerson"/>
                     </td>
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.contactNumber"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="contactNumber"/>
                     </td>
				  </tr>
				  <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.applyShipping"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="applyShipping"/>
                     </td>
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.percentMarkup"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="percentMarkup"/>
                     </td>
				  </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="branch.prompt.shippingAccount"/>
                     </td>
                     <td width="350" height="1" class="gridLabel" colspan="3">
                        <nested:write property="shippingAccount"/>
                     </td>
			      </tr>
			      </nested:iterate>			      
			      </logic:equal>
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["brBranchCode"] != null)
        document.forms[0].elements["brBranchCode"].focus()
	       // -->
</script>
</body>
</html>
