<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="changePassword.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adChangePassword.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="changePassword.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="2" class="statusBar">
		        <logic:equal name="adChangePasswordForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
   	        </td>
	     </tr>
	     <logic:equal name="adChangePasswordForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">	     
         <tr>
		     <td width="130" height="25" class="prompt">
                <bean:message key="changePassword.prompt.userPassword"/>
             </td>
		     <td width="445" height="25" class="control">
		        <html:password property="userPassword" size="15" maxlength="25" styleClass="textRequired"/>
		     </td>
	     </tr>
	     <tr>
	     	 <td width="130" height="25" class="prompt">
		        <bean:message key="changePassword.prompt.newPassword"/>
		     </td>
  		     <td width="445" height="25" class="control">
		        <html:password property="newPassword" size="15" maxlength="25" styleClass="textRequired"/>
		     </td>
		 </tr>	     
	     <tr>
	     	 <td width="130" height="25" class="prompt">
		        <bean:message key="changePassword.prompt.confirmPassword"/>
		     </td>
  		     <td width="445" height="25" class="control">
		        <html:password property="confirmPassword" size="15" maxlength="25" styleClass="textRequired"/>
		     </td>
		 </tr>
	     <tr>
	        <td width="575" height="50" colspan="2"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="saveButton" styleClass="mainButton">
		         <bean:message key="button.save"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.save"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>
	     </logic:equal>
	     <tr>
	       <td width="575" height="10" colspan="2" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		 </td>
	   </tr>
      </table>
     </td>
    </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["userPassword"] != null)
         document.forms[0].elements["userPassword"].focus()
   // -->
</script>
</body>
</html>
