<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="adCompany.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adCompany.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="450">
      <tr valign="top">
        <td width="187" height="450"></td> 
        <td width="581" height="450">
         <table border="0" cellpadding="0" cellspacing="0" width="585" height="450" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="adCompany.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="adCompanyForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="adCompanyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="adCompany.prompt.companyName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
                   <html:text property="companyName" size="50" maxlength="50" styleClass="textRequired"/>
             </td>
         </tr>
         
          <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="adCompany.prompt.taxPayerName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
                   <html:text property="taxPayerName" size="50" maxlength="50" styleClass="textRequired"/>
             </td>
         </tr>
         
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="adCompany.prompt.contacts"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
                   <html:text property="contacts" size="50" maxlength="50" styleClass="textRequired"/>
             </td>
         </tr>
         
		  <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="adCompany.prompt.description"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
               <html:text property="description" size="50" maxlength="50" styleClass="text"/>
            </td>
		 </tr>       
		 <tr>	        	     	     		         		    	     
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.retainedEarnings"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
               <html:text property="retainedEarnings" size="30" maxlength="255" styleClass="text"/>
               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('retainedEarnings','');"/>	            
		    </td>	     				    		    
		 </tr>                               
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.industry"/>
            </td> 
		    <td width="147" height="25" class="control">
               <html:text property="industry" size="25" maxlength="25" styleClass="text"/>
		    </td>   
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.tinNumber"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="tinNumber" size="25" maxlength="25" styleClass="text"/>
		    </td>
         </tr>
         
         <tr>
		    
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.fiscalYearEnding"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="fiscalYearEnding" size="25" maxlength="25" styleClass="text"/>
		    </td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.rdo"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="revenueOffice" size="25" maxlength="25" styleClass="text"/>
		    </td>
         </tr>	     
		  <tr>		     
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.emailAddress"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="emailAddress" size="25" maxlength="30" styleClass="text"/>
		    </td>		     
		    <td class="prompt" width="140" height="25">
            <bean:message key="adCompany.prompt.currency"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="currency" styleClass="comboRequired">
               <html:options property="currencyList"/>
            </html:select>
		    </td>
         </tr>                       
	     <tr>
			<td class="prompt" width="140" height="25">
		       <bean:message key="adCompany.prompt.address"/>
		    </td>
		    <td width="148" height="25" class="control">
		       <html:textarea property="address" cols="20" rows="4" styleClass="text"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.city"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:text property="city" size="25" maxlength="25" styleClass="text"/>
		    </td>		    
         </tr>
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.zipCode"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="zipCode" size="10" maxlength="10" styleClass="text"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.country"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="country" size="25" maxlength="25" styleClass="text"/>
		    </td>				    		    
		 </tr> 
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.telephoneNumber"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="telephoneNumber" size="10" maxlength="10" styleClass="text"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.faxNumber"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="faxNumber" size="10" maxlength="10" styleClass="text"/>
		    </td>				    		    
		 </tr>	
		 
		  <tr>
		    <td class="prompt" width="120" height="30">

            </td>		    		    
		 </tr>
		 
		 <tr>
		 	<td class="prompt" width="140" height="25"> 
		 		<b>MAILING ADDRESS</b>
		 	</td>	 
		 </tr>
		 
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailSectionNo"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="mailSectionNo" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailLotNo"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="mailLotNo" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		  
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailStreet"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="mailStreet" size="25" maxlength="25" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailPoBox"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:textarea property="mailPoBox" cols="20" rows="4" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailCountry"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="mailCountry" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailProvince"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="mailProvince" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailPostOffice"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="mailPostOffice" size="25" maxlength="25" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.mailCareOff"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="mailCareOff" size="25" maxlength="25" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="30">

            </td>		    		    
		 </tr>
		 
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.taxPeriodFrom"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="taxPeriodFrom" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.taxPeriodTo"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="taxPeriodTo" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 
		  <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.publicOfficeName"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="publicOfficeName" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="adCompany.prompt.dateAppointment"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="dateAppointment" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 	 		 		 		        		 		 		 		                         
         </logic:equal>
	     <logic:equal name="adCompanyForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="adCompany.prompt.companyName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
                   <html:text property="companyName" size="50" maxlength="50" styleClass="textRequired" disabled="true"/>
             </td>
         </tr>
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="adCompany.prompt.contacts"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
                   <html:text property="contacts" size="50" maxlength="50" styleClass="textRequired" disabled="true"/>
             </td>
         </tr>
         
         
		  <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="adCompany.prompt.description"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
               <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
            </td>
		 </tr>          
		 <tr>	        	     	     		         		    	     
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.retainedEarnings"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
               <html:text property="retainedEarnings" size="30" maxlength="255" styleClass="text" disabled="true"/>
               <html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>	            
		    </td>	     				    		    
		 </tr>                            
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.industry"/>
            </td> 
		    <td width="147" height="25" class="control">
               <html:text property="industry" size="25" maxlength="25" styleClass="text" disabled="true"/>
		    </td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.tinNumber"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="tinNumber" size="25" maxlength="25" styleClass="text" disabled="true"/>
		    </td>	     
         </tr>                   
		  <tr>		     
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.emailAddress"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="emailAddress" size="25" maxlength="30" styleClass="text" disabled="true"/>
		    </td>		     
		    <td class="prompt" width="140" height="25">
            <bean:message key="adCompany.prompt.currency"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="currency" styleClass="comboRequired" disabled="true">
               <html:options property="currencyList"/>
            </html:select>
		    </td>
         </tr>                       
	     <tr>
			<td class="prompt" width="140" height="25">
		       <bean:message key="adCompany.prompt.address"/>
		    </td>
		    <td width="148" height="25" class="control">
		       <html:textarea property="address" cols="20" rows="4" styleClass="text" disabled="true"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.city"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:text property="city" size="25" maxlength="25" styleClass="text" disabled="true"/>
		    </td>		    
         </tr>
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.zipCode"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="zipCode" size="10" maxlength="10" styleClass="text" disabled="true"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.country"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="country" size="25" maxlength="25" styleClass="text" disabled="true"/>
		    </td>				    		    
		 </tr> 
	     <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.telephoneNumber"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="telephoneNumber" size="10" maxlength="10" styleClass="text" disabled="true"/>
		    </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="adCompany.prompt.faxNumber"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="faxNumber" size="10" maxlength="10" styleClass="text" disabled="true"/>
		    </td>				    		    
		 </tr>		 		 		 		        		 		 		 		                         
         </logic:equal>	     
	     <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
		     <logic:equal name="adCompanyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton">
			       <bean:message key="button.save"/>
			  	</html:submit>
			  </logic:equal>
			    <html:submit property="closeButton" styleClass="mainButton">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			 <logic:equal name="adCompanyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.save"/>
			    </html:submit>
			 </logic:equal>
			    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>		         
			 </td>
		 </tr>
	     <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["companyName"] != null &&
        document.forms[0].elements["companyName"].disabled == false)
        document.forms[0].elements["companyName"].focus()
   // -->
</script>
</body>
</html>
