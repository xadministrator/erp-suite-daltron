<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<html>
<head>
<title>
  <bean:message key="console.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function clickNaturalAccount(name)
{
   
   var property = name.substring(0,name.indexOf("."));  
   var index = parseInt(name.substring(12, 13));

   var ctr = 0;
   
   while (true) {
       
       if (document.forms[0].elements["segmentList[" + ctr + "].naturalAccount"] == null) break;
       if (ctr * 1 != index * 1) {

	       document.forms[0].elements["segmentList[" + ctr + "].naturalAccount"].checked = false;
	       
	   }
	   
	   ctr++;
   
   }  
   
   return true;
}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adConsole.do" onsubmit="return disableButtons();" method="POST" enctype="multipart/form-data">
<table width="100%" height="76" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
  <td width="30%" height="74" bgcolor="#FFFFFF"><img src="images/banner.gif"/></td>
  <td width="40%" height="74" style="filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFFFFF', endColorStr='#6699FF', gradientType='1')"></td>
  <td width="30%" height="74" bgcolor="#6699FF"><p align="right"><img src="images/powered.gif"/></td>
  </tr>
</table>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="200">
      <tr valign="top">
        <td width="768" height="200">
         <table border="0" cellpadding="0" cellspacing="0" width="768" height="200" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="768" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="console.title"/>
     		</td>
	     </tr>
	     <tr>
            <td width="768" height="44" colspan="4" class="statusBar">
	        <html:errors/>	
            </td>
         </tr>
	     <logic:equal name="adConsoleForm" property="pageNumber" value="1">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.companyName"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:text property="companyName" size="50" maxlength="50" styleClass="textRequired"/>
             </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.shortName"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:text property="shortName" size="25" maxlength="25" styleClass="textRequired"/>
             </td>
         </tr>
		  <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="console.prompt.welcomeNote"/>
	        </td>
	        <td width="628" height="25" class="control" colspan="3">
               <html:text property="welcomeNote" size="50" maxlength="50" styleClass="textRequired"/>
            </td>
		 </tr>  
		 <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.numberOfSegment"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:text property="numberOfSegment" size="5" maxlength="2" styleClass="textRequired"/>
             </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.segmentSeparator"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:text property="segmentSeparator" size="5" maxlength="1" styleClass="textRequired"/>
             </td>
         </tr>         
		 </logic:equal>  
		 <logic:equal name="adConsoleForm" property="pageNumber" value="2">
	     <tr valign="top">
	           <td width="300" height="185" colspan="10">
		          <div align="left">
		          <table border="1" cellpadding="0" cellspacing="0" width="300" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="300" height="1" colspan="4" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="console.gridTitle.segmentList"/>
	                 </td>
	              </tr>	              	              
			      <tr>
			         <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            <bean:message key="console.prompt.segmentNumber"/>
			         </td>
			         <td width="210" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            <bean:message key="console.prompt.segmentName"/>
			         </td>
			         <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            <bean:message key="console.prompt.maximumSize"/>
			         </td>
			         <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            <bean:message key="console.prompt.naturalAccount"/>
			         </td>
                  </tr>			       	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="segmentList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="30" height="1" class="gridLabel">
			            <nested:text property="segmentNumber" size="1" maxlength="5" readonly="true" styleClass="textRequired"/>
			         </td>
			         <td width="210" height="1" class="gridLabel">
			            <nested:text property="segmentName" size="25" maxlength="25" styleClass="textRequired"/>
			         </td>	
			         <td width="30" height="1" class="gridLabel">
			            <nested:text property="maximumSize" size="1" maxlength="2" styleClass="textRequired"/>
			         </td>
			         <td width="30" height="1" class="gridLabel">
			            <nested:checkbox property="naturalAccount" onclick="return clickNaturalAccount(name);"/>
			         </td>		
			       </tr>         
			      </nested:iterate>
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.segmentsFile"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:file property="segmentsFile" size="35" styleClass="textRequired"/>
             </td>
            </tr>
	        <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="console.prompt.coaFile"/>
	         </td>
	         <td width="628" height="25" class="control" colspan="3">
                   <html:file property="coaFile" size="35" styleClass="textRequired"/>
             </td>
            </tr>            
		 </logic:equal> 			 	 
		 <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
			    <logic:equal name="adConsoleForm" property="pageNumber" value="2">
			    <html:submit property="previousButton" styleClass="mainButton">
			       <bean:message key="button.previous"/>
			  	</html:submit>
			  	</logic:equal>
			  	<logic:equal name="adConsoleForm" property="pageNumber" value="1">
			    <html:submit property="previousButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.previous"/>
			  	</html:submit>
			  	</logic:equal>
			  	<logic:notEqual name="adConsoleForm" property="pageNumber" value="2">
			  	<html:submit property="nextButton" styleClass="mainButton">
			       <bean:message key="button.next"/>
			    </html:submit>
			    </logic:notEqual>
			    <logic:equal name="adConsoleForm" property="pageNumber" value="2">
			    <html:submit property="finishButton" styleClass="mainButton">
			       <bean:message key="button.finish"/>
			    </html:submit>
			    </logic:equal>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			    <html:submit property="previousButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.previous"/>
			    </html:submit>
			    <logic:notEqual name="adConsoleForm" property="pageNumber" value="2">
			    <html:submit property="nextButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.next"/>
			    </html:submit>
			    </logic:notEqual>
			    <logic:equal name="adConsoleForm" property="pageNumber" value="2">
			    <html:submit property="finishButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.finish"/>
			    </html:submit>
			    </logic:equal>
			 </div>		         
			 </td>
		 </tr>  		 
	     <tr>
	         <td width="768" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["companyName"] != null &&
        document.forms[0].elements["companyName"].disabled == false)
        document.forms[0].elements["companyName"].focus()
   // -->
</script>
</body>
</html>
