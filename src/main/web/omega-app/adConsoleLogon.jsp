<%@ page language="java" import="com.struts.util.Constants"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>

<html:html locale="true">
<head>
<title><bean:message key="app.title"/> - <bean:message key="consoleLogon.title"/></title>
<html:base/>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body bgcolor="white" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<html:form action="/adConsoleLogon.do" onsubmit="return disableButtons();">
<table width="100%" height="76" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
  <td width="30%" height="74" bgcolor="#FFFFFF"><img src="images/banner.gif"/></td>
  <td width="40%" height="74" style="filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFFFFF', endColorStr='#002c48', gradientType='1')"></td>
  <td width="30%" height="74" bgcolor="#002c48"><p align="right"></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
  <tr valign="top">
    <td width="225" height="545" bgcolor="#dfeef5">
       <table border="0" cellpadding="0" cellspacing="0" width="240" height="42">
       <tr valign="top">
          <td width="95" height="19"></td>
          <td width="125" height="19"></td>
        </tr>   
	<tr>
	   <td class="prompt" width="95" height="19">
	       <bean:message key="consoleLogon.prompt.userName"/>
	   </td>
	   <td align="left" width="125" height="19">
	      <html:text property="userName" size="16" maxlength="16" styleClass="textRequired"/>
	   </td>
	</tr>
	<tr>
           <td class="prompt" width="95" height="19">
	      <bean:message key="consoleLogon.prompt.password"/>
	   </td>
	   <td align="left" width="125" height="19">
	       <html:password property="password" size="16" maxlength="16" styleClass="textRequired"/>
	   </td>
	</tr> 
	<tr>
	   <td width="95" height="5">
	   </td>
	</tr>
        <tr>
           <td align="right" width="95" height="19">
              <div id="buttons">
              <html:submit property="connectButton" styleClass="mainButton">
	         <bean:message key="button.connect"/>
              </html:submit>
              </div>
              <div id="buttonsDisabled" style="display: none;">
              <html:submit property="connectButton" styleClass="mainButton" disabled="true">
	         <bean:message key="button.connect"/>
              </html:submit>
              </div>
           </td>
           <td align="left" width="125" height="19">
              <div id="buttons">
              <html:submit property="resetButton" styleClass="mainButton">
		         <bean:message key="button.reset"/>
		      </html:submit>
			  </div>	      
			  <div id="buttonsDisabled" style="display: none;">
              <html:submit property="resetButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.reset"/>
		      </html:submit>
			  </div>
           </td>
        </tr>
        <tr>
	   <td width="95" height="5">
	   </td>
	</tr>
        <tr>
           <!-- <td align="left" width="125" height="19">
              <html:submit property="setupButton" styleClass="mainButton">
	         <bean:message key="button.setup"/>
              </html:submit>
           </td> -->
        </tr>
           
	</table>
    </td>
    <td width="535" height="510">
       <table border="0" cellpadding="0" cellspacing="0" width="533" height="19">          
	       	
			<tr>
          		<td width="530" height="44" class="statusBar"><html:errors/></td>              		                      
		    </tr>
       </table>
    </td>
  </tr>
</table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     
     document.forms[0].elements["userName"].focus();
             
	       // -->
</script>
</body>
</html:html>
