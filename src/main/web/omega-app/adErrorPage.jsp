<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>

<html>
<head>
<title>
  <bean:message key="errorPage.title"/> 
</title>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
  <%@ include file="cmnHeader.jsp" %> 
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign='top'>
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="24">
             <tr>
	        <td width="581" height="44">
		   <bean:message key="errorPage.message"/>	
	        </td>
	     </tr>     
          </table>
        </td>
      </tr>
  </table>
</body>
</html>
