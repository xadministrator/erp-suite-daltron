<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="formFunctionResponsibility.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRGlList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRGlList[" + i + "].select"].checked = true;
          
      i++;   
   }  
   
   i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRApList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRApList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRArList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRArList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRCmList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRCmList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRAdList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRAdList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRInvList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRInvList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
	i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRHrList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRHrList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   
	i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["adFRPmList[" + i + "].select"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["adFRPmList[" + i + "].select"].checked = true;
          
      i++;   
   }
   
   
   return false;   
      
}
//Done Hiding-->
</script>
<%@ include file="cmnSidebar.jsp" %>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adFormFunctionResponsibility.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="formFunctionResponsibility.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adFormFunctionResponsibilityForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="formFunctionResponsibility.prompt.responsibilityName"/>
               </td>
		       <td width="147" height="25" class="control">
                  <html:text property="responsibilityName" styleClass="text" disabled="true"/>
		       </td>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="formFunctionResponsibility.prompt.description"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="description" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>
            <tr>
	           <td width="575" height="30" colspan="4">
		       </td>
	        </tr>            	     
	        <tr>
               <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
               </td>
            </tr>
	        <tr>
	           <td width="575" height="5" colspan="4">
		       </td>
	        </tr>
	        <tr>          	            	       	      	        	                                   	         	        
	           <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adFormFunctionResponsibilityForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adFormFunctionResponsibilityForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				     <bean:message key="button.selectAll"/>
			   </html:submit>
		       <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		       </html:submit>
		       </logic:equal>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adFormFunctionResponsibilityForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adFormFunctionResponsibilityForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		       <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
			        <bean:message key="button.selectAll"/>
			   </html:submit>	
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.close"/>
		       </html:submit>
		       </logic:equal>
		       </div>
               </td>
	        </tr>
	        <tr> 
	      	  <td width="575" height="10" colspan="4">  
	      	    <div class="tabber">
	      	    <% if (user.getUserApps().contains("OMEGA GENERAL LEDGER")) { %>
	            <div class="tabbertab" title="GL">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRGlList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    <% if (user.getUserApps().contains("OMEGA PAYABLES")) { %>
			    <div class="tabbertab" title="AP">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRApList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    <% if (user.getUserApps().contains("OMEGA RECEIVABLES")) { %>
			    <div class="tabbertab" title="AR">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRArList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    <% if (user.getUserApps().contains("OMEGA CASH MANAGEMENT")) { %>
			    <div class="tabbertab" title="CM">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRCmList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    <% if (user.getUserApps().contains("OMEGA ADMINISTRATION")) { %>
			    <div class="tabbertab" title="Admin">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRAdList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    <% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
			    <div class="tabbertab" title="INV">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRInvList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    
			    
			    
			    <% if (user.getUserApps().contains("OMEGA HUMAN RESOURCE")) { %>
			    <div class="tabbertab" title="HR">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRHrList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    
			    
			    <% if (user.getUserApps().contains("OMEGA PROJECT MANAGEMENT")) { %>
			    <div class="tabbertab" title="PM">
	            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
			        <tr valign="top">
			           <td width="575" height="185" colspan="4">
				          <div align="center">
				          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					      <tr>
		                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                    <bean:message key="formFunctionResponsibility.gridTitle.FRDetails"/>
			                 </td>
			              </tr>	              
					      <tr>
					         <td width="744" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.formFunctionName"/>
					         </td>
					         <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					            <bean:message key="formFunctionResponsibility.prompt.parameter"/>
					         </td>
		                  </tr>			                   	              	              
					      <%
					         int i = 0;	
					         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					         String rowBgc = null;
					      %>
					      <nested:iterate property="adFRPmList">
					      <%
					         i++;
					         if((i % 2) != 0){
					            rowBgc = ROW_BGC1;
					         }else{
					            rowBgc = ROW_BGC2;
					         }  
					      %>
					      <tr bgcolor="<%= rowBgc %>">
					         <td width="744" height="1" class="gridLabel">
					            <nested:text property="formFunctionName" size="81" maxlength="80" styleClass="text" readonly="true"/>
					         </td>
					         <td width="80" height="1" class="gridLabel">
					            <nested:select property="parameter" styleClass="combo">
						             <nested:options property="parameterList"/>				          				          
						        </nested:select>
					         </td>		       			       			       			                   	             
				             <td width="70" align="center" height="1">
				                <nested:checkbox property="select"/>
				             </td>
					      </tr>
			              </nested:iterate>
					      </table>
				         </div>
				       </td>
			        </tr>
			    </table>
			    </div>
			    <% } %>
			    
			    </div><script>tabberAutomatic(tabberOptions)</script>
			  </td>
			</tr>		      	         
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
           </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>

</script>
</body>
</html>
