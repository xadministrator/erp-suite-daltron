<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="log.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>


<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<html:form action="/adLog.do" onsubmit="return submitForm();">

  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
      
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="log.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		
	        </td>
	     </tr>
	     <tr>
        	<td width="575" height="10" colspan="4">
    	       
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			       	<tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
			        <tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="log.prompt.module"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="module" size="45" maxlength="25" styleClass="textRequired" disabled="true"/>
						</td>
					</tr> 	 
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="log.prompt.transactionNumber"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="transactionNumber" size="45" maxlength="25" styleClass="textRequired" disabled="true"/>
						</td>
					</tr> 	     	     
					
				</table>
				
			</td>
		</tr>
		<tr>
			<td width="575" height="50" colspan="4">

	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                      <td width="575" height="1" colspan="7" class="gridTitle" 
	             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                         <bean:message key="log.gridTitle.logDetails"/>
                      </td>
                   </tr>
	              
			    <tr>
			       <td width="94" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="log.prompt.date"/>
			       </td>
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="log.prompt.username"/>
			       </td>			       
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="log.prompt.action"/>
			       </td>
			       
			     
			      
                </tr>	
                <tr>
                   <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" colspan="4">
			          <bean:message key="log.prompt.description"/>
			       </td>
                
                </tr>		   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="adLOGList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:text property="date" disabled="true" styleClass="text"/>
			       </td>
			       <td width="220" height="1" class="gridLabel">
			          <nested:text property="username" disabled="true" styleClass="text"/>
			       </td>
			       <td width="220" height="1" class="gridLabel">
			          <nested:text size="35" property="action" disabled="true" styleClass="text"/>
			       </td>
			       
			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			    	<td width="750" height="1" class="gridLabel" colspan="4">
			          <nested:textarea cols="89" rows="5" property="description" disabled="true" styleClass="text"/>
			       </td>
			       
			    </tr>
			  
			    </nested:iterate>
             		    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	</td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["type"] != null &&
        document.forms[0].elements["type"].disabled == false)
        document.forms[0].elements["type"].focus()
   // -->
</script>
</body>
</html>
