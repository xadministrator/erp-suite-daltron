<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="paymentSchedule.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

function formatRelativeAmount(name) 
{

      var num = "";
      var caretPos = parseInt(doGetCaretPosition(document.forms[0].elements[name]));
      var currAmount = document.forms[0].elements[name].value;
      
      if((event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode >= 48 && event.keyCode <= 57) 
	  	 		|| event.keyCode == 110 || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 46) {
		 
		 //this code places a '0' before the '.' 
		 //if no digit was found before the '.'
		 
		 if(currAmount.charAt(0) == '.' && event.keyCode != 8 && event.keyCode != 46) {
		 
			currAmount = 0 + currAmount;
			caretPos++;
			
			document.forms[0].elements[name].value = currAmount;
		 	
		 	//set caret position
		 	doSetCaretPosition (document.forms[0].elements[name], caretPos, 1);		 	
		 
		 }
		 
		 //format decimal part
		 if(event.keyCode == 110 || event.keyCode == 190 || (currAmount.indexOf(".") != -1 && caretPos > currAmount.indexOf("."))) {
		 	
		 	if(event.keyCode == 110 || event.keyCode == 190) {
		 	
		 		if(num.substring(num.indexOf(".") - 1, num.length).length == 0)
			 		currAmount = currAmount + "";
			
			}
			
		 	document.forms[0].elements[name].value = currAmount;
		 	
		 	//set caret position
		 	doSetCaretPosition (document.forms[0].elements[name], caretPos, 1); 
		 	
		 }
		 
	  }
}
//Done Hiding-->
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adPaymentSchedule.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="paymentSchedule.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adPaymentScheduleForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentSchedule.prompt.termName"/>
               </td>
		       <td width="435" height="25" class="control">
                  <html:text property="termName" size="25" styleClass="text" disabled="true"/>
		       </td>
		    </tr>
		    <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentSchedule.prompt.description"/>
               </td>
		       <td width="435" height="25" class="control">
                  <html:text property="description" size="50" styleClass="text" disabled="true"/>
		       </td>		 
            </tr>            
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentSchedule.prompt.baseAmount"/>
               </td>
		       <td width="435" height="25" class="control">
                  <html:text property="baseAmount" size="20" styleClass="textAmount" disabled="true"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentSchedule.prompt.enable"/>
               </td>
		       <td width="435" height="25" class="control">
                  <html:checkbox property="enable" disabled="true"/>
		       </td>		 
            </tr>	    
			<tr>
	           <td class="prompt" width="575" height="25" colspan="4">
                  <p align="right">
                  <html:submit property="paymentTerms" styleClass="mainButtonBig">
		             <bean:message key="button.paymentTerms"/>
		          </html:submit>
		       </td>		 
            </tr>
    
	     	<tr>
	           <td width="575" height="5" colspan="4"> <p align="right">
		          <logic:equal name="adPaymentScheduleForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          </logic:equal>
		       <td>
            </tr>	     
	        <tr>
               <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
               </td>
            </tr>
	        <tr>
	           <td width="575" height="5" colspan="4">
		       </td>
	        </tr>

	        <tr>          	        
	           <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adPaymentScheduleForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adPaymentScheduleForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="150" colspan="4">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="577" height="1" colspan="5" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="paymentSchedule.gridTitle.PSDetails"/>
	                 </td>
	              </tr>	              
	            
			      <tr>
			       	<td width="121" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          	<bean:message key="paymentSchedule.prompt.lineNumber"/>
			       	</td>
					<td width="151" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          	<bean:message key="paymentSchedule.prompt.relativeAmount"/>
			       	</td>
					<td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                        <bean:message key="paymentSchedule.prompt.dueDay"/>
                    </td>
					<td width="195" height="1" colspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				    	<bean:message key="paymentSchedule.prompt.delete"/>
				    </td>
                  </tr>			    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
				  <nested:iterate property="adPSList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="121" height="1" class="control">
			            <nested:text property="lineNumber" size="8" maxlength="4" styleClass="text" disabled="true"/>
			         </td>
			         <td width="151" height="1" class="control">
			            <nested:text property="relativeAmount" size="12" maxlength="9" styleClass="textAmountRequired" onblur="addZeroes(name);" onkeyup="formatRelativeAmount(name);"/>
			         </td>			         
			         <td width="110" height="1" class="control">
                        <nested:text property="dueDay" size="6" maxlength="4" styleClass="textRequired"/>
                     </td>
					 <td width="75" height="1" class="control">	
						<p align="center">
				        <nested:checkbox property="deleteCheckbox"/>
				     </td>
			         <td width="120" align="center" height="1">
			         <div id="buttons">
			            <nested:submit property="discountsButton" styleClass="gridButtonBig">
			               <bean:message key="button.discounts"/>
			            </nested:submit>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			            <nested:submit property="discountsButton" styleClass="gridButtonBig" disabled="true">
			               <bean:message key="button.discounts"/>
			            </nested:submit>
			         </div>
			         </td>
			      </tr>
			      </nested:iterate>
				  </table>
		          </div>
		       </td>
	        </tr>

			<tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="adPaymentScheduleForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="adPaymentScheduleForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="adPaymentScheduleForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="adPaymentScheduleForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	

	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["lineNumber"] != null)
        document.forms[0].elements["lineNumber"].focus()
	       // -->
</script>
</body>
</html>
