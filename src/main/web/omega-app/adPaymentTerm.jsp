<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="paymentTerm.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers

function clickDiscountBox()
{      
   var div = document.getElementById("showDiscountAccount");
   div.style.display = "block";
   return true;
} 

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adPaymentTerm.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="paymentTerm.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adPaymentTermForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>

	        <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.termName"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="termName" size="25" maxlength="25" styleClass="textRequired"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.description"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="description" size="50" maxlength="50" styleClass="text"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.baseAmount"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="baseAmount" size="20" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
		       </td>		 
            </tr>
            <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.monthlyInterestRate"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="monthlyInterestRate" size="20" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
		       </td>		 
            </tr>
            <tr>
                <td class="prompt" width="130" height="25">
	           		<bean:message key="paymentTerm.prompt.scheduleBasis"/>
	        	</td>
	        	<td width="221" height="25" class="control">
		   			<html:select property="scheduleBasis" styleClass="comboRequired">
		      			<html:options property="scheduleBasisList"/>
		   			</html:select>
		   		</td>
			</tr>
			<tr>
				<td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.enable"/>
               </td>
		       <td width="140" height="25" class="control">
                  <html:checkbox property="enable"/>
		       </td>
			</tr>
			
			<tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.enableRebate"/>
               </td>
		       <td width="140" height="25" class="control">
                  <html:checkbox property="enableRebate"/>
		       </td>
		    
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.enableInterest"/>
               </td>
		       <td width="140" height="25" class="control">
                  <html:checkbox property="enableInterest"/>
		       </td>
		    </tr>
	        <tr>
	           		 
			   <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.discountOnInvoice" />
               </td>
		       <td width="155" height="25" class="control">
                  <html:checkbox property="discountOnInvoice" />
		       </td>
            </tr>            
	        </logic:equal>
			
			<div id="showDiscountAccount" style="display: none;">
			<tr>
			   <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.discountAccount"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="discountAccount" size="25" maxlength="25" styleClass="text"/>
				  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('discountAccount','discountAccountDescription');"/>
		       </td>		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.discountAccountDescription"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="discountAccountDescription" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       </td>
			</tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="paymentTerm.prompt.discountDescription"/>
               </td>
		       <td width="435" height="25" class="control" colspan="3">
                  <html:text property="discountDescription" size="50" maxlength="50" styleClass="text" />
		       </td>		 
            </tr>
			</div>

	        <tr>	        
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	        	        
	           <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="185" colspan="4">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="paymentTerm.gridTitle.PTDetails"/>
	                 </td>
	              </tr>	              
	            <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="paymentTerm.prompt.termName"/>
			       </td>
			       <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="paymentTerm.prompt.description"/>			          
			       </td>
				   <td width="294" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					  <bean:message key="paymentTerm.prompt.discountOnInvoice"/>
			       </td>
                </tr>			    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adPTList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="300" height="1" class="gridLabel">
			            <nested:write property="termName"/>
			         </td>			      
			         <td width="300" height="1" class="gridLabel">
			            <nested:write property="description"/>
			         </td>
					 <td width="145" height="1" class="gridLabel">
			            <nested:write property="discountOnInvoice"/>
			         </td>
			         <td width="149" align="center" height="1" colspan="2">
			         <div id="buttons">
			         <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="894" align="right" height="1" colspan="5">
			         <div id="buttons">
			         <logic:notEqual name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="paymentSchedulesButton" styleClass="gridButtonHuge">
			               <bean:message key="button.paymentSchedules"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			         <logic:notEqual name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="paymentSchedulesButton" styleClass="gridButtonHuge" disabled="true">
			               <bean:message key="button.paymentSchedules"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         </td>			      
			      </tr>
			      </nested:iterate>
			      </logic:equal>

			      <logic:equal name="adPaymentTermForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adPTList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="paymentTerm.prompt.termName"/>
			         </td>
			         <td width="570" height="1" class="gridLabel" colspan="2">
			            <nested:write property="termName"/>
			         </td>
			         <td width="149" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adPaymentTermForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="paymentTerm.prompt.description"/>
			         </td>
			         <td width="570" height="1" class="gridLabel" colspan="2">
			            <nested:write property="description"/>
			         </td>
			         <td width="149" align="center" height="1">
			         <div id="buttons">
			         <logic:notEqual name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="paymentSchedulesButton" styleClass="gridButtonHuge">
			               <bean:message key="button.paymentSchedules"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			         <logic:notEqual name="adPaymentTermForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="paymentSchedulesButton" styleClass="gridButtonHuge" disabled="true">
			               <bean:message key="button.paymentSchedules"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         </td>
			      </tr>
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="paymentTerm.prompt.baseAmount"/>
                     </td>
                     <td width="350" height="1" class="gridLabelNum">
                        <nested:write property="baseAmount"/>
                     </td>
			         <td width="220" height="1" class="gridHeader">
                        <bean:message key="paymentTerm.prompt.enable"/>
                     </td>
                     <td width="149" height="1" class="gridLabel">
                        <nested:write property="enable"/>
                     </td>			         
				  </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="paymentTerm.prompt.discountAccount"/>
                     </td>
                     <td width="350" height="1" class="gridLabel">
                        <nested:write property="discountAccount"/>
                     </td>
			         <td width="220" height="1" class="gridHeader">
                        <bean:message key="paymentTerm.prompt.discountOnInvoice"/>
                     </td>
                     <td width="149" height="1" class="gridLabel">
                        <nested:write property="discountOnInvoice"/>
                     </td>			         
				  </tr>
				  <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="paymentTerm.prompt.scheduleBasis"/>
                     </td>
                     <td width="350" height="1" class="gridLabel" colspan="3">
                        <nested:write property="scheduleBasis"/>
                     </td>         
				  </tr>
			      </nested:iterate>			      
			      </logic:equal>
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["termName"] != null)
        document.forms[0].elements["termName"].focus()
	       // -->
</script>
</body>
</html>
