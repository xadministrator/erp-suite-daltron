<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="preference.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
<%@ include file="cmnSidebar.jsp" %>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adPreference.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="250">
      <tr valign="top">
        <td width="187" height="250"></td>
        <td width="581" height="250">
         <table border="0" cellpadding="0" cellspacing="0" width="575" height="250"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="preference.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="adPreferenceForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>
	        </td>
	     </tr>
	     <logic:equal name="adPreferenceForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	        <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <div class="tabbertab" title="AD">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
   				    	<tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
                                        </tr>

   				         <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.adDisableMultipleLogin"/>
				            </td>
				            <td width="425" height="25" class="control" colspan="3">
				               <html:checkbox property="adDisableMultipleLogin"/>
				            </td>
                        </tr>

                         <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.dateFormatInput"/>
				            </td>
                            <td width="425" height="25" class="control" colspan="3">
                                <html:select property="dateFormatInput" styleClass="comboRequired">
                                    <html:options property="dateFormatList"/>
                                </html:select>
                            </td>
                         </tr>

                        <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.dateFormatOutput"/>
				            </td>
                             <td width="425" height="25" class="control" colspan="3">
                                 <html:select property="dateFormatOutput" styleClass="comboRequired">
                                     <html:options property="dateFormatList"/>
                                 </html:select>
                             </td>
                       </tr>


                       <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.adEnableEmailNotification"/>
				            </td>
				            <td width="425" height="25" class="control" colspan="3">
				               <html:checkbox property="adEnableEmailNotification"/>
				            </td>
                        </tr>
                     </table>
					</div>


			        <% if (user.getUserApps().contains("OMEGA GENERAL LEDGER")) { %>
			        <div class="tabbertab" title="GL">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
   				         <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.glJournalLineNumber"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="glJournalLineNumber" styleClass="textRequired"/>
					       </td>
			            </tr>
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.glPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="glPostingType" styleClass="comboRequired">
			                      <html:options property="glPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="preference.prompt.enableGlJournalBatch"/>
			               </td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableGlJournalBatch"/>
					       </td>
			            </tr>

			            <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="preference.prompt.enableGlRecomputeCoaBalance"/>
			               </td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableGlRecomputeCoaBalance"/>
					       </td>
			            </tr>


			             <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="preference.prompt.allowSuspensePosting"/>
			               </td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="allowSuspensePosting"/>
					       </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.yearEndCloseRestriction"/>
				            </td>
				            <td width="425" height="25" class="control" colspan="3">
				               <html:checkbox property="glYearEndCloseRestriction"/>
				            </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA PAYABLES")) { %>
			        <div class="tabbertab" title="Payables">
					<html:hidden property="enableNextSupplierCode" value=""/>
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apJournalLineNumber"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apJournalLineNumber" styleClass="textRequired"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apWTaxRealization"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apWTaxRealization" styleClass="comboRequired">
			                      <html:options property="apWTaxRealizationList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apGlPostingType"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apGlPostingType" styleClass="comboRequired">
			                      <html:options property="apGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApVoucherBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApVoucherBatch"/>
					       </td>
			            </tr>

                                    <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApPOBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApPOBatch"/>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApCheckBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApCheckBatch"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.apUseSupplierPulldown"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apUseSupplierPulldown"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.apAutoGenerateSupplierCode"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apAutoGenerateSupplierCode" />
					       </td>
			            </tr>
						<tr>
						  <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.nextSupplierCode"/>
			               </td>
						   <td width="395" height="25" class="control" colspan="3">
							   <html:text property="nextSupplierCode" styleClass="text"/>
						   </td>
						</tr>
                        <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apFindCheckDefaultType"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apFindCheckDefaultType" styleClass="comboRequired">
			                      <html:options property="apFindCheckDefaultTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultCheckDate"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apDefaultCheckDate" styleClass="comboRequired">
			                      <html:options property="apDefaultCheckDateList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultChecker"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apDefaultChecker" size="25" maxlength="50" styleClass="text"/>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultApprover"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apDefaultApprover" size="25" maxlength="50" styleClass="text"/>
					       </td>
			            </tr>
						<tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apReferenceNumberValidation"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apReferenceNumberValidation"/>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apUseAccruedVat"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apUseAccruedVat"/>
					       </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.accruedVatAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="accruedVatAccount" size="30" maxlength="255" styleClass="text"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accruedVatAccount','accruedVatAccountDescription');"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.accruedVatAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="accruedVatAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.pettyCashAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="pettyCashAccount" size="30" maxlength="255" styleClass="text"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('pettyCashAccount','pettyCashAccountDescription');"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.pettyCashAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="pettyCashAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
  						<tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.checkVoucherDataSource"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apCheckVoucherDataSource" styleClass="comboRequired">
			                      <html:options property="apCheckVoucherDataSourceList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apDefaultPRTaxCode"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apDefaultPRTaxCode" styleClass="comboRequired">
			                      <html:options property="apDefaultPRTaxCodeList"/>
			                   </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apDefaultPRCurrency"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apDefaultPRCurrency" styleClass="comboRequired">
			                      <html:options property="apDefaultPRCurrencyList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apShowPRCost"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:checkbox property="apShowPRCost"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.agingBucket"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:text property="apAgingBucket" size="3" maxlength="3" styleClass="textRequired"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.debitMemoOverrideCost"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:checkbox property="apDebitMemoOverrideCost"/>
				            </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA RECEIVABLES")) { %>
				    <div class="tabbertab" title="Receivables">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arInvoiceLineNumber"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="arInvoiceLineNumber" styleClass="textRequired"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arWTaxRealization"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arWTaxRealization" styleClass="comboRequired">
			                      <html:options property="arWTaxRealizationList"/>
			                  </html:select>
					       </td>
			            </tr>
						<tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.defaultWTaxCode"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="defaultWTaxCode" styleClass="combo">
			                      <html:options property="defaultWTaxCodeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arGlPostingType"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arGlPostingType" styleClass="comboRequired">
			                      <html:options property="arGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArInvoiceBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArInvoiceBatch"/>
					       </td>
			            </tr>
			            
			              <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.salesOrderSalespersonRequired"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arSoSalespersonRequired"/>
					       </td>
			            </tr>
			            
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.invoiceSalespersonRequired"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arInvcSalespersonRequired"/>
					       </td>
			            </tr>
			            

			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArInvoiceInterestGeneration"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArInvoiceInterestGeneration"/>
					       </td>
			            </tr>

			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArReceiptBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArReceiptBatch"/>
					       </td>
			            </tr>

			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArMiscReceiptBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArMiscReceiptBatch"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.arUseCustomerPulldown"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arUseCustomerPulldown"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.arAutoGenerateCustomerCode"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arAutoGenerateCustomerCode"/>
					       </td>
			            </tr>
						<tr>
				   		  <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.nextCustomerCode"/>
			               </td>
						   <td width="395" height="25" class="control" colspan="3">
							   <html:text property="nextCustomerCode" styleClass="text"/>
						   </td>
						</tr>
					    <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arFindReceiptDefaultType"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arFindReceiptDefaultType" styleClass="comboRequired">
			                      <html:options property="arFindReceiptDefaultTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
			            	<td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.salesInvoiceDataSource"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="arSalesInvoiceDataSource" styleClass="comboRequired">
			                      <html:options property="arSalesInvoiceDataSourceList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.customerDepositAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="customerDepositAccount" size="30" maxlength="255" styleClass="text"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('customerDepositAccount','customerDepositAccountDescription');"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.customerDepositAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="customerDepositAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.agingBucket"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:text property="arAgingBucket" size="3" maxlength="3" styleClass="textRequired"/>
				            </td>
			            </tr>

			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arAutoComputeCogs"/>
				            </td>
				            <td width="113" height="25" class="control">
				               <html:checkbox property="arAutoComputeCogs"/>
				            </td>
				        </tr>



				        <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arMonthlyInterestRate"/>
				            </td>

				        <td width="158" height="25" class="control">
                   				<html:text property="arMonthlyInterestRate" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>
				        </tr>

				        <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arAllowPriorDate"/>
				            </td>
				            <td width="112" height="25" class="control">
				               <html:checkbox property="arAllowPriorDate"/>
				            </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arEnablePaymentTerm"/>
				            </td>
				            <td width="395" height="25" class="control">
				              <html:checkbox property="arEnablePaymentTerm"/>
				            </td>
				        </tr>
				        <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arDisableSalesPrice"/>
				            </td>
				            <td width="395" height="25" class="control">
				              <html:checkbox property="arDisableSalesPrice"/>
				            </td>
			            </tr>
			            <tr>
			                <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arValidateEmail"/>
				            </td>
				            <td width="395" height="25" class="control">
				              <html:checkbox property="arValidateCustomerEmail"/>
				            </td>
			            </tr>
			            <tr>
			                <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arCheckStock"/>
				            </td>
				            <td width="395" height="25" class="control">
				              <html:checkbox property="arCheckStock"/>
				            </td>
			            </tr>

			            <tr>
			                <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arDetailedReceivable"/>
				            </td>
				            <td width="395" height="25" class="control">
				              <html:checkbox property="arDetailedReceivable"/>
				            </td>
			            </tr>

					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA CASH MANAGEMENT")) { %>
					<div class="tabbertab" title="CM">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					    <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.cmGlPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="cmGlPostingType" styleClass="comboRequired">
			                      <html:options property="cmGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
						<tr>
							<td class="prompt" width="140" height="25">
							   	<bean:message key="preference.prompt.cmUseBankForm"/>
							</td>
					       	<td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="cmUseBankForm"/>
					       </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
			        <div class="tabbertab" title="Inventory">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
		                <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.centralWarehouseBranch"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                   <html:select property="centralWarehouseBranch" styleClass="comboRequired">
			                      <html:options property="branchList"/>
			                  </html:select>
					       </td>
			             </tr>
			             
			              <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.centralWarehouseLocation"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                   <html:select property="centralWarehouseLocation" styleClass="comboRequired">
			                      <html:options property="locationList"/>
			                  </html:select>
					       </td>
			             </tr>
			             
			             
			             
			             
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invInventoryLineNumber"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invInventoryLineNumber" styleClass="textRequired" maxlength="3"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invQuantityPrecisionUnit"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invQuantityPrecisionUnit" styleClass="textRequired" maxlength="3"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invCostPrecisionUnit"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invCostPrecisionUnit" styleClass="textRequired" maxlength="3"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invGlPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="invGlPostingType" styleClass="comboRequired">
			                      <html:options property="invGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.enableInvShift"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvShift"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.enableInvBUABatch"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvBUABatch"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.enableInvPosIntegration"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvPosIntegration"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.enableInvPosAutoPostUpload"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvPosAutoPostUpload"/>
					       </td>
			            </tr>
			            <tr>
						<td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.invItemLocationShowAll"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:checkbox property="invItemLocationShowAll"/>
				            </td>
			            </tr>

			            <tr>
						<td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.invItemLocationAddByItemList"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:checkbox property="invItemLocationAddByItemList"/>
				            </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invPosAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPosAdjustmentAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invPosAdjustmentAccount','invPosAdjustmentAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invPosAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPosAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invGeneralAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invGeneralAdjustmentAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invGeneralAdjustmentAccount','invGeneralAdjustmentAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invGeneralAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invGeneralAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invIssuanceAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invIssuanceAdjustmentAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invIssuanceAdjustmentAccount','invIssuanceAdjustmentAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invIssuanceAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invIssuanceAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invProductionAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invProductionAdjustmentAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invProductionAdjustmentAccount','invProductionAdjustmentAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invProductionAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invProductionAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invVarianceAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invVarianceAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invVarianceAccount','invVarianceAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invVarianceAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invVarianceAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invWastageAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invWastageAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invWastageAccount','invWastageAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invWastageAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invWastageAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			             <!--<tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invSalesTaxCodeAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invSalesTaxCodeAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invSalesTaxCodeAccount','invSalesTaxCodeAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invSalesTaxCodeAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invSalesTaxCodeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invPreparedFoodTaxCodeAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPreparedFoodTaxCodeAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invPreparedFoodTaxCodeAccount','invPreparedFoodTaxCodeAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invPreparedFoodTaxCodeAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPreparedFoodTaxCodeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr> -->
			        </table>
					</div>
					<div class="tabbertab" title="Mail">
						<table border="0" cellpadding="0" cellspacing="0" width="565" height="25">


				           <tr>
						        <td class="prompt" width="565" height="25" colspan="4">
				                </td>
		             	   </tr>

		             	   <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailHost"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailHost" styleClass="text"/>
						       </td>



			               </tr>

			                <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailSocketFactoryPort"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailSocketFactoryPort" styleClass="text"/>
						       </td>

			               </tr>

			                 <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailPort"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailPort" styleClass="text"/>
						       </td>

			               </tr>

			                 <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailFrom"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailFrom" styleClass="text"/>
						       </td>

			               </tr>

			              

			                 <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailAuthenticator"/>
								 </td>
						         <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="mailAuthenticator"/>
					       </td>

			               </tr>


     						<tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailSenderEmailPasspword"/>
								 </td>
						         <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="mailPassword"  styleClass="text"/>
					       </td>

			            

	           			  </tr>

			                 <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailCc"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailCc" styleClass="text"/>
						       </td>

			               </tr>

			                 </tr>

			                 <tr>
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailBcc"/>
								 </td>
						        <td width="425" height="25" class="control" colspan="3">
				                  <html:text property="mailBcc" styleClass="text"/>

			               </tr>
			               
			               
			               	<tr colspan="2">
								<td class="prompt" width="170" height="25">
								   <bean:message key="preference.prompt.mailConfig"/>
								 </td>
						         <td width="425" height="25" class="control" colspan="3">
			                   <html:textarea property="mailConfig" rows="7" cols="40" styleClass="text"/>
					       </td>

			            

	           			  </tr>
			        	</table>



					</div>


					<div class="tabbertab" title="Misc">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDiscountAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDiscountAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('miscPosDiscountAccount','miscPosDiscountAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDiscountAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDiscountAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosGiftCertificateAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosGiftCertificateAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('miscPosGiftCertificateAccount','miscPosGiftCertificateAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosGiftCertificateAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosGiftCertificateAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosServiceChargeAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosServiceChargeAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('miscPosServiceChargeAccount','miscPosServiceChargeAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosServiceChargeAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosServiceChargeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDineInChargeAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDineInChargeAccount" styleClass="textRequired" maxlength="25"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('miscPosDineInChargeAccount','miscPosDineInChargeAccountDescription');"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDineInChargeAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDineInChargeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			        </table>
					</div>
					<% } %>
			        </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	        </tr>
	        </logic:equal>
	        <logic:equal name="adPreferenceForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	        <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <% if (user.getUserApps().contains("OMEGA GENERAL LEDGER")) { %>
			        <div class="tabbertab" title="General Ledger">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
   				         <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.glJournalLineNumber"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="glJournalLineNumber" styleClass="textRequired" disabled="true"/>
					       </td>
			            </tr>
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.glPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="glPostingType" styleClass="comboRequired" disabled="true">
			                      <html:options property="glPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="preference.prompt.enableGlJournalBatch"/>
			               </td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableGlJournalBatch" disabled="true"/>
					       </td>
			            </tr>
			             <tr>
				           <td class="prompt" width="140" height="25">
			                  <bean:message key="preference.prompt.allowSuspensePosting"/>
			               </td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="allowSuspensePosting" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.yearEndCloseRestriction"/>
				            </td>
				            <td width="425" height="25" class="control" colspan="3">
				               <html:checkbox property="glYearEndCloseRestriction" disabled="true"/>
				            </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA PAYABLES")) { %>
			        <div class="tabbertab" title="AP">
					<html:hidden property="enableNextSupplierCode" value=""/>
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apJournalLineNumber"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apJournalLineNumber" styleClass="textRequired" disabled="true"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apWTaxRealization"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apWTaxRealization" styleClass="comboRequired" disabled="true">
			                      <html:options property="apWTaxRealizationList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apGlPostingType"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apGlPostingType" styleClass="comboRequired" disabled="true">
			                      <html:options property="apGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApVoucherBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApVoucherBatch" disabled="true"/>
					       </td>
			            </tr>

                                    <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApPOBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApPOBatch" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableApCheckBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableApCheckBatch" disabled="true"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.apUseSupplierPulldown"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apUseSupplierPulldown" disabled="true"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.apAutoGenerateSupplierCode"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apAutoGenerateSupplierCode" disabled="true"/>
					       </td>
			            </tr>
						<tr>
						  <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.nextSupplierCode"/>
			               </td>
						   <td width="395" height="25" class="control" colspan="3">
							   <html:text property="nextSupplierCode" styleClass="text" disabled="true"/>
						   </td>
						</tr>
                        <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apFindCheckDefaultType"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apFindCheckDefaultType" styleClass="comboRequired" disabled="true">
			                      <html:options property="apFindCheckDefaultTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultCheckDate"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="apDefaultCheckDate" styleClass="comboRequired" disabled="true">
			                      <html:options property="apDefaultCheckDateList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultChecker"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apDefaultChecker" size="25" maxlength="50" styleClass="text" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apDefaultApprover"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="apDefaultApprover" size="25" maxlength="50" styleClass="text" disabled="true"/>
					       </td>
			            </tr>
						<tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apReferenceNumberValidation"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apReferenceNumberValidation" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.apUseAccruedVat"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="apUseAccruedVat" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.accruedVatAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="accruedVatAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.accruedVatAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="accruedVatAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.pettyCashAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="pettyCashAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.pettyCashAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="pettyCashAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
  						<tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.checkVoucherDataSource"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apCheckVoucherDataSource" styleClass="comboRequired" disabled="true">
			                      <html:options property="apCheckVoucherDataSourceList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apDefaultPRTaxCode"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apDefaultPRTaxCode" styleClass="comboRequired" disabled="true">
			                      <html:options property="apDefaultPRTaxCodeList"/>
			                   </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apDefaultPRCurrency"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="apDefaultPRCurrency" styleClass="comboRequired" disabled="true">
			                      <html:options property="apDefaultPRCurrencyList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.apShowPRCost"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:checkbox property="apShowPRCost" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.agingBucket"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:text property="apAgingBucket" size="3" maxlength="3" styleClass="textRequired" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.debitMemoOverrideCost"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:checkbox property="apDebitMemoOverrideCost"/>
				            </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA RECEIVABLES")) { %>
				    <div class="tabbertab" title="AR">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arInvoiceLineNumber"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="arInvoiceLineNumber" styleClass="textRequired" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arWTaxRealization"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arWTaxRealization" styleClass="comboRequired" disabled="true">
			                      <html:options property="arWTaxRealizationList"/>
			                  </html:select>
					       </td>
			            </tr>
						<tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.defaultWTaxCode"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="defaultWTaxCode" styleClass="combo" disabled="true">
			                      <html:options property="defaultWTaxCodeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arGlPostingType"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arGlPostingType" styleClass="comboRequired" disabled="true">
			                      <html:options property="arGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArInvoiceBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArInvoiceBatch" disabled="true"/>
					       </td>
			            </tr>
			            
			              <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.salesOrderSalespersonRequired"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arSoSalespersonRequired" disabled="true"/>
					       </td>
			            </tr>
			            
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.invoiceSalespersonRequired"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arInvcSalespersonRequired" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArInvoiceInterestGeneration"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArInvoiceInterestGeneration" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArReceiptBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArReceiptBatch" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.enableArMiscReceiptBatch"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableArMiscReceiptBatch" disabled="true"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.arUseCustomerPulldown"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arUseCustomerPulldown" disabled="true"/>
					       </td>
			            </tr>
						<tr>
				           <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.arAutoGenerateCustomerCode"/>
			               </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:checkbox property="arAutoGenerateCustomerCode" disabled="true"/>
					       </td>
			            </tr>
						<tr>
				   		  <td class="prompt" width="170" height="25">
			                  <bean:message key="preference.prompt.nextCustomerCode"/>
			               </td>
						   <td width="395" height="25" class="control" colspan="3">
							   <html:text property="nextCustomerCode" styleClass="text" disabled="true"/>
						   </td>
						</tr>
					    <tr>
						   <td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.arFindReceiptDefaultType"/>
						   </td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:select property="arFindReceiptDefaultType" styleClass="comboRequired" disabled="true">
			                      <html:options property="arFindReceiptDefaultTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
			            	<td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.salesInvoiceDataSource"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:select property="arSalesInvoiceDataSource" styleClass="comboRequired" disabled="true">
			                      <html:options property="arSalesInvoiceDataSourceList"/>
			                  </html:select>
				            </td>
			            </tr>
			            <tr>
					         <td class="prompt" width="170" height="25">
					            <bean:message key="preference.prompt.customerDepositAccount"/>
					         </td>
					         <td width="395" height="25" class="control" colspan="3">
					            <html:text property="customerDepositAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							    <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					         </td>
				        </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.customerDepositAccountDescription"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				               <html:text property="customerDepositAccountDescription" size="50" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arAutoComputeCogs"/>
				            </td>
				            <td width="113" height="25" class="control">
				               <html:checkbox property="arAutoComputeCogs" disabled="true"/>
				            </td>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arAllowPriorDate"/>
				            </td>
				            <td width="112" height="25" class="control">
				               <html:checkbox property="arAllowPriorDate" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.agingBucket"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:text property="arAgingBucket" size="3" maxlength="3" styleClass="textRequired" disabled="true"/>
				            </td>
			            </tr>
			            <tr>
				            <td class="prompt" width="170" height="25">
				               <bean:message key="preference.prompt.arEnablePaymentTerm"/>
				            </td>
				            <td width="395" height="25" class="control" colspan="3">
				              <html:checkbox property="arEnablePaymentTerm" disabled="true"/>
				            </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA CASH MANAGEMENT")) { %>
					<div class="tabbertab" title="Cash Mgt">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					    <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.cmGlPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="cmGlPostingType" styleClass="comboRequired" disabled="true">
			                      <html:options property="cmGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
						<tr>
							<td class="prompt" width="140" height="25">
							   	<bean:message key="preference.prompt.cmUseBankForm"/>
							</td>
					       	<td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="cmUseBankForm" disabled="true"/>
					       </td>
			            </tr>
					</table>
					</div>
					<% } %>
					<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
			        <div class="tabbertab" title="Inventory">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			             
			             <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.centralWarehouseBranch"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                   <html:select property="centralWarehouseBranch" styleClass="comboRequired" disabled="true">
			                      <html:options property="branchList"/>
			                  </html:select>
					       </td>
			             </tr>
			             
			              <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.centralWarehouseLocation"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                   <html:select property="centralWarehouseLocation" styleClass="comboRequired" disabled="true">
			                      <html:options property="locationList"/>
			                  </html:select>
					       </td>
			             </tr>
			             
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invInventoryLineNumber"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invInventoryLineNumber" styleClass="textRequired" maxlength="3" disabled="true"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invQuantityPrecisionUnit"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invQuantityPrecisionUnit" styleClass="textRequired" maxlength="3" disabled="true"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invCostPrecisionUnit"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invCostPrecisionUnit" styleClass="textRequired" maxlength="3" disabled="true"/>
					       </td>
			             </tr>
			             <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invGlPostingType"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:select property="invGlPostingType" styleClass="comboRequired" disabled="true">
			                      <html:options property="invGlPostingTypeList"/>
			                  </html:select>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.enableInvShift"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvShift" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.enableInvBUABatch"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvBUABatch" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.enableInvPosIntegration"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvPosIntegration" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.enableInvPosAutoPostUpload"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:checkbox property="enableInvPosAutoPostUpload" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invPosAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPosAdjustmentAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="preference.prompt.invPosAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invPosAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>










			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invGeneralAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invGeneralAdjustmentAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invGeneralAdjustmentAccount','invGeneralAdjustmentAccountDescription');" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invGeneralAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invGeneralAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invIssuanceAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invIssuanceAdjustmentAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invIssuanceAdjustmentAccount','invIssuanceAdjustmentAccountDescription');" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invIssuanceAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invIssuanceAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invProductionAdjustmentAccount"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invProductionAdjustmentAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('invProductionAdjustmentAccount','invProductionAdjustmentAccountDescription');" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.invProductionAdjustmentAccountDescription"/>
							</td>
					       <td width="425" height="25" class="control" colspan="3">
			                  <html:text property="invProductionAdjustmentAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>

			        </table>
					</div>
					<div class="tabbertab" title="MISC">
   				    <table border="0" cellpadding="0" cellspacing="0" width="565" height="25">
					     <tr>
					        <td class="prompt" width="565" height="25" colspan="4">
			                </td>
			             </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDiscountAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDiscountAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDiscountAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDiscountAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosGiftCertificateAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosGiftCertificateAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosGiftCertificateAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosGiftCertificateAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosServiceChargeAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosServiceChargeAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosServiceChargeAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosServiceChargeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDineInChargeAccount"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDineInChargeAccount" styleClass="textRequired" maxlength="25" disabled="true"/>
			                  <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
					       </td>
			            </tr>
			            <tr>
							<td class="prompt" width="170" height="25">
							   <bean:message key="preference.prompt.miscPosDineInChargeAccountDescription"/>
							</td>
					       <td width="395" height="25" class="control" colspan="3">
			                  <html:text property="miscPosDineInChargeAccountDescription" styleClass="text" size="50" maxlength="255" style="font-size:8pt;" disabled="true"/>
					       </td>
			            </tr>
			        </table>
					</div>
					<% } %>
			        </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	        </tr>
	        </logic:equal>
	     <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
		     <logic:equal name="adPreferenceForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton">
			       <bean:message key="button.save"/>
			  	</html:submit>
			  </logic:equal>
			    <html:submit property="closeButton" styleClass="mainButton">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			 <logic:equal name="adPreferenceForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.save"/>
			    </html:submit>
			 </logic:equal>
			    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 </td>
		 </tr>
	     <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["glJournalLineNumber"] != null &&
        document.forms[0].elements["glJournalLineNumber"].disabled == false)
        document.forms[0].elements["glJournalLineNumber"].focus()
   // -->
</script>
</body>
</html>
