<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="transactionLog.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/adRepTransactionLog.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="350">
      <tr valign="top">
        <td width="187" height="350"></td> 
        <td width="581" height="350">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="350" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="350" height="1" colspan="6" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="transactionLog.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="6" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
         <tr><td>
				<div class="tabber">
			    <div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr> 
					    <td class="prompt" width="575" height="25" colspan="6">
						</td>
					</tr>
			        <tr>
						<td class="prompt" width="102" height="25">
							<bean:message key="transactionLog.prompt.type"/>
						</td>
						<td width="473" height="25" class="control" colspan="5">
	            			<html:select property="type" styleClass="combo">
			      				<html:options property="typeList"/>
			   				</html:select>
	         			</td>
         			</tr>
         			<tr>
				     <td class="prompt" width="102" height="25">
			            <bean:message key="transactionLog.prompt.docDate"/>
			         </td>
			         <td class="prompt" width="100" height="25">
			            <bean:message key="transactionLog.prompt.docDateFrom"/>
			         </td>
			         <td width="91" height="25" class="control">
			            <html:text property="docDateFrom" size="10" maxlength="10" styleClass="text"/>
			         </td>
			         <td class="prompt" width="100" height="25">
			            <bean:message key="transactionLog.prompt.docDateTo"/>
			         </td>
			         <td width="91" height="25" class="control">
			            <html:text property="docDateTo" size="10" maxlength="10" styleClass="text"/>
			         </td>
		         </tr>
         			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.createdBy"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="createdBy" styleClass="combo">
			      				<html:options property="createdByList"/>
			   				</html:select>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateCreatedFrom"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateCreatedFrom" size="10" maxlength="10" styleClass="text"/>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateCreatedTo"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateCreatedTo" size="10" maxlength="10" styleClass="text"/>
	         			</td>
         			</tr>
	     			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.lastModifiedBy"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="lastModifiedBy" styleClass="combo">
			      				<html:options property="lastModifiedByList"/>
			   				</html:select>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateLastModifiedFrom"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateLastModifiedFrom" size="10" maxlength="10" styleClass="text"/>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateLastModifiedTo"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateLastModifiedTo" size="10" maxlength="10" styleClass="text"/>
	         			</td>
         			</tr>
         			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.approvedRejectedBy"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="approvedRejectedBy" styleClass="combo">
			      				<html:options property="approvedRejectedByList"/>
			   				</html:select>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateApprovedRejectedFrom"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateApprovedRejectedFrom" size="10" maxlength="10" styleClass="text"/>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateApprovedRejectedTo"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateApprovedRejectedTo" size="10" maxlength="10" styleClass="text"/>
	         			</td>
         			</tr>
         			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.postedBy"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="postedBy" styleClass="combo">
			      				<html:options property="postedByList"/>
			   				</html:select>
	        			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.datePostedFrom"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="datePostedFrom" size="10" maxlength="10" styleClass="text"/>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.datePostedTo"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="datePostedTo" size="10" maxlength="10" styleClass="text"/>
	         			</td>
         			</tr>
         			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.deletedBy"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="deletedBy" styleClass="combo">
			      				<html:options property="deletedByList"/>
			   				</html:select>
	        			</td>	         			
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateDeletedFrom"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateDeletedFrom" size="10" maxlength="10" styleClass="text"/>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	            			<bean:message key="transactionLog.prompt.dateDeletedTo"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:text property="dateDeletedTo" size="10" maxlength="10" styleClass="text"/>
	         			</td>
         			</tr>
         			<tr>
	         			<td class="prompt" width="102" height="25">
	            			<bean:message key="transactionLog.prompt.orderBy"/>
	         			</td>
	         			<td width="282" height="25" class="control" colspan="3">
	            			<html:select property="orderBy" styleClass="combo">
			      			<html:options property="orderByList"/>
			   				</html:select>
	         			</td>
	         			<td class="prompt" width="100" height="25">
	           				<bean:message key="transactionLog.prompt.viewType"/>
	         			</td>
	         			<td width="91" height="25" class="control">
	            			<html:select property="viewType" styleClass="combo">
			      				<html:options property="viewTypeList"/>
			   				</html:select>
	         			</td>
         			</tr>
				</table>
				</div>
			    <div class="tabbertab" title="Branch Map">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        	<tr> 
						<td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
					<nested:iterate property="adRepBrTransactionLogList">
		            <tr>
		            	<td class="prompt" height="25">
						<nested:write property="brBranchCode"/>
			            </td>
			            <td class="prompt" height="25"> - </td>
			        	<td class="prompt" width="140" height="25">
						<nested:write property="brName"/>
			            </td>
			            <td width="435" height="25" class="control" colspan="3">
						<nested:checkbox property="branchCheckbox"/>
			            </td>
		            </tr>
					</nested:iterate>
		        </table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
		 </td></tr>
	     <tr>
	        <td width="575" height="50" colspan="6"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="6" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["type"] != null)
             document.forms[0].elements["type"].focus()
   // -->
</script>
<logic:equal name="adRepTransactionLogForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
