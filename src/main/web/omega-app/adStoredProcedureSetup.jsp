<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="storedProcedureSetup.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/adStoredProcedureSetup.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="700">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
         <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="storedProcedureSetup.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="adStoredProcedureSetupForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="adStoredProcedureSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <tr>
	        <td class="prompt" width="150" height="25">
               <bean:message key="storedProcedureSetup.prompt.enableGlGeneralLedgerReport"/>
            </td>
		    <td width="50" height="25" class="control">
		       <html:checkbox property="enableGlGeneralLedgerReport"/>
		    </td>
			<td class="prompt" width="10" height="25">
	           <bean:message key="storedProcedureSetup.prompt.nameGlGeneralLedgerReport"/>
	        </td>
	        <td width="150" height="25" class="control">
	           <html:text property="nameGlGeneralLedgerReport" size="40" maxlength="150" styleClass="textRequired"/>
	        </td>
		</tr>
		
		<tr>
	        <td class="prompt" width="150" height="25">
               <bean:message key="storedProcedureSetup.prompt.enableGlTrialBalanceReport"/>
            </td>
		    <td width="50" height="25" class="control">
		       <html:checkbox property="enableGlTrialBalanceReport"/>
		    </td>
			<td class="prompt" width="10" height="25">
	           <bean:message key="storedProcedureSetup.prompt.nameGlTrialBalanceReport"/>
	        </td>
	        <td width="150" height="25" class="control">
	           <html:text property="nameGlTrialBalanceReport" size="40" maxlength="150" styleClass="textRequired"/>
	        </td>
		</tr>
		
		<tr>
	        <td class="prompt" width="150" height="25">
                    <bean:message key="storedProcedureSetup.prompt.enableGlIncomeStatementReport"/>
                </td>
                <td width="50" height="25" class="control">
                    <html:checkbox property="enableGlIncomeStatementReport"/>
                </td>
                <td class="prompt" width="10" height="25">
	           <bean:message key="storedProcedureSetup.prompt.nameGlIncomeStatementReport"/>
	        </td>
	        <td width="150" height="25" class="control">
	           <html:text property="nameGlIncomeStatementReport" size="40" maxlength="150" styleClass="textRequired"/>
	        </td>
		</tr>
		
		<tr>
	        <td class="prompt" width="150" height="25">
               <bean:message key="storedProcedureSetup.prompt.enableGlBalanceSheetReport"/>
                </td>
		    <td width="50" height="25" class="control">
		       <html:checkbox property="enableGlBalanceSheetReport"/>
		    </td>
			<td class="prompt" width="10" height="25">
	           <bean:message key="storedProcedureSetup.prompt.nameGlBalanceSheetReport"/>
	        </td>
	        <td width="150" height="25" class="control">
	           <html:text property="nameGlBalanceSheetReport" size="40" maxlength="150" styleClass="textRequired"/>
	        </td>
		</tr>
                
                <tr>
                    <td class="prompt" width="150" height="25">
                        <bean:message key="storedProcedureSetup.prompt.enableArStatementOfAccountReport"/>
                    </td>
                    <td width="50" height="25" class="control">
                        <html:checkbox property="enableArStatementOfAccountReport"/>
                    </td>
                    <td class="prompt" width="10" height="25">
                       <bean:message key="storedProcedureSetup.prompt.nameArStatementOfAccountReport"/>
                    </td>
                    <td width="150" height="25" class="control">
                       <html:text property="nameArStatementOfAccountReport" size="40" maxlength="150" styleClass="textRequired"/>
                    </td>
		</tr>
               
         </logic:equal>
         
	        
	     <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
		     <logic:equal name="adStoredProcedureSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton">
			       <bean:message key="button.save"/>
			  	</html:submit>
			  </logic:equal>

			    <html:submit property="closeButton" styleClass="mainButton">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			 <logic:equal name="adStoredProcedureSetupForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="saveButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.save"/>
			    </html:submit>
			 </logic:equal>

			    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>		         
			 </td>
		 </tr>
	     <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["approvalQueueExpiration"] != null &&
        document.forms[0].elements["approvalQueueExpiration"].disabled == false)
        document.forms[0].elements["approvalQueueExpiration"].focus()
   // -->
</script>
</body>
</html>
