<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="user.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/adUser.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
         <td width="187" height="510"></td> 
         <td width="581" height="510">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="user.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="adUserForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.userName"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="userName" size="25" maxlength="25" styleClass="textRequired"/>
		       </td>
		       
		       
		       <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.userDepartment"/>
               </td>
		       <td width="147" height="25" class="control">
					<html:select property="userDepartment" styleClass="comboRequired" style="width:130;">
						<html:options property="userDepartmentList"/>
					</html:select>
				</td>
		       
		       
		       
		     
            </tr>
	        <tr>		       		       		 
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.description"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="description" size="25" maxlength="50" styleClass="textRequired"/>
		       </td>
		       
		       <td class="prompt" width="220" height="25">
               		<bean:message key="user.prompt.head"/>
	           </td>
			   <td width="128" height="25" class="control">
			       <html:checkbox property="head"/>
			   </td>

		       
		       
		       		 
            </tr>
            <tr>		       		       		 
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.position"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="position" size="25" maxlength="50" styleClass="text"/>
		       </td>
		       
		       <td class="prompt" width="220" height="25">
               		<bean:message key="user.prompt.inspector"/>
	           </td>
			   <td width="128" height="25" class="control">
			       <html:checkbox property="inspector"/>
			   </td>
	 
            </tr>
            
            <tr>		       		       		 
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.emailAddress"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="emailAddress" size="25" maxlength="100" styleClass="text"/>
		       </td>

	 
            </tr>
            
            <tr>		       		       		 
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.password"/>
               </td>
		       <td width="148" height="25" class="control" colspan="3">
                  <html:password property="password" size="15" maxlength="50" styleClass="textRequired"/>
		       </td>		 
            </tr>
	        <tr>
				<td class="prompt" width="140" height="25">
					<bean:message key="user.prompt.confirmPassword"/>
				</td>
					<td width="148" height="25" class="control">
					<html:password property="confirmPassword" size="15" maxlength="25" styleClass="textRequired"/>
				</td>
				<td class="prompt" width="140" height="25">
					<bean:message key="user.prompt.passwordExpirationCode"/>
				</td>
					<td width="147" height="25" class="control">
					<html:select property="passwordExpirationCode" styleClass="comboRequired" style="width:130;">
						<html:options property="passwordExpirationCodeList"/>
					</html:select>
				</td>		       		       		 
            </tr>            
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.passwordExpirationDays"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="passwordExpirationDays" size="15" maxlength="25" styleClass="text"/>
		       </td>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.passwordExpirationAccess"/>
               </td>
		       <td width="147" height="25" class="control">
                  <html:text property="passwordExpirationAccess" size="15" maxlength="25" styleClass="text"/>
		       </td>		       		 
            </tr>
	        <tr>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.dateFrom"/>
               </td>
		       <td width="148" height="25" class="control">
                  <html:text property="dateFrom" size="10" maxlength="20" styleClass="textRequired"/>
		       </td>
	           <td class="prompt" width="140" height="25">
                  <bean:message key="user.prompt.dateTo"/>
               </td>
		       <td width="147" height="25" class="control">
                  <html:text property="dateTo" size="10" maxlength="20" styleClass="text"/>
		       </td>		       		 
            </tr>            
	        </logic:equal>
	        <tr>	        
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	        	        
	           <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		       <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		       </logic:equal>
		       <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		          <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.update"/>
		          </html:submit>
		          <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.cancel"/>
		          </html:submit>
		       </logic:equal>
		       </logic:equal>
		          <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.close"/>
		          </html:submit>
		       </div>
               </td>
	        </tr>
	        <tr valign="top">
	           <td width="575" height="185" colspan="4">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="user.gridTitle.USRDetails"/>
	                 </td>
	              </tr>	           
   
	            <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			    	<td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="user.prompt.userDepartment"/>
			       </td>

			       <td width=100 height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="user.prompt.userName"/>
			       </td>
			       
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="user.prompt.description"/>
			       </td>
			       <td width="150" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="user.prompt.emailAddress"/>
			       </td>
                </tr>			                                    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adUSRList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			      <td width="150" height="1" class="gridLabel">
			            <nested:write property="userDepartment"/>
			         </td>
			         
			         <td width="150" height="1" class="gridLabel">
			            <nested:write property="userName"/>
			         </td>
			         
			         <td width="200" height="1" class="gridLabel">
			            <nested:write property="description"/>
			         </td>
			         <td width="150" height="1" class="gridLabel">
			            <nested:write property="emailAddress"/>
			         </td>			         
			         <td width="149" align="center" height="1" colspan="2">
			         <div id="buttons">
			         <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="745" height="1" colspan="5">
			         </td>			      
			         <td width="149" align="center" height="1">
			         <div id="buttons">
			         <logic:notEqual name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="userResponsibilityButton" styleClass="gridButtonHuge">
			               <bean:message key="button.userResponsibility"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			         <logic:notEqual name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="userResponsibilityButton" styleClass="gridButtonHuge" disabled="true">
			               <bean:message key="button.userResponsibility"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         </td>
			      </tr>	              			      
			      </nested:iterate>
			      </logic:equal>

			      <logic:equal name="adUserForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="adUSRList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="user.prompt.userName"/>
			         </td>
			         <td width="520" height="1" class="gridLabel" colspan="2">
			            <nested:write property="userName"/>
			         </td>
			         <td width="199" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
				     <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adUserForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.edit"/>
  	                    </nested:submit>
  	                 <logic:equal name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                    <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.delete"/>
  	                    </nested:submit>
				     </logic:equal>  	                      
				     </logic:equal>
				     </div>
			         </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
			            <bean:message key="user.prompt.description"/>
			         </td>
			         <td width="520" height="1" class="gridLabel" colspan="2">
			            <nested:write property="description"/>
			         </td>
			         <td width="199" align="center" height="1">
			         <div id="buttons">
			         <logic:notEqual name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="userResponsibilityButton" styleClass="gridButtonHuge">
			               <bean:message key="button.userResponsibility"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			         <logic:notEqual name="adUserForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
			            <nested:submit property="userResponsibilityButton" styleClass="gridButtonHuge" disabled="true">
			               <bean:message key="button.userResponsibility"/>
			            </nested:submit>
			         </logic:notEqual>
			         </div>
			         </td>
			      </tr>	              
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="user.prompt.passwordExpirationCode"/>
                     </td>
                     <td width="719" height="1" class="gridLabel" colspan="3">
                        <nested:write property="passwordExpirationCode"/>
                     </td>
			      </tr>	
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="user.prompt.passwordExpirationDays"/>
                     </td>
                     <td width="270" height="1" class="gridLabel">
                        <nested:write property="passwordExpirationDays"/>
                     </td>
			         <td width="250" height="1" class="gridHeader">
                        <bean:message key="user.prompt.passwordExpirationAccess"/>
                     </td>
                     <td width="199" height="1" class="gridLabel">
                        <nested:write property="passwordExpirationAccess"/>
                     </td>
			      </tr>
	              <tr bgcolor="<%= rowBgc %>">
			         <td width="175" height="1" class="gridHeader">
                        <bean:message key="user.prompt.dateFrom"/>
                     </td>
                     <td width="270" height="1" class="gridLabel">
                        <nested:write property="dateFrom"/>
                     </td>
			         <td width="250" height="1" class="gridHeader">
                        <bean:message key="user.prompt.dateTo"/>
                     </td>
                     <td width="199" height="1" class="gridLabel">
                        <nested:write property="dateTo"/>
                     </td>                     
			      </tr>			      				      				      		      
			      </nested:iterate>			      
			      </logic:equal>
			      </table>
		          </div>
		       </td>
	        </tr>
	        <tr>
	           <td width="575" height="10" colspan="5" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["userName"] != null)
        document.forms[0].elements["userName"].focus()
	       // -->
</script>
</body>
</html>
