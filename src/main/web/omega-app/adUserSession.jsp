<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="bank.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>   
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmLogout()
{
   if(confirm("Are you sure you want to logout this user?")) return true;
      else return false;
}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/adUserSession.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
     <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
           <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	      <tr>
	         <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    <bean:message key="userSession.title"/>
		 </td>
	      </tr>
              <tr>
	         <td width="575" height="34" colspan="4" class="statusBar">
		 	<logic:equal name="adUserSessionForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                    <bean:message key="app.success"/>
                 </logic:equal>
		    <html:errors/>	
	         </td>
	      </tr>
          <tr>             	                	       	                                                
	         <td width="575" height="25" colspan="4"> 
	         <div id="buttons">
	         <p align="right">
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
		      <p align="right">
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
              </td>
	      </tr>
	      <tr valign="top">
	         <td width="575" height="185" colspan="4">
		    <div align="center">
		       <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			  bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			  bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			  <tr>
                             <td width="575" height="1" colspan="6" class="gridTitle" 
			        bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                        <bean:message key="userSession.gridTitle.USDetails"/>
	                     </td>
	                  </tr>	                  	                  	                  	                  	                  
			    <tr>
			       <td width="261" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="userSession.prompt.username"/>
			       </td>
			       <td width="303" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="userSession.prompt.description"/>
			       </td>
			       <td width="330" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="userSession.prompt.loginTime"/>
			       </td>
                </tr>			       	                  
			  <%
			     int i = 0;	
			     String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
	                     String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
		             String rowBgc = null;
	                    %>
	           <nested:iterate property="adUSList">
			   <%
			     i++;
			     if((i % 2) != 0){
			         rowBgc = ROW_BGC1;
		             }else{
			         rowBgc = ROW_BGC2;
		             }  
			    %>
			     <tr bgcolor="<%= rowBgc %>">
			        <td width="261" height="1" class="gridLabel">
			           <nested:write property="username"/>
			        </td>
			        <td width="303" height="1" class="gridLabel">
			           <nested:write property="description"/>
			        </td>	
			        <td width="200" height="1" class="gridLabel">
			           <nested:write property="loginTime"/>
			        </td>		        
			        <td width="130" align="center" height="1">
			         <div id="buttons">
			         <logic:equal name="adUserSessionForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="logoutButton" styleClass="gridButton" onclick="return confirmLogout();">
	                       <bean:message key="button.logout"/>
  	                    </nested:submit>				     
  	                  </logic:equal>  	
  	                  </div>                  				     				     
  	                  <div id="buttonsDisabled" style="display: none;">
			         <logic:equal name="adUserSessionForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            <nested:submit property="logoutButton" styleClass="gridButton" disabled="true">
	                       <bean:message key="button.logout"/>
  	                    </nested:submit>				     
  	                  </logic:equal> 	
  	                  </div>
			       </td>
			     </tr>
	          </nested:iterate>       		          		          		          
	          </table>
		    </div>
		 </td>      	      	      
	      <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		 </td>
	      </tr>
           </table>
        </td>
     </tr>
  </table>
</html:form>	    	      
</body>
</html>
