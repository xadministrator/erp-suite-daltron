<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="apApproval.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmApprove()
{
   if(confirm("Are you sure you want to Approve/Reject selected records?")) return true;
      else return false;
}

function clickApproveBox(name)
{
   var property = name.substring(0,name.indexOf("."));
   
   var index = property.substring(10, property.indexOf("]"));   
   
   var divList = document.body.getElementsByTagName("div");
      
   for (var i = 0; i < divList.length; i++) {   
   
       var currDiv = divList[i];
       var currId = currDiv.id;   
   
       if ((document.forms[0].elements[property + ".approve"].checked == true) && (currId == "showReasonForRejection" + index)) {
   
           document.forms[0].elements[property + ".reject"].checked = false;
           currDiv.style.display = "none";
   
       }
       
   }
                   
   return true;
      
}

function clickRejectBox(name)
{
   var property = name.substring(0,name.indexOf("."));
   
   var index = property.substring(10, property.indexOf("]"));   
   
   var divList = document.body.getElementsByTagName("div");
      
   for (var i = 0; i < divList.length; i++) {
       
       var currDiv = divList[i];
       var currId = currDiv.id;

       if ((document.forms[0].elements[property + ".reject"].checked == true) && (currId == "showReasonForRejection" + index)) {

           document.forms[0].elements[property + ".approve"].checked = false;
           currDiv.style.display = "block";
          
       } else if ((document.forms[0].elements[property + ".reject"].checked == false) && (currId == "showReasonForRejection" + index)) {

           currDiv.style.display = "none";

       }

   }          
                                     
   return true;
      
}

function showAdDocument(name) 
{
   var property = name.substring(0,name.indexOf(".")); 
      
   var documentCode = document.forms[0].elements[property + ".documentCode"].value;
   
   if (document.forms[0].elements[property + ".document"].value == 'AP VOUCHER') {
   
       window.open("apVoucherEntry.do?forward=1&child=1&voucherCode=" + documentCode,"apVoucherEntryChild","width=605,height=500,scrollbars,status");
   
   } else if (document.forms[0].elements[property + ".document"].value == 'AP CHECK PAYMENT REQUEST') {
   
       window.open("apCheckPaymentRequestEntry.do?forward=1&child=1&paymentRequestCode=" + documentCode,"apCheckPaymentRequestEntryChild","width=605,height=500,scrollbars,status");
   
   } else if (document.forms[0].elements[property + ".document"].value == 'AP DEBIT MEMO') { 
   
       window.open("apDebitMemoEntry.do?forward=1&child=1&debitMemoCode=" + documentCode,"apDebitMemoEntryChild","width=605,height=500,scrollbars,status"); 
   
   } else if (document.forms[0].elements[property + ".document"].value == 'AP PURCHASE ORDER') { 
   
       window.open("apPurchaseOrderEntry.do?forward=1&child=1&purchaseOrderCode=" + documentCode,"apPurchaseOrderEntryChild","width=605,height=500,scrollbars,status"); 
	
   } else if (document.forms[0].elements[property + ".document"].value == 'AP RECEIVING ITEM') { 
	   
       window.open("apReceivingItemEntry.do?forward=1&child=1&receivingItemCode=" + documentCode,"apReceivingItemEntryChild","width=605,height=500,scrollbars,status"); 




   } else if (document.forms[0].elements[property + ".document"].value == 'AP PURCHASE REQUISITION') { 

		window.open("apPurchaseRequisitionEntry.do?forward=1&child=1&purchaseRequisitionCode=" + documentCode,"apPurchaseRequisitionEntryChild","width=605,height=500,scrollbars,status"); 

   

   } else if (document.forms[0].elements[property + ".document"].value == 'AP CANVASS') { 

		window.open("apPurchaseRequisitionEntry.do?forward=1&child=1&purchaseRequisitionCode=" + documentCode,"apPurchaseRequisitionEntryChild","width=605,height=500,scrollbars,status"); 

		
       
   } else {
   
       if (document.forms[0].elements[property + ".documentType"].value == 'PAYMENT') {
          
          window.open("apPaymentEntry.do?forward=1&child=1&checkCode=" + documentCode,"apPaymentEntryChild","width=605,height=500,scrollbars,status"); 
       
       } else {
       
          window.open("apDirectCheckEntry.do?forward=1&child=1&checkCode=" + documentCode,"apDirectCheckEntryChild","width=605,height=500,scrollbars,status");        
       
       }
   
   }
      
   return false;
      
}

function selectAll()
{

   var y = 0;
      
   while(true) {

      if (document.forms[0].elements["apAPRList[" + y + "].approve"] == null) {
          
          break;
      
      }
      
      var divList = document.body.getElementsByTagName("div");

      for (var i = 0; i < divList.length; i++) {   
   
         var currDiv = divList[i];
         var currId = currDiv.id;   
   
         if (currId == "showReasonForRejection" + y) {
   
            currDiv.style.display = "none";
   
         }
       
     }           
      	      	      
     document.forms[0].elements["apAPRList[" + y + "].approve"].checked = true;	      
     document.forms[0].elements["apAPRList[" + y + "].reject"].checked = false;
      
     y++;
   
   }
         
   return false;   
      
}

function unselectAll()
{

   var y = 0;
      
   while(true) {

      if (document.forms[0].elements["apAPRList[" + y + "].approve"] == null) {
          
          break;
      
      }
      
      var divList = document.body.getElementsByTagName("div");

      for (var i = 0; i < divList.length; i++) {   
   
         var currDiv = divList[i];
         var currId = currDiv.id;   
   
         if (currId == "showReasonForRejection" + y) {
   
            currDiv.style.display = "none";
   
         }
       
     }           
      	      	      
     document.forms[0].elements["apAPRList[" + y + "].approve"].checked = false;	      
     document.forms[0].elements["apAPRList[" + y + "].reject"].checked = false;
      
     y++;
   
   }
         
   return false;   
      
}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apApproval.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="400">
      <tr valign="top">
        <td width="187" height="400"></td> 
        <td width="581" height="400">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="400" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="apApproval.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="apApprovalForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>
		        <html:messages id="msg" message="true">
		       			<bean:write name="msg"/>		   
		   			</html:messages>	
	            </td>
	         </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="apApproval.prompt.document"/>
                </td>
		        <td width="390" height="25" class="control" colspan="3">
	               <html:select property="document" styleClass="comboRequired">
                      <html:options property="documentList"/>
                   </html:select>
		        </td>
             </tr>
			<tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="apApproval.prompt.documentNumberFrom"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="apApproval.prompt.documentNumberTo"/>
		       </td>
		       <td width="127" height="25" class="control">
		          <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
		       </td>
	         </tr>
			<tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="apApproval.prompt.dateFrom"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="apApproval.prompt.dateTo"/>
		       </td>
		       <td width="127" height="25" class="control">
		          <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
		       </td>
	         </tr>
             <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="apApproval.prompt.maxRows"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="apApproval.prompt.queryCount"/>
		       </td>
		       <td width="127" height="25" class="control">
		          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       </td>
	         </tr>
			<tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="apApproval.prompt.orderBy"/>
                </td>
		        <td width="390" height="25" class="control" colspan="3">
	               <html:select property="orderBy" styleClass="combo">
                      <html:options property="orderByList"/>
                   </html:select>
		        </td>
             </tr>
	       <tr>
	         <td width="575" height="30" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="200" height="30">
			             <div id="buttons">
			             <logic:equal name="apApprovalForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="apApprovalForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apApprovalForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="375" height="30" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="approveRejectButton" styleClass="mainButtonBig" onclick="return confirmApprove();">
				         <bean:message key="button.approveReject"/>
				         </html:submit>
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="approveRejectButton" styleClass="mainButtonBig" disabled="true">
				         <bean:message key="button.approveReject"/>
				         </html:submit>
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
                 <tr>
                  <td width="575" height="20" colspan="4">
					<div id="buttons">
			        <p align="right">
					<logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				    <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				    <bean:message key="button.selectAll"/>
				    </html:submit>
					<html:submit property="unselectAllButton" styleClass="mainButtonMedium" onclick="return unselectAll();">
				    <bean:message key="button.unselectAll"/>
				    </html:submit>
				    </logic:equal>
                    </div>
                    <div id="buttonsDisabled" style="display: none;">
			        <p align="right">
					<logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				    <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				    <bean:message key="button.selectAll"/>
				    </html:submit>
					<html:submit property="unselectAllButton" styleClass="mainButtonMedium" disabled="true">
				    <bean:message key="button.unselectAll"/>
				    </html:submit>
				    </logic:equal>
                    </div>
                  </td>
				 </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="9" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="apApproval.gridTitle.APRDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			    	<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.department"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.date"/>
			       </td>
			       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.supplierName"/>
			       </td>			       
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.documentNumber"/>
			       </td>			       			       
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.referenceNumber"/>
			       </td>			       			       
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.amount"/>
			       </td>
			       <td width="88" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.approve"/>
			       </td>			       
			       <td width="176" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.reject"/>
			       </td>			       
                </tr>
                <tr>	   	                
			       <td width="894" height="1" class="gridHeader" colspan="9" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apApproval.prompt.reasonForRejection"/>
			       </td>
			    </tr>                  	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apAPRList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <nested:hidden property="documentCode"/>
			          <nested:hidden property="document"/>
			          <nested:hidden property="documentType"/>
			          <tr bgcolor="<%= rowBgc %>">
			          	<td width="100" height="1" class="gridLabel">
			                <nested:write property="department"/>
			             </td>
			             <td width="100" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="130" height="1" class="gridLabel">
			                <nested:write property="supplierName"/>
			             </td>			             
			             <td width="100" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>			             			             
			             <td width="100" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             			             
			             <td width="100" height="1" class="gridLabelNum">
			                <nested:write property="amount"/>
			             </td>	
			             <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="88" align="center" height="1">
			                <nested:checkbox property="approve" onclick="return clickApproveBox(name);"/>
			             </td>
			             <td width="88" align="center" height="1">
			                <nested:checkbox property="reject" onclick="return clickRejectBox(name);"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="88" align="center" height="1">
			                <nested:checkbox property="approve" disabled="true"/>
			             </td>
			             <td width="88" align="center" height="1">
			                <nested:checkbox property="reject" disabled="true"/>
			             </td>
			             </logic:equal>
			             <td width="88" height="1" align="center">
			                <div id="buttons">
			                <nested:submit property="viewButton" styleClass="gridButton" onclick="return showAdDocument(name);">
					         <bean:message key="button.view"/>
					        </nested:submit>
					        </div>
					        <div id="buttonsDisabled" style="display: none;">
					        <nested:submit property="viewButton" disabled="true" styleClass="gridButton">
					         <bean:message key="button.view"/>
					        </nested:submit>
					        </div>
			             </td>	
			          </tr>
				      <tr bgcolor="<%= rowBgc %>">			       
				         <td width="894" height="1" class="control" colspan="8">
				         <div id="<%="showReasonForRejection" + (i - 1)%>" style="display: none;">				         
				            <nested:textarea property="reasonForRejection" cols="68" rows="2" styleClass="text"/>
				         </div>
				         </td>
				      </tr>			          			          
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="apApprovalForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apAPRList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <nested:hidden property="documentCode"/>
			          <nested:hidden property="document"/>
			          <nested:hidden property="documentType"/>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="apApproval.prompt.date"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="date"/>
			             </td>
			             <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="75" align="center" height="1">
			                <nested:checkbox property="approve" onclick="return clickApproveBox(name);"/>
			             </td>
			             <td width="74" align="center" height="1">
			                <nested:checkbox property="reject" onclick="return clickRejectBox(name);"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="apApprovalForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="75" align="center" height="1">
			                <nested:checkbox property="approve" disabled="true"/>
			             </td>
			             <td width="74" align="center" height="1">
			                <nested:checkbox property="reject" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="apApproval.prompt.supplierName"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="supplierName"/>
			             </td>
			             <td width="149" height="1" align="center" colspan="2">
			                <div id="buttons">
			                <nested:submit property="viewButton" styleClass="gridButton" onclick="return showAdDocument(name);">
					         <bean:message key="button.view"/>
					        </nested:submit>
					        </div>
					        <div id="buttonsDisabled" style="display: none;">
					        <nested:submit property="viewButton" disabled="true" styleClass="gridButton">
					         <bean:message key="button.view"/>
					        </nested:submit>
					        </div>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="apApproval.prompt.documentNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="apApproval.prompt.amount"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum" colspan="2">
			                <nested:write property="amount"/>
			             </td>			             
			          </tr>	
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="apApproval.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel" colspan="4">
			                <nested:write property="referenceNumber"/>
			             </td>
			          </tr>	
    			      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			             <div id="<%="showReasonForRejection" + (i - 1)%>" style="display: none;">
			                <bean:message key="apApproval.prompt.reasonForRejection"/>
			             </div>
			             </td>
			             <td width="719" height="1" class="gridLabel" colspan="4">
			             <div id="<%="showReasonForRejection" + (i - 1)%>" style="display: none;">
			                <nested:textarea property="reasonForRejection" cols="60" rows="2" styleClass="text"/>
			             </div>
			             </td>
			          </tr>			          	          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["document"] != null)
             document.forms[0].elements["document"].focus()
   // -->
</script>
</body>
</html>
