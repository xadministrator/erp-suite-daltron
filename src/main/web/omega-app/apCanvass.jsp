<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="apCanvass.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	



  function submitForm()
  {            
      disableButtons();
      enableInputControls();
      
  }
  
function calculateAmount(name,precisionUnit)
{

  var property = name.substring(0,name.indexOf("."));
 
  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;    
  
  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitPrice"].value != "") {  
        
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {
	  
	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {
	  
	  	unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
	  
	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');
	  	
	  }
	  
	  amount = (quantity * unitPrice).toFixed(parseInt(precisionUnit.value));
	  
	  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  
  }
  
}
function checkLowest(name, size){
	
	var property = name.substring(0,name.indexOf("."));
    var listName = property.substring(0, property.indexOf("["));
    var max = 0;
    var min=0;
    var buf=0;
	var i=0;
	var x=0;
	//alert(size);
	
	for(i=0; i<size.value; i++) {
    	var in_amount = parseFloat((document.forms[0].elements[listName + "[" + i + "].amount"].value).replace(/,/g,''));
    	
		if(!isNaN(parseFloat(in_amount))){
			if (i == 0) {  min=in_amount; }
			x++;
			
			
			if(in_amount < min && document.forms[0].elements[listName + "[" + i + "].supplier"] != null && document.forms[0].elements[listName + "[" + i + "].supplier"].value != ''){
				min=in_amount;
				
			}
			
		}

		
    }
	

	if(x == 1){
		document.forms[0].elements["apCNVList[" + 0 + "].poCheckbox"].checked = true; 	
		
	} else {
 	for(i=0; i<size.value; i++) {
 		var amount = (document.forms[0].elements[listName + "[" + i + "].amount"].value).replace(/,/g,'');
 		if(!isNaN(parseInt(amount))){
 			if(amount == min && document.forms[0].elements[listName + "[" + i + "].supplier"] != null && document.forms[0].elements[listName + "[" + i + "].supplier"].value != ''){
 				document.forms[0].elements["apCNVList[" + i + "].poCheckbox"].checked = true;  	
 			}else{
 				document.forms[0].elements["apCNVList[" + i + "].poCheckbox"].checked = false;
 			}
 		}
 	}    	
	}

	
}
function calculateTotalQuantity(name, size, precisionUnit)
{

      var property = name.substring(0,name.indexOf("."));
      var listName = property.substring(0, property.indexOf("["));
      var totalQuantity = 0;
      var i = 0;
      
      
      for(i=0; i<size.value; i++) {
      
      	var quantity = (document.forms[0].elements[listName + "[" + i + "].quantity"].value).replace(/,/g,'');
		
      	var checked = document.forms[0].elements["apCNVList" + "[" + i + "].poCheckbox"].checked;
      		
      	
	    if(!isNaN(parseInt(quantity))) {
	    		if(checked==true) {
	    			
	      			totalQuantity = (parseFloat(totalQuantity) + parseFloat(quantity)).toFixed(parseInt(precisionUnit.value));
	      		}
      	}
      
      }
      
      document.forms[0].elements["quantity"].value = totalQuantity;
	        
}  

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/apCanvass.do" onsubmit="return submitForm();" >
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="apCanvass.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="apCanvassForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.documentNumber"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="documentNumber" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="100" height="25">
	           <bean:message key="apCanvass.prompt.date"/>
	        </td>
	        <td width="147" height="25" class="control">
			   <html:text property="date" size="10" maxlength="10" styleClass="text" disabled="true"/>
	        </td>
         </tr>   
         
        
         <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.itemName"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="itemName" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.location"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="location" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>
          <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.itemDescription"/>
	        </td>
	        <td width="148" height="25" class="control" colspan="3">
	           <html:text property="itemDescription" size="50" maxlength="100" styleClass="text" disabled="true"/>
	        </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.uomName"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="uomName" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="apCanvass.prompt.quantity"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="quantity" size="15" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>
         <tr>
	       <td width="575" height="50" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		    <div id="buttonsDisabled" style="display: none;">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		   <td>
         </tr>	 
         <logic:equal name="apCanvassForm" property="showSaveButton" value="true">    
	     <tr>
            <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
            </td>
         </tr>
	     <tr>
	        <td width="575" height="5" colspan="4">
		    </td>
	     </tr>
	     <tr>    	       	      	     
	        <td width="575" height="50" colspan="4"> 
		       <div id="buttons">
	           <p align="right">
               <html:submit property="saveButton" styleClass="mainButton" onclick="calculateTotalQuantity(name, apCNVListSize, precisionUnit);">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </div>
		    </td>
	     </tr>
	     </logic:equal>
	     <html:hidden property="precisionUnit"/>
	     <logic:equal name="apCanvassForm" property="enableFields" value="true">
	     <tr valign="top">
	        <td width="575" height="185" colspan="7">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                	<td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="apCanvass.gridTitle.CNVDetails"/>
	                </td>
	            </tr>	                    	                    
			    <tr>
			    	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.lineNumber"/>
			       </td>
			       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.supplier"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.quantity"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.unitPrice"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.amount"/>
			       </td>			       			       
			       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.delete"/>
			       </td>
			       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.po"/>
			       </td>			       			       			       
                </tr>
     
                <tr>
                	<td width="270" height="1" class="gridHeader" colspan="8" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				    <bean:message key="apCanvass.prompt.supplierName"/>
				    </td>
                </tr>
                
                <tr>
                	<td width="270" height="1" class="gridHeader" colspan="8" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				    <bean:message key="apCanvass.prompt.remarks"/>
				    </td>
                </tr>
                
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <html:hidden property="apCNVListSize"/>
			    <nested:iterate property="apCNVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <nested:hidden property="isSupplierEntered" value=""/>
			    <tr bgcolor="<%= rowBgc %>">	
			       <td width="50" height="1" class="control">
			          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
			       </td>					       	       
			       <td width="115" height="1" class="control">
			          <nested:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="checkLowest(name,apCNVListSize);">
			          	<nested:options property="supplierList"/>
			          </nested:select>
			          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookupGrid(name, 'supplier', 'supplierName', 'isSupplierEntered');"/>
			       </td>
			       <td width="100" height="1" class="control">
			          <nested:text property="quantity" size="10" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name,precisionUnit); checkLowest(name,apCNVListSize);" onkeyup="calculateAmount(name,precisionUnit);  "/>
			       </td>
			       <td width="100" height="1" class="control">
			          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event); calculateAmount(name,precisionUnit);" onblur="addZeroes(name); calculateAmount(name,precisionUnit); checkLowest(name,apCNVListSize); "/>
			       </td>
			       <td width="110" height="1" class="control">
			          <nested:text property="amount" size="10" maxlength="25" styleClass="textAmount"/>
			       </td>			       
			       <td width="50" align="center" height="1" class="control">
			          <nested:checkbox property="deleteCheckbox"/>
			       </td>
			       <logic:equal name="apCanvassForm" property="enablePoCheckbox" value="true">
			       <td width="50" align="center" height="1" class="control">
			          <nested:checkbox property="poCheckbox" onchange="calculateTotalQuantity(name, apCNVListSize, precisionUnit);"/>
			       </td>			   
			       </logic:equal>
			       <logic:equal name="apCanvassForm" property="enablePoCheckbox" value="false">
			       <td width="50" align="center" height="1" class="control">
			          <nested:checkbox property="poCheckbox" disabled="true"/>
			       </td>			   
			       </logic:equal>    
			    </tr>	
			    <tr bgcolor="<%= rowBgc %>">
			    <td width="30" height="1" class="control"/>				   
				       <td width="270" height="1" class="control" colspan="7">
				          <nested:text property="supplierName" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
			    </tr>
			    
			    <td width="30" height="1" class="control"/>				   
				       <td width="270" height="1" class="control" colspan="7">
				          <nested:text property="remarks" size="50" maxlength="100" styleClass="text" style="font-size:8pt;"/>
				       </td>
			    </tr>
			    				    
			  </nested:iterate>
			  </table>
		      </div>
		      </td>
	       </tr>
         </logic:equal>
	     <logic:equal name="apCanvassForm" property="enableFields" value="false">
	     <tr valign="top">
	        <td width="575" height="185" colspan="7">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                	<td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                	<bean:message key="apCanvass.gridTitle.CNVDetails"/>
	                </td>
	            </tr>	
                <tr>
                   <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.lineNumber"/>
			       </td>
			       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.supplier"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.quantity"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.unitPrice"/>
			       </td>	
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.amount"/>
			       </td>			       			       
			       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.delete"/>
			       </td>
			       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="apCanvass.prompt.po"/>
			       </td>			       			       			       
                </tr>
                
               <tr>
                	<td width="270" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				    <bean:message key="apCanvass.prompt.supplierName"/>
				    </td>
                </tr>					    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apCNVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isSupplierEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">		
				    	<td width="50" height="1" class="control">
			          		<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
			       		</td>				       	       
				    	<td width="115" height="1" class="control">
		          			<nested:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
			          			<nested:options property="supplierList"/>
			          		</nested:select>
			          		<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookupGrid(name, 'supplier', 'supplierName', 'isSupplierEntered');"/>
			       
			       		</td>
			       		<td width="100" height="1" class="control">
			          		<nested:text property="quantity" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
			       		</td>
		       			<td width="100" height="1" class="control">
			          		<nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
			       		</td>	
			       		<td width="110" height="1" class="control">
			          		<nested:text property="amount" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
			       		</td>			       
			       		<td width="50" align="center" height="1" class="control">
			          		<nested:checkbox property="deleteCheckbox" disabled="true"/>
			       		</td>
			       		<logic:equal name="apCanvassForm" property="enablePoCheckbox" value="true">
			       		<td width="50" align="center" height="1" class="control">
			          		<nested:checkbox property="poCheckbox"/>
			       		</td>			   
			       		</logic:equal>
			       		<logic:equal name="apCanvassForm" property="enablePoCheckbox" value="false">
		       			<td width="50" align="center" height="1" class="control">
			          		<nested:checkbox property="poCheckbox" disabled="true"/>
			       		</td>			   
			       		</logic:equal>   			       
	    			</tr>	
	    			
	    			<tr bgcolor="<%= rowBgc %>">
				    <td width="30" height="1" class="control"/>				   
					       <td width="270" height="1" class="control" colspan="3">
					          <nested:text property="supplierName" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
					       </td>
				    </tr>						    
				  </nested:iterate>
				  </table>
	      	</div>
	     	</td>
	     </tr>
         </logic:equal>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apCanvassForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="apCanvassForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apCanvassForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apCanvassForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	           </tr>	
			 </table>
		    </div>
		  </td>
	     </tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["supplier"] != null &&
        document.forms[0].elements["supplier"].disabled == false)
        document.forms[0].elements["supplier"].focus()
      
       // checkLowest("apCNVList[0].poCheckbox", document.forms[0].elements["apCNVListSize"]);
	       // -->

      
</script>
</body>
</html>
