<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="checkBatchPrint.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmPrint()
{
   if(confirm("Are you sure you want to Print selected records?")) return true;
      else return false;     
      
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["apCBPList[" + i + "].print"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["apCBPList[" + i + "].print"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}

var documentWindow;

function showAdDocument(name) 
{

   var property = name.substring(0,name.indexOf(".")); 
      
   var documentCode = document.forms[0].elements[property + ".checkCode"].value;
   var checkType = document.forms[0].elements[property + ".checkType"].value;
   var wihe = 'width=' + screen.availWidth + ',height=' + screen.availHeight;
      
   if (checkType == 'PAYMENT') {

       documentWindow = window.open("apPaymentEntry.do?forward=1&checkCode=" + documentCode, "apPaymentEntry", "resizable,scrollbars,status,titlebar,screenX=0,screenY=0,left=0,top=0," + wihe);     

   } else {

       documentWindow = window.open("apDirectCheckEntry.do?forward=1&checkCode=" + documentCode, "apDirectCheckEntry", "resizable,scrollbars,status,titlebar,screenX=0,screenY=0,left=0,top=0," + wihe);     

   }

   return false;

}

function refreshPage()
{

   if (documentWindow && documentWindow.closed) {
   
       window.location = window.location.protocol + '//' + window.location.host + window.location.pathname + "?refresh=1";
   
   }

}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));" onfocus="refreshPage();">
<html:form action="/apCheckBatchPrint.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="checkBatchPrint.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apCheckBatchPrintForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     
	     <logic:equal name="apCheckBatchPrintForm" property="showBatchName" value="true">
     	 <tr>
            <td width="140" height="25" class="prompt">
               <bean:message key="checkBatchPrint.prompt.batchName"/>
            </td>
            <td width="415" height="25" class="control" colspan="3">
               <html:select property="batchName" styleClass="combo">
                   <html:options property="batchNameList"/>
               </html:select>
            </td>
         </tr>
         </logic:equal>	
	     
	     <tr>
	     <logic:equal name="apCheckBatchPrintForm" property="useSupplierPulldown" value="true">
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.supplierCode"/>
	         </td>
	         <td width="415" height="25" class="control" colspan="3">
	            <html:select property="supplierCode" styleClass="combo">
			      <html:options property="supplierCodeList"/>
			   </html:select>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>
         </logic:equal>	
	     <logic:equal name="apCheckBatchPrintForm" property="useSupplierPulldown" value="false">
	         <td class="prompt" width="160" height="25">
	            <bean:message key="findCheck.prompt.supplierCode"/>
	         </td>
	         <td width="415" height="25" class="control" colspan="3">
	           <html:text property="supplierCode" size="15" maxlength="15" styleClass="text" readonly="true"/>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>
         </logic:equal>	
         </tr> 
	     
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.checkType"/>
	         </td>
	         <td width="415" height="25" class="control" colspan="3">
	            <html:select property="checkType" styleClass="combo">
			      <html:options property="checkTypeList"/>
			   </html:select>
	         </td>
         </tr>
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.bankAccount"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="bankAccount" styleClass="combo">
			      <html:options property="bankAccountList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="130" height="25">
	            <bean:message key="findCheck.prompt.checkVoid"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:checkbox property="checkVoid"/>
	         </td>	         
         </tr>           
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	         </td>
         </tr> 
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.numberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="numberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.numberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="numberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.documentNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.documentNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
                                           
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.released"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="released" styleClass="combo">
			      <html:options property="releasedList"/>
			   </html:select>
	         </td>         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.approvalStatus"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="approvalStatus" styleClass="combo">
			      <html:options property="approvalStatusList"/>
			   </html:select>
	         </td>
         </tr>
                                           
         <tr>	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.posted"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="posted" styleClass="combo">
			      <html:options property="postedList"/>
			   </html:select>
	         </td>    
	         <td class="prompt" width="140" height="25">
	            <bean:message key="checkBatchPrint.prompt.orderBy"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	              
         </tr>
         
         <tr>
	       <td class="prompt" width="140" height="25">
	          <bean:message key="checkBatchPrint.prompt.maxRows"/>
	       </td>
	       <td width="148" height="25" class="control">
	          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
	       </td>
	       <td class="prompt" width="140" height="25">
	          <bean:message key="checkBatchPrint.prompt.queryCount"/>
	       </td>
	       <td width="147" height="35" class="control">
	          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
	       </td>
         </tr>
            
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="200" height="25">
			             <div id="buttons">
			             <logic:equal name="apCheckBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="apCheckBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apCheckBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="375" height="25" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>		
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
				    </tr> 
				     <tr>
				      <td width="575" height="25" colspan="4">
			             <div id="buttons">
			             <p align="right">
				         <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printCheckButton" styleClass="mainButtonMedium" onclick="return confirmPrint();">
				         <bean:message key="button.chkPrint"/>
				         </html:submit>
				         <html:submit property="printCVButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.cvPrint"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         </logic:equal>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printCheckButton" styleClass="mainButtonMedium" disabled="true">
				         <bean:message key="button.chkPrint"/>
				         </html:submit>
				         <html:submit property="printCVButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.cvPrint"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         </logic:equal>		
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="8" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="checkBatchPrint.gridTitle.CBPDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.date"/>
			       </td>
			       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.checkType"/>
			       </td>			       
			       <td width="175" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.supplierCode"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.checkNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.documentNumber"/>
			       </td>			       			       			       			       
			       <td width="229" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="checkBatchPrint.prompt.amount"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="apCBPList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <nested:hidden property="checkCode"/>
 	            <nested:hidden property="checkType"/>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="checkType"/>
			       </td>
			       <td width="175" height="1" class="gridLabel">
			          <nested:write property="supplierCode"/>
			       </td>
			       <td width="140" height="1" class="gridLabel">
			          <nested:write property="checkNumber"/>
			       </td>
			       <td width="110" height="1" class="gridLabel">
			          <nested:write property="documentNumber"/>
			       </td>
			       <td width="159" height="1" class="gridLabelNum">
			          <nested:write property="amount"/>
			       </td>			       			       			       			       
			       <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
			       <td width="20" align="center" height="1">
			          <nested:checkbox property="print"/>
			       </td>
			       </logic:equal>
			       <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
			       <td width="20" align="center" height="1">
			          <nested:checkbox property="print" disabled="true"/>
			       </td>
			       </logic:equal>
			       <td width="50" height="1" class="gridLabel">
	                 <div id="buttons">
	                 <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
			          <bean:message key="button.open"/>
			         </nested:submit>
			         </div>
			         <div id="buttonsDisabled" style="display: none;">
			         <nested:submit property="openButton" disabled="true" styleClass="gridButton">
			          <bean:message key="button.open"/>
			         </nested:submit>
			        </div>
		           </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="apCheckBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="apCBPList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <nested:hidden property="checkCode"/>
 	            <nested:hidden property="checkType"/>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="checkBatchPrint.prompt.checkType"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="checkType"/>
			       </td>
			       <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
		           <td width="49" align="center" height="1">
	                  <nested:checkbox property="print"/>
		           </td>
		           </logic:equal>
		           <logic:equal name="apCheckBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
		           <td width="49" align="center" height="1">
	                  <nested:checkbox property="print" disabled="true"/>
		           </td>
		          </logic:equal>
		          <td width="100" height="1" class="gridLabel">
	                <div id="buttons">
	                <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
			         <bean:message key="button.open"/>
			        </nested:submit>
			        </div>
			        <div id="buttonsDisabled" style="display: none;">
			        <nested:submit property="openButton" disabled="true" styleClass="gridButton">
			         <bean:message key="button.open"/>
			        </nested:submit>
			        </div>
	             </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.supplierCode"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="supplierCode"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.bankAccount"/>
			      </td>
			      <td width="149" height="1" class="gridLabel" colspan="2">
			         <nested:write property="bankAccount"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.date"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="date"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.checkNumber"/>
			      </td>
			      <td width="149" height="1" class="gridLabel" colspan="2">
			         <nested:write property="checkNumber"/>
			      </td>
			    </tr>			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.documentNumber"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="documentNumber"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.amount"/>
			      </td>
			      <td width="149" height="1" class="gridLabelNum" colspan="2">
			         <nested:write property="amount"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="checkBatchPrint.prompt.released"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="4">
			         <nested:write property="released"/>
			      </td>				    	      
			    </tr>				    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["checkType"] != null &&
        document.forms[0].elements["checkType"].disabled == false)
        document.forms[0].elements["checkType"].focus()
   // -->
</script>
<logic:equal name="apCheckBatchPrintForm" property="checkReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckBatchPrintForm" type="com.struts.ap.checkbatchprint.ApCheckBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
  	win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='apCheckBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepCheckPrint.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getCheckCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='checkCode[" +<%=j%>+ "]' value='<%=actionForm.getCheckCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>"); 	 
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>

<logic:equal name="apCheckBatchPrintForm" property="checkVoucherReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckBatchPrintForm" type="com.struts.ap.checkbatchprint.ApCheckBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
    win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
 	win.document.write("<head></head><body><form name='apCheckBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepCheckVoucherPrint.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getCheckCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='checkCode[" +<%=j%>+ "]' value='<%=actionForm.getCheckCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>"); 	 
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>

<logic:equal name="apCheckBatchPrintForm" property="checkEditListReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckBatchPrintForm" type="com.struts.ap.checkbatchprint.ApCheckBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
      	
  	win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");		

	win.document.write("<head></head><body><form name='apCheckBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepCheckEditList.do?forwardBatch=1' method=post>");

	<%
	
	for (int j=0; j<actionForm.getCheckCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='checkCode[" +<%=j%>+ "]' value='<%=actionForm.getCheckCodeList().get(j)%>'>");
	<%
	}
	%>
		
	win.document.write("</form></body>"); 	  	

	win.document.getElementById("printForm").submit();


   	win.focus();

   	
  //-->
</script>
</logic:equal>
</body>
</html>
