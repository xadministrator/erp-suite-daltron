<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="checkPaymentRequestEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{

  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitCost = 0;
  var amount = 0;

  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitCost"].value != "") {

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {

	  	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

	  amount = (quantity * unitCost).toFixed(2);

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  var totalDiscountAmount = 0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }
	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(2);

	  	  }

	  	  totalDiscountAmount = totalDiscountAmount.toFixed(2);

		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toString());
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

      } else {

	  	document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  	document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

	  }

  }

}

  var tempAmount = 0;

  function initializeTempDebit(name)
  {

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');

      } else {

          tempAmount = 0;

      }

  }

  function calculateTotalDebit(name)
  {

      var debitAmount = 0;
      var totalDebit = (document.forms[0].elements["totalDebit"].value).replace(/,/g,'');
      var amount;

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          debitAmount = (document.forms[0].elements[name].value).replace(/,/g,'');

      }

      if (!isNaN(totalDebit - (tempAmount - debitAmount))) {

	      amount = (1 * totalDebit - (1 * tempAmount - 1 * debitAmount)).toFixed(2);
	      document.forms[0].elements["totalDebit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = debitAmount;

	  }

  }


  function initializeTempCredit(name)
  {

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');

      } else {

          tempAmount = 0;

      }

  }

  function calculateTotalCredit(name)
  {

      var creditAmount = 0;
      var totalCredit = (document.forms[0].elements["totalCredit"].value).replace(/,/g,'');
      var amount;

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          creditAmount = (document.forms[0].elements[name].value).replace(/,/g,'');

      }

      if (!isNaN(totalCredit - (tempAmount - creditAmount))) {

	      amount = (1 * totalCredit - (1 * tempAmount - 1 * creditAmount)).toFixed(2);
	      document.forms[0].elements["totalCredit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = creditAmount;

	  }

  }

  function fnSetValues(){
     var iHeight=245;
     var iWidth=260;
     var sFeatures="dialogHeight: " + iHeight + "px;" + "dialogWidth: " + iWidth + "px;";
     return sFeatures;
  }

var currProperty;
var currName;
 function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   retObject = window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}

function fnOpenMisc(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");

   return false;

}
//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


  function enterBillAmount()
  {

    disableButtons();
    enableInputControls();
	document.forms[0].elements["isBillAmountEntered"].value = true;
	document.forms[0].submit();

  }

  function calculateAmountDue(name)
  {
      document.forms[0].elements["amountDue"].value = document.forms[0].elements[name].value;

  }

  function checkPaymentEnterSubmit()
  {
      if (document.activeElement.name != 'billAmount') {

          return enterSubmit(event, new Array('saveSubmitButton'));

      } else {

          return true;

      }

  }

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return checkPaymentEnterSubmit();">
<html:form action="/apCheckPaymentRequestEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   	<bean:message key="checkPaymentRequestEntry.title"/>

			</td>

	     </tr>
		<tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apCheckPaymentRequestEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>

           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   </html:messages>

	        </td>
    	</tr>


	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isBillAmountEntered" value=""/>
	     <!--<html:hidden property="isTypeEntered" value=""/>-->
	     <html:hidden property="isConversionDateEntered" value=""/>
	     <logic:equal name="apCheckPaymentRequestEntryForm" property="enableFields" value="true">
	     <tr>

		 	<td width="575" height="10" colspan="4">
				<div class="tabber">
				<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
				    		<td class="prompt" width="575" height="25" colspan="4">
			         		</td>
			      		</tr>

						<tr>

    	     				<html:hidden property="type" value="REQUEST"/>

							<logic:equal name="apCheckPaymentRequestEntryForm" property="showBatchName" value="true">
	            	    	<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.batchName"/>
                			</td>
	                		<td width="157" height="25" class="control">
    	               			<html:select property="batchName" styleClass="comboRequired" style="width:130;">
        	            			<html:options property="batchNameList"/>
            	       			</html:select>
                			</td>
   							</logic:equal>
						</tr>

						<tr>
						 	<logic:equal name="apCheckPaymentRequestEntryForm" property="useSupplierPulldown" value="true">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.supplier"/>
               				</td>
							<td width="158" height="25" class="control">
               					<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                   					<html:options property="supplierList"/>
	               				</html:select>
								<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>
							<logic:equal name="apCheckPaymentRequestEntryForm" property="useSupplierPulldown" value="false">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.supplier"/>
               				</td>
							<td width="158" height="25" class="control">
									<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
									<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
               					<bean:message key="checkPaymentRequestEntry.prompt.date"/>
               				</td>
               				<td width="157" height="25" class="control">
               					<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
	               			</td>
		 				</tr>
						<tr>
            	   			<td width="130" height="25" class="prompt">
               					<bean:message key="checkPaymentRequestEntry.prompt.referenceNumber"/>
              				</td>
               				<td width="158" height="25" class="control">
               					<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
	               			</td>
    	           			<td width="130" height="25" class="prompt">
        	       				<bean:message key="checkPaymentRequestEntry.prompt.documentNumber"/>
            	   			</td>
               				<td width="157" height="25" class="control">
               					<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
               				</td>
		 				</tr>
						<tr>
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.poNumber"/>
               				</td>
               				<td width="158" height="25" class="control">
               					<html:text property="poNumber" size="15" maxlength="25" styleClass="textRequired"/>
	               			</td>
							<td width="130" height="25" class="prompt">
        	       				<bean:message key="checkPaymentRequestEntry.prompt.billAmount"/>
            	   			</td>
               				<td width="158" height="25" class="control">
               					<html:text property="billAmount" size="15" maxlength="25" styleClass="textAmountRequired" onblur="addZeroes(name); return enterBillAmount(); " onkeypress="javascript:if (event.keyCode == 13) return false;" onkeyup="formatAmount(name, (event)?event:window.event);" />
               				</td>
						</tr>
						<tr>
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.description"/>
               				</td>
               				<td width="445" height="25" class="control" colspan="3">
               					<html:textarea property="description" cols="20" rows="4" styleClass="textRequired"/>
	               			</td>
    	   				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
					</table>
				</div>
				<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				  		<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			           	</tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   				<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="apCheckPaymentRequestEntryForm" property="enableVoucherVoid" value="true">
               				<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.voucherVoid"/>
               				</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkPaymentRequestVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apCheckPaymentRequestEntryForm" property="enableVoucherVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkPaymentRequestVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.amountPaid"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.amountDue"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
               		 		</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
               		 		</td>
         				</tr>
					</table>
				</div>
			   	<div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="return enterBillAmount();">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" onchange="return enterBillAmount();">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
				</div>
				<div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="checkPaymentRequestEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
				</div>
				<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table>
				</div>
				<div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
				</div>
				<div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>

	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>

	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="checkPaymentRequestEntry.gridTitle.VOUDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.accountDescription"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apVOUList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:notEqual property="drClass" value="<%=Constants.AP_DR_CLASS_PAYABLE%>">
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name); addZeroes(name);" onkeyup="calculateTotalDebit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempCredit(name);"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:notEqual>
				    <nested:equal property="drClass" value="<%=Constants.AP_DR_CLASS_PAYABLE%>">
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onfocus="initializeTempCredit(name);" onblur="calculateAmountDue(name); calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateAmountDue(name); calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:equal>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apCheckPaymentRequestEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apCheckPaymentRequestEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>

	     <logic:equal name="apCheckPaymentRequestEntryForm" property="enableFields" value="false">
	     <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
				    		<td class="prompt" width="575" height="25" colspan="4">
			         		</td>
			      		</tr>

						<tr>

    	     				<html:hidden property="type" value="REQUEST"/>

							<logic:equal name="apCheckPaymentRequestEntryForm" property="showBatchName" value="true">
	            	    	<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.batchName"/>
                			</td>
	                		<td width="157" height="25" class="control">
    	               			<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
        	            			<html:options property="batchNameList"/>
            	       			</html:select>
                			</td>
   							</logic:equal>
						</tr>

						<tr>
        	       			<logic:equal name="apCheckPaymentRequestEntryForm" property="useSupplierPulldown" value="true">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.supplier"/>
               				</td>
							<td width="158" height="25" class="control">
               					<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                   					<html:options property="supplierList"/>
	               				</html:select>
								<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
							</td>
							</logic:equal>
							<logic:equal name="apCheckPaymentRequestEntryForm" property="useSupplierPulldown" value="false">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.supplier"/>
               				</td>
							<td width="158" height="25" class="control">
									<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
									<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
							</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
               					<bean:message key="checkPaymentRequestEntry.prompt.date"/>
               				</td>
               				<td width="157" height="25" class="control">
               					<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
	               			</td>
		 				</tr>
						<tr>
            	   			<td width="130" height="25" class="prompt">
               					<bean:message key="checkPaymentRequestEntry.prompt.referenceNumber"/>
              				</td>
               				<td width="158" height="25" class="control">
               					<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
	               			</td>
    	           			<td width="130" height="25" class="prompt">
        	       				<bean:message key="checkPaymentRequestEntry.prompt.documentNumber"/>
            	   			</td>
               				<td width="157" height="25" class="control">
               					<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
               				</td>
		 				</tr>
						<tr>
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.poNumber"/>
               				</td>
               				<td width="158" height="25" class="control">
               					<html:text property="poNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
	               			</td>
							<td width="130" height="25" class="prompt">
        	       				<bean:message key="checkPaymentRequestEntry.prompt.billAmount"/>
            	   			</td>
               				<td width="158" height="25" class="control">
               					<html:text property="billAmount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
               				</td>
						</tr>
						<tr>
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="checkPaymentRequestEntry.prompt.description"/>
               				</td>
               				<td width="445" height="25" class="control" colspan="3">
               					<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
	               			</td>
    	   				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
					</table>
				</div>
				<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				  		<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			           	</tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   				<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="apCheckPaymentRequestEntryForm" property="enableVoucherVoid" value="true">
               				<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.voucherVoid"/>
               				</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkPaymentRequestVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apCheckPaymentRequestEntryForm" property="enableVoucherVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkPaymentRequestVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.amountPaid"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.amountDue"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
               		 		</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="checkPaymentRequestEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
               		 		</td>
         				</tr>
					</table>
				</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="160" height="25">
			                   <bean:message key="checkPaymentRequestEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="10" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton1" value="true">
			                   <%System.out.println("show?"); %>
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton1" value="true">
			                   <%System.out.println("hide?"); %>
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apCheckPaymentRequestEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table>
					</div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="checkPaymentRequestEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showGenerateVoucherButton" value="true">
	           <html:submit property="generateVoucherButton" styleClass="mainButtonBig" onclick="return confirmGenerateVoucher();">
	               <bean:message key="button.generateVoucher"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="apCheckPaymentRequestEntryForm" property="showGenerateVoucherButton" value="true">
	           <html:submit property="generateVoucherButton" styleClass="mainButtonBig" disabled = "true">
	               <bean:message key="button.generateVoucher"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="checkPaymentRequestEntry.gridTitle.VOUDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="checkPaymentRequestEntry.prompt.accountDescription"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apVOUList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>

				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif"  disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apCheckPaymentRequestEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apCheckPaymentRequestEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apCheckPaymentRequestEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     	</tr>
	     </logic:equal>



	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>

<logic:equal name="apCheckPaymentRequestEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckPaymentRequestEntryForm" type="com.struts.ap.checkpaymentrequestentry.ApCheckPaymentRequestEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepVoucherPrint.do?forward=1&voucherCode=<%=actionForm.getCheckPaymentRequestCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apCheckPaymentRequestEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckPaymentRequestEntryForm" type="com.struts.ap.checkpaymentrequestentry.ApCheckPaymentRequestEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apCheckPaymentRequestEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apCheckPaymentRequestEntryForm" type="com.struts.ap.checkpaymentrequestentry.ApCheckPaymentRequestEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
----</body>
</html>
