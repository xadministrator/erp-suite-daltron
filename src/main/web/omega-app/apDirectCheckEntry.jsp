<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="directCheckEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}
//Done Hiding-->
function fnOpenMisc(name){
	//alert("asdee");
   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;

   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array(); //= document.forms[0].elements["apCLIList[" + 0 + "].tagList[" + 1 + "].specs"].value;
   var custodian = new Array(document.forms[0].elements["userList"].value);
   var custodian2 = new Array();
   var propertyCode = new Array();
   var serialNumber = new Array();
   var expiryDate = new Array();

   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");
   //var userList = document.forms[0].elements["userList"].value;
   //check at least one disabled field
	   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
   var index = 0;
   //alert(document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value + "before while");
   /*while (true) {
	  //alert(document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value);
	  if (document.forms[0].elements[property + ".tagList[" + index + "].specs"].value!= null ||
		  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value!= null ||
		  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value!= null  ||
		  document.forms[0].elements[property + ".tagList[" + index + "].propertyCode"].value!=null) {
		  //alert("d2?");
	 	  specs[index] = document.forms[0].elements[property + ".tagList[" + index + "].specs"].value;

	 	  expiryDate[index] = document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value;
	 	  serialNumber[index] = document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value;
		  propertyCode[index] = document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value;
		  custodian2[index] = document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value;
		  //alert("inside if");
	  }else{
		  //alert("o d2?");
		  /*
		  expiryDate[index] = "";
		  propertyCode[index] = "";

		  document.forms[0].elements[property + ".tagList[" + index + "].specs"].value= "";
		  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value="";
		  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].custodian"].value="";
		  //alert("inside else")
		  break;
	  }
	  //alert("after while")
	  if (++index == quantity) break;
   }*/




   //alert(document.forms[0].elements["apRILList[" + index + "].tagList[" + index2 + "].propertyCode"].value);
   //var specsStr = document.forms[0].elements[property + ".specs"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
		   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2
		   + "&tgDocumentNumber=" + tgDocNum + "&serialNumber=" + serialNumber +"&isDisabled=" + isDisabled, "", "width=800,height=350,scrollbars=yes,status=no");

   return false;

}


</script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
  function enterBillAmount()
  {

    disableButtons();
    enableInputControls();
	document.forms[0].elements["isBillAmountEntered"].value = true;
	document.forms[0].submit();

  }
  function calculateAmountDue(name)
  {
      document.forms[0].elements["amount"].value = document.forms[0].elements[name].value;

  }
  function voucherEnterSubmit()
  {
      if (document.activeElement.name != 'billAmount' &&
      	  document.activeElement.name != 'memo') {
          return enterSubmit(event, new Array('saveSubmitButton'));
      } else {
          return true;
      }
  }
  function calculateAmount(name)
  {

  var property = name.substring(0,name.indexOf("."));
  var quantity = 0;
  var unitCost = 0;
  var amount = 0;
  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitCost"].value != "") {
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {

	  	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

	  amount = (quantity * unitCost).toFixed(2);

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  var totalDiscountAmount = 0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }
	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(2);

	  	  }

	  	  totalDiscountAmount = totalDiscountAmount.toFixed(2);

		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toString());
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());


      } else {

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

	  }

  }

}

  var tempAmount = 0;

  function initializeTempDebit(name)
  {

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
      } else {
          tempAmount = 0;
      }
  }

  function calculateTotalDebit(name)
  {
      var debitAmount = 0;
      var totalDebit = (document.forms[0].elements["totalDebit"].value).replace(/,/g,'');
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
          debitAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
      }
      if (!isNaN(totalDebit - (tempAmount - debitAmount))) {
	      document.forms[0].elements["totalDebit"].value = formatDisabledAmount((1 * totalDebit - (1 * tempAmount - 1 * debitAmount)).toFixed(2).toString());
	      tempAmount = debitAmount;
	  }
  }

  function initializeTempCredit(name)
  {
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
      } else {
          tempAmount = 0;
      }
  }

  function calculateTotalCredit(name)
  {
      var creditAmount = 0;
      var totalCredit = (document.forms[0].elements["totalCredit"].value).replace(/,/g,'');
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
          creditAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
      }
      if (!isNaN(totalCredit - (tempAmount - creditAmount))) {
	      document.forms[0].elements["totalCredit"].value = formatDisabledAmount((1 * totalCredit - (1 * tempAmount - 1 * creditAmount)).toFixed(2).toString());
	      tempAmount = creditAmount;
	  }
  }
  var currProperty;
  var currName;

  function fnOpenDiscount(name){
   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");
   return false;
  }

  function setTempSupplierName() {
	document.forms[0].elements["tempSupplierName"].value = document.forms[0].elements["supplierName"].value;
  }

    function setInvestorSelect(elem)
    {

       document.forms[0].elements["invtInscribedStock"].checked = false;
        document.forms[0].elements["invtTreasuryBill"].checked = false;

          elem.checked = true;
    }

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return voucherEnterSubmit();">
<html:form action="/apDirectCheckEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="directCheckEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apDirectCheckEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   </html:messages>
	        </td>
	     </tr>
	<logic:equal name="apDirectCheckEntryForm" property="type" value="ITEMS">
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isTaxCodeEntered" value=""/>
		 <html:hidden property="enableFields"/>
		 <html:hidden property="taxRate"/>
		 <html:hidden property="taxType"/>
		 <html:hidden property="isConversionDateEntered" value=""/>
		 <html:hidden property="isCurrencyEntered" value=""/>
		 <html:hidden property="tempSupplierName"/>
		 <html:hidden property="userList" />
	     <logic:equal name="apDirectCheckEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>
         				<tr>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
							</logic:equal>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="255" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" onblur="return setTempSupplierName();"/>
                			</td>
         				</tr>
			        </table>
					</div>

                                <div class="tabbertab" title="Info">
                                    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                        <tr>
                                            <td class="prompt" width="575" height="25" colspan="4">
                                            </td>
                                        </tr>

                                        <tr>
                                        <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.infoType"/>
                			</td>
                			<td width="158" height="25" class="control">
                                            <html:text property="infoType" size="15" maxlength="25" styleClass="text"/>
                			</td>
                                        </tr>
                                        <tr>
                                        <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.infoBioNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                                            <html:text property="infoBioNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                                        </tr>
                                        <tr>
                                        <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.infoBioDescription"/>
                			</td>
                			<td width="158" height="25" class="control">
                                            <html:text property="infoBioDescription" size="15" maxlength="25" styleClass="text"/>
                			</td>
                                        </tr>
                                        <tr>
                                        <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.infoTypeStatus"/>
                			</td>
                			<td width="158" height="25" class="control">
                                            <html:text property="infoTypeStatus" size="15" maxlength="25" styleClass="text"/>
                			</td>
                                        </tr>
                                        <tr>

                                        <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.infoRequestStatus"/>
                			</td>
                			<td width="158" height="25" class="control">
                                            <html:text property="infoRequestStatus" size="15" maxlength="25" styleClass="text"/>
                                        </td>
                                        </tr>
                                    </table>
                                </div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck"/>
                			</td>
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="return enterSelect('taxCode', 'isTaxCodeEntered');">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
			        <div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				     </table> --%>
			         </div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.CLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unit"/>
                       </td>
                       <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unitCost"/>
                       </td>
                       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.itemDiscount"/>
                       </td>
				       <td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apCLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name);" onkeyup="calculateAmount(name);"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>
                       <td width="145" height="1" class="control">
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name);" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
                       </td>
                       <td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="30" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
					   <td width="100" align="center" height="1">
					   	 <div id="buttons">
				   		    <nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc"/>
				   		    <nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		 <nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	 </div>
					   </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     <logic:equal name="apDirectCheckEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
                			<% } else {%>
    	     				<html:hidden property="type" value="EXPENSES"/>
        	 				<% } %>
	     					<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>

         				<tr>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>

                                        <div class="tabbertab" title="Info">
                                            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                                <tr>
                                                    <td class="prompt" width="575" height="25" colspan="4">
                                                </td>
                                                </tr>
                                                <tr>
                                                <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.infoType" />
                                                </td>
                                                <td width="158" height="25" class="control">
                                                    <html:text property="infoType" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="directCheckEntry.prompt.infoBioNumber"/>
                                                </td>
                                                <td width="158" height="25" class="control">
                                                    <html:text property="infoBioNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="directCheckEntry.prompt.infoBioDescription"/>
                                                </td>
                                                <td width="158" height="25" class="control">
                                                    <html:text property="infoBioDescription" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="directCheckEntry.prompt.infoTypeStatus"/>
                                                </td>
                                                <td width="158" height="25" class="control">
                                                    <html:text property="infoTypeStatus" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                                </td>
                                                </tr>
                                                <tr>

                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="directCheckEntry.prompt.infoRequestStatus"/>
                                                </td>
                                                <td width="158" height="25" class="control">
                                                    <html:text property="infoRequestStatus" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                                </td>
                                                </tr>
                                            </table>
                                        </div>


					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>






			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
			      <div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt" >
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true" />
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
							<td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						  <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2" disabled="true">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
							<td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>

			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2" disabled="true">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
							<td width="127" height="25" class="control">
								<div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
							</td>
						</tr>
				     </table> --%>
			         </div>

			         <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>

					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.CLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unit"/>
                       </td>
                       <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unitCost"/>
                       </td>
                       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.amount"/>
                       </td>
				       <td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apCLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>
                       <td width="145" height="1" class="control">
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>
                       <td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="30" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
					   <td width="100" align="center" height="1">
					   	 <div id="buttons">
				   		    <nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc"/>
					   		 <nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	 </div>
					   	  <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		 <nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	 </div>
					   </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>
	     <logic:equal name="apDirectCheckEntryForm" property="type" value="EXPENSES">
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isBillAmountEntered" value=""/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
		 <html:hidden property="isCurrencyEntered" value=""/>
		 
		 <html:hidden property="tempSupplierName"/>
	     <logic:equal name="apDirectCheckEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
			                <td width="130" height="25" class="prompt">
            			       <bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
				        <% } else {%>
        				<html:hidden property="type" value="EXPENSES"/>
         				<% } %>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>
         				<tr>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
							</logic:equal>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control" colspan="3">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
        	 			<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="255" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
               	 			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.billAmount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="billAmount" size="15" maxlength="25" styleClass="textAmountRequired" onblur="addZeroes(name); return enterBillAmount();" onkeyup="formatAmount(name, (event)?event:window.event);"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" onblur="return setTempSupplierName();"/>
                			</td>
         				</tr>
			        </table>
					</div>

                                <div class="tabbertab" title="Info">
                                   <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                        <tr>
                                            <td class="prompt" width="575" height="25" colspan="4">
                                        </td>
                                        </tr>

                                        <tr>
                                        <td width="130" height="25" class="prompt">
                                            <bean:message key="directCheckEntry.prompt.infoType" />
                                        </td>
                                        <td width="158" height="25" class="control">
                                            <html:text property="infoType" size="15" maxlength="25" styleClass="text"/>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.infoBioNumber"/>
                                        </td>
                                        <td width="158" height="25" class="control">
                                            <html:text property="infoBioNumber" size="15" maxlength="25" styleClass="text" />
                                        </td>
                                        </tr>

                                        <tr>
                                        <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.infoBioDescription"/>
                                        </td>
                                        <td width="158" height="25" class="control">
                                            <html:text property="infoBioDescription" size="15" maxlength="25" styleClass="text"/>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.infoTypeStatus"/>
                                        </td>
                                        <td width="158" height="25" class="control">
                                            <html:text property="infoTypeStatus" size="15" maxlength="25" styleClass="text" />
                                        </td>
                                        </tr>

                                        <tr>

                                        <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.infoRequestStatus"/>
                                        </td>
                                        <td width="158" height="25" class="control">
                                            <html:text property="infoRequestStatus" size="15" maxlength="25" styleClass="text"/>
                                        </td>
                                        </tr>
                                    </table>
                               </div>


			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
               				<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" onchange="return enterBillAmount();">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck"/>
                			</td>
         				</tr>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>


				<logic:equal name="apDirectCheckEntryForm" property="isInvestment" value="true">
                                <div class="tabbertab" title="Investment">
                                    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                        <tr>
                                            <td class="prompt" width="575" height="25" colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.invtInscribedStock" />
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:checkbox property="invtInscribedStock" onclick="setInvestorSelect(this)"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtTreasuryBill"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:checkbox property="invtTreasuryBill" onclick="setInvestorSelect(this)"/>
                                            </td>

                                        </tr>


                                        <tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtNextRunDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtNextRunDate" size="10" maxlength="10" styleClass="textRequired"/>
                                            </td>


         								</tr>

         				<tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtSettlementDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtSettlementDate" size="10" maxlength="10" styleClass="textRequired"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtMaturityDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtMaturityDate" size="10" maxlength="10" styleClass="textRequired"/>
                                            </td>

         				</tr>


         				<tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtBidYield"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtBidYield" size="15" maxlength="25" styleClass="textAmountRequired" />
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtCouponRate"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtCouponRate" size="15" maxlength="25" styleClass="textAmountRequired" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtSettlementAmount"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtSettlementAmount" size="15" maxlength="25" styleClass="textAmountRequired" />
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtFaceValue"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtFaceValue" size="15" maxlength="25" styleClass="textAmountRequired" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtPremiumAmount"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtPremiumAmount" size="15" maxlength="25" styleClass="textAmountRequired" />
                                            </td>

                                        </tr>

                                    </table>
                                </div>
                                </logic:equal>



			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="return enterBillAmount();">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" onchange="return enterBillAmount();">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>

			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.conversionRate"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">

		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text"/>
				    		</td>
			       		</tr>

			        </table>
			        </div>
			        <div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				     </table> --%>
			         </div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.DCDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.accountDescription"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apDCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:notEqual property="drClass" value="<%=Constants.AP_DR_CLASS_CASH%>">
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name); addZeroes(name);" onkeyup="calculateTotalDebit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempCredit(name);"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:notEqual>
				    <nested:equal property="drClass" value="<%=Constants.AP_DR_CLASS_CASH%>">
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onfocus="initializeTempCredit(name);" onblur="calculateAmountDue(name); calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateAmountDue(name); calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:equal>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     <logic:equal name="apDirectCheckEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
			                <td width="130" height="25" class="prompt">
            			       <bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
				        <% } else {%>
        				<html:hidden property="type" value="EXPENSES"/>
         				<% } %>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>
         				<tr>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control" colspan="3">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
        	 			<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
               	 			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.billAmount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="billAmount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>


                                <div class="tabbertab" title="Info">
                                      <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                            <tr>
                                                <td class="prompt" width="575" height="25" colspan="4">
                                            </td>
                                            </tr>

                                            <tr>
                                            <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.infoType" />
                                            </td>
                                            <td width="158" height="25" class="control">
                                                <html:text property="infoType" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                            </td>
                                            </tr>

                                            <tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.infoBioNumber"/>
                                            </td>
                                            <td width="158" height="25" class="control">
                                                <html:text property="infoBioNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                            </td>
                                            </tr>

                                            <tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.infoBioDescription"/>
                                            </td>
                                            <td width="158" height="25" class="control">
                                                <html:text property="infoBioDescription" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                            </td>
                                            </tr>

                                            <tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.infoTypeStatus"/>
                                            </td>
                                            <td width="158" height="25" class="control">
                                                <html:text property="infoTypeStatus" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                            </td>
                                            </tr>

                                            <tr>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.infoRequestStatus"/>
                                            </td>
                                            <td width="158" height="25" class="control">
                                                <html:text property="infoRequestStatus" size="15" maxlength="25" styleClass="text" disabled="true"/>
                                            </td>
                                            </tr>
                                        </table>
                                </div>


			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
               				<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>

                                           <logic:equal name="apDirectCheckEntryForm" property="isInvestment" value="true">
                                <div class="tabbertab" title="Investment">
                                    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                        <tr>
                                            <td class="prompt" width="575" height="25" colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="130" height="25" class="prompt">
                                                <bean:message key="directCheckEntry.prompt.invtInscribedStock" />
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:checkbox property="invtInscribedStock" onclick="setInvestorSelect(this)" disabled="true"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtTreasuryBill"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:checkbox property="invtTreasuryBill" onclick="setInvestorSelect(this)" disabled="true"/>
                                            </td>

                                        </tr>


                                        <tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtNextRunDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtNextRunDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                                            </td>


         				</tr>

         				<tr>
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtSettlementDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtSettlementDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtMaturityDate"/>
                                            </td>
                                            <td width="157" height="25" class="control" colspan="3">
                                                    <html:text property="invtMaturityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                                            </td>

         				</tr>


         				<tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtBidYield"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtBidYield" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtCouponRate"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtCouponRate" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtSettlementAmount"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtSettlementAmount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                                            </td>

                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="directCheckEntry.prompt.invtFaceValue"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtFaceValue" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.invtPremiumAmount"/>
                                            </td>
                                            <td width="445" height="25" class="control" colspan="3">
                                                    <html:text property="invtPremiumAmount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                                            </td>

                                        </tr>

                                    </table>
                                </div>
                                </logic:equal>







			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
			   		<div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				     </table> --%>
			         </div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apDirectCheckEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apDirectCheckEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.DCDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.accountDescription"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apDCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif"  disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>

	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  </td>
	       </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="apDirectCheckEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     <% String encodedSupplierName = java.net.URLEncoder.encode(actionForm.getTempSupplierName()); %>
  	 win = window.open("<%=request.getContextPath()%>/apRepCheckPrint.do?forward=1&checkCode=<%=actionForm.getCheckCode()%>&supplierName=<%=encodedSupplierName%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apDirectCheckEntryForm" property="report2" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     <% String encodedSupplierName = java.net.URLEncoder.encode(actionForm.getTempSupplierName()); %>
  	 win = window.open("<%=request.getContextPath()%>/apRepCheckVoucherPrint.do?forward=1&checkCode=<%=actionForm.getCheckCode()%>&supplierName=<%=encodedSupplierName%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<%-- <logic:equal name="apDirectCheckEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");
</script>
</logic:equal>
<logic:equal name="apDirectCheckEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");
</script>
</logic:equal> --%>
----</body>
</html>
