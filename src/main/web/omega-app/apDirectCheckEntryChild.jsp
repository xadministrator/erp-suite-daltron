<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="directCheckEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
 
function submitForm()
{            
      disableButtons();
      enableInputControls();
}


//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 
  
  function enterBillAmount()
  {
    
    disableButtons();
    enableInputControls();   	
	document.forms[0].elements["isBillAmountEntered"].value = true;
	document.forms[0].submit();
	   
  }
  

var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));  
   currProperty = property;
   currName = name;   
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");
   
   return false;

}

function fnOpenMisc(name){

   var property = name.substring(0,name.indexOf("."));  
   currProperty = property;
   currName = name;   
   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");

   return false;

}
  
//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/apDirectCheckEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="directCheckEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apDirectCheckEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	     <logic:equal name="apDirectCheckEntryForm" property="type" value="EXPENSES">
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isBillAmountEntered" value=""/>
          <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>	
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>	     
			                <td width="130" height="25" class="prompt">
            			       <bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         
				        <% } else {%>
        				<html:hidden property="type" value="EXPENSES"/>         
         				<% } %>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>         
         				<tr>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
	    	 				<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control" colspan="3">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr> 
        	 			<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
               	 			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.billAmount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="billAmount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
               				<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>  
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>         
         				</tr>                          	     
         				<tr>               
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control"">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>    
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>	
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr> 
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>               
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr> 
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         	         				         				         			         				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.DCDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.accountDescription"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apDCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif"  disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		   </logic:equal>
		   <logic:equal name="apDirectCheckEntryForm" property="type" value="ITEMS">
		   <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         
	     					<logic:equal name="apDirectCheckEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>        
         				<tr>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
	     					<logic:equal name="apDirectCheckEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                			
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="debitMemoEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
			        </table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="directCheckEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apDirectCheckEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>                
         				</tr>                         	     
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="directCheckEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="directCheckEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck" disabled="true"/>
                			</td>                        
         				</tr> 
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="directCheckEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr> 
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apDirectCheckEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>               
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr> 
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="directCheckEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>	 
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>			
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         	         				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="directCheckEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	      </tr>
	      <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	      </tr>
	      <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="directCheckEntry.gridTitle.CLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.location"/>
				       </td>				       				    
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unit"/>
                       </td>
                       <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.unitCost"/>
                       </td>
                       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
		               <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="directCheckEntry.prompt.itemDiscount"/>
				       </td>				       
				       <td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="directCheckEntry.prompt.amount"/>
                       </td>			          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apCLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="145" height="1" class="control">
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                       </td>
                       <td width="50" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="30" height="1" class="control"/>				   
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>				       	
				       <td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
					   <td width="50" align="center" height="1">
					    <div id="buttons">
				   			<nested:hidden property="discount1"/>
						   	<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc"/>
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   	</div>
					   </td> 				        			                                     
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apDirectCheckEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apDirectCheckEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	       </tr>	
	       </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="apDirectCheckEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apDirectCheckEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apDirectCheckEntryForm" type="com.struts.ap.directcheckentry.ApDirectCheckEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
----</body>
</html>
