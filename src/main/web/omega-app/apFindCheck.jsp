<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findCheck.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apFindCheck.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findCheck.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apFindCheckForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     
	     
	     
	     
	     
	     
	     
	     
	     <!--
	     -->	     
	     
	     
	     <logic:equal name="apFindCheckForm" property="showBatchName" value="true">
	     	 <tr>
                <td class="prompt" width="140" height="25">
                   <bean:message key="findCheck.prompt.batchName"/>
                </td>
                <td width="147" height="25" class="control">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
		         <td class="prompt" width="130" height="25">
		            &nbsp;
		         </td>
		         <td width="147" height="25" class="control">
		            &nbsp;
		         </td>
	         </tr>
	         </logic:equal>	

	     <tr>
		     <logic:equal name="apFindCheckForm" property="useSupplierPulldown" value="true">			
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.supplierCode"/>
	         </td>
	         <td width="147" height="25" class="control" colspan="2">
	            <html:select property="supplierCode" styleClass="combo">
			      <html:options property="supplierCodeList"/>
			   </html:select>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>
	         <td width="147" height="25" class="control">
	            &nbsp;
	         </td>
	         </logic:equal>	
		     <logic:equal name="apFindCheckForm" property="useSupplierPulldown" value="false">			
	         <td class="prompt" width="160" height="25">
	            <bean:message key="findCheck.prompt.supplierCode"/>
	         </td>
	         <td width="415" height="25" class="control" colspan="2">
			   <html:text property="supplierCode" size="15" maxlength="25" styleClass="text" readonly="true"/>	
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>
	         </logic:equal>	
         </tr> 
	     
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.checkType"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="checkType" styleClass="combo">
			      <html:options property="checkTypeList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="130" height="25">
	            &nbsp;
	         </td>
	         <td width="147" height="25" class="control">
	            &nbsp;
	         </td>
         </tr>
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.bankAccount"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="bankAccount" styleClass="combo">
			      <html:options property="bankAccountList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="130" height="25">
	            <bean:message key="findCheck.prompt.checkVoid"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:checkbox property="checkVoid"/>
	         </td>
         </tr>          
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	         </td>
         </tr> 
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.numberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="numberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.numberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="numberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.documentNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.documentNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr> 
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.referenceNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="referenceNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.referenceNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="referenceNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>                                 
         
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.released"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="released" styleClass="combo">
			      <html:options property="releasedList"/>
			   </html:select>
	         </td>         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.approvalStatus"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="approvalStatus" styleClass="combo">
			      <html:options property="approvalStatusList"/>
			   </html:select>
	         </td>
         </tr>                                  
         
         
         
         <tr>	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.posted"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="posted" styleClass="combo">
			      <html:options property="postedList"/>
			   </html:select>
	         </td>    
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findCheck.prompt.orderBy"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	              
         </tr>
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="apFindCheckForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="apFindCheckForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindCheckForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="8" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="findCheck.gridTitle.FCDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.date"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.checkType"/>
			       </td>			       
			       <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.supplierName"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.checkNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.documentNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.referenceNumber"/>
			       </td>			       			       			       			       
			       <td width="229" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCheck.prompt.amount"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="apFCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="checkType"/>
			       </td>
			       <td width="250" height="1" class="gridLabel">
			          <nested:write property="supplierName"/>
			       </td>
			       <td width="140" height="1" class="gridLabel">
			          <nested:write property="checkNumber"/>
			       </td>
			       <td width="110" height="1" class="gridLabel">
			          <nested:write property="documentNumber"/>
			       </td>
			       <td width="110" height="1" class="gridLabel">
			          <nested:write property="referenceNumber"/>
			       </td>
			       <td width="159" height="1" class="gridLabelNum">
			          <nested:write property="amount"/>
			       </td>			       			       			       			       
			       <td width="70" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="apFindCheckForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="apFCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="findCheck.prompt.checkType"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="checkType"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.supplierName"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="supplierName"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.bankAccount"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="bankAccount"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.date"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="date"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.checkNumber"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="checkNumber"/>
			      </td>
			    </tr>			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.documentNumber"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="documentNumber"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.amount"/>
			      </td>
			      <td width="149" height="1" class="gridLabelNum">
			         <nested:write property="amount"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCheck.prompt.released"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="released"/>
			      </td>				    	      
			    </tr>				    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["checkType"] != null &&
        document.forms[0].elements["checkType"].disabled == false)
        document.forms[0].elements["checkType"].focus()
   // -->
</script>
</body>
</html>
