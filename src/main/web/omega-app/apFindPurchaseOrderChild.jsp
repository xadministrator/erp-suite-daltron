<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findPurchaseOrder.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickAccount(name) 
{

   var property = name.substring(0,name.indexOf("."));      
      
   if (window.opener && !window.opener.closed) {

	
	   var divList = window.opener.document.body.getElementsByTagName("div");
      
       for (var i = 0; i < divList.length; i++) {
      
          var currDiv = divList[i];
          var currId = currDiv.id;
          
          if (currId == "buttons") {
          
              currDiv.style.display = "none";
          
          } else if (currId == "buttonsDisabled") {
          
              currDiv.style.display = "block";
          
          }
      
       }
    
	   for (var i=0; i < window.opener.document.forms[0].elements.length; i++) {
	      
	      if (window.opener.document.forms[0].elements[i].type != 'submit') {
	          
	          window.opener.document.forms[0].elements[i].disabled=false;
	          
	      }
	   }
	 
       window.opener.document.forms[0].elements[document.forms[0].elements["selectedPoNumber"].value].value = 
       document.forms[0].elements[property + ".documentNumber"].value;   
      
       window.opener.document.forms[0].elements["supplier"].value = document.forms[0].elements[property + ".supplierCode"].value;    
       window.opener.document.forms[0].elements["isPurchaseOrderEntered"].value = true;                                                    
       window.opener.document.forms[0].submit();
            
   
   }
   
   window.close();
   
   return false;

}

function closeWin() 
{
   window.close();
   
   return false;
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apFindPurchaseOrder.do?child=1" onsubmit="return disableButtons();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="findPurchaseOrder.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="apFindPurchaseOrderForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>
	         <html:hidden property="selectedPoNumber"/>
            <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findPurchaseOrder.prompt.supplierCode"/>
                </td>
				<logic:equal name="apFindPurchaseOrderForm" property="useSupplierPulldown" value="true">
		        <td width="110" height="25" class="control">
	               <html:select property="supplierCode" styleClass="combo">
                      <html:options property="supplierCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
				</logic:equal>
		        <logic:equal name="apFindPurchaseOrderForm" property="useSupplierPulldown" value="false">
		        <td width="110" height="25" class="control">
	               <html:text property="supplierCode" styleClass="text" size="12" maxlength="25" readonly="true"/>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
				</logic:equal>
             </tr>            
             <!--  
             <tr>
             	<logic:equal name="apFindPurchaseOrderForm" property="isVoucher" value="1">
             	<td class="prompt" width="180" height="25">
                   <bean:message key="findPurchaseOrder.prompt.receiving"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:checkbox property="receiving"/>
		        </td>
		        </logic:equal>
             </tr> 
             
             -->
            <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findPurchaseOrder.prompt.documentNumberFrom"/>
                </td>
		        <td width="110" height="25" class="control">
	               <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findPurchaseOrder.prompt.documentNumberTo"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>		        
	        </tr> 
	        <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findPurchaseOrder.prompt.dateFrom"/>
                </td>
		        <td width="110" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findPurchaseOrder.prompt.dateTo"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>		        
	        </tr>             		        	         
            <tr>
				<td class="prompt" width="185" height="25">
                   <bean:message key="findPurchaseOrder.prompt.referenceNumber"/>
                </td>
		        <td width="390" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>                                       
		        </td>		      	               
	        </tr>
	        <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="findPurchaseOrder.prompt.currency"/>
		       </td>
		       <td width="390" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>		      
	         <tr>
	           <td class="prompt" width="185" height="25">
		           <bean:message key="findPurchaseOrder.prompt.orderBy"/>
		        </td>
		        <td width="390" height="25" class="control" colpsan="3">
		           <html:select property="orderBy" styleClass="combo">
		              <html:options property="orderByList"/>
			       </html:select>
		        </td>	  
	         </tr>         
	       	<tr>
	        	<td width="575" height="50" colspan="4">
	         	<table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			    	<tr>
			        	<td width="290" height="50">
			            	<div id="buttons">
			            	<logic:equal name="apFindPurchaseOrderForm" property="disableFirstButton" value="false">
			             	<html:submit property="firstButton" styleClass="mainButtonSmall">
			             		<bean:message key="button.first"/>
			             	</html:submit>
			             	</logic:equal>
			             	<logic:equal name="apFindPurchaseOrderForm" property="disableFirstButton" value="true">
			             	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             		<bean:message key="button.first"/>
			             	</html:submit>
			             	</logic:equal>
			             	<logic:equal name="apFindPurchaseOrderForm" property="disablePreviousButton" value="false">
			             	<html:submit property="previousButton" styleClass="mainButtonSmall">
					        	<bean:message key="button.previous"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disablePreviousButton" value="true">
			             	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.previous"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableNextButton" value="false">
					     	<html:submit property="nextButton" styleClass="mainButtonSmall">
					        	<bean:message key="button.next"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableNextButton" value="true">
					     	<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.next"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableLastButton" value="false">
				            <html:submit property="lastButton" styleClass="mainButtonSmall">
				             	<bean:message key="button.last"/>
				            </html:submit>
				            </logic:equal>
				            <logic:equal name="apFindPurchaseOrderForm" property="disableLastButton" value="true">
				            <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
				             	<bean:message key="button.last"/>
				            </html:submit>
				            </logic:equal>
			             	<logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             	<html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					        	<bean:message key="button.showDetails"/>
					     	</html:submit>
					     	</logic:equal>			     
			             	<logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             	<html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					        	<bean:message key="button.hideDetails"/>
					     	</html:submit>
					     	</logic:equal>			     			     			     
					     	</div>
					     	<div id="buttonsDisabled" style="display: none;">
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableFirstButton" value="false">
				            <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
				             	<bean:message key="button.first"/>
				            </html:submit>
				            </logic:equal>
				            <logic:equal name="apFindPurchaseOrderForm" property="disableFirstButton" value="true">
				            <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
				             	<bean:message key="button.first"/>
				            </html:submit>
				            </logic:equal>
			             	<logic:equal name="apFindPurchaseOrderForm" property="disablePreviousButton" value="false">
			             	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.previous"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disablePreviousButton" value="true">
			             	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.previous"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableNextButton" value="false">
					     	<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.next"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableNextButton" value="true">
					     	<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					        	<bean:message key="button.next"/>
					     	</html:submit>
					     	</logic:equal>
					     	<logic:equal name="apFindPurchaseOrderForm" property="disableLastButton" value="false">
				            <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
				             	<bean:message key="button.last"/>
				            </html:submit>
				            </logic:equal>
				            <logic:equal name="apFindPurchaseOrderForm" property="disableLastButton" value="true">
				            <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
				             	<bean:message key="button.last"/>
				            </html:submit>
				            </logic:equal>
			             	<logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             	<html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					        	<bean:message key="button.showDetails"/>
					     	</html:submit>
					     	</logic:equal>			     
			             	<logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             	<html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					        	<bean:message key="button.hideDetails"/>
					     	</html:submit>
					     	</logic:equal>			     
					     	</div>
					  	</td>
					  	<td width="285" height="50" colspan="3">
			            	<div id="buttons">
			             	<p align="right">
				         	<html:submit property="goButton" styleClass="mainButton">
				         		<bean:message key="button.go"/>
				         	</html:submit>
				         	<html:submit property="closeButton" styleClass="mainButton" onclick="return closeWin();">
				         		<bean:message key="button.close"/>
				         	</html:submit>
				         	</div>
				         	<div id="buttonsDisabled" style="display: none;">
			             	<p align="right">
				         	<html:submit property="goButton" styleClass="mainButton" disabled="true">
				         		<bean:message key="button.go"/>
				         	</html:submit>
				         	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         		<bean:message key="button.close"/>
				         	</html:submit>
				         	</div>		         
				      	</td>
			     	</tr>
	         	</table>
	         	</td>        
	     	</tr>                 	                                                	                                 
	       	<tr valign="top">
	        	<td width="575" height="185" colspan="6">
		        <div align="center">
				<table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		        	<tr>
                		<td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
							<bean:message key="findPurchaseOrder.gridTitle.FPODetails"/>
	               		</td>
	          		</tr>
	            	<tr>
			    		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findPurchaseOrder.prompt.date"/>
			       		</td>
			       		<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findPurchaseOrder.prompt.documentNumber"/>
			       		</td>
                   		<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findPurchaseOrder.prompt.referenceNumber"/>
			       		</td>	
			       		<td width="320" height="1" class="gridHeader" colspan="2"bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			                <bean:message key="findPurchaseOrder.prompt.supplierCode"/>
        			    </td>	       
                	</tr>
					<logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
					<%
			    		int i = 0;	
			        	String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			        	String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			        	String rowBgc = null;
			    	%>
			    	<nested:iterate property="apFPOList">
			    	<%
			    		i++;
			        	if((i % 2) != 0){
			        		rowBgc = ROW_BGC1;
			        	}else{
			        		rowBgc = ROW_BGC2;
			        	}  
			   		%>
			   		  <tr bgcolor="<%= rowBgc %>">
			    		<td width="134" height="1" class="gridLabel">
			        		<nested:write property="date"/>
			        	</td>
						<td width="220" height="1" class="gridLabel">
			        		<nested:write property="documentNumber"/>
			        		<nested:hidden property="documentNumber"/>
			        	</td>	
			        	<td width="220" height="1" class="gridLabel">
			        		<nested:write property="referenceNumber"/>
			       		</td>
			       		<td width="220" height="1" class="gridLabel">
			        		<nested:write property="supplierCode"/>
			        		<nested:hidden property="supplierCode"/>
			       		</td>			             			             			             
			       		<td width="100" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                           <bean:message key="button.select"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.select"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			  		</tr>		   					   		
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="apFindPurchaseOrderForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			    	<%
			        	int i = 0;	
			            String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			            String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			            String rowBgc = null;
			        %>
			        <nested:iterate property="apFPOList">
			        <%
			        	i++;
			            if((i % 2) != 0){
			            	rowBgc = ROW_BGC1;
			           	}else{
			            	rowBgc = ROW_BGC2;
			            }  
			      	%>
			      	<tr bgcolor="<%= rowBgc %>">
			        	<td width="175" height="1" class="gridHeader">
			            	<bean:message key="findPurchaseOrder.prompt.supplierCode"/>
			           	</td>
			           	<td width="570" height="1" class="gridLabel" colspan="2">
			            	<nested:write property="supplierCode"/>
			           	</td>
			             <td width="149" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                           <bean:message key="button.select"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.select"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			      	</tr>
					<tr bgcolor="<%= rowBgc %>">
					    <td width="175" height="1" class="gridHeader">
			                <bean:message key="findPurchaseOrder.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			        	<td width="220" height="1" class="gridHeader">
			            	<bean:message key="findPurchaseOrder.prompt.documentNumber"/>
			           	</td>
			           	<td width="149" height="1" class="gridLabel">
			            	<nested:write property="documentNumber"/>
			            	<nested:hidden property="documentNumber"/>
			           	</td>
                   	</tr>	
                    <tr bgcolor="<%= rowBgc %>">
			        	<td width="175" height="1" class="gridHeader">
			            	<bean:message key="findPurchaseOrder.prompt.referenceNumber"/>
			            </td>
			            <td width="719" height="1" class="gridLabel" colspan="3">
			            	<nested:write property="referenceNumber"/>
			            </td>
                   	</tr>				          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["date"] != null)
             document.forms[0].elements["date"].focus()
   // -->
</script>
</body>
</html>
