<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findVoucher.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">

function deleteAllLine(){
	
	if(confirm("Are you sure you want to delete all lines")){
	return true;
	}else{
	return false;}
}

</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apFindVoucher.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="findVoucher.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="apFindVoucherForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>

			<logic:equal name="apFindVoucherForm" property="showBatchName" value="true">
	         <tr>
		       <td class="prompt" width="155" height="25">
		          	<bean:message key="findVoucher.prompt.batchName"/>
		       </td>
		       <td width="103" height="25" class="control">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
		       </td>	         
		       <td class="prompt" width="150" height="25">
	               &nbsp;
		       </td>
		       <td width="124" height="25" class="control">
					&nbsp;
		       </td>	         
	        </tr>
			</logic:equal>
			<logic:equal name="apFindVoucherForm" property="useSupplierPulldown" value="true">
	         <tr>
		       <td class="prompt" width="155" height="25">
                   <bean:message key="findVoucher.prompt.supplierCode"/>
		       </td>
		       <td width="103" height="25" class="control" colspan="2">
	               <html:select property="supplierCode" styleClass="combo">
                      <html:options property="supplierCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		       </td>	         
		       <td width="124" height="25" class="control">
					&nbsp;
		       </td>	         
	        </tr>
			</logic:equal>
			<logic:equal name="apFindVoucherForm" property="useSupplierPulldown" value="false">
	         <tr>
		       <td class="prompt" width="155" height="25">
                   <bean:message key="findVoucher.prompt.supplierCode"/>
		       </td>
		       <td width="103" height="25" class="control" colspan="2">
					<html:text property="supplierCode" size="15" maxlength="25" styleClass="text" readonly="true"/>                                       
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		       </td>	         
		       <td width="124" height="25" class="control">
					&nbsp;
		       </td>	         
	        </tr>
			</logic:equal>

	         <tr>
		       <td class="prompt" width="155" height="25">
		          	<bean:message key="findVoucher.prompt.paymentRequest"/>
		       </td>
		       <td width="103" height="25" class="control">
					<html:checkbox property="paymentRequest"/>
		       </td>	         
		       <td class="prompt" width="150" height="25">
	               <bean:message key="findVoucher.prompt.user"/>
		       </td>
		       <td width="124" height="25" class="control">
			       <html:select property="user" styleClass="combo">
			      	 <html:options property="userList"/>
			       </html:select>
		       </td>	         
	        </tr>
	        
	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.debitMemo"/>
		       </td>
		       <td width="103" height="25" class="control">
					<html:checkbox property="debitMemo"/>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.voucherVoid"/>
		       </td>
		       <td width="124" height="25" class="control">
					<html:checkbox property="voucherVoid"/>
		       </td>
		       <html:hidden property="voucherGenerated"/>	         
	        </tr>
	        
	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.dateFrom"/>
		       </td>
		       <td width="103" height="25" class="control">
					<html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.dateTo"/>
		       </td>
		       <td width="124" height="25" class="control">
					<html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
		       </td>	         
	        </tr>
	        
	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.documentNumberFrom"/>
		       </td>
		       <td width="103" height="25" class="control">
					<html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.documentNumberTo"/>
		       </td>
		       <td width="124" height="25" class="control">
					<html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
		       </td>	         
	        </tr>

	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.referenceNumber"/>
		       </td>
		       <td width="103" height="25" class="control">
					<html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.approvalStatus"/>
		       </td>
		       <td width="124" height="25" class="control">
	               <html:select property="approvalStatus" styleClass="combo">
	                  <html:options property="approvalStatusList"/>
		           </html:select>
		       </td>	         
	        </tr>	        
	        
	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.currency"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.posted"/>
		       </td>
		       <td width="100" height="25" class="control">
		          <html:select property="posted" styleClass="combo">
			        <html:options property="postedList"/>
			      </html:select>
		       </td>	         
	        </tr>
	        
	         <tr>
		       <td class="prompt" width="155" height="25">
		          <bean:message key="findVoucher.prompt.paymentStatus"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="paymentStatus" styleClass="combo">
			        <html:options property="paymentStatusList"/>
			      </html:select>
		       </td>	         
		       <td class="prompt" width="150" height="25">
		          <bean:message key="findVoucher.prompt.orderBy"/>
		       </td>
		       <td width="100" height="25" class="control">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>	         
	        </tr>
	        
	       <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="350" height="50">
			             <div id="buttons">
			             <logic:equal name="apFindVoucherForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>	
					     <html:submit property="deleteAllButton" styleClass="mainButtonMedium" onclick="return deleteAllLine()">
			             	Delete All
			             </html:submit>
					     		     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="apFindVoucherForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apFindVoucherForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>	
					     
					     <html:submit property="deleteAllButton" styleClass="mainButtonMedium" disabled="true" onclick="return deleteAllLine()">
			             	Delete All
			             </html:submit>		     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="findVoucher.gridTitle.FVDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.date"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.referenceNumber"/>
			       </td>			       
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.documentNumber"/>
			       </td>			       			       
			       <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.supplierName"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.amountDue"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findVoucher.prompt.open"/>
			       </td>
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apFVList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="140" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             
			             <td width="140" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>			             			             
			             <td width="250" height="1" class="gridLabel">
			                <nested:write property="supplierName"/>
			             </td>
			             <td width="144" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			             <td width="100" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="apFindVoucherForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apFVList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findVoucher.prompt.supplierName"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="supplierName"/>
			             </td>
			             <td width="149" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findVoucher.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="findVoucher.prompt.documentNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findVoucher.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="findVoucher.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			          </tr>
					  <tr bgcolor="<%= rowBgc %>">
		                 <td width="175" height="1" class="gridHeader">
		                     <bean:message key="findVoucher.prompt.amountPaid"/>
		                 </td>
		                 <td width="350" height="1" class="gridLabelNum" colspan="1">
		                     <nested:write property="amountPaid"/>
		                 </td>
		                 <td width="369" height="1" class="gridLabel" colspan="2"></td>
		              </tr>			          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
</body>
</html>
