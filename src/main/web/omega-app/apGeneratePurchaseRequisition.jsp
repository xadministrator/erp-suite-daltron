<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="generatePurchaseRequisition.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers

function confirmGeneratePurchaseRequisition()
{
   if(confirm("Are you sure you want to Generate Purchase Requisition?")) return true;
      else return false;
} 

//Done Hiding--> 
</script>
<link href="css/tabber.css" type=text/css rel=stylesheet>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apGeneratePurchaseRequisition.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="350" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     	<tr>
	        	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="generatePurchaseRequisition.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="35" colspan="4" class="statusBar">
		   			<logic:equal name="apGeneratePurchaseRequisitionForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      	<bean:message key="app.success"/>
                   	</logic:equal>
		   			<html:errors/>	
	        	</td>
	     	</tr>
	     	<tr>
		    	<td class="prompt" width="120" height="25">
        			<bean:message key="generatePurchaseRequisition.prompt.itemName"/>
		        </td>
     			<td width="168" height="25" class="control">
		            <html:text property="itemName" size="25" maxlength="25" styleClass="text"/>
     			</td>
	        	<td class="prompt" width="120" height="25">
					<bean:message key="generatePurchaseRequisition.prompt.itemClass"/>
				</td>
				<td width="167" height="25" class="control">
					<html:select property="itemClass" styleClass="combo">
						<html:options property="itemClassList"/>
					</html:select>
				</td>
			</tr>
 			<tr>
				<td class="prompt" width="120" height="25">
					<bean:message key="generatePurchaseRequisition.prompt.itemCategory"/>
				</td>
				<td width="168" height="25" class="control">
					<html:select property="itemCategory" styleClass="combo">
						<html:options property="itemCategoryList"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<td class="prompt" width="120" height="25">
					<bean:message key="generatePurchaseRequisition.prompt.orderBy"/>
				</td>
				<td width="167" height="25" class="control">
					<html:select property="orderBy" styleClass="combo">
						<html:options property="orderByList"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<td class="prompt" width="120" height="25">
					<bean:message key="generatePurchaseRequisition.prompt.branch"/>
				</td>
				<td width="168" height="25" class="control">
					<html:select property="branchNameSelectedList" size="6" multiple="true" styleClass="comboRequired">
					   <html:options property="branchNameList"/>
					</html:select>	 	
				</td>

			</tr>  			      
			<tr>
				
			</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	         	<table border="0" cellpadding="0" cellspacing="0" width="575" height="50" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     	<tr>
			        	<td width="290" height="50">
			             	<div id="buttons">
			             	<logic:equal name="apGeneratePurchaseRequisitionForm" property="disablePreviousButton" value="false">
				            <html:submit property="previousButton" styleClass="mainButtonSmall">
						        <bean:message key="button.previous"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disablePreviousButton" value="true">
				            <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.previous"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disableNextButton" value="false">
						    <html:submit property="nextButton" styleClass="mainButtonSmall">
						        <bean:message key="button.next"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disableNextButton" value="true">
						    <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.next"/>
						    </html:submit>
						    </logic:equal>
						    </div>
						    <div id="buttonsDisabled" style="display: none;">
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disablePreviousButton" value="false">
				            <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.previous"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disablePreviousButton" value="true">
				            <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.previous"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disableNextButton" value="false">
						    <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.next"/>
						    </html:submit>
						    </logic:equal>
						    <logic:equal name="apGeneratePurchaseRequisitionForm" property="disableNextButton" value="true">
						    <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
						        <bean:message key="button.next"/>
						    </html:submit>
						    </logic:equal>
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="generateButton" styleClass="mainButton" onclick="return confirmGeneratePurchaseRequisition();">
				         <bean:message key="button.generate"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="generateButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.generate"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	     	<td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     		bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     		bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    	<tr>
                    	<td width="575" height="1" colspan="9" class="gridTitle"  bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                        <bean:message key="generatePurchaseRequisition.gridTitle.PRDetails"/>
	                    </td>
	                </tr>
			    	<tr>
			       		<td width="75" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.itemName"/>
			       		</td>
			       		<td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.itemDescription"/>
			       		</td>
			       		<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.branch"/>
			       		</td>
			       		<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.location"/>
			       		</td>
			       		<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.reorderPoint"/>
			       		</td>
			       		<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.reorderQuantity"/>
			       		</td>	
			       		<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.quantityOnHand"/>
			       		</td>
			       		<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.quantity"/>
			       		</td>	
			       		<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="generatePurchaseRequisition.prompt.generate"/>
			       		</td>		       
                	</tr>			   	                    
			    	<%
			       		int i = 0;	
			       		String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       		String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       		String rowBgc = null;
		    		%>
		    		<bean:define id="actionForm" name="apGeneratePurchaseRequisitionForm" type="com.struts.ap.generatepurchaserequisition.ApGeneratePurchaseRequisitionForm"/>
			    	<nested:iterate property="apGPRList">
			    	<%
			       		i++;
			       		if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES - 1) {
			       			if((i % 2) != 0){
			           			rowBgc = ROW_BGC1;
			       			}else{
		          		 		rowBgc = ROW_BGC2;
			       			}  
			    	%>
			    	<tr bgcolor="<%= rowBgc %>">
			       		<td width="75" height="1" class="gridLabel">
			          		<nested:write property="itemName"/>
			       		</td>
   						<td width="200" height="1" class="gridLabel">
			          		<nested:write property="itemDescription"/>
			       		</td>
			       		<td width="100" height="1" class="gridLabel">
			          		<nested:write property="branch"/>
			       		</td>
			       		<td width="100" height="1" class="gridLabel">
			          		<nested:write property="location"/>
			       		</td>
			       		<td width="50" height="1" class="gridLabel">
			          		<nested:write property="reorderPoint"/>
			       		</td>
			       		<td width="50" height="1" class="gridLabel">
			          		<nested:write property="reorderQuantity"/>
			       		</td>
			       		<td width="50" height="1" class="gridLabel">
			          		<nested:write property="quantityOnHand"/>
			       		</td>
			       		<td width="10" height="1" class="gridLabel">
			          		<nested:text property="quantity" size="10" maxlength="25" styleClass="text"/>
			          		
			          		
			       		</td>
			       		<td width="50" height="1" class="gridLabel">
			          		<nested:checkbox property="generate"/>
			       		</td>			       		       
			    	</tr>
			    	<% } %>
			    	</nested:iterate>
			 	</table>
		    </div>
		  	</td>
       	</tr>
	    <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	    </tr>
 	</table>
    </td>
  </tr>
</table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["itemName"] != null &&
        document.forms[0].elements["itemName"].disabled == false)
        document.forms[0].elements["itemName"].focus()
   // -->
</script>
</body>
</html>
