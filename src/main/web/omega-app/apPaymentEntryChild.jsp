<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%> 
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="paymentEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 
 
  function calculateAmountTotal()
  {
      
      var property = "apCHKList";
      
      var amount = 0;      
      
      var i = 0;
      
      while (true) {
      
          if (document.forms[0].elements["apCHKList[" + i + "].voucherNumber"] == null) {
          
              break;
          
          }
          
          var applyAmount = 0;
	      var allocatedCheckAmount = 0;
          
          if (!isNaN(parseFloat(document.forms[0].elements["apCHKList[" + i + "].applyAmount"].value))) {
      
	      	applyAmount = (document.forms[0].elements["apCHKList[" + i + "].applyAmount"].value).replace(/,/g,'');
	      	
	      }
	      	      
	      if (!isNaN(parseFloat(document.forms[0].elements["apCHKList[" + i + "].allocatedCheckAmount"].value))) {
	      
		    allocatedCheckAmount = (document.forms[0].elements["apCHKList[" + i + "].allocatedCheckAmount"].value).replace(/,/g,'');
		    
		  }
		  
		  if (document.forms[0].elements["apCHKList[" + i + "].payCheckbox"].checked == true) {
          
	          if (document.forms[0].elements["apCHKList[" + i + "].currency"].value  ==
	              document.forms[0].elements["currency"].value) {
	              
	          	  amount = (amount * 1 + applyAmount * 1).toFixed(2);            
	                 
	          } else {
	          
	              amount = (amount * 1 + allocatedCheckAmount * 1).toFixed(2);                      
	          }
	          
	      }
          
          i++;
      
      }
      
      document.forms[0].elements["amount"].value = amount;
       
  }
  
  var tempAmount = 0;
  
  function initializeTempAmount(name)
  {
      
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }	     
            
  }
  
  function calculateAmount(name)
  {
      
      var property = name.substring(0,name.indexOf("."));   
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
      var applyAmount = 0;      
      
      if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {
  
      	applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');
      	
      }
              
	  if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {
      
          if (document.forms[0].elements[property + ".currency"].value  ==
              document.forms[0].elements["currency"].value) {
              
              if (!isNaN(1 * amount - (1 * tempAmount - 1 * applyAmount))) {
              
	          	  document.forms[0].elements["amount"].value = (1 * amount - (1 * tempAmount - 1 * applyAmount)).toFixed(2);            
	          	  tempAmount = applyAmount;
	          
	          }
                 
          } 
          
      }
                 
  }
  
  function calculateAmountAllocated(name)
  {
      
      var property = name.substring(0,name.indexOf("."));   
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
      var allocatedCheckAmount = 0;
                  
      if (!isNaN(parseFloat(document.forms[0].elements[property + ".allocatedCheckAmount"].value))) {
      
	    allocatedCheckAmount = (document.forms[0].elements[property + ".allocatedCheckAmount"].value).replace(/,/g,'');
	    
	  }
	  
	  if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {
                       
          if (document.forms[0].elements[property + ".currency"].value  !=
              document.forms[0].elements["currency"].value) {
              
              if (!isNaN(1 * amount - (1 * tempAmount - 1 * allocatedCheckAmount))) {
              
	              document.forms[0].elements["amount"].value = (1 * amount - (1 * tempAmount - 1 * allocatedCheckAmount)).toFixed(2);            
	          	  tempAmount = allocatedCheckAmount;
	          	  
	          }
          }
          
      }
                 
  }
  
  function calculateAmountCheck(name)
  {
      
      var property = name.substring(0,name.indexOf("."));   
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
      var applyAmount = 0;
      var allocatedCheckAmount = 0;
      
      if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {
  
      	applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');
      	
      }
            
      if (!isNaN(parseFloat(document.forms[0].elements[property + ".allocatedCheckAmount"].value))) {
      
	    allocatedCheckAmount = (document.forms[0].elements[property + ".allocatedCheckAmount"].value).replace(/,/g,'');
	    
	  }
	  
	  if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {
      
          if (document.forms[0].elements[property + ".currency"].value  ==
              document.forms[0].elements["currency"].value) {
              
          	  document.forms[0].elements["amount"].value = (1 * amount + 1 * applyAmount).toFixed(2);
                 
          } else {
          
              document.forms[0].elements["amount"].value = (1 * amount + 1 * allocatedCheckAmount).toFixed(2);

          }
          
          if  (document.forms[0].elements["description"].value == "") {
          
              document.forms[0].elements["description"].value = document.forms[0].elements[property + ".voucherDescription"].value;
          
          }
          
      } else {
      
          if (document.forms[0].elements[property + ".currency"].value  ==
              document.forms[0].elements["currency"].value) {
              
          	  document.forms[0].elements["amount"].value = (1 * amount - 1 * applyAmount).toFixed(2);
                 
          } else {
          
              document.forms[0].elements["amount"].value = (1 * amount - 1 * allocatedCheckAmount).toFixed(2);

          }
      
      }
      
   }
   
   function setCheckDate(name) {
   	
   		var property = name.substring(0,name.indexOf("."));

   		if(document.forms[0].elements["defaultCheckDate"].value == "DUE DATE" && 
   				document.forms[0].elements["checkDate"].value == "" &&
   				document.forms[0].elements[property + ".payCheckbox"].checked == true) {
   		
	   		document.forms[0].elements["checkDate"].value = document.forms[0].elements[property + ".dueDate"].value;
	   		
	   	}
   		
   }
   
   function setTempSupplierName() {
	
	document.forms[0].elements["tempSupplierName"].value = document.forms[0].elements["supplierName"].value;
	
   }
   
   function paymentEnterSubmit()
  {            
      if (document.activeElement.name != 'memo') {
      
          return enterSubmit(event, new Array('saveSubmitButton'));
      
      } else {
      
          return true;
      
      }
       
  }
     
//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return paymentEnterSubmit();">
<html:form action="/apPaymentEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="paymentEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apPaymentEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isConversionDateEntered" value=""/>
	     <html:hidden property="defaultCheckDate"/>
	     <html:hidden property="tempSupplierName"/>
	     <logic:equal name="apPaymentEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<logic:equal name="apPaymentEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
						<tr>
						<logic:equal name="apPaymentEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                       			<html:options property="supplierList"/>
                   				</html:select>
                  				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
         				</logic:equal>
						<logic:equal name="apPaymentEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                  				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                			</td>
         				</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
               				<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
			 				<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
         				</tr> 
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>  
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text"/>
                			</td>                
         				</tr> 
			        </table>
					</div>		        
				    <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apPaymentEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apPaymentEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkVoid"/>
               	 			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="paymentEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck"/>
                			</td>
         				</tr> 
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc1"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc1" styleClass="combo">
									<html:options property="misc1List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc2"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc2" styleClass="combo">
									<html:options property="misc2List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc3"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc3" styleClass="combo">
									<html:options property="misc3List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc4"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc4" styleClass="combo">
									<html:options property="misc4List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="150" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc5"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc5" styleClass="combo">
									<html:options property="misc5List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="160" height="26">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc6"/>
							</td>
							<td width="128" height="26" class="control">
							    <html:select property="misc6" styleClass="combo">
									<html:options property="misc6List"/>
								</html:select>
							</td>
					
         				</tr>  
			        </table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="calculateAmountTotal(); return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="paymentEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>                
					</table>
					</div>			
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton2" value="true">			                   
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton3" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton4" value="true">			                   
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         					                   
					</table>
					</div>	
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr> 
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="paymentEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>	    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>			
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         	         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>

		<tr>
	         <td width="575" height="30" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apPaymentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>	           	           	           		       
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apPaymentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>		       
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		</tr>

	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="paymentEntry.gridTitle.CHKDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.voucherNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.installmentNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.applyAmount"/>				           
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.discount"/>				           
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.taxWithheld"/>				           
				       </td>				       			       			    
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.pay"/>
				       </td>
				    </tr>							    
				    <tr>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          APV Number
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.dueDate"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.currency"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.amountDue"/>
				       </td>
				       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.allocatedCheckAmount"/>
				       </td>
				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
   					<bean:define id="actionForm" name="apPaymentEntryForm" type="com.struts.ap.paymententry.ApPaymentEntryForm"/>
				    <nested:iterate property="apCHKList">
				    <%
					   i++;
					   if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {
				       
				          if((i % 2) != 0){
				              rowBgc = ROW_BGC1;
				          }else{
				              rowBgc = ROW_BGC2;
				          }  
				    %>
                    <nested:hidden property="voucherDescription"/>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="149" height="1" class="control">
				          <nested:text property="voucherNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="installmentNumber" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="12" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempAmount(name);"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="12" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="taxWithheld" size="12" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
				       </td>			       
				       <td width="149" align="center" height="1" class="control">
				          <nested:checkbox property="payCheckbox" onclick="calculateAmountCheck(name); setCheckDate(name);"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		       			       
				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="allocatedCheckAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateAmountAllocated(name); addZeroes(name);" onkeyup="calculateAmountAllocated(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempAmount(name);"/>
				       </td>				       			       
				    </tr>
                    <% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>		       
	     </logic:equal>
	     <logic:equal name="apPaymentEntryForm" property="enableFields" value="false">
		 <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<logic:equal name="apPaymentEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
						<tr>
						<logic:equal name="apPaymentEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                  				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
         				</logic:equal>
						<logic:equal name="apPaymentEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
								<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                  				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
         				</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkDate"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
               				<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
			 				<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr> 
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>   
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text"/>
                			</td>                
         				</tr>
			        </table>
					</div>		        
				    <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="apPaymentEntryForm" property="enableCheckVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apPaymentEntryForm" property="enableCheckVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.checkVoid"/>
               	 			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="checkVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="paymentEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="paymentEntry.prompt.crossCheck"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="crossCheck" disabled="true"/>                                
	         				</tr> 
	         				
	         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc1"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc1" styleClass="combo" disabled="true" >
									<html:options property="misc1List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc2"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc2" styleClass="combo" disabled="true" >
									<html:options property="misc2List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc3"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc3" styleClass="combo" disabled="true" >
									<html:options property="misc3List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc4"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc4" styleClass="combo" disabled="true" >
									<html:options property="misc4List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="150" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc5"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc5" styleClass="combo" disabled="true">
									<html:options property="misc5List"/>
								</html:select>
							</td>
					
         				</tr>
         				
         				<tr>
         					<td class="prompt" width="160" height="26">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc6"/>
							</td>
							<td width="128" height="26" class="control">
							    <html:select property="misc6" styleClass="combo" disabled="true">
									<html:options property="misc6List"/>
								</html:select>
							</td>
					
         				</tr>   
			        </table>
					</div>			        
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="paymentEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr> 
				         
				                       
					</table>
					</div>	
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton2" value="true">			                   
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton3" value="true">			                   
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton4" value="true">			                   
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apPaymentEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         
				         					                   
					</table>
					</div>		
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr> 
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="paymentEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>	    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         	         				         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="paymentEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     
	     <tr>
	         <td width="575" height="30" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apPaymentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>	           	           	           		       
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apPaymentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apPaymentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButtonMedium" disabled="true">
	               <bean:message key="button.chkPrint"/>
	           </html:submit>
	           <html:submit property="cvPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.cvPrint"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>		       
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
		 <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="apPaymentEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		 </tr>
	     
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="paymentEntry.gridTitle.CHKDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.voucherNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.installmentNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.applyAmount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.discount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.taxWithheld"/>
				       </td>				       			       			    
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.pay"/>
				       </td>
				    </tr>							    
				    <tr>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          APV Number
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.dueDate"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.currency"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.amountDue"/>
				       </td>
				       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="paymentEntry.prompt.allocatedCheckAmount"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <bean:define id="actionForm" name="apPaymentEntryForm" type="com.struts.ap.paymententry.ApPaymentEntryForm"/>
				    <nested:iterate property="apCHKList">
				    <%
				       i++;
					   if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {	       
			
					      if((i % 2) != 0){
				              rowBgc = ROW_BGC1;
				          }else{
				              rowBgc = ROW_BGC2;
				          }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="149" height="1" class="control">
				          <nested:text property="voucherNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="installmentNumber" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="12" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="taxWithheld" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="149" align="center" height="1" class="control">
				          <nested:checkbox property="payCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		       			       
				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="allocatedCheckAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>				       			       
				    </tr>
				  <% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>		       
	     </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="apPaymentEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPaymentEntryForm" type="com.struts.ap.paymententry.ApPaymentEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
  	 <% String encodedSupplierName = java.net.URLEncoder.encode(actionForm.getTempSupplierName()); %>
  	 win = window.open("<%=request.getContextPath()%>/apRepCheckPrint.do?forward=1&checkCode=<%=actionForm.getCheckCode()%>&supplierName=<%=encodedSupplierName%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apPaymentEntryForm" property="report2" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPaymentEntryForm" type="com.struts.ap.paymententry.ApPaymentEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     <% String encodedSupplierName = java.net.URLEncoder.encode(actionForm.getTempSupplierName()); %>
  	 win = window.open("<%=request.getContextPath()%>/apRepCheckVoucherPrint.do?forward=1&checkCode=<%=actionForm.getCheckCode()%>&supplierName=<%=encodedSupplierName%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apPaymentEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPaymentEntryForm" type="com.struts.ap.paymententry.ApPaymentEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
----</body>
</html>
