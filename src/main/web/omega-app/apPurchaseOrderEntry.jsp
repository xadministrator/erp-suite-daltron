<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="purchaseOrderEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

function calculateAmount(name,precisionUnit)
{

  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitCost = 0;
  var amount = 0;

  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitCost"].value != "" &&
      document.forms[0].elements[property + ".itemName"].value != "") {

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {

	  	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

	  amount = (quantity * unitCost).toFixed(parseInt(precisionUnit.value));

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)!= 0)) {

		  fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  var totalDiscountAmount = 0.0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }
	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(parseInt(precisionUnit.value));
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(parseInt(precisionUnit.value));
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(parseInt(precisionUnit.value));
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(parseInt(precisionUnit.value));
	      }

	      if(fixedDiscountAmount != 0) {
	  	  	totalDiscountAmount	= fixedDiscountAmount;
	  	  	amount = (amount - totalDiscountAmount);
	  	  }

	      if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(parseInt(precisionUnit.value));

	  	  }

		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toFixed(parseInt(precisionUnit.value)).toString());
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

      } else {

	  	document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  	document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

	  }

  }

}

var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}
function fnOpenMisc(name){
		//alert("asdee");
	   var property = name.substring(0,name.indexOf("."));
	   currProperty = property;
	   currName = name;
	   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;

	   var miscStr = document.forms[0].elements[property + ".misc"].value;
	   var specs = new Array(); //= document.forms[0].elements["apRILList[" + 0 + "].tagList[" + 1 + "].specs"].value;
	   var custodian = new Array(document.forms[0].elements["userList"].value);
	   var custodian2 = new Array();
	   var propertyCode = new Array();
	   var serialNumber = new Array();
	   var expiryDate = new Array();

	   var tgDocNum = new Array();
	   var arrayMisc = miscStr.split("_");

		//cut miscStr and save values
		miscStr = arrayMisc[1];
		arrayMisc = miscStr.split("@");
		propertyCode = arrayMisc[0].split(",");

		miscStr = arrayMisc[1];
		arrayMisc = miscStr.split("<");
		serialNumber = arrayMisc[0].split(",");

		miscStr = arrayMisc[1];
		arrayMisc = miscStr.split(">");
		specs = arrayMisc[0].split(",");

		miscStr = arrayMisc[1];
		arrayMisc = miscStr.split(";");
		custodian2 = arrayMisc[0].split(",");
		//expiryDate = arrayMisc[1].split(",");
		miscStr = arrayMisc[1];
		arrayMisc = miscStr.split("%");
		expiryDate = arrayMisc[0].split(",");
		tgDocNum = arrayMisc[1].split(",");
	   //var userList = document.forms[0].elements["userList"].value;
	   //check at least one disabled field
   	   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
	   var index = 0;
	   //alert(document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value + "before while");
	   /*while (true) {
		  //alert(document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value);
		  if (document.forms[0].elements[property + ".tagList[" + index + "].specs"].value!= null ||
			  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value!= null ||
			  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value!= null  ||
			  document.forms[0].elements[property + ".tagList[" + index + "].propertyCode"].value!=null) {
			  //alert("d2?");
		 	  specs[index] = document.forms[0].elements[property + ".tagList[" + index + "].specs"].value;

		 	  expiryDate[index] = document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value;
		 	  serialNumber[index] = document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value;
			  propertyCode[index] = document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value;
			  custodian2[index] = document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value;
			  //alert("inside if");
		  }else{
			  //alert("o d2?");
			  /*
			  expiryDate[index] = "";
			  propertyCode[index] = "";

			  document.forms[0].elements[property + ".tagList[" + index + "].specs"].value= "";
			  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value="";
			  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value="";
			  document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value="";
			  document.forms[0].elements[property +".tagList[" + index + "].custodian"].value="";
			  //alert("inside else")
			  break;
		  }
		  //alert("after while")
		  if (++index == quantity) break;
	   }*/




	   //alert(document.forms[0].elements["apRILList[" + index + "].tagList[" + index2 + "].propertyCode"].value);
	   //var specsStr = document.forms[0].elements[property + ".specs"].value;
	   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
			   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2
			   + "&tgDocumentNumber=" + tgDocNum + "&serialNumber=" + serialNumber +"&isDisabled=" + isDisabled, "", "width=800,height=350,scrollbars=yes,status=no");

	   return false;

	}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/apPurchaseOrderEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
  	<tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
        	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   		<tr>
	     		<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="purchaseOrderEntry.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="apPurchaseOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               		<bean:message key="app.success"/>
           			</logic:equal>
		   			<html:errors/>
		   			<html:messages id="msg" message="true">
		       		<bean:write name="msg"/>
		   			</html:messages>
	        	</td>
	     	</tr>

	     	<html:hidden property="isSupplierEntered" value=""/>
	     	<html:hidden property="precisionUnit"/>
		 	<html:hidden property="isTaxCodeEntered" value=""/>
		    <html:hidden property="enableFields"/>
		    <html:hidden property="taxRate"/>
		    <html:hidden property="taxType"/>
			<html:hidden property="isConversionDateEntered" value=""/>
			<html:hidden property="isCurrencyEntered" value=""/>
			<html:hidden property="userList" />
	     	<logic:equal name="apPurchaseOrderEntryForm" property="enableFields" value="true">
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
                                    <div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
                                            <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
                                                </td>
                                            </tr>
                                            <tr>

                                                <logic:equal name="apPurchaseOrderEntryForm" property="showBatchName" value="true">
                                                <td width="130" height="25" class="prompt">
                                                    <bean:message key="purchaseOrderEntry.prompt.batchName"/>
                                                </td>

                                                 <td width="158" height="25" class="control">
                                                <html:select property="batchName" styleClass="comboRequired" style="width:130;" >
                                                        <html:options property="batchNameList"/>
                                                </html:select>
                                                </td>
                                                </logic:equal>
                                            </tr>
                                            <tr>


                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="purchaseOrderEntry.prompt.supplier"/>
                                                </td>
                                                        <logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="true">
                                                <td width="158" height="25" class="control">
                                                    <html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                                                            <html:options property="supplierList"/>
                                                    </html:select>
                                                    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                                                </td>
                                                                </logic:equal>
                                                <logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="false">
                                                <td width="158" height="25" class="control">
                                                        <html:text property="supplier" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                                                        <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                                                </td>
                                                </logic:equal>
                                                <td width="130" height="25" class="prompt">
                                                        <bean:message key="purchaseOrderEntry.prompt.date"/>
                                                </td>
                                                <td width="157" height="25" class="control">
                                                        <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                                                </td>
                                            </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.description"/>
                			</td>
                			<td width=157 height="25" class="control">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.shipmentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="shipmentNumber" size="15" maxlength="25" styleClass="text"/>
                   				<html:checkbox property="validate"/><bean:message key="purchaseOrderEntry.prompt.validate"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

						<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.deliveryPeriod"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="deliveryPeriod" size="10" maxlength="10" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="purchaseOrderEntry.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>

					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="purchaseOrderVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="purchaseOrderVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
							<td width="180" height="25" class="prompt">
								<bean:message key="purchaseOrderEntry.prompt.dateOfObligation" />
							</td>
							<td width="127" height="25" class="control">
								<html:text property="dateOfObligation" size="10" maxlength="10" styleClass="text" />
							</td>
							<td width="180" height="25" class="prompt">
								<bean:message key="purchaseOrderEntry.prompt.reasonForObligation" />
							</td>
							<td width="445" height="25" class="control" colspan="3">
								<html:textarea property="reasonForObligation" cols="20" rows="4" styleClass="text" />
							</td>
						</tr>
						
			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="purchaseOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text"/>
			    			</td>
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="purchaseOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text"/>
			    			</td>
		 				</tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="return enterSelect('taxCode','isTaxCodeEntered');">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text"  onchange="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>


					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           		<p align="right">
	           		<logic:equal name="apPurchaseOrderEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
					</logic:equal>
					<logic:equal name="apPurchaseOrderEntryForm" property="showSaveDraftButton" value="true">
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>
		       		</logic:equal>
		       		<logic:equal name="apPurchaseOrderEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
					<html:submit property="printButton" styleClass="mainButton">
	              		<bean:message key="button.print"/>
	           		</html:submit>
			   		<html:submit property="closeButton" styleClass="mainButton">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           		<p align="right">
	           		<logic:equal name="apPurchaseOrderEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
					</logic:equal>
					<logic:equal name="apPurchaseOrderEntryForm" property="showSaveDraftButton" value="true">
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>
		       		</logic:equal>
		       		<logic:equal name="apPurchaseOrderEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
					<html:submit property="printButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.print"/>
	           		</html:submit>
			   		<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>
			  	</td>
	     	</tr>
			<tr valign="top">
		    	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="purchaseOrderEntry.gridTitle.PLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.location"/>
				       	</td>
				       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unitCost"/>
                       	</td>
                       	
                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
		            	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDiscount"/>
				       	</td>
				       	<td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.amount"/>
                       	</td>
                       
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apPOList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">

				    	<td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       	</td>
				       	<td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name,precisionUnit);" onkeyup="calculateAmount(name,precisionUnit);"/>
				       	</td>
				       	<td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="100" height="1" class="control">
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);"/>
                       	</td>
                       	<td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="30" height="1" class="control"/>
				       	<td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
					    <td width="100" align="center" height="1">
					   	 <div id="buttons">
				   		 <nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc" />
					   		<nested:equal property="isTraceMisc" value = "true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall"
								onclick="return fnOpenMisc(name);">
								<bean:message key="button.miscButton" />

							</nested:submit>
							</nested:equal>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:equal property="isTraceMisc" value = "true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
								<bean:message key="button.miscButton" />
							</nested:submit>
							</nested:equal>
					   	 </div>
						</td>
				    </tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
		    </tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.addLines"/>
			      	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.deleteLines"/>
			      	</html:submit>
			  	  </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			       	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       	</html:submit>
			      </logic:equal>
		          </div>
				</td>
			</tr>
	     	</logic:equal>

	     	<logic:equal name="apPurchaseOrderEntryForm" property="enableFields" value="false">
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

                                        <tr>
                                            <logic:equal name="apPurchaseOrderEntryForm" property="showBatchName" value="true">
                                            <td width="130" height="25" class="prompt">
                                                <bean:message key="purchaseOrderEntry.prompt.batchName"/>
                                            </td>

                                             <td width="158" height="25" class="control">
                                            <html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                                                    <html:options property="batchNameList"/>
                                            </html:select>
                                            </td>
                                            </logic:equal>
                                        </tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.supplier"/>
                			</td>
							<logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.description"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.shipmentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="shipmentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="150" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.deliveryPeriod"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="deliveryPeriod" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="purchaseOrderEntry.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="purchaseOrderVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="purchaseOrderVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
							<td width="180" height="25" class="prompt">
								<bean:message key="purchaseOrderEntry.prompt.dateOfObligation" />
							</td>
							<td width="127" height="25" class="control">
								<html:text property="dateOfObligation" size="10" maxlength="10" styleClass="text" />
							</td>
							<td width="180" height="25" class="prompt">
								<bean:message key="purchaseOrderEntry.prompt.reasonForObligation" />
							</td>
							<td width="445" height="25" class="control" colspan="3">
								<html:textarea property="reasonForObligation" cols="20" rows="4" styleClass="text" />
							</td>
						</tr>

			        </table>
					</div>
					<div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="purchaseOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="purchaseOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		 				</tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>





					</table>
					</div>
					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           	<p align="right">
	           	<logic:equal name="apPurchaseOrderEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium">
		        	<bean:message key="button.saveSubmit"/>
		       	</html:submit>
				</logic:equal>
				<logic:equal name="apPurchaseOrderEntryForm" property="showSaveDraftButton" value="true">
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>
		       	</logic:equal>
		       	<logic:equal name="apPurchaseOrderEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	            	<bean:message key="button.delete"/>
	           	</html:submit>
	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton">
	            	<bean:message key="button.print"/>
	           	</html:submit>
			   	<html:submit property="closeButton" styleClass="mainButton">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
	           	<logic:equal name="apPurchaseOrderEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       	</html:submit>
				</logic:equal>
				<logic:equal name="apPurchaseOrderEntryForm" property="showSaveDraftButton" value="true">
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>
		       	</logic:equal>
		       	<logic:equal name="apPurchaseOrderEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.delete"/>
	           	</html:submit>
	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.print"/>
	           	</html:submit>
			   	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
			  	</td>
	     	</tr>
	     	<tr valign="top">
		       	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="purchaseOrderEntry.gridTitle.PLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.location"/>
				       	</td>
				       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unitCost"/>
                       	</td>
                       	
                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
		               	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDiscount"/>
				       	</td>
				       	<td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.amount"/>
                       	</td>
                       	
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apPOList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				    	<td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
				       	<td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       	</td>
				       	<td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="145" height="1" class="control" >
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                          </td>
                        
                        <td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       	</td>
                       </tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="30" height="1" class="control"/>
				       	<td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
					    <td width="100" align="center" height="1">
					   	 <div id="buttons">
				   		 	<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc" />
					   		<nested:equal property="isTraceMisc" value = "true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
								<bean:message key="button.miscButton" />
							</nested:submit>
							</nested:equal>

					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:equal property="isTraceMisc" value = "true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
								<bean:message key="button.miscButton" />
							</nested:submit>
							</nested:equal>
					   	 </div>
						</td>
				  	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
				</td>
	     	</tr>
	     	</logic:equal>

	     	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     	</td>
	       	</tr>

        	</table>
    	</td>
  	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)
        document.forms[0].elements["date"].focus();
	       // -->
</script>
<logic:equal name="apPurchaseOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepPurchaseOrderPrint.do?forward=1&purchaseOrderCode=<%=actionForm.getPurchaseOrderCode()%>&viewType=<%=actionForm.getViewType()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apPurchaseOrderEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apPurchaseOrderEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>


<logic:equal name="apPurchaseOrderEntryForm" property="attachmentDownload" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnDownload.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
</body>
</html>
