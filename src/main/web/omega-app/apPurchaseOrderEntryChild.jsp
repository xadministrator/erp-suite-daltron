<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="purchaseOrderEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}

function showAdDocument()
{

   var prCode = document.forms[0].elements["referenceNumber"].value;
   if(prCode != null){
   window.open("apPurchaseRequisitionEntry.do?forward=1&child=1&purchaseRequisitionCode=" + prCode,"apPurchaseRequisitionEntry.do","width=605,height=500,scrollbars,status");
   }

   return false;
}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/apPurchaseOrderEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
  	<tr valign="top">
        <td width="581" height="510">
        	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   		<tr>
	     		<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="purchaseOrderEntry.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="apPurchaseOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               		<bean:message key="app.success"/>
           			</logic:equal>
		   			<html:errors/>
		   			<html:messages id="msg" message="true">
		       		<bean:write name="msg"/>
		   			</html:messages>
	        	</td>
	     	</tr>

	     	<html:hidden property="isSupplierEntered" value=""/>
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.supplier"/>
                			</td>
							<logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="apPurchaseOrderEntryForm" property="useSupplierPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.description"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="description" size="50" maxlength="150" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.shipmentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="shipmentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="purchaseOrderVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apPurchaseOrderEntryForm" property="enablePurchaseOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseOrderEntry.prompt.purchaseOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="purchaseOrderVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>

						
			        </table>
					</div>
					<div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="purchaseOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="purchaseOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		 				</tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="purchaseOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseOrderEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseOrderEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
					<p align="right">
					<html:submit property="reviewPRButton" styleClass="mainButton" onclick="showAdDocument();">
						<bean:message key="button.reviewPRButton"/>
					</html:submit>
					<html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
						<bean:message key="button.close"/>
					</html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
					<p align="right">
					<html:submit property="reviewPRButton" styleClass="mainButton" disabled="true">
						<bean:message key="button.reviewPRButton"/>
					</html:submit>
					<html:submit property="closeButton" styleClass="mainButton" disabled="true">
						<bean:message key="button.close"/>
					</html:submit>
	           </div>
			   </td>
	     	</tr>
	     	<tr valign="top">
		       	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="purchaseOrderEntry.gridTitle.PLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.location"/>
				       	</td>
				       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.unitCost"/>
                       	</td>
                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
		               	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseOrderEntry.prompt.itemDiscount"/>
				       	</td>
				       	<td width="195" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseOrderEntry.prompt.amount"/>
                       	</td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apPOList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				    	<td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
				       	<td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       	</td>
				       	<td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="145" height="1" class="control">
                          <nested:text property="unitCost" size="17" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                       	</td>
                       	<td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="30" height="1" class="control"/>
				       	<td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="145" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
					    <td width="50" align="center" height="1">
					   	 <div id="buttons">
				   		 <nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	 </div>
						</td>
				  	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
		   	<tr>
				<td width="575" height="25" colspan="4">
				<div id="buttons">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.addLines"/>
			      	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.deleteLines"/>
			      	</html:submit>
			  	  </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="apPurchaseOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			       	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       	</html:submit>
			      </logic:equal>
				</div>
				</td>
	     	</tr>
	     	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     	</td>
	       	</tr>

        	</table>
    	</td>
  	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)
        document.forms[0].elements["date"].focus();
	       // -->
</script>
<logic:equal name="apPurchaseOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepPurchaseOrderPrint.do?forward=1&purchaseOrderCode=<%=actionForm.getPurchaseOrderCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apPurchaseOrderEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apPurchaseOrderEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>


<logic:equal name="apPurchaseOrderEntryForm" property="attachmentDownload" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseOrderEntryForm" type="com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnDownload.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
</body>
</html>
