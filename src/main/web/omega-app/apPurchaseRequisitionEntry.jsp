<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="purchaseRequisitionEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}
var currProperty;
var currName;

function fnOpenMisc(name){
   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array(); //= document.forms[0].elements["apRILList[" + 0 + "].tagList[" + 1 + "].specs"].value;
   var custodian = new Array(document.forms[0].elements["userList"].value);
   var custodian2 = new Array();
   var propertyCode = new Array();
   var serialNumber = new Array();
   var expiryDate = new Array();

   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");

   //var userList = document.forms[0].elements["userList"].value;

   var index = 0;
 //check at least one disabled field
   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
  // alert("before while")
/*   while (true) {
	 // alert(document.forms[0].elements[property + ".tagList[" + index + "].specs"].value);
	  if (document.forms[0].elements[property + ".tagList[" + index + "].specs"].value!= null) {
		  //alert("d2?");
	 	  specs[index] = document.forms[0].elements[property + ".tagList[" + index + "].specs"].value;
	 	 //alert("inside if")
	 	  expiryDate[index] = document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value;
		  propertyCode[index] = document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value;
		  custodian2[index] = document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value;
	  }else{
		  /*alert("o d2?");

		  expiryDate[index] = "";
		  propertyCode[index] = "";

		  document.forms[0].elements[property + ".tagList[" + index + "].specs"].value= "";
		  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].custodian"].value="";
		  //alert("inside else")
		  //break;
	  }
	  //alert("after while")
	  if (++index == quantity) break;
   }*/
		//alert(document.forms[0].elements["apRILList[" + index + "].tagList[" + index2 + "].propertyCode"].value);
	   //var specsStr = document.forms[0].elements[property + ".specs"].value;
	   //alert('hey');
	   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
			   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&tgDocumentNumber=" + tgDocNum +
			   "&serialNumber=" + serialNumber +"&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

	   return false;
}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/apPurchaseRequisitionEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
  	<tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
        	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   		<tr>
	     		<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="purchaseRequisitionEntry.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="apPurchaseRequisitionEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               			<bean:message key="app.success"/>
           			</logic:equal>
		   			<html:errors/>
		   			<html:messages id="msg" message="true">
		       			<bean:write name="msg"/>
		   			</html:messages>
	        	</td>
	     	</tr>
	     	<html:hidden property="userList" />
	     	<logic:equal name="apPurchaseRequisitionEntryForm" property="enableFields" value="true">
	     	<html:hidden property="enablePurchaseRequisitionVoid"/>
			<html:hidden property="isConversionDateEntered" value=""/>
			<html:hidden property="isCurrencyEntered" value=""/>
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="50">


						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.documentNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td class="prompt" width="575" height="25" colspan="2">
			                </td>

							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>


         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>

                			<td class="prompt" width="575" height="25" colspan="2">
			                </td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseRequisitionEntry.prompt.prType"/>
                			</td>


                			<td width="445" height="25" class="control">
			                   <html:select property="prType" styleClass="comboRequired" style="width:100;">
		                       <html:options property="prTypeList"/>
		                       </html:select>
			                </td>


						</tr>


						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.tagStatus"/>
                			</td>
                			<td width="445" height="25" class="control">
			                   <html:select property="tag" styleClass="comboRequired" style="width:100;">
		                       <html:options property="tagTypeList"/>
		                       </html:select>
			                </td>

         				</tr>
         				<tr>

                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseRequisitionEntry.prompt.deliveryPeriod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                				<html:text property="deliveryPeriod" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.totalBasedPo"/>
                			</td>
                			<td width="445" height="25" class="control">
			                   <html:checkbox property="totalBasedPo"/>
			                </td>

         				</tr>


					</table>
					</div>
					<div class="tabbertab" title="Misc">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4"/>
			             </tr>
			             <tr>
			             	<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.purchaseRequisitionVoid"/>
			                </td>
			                <logic:equal name="apPurchaseRequisitionEntryForm" property="enablePurchaseRequisitionVoid" value="true">
			                <td width="157" height="25" class="control">
			                   <html:checkbox property="purchaseRequisitionVoid"/>
			                </td>
			                </logic:equal>
			                <logic:equal name="apPurchaseRequisitionEntryForm" property="enablePurchaseRequisitionVoid" value="false">
			                <td width="157" height="25" class="control">
			                   <html:checkbox property="purchaseRequisitionVoid" disabled="true"/>
			                </td>
			                </logic:equal>


				         </tr>
				         	

					</table>
					</div>
					<div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4"/>
			             </tr>
			             <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.taxCode"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:select property="taxCode" styleClass="comboRequired" style="width:100;">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.currency"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" style="width:60;" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.conversionDate"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text"  onchange="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="purchaseRequisitionEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.posted"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="1" styleClass="text" disabled="true"/>
			                </td>
				        </tr>

				         <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassApprovalStatus"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="canvassApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassPosted"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="canvassPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassReasonForRejection"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:textarea property="canvassReasonForRejection" cols="44" rows="1" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.createdBy"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateCreated"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>




				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassApprovedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="canvassApprovedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassDateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="canvassDateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>

				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassPostedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="canvassPostedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassDatePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="canvassDatePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>


				        <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

			            <tr>
		                	<td width="575" height="1" colspan="4" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                    	<bean:message key="purchaseRequisitionEntry.gridTitle.PRAprDetails"/>
			                </td>
		            	</tr>

			            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprDate"/>
				       	</td>
		               	<td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprName"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprStatus"/>
				       	</td>


                       	</tr>

				        <nested:iterate property="apAPRList">
						 <tr>

						 		<td class="prompt" width="50" height="25">
							  		<nested:write property="approvedDate"/>
							  </td>

						 	  <td class="prompt" width="50" height="25">
							  		<nested:write property="approverName"/>
							  </td>

							  <td class="prompt" width="200" height="25" colspan="1">
							  		<nested:write property="status"/>
							  </td>


						 </tr>
						 </nested:iterate>


					</table>
					</div>



					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="Schedule">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="415" height="25" class="control">
			                   <html:select property="reminderSchedule" styleClass="comboRequired">
			                       <html:options property="reminderScheduleList"/>
			                   </html:select>
			                </td>

			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.notifiedUser1"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser1" styleClass="comboRequired">
			                       <html:options property="notifiedUser1List"/>
			                   </html:select>
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text"/>
			                </td>
				         </tr>
					 </table>
					 </div>




					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           		<p align="right">
	           		<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>

		       		</logic:equal>



		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveCanvassButton" value="true">
		       		<html:submit property="saveCanvassButton" styleClass="mainButton" style="width:100;" onclick="return confirmSaveCanvass();">
	              		<bean:message key="button.saveCanvass"/>
	           		</html:submit>

	           		</logic:equal>

		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showGeneratePoButton" value="true">


		       		<html:submit property="generatePoButton" styleClass="mainButton" style="width:100;" onclick="return confirmGeneratePO();">
	              		<bean:message key="button.generatePo"/>
	           		</html:submit>
	           		</logic:equal>



		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
	           		<html:submit property="printButton" styleClass="mainButton">
	              		<bean:message key="button.print"/>
	           		</html:submit>
	           		<html:submit property="printQuotationButton" styleClass="mainButtonBig">
	              		<bean:message key="button.printQuotation"/>
	           		</html:submit>
	           		<html:submit property="crPrintButton" styleClass="mainButtonMedium">
	              		<bean:message key="button.crPrint"/>
	           		</html:submit>
					<html:submit property="closeButton" styleClass="mainButton">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           		<p align="right">
	           		<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>


		       		</logic:equal>


		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveCanvassButton" value="true">
		       		<html:submit property="saveCanvassButton" styleClass="mainButton" disabled="true" style="width:100;" onclick="return confirmSaveCanvass();">
	              		<bean:message key="button.saveCanvass"/>
	           		</html:submit>


	           		</logic:equal>


		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showGeneratePoButton" value="true">


		       		<html:submit property="generatePoButton" styleClass="mainButton" disabled="true" style="width:100;">
	              		<bean:message key="button.generatePo"/>
	           		</html:submit>
	           		</logic:equal>




		       		<logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
					<html:submit property="printButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.print"/>
	           		</html:submit>
	           		<html:submit property="printQuotationButton" styleClass="mainButtonBig" disabled="true">
	              		<bean:message key="button.printQuotation"/>
	           		</html:submit>
	           		<html:submit property="crPrintButton" styleClass="mainButtonMedium" disabled="true">
	              		<bean:message key="button.crPrint"/>
	           		</html:submit>
			   		<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>
			  	</td>
	     	</tr>
			<tr valign="top">
		    	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="purchaseRequisitionEntry.gridTitle.PRLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemName"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.location"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.quantity"/>
                       	</td>
                       	<td width="230" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.unit"/>
                       	</td>
                       	<td width="70" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.budgetQuantity"/>
                       	</td>
                       	<td width="50" height="1" class="gridHeader"bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
				    	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
		            	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="370" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="280" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.amount"/>
                       	</td>
                       	<td width="50" height="1" class="gridHeader"bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="purchaseRequisitionEntry.prompt.misc"/>
                       	</td>
                       	</logic:equal>
                       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
		            	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="370" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemDescription"/>
				       	</td>
                       	</logic:equal>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apPRList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isLocationEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
<%-- 				    	<nested:iterate property="tagList"> --%>
<%-- 							<nested:hidden property="propertyCode" /> --%>
<%-- 							<nested:hidden property="specs" /> --%>
<%-- 							<nested:hidden property="custodian" /> --%>
<%-- 							<nested:hidden property="expiryDate" /> --%>
<%-- 							<nested:hidden property="serialNumber" /> --%>
<%-- 						</nested:iterate>	 --%>
				    	<td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="180" height="1" class="control">
				          <nested:text property="itemName" size="20" maxlength="25" styleClass="textRequired" readonly="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" onchange="return enterSelectGrid(name, 'location','isLocationEntered');">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
				       	<td width="70" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="text" disabled="false"/>
				       	</td>
				       	<td width="115" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="70" height="1" class="control">
 				          <nested:text property="budgetQuantity" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       	</td>
                       	<td width="115" align="center" height="1">
			             <div id="buttons">
			                <nested:submit property="supplierButton" styleClass="gridButton" style="width:50;">
	                           <bean:message key="button.supplier"/>
  	                        </nested:submit>
  	                     </div>
  	                     <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="supplierButton" styleClass="gridButton" disabled="true" style="width:50;">
	                           <bean:message key="button.supplier"/>
  	                        </nested:submit>
  	                     </div>
			          	</td>

			          	</logic:equal>
			          	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
				       	<td width="70" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="text"/>
				       	</td>
				       	<td width="115" height="1" class="control" colspan="2">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	</logic:equal>
			          	<td width="50" height="1" class="control">
				          	<p align="center">
                          	<nested:checkbox property="deleteCheckbox"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				   		<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
				       	<td width="50" height="1" class="control"/>
				       	<td width="370" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="280" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	</logic:equal>
				   		<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
				       	<td width="50" height="1" class="control"/>
				       	<td width="370" height="1" class="control" colspan="6">
				          <nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	</logic:equal>
				       	<td>
				       	<div id="buttons"><nested:hidden property="misc" />
					       	<nested:equal property="isTraceMisc" value = "true">
					       	<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
								<bean:message key="button.miscButton" />
							</nested:submit>
							</nested:equal>
						</div>
				       	</td>

				    </tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
		    </tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="apPurchaseRequisitionEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.addLines"/>
			      	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.deleteLines"/>
			      	</html:submit>
			  	  </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="apPurchaseRequisitionEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			       	</html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       	</html:submit>
			      </logic:equal>
		          </div>
				</td>
			</tr>
	     	</logic:equal>

	     	<logic:equal name="apPurchaseRequisitionEntryForm" property="enableFields" value="false">
	     	<html:hidden property="enablePurchaseRequisitionVoid"/>
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.documentNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.tagStatus"/>
                			</td>
                			<td width="445" height="25" class="control">
			                   <html:select property="tag" styleClass="comboRequired" disabled="true" style="width:100;">
		                       <html:options property="tagTypeList"/>
		                       </html:select>
			                </td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="purchaseRequisitionEntry.prompt.deliveryPeriod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                				<html:text property="deliveryPeriod" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.totalBasedPo"/>
                			</td>
                			<td width="445" height="25" class="control">
			                   <html:checkbox property="totalBasedPo" disabled="true"/>
			                </td>

         				</tr>
			        </table>
					</div>
					<div class="tabbertab" title="Misc">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4"/>
			             </tr>
			             <tr>
			             	<td width="130" height="25" class="prompt">
                   				<bean:message key="purchaseRequisitionEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.purchaseRequisitionVoid"/>
			                </td>
			                <logic:equal name="apPurchaseRequisitionEntryForm" property="enablePurchaseRequisitionVoid" value="true">
			                <td width="157" height="25" class="control">
			                   <html:checkbox property="purchaseRequisitionVoid"/>
			                </td>
			                </logic:equal>
			                <logic:equal name="apPurchaseRequisitionEntryForm" property="enablePurchaseRequisitionVoid" value="false">
			                <td width="157" height="25" class="control">
			                   <html:checkbox property="purchaseRequisitionVoid" disabled="true"/>
			                </td>
			                </logic:equal>


				         </tr>


				         	

					</table>
					</div>
					<div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4"/>
			             </tr>
			             <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.taxCode"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true" style="width:100;">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.currency"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" style="width:60;" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.conversionDate"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="purchaseRequisitionEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.posted"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="1" styleClass="text" disabled="true"/>
			                </td>
				        </tr>

				        <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassApprovalStatus"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="canvassApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassPosted"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="canvassPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>

				         <tr>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassReasonForRejection"/>
			                </td>
			                <td width="445" height="25" class="control" colspan="3">
			                   <html:textarea property="canvassReasonForRejection" cols="44" rows="1" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.createdBy"/>
			                </td>
			                <td width="158" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="130" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateCreated"/>
			                </td>
			                <td width="157" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>



				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassApprovedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="canvassApprovedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassDateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="canvassDateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>

				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassPostedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="canvassPostedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.canvassDatePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="canvassDatePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>


				        <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

			            <tr>
		                	<td width="575" height="1" colspan="4" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                    	<bean:message key="purchaseRequisitionEntry.gridTitle.PRAprDetails"/>
			                </td>
		            	</tr>

			            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprDate"/>
				       	</td>
		               	<td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprName"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.aprStatus"/>
				       	</td>

                       	</tr>



			            <%
					       int x = 0;
					       String ROW_ABGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_ABGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowABgc = null;
					    %>
					    <nested:iterate property="apAPRList">
					    <%
					       x++;
					       if((x % 2) != 0){
					           rowABgc = ROW_ABGC1;
					       }else{
					           rowABgc = ROW_ABGC2;
					       }
					    %>
					    <tr bgcolor="<%= rowABgc %>">



						 		<td class="prompt" width="50" height="25">
							  		<nested:write property="approvedDate"/>
							  </td>

						 	  <td class="prompt" width="50" height="25">
							  		<nested:write property="approverName"/>
							  </td>

							  <td class="prompt" width="200" height="25" colspan="2">
							  		<nested:write property="status"/>
							  </td>


						 </tr>
						 </nested:iterate>


					</table>
					</div>



					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="purchaseRequisitionEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton" >
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="apPurchaseRequisitionEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>


					<div class="tabbertab" title="Schedule">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="reminderSchedule" size="25" maxlength="25" styleClass="textRequired" disabled="true"/>
			                </td>

			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.notifiedUser1"/>
			                </td>

			                <td width="128" height="25" class="control">
			                   <html:text property="notifiedUser1" size="25" maxlength="25" styleClass="textRequired" disabled="true"/>
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="purchaseRequisitionEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>

					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           	<p align="right">
	           	<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium">
		        	<bean:message key="button.voidPR"/>
		       	</html:submit>
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>



		       	</logic:equal>



		       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveCanvassButton" value="true">
		       	<html:submit property="saveCanvassButton" styleClass="mainButton" style="width:100;" onclick="return confirmSaveCanvass();">
	              		<bean:message key="button.saveCanvass"/>
	           	</html:submit>

	           	</logic:equal>



		       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showGeneratePoButton" value="true">

		       	<html:submit property="generatePoButton" styleClass="mainButton" style="width:100;" onclick="return confirmGeneratePO();">
	            	<bean:message key="button.generatePo"/>
	           	</html:submit>
	           	</logic:equal>

		       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	            	<bean:message key="button.delete"/>
	           	</html:submit>
	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton">
	            	<bean:message key="button.print"/>
	           	</html:submit>
	           	<html:submit property="printQuotationButton" styleClass="mainButtonBig">
	              		<bean:message key="button.printQuotation"/>
	           		</html:submit>
	           	<html:submit property="crPrintButton" styleClass="mainButtonMedium">
	            	<bean:message key="button.crPrint"/>
	           	</html:submit>
			   	<html:submit property="closeButton" styleClass="mainButton">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
	           	<logic:equal name="apPurchaseRequisitionEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       	</html:submit>
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>



		       	</logic:equal>


		       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showGeneratePoButton" value="true">

	           	<html:submit property="saveCanvassButton" styleClass="mainButton" style="width:100;" onclick="return confirmSaveCanvass();">
	              		<bean:message key="button.saveCanvass"/>
           		</html:submit>


		       	<html:submit property="generatePoButton" styleClass="mainButton" disabled="true" style="width:100;">
	            	<bean:message key="button.generatePo"/>
	           	</html:submit>
	           	</logic:equal>

		       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.delete"/>
	           	</html:submit>


	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.print"/>
	           	</html:submit>
	           	<html:submit property="printQuotationButton" styleClass="mainButtonBig">
	              		<bean:message key="button.printQuotation"/>
	           		</html:submit>
	           	<html:submit property="crPrintButton" styleClass="mainButtonMedium" disabled="true">
	            	<bean:message key="button.crPrint"/>
	           	</html:submit>
			   	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
			  	</td>
	     	</tr>
	     	<tr valign="top">
		       	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="purchaseRequisitionEntry.gridTitle.PRLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemName"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.location"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.quantity"/>
                       	</td>

                       	<td width="230" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.unit"/>
                       	</td>
                       	<td width="70" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.budgetQuantity"/>
                       	</td>
                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
				    	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="370" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="280" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="purchaseRequisitionEntry.prompt.amount"/>
                       	</td>
                       	</logic:equal>
				    	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="370" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="purchaseRequisitionEntry.prompt.itemDescription"/>
				       	</td>
                       	</logic:equal>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apPRList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
<%-- 				    	<nested:iterate property="tagList"> --%>
<%-- 							<nested:hidden property="propertyCode" /> --%>
<%-- 							<nested:hidden property="specs" /> --%>
<%-- 							<nested:hidden property="custodian" /> --%>
<%-- 							<nested:hidden property="expiryDate" /> --%>
<%-- 							<nested:hidden property="serialNumber" /> --%>
<%-- 						</nested:iterate> --%>
				    	<td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="180" height="1" class="control">
				          <nested:text property="itemName" size="20" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
				       	<td width="70" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="115" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="70" height="1" class="control">
				          <nested:text property="budgetQuantity" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       	</td>
                       	<td width="115" align="center" height="1">
			             <div id="buttons">
			                <nested:submit property="supplierButton" styleClass="gridButton" style="width:50;">
	                           <bean:message key="button.supplier"/>
  	                        </nested:submit>
  	                     </div>
  	                     <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="supplierButton" styleClass="gridButton" disabled="true" style="width:50;">
	                           <bean:message key="button.supplier"/>
  	                        </nested:submit>
  	                     </div>
			          	</td>

			          	</logic:equal>
				       	<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
				       	<td width="70" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="115" height="1" class="control" colspan="2">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	</logic:equal>
			          	<td width="50" height="1" class="control">
				          	<p align="center">
                          	<nested:checkbox property="deleteCheckbox" disabled="true"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				   		<logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="true">
				       	<td width="50" height="1" class="control"/>
				       	<td width="370" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="280" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	</logic:equal>
                        <logic:equal name="apPurchaseRequisitionEntryForm" property="showPrCost" value="false">
				       	<td width="50" height="1" class="control"/>
				       	<td width="370" height="1" class="control" colspan="6">
				          <nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	</logic:equal>
				       	<td>
				       	<div id="buttons"><nested:hidden property="misc" />
					       	<nested:equal property="isTraceMisc" value = "true">
					       	<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
								<bean:message key="button.miscButton" />
							</nested:submit>
							</nested:equal>
						</div>
				       	</td>
				  	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="apPurchaseRequisitionEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="apPurchaseRequisitionEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="apPurchaseRequisitionEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
				</td>
	     	</tr>
	     	</logic:equal>

	     	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     	</td>
	       	</tr>

        	</table>
    	</td>
  	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)
        document.forms[0].elements["date"].focus();
	       // -->
</script>
<logic:equal name="apPurchaseRequisitionEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepPurchaseRequisitionPrint.do?forward=1&purchaseRequisitionCode=<%=actionForm.getPurchaseRequisitionCode()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apPurchaseRequisitionEntryForm" property="report2" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepCanvassReportPrint.do?forward=1&purchaseRequisitionCode=<%=actionForm.getPurchaseRequisitionCode()%>&totalBasedPo=<%=actionForm.getTotalBasedPo()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>

<logic:equal name="apPurchaseRequisitionEntryForm" property="reportQuotation" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepPurchaseRequisitionPrint.do?forward=1&quotation=y&purchaseRequisitionCode=<%=actionForm.getPurchaseRequisitionCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>

<logic:equal name="apPurchaseRequisitionEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apPurchaseRequisitionEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>

<logic:equal name="apPurchaseRequisitionEntryForm" property="attachmentDownload" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apPurchaseRequisitionEntryForm" type="com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnDownload.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>

</body>
</html>
