<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="apReceivingItemEntry2"/>
</logic:notPresent>
	<jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>

<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="receivingItemEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1" type="text/css">
<script src="js/global.js">
</script>
<script language="javascript" type="text/javascript">
	
	var snCtr=1;
	var pnCtr=0;
	var bCodes=new Array(); /*use 1-indexing*/
	var poNumber;
	var index=0;

	var blockEnter=false;
	
	/*prevent data collector to autosubmit by disabling ENTER key*/
	document.onkeypress=disableEnterKey;
	function disableEnterKey(e)
	{
	     var key;      
	     if(window.event) key = window.event.keyCode;
	     else key = e.which;    
	     return (key != 13);
	}
	
	function validatePartNumber(pNumber) {
		return true;
		/*if (pNumber.length>=10) return true;
		return false;*/
	}
	
	function setTextField(elem) {
		
		elem.type="text";
		elem.style.background = "#ffffe1";
		elem.style.borderColor = "#000000";
		elem.style.borderStyle = "solid";
		elem.style.borderWidth = "1px";
		elem.style.fontSize = "12pt";
		elem.size="18"; 
		elem.maxlength="18";
		return elem;
		
	}
	
	function setLabel(elem,value) {
		
		elem.innerHTML=value+" ";
		elem.style.fontSize = "9pt";
		elem.style.fontWeight = "bold";
		return elem;
		
	}
	
	function createSerialField() {
		var tbody=document.getElementById("tbody");
		var tr=document.createElement("tr");
		var td=document.createElement("td");
		var label=setLabel(document.createElement("label"),"SN "+snCtr);
		var text=setTextField(document.createElement("input"));
		text.id="snId"+(snCtr++);
		td.appendChild(label);
		td.appendChild(text);
		tr.appendChild(td);
		tr.style.textAlign="center";
		tbody.appendChild(tr);
		text.focus();
	}

	function overWriteCookie(inputStr)
	{

		document.cookie = inputStr;

	}
	
	function appendCookie(inputStr)
	{

		var temp = document.cookie;
		var arr = temp.split(";");
		var str = arr[0] + "|" +inputStr;
		document.cookie = str;
		
	}
	
	function hasDuplicates(scannedStr) {
		var i;
		for (i=1;i<snCtr-1;i++) {
			if (bCodes[i]==scannedStr) return true;
		}
		return false;
	}
	
	function setQuantity2() {
		appendCookie(document.getElementById("pnId").value);
		appendCookie(document.getElementById("qtyId").value);
		deleteNodes();
		document.getElementById("pnId").value="";
		document.getElementById("pnId").focus();
		pnCtr++;
	}
	
	function createQuantityField2() {
		var tbody=document.getElementById("tbody");
		var tr=document.createElement("tr");
		var td=document.createElement("td");
		var button=document.createElement("input");
			button.type = "button";
			button.value = "OK";
			button.style.background = "lightblue";
			button.onclick=setQuantity2;
		var label=setLabel(document.createElement("label"),"Quantity");
		var text=setTextField(document.createElement("input"));
			text.id="qtyId";
			text.size="10"; 
			text.maxlength="10";
		
		td.appendChild(label);
		td.appendChild(text);
		td.appendChild(button);
		tr.appendChild(td);
		tr.style.textAlign="center";
		tbody.appendChild(tr);
		text.focus();
	}
	
	function createSerialField2() {
		var scannedStr=event.srcElement.value;
		/*if input is valid part number or serial number*/
		if (validatePartNumber(scannedStr)) {
			/*input is a non-serialized item*/
			if (/*scannedStr.length==10&&scannedStr.charAt(2)=='0'*/true) {
				document.getElementById("pnId").value=scannedStr;
				for (i=0; i<2; i++) deleteNodes();
				createQuantityField2();
				snCtr=1;
			}
			/*input is a serialized item*/
			else if (scannedStr.length==16&&scannedStr.charAt(2)=='1') {
				/*if scanned serial number belongs to the part number*/
				if (scannedStr.substr(0,10)==document.getElementById("pnId").value) {
					/*check if has duplicates*/
					if (!hasDuplicates(scannedStr)) {
						if (snCtr>2) {
							var child = document.getElementById("snId"+(snCtr-2)).parentNode.parentNode;
							var parent = child.parentNode;
							parent.removeChild(child);
						}
						bCodes[snCtr-1]=scannedStr;
						appendCookie(scannedStr);
						var tbody=document.getElementById("tbody");
						var tr=document.createElement("tr");
						var td=document.createElement("td");
						var label=setLabel(document.createElement("label"),"SN "+snCtr);
						var text=setTextField(document.createElement("input"));
						text.onkeypress=createSerialField2;
						text.id="snId"+(snCtr++);
						td.appendChild(label);
						td.appendChild(text);
						tr.appendChild(td);
						tr.style.textAlign="center";
						tbody.appendChild(tr);
						text.focus();
					}
					else {
						document.getElementById("snId"+(snCtr-1)).value="";
						alert("ERROR: ITEM was already SCANNED.");
					}
				}
				else {
					
					/*reset form*/
					var i=0;
					var tempValue = document.getElementById("snId"+(snCtr-1)).value;
					for (i=0; i<2; i++) deleteNodes();
					document.getElementById("pnId").value=tempValue.substr(0,10);
					snCtr=1;
					blockEnter=true;
					createSerialNumber();
				}
			}
			else {
				event.srcElement.value="";
				alert("ERROR: Invalid Input...");
			}
		}
		else {
			event.srcElement.value="";
			alert("ERROR: Invalid Input...");
		}
	}
	
	function deleteNodes() {
		var tbody=document.getElementById("tbody");
		var child = tbody.lastChild;
		tbody.removeChild(child);
	}
	
	function setQuantity() {
		appendCookie(document.getElementById("pnId").value);
		appendCookie(document.getElementById("qtyId").value);
		deleteNodes();
		document.getElementById("pnId").value="";
		document.getElementById("pnId").focus();
		pnCtr++;
	}
	
	function createQtyField() {
		var tbody=document.getElementById("tbody");
		var tr=document.createElement("tr");
		var td=document.createElement("td");
		var button=document.createElement("input");
			button.type = "button";
			button.value = "OK";
			button.style.background = "lightblue";
			button.onclick=setQuantity;
		var label=setLabel(document.createElement("label"),"Quantity");
		var text=setTextField(document.createElement("input"));
			text.id="qtyId";
			text.size="10"; 
			text.maxlength="10";
		
		td.appendChild(label);
		td.appendChild(text);
		td.appendChild(button);
		tr.appendChild(td);
		tr.style.textAlign="center";
		tbody.appendChild(tr);
		text.focus();
	}
	
	function createSerialNumber() {
		poNumber=document.forms[0].elements["poNumber"].value;
		if (poNumber!="" && poNumber!="NO RECORD FOUND") {
    
	//alert(event.srcElement.value);
			var scannedStr=event.srcElement.value;
      
			/*if input is valid part number or serial number*/
			if (validatePartNumber(scannedStr)) {

	  //alert(scannedStr);
				/*input is a non-serialized item*/
				if (/*scannedStr.length==10&&scannedStr.charAt(2)=='0'*/true) {
  
					if (pnCtr==0) overWriteCookie(poNumber);
     
					createQtyField();
				}
				/*input is a serialized item*/
				else if (scannedStr.length==16&&scannedStr.charAt(2)=='1') {
    
					if (pnCtr==0) overWriteCookie(poNumber);

					createSerialField();
					/*first item was scanned*/
					if (snCtr==2) {
      
						bCodes[1]=scannedStr;
						appendCookie(scannedStr)
						document.getElementById("pnId").value=scannedStr.substr(0,10);
						document.getElementById("snId"+(snCtr-1)).value=scannedStr;
						createSerialField();
						document.getElementById("snId"+(snCtr-1)).onkeypress=createSerialField2;
						pnCtr++;
					}
				}
				else {
    
					document.getElementById("pnId").value="";
					alert("ERROR: Invalid Input...");
				}
			}
			else {
 
				document.getElementById("pnId").value="";
				alert("ERROR: Invalid Input...");
			}
		}
		else {

			document.getElementById("pnId").value="";
			alert("ERROR: Please Select PO Number.");
		}
	}
	
	function readCookieForSaving() {
		var cookieStr = document.cookie.split(";");
		var cookieArray = cookieStr[0].split("|");

		/*set properties*/
		document.forms[0].elements["type"].value ="PO MATCHED";
		cookieArray.sort();
		var i=0;
		while (i<cookieArray.length-1) {
			/*serialized*/
			if (cookieArray[i].charAt(2)=='1') {
				var baseStr = cookieArray[i].substr(0,10);
				var j=i, received=0;
				var misc="", propertyCode="", serialNumber="", specs="", custodian="", expiryDate="",  tgDocumentNumber="";
				while (j<cookieArray.length-1) {
					if (cookieArray[j].charAt(2)=='1') {
						if (cookieArray[j].substr(0,10)==baseStr) {

							propertyCode += " ,";
							serialNumber += cookieArray[j]+",";
							specs += " ,";
							custodian += " ,";
							expiryDate += " ,";
							tgDocumentNumber += " ,";
							j++;
							i++;
							received++;
						}
						else break;
					}
					else break;
				}

				misc = 	received+"_"+
						propertyCode.substr(0,propertyCode.length-1)+"@"+
				   		serialNumber.substr(0,serialNumber.length-1)+"<"+
				   		specs.substr(0,specs.length-1)+">"+
				   		custodian.substr(0,custodian.length-1)+";"+
				   		expiryDate.substr(0,expiryDate.length-1)+"%"+
				   		tgDocumentNumber.substr(0,tgDocumentNumber.length-1);
				
				document.forms[0].elements["apRILList[" + index + "].partNumber"].value = baseStr;
				document.forms[0].elements["apRILList[" + index + "].misc"].value = misc;
				document.forms[0].elements["apRILList[" + index + "].received"].value = received;
				index++;
			}
			/*non-serialized*/
			else {

				document.forms[0].elements["apRILList[" + index + "].partNumber"].value = cookieArray[i];
				document.forms[0].elements["apRILList[" + index + "].misc"].value = "";
				document.forms[0].elements["apRILList[" + index + "].received"].value = cookieArray[i+1];
				
				index++;
				i+=2;
			}
		}
		
		
	}
	
	function saveSubmit() {
		
		
		readCookieForSaving();
		setCookieExDate(10);
		//alert(document.forms[0].elements["apRILList[0].partNumber"].value);
		//alert(document.forms[0].elements["apRILList[0].received"].value);
		//alert(document.forms[0].elements["apRILList[0].misc"].value);


	}
	
	function validateSubmitForm() {
		
		if (blockEnter==true) {
			blockEnter=false;
			document.getElementById("snId2").focus();
			return false;
		}
		saveSubmit();
		return true;
	}

	function setCookieExDate(exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		expires=exdate.toUTCString();
	}
	
	function restoreSession() {
		var cookieStr = document.cookie.split(";");
		if (cookieStr[0]!="") {
			var cookieArray = cookieStr[0].split("|");
			var i;
			for (i=0; i<document.forms[0].elements["poNumber"].options.length; i++) {
				if (document.forms[0].elements["poNumber"].options[i].value==cookieArray[0]) {
					document.forms[0].elements["poNumber"].selectedIndex=i;
					break;
				}
			}

			if (i==document.forms[0].elements["poNumber"].options.length) {
				alert("ERROR: Previous Transaction was already saved.\r\nPlease Select Another PO NUMBER.");
				document.getElementById("pnId").focus();
			}
			else {
				alert("You Can Now Resume Transaction.\r\nPrevious Transaction was Successfully restored.");
				//alert(document.cookie);
				document.getElementById("pnId").focus();
			}
		}
		else alert("ERROR: Previous Transaction was Already Deleted.");
	}
	
	function load() {
		document.getElementById('pnId').focus();
	}
	
</script>

<body onload="load();">
	<html:form action="/apReceivingItemEntry.do" method="POST" enctype="multipart/form-data" onsubmit="return validateSubmitForm();">
		<table border="1" border-collapse="collapse" border-width="1px"  border-style="outset" cellpadding="0" cellspacing="0" padding="1px" width="210">
			<th style="font-size: 11pt; background: lightblue;">AP Receiving Entry Mobile</th>
			<tbody id="tbody">
				<tr>
					<td colspan="4" class="statusBar">
					<logic:equal name="apReceivingItemEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
						<bean:message key="app.success"/>
					</logic:equal>
					<html:errors/>	
					<html:messages id="msg" message="true">
						<bean:write name="msg"/>
					</html:messages>
					
					</td>
				</tr>
				<tr>
					<td align="center">
						<label style="font-size : 9pt; font-weight : bold;">PO NUMBER</label>
						<html:select property="poNumber" styleClass="comboRequired" style="width:100; font-size : 9pt;">
							<html:options property="poNumberList" />
						</html:select>
						<html:hidden property="type"/>
				    	<nested:iterate property="apRILList">
				    		<nested:hidden property="received"/>
				    		<nested:hidden property="partNumber"/>
				    		<nested:hidden property="misc"/>
						</nested:iterate>
					</td>
				</tr>
				<tr>
					<td align="center" height="30px">
						<label style="font-size : 9pt; font-weight:bold;">PN</label>
						<input type="text" id="pnId" onkeypress="createSerialNumber();" size="18" maxlength="18" style="background : #ffffe1; border-color : #000000; border-style : solid; border-width : 1px; font-size : 12pt;"/>
					</td>
				</tr>
			</tbody>
		</table>
		<div>
			<html:submit property="saveSubmitButton2" style="background : #f4f3ef; border-color : #999999; color : #000000; border-style : solid; border-width : 1px; font-size : 7pt; font-weight : bold; width : 90px; height : 20px;">
				<bean:message key="button.saveSubmit"/>
			</html:submit>
			<!--input type="button" value="Restore Session" onclick="restoreSession();" style="background : #f4f3ef; border-color : #999999; color : #000000; border-style : solid; border-width : 1px; font-size : 7pt; font-weight : bold; width : 90px; height : 20px;"/-->
		</div>
	</html:form>
	
</body>
</html>