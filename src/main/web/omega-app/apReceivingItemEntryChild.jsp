<%@ page language="java" import="com.struts.util.Constants"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
	<logic:forward name="adLogon" />
</logic:notPresent><jsp:useBean id="user" scope="session"
	class="com.struts.util.User" />
<html>
<head>
<title><%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%>
- <%=user.getUserName()%> - <bean:message key="receivingItemEntry.title" />
</title>
<link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1"
	type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

function calculateTotalAmount()
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["apRILList[" + i + "].amount"].value))) {

 	        break;

 	        }


 			  var in_amount = (document.forms[0].elements["apRILList[" + i + "].amount"].value).replace(/,/g,'');


 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;
 				 document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(2);

 			  }


 		  i++;
	}

	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(2);
}


function distributeCharges()
{
	var freightCharge = (document.forms[0].elements["freight"].value * 1);
	var duties = (document.forms[0].elements["duties"].value * 1);
	var brokerageFees = (document.forms[0].elements["brokerageFees"].value * 1);
	var others1 = (document.forms[0].elements["others1"].value * 1);
	var others4 = (document.forms[0].elements["others4"].value * 1);

	if(isNaN(freightCharge) || isNaN(duties) || isNaN(brokerageFees)
		|| isNaN(others1) || isNaN(others4) ) return;

	var totalMiscCharges = freightCharge + duties + brokerageFees + others1 + others4;

	var totalValue = 0;
	var index=0;

	while(document.forms[0].elements["apRILList[" + index + "].received"]!=null)
	{
		var quantity = document.forms[0].elements["apRILList[" + index + "].received"].value.replace(/,/g,'') * 1;
		var unitCost = document.forms[0].elements["apRILList[" + index + "].unitCost"].value.replace(/,/g,'') * 1;
		var amount = quantity * unitCost;

		if(!isNaN(amount))
			totalValue+=amount;

		index++;

	}

	if(totalValue==0)
		return;

	index=0;

	while(document.forms[0].elements["apRILList[" + index + "].received"]!=null)
	{
		var quantity = document.forms[0].elements["apRILList[" + index + "].received"].value.replace(/,/g,'') * 1;
		var unitCost = document.forms[0].elements["apRILList[" + index + "].unitCost"].value.replace(/,/g,'') * 1;
		var amount = quantity * unitCost;

		if(!isNaN(quantity) && !isNaN(unitCost) && quantity!=0)
		{
			unitCost = (unitCost + (totalMiscCharges * ((amount/totalValue)/quantity))).toFixed(2)
			document.forms[0].elements["apRILList[" + index + "].unitCost"].value = formatDisabledAmount(unitCost.toString());
			calculateAmount("apRILList[" + index + "].unitCost");
		}

		index++;
	}


}

function calculateAmount(name)
{
  var property = name.substring(0,name.indexOf("."));

  var received = 0;
  var unitCost = 0;
  var amount = 0;

  if (document.forms[0].elements[property + ".received"].value != "" &&
      document.forms[0].elements[property + ".unitCost"].value != "") {

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".received"].value))) {

	  	received = (document.forms[0].elements[property + ".received"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {

	  	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

	  amount = (received * unitCost).toFixed(2);

      if (document.forms[0].elements["type"].value == "ITEMS" && !isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0  ||
       	parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

   		  var totalDiscountAmount = 0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }
	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount).toFixed(2);
	      }
	      if(fixedDiscountAmount != 0) {
	  	  	totalDiscountAmount	= fixedDiscountAmount;
	  	  	amount = (amount - totalDiscountAmount).toFixed(2);
	  	  }
	      if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(2);

	  	  }

	  	  totalDiscountAmount = totalDiscountAmount.toFixed(2);

		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toString());
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

		  calculateTotalAmount();
      } else {


	  	if (document.forms[0].elements["type"].value == "ITEMS") {

			document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

		  } else {

		  		var totalDiscountAmount = document.forms[0].elements[property + ".totalDiscount"].value.replace(/,/g,'');

				var quantity = document.forms[0].elements[property + ".received"].value.replace(/,/g,'') * 1;

				var remaining = document.forms[0].elements[property + ".remaining"].value.replace(/,/g,'') * 1;

				var fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

				totalDiscountAmount = (fixedDiscountAmount * 1) * (quantity / remaining);
				document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toFixed(2).toString());


				if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {
					amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

				}

				amount = (amount - totalDiscountAmount);

				if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  				amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(2);

				}

		  }

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

		  calculateTotalAmount();

	  }

  }

}


var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}

function fnOpenMisc(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   var quantity = document.forms[0].elements[property + ".received"].value.replace(/,/g,'') * 1;

   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array(); //= document.forms[0].elements["apRILList[" + 0 + "].tagList[" + 1 + "].specs"].value;
   var custodian = new Array(document.forms[0].elements["userList"].value);
   var custodian2 = new Array();
   var propertyCode = new Array();
   var expiryDate = new Array();
   var serialNumber = new Array();

   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");

   //var userList = document.forms[0].elements["userList"].value;
   //check at least one disabled field
   var isDisabled = document.forms[0].elements[property + ".received"].disabled;
   var index = 0;
   //alert("before while")
   /*while (true) {
//	  alert(document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value);
	  if (document.forms[0].elements[property + ".tagList[" + index + "].specs"].value!= null ||
		  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value!= null ||
		  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value!= null ||
		  document.forms[0].elements[property + ".tagList[" + index + "].propertyCode"].value!=null) {
		  //alert("d2?");
	 	  specs[index] = document.forms[0].elements[property + ".tagList[" + index + "].specs"].value;
	 	 //alert("inside if")
	 	  serialNumber[index] = document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value;
	 	  expiryDate[index] = document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value;
		  propertyCode[index] = document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value;
		  custodian2[index] = document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value;
	  }else{
		  /*alert("o d2?");

		  expiryDate[index] = "";
		  propertyCode[index] = "";
		  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value="";
		  document.forms[0].elements[property + ".tagList[" + index + "].specs"].value= "";
		  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value="";
		  document.forms[0].elements[property +".tagList[" + index + "].custodian"].value="";
		  //alert("inside else")
		  //break;
	  }
	  //alert("after while")
	  if (++index == quantity) break;
   }*/
   //alert(document.forms[0].elements["apRILList[" + index + "].tagList[" + index2 + "].propertyCode"].value);
   //var specsStr = document.forms[0].elements[property + ".specs"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
		   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&serialNumber=" + serialNumber
		   + "&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

   return false;

}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


  function purchaseOrderEnterSubmit()
  {

      if (document.activeElement.name != 'poNumber') {

          return enterSubmit(event, new Array('saveSubmitButton'));

      } else {

          return true;

      }

  }

  function enterPurchaseOrder()
  {

    disableButtons();
    enableInputControls();
	document.forms[0].elements["isPurchaseOrderEntered"].value = true;
	document.forms[0].submit();

  }

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return purchaseOrderEnterSubmit();">
<html:form action="/apReceivingItemEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
	<%@ include file="cmnHeader.jsp"%>
	<%@ include file="cmnSidebar.jsp"%>
	<table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
		<tr valign="top">
			<td width="187" height="510"></td>
			<td width="581" height="510">
			<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
				<tr>
					<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
					<bean:message key="receivingItemEntry.title" />
					</td>
				</tr>
				<tr>
					<td width="575" height="44" colspan="4" class="statusBar">
					<logic:equal name="apReceivingItemEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
						<bean:message key="app.success" />
					</logic:equal> 
					<html:errors /> 
					<html:messages id="msg" message="true">
						<bean:write name="msg" />
					</html:messages><
					/td>
				</tr>
				<html:hidden property="isPurchaseOrderEntered" value="" />
				<html:hidden property="isTypeEntered" value="" />
				<html:hidden property="isSupplierEntered" value="" />
				<html:hidden property="isTaxCodeEntered" value="" />
				<html:hidden property="isConversionDateEntered" value="" />
				<html:hidden property="enableFields" />
				<html:hidden property="taxRate" />
				<html:hidden property="taxType" />
				<html:hidden property="userList" />

				<logic:equal name="apReceivingItemEntryForm" property="enableFields"
					value="true">
					<tr>
						<td width="575" height="10" colspan="4">
						<div class="tabber">
						<div class="tabbertab" title="Header">
						<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.type" /></td>
								<td width="158" height="25" class="control"><html:select
									property="type" styleClass="comboRequired" style="width:130;"
									onchange="return enterSelect('type','isTypeEntered');">
									<html:options property="typeList" />
								</html:select></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt">
								<bean:message key="receivingItemEntry.prompt.supplier" /></td>
								<logic:equal name="apReceivingItemEntryForm"
									property="useSupplierPulldown" value="true">
									<td width="158" height="25" class="control"><html:select
										property="supplier" styleClass="comboRequired"
										style="width:130;" onchange="return enterPurchaseOrder();">
										<html:options property="supplierList" />
									</html:select> <html:image property="lookupButton" src="images/lookup.gif"
										onclick="return showApSplLookup('supplier', '', 'isPurchaseOrderEntered');" />
									</td>
								</logic:equal>
								<logic:equal name="apReceivingItemEntryForm"
									property="useSupplierPulldown" value="false">
									<td width="158" height="25" class="control"><html:text
										property="supplier" styleClass="textRequired" size="15"
										maxlength="25" readonly="true" /> <html:image
										property="lookupButton" src="images/lookup.gif"
										onclick="return showApSplLookup('supplier', '', 'isPurchaseOrderEntered');" />
									</td>
								</logic:equal>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.date" /></td>
								<td width="157" height="25" class="control"><html:text
									property="date" size="10" maxlength="10"
									styleClass="textRequired" /></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.referenceNumber" /></td>
								<td width="158" height="25" class="control"><html:text
									property="referenceNumber" size="15" maxlength="25"
									styleClass="textRequired" /></td>
								<td width="130" height="25" class="prompt">Control Number</td>
								<td width="157" height="25" class="control"><html:text
									property="documentNumber" size="15" maxlength="25"
									styleClass="text" /></td>
							</tr>
							<logic:equal name="apReceivingItemEntryForm" property="type"
								value="PO MATCHED">
								<tr>
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.poNumber" /></td>
									<td width="445" height="25" class="control" colspan="3"><html:text
										property="poNumber" size="15" maxlength="25"
										styleClass="textRequired"
										onblur="return enterPurchaseOrder();"
										onkeypress="javascript:if (event.keyCode == 13) return false;" />
									<html:image property="lookupButton" src="images/lookup.gif"
										onclick="return showApPoLookup('poNumber');" /></td>
								</tr>
							</logic:equal>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.description" /></td>
								<td width="445" height="25" class="control" colspan="3"><html:textarea
									property="description" cols="20" rows="4" styleClass="textRequired" />
								</td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.supplierName" /></td>
								<td width="445" height="25" class="control" colspan="3"><html:text
									property="supplierName" size="50" maxlength="50"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
							
								<td width="130" height="25" class="prompt">
	                   				<bean:message key="receivingItemEntry.prompt.totalAmount"/>
	                			</td>
	                			<td width="157" height="25" class="control">
	                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
	                			</td>
							</tr>
							

						</table>
						</div>
						<div class="tabbertab" title="Misc">
						<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>

							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.paymentTerm" /></td>
								<td width="158" height="25" class="control"><html:select
									property="paymentTerm" styleClass="comboRequired"
									style="width:130;">
									<html:options property="paymentTermList" />
								</html:select></td>
								<logic:equal name="apReceivingItemEntryForm"
									property="enableReceivingItemVoid" value="true">
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.receivingItemVoid" /></td>
									<td width="157" height="25" class="control"><html:checkbox
										property="receivingItemVoid" /></td>
								</logic:equal>
								<logic:equal name="apReceivingItemEntryForm"
									property="enableReceivingItemVoid" value="false">
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.receivingItemVoid" /></td>
									<td width="157" height="25" class="control"><html:checkbox
										property="receivingItemVoid" disabled="true" /></td>
								</logic:equal>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.usePettyCash" /></td>
								<td width="445" height="25" class="control"><html:checkbox
									property="usePettyCash" /></td>

								<td width="130" height="25" class="prompt">
                   				<bean:message key="receivingItemEntry.prompt.shipmentNumber"/>
                				</td>
                				<td width="157" height="25" class="control">
                   				<html:text property="shipmentNumber" size="15" maxlength="25" styleClass="text"/>
                				</td>

							</tr>

							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.freight" /></td>
								<td width="128" height="25" class="control"><html:text
									property="freight" size="10" maxlength="10" styleClass="text" />
								</td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.firstReceiving" /></td>
								<td width="127" height="25" class="control"><html:checkbox
									property="firstReceiving" /></td>
							</tr>
							<tr>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.tax1" /></td>
								<td width="127" height="25" class="control"><html:text
									property="tax1" size="10" maxlength="10" styleClass="text" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.tax2" /></td>
								<td width="127" height="25" class="control"><html:text
									property="tax2" size="10" maxlength="10" styleClass="text" /></td>
							</tr>
							<tr>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.duties" /></td>
								<td width="127" height="25" class="control"><html:text
									property="duties" size="10" maxlength="10" styleClass="text" />
								</td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.brokerageFees" /></td>
								<td width="127" height="25" class="control"><html:text
									property="brokerageFees" size="10" maxlength="10"
									styleClass="text" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.others1" /></td>
								<td width="128" height="25" class="control"><html:text
									property="others1" size="10" maxlength="10" styleClass="text" />
								</td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.others2" /></td>
								<td width="127" height="25" class="control"><html:text
									property="others2" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.others3" /></td>
								<td width="128" height="25" class="control"><html:text
									property="others3" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.others4" /></td>
								<td width="127" height="25" class="control"><html:text
									property="others4" size="10" maxlength="10" styleClass="text" />
								</td>
							</tr>



							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy" styleClass="comboRequired">
				                       <html:options property="inspectedByList"/>
				                   </html:select>
				                	</td>

								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.dateInspected" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateInspected" size="10" maxlength="10" styleClass="text" />
								</td>
							</tr>

							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy2" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy2" styleClass="comboRequired">
				                       <html:options property="inspectedByList2"/>
				                   </html:select>
				               	</td>
				               	<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.dateReleased" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateReleased" size="10" maxlength="10" styleClass="text" />
								</td>
							</tr>

							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy3" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy3" styleClass="comboRequired">
				                       <html:options property="inspectedByList3"/>
				                   </html:select>
				               	</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy4" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy4" styleClass="comboRequired">
				                       <html:options property="inspectedByList4"/>
				                   </html:select>
				               	</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.releasedBy" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="releasedBy" styleClass="comboRequired">
				                       <html:options property="releasedByList"/>
				                   </html:select>
				                	</td>

							</tr>
							<tr>
								<td width="300" height="50"><html:submit
									property="distChargesButton" styleClass="mainButtonBig"
									onclick="distributeCharges(); return false;">
									<bean:message key="button.distCharges" />
								</html:submit></td>
							</tr>

						</table>
						</div>
						<div class="tabbertab" title="Tax">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.taxCode" /></td>
								<logic:equal name="apReceivingItemEntryForm" property="type"
									value="ITEMS">
									<td width="415" height="25" class="control" colspan="3"><html:select
										property="taxCode" styleClass="comboRequired"
										onchange="return enterSelect('taxCode','isTaxCodeEntered');">
										<html:options property="taxCodeList" />
									</html:select></td>
								</logic:equal>
								<logic:equal name="apReceivingItemEntryForm" property="type"
									value="PO MATCHED">
									<td width="415" height="25" class="control" colspan="3"><html:select
										property="taxCode" styleClass="comboRequired" disabled="true">
										<html:options property="taxCodeList" />
									</html:select></td>
								</logic:equal>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Currency">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.currency" /></td>
								<td width="415" height="25" class="control" colspan="3"><html:select
									property="currency" styleClass="comboRequired"
									onchange="return enterSelect('conversionDate','isConversionDateEntered');">
									<html:options property="currencyList" />
								</html:select></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.conversionDate" /></td>
								<td width="128" height="25" class="control"><html:text
									property="conversionDate" size="10" maxlength="10"
									styleClass="text"
									onblur="return enterSelect('conversionDate','isConversionDateEntered');" />
								</td>
								<td class="prompt" width="100" height="25"><bean:message
									key="receivingItemEntry.prompt.conversionRate" /></td>
								<td width="127" height="25" class="control"><html:text
									property="conversionRate" size="20" maxlength="20"
									styleClass="text" /></td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Attachment">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename1" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename1" size="35" styleClass="text" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton1" value="true">
									<html:submit property="viewAttachmentButton1"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton1" value="true">
									<html:submit property="viewAttachmentButton1"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename2" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename2" size="35" styleClass="text" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton2" value="true">
									<html:submit property="viewAttachmentButton2"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton2" value="true">
									<html:submit property="viewAttachmentButton2"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename3" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename3" size="35" styleClass="text" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton3" value="true">
									<html:submit property="viewAttachmentButton3"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton3" value="true">
									<html:submit property="viewAttachmentButton3"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename4" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename4" size="35" styleClass="text" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton4" value="true">
									<html:submit property="viewAttachmentButton4"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton4" value="true">
									<html:submit property="viewAttachmentButton4"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Status">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.approvalStatus" /></td>
								<td width="128" height="25" class="control"><html:text
									property="approvalStatus" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.posted" /></td>
								<td width="127" height="25" class="control"><html:text
									property="posted" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.reasonForRejection" /></td>
								<td width="415" height="25" class="control" colspan="3"><html:textarea
									property="reasonForRejection" cols="44" rows="2"
									styleClass="text" disabled="true" /></td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Log">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.createdBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="createdBy" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateCreated" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateCreated" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.lastModifiedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="lastModifiedBy" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateLastModified" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateLastModified" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.approvedRejectedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="approvedRejectedBy" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateApprovedRejected" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateApprovedRejected" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.postedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="postedBy" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.datePosted" /></td>
								<td width="127" height="25" class="control"><html:text
									property="datePosted" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
						</table>
						</div>
						</div>
						<script>tabberAutomatic(tabberOptions)</script></td>
					</tr>
					<tr>
						<td width="575" height="50" colspan="4">
						<div id="buttons">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="showSaveButton" value="true">
							<html:submit property="saveSubmitButton"
								styleClass="mainButtonMedium"
								onclick="return confirmSaveSubmit(); ">
								<bean:message key="button.saveSubmit" />
							</html:submit>
							<html:submit property="saveAsDraftButton"
								styleClass="mainButtonMedium">
								<bean:message key="button.saveAsDraft" />
							</html:submit>
						</logic:equal>

						<logic:equal name="apReceivingItemEntryForm"
							property="showDeleteButton" value="true">
							<html:submit property="deleteButton" styleClass="mainButton"
								onclick="return confirmDelete();">
								<bean:message key="button.delete" />
							</html:submit>
						</logic:equal>


						<html:submit property="printInspectionButton" styleClass="mainButtonMedium">
							<bean:message key="button.printInspection" />
						</html:submit>
						<html:submit property="printReleaseButton" styleClass="mainButtonMedium">
							<bean:message key="button.printRelease" />
						</html:submit>

						<html:submit property="printButton" styleClass="mainButton">
							<bean:message key="button.print" />
						</html:submit>


						<html:submit property="printReceivingReportButton"
							styleClass="mainButtonMedium">
							<bean:message key="button.printReceivingReportButton" />
						</html:submit>

						 <html:submit property="journalButton" styleClass="mainButton">
							<bean:message key="button.journal" />
						</html:submit>

						<html:submit property="closeButton" styleClass="mainButton">
							<bean:message key="button.close" />
						</html:submit>
						</div>
						<div id="buttonsDisabled" style="display: none;">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="showSaveButton" value="true">
							<html:submit property="saveSubmitButton"
								styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveSubmit" />
							</html:submit>
							<html:submit property="saveAsDraftButton"
								styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveAsDraft" />
							</html:submit>
						</logic:equal> <logic:equal name="apReceivingItemEntryForm"
							property="showDeleteButton" value="true">
							<html:submit property="deleteButton" styleClass="mainButton"
								disabled="true">
								<bean:message key="button.delete" />
							</html:submit>
						</logic:equal>

						<html:submit property="printInspectionButton" styleClass="mainButtonMedium"
							disabled="true">
							<bean:message key="button.printInspection" />
						</html:submit>

						<html:submit property="printReleaseButton" styleClass="mainButtonMedium"
							disabled="true">
							<bean:message key="button.printRelease" />
						</html:submit>

						<html:submit property="printButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.print" />
						</html:submit>


						<html:submit property="printReceivingReportButton"
							styleClass="mainButtonMedium" disabled="true">
							<bean:message key="button.printReceivingReportButton" />
						</html:submit>

						<html:submit property="journalButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.journal" />
						</html:submit>

						<html:submit property="closeButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.close" />
						</html:submit>
						</div>
						</td>
					</tr>
					<logic:equal name="apReceivingItemEntryForm" property="type"
						value="PO MATCHED">
						<tr valign="top">
							<td width="590" height="185" colspan="4">
							<div align="center">
							<table border="1" cellpadding="0" cellspacing="0" width="590"
								height="47"
								bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
								bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
								<tr>
									<td width="590" height="1" colspan="9" class="gridTitle"
										bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>"><bean:message
										key="receivingItemEntry.gridTitle.RILDetails" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.lineNumber" /></td>
									<td width="120" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemName" /></td>
									<td width="100" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.location" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.remaining" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.received" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unit" /></td>
									<td width="165" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unitCost" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.misc" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
									<td width="320" height="1" class="gridHeader" colspan="4"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDescription" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDiscount" /></td>
									<td width="165" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.amount" /></td>
									<td width="40" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.conversionFactor" /></td>
								</tr>
								<%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>

								<nested:iterate property="apRILList">
									<%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
									<nested:hidden property="isItemEntered" value="" />
									<nested:hidden property="isUnitEntered" value="" />
									<nested:hidden property="receivingItemLineCode" />
									<nested:hidden property="plPlCode" />
									<nested:hidden property="discount1" />
									<nested:hidden property="discount2" />
									<nested:hidden property="discount3" />
									<nested:hidden property="discount4" />
									<nested:hidden property="fixedDiscountAmount" />

									<tr bgcolor="<%= rowBgc %>">
										<td width="30" height="1" class="control"><nested:text
											property="lineNumber" size="1" maxlength="4"
											styleClass="text" disabled="true" /></td>
										<td width="120" height="1" class="control"><nested:text
											property="itemName" size="10" maxlength="25"
											styleClass="textRequired" disabled="true" /> <nested:image
											property="lookupButton" src="images/lookup.gif"
											disabled="true" /></td>
										<td width="100" height="1" class="control"><nested:select
											property="location" styleClass="comboRequired"
											style="width:80;" disabled="true">
											<nested:options property="locationList" />
										</nested:select></td>
										<td width="50" height="1" class="control"><nested:text
											property="remaining" size="5" maxlength="10"
											styleClass="text" disabled="true" /></td>
										<td width="50" height="1" class="control"><nested:text
											property="received" size="5" maxlength="10"
											styleClass="textRequired" onblur="calculateAmount(name);"
											onkeyup="calculateAmount(name);" /></td>
										<td width="60" height="1" class="control"><nested:select
											property="unit" styleClass="comboRequired" style="width:80;"
											onchange="return enterSelectGrid(name, 'unit','isUnitEntered');"
											disabled="true">
											<nested:options property="unitList" />
										</nested:select></td>
										<td width="165" height="1" class="control"><nested:text
											property="unitCost" size="20" maxlength="25"
											styleClass="textAmountRequired"
											onchange="calculateAmount(name);"
											onblur="calculateAmount(name); addZeroes(name);"
											onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"
											disabled="false" /></td>
										<td width="50" align="center" height="1">
										<div id="buttons"><nested:hidden property="misc" />
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												onclick="return fnOpenMisc(name);">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										<div id="buttonsDisabled" style="display: none;">
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												disabled="true">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										</td>
									</tr>
									<tr bgcolor="<%= rowBgc %>">
										<td width="30" height="1" class="control" />
										<td width="320" height="1" class="control" colspan="4"><nested:text
											property="itemDescription" size="40" maxlength="70"
											styleClass="text" style="font-size:8pt;" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:text
											property="totalDiscount" size="9" maxlength="10"
											styleClass="textAmount" disabled="true" /></td>
										<td width="165" height="1" class="control"><nested:text
											property="amount" size="20" maxlength="25"
											styleClass="textAmount" disabled="true" /></td>
									   <td width="10" height="1" class="control">
									       <nested:text property="conversionFactor" size="4" maxlength="4" styleClass="textAmount" disabled="false" />
									   </td>
									</tr>

								</nested:iterate>
							</table>
							</div>
							</td>
						</tr>
					</logic:equal>

					<logic:equal name="apReceivingItemEntryForm" property="type"
						value="ITEMS">
						<tr valign="top">
							<td width="575" height="185" colspan="4">
							<div align="center">
							<table border="1" cellpadding="0" cellspacing="0" width="577"
								height="47"
								bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
								bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
								<tr>
									<td width="575" height="1" colspan="8" class="gridTitle"
										bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>"><bean:message
										key="receivingItemEntry.gridTitle.RILDetails" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.lineNumber" /></td>
									<td width="120" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemName" /></td>
									<td width="100" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.location" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.quantity" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unit" /></td>
									<td width="145" height="1" class="gridHeader" colspan="2"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unitCost" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.delete" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
									<td width="270" height="1" class="gridHeader" colspan="3"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDescription" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDiscount" /></td>
									<td width="195" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.amount" /></td>
									<td width="195" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.conversionFactor" /></td>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
								</tr>
								<%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
								<nested:iterate property="apRILList">
									<%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
									<nested:hidden property="isItemEntered" value="" />
									<nested:hidden property="isUnitEntered" value="" />
									<tr bgcolor="<%= rowBgc %>">
<%-- 									<nested:iterate property="tagList"> --%>
<%-- 										<nested:hidden property="propertyCode" /> --%>
<%-- 										<nested:hidden property="specs" /> --%>
<%-- 										<nested:hidden property="expiryDate" /> --%>
<%-- 										<nested:hidden property="serialNumber" /> --%>
<%-- 										<nested:hidden property="custodian" /> --%>
<%-- 									</nested:iterate> --%>
										<td width="30" height="1" class="control"><nested:text
											property="lineNumber" size="1" maxlength="4"
											styleClass="text" disabled="true" /></td>
										<td width="120" height="1" class="control"><nested:text
											property="itemName" size="12" maxlength="25"
											styleClass="textRequired" /> <nested:image
											property="lookupButton" src="images/lookup.gif"
											onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');" />
										</td>
										<td width="100" height="1" class="control"><nested:select
											property="location" styleClass="comboRequired"
											style="width:100;">
											<nested:options property="locationList" />
										</nested:select></td>
										<td width="50" height="1" class="control"><nested:text
											property="received" size="5" maxlength="10"
											styleClass="textRequired" onblur="calculateAmount(name);"
											onkeyup="calculateAmount(name);" /></td>
										<td width="60" height="1" class="control"><nested:select
											property="unit" styleClass="comboRequired" style="width:80;"
											onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
											<nested:options property="unitList" />
										</nested:select></td>
										<td width="145" height="1" class="control"><nested:text
											property="unitCost" size="17" maxlength="25"
											styleClass="textAmountRequired"
											onchange="calculateAmount(name);"
											onblur="calculateAmount(name); addZeroes(name);"
											onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);" />
										</td>
										<td width="30" height="1" class="control" />
										<td width="50" height="1" class="control">
										<p align="center"><nested:checkbox
											property="deleteCheckbox" />
										</td>
									</tr>
									<tr bgcolor="<%= rowBgc %>">
										<td width="30" height="1" class="control" />
										<td width="270" height="1" class="control" colspan="3"><nested:text
											property="itemDescription" size="40" maxlength="70"
											styleClass="text" style="font-size:8pt;" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:text
											property="totalDiscount" size="9" maxlength="10"
											styleClass="textAmount" disabled="true" /></td>
										<td width="145" height="1" class="control"><nested:text
											property="amount" size="17" maxlength="25"
											styleClass="textAmount" onchange="calculateAmount(name);"
											onblur="calculateAmount(name); addZeroes(name);"
											onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);" />
										</td>
									   <td width="173.5" height="1" class="control">
									       <nested:text property="conversionFactor" size="5" maxlength="10" styleClass="textAmount" disabled="false" />
									   </td>
										<td width="100" align="center" height="1">
										<div id="buttons"><nested:hidden property="discount1" />
											<nested:hidden property="discount2" /> <nested:hidden
												property="discount3" /> <nested:hidden property="discount4" />
											<nested:hidden property="fixedDiscountAmount" /> <nested:submit
												property="discountButton" styleClass="mainButtonSmall"
												onclick="return fnOpenDiscount(name);">
												<bean:message key="button.discounts" />
											</nested:submit>
											 <nested:hidden property="misc" />
											 <nested:equal property="isTraceMisc" value = "true">
											 <nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												onclick="return fnOpenMisc(name);">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										<div id="buttonsDisabled" style="display: none;">
											<nested:submit
											property="discountButton" styleClass="mainButtonSmall"
											disabled="true">
											<bean:message key="button.discounts" />
											</nested:submit>
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit property="miscButton"
												styleClass="mainButtonSmall" disabled="true">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										</td>
									</tr>
								</nested:iterate>
							</table>
							</div>
							</td>
						</tr>
					</logic:equal>
					<tr>
						<td width="575" height="25" colspan="4">
						<div id="buttons">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="type" value="ITEMS">
							<logic:equal name="apReceivingItemEntryForm"
								property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton"
									styleClass="mainButtonMedium">
									<bean:message key="button.addLines" />
								</html:submit>
							</logic:equal>
							<logic:equal name="apReceivingItemEntryForm"
								property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton"
									styleClass="mainButtonMedium">
									<bean:message key="button.deleteLines" />
								</html:submit>
							</logic:equal>
						</logic:equal>
						</div>
						<div id="buttonsDisabled" style="display: none;">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="type" value="ITEMS">
							<logic:equal name="apReceivingItemEntryForm"
								property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton"
									styleClass="mainButtonMedium" disabled="true">
									<bean:message key="button.addLines" />
								</html:submit>
							</logic:equal>
							<logic:equal name="apReceivingItemEntryForm"
								property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton"
									styleClass="mainButtonMedium" disabled="true">
									<bean:message key="button.deleteLines" />
								</html:submit>
							</logic:equal>
						</logic:equal>
						</div>
						</td>
					</tr>
				</logic:equal>

				<logic:equal name="apReceivingItemEntryForm" property="enableFields"
					value="false">
					<tr>
						<td width="575" height="10" colspan="4">
						<div class="tabber">
						<div class="tabbertab" title="Header">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.type" /></td>
								<td width="158" height="25" class="control"><html:select
									property="type" styleClass="comboRequired" style="width:130;"
									disabled="true">
									<html:options property="typeList" />
								</html:select></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.supplier" /></td>
								<logic:equal name="apReceivingItemEntryForm"
									property="useSupplierPulldown" value="true">
									<td width="158" height="25" class="control"><html:select
										property="supplier" styleClass="comboRequired"
										style="width:130;" disabled="true">
										<html:options property="supplierList" />
									</html:select> <html:image property="lookupButton" src="images/lookup.gif"
										disabled="true" /></td>
								</logic:equal>
								<logic:equal name="apReceivingItemEntryForm"
									property="useSupplierPulldown" value="false">
									<td width="158" height="25" class="control"><html:text
										property="supplier" styleClass="textRequired" size="15"
										maxlength="25" readonly="true" disabled="true" /> <html:image
										property="lookupButton" src="images/lookup.gif"
										disabled="true" /></td>
								</logic:equal>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.date" /></td>
								<td width="157" height="25" class="control"><html:text
									property="date" size="10" maxlength="10"
									styleClass="textRequired" disabled="true" /></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.referenceNumber" /></td>
								<td width="158" height="25" class="control"><html:text
									property="referenceNumber" size="15" maxlength="25"
									styleClass="textRequired" disabled="true" /></td>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.documentNumber" /></td>
								<td width="157" height="25" class="control"><html:text
									property="documentNumber" size="15" maxlength="25"
									styleClass="text" disabled="true" /></td>
							</tr>
							<logic:equal name="apReceivingItemEntryForm" property="type"
								value="PO MATCHED">
								<tr>
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.poNumber" /></td>
									<td width="445" height="25" class="control" colspan="3"><html:text
										property="poNumber" size="15" maxlength="25"
										styleClass="textRequired" disabled="true" /> <html:image
										property="lookupButton" src="images/lookup.gif"
										disabled="true" /></td>
								</tr>
							</logic:equal>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.description" /></td>
								<td width="445" height="25" class="control" colspan="3"><html:text
									property="description" size="50" maxlength="150"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.supplierName" /></td>
								<td width="445" height="25" class="control" colspan="3"><html:text
									property="supplierName" size="50" maxlength="50"
									styleClass="text" disabled="true" /></td>
							</tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="receivingItemEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</table>
						</div>
						<div class="tabbertab" title="Misc">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>

							<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc1"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc1" styleClass="combo" disabled="true" >
									<html:options property="misc1List"/>
								</html:select>
							</td>

         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc2"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc2" styleClass="combo" disabled="true" >
									<html:options property="misc2List"/>
								</html:select>
							</td>

         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc3"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc3" styleClass="combo" disabled="true" >
									<html:options property="misc3List"/>
								</html:select>
							</td>

         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc4"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc4" styleClass="combo"  disabled="true">
									<html:options property="misc4List"/>
								</html:select>
							</td>

         				</tr>

         				<tr>
         					<td class="prompt" width="150" height="25">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc5"/>
							</td>
							<td width="128" height="25" class="control">
							    <html:select property="misc5" styleClass="combo"  disabled="true">
									<html:options property="misc5List"/>
								</html:select>
							</td>

         				</tr>

         				<tr>
         					<td class="prompt" width="160" height="26">
					    		<bean:message key="purchaseRequisitionEntry.prompt.misc6"/>
							</td>
							<td width="128" height="26" class="control">
							    <html:select property="misc6" styleClass="combo"  disabled="true">
									<html:options property="misc6List"/>
								</html:select>
							</td>

         				</tr>

							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.paymentTerm" /></td>
								<td width="158" height="25" class="control"><html:select
									property="paymentTerm" styleClass="comboRequired"
									style="width:130;" disabled="true">
									<html:options property="paymentTermList" />
								</html:select></td>
								<logic:equal name="apReceivingItemEntryForm"
									property="enableReceivingItemVoid" value="true">
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.receivingItemVoid" /></td>
									<td width="157" height="25" class="control"><html:checkbox
										property="receivingItemVoid" /></td>
								</logic:equal>
								<logic:equal name="apReceivingItemEntryForm"
									property="enableReceivingItemVoid" value="false">
									<td width="130" height="25" class="prompt"><bean:message
										key="receivingItemEntry.prompt.receivingItemVoid" /></td>
									<td width="157" height="25" class="control"><html:checkbox
										property="receivingItemVoid" disabled="true" /></td>
								</logic:equal>
							</tr>
							<tr>
								<td width="130" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.usePettyCash" /></td>
								<td width="445" height="25" class="control"><html:checkbox
									property="usePettyCash" disabled="true" /></td>

									<td width="130" height="25" class="prompt">
                   				<bean:message key="receivingItemEntry.prompt.shipmentNumber"/>
                				</td>
                				<td width="157" height="25" class="control">
                   				<html:text property="shipmentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                				</td>
							</tr>


							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.freight" /></td>
								<td width="128" height="25" class="control"><html:text
									property="freight" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.tax1" /></td>
								<td width="127" height="25" class="control"><html:text
									property="tax1" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.tax2" /></td>
								<td width="127" height="25" class="control"><html:text
									property="tax2" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.duties" /></td>
								<td width="127" height="25" class="control"><html:text
									property="duties" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.brokerageFees" /></td>
								<td width="127" height="25" class="control"><html:text
									property="brokerageFees" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.others1" /></td>
								<td width="128" height="25" class="control"><html:text
									property="others1" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.others2" /></td>
								<td width="127" height="25" class="control"><html:text
									property="others2" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.others3" /></td>
								<td width="128" height="25" class="control"><html:text
									property="others3" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.others4" /></td>
								<td width="127" height="25" class="control"><html:text
									property="others4" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy" styleClass="comboRequired" disabled="true">
				                       <html:options property="inspectedByList" />
				                   </html:select>
				                	</td>

								<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.dateInspected" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateInspected" size="10" maxlength="10" styleClass="text" />
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy2" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy2" styleClass="comboRequired" disabled="true">
				                       <html:options property="inspectedByList2"/>
				                   </html:select>
				               	</td>
				               	<td class="prompt" width="160" height="25"><bean:message
									key="receivingItemEntry.prompt.dateReleased" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateReleased" size="10" maxlength="10" styleClass="text" />
								</td>
							</tr>

							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy3" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy3" styleClass="comboRequired" disabled="true">
				                       <html:options property="inspectedByList3"/>
				                   </html:select>
				               	</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.inspectedBy4" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="inspectedBy4" styleClass="comboRequired" disabled="true">
				                       <html:options property="inspectedByList4"/>
				                   </html:select>
				               	</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.releasedBy" /></td>
									<td width="128" height="25" class="control">
				                   <html:select property="releasedBy" styleClass="comboRequired" disabled="true">
				                       <html:options property="releasedByList"/>
				                   </html:select>
				               	</td>

							</tr>


						</table>
						</div>
						<div class="tabbertab" title="Tax">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.taxCode" /></td>
								<td width="415" height="25" class="control" colspan="3"><html:select
									property="taxCode" styleClass="comboRequired" disabled="true">
									<html:options property="taxCodeList" />
								</html:select></td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Currency">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.currency" /></td>
								<td width="415" height="25" class="control" colspan="3"><html:select
									property="currency" styleClass="comboRequired" disabled="true">
									<html:options property="currencyList" />
								</html:select></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.conversionDate" /></td>
								<td width="128" height="25" class="control"><html:text
									property="conversionDate" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td class="prompt" width="100" height="25"><bean:message
									key="receivingItemEntry.prompt.conversionRate" /></td>
								<td width="127" height="25" class="control"><html:text
									property="conversionRate" size="20" maxlength="20"
									styleClass="text" disabled="true" /></td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Attachment">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename1" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename1" size="35" styleClass="text"
									disabled="true" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton1" value="true">
									<html:submit property="viewAttachmentButton1"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton1" value="true">
									<html:submit property="viewAttachmentButton1"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename2" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename2" size="35" styleClass="text"
									disabled="true" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton2" value="true">
									<html:submit property="viewAttachmentButton2"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton2" value="true">
									<html:submit property="viewAttachmentButton2"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename3" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename3" size="35" styleClass="text"
									disabled="true" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton3" value="true">
									<html:submit property="viewAttachmentButton3"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton3" value="true">
									<html:submit property="viewAttachmentButton3"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.filename4" /></td>
								<td width="288" height="25" class="control" colspan="2"><html:file
									property="filename4" size="35" styleClass="text"
									disabled="true" /></td>
								<td width="127" height="25" class="control">
								<div id="buttons">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton4" value="true">
									<html:submit property="viewAttachmentButton4"
										styleClass="mainButton">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								<div id="buttonsDisabled" style="display: none;">
								<p align="center"><logic:equal
									name="apReceivingItemEntryForm"
									property="showViewAttachmentButton4" value="true">
									<html:submit property="viewAttachmentButton4"
										styleClass="mainButton" disabled="true">
										<bean:message key="button.viewAttachment" />
									</html:submit>
								</logic:equal>
								</div>
								</td>
							</tr>
						</table>
						</div>

						<div class="tabbertab" title="Status">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.approvalStatus" /></td>
								<td width="128" height="25" class="control"><html:text
									property="approvalStatus" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.posted" /></td>
								<td width="127" height="25" class="control"><html:text
									property="posted" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.reasonForRejection" /></td>
								<td width="415" height="25" class="control" colspan="3"><html:textarea
									property="reasonForRejection" cols="44" rows="2"
									styleClass="text" disabled="true" /></td>
							</tr>
						</table>
						</div>
						<div class="tabbertab" title="Log">
						<table border="0" cellpadding="0" cellspacing="0" width="575"
							height="25">
							<tr>
								<td class="prompt" width="575" height="25" colspan="4"></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.createdBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="createdBy" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateCreated" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateCreated" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.lastModifiedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="lastModifiedBy" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateLastModified" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateLastModified" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.approvedRejectedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="approvedRejectedBy" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.dateApprovedRejected" /></td>
								<td width="127" height="25" class="control"><html:text
									property="dateApprovedRejected" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
							<tr>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.postedBy" /></td>
								<td width="128" height="25" class="control"><html:text
									property="postedBy" size="10" maxlength="10" styleClass="text"
									disabled="true" /></td>
								<td width="160" height="25" class="prompt"><bean:message
									key="receivingItemEntry.prompt.datePosted" /></td>
								<td width="127" height="25" class="control"><html:text
									property="datePosted" size="10" maxlength="10"
									styleClass="text" disabled="true" /></td>
							</tr>
						</table>
						</div>
						</div>
						<script>tabberAutomatic(tabberOptions)</script></td>
					</tr>
					<tr>
						<td width="575" height="50" colspan="4">
						<div id="buttons">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="showSaveButton" value="true">
							<html:submit property="saveSubmitButton"
								styleClass="mainButtonMedium"
								onclick="return confirmSaveSubmit();">
								<bean:message key="button.saveSubmit" />
							</html:submit>
							<html:submit property="saveAsDraftButton"
								styleClass="mainButtonMedium">
								<bean:message key="button.saveAsDraft" />
							</html:submit>
						</logic:equal> <logic:equal name="apReceivingItemEntryForm"
							property="showDeleteButton" value="true">
							<html:submit property="deleteButton" styleClass="mainButton"
								onclick="return confirmDelete();">
								<bean:message key="button.delete" />
							</html:submit>
						</logic:equal>

						<html:submit property="printInspectionButton" styleClass="mainButtonMedium">
							<bean:message key="button.printInspection" />
						</html:submit>

						<html:submit property="printReleaseButton" styleClass="mainButtonMedium">
							<bean:message key="button.printRelease" />
						</html:submit>

						<html:submit property="printButton" styleClass="mainButton">
							<bean:message key="button.print" />
						</html:submit>

						<html:submit property="printReceivingReportButton"
							styleClass="mainButtonMedium">
							<bean:message key="button.printReceivingReportButton" />
						</html:submit>

						<html:submit property="journalButton" styleClass="mainButton">
							<bean:message key="button.journal" />
						</html:submit>

						<html:submit property="closeButton" styleClass="mainButton">
							<bean:message key="button.close" />
						</html:submit>

						</div>
						<div id="buttonsDisabled" style="display: none;">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="showSaveButton" value="true">
							<html:submit property="saveSubmitButton"
								styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveSubmit" />
							</html:submit>
							<html:submit property="saveAsDraftButton"
								styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveAsDraft" />
							</html:submit>
						</logic:equal> <logic:equal name="apReceivingItemEntryForm"
							property="showDeleteButton" value="true">
							<html:submit property="deleteButton" styleClass="mainButton"
								disabled="true">
								<bean:message key="button.delete" />
							</html:submit>
						</logic:equal> <html:submit property="printButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.print" />
						</html:submit> <html:submit property="printReceivingReportButton"
							styleClass="mainButton" disabled="true">
							<bean:message key="button.printReceivingReportButton" />
						</html:submit> <html:submit property="journalButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.journal" />
						</html:submit> <html:submit property="closeButton" styleClass="mainButton"
							disabled="true">
							<bean:message key="button.close" />
						</html:submit>
						</div>
						</td>
					</tr>
					<logic:equal name="apReceivingItemEntryForm" property="type"
						value="PO MATCHED">
						<tr valign="top">
							<td width="590" height="185" colspan="4">
							<div align="center">
							<table border="1" cellpadding="0" cellspacing="0" width="590"
								height="47"
								bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
								bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
								<tr>
									<td width="590" height="1" colspan="9" class="gridTitle"
										bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>"><bean:message
										key="receivingItemEntry.gridTitle.RILDetails" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.lineNumber" /></td>
									<td width="120" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemName" /></td>
									<td width="100" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.location" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.remaining" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.received" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unit" /></td>
									<td width="165" height="1" class="gridHeader" colspan="2"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unitCost" /></td>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
									<td width="320" height="1" class="gridHeader" colspan="4"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDescription" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDiscount" /></td>
									<td width="165" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.amount" /></td>
									<td width="165" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.conversionFactor" /></td>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
								</tr>
								<%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
								<nested:iterate property="apRILList">
									<%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
									<nested:hidden property="isItemEntered" value="" />
									<nested:hidden property="isUnitEntered" value="" />
									<nested:hidden property="receivingItemLineCode" />
									<nested:hidden property="plPlCode" />

									<tr bgcolor="<%= rowBgc %>">
<%-- 									<nested:iterate property="tagList"> --%>
<%-- 										<nested:hidden property="propertyCode" /> --%>
<%-- 										<nested:hidden property="specs" /> --%>
<%-- 										<nested:hidden property="expiryDate" /> --%>
<%-- 										<nested:hidden property="serialNumber" /> --%>
<%-- 										<nested:hidden property="custodian" /> --%>
<%-- 									</nested:iterate> --%>
										<td width="30" height="1" class="control"><nested:text
											property="lineNumber" size="1" maxlength="4"
											styleClass="text" disabled="true" /></td>
										<td width="120" height="1" class="control"><nested:text
											property="itemName" size="10" maxlength="25"
											styleClass="textRequired" disabled="true" /> <nested:image
											property="lookupButton" src="images/lookup.gif"
											disabled="true" /></td>
										<td width="100" height="1" class="control"><nested:select
											property="location" styleClass="comboRequired"
											style="width:80;" disabled="true">
											<nested:options property="locationList" />
										</nested:select></td>
										<td width="50" height="1" class="control"><nested:text
											property="remaining" size="5" maxlength="10"
											styleClass="text" disabled="true" /></td>
										<td width="50" height="1" class="control"><nested:text
											property="received" size="5" maxlength="10"
											styleClass="textRequired" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:select
											property="unit" styleClass="comboRequired" style="width:80;"
											disabled="true">
											<nested:options property="unitList" />
										</nested:select></td>
										<td width="165" height="1" class="control"><nested:text
											property="unitCost" size="20" maxlength="25"
											styleClass="textAmountRequired" disabled="true" /></td>
										<td width="50" align="center" height="1"></td>
										<td width="50" align="center" height="1">
										<div id="buttons">

											<nested:hidden property="misc" />
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												onclick="return fnOpenMisc(name);">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										<div id="buttonsDisabled" style="display: none;">
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												disabled="true">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										</td>
									</tr>
									<tr bgcolor="<%= rowBgc %>">
										<td width="30" height="1" class="control" />
										<td width="320" height="1" class="control" colspan="4"><nested:text
											property="itemDescription" size="40" maxlength="70"
											styleClass="text" style="font-size:8pt;" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:text
											property="totalDiscount" size="9" maxlength="10"
											styleClass="textAmount" disabled="true" /></td>
										<td width="165" height="1" class="control"><nested:text
											property="amount" size="20" maxlength="25"
											styleClass="textAmount" disabled="true" /></td>
									    <td width="173.5" height="1" class="control">
									        <nested:text property="conversionFactor" size="5" maxlength="10" styleClass="textAmount" disabled="true" />
									    </td>
									    <td width="165" height="1" class="control"/>
									</tr>
								</nested:iterate>
							</table>
							</div>
							</td>
						</tr>
					</logic:equal>
					<logic:equal name="apReceivingItemEntryForm" property="type"
						value="ITEMS">
						<tr valign="top">
							<td width="575" height="185" colspan="4">
							<div align="center">
							<table border="1" cellpadding="0" cellspacing="0" width="577"
								height="47"
								bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
								bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
								<tr>
									<td width="575" height="1" colspan="8" class="gridTitle"
										bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>"><bean:message
										key="receivingItemEntry.gridTitle.RILDetails" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.lineNumber" /></td>
									<td width="120" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemName" /></td>
									<td width="100" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.location" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.quantity" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unit" /></td>
									<td width="145" height="1" class="gridHeader" colspan="2"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.unitCost" /></td>
									<td width="50" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.delete" /></td>
								</tr>
								<tr>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
									<td width="270" height="1" class="gridHeader" colspan="3"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDescription" /></td>
									<td width="60" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.itemDiscount" /></td>
									<td width="145" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.amount" /></td>
									<td width="145" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"><bean:message
										key="receivingItemEntry.prompt.conversionFactor" /></td>
									<td width="30" height="1" class="gridHeader"
										bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" />
								</tr>
								<%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
								<nested:iterate property="apRILList">
									<%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
									<nested:hidden property="isItemEntered" value="" />
									<nested:hidden property="isUnitEntered" value="" />
									<tr bgcolor="<%= rowBgc %>">
<%-- 									<nested:iterate property="tagList"> --%>
<%-- 										<nested:hidden property="propertyCode" /> --%>
<%-- 										<nested:hidden property="specs" /> --%>
<%-- 										<nested:hidden property="expiryDate" /> --%>
<%-- 										<nested:hidden property="serialNumber" /> --%>
<%-- 										<nested:hidden property="custodian" /> --%>
<%-- 									</nested:iterate> --%>
										<td width="30" height="1" class="control"><nested:text
											property="lineNumber" size="1" maxlength="4"
											styleClass="text" disabled="true" /></td>
										<td width="120" height="1" class="control"><nested:text
											property="itemName" size="12" maxlength="25"
											styleClass="textRequired" disabled="true" /> <nested:image
											property="lookupButton" src="images/lookup.gif"
											disabled="true" /></td>
										<td width="100" height="1" class="control"><nested:select
											property="location" styleClass="comboRequired"
											style="width:100;" disabled="true">
											<nested:options property="locationList" />
										</nested:select></td>
										<td width="50" height="1" class="control"><nested:text
											property="received" size="5" maxlength="10"
											styleClass="textRequired" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:select
											property="unit" styleClass="comboRequired" style="width:80;"
											disabled="true">
											<nested:options property="unitList" />
										</nested:select></td>
										<td width="145" height="1" class="control"><nested:text
											property="unitCost" size="17" maxlength="25"
											styleClass="textAmountRequired" disabled="true" /></td>
										<td width="50" height="1" class="control">
										<p align="center"><nested:checkbox
											property="deleteCheckbox" disabled="true" />
										</td>
									</tr>
									<tr bgcolor="<%= rowBgc %>">
										<td width="30" height="1" class="control" />
										<td width="270" height="1" class="control" colspan="3"><nested:text
											property="itemDescription" size="40" maxlength="70"
											styleClass="text" style="font-size:8pt;" disabled="true" /></td>
										<td width="60" height="1" class="control"><nested:text
											property="totalDiscount" size="9" maxlength="10"
											styleClass="textAmount" disabled="true" /></td>
										<td width="145" height="1" class="control"><nested:text
											property="amount" size="17" maxlength="10"
											styleClass="textAmount" disabled="true" /></td>
										<td width="100" align="center" height="1">
										<div id="buttons"><nested:hidden property="discount1" />
											<nested:hidden property="discount2" /> <nested:hidden
												property="discount3" /> <nested:hidden property="discount4" />
											<nested:hidden property="fixedDiscountAmount" /> <nested:submit
												property="discountButton" styleClass="mainButtonSmall"
												onclick="return fnOpenDiscount(name);">
												<bean:message key="button.discounts" />
											</nested:submit>
											<nested:hidden property="misc" />
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit
												property="miscButton" styleClass="mainButtonSmall"
												onclick="return fnOpenMisc(name);">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										<div id="buttonsDisabled" style="display: none;"><nested:submit
											property="discountButton" styleClass="mainButtonSmall"
											disabled="true">
											<bean:message key="button.discounts" />
											</nested:submit>
											<nested:equal property="isTraceMisc" value = "true">
											<nested:submit property="miscButton"
												styleClass="mainButtonSmall" disabled="true">
												<bean:message key="button.miscButton" />
											</nested:submit>
											</nested:equal>
										</div>
										</td>
									</tr>
								</nested:iterate>
							</table>
							</div>
							</td>
						</tr>
					</logic:equal>
					<tr>
						<td width="575" height="25" colspan="4">
						<div id="buttons">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="type" value="ITEMS">
							<logic:equal name="apReceivingItemEntryForm"
								property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton"
									styleClass="mainButtonMedium">
									<bean:message key="button.addLines" />
								</html:submit>
							</logic:equal>
							<logic:equal name="apReceivingItemEntryForm"
								property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton"
									styleClass="mainButtonMedium">
									<bean:message key="button.deleteLines" />
								</html:submit>
							</logic:equal>
						</logic:equal>
						</div>
						<div id="buttonsDisabled" style="display: none;">
						<p align="right"><logic:equal name="apReceivingItemEntryForm"
							property="type" value="ITEMS">
							<logic:equal name="apReceivingItemEntryForm"
								property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton"
									styleClass="mainButtonMedium" disabled="true">
									<bean:message key="button.addLines" />
								</html:submit>
							</logic:equal>
							<logic:equal name="apReceivingItemEntryForm"
								property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton"
									styleClass="mainButtonMedium" disabled="true">
									<bean:message key="button.deleteLines" />
								</html:submit>
							</logic:equal>
						</logic:equal>
						</div>
						</td>
					</tr>
				</logic:equal>
				<tr>
					<td width="575" height="10" colspan="4"
						bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
				</tr>
			</table>

			</td>
		</tr>
	</table>
</html:form>
<logic:equal name="apReceivingItemEntryForm" property="report"
	value="<%=Constants.STATUS_SUCCESS%>">
	<bean:define id="actionForm" name="apReceivingItemEntryForm"
		type="com.struts.ap.receivingitementry.ApReceivingItemEntryForm" />
	<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/apRepReceivingReportPrint.do?forward=1&receivingItemCode=<%=actionForm.getReceivingItemCode()%>&receivingReport=<%=actionForm.getReceivingReport()%>&inspectedReport=<%=actionForm.getInspectedReport()%>&releasedReport=<%=actionForm.getReleasedReport()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="apReceivingItemEntryForm" property="attachment"
	value="<%=Constants.STATUS_SUCCESS%>">
	<bean:define id="actionForm" name="apReceivingItemEntryForm"
		type="com.struts.ap.receivingitementry.ApReceivingItemEntryForm" />
	<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="apReceivingItemEntryForm" property="attachmentPDF"
	value="<%=Constants.STATUS_SUCCESS%>">
	<bean:define id="actionForm" name="apReceivingItemEntryForm"
		type="com.struts.ap.receivingitementry.ApReceivingItemEntryForm" />
	<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
</body>
</html>
