<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="recurringVoucherEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 
 
  
  function enterAmount()
  {
    
    disableButtons();
    enableInputControls();   	
	document.forms[0].elements["isAmountEntered"].value = true;
	document.forms[0].submit();
	   
  }
  
  function calculateAmountDue(name)
  {            
      document.forms[0].elements["amountDue"].value = document.forms[0].elements[name].value;
       
  }
  
  function voucherEnterSubmit()
  {            
      if (document.activeElement.name != 'amount') {
      
          return enterSubmit(event, new Array('saveButton'));
      
      } else {
      
          return true;
      
      }
       
  }
  
  var tempAmount = 0;
  
  function initializeTempDebit(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalDebit(name)
  {
        
      var debitAmount = 0;
      var totalDebit = (document.forms[0].elements["totalDebit"].value).replace(/,/g,'');
      var amount;
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          debitAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalDebit - (tempAmount - debitAmount))) {
      
	      amount = (1 * totalDebit - (1 * tempAmount - 1 * debitAmount)).toFixed(2);
	      document.forms[0].elements["totalDebit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = debitAmount;     
	      
	  }
            
  }  
  
    
  function initializeTempCredit(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalCredit(name)
  {
        
      var creditAmount = 0;
      var totalCredit = (document.forms[0].elements["totalCredit"].value).replace(/,/g,'');
      var amount;
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          creditAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalCredit - (tempAmount - creditAmount))) {
      
	      amount = (1 * totalCredit - (1 * tempAmount - 1 * creditAmount)).toFixed(2);
	      document.forms[0].elements["totalCredit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = creditAmount;     
	      
	  }
            
  }  
    
//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return voucherEnterSubmit();">
<html:form action="/apRecurringVoucherEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="recurringVoucherEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="apRecurringVoucherEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isAmountEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>

	     <logic:equal name="apRecurringVoucherEntryForm" property="enableFields" value="true">
              
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			             <logic:equal name="apRecurringVoucherEntryForm" property="showBatchName" value="true">
	            	    	<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.batchName"/>
                			</td>
	                		<td width="157" height="25" class="control">
    	               			<html:select property="batchName" styleClass="comboRequired" style="width:130;">
        	            			<html:options property="batchNameList"/>
            	       			</html:select>
                			</td>
   							</logic:equal>
   						</tr>
						 <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.name"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="name" size="25" maxlength="25" styleClass="textRequired"/>
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr> 	     
         				<tr>
							<logic:equal name="apRecurringVoucherEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
               	 			</td>
							</logic:equal>
							<logic:equal name="apRecurringVoucherEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
								<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
               	 			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
								<html:options property="paymentTermList"/>
                   				</html:select>
                			</td>                
         				</tr>
        		 		<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.amount"/>
                			</td>
                			<td width="128" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" onblur="addZeroes(name); return enterAmount();" onkeypress="javascript:if (event.keyCode == 13) return false;" onkeyup="formatAmount(name, (event)?event:window.event);"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.amountDue"/>
                			</td>
                			<td width="127" height="25" class="control">
                  			 	<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.totalDebit"/>
                			</td>
                			<td width="128" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.totalCredit"/>
                			</td>
                			<td width="127" height="25" class="control">
                  			 	<html:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="return enterAmount();">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" onchange="return enterAmount();">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="recurringVoucherEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>                
					</table>
					</div>				    
					 <div class="tabbertab" title="Schedule">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="reminderSchedule" styleClass="comboRequired">
			                       <html:options property="reminderScheduleList"/>
			                   </html:select>
			                </td>
			             </tr>
				         <tr>			                				         
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Notification">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser1"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser1" styleClass="comboRequired">
			                       <html:options property="notifiedUser1List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser2"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser2" styleClass="combo">
			                       <html:options property="notifiedUser2List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser3"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser3" styleClass="combo">
			                       <html:options property="notifiedUser3List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser4"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser4" styleClass="combo">
			                       <html:options property="notifiedUser4List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser5"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="notifiedUser3" styleClass="combo">
			                       <html:options property="notifiedUser5List"/>
			                   </html:select>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apRecurringVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>		       
		       </logic:equal>
		       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>		       
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apRecurringVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>		       
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="recurringVoucherEntry.gridTitle.RVDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.accountDescription"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apRVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:notEqual property="drClass" value="<%=Constants.AP_DR_CLASS_PAYABLE%>">
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name); addZeroes(name);" onkeyup="calculateTotalDebit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempCredit(name);"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:notEqual>
				    <nested:equal property="drClass" value="<%=Constants.AP_DR_CLASS_PAYABLE%>">
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onfocus="initializeTempCredit(name);" onblur="calculateAmountDue(name); calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateAmountDue(name); calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:equal>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apRecurringVoucherEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apRecurringVoucherEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>

	     <logic:equal name="apRecurringVoucherEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			             <logic:equal name="apRecurringVoucherEntryForm" property="showBatchName" value="true">
	            	    	<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.batchName"/>
                			</td>
	                		<td width="157" height="25" class="control">
    	               			<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
        	            			<html:options property="batchNameList"/>
            	       			</html:select>
                			</td>
   							</logic:equal>
						</tr>
						 <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.name"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="name" size="25" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr> 	     
         				<tr>
							<logic:equal name="apRecurringVoucherEntryForm" property="useSupplierPulldown" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
               	 			</td>
							</logic:equal>
							<logic:equal name="apRecurringVoucherEntryForm" property="useSupplierPulldown" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.supplier"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
               	 			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
								<html:options property="paymentTermList"/>
                   				</html:select>
                			</td>                
         				</tr>
        		 		<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.amount"/>
                			</td>
                			<td width="128" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.amountDue"/>
                			</td>
                			<td width="127" height="25" class="control">
                  			 	<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.totalDebit"/>
                			</td>
                			<td width="128" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringVoucherEntry.prompt.totalCredit"/>
                			</td>
                			<td width="127" height="25" class="control">
                  			 	<html:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="recurringVoucherEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>				    
					 <div class="tabbertab" title="Schedule">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="reminderSchedule" size="25" maxlength="25" styleClass="textRequired" disabled="true"/>
			                </td>
			             </tr>
				         <tr>			                				         
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Notification">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser1"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser1" styleClass="comboRequired" disabled="true">
			                       <html:options property="notifiedUser1List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser2"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser2" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser2List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser3"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser3" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser3List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser4"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser4" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser4List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringVoucherEntry.prompt.notifiedUser5"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="notifiedUser3" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser5List"/>
			                   </html:select>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apRecurringVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apRecurringVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>		       
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="recurringVoucherEntry.gridTitle.RVDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringVoucherEntry.prompt.accountDescription"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="apRVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif"  disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="apRecurringVoucherEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="apRecurringVoucherEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="apRecurringVoucherEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>     	       
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
	       // -->
</script>
</body>
</html>
