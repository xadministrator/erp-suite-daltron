<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="apAging.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apRepAging.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="290">
      <tr valign="top">
        <td width="187" height="290"></td> 
        <td width="581" height="290">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="290" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="apAging.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>

		 <tr>
		 <td width="575" height="10" colspan="4">
	     <div class="tabber">
	     <div class="tabbertab" title="Header">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	<td class="prompt" width="575" height="25" colspan="4">
			</td>
	     </tr>
		 <tr>
		 <td width="575" height="10" colspan="4">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.supplierCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="supplierCode" size="25" maxlength="25" styleClass="text"/>
	            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', 'isSupplierEntered');"/>				            
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.supplierType"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="supplierType" styleClass="combo">
			      <html:options property="supplierTypeList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.supplierClass"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="supplierClass" styleClass="combo">
			      <html:options property="supplierClassList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.date"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.agingBy"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="agingBy" styleClass="combo">
			      <html:options property="agingByList"/>
			   </html:select>
	         </td>
         </tr>
		 <tr>
			<td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.groupBy"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="groupBy" styleClass="combo">
			      <html:options property="groupByList"/>
			   </html:select>
	         </td>
			<td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.orderBy"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>
		 </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.includePaid"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includePaid"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.includeUnpostedTransaction"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:checkbox property="includeUnpostedTransaction"/>
	         </td>	         
         </tr>
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="apAging.prompt.viewType"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="viewType" styleClass="combo">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>         
         </tr>
         <tr>
            <td class="prompt" width="140" height="25">
				<bean:message key="arAgingSummary.prompt.currency"/>
			</td>
			<td width="128" height="25" class="control">
				<html:select property="currency" styleClass="combo">
					<html:options property="currencyList"/>
				</html:select>
			</td>         
         </tr>
		 </table>   
		 </div> 
		 <div class="tabbertab" title="Branch Map">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
		 <nested:iterate property="apRepBrAgingList">
		 <tr>
		 	  <td class="prompt" height="25">
			  		<nested:write property="brBranchCode"/>
			  </td>
	 		  <td class="prompt" height="25"> - </td>
			  <td class="prompt" width="140" height="25">
			  		<nested:write property="brName"/>
			  </td>
			  <td width="435" class="control">
			        <nested:checkbox property="branchCheckbox"/>
			  </td>
		 </tr>
		 </nested:iterate>
		 </table>
		 </div> 
		 </div><script>tabberAutomatic(tabberOptions)</script>
	     </td>
		 </tr>

	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
<logic:equal name="apRepAgingForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
