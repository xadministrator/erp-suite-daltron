<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="apRegister.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

  function enableSummarize2() {

	if (document.forms[0].groupBy.value == "SUPPLIER CODE") {
		document.forms[0].summarize.disabled=false;
	} else {
		document.forms[0].summarize.disabled=true;
		document.forms[0].summarize.checked=false;
	}
				
  }

  function enableSummarize() {

	if (document.forms[0].showEntries.checked == true) {
		document.forms[0].summarize.disabled=false;
	} else {
		document.forms[0].summarize.disabled=true;
		document.forms[0].summarize.checked=false;
	}
				
  }
  
//Done Hiding--> 
</script> 

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apRepApRegister.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="apRegister.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>

 		 <tr>
		 <td width="575" height="10" colspan="4">
	     <div class="tabber">
	     <div class="tabbertab" title="Header">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	<td class="prompt" width="575" height="25" colspan="4">
			</td>
	     </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.supplierCode"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:text property="supplierCode" size="20" maxlength="20" styleClass="text"/>
	            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', 'isSupplierEntered');"/>				            
	         </td>
                 
                 
                 <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.voucherBatch"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="voucherBatch" styleClass="combo">
                        <html:options property="voucherBatchList"/>
                    </html:select>
	         </td>
            </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.type"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="type" styleClass="combo">
			      <html:options property="typeList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.supplierClass"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="supplierClass" styleClass="combo">
			      <html:options property="supplierClassList"/>
			   </html:select>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.dateFrom"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.dateTo"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	         </td>	         
         </tr> 
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.documentNumberFrom"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="documentNumberFrom" size="25" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.documentNumberTo"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:text property="documentNumberTo" size="25" maxlength="25" styleClass="text"/>
	         </td>	         
         </tr>                  
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.groupBy"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="groupBy" styleClass="combo" onclick="return enableSummarize2();">
			      <html:options property="groupByList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.orderBy"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>
         </tr>
		 <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.paymentStatus"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="paymentStatus" styleClass="combo">
			      <html:options property="paymentStatusList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.viewType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired" onchange="showDigiBank()">
                        <html:options property="viewTypeList"/>
                     </html:select>
	         </td>	         
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.includedUnposted"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includedUnposted"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.includedPayments"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:checkbox property="includedPayments"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.showEntries"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="showEntries" onclick="return enableSummarize();"/>
	         </td>
			 <td class="prompt" width="140" height="25">
	            <bean:message key="apCheckRegister.prompt.summarize"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:checkbox property="summarize" disabled="true"/>
			 </td>
	         
         </tr>
            <tr>
               <td class="prompt" width="140" height="25">
                   <bean:message key="apCheckRegister.prompt.includeDirectCheck"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:checkbox property="includeDirectCheck"/>
               </td>
            </tr>
            
        </table>  
        <!-- DIGIBANK FORM !-->   
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="180" id="tableDigiBank" 
                                     bgcolor="<%=Constants.TXN_MAIN_BGC%>">

            <tr>
                 <td class="prompt" width="180" height="25" colspan="4" >
                     <b><bean:message key="apRegister.prompt.digiBankHeader"/></b>
                </td>
            </tr>
            <tr>
                
                <td class="prompt" width="180" height="25">
	            <bean:message key="apRegister.prompt.digiBankPostingDate"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:text property="digiBankPostingDate" size="20" maxlength="20" styleClass="textRequired"/> 
	         </td>
                 
            </tr>
            <tr>
                <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.digiBankFundingAccountNumber"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:text property="digiBankFundingAccountNumber" size="20" maxlength="20" styleClass="textRequired"/> 
	         </td>
                
            </tr>
            
           
            <tr>
                
                <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.digiBankBatchNumber"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:text property="digiBankBatchNumber" size="20" maxlength="20" styleClass="text"/> 
	         </td>
                  
            </tr>
            
            <tr>
                
                <td class="prompt" width="140" height="25">
	            <bean:message key="apRegister.prompt.enableApplyPayment"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:checkbox property="enablePayment" onchange="showEnablePayment()"/>
	         </td>
                  
            </tr>
            
          
            
        </table >     
        <!-- !--> 
        
        
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="180" id="tablePayment" 
                                     bgcolor="<%=Constants.TXN_MAIN_BGC%>">
              <tr >
                
                <td id="refernceNumberId" class="prompt" width="180" height="25"  colspan="4">
	            <bean:message key="apRegister.prompt.paymentReferenceNumber"/>
	         </td>
	         <td width="128" height="25" class="control">
	             <html:text property="paymentReferenceNumber" size="20" maxlength="20" styleClass="textRequired"/>
	         </td>
                  
            </tr>
            
            <tr>
                
                <td id="bankAccountId" class="prompt" width="140" height="25"  colspan=4">
	            <bean:message key="apRegister.prompt.paymentBankAccount"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="bankAccount" styleClass="comboRequired">
                        <html:options property="bankAccountList"/>
                     </html:select>
	         </td>	 
            </tr>
            
            <tr>
                
                <td id="bankAccountId" class="prompt" width="140" height="25"  colspan=4">
	            <bean:message key="apRegister.prompt.isDraftPayment"/>
	         </td>
	          <td width="128" height="25" class="control" >
	            <html:checkbox property="isDraftPayment" />
	         </td>	 
            </tr>
            
            
        </table>
        
        
               
		 </div> 
		 <div class="tabbertab" title="Branch Map">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
		 <nested:iterate property="apRepBrApRgstrList">
		 <tr>
			  <td class="prompt" height="25">
			  		<nested:write property="brBranchCode"/>
			  </td> 		
			  <td class="prompt" height="25"> - </td>
			  <td class="prompt" width="140" height="25">
			  		<nested:write property="brName"/>
			  </td>
			  <td width="435" class="control">
			        <nested:checkbox property="branchCheckbox"/>
			  </td>
		 </tr>
		 </nested:iterate>
		 </table>
		 </div> 
		 </div><script>tabberAutomatic(tabberOptions)</script>
	     </td>
		 </tr>

	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
                    </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>

<script language=JavaScript type=text/javascript>
  <!--
    showDigiBank();
    showEnablePayment();
    function showDigiBank() {
        
        d = document.forms[0].elements["viewType"].value;
        if(d=="DIGIBANK"){
            document.getElementById("tableDigiBank").style.visibility = "visible";
            document.getElementById("tableDigiBank").style.display = "inline";
        }else{
           document.getElementById("tableDigiBank").style.visibility = "hidden";
           document.getElementById("tableDigiBank").style.display = "none";
           
           document.forms[0].elements["enablePayment"].checked = false;
           document.getElementById("tablePayment").style.visibility = "hidden";
           document.getElementById("tablePayment").style.display = "none";
        } 
     
    }
    
    function showEnablePayment() {
             
        d = document.forms[0].elements["enablePayment"].checked;
  
        if(d){
            document.getElementById("tablePayment").style.visibility = "visible";
            document.getElementById("tablePayment").style.display = "inline";
        }else{
           document.getElementById("tablePayment").style.visibility = "hidden";
           document.getElementById("tablePayment").style.display = "none";
        } 
      
        
    }
   // -->
</script>

<logic:equal name="apRepApRegisterForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
  
     
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");

    
    //-->
</script>
</logic:equal>
<logic:equal name="apRepApRegisterForm" property="statusReport" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
 
    win2 = window.open("<%=request.getContextPath()%>/cmnStatus.jsp","_blank");
   
    //-->
</script>
</logic:equal>


</body>
</html>
