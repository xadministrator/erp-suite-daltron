<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="receivingItemsRep.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apRepReceivingItems" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  		 <bean:message key="receivingItemsRep.title"/>
			</td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
		</tr>
	     <html:hidden property="isSupplierEntered" value=""/>
	     <html:hidden property="isItemEntered" value=""/>
	     </tr>
		 <tr>
		 <td width="575" height="10" colspan="4">
	     <div class="tabber">
	     <div class="tabbertab" title="Header">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	<td class="prompt" width="575" height="25" colspan="4">
			</td>
	     </tr>
	     <tr>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.dateFrom"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.dateTo"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>	         
         </tr> 
	     <tr>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.itemName"/>
	         </td>
	         <td width="127" height="25" class="control" >
	            <html:text property="itemName" size="15" maxlength="25" styleClass="text"/>
				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvItemLookup('itemName', '', 'isItemEntered');"/>
	         </td>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.category"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:select property="category" styleClass="combo">
					<html:options property="categoryList"/>
				</html:select>
	         </td>        
         </tr>
	     <tr>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.supplierCode"/>
	         </td>
	         <td width="127" height="25" class="control" >
	            <html:text property="supplierCode" size="12" maxlength="25" styleClass="text"/>
				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', 'isSupplierEntered');"/>
	         </td>	       			
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.location"/>
	         </td>
	         <td width="128" height="25" class="control" >
	            <html:select property="location" styleClass="combo">
					<html:options property="locationList"/>
				</html:select>
	         </td>        
         </tr>
		 <tr>
			<td class="prompt" width="160" height="25">
				<bean:message key="receivingItemsRep.prompt.poReceivingDocumentNumberFrom"/>
			</td>
			<td width="127" height="25" class="control">
			    <html:text property="poReceivingDocumentNumberFrom" size="15" maxlength="25" styleClass="text"/>
			</td>
			<td class="prompt" width="160" height="25">
			    <bean:message key="receivingItemsRep.prompt.poReceivingDocumentNumberTo"/>
			</td>
			<td width="128" height="25" class="control">
				<html:text property="poReceivingDocumentNumberTo" size="15" maxlength="25" styleClass="text"/>
			</td>	
		</tr>
		<tr>
			<td class="prompt" width="150" height="25">
				<bean:message key="receivingItemsRep.prompt.voucherDocumentNumberFrom"/>
			</td>
			<td width="127" height="25" class="control">
			    <html:text property="voucherDocumentNumberFrom" size="15" maxlength="25" styleClass="text"/>
			</td>
			<td class="prompt" width="150" height="25">
			    <bean:message key="receivingItemsRep.prompt.voucherDocumentNumberTo"/>
			</td>
			<td width="128" height="25" class="control">
				<html:text property="voucherDocumentNumberTo" size="15" maxlength="25" styleClass="text"/>
			</td>	         
		</tr>   
         <tr>
			<td class="prompt" width="150" height="25">
				<bean:message key="receivingItemsRep.prompt.checkDocumentNumberFrom"/>
			</td>
			<td width="127" height="25" class="control">
			    <html:text property="checkDocumentNumberFrom" size="15" maxlength="25" styleClass="text"/>
			</td>
			<td class="prompt" width="150" height="25">
			    <bean:message key="receivingItemsRep.prompt.checkDocumentNumberTo"/>
			</td>
			<td width="128" height="25" class="control">
				<html:text property="checkDocumentNumberTo" size="15" maxlength="25" styleClass="text"/>
			</td>	         
		</tr>   
          
         <tr>
			<td class="prompt" width="150" height="25">
				<bean:message key="receivingItemsRep.prompt.groupBy"/>
			</td>
			<td width="127" height="25" class="control">
			    <html:select property="groupBy" styleClass="combo">
			    <html:options property="groupByList"/>
				</html:select>
			 </td>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.orderBy"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="orderBy" styleClass="comboRequired">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	                
         </tr>
         <tr>
			<td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.includeUnposted"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includeUnposted" styleClass="checkBox"/>
	         </td>
	         <td class="prompt" width="150" height="25">
	            <bean:message key="receivingItemsRep.prompt.viewType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>
		 </tr>
		<tr>
			 <td class="prompt" width="150" height="25">
		         <bean:message key="receivingItemsRep.prompt.includedPoReceiving"/>
		     </td>
		     <td width="127" height="25" class="control">
		         <html:checkbox property="includedPoReceiving"/>
		     </td>
		     <td class="prompt" width="150" height="25">
		         <bean:message key="receivingItemsRep.prompt.includedDirectCheck"/>
		     </td>
		     <td width="128" height="25" class="control">
		         <html:checkbox property="includedDirectCheck"/>
		     </td>
		</tr>
		<tr>
			 <td class="prompt" width="150" height="25">
		         <bean:message key="receivingItemsRep.prompt.includedVoucher"/>
		     </td>
		     <td width="127" height="25" class="control">
		         <html:checkbox property="includedVoucher"/>
		     </td>
		</tr>
		</table>   
		 </div> 
		 <div class="tabbertab" title="Branch Map">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
		 <nested:iterate property="apRepBrRcvngItmsList">
		 <tr>
		 	<td class="prompt" height="25">
			  		<nested:write property="brBranchCode"/>
		  	</td>
			  <td class="prompt" height="25"> - </td>
			  <td class="prompt" width="140" height="25">
			  		<nested:write property="brName"/>
			  </td>
			  <td width="435" class="control">
			        <nested:checkbox property="branchCheckbox"/>
			  </td>
		 </tr>
		 </nested:iterate>
		 </table>
		 </div> 
		 </div><script>tabberAutomatic(tabberOptions)</script>
	     </td>
		 </tr>

	     <tr>
	        <td width="575" height="45" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
	          
	          
	       
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
<logic:equal name="apRepReceivingItemsForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
