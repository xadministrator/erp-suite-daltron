<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>

<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>

<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="voucherEditList.title"/>
  </title>
   <link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1" type="text/css">
</head>
<frameset rows="80,*" framespacing="0" border="0" frameborder="0">
   <frame name="form" marginwidth="0" marginheight="0" scrolling="no" noresize src="apRepVoucherEditListForm.jsp">
   <frame name="pdf"  marginwidth="0" marginheight="0" scrolling="no" noresize src="cmnReport.jsp">
   <body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
      <p>This page uses frames, but your browser doesn't support them.</p>
   </body>
</frameset>
</html>
