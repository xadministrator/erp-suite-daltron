<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="supplierEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/apSupplierEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="410">
      <tr valign="top">
        <td width="187" height="410"></td>
        <td width="581" height="410">
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="410"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="supplierEntry.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="apSupplierEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>
	        </td>
	     </tr>
	     <html:hidden property="isSupplierClassEntered" value=""/>
	     <html:hidden property="isSupplierTypeEntered" value=""/>
	     <logic:equal name="apSupplierEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="supplierEntry.prompt.supplierCode"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="supplierCode" size="25" maxlength="100" styleClass="textRequired"/>
	        </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="supplierEntry.prompt.enable"/>
                </td>
                <td width="147" height="25" class="control">
                   <html:checkbox property="enable"/>
                </td>
            </tr>



		  <logic:equal name="apSupplierEntryForm" property="autoGenerateSupplierCode" value="true">
		  	<logic:equal name="apSupplierEntryForm" property="isNew" value="true">
		  		<tr>
		  			<td class="prompt" width="140" height="25">
	           			<bean:message key="supplierEntry.prompt.nextSupplierCode"/>
	        		</td>
					<td class="prompt" width="435" height="25" colspan="3" style="text-indent:0.000000pt; font-size:10pt; font-weight:bold;">
						<bean:write name="apSupplierEntryForm" property="nextSupplierCode"/>
	        		</td>
		  		</tr>
		  	</logic:equal>
		  </logic:equal>
	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="supplierEntry.prompt.supplierName"/>
            </td>
		    <td width="155" height="25" class="control" colspan="3">
		       <html:text property="supplierName" size="30" maxlength="100" styleClass="textRequired"/>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.supplierClass"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="supplierClass" styleClass="comboRequired" onchange="return enterSelect('supplierClass','isSupplierClassEntered');">
               <html:options property="supplierClassList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.paymentTerm"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentTerm" styleClass="comboRequired">
               <html:options property="paymentTermList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.supplierType"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="supplierType" styleClass="combo" onchange="return enterSelect('supplierType','isSupplierTypeEntered');">
               <html:options property="supplierTypeList"/>
            </html:select>
		    </td>

		  </tr>

		  <tr>
		  <td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.bankAccount"/>
			</td>
			<td width="147" height="25" class="control" colspan="3">
			   <html:select property="bankAccount" styleClass="comboRequired">
	               <html:options property="bankAccountList"/>
	           </html:select>
			</td>

		  </tr>
		  <tr>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.tinNumber"/>
			</td>
			<td width="148" height="25" class="control">
			   <html:text property="tinNumber" size="25" maxlength="100" styleClass="text"/>
			</td>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.remarks"/>
			</td>
			<td width="147" height="25" class="control">
			   <html:text property="remarks" size="20" maxlength="100" styleClass="text"/>
			</td>
		  </tr>

                  <tr>
                    <td class="prompt" width="140" height="25">
                       <bean:message key="supplierEntry.prompt.accountNumber"/>
                    </td>
                    <td width="148" height="25" class="control">
                       <html:text property="accountNumber" size="25" maxlength="100" styleClass="text"/>
                    </td>

                    </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.templateName"/>
            </td>
            <td width="148" height="25" class="control">
            <html:select property="templateName" styleClass="combo" style="width:150;">
               <html:options property="templateList"/>
            </html:select>
		    </td>
		  </tr>



		 <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Address Info">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
		          <tr>
				    <td class="prompt" width="575" height="20" colspan="4">
		            </td>
		          </tr>
		          <tr>
				    <td class="prompt" width="140" height="25">
				    <bean:message key="supplierEntry.prompt.address"/>
				    </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="address" cols="20" rows="4" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.city"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:text property="city" size="20" maxlength="100" styleClass="text"/>
				    </td>
		          </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.stateProvince"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="stateProvince" size="25" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.postalCode"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="postalCode" size="10" maxlength="10" styleClass="text"/>
				    </td>
				 </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.country"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="country" size="25" maxlength="100" styleClass="text"/>
				    </td>
				 </tr>
				</table>
			    </div>
			    <div class="tabbertab" title="Contact Info">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.contact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="contact" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.phone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="phone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			    </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.alternateContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="alternateContact" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.alternatePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternatePhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
				  </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.fax"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="fax" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.email"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="email" size="20" maxlength="30" styleClass="text"/>
				    </td>
			      </tr>
				</table>
			    </div>
			    <div class="tabbertab" title="Accounts">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.payableAccount"/>
		            </td>
				    <td width="435" height="25" class="control" colspan="3">
		               <html:text property="payableAccount" size="30" maxlength="255" styleClass="textRequired"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('payableAccount','payableDescription');"/>
				    </td>
				    </tr>
				    <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.payableDescription"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="payableDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
					    </td>
				    </tr>
				    <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.expenseAccount"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="expenseAccount" size="30" maxlength="255" styleClass="textRequired"/>
			               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('expenseAccount','expenseDescription');"/>
					    </td>
				   </tr>
				   <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.expenseDescription"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="expenseDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
					    </td>
				    </tr>
				   </table>
			    </div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="575" height="25" colspan="4">
						</td>
					</tr>
					<nested:iterate property="apBSplList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="brName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="supplierEntry.prompt.payableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchPayableAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchPayableAccount','branchPayableDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="supplierEntry.prompt.payableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3 align="left">
						   <nested:text property="branchPayableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="supplierEntry.prompt.expenseAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchExpenseAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchExpenseAccount','branchExpenseDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="supplierEntry.prompt.expenseDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchExpenseDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
		            </nested:iterate>
				</table>
		        </div>
		        </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
	     <logic:equal name="apSupplierEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="supplierEntry.prompt.supplierCode"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="supplierCode" size="25" maxlength="100" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="supplierEntry.prompt.enable"/>
            </td>
		    <td width="147" height="25" class="control" disabled="true">
		       <html:checkbox property="enable"/>
		    </td>
            </tr>


	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="supplierEntry.prompt.supplierName"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="supplierName" size="50" maxlength="100" styleClass="textRequired" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.supplierClass"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="supplierClass" styleClass="comboRequired" disabled="true">
               <html:options property="supplierClassList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.paymentTerm"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentTerm" styleClass="comboRequired" disabled="true">
               <html:options property="paymentTermList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="supplierEntry.prompt.supplierType"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="supplierType" styleClass="combo" disabled="true">
               <html:options property="supplierTypeList"/>
            </html:select>
		    </td>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.bankAccount"/>
			</td>
			<td width="147" height="25" class="control">
			   <html:select property="bankAccount" styleClass="comboRequired" disabled="true">
	               <html:options property="bankAccountList"/>
	           </html:select>
			</td>
		  </tr>
		  <tr>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.tinNumber"/>
			</td>
			<td width="148" height="25" class="control">
			   <html:text property="tinNumber" size="25" maxlength="100" styleClass="text" disabled="true"/>
			</td>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="supplierEntry.prompt.remarks"/>
			</td>
			<td width="147" height="25" class="control">
			   <html:text property="remarks" size="20" maxlength="100" styleClass="text" disabled="true"/>
			</td>
		  </tr>


		    <tr>
                        <td class="prompt" width="140" height="25">
                           <bean:message key="supplierEntry.prompt.accountNumber"/>
                        </td>
                        <td width="148" height="25" class="control">
                           <html:text property="accountNumber" size="25" maxlength="100" styleClass="text" disabled="true"/>
                        </td>

                    </tr>


		 <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Address Info">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
		          <tr>
				    <td class="prompt" width="575" height="20" colspan="4">
		            </td>
		          </tr>
		          <tr>
				    <td class="prompt" width="140" height="25">
				    <bean:message key="supplierEntry.prompt.address"/>
				    </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="address" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.city"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:text property="city" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
		          </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.stateProvince"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="stateProvince" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.postalCode"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="postalCode" size="10" maxlength="10" styleClass="text" disabled="true"/>
				    </td>
				 </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.country"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="country" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				 </tr>
				</table>
			    </div>
                <div class="tabbertab" title="Contact Info">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.contact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="contact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.phone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="phone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.alternateContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="alternateContact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.alternatePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternatePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.fax"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="fax" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.email"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="email" size="20" maxlength="30" styleClass="text" disabled="true"/>
				    </td>
			      </tr>
				</table>
			    </div>
			    <div class="tabbertab" title="Accounts">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="supplierEntry.prompt.payableAccount"/>
		            </td>
				    <td width="435" height="25" class="control" colspan="3">
		               <html:text property="payableAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('payableAccount','');" disabled="true"/>
				    </td>
				   </tr>
				   <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.payableDescription"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="payableDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
					    </td>
				   </tr>
				   <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.expenseAccount"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="expenseAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
			               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('expenseAccount');" disabled="true"/>
					    </td>
				   </tr>
				   <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="supplierEntry.prompt.expenseDescription"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="expenseDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
					    </td>
				   </tr>
				</table>
			    </div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="575" height="25" colspan="4">
						</td>
					</tr>
					<nested:iterate property="apBSplList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="brName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="supplierEntry.prompt.payableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchPayableAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchPayableAccount','branchPayableDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="supplierEntry.prompt.payableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3 align="left">
						   <nested:text property="branchPayableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="supplierEntry.prompt.expenseAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchExpenseAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchExpenseAccount','branchExpenseDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="supplierEntry.prompt.expenseDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchExpenseDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
		            </nested:iterate>
				</table>
		        </div>
		        </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
	     <tr>
	        <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="apSupplierEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="apSupplierEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
            </td>
	     </tr>
	     <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["supplierCode"] != null &&
        document.forms[0].elements["supplierCode"].disabled == false)
        document.forms[0].elements["supplierCode"].focus()
	       // -->
</script>
</body>
</html>
