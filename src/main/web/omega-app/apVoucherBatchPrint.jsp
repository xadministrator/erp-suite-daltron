<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="voucherBatchPrint.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function confirmPrint()
{
   if(confirm("Are you sure you want to Print selected records?")) return true;
      else return false;     
      
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["apVBPList[" + i + "].print"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["apVBPList[" + i + "].print"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}

var documentWindow;

function showAdDocument(name) 
{

   var property = name.substring(0,name.indexOf(".")); 
      
   var documentCode = document.forms[0].elements[property + ".voucherCode"].value;
   var debitMemo = document.forms[0].elements[property + ".debitMemo"].value;        
   var wihe = 'width=' + screen.availWidth + ',height=' + screen.availHeight;
      
   if (debitMemo == 'false') {

       documentWindow = window.open("apVoucherEntry.do?forward=1&voucherCode=" + documentCode, "apVoucherEntry", "resizable,scrollbars,status,titlebar,screenX=0,screenY=0,left=0,top=0," + wihe);     

   } else {

       documentWindow = window.open("apDebitMemoEntry.do?forward=1&debitMemoCode=" + documentCode, "apDebitMemoEntry", "resizable,scrollbars,status,titlebar,screenX=0,screenY=0,left=0,top=0," + wihe);     

   }

   return false;

}

function refreshPage()
{
   if (documentWindow && documentWindow.closed) {
   
       window.location = window.location.protocol + '//' + window.location.host + window.location.pathname + "?refresh=1";
   
   }

}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));" onfocus="refreshPage();">
<html:form action="/apVoucherBatchPrint.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="voucherBatchPrint.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="apVoucherBatchPrintForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>

	         <logic:equal name="apVoucherBatchPrintForm" property="showBatchName" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="voucherBatchPrint.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
	         </tr>
	         </logic:equal>
	         	
	         <tr>
	         <logic:equal name="apVoucherBatchPrintForm" property="useSupplierPulldown" value="true">		
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.supplierCode"/>
                </td>
		        <td width="390" height="25" class="control" colspan="3">
	               <html:select property="supplierCode" styleClass="combo">
                      <html:options property="supplierCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
	         </logic:equal>	
	         <logic:equal name="apVoucherBatchPrintForm" property="useSupplierPulldown" value="false">		
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchPrint.prompt.supplierCode"/>
                </td>
		        <td width="390" height="25" class="control" colspan="3">
	               <html:text property="supplierCode" size="15" maxlength="25" styleClass="text" readonly="true"/>                                       
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
	         </logic:equal>	
             </tr>
             
             <tr>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.debitMemo"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:checkbox property="debitMemo"/>
		        </td>
		        <td class="prompt" width="160" height="25">
	               <bean:message key="voucherBatchPrint.prompt.voucherVoid"/>
	            </td>
			    <td width="127" height="25" class="control">
			       <html:checkbox property="voucherVoid"/>
			    </td>
             </tr>
                          
	         <tr>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.dateFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.dateTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>		        
	         </tr>
	         
	         <tr>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.documentNumberFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.documentNumberTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>		        
	         </tr>
	         	         
	         <tr>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.referenceNumber"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="referenceNumber" size="20" maxlength="20" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchPrint.prompt.approvalStatus"/>
                </td>
		        <td width="100" height="25" class="control">
	               <html:select property="approvalStatus" styleClass="combo">
	                  <html:options property="approvalStatusList"/>
		           </html:select>
		        </td>		        
	         </tr>

	         <tr>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.currency"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.posted"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:select property="posted" styleClass="combo">
			        <html:options property="postedList"/>
			      </html:select>
		       </td>
	         </tr>	                      	         	         
	         
	         <tr>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.paymentStatus"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="paymentStatus" styleClass="combo">
			        <html:options property="paymentStatusList"/>
			      </html:select>
		       </td>	         
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.orderBy"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>	 		               
	        </tr>	     
	       
	        <tr>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.maxRows"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchPrint.prompt.queryCount"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       </td>
	       </tr>
	       
	       
	       
	       
	       
	              
	       <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="200" height="50">
			             <div id="buttons">
			             <logic:equal name="apVoucherBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="apVoucherBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="375" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.print"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.print"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>		
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="voucherBatchPrint.gridTitle.VBPDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchPrint.prompt.date"/>
			       </td>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchPrint.prompt.referenceNumber"/>
			       </td>			       
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchPrint.prompt.documentNumber"/>
			       </td>			       			       
			       <td width="234" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchPrint.prompt.supplierCode"/>
			       </td>
			       <td width="264" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchPrint.prompt.amountDue"/>
			       </td>			       
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apVBPList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <nested:hidden property="voucherCode"/>
			          <nested:hidden property="debitMemo"/>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>			             			             
			             <td width="234" height="1" class="gridLabel">
			                <nested:write property="supplierCode"/>
			             </td>
			             <td width="164" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>	
			              <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
				          <td width="50" align="center" height="1">
				             <nested:checkbox property="print"/>
				          </td>
				          </logic:equal>
				          <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
				          <td width="50" align="center" height="1">
				             <nested:checkbox property="print" disabled="true"/>
				          </td>
				          </logic:equal>	
				          <td width="50" height="1" class="gridLabel">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
					         <bean:message key="button.open"/>
					        </nested:submit>
					        </div>
					        <div id="buttonsDisabled" style="display: none;">
					        <nested:submit property="openButton" disabled="true" styleClass="gridButton">
					         <bean:message key="button.open"/>
					        </nested:submit>
					        </div>
			             </td>		             
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="apVoucherBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apVBPList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <nested:hidden property="voucherCode"/>
			          <nested:hidden property="debitMemo"/>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchPrint.prompt.supplierCode"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="supplierCode"/>
			             </td>
			              <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
				          <td width="49" align="center" height="1">
			                 <nested:checkbox property="print"/>
				          </td>
				          </logic:equal>
				          <logic:equal name="apVoucherBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
				          <td width="49" align="center" height="1">
			                 <nested:checkbox property="print" disabled="true"/>
				          </td>
				          </logic:equal>
				          <td width="100" height="1" class="gridLabel">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
					         <bean:message key="button.open"/>
					        </nested:submit>
					        </div>
					        <div id="buttonsDisabled" style="display: none;">
					        <nested:submit property="openButton" disabled="true" styleClass="gridButton">
					         <bean:message key="button.open"/>
					        </nested:submit>
					        </div>
			             </td>	
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchPrint.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="voucherBatchPrint.prompt.documentNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel" colspan="2">
			                <nested:write property="documentNumber"/>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchPrint.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="voucherBatchPrint.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum" colspan="2">
			                <nested:write property="amountDue"/>
			             </td>			             
			          </tr>
					  <tr bgcolor="<%= rowBgc %>">
		                 <td width="175" height="1" class="gridHeader">
		                     <bean:message key="voucherBatchPrint.prompt.amountPaid"/>
		                 </td>
		                 <td width="350" height="1" class="gridLabelNum" colspan="1">
		                     <nested:write property="amountPaid"/>
		                 </td>
		                 <td width="369" height="1" class="gridLabel" colspan="3"></td>
		              </tr>			          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
<logic:equal name="apVoucherBatchPrintForm" property="voucherReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apVoucherBatchPrintForm" type="com.struts.ap.voucherbatchprint.ApVoucherBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
    win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='apVoucherBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepVoucherPrint.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getVoucherCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='voucherCode[" +<%=j%>+ "]' value='<%=actionForm.getVoucherCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>");
	
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>
<logic:equal name="apVoucherBatchPrintForm" property="debitMemoReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apVoucherBatchPrintForm" type="com.struts.ap.voucherbatchprint.ApVoucherBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='apVoucherBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepDebitMemoPrint.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getVoucherCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='voucherCode[" +<%=j%>+ "]' value='<%=actionForm.getVoucherCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>");
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>
<logic:equal name="apVoucherBatchPrintForm" property="voucherEditListReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="apVoucherBatchPrintForm" type="com.struts.ap.voucherbatchprint.ApVoucherBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='apVoucherBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/apRepVoucherEditList.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getVoucherCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='voucherCode[" +<%=j%>+ "]' value='<%=actionForm.getVoucherCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>");
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>
</body>
</html>
