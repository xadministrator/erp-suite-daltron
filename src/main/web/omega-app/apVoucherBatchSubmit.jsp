<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="voucherBatchSubmit.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmSubmit()
{
   if(confirm("Are you sure you want to Submit selected records?")) return true;
      else return false;
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["apVBSList[" + i + "].submit"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["apVBSList[" + i + "].submit"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/apVoucherBatchSubmit.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="voucherBatchSubmit.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="apVoucherBatchSubmitForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>
	         <logic:equal name="apVoucherBatchSubmitForm" property="showBatchName" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="voucherBatchSubmit.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
	         </tr>
	         </logic:equal>	
	         <tr>
	         <logic:equal name="apVoucherBatchSubmitForm" property="useSupplierPulldown" value="true">
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.supplierCode"/>
                </td>
		        <td width="415" height="25" class="control" colspan="3">
	               <html:select property="supplierCode" styleClass="combo">
                      <html:options property="supplierCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
	         </logic:equal>	
	         <logic:equal name="apVoucherBatchSubmitForm" property="useSupplierPulldown" value="false">
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.supplierCode"/>
                </td>
		        <td width="415" height="25" class="control" colspan="3">
	               <html:text property="supplierCode" size="15" maxlength="25" styleClass="text" readonly="true"/>                                       
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
		        </td>
	         </logic:equal>	
             </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
	               <bean:message key="voucherBatchSubmit.prompt.debitMemo"/>
	            </td>
			    <td width="415" height="25" class="control" colspan="3">
			       <html:checkbox property="debitMemo"/>
			    </td>
             </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.dateFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.dateTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>		        
	         </tr>             
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.documentNumberFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.documentNumberTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>		        
	         </tr>             	         
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="voucherBatchSubmit.prompt.referenceNumber"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchSubmit.prompt.currency"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>
	         </tr>
	         <tr>
		       
		       <td class="prompt" width="185" height="25">
		          <bean:message key="voucherBatchSubmit.prompt.orderBy"/>
		       </td>
		       <td width="387" height="25" class="control" colspan="3">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>
	         </tr>	   
	         <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="voucherBatchSubmit.prompt.maxRows"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="voucherBatchSubmit.prompt.queryCount"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       </td>
	         </tr>                      	         	         	         
	       <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="apVoucherBatchSubmitForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="apVoucherBatchSubmitForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="apVoucherBatchSubmitForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="submitButton" styleClass="mainButton" onclick="return confirmSubmit();">
				         <bean:message key="button.submit"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>				         
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="submitButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.submit"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>				         	
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="voucherBatchSubmit.gridTitle.VBSDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchSubmit.prompt.date"/>
			       </td>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchSubmit.prompt.referenceNumber"/>
			       </td>			       
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchSubmit.prompt.documentNumber"/>
			       </td>			       			       
			       <td width="234" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchSubmit.prompt.supplierCode"/>
			       </td>
			       <td width="264" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="voucherBatchSubmit.prompt.amountDue"/>
			       </td>			       
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apVBSList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>			             			             
			             <td width="234" height="1" class="gridLabel">
			                <nested:write property="supplierCode"/>
			             </td>
			             <td width="164" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>	
			             <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="submit"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="submit" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="apVoucherBatchSubmitForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="apVBSList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchSubmit.prompt.supplierCode"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="supplierCode"/>
			             </td>
			             <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="submit"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="apVoucherBatchSubmitForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="submit" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchSubmit.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="voucherBatchSubmit.prompt.documentNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="voucherBatchSubmit.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="voucherBatchSubmit.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			          </tr>		          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null)
             document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
</body>
</html>
