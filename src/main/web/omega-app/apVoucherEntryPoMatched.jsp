 		 <%@ page language="java" import="com.struts.util.Constants" %>
		 <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
 		 <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
		 <%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
		 <%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
		 <% String apVPOListSize = null; %>
		 <script type="text/javascript">
		 
		 function checkIssueAllPo(name,size){
			 var property = name.substring(0,name.indexOf("."));
			 var listName = property.substring(0, property.indexOf("["))
			 
			 var index = name.charAt(10);
			 var checked = document.forms[0].elements[name].checked;
			 var i=0;
			 var docNum = document.forms[0].elements["apVPOList[" + index + "].docNumber"].value;
			if(checked==true){
				document.forms[0].elements["poNumber"].value=document.forms[0].elements["apVPOList[" + index + "].poNumber"].value;
				
				 document.forms[0].elements["conversionRate"].value = document.forms[0].elements["apVPOList[" + index + "].poConversionRate"].value;
				 document.forms[0].elements["conversionDate"].value = document.forms[0].elements["apVPOList[" + index + "].poConversionDate"].value;
				 
			 for(i=0; i<size.value; i++){
				 //alert(docNum);
				 docNum1 = document.forms[0].elements["apVPOList[" + i + "].docNumber"].value;
				 if(docNum==docNum1){
					 document.forms[0].elements["apVPOList[" + i + "].issueCheckbox"].checked = true;
				 }
			 }
			}else if(checked==false){
				for(i=0; i<size.value; i++){
					 //alert(docNum);
					 docNum1 = document.forms[0].elements["apVPOList[" + i + "].docNumber"].value;
					 if(docNum==docNum1){
						 document.forms[0].elements["apVPOList[" + i + "].issueCheckbox"].checked = false;
					 }
				 }
			}
			 
		 }
		 
		 function loadReferenceNumber(name,size) {
			 var index = name.charAt(10);
			 if (document.forms[0].elements["apVPOList[" + index + "].issueCheckbox"].checked == true) {
				 
				 document.forms[0].elements["conversionRate"].value = document.forms[0].elements["apVPOList[" + index + "].poConversionRate"].value;
				 document.forms[0].elements["conversionDate"].value = document.forms[0].elements["apVPOList[" + index + "].poConversionDate"].value;
				 document.forms[0].elements["poNumber"].value=document.forms[0].elements["apVPOList[" + index + "].poNumber"].value;
			 }
		 }
		 
		 function checkIssueAll(name,size){
			 var i=0;
			 var checked = document.forms[0].elements[name].checked;
			 var index = name.charAt(10);
			 if(checked==true){
				 var tmp1="";
				 for(i=0; i<size.value; i++){
					 document.forms[0].elements["apVPOList[" + i + "].issueCheckbox"].checked = true; 
					 var tmp2 = document.forms[0].elements["apVPOList[" + i + "].poNumber"].value;
					 if (tmp1.lastIndexOf(tmp2)<0) {
						if (tmp1!="") tmp1+=";";
						 tmp1 += tmp2;
					 }
				 } 
				 document.forms[0].elements["poNumber"].value=tmp1;
			 }else if(checked==false){
				 for(i=0; i<size.value; i++){
					 document.forms[0].elements["apVPOList[" + i + "].issueCheckbox"].checked = false; 
					 
				 } 
			 }
			 
			 
			 
		 }
		 function rowspanAllPo(){
			 
			document.getElementById("myHeader1").rowSpan="2";
		}
		 
		 function rowspanAll(){
			 
			document.getElementById("myHeader1").rowSpan="2";
			
		}
		 
		 function enterPurchaseOrder()
		  {

		    disableButtons();
		    enableInputControls();
			document.forms[0].elements["isPurchaseOrderEntered"].value = true;
			document.forms[0].submit();

		  }
		</script>
		 <html:hidden property="isPurchaseOrderEntered" value="" />
         <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isSupplierEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
		 <html:hidden property="isCurrencyEntered" value=""/>
	     <logic:equal name="apVoucherEntryForm" property="enableFields" value="true">
	     <tr>
		 	<td width="575" height="10" colspan="4">
				<div class="tabber">
			    <div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					    <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type', 'isTypeEntered');">
                   	   				<html:options property="typeList"/>
	               				</html:select>
                			</td>         
	     					<logic:equal name="apVoucherEntryForm" property="showBatchName" value="true">
	            			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       				<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
         				</tr>
         				<tr>
							<logic:equal name="apVoucherEntryForm" property="useSupplierPulldown" value="true">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="voucherEntry.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
               					<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                   					<html:options property="supplierList"/>
	               				</html:select>
								<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>
							<logic:equal name="apVoucherEntryForm" property="useSupplierPulldown" value="false">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="voucherEntry.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
									<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>							
									<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
		 				</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:text property="documentNumber" size="15" maxlength="100" styleClass="text"/>
                			</td>
		 				</tr>  
		 				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.poNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="poNumber" size="15" maxlength="25" styleClass="text" disabled="true" onblur="return enterPurchaseOrder();" onkeypress="javascript:if (event.keyCode == 13) return false;"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApPoByVouLookup('poNumber');" />
                			</td>
                			
                			
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="textRequired"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
					</table>
				</div>
				<div class="tabbertab" title="Misc">
		    		<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     	<tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="127" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   				<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
         					<logic:equal name="apVoucherEntryForm" property="enableVoucherVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="voucherVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apVoucherEntryForm" property="enableVoucherVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="voucherVoid" disabled="true"/>
                			</td>
                			</logic:equal>         
         				</tr>  
         				<tr>
                			<td width="130" height="25" class="prompt">
                  				<bean:message key="voucherEntry.prompt.amountPaid"/>
							</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.amountDue"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>                
         				</tr>
         				
 					</table>
				</div>
			    <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="voucherEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr> 
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>               
					</table>
					</div>	   
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apVoucherEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apVoucherEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="590" height="185" colspan="9">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="590" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="590" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="voucherEntry.gridTitle.VPODetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.docNumber"/>
				       </td>
					   <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.itemName"/>
				       </td>
		               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.location"/>
				       </td>				       				    
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.unit"/>
                       </td>
                       <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.unitCost"/>
                       </td>
                       <td width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.issue"/>
                       </td>
                       <td id="allPo" width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.allPo"/>
                       </td>
                       <td id="all" width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.all"/>
                       </td>
                       				       				       	                                             
				    </tr>				       			    
				    <tr>
				       <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poNumber"/>
				       </td>
				       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.itemDescription"/>
				       </td>
				       
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poQuantity"/>
				       </td>
				       
				       <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poAmount"/>
				       </td>
				       
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.amount"/>
                       </td> 
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <html:hidden property="apVPOListSize"/>
				    <nested:iterate name="apVoucherEntryForm" property="apVPOList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				       
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="105" height="1" class="gridLabel">
				          <nested:hidden property="docNumber"/>
				          <nested:write property="docNumber"/>
				       </td>
					   <td width="160" height="1" class="gridLabel" onclick="return enterSelectSupplier(event, 'supplier', 'isSupplierEntered');">
						  <nested:write property="itemName"/>
				       </td>
				       <td width="150" height="1" class="gridLabel">
				          <nested:write property="location"/>
				       </td>								   
				       <td width="50" height="1" class="gridLabelNum">
				          <nested:write property="quantity"/>
				       </td>
				       <td width="60" height="1" class="gridLabel">
                          <nested:write property="unit"/>                                               
                       </td>			       
                       <td width="105" height="1" class="gridLabelNum">
                          <nested:write property="unitCost"/>
                       </td>
                       <td width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="issueCheckbox" onchange="loadReferenceNumber(name,apVPOListSize)"/>
                       </td>
                       <td id="allPo" width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="allPoCheckbox" onchange="checkIssueAllPo(name,apVPOListSize);"/>
                       </td>
                       <td id="all" width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="allCheckbox" onchange="checkIssueAll(name,apVPOListSize);"/>
                       </td>
                       	  <nested:hidden property="poConversionRate"/>
                       	  <nested:hidden property="poConversionDate"/>
                           				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="105" height="1" class="gridLabel">
				       	  <nested:hidden property="poNumber"/>
				          <nested:write property="poNumber"/>
				       </td>				    								    				       	       
				       <td width="110" height="1" class="gridLabel">
				          <nested:write property="itemDescription"/>
				       </td>
				       <td width="50" height="1" class="gridLabelNum">
				          <nested:write property="poQuantity"/>
				       </td>
				       <td width="105" height="1" class="gridLabelNum">
                          <nested:write property="poAmount"/>
                       </td>
				       <td width="60" height="1" class="gridLabelNum">
				          <nested:write property="totalDiscount"/>
				       </td>
                       <td width="145" height="1" class="gridLabelNum">
				          <nested:write property="amount"/>
				       </td>  
				           
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>

	     <logic:equal name="apVoucherEntryForm" property="enableFields" value="false">
	     <tr>
		 	<td width="575" height="10" colspan="4">
				<div class="tabber">
				<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					    <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   				<html:options property="typeList"/>
	               				</html:select>
                			</td>         
	     					<logic:equal name="apVoucherEntryForm" property="showBatchName" value="true">
	            			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       				<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
         				</tr>
         				<tr>
							<logic:equal name="apVoucherEntryForm" property="useSupplierPulldown" value="true">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="voucherEntry.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
               					<html:select property="supplier" styleClass="comboRequired" style="width:130;" disabled="true">
                   					<html:options property="supplierList"/>
	               				</html:select>
								<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
							</td>
							</logic:equal>
							<logic:equal name="apVoucherEntryForm" property="useSupplierPulldown" value="false">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="voucherEntry.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
									<html:text property="supplier" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>							
									<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
							</td>
							</logic:equal>					
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
		 				</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:text property="documentNumber" size="15" maxlength="100" styleClass="text" disabled="true"/>
                			</td>
		 				</tr>  
		 				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.poNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="poNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:textarea property="description" cols="20" rows="4" styleClass="text"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
					</table>
				</div>
				<div class="tabbertab" title="Misc">
		    		<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     	<tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="127" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   				<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
         					<logic:equal name="apVoucherEntryForm" property="enableVoucherVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="voucherVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="apVoucherEntryForm" property="enableVoucherVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.voucherVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="voucherVoid" disabled="true"/>
                			</td>
                			</logic:equal>         
         				</tr>  
         				<tr>
                			<td width="130" height="25" class="prompt">
                  				<bean:message key="voucherEntry.prompt.amountPaid"/>
							</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="voucherEntry.prompt.amountDue"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>                
         				</tr>
         				
         				
 					</table>
				</div>
			   	<div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="voucherEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>	
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr> 
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="apVoucherEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>               
					</table>
					</div>		    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         		         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="voucherEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="apVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apVoucherEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="apVoucherEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="apVoucherEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="600" height="185" colspan="9">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="600" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="600" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="voucherEntry.gridTitle.VPODetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.docNumber"/>
				       </td>
					   <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.itemName"/>
				       </td>
		               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.location"/>
				       </td>				       				    
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.unit"/>
                       </td>
                       <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.unitCost"/>
                       </td>
                       <td width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.issue"/>
                       </td>	
                       <td width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.allPo"/>
                       </td>
                       <td width="40" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.all"/>
                       </td>
                       		       				       	                                             
				    </tr>				       			    
				    <tr>
		               <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poNumber"/>
				       </td>
				       		            
		               <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.itemDescription"/>
				       </td>
				       
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poQuantity"/>
				       </td>
				       
				       <td width="105" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="voucherEntry.prompt.poAmount"/>
				       </td>
				       
					   <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.itemDiscount"/>
					   
					   <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="voucherEntry.prompt.amount"/>
                       </td>
                       <td></td><td></td>    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <html:hidden property="apVPOListSize"/>
				    <nested:iterate name="apVoucherEntryForm" property="apVPOList">
				    <%
				   
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="105" height="1" class="gridLabel">
				          <nested:hidden property="docNumber"/>
				          <nested:write property="docNumber"/>
				       </td>
					   <td width="160" height="1" class="gridLabel">
				          <nested:write property="itemName"/>
				       </td>
				       <td width="150" height="1" class="gridLabel">
				          <nested:write property="location"/>
				       </td>								   
				       <td width="50" height="1" class="gridLabel">
				          <nested:write property="quantity"/>
				       </td>
				       <td width="60" height="1" class="gridLabel">
                          <nested:write property="unit"/>                                               
                       </td>			       
                       <td width="105" height="1" class="gridLabelNum">
                          <nested:write property="unitCost"/>
                       </td>
                       <td width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="issueCheckbox" disabled="true"/>
                       </td>   
                       <td id="allPo" width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="allPoCheckbox" disabled="true"/>
                       </td>
                       <td id="all" width="40" height="1" rowspan="2" class="gridLabel">
				          <p align="center">				       
                          <nested:checkbox property="allCheckbox" disabled="true"/>
                       </td>   				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">		   
				       <td width="105" height="1" class="gridLabel">
				          <nedted:write property="poNumber"/>
				       </td>				    								    				       	       
						<td width="110" height="1" class="gridLabel">
				          <nested:write property="itemDescription"/>
				       </td>
				       <td width="50" height="1" class="gridLabelNum">
				          <nested:write property="poQuantity"/>
				       </td>
				       <td width="105" height="1" class="gridLabelNum">
                          <nested:write property="poAmount"/>
                       </td>
				       <td width="60" height="1" class="gridLabelNum">
				          <nested:write property="totalDiscount"/>
				       </td>
				       <td width="145" height="1" class="gridLabelNum">
				          <nested:write property="amount"/>
				       </td>	              
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>