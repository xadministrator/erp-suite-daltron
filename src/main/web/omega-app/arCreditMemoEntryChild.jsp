<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="creditMemoEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{
  
  var property = name.substring(0,name.indexOf("."));
  
  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;      
        
  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {
  
  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');
  	
  }
  
  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {
  
  	unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');
  	
  }
  
  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
  
  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');
  	
  }
  
  amount = (quantity * unitPrice).toFixed(2);
  
  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
   
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/arCreditMemoEntry.do" onsubmit="return submitForm();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="creditMemoEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arCreditMemoEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>	
	        </td>
	     </tr>
	     <logic:equal name="arCreditMemoEntryForm" property="type" value="MEMO LINES">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>	     
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>         
         				<% } %>
	     					<logic:equal name="arCreditMemoEntryForm" property="showBatchName" value="true">	     
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
							</logic:equal>
         				</tr>         
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arCreditMemoEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arCreditMemoEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>	            
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.cmNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="cmNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>	
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.amount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="amount" size="15" maxlength="15" styleClass="textAmountRequired" disabled="true"/>
                			</td>
						</tr>
						<tr>
               		 		<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
         				<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
                			<logic:equal name="arCreditMemoEntryForm" property="enableCreditMemoVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.creditMemoVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="creditMemoVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arCreditMemoEntryForm" property="enableCreditMemoVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.creditMemoVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="creditMemoVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
         					<td width="160" height="25" class="prompt">
         						<bean:message key="creditMemoEntry.prompt.totalDebit"/>
         					</td>
         					<td width="128" height="25" class="prompt">
         						<html:text property="totalDebit" size="17" maxlength="25" styleClass="text" disabled="true"/>
         					</td>
         					<td width="160" height="25" class="prompt">
         						<bean:message key="creditMemoEntry.prompt.totalCredit"/>
         					</td>
         					<td width="127" height="25" class="prompt">
         						<html:text property="totalCredit" size="17" maxlength="25" styleClass="text" disabled="true"/>
         					</td>
         				</tr>
						<tr>
							<td class="prompt" width="150" height="25">
								<bean:message key="creditMemoEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control" colspan="3">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
					</table>
					</div>		
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton3" value="true">			                   
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton4" value="true">			                   
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				     </table>
			         </div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         				         				         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="creditMemoEntry.gridTitle.CMDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.account"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.description"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arCMList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="3" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="385" height="1" class="control">
				          <nested:text property="account" size="26" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="accountDescription" size="110" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		   </logic:equal>
		   <logic:equal name="arCreditMemoEntryForm" property="type" value="ITEMS">
	       <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         
	     					<logic:equal name="arCreditMemoEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>        				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arCreditMemoEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arCreditMemoEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                               
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.cmNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="cmNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td> 
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.amount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="amount" size="15" maxlength="15" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
				    </table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<logic:equal name="arCreditMemoEntryForm" property="showShift" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.shift"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
                			<logic:equal name="arCreditMemoEntryForm" property="enableCreditMemoVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.creditMemoVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="creditMemoVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arCreditMemoEntryForm" property="enableCreditMemoVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="creditMemoEntry.prompt.creditMemoVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   			<html:checkbox property="creditMemoVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
							<td class="prompt" width="150" height="25">
								<bean:message key="creditMemoEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control" colspan="3">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
				     </table>
					</div>		        
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton3" value="true">			                   
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
						 <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton4" value="true">			                   
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arCreditMemoEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				     </table>
			         </div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         				         				         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="creditMemoEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="creditMemoEntry.gridTitle.CLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="320" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.itemName"/>
				       </td>
		               <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.location"/>
				       </td>				       				    
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.quantity"/>
                       </td>
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.unit"/>
                       </td>
                       <td width="164" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.unitPrice"/>
                       </td>
                       <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.autoBuild"/>
                       </td>      
				       <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="610" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="creditMemoEntry.prompt.itemDescription"/>
				       </td>	
				       <td width="244" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="creditMemoEntry.prompt.amount"/>
                       </td>  		          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arCLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="320" height="1" class="control">
				          <nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="130" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="80" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="164" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="15" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                       </td>
                       <td width="40" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>		       
				       <td width="40" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>				   
				       <td width="610" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="70" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="244" height="1" class="control" colspan="4">
				          <nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td> 				                                     
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		   </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arCreditMemoEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arCreditMemoEntryForm" type="com.struts.ar.creditmemoentry.ArCreditMemoEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="arCreditMemoEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arCreditMemoEntryForm" type="com.struts.ar.creditmemoentry.ArCreditMemoEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
</body>
</html>
