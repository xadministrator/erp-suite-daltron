<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="customerClass.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/arCustomerClasses.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="customerClass.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arCustomerClassForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.name"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="name" size="25" maxlength="25" styleClass="textRequired"/>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.description"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="description" size="50" maxlength="50" styleClass="text"/>
	         </td>
         </tr> 
         
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.nextCustomerCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="nextCustomerCode" size="25" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
         
         <tr>
			<td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.customerBatch"/>
	         </td>
	          <td width="127" height="25" class="control">
	            <html:select property="customerBatch" styleClass="combo">
	              <html:options property="customerBatchList"/>
			   </html:select>
	         </td>
		
		</tr>
		
		<tr>
			<td class="prompt" width="140" height="25">
               <bean:message key="customerClass.prompt.dealPrice"/>
            </td>
		    <td width="147" height="25" class="control">
		       	<html:select property="dealPrice" styleClass="combo">
	               <html:options property="dealPriceList"/>
            	</html:select>
		    </td>
		</tr>
		
		<tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.monthlyInterestRate"/>
	         </td>
	         <td width="135" height="25" class="control" colspan="1">
	            <html:text property="monthlyInterestRate" size="15" maxlength="25" styleClass="textAmount" onblur="addZeroes(name);" onkeypress="javascript:if (event.keyCode == 13) return false;" onkeyup="formatAmount(name, (event)?event:window.event);" /> 
	         </td>
         </tr>
		
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.receivableAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="receivableAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('receivableAccount','receivableAccountDescription');"/>	            
	         </td>
         </tr>             
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.receivableAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="receivableAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
	     <tr>	         
			 <logic:equal name="arCustomerClassForm" property="enableRevenueAccount" value="true">	     
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.revenueAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','revenueAccountDescription');"/>	            
	         </td>
	         </logic:equal>
			 <logic:equal name="arCustomerClassForm" property="enableRevenueAccount" value="false">	     
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.revenueAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
			    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','revenueAccountDescription');" disabled="true"/>	            
	         </td>
	         </logic:equal>	         	         
         </tr>  
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.revenueAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="revenueAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         
   <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.autoComputeInterest"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:checkbox property="autoComputeInterest" onchange="showInterestAccount();"/>
	         </td>
	         
	                           
         </tr>
         <tr class="interestAccount">
             <td class="prompt" width="140" height="25" >
	            <bean:message key="customerClass.prompt.unEarnedInterestAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="unEarnedInterestAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedInterestAccount','unEarnedInterestAccountDescription');"/>	            
	         </td>
         </tr>             
         <tr class="interestAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.unEarnedInterestAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="unEarnedInterestAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         
            
      
  
         
         <tr class="interestAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.earnedInterestAccount"/>
            </td>
            <td width="435" height="25" class="control" colspan="3">
	            <html:text property="earnedInterestAccount" size="30" maxlength="255" styleClass="textRequired"/>
                    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedInterestAccount','earnedInterestAccountDescription');"/>	            
            </td>
         </tr>             
         <tr class="interestAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.earnedInterestAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="earnedInterestAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         
        <tr>
         	<td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.autoComputePenalty"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:checkbox property="autoComputePenalty" onchange="showPenaltyAccount()"/>
	         </td>	
         </tr>
         <tr class="penaltyAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.unEarnedPenaltyAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="unEarnedPenaltyAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedPenaltyAccount','unEarnedPenaltyAccountDescription');"/>	            
	         </td>
         </tr>             
         <tr class="penaltyAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.unEarnedPenaltyAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="unEarnedPenaltyAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         
         
         
         <tr class="penaltyAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.earnedPenaltyAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="earnedPenaltyAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedPenaltyAccount','earnedPenaltyAccountDescription');"/>	            
	         </td>
         </tr>             
         <tr class="penaltyAccount">
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.earnedPenaltyAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="earnedPenaltyAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         
                                         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.taxCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="taxCode" styleClass="comboRequired">
			      <html:options property="taxCodeList"/>
			   </html:select>
	         </td>	     
	     </tr>
	     <tr>	     
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.withholdingTaxCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="withholdingTaxCode" styleClass="comboRequired">
			      <html:options property="withholdingTaxCodeList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.creditLimit"/>
	         </td>
	         <td width="135" height="25" class="control" colspan="1">
	            <html:text property="creditLimit" size="15" maxlength="25" styleClass="textAmount" onblur="addZeroes(name);" onkeypress="javascript:if (event.keyCode == 13) return false;" onkeyup="formatAmount(name, (event)?event:window.event);" /> 
	         </td>
         </tr>         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.enable"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:checkbox property="enable"/>
	         </td>	                  
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="customerClass.prompt.enableRebate"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:checkbox property="enableRebate"/>
	         </td>	                  
         </tr>
      
                                         
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="160" height="50">
			             <div id="buttons">
			             <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="415" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				       	 <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	 <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton">
				               <bean:message key="button.save"/>
				          	</html:submit>
				         </logic:equal>
				         <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
                                            
                                             
                                             <html:submit property="memoLineClassButton" styleClass="mainButtonMedium" style="width:120px;">
				               <bean:message key="button.memoLineClass"/>
				            </html:submit> 
                                             
                                             
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.save"/>
				            </html:submit>
				         </logic:equal>
				         <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				     
                                              <html:submit property="memoLineClassButton" styleClass="mainButtonMedium" style="width:120px;" disabled="true">
				               <bean:message key="button.memoLineClass"/>
				            </html:submit> 
                                             
                                             
                                             <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="8" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="customerClass.gridTitle.CCDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.name"/>
			       </td>
			       <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.receivableAccount"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.revenueAccount"/>
			       </td>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.taxCode"/>
			       </td>			       
			        <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.withholdingTaxCode"/>
			       </td>
			       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.creditLimit"/>
			       </td>
			       			       
			       <td width="150" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.enable"/>
			       </td>			       			       			       			       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arCCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="name"/>
			       </td>			       
			       <td width="160" height="1" class="gridLabel">
			          <nested:write property="receivableAccount"/>
			       </td>
			       <td width="140" height="1" class="gridLabel">
			          <nested:write property="revenueAccount"/>
			       </td>
			       <td width="132" height="1" class="gridLabel">
			          <nested:write property="taxCode"/>
			       </td>
			       <td width="132" height="1" class="gridLabel">
			          <nested:write property="withholdingTaxCode"/>
			       </td>
			        <td width="80" height="1" class="gridLabel">
			          <nested:write property="creditLimit"/>
			       </td>			       
			       <td width="50" height="1" class="gridLabel">
			          <nested:write property="enable"/>
			       </td>
			       <td width="100" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
   			       </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		              <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="arCustomerClassForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arCCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="customerClass.prompt.name"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="name"/>
			       </td>
			       <td width="149" align="center" height="1">
			       <div id="buttons">
			        <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
                                    <nested:submit property="editButton" styleClass="gridButton">
                                        <bean:message key="button.edit"/>
                                    </nested:submit>
                                    <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
                                        <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
                                           <bean:message key="button.delete"/>
                                        </nested:submit>
                                    </logic:equal>  	                      
                                </logic:equal>
				   </div>
				   <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="arCustomerClassForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arCustomerClassForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.description"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="description"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.enable"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="enable"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.receivableAccount"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="receivableAccount"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.taxCode"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="taxCode"/>
			      </td>			      
			    </tr>	    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.receivableAccountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="receivableAccountDescription"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.revenueAccount"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="revenueAccount"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.withholdingTaxCode"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="withholdingTaxCode"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="customerClass.prompt.revenueAccountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="revenueAccountDescription"/>
			      </td>
			    </tr>			    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
    
    
  <!--
      if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
   // -->
</script>

<script language=JavaScript type=text/javascript>
    showInterestAccount();
    showPenaltyAccount();
    function showInterestAccount(){

        d =  document.forms[0].elements["autoComputeInterest"].checked;

        if(d){
  
            var x = document.getElementsByClassName("interestAccount");
            var i;
            for (i = 0; i < x.length; i++) {
                
                x[i].style.visibility = "visible";
           
               x[i].style.display = "table-row";
            }
            
      
        }else{

            var x = document.getElementsByClassName("interestAccount");

            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "hidden";
            
                x[i].style.display = "none";
            }

        } 
        
    }
    
    function showPenaltyAccount(){

        d =  document.forms[0].elements["autoComputePenalty"].checked;

        if(d){
  
            var x = document.getElementsByClassName("penaltyAccount");
            var i;
            for (i = 0; i < x.length; i++) {
                
                x[i].style.visibility = "visible";
           
               x[i].style.display = "table-row";
            }
            
      
        }else{

            var x = document.getElementsByClassName("penaltyAccount");

            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "hidden";
            
                x[i].style.display = "none";
            }

        } 
        
    }

    
    
</script>


</body>
</html>
