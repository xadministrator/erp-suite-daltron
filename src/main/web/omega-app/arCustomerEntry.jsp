<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="customerEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


  function customerEnterSubmit()
  {
      if (document.activeElement.name != 'address' &&
          document.activeElement.name != 'billToAddress' &&
          document.activeElement.name != 'shipToAddress' &&
          document.activeElement.name != 'billToContact' &&
          document.activeElement.name != 'shipToContact' &&
          document.activeElement.name != 'billToAltContact' &&
          document.activeElement.name != 'shipToAltContact' &&
          document.activeElement.name != 'billingHeader' &&
          document.activeElement.name != 'billingFooter' &&
          document.activeElement.name != 'billingHeader2' &&
          document.activeElement.name != 'billingFooter2' &&
          document.activeElement.name != 'billingHeader3' &&
          document.activeElement.name != 'billingFooter3' &&
          document.activeElement.name != 'memo') {

          return enterSubmit(event, new Array('saveButton'));

      } else {

          return true;

      }

  }
//Done Hiding-->
</script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

  function submitForm()
  {
      disableButtons();
      enableInputControls();
  }

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return customerEnterSubmit();">
<html:form action="/arCustomerEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="805" height="420">
      <tr valign="top">
        <td width="187" height="420"></td>
        <td width="581" height="420">
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="420"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="customerEntry.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="arCustomerEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>
	       <html:messages id="msg" message="true">
       			<bean:write name="msg"/>
   			</html:messages>
	        </td>
	     </tr>
	     <html:hidden property="isCustomerClassEntered" value=""/>
	     <html:hidden property="isCustomerTypeEntered" value=""/>
	     <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">

	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="customerEntry.prompt.customerCode"/>
	        </td>
	        <logic:equal name="arCustomerEntryForm" property="autoGenerateCustomerCode" value="true">
	        <td width="148" height="25" class="control">
	           <html:text property="customerCode" size="25" maxlength="100" styleClass="text"/>
	        </td>
	        </logic:equal>
	        <logic:equal name="arCustomerEntryForm" property="autoGenerateCustomerCode" value="false">
	        <td width="148" height="25" class="control">
	           <html:text property="customerCode" size="25" maxlength="100" styleClass="textRequired"/>
	        </td>
	        </logic:equal>

		  </tr>

		  <tr>
                    <td class="prompt" width="140" height="25">
                       <bean:message key="customerEntry.prompt.enablePayroll"/>
                    </td>

                    <td width="148" height="25" class="control">
                        <html:checkbox property="enablePayroll" disabled="true"/>
                    </td>


		  </tr>



		  <logic:equal name="arCustomerEntryForm" property="autoGenerateCustomerCode" value="true">
		  	<logic:equal name="arCustomerEntryForm" property="isNew" value="true">
		  	  	<tr>
			  		<td class="prompt" width="140" height="25">
	           			<bean:message key="customerEntry.prompt.nextCustomerCode"/>
	        		</td>
					<td class="prompt" width="435" height="25" colspan="3" style="text-indent:0.000000pt; font-size:10pt; font-weight:bold;">
						<bean:write name="arCustomerEntryForm" property="nextCustomerCode"/>
	        		</td>
			  	</tr>
			</logic:equal>
		  </logic:equal>

		  <tr>


		   <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enable"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enable"/>
		    </td>

		  	<td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enableRetailCashier"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enableRetailCashier"/>
		    </td>

		  </tr>

		  <tr>


		   <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enableRebate"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enableRebate"/>
		    </td>



		  </tr>

		  <tr>
		  <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputeInterest"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputeInterest" onchange="showInterestAccount()"/>
		    </td>


		  <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputePenalty"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputePenalty" onchange="showPenaltyAccount()"/>
		    </td>
		  </tr>



	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.customerName"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="customerName" size="50" maxlength="100" styleClass="textRequired"/>
		    </td>
		  </tr>

		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerClass"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="customerClass" styleClass="comboRequired" onchange="return enterSelect('customerClass','isCustomerClassEntered');">
               <html:options property="customerClassList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.paymentTerm"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentTerm" styleClass="comboRequired">
               <html:options property="paymentTermList"/>
            </html:select>
		    </td>
		  </tr>

		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerType"/>
            </td>
		    <td width="148" height="25" class="control" colspan="3">
            <html:select property="customerType" styleClass="combo" onchange="return enterSelect('customerType','isCustomerTypeEntered');">
               <html:options property="customerTypeList"/>
            </html:select>
		    </td>
		    </tr>
		    <tr>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.bankAccount"/>
			</td>
			<td width="147" height="25" class="control" colspan="3">
			   <html:select property="bankAccount" styleClass="comboRequired">
	              <html:options property="bankAccountList"/>
	           </html:select>
			</td>
		  </tr>

	      <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.salesperson"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="salesperson" styleClass="combo">
               <html:options property="salespersonList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.paymentMethod"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentMethod" styleClass="combo">
               <html:options property="paymentMethodList"/>
            </html:select>
		    </td>
		  </tr>

	      <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.salesperson2"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="salesperson2" styleClass="combo">
               <html:options property="salesperson2List"/>
            </html:select>
		    </td>
		  </tr>

		  <tr>
			<td class="prompt" width="140" height="25">
	            <bean:message key="customerEntry.prompt.customerBatch"/>
	         </td>
	          <td width="127" height="25" class="control">
	            <html:select property="customerBatch" styleClass="combo">
	              <html:options property="customerBatchList"/>
			   </html:select>
	         </td>

		</tr>

		<tr>
			<td class="prompt" width="140" height="25">
	            <bean:message key="customerEntry.prompt.customerDepartment"/>
	         </td>
	          <td width="127" height="25" class="control">
	            <html:select property="customerDepartment" styleClass="combo">
	              <html:options property="customerDepartmentList"/>
			   </html:select>
	         </td>

		</tr>

		  <tr>
	        <td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.tinNumber"/>
			</td>
			<td width="148" height="25" class="control">
			   <html:text property="tinNumber" size="25" maxlength="100" styleClass="text"/>
			</td>
		  </tr>

	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.description"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="description" size="50" maxlength="50" styleClass="text"/>
		    </td>
		  </tr>

		  <tr>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.creditLimit"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="creditLimit" size="25" maxlength="100" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
			</td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.dealPrice"/>
            </td>
		    <td width="147" height="25" class="control">
		       	<html:select property="dealPrice" styleClass="combo">
	               <html:options property="dealPriceList"/>
            	</html:select>
		    </td>
		  </tr>

		  <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.templateName"/>
            </td>
            <td width="148" height="25" class="control">
            <html:select property="templateName" styleClass="combo" style="width:150;">
               <html:options property="templateList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.area"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="area" size="20" maxlength="20" styleClass="text" />
			</td>
		  </tr>

		  <tr>
		  	<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.effectivityDays"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="effectivityDays" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.entryDate"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="entryDate" size="10" maxlength="100" styleClass="text" />
			</td>
		</tr>

		<tr>
		  	<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.squareMeter"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="squareMeter" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>


			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.associationDuesRate"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="associationDuesRate" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>

		</tr>

		<tr>
		  	<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.numbersParking"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="numbersParking" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>


			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.parkingID"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="parkingID" size="10" maxlength="100"  styleClass="text"/>
			</td>

		</tr>





		<tr>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.realPropertyTaxRate"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="realPropertyTaxRate" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>




		</tr>

		<tr>
		  	<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.monthlyInterestRate"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="monthlyInterestRate" size="10" maxlength="100"  styleClass="textAmount"/>
			</td>

		</tr>

		<tr>
			<td width="130" height="25" class="prompt">
			<bean:message key="customerEntry.prompt.supplier"/>
			</td>
			<logic:equal name="arCustomerEntryForm" property="useSupplierPulldown" value="true">
			<td width="158" height="25" class="control">
			<html:select property="supplier"  style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
			<html:options property="supplierList"/>
			</html:select>
			<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
			</td>
			</logic:equal>
			<logic:equal name="arCustomerEntryForm" property="useSupplierPulldown" value="false">
			<td width="158" height="25" class="control">
			<html:text property="supplier" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
			<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
			</td>
			</logic:equal>

			</tr>



		 <tr>
	        <td width="600" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Address">
		        <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
		          <tr>
				    <td class="prompt" width="575" height="20" colspan="4">
		            </td>
		          </tr>
		          <tr>
				    <td class="prompt" width="140" height="25">
				    <bean:message key="customerEntry.prompt.address"/>
				    </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="address" cols="20" rows="4" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.city"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:text property="city" size="20" maxlength="100" styleClass="text"/>
				    </td>
		          </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.stateProvince"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="stateProvince" size="25" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.postalCode"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="postalCode" size="10" maxlength="10" styleClass="text"/>
				    </td>
				 </tr>
			     <tr>
					<td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.region"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:select property="region" styleClass="combo">
		               	<html:options property="regionList"/>
		               </html:select>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.country"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="country" size="25" maxlength="100" styleClass="text"/>
				    </td>
				 </tr>
				</table>
			    </div>

                <div class="tabbertab" title="Contact">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.birthday"/>
		            </td>
				    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="birthday" size="10" maxlength="10" styleClass="text"/>
				    </td>
				  </tr>

				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.wpCustomerID"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="wpCustomerID" size="25" maxlength="100" styleClass="text"/>
				    </td>

			      </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.employeeID"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="employeeID" size="20" maxlength="100" styleClass="text"/>
				    </td>

			      </tr>
			       <tr>

			      <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.accountNumber"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="accountNumber" size="20" maxlength="100" styleClass="text"/>
				    </td>

			      </tr>

                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.contact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="contact" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.phone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="phone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			      </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.mobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="mobilePhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			      </tr>
			      <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="alternateContact" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternatePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternatePhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
				  </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateMobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternateMobilePhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			      </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.fax"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="fax" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.email"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="email" size="20" maxlength="150" styleClass="text"/>
				    </td>
			      </tr>
				</table>
			    </div>


			    <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="600" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="600" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>


                <div class="tabbertab" title="BILL/SHIP">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAddress"/>
		            </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="billToAddress" cols="20" rows="4" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAddress"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:textarea property="shipToAddress" cols="20" rows="4" styleClass="text"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAltContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAltContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToPhone"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="billToPhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToPhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="shipToPhone" size="20" maxlength="100" styleClass="text"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingSignatory"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:text property="billingSignatory" size="20" maxlength="100" styleClass="text"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.signatoryTitle"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="signatoryTitle" size="20" maxlength="100" styleClass="text"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader2"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter2"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader3"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter3"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text"/>
				    </td>
			    </tr>
			</table>
	    </div>


	    <div class="tabbertab" title="HR">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="customerEntry.prompt.deployedBranchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="deployedBranchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="deployedBranchNameList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="customerEntry.prompt.employeeNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="employeeNumber" styleClass="comboRequired" style="width:130;">
                       			<html:options property="employeeNumberList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.bioNumber"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="bioNumber" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.managingBranch"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="managingBranch" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="customerEntry.prompt.currentJobPosition"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="currentJobPosition" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

					 </table>
					 </div>



		    <div class="tabbertab" title="Accounts">
		       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableAccount" size="30" maxlength="255" styleClass="textRequired"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('receivableAccount','receivableDescription');"/>
				     </td>
				   </tr>
				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>
				    <tr>
				     <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired"/>
					    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','revenueDescription');"/>
			         </td>
			         </logic:equal>
					 <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
					    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','revenueDescription');" disabled="true"/>
			         </td>
			         </logic:equal>
				   </tr>

				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.revenueDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="revenueDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>


                            <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestAccount" size="30" maxlength="255" styleClass="text"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedInterestAccount','unEarnedInterestDescription');"/>
				     </td>
                            </tr>
                            <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
                            </tr>

                            <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestAccount" size="30" maxlength="255" styleClass="textRequired"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedInterestAccount','earnedInterestDescription');"/>
				     </td>
                            </tr>
                            <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
                            </tr>



                            <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedPenaltyAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedPenaltyAccount','unEarnedPenaltyDescription');"/>
				     </td>
                            </tr>
                            <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedPenaltyDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedPenaltyDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
                            </tr>

                            <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedPenaltyAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedPenaltyAccount" size="30" maxlength="255" styleClass="textRequired"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedPenaltyAccount','earnedPenaltyDescription');"/>
				     </td>
                            </tr>
                            <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedPenaltyDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedPenaltyDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
                                </tr>






				</table>
			    </div>
			    <div class="tabbertab" title="Notes">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.memo"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="memo" cols="60" rows="7" styleClass="text"/>
				    </td>
			       </tr>
			       </table>
			    </div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="575" height="20" colspan="4">
						</td>
					</tr>
					<nested:iterate property="bcstList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="branchName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.receivableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchReceivableAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchReceivableAccount','branchReceivableDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.receivableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchReceivableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');"/>
						</td>
						</logic:equal>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');" disabled="true"/>
						</td>
						</logic:equal>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="customerEntry.prompt.revenueDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchRevenueDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>



					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedInterestAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedInterestAccount','branchUnEarnedInterestDescription');"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>


					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedInterestAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedInterestAccount','branchEarnedInterestDescription');"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>


					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedPenaltyAccount','branchUnEarnedPenaltyDescription');"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>


					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedPenaltyAccount','branchEarnedPenaltyDescription');"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>

		            </nested:iterate>
				</table>
		        </div>
			  </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
	     <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="customerEntry.prompt.customerCode"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="customerCode" size="25" maxlength="100" styleClass="text" disabled="true"/>
	        </td>

		  </tr>

		   <tr>


		   <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enable"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enable" disabled="true"/>
		    </td>

		  	<td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enableRetailCashier"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enableRetailCashier" disabled="true"/>
		    </td>

		  </tr>


		  <tr>

		   <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enableRebate"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enableRebate" disabled="true"/>
		    </td>

		  </tr>

		  <tr>

		  <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputeInterest"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputeInterest"  onchange="showInterestAccount();" disabled="true"/>
		    </td>
		  </tr>

		   <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputePenalty"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputePenalty" onchange="showPenaltyAccount();" disabled="true"/>
		    </td>
		  </tr>

		  <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.customerName"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="customerName" size="50" maxlength="100" styleClass="textRequired" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerClass"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="customerClass" styleClass="comboRequired" disabled="true">
               <html:options property="customerClassList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.paymentTerm"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentTerm" styleClass="comboRequired" disabled="true">
               <html:options property="paymentTermList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerType"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="customerType" styleClass="combo" disabled="true">
               <html:options property="customerTypeList"/>
            </html:select>
		    </td>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.bankAccount"/>
			</td>
			<td width="147" height="25" class="control">
			   <html:select property="bankAccount" styleClass="comboRequired" disabled="true">
	              <html:options property="bankAccountList"/>
	           </html:select>
			</td>
		  </tr>
	      <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.salesperson"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="salesperson" styleClass="combo" disabled="true">
               <html:options property="salespersonList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.paymentMethod"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentMethod" styleClass="combo" disabled="true">
               <html:options property="paymentMethodList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.tinNumber"/>
			</td>
			<td width="148" height="25" class="control">
			   <html:text property="tinNumber" size="25" maxlength="100" styleClass="text" disabled="true"/>
			</td>
		  </tr>
	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.description"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.creditLimit"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="creditLimit" size="25" maxlength="100" styleClass="textAmount" disabled="true"/>
			</td>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.dealPrice"/>
			</td>
			<td width="147" height="25" class="control">
				<html:select property="dealPrice" styleClass="combo" disabled="true">
               		<html:options property="dealPriceList"/>
        		</html:select>
			</td>
		  </tr>
		 <tr>
	        <td width="600" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Address">
		        <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
		          <tr>
				    <td class="prompt" width="575" height="20" colspan="4">
		            </td>
		          </tr>
		          <tr>
				    <td class="prompt" width="140" height="25">
				    <bean:message key="customerEntry.prompt.address"/>
				    </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="address" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.city"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:text property="city" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
		          </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.stateProvince"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="stateProvince" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.postalCode"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="postalCode" size="10" maxlength="10" styleClass="text" disabled="true"/>
				    </td>
				 </tr>

				 <tr>
			       	<td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.region"/>
		            </td>
				    <td width="147" height="25" class="control" >
		               <html:select property="region" styleClass="combo" disabled="true">
		               	<html:options property="regionList"/>
		               </html:select>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.country"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="country" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				 </tr>
				</table>
			    </div>
                <div class="tabbertab" title="Contact">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.birthday"/>
		            </td>
				    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="birthday" size="10" maxlength="10" styleClass="text" disabled="true"/>
				    </td>
				  </tr>

				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.wpCustomerID"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="wpCustomerID" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>

			      </tr>

				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.employeeID"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="employeeID" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>

			      </tr>



			      <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.accountNumber"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="accountNumber" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>

			      </tr>

                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.contact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="contact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.phone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="phone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.mobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="mobilePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="alternateContact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternatePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternatePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateMobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternateMobilePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.fax"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="fax" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.email"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="email" size="20" maxlength="150" styleClass="text" disabled="true"/>
				    </td>
			      </tr>
				</table>
			    </div>
                <div class="tabbertab" title="Billing/Shipping">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAddress"/>
		            </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="billToAddress" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAddress"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:textarea property="shipToAddress" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAltContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAltContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToPhone"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="billToPhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToPhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="shipToPhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingSignatory"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:text property="billingSignatory" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.signatoryTitle"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="signatoryTitle" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader2"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter2"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
				<tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader3"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter3"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			</table>
	    </div>
		    <div class="tabbertab" title="Accounts">
		       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('receivableAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>
				   <tr>
				     <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','');" />
			         </td>
			         </logic:equal>
					 <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','');" disabled="true"/>
			         </td>
			         </logic:equal>
				   </tr>
				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.revenueDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="revenueDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>


				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedInterestAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>

				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedInterestAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>


				   <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedPenaltyAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedPenaltyAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedPenaltyDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedPenaltyDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>

				   <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedPenaltyAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedPenaltyAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedPenaltyAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="penaltyAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedPenaltyDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedPenaltyDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>

				</table>
			    </div>
	            <div class="tabbertab" title="Notes">
			       <table border="0" cellpadding="0" cellspacing="0" width="600" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.memo"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    </td>
			       </tr>
			       </table>
			    </div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="600" height="25">
					<tr>
						<td class="prompt" width="575" height="20" colspan="4">
						</td>
					</tr>
					<nested:iterate property="bcstList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="branchName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.receivableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchReceivableAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchReceivableAccount','branchReceivableDescription');" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.receivableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchReceivableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>
					<tr>
						<td></td>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');"/>
						</td>
						</logic:equal>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');" disabled="true"/>
						</td>
						</logic:equal>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="customerEntry.prompt.revenueDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchRevenueDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>



					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedInterestAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedInterestAccount','branchUnEarnedInterestDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>






					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedInterestAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedInterestAccount','branchEarnedInterestDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>



					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedPenaltyAccount','branchUnEarnedPenaltyDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>






					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedPenaltyAccount','branchEarnedPenaltyDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>

		            </nested:iterate>
				</table>
		        </div>
			  </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
	     <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="customerEntry.prompt.customerCode"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="customerCode" size="25" maxlength="100" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.enable"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="enable" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
		  <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputeInterest"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputeInterest" onchange="showInterestAccount();" disabled="true"/>
		    </td>
		  </tr>

		  <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.autoComputePenalty"/>
            </td>
		    <td width="147" height="25" class="control">
		       <html:checkbox property="autoComputePenalty" onchange="showPenaltyAccount();" disabled="true"/>
		    </td>
		  </tr>

		  <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.customerName"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="customerName" size="50" maxlength="100" styleClass="textRequired" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerClass"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="customerClass" styleClass="comboRequired" disabled="true">
               <html:options property="customerClassList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.paymentTerm"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentTerm" styleClass="comboRequired" disabled="true">
               <html:options property="paymentTermList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.customerType"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="customerType" styleClass="combo" disabled="true">
               <html:options property="customerTypeList"/>
            </html:select>
		    </td>
			<td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.bankAccount"/>
			</td>
			<td width="147" height="25" class="control">
			   <html:select property="bankAccount" styleClass="comboRequired" disabled="true">
	              <html:options property="bankAccountList"/>
	           </html:select>
			</td>
		  </tr>
	      <tr>
		    <td class="prompt" width="140" height="25">
            <bean:message key="customerEntry.prompt.salesperson"/>
            </td>
		    <td width="148" height="25" class="control">
            <html:select property="salesperson" styleClass="combo" disabled="true">
               <html:options property="salespersonList"/>
            </html:select>
		    </td>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.paymentMethod"/>
            </td>
		    <td width="147" height="25" class="control">
            <html:select property="paymentMethod" styleClass="combo" disabled="true">
               <html:options property="paymentMethodList"/>
            </html:select>
		    </td>
		  </tr>
		  <tr>
		    <td class="prompt" width="140" height="25">
		  	   <bean:message key="customerEntry.prompt.tinNumber"/>
			</td>
			<td width="148" height="25" class="control">
			   <html:text property="tinNumber" size="25" maxlength="100" styleClass="text" disabled="true"/>
			</td>
		  </tr>
	      <tr>
		    <td class="prompt" width="140" height="25">
               <bean:message key="customerEntry.prompt.description"/>
            </td>
		    <td width="435" height="25" class="control" colspan="3">
		       <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
		    </td>
		  </tr>
		  <tr>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.creditLimit"/>
			</td>
			<td width="147" height="25" class="control">
				<html:text property="creditLimit" size="25" maxlength="100" styleClass="textAmount" disabled="true"/>
			</td>
			<td class="prompt" width="140" height="25">
				<bean:message key="customerEntry.prompt.dealPrice"/>
			</td>
			<td width="147" height="25" class="control">
				<html:select property="dealPrice" styleClass="combo" disabled="true">
               		<html:options property="dealPriceList"/>
        		</html:select>
			</td>
		  </tr>
		 <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Address Info">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
		          <tr>
				    <td class="prompt" width="575" height="20" colspan="4">
		            </td>
		          </tr>
		          <tr>
				    <td class="prompt" width="140" height="25">
				    <bean:message key="customerEntry.prompt.address"/>
				    </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="address" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.city"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:text property="city" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
		          </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.stateProvince"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="stateProvince" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.postalCode"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="postalCode" size="10" maxlength="10" styleClass="text" disabled="true"/>
				    </td>
				 </tr>

				 <tr>
			       	<td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.region"/>
		            </td>
				    <td width="147" height="25" class="control" >
		               <html:select property="region" styleClass="combo" disabled="true">
		               	<html:options property="regionList"/>
		               </html:select>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.country"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="country" size="25" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				 </tr>
				</table>
			    </div>
                <div class="tabbertab" title="Contact Info">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
			      <tr>
				    <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.birthday"/>
		            </td>
				    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="birthday" size="10" maxlength="10" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.contact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="contact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.phone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="phone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.mobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="mobilePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="alternateContact" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternatePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternatePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
			     <tr>
			        <td class="prompt" width="140" height="25">
		            </td>
				    <td width="148" height="25" class="control">
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.alternateMobilePhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="alternateMobilePhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				  </tr>
				  <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.fax"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="fax" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.email"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="email" size="20" maxlength="150" styleClass="text" disabled="true"/>
				    </td>
			      </tr>
				</table>
			    </div>
                <div class="tabbertab" title="Billing/Shipping">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAddress"/>
		            </td>
				    <td width="148" height="25" class="control">
				       <html:textarea property="billToAddress" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAddress"/>
		            </td>
				    <td width="147" height="25" class="control">
				       <html:textarea property="shipToAddress" cols="20" rows="4" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToAltContact"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToAltContact"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
                <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billToPhone"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="billToPhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.shipToPhone"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="shipToPhone" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingSignatory"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:text property="billingSignatory" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.signatoryTitle"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:text property="signatoryTitle" size="20" maxlength="100" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader2"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter2"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
				<tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingHeader3"/>
		            </td>
				    <td width="148" height="25" class="control" colspan="3">
		               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
				</tr>
			    <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.billingFooter3"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
				    </td>
			    </tr>
			</table>
	    </div>
		    <div class="tabbertab" title="Accounts">
		       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('receivableAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.receivableDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="receivableDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>
				   <tr>
				     <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','');" />
			         </td>
			         </logic:equal>
					 <logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
			         <td class="prompt" width="140" height="25">
			            <bean:message key="customerEntry.prompt.revenueAccount"/>
			         </td>
			         <td width="435" height="25" class="control" colspan="3">
			            <html:text property="revenueAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccount','');" disabled="true"/>
			         </td>
			         </logic:equal>
				   </tr>
				   <tr>
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.revenueDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="revenueDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>




				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unEarnedInterestAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="unEarnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>
				   <tr>


				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestAccount"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
		               <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('earnedInterestAccount','');" disabled="true"/>
				     </td>
				   </tr>
				   <tr class="interestAccount">
			         <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
		             </td>
				     <td width="435" height="25" class="control" colspan="3">
		               <html:text property="earnedInterestDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				     </td>
				   </tr>

                                   //continue
				   <tr>

				</table>
			    </div>
	            <div class="tabbertab" title="Notes">
			       <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       <tr>
				     <td class="prompt" width="575" height="20" colspan="4">
		             </td>
		           </tr>
                   <tr>
			        <td class="prompt" width="140" height="25">
		               <bean:message key="customerEntry.prompt.memo"/>
		            </td>
				    <td width="147" height="25" class="control" colspan="3">
		               <html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    </td>
			       </tr>
			       </table>
			    </div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="575" height="20" colspan="4">
						</td>
					</tr>
					<nested:iterate property="bcstList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="branchName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.receivableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchReceivableAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchReceivableAccount','branchReceivableDescription');" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.receivableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchReceivableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>
					<tr>
						<td></td>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="true">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');"/>
						</td>
						</logic:equal>
						<logic:equal name="arCustomerEntryForm" property="enableRevenueAccount" value="false">
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="customerEntry.prompt.revenueAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchRevenueAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccount','branchRevenueDescription');" disabled="true"/>
						</td>
						</logic:equal>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="customerEntry.prompt.revenueDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchRevenueDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>



					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.receivableAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchReceivableAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchReceivableAccount','branchReceivableDescription');" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.receivableDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchReceivableDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>













					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedInterestAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedInterestAccount','branchUnEarnedInterestDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>



					<tr class="interestAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedInterestAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedInterestAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedInterestAccount','branchEarnedInterestDescription');" disabled="true"/>
						</td>
					</tr>
					<tr class="interestAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedInterestDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedInterestDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true" />
						</td>
					</tr>







					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.unEarnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchUnEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text"  disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchUnEarnedPenaltyAccount','branchUnEarnedPenaltyDescription');"  disabled="true"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.unEarnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchUnEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"  disabled="true"/>
						</td>
					</tr>


					<tr class="penaltyAccount">
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="customerEntry.prompt.earnedPenaltyAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchEarnedPenaltyAccount" size="30" maxlength="255" styleClass="text"  disabled="true"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchEarnedPenaltyAccount','branchEarnedPenaltyDescription');"  disabled="true"/>
						</td>
					</tr>
					<tr class="penaltyAccount">
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="customerEntry.prompt.earnedPenaltyDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
						   <nested:text property="branchEarnedPenaltyDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"  disabled="true"/>
						</td>
					</tr>




















		            </nested:iterate>
				</table>
		        </div>
			  </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
	     <tr>
	        <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
		      <%--  <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">



               <html:submit property="saveButton" styleClass="mainButton" >
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">


               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal> --%>






		       <logic:equal name="arCustomerEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveButton" styleClass="mainButton">
		            	<bean:message key="button.save"/>
		       		</html:submit>


		       		</logic:equal>



		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arCustomerEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
            </td>
	     </tr>
	     <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["customerCode"] != null &&
        document.forms[0].elements["customerCode"].disabled == false)
        document.forms[0].elements["customerCode"].focus()
	       // -->
</script>



<script language=JavaScript type=text/javascript>
    showInterestAccount();
    showPenaltyAccount();
    function showInterestAccount(){

        d =  document.forms[0].elements["autoComputeInterest"].checked;

        if(d){

            var x = document.getElementsByClassName("interestAccount");
            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "visible";

               x[i].style.display = "table-row";
            }


        }else{

            var x = document.getElementsByClassName("interestAccount");

            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "hidden";

                x[i].style.display = "none";
            }

        }

    }

    function showPenaltyAccount(){

        d =  document.forms[0].elements["autoComputePenalty"].checked;

        if(d){

            var x = document.getElementsByClassName("penaltyAccount");
            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "visible";

               x[i].style.display = "table-row";
            }


        }else{

            var x = document.getElementsByClassName("penaltyAccount");

            var i;
            for (i = 0; i < x.length; i++) {

                x[i].style.visibility = "hidden";

                x[i].style.display = "none";
            }

        }

    }



</script>
</body>
</html>
