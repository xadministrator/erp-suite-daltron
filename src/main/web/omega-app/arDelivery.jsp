<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="delivery.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 

</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/arDelivery.do" onsubmit="return submitForm();" >
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  
  
  <html:hidden property="soCode"/>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="delivery.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="arDeliveryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	        <td class="prompt" width="200" height="25">
	           <bean:message key="delivery.prompt.soDocumentNumber"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="soDocumentNumber" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	        
         </tr>   
         <tr>
           <td class="prompt" width="200" height="25">
	           <bean:message key="delivery.prompt.soReferenceNumber"/>
	        </td>
	        <td width="147" height="25" class="control">
			   <html:text property="soReferenceNumber" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>	
         
         <tr>
           <td class="prompt" width="200" height="25">
	           <bean:message key="delivery.prompt.customerName"/>
	        </td>
	        <td width="147" height="25" class="control">
			   <html:text property="cstCustomerCode" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>	
         
         	<tr>
			<td width="130" height="25" class="prompt">
             				<bean:message key="salesOrderEntry.prompt.transactionStatus"/>
          			</td>

          			<td width="158" height="25" class="control">
             				<html:select property="transactionStatus" styleClass="combo" style="width:130;" >
                 			<html:options property="transactionStatusList"/>
             				</html:select>
             			</td>
		</tr>
         
       	<tr>
			<td class="prompt" width="140" height="25">
            <bean:message key="sales.prompt.viewType"/>
          </td>
          <td width="128" height="25" class="control">
            <html:select property="viewType" styleClass="comboRequired">
		      <html:options property="viewTypeList"/>
		   </html:select>
          </td>
		</tr>
        
        
         <tr>
	       <td width="575" height="50" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton">
		         Open SO
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		    <div id="buttonsDisabled" style="display: none;">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		   <td>
         </tr>	 

	     <tr>
            <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
            </td>
         </tr>
	     <tr>
	        <td width="575" height="5" colspan="4">
		    </td>
	     </tr>
	     <tr>    
	     
	     	<td width="50" height="50" colspan="1"> 
	     	   <div id="buttons">
		          <p align="left">
		          <logic:equal name="arDeliveryForm" property="showAddLinesButton" value="true">
		            <html:submit property="deleteButton" styleClass="mainButtonMedium" onclick="return confirmDelete();">
		              <bean:message key="button.deleteSelected"/>
			       </html:submit>
			       </logic:equal>
		        </p>
		        </div>
     	   </td>
	     
     	   <td width="50" height="50" colspan="1"> 
	     	   <div id="buttons">
		          <p align="left">
		            <html:submit property="printAllButton" styleClass="mainButtonMedium" >
		              <bean:message key="button.printAll"/>
			       </html:submit>
		        </p>
		        </div>
     	   </td>
	     
	     
	     	   
	     	       	      	     
	        <td width="50" height="50" colspan="2"> 
	         
		       <div id="buttons">
	           <p align="right">
	           <html:submit property="saveButton" styleClass="mainButton" >
	              <bean:message key="button.save"/>
		       </html:submit>
              
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	     
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </div>
		    </td>
	     </tr>
	
	    
	     
	     <tr valign="top">
	         
	     	<html:hidden property="arDVListSize"/>
	        <td width="575" height="185" colspan="7">
	        <nested:notEqual property="arDVListSize" value="0">
		    <div align="center" style="width:575px; height:480px; overflow-x:scroll; position:relative; overflow-y:scroll; position:relative; ">
		         <table border="1" cellpadding="0" cellspacing="0" width="1000" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>" >
			     <thead>                 	                    
			  
			    <tr>
                	<td width="575" height="1" colspan="14" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>" style="position: sticky; position: -webkit-sticky; top: 0;  z-index: 999;">
	                          <bean:message key="delivery.gridTitle.DVDetails"/>
	                </td>
	            </tr>	   
	             <tr>
			    	<td width="20" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;  z-index: 999;">
			          <bean:message key="delivery.prompt.selected"/>
			       </td>
			        <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15; left: 1;z-index: 999;" >
			          <bean:message key="delivery.prompt.deliveryNumber"/>
			       </td>
			    	<td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15; left: 137;z-index: 999;">
			          <bean:message key="delivery.prompt.container"/>
			       </td>
			       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.bookingTime"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.tabsFeePullOut"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.releasedDate"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.deliveredDate"/>
			       </td>			       			       
			       <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.deliveredTo"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.plateNumber"/>
			       </td>
			        <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.driverName"/>
			       </td>	
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.emptyReturnDate"/>
			       </td>	
			       <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.emptyReturnTo"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 8;">
			          <bean:message key="delivery.prompt.tabsFeeReturn"/>
			       </td>
			       
			       <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" style="position: sticky; position: -webkit-sticky; top: 15;z-index: 999;">
			          <bean:message key="delivery.prompt.status"/>
			       </td>		       			       			       
                </tr>
     
                <tr>
                   <td width="50" height="1" colspan="12" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" >
			          <bean:message key="delivery.prompt.remarks"/>
			       </td>
                </tr>
                </thead>
              
                
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			   
			    <nested:iterate property="arDVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <nested:hidden property="dvCode"/>
			    
			    <tr bgcolor="<%= rowBgc %>">	
			        <td width="100" height="1" rowspan="2" class="control">
			          <nested:checkbox property="selectCheckbox" />
			          
			          	<nested:notEqual property="dvCode" value="">
				          <nested:submit property="printButton" styleClass="mainButton" >
					   			<bean:message key="button.print"/>
					   		</nested:submit>
				   		</nested:notEqual>
				   		
				   		<nested:equal property="dvCode" value="">
				          <nested:submit property="printButton" styleClass="mainButton" disabled="true">
					   			<bean:message key="button.print"/>
					   		</nested:submit>
				   		</nested:equal>
			           
			       </td>
			        <td width="100" height="1" class="control" style="position: sticky; position: -webkit-sticky; left: 1;z-index: 999;">
			          <nested:text property="dvDeliveryNumber" styleClass="text" disabled="true" />  
			        </td>
			     
			       <td width="100" height="1" class="control" style="position: sticky; position: -webkit-sticky; left: 137;z-index: 999;">
			       	   <nested:text property="dvContainer" styleClass="textRequired"/>
			       </td>
			       
			          <td width="100" height="1" class="control">
			          <nested:text property="dvBookingTime" styleClass="text"/>  
			          </td>
			        <td width="100" height="1" class="control">
			           <nested:text property="dvTabsFeePullOut" styleClass="text"/>
			           </td>
			        <td width="100" height="1" class="control">
			          <nested:text property="dvReleasedDate" styleClass="text"/>
			           </td>
			        <td width="100" height="1" class="control">
			          <nested:text property="dvDeliveredDate" styleClass="text"/>
			           </td>
			        <td width="100" height="1" class="control">
			        
			         <nested:text property="dvDeliveredTo" styleClass="textRequired" />
			          
		             </td>
			        <td width="100" height="1" class="control">
			          <nested:text property="dvPlateNo" styleClass="text"/>
			        </td>
			         <td width="100" height="1" class="control">
			          <nested:text property="dvDriverName" styleClass="text"/>
			        </td>
			         <td width="100" height="1" class="control">
			          <nested:text property="dvEmptyReturnDate" styleClass="text"/>
			        </td>
			         <td width="100" height="1" class="control">
			          <nested:text property="dvEmptyReturnTo" styleClass="text"/>
			        </td>
			         <td width="100" height="1" class="control">
			          <nested:text property="dvTabsFeeReturn" styleClass="text"/>
			        </td>
			         <td width="100" height="1" class="control">
			         
			         	<nested:select property="dvStatus" styleClass="combo" >
                              <nested:options property="dvStatusList"/>
                          </nested:select>
			         
			       
			        </td>
			        
			    </tr>	
			   
			    
			    <tr>
			    			   
				       <td width="270" height="1" class="control" colspan="14">
				          <nested:textarea property="dvRemarks" cols="45" rows="5"  styleClass="text" style="font-size:8pt;"/>
				       </td>
			    </tr>
			    				    
			  </nested:iterate>
			  </table>
		      </div>
		      
		       </nested:notEqual>
		      </td>
	       </tr>
     
	    
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arDeliveryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			      
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arDeliveryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			      
		           </div>
				  </td>
	           </tr>	
			 </table>
		    </div>
		  </td>
	     </tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arDeliveryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arDeliveryForm" type="com.struts.ar.delivery.ArDeliveryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepDeliveryPrint.do?forward=1&soCode=<%=actionForm.getReportSoCode()%>&viewType=<%=actionForm.getViewType()%>&dvCode=<%=actionForm.getReportDvCode()%>&reportType=<%=actionForm.getReportType()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<script language=JavaScript type=text/javascript>
 
      
</script>
</body>
</html>
