<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

  function closeWin()
  {
    var ret = new Object();
    ret.status = 0;       
    window.returnValue = ret;
    window.close();
    return false;
	   
  }
  
  function returnInvoiceDiscount()
  {
    okButton = 1;
    var ret = new Object();
    ret.status = 1;
    ret.discount1 = document.forms[0].elements["discount1"].value;
    ret.discount2 = document.forms[0].elements["discount2"].value;
    ret.discount3 = document.forms[0].elements["discount3"].value;
    ret.discount4 = document.forms[0].elements["discount4"].value;
    window.returnValue = ret;
	window.close();
	return false;       
  }
  
  
//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('okButton'));">
<form name="arInvoiceDiscountForm">
<table border="0" cellpadding="0" cellspacing="0" width="250" height="80">
      <tr valign="top">
        <td width="250" height="80">
          <table border="0" cellpadding="0" cellspacing="0" width="250" height="80" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
		        <td width="250" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		            <bean:message key="invoiceEntry.invoiceDiscount"/>
			    </td>
		    </tr>
		    <tr>
	          <td width="150" height="25" colspan="2" class="statusBar">
	          </td>
	        </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="invoiceEntry.prompt.discount1"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount1" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="invoiceEntry.prompt.discount2"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount2" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="invoiceEntry.prompt.discount3"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount3" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="invoiceEntry.prompt.discount4"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount4" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
	         <td width="400" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" onclick="return returnInvoiceDiscount();">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" onclick="return closeWin();">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
			  </td>
	        </tr> 
		  </table>
        </td>
      </tr>
</table>
</form>
<script language=JavaScript type=text/javascript>
  <!--
     var myObject = window.dialogArguments;
     
     if (myObject) {
         
	     document.forms[0].elements["discount1"].value = myObject.discount1;
	     document.forms[0].elements["discount2"].value = myObject.discount2;
	     document.forms[0].elements["discount3"].value = myObject.discount3;
	     document.forms[0].elements["discount4"].value = myObject.discount4;

	 }
     
	       // -->
</script>
</body>
</html>
