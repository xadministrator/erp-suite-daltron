<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findCustomer.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickAccount(name) 
{
   
   var property = name.substring(0,name.indexOf("."));     
   var findType = document.forms[0].elements["sourceType"].value;
      
        if (window.opener && !window.opener.closed) {
    
        if(findType=="memoLineClass"){
            
            var propertyLocation = document.forms[0].elements["selectedLocation"].value.replace(".","");

            window.opener.document.forms[0].elements[propertyLocation + ".customerCode"].value =  document.forms[0].elements[property + ".customerCode"].value;
   
            window.opener.document.forms[0].elements[propertyLocation + ".isCustomerEntered"].value = true;
  
            window.opener.document.forms[0].submit();
            

        }else{
            window.opener.document.forms[0].elements[document.forms[0].elements["selectedCustomerCode"].value].value = 
            document.forms[0].elements[property + ".customerCode"].value;                         
                               
            if (document.forms[0].elements["selectedCustomerName"] != null &&
                document.forms[0].elements["selectedCustomerName"].value != "" &&
                window.opener.document.forms[0].elements[document.forms[0].elements["selectedCustomerName"].value] != null) {   

                window.opener.document.forms[0].elements[document.forms[0].elements["selectedCustomerName"].value].value = 
                    document.forms[0].elements[property + ".name"].value;         

            }

            if (document.forms[0].elements["selectedCustomerEntered"] != null &&
                document.forms[0].elements["selectedCustomerEntered"].value != "" &&
                window.opener.document.forms[0].elements[document.forms[0].elements["selectedCustomerEntered"].value] != null) {

                var divList = window.opener.document.body.getElementsByTagName("div");

                    for (var i = 0; i < divList.length; i++) {

                       var currDiv = divList[i];
                       var currId = currDiv.id;

                       if (currId == "buttons") {

                           currDiv.style.display = "none";

                       } else if (currId == "buttonsDisabled") {

                           currDiv.style.display = "block";

                       }

                    }

                        for (var i=0; i < window.opener.document.forms[0].elements.length; i++) {

                           if (window.opener.document.forms[0].elements[i].type != 'submit') {

                               window.opener.document.forms[0].elements[i].disabled=false;

                           }
                        }
                window.opener.document.forms[0].elements[document.forms[0].elements["selectedCustomerEntered"].value].value = true;           
                window.opener.document.forms[0].submit();
             }
        

       
                      
       }

     
   }
   document.forms[0].elements["sourceType"].value = "";
   window.close();
   
   return false;

}

function closeWin() 
{
   window.close();
   
   return false;
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arFindCustomer.do?child=1" onsubmit="return disableButtons();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findCustomer.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arFindCustomerForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="selectedCustomerCode"/>
	     <html:hidden property="selectedCustomerName"/>
	     <html:hidden property="selectedCustomerEntered"/>
             <html:hidden property="selectedLocation"/>
             <html:hidden property="sourceType"/>  
	     <tr>
	     	<td width="575" height="10" colspan="4">
	     	
     			<tr>
	             	<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.customerBatch"/>
					</td>
					<td width="147" height="25" class="control">
						<html:select property="customerBatch" styleClass="combo">
							<html:options property="customerBatchList"/>
						</html:select>
					</td>
	             </tr>
	             <tr>
			         <td class="prompt" width="130" height="25">
            			<bean:message key="findCustomer.prompt.customerCode"/>
			         </td>
         			<td width="435" height="25" class="control" colspan="3">
			            <html:text property="customerCode" size="25" maxlength="25" styleClass="text"/>
         			</td>
		          </tr>
		         <tr>
        			<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.name"/>
					</td>
					<td width="435" height="25" class="control" colspan="3">
						<html:text property="name" size="35" maxlength="50" styleClass="text"/>
					</td>
				</tr>
				<tr>
        			<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.employeeId"/>
					</td>
					<td width="435" height="25" class="control" colspan="3">
						<html:text property="employeeId" size="35" maxlength="50" styleClass="text"/>
					</td>
				</tr>
     			<tr>
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.customerType"/>
					</td>
					<td width="148" height="25" class="control">
						<html:select property="customerType" styleClass="combo">
							<html:options property="customerTypeList"/>
						</html:select>
					</td>
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.customerClass"/>
					</td>
					<td width="147" height="25" class="control">
						<html:select property="customerClass" styleClass="combo">
							<html:options property="customerClassList"/>
						</html:select>
					</td>
				</tr>         
				<tr>
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.enable"/>
					</td>
					<td width="148" height="25" class="control">
						<html:checkbox property="enable"/>
					</td>
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.disable"/>
					</td>
					<td width="147" height="25" class="control">
						<html:checkbox property="disable"/>
					</td>	         
				</tr>
				<tr>
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.email"/>
					</td>
					<td width="147" height="25" class="control">
						<html:text property="email" size="25" maxlength="50" styleClass="text"/>
					</td>	     
					<td class="prompt" width="140" height="25">
						<bean:message key="findCustomer.prompt.orderBy"/>
					</td>
					<td width="148" height="25" class="control">
						<html:select property="orderBy" styleClass="combo">
							<html:options property="orderByList"/>
						</html:select>
					</td>	              
				</tr>
		 	</td>
		 </tr> 
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="arFindCustomerForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="arFindCustomerForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindCustomerForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" onclick="return closeWin();">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="findCustomer.gridTitle.FCDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.customerCode"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.name"/>
			       </td>
			       
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.employeeId"/>
			       </td>
			       
			       <td width="447" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.address"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.area"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findCustomer.prompt.salesPerson"/>
			       </td>		       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arFCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       			       
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="customerCode"/>
			          <nested:hidden property="customerCode"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="name"/>
			          <nested:hidden property="name"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="employeeId"/>
			          <nested:hidden property="employeeId"/>
			       </td>
			       <td width="447" height="1" class="gridLabel">
			          <nested:write property="address"/>
			          <nested:hidden property="address"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="area"/>
			          <nested:hidden property="area"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="salesPerson"/>
			          <nested:hidden property="salesPerson"/>
			       </td>		       		       
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="arFindCustomerForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arFCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="findCustomer.prompt.customerCode"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="customerCode"/>
			          <nested:hidden property="customerCode"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.name"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="name"/>
			         <nested:hidden property="name"/>
			      </td>		      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.tinNumber"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="tinNumber"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.email"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="email"/>
			      </td>
			    </tr>			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.customerType"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="customerType"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.customerClass"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="customerClass"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.paymentTerm"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="paymentTerm"/>
			      </td>				    
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findCustomer.prompt.enable"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="enable"/>
			      </td>		      
			    </tr>				    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null &&
        document.forms[0].elements["customerCode"].disabled == false)
        document.forms[0].elements["customerCode"].focus()
   // -->
</script>
</body>
</html>
