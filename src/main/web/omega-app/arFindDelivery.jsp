<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findDelivery.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arFindDelivery.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="800" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
		<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	    	<tr>
	        	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="findDelivery.title"/>
		        </td>
	      	</tr>
            <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="arFindDeliveryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>
	            </td>
	        </tr>
	        
	         <tr>
		        <td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.deliveryNumberFrom"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="deliveryNumberFrom" size="15" maxlength="25" styleClass="text"/>
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findDelivery.prompt.deliveryNumberTo"/>
                </td>
		        <td width="115" height="25" class="control">
	               <html:text property="deliveryNumberTo" size="15" maxlength="25" styleClass="text"/>
		        </td>
	        </tr>
	        <tr>
		        <td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.customerCode"/>
                </td>
				<logic:equal name="arFindDeliveryForm" property="useCustomerPulldown" value="true">
		        <td width="120" height="25" class="control">
	               <html:select property="customerCode" styleClass="combo">
                      <html:options property="customerCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
		        <logic:equal name="arFindDeliveryForm" property="useCustomerPulldown" value="false">
		        <td width="120" height="25" class="control">
	               <html:text property="customerCode" styleClass="text" size="12" maxlength="25" readonly="true"/>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
				<td class="prompt" width="160" height="25">
	               <bean:message key="findDelivery.prompt.salesOrderVoid"/>
	            </td>
			    <td width="115" height="25" class="control">
			       <html:checkbox property="salesOrderVoid"/>
			    </td>
             </tr>
             
              <tr>
		        <td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.container"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="container" size="15" maxlength="25" styleClass="text"/>
		        </td>
		      
	        </tr>
           
            <tr>
		        <td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.documentNumberFrom"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findDelivery.prompt.documentNumberTo"/>
                </td>
		        <td width="115" height="25" class="control">
	               <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
		        </td>
	        </tr>
	      
            <tr>
				<td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.referenceNumber"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>
		        </td>
		       
	        </tr>

	         <tr>
				<td class="prompt" width="180" height="25">
                   <bean:message key="findDelivery.prompt.transactionStatus"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:select property="transactionStatus" styleClass="combo">
			        <html:options property="transactionStatusList"/>
			      </html:select>
		        </td>

	        </tr>
	        <tr>
		       
		       <td class="prompt" width="160" height="25">
		          <bean:message key="findDelivery.prompt.posted"/>
		       </td>
		       <td width="115" height="25" class="control">
		          <html:select property="posted" styleClass="combo">
			        <html:options property="postedList"/>
			      </html:select>
		       </td>
	         </tr>
	         <tr>
	           <td class="prompt" width="180" height="25">
		           <bean:message key="findDelivery.prompt.orderBy"/>
		        </td>
		        <td width="390" height="25" class="control" colpsan="3">
		           <html:select property="orderBy" styleClass="combo">
		              <html:options property="orderByList"/>
			       </html:select>
		        </td>
	         </tr>
	       	<tr>
	        	<td width="575" height="50" colspan="4">
	         	<table border="0" cellpadding="0" cellspacing="0" width="575" height="50"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			    	<tr>
			        	
					  	<td width="285" height="50" colspan="3">
			            	<div id="buttons">
			             	<p align="right">
				         	<html:submit property="goButton" styleClass="mainButton">
				         		<bean:message key="button.go"/>
				         	</html:submit>
				         	<html:submit property="closeButton" styleClass="mainButton">
				         		<bean:message key="button.close"/>
				         	</html:submit>
				         	</div>
				         	<div id="buttonsDisabled" style="display: none;">
			             	<p align="right">
				         	<html:submit property="goButton" styleClass="mainButton" disabled="true">
				         		<bean:message key="button.go"/>
				         	</html:submit>
				         	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         		<bean:message key="button.close"/>
				         	</html:submit>
				         	</div>
				      	</td>
			     	</tr>
	         	</table>
	         	</td>
	     	</tr>
	       	<tr valign="top">
	        	<td width="575" height="180" colspan="6">
		        <div align="center" style="width:575px; overflow-x:scroll; position:relative;" >
				<table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		        	<tr>
                		<td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
							<bean:message key="findDelivery.gridTitle.FDVDetails"/>
	               		</td>
	          		</tr>
					<logic:equal name="arFindDeliveryForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	            	<tr>
	            	
	            		<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		
			       		</td>
	            	
			    		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findDelivery.prompt.deliveryNumber"/>
			       		</td>
			       		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findDelivery.prompt.container"/>
			       		</td>
			       		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findDelivery.prompt.documentNumber"/>
			       		</td>
                   		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			        		<bean:message key="findDelivery.prompt.referenceNumber"/>
			       		</td>
			       		<td width="134" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			                <bean:message key="findDelivery.prompt.customerCode"/>
        			    </td>
                	</tr>
	            	

					<%
			    		int i = 0;
			        	String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			        	String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			        	String rowBgc = null;
			    	%>
			    	<nested:iterate property="arFDVList">
			    	<%
			    		i++;
			        	if((i % 2) != 0){
			        		rowBgc = ROW_BGC1;
			        	}else{
			        		rowBgc = ROW_BGC2;
			        	}
			   		%>
			    	<tr bgcolor="<%= rowBgc %>">
			    	
			    		<td width="100" align="center" height="1">
			        		<div id="buttons">
			            	<nested:submit property="openButton" styleClass="gridButton">
	                    		<bean:message key="button.open"/>
  	                    	</nested:submit>
  	                    	</div>
  	                    	<div id="buttonsDisabled" style="display: none;">
			            	<nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                    		<bean:message key="button.open"/>
  	                   		</nested:submit>
  	                    	</div>
			      		</td>
			    		<td width="134" height="1" class="gridLabel">
			        		<nested:write property="deliveryNumber"/>
			        	</td>
						<td width="134" height="1" class="gridLabel">
			        		<nested:write property="container"/>
			        	</td>
			        	<td width="134" height="1" class="gridLabel">
			        		<nested:write property="documentNumber"/>
			       		</td>
			        	<td width="134" height="1" class="gridLabel">
			        		<nested:write property="referenceNumber"/>
			       		</td>
			       		<td width="134" height="1" class="gridLabel">
			        		<nested:write property="customerCode"/>
			       		</td>
			       		
			  		</tr>
			    
			    	</nested:iterate>
               		</logic:equal>
		      	

				</table>
		    	</div>
		        </td>
	      	</tr>
	       	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		        </td>
	    	</tr>

        </table>
		</td>
    </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["documentNumberFrom"] != null &&
        document.forms[0].elements["documentNumberFrom"].disabled == false)
        document.forms[0].elements["documentNumberFrom"].focus()
   // -->
</script>
</body>
</html>

