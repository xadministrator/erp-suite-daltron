<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
<%@ page import="com.struts.util.Common" %>
<%@ page import="com.struts.util.Constants" %>


<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findInvoice.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function formatStr(name,maxchars)
{
   var property = name.substring(0,name.indexOf("."));

   var customerName = document.forms[0].elements[property + ".customerName"].value

   strbuff=cstName.innerHTML;
   newstr='';
   startI = 0;
   max=maxchars;
   str='';
   subarr=new Array(parseInt(strbuff.length/max+1));
   for (i=0;i<subarr.length;i++)
   {
      subarr[i]=strbuff.substr(startI,max);
      startI+=max;
   }
   for (i=0;i<subarr.length-1;i++)
   {
      newstr+=subarr[i]+'<br />';
   }
   str+=subarr[subarr.length-1];
   cstName.innerHTML=newstr;
}

//Done Hiding-->

function deleteAllLine(){

	if(confirm("Are you sure you want to delete all lines")){
	return true;
	}else{
	return false;}
}


</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arFindInvoice.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="785" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="600" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="600" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="findInvoice.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="arFindInvoiceForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>
		        <html:messages id="msg" message="true">
		        	<font color="green"><bean:write name="msg"/></font><br>
		        </html:messages>
	            </td>
	         </tr>
	         <logic:equal name="arFindInvoiceForm" property="showBatchName" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findInvoice.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
	         </tr>
	         </logic:equal>
	         <tr>
	            <td width="160" height="25" class="prompt">
                   <bean:message key="findInvoice.prompt.invoiceType"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="invoiceType" styleClass="combo">
                       <html:options property="invoiceTypeList"/>
                   </html:select>
                </td>
	         </tr>



	         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findInvoice.prompt.customerBatch"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="customerBatch" styleClass="combo">
                       <html:options property="customerBatchList"/>
                   </html:select>
                </td>
	         </tr>
	         <logic:equal name="arFindInvoiceForm" property="showShift" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findInvoice.prompt.shift"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="shift" styleClass="combo">
                       <html:options property="shiftList"/>
                   </html:select>
                </td>
	         </tr>
	         </logic:equal>
	         <tr>


		        <td class="prompt" width="160" height="25">
                   <bean:message key="findInvoice.prompt.customerCode"/>
                </td>
				<logic:equal name="arFindInvoiceForm" property="useCustomerPulldown" value="true">
		        <td width="415" height="25" class="control" colspan="3">
	               <html:select property="customerCode" styleClass="combo">
                      <html:options property="customerCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>

             </tr>
             <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.creditMemo"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:checkbox property="creditMemo"/>
		        </td>
		        <td class="prompt" width="160" height="25">
	               <bean:message key="findInvoice.prompt.invoiceVoid"/>
	            </td>
			    <td width="127" height="25" class="control">
			       <html:checkbox property="invoiceVoid"/>
			    </td>
             </tr>

             <tr>

            	 <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.interest"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:checkbox property="interest"/>
		        </td>
             </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.dateFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findInvoice.prompt.dateTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
		        </td>
	         </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.invoiceNumberFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="invoiceNumberFrom" size="15" maxlength="25" styleClass="text"/>
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findInvoice.prompt.invoiceNumberTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="invoiceNumberTo" size="15" maxlength="25" styleClass="text"/>
		        </td>
	         </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.referenceNumber"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="findInvoice.prompt.approvalStatus"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:select property="approvalStatus" styleClass="combo">
	                  <html:options property="approvalStatusList"/>
		           </html:select>
		        </td>
	         </tr>
	         
	         <tr>
	         	<td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.serialNumber"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="serialNumber" size="25" maxlength="25" styleClass="text"/>
		        </td>
	         </tr>
	         <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="findInvoice.prompt.currency"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="findInvoice.prompt.posted"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:select property="posted" styleClass="combo">
			        <html:options property="postedList"/>
			      </html:select>
		       </td>
	         </tr>
	         <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="findInvoice.prompt.paymentStatus"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="paymentStatus" styleClass="combo">
			        <html:options property="paymentStatusList"/>
			      </html:select>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="findInvoice.prompt.orderBy"/>
		       </td>
		       <td width="124" height="25" class="control" colspan="3">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>
	        </tr>
	       <tr>
	         <td width="600" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="600" height="50"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="350" height="50">
			             <div id="buttons">
			             <logic:equal name="arFindInvoiceForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>

					     <logic:equal name="arFindInvoiceForm" property="disableDeleteAllButton" value="false">
			             <html:submit property="deleteAllButton" styleClass="mainButtonMedium" onclick="return deleteAllLine()">
			             	Delete All
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableDeleteAllButton" value="true">
			             <html:submit property="deleteAllButton" styleClass="mainButtonMedium" disabled="true" onclick="return deleteAllLine()">
			             	Delete All
			             </html:submit>
			             </logic:equal>



					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="arFindInvoiceForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindInvoiceForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>

					     <logic:equal name="arFindInvoiceForm" property="disableDeleteAllButton" value="false">
			             <html:submit property="deleteAllButton" styleClass="mainButtonMedium" disabled="true">
			             	Delete All
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindInvoiceForm" property="disableDeleteAllButton" value="true">
			             <html:submit property="deleteAllButton" styleClass="mainButtonMedium" disabled="true">
			             	Delete All
			             </html:submit>
			             </logic:equal>

					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
			             <%  if (Common.getUserPermission(sideBarUser, Constants.AR_CREDIT_MEMO_ENTRY_ID) != null) { %>
			             	<logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">

			             		 <html:submit property="creditMemoButton" styleClass="mainButtonMedium">
						         	<bean:message key="button.creditMemo"/>
						         </html:submit>
						    </logic:equal>
					  		<% } %>
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
			              	<logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">

			               		<html:submit property="creditMemoButton" styleClass="mainButtonMedium" disabled="true" disabled="true">
						        	 <bean:message key="button.creditMemo"/>
						        </html:submit>
			            	</logic:equal>

				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				      </td>



			     </tr>
	         </table>
	         </td>
	     </tr>
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>"
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="8" class="gridTitle"
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="findInvoice.gridTitle.FIDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			    <tr>

			       <logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">
			     	<td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.selectCreditMemo"/>
			       </td>
			   	   </logic:equal>

			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.invoiceType"/>
			       </td>

			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.date"/>
			       </td>

			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.invoiceNumber"/>
			       </td>
			       <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.customerName"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.amountDue"/>
			       </td>

			       <td width="244" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findInvoice.prompt.amountPaid"/>
			       </td>
                </tr>
			          <%
			             int i = 0;
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arFIList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }
			          %>
			          <tr bgcolor="<%= rowBgc %>">

			          	 <logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">


			          	 <td width="132" height="1" class="gridLabel" rowspan="2">

			          	    <nested:equal property="invoiceType" value="MEMO LINES">
				          	 	<nested:equal property="ifPosted" value="true">
				               	 	<nested:checkbox property="selectCreditMemo"/>
				                </nested:equal>

				                <nested:equal property="ifPosted" value="false">
				                	<nested:checkbox property="selectCreditMemo" disabled="true"/>
				                </nested:equal>
			                </nested:equal>
			                <nested:equal property="invoiceType" value="ITEMS">
			                	<nested:checkbox property="selectCreditMemo" disabled="true"/>
			                </nested:equal>
			             </td>

			          	 </logic:equal>

			          	 <td width="132" height="1" class="gridLabel">
			                <b><nested:write property="invoiceType"/></b>
			             </td>

			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>

			             <td width="140" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>
			             <td width="250" height="1" class="gridLabel">
							<nested:write property="customerName"/>
			             </td>
			              <td width="140" height="1" class="gridLabel">
			                <nested:write property="amountDue"/>
			             </td>

			             <td width="144" height="1" class="gridLabelNum">
			                <nested:write property="amountPaid"/>
			             </td>
			             <td width="100" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			          </tr >
			          <logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">
			          <tr bgcolor="<%= rowBgc %>">



			          		<nested:equal property="invoiceType" value="MEMO LINES">

					          <nested:equal property="ifPosted" value="true">
						           <td width="100" height="1" colspan="2" class="gridHeader">
						           		<bean:message key="findInvoice.prompt.creditMemoAmount"/>
									</td>
									<td colspan="2">
									 	 <nested:text property="creditMemoAmount" size="15" maxlength="25" styleClass="text"/>
							       </td>


							       <logic:equal name="arFindInvoiceForm" property="showBatchName" value="true">

						                <td width="100" height="1" class="gridHeader">
						                   <bean:message key="findInvoice.prompt.batchName"/>
						                </td>
						                <td width="415" height="25" class="control" colspan="4">
						                   <nested:select property="invoiceBatch" styleClass="combo">
						                       <html:options property="cmBatchNameList"/>
						                   </nested:select>
						                </td>

						          </logic:equal>
					          </nested:equal>

					          <nested:equal property="ifPosted" value="false">
					          		<td width="100" height="1" colspan="7" class="gridHeader">
					          			UNPOSTED
					          		</td>
					          </nested:equal>

			           		</nested:equal>
			          		<nested:equal property="invoiceType" value="ITEMS">
			                	<td width="100" height="1" colspan="7" class="gridHeader">

				          		</td>
			                </nested:equal>

			          <tr>
			          </logic:equal>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="arFindInvoiceForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			          <%
			             int i = 0;
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arFIList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }
			          %>
			          <logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">

			          <tr bgcolor="<%= rowBgc %>" >
			          	   <td width="175" height="1" class="gridHeader" rowspan="5">

			          	   		<nested:equal property="invoiceType" value="MEMO LINES">
					                <bean:message key="findInvoice.prompt.selectCreditMemo"/>
					                <br>

					                <nested:equal property="ifPosted" value="true">
					                 	<nested:checkbox property="creditMemo"/>
					                </nested:equal>

					                 <nested:equal property="ifPosted" value="false">
					                 	<nested:checkbox property="creditMemo" disabled="true"/>
					                </nested:equal>
				             	</nested:equal>


				           		<nested:equal property="invoiceType" value="ITEMS">
				                	<nested:checkbox property="selectCreditMemo" disabled="true"/>
				                </nested:equal>
			         	   </td>



			          </tr>
			          </logic:equal>

			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findInvoice.prompt.customerName"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="customerName"/>
			             </td>
			             <td width="149" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findInvoice.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="findInvoice.prompt.invoiceNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>
                      </tr>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="findInvoice.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="findInvoice.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>
			          </tr>
					  <tr bgcolor="<%= rowBgc %>">

					    <logic:equal name="arFindInvoiceForm" property="creditMemo" value="false">
					   		 <nested:equal property="invoiceType" value="MEMO LINES">
							  	<nested:equal property="ifPosted" value="true">
								  	 <td width="175" height="1" class="gridHeader">
					                     <bean:message key="findInvoice.prompt.creditMemoAmount"/>
					                 </td>
					                 <td width="350" height="1" class="gridLabelNum" colspan="1">
					                 	 <nested:text property="creditMemoAmount" size="25" maxlength="25" styleClass="text"/>
					                 </td>
				                </nested:equal>
				                <nested:equal property="ifPosted" value="false">
					                 <td width="175" height="1" class="gridHeader">
					                     Unposted
					                 </td>
				                </nested:equal>
			                 </nested:equal>
			                <nested:equal property="invoiceType" value="ITEMS">
				                 <td width="175" height="1" class="gridHeader">

				                 </td>
			                </nested:equal>
					    </logic:equal>


		                 <td width="175" height="1" class="gridHeader">
		                     <bean:message key="findInvoice.prompt.amountPaid"/>
		                 </td>
		                 <td width="350" height="1" class="gridLabelNum" colspan="1">
		                     <nested:write property="amountPaid"/>
		                 </td>
		                 <td width="369" height="1" class="gridLabelNum" colspan="2"/>
		              </tr>
			          </nested:iterate>
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null)
             document.forms[0].elements["customerCode"].focus()
   // -->
</script>
</body>
</html>
