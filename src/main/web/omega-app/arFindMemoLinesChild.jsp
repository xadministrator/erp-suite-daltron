<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="standardMemoLine.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickAccount(name) 
{

   	var property = name.substring(0,name.indexOf("."));  

   	var selectedItemProperty = document.forms[0].elements["selectedItemName"].value.substring(0,name.indexOf(".")); 
 
	if (window.opener && !window.opener.closed) {   

            
            if(document.forms[0].elements["sourceType"].value=="memoLineClass"){
               
                selectedItemProperty = selectedItemProperty.replace(".","");
               
            
                window.opener.document.forms[0].elements[selectedItemProperty + ".memoLine"].value =  document.forms[0].elements[property + ".name"].value;
             
                window.opener.document.forms[0].elements[selectedItemProperty + ".isMemoLineEntered"].value = true;
                window.opener.document.forms[0].submit();
                
            }else{
                window.opener.document.forms[0].elements[document.forms[0].elements["selectedItemName"].value].value = 
                document.forms[0].elements[property + ".name"].value;
        
                  
                  selectedItemProperty = selectedItemProperty.replace(".","");
                  
                  window.opener.document.forms[0].elements[selectedItemProperty + ".isMemoLineEntered"].value = true;           
                  window.opener.document.forms[0].submit();
                //window.opener.document.forms[0].elements[selectedItemProperty + ".quantity"].value = 1;
               // window.opener.document.forms[0].elements[selectedItemProperty + ".unitPrice"].value = "0.00";
               // window.opener.document.forms[0].elements[selectedItemProperty + ".amount"].value = "0.00";  
            }
            
    
	}       
	window.close();
	   
	return false;

}


function closeWin() 
{
   window.close();
   
   return false;
}


//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arStandardMemoLines.do?child=1" onsubmit="return disableButtons();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="standardMemoLine.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arStandardMemoLineForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="selectedItemName"/>
             <html:hidden property="sourceType"/>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="standardMemoLine.gridTitle.SMLDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="94" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.type"/>
			       </td>
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.name"/>
			       </td>			       
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.accountNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.unitPrice"/>
			       </td>
				   <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.unitOfMeasure"/>
			       </td>			       
			       <td width="200" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.enable"/>
			       </td>			       			       			       			       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arSMLList">
			    <nested:hidden property="name" />			    
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="94" height="1" class="gridLabel">
			          <nested:write property="type"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="name"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="accountNumber"/>
			       </td>
			       <td width="110" height="1" class="gridLabelNum">
			          <nested:write property="unitPrice"/>
			       </td>			       
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="unitOfMeasure"/>
			       </td>
			       <td width="60" height="1" class="gridLabel">
			          <nested:write property="enable"/>
			       </td>
			       <td width="140" align="center" height="1">
			       <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
				   </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arSMLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="standardMemoLine.prompt.type"/>
			       </td>
			       <td width="550" height="1" class="gridLabel" colspan="2">
			          <nested:write property="type"/>
			       </td>
			       <td width="169" align="center" height="1">
			       <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
				   </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.name"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="name"/>
			      </td>			      		      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.description"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="description"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.unitPrice"/>
			      </td>
			      <td width="350" height="1" class="gridLabelNum">
			         <nested:write property="unitPrice"/>
			      </td>
			      <td width="200" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.taxable"/>
			      </td>
			      <td width="169" height="1" class="gridLabel">
			         <nested:write property="taxable"/>
			      </td>			      
			    </tr>			    			    			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.unitOfMeasure"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="unitOfMeasure"/>
			      </td>
			      <td width="200" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.enable"/>
			      </td>
			      <td width="169" height="1" class="gridLabel">
			         <nested:write property="enable"/>
			      </td>				      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.accountNumber"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="accountNumber"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.accountNumberDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel">
			         <nested:write property="accountNumberDescription"/>
			      </td>
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.subjectToCommission"/>
			      </td>
			      <td width="719" height="1" class="gridLabel">
			         <nested:write property="subjectToCommission"/>
			      </td>
			    </tr>			    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
   // -->
</script>
</body>
</html>
