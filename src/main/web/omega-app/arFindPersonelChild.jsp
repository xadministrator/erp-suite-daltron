<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findPersonel.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickPersonel(name) 
{

   var property = name.substring(0,name.indexOf("."));
      
   if (window.opener && !window.opener.closed) {
     
      
       window.opener.document.forms[0].elements[document.forms[0].elements["selectedIdNumber"].value].value = 
           document.forms[0].elements[property + ".idNumber"].value;                         

      
  
       
       if (document.forms[0].elements["selectedPersonelEntered"] != null &&
           document.forms[0].elements["selectedPersonelEntered"].value != "" &&
           window.opener.document.forms[0].elements[document.forms[0].elements["selectedPersonelEntered"].value] != null) {
           
    	
           var divList = window.opener.document.body.getElementsByTagName("div");
      
	       for (var i = 0; i < divList.length; i++) {
	      
	          var currDiv = divList[i];
	          var currId = currDiv.id;
	          
	          if (currId == "buttons") {
	          
	              currDiv.style.display = "none";
	          
	          } else if (currId == "buttonsDisabled") {
	          
	              currDiv.style.display = "block";
	          	
	          }
	      
	       }
           
		   for (var i=0; i < window.opener.document.forms[0].elements.length; i++) {
		      
		      if (window.opener.document.forms[0].elements[i].type != 'submit') {
		          
		          window.opener.document.forms[0].elements[i].disabled=false;
		          
		      }
		   }
           window.opener.document.forms[0].elements[document.forms[0].elements["selectedPersonelEntered"].value].value = true;           
           window.opener.document.forms[0].submit();
                      
       }
     
   }
  
   window.close();
   
   return false;

}

function closeWin() 
{
   window.close();
   
   return false;
}

//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arFindPersonel.do?child=1" onsubmit="return disableButtons();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findPersonel.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arFindPersonelForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="selectedIdNumber"/>
	     <html:hidden property="selectedName"/>
	     <html:hidden property="selectedPersonelEntered"/>
         <html:hidden property="selectedLocation"/>
         <html:hidden property="sourceType"/>  
	     <tr>
	     	<td width="575" height="10" colspan="4">
	     	
     			
	             <tr>
			         <td class="prompt" width="130" height="25">
            			<bean:message key="findPersonel.prompt.personelIdNumber"/>
			         </td>
         			<td width="435" height="25" class="control" colspan="3">
			            <html:text property="idNumber" size="25" maxlength="25" styleClass="text"/>
         			</td>
		          </tr>
		         <tr>
        			<td class="prompt" width="140" height="25">
						<bean:message key="findPersonel.prompt.personelName"/>
					</td>
					<td width="435" height="25" class="control" colspan="3">
						<html:text property="name" size="35" maxlength="50" styleClass="text"/>
					</td>
				</tr>
			
     			<tr>
					<td class="prompt" width="140" height="25">
						<bean:message key="findPersonel.prompt.personelType"/>
					</td>
					<td width="148" height="25" class="control">
						<html:select property="personelType" styleClass="combo">
							<html:options property="personelTypeList"/>
						</html:select>
					</td>
					
				</tr>      
				
				
				
				<tr>
						<td class="prompt" width="160" height="25">
		                   <bean:message key="findPersonel.prompt.orderBy"/>
		                </td>
				        <td width="124" height="25" class="control">
			               <html:select property="orderBy" styleClass="combo">
			                  <html:options property="orderByList"/>
				           </html:select>
				        </td>
					</tr> 
					
					
					
				
		 	</td>
		 </tr> 
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="arFindPersonelForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             		     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="arFindPersonelForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindPersonelForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindPersonelForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             		     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" onclick="return closeWin();">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                      <td width="575" height="1" colspan="6" class="gridTitle" 
	             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                         <bean:message key="findPersonel.gridTitle.personelDetails"/>
                      </td>
                   </tr>
	      
			    <tr>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findPersonel.prompt.personelIdNumber"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findPersonel.prompt.personelName"/>
			       </td>
			       
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findPersonel.prompt.personelType"/>
			       </td>
			       
			          
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arFPList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       			       
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="idNumber"/>
			          <nested:hidden property="idNumber"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="name"/>
			          <nested:hidden property="name"/>
			       </td>
			       <td width="200" height="1" class="gridLabel">
			          <nested:write property="personelType"/>
			          <nested:hidden property="personelType"/>
			       </td>
			      	       		       
			     
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickPersonel(name);">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    </nested:iterate>
        
               		    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null &&
        document.forms[0].elements["customerCode"].disabled == false)
        document.forms[0].elements["customerCode"].focus()
   // -->
</script>
</body>
</html>
