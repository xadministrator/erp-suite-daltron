<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findReceipt.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arFindReceipt.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findReceipt.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arFindReceiptForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="arFindReceiptForm" property="showBatchName" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findReceipt.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
	         </tr>
	     </logic:equal>
	     <logic:equal name="arFindReceiptForm" property="showShift" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findReceipt.prompt.shift"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="shift" styleClass="combo">
                       <html:options property="shiftList"/>
                   </html:select>
                </td>
	         </tr>
	     </logic:equal>		
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.customerCode"/>
	         </td>
			 <logic:equal name="arFindReceiptForm" property="useCustomerPulldown" value="true">
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="customerCode" styleClass="combo">
			      <html:options property="customerCodeList"/>
			   </html:select>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
	         </td>
			 </logic:equal>
			 <logic:equal name="arFindReceiptForm" property="useCustomerPulldown" value="false">
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="customerCode" size="15" maxlength="25" styleClass="text" readonly="true"/>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
	         </td>
			 </logic:equal>
         </tr> 
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.receiptType"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="receiptType" styleClass="comboRequired">
			      <html:options property="receiptTypeList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.bankAccount"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="bankAccount" styleClass="combo">
			      <html:options property="bankAccountList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="130" height="25">
	            <bean:message key="findReceipt.prompt.receiptVoid"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:checkbox property="receiptVoid"/>
	         </td>
         </tr>          
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	         </td>
         </tr> 
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.receiptNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="receiptNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.receiptNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="receiptNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>    
         
         
         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.receiptReferenceNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="receiptReferenceNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.receiptReferenceNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="receiptReferenceNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
         
                                       
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.posted"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="posted" styleClass="combo">
			      <html:options property="postedList"/>
			   </html:select>
	         </td>          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.approvalStatus"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="approvalStatus" styleClass="combo">
			      <html:options property="approvalStatusList"/>
			   </html:select>
	         </td>
         </tr>                                  
         <tr>	            
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findReceipt.prompt.orderBy"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	         
         </tr>
         <tr>
         	 <td class="prompt" width="130" height="25">
	            <bean:message key="findReceipt.prompt.computeRatio"/>
	         </td>
	         <td width="147" height="25" class="control" colspan="3">
	            <html:checkbox property="computeRatio"/>
	         </td>         
         
         </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="arFindReceiptForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="arFindReceiptForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arFindReceiptForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="8" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="findReceipt.gridTitle.FRDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.date"/>
			       </td>
			       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.receiptType"/>
			       </td>			       
			       <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.customerName"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.receiptNumber"/>
			       </td>
			       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.receiptReferenceNumber"/>
			       </td>			       			       			       			       
			       <td width="159" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.amount"/>
			       </td>
			       
			      
			       <td width="135" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findReceipt.prompt.creditedBalance"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arFRList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="receiptType"/>
			       </td>
			       <td width="220" height="1" class="gridLabel">
			          <nested:write property="customerName"/>
			       </td>
			       <td width="140" height="1" class="gridLabel">
			          <nested:write property="receiptNumber"/>
			       </td>
			       <td width="140" height="1" class="gridLabel">
			          <nested:write property="receiptReferenceNumber"/>
			       </td>
			       <td width="159" height="1" class="gridLabelNum">
			          <nested:write property="amount"/>
			       </td>
			       			       			       			       
			       <td width="65" height="1" class="gridLabelNum">
			          <nested:write property="creditedBalance"/>
			       </td>			       			       			       			       
			       <td width="70" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="arFindReceiptForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arFRList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="findReceipt.prompt.receiptType"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="receiptType"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.customerName"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="customerName"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.bankAccount"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="bankAccount"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.date"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="date"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.receiptNumber"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="receiptNumber"/>
			      </td>
			    </tr>			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.amount"/>
			      </td>
			      <td width="350" height="1" class="gridLabelNum" colspan="1">
			         <nested:write property="amount"/>
			      </td>	
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findReceipt.prompt.ratio"/>
			      </td>
			      <td width="350" height="1" class="gridLabelNum" colspan="1">
			         <nested:write property="ratio"/>
			      </td>	
			    </tr>				    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["receiptType"] != null &&
        document.forms[0].elements["receiptType"].disabled == false)
        document.forms[0].elements["receiptType"].focus()
   // -->
</script>
</body>
</html>
