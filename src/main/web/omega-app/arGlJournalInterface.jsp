<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="arGlJournalInterface.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arGlJournalInterface.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="arGlJournalInterface.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="2" class="statusBar">
		        <logic:equal name="arGlJournalInterfaceForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                   <bean:write name="arGlJournalInterfaceForm" property="importedJournals"/>
                </logic:equal>
		        <html:errors/>	
   	        </td>
	     </tr>
         <tr>
		     <td class="prompt" width="130" height="25">
                <bean:message key="arGlJournalInterface.prompt.dateFrom"/>
             </td>
		     <td width="445" height="25" class="control">
		        <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
		     </td>
	     </tr>
	     <tr>
	     	 <td width="130" height="25" class="prompt">
		        <bean:message key="arGlJournalInterface.prompt.dateTo"/>
		     </td>
  		     <td width="445" height="25" class="control">
		        <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
		     </td>
		 </tr>
	     <tr>
	        <td width="575" height="50" colspan="2"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>
	     <tr>
	       <td width="575" height="10" colspan="2" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		 </td>
	   </tr>
      </table>
     </td>
    </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["dateFrom"] != null)
             document.forms[0].elements["dateFrom"].focus()
   // -->
</script>
</body>
</html>
