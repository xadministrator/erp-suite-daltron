<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceBatchCopy.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function confirmCopy()
{
   if(confirm("Are you sure you want to Copy selected records?")) return true;
      else return false;     
      
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["arIBCList[" + i + "].copy"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["arIBCList[" + i + "].copy"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}

//Done Hiding--> 
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('copyButton'));">
<html:form action="/arInvoiceBatchCopy.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="200">
      <tr valign="top">
        <td width="187" height="200"></td> 
        <td width="581" height="200">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invoiceBatchCopy.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arInvoiceBatchCopyForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
	        </td>
	     </tr>
	     <html:hidden property="isBatchFromEntered" value=""/>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchCopy.prompt.batchFrom"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchFrom" styleClass="comboRequired" onchange="return enterSelect('batchFrom','isBatchFromEntered');">
                       <html:options property="batchFromList"/>
                   </html:select>
                </td>
         </tr> 
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchCopy.prompt.batchTo"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchTo" styleClass="comboRequired">
                       <html:options property="batchToList"/>
                   </html:select>
                </td>
         </tr>         	       
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
				         <html:submit property="copyButton" styleClass="mainButton" onclick="return confirmCopy();">
				            <bean:message key="button.copy"/>
 				         </html:submit>
 				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
 				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
				         <html:submit property="copyButton" styleClass="mainButton" disabled="true">
				            <bean:message key="button.copy"/>
 				         </html:submit>
 				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
 				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>       
	     <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="invoiceBatchCopy.gridTitle.IBCDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoiceBatchCopy.prompt.date"/>
			       </td>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoiceBatchCopy.prompt.referenceNumber"/>
			       </td>			       
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoiceBatchCopy.prompt.invoiceNumber"/>
			       </td>			       			       
			       <td width="234" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoiceBatchCopy.prompt.customerCode"/>
			       </td>
			       <td width="264" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoiceBatchCopy.prompt.amountDue"/>
			       </td>			       
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arIBCList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>			             			             
			             <td width="234" height="1" class="gridLabel">
			                <nested:write property="customerCode"/>
			             </td>
			             <td width="164" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="copy"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="copy" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="arInvoiceBatchCopyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arIBCList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoiceBatchCopy.prompt.customerCode"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="customerCode"/>
			             </td>
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="copy"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="arInvoiceBatchCopyForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="copy" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoiceBatchCopy.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="invoiceBatchCopy.prompt.invoiceNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoiceBatchCopy.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="invoiceBatchCopy.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			          </tr>
					  <tr bgcolor="<%= rowBgc %>">
		                 <td width="175" height="1" class="gridHeader">
		                     <bean:message key="invoiceBatchCopy.prompt.amountPaid"/>
		                 </td>
		                 <td width="350" height="1" class="gridLabelNum" colspan="1">
		                     <nested:write property="amountPaid"/>
		                 </td>
		                 <td width="369" height="1" class="gridLabel" colspan="2"></td>
		              </tr>			          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>   	     
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["batchFrom"] != null &&
        document.forms[0].elements["batchFrom"].disabled == false)
        document.forms[0].elements["batchFrom"].focus()
	       // -->
</script>
</body>
</html>
