<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceBatchEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/arInvoiceBatchEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="300">
      <tr valign="top">
        <td width="187" height="300"></td> 
        <td width="581" height="300">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invoiceBatchEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arInvoiceBatchEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>	
	        </td>
	     </tr>
	     <logic:equal name="arInvoiceBatchEntryForm" property="enableFields" value="true">
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="batchName" size="50" maxlength="50" styleClass="textRequired"/>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.description"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="description" size="50" maxlength="50" styleClass="text"/>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.status"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:select property="status" styleClass="comboRequired">
                       <html:options property="statusList"/>
                   </html:select>
                </td>
                
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.type"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:select property="type" styleClass="comboRequired">
                       <html:options property="typeList"/>
                   </html:select>
                </td>
         </tr>
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.dateCreated"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.createdBy"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                </td>
         </tr>         
         </logic:equal>	  
         <logic:equal name="arInvoiceBatchEntryForm" property="enableFields" value="false">
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="batchName" size="50" maxlength="50" styleClass="textRequired" disabled="true"/>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.description"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.status"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:select property="status" styleClass="comboRequired" disabled="true">
                       <html:options property="statusList"/>
                   </html:select>
                </td>
                
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.type"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:select property="type" styleClass="comboRequired" disabled="true">
                       <html:options property="typeList"/>
                   </html:select>
                </td>
         </tr>
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.dateCreated"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="invoiceBatchEntry.prompt.createdBy"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                </td>
         </tr>         
         </logic:equal>	     	       
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arInvoiceBatchEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>		       
		       <logic:equal name="arInvoiceBatchEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
   	           <html:submit property="invoiceButton" styleClass="mainButton">
	              <bean:message key="button.invoice"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arInvoiceBatchEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>		       
		       <logic:equal name="arInvoiceBatchEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="invoiceButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.invoice"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	       </tr>	     
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["batchName"] != null &&
        document.forms[0].elements["batchName"].disabled == false)
        document.forms[0].elements["batchName"].focus()
	       // -->
</script>
</body>
</html>
