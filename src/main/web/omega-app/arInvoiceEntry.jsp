<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers



function submitForm()
{
      disableButtons();
      enableInputControls();
}

function calculateTotalAmount(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arINVList[" + i + "].amount"].value))) {

 	        break;

 	        }
		  //alert("Hello");

 			  var in_amount = (document.forms[0].elements["arINVList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;

	}

	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}

function calculateTotalAmountItems(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arILIList[" + i + "].amount"].value))) {

 	        break;

 	        }


 			  var in_amount = (document.forms[0].elements["arILIList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;
	}
	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}

function calculateTotalAmountSO(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arSOLList[" + i + "].amount"].value))) {

 	        break;

 	        }


 			  var in_amount = (document.forms[0].elements["arSOLList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;
	}
	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}



function calculateTotalAmountJO(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arJOLList[" + i + "].amount"].value))) {

 	        break;

 	        }


 			  var in_amount = (document.forms[0].elements["arJOLList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;
	}
	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}

function calculateAmount(name,precisionUnit)
{


  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;

  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitPrice"].value != "") {

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {

        unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

      amount = (quantity * unitPrice).toFixed(parseInt(precisionUnit.value));

      if (document.forms[0].elements["type"].value == "ITEMS" && !isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

   		  var totalDiscountAmount = 0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }

	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if(fixedDiscountAmount != 0) {
	  	  	totalDiscountAmount	= fixedDiscountAmount;
	  	  	amount = (amount - totalDiscountAmount);
	  	  }

	      if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(parseInt(precisionUnit.value));

	  	  }

		  if (document.forms[0].elements["type"].value == "ITEMS") {
		  	document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toFixed(parseInt(precisionUnit.value)).toString());
		  }

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

      } else {

		  if (document.forms[0].elements["type"].value == "ITEMS") {

			document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

		  } else {

		  	if (document.forms[0].elements["type"].value == "SO MATCHED") {

				var totalDiscountAmount = document.forms[0].elements[property + ".totalDiscount"].value.replace(/,/g,'');

				var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
				var remaining = document.forms[0].elements[property + ".remaining"].value.replace(/,/g,'') * 1;
				var origDiscountAmount = document.forms[0].elements[property + ".origDiscountAmount"].value.replace(/,/g,'') * 1;

				totalDiscountAmount = (origDiscountAmount * 1) * (quantity / remaining);
				document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toFixed(parseInt(precisionUnit.value)).toString());


				if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {
					amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

				}

				amount = (amount - totalDiscountAmount);

				if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  				amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(parseInt(precisionUnit.value));

				}

	  	  	}

		  }

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	   }

  }

}

function enterSalesOrder()
{

    disableButtons();
    enableInputControls();
	document.forms[0].elements["isSalesOrderEntered"].value = true;
	document.forms[0].submit();

}

function enterJobOrder()
{

    disableButtons();
    enableInputControls();
	document.forms[0].elements["isJobOrderEntered"].value = true;
	document.forms[0].submit();

}

var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;

   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}

function setEffectivityDate(field1, field2) {

	document.forms[0].elements[field2].value = document.forms[0].elements[field1].value;

}

//Done Hiding-->

function fnOpenMisc(name){


   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;


   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array();
   var custodian = new Array(document.forms[0].elements["userList"].value);
 //  var custodian = new Array();

   var custodian2 = new Array();
   var propertyCode = new Array();
   var expiryDate = new Array();
   var serialNumber = new Array();


   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");

   //var userList = document.forms[0].elements["userList"].value;
   //check at least one disabled field
   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
   var index = 0;

 
///   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");
 window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
		   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&serialNumber=" + serialNumber
		   + "&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

   return false;

}

function showAdLog(module)
{

	var moduleKey = document.forms[0].elements["moduleKey"].value;
	var transactionNumber = document.forms[0].elements[""].value;
	 window.open("adLog.do?forward=1&moduleKey=" + moduleKey + "&module=" + module + "&transactionNumber="+ transactionNumber ,"adLog","width=605,height=500,scrollbars,status");

	   return false;


}



</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

  function invoiceEnterSubmit()
  {
      if (document.activeElement.name != 'billTo' &&
          document.activeElement.name != 'shipTo' &&
          document.activeElement.name != 'billToContact' &&
          document.activeElement.name != 'shipToContact' &&
          document.activeElement.name != 'billToAltContact' &&
          document.activeElement.name != 'shipToAltContact' &&
          document.activeElement.name != 'billingHeader' &&
          document.activeElement.name != 'billingFooter' &&
          document.activeElement.name != 'billingHeader2' &&
          document.activeElement.name != 'billingFooter2' &&
          document.activeElement.name != 'billingHeader3' &&
          document.activeElement.name != 'billingFooter3') {

          return enterSubmit(event, new Array('saveSubmitButton'));

      } else {

          return true;

      }

  }

//Done Hiding-->
</script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return invoiceEnterSubmit();">
<html:form action="/arInvoiceEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="800" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invoiceEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arInvoiceEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   </html:messages>
	        </td>
	     </tr>
	     <logic:equal name="arInvoiceEntryForm" property="type" value="MEMO LINES">
	   	  <bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
			<html:hidden property="moduleKey" value="<%=actionForm.getInvoiceCode()==null?"":actionForm.getInvoiceCode().toString()%>"/>
			
	     <html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="isProjectEntered" value=""/>
	     <html:hidden property="isProjectTypeEntered" value=""/>
	     <html:hidden property="precisionUnit"/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
 		 <html:hidden property="isCurrencyEntered" value=""/>
	     <logic:equal name="arInvoiceEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>
         				<% } %>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" onblur="return setEffectivityDate('date','effectivityDate');"/>
                			</td>
						</tr>
						<!-- 
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
						 -->
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.uploadNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="uploadNumber" size="15" maxlength="25" styleClass="text" />
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<logic:equal name="arInvoiceEntryForm" property="enablePaymentTerm" value="true">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>


         				</tr>
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>



         				<logic:equal name="arInvoiceEntryForm" property="type" value="MEMO LINES">
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="invoiceEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeMemoLineList"/>
						   </html:select>
				          </td>

         				</tr>
         				</logic:equal>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>

                			<logic:equal name="arInvoiceEntryForm" property="enablePaymentTerm" value="false">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>
							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="150" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="150" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>


                			<td width="150" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.disableInterest"/>
                			</td>
                			<td height="25" class="control">
                   				<html:checkbox property="disableInterest"/>
                			</td>


         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

         				</tr>


         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="penaltyDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="penaltyPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

                        <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" onchange="enableSubjectToCommission()">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="150" height="25">
								<bean:message key="invoiceEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control">
								<html:checkbox property="subjectToCommission"/>
							</td>
							<script Language="JavaScript" type="text/javascript">
								enableSubjectToCommission();
							</script>
						</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25"  >
                   				<html:checkbox property="debitMemo"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" readonly="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>
                		<tr>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.previousReading"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="previousReading" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.presentReading"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="presentReading" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                		</tr>


                		 <nested:iterate name="arInvoiceEntryForm" property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:textarea property="parameterValue" cols="40" rows="4" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
         	        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="150" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="145" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
		                	</td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="Upload">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>

				             <tr>
				             	<td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.uploadInvoiceEntries"/>
				                </td>
				             	<td width="288" height="25" class="control" colspan="2">
				                   <html:file property="invoiceFile" size="35" styleClass="text"/>
				                </td>
				             </tr>

				              <tr>

				             	<td width="288" height="25" class="control" colspan="2">
				                   <div id="buttons">
				                   <p align="center">
				                    <logic:equal name="arInvoiceEntryForm" property="showGenerateButton" value="true">
						           <html:submit property="generateButton" styleClass="mainButton" onclick="alert('hello')">
						              <bean:message key="button.generate"/>
						           </html:submit>
						        	</logic:equal>
				                   </div>


				                </td>
				             </tr>


				              <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>


				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="BILL/SHIP">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text"/>
						    </td>
					     </tr>
					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>

					    -->
					</table>
					</div>

					<div class="tabbertab" title="PM">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.projectCode"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="projectCode" styleClass="comboRequired" style="width:130;"  onchange="return enterSelect('projectCode','isProjectEntered');">
                       			<html:options property="projectCodeList"/>
                   				</html:select>
                			</td>
				         </tr>


				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.projectTypeCode"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="projectTypeCode" styleClass="comboRequired" style="width:130;"  onchange="return enterSelect('projectTypeCode','isProjectTypeEntered');">
                       			<html:options property="projectTypeCodeList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.projectPhaseName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="projectPhaseName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="projectPhaseNameList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.contractTermName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="contractTermName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="contractTermNameList"/>
                   				</html:select>
                			</td>
				         </tr>



					 </table>
					 </div>


					<div class="tabbertab" title="Interest">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			             	 <td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceInterest"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceInterest"/>
                			</td>

			             </tr>

			             <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.interestReferenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="interestReferenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.interestAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="interestAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestCreatedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="interestCreatedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestDateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="interestDateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>



				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestNextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="interestNextRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestLastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="interestLastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>

					 </table>
					 </div>




					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         
				          <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR INVOICE - MEMO LINE');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				       
				        
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry"  cols="20" rows="9" styleClass="text" />
			                </td>
		                </tr>
				         
				         
					 </table>
					 </div>




					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">


	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDisableInterestButton" value="true">
	           <html:submit property="disableInterestButton" styleClass="mainButtonMedium">
		            <bean:message key="button.disableInterest"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>

	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.INVDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.description"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arINVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isMemoLineEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" styleClass="comboRequired" style="width:110;" onchange="return enterSelectGrid(name, 'memoLine','isMemoLineEntered');">
				              <nested:options property="memoLineList"/>
				          </nested:select>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArMlLookupGrid(name);"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" onblur="calculateAmount(name,precisionUnit); calculateTotalAmount(precisionUnit);" onkeyup="calculateAmount(name); calculateTotalAmount(precisionUnit);"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmount(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmount(precisionUnit);"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable"/>
				       </td>
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" style="font-size:8pt;" styleClass="text"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     <logic:equal name="arInvoiceEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>
         				<% } %>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                       			<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						<!-- 
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
						 -->
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.uploadNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="uploadNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>

         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>



         				<logic:equal name="arInvoiceEntryForm" property="type" value="MEMO LINES">
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="invoiceEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeMemoLineList"/>
						   </html:select>
				          </td>

         				</tr>
         				</logic:equal>

         				</table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>

							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>

                			<td width="150" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.disableInterest"/>
                			</td>
                			<td height="25" class="control">
                   				<html:checkbox property="disableInterest"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="penaltyDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="penaltyPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25"  >
                   				<html:checkbox property="debitMemo" disabled="true"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>
                		<tr>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.previousReading"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="previousReading" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.presentReading"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="presentReading" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                		</tr>

                		 <nested:iterate name="arInvoiceEntryForm" property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:textarea property="parameterValue" cols="40" rows="4" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
	     	        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="BILL/SHIP">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
					     </tr>

					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo" disabled="true">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>

					    -->
					</table>
					</div>


					<div class="tabbertab" title="PM">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.projectCode"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="projectCode" styleClass="comboRequired" disabled="true" style="width:130;">
                       			<html:options property="projectCodeList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.projectPhaseName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="projectPhaseName" styleClass="comboRequired" disabled="true" style="width:130;">
                       			<html:options property="projectPhaseNameList"/>
                   				</html:select>
                			</td>
				         </tr>



					 </table>
					 </div>

					<div class="tabbertab" title="Interest">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			             	 <td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceInterest"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceInterest" disabled="true"/>
                			</td>

			             </tr>

			             <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.interestReferenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="interestReferenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.interestAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="interestAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestCreatedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="interestCreatedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestDateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="interestDateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>

				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestNextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="interestNextRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.interestLastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="interestLastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>


					 </table>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         
				           <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR INVOICE - MEMO LINE');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				       
				        
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry"  cols="20" rows="9" styleClass="text" />
			                </td>
		                </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="arInvoiceEntryForm" property="showDisableInterestButton" value="true">
	           <html:submit property="disableInterestButton" styleClass="mainButtonMedium">
		            <bean:message key="button.disableInterest"/>
		       </html:submit>
		       </logic:equal>

			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="arInvoiceEntryForm" property="showDisableInterestButton" value="true">
	           <html:submit property="disableInterestButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.disableInterest"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.INVDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.description"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arINVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" style="width:110;" styleClass="comboRequired" disabled="true">
				              <nested:options property="memoLineList"/>
				          </nested:select>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArMlLookupGrid(name);" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>

	     <logic:equal name="arInvoiceEntryForm" property="type" value="ITEMS">
			<jsp:include page="arInvoiceEntryItems.jsp" />
	     </logic:equal>

		 <logic:equal name="arInvoiceEntryForm" property="type" value="SO MATCHED">
	     	<jsp:include page="arInvoiceEntrySoMatched.jsp" />
	     </logic:equal>
	     
	     <logic:equal name="arInvoiceEntryForm" property="type" value="JO MATCHED">
	     	<jsp:include page="arInvoiceEntryJoMatched.jsp" />
	     </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arInvoiceEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepInvoicePrint.do?forward=1&invoiceCode=<%=actionForm.getInvoiceCode()%>&viewType=<%=actionForm.getViewType()%>&invoiceType=<%=actionForm.getType()%>&reportType=<%=actionForm.getReportType()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>

<logic:equal name="arInvoiceEntryForm" property="report2" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepDeliveryReceiptPrint.do?forward=1&invoiceCode=<%=actionForm.getInvoiceCode()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->

</script>
</logic:equal>

<logic:equal name="arInvoiceEntryForm" property="report3" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepTransmittalDocumentPrint.do?forward=1&invoiceCode=<%=actionForm.getInvoiceCode()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="arInvoiceEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="arInvoiceEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>

<logic:equal name="arInvoiceEntryForm" property="attachmentDownload" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnDownload.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>

----</body>
</html>
