<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{

  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;

  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

  }

  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {

  	unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');

  }

  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

  }

  amount = (quantity * unitPrice).toFixed(2);

  document.forms[0].elements[property + ".amount"].value = amount;

}

var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/arInvoiceEntry.do?child=1" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="0">
      <tr valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invoiceEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arInvoiceEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   </html:messages>
	        </td>
	     </tr>
	     <logic:equal name="arInvoiceEntryForm" property="type" value="MEMO LINES">
		 <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>
         				<% } %>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount"  disabled="true"/>
                			</td>

         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
                        <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="debitMemo" disabled="true"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>
         	        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="Billing/Shipping">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
					     </tr>

					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo" disabled="true">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     -->
					</table>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.INVDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.description"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arINVList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" style="width:110;" styleClass="comboRequired" disabled="true">
				              <nested:options property="memoLineList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		   </logic:equal>
		   <logic:equal name="arInvoiceEntryForm" property="type" value="ITEMS">
	       <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
				         	</logic:equal>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
               	 			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>


         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount"  disabled="true"/>
                			</td>

         				</tr>


         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>


         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="penaltyDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="penaltyPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountUnearnedInterest"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountUnearnedInterest" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>
         					<logic:equal name="arInvoiceEntryForm" property="enablePaymentTerm" value="true">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>


         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<logic:equal name="arInvoiceEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25" class="control" >
                   				<html:checkbox property="debitMemo" disabled="true"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="Billing/Shipping">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo" disabled="true">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>

					     -->
					</table>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	       </tr>
	       <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	       </tr>
	       <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.ILIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unit"/>
                       </td>
                       <td width="115" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unitPrice"/>
                       </td>
                       <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.autoBuild"/>
                       </td>
				       <td width="65" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemDescription"/>
				       </td>
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="invoiceEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="205" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arILIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="7" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>
                       <td width="100" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>
                       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
				       <td width="65" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
					   <td width="65" align="center" height="1">
					   	<div id="buttons">
					   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   </td>
				    </tr>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	         </logic:equal>
 			<logic:equal name="arInvoiceEntryForm" property="type" value="SO MATCHED">
			<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
				         	</logic:equal>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
               	 			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invoiceEntry.prompt.soNumber"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
								<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<logic:equal name="arInvoiceEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25" class="control" >
                   				<html:checkbox property="debitMemo" disabled="true"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>

					<div class="tabbertab" title="Billing/Shipping">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo" disabled="true">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="customerEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					</table>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.SOLDetails"/>
		                </td>
		            </tr>
		            <tr>
					   <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						  <bean:message key="invoiceEntry.prompt.lineNumber"/>
					   </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemName"/>
				       </td>
		               <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.remaining"/>
                       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.quantity"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unit"/>
                       </td>
                       <td width="135" height="1" class="gridHeader" colspan="2"bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unitPrice"/>
                       </td>
                       <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						   <bean:message key="invoiceEntry.prompt.issue"/>
					   </td>
				    </tr>
				    <tr>
					   <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="320" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemDescription"/>
				       </td>
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="invoiceEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="165" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate name="arInvoiceEntryForm" property="arSOLList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
					   <td width="30" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:80;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="remaining" size="5" maxlength="10" styleClass="text"  disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       </td>
				       <td width="70" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>
                       <td width="135" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>
                       <td width="30" align="center" height="1" class="control">
						  <nested:checkbox property="issueCheckbox" disabled="true"/>
					   </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
					   <td width="30" height="1" class="control"/>
				       <td width="300" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="70" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="165" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="15" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
			 </logic:equal>
	         <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		      </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arInvoiceEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
----</body>
</html>
