		<%@ page language="java" import="com.struts.util.Constants" %>
		<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
		<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
		<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
		<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

		 	  <bean:define id="actionForm" name="arInvoiceEntryForm" type="com.struts.ar.invoiceentry.ArInvoiceEntryForm"/>
			<html:hidden property="moduleKey" value="<%=actionForm.getInvoiceCode()==null?"":actionForm.getInvoiceCode().toString()%>"/>
			
		 <html:hidden property="isCustomerEntered" value=""/>
		 <html:hidden property="precisionUnit"/>
	     <html:hidden property="isTypeEntered" value=""/>
	     <html:hidden property="taxRate"/>
	     <html:hidden property="taxType"/>
	     <html:hidden property="arILIListSize"/>
		 <html:hidden property="isTaxCodeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
		 <html:hidden property="isCurrencyEntered" value=""/>
		 <html:hidden property="enableFields"/>
		  <html:hidden property="userList"/>
	     <logic:equal name="arInvoiceEntryForm" property="enableFields" value="true">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
				         	</logic:equal>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" onblur="return setEffectivityDate('date','effectivityDate');"/>
                			</td>
						</tr>
						<!-- 
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
						 -->
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
               	 			</td>
						</tr>

						<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.uploadNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="uploadNumber" size="15" maxlength="25" styleClass="text" />
                			</td>
         				</tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.clientPO"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="clientPO" size="15" maxlength="25" styleClass="text"/>
                			</td>

						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>


         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="penaltyDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="penaltyPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountUnearnedInterest"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountUnearnedInterest" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>
         					<logic:equal name="arInvoiceEntryForm" property="enablePaymentTerm" value="true">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>


         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         					</tr>


         					<logic:equal name="arInvoiceEntryForm" property="type" value="ITEMS">
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="invoiceEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeItemList"/>
						   </html:select>
				          </td>

         				</tr>
         				</logic:equal>

			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


						<tr>



							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" onchange="enableSubjectToCommission()">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission"/>
							</td>
							<script Language="JavaScript" type="text/javascript">
								enableSubjectToCommission();
							</script>
						</tr>
						<logic:equal name="arInvoiceEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
							<td width="130" height="25" class="prompt" >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25" class="control" >
                   				<html:checkbox property="debitMemo"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" readonly="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>


                		 <nested:iterate name="arInvoiceEntryForm" property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:textarea property="parameterValue" cols="40" rows="4" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>

			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="enterSelect('taxCode','isTaxCodeEntered');">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="ATT">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>
					<div class="tabbertab" title="BILL/SHIP">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text"/>
						    </td>
					     </tr>

					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text"/>
						    </td>
					    </tr>
					     -->
					</table>
					</div>

					<div class="tabbertab" title="HR">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


				         <tr>

				         		<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.deployedBranchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="deployedBranchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="deployedBranchNameList"/>
                   				</html:select>
                			</td>
				         </tr>


				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.employeeNumber"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="employeeNumber" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.bioNumber"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="bioNumber" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.managingBranch"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="managingBranch" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currentJobPosition"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="currentJobPosition" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

					 </table>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         
				           <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR INVOICE - ITEM');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				       
				        
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry"  cols="20" rows="9" styleClass="text" />
			                </td>
		                </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="drPrintButton" styleClass="mainButton">
	               <bean:message key="button.drPrint"/>
	           </html:submit>

	           <!--
			   <html:submit property="tdPrintButton" styleClass="mainButton">
	               <bean:message key="button.tdPrint"/>
	           </html:submit>
	           -->
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="drPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.drPrint"/>
	           </html:submit>
	           <!--
			   <html:submit property="tdPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.tdPrint"/>
	           </html:submit>
	           -->
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		      <td width="575" height="185" colspan="4">
			  <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.ILIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unit"/>
                       </td>
                       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unitPrice"/>
                       </td>
                       <td width="40" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.tax"/>
                       </td>

                       <td width="40" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.autoBuild"/>
                       </td>
				       <td width="65" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="invoiceEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="205" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate name="arInvoiceEntryForm" property="arILIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>

				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>

				    <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGridCst(name, 'itemName', 'itemDescription', 'location', 'isItemEntered'); calculateTotalAmountItems(precisionUnit);"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="3" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name,precisionUnit); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); calculateTotalAmountItems(precisionUnit);" />
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>

                       <nested:equal property="disableSalesPrice" value="false">
                       <td width="115" height="1" class="control" >
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmount(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItems(precisionUnit);"/>
                       </td>
                       </nested:equal>
                       <nested:equal property="disableSalesPrice" value="true">
                       <td width="115" height="1" class="control" >
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmount(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItems(precisionUnit);" disabled="true"/>
                       </td>
                       </nested:equal>

					    <td width="40" height="1" rowspan="2" class="control">
				          <p align="center">
                          <nested:select property="tax" styleClass="comboRequired" style="width:40px;">
				              <nested:options property="taxList"/>
				          </nested:select>
                       </td>



                       <nested:equal property="isAssemblyItem" value="true">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox"/>
                       </td>
					   </nested:equal>
					   <nested:equal property="isAssemblyItem" value="false">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
					   </nested:equal>
				       <td width="65" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="30" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
 				       <td width="140" height="1" class="control" >
				          <nested:text property="amount" size="10" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1">
				        	<div id="buttons">
				        	<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
				        	</div>

			        		<div id="buttonsDisabled" style="display: none;">
			        		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
			        		</div>
				       </td>



					   <td width="50" align="center" height="1">
					   	<div id="buttons">

					   		<nested:hidden property="misc"/>
					   		<nested:equal property="isTraceMisc" value="true">
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
						   	 </nested:equal>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
							<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   </td>
				   </tr>
				 </nested:iterate>
				 </table>
			     </div>
			     </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
				   </div>
				  </td>
	     		</tr>
	     </logic:equal>

	     <logic:equal name="arInvoiceEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arInvoiceEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
				         	</logic:equal>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						<!-- 
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="documentTypeList"/>
	               				</html:select>
                			</td>
						</tr>
						 -->
						
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="invoiceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
               	 			</td>
						</tr>

						<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.uploadNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="uploadNumber" size="15" maxlength="25" styleClass="text" disabled="true" />
                			</td>
         				</tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.clientPO"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="clientPO" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>

						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invoiceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.downPayment"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="downPayment" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="penaltyDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.penaltyPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="penaltyPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountUnearnedInterest"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountUnearnedInterest" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>

         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>


         				<logic:equal name="arInvoiceEntryForm" property="type" value="ITEMS">
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="invoiceEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeItemList"/>
						   </html:select>
				          </td>

         				</tr>
         				</logic:equal>
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>

							<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arInvoiceEntryForm" property="enableInvoiceVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.invoiceVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="invoiceVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountDue"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountDue" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="140" height="25">
								<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
							</td>
							<td width="157" height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<logic:equal name="arInvoiceEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt"  >
                   				<bean:message key="invoiceEntry.prompt.debitMemo"/>
                			</td>
                			<td width="157" height="25" class="control" >
                   				<html:checkbox property="debitMemo" disabled="true"/>
                			</td>
                		</tr>
                		<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.dueDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.recieveDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="recieveDate" size="10" maxlength="10" styleClass="text"/>
                			</td>
                		</tr>


                		 <nested:iterate name="arInvoiceEntryForm" property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:textarea property="parameterValue" cols="40" rows="4" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="invoiceEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="ATT">
	   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						     <tr>
						        <td class="prompt" width="575" height="25" colspan="4">
				                </td>
				             </tr>
				             <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename1"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
						           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton1" value="true">
				                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						     <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename2"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
						           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton2" value="true">
				                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename3"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
						           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton3" value="true">
				                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
					         <tr>
				                <td width="160" height="25" class="prompt">
				                   <bean:message key="invoiceEntry.prompt.filename4"/>
				                </td>
				                <td width="288" height="25" class="control" colspan="2">
				                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
				                </td>
				                <td width="127" height="25" class="control">
				                   <div id="buttons">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
						           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                   <div id="buttonsDisabled" style="display: none;">
				                   <p align="center">
				                   <logic:equal name="arInvoiceEntryForm" property="showViewAttachmentButton4" value="true">
				                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
						              <bean:message key="button.viewAttachment"/>
						           </html:submit>
						           </logic:equal>
				                   </div>
				                </td>
					         </tr>
						</table>
					</div>
					<div class="tabbertab" title="BILL/SHIP">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
   				         <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.billTo"/>
				            </td>
						    <td width="128" height="25" class="control">
						       <html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
						    <td class="prompt" width="160" height="25">
				               <bean:message key="invoiceEntry.prompt.shipTo"/>
				            </td>
						    <td width="127" height="25" class="control">
						       <html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
						    </td>
					     </tr>

					     <!--
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToAltContact"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:textarea property="billToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToAltContact"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:textarea property="shipToAltContact" cols="20" rows="3" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
		                <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billToPhone"/>
				            </td>
						    <td width="148" height="25" class="control">
				               <html:text property="billToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipToPhone"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:text property="shipToPhone" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     <tr>
				            <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.freight"/>
				            </td>
						    <td width="147" height="25" class="control">
				               <html:select property="freight" styleClass="combo" disabled="true">
				                   <html:options property="freightList"/>
				               </html:select>
						    </td>
						    <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.shipDate"/>
				            </td>
						    <td width="148" height="25" class="control">
						       <html:text property="shipDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
						    </td>
					     </tr>
					     <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingSignatory"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:text property="billingSignatory" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.signatoryTitle"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:text property="signatoryTitle" size="20" maxlength="25" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader2"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter2"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter2" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
						<tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingHeader3"/>
				            </td>
						    <td width="148" height="25" class="control" colspan="3">
				               <html:textarea property="billingHeader3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
						</tr>
					    <tr>
					        <td class="prompt" width="140" height="25">
				               <bean:message key="invoiceEntry.prompt.billingFooter3"/>
				            </td>
						    <td width="147" height="25" class="control" colspan="3">
				               <html:textarea property="billingFooter3" cols="50" rows="5" styleClass="text" disabled="true"/>
						    </td>
					    </tr>
					     -->
					</table>
					</div>


					<div class="tabbertab" title="HR">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>

				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="invoiceEntry.prompt.deployedBranchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="deployedBranchName" styleClass="comboRequired" disabled="true" style="width:130;">
                       			<html:options property="deployedBranchNameList"/>
                   				</html:select>
                			</td>
				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.employeeNumber"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="employeeNumber" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.bioNumber"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="bioNumber" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.managingBranch"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="managingBranch" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.currentJobPosition"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="currentJobPosition" size="30" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

				         </tr>

					 </table>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invoiceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         
				           <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR INVOICE - ITEM');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				       
				        
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry"  cols="20" rows="9" styleClass="text" />
			                </td>
		                </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="drPrintButton" styleClass="mainButton">
	               <bean:message key="button.drPrint"/>
	           </html:submit>
	           <!--
			   <html:submit property="tdPrintButton" styleClass="mainButton">
	               <bean:message key="button.tdPrint"/>
	           </html:submit>
	           -->
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arInvoiceEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arInvoiceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="drPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.drPrint"/>
	           </html:submit>
	           <!--
			   <html:submit property="tdPrintButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.tdPrint"/>
	           </html:submit>
	           -->
	           <logic:equal name="arInvoiceEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
	           <logic:equal name="arInvoiceEntryForm" property="showSaveReceivedDateButton" value="true">
	           <html:submit property="saveReceivedDateButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveReceivedDate"/>
		       </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invoiceEntry.gridTitle.ILIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unit"/>
                       </td>
                       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.unitPrice"/>
                       </td>
                       <td width="40" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.tax"/>
                       </td>

                       <td width="40" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.autoBuild"/>
                       </td>
				       <td width="65" height="1" rowspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invoiceEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="invoiceEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="205" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invoiceEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate name="arInvoiceEntryForm" property="arILIList">

				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>

<tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true" onclick="return showInvIiLookupGridCst(name, 'itemName', 'itemDescription', 'location', 'isItemEntered'); calculateTotalAmountItems(precisionUnit);"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" disabled="true" style="width:100;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="3" maxlength="10" disabled="true" styleClass="textRequired"  onblur="calculateAmount(name,precisionUnit); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); calculateTotalAmountItems(precisionUnit);" />
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" disabled="true"style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>

                       <nested:equal property="disableSalesPrice" value="false">
                       <td width="115" height="1" class="control" >
                          <nested:text property="unitPrice" size="10" maxlength="25" disabled="true" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmount(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItems(precisionUnit);"/>
                       </td>
                       </nested:equal>
                       <nested:equal property="disableSalesPrice" value="true">
                       <td width="115" height="1" class="control" >
                          <nested:text property="unitPrice" size="10" maxlength="25" disabled="true" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmount(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItems(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItems(precisionUnit);" disabled="true"/>
                       </td>
                       </nested:equal>

					    <td width="40" height="1" rowspan="2" class="control">
				          <p align="center">
                           <nested:select property="tax" styleClass="comboRequired" style="width:40px;" disabled="true" >
				              <nested:options property="taxList"/>
				          </nested:select>
                       </td>



                       <nested:equal property="isAssemblyItem" value="true">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
					   </nested:equal>
					   <nested:equal property="isAssemblyItem" value="false">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
					   </nested:equal>
				       <td width="65" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="30" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
 				       <td width="140" height="1" class="control" >
				          <nested:text property="amount" size="10" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1">
				        	<div id="buttons">
				        	<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
				        	</div>

			        		<div id="buttonsDisabled" style="display: none;">
			        		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
			        		</div>
				       </td>



					   <td width="50" align="center" height="1">
					   	<div id="buttons">

					   		<nested:hidden property="misc"/>
					   		<nested:equal property="isTraceMisc" value="true">
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
						   	 </nested:equal>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
							<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   </td>
				   </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arInvoiceEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arInvoiceEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
