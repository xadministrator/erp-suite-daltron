<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoicePost.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmPost()
{
   if(confirm("Are you sure you want to Post selected records?")) return true;
      else return false;
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["arIPList[" + i + "].post"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["arIPList[" + i + "].post"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arInvoicePost.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="invoicePost.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="arInvoicePostForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>
	         <logic:equal name="arInvoicePostForm" property="showBatchName" value="true">
		     <tr>
	            <td width="160" height="25" class="prompt">
	               <bean:message key="invoicePost.prompt.batchName"/>
	            </td>
	            <td width="415" height="25" class="control" colspan="3">
	               <html:select property="batchName" styleClass="combo">
	                   <html:options property="batchNameList"/>
	               </html:select>
	            </td>
	         </tr>
	         </logic:equal>	
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="invoicePost.prompt.customerCode"/>
                </td>
				<logic:equal name="arInvoicePostForm" property="useCustomerPulldown" value="true">
		        <td width="390" height="25" class="control" colspan="3">
	               <html:select property="customerCode" styleClass="combo">
                      <html:options property="customerCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
				<logic:equal name="arInvoicePostForm" property="useCustomerPulldown" value="false">
		        <td width="390" height="25" class="control" colspan="3">
	               <html:text property="customerCode" styleClass="text" size="15" maxlength="25" readonly="true"/>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
		     </tr>
		     <tr>
		        <td class="prompt" width="185" height="25">
	               <bean:message key="invoicePost.prompt.creditMemo"/>
	            </td>
			    <td width="390" height="25" class="control" colspan="3">
			       <html:checkbox property="creditMemo"/>
			    </td>
             </tr>
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="invoicePost.prompt.dateFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="invoicePost.prompt.dateTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>		        
	         </tr>             
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="invoicePost.prompt.invoiceNumberFrom"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="invoiceNumberFrom" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="invoicePost.prompt.invoiceNumberTo"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:text property="invoiceNumberTo" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>		        
	         </tr>             	         
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="invoicePost.prompt.referenceNumber"/>
                </td>
		        <td width="103" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="160" height="25">
                   <bean:message key="invoicePost.prompt.approvalStatus"/>
                </td>
		        <td width="124" height="25" class="control">
	               <html:select property="approvalStatus" styleClass="combo">
	                  <html:options property="approvalStatusList"/>
		           </html:select>
		        </td>		        
	         </tr>
	         <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="invoicePost.prompt.currency"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:select property="currency" styleClass="combo">
			        <html:options property="currencyList"/>
			      </html:select>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="invoicePost.prompt.orderBy"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>
	         </tr>	       
	         <tr>
		       <td class="prompt" width="185" height="25">
		          <bean:message key="invoicePost.prompt.maxRows"/>
		       </td>
		       <td width="103" height="25" class="control">
		          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
		       </td>
		       <td class="prompt" width="160" height="25">
		          <bean:message key="invoicePost.prompt.queryCount"/>
		       </td>
		       <td width="124" height="25" class="control">
		          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       </td>
	         </tr>               	         	         	         
	       <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="arInvoicePostForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="arInvoicePostForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="arInvoicePostForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="postButton" styleClass="mainButton" onclick="return confirmPost();">
				         <bean:message key="button.post"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="postButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.post"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="invoicePost.gridTitle.IPDetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoicePost.prompt.date"/>
			       </td>
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoicePost.prompt.referenceNumber"/>
			       </td>			       
			       <td width="132" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoicePost.prompt.invoiceNumber"/>
			       </td>			       			       
			       <td width="234" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoicePost.prompt.customerCode"/>
			       </td>
			       <td width="264" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="invoicePost.prompt.amountDue"/>
			       </td>			       
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arIPList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>			             
			             <td width="132" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>			             			             
			             <td width="234" height="1" class="gridLabel">
			                <nested:write property="customerCode"/>
			             </td>
			             <td width="164" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>	
			             <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="post"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="100" align="center" height="1">
			                <nested:checkbox property="post" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="arInvoicePostForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="arIPList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoicePost.prompt.customerCode"/>
			             </td>
			             <td width="570" height="1" class="gridLabel" colspan="2">
			                <nested:write property="customerCode"/>
			             </td>
			             <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="post"/>
			             </td>
			             </logic:equal>
			             <logic:equal name="arInvoicePostForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
			             <td width="149" align="center" height="1">
			                <nested:checkbox property="post" disabled="true"/>
			             </td>
			             </logic:equal>
			          </tr>
                      <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoicePost.prompt.date"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="invoicePost.prompt.invoiceNumber"/>
			             </td>
			             <td width="149" height="1" class="gridLabel">
			                <nested:write property="invoiceNumber"/>
			             </td>
                      </tr>			    
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="175" height="1" class="gridHeader">
			                <bean:message key="invoicePost.prompt.referenceNumber"/>
			             </td>
			             <td width="350" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			             <td width="220" height="1" class="gridHeader">
			                <bean:message key="invoicePost.prompt.amountDue"/>
			             </td>
			             <td width="149" height="1" class="gridLabelNum">
			                <nested:write property="amountDue"/>
			             </td>			             
			          </tr>		          
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null)
             document.forms[0].elements["customerCode"].focus()
   // -->
</script>
</body>
</html>
