<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="jobOrderEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}


var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));  
   currProperty = property;
   currName = name;   
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");
   
   return false;

}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/arJobOrderEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
  	<tr valign="top">
        <td width="768" height="510">
        	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   		<tr>
	     		<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="jobOrderEntry.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="arJobOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               		<bean:message key="app.success"/>
           			</logic:equal>
		   			<html:errors/>	
		   			<html:messages id="msg" message="true">
		       		<bean:write name="msg"/>		   
		   			</html:messages>
	        	</td>
	     	</tr>
	     	
	     	<html:hidden property="isCustomerEntered" value=""/>
		    <html:hidden property="taxRate"/>
		    <html:hidden property="taxType"/>
		    <html:hidden property="arJOLListSize"/>
		 	<html:hidden property="isTaxCodeEntered" value=""/>
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
						<tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arJobOrderEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arJobOrderEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>               
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>         	 
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
         				<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
         				
         				
         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="jobOrderEntry.prompt.jobOrderStatus"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="jobOrderStatus" styleClass="combo" disabled="true">
						      <html:options property="jobOrderStatusList"/>
						   </html:select>
				          </td>
         				</tr>
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="jobOrderEntry.prompt.jobOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="jobOrderVoid" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.salesperson"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="jobOrderEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
			        </table>
					</div>
					<div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="jobOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td> 
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="jobOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>				   				             
		 				</tr>         
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="jobOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>	
					<div class="tabbertab" title="Attachment">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.filename1" />
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton1" value="true">			                   
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr> 
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrder.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arJobOrderEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>               
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr> 
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="jobOrderEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
						<tr> 
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				        </tr>	
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>	
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.orderStatus"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="orderStatus" size="50" maxlength="50" styleClass="text" disabled="true"/>
			                </td>
				        </tr>				         
					</table>
					</div>	    
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
						<tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="jobOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4"> 
	           	<div id="buttons">
	           	<p align="right">
			   	<html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
			   	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
			  	</td>
	     	</tr>
	     	<tr valign="top">
		       	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="jobOrderEntry.gridTitle.JOLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="jobOrderEntry.prompt.lineNumber"/>
				       	</td>		            
		               	<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="jobOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="jobOrderEntry.prompt.location"/>
				       	</td>				       				    
				       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="jobOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="jobOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="204" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="jobOrderEntry.prompt.unitPrice"/>
                       	</td>
                       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="jobOrderEntry.prompt.delete"/>
                       	</td>				       				       	                                             
				  	</tr>				       			    
				    <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="510" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="jobOrderEntry.prompt.itemDescription"/>
				       	</td>	
				       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="jobOrderEntry.prompt.itemDiscount"/>
				       	</td>	
				       	<td width="274" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="jobOrderEntry.prompt.amount"/>
                       	</td> 		          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arJOLList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				    	<td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>				    								    				       	       
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
				       	<td width="75" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       	</td>								   
				       	<td width="60" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       	</td>
				       	<td width="70" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       	</td>			       
                       	<td width="125" height="1" class="control">
                          <nested:text property="unitPrice" size="17" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                       	</td>
                       	<td width="50" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       	</td>    				       
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="40" height="1" class="control"/>				   
				       	<td width="280" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>
				       	<td width="70" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="125" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmountRequired" disabled="true"/>
				       	</td>
					   <td width="50" align="center" height="1">
					   	<div id="buttons">
				   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   </td>				                                     
				  	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4"> 
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="arJobOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			        	<bean:message key="button.addLines"/>
			        </html:submit>			   
			      </logic:equal>
			      <logic:equal name="arJobOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="arJobOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="arJobOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>			   
			      </logic:equal>
		          </div>
				</td>
	     	</tr>	
	     	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     	</td>
	       	</tr>
          
        	</table>
    	</td>
  	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)        
        document.forms[0].elements["date"].focus();
	       // -->
</script>

<logic:equal name="arJobOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arJobOrderEntryForm" type="com.struts.ar.joborderentry.ArJobOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepJobOrderPrint.do?forward=1&jobOrderCode=<%=actionForm.getJobOrderCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="arJobOrderEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arJobOrderEntryForm" type="com.struts.ar.joborderentry.ArJobOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="arJobOrderEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arJobOrderEntryForm" type="com.struts.ar.joborderentry.ArJobOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
</body>
</html>

