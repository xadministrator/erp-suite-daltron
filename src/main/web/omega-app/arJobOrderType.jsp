<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="jobOrderType.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/arJobOrderType.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="jobOrderType.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arJobOrderTypeForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="jobOrderType.prompt.name"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="name" size="25" maxlength="25" styleClass="textRequired"/>
	         </td>
         </tr>
         
         <tr>
			<td class="prompt" width="140" height="25">
               <bean:message key="jobOrderType.prompt.documentType"/>
            </td>
		    <td width="147" height="25" class="control">
		       	<html:select property="documentType" styleClass="combo">
	               <html:options property="documentTypeList"/>
            	</html:select>
		    </td>
		</tr>
         
         <tr>
			<td class="prompt" width="140" height="25">
               <bean:message key="jobOrderType.prompt.reportType"/>
            </td>
		    <td width="147" height="25" class="control">
		       	<html:select property="reportType" styleClass="combo">
	               <html:options property="reportTypeList"/>
            	</html:select>
		    </td>
		</tr>
         
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="jobOrderType.prompt.description"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="description" size="50" maxlength="50" styleClass="text"/>
	         </td>
         </tr> 
         
         
      
        
		
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="jobOrderType.prompt.jobOrderAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="jobOrderAccount" size="30" maxlength="255" styleClass="text"/>
			    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('jobOrderAccount','jobOrderAccountDescription');"/>	            
	         </td>
         </tr>             
         <tr>
             <td class="prompt" width="140" height="25">
	            <bean:message key="jobOrderType.prompt.jobOrderAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="jobOrderAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
	     
       
                     
        
                 
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="jobOrderType.prompt.enable"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:checkbox property="enable"/>
	         </td>	                  
         </tr>
        
      
                                         
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        
					  <td width="415" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				       	 <logic:equal name="arJobOrderTypeForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	 <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton">
				               <bean:message key="button.save"/>
				          	</html:submit>
				         </logic:equal>
				         <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
                                            
                                           
                                             
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="arJobOrderTypeForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.save"/>
				            </html:submit>
				         </logic:equal>
				         <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				     
                                            
                                             
                             <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="8" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="jobOrderType.gridTitle.JOTDetails"/>
	                       </td>
	                    </tr>
	                
			    <tr>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="jobOrderType.prompt.name"/>
			       </td>
			       <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="jobOrderType.prompt.jobOrderAccount"/>
			       </td>
			    
			     			       
			       
			    
			       			       
			       <td width="150" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="customerClass.prompt.enable"/>
			       </td>			       			       			       			       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arJOTList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="name"/>
			       </td>			       
			       <td width="160" height="1" class="gridLabel">
			          <nested:write property="jobOrderAccount"/>
			       </td>
	
			       
			     
		       
			       <td width="50" height="1" class="gridLabel">
			          <nested:write property="enable"/>
			       </td>
			       <td width="100" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="arJobOrderTypeForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
   			       </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="arJobOrderTypeForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		              <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arJobOrderTypeForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    </nested:iterate>
            
              		    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
    
    
  <!--
      if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
   // -->
</script>


</body>
</html>
