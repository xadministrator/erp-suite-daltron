<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="miscReceiptEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
	disableButtons();
    enableInputControls();
    /*
    disableButtons();

    if (document.forms[0].elements["type"].value == "ITEMS" &&
    		document.forms[0].elements["saveAsDraftButton"] != null) {

        document.forms[0].elements["customerName"].disabled=false;
        document.forms[0].elements["receiptVoid"].disabled=false;
        document.forms[0].elements["amount"].disabled=false;
        document.forms[0].elements["approvalStatus"].disabled=false;
        document.forms[0].elements["posted"].disabled=false;
        document.forms[0].elements["voidApprovalStatus"].disabled=false;
        document.forms[0].elements["voidPosted"].disabled=false;
        document.forms[0].elements["createdBy"].disabled=false;
        document.forms[0].elements["dateCreated"].disabled=false;
        document.forms[0].elements["lastModifiedBy"].disabled=false;
        document.forms[0].elements["dateLastModified"].disabled=false;

    } else {

        enableInputControls();

    }
    */


}

function calculateTotalAmountItem(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arRLIList[" + i + "].amount"].value))) {

 	        break;

 	        }
		  //alert("Hello");

 			  var in_amount = (document.forms[0].elements["arRLIList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;
	}
	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}

function calculateTotalAmountMemo(precisionUnit)
{
	var i =0;
	var totalAmount = 0;

	while(true){
 		  if (isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].amount"].value))) {

 	        break;

 	        }
		  //alert("Hello");

 			  var in_amount = (document.forms[0].elements["arRCTList[" + i + "].amount"].value).replace(/,/g,'');

 			  //alter(in_amount);
 			  if(!isNaN(parseFloat(in_amount))){

 				  totalAmount = parseFloat(totalAmount) + parseFloat(in_amount) ;

 			  }


 		  i++;
	}
	document.forms[0].elements["totalAmount"].value = totalAmount.toFixed(parseInt(precisionUnit.value));
}

function calculateAmount(name,precisionUnit)
{

  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;
  var totalDiscount = 0;

  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitPrice"].value != "") {


	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }



	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {

	        unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');

	  }


	  //if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  //	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	 // }


      amount = (quantity * unitPrice).toFixed(parseInt(precisionUnit.value));


      if (document.forms[0].elements["type"].value != "MEMO LINES" &&
    	!isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) &&
    	!isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

   		  var totalDiscountAmount = 0;

		  /* if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));


		  }     */

	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }

	      if(fixedDiscountAmount > 0) {
	  	  	totalDiscountAmount	= fixedDiscountAmount.toFixed(parseInt(precisionUnit.value));
	  	  	amount = (amount - totalDiscountAmount);
	  	  }

	      /* if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {
		      alert('HOHo');
			amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(parseInt(precisionUnit.value));

	  	  } */

	  		amount = (amount).toFixed(parseInt(precisionUnit.value));
		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toString());

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
		  calculateTotalAmountMemo(precisionUnit);


      } else {

          document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
          calculateTotalAmountItem(parseInt(precisionUnit.value));
	  	  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");
      }

  }

}

var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}


//Done Hiding-->



function fnOpenMisc(name){


   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;


   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array();
   var custodian = new Array(document.forms[0].elements["userList"].value);
 //  var custodian = new Array();

   var custodian2 = new Array();
   var propertyCode = new Array();
   var expiryDate = new Array();
   var serialNumber = new Array();


   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");

   //var userList = document.forms[0].elements["userList"].value;
   //check at least one disabled field
   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
   var index = 0;

///   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");
 window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
		   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&serialNumber=" + serialNumber
		   + "&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

   return false;

}

</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/arMiscReceiptEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="miscReceiptEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arMiscReceiptEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   </html:messages>
	        </td>
	     </tr>
	     <logic:equal name="arMiscReceiptEntryForm" property="type" value="MEMO LINES">
	     <html:hidden property="isCustomerEntered" value=""/>
	<html:hidden property="precisionUnit"/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
         <logic:equal name="arMiscReceiptEntryForm" property="enableFields" value="true">

         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">A
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>
         				<% } %>
	     					<logic:equal name="arMiscReceiptEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control" >
                   				<html:select property="documentType" styleClass="combo" style="width:130;">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
							
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerAddress"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="customerAddress" size="50" maxlength="255" styleClass="text"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>

         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>


                                        <logic:equal name="arMiscReceiptEntryForm" property="isInvestorSupplier" value="true">
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="miscReceiptEntry.prompt.isInvestorBeginingBalance"/>
                                            </td>
                                            <td width="157" height="25" class="control">
                                                    <html:checkbox property="investorBeginningBalance" />
                                            </td>
                                        </logic:equal>
                		</tr>
			        </table>
					</div>

			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
		        			</logic:equal>
		        			<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;">
	                   			<html:options property="paymentMethodList"/>
	               				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
               	 			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" onchange="enableSubjectToCommission()">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="150" height="25">
								<bean:message key="miscReceiptEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control">
								<html:checkbox property="subjectToCommission"/>
							</td>
							<script Language="JavaScript" type="text/javascript">
								enableSubjectToCommission();
							</script>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerDeposit"/>
                			</td>
                			<td width="333" height="25" class="control" colspan="3">
                   				<html:checkbox property="customerDeposit"/>
                			</td>
						</tr>

						 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.soldTo"/>
			                </td>
			                <td width="415" eight="25" class="control" colspan="3">
			                   <html:textarea property="soldTo" cols="20" rows="4" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>



					<div class="tabbertab" title="Investor">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


						 <tr>

						 	<td width="130" height="25" class="prompt">
                				<bean:message key="miscReceiptEntry.prompt.invtrInvestorFund"/>
                			</td>
                			<td width="157" height="25" class="control" colspan="3">
                				<html:checkbox property="invtrInvestorFund"/>
                			</td>



						 </tr>

						 <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.invtrNextRunDate"/>
                			</td>
                			<td width="157" height="25" class="control" colspan="3">
                   				<html:text property="invtrNextRunDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>


         				</tr>



			        </table>
					</div>


			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="miscReceiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table> --%>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="miscReceiptEntry.gridTitle.RCTDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.description"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isMemoLineEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" style="width:110;" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'memoLine','isMemoLineEntered');">
				              <nested:options property="memoLineList"/>
				          </nested:select>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArMlLookupGrid(name);"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" onkeyup="calculateAmount(name,precisionUnit); calculateTotalAmountMemo(precisionUnit);" onblur="calculateAmount(name,precisionUnit); calculateTotalAmountMemo(precisionUnit);"/>
				       </td>


				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);"/>
				       </td>


				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable"/>
				       </td>
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" style="font-size:8pt;" styleClass="text"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     <logic:equal name="arMiscReceiptEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">B
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>
         				<% } %>
	     					<logic:equal name="arMiscReceiptEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control" >
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
         				
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerAddress"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerAddress" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>
         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>


                                        <logic:equal name="arMiscReceiptEntryForm" property="isInvestorSupplier" value="true">
                                            <td width="130" height="25" class="prompt">
                                                    <bean:message key="miscReceiptEntry.prompt.isInvestorBeginingBalance"/>
                                            </td>
                                            <td width="157" height="25" class="control">
                                                    <html:checkbox property="investorBeginningBalance" disabled="true"/>
                                            </td>
                                        </logic:equal>

                		</tr>
			        </table>
					</div>

			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
		        			</logic:equal>
		        			<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="paymentMethodList"/>
	               				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
               	 			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="150" height="25">
								<bean:message key="miscReceiptEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerDeposit"/>
                			</td>
                			<td width="333" height="25" class="control" colspan="3">
                   				<html:checkbox property="customerDeposit" disabled="true"/>
                			</td>
						</tr>

						 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.soldTo"/>
			                </td>
			                <td width="415" eight="25" class="control" colspan="3">
			                   <html:textarea property="soldTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="miscReceiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table> --%>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="miscReceiptEntry.gridTitle.RCTDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.delete"/>
				       </td>
				    </tr>
				    <tr>

				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.description"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" style="width:110;" styleClass="comboRequired" disabled="true">
				              <nested:options property="memoLineList"/>
				          </nested:select>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArMlLookupGrid(name);" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>
				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>
	     <logic:equal name="arMiscReceiptEntryForm" property="type" value="ITEMS">
	     <html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="precisionUnit"/>
	     <html:hidden property="isTypeEntered" value=""/>
	     <html:hidden property="taxRate"/>
	     <html:hidden property="taxType"/>
	     <html:hidden property="arRLIListSize"/>
		 <html:hidden property="isTaxCodeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
	     <html:hidden property="enableFields"/>
	     <html:hidden property="userList"/>
	     <logic:equal name="arMiscReceiptEntryForm" property="enableFields" value="true">

	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">C
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arMiscReceiptEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
         					<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				
         				
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control" >
                   				<html:select property="documentType" styleClass="combo" style="width:130;">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
						</tr>
         				
         	
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                		
                 		</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerAddress"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerAddress" size="50" maxlength="255" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>
         				<tr>
         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                		</tr>
			        </table>
					</div>

					<div class="tabbertab" title="Payment">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="5">
			                </td>
			             </tr>
			             <tr>
			             	<td width="130" height="25" class="prompt">

                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.cash"/>
                			</td>


                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.card"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.cheque"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.voucher"/>
                			</td>



			             </tr>
						<tr>



                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>


                			<td width="158" height="25" class="control">
                   				<html:text property="amountCash" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard1" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountCheque" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountVoucher" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>

         				</tr>



         				<tr>

         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">

                			</td>

         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber1" size="15" maxlength="150" styleClass="text"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="chequeNumber" size="15" maxlength="150" styleClass="text"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="voucherNumber" size="15" maxlength="150" styleClass="text"/>
                			</td>


         				</tr>


         				<tr>

         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>

         					<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard1" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountCard1List"/>
	               				</html:select>
                			</td>

                			<%-- <td width="158" height="25" class="control">
                   				<html:select property="bankAccountCheque" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountChequeList"/>
	               				</html:select>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountVoucher" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountVoucherList"/>
	               				</html:select>
                			</td> --%>



         				</tr>

         				<tr width="130" height="25">
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard2" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber2" size="15" maxlength="150" styleClass="text"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
         					<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard2" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountCard2List"/>
	               				</html:select>
                			</td>
         				</tr>



         				<tr width="130" height="25">
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard3" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber3" size="15" maxlength="150" styleClass="text"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
         					<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard3" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountCard3List"/>
	               				</html:select>
                			</td>
         				</tr>


			        </table>
					</div>

			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
		        			</logic:equal>
		        			<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.paymentMethod"/>
                			</td>
               				<td width="158" height="25" class="control">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;">
	                   			<html:options property="paymentMethodList"/>
	               				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" onchange="enableSubjectToCommission()">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="150" height="25">
								<bean:message key="miscReceiptEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control">
								<html:checkbox property="subjectToCommission"/>
							</td>
							<script Language="JavaScript" type="text/javascript">
								enableSubjectToCommission();
							</script>
						</tr>
         				<logic:equal name="arMiscReceiptEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;">
                   				<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>

         				 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.soldTo"/>
			                </td>
			                <td width="415" eight="25" class="control" colspan="3">
			                   <html:textarea property="soldTo" cols="20" rows="4" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="enterSelect('taxCode','isTaxCodeEntered');">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired"  onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text"  onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="miscReceiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" />
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table> --%>
					</div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="miscReceiptEntry.gridTitle.RLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.unit"/>
                       </td>
                       <td width="115" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.unitPrice"/>
                       </td>
                       <td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.autoBuild"/>
                       </td>
				       <td width="65" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemDescription"/>
				       </td>
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="205" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arRLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGridCst(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="7" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name,precisionUnit); calculateTotalAmountItem(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); calculateTotalAmountItem(precisionUnit);"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>

                       <logic:equal name="arMiscReceiptEntryForm" property="disableSalesPrice" value="false">
                       <td width="115" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmountItem(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItem(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItem(precisionUnit);" />
                       </td>
                       </logic:equal>
                       <logic:equal name="arMiscReceiptEntryForm" property="disableSalesPrice" value="true">
                       <td width="115" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit); calculateTotalAmountItem(precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name); calculateTotalAmountItem(precisionUnit);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event); calculateTotalAmountItem(precisionUnit);" disabled="true"/>
                       </td>
                       </logic:equal>

                       <nested:equal property="isAssemblyItem" value="true">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox"/>
                       </td>
                       </nested:equal>
                       <nested:equal property="isAssemblyItem" value="false">
				       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
                       </nested:equal>
				       <td width="65" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" readonly="true"/>
				       </td>
				       <td width="140" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="15" maxlength="10" styleClass="textAmount" readonly="true"/>
				       </td>
					   <td width="100" align="center" height="1">
					   	<div id="buttons">
					   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc"/>
					   		<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>

	     <logic:equal name="arMiscReceiptEntryForm" property="enableFields" value="false">

	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">D
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>
	     					<logic:equal name="arMiscReceiptEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
         					<logic:equal name="arMiscReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control" >
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
         				
         				
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			
                 		</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.customerAddress"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="customerAddress" size="50" maxlength="255" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>

         				<tr>
         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                		</tr>
			        </table>
					</div>


					<div class="tabbertab" title="Payment">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="5">
			                </td>
			             </tr>
			             <tr>
			             	<td width="130" height="25" class="prompt">

                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.cash"/>
                			</td>


                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.card"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.cheque"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.voucher"/>
                			</td>



			             </tr>
						<tr>



                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>


                			<td width="158" height="25" class="control">
                   				<html:text property="amountCash" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard1" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountCheque" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="amountVoucher" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>

         				</tr>



         				<tr>

         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">

                			</td>

         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber1" size="15" maxlength="150" styleClass="text" disabled="true"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="chequeNumber" size="15" maxlength="150" styleClass="text" disabled="true"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:text property="voucherNumber" size="15" maxlength="150" styleClass="text" disabled="true"/>
                			</td>


         				</tr>


         				<tr>

         				<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>

         					<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard1" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountCard1List"/>
	               				</html:select>
                			</td>

                			<%-- <td width="158" height="25" class="control">
                   				<html:select property="bankAccountCheque" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountChequeList"/>
	               				</html:select>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountVoucher" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="bankAccountVoucherList"/>
	               				</html:select>
                			</td> --%>



         				</tr>

         				<tr width="130" height="25">
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard2" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber2" size="15" maxlength="150" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
         					<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard2" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountCard2List"/>
	               				</html:select>
                			</td>
         				</tr>



         				<tr width="130" height="25">
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amountCard3" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.number"/>
                			</td>
                			<td width="158" height="25" class="control">
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:text property="cardNumber3" size="15" maxlength="150" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
         					<td width="158" height="25" class="control">
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccountCard3" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountCard3List"/>
	               				</html:select>
                			</td>
         				</tr>


			        </table>
					</div>


			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.bankAccount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="bankAccountList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
		        			</logic:equal>
		        			<logic:equal name="arMiscReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.paymentMethod"/>
                			</td>
               				<td width="158" height="25" class="control">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;" disabled="true">
	                   			<html:options property="paymentMethodList"/>
	               				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.salesperson"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
							<td class="prompt" width="150" height="25">
								<bean:message key="miscReceiptEntry.prompt.subjectToCommission"/>
							</td>
							<td height="25" class="control">
								<html:checkbox property="subjectToCommission" disabled="true"/>
							</td>
						</tr>
         				<logic:equal name="arMiscReceiptEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="miscReceiptEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                   				<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>

         				 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.soldTo"/>
			                </td>
			                <td width="415" eight="25" class="control" colspan="3">
			                   <html:textarea property="soldTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="miscReceiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Attachment">
   				    <%-- <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arMiscReceiptEntryForm" property="showViewAttachmentB4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					 </table> --%>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="miscReceiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arMiscReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arMiscReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="miscReceiptEntry.gridTitle.RLIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemName"/>
				       </td>
		               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.location"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.quantity"/>
                       </td>
                       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.unit"/>
                       </td>
                       <td width="115" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.unitPrice"/>
                       </td>
                       <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.autoBuild"/>
                       </td>
				       <td width="65" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.delete"/>
                       </td>
				    </tr>
				    <tr>
		               <td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="270" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemDescription"/>
				       </td>
				       <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="miscReceiptEntry.prompt.itemDiscount"/>
				       </td>
				       <td width="205" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="miscReceiptEntry.prompt.amount"/>
                       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arRLIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="7" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       </td>
                       <td width="115" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>
                       <td width="40" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="autoBuildCheckbox" disabled="true"/>
                       </td>
				       <td width="65" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="40" height="1" class="control"/>
				       <td width="270" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="60" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="15" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td>
					   <td width="100" align="center" height="1">
					   	<div id="buttons">
					   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:hidden property="misc"/>
					   		<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   		<nested:equal property="isTraceMisc" value="true">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		</nested:equal>
					   	</div>
					   </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4">
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arMiscReceiptEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arMiscReceiptEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arMiscReceiptEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arMiscReceiptEntryForm" type="com.struts.ar.miscreceiptentry.ArMiscReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepReceiptPrint.do?forward=1&receiptCode=<%=actionForm.getReceiptCode()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="arMiscReceiptEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arMiscReceiptEntryForm" type="com.struts.ar.miscreceiptentry.ArMiscReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<%-- <logic:equal name="arMiscReceiptEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arMiscReceiptEntryForm" type="com.struts.ar.miscreceiptentry.ArMiscReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal> --%>
----</body>
</html>
