<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="pdcEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{
  
  var property = name.substring(0,name.indexOf("."));
  
  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;      
  
  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitPrice"].value != "") {  
        
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {
	  
	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {
	  
	  	unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
	  
	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');
	  	
	  }
	  
	  amount = (quantity * unitPrice).toFixed(2);
	  
	  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  
  }
   
}

function setEffectivityDate(field1, field2) {

	document.forms[0].elements[field2].value = document.forms[0].elements[field1].value;
	
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/arPdcEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="pdcEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arPdcEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>

	     <logic:equal name="arPdcEntryForm" property="type" value="MEMO LINES">
	     <html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>						
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
                        <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" onblur="return setEffectivityDate('maturityDate','effectivityDate');"/>
                			</td>
                        </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arPdcEntryForm" property="enableCancelled" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="enableCancelled" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>  
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
                		</tr>
         	        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>	       
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.PDCDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.description"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arPDCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isMemoLineEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'memoLine','isMemoLineEntered');">
				              <nested:options property="memoLineList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" onblur="calculateAmount(name);" onkeyup="calculateAmount(name);"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable"/>
				       </td>			       
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="70" maxlength="255" style="font-size:8pt;" styleClass="text"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>						
						<tr>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>	     
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>         
         				<% } else {%>
         					<html:hidden property="type" value="MEMO LINES"/>         
         				<% } %>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
                			</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
                           <td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                            <td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arPdcEntryForm" property="enableCancelled" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="enableCancelled" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>  
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                		</tr>
         	        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.PDCDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.memoLine"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.quantity"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.unitPrice"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.amount"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.taxable"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.description"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arPDCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="300" height="1" class="control">
				          <nested:select property="memoLine" styleClass="comboRequired" disabled="true">
				              <nested:options property="memoLineList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="unitPrice" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="amount" size="23" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:checkbox property="taxable" disabled="true"/>
				       </td>			       
				       <td width="50" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="70" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
	     </logic:equal>       	       
	     <logic:equal name="arPdcEntryForm" property="type" value="ITEMS">
	     	<jsp:include page="arPdcEntryItems.jsp" />
	     </logic:equal>


	     <logic:equal name="arPdcEntryForm" property="type" value="PR">
		<html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="true">	     
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="false">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>   
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>      
         				</tr>         				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired" disabled="false"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" disabled="false"/>
                			</td>                									
						</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="false"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired" disabled="false"/>
               	 			</td>
						</tr>                
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="false"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.bankAccount"/>
               	 			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;">
                       			<html:options property="paymentMethodList"/>
                   				</html:select>
                			</td>   							                            
         				</tr>						
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="false">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="false"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="false"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
		  <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				 <logic:equal name="arPdcEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				 <logic:equal name="arPdcEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		 </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.PRDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.invoiceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.referenceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.applyAmount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.discount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.creditableWTax"/>
				       </td>				       			       			    
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.pay"/>
				       </td>
				    </tr>							    
				    <tr>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.dueDate"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.currency"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.amountDue"/>
				       </td>		
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.aiappliedDeposit"/>
				       </td>		       
				       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.allocatedReceiptAmount"/>
				       </td>				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define id="actionForm" name="arPdcEntryForm" type="com.struts.ar.pdcentry.ArPdcEntryForm"/>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {

						if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       	}else{
				           rowBgc = ROW_BGC2;
				       	}  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="149" height="1" class="control">
				          <nested:text property="invoiceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="12" maxlength="25" styleClass="textAmountRequired" disabled="false"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="12" maxlength="25" styleClass="textAmount" disabled="false"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="creditableWTax" size="12" maxlength="25" styleClass="textAmount" disabled="false"/>
				       </td>			       
				       <td width="149" align="center" height="1" class="control">
				          <nested:checkbox property="payCheckbox" disabled="false"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="appliedDeposit" size="12" maxlength="25" styleClass="textAmount" disabled="false"/>
				       </td>		       			       
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="allocatedReceiptAmount" size="12" maxlength="25" styleClass="textAmount" disabled="false"/>
				       </td>				       			       
				    </tr>
					<% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>   
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>      
         				</tr>         				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                									
						</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
               	 			</td>
						</tr>                
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.bankAccount"/>
               	 			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="paymentMethodList"/>
                   				</html:select>
                			</td>   							                            
         				</tr>						
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="false">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
		  <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				 <logic:equal name="arPdcEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				 <logic:equal name="arPdcEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arPdcEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		 </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.PRDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.invoiceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.referenceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.applyAmount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.discount"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.creditableWTax"/>
				       </td>				       			       			    
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.pay"/>
				       </td>
				    </tr>							    
				    <tr>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.dueDate"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.currency"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.amountDue"/>
				       </td>		
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.aiappliedDeposit"/>
				       </td>		       
				       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.allocatedReceiptAmount"/>
				       </td>				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define id="actionForm" name="arPdcEntryForm" type="com.struts.ar.pdcentry.ArPdcEntryForm"/>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {

						if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       	}else{
				           rowBgc = ROW_BGC2;
				       	}  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="149" height="1" class="control">
				          <nested:text property="invoiceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="12" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="creditableWTax" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="149" align="center" height="1" class="control">
				          <nested:checkbox property="payCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="appliedDeposit" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>		       			       
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="allocatedReceiptAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>				       			       
				    </tr>
					<% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>
	     </logic:equal>
	     <tr>
	     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
	     	</td>
	     </tr>
	</table>
        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arPdcEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
	<bean:define id="actionForm" name="arPdcEntryForm" type="com.struts.ar.pdcentry.ArPdcEntryForm" />
	<script type="text/javascript" langugage="JavaScript">
	<!--
    	win = window.open("<%=request.getContextPath()%>/arRepPdcPrint.do?forward=1&pdcCode=<%=actionForm.getPdcCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
	//-->
	</script>
</logic:equal>
</body>
</html>