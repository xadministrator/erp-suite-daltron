<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
	     <html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="isTypeEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="true">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td> 
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>        
         				</tr>
         				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
	                    <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" onblur="return setEffectivityDate('maturityDate','effectivityDate');"/>
                			</td>
	                    </tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" />
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired"/>
               	 			</td>
						</tr>                
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arPdcEntryForm" property="enableCancelled" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="enableCancelled" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>  
						<logic:equal name="arPdcEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
                		</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('conversionDate','isConversionDateEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.ILIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.itemName"/>
				       </td>
		               <td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.location"/>
				       </td>				       				    
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.quantity"/>
                       </td>
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.unit"/>
                       </td>
                       <td width="204" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.unitPrice"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="570" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.itemDescription"/>
				       </td>
				       <td width="274" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.amount"/>
                       </td>			          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate name="arPdcEntryForm" property="arILIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>	
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="220" height="1" class="control">
				          <nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="190" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="80" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name);" onkeyup="calculateAmount(name);"/>
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="204" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="15" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name);" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
                       </td>
                       <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>				   
				       <td width="570" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>	
				       <td width="274" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td> 			                                     
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	     <logic:equal name="arPdcEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.type"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
                   	   			<html:options property="typeList"/>
	               				</html:select>
                			</td>   
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.status"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="status" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>      
         				</tr>         				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.amount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.dateReceived"/>
                			</td>
                			<td width="158" eight="25" class="control">
                   				<html:text property="dateReceived" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.maturityDate"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="maturityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                									
						</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                  				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.checkNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="checkNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
               	 			</td>
						</tr>                
						<tr>
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="pdcEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>   
			        </table>
					</div>
			        <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arPdcEntryForm" property="enableCancelled" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arPdcEntryForm" property="enableCancelled" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.cancelled"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="cancelled" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>  
						<logic:equal name="arPdcEntryForm" property="showShift" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.shift"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="shiftList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="pdcEntry.prompt.effectivityDate"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="effectivityDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
                		</tr>
			        </table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.withholdingTax"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="withholdingTax" styleClass="comboRequired" disabled="true">
			                       <html:options property="withholdingTaxList"/>
			                   </html:select>
			                </td>
				         </tr>             
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="pdcEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>			    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="pdcEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arPdcEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arPdcEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="pdcEntry.gridTitle.ILIDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.itemName"/>
				       </td>
		               <td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.location"/>
				       </td>				       				    
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.quantity"/>
                       </td>
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.unit"/>
                       </td>
                       <td width="204" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.unitPrice"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       <td width="570" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="pdcEntry.prompt.itemDescription"/>
				       </td>
				       <td width="274" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="pdcEntry.prompt.amount"/>
                       </td>			          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate name="arPdcEntryForm" property="arILIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="220" height="1" class="control">
				          <nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="190" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="80" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="204" height="1" class="control" colspan="2">
                          <nested:text property="unitPrice" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>
                       <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>				   
				       <td width="570" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="274" height="1" class="control" colspan="3">
				          <nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" disabled="true"/>
				       </td> 				                                     
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="arPdcEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="arPdcEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>
	     </logic:equal>
