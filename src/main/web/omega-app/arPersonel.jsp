<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="personel.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

  function submitForm()
  {            
      disableButtons();
      enableInputControls();
  }

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/arPersonel.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="personel.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arPersonelForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     
	     <html:hidden property="isPersonelTypeEntered" value=""/>
	     <tr>
        	<td width="575" height="10" colspan="4">
    	       	<div class="tabber">
		        <div class="tabbertab" title="Header">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			       	<tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
			        <tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelIdNumber"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="personelIdNumber" size="15" maxlength="25" styleClass="textRequired"/>
						</td>
					</tr> 	     	     
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelName"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="personelName" size="25" maxlength="100" styleClass="textRequired"/>
						</td>
					</tr>
					
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelDescription"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="personelDescription" size="50" maxlength="100" styleClass="text"/>
						</td>
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelAddress"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="personelAddress" size="50" maxlength="100" styleClass="text"/>
						</td>
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelRate"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="personelRate" size="50" maxlength="100" styleClass="text"/>
						</td>
					</tr>
					
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="personel.prompt.personelType"/>
			            </td>
					    <td width="148" height="25" class="control">
			            <html:select property="personelType" styleClass="comboRequired" onchange="return enterSelect('personelType','isPersonelTypeEntered');">
			               <html:options property="personelTypeList"/>
			            </html:select>
			            </td>
					</tr>
					
				</table>
				</div>
				
		        </div><script>tabberAutomatic(tabberOptions)</script>    
			</td>
		</tr>
		<tr>
			<td width="575" height="50" colspan="4">

			<table border="0" cellpadding="0" cellspacing="0" width="575" height="50" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="160" height="50">
			           
					   
				    </td>
					  <td width="415" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				       	 <logic:equal name="arPersonelForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	 <logic:equal name="arPersonelForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton">
				               <bean:message key="button.save"/>
				          	</html:submit>
				         </logic:equal>
				         <logic:equal name="arPersonelForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="arPersonelForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="arPersonelForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.save"/>
				            </html:submit>
				         </logic:equal>
				         <logic:equal name="arPersonelForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	    
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["type"] != null &&
        document.forms[0].elements["type"].disabled == false)
        document.forms[0].elements["type"].focus()
   // -->
</script>
</body>
</html>
