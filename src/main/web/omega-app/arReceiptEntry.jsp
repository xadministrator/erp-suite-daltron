<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="receiptEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitForm()
{
      disableButtons();
      enableInputControls();
}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers



function checkIssueAllSi(name,size){
	 var property = name.substring(0,name.indexOf("."));
	 var listName = property.substring(0, property.indexOf("["))
	 var index = name.charAt(10);
	 var checked = document.forms[0].elements[name].checked;
	 var i=0;
	 var docNum = document.forms[0].elements["arRCTList[" + index + "].invoiceNumber"].value;
	if(checked==true){

	 for(i=0; i<size.value; i++){
		 //alert(docNum);
		 docNum1 = document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"].value;
		 if(docNum==docNum1){
			 document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked = true;
		 }
	 }
	}else if(checked==false){
		for(i=0; i<size.value; i++){
			 //alert(docNum);
			 docNum1 = document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"].value;
			 if(docNum==docNum1){
				 document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked = false;
			 }
		 }
	}

}


  function calculateAmountTotal(precisionUnit)
  {

      var property = "arRCTList";

      var amount = 0;

      var i = 0;

      while (true) {

          if (document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"] == null) {

              break;

          }

          var applyAmount = 0;
          var penaltyApplyAmount = 0;

	      var allocatedReceiptAmount = 0;


          if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value))) {

	      	applyAmount = (document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value).replace(/,/g,'');

	      }

          if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].penaltyApplyAmount"].value))) {

        	  penaltyApplyAmount = (document.forms[0].elements["arRCTList[" + i + "].penaltyApplyAmount"].value).replace(/,/g,'');

  	      }

	      /* if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].allocatedReceiptAmount"].value))) {

		    allocatedReceiptAmount = (document.forms[0].elements["arRCTList[" + i + "].allocatedReceiptAmount"].value).replace(/,/g,'');

		  } */

		  if (document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked == true) {


	          if (document.forms[0].elements["arRCTList[" + i + "].currency"].value  ==
	              document.forms[0].elements["currency"].value) {

	          	  amount = (amount * 1 + applyAmount * 1 + penaltyApplyAmount * 1).toFixed(parseInt(precisionUnit.value));

	          } else {

	              amount = (amount * 1 ).toFixed(parseInt(precisionUnit.value));
	          }

	      }

          i++;

      }


      document.forms[0].elements["referenceNumber"].value = invNumList.toString().trim();
      document.forms[0].elements["amount"].value = formatDisabledAmount(amount.toString());

  }

  var tempAmount = 0;

  function initializeTempAmount(name)
  {

      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');

      } else {

          tempAmount = 0;

      }

  }







  function calculateAmount(name,precisionUnit)
  {

      var property = name.substring(0,name.indexOf("."));
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;
      var num;

      var applyAmount = 0;
      var amountDue = (document.forms[0].elements[property + ".amountDue"].value).replace(/,/g,'');

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {

    	  applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');


      }




	  if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {

          if (document.forms[0].elements[property + ".currency"].value  ==
              document.forms[0].elements["currency"].value) {


              if (!isNaN(1 * amount - (1 * tempAmount - 1 * applyAmount ))) {

	          	  num = (1 * amount - (1 * tempAmount - 1 * applyAmount )).toFixed(parseInt(precisionUnit.value));



	          	 document.forms[0].elements["amount"].value = formatDisabledAmount(num);
	          	  tempAmount = applyAmount;
	          }

          }

      }

  }


  function calculateAmountAllocated(name,precisionUnit)
  {

    /*   var property = name.substring(0,name.indexOf("."));
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;

      var allocatedReceiptAmount = 0;

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".allocatedReceiptAmount"].value))) {

	    allocatedReceiptAmount = (document.forms[0].elements[property + ".allocatedReceiptAmount"].value).replace(/,/g,'');

	  }

	  if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {

          if (document.forms[0].elements[property + ".currency"].value  !=
              document.forms[0].elements["currency"].value) {

              if (!isNaN(1 * amount - (1 * tempAmount - 1 * allocatedReceiptAmount))) {

	              document.forms[0].elements["amount"].value = (1 * amount - (1 * tempAmount - 1 * allocatedReceiptAmount)).toFixed(parseInt(precisionUnit.value));
	          	  tempAmount = allocatedReceiptAmount;

	          }
          }

      }
          */
  }




  function calculateRebate(name,precisionUnit)
  {

      var property = name.substring(0,name.indexOf("."));
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;
      var enableAdvance = document.forms[0].elements["enableAdvancePayment"].checked;

      var selectInvoNum = "";
      var applyAmount = 0;
      var penaltyApplyAmount = 0;
      var creditableWTax = 0;
      var discount = 0;
      var allocatedReceiptAmount = 0;
      var creditBalancePaid = 0;
      var rebate = 0;
      var amountDue = (document.forms[0].elements[property + ".amountDue"].value).replace(/,/g,'');
      var discount = 0;

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {

      	applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');


      }

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".penaltyApplyAmount"].value))) {

    	  penaltyApplyAmount = (document.forms[0].elements[property + ".penaltyApplyAmount"].value).replace(/,/g,'');


        }


	if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount"].value))) {

		discount = (document.forms[0].elements[property + ".discount"].value).replace(/,/g,'');

        }



      if (!isNaN(parseFloat(document.forms[0].elements[property + ".creditBalancePaid"].value))) {

    	  creditBalancePaid = (document.forms[0].elements[property + ".creditBalancePaid"].value).replace(/,/g,'');

  	  }

	if (!isNaN(parseFloat(document.forms[0].elements[property + ".rebate"].value))) {

		rebate = (document.forms[0].elements[property + ".rebate"].value).replace(/,/g,'');

  	  }



      if (document.forms[0].elements[property + ".rebateCheckbox"].checked == true) {

    	  document.forms[0].elements[property + ".applyAmount"].value = (1 * applyAmount - 1 *  rebate).toFixed(parseInt(precisionUnit.value));

      } else {

    	  document.forms[0].elements[property + ".applyAmount"].value = (1 * applyAmount+ 1 *  rebate).toFixed(parseInt(precisionUnit.value));


      }






  }




  function calculateAmountCheck(name,precisionUnit)
  {

      var property = name.substring(0,name.indexOf("."));

      var enableAdvance = document.forms[0].elements["enableAdvancePayment"].checked;

      var selectInvoNum = "";

      var penaltyApplyAmount = 0;
      var creditableWTax = 0;
      var discount = 0;
      var allocatedReceiptAmount = 0;
      var creditBalancePaid = 0;

      var discount = 0;





     var i = 0;
     var amount = 0;
     var applyAmount = 0;
     var rebate = 0;

     var invoList = "";

      while (true) {

          if (document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"] == null) {

              break;

          }

          if (document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked == true
        		  && invoList.indexOf(document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"].value) < 0) {

        	  invoList = invoList + document.forms[0].elements["arRCTList[" + i + "].invoiceNumber"].value + " ";


          }



           if (document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked == true
        		&& document.forms[0].elements["arRCTList[" + i + "].rebateCheckbox"].checked == true) {

	           	if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value))) {

	              	applyAmount = (document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value).replace(/,/g,'');

	            }

	           	if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].rebate"].value))) {

	        		rebate = (document.forms[0].elements["arRCTList[" + i + "].rebate"].value).replace(/,/g,'');

	          	}


           		amount += 1 * applyAmount

           } else if (document.forms[0].elements["arRCTList[" + i + "].payCheckbox"].checked == true
        		&& document.forms[0].elements["arRCTList[" + i + "].rebateCheckbox"].checked == false) {

              	if (!isNaN(parseFloat(document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value))) {

                 	applyAmount = (document.forms[0].elements["arRCTList[" + i + "].applyAmount"].value).replace(/,/g,'');

               }

              	amount += 1 * applyAmount;


           }



          i++;
      }



      document.forms[0].elements["amount"].value = (1 * amount).toFixed(parseInt(precisionUnit.value));

       document.forms[0].elements["referenceNumber"].value =  invoList.trim();




  }

  function calculateApplyAmount(name,precisionUnit)
  {

	  var property = name.substring(0,name.indexOf("."));

      var amountDue = 0;
      var discount = 0;
      var tempDiscount = 0;
      var applyAmount = 0;
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".amountDue"].value))) {

	      amountDue = (document.forms[0].elements[property + ".amountDue"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount"].value))) {

      	  discount = (document.forms[0].elements[property + ".discount"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".tempDiscount"].value))) {

      	  tempDiscount = (document.forms[0].elements[property + ".tempDiscount"].value).replace(/,/g,'');

 	  }

 	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {

          applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');

	  }

      if (((1 * discount) != (1 * tempDiscount)) && document.forms[0].elements[property + ".payCheckbox"].checked == true) {

    	  amount = 1 * amount -  applyAmount * 1;
    	  amountDiff = 1* amountDue  - (1 * applyAmount + (tempDiscount * 1));

    	  if(applyAmount * 1 < amountDue * 1){
    		  amountDiff = amountDiff + discount * 1;
    		  applyAmount = 1 * amountDue - amountDiff;

    	  }else if(applyAmount * 1 > amountDue * 1){

    		  amountDiff = amountDiff + discount * 1;
    		  applyAmount = 1 * amountDue - amountDiff;
    	  }else{

    		  applyAmount = 1 * applyAmount -  discount * 1;

    	  }


    	//  amount = 1 * amount - 1 * applyAmount;
	   //   applyAmount = 1 * applyAmount - 1 * discount;
	      amount = 1 * amount + 1 * applyAmount;



	  	  document.forms[0].elements[property + ".applyAmount"].value = (applyAmount).toFixed(parseInt(precisionUnit.value));
	  	  document.forms[0].elements["amount"].value = formatDisabledAmount((amount).toFixed(parseInt(precisionUnit.value)));

   	  	  document.forms[0].elements[property + ".tempDiscount"].value = document.forms[0].elements[property + ".discount"].value

      }

  }



  function calculateApplyAmountWithAdvancePayment(name,precisionUnit)
  {

	  var property = name.substring(0,name.indexOf("."));

      var amountDue = 0;
      var creditBalancePaid = 0;
      var tempCreditBalancePaid = 0;
      var applyAmount = 0;
      var penaltyApplyAmount = 0;
      var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;
      var amountDiff = 0;
      if (!isNaN(parseFloat(document.forms[0].elements[property + ".amountDue"].value))) {

	      amountDue = (document.forms[0].elements[property + ".amountDue"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".creditBalancePaid"].value))) {

		  creditBalancePaid = (document.forms[0].elements[property + ".creditBalancePaid"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".tempCreditBalancePaid"].value))) {

		  tempCreditBalancePaid = (document.forms[0].elements[property + ".tempCreditBalancePaid"].value).replace(/,/g,'');

 	  }

 	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".applyAmount"].value))) {

          applyAmount = (document.forms[0].elements[property + ".applyAmount"].value).replace(/,/g,'');

	  }

 	if (!isNaN(parseFloat(document.forms[0].elements[property + ".penaltyApplyAmount"].value))) {

 		penaltyApplyAmount = (document.forms[0].elements[property + ".penaltyApplyAmount"].value).replace(/,/g,'');

	  }

      if (document.forms[0].elements[property + ".payCheckbox"].checked == true) {


    	  amountDiff = (1* amountDue)  -  ((applyAmount * 1) + (tempCreditBalancePaid * 1) );


    	  if((applyAmount * 1) < (amountDue * 1)){

    		  amountDiff = amountDiff * 1 + creditBalancePaid * 1;


    		  applyAmount = (1 * amountDue) - amountDiff;

    	  }else if((applyAmount *1)> amountDue){

    		  amountDiff = amountDiff + (creditBalancePaid * 1);
    		  applyAmount = (1 * amountDue) - amountDiff;
    	  }else{

    		  applyAmount = (1 * applyAmount) - ( creditBalancePaid	*1);

    	  }



	  	  document.forms[0].elements[property + ".applyAmount"].value = (applyAmount).toFixed(parseInt(precisionUnit.value));


	  	  document.forms[0].elements["amount"].value = formatDisabledAmount((applyAmount * 1 + penaltyApplyAmount * 1 ).toFixed(parseInt(precisionUnit.value)));


   	  	  document.forms[0].elements[property + ".tempCreditBalancePaid"].value = document.forms[0].elements[property + ".creditBalancePaid"].value;

      }

  }


//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/arReceiptEntry.do" onsubmit="return submitForm();" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="830" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="receiptEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arReceiptEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <font color="green"><bean:write name="msg"/></font><br>
		   </html:messages>
	        </td>
	     </tr>
	     <html:hidden property="isCustomerEntered" value=""/>
	     <html:hidden property="precisionUnit"/>
		 <html:hidden property="isCurrencyEntered" value=""/>
                 <html:hidden property="isInvoiceNumberEntered" value=""/>
		 <html:hidden property="isConversionDateEntered" value=""/>
	     <logic:equal name="arReceiptEntryForm" property="enableFields" value="true">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<logic:equal name="arReceiptEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customer"/>
                			</td>
                                        <logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="true">
                                        <td width="158" height="25" class="control">
                                            <html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                                                <html:options property="customerList"/>
                                            </html:select>
                                            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                                        </td>
                                        </logic:equal>
                			<logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
                                        </logic:equal>





                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" onchange="return enterSelect('customer','isCustomerEntered');"/>
                			</td>

                             </tr>
                                 <tr>

                                     <td width="130" height="25" class="prompt">
        				<bean:message key="receiptEntry.prompt.selectInvoiceNumber"/>
                                     </td>

                                     <logic:equal name="arReceiptEntryForm" property="showInvoiceNumber" value="true">
                                     <td width="158" height="25" class="control">

                                     <html:text property="invoiceNumber" size="10" maxlength="10" styleClass="text" onchange="return enterSelect('invoiceNumber','isInvoiceNumberEntered');"/>

                                     <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArInvLookup('invoiceNumber', '', 'isInvoiceNumberEntered','Receipt Entry');"/>

                                     </td>
                                   </logic:equal>
                             </tr>


						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control" >
                   				<html:select property="documentType" styleClass="combo" style="width:130;">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
						</tr>
						
						



         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.checkNo"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNo" size="15" maxlength="25" styleClass="text"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.payfileReferenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="payfileReferenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>

         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.creditBalance"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="creditBalanceRemaining" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>

         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.advancePayment"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="enableAdvancePayment"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmountPaid" size="15" maxlength="25" styleClass="textAmount"/>
                			</td>


         				</tr>

							<tr>
        					  <logic:equal name="arReceiptEntryForm" property="showRecalculateJournal" value="true">
        					  <td class="prompt" width="140" height="25">
					            <bean:message key="receiptEntry.prompt.recalculateJournal"/>
					          </td>
					          
					          <td class="prompt" width="140" height="25">
					        	<html:checkbox property="recalculateJournal" />
					          </td>
					          
					          </logic:equal>
        					</tr>

			        </table>
					</div>
				   <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.bankAccount"/>
               	 			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="arReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="paymentMethod" styleClass="comboRequired" style="width:130;">
                       			<html:options property="paymentMethodList"/>
                   				</html:select>
                			</td>
         				</tr>

         				 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="receiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="ATT">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						 <tr>
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename1"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename1" size="35" styleClass="text"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename2"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename2" size="35" styleClass="text"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename3"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename3" size="35" styleClass="text"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename4"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename4" size="35" styleClass="text"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
					 </table>
					 </div>


					 <div class="tabbertab" title="HR">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="receiptEntry.prompt.payrollPeriodName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="payrollPeriodName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="payrollPeriodNameList"/>
                   				</html:select>
                			</td>
				         </tr>

					 </table>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>




					 <div class="tabbertab" title="Upload 1">
					 <br>
					 <br>
						 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

							<tr>
								<td class="prompt" width="160" height="25">
						          Columns List
						        </td>
							 <td width="148" height="25" class="control">
								<html:select property="columnName" size="6" multiple="true" styleClass="comboRequired" style="width:230;">
								   <html:options property="columnList"/>
								</html:select>
							 </td>
							</tr>


						    <tr>
	         					<td class="prompt" width="160" height="25">
						          	Unique ID Code
						        </td>
						        <td width="415" height="25" class="control" colspan="3">
				                   <html:select property="uniqueIdCode" styleClass="comboRequired" >
				                       <html:options property="uniqueIdCodeList"/>
				                   </html:select>
			              	    </td>
         					</tr>

						 	<tr>
	         					<td class="prompt" width="160" height="25">
						          	Upload Receipt
						        </td>
						        <td>
		         					<html:file property="uploadFile" size="35" styleClass="textRequired"/>

							    </td>
         					</tr>
         					<tr>
         						<td class="prompt" width="160" height="25" colspan="3">
         						</td>
         					</tr>

         					<tr>
         						<td class="prompt" width="160" height="25" colspan="3">


         						<center>
         							<html:submit property="saveUploadButton" styleClass="mainButtonMedium">
							            Save Upload File
							       </html:submit>
         						</center>
         						</td>
         					</tr>


						 </table>
					 </div>
					 <div class="tabbertab" title="Upload 2">
					 <br>
					 <br>
						 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">





							<tr>
	         					<td class="prompt" width="160" height="25">
						          	Upload File Type
						        </td>
						        <td>
							        <html:select property="uploadFileType" size="6" multiple="true" styleClass="comboRequired" style="width:230;">
									   <html:options property="uploadFileTypeList"/>
									</html:select>
								</td>
         					</tr>



						 	<tr>
	         					<td class="prompt" width="160" height="25">
						          	Upload Receipt
						        </td>
						        <td>
		         					<html:file property="uploadFile2" size="35" styleClass="textRequired"/>

							    </td>
         					</tr>
         					<tr>

         						<td class="prompt" width="160" height="25" colspan="3">
         							Create Advance Payment If no invoice list found
         						    <nested:checkbox property="enableCreateAdvance" />
         						</td>
         						<td >


						        </td>
         					</tr>



         					<tr>
         						<td class="prompt" width="160" height="25" colspan="3">


         						<center>
         							<html:submit property="saveUpload2Button" styleClass="mainButtonMedium">
							            Save Upload File
							       </html:submit>
         						</center>
         						</td>
         					</tr>

						 </table>
					 </div>


					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="30" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>

		  <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		 </tr>

	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="10" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="receiptEntry.gridTitle.RCTDetails"/>
		                </td>
		            </tr>
		            <tr>
		            	<td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.pay"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.allSI"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.rebate"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <center><bean:message key="receiptEntry.prompt.ipsNumber"/></center>
				       </td>

		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.invoiceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.referenceNumber"/>
				       </td>


				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.applyAmount"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyApplyAmount"/>
				       </td>

				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.discount"/>
				       </td>
				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.currency"/>
				       </td>

				    </tr>
				    <tr>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.date"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.dueDate"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.amountDue"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyDue"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.creditBalancePaid"/>
				       </td>
				       <td width="298" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.rebate"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
				    <html:hidden property="arRCTListSize"/>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {

						if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
					    }else{
				           rowBgc = ROW_BGC2;
				       	}
				    %>
				    <nested:hidden property="tempDiscount"/>
				    <nested:hidden property="tempCreditBalancePaid"/>
				    <tr bgcolor="<%= rowBgc %>">
				    	<td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="payCheckbox" onclick="calculateAmountCheck(name,precisionUnit);"/>
				        </td>

				        <td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="allSiCheckbox" onclick="checkIssueAllSi(name,arRCTListSize);calculateAmountCheck(name,precisionUnit);"/>
				        </td>

				        <td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="rebateCheckbox" onclick="calculateRebate(name,precisionUnit);calculateAmountCheck(name,precisionUnit);"/>
				        </td>

				    	<td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:text property="installmentNumber"styleClass="text" disabled="true" size="2" maxlength="2"/>
				        </td>


				       <td width="149" height="1" class="control">
				          <nested:text property="invoiceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="100" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="10" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name,precisionUnit); addZeroes(name);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempAmount(name);"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyApplyAmount" size="10" maxlength="25" styleClass="textAmountRequired" onfocus="initializeTempAmount(name);" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="10" maxlength="25" styleClass="textAmount" onchange="calculateApplyAmount(name,precisionUnit);" onblur="calculateApplyAmount(name,precisionUnit); addZeroes(name);" onkeyup="calculateApplyAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				        <td width="149" height="1" class="control">
				          <nested:text property="currency" size="8" maxlength="10" styleClass="text" disabled="true"/>
				       </td>

				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="149" height="1" class="control">
				          <nested:text property="date" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>


				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyDue" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="creditBalancePaid" size="10" maxlength="25" styleClass="textAmount" onchange="calculateApplyAmountWithAdvancePayment(name,precisionUnit);" onblur="addZeroes(name);" onkeyup=" formatAmount(name, (event)?event:window.event);"/>
				       </td>
				       <td width="298" height="1" class="control" >
				          <nested:text property="rebate" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				    </tr>
					<% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>

	     <logic:equal name="arReceiptEntryForm" property="enableFields" value="false">
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<logic:equal name="arReceiptEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
							<td width="445" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                			</td>
						
							<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>
						
						
						
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.checkNo"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="checkNo" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.payfileReferenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="payfileReferenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>


         				</tr>
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.creditBalance"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="creditBalanceRemaining" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="4">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.advancePayment"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="enableAdvancePayment" disabled="true"/>
                			</td>

                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amountPaid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmountPaid" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>
				    <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.bankAccount"/>
               	 			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<logic:equal name="arReceiptEntryForm" property="enableReceiptVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arReceiptEntryForm" property="enableReceiptVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
                			</logic:equal>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="paymentMethod" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="paymentMethodList"/>
                   				</html:select>
                			</td>
         				</tr>

         				 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="receiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="ATT">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						 <tr>
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename1"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename2"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename3"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename4"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
					 </table>
					 </div>

					 <div class="tabbertab" title="HR">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>


				         <tr>
			         		<td width="130" height="25" class="prompt">
                  				<bean:message key="receiptEntry.prompt.payrollPeriodName"/>
                			</td>
                			<td width="157" height="25" class="control">
                  	 			<html:select property="payrollPeriodName" styleClass="comboRequired" disabled="true" style="width:130;">
                       			<html:options property="payrollPeriodNameList"/>
                   				</html:select>
                			</td>
				         </tr>

					 </table>
					 </div>

					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="30" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="arReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="arReceiptEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="arReceiptEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="arReceiptEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>

		  <tr>
	         <td width="575" height="25" colspan="4">
			 	<div id="buttons">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
		 </tr>

	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="10" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="receiptEntry.gridTitle.RCTDetails"/>
		                </td>
		            </tr>
		            <tr>
		            	<td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.pay"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.allSI"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.rebate"/></center>
				       </td>

				       <td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <center><bean:message key="receiptEntry.prompt.ipsNumber"/></center>
				       </td>

		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.invoiceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.referenceNumber"/>
				       </td>


				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.applyAmount"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyApplyAmount"/>
				       </td>

				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.discount"/>
				       </td>
				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.currency"/>
				       </td>

				    </tr>
				    <tr>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.date"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.dueDate"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.amountDue"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyDue"/>
				       </td>

				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.creditBalancePaid"/>
				       </td>
				       <td width="298" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.rebate"/>
				       </td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
				    <html:hidden property="arRCTListSize"/>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {

						if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       	}else{
				           rowBgc = ROW_BGC2;
				       	}
				    %>
				    <tr bgcolor="<%= rowBgc %>">

				       <td rowspan="2"width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="payCheckbox" disabled="true"/>
				       </td>


				       <td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="allSiCheckbox" disabled="true"/>
				        </td>

				        <td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="rebateCheckbox" disabled="true"/>
				        </td>

				        <td width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:text property="installmentNumber" styleClass="text" disabled="true" size="2" maxlength="2"/>
				        </td>


				       <td width="149" height="1" class="control">
				          <nested:text property="invoiceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="30" styleClass="text" disabled="true"/>
				       </td>
				        <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyApplyAmount" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>

				    </tr>
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="149" height="1" class="control">
				          <nested:text property="date" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>

				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>


				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyDue" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>


				       <td width="149" height="1" class="control">
				          <nested:text property="creditBalancePaid" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="rebate" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				    </tr>
					<% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	     </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arReceiptEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepReceiptPrint.do?forward=1&receiptCode=<%=actionForm.getReceiptCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="arReceiptEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

	 win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="arReceiptEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

	 win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>

----</body>
</html>
