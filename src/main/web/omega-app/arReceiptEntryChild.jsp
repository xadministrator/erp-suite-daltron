<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="receiptEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/arReceiptEntry.do?child=1" onsubmit="return submitForm();" enctype="multipart/form-data">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="0">
      <tr valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="receiptEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arReceiptEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>	
	        </td>
	     </tr>
	     <html:hidden property="isCustomerEntered" value=""/>
	     <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<logic:equal name="arReceiptEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         				</tr>
         				</logic:equal>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arReceiptEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>   
         				<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.amount"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customerDeposit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="customerDeposit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
                			</td>
         				</tr>   
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="250" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
         				<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
			        </table>
					</div>		        
				    <div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.bankAccount"/>
               	 			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="bankAccountList"/>
                   				</html:select>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.receiptVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="receiptVoid" disabled="true"/>
                			</td>
         				</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="receiptEntry.prompt.paymentMethod"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="paymentMethod" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="paymentMethodList"/>
                   				</html:select>
                			</td>   							                           
         				</tr>						
			        </table>
					</div>			        
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="receiptEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>				    
					<div class="tabbertab" title="Attachment">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
						 <tr> 
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename1"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">			                   
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton1" value="true">
							   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename2"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton2" value="true">
							   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename3"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">			                   
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton3" value="true">
							   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
						 <tr>
							<td width="160" height="25" class="prompt">
							   <bean:message key="receiptEntry.prompt.filename4"/>
							</td>
							<td width="288" height="25" class="control" colspan="2">
							   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
							</td>
							<td width="127" height="25" class="control">
							   <div id="buttons">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">			                   
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							   <div id="buttonsDisabled" style="display: none;">
							   <p align="center">
							   <logic:equal name="arReceiptEntryForm" property="showViewAttachmentButton4" value="true">
							   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								  <bean:message key="button.viewAttachment"/>
							   </html:submit>
							   </logic:equal>
							   </div>
							</td>
						 </tr>
					 </table>
					 </div>	 		
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="receiptEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>

		 <tr>
	         <td width="160" height="50">
			 	<div id="buttons">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			    <logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			   	</div>
				<div id="buttonsDisabled" style="display: none;">
				<p align="left">
				 <logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="false">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableFirstButton" value="true">
			   	<html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.first"/>
				</html:submit>
				</logic:equal>
			   	<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="false">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disablePreviousButton" value="true">
			   	<html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.previous"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="false">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableNextButton" value="true">
				<html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.next"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="false">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
				<logic:equal name="arReceiptEntryForm" property="disableLastButton" value="true">
			   	<html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					<bean:message key="button.last"/>
				</html:submit>
				</logic:equal>
			 	</div>
			 </td>
			 <td width="415" height="50" colspan="3"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			 </td>
		</tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="receiptEntry.gridTitle.RCTDetails"/>
		                </td>
		            </tr>
		            <tr>
		           
		               <tr>
		            	<td width="149"  rowspan="2" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <center><bean:message key="receiptEntry.prompt.pay"/></center>
				       </td>
				       
				       
		            	
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.invoiceNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.referenceNumber"/>
				       </td>
				       
				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.applyAmount"/>				           
				       </td>
				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyApplyAmount"/>
				       </td>
				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.discount"/>				           
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.currency"/>				           
				       </td>				       			       			    
				      
				    </tr>							    
				    <tr>
				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.date"/>
				       </td>				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.dueDate"/>
				       </td>
				      
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.amountDue"/>				           
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.penaltyDue"/>				           
				       </td>
				       
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.creditBalancePaid"/>
				       </td>				       
				       <td width="298" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="receiptEntry.prompt.rebate"/>
				       </td>				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define 	id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
				    <nested:iterate property="arRCTList">
				    <%
				       i++;
				       if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + actionForm.getMaxRows() - 1) {

						if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       	}else{
				           rowBgc = ROW_BGC2;
				       	}  
				    %>
				    <tr bgcolor="<%= rowBgc %>">	
				    
				       <td rowspan="2"width="149" align="center" height="1" class="control" rowspan="2">
				          <nested:checkbox property="payCheckbox" disabled="true"/>
				       </td>					       	       
				       <td width="149" height="1" class="control">
				          <nested:text property="invoiceNumber" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="referenceNumber" size="12" maxlength="30" styleClass="text" disabled="true"/>
				       </td>
				        <td width="149" height="1" class="control">
				          <nested:text property="applyAmount" size="12" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				       
				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyApplyAmount" size="12" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
				       </td>
				      
				       <td width="149" height="1" class="control">
				          <nested:text property="discount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="currency" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				      			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="149" height="1" class="control">
				          <nested:text property="date" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>	
				       
				       <td width="149" height="1" class="control">
				          <nested:text property="dueDate" size="12" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       			       
				      
				       <td width="149" height="1" class="control">
				          <nested:text property="amountDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="penaltyDue" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				      
				       
				       <td width="149" height="1" class="control">
				          <nested:text property="creditBalancePaid" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>		       			       
				       <td width="298" height="1" class="control" colspan="2">
				          <nested:text property="rebate" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>				       			       
				    </tr>
					<% } %>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>		       
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="arReceiptEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arReceiptEntryForm" type="com.struts.ar.receiptentry.ArReceiptEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  
	 win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>


</body>
</html>
