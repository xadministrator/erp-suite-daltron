<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="receiptImport.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers



function enableMiscReceiptFields(val) 
{

	if(val == false) {
	
		document.forms[0].miscReceiptType.disabled = true;
		document.forms[0].taxCode.disabled = true;
		document.forms[0].customer.disabled = true;
		document.forms[0].discountTaxable.disabled = true;
		document.forms[0].serviceChargeTaxable.disabled = true;
		
	} else {
	
		document.forms[0].miscReceiptType.disabled = false;
		document.forms[0].taxCode.disabled = false;
		document.forms[0].customer.disabled = false;
		document.forms[0].discountTaxable.disabled = false;
		document.forms[0].serviceChargeTaxable.disabled = false;
				
	}
	
}

//Done Hiding--> 
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arReceiptImport.do" onsubmit="return disableButtons();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="260">
      <tr valign="top">
        <td width="187" height="260"></td> 
        <td width="581" height="260">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="260" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="receiptImport.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		  	 	<logic:equal name="arReceiptImportForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               		<bean:message key="app.success"/>
           		</logic:equal>
		   		<html:errors/>	
			   	<html:messages id="msg" message="true">
			       <bean:write name="msg"/>		   
			   	</html:messages>
	        </td>

	     </tr>
	     <tr>
	     	<td width="575" height="10" colspan="4">
	     		<div class="tabber">
        	    <div class="tabbertab" title="Header">
        	    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
        	    	<tr>
        	    		<td class="prompt" width="150" height="25"/>
        	    	</tr>
			     	<tr>
				     	 <td class="prompt" width="150" height="25">
				            <bean:message key="receiptImport.prompt.headerFile"/>
				         </td>
				         <td width="425" height="25" class="control" colspan="3">
				            <html:file property="filename1" size="35" styleClass="textRequired"/>
				         </td>
					 </tr>
					 <tr>
						 <td class="prompt" width="150" height="25">
				            <bean:message key="receiptImport.prompt.detailsFile"/>
				         </td>
				         <td width="425" height="25" class="control" colspan="3">
				            <html:file property="filename2" size="35" styleClass="textRequired"/>
				         </td>
					 </tr>
				</table>
				</div>
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
        	    		<td class="prompt" width="150" height="25"/>
        	    	</tr>
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.importMiscReceipt"/>
						</td>
						<td class="control" width="425" height="25">
							<html:checkbox property="importMiscReceipt" onclick="enableMiscReceiptFields(checked);"/>
						</td>
					</tr>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="false">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.miscReceiptType"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="miscReceiptType" styleClass="comboRequired" disabled="true">
						       <html:options property="miscReceiptTypeList"/>
						    </html:select>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="true">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.miscReceiptType"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="miscReceiptType" styleClass="comboRequired">
						       <html:options property="miscReceiptTypeList"/>
						    </html:select>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="false">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.customer"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="customer" styleClass="combo" disabled="true">
						       <html:options property="customerList"/>
						    </html:select>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="true">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.customer"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="customer" styleClass="combo">
						       <html:options property="customerList"/>
						    </html:select>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="false">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.taxCode"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="taxCode" styleClass="comboRequired" disabled="true">
						       <html:options property="taxCodeList"/>
						    </html:select>
						</td>
					</tr>	
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="true">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.taxCode"/>
						</td>	
						<td class="control" width="425" height="25">
							<html:select property="taxCode" styleClass="comboRequired">
						       <html:options property="taxCodeList"/>
						    </html:select>
						</td>
					</tr>	
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="false">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.serviceChargeTaxable"/>
						</td>
						<td class="control" width="425" height="25">
							<html:checkbox property="serviceChargeTaxable" disabled="true"/>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="true">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.serviceChargeTaxable"/>
						</td>
						<td class="control" width="425" height="25">
							<html:checkbox property="serviceChargeTaxable"/>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="false">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.discountTaxable"/>
						</td>
						<td class="control" width="425" height="25">
							<html:checkbox property="discountTaxable" disabled="true"/>
						</td>
					</tr>
					</logic:equal>
					<logic:equal name="arReceiptImportForm" property="importMiscReceipt" value="true">
					<tr>
						<td class="prompt" width="150" height="25">
							<bean:message key="receiptImport.prompt.discountTaxable"/>
						</td>
						<td class="control" width="425" height="25">
							<html:checkbox property="discountTaxable"/>
						</td>
					</tr>
					</logic:equal>
				</table>		
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			 </td>
	     </tr>
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="importButton" styleClass="mainButton">
		         <bean:message key="button.import"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="importButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.import"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["headerFile"] != null)
             document.forms[0].elements["headerFile"].focus()
   // -->
</script>
</body>
</html>
