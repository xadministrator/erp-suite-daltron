<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="customerList.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/arRepCustomerList.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="250">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="customerList.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	     	<td width="575" height="10" colspan="4">
		     	 <div class="tabber">
	     		 <div class="tabbertab" title="Header">
	     		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	     		 	 <tr> 
				         <td class="prompt" width="575" height="25" colspan="4">
		                 </td>
		             </tr>
				     <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.customerCode"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:text property="customerCode" size="25" maxlength="25" styleClass="text"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.date"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
				         </td>	         
			         </tr>         
			         <tr>
			         	 <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.salesPerson"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:text property="salesPerson" size="25" maxlength="25" styleClass="text"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.region"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:text property="region" size="25" maxlength="25" styleClass="text"/>
				         </td>
				     </tr>
			         <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.type"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:select property="type" styleClass="combo">
						      <html:options property="typeList"/>
						   </html:select>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.customerClass"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:select property="customerClass" styleClass="combo">
						      <html:options property="customerClassList"/>
						   </html:select>
				         </td>
			         </tr>                 
			         <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.viewType"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="customerList.prompt.orderBy"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:select property="orderBy" styleClass="combo">
						      <html:options property="orderByList"/>
						   </html:select>
				         </td>
			         </tr>
			         <tr>
			     			<td class="prompt" width="140" height="25">
				            Group By
				         	</td>
			        		<td width="127" height="25" class="control">
				            <html:select property="groupBy" styleClass="combo">
						      <html:options property="groupByList"/>
						   </html:select>
				         </td>
				         <td class="prompt" width="140" height="25">
				            Split by Salesperson
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="splitBySalesperson"/>
				         </td>
			         </tr>
			         <tr>
			         	 <td class="prompt" width="140" height="25">
				            Include Negative Balances
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="includedZeroes"/>
				         </td>
			         </tr>
				</table>
				</div>
				<div class="tabbertab" title="Branch Map">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	         	 	 <tr> 
				         <td class="prompt" width="575" height="25" colspan="4">
		                 </td>
		             </tr>
	            	 <nested:iterate property="arRepBrCstList">
	            		 <tr>
	            		     <td class="prompt" width="" height="25">
		            			<nested:write property="brBranchCode"/>
		            		 </td>
		            		 <td class="prompt" width="" height="25"> - 
		            		 </td>
		            		 <td class="prompt" width="140" height="25">
		            			<nested:write property="branchName"/>
		            		 </td>
		            		 <td width="435" height="25" class="control" colspan="3">
		            			<nested:checkbox property="branchCheckbox"/>
		            		 </td>
		            	 </tr>
	            	 </nested:iterate>
		         </table>
		         </div>
		         </div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		 </tr>
	     <tr>
	        <td width="575" height="45" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null)
             document.forms[0].elements["customerCode"].focus()
   // -->
</script>
<logic:equal name="arRepCustomerListForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
