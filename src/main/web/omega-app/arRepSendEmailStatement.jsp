<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="statement.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


<script Language="JavaScript" type="text/javascript"> 

  function viewPDF()
  {            
      if (document.activeElement.name != 'customerMessage') {
      
          return enterSubmit(event, new Array('goButton'));
      
      } else {
      
          return true;
      
      }
       
  }
  
  function clickIncludeCollections()
  {
  		if(document.forms[0].includeCollections.checked == true) {
			document.forms[0].dateFrom.disabled=false;
	  		document.forms[0].dateFrom.value="";
	  	} else  {
  			document.forms[0].dateFrom.disabled=true;
	  		document.forms[0].dateFrom.value="";
	  	}
		
  }

</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return statementEnterSubmit();">
<html:form action="/arRepSendEmailStatement.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   Ar Send Statement by Email
		</td>
	     </tr>
         <tr>
            <!-- success and error message reminders -->
             
	        <td width="575" height="44" colspan="4" class="statusBar">
		   		<logic:equal name="arRepSendEmailStatementForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                </logic:equal>
                <br>
		  		<html:errors/>	
	        </td>
	        
	     </tr>	 
	     
	     <tr>
	     	<td class="prompt" width="185" height="25">
	          Host
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="emailHost" size="35" maxlength="50" styleClass="textRequired"/>
	       </td>
	     
	     
	     </tr>
	     
	     <tr>
	     	<td class="prompt" width="185" height="25">
	          Port
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="emailPort" size="35" maxlength="50" styleClass="textRequired"/>
	       </td>
	     
	     
	     </tr>
	     
	     
	     
	     
	     
	     <tr>
	     	<td class="prompt" width="185" height="25">
	          Username/E-mail Recipient
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="emailUsername" size="35" maxlength="50" styleClass="textRequired"/>
	       </td>
	     </tr>
	     
	     <tr>
	     
	     	<td class="prompt" width="185" height="25">
	         	Password
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:password property="emailPassword" size="35" maxlength="50" styleClass="textRequired"/>
	       </td>
	     
	     
	     </tr>
	     
	     <tr>
	     	<td class="prompt" width="185" height="25">
	         	CC 1
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="cc1" size="35" maxlength="50" styleClass="text"/>
	       </td>
	     </tr>
	     
	     <tr>
	     	<td class="prompt" width="185" height="25">
	         	CC 2
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="cc2" size="35" maxlength="50" styleClass="text"/>
	       </td>
	     </tr>
	     
	     
	     
	     <tr>
	     
	     	<td class="prompt" width="185" height="25">
	         	Save info when Send
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:checkbox property="saveInfo"  />
	       </td>
	     	
	     
	     </tr>
	     
	         
         <tr>
	       <td class="prompt" width="185" height="25">
	          Subject/Title
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="emailSubject" size="35" maxlength="50" styleClass="text"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width=185 height="25">
	          Message
	       </td>
	       <td width="390" height="25" class="control" colspan="1">
	          <html:textarea property="emailMessage" styleClass="text" rows="10" cols="30"/>
	       </td>
	      
         </tr>               
         
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			       
					<td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="sendButton" styleClass="mainButton">
				         	Send
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>	
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="sendButton" styleClass="mainButton" disabled="true">
				         	Send
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				    </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	        <td width="575" height="185" colspan="4">
			    <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
				     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
				     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				      <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          Customer List
	                       </td>
	                  </tr>
				     	<tr>
					       <td width="186" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" rowspan="2">
					          Customer Code
					       </td>
					       
					       <td width="186" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          Customer Name
					       </td>
					       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          Email Address
					       </td>	
					       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" rowspan="2">
					          View
					       </td>		       
					       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" rowspan="2">
					          Enable Send
					       </td>
					       		       			       			       			       
		                </tr>	
		                <tr>
		               		 <td width="300" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          File Location
					       </td>
		                </tr>
		                <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate  property="arCtmrList"  >
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="186" height="1" class="gridLabel" rowspan="2" >
					          <nested:write property="customerCode"/>
					       </td>
					       <td width="186" height="1" class="gridLabel">
					          <nested:write property="customerName"/>
					       </td>
					   
					       <td width="187" height="1" class="gridLabel">
					          <nested:write property="customerEmail"/>
					       </td>
					      
					       <td width="100" align="center" height="1" rowspan="2">
					     		 <div id="buttons">
					     	
					             	<nested:submit  property="viewPDFButton" styleClass="mainButton">
						              	View PDF
						            </nested:submit>
					             
							  
						         </div>	
					       </td>
					        <td width="80" align="center" height="1" rowspan="2" class="control">
					     		 <nested:checkbox property="selectedMail"/>
					       </td>
					   </tr>
					 
				
					   <tr>
					       <td width="186" colspan="2" height="1" class="gridLabel">
					      		<nested:write  property="fileLocation"/>
					       </td>
					   </tr>
					  </nested:iterate>
				  
				      
				      
				    </table>    
			    </div>
		    </td>
	     </tr>
	     <tr>
	       <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		   </td>
	     </tr>
      </table>

    </td>
   </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null)
             document.forms[0].elements["customerCode"].focus()
   // -->
</script>

<logic:equal name="arRepSendEmailStatementForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
 
 
 
 
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
 
</script>
</logic:equal>

</body>
</html>
