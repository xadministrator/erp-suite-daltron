<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="statement.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript">




function exportReminder(){

	var r = confirm("You will export SOA, water and Customer Ledger to all customers. It takes several minutes to export it all. Do you want to continue?");

	return r;
}








</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


<script Language="JavaScript" type="text/javascript">

  function statementEnterSubmit()
  {
      if (document.activeElement.name != 'customerMessage') {

          return enterSubmit(event, new Array('goButton'));

      } else {

          return true;

      }

  }

  function clickIncludeCollections()
  {
        if(document.forms[0].includeCollections.checked == true) {
                document.forms[0].dateFrom.disabled=false;
                document.forms[0].dateFrom.value="";
        } else  {
                document.forms[0].dateFrom.disabled=true;
                document.forms[0].dateFrom.value="";
        }

  }

</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return statementEnterSubmit();">
<html:form action="/arRepStatement.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="350">
      <tr valign="top">
        <td width="187" height="350"></td>
        <td width="581" height="350">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="350"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="statement.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>
		       <html:messages id="messages" message="true">
		       		<br><font color="green"><bean:write name="messages"/></font><br>
		   	   </html:messages>
	        </td>
	     </tr>
	     <tr>
	     	 <td width="575" height="10" colspan="4">
		     	 <div class="tabber">
	     		 <div class="tabbertab" title="Header">
	     		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	     		 	 <tr>
				         <td class="prompt" width="575" height="25" colspan="4">
		                 </td>
		             </tr>

		             <tr>
						<td class="prompt" width="140" height="25" colspan="3">
				            <bean:message key="statement.prompt.customerBatch"/>
				             <br>&emsp;
				            <html:select property="customerBatchSelectedList" style="width:180px" size="7" multiple="true" styleClass="combo">
							   <html:options property="customerBatchList"/>
							</html:select>
				         </td>



  						<td width="127" height="25" class="control" colspan="2">

						  </td>


					</tr>


					<tr>

							<td class="prompt" width="140" height="25" colspan="3">
				            <bean:message key="statement.prompt.customerDepartment"/>
				               <br>&emsp;

				               <html:select property="customerDepartmentSelectedList" style="width:250px" size="7" multiple="true" styleClass="combo">
							   <html:options property="customerDepartmentList"/>
							</html:select>
				         </td>


 						 <td width="127" height="25" class="control" colspan="2">

						  </td>

					</tr>


				     <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.customerCode"/>
				         </td>
						 <logic:equal name="arRepStatementForm" property="useCustomerPulldown" value="true">
				         <td width="435" height="25" class="control" colspan="3">
				            <html:select property="customerCode" styleClass="comboRequired">
						      <html:options property="customerCodeList"/>
						    </html:select>
							<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
						 </td>
						 </logic:equal>
						 <logic:equal name="arRepStatementForm" property="useCustomerPulldown" value="false">
				         <td width="435" height="25" class="control" colspan="3">
				            <html:text property="customerCode" styleClass="text" size="15" maxlength="25" readonly="true"/>
							<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
						 </td>
						 </logic:equal>
					</tr>


					<tr>
						<td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.invoiceBatchName"/>
				         </td>
				          <td width="127" height="25" class="control">
				            <html:select property="invoiceBatchName" styleClass="combo">
				              <html:options property="invoiceBatchNameList"/>
						   </html:select>
				         </td>

					</tr>
			        <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.customerType"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:select property="customerType" styleClass="combo">
						      <html:options property="customerTypeList"/>
						   </html:select>
				         </td>

			         </tr>

			         <tr>
			          <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.customerClass"/>
				         </td>
				         <td width="128" height="25" class="control" colspan="2">
				            <html:select property="customerClass" styleClass="combo">
						      <html:options property="customerClassList"/>
						   </html:select>
				         </td>
			         </tr>
			         <tr>
			             <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.dateFrom"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.dateTo"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
				         </td>
				     </tr>
				     <tr>
			             <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.invoiceNumberFrom"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:text property="invoiceNumberFrom" size="10" maxlength="10" styleClass="text"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.invoiceNumberTo"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:text property="invoiceNumberTo" size="10" maxlength="10" styleClass="text"/>
				         </td>
				     </tr>

				     <tr>
						<td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.groupBy"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:select property="groupBy" styleClass="combo">
						      <html:options property="groupByList"/>
						   </html:select>
				         </td>
						<td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.orderBy"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:select property="orderBy" styleClass="combo">
						      <html:options property="orderByList"/>
						   </html:select>
				         </td>
					 </tr>


			         <tr>

				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeCollections"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="includeCollections"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.showAll"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="showAll"/>
				         </td>
				     </tr>
				     <tr>

				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeZero"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="includeZero"/>
				         </td>

				           <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeAdvanceOnly"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="includeAdvanceOnly"/>
				         </td>
				     </tr>
				     
				     
				  
				     
				     
				   <%--  <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.waterOnly"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="waterOnly"/>
				         </td>


			         </tr>

			          <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeWater"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="includeWater"/>
				         </td>


			         </tr> --%>

			         <tr>


				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeAdvance"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="includeAdvance"/>
				         </td>


    					<td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.showEntries"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="showEntries"/>
				         </td>


			         </tr>
			         
			            
				     <tr>
				     
			            <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.includeCreditMemo"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:checkbox property="includeCreditMemo"/>
				         </td>
				     
				     </tr>

			         <tr>



				         <td class="prompt" width="140" height="25" colspan="2">
				            <bean:message key="statement.prompt.includeUnpostedTransaction"/>
				             &nbsp;<html:checkbox property="includeUnpostedTransaction"/>
				         </td>
				         <td width="127" height="25" class="control">

				         </td>



			         </tr>

			         <tr>



				         <td class="prompt" width="140" height="25" colspan="2">
				            <bean:message key="statement.prompt.showUnearnedInterestTransactionOnly"/>
				            &nbsp;<html:checkbox property="showUnearnedInterestTransactionOnly"/>
				         </td>
				         <td width="127" height="25" class="control">

				         </td>




			         </tr>

			       <%--   <tr>

			         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.sortByTransaction"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="sortByTransaction"/>
				         </td>


			         </tr>
			         <tr>
			         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.sortByItem"/>
				         </td>
				         <td width="127" height="25" class="control">
				            <html:checkbox property="sortByItem"/>
				         </td>


			         </tr> --%>


			         <tr>
				         <td class="prompt" width="140" height="25" colspan="3">
				            <bean:message key="statement.prompt.customerMessage"/>
				             <br>&emsp;
				             <html:textarea property="customerMessage" cols="30" rows="5" styleClass="text"/>
				         </td>

			         </tr>
			         <tr>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.collectionHead"/>
				         </td>
				         <td width="435" height="25" class="control" colspan="3">
				            <html:text property="collectionHead" size="25" maxlength="25" styleClass="text"/>
				         </td>
			         </tr>
			         <tr>
						  <td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.viewType"/>
				         </td>
				         <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				         </td>
			         </tr>

			         <tr>

						<td class="prompt" width="140" height="25">
						    <bean:message key="statement.prompt.memoLine"/>
						</td>
						<td width="128" height="25" class="control" colspan="3">
						    <html:select property="memoLine" styleClass="combo">
							<html:options property="memoLineList"/>
							</html:select>
						</td>



					</tr>

					<tr>

						<td class="prompt" width="140" height="25">
						    <bean:message key="statement.prompt.reportType"/>
						</td>
						<td width="128" height="25" class="control" colspan="2">
						    <html:select property="reportType" styleClass="combo">
							<html:options property="reportTypeList"/>
							</html:select>
						</td>



					</tr>


			    </table>
			    </div>
			    <div class="tabbertab" title="Branch Map">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	         	 	 <tr>
				         <td class="prompt" width="575" height="25" colspan="4">
		                 </td>
		             </tr>
	            	 <nested:iterate property="arRepBrStList">
	            		 <tr>
	            		     <td class="prompt" width="" height="25">
		            			<nested:write property="brBranchCode"/>
		            		 </td>
		            		 <td class="prompt" width="" height="25"> -
		            		 </td>
		            		 <td class="prompt" width="140" height="25">
		            			<nested:write property="branchName"/>
		            		 </td>
		            		 <td width="435" height="25" class="control" colspan="3">
		            			<nested:checkbox property="branchCheckbox"/>
		            		 </td>
		            	 </tr>
	            	 </nested:iterate>
		         </table>
		         </div>

		         <div class="tabbertab" title="Export File">

		         <br>
		         <br>
			         <table border="0" cellpadding="0" cellspacing="0" width="270" height="25">
		         	 	<tr>
							<td class="prompt" width="140" height="25">
					            <bean:message key="statement.prompt.customerBatch"/>
					         </td>


					         <td width="127" height="25" class="control">
								<html:select property="customerBatchSelectedListExport" size="7" multiple="true" styleClass="combo">
								   <html:options property="customerBatchList"/>
								</html:select>
							  </td>

						</tr>
		         	 	<tr>
		         	 		<td class="prompt" width="140" height="25">
		            			SOA Date
		            		</td>

		         	 		  <td width="127" height="25" class="control">

						       	<html:text property="soaDateExport" size="10" maxlength="10" styleClass="text"/>

		         	 		</td>
		         	 	</tr>

		         	 	<tr>
		         	 		<td class="prompt" width="140" height="25">

		            		</td>
			              	<td width="127" height="25" class="control">

			               		<div id="buttons">
			               		 	 <p align="center">
			                 		<html:submit property="exportButton" styleClass="mainButton" onclick="return exportReminder()" >
							         Export PDF
							  		</html:submit>
							  		 </p>
							   </div>
							    <div id="buttonsDisabled" style="display: none;">


							     <html:submit property="exportButton" styleClass="mainButton" >
							         Export PDF
							      </html:submit>
							      </p>
							    </div>

			                </td>
			             </tr>



			         </table>

		         </div>

		          <div class="tabbertab" title="Create CSV for E-mail">

			         <br>
			         <br>
				         <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				         	<tr>
				         	  <td class="prompt" width="140" height="25">
						         CSV File name
						      </td>

				         	  <td width="127" height="25" class="control">
						         <html:text property="csvFilename" size="50" maxlength="70" styleClass="textRequired"/>
						      </td>
				         	</tr>

				         	<tr>
				         		<td class="prompt" width="140" height="25">
						         Subject
						      </td>

				         	  <td width="127" height="25" class="control">
						         <html:text property="emailSubject" size="50" maxlength="250" styleClass="text"/>
						      </td>

				         	</tr>


				         	<tr>
				         		<td class="prompt" width="140" height="25">
						         Message
						      </td>

				         	  <td width="127" height="25" class="control">
						         <html:textarea property="emailMessage"  cols="50" rows="10" styleClass="text"/>
						      </td>

				         	</tr>



				         	 <tr>
						         <td class="prompt" width="575" height="15" colspan="4">
				                 </td>
		             		 </tr>

		             		 <tr>
								<td class="prompt" width="140" height="25">
						            <bean:message key="statement.prompt.customerBatch"/>
						         </td>


						         <td width="127" height="25" class="control">
									<html:select property="customerBatchSelectedListMail" size="7" multiple="true" styleClass="combo">
									   <html:options property="customerBatchList"/>
									</html:select>
								  </td>

							</tr>

		             		  <tr>
						         <td class="prompt" width="575" height="15" colspan="4">
				                 </td>
		             		 </tr>

						     <tr>
						         <td class="prompt" width="140" height="25">
						            <bean:message key="statement.prompt.customerCode"/>
						         </td>
								 <logic:equal name="arRepStatementForm" property="useCustomerPulldown" value="true">
						        	 <td width="435" height="25" class="control" colspan="3">
						            	<html:select property="customerCodeMail" styleClass="comboRequired">
								      		<html:options property="customerCodeList"/>
								    	</html:select>
										<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCodeMail', '', '');"/>
								 	</td>
								 </logic:equal>
							 </tr>

			         	 	<tr>

			         	 		 <td class="prompt" width="170" height="25">
						            Include customer have no email
						         </td>
						         <td width="127" height="25" class="control">
						            <html:checkbox property="includeNoEmail"/>
						         </td>

			         	 	</tr>



		         	 		<tr>
		         	 			<td class="prompt"  height="25">
		            				SOA Date
		            			</td>

		         	 		 	 <td width="127" height="25" class="control">

						       		<html:text property="soaDateMail" size="10" maxlength="10" styleClass="text"/>

		         	 			</td>

		         	 		</tr>
		         	 		<tr>
		         	 			<td>
		         	 			</td>
		         	 		     <td >
		         	 				<html:submit property="emailButton" styleClass="mainButton" >
					        			Create CSV
					      			</html:submit>
		         	 			</td>
				         	</tr>

				     	</table>

		          </div>
		          <div class="tabbertab" title="Upload Invoice">

			         <br>
			         <br>
			          <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			         	<tr>
			         		<td  width="127" class="prompt"  height="25">
		            			Upload Type
		            		</td>
						 	<td class="prompt" width="575" height="15" colspan="4">
						 		<html:select property="uploadInvoiceType" styleClass="comboRequired">
							   	  <html:options property="uploadInvoiceTypeList"/> -
							 	</html:select>

				            </td>
		             	</tr>

		             	<tr>
		             		<td class="prompt" width="575" height="25" colspan="4">

			               		<div id="buttons">
			               		 	 <p align="center">
			                 		<html:submit property="exportInvoice" styleClass="mainButton"  >
							         Export All
							  		</html:submit>
							  		 </p>
							    </div>
							    <div id="buttonsDisabled" style="display: none;">

							    <p align="center">
							     <html:submit property="exportInvoice" styleClass="mainButton" >
							         Export All
							      </html:submit>
							      </p>
							    </div>

			                </td>
		             	</tr>





			          </table>
			      </div>

		       </div><script>tabberAutomatic(tabberOptions)</script>
		 	</td>
	     <tr>
	        <td width="575" height="50" colspan="4">
	          <div id="buttons">




	          <p align="right">



		      <html:submit property="goButton" styleClass="mainButton" >
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
		        <p align="right">


		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>

</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["customerCode"] != null)
             document.forms[0].elements["customerCode"].focus()
   // -->
</script>
<logic:equal name="arRepStatementForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
 // <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
