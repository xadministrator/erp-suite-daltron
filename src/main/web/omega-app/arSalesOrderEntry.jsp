<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="salesOrderEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function enableItemDescTrig(lnNum)
{
	var lnNumName = lnNum;
	for (var i=0; i < document.forms[0].elements.length; i++) {
         if (document.forms[0].elements[i].name == lnNumName) {
			if(document.forms[0].elements[i].checked == true) {
				document.forms[0].elements[i - 1].disabled = false;
			} else if(document.forms[0].elements[i].checked == false) {
				document.forms[0].elements[i - 1].disabled = true;
			}

         }
    }
}

function submitForm()
{
      disableButtons();
      enableInputControls();
}

function salesOrderEnterSubmit()
  {
      if (document.activeElement.name != 'shipTo' &&
          document.activeElement.name != 'billTo' &&
          document.activeElement.name != 'memo') {

          return enterSubmit(event, new Array('saveSubmitButton'));

      } else {

          return true;

      }

  }

function calculateAmount(name,precisionUnit)
{


  var property = name.substring(0,name.indexOf("."));

  var quantity = 0;
  var unitPrice = 0;
  var amount = 0;

  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitPrice"].value != "" &&
      document.forms[0].elements[property + ".itemName"].value != "") {

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {

	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitPrice"].value))) {

	  	unitPrice = (document.forms[0].elements[property + ".unitPrice"].value).replace(/,/g,'');

	  }

	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

	  }

      amount = (quantity * unitPrice).toFixed(parseInt(precisionUnit.value));

      if (!isNaN(parseFloat(document.forms[0].elements[property + ".discount1"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount2"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".discount3"].value)) && !isNaN(parseFloat(document.forms[0].elements[property + ".discount4"].value)) &&
       	!isNaN(parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)) &&
       	(parseFloat(document.forms[0].elements[property + ".discount1"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount2"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".discount3"].value)!= 0 || parseFloat(document.forms[0].elements[property + ".discount4"].value)!= 0 ||
       	parseFloat(document.forms[0].elements[property + ".fixedDiscountAmount"].value)!= 0)) {

   		  discount1 = document.forms[0].elements[property + ".discount1"].value.replace(/,/g,'') / 100;
   		  discount2 = document.forms[0].elements[property + ".discount2"].value.replace(/,/g,'') / 100;
   		  discount3 = document.forms[0].elements[property + ".discount3"].value.replace(/,/g,'') / 100;
   		  discount4 = document.forms[0].elements[property + ".discount4"].value.replace(/,/g,'') / 100;

   		  fixedDiscountAmount = document.forms[0].elements[property + ".fixedDiscountAmount"].value.replace(/,/g,'') * 1;

   		  var totalDiscountAmount = 0;

		  if (document.forms[0].elements["taxType"] != null && document.forms[0].elements["taxType"].value == "INCLUSIVE") {

			 amount = (amount / (1 + (document.forms[0].elements["taxRate"].value / 100)));

		  }
	      if (discount1 > 0) {
	    	var discountAmount = amount * discount1;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount2 > 0) {
	    	var discountAmount = amount * discount2;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount3 > 0) {
	    	var discountAmount = amount * discount3;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }
	      if (discount4 > 0) {
	    	var discountAmount = amount * discount4;
	  		totalDiscountAmount += discountAmount;
	  		amount = (amount -discountAmount);
	      }

	      if(fixedDiscountAmount != 0) {
	  	  	totalDiscountAmount	= fixedDiscountAmount.toFixed(parseInt(precisionUnit.value));
	  	  	amount = (amount - totalDiscountAmount);
	  	  }

	      if (document.forms[0].elements["taxType"].value == "INCLUSIVE") {

	  		amount = (amount * (1 + (document.forms[0].elements["taxRate"].value / 100))).toFixed(parseInt(precisionUnit.value));

	  	  }

		  document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount(totalDiscountAmount.toString());
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

      } else {

          document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  	 document.forms[0].elements[property + ".totalDiscount"].value = formatDisabledAmount("0.00");

      }

  }

}


var currProperty;
var currName;
function fnOpenDiscount(name){

   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;
   window.open("discount.jsp", "", "width=260,height=230,scrollbars=no,status=no");

   return false;

}


function showAdLog(module)
{

	var moduleKey = document.forms[0].elements["moduleKey"].value;
	//alert(moduleKey);
	//document.forms[0].elements["salesOrderCode"].value;
	var transactionNumber = document.forms[0].elements["documentNumber"].value;
	 window.open("adLog.do?forward=1&moduleKey=" + moduleKey + "&module=" + module + "&transactionNumber="+ transactionNumber ,"adLog","width=605,height=500,scrollbars,status");

	   return false;


}




//Done Hiding-->


function fnOpenMisc(name){


   var property = name.substring(0,name.indexOf("."));
   currProperty = property;
   currName = name;


   var quantity = document.forms[0].elements[property + ".quantity"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   var specs = new Array();
   var custodian = new Array(document.forms[0].elements["userList"].value);

   var custodian2 = new Array();
   var propertyCode = new Array();
   var expiryDate = new Array();
   var serialNumber = new Array();


   var tgDocNum = new Array();
   var arrayMisc = miscStr.split("_");

	//cut miscStr and save values
	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("@");
	propertyCode = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("<");
	serialNumber = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(">");
	specs = arrayMisc[0].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split(";");
	custodian2 = arrayMisc[0].split(",");
	//expiryDate = arrayMisc[1].split(",");

	miscStr = arrayMisc[1];
	arrayMisc = miscStr.split("%");
	expiryDate = arrayMisc[0].split(",");
	tgDocNum = arrayMisc[1].split(",");

   //var userList = document.forms[0].elements["userList"].value;
   //check at least one disabled field
   var isDisabled = document.forms[0].elements[property + ".quantity"].disabled;
   var index = 0;

///   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");
 window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
		   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&serialNumber=" + serialNumber
		   + "&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

   return false;

}






</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return salesOrderEnterSubmit();">
<html:form action="/arSalesOrderEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data" >
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="800" height="510">
  	<tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
        	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   		<tr>
	     		<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="salesOrderEntry.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="arSalesOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               		<bean:message key="app.success"/>
           			</logic:equal>
		   			<html:errors/>
		   			<html:messages id="msg" message="true">
		       		<bean:write name="msg"/>
		   			</html:messages>
	        	</td>
	     	</tr>

		    <bean:define id="actionForm" name="arSalesOrderEntryForm" type="com.struts.ar.salesorderentry.ArSalesOrderEntryForm"/>
			<html:hidden property="moduleKey" value="<%=actionForm.getSalesOrderCode()==null?"":actionForm.getSalesOrderCode().toString()%>"/>
			
			
	     	<html:hidden property="isCustomerEntered" value=""/>
	     	<html:hidden property="precisionUnit"/>
		    <html:hidden property="taxRate"/>
		    <html:hidden property="taxType"/>
		    <html:hidden property="arSOLListSize"/>
		 	<html:hidden property="isTaxCodeEntered" value=""/>
		    <html:hidden property="enableFields"/>
	     	<html:hidden property="isConversionDateEntered" value=""/>
	     	<html:hidden property="isCurrencyEntered" value=""/>
	     	
	     	<html:hidden property="userList"/>
	     	<logic:equal name="arSalesOrderEntryForm" property="enableFields" value="true">
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
                        <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arSalesOrderEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arSalesOrderEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				
         				<tr>
         				<!-- 
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" >
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                   			</td>
                   		-->	
                   			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.documentNumber" />
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				
         				</tr>
         				 
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
							
						</tr>
						
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.transactionStatus"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="transactionStatus" styleClass="combo" style="width:130;" >
                       			<html:options property="transactionStatusList"/>
                   				</html:select>
                   			</td>
						</tr>
						
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="salesOrderEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeList"/>
						   </html:select>
				          </td>

         				</tr>
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderMobile"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="salesOrderMobile"/>
                			</td>
			            </tr>
						<tr>
							<logic:equal name="arSalesOrderEntryForm" property="enablePaymentTerm" value="true">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>
                			<logic:equal name="arSalesOrderEntryForm" property="enablePaymentTerm" value="false">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>
							<logic:equal name="arSalesOrderEntryForm" property="enableSalesOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="salesOrderVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arSalesOrderEntryForm" property="enableSalesOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="salesOrderVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.salesperson"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>

						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.soAdvanceAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="soAdvanceAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>

						</tr>

						 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>

			        </table>
					</div>
			        <div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
			            <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="salesOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text"/>
			    			</td>
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="salesOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text"/>
			    			</td>
						</tr>


					</table>

					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25" >
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
					        
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="taxCode" styleClass="comboRequired" onchange="enterSelect('taxCode','isTaxCodeEntered');">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" onchange="return enterSelect('currency','isCurrencyEntered');">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
					
					<div class="tabbertab" title="Delivery">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             
			             <tr>
			              <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.shippingLine"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="shippingLine" size="20" maxlength="20" styleClass="text"/>
		                  </td>
			             </tr>
			             
			              <tr>
			              <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.port"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="port" size="20" maxlength="20" styleClass="text"/>
		                  </td>
			             </tr>
			             
			             
			             
			             <tr>
			            <td class="prompt" width="50" height="25">
			                </td>
			               
			              <td width="127" height="25" class="control">
			              
			              <logic:equal name="arSalesOrderEntryForm" property="showDeliveryButton" value="true">
			                   <html:submit property="deliveryButton" styleClass="mainButton" >
						   			DELIVERY
						   		</html:submit>
						   	</logic:equal>
		                  </td>
			             </tr>	
		              
					</table>
					</div>

					<div class="tabbertab" title="Files">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table>
					</div>

					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="salesOrderEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                  <td width="127" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.orderStatus"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="orderStatus" size="25" maxlength="25" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
			            <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR SALES ORDER ENTRY');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry" cols="40" rows="7" styleClass="text" />
			                </td>
		                </tr>
		                
		                
		                 <tr>
					    	<td width="575" height="1" colspan="4" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
					        	<bean:message key="approverList.gridTitle.approverListDetails"/>
					        </td>
						</tr>
					
					    <tr>
					   	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					       <bean:message key="approverList.prompt.date"/>
					   	</td>
					   	<td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					       <bean:message key="approverList.prompt.name"/>
					   	</td>
					   	<td width="120" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					       <bean:message key="approverList.prompt.status"/>
					   	</td>
					
					   	</tr>
					   	 <%
					       int x = 0;
					       String ROW_ABGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_ABGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowABgc = null;
					    %>
					    <nested:iterate name="arSalesOrderEntryForm" property="approverList">
					    <%
					       x++;
					       if((x % 2) != 0){
					           rowABgc = ROW_ABGC1;
					       }else{
					           rowABgc = ROW_ABGC2;
					       }
					    %>
					    <tr bgcolor="<%= rowABgc %>">
					
					
					
						 		<td class="prompt" width="50" height="25">
							  		<nested:write property="approvedDate"/>
							  </td>
					
						 	  <td class="prompt" width="50" height="25">
							  		<nested:write property="approverName"/>
							  </td>
					
							  <td class="prompt" width="200" height="25" colspan="2">
							  		<nested:write property="status"/>
							  </td>
					
					
						 </tr>
						 </nested:iterate>
							                
					</table>
					</div>
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           		<p align="right">
	           		<logic:equal name="arSalesOrderEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
		       		</logic:equal>
	           		<logic:equal name="arSalesOrderEntryForm" property="showSaveButtonDraft" value="true">
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>
					</logic:equal>
		       		<logic:equal name="arSalesOrderEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
					<html:submit property="printButton" styleClass="mainButton">
	              		<bean:message key="button.print"/>
	           		</html:submit>
			   		<html:submit property="closeButton" styleClass="mainButton">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>

	           	<div id="buttonsDisabled" style="display: none;">
	           		<p align="right">
	           		<logic:equal name="arSalesOrderEntryForm" property="showSaveButton" value="true">
	           		<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveSubmit"/>
		       		</html:submit>
		       		</logic:equal>
	           		<logic:equal name="arSalesOrderEntryForm" property="showSaveButtonDraft" value="true">
					<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.saveAsDraft"/>
		       		</html:submit>
		       		</logic:equal>
		       		<logic:equal name="arSalesOrderEntryForm" property="showDeleteButton" value="true">
			   		<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               		<bean:message key="button.delete"/>
	           		</html:submit>
	           		</logic:equal>
					<html:submit property="printButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.print"/>
	           		</html:submit>
			   		<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              		<bean:message key="button.close"/>
	           		</html:submit>
	           	</div>
			  	</td>
	     	</tr>
			<tr valign="top">
		    	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="salesOrderEntry.gridTitle.SOLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.location"/>
				       	</td>
				       	<td width="40" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="125" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.unitPrice"/>
                       	</td>

						<td width="50" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.tax"/>
                       	</td>

                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
		            	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="280" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemDiscount"/>
				       	</td>
				       	<td width="175" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.amount"/>
                       	</td>

                       	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.discount"/>
                       	</td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arSOLList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">
				    	<td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGridCst(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       	</td>
				       	<td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:80;">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<td width="60" height="1" colspan="2" class="control">
				          <nested:text property="quantity" size="6"  maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name,precisionUnit);" onkeyup="calculateAmount(name,precisionUnit);"/>
				       	</td>
				       	<td width="70" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<logic:equal name="arSalesOrderEntryForm" property="enablePaymentTerm" value="true">

	                       <logic:equal name="arSalesOrderEntryForm" property="disableSalesPrice" value="false">
    	                   	<td width="125" height="1" class="control">
        	                  <nested:text property="unitPrice" size="17" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);"/>
            	           	</td>
                	       	</logic:equal>
                	       	<logic:equal name="arSalesOrderEntryForm" property="disableSalesPrice" value="true">
    	                   	<td width="125" height="1" class="control">
        	                  <nested:text property="unitPrice" size="17" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name,precisionUnit);" onblur="calculateAmount(name,precisionUnit); addZeroes(name);" onkeyup="calculateAmount(name,precisionUnit); formatAmount(name, (event)?event:window.event);" disabled="true"/>
            	           	</td>
                	       	</logic:equal>

               	       	</logic:equal>
                       	<logic:equal name="arSalesOrderEntryForm" property="enablePaymentTerm" value="false">
                       	<td width="125" height="1" class="control">
                          <nested:text property="unitPrice" size="17" maxlength="25" styleClass="textAmount" disabled="true"/>
                       	</td>


                       	</logic:equal>


						<td width="40" height="1"  class="control">
				          <p align="center">
                          <nested:select property="tax" styleClass="comboRequired" style="width:40px;">
				              <nested:options property="taxList"/>
				          </nested:select>
                       </td>





                       	<td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="40" height="1" class="control"/>
				       	<td width="280" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;"/>

				       	</td>

				       	<td width="70" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="125" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>




						<td width="40" height="1"  class="control">

				          	<div id="buttons">

							<nested:equal property="isTraceMisc" value="true">
					   		<nested:hidden property="misc"/>
					   		<!--
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
						   	 -->
						   	 </nested:equal>

					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">

							<nested:equal property="isTraceMisc" value="true">
							<!--
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		-->
					   		</nested:equal>

					   	</div>
                       </td>



					   <td width="50" align="center" height="1">
					   	<div id="buttons">
				   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
					   		<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   </td>
				    </tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
		    </tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="arSalesOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.addLines"/>
			      	</html:submit>
			      </logic:equal>
			      <logic:equal name="arSalesOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			      		<bean:message key="button.deleteLines"/>
			      	</html:submit>
			  	  </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="arSalesOrderEntryForm" property="showAddLinesButton" value="true">
		          	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			       	</html:submit>
			      </logic:equal>
			      <logic:equal name="arSalesOrderEntryForm" property="showDeleteLinesButton" value="true">
			      	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       	</html:submit>
			      </logic:equal>
		          </div>
				</td>
			</tr>
	     	</logic:equal>

	     	<logic:equal name="arSalesOrderEntryForm" property="enableFields" value="false">
         	<tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.customer"/>
                			</td>
							<logic:equal name="arSalesOrderEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<logic:equal name="arSalesOrderEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" styleClass="textRequired" size="15" maxlength="25" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				
         				<tr>
         				<!-- 
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="documentType.label"/>
                			</td>
         					<td width="158" height="25" class="control">
                   				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="documentTypeList"/>
                   				</html:select>
                   			</td>
                   			-->
                   			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				
         				</tr>
         				 
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
							
						</tr>
						
						
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.transactionStatus"/>
                			</td>

                			<td width="158" height="25" class="control">
                   				<html:select property="transactionStatus" styleClass="combo" style="width:130;" disabled="true">
                       			<html:options property="transactionStatusList"/>
                   				</html:select>
                   			</td>
						</tr>


					
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>

         				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="salesOrderEntry.prompt.printType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="reportType" styleClass="combo">
						      <html:options property="reportTypeList"/>
						   </html:select>
				          </td>

         				</tr>
					</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderMobile"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="salesOrderMobile"/>
                			</td>
			            </tr>
						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;" disabled="true">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
							<logic:equal name="arSalesOrderEntryForm" property="enableSalesOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                				<html:checkbox property="salesOrderVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="arSalesOrderEntryForm" property="enableSalesOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                				<bean:message key="salesOrderEntry.prompt.salesOrderVoid"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:checkbox property="salesOrderVoid" disabled="true"/>
                			</td>
                			</logic:equal>
						</tr>
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.salesperson"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="salesperson" styleClass="combo" style="width:130;" disabled="true">
                       				<html:options labelProperty="salespersonNameList" property="salespersonList"/>
                   				</html:select>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.totalAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
						</tr>



						<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="salesOrderEntry.prompt.soAdvanceAmount"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="soAdvanceAmount" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>

						</tr>

						 <nested:iterate property="reportParameters">
						 <tr>
						 	<td width="160" height="25" class="prompt">
						 		<nested:write property="parameterName" />
						 	</td>

						 	<td width="160" height="25" class="prompt" colspan="3">
						 		<nested:text property="parameterValue" size="50" maxlength="50" styleClass="text" />
						 	</td>
						 </tr>
						 </nested:iterate>
			        </table>
					</div>
					<div class="tabbertab" title="Address">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			    			<td width="160" height="25" class="prompt">
			       				<bean:message key="salesOrderEntry.prompt.billTo"/>
			    			</td>
			    			<td width="128" height="25" class="control">
			       				<html:textarea property="billTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		        			<td width="160" height="25" class="prompt">
	               				<bean:message key="salesOrderEntry.prompt.shipTo"/>
	            			</td>
			    			<td width="127" height="25" class="control">
			       				<html:textarea property="shipTo" cols="20" rows="4" styleClass="text" disabled="true"/>
			    			</td>
		 				</tr>
					</table>
					</div>
			        <div class="tabbertab" title="Tax">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.taxCode"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="taxCode" styleClass="comboRequired" disabled="true">
			                       <html:options property="taxCodeList"/>
			                   </html:select>
			                </td>
				         </tr>
					</table>
					</div>
				    <div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="currency" styleClass="comboRequired" disabled="true">
			                       <html:options property="currencyList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					
					<div class="tabbertab" title="Delivery">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			              <tr>
			              <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.shippingLine" />
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="shippingLine" size="20" maxlength="20" styleClass="text" disabled="true"/>
		                  </td>
			             </tr>
			             
			              <tr>
			              <td class="prompt" width="100" height="25">
			                   <bean:message key="salesOrderEntry.prompt.port"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="port" size="20" maxlength="20" styleClass="text" disabled="true"/>
		                  </td>
			             </tr>
			             
			              <tr>
			            	<td class="prompt" width="50" height="25">
			                </td>
			               
			              <td width="127" height="25" class="control">
			              	<logic:equal name="arSalesOrderEntryForm" property="showDeliveryButton" value="true">
			                   <html:submit property="deliveryButton" styleClass="mainButton" >
						   			DELIVERY
						   		</html:submit>
						   		</logic:equal>
		                  </td>
			             </tr>	
		                
					</table>
					
					</div>
					<div class="tabbertab" title="Files">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename1"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton1" value="true">
					           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton1" value="true">
			                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename2"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton2" value="true">
					           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton2" value="true">
			                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename3"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton3" value="true">
					           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton3" value="true">
			                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.filename4"/>
			                </td>
			                <td width="288" height="25" class="control" colspan="2">
			                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <div id="buttons">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton4" value="true">
					           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                   <div id="buttonsDisabled" style="display: none;">
			                   <p align="center">
			                   <logic:equal name="arSalesOrderEntryForm" property="showViewAttachmentButton4" value="true">
			                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
					              <bean:message key="button.viewAttachment"/>
					           </html:submit>
					           </logic:equal>
			                   </div>
			                </td>
				         </tr>
					</table>
					</div>
					<div class="tabbertab" title="Remarks">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
			       		<tr>
				     		<td class="prompt" width="575" height="25" colspan="4">
		             		</td>
		           		</tr>
                   		<tr>
			        		<td class="prompt" width="140" height="25">
		               			<bean:message key="salesOrderEntry.prompt.memo"/>
		            		</td>
				    		<td width="147" height="25" class="control" colspan="3">
		               			<html:textarea property="memo" cols="60" rows="7" styleClass="text" disabled="true"/>
				    		</td>
			       		</tr>
			        </table>
			        </div>
					<div class="tabbertab" title="Status">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					    	<td class="prompt" width="575" height="25" colspan="4">
			                </td>
			          	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.orderStatus"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="orderStatus" size="50" maxlength="50" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
					</table>
					</div>
					<div class="tabbertab" title="Log">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
						<tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			            </tr>
				        <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				       	</tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="salesOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				        </tr>
				        <tr>
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
					        	 <html:submit styleClass="mainButtonMedium" onclick="return showAdLog('AR SALES ORDER ENTRY');">
					            	<bean:message key="log.prompt.viewLog"/>
					       		 </html:submit>
				        	</td>
				        	
				        	<td width="127" height="25" class="control" align="center">
				        	</td>
				        </tr>
				       
				        
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="log.prompt.logEntry"/>
			                </td>
			                
			                <td width="127" height="25" class="control">
			                   <html:textarea property="logEntry"  cols="20" rows="9" styleClass="text" />
			                </td>
		                </tr>
					</table>
					
					</div>
					
					</div><script>tabberAutomatic(tabberOptions)</script>
				</td>
	     	</tr>
	     	<tr>
	         	<td width="575" height="50" colspan="4">
	           	<div id="buttons">
	           	<p align="right">
	           	<logic:equal name="arSalesOrderEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium">
		        	<bean:message key="button.saveSubmit"/>
		       	</html:submit>
		       	</logic:equal>
	           	<logic:equal name="arSalesOrderEntryForm" property="showSaveButtonDraft" value="true">
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>
		       	</logic:equal>
		       	<logic:equal name="arSalesOrderEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	            	<bean:message key="button.delete"/>
	           	</html:submit>
	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton">
	            	<bean:message key="button.print"/>
	           	</html:submit>
			   	<html:submit property="closeButton" styleClass="mainButton">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
	           	<logic:equal name="arSalesOrderEntryForm" property="showSaveButton" value="true">
	           	<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       	</html:submit>
		       	</logic:equal>
	           	<logic:equal name="arSalesOrderEntryForm" property="showSaveButtonDraft" value="true">
				<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		           	<bean:message key="button.saveAsDraft"/>
		       	</html:submit>
		       	</logic:equal>
		       	<logic:equal name="arSalesOrderEntryForm" property="showDeleteButton" value="true">
			   	<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.delete"/>
	           	</html:submit>
	           	</logic:equal>
				<html:submit property="printButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.print"/>
	           	</html:submit>






			   	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
	            	<bean:message key="button.close"/>
	           	</html:submit>
	           	</div>
			  	</td>
	     	</tr>
	     	<tr valign="top">
		       	<td width="575" height="185" colspan="4">
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="salesOrderEntry.gridTitle.SOLDetails"/>
		                </td>
		            </tr>
		            <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.lineNumber"/>
				       	</td>
		               	<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemName"/>
				       	</td>
		               	<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.location"/>
				       	</td>
				       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.quantity"/>
                       	</td>
                       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.unit"/>
                       	</td>
                       	<td width="204" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.unitPrice"/>
                       	</td>

						<td width="70" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.tax"/>
                       	</td>

                       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.delete"/>
                       	</td>
				  	</tr>
				    <tr>
		               	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="510" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemDescription"/>
				       	</td>
				       	<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="salesOrderEntry.prompt.itemDiscount"/>
				       	</td>
				       	<td width="274" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.amount"/>
                       	</td>

                       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="salesOrderEntry.prompt.discount"/>
                       	</td>
				    </tr>
				    <%
				       int i = 0;
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arSOLList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }
				    %>
				    <tr bgcolor="<%= rowBgc %>">
				    	<td width="40" height="1" class="control">
				          <nested:text property="lineNumber" size="3" maxlength="4" styleClass="text" disabled="true"/>
				       	</td>
				       	<td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired"  disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
				       	<td width="75" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:100;" disabled="true">
				              <nested:options property="locationList"/>
				          </nested:select>
				       	</td>
				       	<td width="60" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired"  disabled="true"/>
				       	</td>
				       	<td width="70" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>
                          </nested:select>
                       	</td>
                       	<td width="125" height="1" class="control">
                          <nested:text property="unitPrice" size="17" maxlength="25" styleClass="textAmountRequired"  disabled="true"/>
                       	</td>

						<td width="40" height="1"class="control">
				          <p align="center">
                          <nested:select property="tax" styleClass="comboRequired" style="width:40px;" disabled="true">
				              <nested:options property="taxList"/>
				          </nested:select>
                       </td>


                       	<td width="50" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       	</td>
				   	</tr>
				   	<tr bgcolor="<%= rowBgc %>">
				       	<td width="40" height="1" class="control"/>
				       	<td width="280" height="1" class="control" colspan="3">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>

				       	</td>
				       	<td width="70" height="1" class="control">
				          <nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
				       	</td>
				       	<td width="125" height="1" class="control">
				          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmountRequired" disabled="true"/>
				       	</td>



			       		<td width="40" height="1"  class="control">
				          	<div id="buttons">

					   		<nested:hidden property="misc"/>
					   		<nested:equal property="isTraceMisc" value="true">
					   		<!--
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
						   	 -->
						   	 </nested:equal>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
							<nested:equal property="isTraceMisc" value="true">
							<!--
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   		-->
					   		</nested:equal>
					   	</div>
                       </td>




					   <td width="50" align="center" height="1">
					   	<div id="buttons">
				   		<nested:hidden property="discount1"/>
					   		<nested:hidden property="discount2"/>
					   		<nested:hidden property="discount3"/>
					   		<nested:hidden property="discount4"/>
							<nested:hidden property="fixedDiscountAmount"/>
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" onclick="return fnOpenDiscount(name);">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   	<div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="discountButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.discounts"/>
					   		</nested:submit>
					   	</div>
					   </td>
				  	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
		   	<tr>
		    	<td width="575" height="25" colspan="4">
		          <div id="buttons">
		          <p align="right">
		          <logic:equal name="arSalesOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="arSalesOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
		          <div id="buttonsDisabled" style="display: none;">
		          <p align="right">
		          <logic:equal name="arSalesOrderEntryForm" property="showAddLinesButton" value="true">
		            <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			        	<bean:message key="button.addLines"/>
			        </html:submit>
			      </logic:equal>
			      <logic:equal name="arSalesOrderEntryForm" property="showDeleteLinesButton" value="true">
			        <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			        </html:submit>
			      </logic:equal>
		          </div>
				</td>
	     	</tr>
	     	</logic:equal>

	     	<tr>
	        	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     	</td>
	       	</tr>

        	</table>
    	</td>
  	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)
        document.forms[0].elements["date"].focus();
	       // -->
</script>

<logic:equal name="arSalesOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arSalesOrderEntryForm" type="com.struts.ar.salesorderentry.ArSalesOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/arRepSalesOrderPrint.do?forward=1&salesOrderCode=<%=actionForm.getSalesOrderCode()%>&reportType=<%=actionForm.getReportType()%>","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<logic:equal name="arSalesOrderEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arSalesOrderEntryForm" type="com.struts.ar.salesorderentry.ArSalesOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="arSalesOrderEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="arSalesOrderEntryForm" type="com.struts.ar.salesorderentry.ArSalesOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>



</body>
</html>

