<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

</script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

    function submitForm()
    {            
        disableButtons();
        enableInputControls();
    }
  
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >
<html:form action="/arStandardMemoLineClass.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">

    <%@ include file="cmnHeader.jsp" %> 
    <%@ include file="cmnSidebar.jsp" %>
    <table border="0" cellpadding="0" cellspacing="0" width="800" height="510">
        <tr valign="top">
            <td width="187" height="510"></td> 
            <td width="581" height="510">
                <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
                                           bgcolor="<%=Constants.TXN_MAIN_BGC%>">
                    <tr>
                        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
                           <bean:message key="standardMemoLineClass.title"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="575" height="44" colspan="4" class="statusBar">
                            <logic:equal name="arStandardMemoLineClassForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                                <bean:message key="app.success"/>
                            </logic:equal>
                            <html:errors/>	
                           
                        </td>
                    </tr>

                    <html:hidden property="isCustomerClassEntered" value=""/>

                    <tr>
                        <td width="575" height="10" colspan="4">

                            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
                                						
                                <tr>
                                    <td width="130" height="25" class="prompt">
                                        <bean:message key="standardMemoLineClass.prompt.customerClass"/>
                                    </td>
                                    <td width="158" height="25" class="control">
                                        <html:select property="customerClassName" styleClass="comboRequired" onchange="return enterSelectWithBlank('customerClassName','isCustomerClassEntered');"style="width:130;">
                                            <html:options property="customerClassList"/>
                                        </html:select>
                                    </td>     
                                </tr>
                                
                            </table>
                            

                        </td>
                    </tr>
                    <tr>
                          
                        <td width="575" height="50" colspan="4"> 
                            
                            <div id="buttons">
                                <p align="right">
                                <logic:equal name="arStandardMemoLineClassForm" property="showSaveButton" value="true">

                                <html:submit property="saveButton" styleClass="mainButtonMedium">
                                    <bean:message key="button.save"/>
                                </html:submit> 
                                </logic:equal>
                                    
                                <html:submit property="closeButton" styleClass="mainButton">
                                    <bean:message key="button.close"/>
                                </html:submit>
                            </div>

                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="575" height="185" colspan="4">
                            <div align="center">
                                <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                                    <tr>
                                        <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                                            <bean:message key="standardMemoLineClass.gridTitle.SMCDetails"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.lineNumber"/>
                                        </td>
                                        <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.memoLine"/>
                                        </td>

                                        <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.customerCode"/>
                                        </td>
                                        
                                        
                                        <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.unitPrice"/>
                                        </td>

                                        
                                        <td width="140" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.delete"/>
                                        </td>			       			    
                                    </tr>							    
                                    <tr>				       
                                        <td width="294" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.description"/>
                                        </td>	
                                        
                                        <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.customerName"/>
                                        </td>
                                        
                                        <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                            <bean:message key="standardMemoLineClass.prompt.branch"/>
                                        </td>
                                                
                                       
                                    </tr>
                                        <%
                                           int i = 0;	
                                           String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                                           String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                                           String rowBgc = null;
                                        %>
                                        <nested:iterate property="arSmcList">
                                        <%
                                           i++;
                                           if((i % 2) != 0){
                                               rowBgc = ROW_BGC1;
                                           }else{
                                               rowBgc = ROW_BGC2;
                                           }  
                                        %>
                                    <nested:hidden property="isMemoLineEntered" value=""/>
                                    <nested:hidden property="isCustomerEntered" value=""/>
                                    <tr bgcolor="<%= rowBgc %>">	

                                        <td width="30" height="1" class="control" rowspan="2">
                                            <nested:text property="lineNumber" size="5" maxlength="25" styleClass="textAmount" disabled="true"/>

                                        </td>


                                        <td width="300" height="1" class="control">
                                            <nested:equal property="isListed" value="true">
                                                <nested:text property="memoLine" size="50" maxlength="25" styleClass="textRequired"  style="width:150px;" disabled="true"/>
                                            </nested:equal>
                                            <nested:equal property="isListed" value="false">
                                                <nested:select property="memoLine" styleClass="comboRequired" style="width:150px;" onchange="return enterSelectGridBlank(name, 'memoLine','isMemoLineEntered');">
                                                    <nested:options property="memoLineList"/>				          				          
                                                </nested:select>
                                                 <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArMlLookupGridwithType(name,'memoLineClass');"/>
                                    
                                            </nested:equal>
                                            
                                           
                                        </td>
                                        
                                        <td width="294" height="1" class="control" colspan="1">
                                            <nested:select property="customerCode" styleClass="comboRequired" style="width:130px;" onchange="return enterSelectGridBlank(name, 'customerCode','isCustomerEntered');">
                                                <nested:options property="customerCodeList"/>				          				          
                                            </nested:select>
                                            <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookupGridwithType(name, 'memoLineClass');"/>
                                        </td>

                                        <td width="50" height="1" class="control">
                                            <nested:text property="unitPrice" size="15" maxlength="25" styleClass="textAmountRequired"  />
                                        </td>

                                        
                                        
                                      	

                                        <td width="140" align="center" height="1" class="control" rowspan="2">
                                            <center>
                                                <nested:equal property="isListed" value="true">
                                                    <nested:submit property="deleteButton" styleClass="gridButton"  onclick="return confirmDelete();" >
                                                        <bean:message key="button.delete"/>
                                                    </nested:submit>
                                                </nested:equal>
                                                
                                                <nested:equal property="isListed" value="false">
                                                    <nested:submit property="deleteButton" styleClass="gridButton">
                                                        <bean:message key="button.delete"/>
                                                    </nested:submit>
                                                </nested:equal>
                                        
                                            </center>
                                        </td>			       
                                    </tr>							    
                                    <tr bgcolor="<%= rowBgc %>">
                                        
                                        <td width="294" height="1" class="control" colspan="1">
                                          <nested:text property="description" size="31" maxlength="255" style="font-size:8pt;width:150px;" styleClass="text"/>
                                        </td>
                                        
                                        <td width="50" height="1" class="control">
                                            <nested:text property="customerName" size="25" maxlength="25" styleClass="text" disabled="true" />
                                        </td>
                                       
                                        <td width="222" height="1" class="control">
                                            <nested:select property="branchName" styleClass="comboRequired" style="width:110;">
                                                <nested:options property="branchList"/>
                                            </nested:select>
                                        </td>
                                       
                                      
                                    </tr>
                                    </nested:iterate>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="575" height="25" colspan="4"> 
                            <div id="buttons">
                                <p align="right">
                                    
                                <logic:equal name="arStandardMemoLineClassForm" property="showAddLines" value="true">

                                    <html:submit property="addLinesButton" styleClass="mainButtonMedium">
                                        <bean:message key="button.addLines"/>
                                    </html:submit>			   
                                </logic:equal>

                            </div>

                        </td>
                    </tr>	

                    <tr>
                        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</html:form>

    

</body>
</html>
