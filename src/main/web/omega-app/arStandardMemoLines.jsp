<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="standardMemoLine.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

  function submitForm()
  {            
      disableButtons();
      enableInputControls();
  }

//Done Hiding--> 
function confirmPost()
{
   if(confirm("Are you sure you want to Generate Interest?")) return true;
      else return false;
}

	function deleteAllConfirmation(){
		
		var cstData = document.forms[0].elements["customer"].value;
		
		if(cstData==""){
			if(confirm("You selected All Customers. Do you want to add invoices to all?")){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
		
		
	}
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/arStandardMemoLines.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="840" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="650" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="standardMemoLine.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="arStandardMemoLineForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
            </logic:equal>
        	<html:errors/>
            
            
           <html:messages id="messages" message="true">
		       <br><font color="green"><bean:write name="messages"/></font><br> 
		   </html:messages>	
		   <br>
		   	
		   
	        </td>
	     </tr>
	     <tr>
        	<td width="575" height="10" colspan="4">
    	       	<div class="tabber">
		        <div class="tabbertab" title="Header">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			       	<tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
			        <tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.type"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:select property="type" styleClass="comboRequired">
							<html:options property="typeList"/>
							</html:select>
						</td>
					</tr> 	     	     
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.name"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="name" size="50" maxlength="100" styleClass="textRequired"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="standardMemoLine.prompt.description"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="description" size="50" maxlength="100" styleClass="text"/>
						</td>
					</tr>                          
				     <tr>
						 <logic:equal name="arStandardMemoLineForm" property="enableAccountNumber" value="true">	     
				         <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.accountNumber"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="accountNumber" size="30" maxlength="255" styleClass="textRequired"/>
						    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountNumber','accountNumberDescription');"/>	            
				         </td>
	        				 </logic:equal>
							 <logic:equal name="arStandardMemoLineForm" property="enableAccountNumber" value="false">	     
					     <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.accountNumber"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="accountNumber" size="30" maxlength="255" styleClass="text" disabled="true"/>
						    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountNumber','accountNumberDescription');" disabled="true"/>	            
				         </td>
	        			 </logic:equal>	         	         	         	         
			         </tr>  
			         <tr>
				         <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.accountNumberDescription"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="accountNumberDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	        			 </td>
			         </tr> 
			         
			         
			         
			         <tr>
					
	        				      
					     <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.receivableAccountNumber"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="receivableAccountNumber" size="30" maxlength="255" styleClass="text"/>
						    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('receivableAccountNumber','receivableAccountNumberDescription');"/>	            
				         </td>
	        			 	         	         	         
			         </tr>  
			         <tr>
				         <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.receivableAccountNumberDescription"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="receivableAccountNumberDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	        			 </td>
			         </tr> 
			         
			         
			         
			         <tr>
      
					     <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.revenueAccountNumber"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="revenueAccountNumber" size="30" maxlength="255" styleClass="text"/>
						    <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('revenueAccountNumber','revenueAccountNumberDescription');"/>	            
				         </td>
	        			 	         	         	         
			         </tr>  
			         <tr>
				         <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.revenueAccountNumberDescription"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="revenueAccountNumberDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	        			 </td>
			         </tr> 
			         
			         
			         
			         
			         
			           
				    <tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.unitPrice"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="unitPrice" size="20" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
						</td>
					</tr>
					<tr>
				         <td class="prompt" width="140" height="25">
	        			    <bean:message key="standardMemoLine.prompt.interimAccount"/>
				         </td>
	        			 <td width="435" height="25" class="control" colspan="3">
				            <html:text property="interimAccount" size="30" maxlength="255" styleClass="text"/>
						    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('interimAccount','interimAccountDescription');"/>	            
				         </td>
				     </tr>
				     <tr>
	        			 <td class="prompt" width="140" height="25">
			            	<bean:message key="standardMemoLine.prompt.interimAccountDescription"/>
		    		     </td>
				         <td width="435" height="25" class="control" colspan="3">
	    			        <html:text property="interimAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
				         </td>
				    </tr>					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.unitOfMeasure"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="unitOfMeasure" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>	     	                  
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.taxable"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:checkbox property="taxable"/>
						</td>                  
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.enable"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:checkbox property="enable"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:checkbox property="subjectToCommission"/>
						</td>
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="standardMemoLine.prompt.wordPressProductId"/>
						</td>
						<td width="435" height="25" class="control" colspan="3">
							<html:text property="wpProductID" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>
					
				</table>
				</div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="560" height="25">
					<tr> 
						<td class="prompt" width="575" height="25" colspan="4">
						</td>
					</tr>
					<nested:iterate property="arBSMLList">
					<tr>
			            <td class="prompt" valign="top" width="25">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
						<td colspan="3">
							<table border="0" cellpadding="0" cellspacing="0" >
							<tr>
					        	<td class="prompt" width="95" height="25">
					        		<nested:write property="brName"/>
			    		        </td>
							</tr>
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.accountNumber"/>
								</td>
								<td>
									<nested:text property="branchAccountNumber" styleClass="text"/>
							        <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchAccountNumber', 'branchAccountNumberDescription');"/>
								</td>
							</tr>
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.accountNumberDescription"/>
								</td>
								<td>
									<nested:text property="branchAccountNumberDescription" style="font-size:8pt;" size="40" styleClass="text" disabled="true"/>
								</td>
							</tr>
							
							
							
							
							
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.receivableAccountNumber"/>
								</td>
								<td>
									<nested:text property="branchReceivableAccountNumber" styleClass="text"/>
							        <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchReceivableAccountNumber', 'branchReceivableAccountNumberDescription');"/>
								</td>
							</tr>
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.receivableAccountNumberDescription"/>
								</td>
								<td>
									<nested:text property="branchReceivableAccountNumberDescription" style="font-size:8pt;" size="40" styleClass="text" disabled="true"/>
								</td>
							</tr>
							
							
							
							
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.revenueAccountNumber"/>
								</td>
								<td>
									<nested:text property="branchRevenueAccountNumber" styleClass="text"/>
							        <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'branchRevenueAccountNumber', 'branchRevenueAccountNumberDescription');"/>
								</td>
							</tr>
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.revenueAccountNumberDescription"/>
								</td>
								<td>
									<nested:text property="branchRevenueAccountNumberDescription" style="font-size:8pt;" size="40" styleClass="text" disabled="true"/>
								</td>
							</tr>
							
							
							
							<tr>
								<td width="140" height="25" class="prompt">
			        			    <bean:message key="standardMemoLine.prompt.subjectToCommission"/>
								</td>
								<td>
			            			<nested:checkbox property="subjectToCommission"/>
								</td>
							</tr>
							</table>
						</td>
		            </tr>
		            </nested:iterate>
				</table>
		        </div> 
		        
		      <br>
			    <div class="tabbertab" title="Generate Invoice">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        		<tr>
					         <td class="prompt" width="140" height="25">
		        			    Customers
					         </td>
					         <td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" >
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                			  </td>
                			  
                			  
					         
					          <!--  
		        			  <td width="435" height="25" class="control" >
					            <html:checkbox property="isAllCustomer"  />
							  </td>
							  -->
							  <logic:equal name="arStandardMemoLineForm" property="showBatchName" value="true">
	                			<td width="130" height="25" class="prompt">
	                   				<bean:message key="invoiceEntry.prompt.batchName"/>
	                			</td>
	                			<td width="157" height="25" class="control">
	                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
	                       			<html:options property="batchNameList"/>
	                   				</html:select>
	                			</td>
	         				 </logic:equal>
							  
					     </tr>
					     
					     <tr>
						<td class="prompt" width="140" height="25">
				            <bean:message key="statement.prompt.customerBatch"/>
				         </td>
				          <td width="127" height="25" class="control">
				          
				          <html:select property="customerBatchSelectedList" size="7" multiple="true" styleClass="combo">
					
				              <html:options property="customerBatchList"/>
						   </html:select>
				         </td>
				         
				       
					
					</tr>
					     <tr>
						     <td class="prompt" width="140" height="25">
					           	
					         </td>
					     </tr>
					     <tr>
					         <td width="435" class="prompt" width="80" height="25">
		        			    Effective Date
					         </td>
		        			 <td width="435" height="25" class="control" >
					            <html:text property="invcEffDate" size="10" styleClass="textRequired"/>
							  </td>
							  
							  <td width="435" class="prompt" width="80" height="25">
		        			    Receive Date
					         </td>
		        			 <td width="435" height="25" class="control" >
					            <html:text property="invcRcvDate" size="10" />
							  </td>
					     </tr>
					     
					     <tr>
					     	  <td width="435" class="prompt" width="80" height="25">
		        			    Description
					         </td>
		        			 <td width="435" height="25" class="control"  width = "100">
					            <html:text property="genInvoiceDesc" size="20" styleClass="textRequired"/>
							  </td>
					     
					     </tr>
					     
					     
					     <tr>
					     	<logic:equal name="arStandardMemoLineForm" property="enablePaymentTerm" value="true">
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.paymentTerm"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="paymentTerm" styleClass="comboRequired" style="width:130;">
	                   			<html:options property="paymentTermList"/>
	               				</html:select>
                			</td>
                			</logic:equal>
					     
					     </tr>
					     
					     <tr>
					     	 <td class="prompt" width="350" height="25" colspan=3>
					     		
					     		<div id="buttons">
						             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
						             <html:submit property="generateButton" styleClass="mainButtonMedium" onclick="return deleteAllConfirmation()" >
								        Generate Invoice
								     </html:submit>
								     </logic:equal>			     
						             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
						             <html:submit property="generateButton" styleClass="mainButtonMedium" onclick="return deleteAllConfirmation()">
								          Generate Invoice
								     </html:submit>
								     </logic:equal>			     			     			     
							     </div>
					     		
					     	
					     	</td>
					     </tr>
		        	</table>

		        </div>
		        
		        <div class="tabbertab" title="Generate Penalty">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        		
		        		
					     
					     <tr>
					     	 <td class="prompt" width="350" height="25" colspan=3>
					     		
					     		
					     		
					     		
					     		
					     		<div id="buttons">
						             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
						             <html:submit property="generatePenalty" styleClass="mainButtonMedium" onclick="return confirmPost();" >
								        Generate Penalty
								     </html:submit>
								     </logic:equal>			     
						             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
						             <html:submit property="generatePenalty" styleClass="mainButtonMedium" >
								          Generate Penalty
								     </html:submit>
								     </logic:equal>			     			     			     
							     </div>
					     		
					     	
					     	</td>
					     </tr>
		        	</table>

		        </div>
		        
		        <div class="tabbertab" title="Generate Bonus/Interest">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        		
                                    <tr>
                                         <td class="prompt" width="140" height="25">
                                            Supplier
                                        </td>
                                        
                                        
                                        <td width="158" height="25" class="control">
                                            <html:select property="supplier" styleClass="combo" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                                                    <html:options property="supplierList"/>
                                            </html:select>
                                            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td class="prompt" width="140" height="25">
                                             Recalculate Month
                                        </td>
                                       <td width="435" height="25" class="control" >
                                            <html:checkbox property="isRecalculate"/>
                                        </td>
                                        
                                    </tr>
                                    
                                   
                                    <tr>
                                        <td class="prompt" width="140" height="25">
                                            Period
                                        </td>
                                        <td width="50" height="25" class="control">
                                        <html:select property="period" styleClass="comboRequired">
                                            <html:options property="periodList"/>
                                        </html:select>
                                    </td>
                                    </tr>

                                    <tr>
                                        <td class="prompt" width="350" height="25" colspan=3>
                                            <html:submit property="generateArInvestorBonusAndInterest" styleClass="mainButtonMedium"  >
                                                GENERATE
                                            </html:submit>	

                                        </td>
                                    </tr>
		        	</table>

		        </div>
		        
		        
		        
		        </div><script>tabberAutomatic(tabberOptions)</script>    
			</td>
		</tr>
		<tr>
			<td width="575" height="50" colspan="4">

			<table border="0" cellpadding="0" cellspacing="0" width="575" height="50" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="160" height="50">
			             <div id="buttons">
			             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="415" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				       	 <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	 <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton">
				               <bean:message key="button.save"/>
				          	</html:submit>
				         </logic:equal>
				         <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.save"/>
				            </html:submit>
				         </logic:equal>
				         <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="standardMemoLine.gridTitle.SMLDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			    	<td width="40" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          
			       </td>
			    
			       <td width="94" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.type"/>
			       </td>
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.name"/>
			       </td>			       
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.accountNumber"/>
			       </td>
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.receivableAccountNumber"/>
			       </td>
			       <td width="185" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.revenueAccountNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.unitPrice"/>
			       </td>
				   <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.unitOfMeasure"/>
			       </td>			       
			       <td width="200" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="standardMemoLine.prompt.enable"/>
			       </td>	
			       
			       
			  	       			       			       			       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arSMLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			    
			   	 	<td width="60" height="1" class="gridLabel">
			          <nested:checkbox property="genInvoiceEnable"/>
			       </td>
			    	
			       <td width="94" height="1" class="gridLabel">
			          <nested:write property="type"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="name"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="accountNumber"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="receivableAccountNumber"/>
			       </td>
			       <td width="185" height="1" class="gridLabel">
			          <nested:write property="revenueAccountNumber"/>
			       </td>
			       <td width="110" height="1" class="gridLabelNum">
			          <nested:write property="unitPrice"/>
			       </td>			       
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="unitOfMeasure"/>
			       </td>
			       <td width="60" height="1" class="gridLabel">
			          <nested:write property="enable"/>
			       </td>
			       
			      
			       
			       <td width="140" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
   			       </logic:equal>  	                      
				   </logic:equal>
				   
				   
				   </div>
			       <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		              <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
				   
				   
			       </td>
			       
			
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="arStandardMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="arSMLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			    	<td width="175" height="1" class="gridHeader">
			       
			       </td>
			       <td width="550" height="1" class="gridLabel" colspan="2">
			 
			       </td>
			    
			    
			    
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="standardMemoLine.prompt.type"/>
			       </td>
			       <td width="550" height="1" class="gridLabel" colspan="2">
			          <nested:write property="type"/>
			       </td>
			       <td width="169" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
				   <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="arStandardMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="arStandardMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.name"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="name"/>
			      </td>			      		      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.description"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="description"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.unitPrice"/>
			      </td>
			      <td width="350" height="1" class="gridLabelNum">
			         <nested:write property="unitPrice"/>
			      </td>
			      <td width="200" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.taxable"/>
			      </td>
			      <td width="169" height="1" class="gridLabel">
			         <nested:write property="taxable"/>
			      </td>			      
			    </tr>			    			    			    
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.unitOfMeasure"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="unitOfMeasure"/>
			      </td>
			      <td width="200" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.enable"/>
			      </td>
			      <td width="169" height="1" class="gridLabel">
			         <nested:write property="enable"/>
			      </td>				      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.accountNumber"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="accountNumber"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.accountNumberDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel">
			         <nested:write property="accountNumberDescription"/>
			      </td>
			      <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.interimAccount"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="interimAccount"/>
			      </td>
			      <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.interimAccountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="interimAccountDescription"/>
			      </td>
			    </tr>
			    </tr>
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="standardMemoLine.prompt.subjectToCommission"/>
			      </td>
			      <td width="719" height="1" class="gridLabel">
			         <nested:write property="subjectToCommission"/>
			      </td>
			    </tr>			    			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["type"] != null &&
        document.forms[0].elements["type"].disabled == false)
        document.forms[0].elements["type"].focus()
   // -->
</script>
</body>
</html>
