<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invoiceEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return invoiceEnterSubmit();">
<html:form action="/arUploadInvoiceItemEntry.do" onsubmit="return submitForm();" method="POST" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          	<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	    <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="uploadInvoiceItemEntry.title"/>
			</td>
	    </tr>
        <tr>
	      	<td width="575" height="44" colspan="4" class="statusBar">
			   	<logic:equal name="arUploadInvoiceItemForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
	               <bean:message key="app.success"/>
	           	</logic:equal>
			   	<html:errors/>	
			   	<html:messages id="msg" message="true">
			       <bean:write name="msg"/>		   
			   	</html:messages>
	        </td>
	    </tr>
	    
	    <html:hidden property="isGenerateCsv" value=""/>
	    <html:hidden property="isUpload" value=""/>
	 	
			
			    <ss>
                	<td class="prompt" width="160" height="25">
						Browse CSV file
					</td>
					<td>
		         		<html:file property="csvFile" size="35" styleClass="textRequired"/>
					  	<html:submit property="csvButton" style = "margin-left:10px;" styleClass="mainButtonMedium" value="Browse CSV File">
					     	<bean:message key="button.browseCsv"/>
					    </html:submit>
					</td>
                </tr>
                <tr>
                
                	 <td width="160" height="25" class="prompt">
			           	<bean:message key="uploadInvoiceItemEntry.prompt.lines"/>
			         </td>
			         <td width="128" height="25" class="control">
			            <html:text property="noOfLines" size="10" maxlength="10" styleClass="text" disabled="true"/>
			         </td>
                
                
                	 <td width="160" height="25" class="prompt">
			           	<bean:message key="uploadInvoiceItemEntry.prompt.noOfTransactions"/>
			         </td>
			         <td width="128" height="25" class="control">
			            <html:text property="noOfTransactions" size="10" maxlength="10" styleClass="text" disabled="true"/>
			         </td>
                </tr>
                <tr>
				     <td width="160" height="25" class="prompt">
			           	<bean:message key="uploadInvoiceItemEntry.prompt.noOfValid"/>
			         </td>
			         <td width="128" height="25" class="control">
			            <html:text property="noOfValid" size="10" maxlength="10" styleClass="text" disabled="true"/>
			         </td>
			         <td width="160" height="25" class="prompt">
			            <bean:message key="uploadInvoiceItemEntry.prompt.noOfFailed"/>
			         </td>
			         <td width="127" height="25" class="control">
			            <html:text property="noOfFailed" size="10" maxlength="10" styleClass="text" disabled="true"/>
			         </td>			                
				</tr>
	
         	
         	
        
	     <tr>
	 
	         <td width="575" height="50" colspan="4"> 
	          	<div id="buttons">
	           
		           <p align="right">
		           <logic:equal name="arUploadInvoiceItemForm" property="showUploadButton" value="true">
		           <html:submit property="uploadButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
			            <bean:message key="button.upload"/>
			       </html:submit>
	
			       </logic:equal>
				   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		              <bean:message key="button.close"/>
		           </html:submit>
	           
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="uploadInvoiceItemEntry.gridTitle.uploadDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="300" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.control"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.customer"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.dateTransaction"/>
				       </td>
				       <td width="222" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.invoice"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.taxCode"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.lines"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="uploadInvoiceItemEntry.prompt.status"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="arTrnsctnLst">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				   
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="300" height="1" class="control">
				      	  <nested:text property="control" size="23" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:text property="customer" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="date" size="23" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="222" height="1" class="control">
				          <nested:text property="invoiceNo" size="23" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="100" height="1" class="control">
				           <nested:text property="itemLines" size="23" maxlength="25" styleClass="text" disabled="true"/> 
				       </td>			       
				     		       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="status" size="80" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		</tr>

	 
	 
	 
	    <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		    </td>
	    </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>

</body>
</html>
