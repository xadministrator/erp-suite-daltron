<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="adjustmentEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function submitSelect(field1,field2)
{
      enableInputControls();
      enterSelect(field1, field2);
}

function submitButton()
{
      disableButtons();
      enableInputControls();
}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/cmAdjustmentEntry.do" onsubmit="return submitButton();">
   <%@ include file="cmnHeader.jsp" %>
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="310">
      <tr valign="top">
         <td width="187" height="310"></td>
         <td width="581" height="310">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="310"
                                      bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="adjustmentEntry.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="cmAdjustmentEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>
		       <html:messages id="msg" message="true">
		       <bean:write name="msg"/>
		   	   </html:messages>
	           </td>
	        </tr>
	        <html:hidden property="isBankAccountEntered" value=""/>
	        <html:hidden property="isTypeEntered" value=""/>
	        <html:hidden property="isConversionDateEntered" value=""/>
	         <html:hidden property="isSalesOrderEntered" value=""/>
	        <logic:equal name="cmAdjustmentEntryForm" property="enableFields" value="true">
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				   	<tr>
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
	        		<tr>
	        		<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.type"/>
               			</td>
						<td width="158" height="25" class="control"	onchange="return submitSelect('type','isTypeEntered');">
				   			<html:select property="type" styleClass="comboRequired" style="width:130;">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>


						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.date"/>
                		</td>
		        		<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
		        		</td>
            		</tr>

            		<tr>
            		<logic:equal name="cmAdjustmentEntryForm" property="showCustomerList" value="true">
            			<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.customer"/>
                		</td>


						<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered');">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered');"/>
                		</td>
                	</logic:equal>



                		<logic:equal name="cmAdjustmentEntryForm" property="showSupplierList" value="true">
            			<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.supplier"/>
                		</td>


						<td width="158" height="25" class="control">
                   				<html:select property="supplier" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                       			<html:options property="supplierList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
                		</td>
                		</logic:equal>




                		<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
		       			</td>

            		</tr>

	        		<tr>
	           			<td class="prompt" width="130" height="25">
				   			<bean:message key="adjustmentEntry.prompt.bankAccount"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" onchange="return submitSelect('bankAccount','isBankAccountEntered');">
				      			<html:options property="bankAccountList"/>
				   			</html:select>
						</td>

						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
		       			</td>



					</tr>

					<tr>

						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.checkNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="checkNumber" size="15" maxlength="25" styleClass="text"/>
		       			</td>
					</tr>

					<logic:equal name="cmAdjustmentEntryForm" property="type" value="SO ADVANCE">
					<tr>

						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.soNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
		       				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArSoLookupForAdj('soNumber');"/>
		       			</td>
					</tr>
					</logic:equal>

	        		<tr>

		       			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.amount"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
		       			</td>

		       			<td class="prompt" width="130" height="25">
	                  			<bean:message key="adjustmentEntry.prompt.amountApplied"/>
	               			</td>
			       			<td width="415" height="25" class="control" colspan="3">
			                   <html:text property="amountApplied" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>

            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="adjustmentEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text"/>
		       			</td>
            		</tr>

            		<tr>
               			<td width="130" height="25" class="prompt">
                  				<bean:message key="adjustmentEntry.prompt.customerName"/>
               			</td>
               			<td width="445" height="25" class="control" colspan="3">
                  				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
               			</td>
        			</tr>

				</table>
				</div>
				<div class="tabbertab" title="Misc">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				   	<tr>
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
					<tr>
						<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>
					</tr>



					<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
				</table>
				</div>
				<div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="currency" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="adjustmentEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"/>
			                </td>
				         </tr>
					</table>
					</div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>


				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>


				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>

	     <logic:equal name="cmAdjustmentEntryForm" property="enableFields" value="false">
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				   	<tr>
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
	        		<tr>

	        		<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.type"/>
               			</td>
						<td width="158" height="25" class="control"	>
				   			<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>

						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.date"/>
                		</td>
		        		<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
		        		</td>
            		</tr>

            		<tr>
            		<logic:equal name="cmAdjustmentEntryForm" property="showCustomerList" value="true">
            			<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.customer"/>
                		</td>

            			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>

                		</logic:equal>
                			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>

            		</tr>

	        		<tr>
	        			<td class="prompt" width="130" height="25">
				   			<bean:message key="adjustmentEntry.prompt.bankAccount"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountList"/>
				   			</html:select>
						</td>

						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>





					</tr>

					<tr>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.checkNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="checkNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>
					</tr>
					<tr>

						<logic:equal name="cmAdjustmentEntryForm" property="type" value="SO ADVANCE">
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.soNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
		       			</td>

		       			</logic:equal>
					</tr>

	        		<tr>

	        		<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.amount"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		       			</td>


	           			<td class="prompt" width="130" height="25">
	                  			<bean:message key="adjustmentEntry.prompt.amountApplied"/>
	               			</td>
			       			<td width="157" height="25" class="control">
	                  			<html:text property="amountApplied" size="10" maxlength="10" styleClass="text" disabled="true"/>
			       			</td>

            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="adjustmentEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       			</td>
            		</tr>
					<logic:equal name="cmAdjustmentEntryForm" property="showCustomerList" value="true">
            		<tr>
               			<td width="130" height="25" class="prompt">
                  				<bean:message key="adjustmentEntry.prompt.customerName"/>
               			</td>
               			<td width="445" height="25" class="control" colspan="3">
                  				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
               			</td>
        			</tr>

        			</logic:equal>
				</table>
				</div>
				<div class="tabbertab" title="Misc">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				   	<tr>
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
					<tr>
						<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid" />
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>


					</tr>



					<tr>
         					<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.totalDebit"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="totalDebit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="adjustmentEntry.prompt.totalCredit"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="totalCredit" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr>


				</table>
				</div>

				<logic:equal name="cmAdjustmentEntryForm" property="type" value="ADVANCE">
				<div class="tabbertab" title="Refund">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				   	<tr>
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>


					<tr>
						<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentRefund" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentRefund"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentRefund"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentRefund" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentRefund"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentRefund" disabled="true"/>
		       			</td>
		       			</logic:equal>
					</tr>

					<tr>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.refundAmount"/>
               			</td>
		       			<td width="415" height="25" class="control" colspan="3">
		                   <html:text property="refundAmount" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
					</tr>

					<tr>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.refundReferenceNumber"/>
               			</td>
		       			<td width="415" height="25" class="control" colspan="3">
		                   <html:text property="refundReferenceNumber" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
					</tr>

				</table>
				</div>
				</logic:equal>

				<div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="currency" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text"  disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="adjustmentEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text"  disabled="true"/>
			                </td>
				         </tr>
					</table>
					</div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.voidApprovalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="voidApprovalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.voidPosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="voidPosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>

				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					     <tr>
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>
		     <tr>
	         <td width="575" height="30" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="cmAdjustmentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="cmAdjustmentEntryForm" property="showRefundButton" value="true">
	           <html:submit property="refundButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.refund"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="cmAdjustmentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="cmAdjustmentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="cmAdjustmentEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="cmAdjustmentEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="cmAdjustmentEntryForm" property="showRefundButton" value="true">
	           <html:submit property="refundButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.refund"/>
		       </html:submit>
		       </logic:equal>

		       <logic:equal name="cmAdjustmentEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="cmAdjustmentEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
	           <html:submit property="printButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.print"/>
	           </html:submit>
	           <logic:equal name="cmAdjustmentEntryForm" property="showJournalButton" value="true">
	           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>

<logic:equal name="cmAdjustmentEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="cmAdjustmentEntryForm" type="com.struts.cm.adjustmententry.CmAdjustmentEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--

     win = window.open("<%=request.getContextPath()%>/cmRepAdjustmentPrint.do?forward=1&adjustmentCode=<%=actionForm.getAdjustmentCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["bankAccount"] != null)
        document.forms[0].elements["bankAccount"].focus()
	       // -->
</script>
</body>
</html>
