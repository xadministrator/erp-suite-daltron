<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="adjustmentEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/cmAdjustmentEntry.do" onsubmit="return submitForm();">
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="310">
      <tr valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="310" 
                                      bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="adjustmentEntry.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="cmAdjustmentEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>
		       <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   	   </html:messages>	
	           </td>
	        </tr>
	        <html:hidden property="isBankAccountEntered"/>              
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				   	<tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="adjustmentEntry.prompt.bankAccount"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccount" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountList"/>
				   			</html:select>
						</td>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.date"/>
                		</td>
		        		<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
		        		</td>
            		</tr> 
	        		<tr>		 		 		 
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.type"/>
               			</td>
						<td width="158" height="25" class="control"	>
				   			<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.amount"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		       			</td>
					</tr>
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>	
						<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>	 
            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="adjustmentEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       			</td>		 		 
            		</tr>
				</table>
				</div>
				<div class="tabbertab" title="Misc">
   				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				   	<tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
			        	</td>
			    	</tr>
					<tr>
						<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmAdjustmentEntryForm" property="enableAdjustmentVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="adjustmentEntry.prompt.adjustmentVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="adjustmentVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>		 
					</tr>
				</table>
				</div>
				<div class="tabbertab" title="Currency">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.currency"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="currency" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.conversionDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td class="prompt" width="100" height="25">
			                   <bean:message key="adjustmentEntry.prompt.conversionRate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>				    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>					         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="adjustmentEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>           	                             	     
		     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
      </tr>
   </table>
</html:form>
</body>
</html>
