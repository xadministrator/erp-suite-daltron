<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="bankReconciliation.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

function submitForm()
{
      disableButtons();
      enableInputControls();
}


function selectAllDeposits()
{

   var i = 0;
   
   var endingBalance =(document.forms[0].elements["endingBalance"].value).replace(/,/g,'');
   var clearedBalance = (document.forms[0].elements["clearedBalance"].value).replace(/,/g,'');
 
   var totalDepositAmount = 0;
   while(true) {
   
      if (document.forms[0].elements["cmDINList[" + i + "].depositMark"] == null 
    		  //|| document.forms[0].elements["cmOCList[" + i + "].checkMark"] == null
    		  ) {
          
          break;
      
      }
      
      if(document.forms[0].elements["cmDINList["+ i +"]" + ".depositMark"].checked == false){
         var depositAmount = (document.forms[0].elements["cmDINList["+ i +"]" + ".depositAmount"].value).replace(/,/g,'');
      
         totalDepositAmount += parseFloat(depositAmount);

         document.forms[0].elements["cmDINList[" + i + "].depositMark"].checked = true;

         document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 + 
                totalDepositAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      }
     
      
      //alert("totalDepositAmount="+totalDepositAmount);
      
      
      
      
      //document.forms[0].elements["cmOCList[" + i + "].checkMark"].checked = true;
      
      /*
      if (document.forms[0].elements["cmDINList["+ i +"]" + ".depositMark"].checked == true) {
          
          
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 - 
            		totalDepositAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
        */  
      i++;   
   }  
   
   //cmOCList
   return false;   
      
}




function selectAllChecks()
{

   var i = 0;
   
   var endingBalance = (document.forms[0].elements["endingBalance"].value).replace(/,/g,'');
   var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
   
   var totalCheckAmount = 0;
   while(true) {
   
      if (document.forms[0].elements["cmOCList[" + i + "].checkMark"] == null 
    		  //|| document.forms[0].elements["cmOCList[" + i + "].checkMark"] == null
    		  ) {
          
          break;
      
      }


      if(document.forms[0].elements["cmOCList[" + i + "].checkMark"].checked == false){

        var checkAmount = (document.forms[0].elements["cmOCList["+ i +"]" + ".checkAmount"].value).replace(/,/g,'');
        totalCheckAmount += parseFloat(checkAmount);
        document.forms[0].elements["cmOCList[" + i + "].checkMark"].checked = true;

         document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 -
                totalCheckAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());

      }
      
      
      
      //alert("totalDepositAmount="+totalDepositAmount);
      
      /*
      
      document.forms[0].elements["cmOCList[" + i + "].checkMark"].checked = true;
      
      
      
      if (document.forms[0].elements["cmOCList["+ i +"]" + ".checkMark"].checked == true) {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 -
            		totalCheckAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 + 
            		totalCheckAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
      */
      i++;   
   }  
   
   //cmOCList
   return false;   
      
}




 function unselectAll()
  {

   var depositIndex = 0;
   var checkIndex = 0;
   var bankDebitIndex = 0;
   var bankCreditIndex = 0;
   
   var totalDeposit = 0.0;
   var totalCheck = 0.0;
   var totalBankDebit = 0.0;
   var totalBankCredit = 0.0;

   var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
   var difference = parseFloat((document.forms[0].elements['difference'].value).replace(/,/g,''));

   //Deposit List Uncheck method
   while(true) {

    if( document.forms[0].elements["cmDINList[" + depositIndex + "].depositMark"].checked==null){

      break;
    }

    if(document.forms[0].elements["cmDINList[" + depositIndex + "].depositMark"].checked == true){

       var depositAmount = (document.forms[0].elements["cmDINList["+ depositIndex +"]" + ".depositAmount"].value).replace(/,/g,'');
      
       totalDeposit += depositAmount;

       document.forms[0].elements["cmDINList[" + depositIndex + "].depositMark"].checked = false;
    }    
    depositIndex++;
   
   }

   clearBalance  = clearedBalance - totalDeposit ;
   difference = difference + totalDeposit;

   //Check List Uncheck method

   while(true) {

    if( document.forms[0].elements["cmOCList[" + checkIndex + "].checkMark"].checked==null){

      break;
    }

    if(document.forms[0].elements["cmOCList[" + checkIndex + "].checkMark"].checked == true){

       var checkAmount = (document.forms[0].elements["cmOCList["+ checkIndex +"]" + ".checkAmount"].value).replace(/,/g,'');
      
       totalCheck += checkAmount;

       document.forms[0].elements["cmOCList[" + checkIndex + "].checkMark"].checked = false;
    }    
    checkIndex++;
   
   }

   clearBalance  = clearedBalance + totalDeposit ;
   difference = difference - totalDeposit;


   //Bank Credit List Uncheck method
   while(true) {

    if( document.forms[0].elements["cmBCList[" + bankCreditIndex + "].bankCreditMark"].checked==null){

      break;
    }

    if(document.forms[0].elements["cmBCList[" + bankCreditIndex + "].bankCreditMark"].checked == true){

       var bankCreditAmount = (document.forms[0].elements["cmBCList["+ bankCreditIndex +"]" + ".bankCreditAmount"].value).replace(/,/g,'');
      
       totalBankCredit += bankCreditAmount;

       document.forms[0].elements["cmBCList[" + bankCreditIndex + "].bankCreditMark"].checked = false;
    }    
    bankCreditIndex++;
   
   }

   clearBalance  = clearedBalance - totalBankCredit ;
   difference = difference + totalBankCredit;



   //Bank Debit List Uncheck method

   while(true) {

    if( document.forms[0].elements["cmBCList[" + bankDebitIndex + "].bankDebitMark"].checked==null){

      break;
    }

    if(document.forms[0].elements["cmBCList[" + bankDebitIndex + "].bankDebitMark"].checked == true){

       var bankDebitAmount = (document.forms[0].elements["cmBCList["+ bankDebitIndex +"]" + ".bankDebitAmount"].value).replace(/,/g,'');
      
       totalBankDebit += bankDebitAmount;

       document.forms[0].elements["cmBCList[" + bankDebitIndex + "].bankDebitMark"].checked = false;
    }    
    bankDebitIndex++;
   
   }

   clearBalance  = clearedBalance + totalBankDebit ;
   difference = difference - totalBankDebit;


    document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance).toFixed(2).toString());
            
          
   


    document.forms[0].elements["difference"].value = 
            formatDisabledAmount((difference).toFixed(2).toString());

         
   return false;   
      
} 

function markDeposit(name)
{
	//alert(name);//cmDINList[0].depositMark
      var property = name.substring(0,name.indexOf("."));  
      //alert(property);//cmDINList[0]
      var endingBalance = 0.00;  
      var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
      var depositAmount = parseFloat((document.forms[0].elements[property + ".depositAmount"].value).replace(/,/g,''));
      
      if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
      }      
            
      if (document.forms[0].elements[property + ".depositMark"].checked == true) {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 + 
            depositAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 - 
            depositAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
      
}


function markBankDebit(name)
{
      var property = name.substring(0,name.indexOf("."));      
      var endingBalance = 0.00;  
      var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
      var bankDebitAmount = parseFloat((document.forms[0].elements[property + ".bankDebitAmount"].value).replace(/,/g,''));
      
      if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
      }      
            
      if (document.forms[0].elements[property + ".bankDebitMark"].checked == true) {
      
          document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 +
            bankDebitAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 - 
            bankDebitAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
}



function selectAllBankDebits()
{


   var i = 0;
   
   var endingBalance = (document.forms[0].elements["endingBalance"].value).replace(/,/g,''); 
   var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
   
   var totalBankDebitAmount = 0;
   while(true) {
   
      if (document.forms[0].elements["cmBDList[" + i + "].bankDebitMark"] == null 
          //|| document.forms[0].elements["cmOCList[" + i + "].checkMark"] == null
          ) {
          
          break;
      
      }
      
      if(document.forms[0].elements["cmBDList["+ i +"]" + ".bankDebitMark"].checked == false){
        var bankDebitAmount = (document.forms[0].elements["cmBDList["+ i +"]" + ".bankDebitAmount"].value).replace(/,/g,'');

        totalBankDebitAmount += parseFloat(bankDebitAmount);

       
            document.forms[0].elements["cmBDList[" + i + "].bankDebitMark"].checked = true;

            document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 +
                totalBankDebitAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      }
      
     
  
      i++;   
   }  
   
   //cmBDList
   return false;   
      
}










function markCheck(name)
{
      var property = name.substring(0,name.indexOf("."));      
      var endingBalance = 0.00;  
      var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
      var checkAmount = parseFloat((document.forms[0].elements[property + ".checkAmount"].value).replace(/,/g,''));
      
      if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
      }      
            
      if (document.forms[0].elements[property + ".checkMark"].checked == true) {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 -
            checkAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
        	  formatDisabledAmount((clearedBalance * 1 + 
            checkAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
        	  formatDisabledAmount((endingBalance * 1 - 
        			  parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
}



function markBankCredit(name)
{
  //alert(name);//cmDINList[0].depositMark
      var property = name.substring(0,name.indexOf("."));  
      //alert(property);//cmDINList[0]
      var endingBalance = 0.00;  
      var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
      var bankCreditAmount = parseFloat((document.forms[0].elements[property + ".bankCreditAmount"].value).replace(/,/g,''));
      
      if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
      }      
            
      if (document.forms[0].elements[property + ".bankCreditMark"].checked == true) {
      
          document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 - 
            bankCreditAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      } else {
      
          document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 + 
            bankCreditAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());
      
      }
      
}



function selectAllBankCredits()
{

   var i = 0;
   
   var endingBalance = (document.forms[0].elements["endingBalance"].value).replace(/,/g,'');  
   var clearedBalance = (document.forms[0].elements["clearedBalance"].value).replace(/,/g,'');
   
   var totalBankCreditAmount = 0;
   while(true) {
   
      if (document.forms[0].elements["cmBCList[" + i + "].bankCreditMark"] == null 
          //|| document.forms[0].elements["cmOCList[" + i + "].checkMark"] == null
          ) {
          
          break;
      
      }
      

      if(document.forms[0].elements["cmBCList[" + i + "].bankCreditMark"].checked == false){
         var bankCreditAmount = (document.forms[0].elements["cmBCList["+ i +"]" + ".bankCreditAmount"].value).replace(/,/g,'');
      
         totalBankCreditAmount += parseFloat(bankCreditAmount);
        document.forms[0].elements["cmBCList[" + i + "].bankCreditMark"].checked  =true;
           document.forms[0].elements["clearedBalance"].value = 
            formatDisabledAmount((clearedBalance * 1 - 
                totalBankCreditAmount * 1).toFixed(2).toString());
            
          
          document.forms[0].elements["difference"].value = 
            formatDisabledAmount((endingBalance * 1 - 
                parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());

      }
     
      
      
      i++;   
   }  
   
   //cmBCList
   return false;   
      
}


function enterEndingBalance() 
{

    var endingBalance = 0.00;
    var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
    
    if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
    }
    
    document.forms[0].elements["difference"].value = 
    	formatDisabledAmount((endingBalance * 1 - 
            clearedBalance * 1).toFixed(2).toString());

}

function enterInterestEarned() 
{

    var endingBalance = 0.00;
    var interestEarned = 0.00;
    var clearedBalance = parseFloat((document.forms[0].elements["clearedBalance"].value).replace(/,/g,''));
    
    if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = parseFloat((document.forms[0].elements["endingBalance"].value).replace(/,/g,''));
      
    }
    
    if (!isNaN(parseFloat(document.forms[0].elements["interestEarned"].value))) {
      
         interestEarned = parseFloat((document.forms[0].elements["interestEarned"].value).replace(/,/g,''));
      
    }
    
    document.forms[0].elements["clearedBalance"].value = 
    	formatDisabledAmount((clearedBalance * 1 - this.originalInterestEarned * 1 + 
        interestEarned * 1).toFixed(2).toString());
        
   this.originalInterestEarned = interestEarned;
            
          
    document.forms[0].elements["difference"].value = 
    	formatDisabledAmount((endingBalance * 1 - 
    			parseFloat(document.forms[0].elements["clearedBalance"].value.replace(/,/g,'')) * 1).toFixed(2).toString());

}

function enterServiceCharge() 
{

    var endingBalance = 0.00;
    var serviceCharge = 0.00;
    var clearedBalance = (document.forms[0].elements["clearedBalance"].value).replace(/,/g,'');
    
    if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
      
         endingBalance = (document.forms[0].elements["endingBalance"].value).replace(/,/g,'');
      
    }
    
    if (!isNaN(parseFloat(document.forms[0].elements["serviceCharge"].value))) {
      
         serviceCharge = (document.forms[0].elements["serviceCharge"].value).replace(/,/g,'');
      
    }
    
    document.forms[0].elements["clearedBalance"].value = 
        (clearedBalance * 1 + this.originalServiceCharge * 1 - 
        serviceCharge * 1).toFixed(2);
        
    this.originalServiceCharge = serviceCharge;
            
          
    document.forms[0].elements["difference"].value = 
       (endingBalance * 1 - 
       document.forms[0].elements["clearedBalance"].value * 1).toFixed(2);

}



function clickUpload(){
return true;


}

function clickValidate(){
	return true;
}


function clickReconcile()
{




    if(confirm("Are you sure you want to reconcile this account?")) {
    

     





        var endingBalance = 0.00;
	    var clearedBalance = (document.forms[0].elements["clearedBalance"].value).replace(/,/g,'');
	    
	    if (!isNaN(parseFloat(document.forms[0].elements["endingBalance"].value))) {
	      
	         endingBalance = (document.forms[0].elements["endingBalance"].value).replace(/,/g,'');
	      
	    }
	    
	    if (endingBalance * 1 != clearedBalance * 1) {
	    
	        if(confirm("Warning! There is a difference of " + document.forms[0].elements["difference"].value +
	            " between the cleared items and the ending balance. Do you want to create auto adjustment?")) {
	        
	            document.forms[0].elements["createAutoAdjustment"].value = true;
	            return true;
	        
	        } else {
	        
	            document.forms[0].elements["createAutoAdjustment"].value = false;
	            return true;
	        
	        }
	    
	    }
    
    
    } else {
    
        return false;
        
    }

}


//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('reconcileButton'));">
<html:form action="/cmBankReconciliation.do" onsubmit="return submitForm();" enctype="multipart/form-data">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="820" height="350">
      <tr valign="top">
        <td width="187" height="300"></td> 
        <td width="581" height="300">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="bankReconciliation.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="cmBankReconciliationForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>	     
	     <html:hidden property="isBankAccountEntered"/>
   	     <html:hidden property="isReconcileDateEntered"/>
	     <html:hidden property="createAutoAdjustment"/>
	     <tr>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.bankAccountName"/>
	       </td>
	       <td width="128" height="25" class="control">
	          <html:select property="bankAccountName" styleClass="comboRequired" onchange="return enterSelect('bankAccountName','isBankAccountEntered');">
	              <html:options property="bankAccountNameList"/>
	          </html:select>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.currency"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:select property="currency" styleClass="combo" disabled="true">
	              <html:options property="currencyList"/>
	          </html:select>
	       </td>
         </tr>	 
         <tr>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.lastReconciledDate"/>
	       </td>
	       <td width="128" height="25" class="control">
	          <html:text property="lastReconciledDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.reconcileDate"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="reconcileDate" size="10" maxlength="10" styleClass="textRequired" onblur="return enterSelect('bankAccountName','isReconcileDateEntered');"/>
	       </td>
         </tr>	
         <tr>
           <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.accountBalance"/>
	       </td>
	       <td width="128" height="25" class="control">
	          <html:text property="accountBalance" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
	       </td>
	       
         </tr>
         <tr>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.openingBalance"/>
	       </td>
	       <td width="128" height="25" class="control">
	          <html:text property="openingBalance" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.endingBalance"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="endingBalance" size="25" maxlength="25" styleClass="textAmountRequired" onblur="addZeroes(name); return enterEndingBalance();" onkeyup="formatAmount(name, (event)?event:window.event);"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.clearedBalance"/>
	       </td>
	       <td width="128" height="25" class="control">
	          <html:text property="clearedBalance" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.difference"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="difference" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
	       </td>
         </tr>	
         <tr>
           <td class="prompt" width="160" height="25">
	          <bean:message key="bankReconciliation.prompt.UploadBankStatement"/>
	       </td>
	       
	       <td width="628" height="25" class="control" colspan="3">
              <html:file property="uploadFile" size="35" styleClass="textRequired"/>
              <html:submit property="uploadButton" style = "margin-left:10px;" styleClass="mainButtonMedium" onclick="return clickUpload();" value="Upload File">
		     	<bean:message key="button.BankStatementUpload"/>
		     </html:submit>
           </td>
           
          
          
         </tr>     


         <!--
         <tr>
          <td class="prompt" width="160" height="25">
	          Validattion Bank Statement
	       </td>
	       
	       <td width="628" height="25" class="control" colspan="3">
              <html:file property="validationFile" size="35" styleClass="textRequired"/>
              <html:submit property="validationButton" style = "margin-left:10px;" styleClass="mainButtonMedium"  value="Upload File">
		     	
		     </html:submit>
		     
		     
		   
           </td>
         
         
         
         </tr>
         !-->
        
	     <tr>
		    <td width="160" height="50">
			   <div id="buttons">
			   <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			   <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			      <bean:message key="button.showDetails"/>
			   </html:submit>
			   </logic:equal>			     
			   <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			   <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			      <bean:message key="button.hideDetails"/>
			   </html:submit>
			   </logic:equal>			     			     
			   </div>
			   <div id="buttonsDisabled" style="display: none;">
			   <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			   <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.showDetails"/>
			   </html:submit>
			   </logic:equal>			     
			   <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			   <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			      <bean:message key="button.hideDetails"/>
			   </html:submit>
			   </logic:equal>			     
			   </div>
		   </td>	       	       	      	     
	       <td width="415" height="50" colspan="3"> 
	         <div id="buttons">
	         <p align="right">		         
		         <html:submit property="reconcileButton" styleClass="mainButton" onclick="return clickReconcile();">
		            <bean:message key="button.reconcile"/>
		         </html:submit>
		         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAllDeposits();">
				         <bean:message key="button.selectAllDeposits"/>
		         </html:submit>
		         
		         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAllChecks();">
				         <bean:message key="button.selectAllChecks"/>
		         </html:submit>

              <html:submit property="selectAllButton" styleClass="mainButtonMedium" onclick="return selectAllBankCredits();">
                 <bean:message key="button.selectAllBankCredits"/>
             </html:submit>

              <html:submit property="selectAllButton" styleClass="mainButtonMedium" onclick="return selectAllBankDebits();">
                 <bean:message key="button.selectAllBankDebits"/>
             </html:submit>

              <html:submit property="selectAllButton" styleClass="mainButtonMedium" onclick="return unselectAll();">
                 <bean:message key="button.unselectAll"/>
             </html:submit>
		         
		         <%-- <html:submit property="unselectAllButton" styleClass="mainButtonMedium" onclick="return unselectAll();">
				    <bean:message key="button.unselectAll"/>
				    </html:submit>	 --%>	         
		         <html:submit property="closeButton" styleClass="mainButton">
		            <bean:message key="button.close"/>
		         </html:submit>
	         </div>
	         <div id="buttonsDisabled" style="display: none;">
	         <p align="right">		         
		         <html:submit property="reconcileButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.reconcile"/>
		         </html:submit>	
		         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAllDeposits"/>
				 </html:submit>


				 <html:submit property="selectAllButton" styleClass="mainButtonMedium" disabled="true">
				         <bean:message key="button.selectAllChecks"/>
				 </html:submit>	

				   <html:submit property="unselectAllButton" styleClass="mainButtonMedium" disabled="true">
				    <bean:message key="button.unselectAll"/>
				    </html:submit>  

		         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.close"/>
		         </html:submit>
		      </div>
	        </td>
	      </tr>	 	 
	      <tr> 
	      	<td width="575" height="10" colspan="4">  
	      	  <div class="tabber">
	          <div class="tabbertab" title="Deposits(+)">
	          <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
			      <tr valign="top">
			          <td width="575" height="185" colspan="4">
				         <div align="center">
				         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
				             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
						     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					     <tr>
		                     <td width="575" height="1" colspan="9" class="gridTitle" 
					             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                     <bean:message key="bankReconciliation.gridTitle.DINDetails"/>
			                 </td>
			            </tr>
			            <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
					    <tr>
					       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositDate"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositDocumentNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositReferenceNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositType"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositPayee"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.depositAmount"/>
					       </td>
					       <td width="170" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.dateCleared"/>
					       </td>
		                </tr>			   			            
					    <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate property="cmDINList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="100" height="1" class="gridLabel">
					          <nested:write property="depositDate"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="depositDocumentNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="depositReferenceNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="depositNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="depositType"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="depositPayee"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridLabelNum">
					          <nested:write property="depositAmount"/>
					          <nested:hidden property="depositAmount"/>
					       </td>
					       <td width="100" height="1" class="gridLabel">
					          <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
					       </td>
					       <td width="70" align="center" height="1">
					          <nested:checkbox property="depositMark" onclick="return markDeposit(name);"/>
					       </td>
					    </tr>
					    </nested:iterate>
					    </logic:equal>
					    <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
					    <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate property="cmDINList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="175" height="1" class="gridHeader">
					          <bean:message key="bankReconciliation.prompt.depositDate"/>
					       </td>
					       <td width="570" height="1" class="gridLabel" colspan="2">
					          <nested:write property="depositDate"/>
					       </td>
					       <td width="149" align="center" height="1">
					          <nested:checkbox property="depositMark" onclick="return markDeposit(name);"/>
					       </td>
					    </tr>
					    <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.depositNumber"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel">
		                      <nested:write property="depositNumber"/>
		                   </td>
					       <td width="270" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.depositPayee"/>
		                   </td>
		                   <td width="149" height="1" class="gridLabel">
		                      <nested:write property="depositPayee"/>
		                   </td>
		                </tr>
		                <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.depositType"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel">
		                      <nested:write property="depositType"/>
		                   </td>
					       <td width="270" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.depositAmount"/>		                      
		                   </td>
		                   <td width="149" height="1" class="gridLabelNum">
		                      <nested:write property="depositAmount"/>
		                      <nested:hidden property="depositAmount"/>
		                   </td>
		                </tr>
		                <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.dateCleared"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel" colspan="3">
		                      <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
		                   </td>
		                </tr>			    
					    </nested:iterate>					    
					    </logic:equal>
					    </table>
				        </div>
				     </td>
			       </tr> 
		       </table>
		       </div>
		       
		       
		       
		       <div class="tabbertab" title="Bank Debit(+)">
            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
            <tr valign="top">
                <td width="575" height="185" colspan="4">
                 <div align="center">
                 <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
                     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
                 bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
               <tr>
                         <td width="575" height="1" colspan="8" class="gridTitle" 
                       bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                           <bean:message key="bankReconciliation.gridTitle.BDDetails"/>
                       </td>
                  </tr>
                  <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
              <tr>
                 <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitDate"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitDocumentNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitReferenceNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitType"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitPayee"/>
                 </td>                                                 
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankDebitAmount"/>
                 </td>
                  <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    Posted
                 </td>
                 <td width="170" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.dateCleared"/>
                 </td>

                    </tr>                         
              <%
                 int i = 0; 
                 String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                 String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                 String rowBgc = null;
              %>
              <nested:iterate property="cmBDList">
              <%
                 i++;
                 if((i % 2) != 0){
                     rowBgc = ROW_BGC1;
                 }else{
                     rowBgc = ROW_BGC2;
                 }  
              %>
              <tr bgcolor="<%= rowBgc %>">
                 <td width="100" height="1" class="gridLabel">
                    <nested:write property="bankDebitDate"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankDebitDocumentNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankDebitReferenceNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankDebitNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankDebitType"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankDebitPayee"/>
                 </td>                                                 
                 <td width="149" height="1" class="gridLabelNum">
                    <nested:write property="bankDebitAmount"/>
                    <nested:hidden property="bankDebitAmount"/>
                 </td>
                  <td width="50" height="1" class="gridLabelNum">
                    <nested:write property="posted"/>
                 </td>
                 <td width="100" height="1" class="gridLabel">
                    <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
                 </td>
                 <td width="70" align="center" height="1">
                    <nested:checkbox property="bankDebitMark" onclick="return markBankDebit(name);"/>
                 </td>
              </tr>
              </nested:iterate>
              </logic:equal>
              <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
              <%
                 int i = 0; 
                 String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                 String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                 String rowBgc = null;
              %>
              <nested:iterate property="cmBDList">
              <%
                 i++;
                 if((i % 2) != 0){
                     rowBgc = ROW_BGC1;
                 }else{
                     rowBgc = ROW_BGC2;
                 }  
              %>
              <tr bgcolor="<%= rowBgc %>">
                 <td width="175" height="1" class="gridHeader">
                    <bean:message key="bankReconciliation.prompt.bankDebitDate"/>
                 </td>
                 <td width="570" height="1" class="gridLabel" colspan="2">
                    <nested:write property="bankDebitDate"/>
                 </td>
                 <td width="149" align="center" height="1">
                    <nested:checkbox property="bankDebitMark" onclick="return markBankDebit(name);"/>
                 </td>
              </tr>
              <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankDebitNumber"/>
                       </td>
                       <td width="300" height="1" class="gridLabel">
                          <nested:write property="bankDebitNumber"/>
                       </td>
                 <td width="270" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankDebitPayee"/>
                       </td>
                       <td width="149" height="1" class="gridLabel">
                          <nested:write property="bankDebitPayee"/>
                       </td>
                    </tr>
                    <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankDebitType"/>
                       </td>
                       <td width="300" height="1" class="gridLabel">
                          <nested:write property="bankDebitType"/>
                       </td>
                 <td width="270" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankDebitAmount"/>                         
                       </td>
                       <td width="149" height="1" class="gridLabelNum">
                          <nested:write property="bankDebitAmount"/>
                          <nested:hidden property="bankDebitAmount"/>
                       </td>
                        <td width="270" height="1" class="gridHeader">
                          Posted                        
                       </td>
                       <td width="149" height="1" class="gridLabelNum">
                          <nested:write property="Posted"/>
                        
                       </td>
                    </tr>
                    <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.dateCleared"/>
                       </td>
                       <td width="300" height="1" class="gridLabel" colspan="3">
                          <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
                       </td>
                    </tr>         
              </nested:iterate>             
              </logic:equal>
              </table>
                </div>
             </td>
             </tr> 
           </table>
           </div>
		       
		       <div class="tabbertab" title="Checks">
	           <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
			      <tr valign="top">
			          <td width="575" height="185" colspan="4">
				         <div align="center">
				         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
				             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
						     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					     <tr>
		                     <td width="575" height="1" colspan="9" class="gridTitle" 
					             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                     <bean:message key="bankReconciliation.gridTitle.OCDetails"/>
			                 </td>
			            </tr>
			            <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
					    <tr>
					       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkDate"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkDocumentNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkReferenceNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkType"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkPayee"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.checkAmount"/>
					       </td>
					       <td width="170" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="bankReconciliation.prompt.dateCleared"/>
					       </td>
		                </tr>			            
					    <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate property="cmOCList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="100" height="1" class="gridLabel">
					          <nested:write property="checkDate"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="checkDocumentNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="checkReferenceNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="checkNumber"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="checkType"/>
					       </td>
					       <td width="149" height="1" class="gridLabel">
					          <nested:write property="checkPayee"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridLabelNum">
					          <nested:write property="checkAmount"/>
					          <nested:hidden property="checkAmount"/>
					       </td>
					       <td width="100" height="1" class="gridLabel">
					          <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
					       </td>
					       <td width="70" align="center" height="1">
					          <nested:checkbox property="checkMark" onclick="return markCheck(name);"/>
					       </td>
					    </tr>
					    </nested:iterate>
                        </logic:equal>
                        <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        					    
					    <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate property="cmOCList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="175" height="1" class="gridHeader">
					          <bean:message key="bankReconciliation.prompt.checkDate"/>
					       </td>
					       <td width="570" height="1" class="gridLabel" colspan="2">
					          <nested:write property="checkDate"/>
					       </td>
					       <td width="149" align="center" height="1">
					          <nested:checkbox property="checkMark" onclick="return markCheck(name);"/>
					       </td>
					    </tr>
					    <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.checkNumber"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel">
		                      <nested:write property="checkNumber"/>
		                   </td>
					       <td width="270" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.checkPayee"/>
		                   </td>
		                   <td width="149" height="1" class="gridLabel">
		                      <nested:write property="checkPayee"/>
		                   </td>
		                </tr>
		                <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.checkType"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel">
		                      <nested:write property="checkType"/>
		                   </td>
					       <td width="270" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.checkAmount"/>
		                   </td>
		                   <td width="149" height="1" class="gridLabelNum">
		                      <nested:write property="checkAmount"/>
		                      <nested:hidden property="checkAmount"/>
		                   </td>
		                </tr>
		                <tr bgcolor="<%= rowBgc %>">
		                   <td width="175" height="1" class="gridHeader">
		                      <bean:message key="bankReconciliation.prompt.dateCleared"/>
		                   </td>
		                   <td width="300" height="1" class="gridLabel" colspan="3">
		                      <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
		                   </td>
					    </tr>			    
					    </nested:iterate>					    
					    </logic:equal>
					    </table>
				        </div>
				     </td>
			       </tr> 
		       </table>
		       </div>

           <!-- Bank Credit !-->

           <div class="tabbertab" title="Bank Credit">
            <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
            <tr valign="top">
                <td width="575" height="185" colspan="4">
                 <div align="center">
                 <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
                     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
                 bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
               <tr>
                         <td width="575" height="1" colspan="9" class="gridTitle" 
                       bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                           <bean:message key="bankReconciliation.gridTitle.BCDetails"/>
                       </td>
                  </tr>
                  <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
              <tr>
                 <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditDate"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditDocumentNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditReferenceNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditNumber"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditType"/>
                 </td>
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditPayee"/>
                 </td>                                                 
                 <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.bankCreditAmount"/>
                 </td>
                  <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    Posted
                 </td>
                 <td width="170" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                    <bean:message key="bankReconciliation.prompt.dateCleared"/>
                 </td>
                
                  
              </tr>                         
              <%
                 int i = 0; 
                 String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                 String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                 String rowBgc = null;
              %>
              <nested:iterate property="cmBCList">
              <%
                 i++;
                 if((i % 2) != 0){
                     rowBgc = ROW_BGC1;
                 }else{
                     rowBgc = ROW_BGC2;
                 }  
              %>
              <tr bgcolor="<%= rowBgc %>">
                 <td width="100" height="1" class="gridLabel">
                    <nested:write property="bankCreditDate"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankCreditDocumentNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankCreditReferenceNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankCreditNumber"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankCreditType"/>
                 </td>
                 <td width="149" height="1" class="gridLabel">
                    <nested:write property="bankCreditPayee"/>
                 </td>                                                 
                 <td width="149" height="1" class="gridLabelNum">
                    <nested:write property="bankCreditAmount"/>
                    <nested:hidden property="bankCreditAmount"/>
                 </td>
                  <td width="50" height="1" class="gridLabelNum">
                    <nested:write property="posted"/>
                 </td>
                 <td width="100" height="1" class="gridLabel">
                    <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
                 </td>
                 <td width="70" align="center" height="1">
                    <nested:checkbox property="bankCreditMark" onclick="return markBankCredit(name);"/>
                 </td>
                 
              </tr>
              </nested:iterate>
              </logic:equal>
              <logic:equal name="cmBankReconciliationForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
              <%
                 int i = 0; 
                 String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                 String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                 String rowBgc = null;
              %>
              <nested:iterate property="cmBCList">
              <%
                 i++;
                 if((i % 2) != 0){
                     rowBgc = ROW_BGC1;
                 }else{
                     rowBgc = ROW_BGC2;
                 }  
              %>
              <tr bgcolor="<%= rowBgc %>">
                 <td width="175" height="1" class="gridHeader">
                    <bean:message key="bankReconciliation.prompt.bankCreditDate"/>
                 </td>
                 <td width="570" height="1" class="gridLabel" colspan="2">
                    <nested:write property="  "/>
                 </td>
                 <td width="149" align="center" height="1">
                    <nested:checkbox property="bankCreditMark" onclick="return markBankCredit(name);"/>
                 </td>
              </tr>
              <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankCreditNumber"/>
                       </td>
                       <td width="300" height="1" class="gridLabel">
                          <nested:write property="bankCreditNumber"/>
                       </td>
                 <td width="270" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankCreditPayee"/>
                       </td>
                       <td width="149" height="1" class="gridLabel">
                          <nested:write property="bankCreditPayee"/>
                       </td>
                    </tr>
                    <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankCreditType"/>
                       </td>
                       <td width="300" height="1" class="gridLabel">
                          <nested:write property="bankCreditType"/>
                       </td>
                 <td width="270" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.bankCreditAmount"/>                         
                       </td>
                       <td width="149" height="1" class="gridLabelNum">
                          <nested:write property="bankCreditAmount"/>
                          <nested:hidden property="bankCreditAmount"/>
                       </td>

                       <td width="270" height="1" class="gridHeader">
                          Posted                        
                       </td>
                       <td width="149" height="1" class="gridLabelNum">
                          <nested:write property="Posted"/>
                        
                       </td>
                    </tr>
                    <tr bgcolor="<%= rowBgc %>">
                       <td width="175" height="1" class="gridHeader">
                          <bean:message key="bankReconciliation.prompt.dateCleared"/>
                       </td>
                       <td width="300" height="1" class="gridLabel" colspan="3">
                          <nested:text property="dateCleared" size="10" maxlength="10" style="font-size:8pt;"/>
                       </td>
                    </tr>         
              </nested:iterate>             
              </logic:equal>
              </table>
                </div>
             </td>
             </tr> 
           </table>
           </div>


           <!-- End Bank Credit !-->


           <!-- Bank Debit !-->

           


           <!-- End Bank Debit !-->


		       <div class="tabbertab" title="Adjustment">
	           <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
			      <tr> 
			        <td class="prompt" width="575" height="25" colspan="4">
	                </td>
	              </tr>
	              <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.adjustmentDate"/>
			       </td>
			       <td width="415" height="25" class="control" colspan="3">
			          <html:text property="adjustmentDate" size="10" maxlength="10" styleClass="text"/>
			       </td>			       
		         </tr>
		         <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.adjustmentConversionDate"/>
			       </td>
			       <td width="128" height="25" class="control">
			          <html:text property="adjustmentConversionDate" size="10" maxlength="10" styleClass="text"/>
			       </td>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.adjustmentConversionRate"/>
			       </td>
			       <td width="127" height="25" class="control">
			          <html:text property="adjustmentConversionRate" size="10" maxlength="10" styleClass="text"/>
			       </td>
		         </tr>
		       </table>
		       </div>
		       <div class="tabbertab" title="Interest">
	           <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
			      <tr> 
			        <td class="prompt" width="575" height="25" colspan="4">
	                </td>
	              </tr>
	              <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.interestDate"/>
			       </td>
			       <td width="415" height="25" class="control" colspan="3">
			          <html:text property="interestDate" size="10" maxlength="10" styleClass="text"/>
			       </td>			       
		         </tr>
		         <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.interestEarned"/>
			       </td>
			       <td width="128" height="25" class="control">
			          <html:text property="interestEarned" size="25" maxlength="25" styleClass="textAmount" onblur="addZeroes(name); return enterInterestEarned();" onkeyup="formatAmount(name, (event)?event:window.event);"/>
			       </td>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.interestReferenceNumber"/>
			       </td>
			       <td width="127" height="25" class="control">
			          <html:text property="interestReferenceNumber" size="25" maxlength="25" styleClass="text"/>
			       </td>
		         </tr>
		         <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.interestConversionDate"/>
			       </td>
			       <td width="128" height="25" class="control">
			          <html:text property="interestConversionDate" size="10" maxlength="10" styleClass="text"/>
			       </td>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.interestConversionRate"/>
			       </td>
			       <td width="127" height="25" class="control">
			          <html:text property="interestConversionRate" size="10" maxlength="10" styleClass="text"/>
			       </td>
		         </tr>
		       </table>
		       </div>
		       <div class="tabbertab" title="Charge">
	           <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
			      <tr> 
			        <td class="prompt" width="575" height="25" colspan="4">
	                </td>
	              </tr>
	              <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.serviceChargeDate"/>
			       </td>
			       <td width="415" height="25" class="control" colspan="3">
			          <html:text property="serviceChargeDate" size="10" maxlength="10" styleClass="text"/>
			       </td>			       
		         </tr>
		         <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.serviceCharge"/>
			       </td>
			       <td width="128" height="25" class="control">
			          <html:text property="serviceCharge" size="25" maxlength="25" styleClass="textAmount" onblur="addZeroes(name); return enterServiceCharge();" onkeyup="formatAmount(name, (event)?event:window.event);"/>
			       </td>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.serviceChargeReferenceNumber"/>
			       </td>
			       <td width="127" height="25" class="control">
			          <html:text property="serviceChargeReferenceNumber" size="25" maxlength="25" styleClass="text"/>
			       </td>
		         </tr>
		         <tr>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.serviceChargeConversionDate"/>
			       </td>
			       <td width="128" height="25" class="control">
			          <html:text property="serviceChargeConversionDate" size="10" maxlength="10" styleClass="text"/>
			       </td>
			       <td class="prompt" width="160" height="25">
			          <bean:message key="bankReconciliation.prompt.serviceChargeConversionRate"/>
			       </td>
			       <td width="127" height="25" class="control">
			          <html:text property="serviceChargeConversionRate" size="10" maxlength="10" styleClass="text"/>
			       </td>
		         </tr>
		       </table>
		       </div>
		       </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
	       </tr>     
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		      </td>
	       </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["bankAccount"] != null)
        document.forms[0].elements["bankAccount"].focus()
	       // -->
</script>
<script language=JavaScript type=text/javascript>
  <!--
     // initialize originalInterestEarned and original serviceCharge
     this.originalInterestEarned = 0.00;
     this.originalServiceCharge = 0.00;
	       // -->
</script>

</body>
</html>
