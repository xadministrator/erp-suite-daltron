<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="fundTransferEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitSelect(field1,field2)
{           
      enableInputControls();
      enterSelect(field1, field2);
}

function submitButton()
{     
      disableButtons();
      enableInputControls();      
}

function calculateAmount(name)
{
   
   var property = name.substring(0,name.indexOf("."));   
   var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
   var amountDeposited = 0;
      
   if (!isNaN(parseFloat(document.forms[0].elements[property + ".amountDeposited"].value))) {
  
      	amountDeposited = (document.forms[0].elements[property + ".amountDeposited"].value).replace(/,/g,'');
      	
   }
     
   if (document.forms[0].elements[property + ".depositMark"].checked == true) {
      
         amount = (1 * amount + 1 * amountDeposited).toFixed(2);
          
   } else {
      
         amount = (1 * amount - 1 * amountDeposited).toFixed(2);
      
   }
   
   document.forms[0].elements["amount"].value = formatDisabledAmount(amount.toString());
                 
}

var origAmt;
function setOrigAmt(name)
{
   
   var property = name.substring(0,name.indexOf("."));   
   var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
   var amountDeposited = 0;
   
   if (!isNaN(parseFloat(document.forms[0].elements[property + ".amountDeposited"].value))) {
  
      	amountDeposited = (document.forms[0].elements[property + ".amountDeposited"].value).replace(/,/g,'');
      	
   }
   origAmt = amountDeposited;
   
}

function calculateAmountOnKeyUp(name)
{
   
   var property = name.substring(0,name.indexOf("."));   
   var amount = (document.forms[0].elements["amount"].value).replace(/,/g,'');;      
                    
   var amountDeposited = 0;
      
   if (!isNaN(parseFloat(document.forms[0].elements[property + ".amountDeposited"].value))) {
  
      	amountDeposited = (document.forms[0].elements[property + ".amountDeposited"].value).replace(/,/g,'');
      	
   }
     
   if (document.forms[0].elements[property + ".depositMark"].checked == true) {
         
         amount = (1 * amount - 1 * origAmt).toFixed(2);
         amount = (1 * amount + 1 * amountDeposited).toFixed(2);
          
   } 
   
   document.forms[0].elements["amount"].value = formatDisabledAmount(amount.toString());
                 
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/cmFundTransferEntry.do" onsubmit="return submitButton();">
   <%@ include file="cmnHeader.jsp" %> 
   <%@ include file="cmnSidebar.jsp" %>
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="310">
      <tr valign="top">
         <td width="187" height="310"></td> 
         <td width="581" height="310">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="310" 
                                      bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="fundTransferEntry.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="cmFundTransferEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>
		       <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   	   </html:messages>		
	           </td>
	        </tr>
	        <html:hidden property="isBankAccountFromEntered"/>
	        <html:hidden property="isBankAccountToEntered"/>
			<html:hidden property="isTypeEntered"/>
			<html:hidden property="isConversionDateEntered" value=""/>
			<logic:equal name="cmFundTransferEntryForm" property="enableFields" value="true">	         
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountFrom"/>
						</td>
						<td width="158" height="25" class="control" >
				   			<html:select property="bankAccountFrom" styleClass="comboRequired" style="width:130;" onchange="return submitSelect('bankAccountFrom','isBankAccountFromEntered');">
				      		<html:options property="bankAccountFromList"/>
				   			</html:select>
						</td>
						<logic:equal name="cmFundTransferEntryForm" property="bankAccountFromIsCash" value="true">
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountType"/>
						</td>
						<td width="157" height="25" class="control" colspan="3">
				   			<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>
						</logic:equal>		 
            		</tr> 
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountTo"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccountTo" styleClass="comboRequired" style="width:130;" onchange="return submitSelect('bankAccountTo','isBankAccountToEntered');">
				      		<html:options property="bankAccountToList"/>
				   			</html:select>
						</td>	
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.date"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
		       			</td>
            		</tr>	 
					<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
		       			</td>	 
			   			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                 			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
		       			</td>
            		</tr>
                  	<logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_TRANSFER%>">
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.amount"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
		       			</td>
            		</tr>
					<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="fundTransferEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text"/>
		       			</td>		 		 
            		</tr>
					</logic:equal>
					<logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT%>">
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.amount"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" readonly="true"/>
		       			</td>
            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="fundTransferEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text"/>
		       			</td>		 		 
            		</tr>
					<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.receiptDateFrom"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="receiptDateFrom" size="10" maxlength="10" styleClass="text"/>
		       			</td>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.receiptDateTo"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="receiptDateTo" size="10" maxlength="10" styleClass="text"/>
		       			</td>
            		</tr>
            		<tr>
            			<td class="prompt" width="130" height="25">
	          				<bean:message key="fundTransferEntry.prompt.maxRows"/>
	       				</td>
	       				<td width="157" height="25" class="control">
	          				<html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
	       				</td>
	       				<td class="prompt" width="130" height="25">
	            			<bean:message key="fundTransferEntry.prompt.orderBy"/>
	         			</td>
	         			<td width="157" height="25" class="control">
	            			<html:select property="orderBy" styleClass="combo">
			      			<html:options property="orderByList"/>
			   				</html:select>
	         			</td>
            		</tr>
					</logic:equal>
				</table>
				</div>
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
					<tr>
						<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>
					</tr>
				</table>
				</div>
				<div class="tabbertab" title="Currency">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
		             <tr>
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyFrom"/>
		               </td>
				       <td width="148" height="25" class="control">
		                  <html:text property="currencyFrom" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyTo"/>
		               </td>
				       <td width="147" height="25" class="control">
		                  <html:text property="currencyTo" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
		            </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			           <bean:message key="fundTransferEntry.prompt.conversionDate"/>
			        </td>
					    <td width="147" height="25" class="control">
			           <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateFrom"/>
			        	</td>
					    <td width="147" height="25" class="control">
					       <html:text property="conversionRateFrom" size="10" maxlength="10" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateTo"/>
			    	    </td>
					    <td width="148" height="25" class="control">
					       <html:text property="conversionRateTo" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				  <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>					         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>           	                             
	     </logic:equal>	

	     <logic:equal name="cmFundTransferEntryForm" property="enableFields" value="false">          
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountFrom"/>
						</td>
						<td width="158" height="25" class="control" >
				   			<html:select property="bankAccountFrom" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountFromList"/>
				   			</html:select>
						</td>
						<logic:equal name="cmFundTransferEntryForm" property="bankAccountFromIsCash" value="true">
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountType"/>
						</td>
						<td width="157" height="25" class="control" colspan="3">
				   			<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>
						</logic:equal>		 
            		</tr> 
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountTo"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccountTo" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountToList"/>
				   			</html:select>
						</td>	
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.date"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
		       			</td>
            		</tr>	 
					<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>	 
			   			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                 			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>
            		</tr>
                  	<logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_TRANSFER%>">
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.amount"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		       			</td>
            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="fundTransferEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       			</td>		 		 
            		</tr>
                  	</logic:equal>
					<logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT%>">
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.amount"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		       			</td>
            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="fundTransferEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       			</td>		 		 
            		</tr>
					<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.receiptDateFrom"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="receiptDateFrom" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       			</td>
						<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.receiptDateTo"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="receiptDateTo" size="10" maxlength="10" styleClass="text" disabled="true"/>
		       			</td>
            		</tr>
            		<tr>
            			<td class="prompt" width="130" height="25">
	          				<bean:message key="fundTransferEntry.prompt.maxRows"/>
	       				</td>
	       				<td width="157" height="25" class="control">
	          				<html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired" disabled="true"/>
	       				</td>
	       				<td class="prompt" width="130" height="25">
	            			<bean:message key="fundTransferEntry.prompt.orderBy"/>
	         			</td>
	         			<td width="157" height="25" class="control">
	            			<html:select property="orderBy" styleClass="combo" disabled="true">
			      			<html:options property="orderByList"/>
			   				</html:select>
	         			</td>
            		</tr>
					</logic:equal>               		
				</table>
				</div>
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
					<tr>
						<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>
					</tr>
				</table>
				</div>
				<div class="tabbertab" title="Currency">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
		             <tr>
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyFrom"/>
		               </td>
				       <td width="148" height="25" class="control">
		                  <html:text property="currencyFrom" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyTo"/>
		               </td>
				       <td width="147" height="25" class="control">
		                  <html:text property="currencyTo" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
		            </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			           <bean:message key="fundTransferEntry.prompt.conversionDate"/>
			        </td>
					    <td width="147" height="25" class="control">
			           <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateFrom"/>
			        	</td>
					    <td width="147" height="25" class="control">
					       <html:text property="conversionRateFrom" size="10" maxlength="10" styleClass="text" disabled="true"/>
					    </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateTo"/>
			    	    </td>
					    <td width="148" height="25" class="control">
					       <html:text property="conversionRateTo" size="10" maxlength="10" styleClass="text" disabled="true"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				  <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>					         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     </logic:equal>                        
		     <tr>				
		        <td width="575" height="30" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
			       <logic:equal name="cmFundTransferEntryForm" property="showSaveButton" value="true">
		           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
			            <bean:message key="button.saveSubmit"/>
			       </html:submit>
		           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
			            <bean:message key="button.saveAsDraft"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="cmFundTransferEntryForm" property="showDeleteButton" value="true">
			       <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                    <bean:message key="button.delete"/>
	               </html:submit>
	               </logic:equal>
			       <logic:equal name="cmFundTransferEntryForm" property="showJournalButton" value="true">
		           <html:submit property="journalButton" styleClass="mainButton">
		               <bean:message key="button.journal"/>
		           </html:submit>
		           </logic:equal>
				   <html:submit property="closeButton" styleClass="mainButton">
		              <bean:message key="button.close"/>
		           </html:submit>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="cmFundTransferEntryForm" property="showSaveButton" value="true">
		           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.saveSubmit"/>
			       </html:submit>
		           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.saveAsDraft"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="cmFundTransferEntryForm" property="showDeleteButton" value="true">
			       <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                    <bean:message key="button.delete"/>
	               </html:submit>
	               </logic:equal>
			       <logic:equal name="cmFundTransferEntryForm" property="showJournalButton" value="true">
		           <html:submit property="journalButton" styleClass="mainButton" disabled="true">
		               <bean:message key="button.journal"/>
		           </html:submit>
		           </logic:equal>
				   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		              <bean:message key="button.close"/>
		           </html:submit>
			       </div>
	            </td>
		     </tr>
			 <logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT%>"> 
			 <tr>
				<td width="575" height="30" colspan="4">
					<div id="buttons">
						 <logic:equal name="cmFundTransferEntryForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <html:submit property="goButton" styleClass="mainButtonSmall">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </div>
					<div id="buttonsDisabled" style="display: none;">
						<logic:equal name="cmFundTransferEntryForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmFundTransferEntryForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <html:submit property="goButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
						 </div>
					</td>
			</tr>
			<tr>
				<td width="575"> 
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.gridTitle.FTDetails"/>
		                </td>
		            </tr>
				    <tr>
	                	<td height="1" width="100" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.receiptNumber"/>
		                </td>
	                	<td height="1" width="100" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.receiptDate"/>
		                </td>
	                	<td height="1" width="125" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.totalAmount"/>
		                </td>
	                	<td height="1" width="125" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.amountDeposited"/>
		                </td>
						
	                	<td height="1" width="50" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.deposit"/>
		                </td>
		            </tr>

				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
					<bean:define id="actionForm" name="cmFundTransferEntryForm" type="com.struts.cm.fundtransferentry.CmFundTransferEntryForm"/>
				    <nested:iterate property="cmFTList">
				    <%
				       i++;

 					   if (i-1 >= actionForm.getLineCount() && i-1 <= actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()) - 1) {

				   	   	if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       	}else{
				           rowBgc = ROW_BGC2;
				       	}  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				    	<td width="100" height="1" class="control">
				          <nested:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
				       	</td>
				    	<td width="100" height="1" class="control">
				          <nested:text property="receiptDate" size="15" maxlength="25" styleClass="text" disabled="true"/>
				       	</td>
				    	<td width="125" height="1" class="control">
				          <nested:text property="totalAmount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
				       	</td>
						<logic:equal name="cmFundTransferEntryForm" property="enableFields" value="true">          
				    	<td width="125" height="1" class="control">
							<nested:text property="amountDeposited" size="15" maxlength="25" styleClass="textAmount" onfocus="setOrigAmt(name);" onblur="calculateAmountOnKeyUp(name);"/>
				       	</td>
						<td width="100" align="center" height="1" class="control">
		                	<nested:checkbox property="depositMark" onclick="calculateAmount(name);"/>
				       	</td>
						</logic:equal>
						<logic:equal name="cmFundTransferEntryForm" property="enableFields" value="false">          
						<td width="150" height="1" class="control">
							<nested:text property="amountDeposited" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>
				    	<td width="100" align="center" height="1" class="control">
		                	<nested:checkbox property="depositMark"  disabled="true"/>
				       	</td>
						</logic:equal>
				   	</tr>
					<% } %>
					</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
			</logic:equal>
			<tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["bankAccountFrom"] != null)
        document.forms[0].elements["bankAccountFrom"].focus();

	       // -->
</script>
</body>
</html>
