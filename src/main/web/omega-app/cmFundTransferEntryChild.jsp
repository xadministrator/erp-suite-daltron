<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="fundTransferEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/cmFundTransferEntry.do" onsubmit="return submitForm();">
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="310">
      <tr valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="310" 
                                      bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="fundTransferEntry.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="cmFundTransferEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>
		       <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   	   </html:messages>		
	           </td>
	        </tr>
	        <html:hidden property="isBankAccountFromEntered"/>
	        <html:hidden property="isBankAccountToEntered"/>	         
	       <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountFrom"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccountFrom" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountFromList"/>
				   			</html:select>
						</td>		 
						<logic:equal name="cmFundTransferEntryForm" property="bankAccountFromIsCash" value="true">
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountType"/>
						</td>
						<td width="157" height="25" class="control" colspan="3">
				   			<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
				      		<html:options property="typeList"/>
				   			</html:select>
						</td>
						</logic:equal>		 
            		</tr>
	        		<tr>
						<td class="prompt" width="130" height="25">
				   			<bean:message key="fundTransferEntry.prompt.bankAccountTo"/>
						</td>
						<td width="158" height="25" class="control">
				   			<html:select property="bankAccountTo" styleClass="comboRequired" style="width:130;" disabled="true">
				      		<html:options property="bankAccountToList"/>
				   			</html:select>
						</td>	
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.date"/>
               			</td>
		       			<td width="157" height="25" class="control">
                  			<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
		       			</td>
            		</tr>	 
					<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.referenceNumber"/>
               			</td>
		       			<td width="158" height="25" class="control">
                  			<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>	 
			   			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.documentNumber"/>
               			</td>
		       			<td width="157" height="25" class="control">
                 			<html:text property="documentNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
		       			</td>
            		</tr>
	        		<tr>
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.amount"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="amount" size="15" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		       			</td>
            		</tr>
            		<tr>
						<td class="prompt" width="130" height="25">
				  	 		<bean:message key="fundTransferEntry.prompt.memo"/>
						</td>
						<td width="445" height="25" class="control" colspan="3">
                  			<html:text property="memo" size="50" maxlength="50" styleClass="text" disabled="true"/>
		       			</td>		 		 
            		</tr>
				</table>
				</div>
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
					<tr>
						<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="true">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid"/>
		       			</td>
		       			</logic:equal>
		       			<logic:equal name="cmFundTransferEntryForm" property="enableFundTransferVoid" value="false">
	           			<td class="prompt" width="130" height="25">
                  			<bean:message key="fundTransferEntry.prompt.fundTransferVoid"/>
               			</td>
		       			<td width="445" height="25" class="control" colspan="3">
                  			<html:checkbox property="fundTransferVoid" disabled="true"/>
		       			</td>
		       			</logic:equal>
					</tr>
				</table>
				</div>
				<div class="tabbertab" title="Currency">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
		             <tr>
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyFrom"/>
		               </td>
				       <td width="148" height="25" class="control">
		                  <html:text property="currencyFrom" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
			           <td class="prompt" width="140" height="25">
		                  <bean:message key="fundTransferEntry.prompt.currencyTo"/>
		               </td>
				       <td width="147" height="25" class="control">
		                  <html:text property="currencyTo" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>		 
		            </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			           <bean:message key="fundTransferEntry.prompt.conversionDate"/>
			        </td>
					    <td width="147" height="25" class="control">
			           <html:text property="conversionDate" size="10" maxlength="10" styleClass="text" onblur="return enterSelect('conversionDate','isConversionDateEntered');"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateFrom"/>
			        	</td>
					    <td width="147" height="25" class="control">
					       <html:text property="conversionRateFrom" size="10" maxlength="10" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="fundTransferEntry.prompt.conversionRateTo"/>
			    	    </td>
					    <td width="148" height="25" class="control">
					       <html:text property="conversionRateTo" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				  <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>					         			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="fundTransferEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>           	                                                     
		     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	       </tr>
			<logic:equal name="cmFundTransferEntryForm" property="type" value="<%=Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT%>">
			<tr>
				<td width="575"> 
			      <div align="center">
			      <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.gridTitle.FTDetails"/>
		                </td>
		            </tr>
				    <tr>
	                	<td height="1" width="100" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.receiptNumber"/>
		                </td>
	                	<td height="1" width="150" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.totalAmount"/>
		                </td>
	                	<td height="1" width="150" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.amountDeposited"/>
		                </td>
	                	<td height="1" width="50" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="fundTransferEntry.prompt.deposit"/>
		                </td>
		            </tr>

				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="cmFTList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">		
				    	<td width="100" height="1" class="control">
				          <nested:text property="receiptNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
				       	</td>
				    	<td width="150" height="1" class="control">
				          <nested:text property="totalAmount" size="10" maxlength="25" styleClass="text" disabled="true"/>
				       	</td>
						<logic:equal name="cmFundTransferEntryForm" property="enableFields" value="true">          
				    	<td width="150" height="1" class="control">
							<nested:text property="amountDeposited" size="5" maxlength="10" styleClass="text"/>
				       	</td>
				    	<td width="100" height="1" class="control">
		                	<nested:checkbox property="depositMark"/>
				       	</td>
						</logic:equal>
						<logic:equal name="cmFundTransferEntryForm" property="enableFields" value="false">          
						<td width="150" height="1" class="control">
							<nested:text property="amountDeposited" size="5" maxlength="10" styleClass="text" disabled="true"/>
						</td>
				    	<td width="100" height="1" class="control">
		                	<nested:checkbox property="depositMark" disabled="true"/>
				       	</td>
						</logic:equal>
				   	</tr>
				  	</nested:iterate>
				  </table>
			      </div>
				</td>
			</tr>
			</logic:equal>
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
</body>
</html>
