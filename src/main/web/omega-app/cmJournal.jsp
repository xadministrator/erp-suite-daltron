<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="cmJournal.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 

var tempAmount = 0;

function initializeTempDebit(name)
{
            
   /*  if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
        tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');               
    } else {      
        tempAmount = 0;    
    }       */   
}   

function calculateTotalDebit(name,size)
{       
	var i =0;
	var totalDebit = 0;

	for(i=0; i<size.value; i++) {
		  if (isNaN(parseFloat(document.forms[0].elements["cmJRList[" + i + "].debitAmount"].value))) {
	        
	        continue;
	    
	        }
		      
			  var in_amount = (document.forms[0].elements["cmJRList[" + i + "].debitAmount"].value).replace(/,/g,'');

			  totalDebit += parseFloat(in_amount);

		 
	}
	document.forms[0].elements["totalDebit"].value = formatDisabledAmount(totalDebit.toFixed(2).toString());
            
}  
   
function initializeTempCredit(name)
{              
   /*  if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {
        tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');               
    } else {      
        tempAmount = 0;      
    }    */         
}  
  
function calculateTotalCredit(name,size)
{

	var i =0;
	var totalCredit = 0;

	for(i=0; i<size.value; i++) {
		  if (isNaN(parseFloat(document.forms[0].elements["cmJRList[" + i + "].creditAmount"].value))) {
	        
	        continue;
	    
	        }
		      
			  var in_amount = (document.forms[0].elements["cmJRList[" + i + "].creditAmount"].value).replace(/,/g,'');

			  totalCredit += parseFloat(in_amount);

		 
	}
	document.forms[0].elements["totalCredit"].value = formatDisabledAmount(totalCredit.toFixed(2).toString());

}


</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onload="alert('HI');" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/cmJournal.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="cmJournal.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="cmJournalForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="cmJournal.prompt.transaction"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:text property="transaction" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="cmJournal.prompt.date"/>
	        </td>
	        <td width="147" height="25" class="control">
			   <html:text property="date" size="10" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>   
         <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="cmJournal.prompt.referenceNumber"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
	           <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>
         <tr>
         	<td class="prompt" width="140" height="25">
         		<bean:message key="cmJournal.prompt.totalDebit"/>
         	</td>
         	<td width="148" height="25" class="control">
         		<html:text property="totalDebit" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
        	</td>
         	<td class="prompt" width="140" height="25">
         		<bean:message key="cmJournal.prompt.totalCredit"/>
         	</td>
         	<td width="148" height="25" class="control">
         		<html:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
         	</td>
         </tr>  
	     <tr>
	       <td width="575" height="50" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		    <div id="buttonsDisabled" style="display: none;">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		   <td>
         </tr>	     
	     <tr>
            <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
            </td>
         </tr>
	     <tr>
	        <td width="575" height="5" colspan="4">
		    </td>
	     </tr>
	     <tr>    	       	      	     
	        <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="cmJournalForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="cmJournalForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       </div>
		    </td>
	     </tr>
         <logic:equal name="cmJournalForm" property="enableFields" value="true">
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="cmJournal.gridTitle.JRDetails"/>
	                       </td>
	                    </tr>	                    
                 <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.accountNumber"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.description"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <html:hidden property="cmJRListSize"/>
				    <nested:iterate property="cmJRList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
					<nested:notEqual property="drClass" value="CASH">
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="370" height="1" class="control">
				          <nested:text property="accountNumber" size="23" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'accountNumber', 'description');"/>
				       </td>
				       <td width="145" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name,cmJRListSize); addZeroes(name);" onkeyup="calculateTotalDebit(name,cmJRListSize); " onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name,cmJRListSize); addZeroes(name);" onkeyup="calculateTotalCredit(name,cmJRListSize); " onfocus="initializeTempCredit(name);"/>
				       </td>
				       
				      <%--  <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name); addZeroes(name);" onkeyup="calculateTotalDebit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempCredit(name);"/>
				       </td>
				        --%>
				       			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
					</nested:notEqual>
					 <nested:equal property="drClass" value="CASH">
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="370" height="1" class="control">
				          <nested:text property="accountNumber" size="23" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'accountNumber', 'description');"/>
				       </td>
				       <td width="145" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				    </nested:equal>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
         </logic:equal>
         <logic:equal name="cmJournalForm" property="enableFields" value="false">
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="cmJournal.gridTitle.JRDetails"/>
	                       </td>
	                    </tr>	                    
                 <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.lineNumber"/>
				       </td>
				       <td width="385" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.accountNumber"/>
				       </td>
				       <td width="130" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.drClass"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.debitAmount"/>
				       </td>
				       <td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>				       				       
				       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="cmJournal.prompt.description"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    
				    <nested:iterate property="cmJRList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">						       	       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="370" height="1" class="control">
				          <nested:text property="accountNumber" size="23" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="145" height="1" class="control">
				          <nested:select property="drClass" styleClass="combo" disabled="true">
				              <nested:options property="drClassList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="debitAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="140" height="1" class="control">
				          <nested:text property="creditAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">				       			       
				       <td width="894" height="1" class="control" colspan="6">
				          <nested:text property="description" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
         </logic:equal>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="cmJournalForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="cmJournalForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="cmJournalForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="cmJournalForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	           </tr>	
			 </table>
		    </div>
		  </td>
	     </tr>
	     <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["accountNumber"] != null &&
        document.forms[0].elements["accountNumber"].disabled == false)
        document.forms[0].elements["accountNumber"].focus()
	       // -->
</script>
</body>
</html>
