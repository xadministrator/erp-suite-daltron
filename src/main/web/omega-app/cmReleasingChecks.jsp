<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="releasingChecks.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmRelease()
{
   if(confirm("Are you sure you want to Release selected checks?")) return true;
      else return false;
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["cmRCList[" + i + "].release"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["cmRCList[" + i + "].release"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/cmReleasingChecks.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="releasingChecks.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="cmReleasingChecksForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="cmReleasingChecksForm" property="showBatchName" value="true">
	     <tr>
            <td width="160" height="25" class="prompt">
               <bean:message key="releasingChecks.prompt.batchName"/>
            </td>
            <td width="415" height="25" class="control" colspan="3">
               <html:select property="batchName" styleClass="combo">
                   <html:options property="batchNameList"/>
               </html:select>
            </td>
         </tr>
         </logic:equal>	
	     <tr>
	     <logic:equal name="cmReleasingChecksForm" property="useSupplierPulldown" value="true">
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.supplierCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="supplierCode" styleClass="combo" style="width:100;">
			      <html:options property="supplierCodeList"/>
			   </html:select>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>	         
         </logic:equal>	
	     <logic:equal name="cmReleasingChecksForm" property="useSupplierPulldown" value="false">
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.supplierCode"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	           <html:text property="supplierCode" size="15" maxlength="15" styleClass="text" readonly="true"/>
			   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplierCode', '', '');"/>
	         </td>	    
         </logic:equal>	
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.bankAccount"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="bankAccount" styleClass="combo">
			      <html:options property="bankAccountList"/>
			   </html:select>
	         </td>
         </tr>          
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	         </td>
         </tr> 
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.numberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="numberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.numberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="numberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.documentNumberFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.documentNumberTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
	         </td>
         </tr>                                  
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.currency"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="currency" styleClass="combo">
			      <html:options property="currencyList"/>
			   </html:select>
	         </td>         
         </tr>                                  
         <tr>	            
	         <td class="prompt" width="140" height="25">
	            <bean:message key="releasingChecks.prompt.orderBy"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	              
         </tr>
         <tr>
	       <td class="prompt" width="140" height="25">
	          <bean:message key="releasingChecks.prompt.maxRows"/>
	       </td>
	       <td width="147" height="25" class="control">
	          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
	       </td>
	       <td class="prompt" width="140" height="25">
	          <bean:message key="releasingChecks.prompt.queryCount"/>
	       </td>
	       <td width="147" height="25" class="control">
	          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
	       </td>
         </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="cmReleasingChecksForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="cmReleasingChecksForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="cmReleasingChecksForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="releaseButton" styleClass="mainButton" onclick="return confirmRelease();">
				         <bean:message key="button.release"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>				         
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="releaseButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.release"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="releasingChecks.gridTitle.RCDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.date"/>
			       </td>
			       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.supplierCode"/>
			       </td>
   			       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.amount"/>
			       </td>
			       <td width="65" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.checkNumber"/>
			       </td> 
					<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.chkCheckDate"/>
			       </td>			       
					<td width="80" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.dateReleased"/>
			       </td>
			    </tr>
			    <tr> 			      
			       <td width="160" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="releasingChecks.prompt.remarks"/>
			       </td>			      
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="cmRCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="120" height="1" class="gridLabel">
			          <nested:write property="supplierName"/>
			       </td>
			       <td width="159" height="1" class="gridLabelNum">
			          <nested:write property="amount"/>
			       </td>					       
			       <td width="125" height="1" class="gridLabel">
			          <nested:text property="checkNumber" size="10" maxlength="10" styleClass="text"/>
			       </td>      			       			       			       
			       <td width="110" height="1" class="gridLabel">
			          <nested:text property="chkCheckDate" size="10" maxlength="10" styleClass="text"/>		 			       			       
			       </td>       
			       <td width="90" height="1" class="gridLabel">
			          <nested:text property="dateReleased" size="10" maxlength="10" styleClass="text"/>		 			       			       
			       </td>			       
		           <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
		             <td width="70" align="center" height="1">
		                <nested:checkbox property="release"/>
		             </td>
		           </logic:equal>
		           <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
		             <td width="70" align="center" height="1">
		                <nested:checkbox property="release" disabled="true"/>
		             </td>
		           </logic:equal>
			    </tr>
			    <tr bgcolor=",%= rowBgc %>">
			    	<td width="160" height="1" colspan="6" class="gridLabel1">
			    		<nested:text property="remarks" size="100" maxlength="255" styleClass="text"/>		 			       			       
			    	</td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="cmReleasingChecksForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="cmRCList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.date"/>
			      </td>
			      <td width="100" height="1" class="gridLabel">
			         <nested:write property="date"/>
			      </td>
			      <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.supplierCode"/>
			      </td>
			      <td width="150" height="1" class="gridLabel" colspan="2">
			         <nested:write property="supplierName"/>
			      </td>
			      <td width="50" align="right" height="1">
			         <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          		             
		                <nested:checkbox property="release"/>
		             </logic:equal>
	    	         <logic:equal name="cmReleasingChecksForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          		             
		    	        <nested:checkbox property="release" disabled="true"/>
	          		 </logic:equal>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
				  <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.checkNumber"/>
			      </td>
			      <td width="100" height="1" class="gridLabel">
			         <nested:write property="checkNumber"/>
			      </td>
			      <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.documentNumber"/>
			      </td>
			      <td width="100" height="1" class="gridLabel">
			         <nested:write property="documentNumber"/>
			      </td>
			      <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.amount"/>
			      </td>
			      <td width="50" height="1" class="gridLabelNum">
			         <nested:write property="amount"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.referenceNumber"/>
			      </td>
			      <td width="100" height="1" class="gridLabel">
			         <nested:write property="referenceNumber"/>
				  </td>
			      <td width="50" height="1" class="gridHeader">
			         <bean:message key="releasingChecks.prompt.description"/>
			      </td>
			      <td width="100" height="1" class="gridLabel"  colspan="4">
			         <nested:write property="description"/>
			      </td>
			    </tr>
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["supplierCode"] != null &&
        document.forms[0].elements["supplierCode"].disabled == false)
        document.forms[0].elements["supplierCode"].focus()
   // -->
</script>
</body>
</html>
