<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="cmRepDailyCashForecastDetail.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/cmRepDailyCashForecastDetail.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="cmRepDailyCashForecastDetail.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>

		 <tr>
		 <td width="575" height="10" colspan="4">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="cmRepDailyCashForecastDetail.prompt.bankAccount"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="selectedBankAccount" multiple="true" styleClass="comboRequired">
			      <html:options property="bankAccountList"/>
			   </html:select>
	         </td>	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="cmRepDailyCashForecastDetail.prompt.date"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
         </tr>
		 <td width="187" height="5"></td>         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="cmRepDailyCashForecastDetail.prompt.reportType"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="reportType" styleClass="comboRequired">
			      <html:options property="reportTypeList"/>
			   </html:select>
	         </td>
    	  	 <td class="prompt" width="140" height="25">
	            <bean:message key="cmRepDailyCashForecastDetail.prompt.viewType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="cmRepDailyCashForecastDetail.prompt.includedUnposted"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includedUnposted"/>
	         </td>
         </tr>
		 </table> 
         </tr>
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<logic:equal name="cmRepDailyCashForecastDetailForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
