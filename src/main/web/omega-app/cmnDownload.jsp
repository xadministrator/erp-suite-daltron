<%@ page errorPage="cmnErrorPage.jsp" %>
<%@ page import="com.struts.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Pragma", "public");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", 0);


 	//File filePDF = (File)session.getAttribute(Constants.PDF_REPORT_KEY);

   	byte[] bytes = (byte[])session.getAttribute(Constants.DOWNLOAD_REPORT_KEY);
   	String filename = (String)session.getAttribute("DownloadFileName");
   	response.setContentType("text/csv");
	response.setContentLength(bytes.length);
	response.setHeader("Content-Disposition","inline;filename=\""+ filename + "\"");
	ServletOutputStream outputStream = response.getOutputStream();
	outputStream.write(bytes, 0, bytes.length);
	outputStream.flush();
	outputStream.close();

%>