<%@ page errorPage="cmnErrorPage.jsp" %>
<%@ page import="com.struts.util.Constants" %>
<%@ page import="com.struts.util.Image" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Pragma", "public");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", 0);
    
    Image image = (Image)session.getAttribute(Constants.IMAGE_KEY);
    
	byte[] bytes = image.getBytes();
	response.setContentLength(bytes.length);	
	ServletOutputStream outputStream = response.getOutputStream();
	outputStream.write(bytes, 0, bytes.length);
	outputStream.flush();
	outputStream.close();

%>