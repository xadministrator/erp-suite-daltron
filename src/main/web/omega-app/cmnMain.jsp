<%@ page language="java" import="com.struts.util.Constants"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
	<logic:forward name="adLogon" />
</logic:notPresent>
<jsp:useBean id="user" scope="session" class="com.struts.util.User" />
<html>
<head>
<title><%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%>
- <%=user.getUserName()%> - <bean:message key="main.title" /></title>
<link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1"
	type="text/css">
<style type="text/css">
a.linkMain:link {
color: #000000;
text-decoration: none;
}

a.linkMain:visited {
color: #000000;
text-decoration: none;
}

a.linkMain:hover {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:link {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:visited {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:hover {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}
</style>
<script>
function confirmPost()
{
   if(confirm("Are you sure you want to Generate Interest")) return true;
      else return false;
}

function confirmPenalty()
{
   if(confirm("Are you sure you want to Generate Penalty")) return true;
      else return false;
}

function confirmAccruedInterestIS()
{
   if(confirm("Are you sure you want to Generate Accrued Interest for IS")) return true;
      else return false;
}

function confirmAccruedInterestTB()
{
   if(confirm("Are you sure you want to Generate Accrued Interest for TB")) return true;
      else return false;
}

function confirmGenerateLoan()
{
   if(confirm("Are you sure you want to Generate Loan")) return true;
      else return false;
}

</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"
	marginwidth="0" marginheight="0">
	<div style="margin:0 auto; width:1300px;">
	<html:form action="/adMain.do" >
	<%@ include file="cmnHeader.jsp"%>
	<%@ include file="cmnSidebar.jsp"%>
	<table border="0" cellpadding="0" cellspacing="0" width="768"
		height="510">
		<tr valign='top'>
			<td width="187" height="510"></td>
			<td width="581" height="510">
			<table border="0" cellpadding="0" cellspacing="0" width="585"
				height="24">
				<tr>
					<td width="291" height="10" class="statusBar"><html:errors />
					</td>
					<td width="290" height="10" class="statusBar">
					</td>
				</tr>
				<tr>
					<td width="581" height="40" class="mainWelcome" colspan="2"><%=user.getWelcomeNote()%>
					</td>
				</tr>
				<tr>
					<td width="291" height="35" class="userWelcome" colspan="2">
						<bean:message key="main.prompt.userWelcome"/>
						<bean:write name="adMainForm" property="userDescription" />
						<bean:message key="main.prompt.today"/>
						<bean:write name="adMainForm" property="today" />
					</td>
				</tr>
				<tr>
					<% if (user.getCurrentAppCode() == 1) { %>
						<td width="291" height="1">
							<p align="left">
							<html:image src="images/actionCenter.gif" disabled="true"/>
						</td>
					<% } else { %>
						<td width="291" height="1">
							<p align="left">
							<html:image src="images/actionCenter.gif" disabled="true"/>
						</td>
					<% } %>
					<% if (user.getCurrentAppCode() == 4) { %>
						<td width="290" height="1">
							<p align="left">
							<html:image src="images/reminderTitle.gif" disabled="true"/>
						</td>
					<% } else { %>
						<td width="290" height="1">
							<p align="left">
							<html:image src="images/reminderTitle.gif" disabled="true"/>
						</td>
					<% } %>
				</tr>
				<tr valign="top">
					<td width="291" height="120" class="moduleTitle">
						<p align="left">
						<% if (user.getUserApps().contains("OMEGA GENERAL LEDGER")) { %>
							<html:image src="images/gl.gif"
								onmouseover="this.src='images/gl2.gif'"
								onmouseout="this.src='images/gl.gif'"
								onclick="window.location='adChangeApp.do?appCode=1';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.generalLedger" />
						<html:image src="images/mainLine.gif"/>
						<% if (user.getUserApps().contains("OMEGA PAYABLES")) { %>
							<html:image src="images/ap.gif"
								onmouseover="this.src='images/ap2.gif'"
								onmouseout="this.src='images/ap.gif'"
								onclick="window.location='adChangeApp.do?appCode=3';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.payables" />
						<html:image src="images/mainLine.gif"/>
						<% if (user.getUserApps().contains("OMEGA RECEIVABLES")) { %>
							<html:image src="images/ar.gif"
								onmouseover="this.src='images/ar2.gif'"
								onmouseout="this.src='images/ar.gif'"
								onclick="window.location='adChangeApp.do?appCode=2';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.receivables" />
						<html:image src="images/mainLine.gif"/>
						<% if (user.getUserApps().contains("OMEGA CASH MANAGEMENT")) { %>
							<html:image src="images/cm.gif"
								onmouseover="this.src='images/cm2.gif'"
								onmouseout="this.src='images/cm.gif'"
								onclick="window.location='adChangeApp.do?appCode=5';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.cashManagement" />
						<html:image src="images/mainLine.gif"/>
						<% if (user.getUserApps().contains("OMEGA ADMINISTRATION")) { %>
							<html:image src="images/sysad.gif"
								onmouseover="this.src='images/sysad2.gif'"
								onmouseout="this.src='images/sysad.gif'"
								onclick="window.location='adChangeApp.do?appCode=4';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.systemAdmin" />
						<html:image src="images/mainLine.gif"/>
						<% if (user.getUserApps().contains("OMEGA INVENTORY")) { %>
							<html:image src="images/inv.gif"
								onmouseover="this.src='images/inv2.gif'"
								onmouseout="this.src='images/inv.gif'"
								onclick="window.location='adChangeApp.do?appCode=6';
								return false"/>
						<% } else { %>
							<html:image src="images/na.gif" disabled="true"/>
						<% } %>
						<bean:message key="main.prompt.inventory" />
					</td>
					<td width="290" height="120" class="moduleTitle">
					<p align="left">
						<table border="0" cellpadding="0" cellspacing="0" width="290"
						height=280>

						<tr>
							<% if (user.getCurrentAppCode() == 1) { %>
							<td width="290" height="200" class="mainSegmentDetail">
							<ul type="square">
							<logic:greaterThan name="adMainForm" property="recurringJournalsToGenerate" value="0">
								<li><html:link
									page="/glRecurringJournalGeneration.do?forward=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.recurringJournalsToGenerate" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="recurringJournalsToGenerate" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="journalsForReversal" value="0">
								<li><html:link page="/glJournalReversal.do?forward=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.journalsForReversal" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="journalsForReversal" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="journalsToApprove" value="0">
								<li><html:link
									page="/glApproval.do?goButton=1&maxRows=20&document=GL%20JOURNAL"
									styleClass="linkMain">
									<bean:message key="main.prompt.journalsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="journalsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="journalsInDraft" value="0">
								<li><html:link page="/glFindJournal.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.journalsInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="journalsInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>
							</ul>
							</td>
							<% } else if (user.getCurrentAppCode() == 2) { %>
							<td width="290" height="100" class="mainSegmentDetail">
							<ul type="square">
							<logic:greaterThan name="adMainForm" property="pdcInvoicesToGenerate" value="0">
								<li><html:link page="/arPdcInvoiceGeneration.do?forward=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.pdcInvoicesToGenerate" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="pdcInvoicesToGenerate" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invoicesToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%20INVOICE"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>
							<logic:equal name="adMainForm" property="showGenerateInterest" value="true">
								 <logic:greaterThan name="adMainForm" property="overDueInvoices" value="0">
									<li><html:link
										page="/arStandardMemoLines.do?generateOverDue=1"
										styleClass="linkMain" onclick="return confirmPost();">
										<bean:message key="main.prompt.overDueInvoices" />
										<font class="mainSegmentDetailValue"> <bean:write
											name="adMainForm" property="overDueInvoices" /> </font>
									</html:link></li>
								</logic:greaterThan>
							</logic:equal>
							<logic:greaterThan name="adMainForm" property="pastDueInvoices" value="0">
								<li><html:link
									page="/arStandardMemoLines.do?generatePenalty=1"
									styleClass="linkMain" onclick="return confirmPenalty();">
									<bean:message key="main.prompt.pastDueInvoices" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="pastDueInvoices" /> </font>
								</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="accruedInterestIS" value="0">
								<li><html:link
									page="/arStandardMemoLines.do?generateAccruedInterestIS=1"
									styleClass="linkMain" onclick="return confirmAccruedInterestIS();">
									<bean:message key="main.prompt.generateAccruedInterestIS" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="accruedInterestIS" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="accruedInterestTB" value="0">
								<li><html:link
									page="/arStandardMemoLines.do?generateAccruedInterestTB=1"
									styleClass="linkMain" onclick="return confirmAccruedInterestTB();">
									<bean:message key="main.prompt.generateAccruedInterestTB" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="accruedInterestTB" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invoicesInDraftItem" value="0">
								<li><html:link page="/arFindInvoice.do?findDraft=1&invoiceType=ITEMS"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesInDraftItem" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesInDraftItem" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="ordersInDraft" value="0">
								<li><html:link page="/arFindSalesOrder.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.ordersInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="ordersInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="salesOrderDeliveryStatus" value="0">
								<li><html:link page="/arFindDelivery.do?findDraft=1&transactionStatus=READY TO DELIVER"
									styleClass="linkMain">
									<bean:message key="main.prompt.salesOrderDeliveryStatus" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="salesOrderDeliveryStatus" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="jobOrdersInDraft" value="0">
								<li><html:link page="/arFindJobOrder.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.jobOrdersInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="jobOrdersInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							
							

							<logic:greaterThan name="adMainForm" property="invoicesInDraftMemoLines" value="0">
								<li><html:link page="/arFindInvoice.do?findDraft=1&invoiceType=MEMO%20LINES"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesInDraftMemoLines" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesInDraftMemoLines" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="invoicesInDraftSoMatched" value="0">
								<li><html:link page="/arFindInvoice.do?findDraft=1&invoiceType=SO%20MATCHED"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesInDraftSoMatched" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesInDraftSoMatched" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="invoicesInDraftJoMatched" value="0">
								<li><html:link page="/arFindInvoice.do?findDraft=1&invoiceType=JO%20MATCHED"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesInDraftJoMatched" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesInDraftJoMatched" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invoicesForPostingItem" value="0">
								<li><html:link page="/arInvoicePost.do?forPosting=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesForPostingItem" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesForPostingItem" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invoicesForPostingMemoLines" value="0">
								<li><html:link page="/arInvoicePost.do?forPosting=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invoicesForPostingMemoLines" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invoicesForPostingMemoLines" /> </font>
								</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="creditMemosToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%20CREDIT%20MEMO"
									styleClass="linkMain">
									<bean:message key="main.prompt.creditMemosToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="creditMemosToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="creditMemosInDraftItems" value="0">
								<li><html:link
									page="/arFindInvoice.do?findDraft=1&creditMemo=true&findType=ITEMS"
									styleClass="linkMain">
									<bean:message key="main.prompt.creditMemosInDraftItems" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="creditMemosInDraftItems" /> </font>
								</html:link></li>
						    </logic:greaterThan>

						    <logic:greaterThan name="adMainForm" property="creditMemosInDraftMemoLines" value="0">
								<li><html:link
									page="/arFindInvoice.do?findDraft=1&creditMemo=true&findType=MEMO"
									styleClass="linkMain">
									<bean:message key="main.prompt.creditMemosInDraftMemoLines" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="creditMemosInDraftMemoLines" /> </font>
								</html:link></li>
						    </logic:greaterThan>


						    <logic:greaterThan name="adMainForm" property="creditMemosForPosting" value="0">
								<li><html:link
									page="/arInvoicePost.do?creditMemo=1&forPosting=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.creditMemosForPosting" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="creditMemosForPosting" /> </font>
								</html:link></li>
							</logic:greaterThan>

						    <logic:greaterThan name="adMainForm" property="receiptsToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%20RECEIPT"
									styleClass="linkMain">
									<bean:message key="main.prompt.receiptsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receiptsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>



							<logic:greaterThan name="adMainForm" property="receiptsCollectionForPosting" value="0">
								<li><html:link
									page="/arReceiptPost.do?receiptType=COLLECTION&maxRows=20&goButton=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receiptsCollectionForPosting" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receiptsCollectionForPosting" /> </font>
								</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="receiptsMiscForPosting" value="0">
								<li><html:link
									page="/arReceiptPost.do?receiptType=MISC&maxRows=20&goButton=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receiptsMiscForPosting" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receiptsMiscForPosting" /> </font>
								</html:link></li>
							</logic:greaterThan>



							<logic:greaterThan name="adMainForm" property="receiptsCollectionInDraft" value="0">
								<li><html:link page="/arFindReceipt.do?receiptType=COLLECTION&findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receiptsCollectionInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receiptsCollectionInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="receiptsMiscInDraft" value="0">
								<li><html:link page="/arFindReceipt.do?receiptType=MISC&findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receiptsMiscInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receiptsMiscInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="salesOrdersToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%20SALES%20ORDER"
									styleClass="linkMain">
									<bean:message key="main.prompt.salesOrdersToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="salesOrdersToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							<logic:greaterThan name="adMainForm" property="jobOrdersToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%JOB%20ORDER"
									styleClass="linkMain">
									<bean:message key="main.prompt.jobOrdersToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="jobOrdersToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="customerToApprove" value="0">
								<li><html:link
									page="/arApproval.do?goButton=1&maxRows=20&document=AR%20CUSTOMER"
									styleClass="linkMain">
									<bean:message key="main.prompt.customerToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="customerToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="customerRejected" value="0">
								<li><html:link
									page="/arFindCustomer.do?findRejected=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.customerRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="customerRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="salesOrdersForProcessing" value="0">
								<li><html:link
									page="/arFindSalesOrder.do?findSalesOrderProcessing=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.salesOrdersForProcessing" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="salesOrdersForProcessing" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							
							<logic:greaterThan name="adMainForm" property="jobOrdersForProcessing" value="0">
								<li><html:link
									page="/arFindJobOrder.do?findJobOrderProcessing=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.jobOrdersForProcessing" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="jobOrdersForProcessing" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="salesOrdersApproveToday" value="0">
								<li><html:link
									page="<%=user.salesOrderURL()%>"
									styleClass="linkMain">
									<bean:message key="main.prompt.salesOrdersApproveToday" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="salesOrdersApproveToday" /> </font>
								</html:link></li>
							</logic:greaterThan>
							
							
							<logic:greaterThan name="adMainForm" property="jobOrdersApproveToday" value="0">
								<li><html:link
									page="<%=user.salesOrderURL()%>"
									styleClass="linkMain">
									<bean:message key="main.prompt.jobOrdersApproveToday" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="jobOrdersApproveToday" /> </font>
								</html:link></li>
							</logic:greaterThan>






							<logic:greaterThan name="adMainForm" property="investorBonusAndInterest" value="0">
								<li><html:link
									page="/arStandardMemoLines.do"
									styleClass="linkMain">
									<bean:message key="main.prompt.investorBonusAndInterest" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="investorBonusAndInterest" /> </font>
								</html:link></li>
							</logic:greaterThan>

							</ul>
							</td>
							<% } else if (user.getCurrentAppCode() == 3) {%>
							<td width="290" height="200" class="mainSegmentDetail">
							<ul type="square">
							<logic:greaterThan name="adMainForm" property="recurringVouchersToGenerate" value="0">
								<li><html:link
									page="/apRecurringVoucherGeneration.do?forward=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.recurringVouchersToGenerate" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="recurringVouchersToGenerate" /> </font>
								</html:link></li>
							</logic:greaterThan>

<%-- 							<logic:greaterThan name="adMainForm" property="paymentRequestInDraft" value="0">		 --%>

<%-- 								<li><html:link --%>
<%-- 									page="/apFindVoucher.do?findDraft=1&paymentRequest=true" --%>
<%-- 									styleClass="linkMain"> --%>
<%-- 									<bean:message key="main.prompt.paymentRequestInDraft" /> --%>
<%-- 									<font class="mainSegmentDetailValue"> <bean:write --%>
<%-- 										name="adMainForm" property="paymentRequestInDraft" /> </font> --%>
<%-- 								</html:link></li> --%>
<%-- 							</logic:greaterThan> --%>

							<logic:greaterThan name="adMainForm" property="checkPaymentRequestsToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20CHECK%20PAYMENT%20REQUEST"
									styleClass="linkMain">
									<bean:message key="main.prompt.checkPaymentRequestsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checkPaymentRequestsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="paymentRequestRejected" value="0">
								<li><html:link
									page="/apFindVoucher.do?findDraft=1&paymentRequest=true&approvalStatus=REJECTED"
									styleClass="linkMain">
									<bean:message key="main.prompt.paymentRequestRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="paymentRequestRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="vouchersToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20VOUCHER"
									styleClass="linkMain">
									<bean:message key="main.prompt.vouchersToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="vouchersToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="vouchersInDraft" value="0">
								<li><html:link page="/apFindVoucher.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.vouchersInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="vouchersInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="vouchersRejected" value="0">
								<li><html:link page="/apFindVoucher.do?findRejected=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.vouchersRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="vouchersRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="vouchersForProcessing" value="0">
								<li><html:link
									page="/apVoucherEntry.do"
									styleClass="linkMain">
									<bean:message key="main.prompt.vouchersForProcessing" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="vouchersForProcessing" /> </font>
								</html:link></li>
							</logic:greaterThan>


<%-- 								<li><html:link --%>
<%-- 									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20CHECK%20PAYMENT%20REQUEST" --%>
<%-- 									styleClass="linkMain"> --%>
<%-- 									<bean:message key="main.prompt.checkPaymentRequestsToGenerate" /> --%>
<%-- 									<font class="mainSegmentDetailValue"> <bean:write --%>
<%-- 										name="adMainForm" property="checkPaymentRequestsToGenerate" /> </font> --%>
<%-- 								</html:link></li> --%>




							<logic:greaterThan name="adMainForm" property="debitMemosToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20DEBIT%20MEMO"
									styleClass="linkMain">
									<bean:message key="main.prompt.debitMemosToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="debitMemosToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="debitMemosInDraft" value="0">
								<li><html:link
									page="/apFindVoucher.do?findDraft=1&debitMemo=true"
									styleClass="linkMain">
									<bean:message key="main.prompt.debitMemosInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="debitMemosInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>



							<logic:greaterThan name="adMainForm" property="checksInDraft" value="0">
								<li><html:link page="/apFindCheck.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.checksInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checksInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="checksRejected" value="0">
								<li><html:link page="/apFindCheck.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.checksRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checksRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="checksToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20CHECK"
									styleClass="linkMain">
									<bean:message key="main.prompt.checksToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checksToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="directChecksInDraft" value="0">
								<li><html:link page="/apFindCheck.do?findDirectDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.directChecksInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="directChecksInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="directChecksRejected" value="0">
								<li><html:link page="/apFindCheck.do?findDirectDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.directChecksRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="directChecksRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="directChecksToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20CHECK"
									styleClass="linkMain">
									<bean:message key="main.prompt.directChecksToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="directChecksToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>




							<logic:greaterThan name="adMainForm" property="checksForPrinting" value="0">
								<li><html:link page="/apFindCheck.do?findChecksForPrinting=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.checksForPrinting" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checksForPrinting" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="receivingItemsToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20RECEIVING%20ITEM"
									styleClass="linkMain">
									<bean:message key="main.prompt.receivingItemsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receivingItemsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="receivingItemsInDraft" value="0">
								<li><html:link page="/apFindPurchaseOrder.do?findDraft=1&receiving=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receivingItemsInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receivingItemsInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="receivingItemsRejected" value="0">
								<li><html:link page="/apFindPurchaseOrder.do?findReceivingRejected=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.receivingItemsRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="receivingItemsRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="purchaseOrdersToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20PURCHASE%20ORDER"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseOrdersToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseOrdersToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="purchaseOrdersInDraft" value="0">
								<li><html:link page="/apFindPurchaseOrder.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseOrdersInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseOrdersInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="purchaseOrdersRejected" value="0">
								<li><html:link page="/apFindPurchaseOrder.do?findRejected=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseOrdersRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseOrdersRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

								<% if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_ORDER_ENTRY_ID) != null) {%>
							<logic:greaterThan name="adMainForm" property="purchaseOrdersForGeneration" value="0">
								<li><html:link page="/apFindPurchaseRequisition.do?forwardCnv=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseOrdersForGeneration" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseOrdersForGeneration" /> </font>
								</html:link></li>
							</logic:greaterThan>
								<% } %>
							<logic:greaterThan name="adMainForm" property="purchaseOrdersForPrinting" value="0">
								<li><html:link page="/apFindPurchaseOrder.do?forward=1&print=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseOrdersForPrinting" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseOrdersForPrinting" /> </font>
								</html:link></li>
							</logic:greaterThan>
								<% if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_ORDER_ENTRY_ID) != null) {%>
							<logic:greaterThan name="adMainForm" property="recurringPurchaseRequisitionsToGenerate" value="0">
								<li><html:link
									page="/apFindPurchaseRequisition.do?forwardRec=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.recurringPurchaseRequisitionsToGenerate" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="recurringPurchaseRequisitionsToGenerate" /> </font>
								</html:link></li>
							</logic:greaterThan>
								<%  } %>

							<logic:greaterThan name="adMainForm" property="purchaseRequisitionsToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20PURCHASE%20REQUISITION"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseRequisitionsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseRequisitionsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="purchaseRequisitionsInDraft" value="0">
								<li><html:link page="/apFindPurchaseRequisition.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseRequisitionsInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseRequisitionsInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

								<% if (Common.getUserPermission(sideBarUser, Constants.AP_CANVASS_ID) != null) { %>
								<logic:greaterThan name="adMainForm" property="purchaseRequisitionsForCanvass" value="0">
								<li><html:link page="/apFindPurchaseRequisition.do?forwardForCanvass=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseRequisitionsForCanvass" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseRequisitionsForCanvass" /> </font>
								</html:link></li>
							</logic:greaterThan>
								<% } %>

							<logic:greaterThan name="adMainForm" property="purchaseRequisitionsRejected" value="0">
								<li><html:link page="/apFindPurchaseRequisition.do?forwardRej=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.purchaseRequisitionsRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="purchaseRequisitionsRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="canvassRejected" value="0">
								<li><html:link page="/apFindPurchaseRequisition.do?findCanvassRejected=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.canvassRejected" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="canvassRejected" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="canvassToApprove" value="0">
								<li><html:link
									page="/apApproval.do?goButton=1&maxRows=20&document=AP%20CANVASS"
									styleClass="linkMain">
									<bean:message key="main.prompt.canvassToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="canvassToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="paymentRequestDraft" value="0">
							<li><html:link
								page="/apFindVoucher.do?findDraft=1&paymentRequest=true"
								styleClass="linkMain">
								<bean:message key="main.prompt.paymentRequestDraft" />
								<font class="mainSegmentDetailValue"> <bean:write
									name="adMainForm" property="paymentRequestDraft" /> </font>
							</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="paymentRequestGeneration" value="0">
							<li><html:link
								page="/apFindVoucher.do?findGeneration=1&paymentRequest=true"
								styleClass="linkMain">
								<bean:message key="main.prompt.paymentRequestGeneration" />
								<font class="mainSegmentDetailValue"> <bean:write
									name="adMainForm" property="paymentRequestGeneration" /> </font>
							</html:link></li>
							</logic:greaterThan>

							<!--
							<logic:greaterThan name="adMainForm" property="paymentRequestProcessing" value="0">
							<li><html:link
								page="/apFindVoucher.do?findProcessing=1&paymentRequest=true"
								styleClass="linkMain">
								<bean:message key="main.prompt.paymentRequestProcessing" />
								<font class="mainSegmentDetailValue"> <bean:write
									name="adMainForm" property="paymentRequestProcessing" /> </font>
							</html:link></li>
							</logic:greaterThan>
							-->

							<logic:greaterThan name="adMainForm" property="paymentsForProcessing" value="0">
							<li><html:link
								page="/apPaymentEntry.do"
								styleClass="linkMain">
								<bean:message key="main.prompt.paymentsForProcessing" />
								<font class="mainSegmentDetailValue"> <bean:write
									name="adMainForm" property="paymentsForProcessing" /> </font>
							</html:link></li>
							</logic:greaterThan>


							<logic:greaterThan name="adMainForm" property="generateLoan" value="0">
							<li><html:link
								page="/adChangeApp.do?appCode=2&generateLoan=1"
								styleClass="linkMain" onclick="return confirmGenerateLoan();">
								<bean:message key="main.prompt.generateLoan" />
								<font class="mainSegmentDetailValue"> <bean:write
									name="adMainForm" property="generateLoan" /> </font>
							</html:link></li>
							</logic:greaterThan>


							</ul>
							</td>



							<% } else if (user.getCurrentAppCode() == 4) {%>
							<td width="290" height="100" class="mainSegmentDetail">
							<!--ul type="square">
								<li><bean:message key="main.prompt.noReminders" /></li>
							</ul-->
							</td>
							<% } else if (user.getCurrentAppCode() == 5) {%>
							<td width="290" height="100" class="mainSegmentDetail">
							<ul type="square">

							<logic:greaterThan name="adMainForm" property="bankAdjustmentsToApprove" value="0">
								<li><html:link
									page="/cmApproval.do?goButton=1&maxRows=20&document=CM%20ADJUSTMENT"
									styleClass="linkMain">
									<bean:message key="main.prompt.bankAdjustmentsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="bankAdjustmentsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="bankAdjustmentsInDraft" value="0">
								<li><html:link page="/cmFindAdjustment.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.bankAdjustmentsInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="bankAdjustmentsInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="fundTransfersToApprove" value="0">
								<li><html:link
									page="/cmApproval.do?goButton=1&maxRows=20&document=CM%20FUND%20TRANSFER"
									styleClass="linkMain">
									<bean:message key="main.prompt.fundTransfersToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="fundTransfersToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="fundTransfersInDraft" value="0">
								<li><html:link page="/cmFindFundTransfer.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.fundTransfersInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="fundTransfersInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="checksForRelease" value="0">
							<li><html:link page="/cmReleasingChecks.do?findChecksForRelease=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.checksForRelease" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="checksForRelease" /> </font>
							</html:link></li>
							</logic:greaterThan>

							</ul>
							</td>
							<% } else if (user.getCurrentAppCode() == 6) {%>
							<td width="290" height="100" class="mainSegmentDetail">
							<ul type="square">

							<logic:greaterThan name="adMainForm" property="invAdjustmentsToApprove" value="0">
								<li><html:link
									page="/invApproval.do?goButton=1&maxRows=20&document=INV%20ADJUSTMENT"
									styleClass="linkMain">
									<bean:message key="main.prompt.invAdjustmentsToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invAdjustmentsToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="inventoryAdjustmentsInDraft" value="0">
								<li><html:link page="/invFindAdjustment.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.inventoryAdjustmentsInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="inventoryAdjustmentsInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferOrderInDraft" value="0">
								<li><html:link page="/invFindBranchStockTransfer.do?findOrderInDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferOrderInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferOrderInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferUnservedOrders" value="0">
								<li><html:link page="/invBranchStockTransferOutEntry.do"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferUnservedOrders" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferUnservedOrders" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferOutInDraft" value="0">
								<li><html:link page="/invFindBranchStockTransfer.do?findDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferOutInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferOutInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferOutIncoming" value="0">
								<li><html:link page="/invBranchStockTransferInEntry.do"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferOutIncoming" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferOutIncoming" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferInDraft" value="0">
								<li><html:link page="/invFindBranchStockTransfer.do?findInDraft=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBuildUnbuildAssemblyToApprove" value="0">
								<li><html:link
									page="/invApproval.do?goButton=1&maxRows=20&document=INV%20BUILD%20ASSEMBLY"
									styleClass="linkMain">
									<bean:message
										key="main.prompt.invBuildUnbuildAssemblyToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBuildUnbuildAssemblyToApprove" />
									</font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="buildUnbuildAssemblyOrderInDraft" value="0">
								<li><html:link
									page="/invFindBuildUnbuildAssembly.do?findDraft=1&receiving=0"
									styleClass="linkMain">
									<bean:message key="main.prompt.buildUnbuildAssemblyOrderInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="buildUnbuildAssemblyOrderInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="buildUnbuildAssemblyInDraft" value="0">
								<li><html:link
									page="/invFindBuildUnbuildAssembly.do?findDraft=1&receiving=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.buildUnbuildAssemblyInDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="buildUnbuildAssemblyInDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>



							<logic:greaterThan name="adMainForm" property="buildUnbuildAssemblyOrderInDue" value="0">
								<li><html:link
									page="/invFindBuildUnbuildAssembly.do?findDraft=1&receiving=0"
									styleClass="linkMain">
									<bean:message key="main.prompt.buildUnbuildAssemblyOrderInDue" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="buildUnbuildAssemblyOrderInDue" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invStockTransferToApprove" value="0">
								<li><html:link
									page="/invApproval.do?goButton=1&maxRows=20&document=INV%20STOCK%20TRANSFER"
									styleClass="linkMain">
									<bean:message key="main.prompt.invStockTransferToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invStockTransferToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

                                                        <logic:greaterThan name="adMainForm" property="invStockTransferDraft" value="0">
								<li><html:link
									page="/invFindStockTransfer.do?goButton=1&approvalStatus=DRAFT&posted=NO&orderBy="
									styleClass="linkMain">
									<bean:message key="main.prompt.invStockTransferDraft" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invStockTransferDraft" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invBranchStockTransferOrderToApprove" value="0">
								<li><html:link
									page="/invApproval.do?goButton=1&maxRows=20&document=INV%20BRANCH%20STOCK%20TRANSFER%20ORDER"
									styleClass="linkMain">
									<bean:message key="main.prompt.invBranchStockTransferOrderToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invBranchStockTransferOrderToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invAdjustmentRequestToApprove" value="0">
								<li><html:link
									page="/invApproval.do?goButton=1&maxRows=20&document=INV%20ADJUSTMENT%20REQUEST"
									styleClass="linkMain">
									<bean:message key="main.prompt.invAdjustmentRequestToApprove" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invAdjustmentRequestToApprove" /> </font>
								</html:link></li>
							</logic:greaterThan>

							<logic:greaterThan name="adMainForm" property="invAdjustmentRequestToGenerate" value="0">
								<li><html:link
									page="/invFindAdjustment.do?findAdjustmentRequestToGenerate=1"
									styleClass="linkMain">
									<bean:message key="main.prompt.invAdjustmentRequestToGenerate" />
									<font class="mainSegmentDetailValue"> <bean:write
										name="adMainForm" property="invAdjustmentRequestToGenerate" /> </font>
								</html:link></li>
							</logic:greaterThan>
							</ul>
							</td>


							<% } else if (user.getCurrentAppCode() == 7) {%>
							<td width="290" height="100" class="mainSegmentDetail">
							<!--ul type="square">
								<li><bean:message key="main.prompt.noReminders" /></li>
							</ul-->
							</td>

							<% } else if (user.getCurrentAppCode() == 8) {%>
							<td width="290" height="100" class="mainSegmentDetail">
							<!--ul type="square">
								<li><bean:message key="main.prompt.noReminders" /></li>
							</ul-->
							</td>

							<% } %>
						</tr>
					</table>
					</td>
				</tr>

			</table>
			</td>
	</table>
</html:form>
</div>
</body>
</html>
