<%@ page errorPage="cmnErrorPage.jsp" %>
<%@ page import="com.struts.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<% 
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Pragma", "public");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", 0);
    
    File filePDF = (File)session.getAttribute(Constants.PDF_REPORT_KEY);
    
   	byte[] bytes = new byte[(int)filePDF.length()];
   	response.setContentType("application/pdf");
	response.setContentLength((int)filePDF.length());	
	FileInputStream fis = new FileInputStream(filePDF);
	ServletOutputStream outputStream = response.getOutputStream();
	int len;
	while ((len = fis.read(bytes))>0){
		outputStream.write(bytes, 0, bytes.length);
	}
	
	outputStream.flush();
	outputStream.close();
	
	
%>