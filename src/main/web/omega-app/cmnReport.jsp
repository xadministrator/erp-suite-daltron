<%@ page errorPage="cmnErrorPage.jsp" %>
<%@ page import="com.struts.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.io.*" %>

<%@	page import="net.sf.jasperreports.engine.export.JRCsvExporter "%>
<%@	page import="net.sf.jasperreports.engine.JasperPrint "%>
<%@ page import="net.sf.jasperreports.engine.JRExporterParameter;" %>

<%

    response.setHeader("Cache-Control", "max-age=0");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Pragma", "public");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", 0);

    Report report = (Report)session.getAttribute(Constants.REPORT_KEY);

    if (report != null) {

	    if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_PDF)) {
			System.out.println("pasok 1");
			if (report.getBytes()  != null) {
				
				byte[] bytes = report.getBytes();
				
				System.out.println("size byte is: "+ bytes.length);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				ServletOutputStream outputStream = response.getOutputStream();
				outputStream.write(bytes, 0, bytes.length);
				outputStream.flush();
				outputStream.close();

			}

	    }  else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_TEXT)) {

	        if (report.getBytes()  != null) {

				byte[] bytes = report.getBytes();
				response.setContentType("text/csv");
				response.setContentLength(bytes.length);
				response.setHeader("Content-Disposition","inline;filename=\"Report.txt\"");
				ServletOutputStream outputStream = response.getOutputStream();
				outputStream.write(bytes, 0, bytes.length);
				outputStream.flush();
				outputStream.close();

			}

            }else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_DIGIBANK)) {

	        if (report.getBytes()  != null) {



                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh-mm");
                    String txtFileName = "DigiBank - " + formatter.format(new Date());

                    byte[] bytes = report.getBytes();
                    response.setContentType("text/csv");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-Disposition","inline;filename=\"" + txtFileName + ".txt\"");
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(bytes, 0, bytes.length);

                    outputStream.flush();
                    outputStream.close();


                }


            } else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_PURCHASES)) {

    	        if (report.getBytes()  != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh-mm");
                        String txtFileName = "RELIEF PURCHASES - " + formatter.format(new Date());

                        byte[] bytes = report.getBytes();
                        response.setContentType("text/csv");
                        response.setContentLength(bytes.length);
                        response.setHeader("Content-Disposition","inline;filename=\"" + txtFileName + ".data\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(bytes, 0, bytes.length);
                        outputStream.flush();
                        outputStream.close();

                    }


			} else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_SALES)) {

    	        if (report.getBytes()  != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh-mm");
                        String txtFileName = "RELIEF SALES - " + formatter.format(new Date());

                        byte[] bytes = report.getBytes();
                        response.setContentType("text/csv");
                        response.setContentLength(bytes.length);
                        response.setHeader("Content-Disposition","inline;filename=\"" + txtFileName + ".data\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(bytes, 0, bytes.length);
                        outputStream.flush();
                        outputStream.close();

                    }

			} else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_RELIEF_IMPORTATIONS)) {

    	        if (report.getBytes()  != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh-mm");
                        String txtFileName = "RELIEF IMPORTATIONS - " + formatter.format(new Date());

                        byte[] bytes = report.getBytes();
                        response.setContentType("text/csv");
                        response.setContentLength(bytes.length);
                        response.setHeader("Content-Disposition","inline;filename=\"" + txtFileName + ".data\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(bytes, 0, bytes.length);
                        outputStream.flush();
                        outputStream.close();

                    }

            } else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_EXCEL)) {

                if (report.getBytes()  != null) {

                            byte[] bytes = report.getBytes();
                            response.setContentType("application/vnd.ms-excel");
                            response.setContentLength(bytes.length);
                            response.setHeader("Content-Disposition","inline;filename=\"Report.xls\"");
                            ServletOutputStream outputStream = response.getOutputStream();
                            outputStream.write(bytes, 0, bytes.length);
                            outputStream.flush();
                            outputStream.close();

                    }

            } else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_HTML)) {

                if (report.getJasperPrint() != null) {

                    Map imagesMap = new HashMap();
                    session.setAttribute(Constants.IMAGES_MAP_KEY, imagesMap);

                    JasperRunManagerExt.runReportToHtml(report.getJasperPrint(),
                        out, imagesMap, "cmnReportImage.jsp?image=");

                }

            } else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV)) {

                 if (report.getBytes()  != null) {

                    byte[] bytes = report.getBytes();
                    response.setContentType("text/csv");
                    response.setHeader("Content-Disposition","inline;filename=\"Report.csv\"");
                    response.setContentLength(bytes.length);
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(bytes, 0, bytes.length);
                    outputStream.flush();
                    outputStream.close();

                 }
	     	} else if (report.getViewType().equals(Constants.REPORT_VIEW_TYPE_CSV_SP)) {

                if (report.getJasperPrint()  != null) {


				   JasperPrint jrprint = report.getJasperPrint();


                   response.setContentType("text/csv");
                   response.setHeader("Content-Disposition","inline;filename=\"Report.csv\"");

                   JRCsvExporter exporter = new JRCsvExporter();

		       	   exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrprint);


                 //  response.setContentLength(bytes.length);
                   ServletOutputStream outputStream = response.getOutputStream();

                   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
                   exporter.exportReport();
                   outputStream.flush();
                   outputStream.close();


                }
	     	}

		    else if (report.getViewType().equals(Constants.REPORT_EMAIL_BLAST_CSV)) {

		        if (report.getBytes()  != null) {

		        	String csvFileName = (String)session.getAttribute("csvFileName");
	                byte[] bytes = report.getBytes();
	                response.setContentType("text/csv");
	                response.setHeader("Content-Disposition","inline;filename=\"" + csvFileName + ".csv\"");
	                response.setContentLength(bytes.length);
	                ServletOutputStream outputStream = response.getOutputStream();
	                outputStream.write(bytes, 0, bytes.length);
	                outputStream.flush();
	                outputStream.close();

		        }
			}
	}
%>