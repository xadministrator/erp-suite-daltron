<%@ page errorPage="cmnErrorPage.jsp" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.struts.util.Constants" %>

<%
	Map imagesMap = (Map)session.getAttribute(Constants.IMAGES_MAP_KEY);

	if (imagesMap != null)
	{
		String imageName = request.getParameter("image");
		if (imageName != null)
		{
			byte[] imageData = (byte[])imagesMap.get(imageName);
			response.setContentLength(imageData.length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(imageData, 0, imageData.length);
			ouputStream.flush();
			ouputStream.close();
		}
	}
%>
