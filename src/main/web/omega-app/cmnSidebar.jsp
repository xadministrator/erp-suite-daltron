<%@ page import="com.struts.util.User" %>
<%@ page import="com.struts.util.Responsibility" %>
<%@ page import="com.struts.util.Branch" %>
<%@ page import="com.struts.util.Constants" %>
<%@ page import="com.struts.util.Common" %>
<%
    User sideBarUser = (User) session.getAttribute(Constants.USER_KEY);
%>

<SCRIPT language=JavaScript type=text/javascript>

// The following line is critical for menu operation, and must appear only once.
menunum=0;menus=new Array();_d=document;
function addmenu(){
	menunum++;
	menus[menunum]=menu;
}

function dumpmenus(){
	mt="<script language=javascript>";
	for(a=1;a<menus.length;a++){
		mt+=" menu"+a+"=menus["+a+"];"
	}mt+="<\/script>";
	_d.write(mt)
}
//Please leave the above line intact

////////////////////////////////////
// Editable properties START here //
////////////////////////////////////


effect = "randomdissolve(duration=0.5);Alpha(style=0,opacity=100);Shadow(color='#6699ff', Direction=135, Strength=0)" // Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters

timegap=1000				// The time delay for menus to remain visible
followspeed=5			// Follow Scrolling speed
followrate=40			// Follow Scrolling Rate
suboffset_top=10;		// Sub menu offset Top position
suboffset_left=10;		// Sub menu offset Left position

res_style=[				// res_style is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"black",				// Mouse Off Font Color
"dfeef5",				// Mouse Off Background Color
"0066a5",				// Mouse On Font Color
"d6d6d6",				// Mouse On Background Color
"90afd7",				// Menu Border Color
11,					// Font Size in pixels
"normal",				// Font Style (italic or normal)
"bold",					// Font Weight (bold or normal)
"Tahoma",				// Font Name
1,					// Menu Item Padding
"images/arrow.gif",			// Sub Menu Image (Leave this blank if not needed)
,					// 3D Border & Separator bar
"66ffff",				// 3D High Color
"000099",				// 3D Low Color
"Purple",				// Current Page Item Font Color (leave this blank to disable)
"ccccff",				// Current Page Item Background Color (leave this blank to disable)
"images/arrowdn.gif",			// Top Bar image (Leave this blank to disable)
"black",				// Menu Header Font Color (Leave blank if headers are not needed)
"dfeef5",				// Menu Header Background Color (Leave blank if headers are not needed)
]

// Header Properties

addmenu(menu=[			// This is the array that contains your menu properties and details
"resmenu",			// Menu Name - This is needed in order for the menu to be called
58,				// Menu Top - The Top position of the menu in pixels
180,				// Menu Left - The Left position of the menu in pixels
148,				// Menu Width - Menus width in pixels
1,				// Menu Border Width
,				// Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
res_style,			// Properties Array - this is set higher up, as above
1,				// Always Visible - allows the menu item to be visible at all time (1=on/0=off)
"left",				// Alignment - sets the menu elements text alignment, values valid here are: left, right or center
effect,				// Filter - Text variable for setting transitional effects on menu activation - see above for more info
,				// Follow Scrolling - Tells the menu item to follow the user down the screen (visible at all times) (1=on/0=off)
1, 				// Horizontal Menu - Tells the menu to become horizontal instead of top to bottom style (1=on/0=off)
,				// Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page (1=on/0=off)
,				// Position of TOP sub image left:center:right
,				// ..Now Obsolete..
,				// Right To Left - Used in Hebrew for example. (1=on/0=off)
,				// Open the Menus OnClick - leave blank for OnMouseover (1=on/0=off)
,				// ID of the div you want to hide on MouseOver (useful for hiding form elements)
,				// Reserved for future use
,				// Reserved for future use
,				// Reserved for future use
,"Select Responsibility&nbsp;&nbsp;","show-menu=responsibility",,"",1 // "Description Text", "URL", "Alternate URL", "Status", "Separator Bar"
,"Select Branch&nbsp;&nbsp-<%=sideBarUser.getCurrentBranch().getBrBranchCode()%>","show-menu=branch",,"",1
,"Select Application","show-menu=application",,,1
,"Logoff","adLogoff.do",,,1
])

// add responsibilities

	menu=["responsibility",,,145,1,,res_style,0,"left",effect,0,,,,,,,,,,,]
	var i = 21;

	<%
	    int resCount = sideBarUser.getResCount();

        for (int j=0; j <= resCount-1; j++) {

	       Responsibility res = sideBarUser.getRes(j);

	%>
           menu[++i] = "<%=res.getRSName()%>"; menu[++i] = "adChangeRes.do?resCode=<%=res.getRSCode()%>"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
	<%
	    }
	%>

	addmenu(menu);

// add branches

	menu=["branch",,,145,1,,res_style,0,"left",effect,0,,,,,,,,,,,]
	i = 21;
	var string1 = window.location.href;
	var lastIndex = string1.lastIndexOf("/");
	var string2 = string1.substring(parseInt(lastIndex));
	lastIndex = string2.lastIndexOf(".do");
	var returnLink = string2.substring(0, (parseInt(lastIndex)+3) );
	<%

		int brnchCount = sideBarUser.getBrnchCount();

		for (int j=0; j <= brnchCount-1; j++) {

			Branch brnch = sideBarUser.getBranch(j);

	%>
			menu[++i] = "<%=brnch.getBrBranchCode()%>"; menu[++i] = "adChangeBranch.do?brnchCode=<%=brnch.getBrCode()%>&location="+returnLink+""; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
	<%
		}
	%>

	addmenu(menu);

// add application

    i = 21;
    menu=["application",,,145,1,,res_style,0,"left",effect,0,,,,,,,,,,,]

    <% if (sideBarUser.getUserApps().contains("OMEGA GENERAL LEDGER")) { %>
    menu[++i] = "General Ledger"; menu[++i] = "adChangeApp.do?appCode=1"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA RECEIVABLES")) { %>
    menu[++i] = "Receivables"; menu[++i] = "adChangeApp.do?appCode=2"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA PAYABLES")) { %>
    menu[++i] = "Payables"; menu[++i] = "adChangeApp.do?appCode=3"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA ADMINISTRATION")) { %>
    menu[++i] = "System Admin"; menu[++i] = "adChangeApp.do?appCode=4"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA CASH MANAGEMENT")) { %>
    menu[++i] = "Cash Mngt"; menu[++i] = "adChangeApp.do?appCode=5"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA INVENTORY")) { %>
    menu[++i] = "Inventory"; menu[++i] = "adChangeApp.do?appCode=6"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>
    <% if (sideBarUser.getUserApps().contains("OMEGA HUMAN RESOURCE")) { %>
    menu[++i] = "Human Resource"; menu[++i] = "adChangeApp.do?appCode=7"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>

    <% if (sideBarUser.getUserApps().contains("OMEGA PROJECT MANAGEMENT")) { %>
    menu[++i] = "Project Management"; menu[++i] = "adChangeApp.do?appCode=8"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
    <% } %>

    addmenu(menu);

dumpmenus()

</SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>

// The following line is critical for menu operation, and must appear only once.
//menunum=0;menus=new Array();_d=document;function addmenu(){menunum++;menus[menunum]=menu;}function dumpmenus(){mt="<script language=javascript>";for(a=1;a<menus.length;a++){mt+=" menu"+a+"=menus["+a+"];"}mt+="<\/script>";_d.write(mt)}
//Please leave the above line intact

////////////////////////////////////
// Editable properties START here //
////////////////////////////////////


effect = "randomdissolve(duration=0.5);Alpha(style=0,opacity=100);Shadow(color='#999999', Direction=135, Strength=5)" // Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters

timegap=1000				// The time delay for menus to remain visible
followspeed=5			// Follow Scrolling speed
followrate=40			// Follow Scrolling Rate
suboffset_top=10;		// Sub menu offset Top position
suboffset_left=10;		// Sub menu offset Left position

menu_style=[				// menu_style is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"black",				// Mouse Off Font Color
"dfeef5",				// Mouse Off Background Color
"0066a5",				// Mouse On Font Color
"eaeaea",				// Mouse On Background Color
"90afd7",				// Menu Border Color
12,					// Font Size in pixels
"normal",				// Font Style (italic or normal)
"normal",				// Font Weight (bold or normal)
"Tahoma",				// Font Name
3,					// Menu Item Padding
"images/arrow.gif",			// Sub Menu Image (Leave this blank if not needed)
,					// 3D Border & Separator bar
"66ffff",				// 3D High Color
"000099",				// 3D Low Color
"000000",				// Current Page Item Font Color (leave this blank to disable)
"c0c0c0",				// Current Page Item Background Color (leave this blank to disable)
"images/arrowdn.gif",		// Top Bar image (Leave this blank to disable)
"132e51",				// Menu Header Font Color (Leave blank if headers are not needed)
"eef7fc",				// Menu Header Background Color (Leave blank if headers are not needed)
]

// side bar

menu=[		// This is the array that contains your menu properties and details
"mainmenu",			// Menu Name - This is needed in order for the menu to be called
77,					// Menu Top - The Top position of the menu in pixels
1,	     			// Menu Left - The Left position of the menu in pixels
177,				// Menu Width - Menus width in pixels
1,					// Menu Border Width
,					// Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
menu_style,				// Properties Array - this is set higher up, as above
1,					// Always Visible - allows the menu item to be visible at all time (1=on/0=off)
"left",				// Alignment - sets the menu elements text alignment, values valid here are: left, right or center
effect,				// Filter - Text variable for setting transitional effects on menu activation - see above for more info
,					// Follow Scrolling - Tells the menu item to follow the user down the screen (visible at all times) (1=on/0=off)
0, 					// Horizontal Menu - Tells the menu to become horizontal instead of top to bottom style (1=on/0=off)
,					// Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page (1=on/0=off)
,					// Position of TOP sub image left:center:right
,					// ..Now Obsolete..
,					// Right To Left - Used in Hebrew for example. (1=on/0=off)
,					// Open the Menus OnClick - leave blank for OnMouseover (1=on/0=off)
,					// ID of the div you want to hide on MouseOver (useful for hiding form elements)
,					// Reserved for future use
,					// Reserved for future use
,					// Reserved for future use
]

var i = 21;

<% if (sideBarUser.getCurrentAppCode() == 1) { %>

// gl menu/submenu

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_BATCH_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_PRINT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_SUBMIT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_FIND_RECURRING_JOURNAL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_GENERATION_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_REVERSAL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_IMPORT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_APPROVAL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_POST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_OPEN_CLOSE_PERIODS_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_YEAR_END_CLOSING_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_FOREX_REVALUATION_ID) != null) { %>
menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_BATCH_ID) != null) { %>
menu[++i] = "Journal Batch"; menu[++i] = "show-menu=journalbatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_PRINT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_SUBMIT_ID) != null||
		Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_COPY_ID) != null) { %>
menu[++i] = "Journal"; menu[++i] = "show-menu=journal"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_RECURRING_JOURNAL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_GENERATION_ID) != null) { %>
menu[++i] = "Recurring"; menu[++i] = "show-menu=recurring"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_REVERSAL_ID) != null) { %>
menu[++i] = "Reversal"; menu[++i] = "glJournalReversal.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_IMPORT_ID) != null) { %>
menu[++i] = "Import"; menu[++i] = "glJournalImport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FOREX_REVALUATION_ID) != null) { %>
menu[++i] = "FOREX Revaluation"; menu[++i] = "glForexRevaluation.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_APPROVAL_ID) != null) { %>
menu[++i] = "Approval"; menu[++i] = "glApproval.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_POST_ID) != null) { %>
menu[++i] = "Posting"; menu[++i] = "glJournalPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_OPEN_CLOSE_PERIODS_ID) != null) { %>
menu[++i] = "Open/Close Periods"; menu[++i] = "glOpenClosePeriods.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_YEAR_END_CLOSING_ID) != null) { %>
menu[++i] = "Year-End Closing"; menu[++i] = "glYearEndClosing.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end journals menu

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_DEFINITION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_ORGANIZATION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_BUDGET_ID) != null) { %>

menu[++i] = "<b>Budget</b>"; menu[++i] = "# type=header;align left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_BUDGET_ID) != null) { %>
menu[++i] = "Budget"; menu[++i] = "show-menu=budget"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_DEFINITION_ID) != null) { %>
menu[++i] = "Budget Definition"; menu[++i] = "glBudgetDefinition.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_ORGANIZATION_ID) != null) { %>
menu[++i] = "Budget Organization"; menu[++i] = "glBudgetOrganization.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end budget menu

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_CHART_OF_ACCOUNTS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_CHART_OF_ACCOUNT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GEN_VALUE_SET_VALUE_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_COA_GENERATOR_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_CURRENCY_MAINTENANCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_DAILY_RATES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_DAILY_RATE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_PERIOD_TYPES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_LINES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_TRANSACTION_CALENDAR_LINES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_SOURCES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_CATEGORIES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_SUSPENSE_ACCOUNTS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ACCOUNT_ASSIGNMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_RESPONSIBILITY_ASSIGNMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FRG_FINANCIAL_REPORT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FRG_ROW_SET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FRG_COLUMN_SET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_INTERFACE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_INTERFACE_UPLOAD_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_STATIC_REPORT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_USER_STATIC_REPORT_ID) != null) { %>

menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_CHART_OF_ACCOUNTS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_CHART_OF_ACCOUNT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GEN_VALUE_SET_VALUE_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_COA_GENERATOR_ID) != null) { %>
menu[++i] = "Chart Of Accounts"; menu[++i] = "show-menu=chartofaccounts"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_CURRENCY_MAINTENANCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_DAILY_RATES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_FIND_DAILY_RATE_ID) != null) { %>
menu[++i] = "Currency"; menu[++i] = "show-menu=currency"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_PERIOD_TYPES_ID) != null) { %>
menu[++i] = "Period Types"; menu[++i] = "glPeriodTypes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_LINES_ID) != null) { %>
menu[++i] = "Accounting Calendar"; menu[++i] = "show-menu=accountingcalendar"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_TRANSACTION_CALENDAR_LINES_ID) != null) { %>
menu[++i] = "Transaction Calendar"; menu[++i] = "glTransactionCalendarLines.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_SOURCES_ID) != null) { %>
menu[++i] = "Journal Sources"; menu[++i] = "glJournalSources.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_CATEGORIES_ID) != null) { %>
menu[++i] = "Journal Categories"; menu[++i] = "glJournalCategories.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_SUSPENSE_ACCOUNTS_ID) != null) { %>
menu[++i] = "Suspense Accounts"; menu[++i] = "glSuspenseAccounts.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ACCOUNT_ASSIGNMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_RESPONSIBILITY_ASSIGNMENT_ID) != null) { %>
menu[++i] = "Organization"; menu[++i] = "show-menu=organization"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_TAX_INTERFACE_RUN_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_TAX_INTERFACE_MAINTENANCE_ID) != null) { %>
menu[++i] = "Tax Interface"; menu[++i] = "show-menu=taxinterface"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FRG_FINANCIAL_REPORT_ENTRY_ID) != null) { %>
menu[++i] = "Financial Report Generator"; menu[++i] = "show-menu=financial"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_INTERFACE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_INTERFACE_UPLOAD_ID) != null) { %>
menu[++i] = "Journal Interface Maintenance"; menu[++i] = "show-menu=journalinterfacemaintenance"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_STATIC_REPORT_ID) != null) { %>
menu[++i] = "Static Report"; menu[++i] = "glStaticReport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end administrative tools menu

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_TRIAL_BALANCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_INCOME_STATEMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_BALANCE_SHEET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_GENERAL_LEDGER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_INVESTOR_LEDGER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_FINANCIAL_REPORT_RUN_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_CHART_OF_ACCOUNT_LIST_ID) != null ||
     	Common.getUserPermission(sideBarUser, Constants.GL_REP_INCOME_TAX_RETURN_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_INCOME_TAX_WITHHELD_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_MONTHLY_VAT_DECLARATION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_QUARTERLY_VAT_RETURN_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_CSV_QUARTERLY_VAT_RETURN_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GL_REP_BUDGET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.GEN_REP_VALUE_SET_VALUE_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_REP_STATIC_REPORT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_REP_JOURNAL_REGISTER_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.GL_REP_DAILY_RATE_LIST_ID) != null) { %>

menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_FINANCIAL_REPORT_RUN_ID) != null) { %>
menu[++i] = "Financial Report Run"; menu[++i] = "glRepFinancialReportRun.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_TRIAL_BALANCE_ID) != null) { %>
menu[++i] = "Detail Trial Balance"; menu[++i] = "glRepDetailTrialBalance.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_INCOME_STATEMENT_ID) != null) { %>
menu[++i] = "Detail Income Statement"; menu[++i] = "glRepDetailIncomeStatement.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_DETAIL_BALANCE_SHEET_ID) != null) { %>
menu[++i] = "Detail Balance Sheet"; menu[++i] = "glRepDetailBalanceSheet.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_GENERAL_LEDGER_ID) != null) { %>
menu[++i] = "General Ledger"; menu[++i] = "glRepGeneralLedger.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_INVESTOR_LEDGER_ID) != null) { %>
menu[++i] = "Investor Ledger"; menu[++i] = "glRepInvestorLedger.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_JOURNAL_REGISTER_ID) != null) { %>
menu[++i] = "Journal Register"; menu[++i] = "glRepJournalRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_CHART_OF_ACCOUNT_LIST_ID) != null) { %>
menu[++i] = "Chart Of Account List"; menu[++i] = "glRepChartOfAccountList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_BUDGET_ID) != null) { %>
menu[++i] = "Budget"; menu[++i] = "glRepBudget.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_INCOME_TAX_RETURN_ID) != null) { %>
menu[++i] = "Income Tax Return"; menu[++i] = "glRepIncomeTaxReturn.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_INCOME_TAX_WITHHELD_ID) != null) { %>
menu[++i] = "Income Tax Withheld"; menu[++i] = "glRepIncomeTaxWithheld.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_MONTHLY_VAT_DECLARATION_ID) != null) { %>
menu[++i] = "Monthly VAT Declaration"; menu[++i] = "glRepMonthlyVatDeclaration.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_QUARTERLY_VAT_RETURN_ID) != null) { %>
menu[++i] = "Quarterly VAT Return"; menu[++i] = "glRepQuarterlyVatReturn.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_CSV_QUARTERLY_VAT_RETURN_ID) != null) { %>
menu[++i] = "Csv Quarterly VAT Return"; menu[++i] = "glRepCsvQuarterlyVatReturn.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GEN_REP_VALUE_SET_VALUE_LIST_ID) != null) { %>
menu[++i] = "Segment Value List"; menu[++i] = "genRepValueSetValueList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_STATIC_REPORT_ID) != null) { %>
menu[++i] = "Static Report"; menu[++i] = "glRepStaticReport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_REP_DAILY_RATE_LIST_ID) != null) { %>
menu[++i] = "Daily Rate List"; menu[++i] = "glRepDailyRateList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end reports/list menu
addmenu(menu);

// gl function/window journalbatch

i = 21;
menu=["journalbatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "glJournalBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_BATCH_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindJournalBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


// gl function/window journal

i = 21;
menu=["journal",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "glJournalEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindJournal.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_PRINT_ID) != null) { %>
menu[++i] = "Print / Edit"; menu[++i] = "glJournalBatchPrint.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "glJournalBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_BATCH_COPY_ID) != null) { %>
menu[++i] = "Copy"; menu[++i] = "glJournalBatchCopy.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


addmenu(menu);

// gl function/window recurring

i = 21;
menu=["recurring",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "glRecurringJournalEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_RECURRING_JOURNAL_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindRecurringJournal.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_RECURRING_JOURNAL_GENERATION_ID) != null) { %>
menu[++i] = "Generate"; menu[++i] = "glRecurringJournalGeneration.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window budget

i = 21;
menu=["budget",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_BUDGET_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "glBudgetEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_BUDGET_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindBudget.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window chart of accounts

i = 21;
menu=["chartofaccounts",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_CHART_OF_ACCOUNTS_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "glChartOfAccounts.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_CHART_OF_ACCOUNT_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindChartOfAccount.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GEN_VALUE_SET_VALUE_ID) != null) { %>
menu[++i] = "Segment Values"; menu[++i] = "genValueSetValue.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_COA_GENERATOR_ID) != null) { %>
menu[++i] = "Generate COA"; menu[++i] = "glCoaGenerator.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window accounting calendar

i = 21;
menu=["accountingcalendar",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_ID) != null) { %>
menu[++i] = "Accounting Calendar"; menu[++i] = "glAccountingCalendar.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ACCOUNTING_CALENDAR_LINES_ID) != null) { %>
menu[++i] = "Accounting Calendar Lines"; menu[++i] = "glAccountingCalendarLines.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


addmenu(menu);

// gl function/window currency

i = 21;
menu=["currency",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_CURRENCY_MAINTENANCE_ID) != null) { %>
menu[++i] = "Currency Maintenance"; menu[++i] = "glCurrencyMaintenance.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_DAILY_RATES_ID) != null) { %>
menu[++i] = "Daily Rates"; menu[++i] = "glDailyRates.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_DAILY_RATE_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindDailyRate.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window organization

i = 21;
menu=["organization",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ENTRY_ID) != null) { %>
menu[++i] = "Organizations"; menu[++i] = "glOrganizationEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_ACCOUNT_ASSIGNMENT_ID) != null) { %>
menu[++i] = "Account Assignments"; menu[++i] = "glOrganizationAccountAssignment.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_ORGANIZATION_RESPONSIBILITY_ASSIGNMENT_ID) != null) { %>
menu[++i] = "Responsibility Assignments"; menu[++i] = "glOrganizationResponsibilityAssignment.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window tax interface

i = 21;
menu=["taxinterface",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_TAX_INTERFACE_RUN_ID) != null) { %>
menu[++i] = "Run"; menu[++i] = "glTaxInterfaceRun.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_TAX_INTERFACE_MAINTENANCE_ID) != null) { %>
menu[++i] = "Maintenance"; menu[++i] = "glTaxInterfaceMaintenance.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window financial

i = 21;
menu=["financial",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FRG_FINANCIAL_REPORT_ENTRY_ID) != null) { %>
menu[++i] = "Financial Report";
menu[++i] = "glFrgFinancialReportEntry.do";
menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FRG_ROW_SET_ID) != null) { %>
menu[++i] = "Row Set"; menu[++i] = "glFrgRowSet.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FRG_COLUMN_SET_ID) != null) { %>
menu[++i] = "Column Set"; menu[++i] = "glFrgColumnSet.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// gl function/window journal interface maintenance

i = 21;
menu=["journalinterfacemaintenance",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_JOURNAL_INTERFACE_UPLOAD_ID) != null) { %>
menu[++i] = "Journal Interface Upload"; menu[++i] = "glJournalInterfaceUpload.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.GL_FIND_JOURNAL_INTERFACE_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "glFindJournalInterface.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

<% } else if (sideBarUser.getCurrentAppCode() == 2) { %> // end gl menu/submenu

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_CREDIT_MEMO_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_SUBMIT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_BATCH_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_IMPORT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_MISC_RECEIPT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_SUBMIT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_BATCH_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_IMPORT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_APPROVAL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_GL_JOURNAL_INTERFACE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_PDC_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_PDC_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_PDC_INVOICE_GENERATION_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_SALES_ORDER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_DELIVERY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_FIND_DELIVERY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_FIND_SALES_ORDER_ID) != null || 
		Common.getUserPermission(sideBarUser, Constants.AR_JOB_ORDER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_FIND_JOB_ORDER_ID) != null) { %>

menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_ENTRY_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_BATCH_ID) != null){ %>
menu[++i] = "Invoice Batch"; menu[++i] = "show-menu=invoicebatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_CREDIT_MEMO_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_PRINT_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_SUBMIT_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_IMPORT_ID) != null){ %>
menu[++i] = "Sales Invoice"; menu[++i] = "show-menu=invoice"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%	if (Common.getUserPermission(sideBarUser, Constants.AR_SALES_ORDER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_FIND_SALES_ORDER_ID) != null) { %>
menu[++i] = "Sales Order"; menu[++i] = "show-menu=salesorder"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_DELIVERY_ID) != null) { %>
menu[++i] = "Delivery"; menu[++i] = "arFindDelivery.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_BATCH_ID) != null){ %>
menu[++i] = "Receipt Batch"; menu[++i] = "show-menu=receiptbatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_MISC_RECEIPT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_PRINT_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_SUBMIT_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_COPY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_IMPORT_ID) != null) { %>
menu[++i] = "Collection"; menu[++i] = "show-menu=collection"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_JOB_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_JOB_ORDER_ID) != null) { %>
menu[++i] = "Job Order"; menu[++i] = "show-menu=jobOrder"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>




<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PDC_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_PDC_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_PDC_INVOICE_GENERATION_ID) != null) { %>
menu[++i] = "Post Dated Check"; menu[++i] = "show-menu=postdatedcheck"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_APPROVAL_ID) != null) { %>
menu[++i] = "Approval"; menu[++i] = "arApproval.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_POST_ID) != null) { %>
menu[++i] = "Posting"; menu[++i] = "show-menu=posting"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_GL_JOURNAL_INTERFACE_ID) != null) { %>
menu[++i] = "GL Journal Interface Run"; menu[++i] = "arGlJournalInterface.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end transaction menu

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_CUSTOMER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_TAX_CODE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_WITHHOLDING_TAX_CODE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_CLASS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_TYPE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_STANDARD_MEMO_LINE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_JOB_ORDER_TYPE_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AD_PAYMENT_TERM_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_SALESPERSON_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_PERSONEL_ID) != null ||
     	Common.getUserPermission(sideBarUser, Constants.AR_FIND_PERSONEL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_PERSONEL_TYPE_ID) != null 
       ) { %>

menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_CUSTOMER_ID) != null) { %>
menu[++i] = "Customer"; menu[++i] = "show-menu=customer"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_TAX_CODE_ID) != null) { %>
menu[++i] = "Tax Codes"; menu[++i] = "arTaxCodes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_WITHHOLDING_TAX_CODE_ID) != null) { %>
menu[++i] = "Withholding Tax Codes"; menu[++i] = "arWithholdingTaxCodes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_CLASS_ID) != null) { %>
menu[++i] = "Customer Classes"; menu[++i] = "arCustomerClasses.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_TYPE_ID) != null) { %>
menu[++i] = "Customer Types"; menu[++i] = "arCustomerTypes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_STANDARD_MEMO_LINE_ID) != null) { %>
menu[++i] = "Standard Memo Lines"; menu[++i] = "arStandardMemoLines.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_JOB_ORDER_TYPE_ID) != null) { %>
menu[++i] = "Job Order Type"; menu[++i] = "arJobOrderType.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_PAYMENT_TERM_ID) != null) { %>
menu[++i] = "Payment Terms"; menu[++i] = "adPaymentTerm.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_SALESPERSON_ID) != null) { %>
menu[++i] = "Salesperson"; menu[++i] = "arSalesperson.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PERSONEL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_FIND_PERSONEL_ID) != null) { %>
menu[++i] = "Personel"; menu[++i] = "show-menu=personel"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PERSONEL_TYPE_ID) != null) { %>
menu[++i] = "Personel Type"; menu[++i] = "arPersonelType.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end maintenance menu

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_STATEMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_AGING_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_ORDER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_JOB_STATUS_REPORT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_PERSONEL_SUMMARY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_OR_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_OUTPUT_TAX_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_CREDITABLE_WITHHOLDING_TAX_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_PERSONEL_SUMMARY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_TAX_CODE_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_WITHHOLDING_TAX_CODE_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_CLASS_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_TYPE_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AR_REP_STANDARD_MEMO_LINE_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_REP_PDC_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_REP_AGING_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AR_REP_DELIVERY_RECEIPT_ID) != null) { %>

menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_STATEMENT_ID) != null) { %>
menu[++i] = "Statement of Accounts"; menu[++i] = "arRepStatement.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_AGING_ID) != null) { %>
menu[++i] = "Aging Detail"; menu[++i] = "arRepAging.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_AGING_SUMMARY_ID) != null) { %>
menu[++i] = "Aging Summary"; menu[++i] = "arRepAgingSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_REGISTER_ID) != null) { %>
menu[++i] = "Sales Register"; menu[++i] = "arRepSalesRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_ORDER_ID) != null) { %>
menu[++i] = "Sales Order"; menu[++i] = "arRepSalesOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_JOB_STATUS_REPORT_ID) != null) { %>
menu[++i] = "Job Status Report"; menu[++i] = "arRepJobStatusReport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_PERSONEL_SUMMARY_ID) != null) { %>
menu[++i] = "Personel Summary"; menu[++i] = "arRepPersonelSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_OR_REGISTER_ID) != null) { %>
menu[++i] = "OR Register"; menu[++i] = "arRepOrRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_OUTPUT_TAX_ID) != null) { %>
menu[++i] = "Output Tax"; menu[++i] = "arRepOutputTax.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_CREDITABLE_WITHHOLDING_TAX_ID) != null) { %>
menu[++i] = "Creditable Withholding Tax"; menu[++i] = "arRepCreditableWithholdingTax.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_LIST_ID) != null) { %>
menu[++i] = "Customer List"; menu[++i] = "arRepCustomerList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_SALES_ID) != null) { %>
menu[++i] = "Item Sales"; menu[++i] = "arRepSales.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_TAX_CODE_LIST_ID) != null) { %>
menu[++i] = "Tax Code List"; menu[++i] = "arRepTaxCodeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_WITHHOLDING_TAX_CODE_LIST_ID) != null) { %>
menu[++i] = "Withholding Tax Code List"; menu[++i] = "arRepWithholdingTaxCodeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_CLASS_LIST_ID) != null) { %>
menu[++i] = "Customer Class List"; menu[++i] = "arRepCustomerClassList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_CUSTOMER_TYPE_LIST_ID) != null) { %>
menu[++i] = "Customer Type List"; menu[++i] = "arRepCustomerTypeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_STANDARD_MEMO_LINE_LIST_ID) != null) { %>
menu[++i] = "Standard Memo Line List"; menu[++i] = "arRepStandardMemoLineList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_PDC_ID) != null) { %>
menu[++i] = "Post Dated Check List"; menu[++i] = "arRepPdc.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_SALESPERSON_ID) != null) { %>
menu[++i] = "Salesperson Report"; menu[++i] = "arRepSalesperson.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_REP_DELIVERY_RECEIPT_ID) != null) { %>
menu[++i] = "Delivery Receipt Report"; menu[++i] = "arRepDeliveryReceipt.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end report/list menu

addmenu(menu);

// ar function/window customer

i = 21;
menu=["customer",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]


<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CUSTOMER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arCustomerEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_CUSTOMER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindCustomer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window invoice batch

i = 21;
menu=["invoicebatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arInvoiceBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_BATCH_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindInvoiceBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window invoice

i = 21;
menu=["invoice",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_ENTRY_ID) != null) { %>
menu[++i] = "New Invoice"; menu[++i] = "arInvoiceEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  //if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_ENTRY_ID) != null) { %>
//menu[++i] = "Upload Invoice Items"; menu[++i] = "arUploadInvoiceItemEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% //} %>



<%  //if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_ENTRY_ID) != null) { %>
//menu[++i] = "Upload Invoice Memo Lines"; menu[++i] = "arInvoiceEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% //} %>


<%  if (Common.getUserPermission(sideBarUser, Constants.AR_CREDIT_MEMO_ENTRY_ID) != null) { %>
menu[++i] = "New Credit Memo"; menu[++i] = "arCreditMemoEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_INVOICE_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindInvoice.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_PRINT_ID) != null) { %>
menu[++i] = "Print / Edit"; menu[++i] = "arInvoiceBatchPrint.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "arInvoiceBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_BATCH_COPY_ID) != null) { %>
menu[++i] = "Copy"; menu[++i] = "arInvoiceBatchCopy.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_IMPORT_ID) != null) { %>
menu[++i] = "Import"; menu[++i] = "arInvoiceImport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window sales order

i = 21;
menu=["salesorder",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<% if (Common.getUserPermission(sideBarUser, Constants.AR_SALES_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arSalesOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_SALES_ORDER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindSalesOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window collection batch

i = 21;
menu=["receiptbatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arReceiptBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_BATCH_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindReceiptBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window collection

i = 21;
menu=["collection",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_ENTRY_ID) != null) { %>
menu[++i] = "New Receipt"; menu[++i] = "arReceiptEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_MISC_RECEIPT_ENTRY_ID) != null) { %>
menu[++i] = "New Misc Receipt"; menu[++i] = "arMiscReceiptEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_RECEIPT_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindReceipt.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_PRINT_ID) != null) { %>
menu[++i] = "Print / Edit"; menu[++i] = "arReceiptBatchPrint.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "arReceiptBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_BATCH_COPY_ID) != null) { %>
menu[++i] = "Copy"; menu[++i] = "arReceiptBatchCopy.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_IMPORT_ID) != null) { %>
menu[++i] = "Import"; menu[++i] = "arReceiptImport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


//ar function/window job order

i = 21;
menu=["jobOrder",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_JOB_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arJobOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_JOB_ORDER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindJobOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>



addmenu(menu);

//ar function/window personel

i = 21;
menu=["personel",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PERSONEL_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arPersonel.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_PERSONEL_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindPersonel.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>



addmenu(menu);






// ar function/window post dated check

i = 21;
menu=["postdatedcheck",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PDC_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "arPdcEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_FIND_PDC_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "arFindPdc.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_PDC_INVOICE_GENERATION_ID) != null) { %>
menu[++i] = "Generate Receipt"; menu[++i] = "arPdcInvoiceGeneration.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ar function/window posting

i = 21;
menu=["posting",,,150,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_INVOICE_POST_ID) != null) { %>
menu[++i] = "Invoice/Credit Memo"; menu[++i] = "arInvoicePost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AR_RECEIPT_POST_ID) != null) { %>
menu[++i] = "Receipt"; menu[++i] = "arReceiptPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

<% } else if (sideBarUser.getCurrentAppCode() == 3) {%> // end ar menu/submenu


<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_ENTRY_ID) != null ||
	    Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_BATCH_ID) != null ||
	    Common.getUserPermission(sideBarUser, Constants.AP_CHECK_PAYMENT_REQUEST_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_DEBIT_MEMO_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_SUBMIT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_IMPORT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_GENERATION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_RECURRING_VOUCHER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_BATCH_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_PAYMENT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_DIRECT_CHECK_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_SUBMIT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_CHECK_IMPORT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_APPROVAL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_GL_JOURNAL_INTERFACE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_ORDER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_RECEIVING_ITEM_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_REQUISITION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_REQUISITION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_ANNUAL_PROCUREMENT_UPLOAD) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_GENERATE_PURCHASE_REQUISITION_ID) != null) { %>

menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_BATCH_ID) != null) { %>
menu[++i] = "Voucher Batch"; menu[++i] = "show-menu=voucherbatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_PAYMENT_REQUEST_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_DEBIT_MEMO_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_SUBMIT_ID) != null||
        Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_IMPORT_ID) != null) { %>
menu[++i] = "Voucher"; menu[++i] = "show-menu=voucher"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_REQUISITION_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_REQUISITION_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_GENERATE_PURCHASE_REQUISITION_ID) != null) { %>
menu[++i] = "Purchase Requisition"; menu[++i] = "show-menu=purchaseRequisition"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_ORDER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_RECEIVING_ITEM_ID) != null) { %>
menu[++i] = "Purchase Order"; menu[++i] = "show-menu=purchaseOrder"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_RECURRING_VOUCHER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_GENERATION_ID) != null) { %>
menu[++i] = "Recurring Voucher"; menu[++i] = "show-menu=recurringVoucher"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_BATCH_ID) != null) { %>
menu[++i] = "Payment Batch"; menu[++i] = "show-menu=paymentbatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PAYMENT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_DIRECT_CHECK_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_PRINT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_SUBMIT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_COPY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_IMPORT_ID) != null) { %>
menu[++i] = "Payment"; menu[++i] = "show-menu=payment"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_APPROVAL_ID) != null) { %>
menu[++i] = "Approval"; menu[++i] = "apApproval.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_CHECK_POST_ID) != null) { %>
menu[++i] = "Posting"; menu[++i] = "show-menu=posting"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_ANNUAL_PROCUREMENT_UPLOAD) != null) { %>
menu[++i] = "Annual Procurement Plan Upload"; menu[++i] = "apAnnualProcurementUpload.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_GL_JOURNAL_INTERFACE_ID) != null) { %>
menu[++i] = "GL Journal Interface Run"; menu[++i] = "apGlJournalInterface.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<% } %> // end transaction menu

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_SUPPLIER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_TAX_CODE_ID) != null  ||
        Common.getUserPermission(sideBarUser, Constants.AP_WITHHOLDING_TAX_CODE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_CLASS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_TYPE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_PAYMENT_TERM_ID) != null) { %>
menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_FIND_SUPPLIER_ID) != null) { %>
menu[++i] = "Supplier"; menu[++i] = "show-menu=supplier"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_TAX_CODE_ID) != null) { %>
menu[++i] = "Tax Codes"; menu[++i] = "apTaxCodes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_WITHHOLDING_TAX_CODE_ID) != null) { %>
menu[++i] = "Withholding Tax Codes"; menu[++i] = "apWithholdingTaxCodes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_CLASS_ID) != null) { %>
menu[++i] = "Supplier Classes"; menu[++i] = "apSupplierClasses.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_TYPE_ID) != null) { %>
menu[++i] = "Supplier Types"; menu[++i] = "apSupplierTypes.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_PAYMENT_TERM_ID) != null) { %>
menu[++i] = "Payment Terms"; menu[++i] = "adPaymentTerm.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end maintenance menu

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_AGING_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_AP_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_CHECK_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_INPUT_TAX_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_WITHHOLDING_TAX_EXPANDED_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_TAX_CODE_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_WITHHOLDING_TAX_CODE_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_CLASS_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_TYPE_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_REP_AGING_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AP_REP_RECEIVING_ITEMS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_PURCHASE_ORDER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_ANNUAL_PROCUREMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AP_REP_PURCHASE_REQUISITION_REGISTER_ID) != null) { %>


menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_AGING_ID) != null) { %>
menu[++i] = "Aging Detail"; menu[++i] = "apRepAging.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_AGING_SUMMARY_ID) != null) { %>
menu[++i] = "Aging Summary"; menu[++i] = "apRepAgingSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_AP_REGISTER_ID) != null) { %>
menu[++i] = "AP Register"; menu[++i] = "apRepApRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_ANNUAL_PROCUREMENT_ID) != null) { %>
menu[++i] = "AP Annual Procurement Plan"; menu[++i] = "apRepAnnualProcurement.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_CHECK_REGISTER_ID) != null) { %>
menu[++i] = "Check Register"; menu[++i] = "apRepCheckRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_INPUT_TAX_ID) != null) { %>
menu[++i] = "Input Tax"; menu[++i] = "apRepInputTax.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_WITHHOLDING_TAX_EXPANDED_ID) != null) { %>
menu[++i] = "Withholding Tax Expanded"; menu[++i] = "apRepWithholdingTaxExpanded.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_LIST_ID) != null) { %>
menu[++i] = "Supplier List"; menu[++i] = "apRepSupplierList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_TAX_CODE_LIST_ID) != null) { %>
menu[++i] = "Tax Code List"; menu[++i] = "apRepTaxCodeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_WITHHOLDING_TAX_CODE_LIST_ID) != null) { %>
menu[++i] = "Withholding Tax Code List"; menu[++i] = "apRepWithholdingTaxCodeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_CLASS_LIST_ID) != null) { %>
menu[++i] = "Supplier Class List"; menu[++i] = "apRepSupplierClassList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_SUPPLIER_TYPE_LIST_ID) != null) { %>
menu[++i] = "Supplier Type List"; menu[++i] = "apRepSupplierTypeList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_PURCHASE_ORDER_ID) != null) { %>
menu[++i] = "Purchase Order"; menu[++i] = "apRepPurchaseOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_RECEIVING_ITEMS_ID) != null) { %>
menu[++i] = "Receiving Items"; menu[++i] = "apRepReceivingItems.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_REP_PURCHASE_REQUISITION_REGISTER_ID) != null) { %>
menu[++i] = "Purchase Requisition Register"; menu[++i] = "apRepPurchaseRequisitionRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end report/list menu
addmenu(menu);

// ap function/window voucher batch

i = 21;
menu=["voucherbatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "apVoucherBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_BATCH_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindVoucherBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window voucher

i = 21;
menu=["voucher",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_PAYMENT_REQUEST_ENTRY_ID) != null) { %>
menu[++i] = "New Check Payment Request"; menu[++i] = "apCheckPaymentRequestEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_ENTRY_ID) != null) { %>
menu[++i] = "New Voucher"; menu[++i] = "apVoucherEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_DEBIT_MEMO_ENTRY_ID) != null) { %>
menu[++i] = "New Debit Memo"; menu[++i] = "apDebitMemoEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_VOUCHER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindVoucher.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_PRINT_ID) != null) { %>
menu[++i] = "Print / Edit"; menu[++i] = "apVoucherBatchPrint.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "apVoucherBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_BATCH_COPY_ID) != null) { %>
menu[++i] = "Copy"; menu[++i] = "apVoucherBatchCopy.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_IMPORT_ID) != null) { %>
menu[++i] = "Import"; menu[++i] = "apVoucherImport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window purchase requisition

i = 21;
menu=["purchaseRequisition",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_REQUISITION_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "apPurchaseRequisitionEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_REQUISITION_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindPurchaseRequisition.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_GENERATE_PURCHASE_REQUISITION_ID) != null) { %>
menu[++i] = "Generate"; menu[++i] = "apGeneratePurchaseRequisition.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window purchase order

i = 21;
menu=["purchaseOrder",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PURCHASE_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New PO"; menu[++i] = "apPurchaseOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_RECEIVING_ITEM_ID) != null) { %>
menu[++i] = "New Receiving Item"; menu[++i] = "apReceivingItemEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_PURCHASE_ORDER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindPurchaseOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


// ap function/window recurring voucher

i = 21;
menu=["recurringVoucher",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]


<%  if (Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "apRecurringVoucherEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_RECURRING_VOUCHER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindRecurringVoucher.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_RECURRING_VOUCHER_GENERATION_ID) != null) { %>
menu[++i] = "Generate"; menu[++i] = "apRecurringVoucherGeneration.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window payment batch

i = 21;
menu=["paymentbatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "apCheckBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_BATCH_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindCheckBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window payment

i = 21;
menu=["payment",,,120,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_PAYMENT_ENTRY_ID) != null) { %>
menu[++i] = "New Payment"; menu[++i] = "apPaymentEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_DIRECT_CHECK_ENTRY_ID) != null) { %>
menu[++i] = "New Direct Check"; menu[++i] = "apDirectCheckEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_CHECK_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindCheck.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_PRINT_ID) != null) { %>
menu[++i] = "Print / Edit"; menu[++i] = "apCheckBatchPrint.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "apCheckBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_BATCH_COPY_ID) != null) { %>
menu[++i] = "Copy"; menu[++i] = "apCheckBatchCopy.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_IMPORT_ID) != null) { %>
menu[++i] = "Import"; menu[++i] = "apCheckImport.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


// ap function/window posting

i = 21;
menu=["posting",,,150,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_VOUCHER_POST_ID) != null) { %>
menu[++i] = "Voucher/Debit Memo"; menu[++i] = "apVoucherPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_CHECK_POST_ID) != null) { %>
menu[++i] = "Check"; menu[++i] = "apCheckPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// ap function/window supplier

i = 21;
menu=["supplier",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]


<%  if (Common.getUserPermission(sideBarUser, Constants.AP_SUPPLIER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "apSupplierEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AP_FIND_SUPPLIER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "apFindSupplier.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

<% } else if (sideBarUser.getCurrentAppCode() == 4) {%> // end ap menu/submenu

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_BRANCH_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AD_RESPONSIBILITY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_USER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_CHANGE_PASSWORD_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_USER_SESSION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_PREFERENCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_COMPANY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCE_ASSIGNMENTS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_CATEGORY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_APPROVAL_SETUP_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_LOOK_UP_VALUES_ID) != null) { %>
menu[++i] = "<b>System Admin</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<% if (Common.getUserPermission(sideBarUser, Constants.AD_BRANCH_ID) != null) { %>
menu[++i] = "Branch"; menu[++i] = "adBranch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AD_RESPONSIBILITY_ID) != null) { %>
menu[++i] = "Responsibility"; menu[++i] = "adResponsibility.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AD_USER_ID) != null) { %>
menu[++i] = "User"; menu[++i] = "adUser.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AD_CHANGE_PASSWORD_ID) != null) { %>
menu[++i] = "Change Password"; menu[++i] = "adChangePassword.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AD_USER_SESSION_ID) != null) { %>
menu[++i] = "User Sessions"; menu[++i] = "adUserSession.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_PREFERENCE_ID) != null) { %>
menu[++i] = "Global Preference"; menu[++i] = "adPreference.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_COMPANY_ID) != null) { %>
menu[++i] = "Company"; menu[++i] = "adCompany.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_APPROVAL_SETUP_ID) != null) { %>
menu[++i] = "Approval Setup"; menu[++i] = "adApprovalSetup.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_STORED_PROCEDURE_SETUP_ID) != null) { %>
menu[++i] = "Stored Procedure Setup"; menu[++i] = "adStoredProcedureSetup.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCE_ASSIGNMENTS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_CATEGORY_ID) != null) { %>
menu[++i] = "Document Sequences"; menu[++i] = "show-menu=documentsequences"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_LOOK_UP_VALUES_ID) != null) { %>
menu[++i] = "Look Up"; menu[++i] = "adLookUpValue.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end system admin menu

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_TRANSACTION_LOG_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AD_REP_TRANSACTION_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AD_REP_RESPONSIBILITY_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AD_REP_USER_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.AD_REP_APPROVAL_LIST_ID) != null) { %>
menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_TRANSACTION_LOG_ID) != null) { %>
menu[++i] = "Transaction Log"; menu[++i] = "adRepTransactionLog.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_TRANSACTION_SUMMARY_ID) != null) { %>
menu[++i] = "Transaction Summary"; menu[++i] = "adRepTransactionSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_RESPONSIBILITY_LIST_ID) != null) { %>
menu[++i] = "Responsibility List"; menu[++i] = "adRepResponsibilityList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_USER_LIST_ID) != null) { %>
menu[++i] = "User List"; menu[++i] = "adRepUserList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_REP_APPROVAL_LIST_ID) != null) { %>
menu[++i] = "Approval List"; menu[++i] = "adRepApprovalList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end report/list menu

addmenu(menu);

// ad function/window document sequences

i = 21;
menu=["documentsequences",,,180,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCES_ID) != null) { %>
menu[++i] = "Document Sequences"; menu[++i] = "adDocumentSequences.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_SEQUENCE_ASSIGNMENTS_ID) != null) { %>
menu[++i] = "Document Sequences Assign"; menu[++i] = "adDocumentSequenceAssignments.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.AD_DOCUMENT_CATEGORY_ID) != null) { %>
menu[++i] = "Document Categories"; menu[++i] = "adDocumentCategories.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


<% } else if (sideBarUser.getCurrentAppCode() == 5) {%> // end sa menu/submenu

<% if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_ENTRY_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_FIND_ADJUSTMENT_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_ENTRY_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_FIND_FUND_TRANSFER_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_BANK_RECONCILIATION_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_GL_JOURNAL_INTERFACE_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_APPROVAL_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_POST_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_POST_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_BATCH_SUBMIT_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_BATCH_SUBMIT_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.CM_RELEASING_CHECKS_ID) != null) { %>
menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.CM_FIND_ADJUSTMENT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Adjustment"; menu[++i] = "show-menu=adjustment"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.CM_FIND_FUND_TRANSFER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Fund Transfer"; menu[++i] = "show-menu=fundtransfer"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_BANK_RECONCILIATION_ID) != null) { %>
menu[++i] = "Bank Reconciliation"; menu[++i] = "cmBankReconciliation.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_RELEASING_CHECKS_ID) != null) { %>
menu[++i] = "Releasing Checks"; menu[++i] = "cmReleasingChecks.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_APPROVAL_ID) != null) { %>
menu[++i] = "Approval"; menu[++i] = "cmApproval.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_POST_ID) != null) { %>
menu[++i] = "Posting"; menu[++i] = "show-menu=posting"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_GL_JOURNAL_INTERFACE_ID) != null) { %>
menu[++i] = "GL Journal Interface Run"; menu[++i] = "cmGlJournalInterface.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end transaction menu

<% if (Common.getUserPermission(sideBarUser, Constants.AD_BANK_ID) != null ||
       Common.getUserPermission(sideBarUser, Constants.AD_BANK_ACCOUNT_ID) != null) { %>
menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<% if (Common.getUserPermission(sideBarUser, Constants.AD_BANK_ID) != null) { %>
menu[++i] = "Banks"; menu[++i] = "adBank.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.AD_BANK_ACCOUNT_ID) != null) { %>
menu[++i] = "Bank Accounts"; menu[++i] = "adBankAccount.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<% } %> // end maintenance menu

addmenu(menu);

<% if (Common.getUserPermission(sideBarUser, Constants.AD_REP_BANK_ACCOUNT_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_DEPOSIT_LIST_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_RELEASED_CHECKS_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_CASH_POSITION_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_POSITION_DETAIL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_POSITION_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_FORECAST_DETAIL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_FORECAST_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_WEEKLY_CASH_FORECAST_DETAIL_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_WEEKLY_CASH_FORECAST_SUMMARY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.CM_REP_BANK_RECONCILIATION_ID) != null ) { %>
menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<% if (Common.getUserPermission(sideBarUser, Constants.AD_REP_BANK_ACCOUNT_LIST_ID) != null) { %>
menu[++i] = "Bank Account List"; menu[++i] = "adRepBankAccountList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_BANK_RECONCILIATION_ID) != null) { %>
menu[++i] = "Bank Reconciliation"; menu[++i] = "cmRepBankReconciliation.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_DEPOSIT_LIST_ID) != null) { %>
menu[++i] = "Deposit List"; menu[++i] = "cmRepDepositList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_RELEASED_CHECKS_ID) != null) { %>
menu[++i] = "Released Checks List"; menu[++i] = "cmRepReleasedChecks.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_CASH_POSITION_ID) != null) { %>
menu[++i] = "Cash Position"; menu[++i] = "cmRepCashPosition.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_POSITION_DETAIL_ID) != null) { %>
menu[++i] = "Daily Cash Position Detail"; menu[++i] = "cmRepDailyCashPositionDetail.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_POSITION_SUMMARY_ID) != null) { %>
menu[++i] = "Daily Cash Position Summary"; menu[++i] = "cmRepDailyCashPositionSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_FORECAST_DETAIL_ID) != null) { %>
menu[++i] = "Daily Cash Forecast Detail"; menu[++i] = "cmRepDailyCashForecastDetail.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_DAILY_CASH_FORECAST_SUMMARY_ID) != null) { %>
menu[++i] = "Daily Cash Forecast Summary"; menu[++i] = "cmRepDailyCashForecastSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_WEEKLY_CASH_FORECAST_DETAIL_ID) != null) { %>
menu[++i] = "W-Cash Forecast Detail"; menu[++i] = "cmRepWeeklyCashForecastDetail.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_REP_WEEKLY_CASH_FORECAST_SUMMARY_ID) != null) { %>
menu[++i] = "W-Cash Forecast Summary"; menu[++i] = "cmRepWeeklyCashForecastSummary.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<% } %> // end reports/list menu

addmenu(menu);

// cm function/window adjustment

i = 21;
menu=["adjustment",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<% if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "cmAdjustmentEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_FIND_ADJUSTMENT_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "cmFindAdjustment.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "cmAdjustmentBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// cm function/window fund transfer

i = 21;
menu=["fundtransfer",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<% if (Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "cmFundTransferEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_FIND_FUND_TRANSFER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "cmFindFundTransfer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_BATCH_SUBMIT_ID) != null) { %>
menu[++i] = "Submit"; menu[++i] = "cmFundTransferBatchSubmit.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// cm function/window posting

i = 21;
menu=["posting",,,150,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<% if (Common.getUserPermission(sideBarUser, Constants.CM_ADJUSTMENT_POST_ID) != null) { %>
menu[++i] = "Adjustment"; menu[++i] = "cmAdjustmentPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.CM_FUND_TRANSFER_POST_ID) != null) { %>
menu[++i] = "Fund Transfer"; menu[++i] = "cmFundTransferPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

<% } else if (sideBarUser.getCurrentAppCode() == 6) { %> // end cm menu/submenu

// inv menu/submenu

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_REQUEST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ADJUSTMENT_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_STOCK_TRANSFER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_TRANSFER_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_PHYSICAL_INVENTORY_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_PHYSICAL_INVENTORY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_APPROVAL_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_GL_JOURNAL_INTERFACE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_OVERHEAD_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_OVERHEAD_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_BUILD_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_ORDER_ID) != null ||
  		Common.getUserPermission(sideBarUser, Constants.INV_STOCK_ISSUANCE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_ISSUANCE_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_TRANSFER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ASSEMBLY_TRANSFER_ID) != null ||
       	Common.getUserPermission(sideBarUser, Constants.INV_TRANSACTIONAL_BUDGET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_OUT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_IN_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BRANCH_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ADJUSTMENT_ID) != null) { %>
menu[++i] = "Adjustment"; menu[++i] = "show-menu=adjustment"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_BATCH_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID) != null) { %>
menu[++i] = "Build Unbuild Assembly Batch"; menu[++i] = "show-menu=buildUnbuildAssemblyBatch"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>



<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ORDER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID) != null) { %>
menu[++i] = "Build Unbuild Assembly"; menu[++i] = "show-menu=buildUnbuildAssembly"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_PHYSICAL_INVENTORY_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_PHYSICAL_INVENTORY_ID) != null) { %>
menu[++i] = "Physical Inventory"; menu[++i] = "show-menu=physicalInventory"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_OVERHEAD_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_OVERHEAD_ID) != null) { %>
menu[++i] = "Overhead"; menu[++i] = "show-menu=overhead"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_ORDER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_ORDER_ID) != null) { %>
menu[++i] = "Build Order"; menu[++i] = "show-menu=buildOrder"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_STOCK_ISSUANCE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_ISSUANCE_ID) != null) { %>
menu[++i] = "Stock Issuance"; menu[++i] = "show-menu=stockIssuance"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_TRANSFER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ASSEMBLY_TRANSFER_ID) != null) { %>
menu[++i] = "Assembly Transfer"; menu[++i] = "show-menu=assemblyTransfer"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_STOCK_TRANSFER_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "Stock Transfer"; menu[++i] = "show-menu=stockTransfer"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_OUT_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_IN_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_BRANCH_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "Branch Stock Transfer"; menu[++i] = "show-menu=branchStockTransfer"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%//  if (Common.getUserPermission(sideBarUser, Constants.INV_TRANSACTIONAL_BUDGET_ID) != null) { %>
//menu[++i] = "Transactional Budget"; menu[++i] = "show-menu=transactionalBudget"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<%// } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_APPROVAL_ID) != null) { %>
menu[++i] = "Approval"; menu[++i] = "invApproval.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_POST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_POST_ID) != null) { %>
menu[++i] = "Posting"; menu[++i] = "show-menu=posting"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% if (Common.getUserPermission(sideBarUser, Constants.INV_GL_JOURNAL_INTERFACE_ID) != null) { %>
menu[++i] = "GL Journal Interface Run"; menu[++i] = "invGlJournalInterface.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

<% } %> // end transaction menu

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ITEM_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_ITEM_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_LOCATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_LOCATION_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_ITEM_LOCATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_LOCATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_UNIT_OF_MEASURE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_OVERHEAD_MEMO_LINE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_LINE_ITEM_TEMPLATE_ID) != null) { %>
menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ITEM_ENTRY_ID) != null ||
		Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_ITEM_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_ID) != null) { %>
menu[++i] = "Item"; menu[++i] = "show-menu=item"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_LOCATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_LOCATION_ID) != null) { %>
menu[++i] = "Location"; menu[++i] = "show-menu=location"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ITEM_LOCATION_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_LOCATION_ENTRY_ID) != null) { %>
menu[++i] = "Item Location"; menu[++i] = "show-menu=itemLocation"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_UNIT_OF_MEASURE_ID) != null) { %>
menu[++i] = "Unit Of Measures"; menu[++i] = "invUnitOfMeasures.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_OVERHEAD_MEMO_LINE_ID) != null) { %>
menu[++i] = "Overhead Memo Lines"; menu[++i] = "invOverheadMemoLines.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_LINE_ITEM_TEMPLATE_ID) != null) { %>
menu[++i] = "Item Templates"; menu[++i] = "invLineItemTemplate.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<% } %> // end maintenance menu

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_USAGE_VARIANCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_COSTING_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_ASSEMBLY_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_COUNT_VARIANCE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_COST_OF_SALES_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_INVENTORY_PROFITABILITY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_REORDER_ITEMS_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_ON_HAND_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_BUILD_UNBUILD_ASSEMBLY_ORDER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_INVENTORY_LIST_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_TRANSFER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_PHYSICAL_INVENTORY_WORKSHEET_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_CARD_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_LEDGER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_ADJUSTMENT_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_BRANCH_STOCK_TRANSFER_REGISTER_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.INV_REP_MARKUP_LIST_ID) != null) { %>


menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_INVENTORY_PROFITABILITY_ID) != null) { %>
menu[++i] = "Inventory Profitability"; menu[++i] = "invRepInventoryProfitability.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_COSTING_ID) != null) { %>
menu[++i] = "Item Costing Report"; menu[++i] = "invRepItemCosting.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_COST_OF_SALES_ID) != null) { %>
menu[++i] = "Cost Of Sales"; menu[++i] = "invRepCostOfSales.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_ADJUSTMENT_REGISTER_ID) != null) { %>
menu[++i] = "Adjustment Register"; menu[++i] = "invRepAdjustmentRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_BRANCH_STOCK_TRANSFER_REGISTER_ID) != null) { %>
menu[++i] = "Branch Stock Transfer Register"; menu[++i] = "invRepBranchStockTransferRegister.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "Stock Transfer"; menu[++i] = "invRepStockTransfer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_REORDER_ITEMS_ID) != null) { %>
menu[++i] = "Reorder Items"; menu[++i] = "invRepReorderItems.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_ON_HAND_ID) != null) { %>
menu[++i] = "Stock On Hand"; menu[++i] = "invRepStockOnHand.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_BUILD_UNBUILD_ASSEMBLY_ORDER_ID) != null) { %>
menu[++i] = "Build Unbuild Assembly Order"; menu[++i] = "invRepBuildUnbuildAssemblyOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_INVENTORY_LIST_ID) != null) { %>
menu[++i] = "Inventory List"; menu[++i] = "invRepInventoryList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_MARKUP_LIST_ID) != null) { %>
menu[++i] = "Inventory Markup List"; menu[++i] = "invRepMarkupList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_LIST_ID) != null) { %>
menu[++i] = "Item List"; menu[++i] = "invRepItemList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_ASSEMBLY_LIST_ID) != null) { %>
menu[++i] = "Assembly List"; menu[++i] = "invRepAssemblyList.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_PHYSICAL_INVENTORY_WORKSHEET_ID) != null) { %>
menu[++i] = "Physical Inventory Worksheet"; menu[++i] = "invRepPhysicalInventoryWorksheet.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_STOCK_CARD_ID) != null) { %>
menu[++i] = "Stock Card"; menu[++i] = "invRepStockCard.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_REP_ITEM_LEDGER_ID) != null) { %>
menu[++i] = "Item Ledger"; menu[++i] = "invRepItemLedger.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<% } %> // end reports/list menu

addmenu(menu);

// inv function/window item

i = 21;
menu=["item",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ITEM_ENTRY_ID) != null) { %>
menu[++i] = "New Item"; menu[++i] = "invItemEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_ITEM_ENTRY_ID) != null) { %>
menu[++i] = "New Assembly"; menu[++i] = "invAssemblyItemEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindItem.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window location

i = 21;
menu=["location",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_LOCATION_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invLocationEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_LOCATION_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindLocation.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window item location

i = 21;
menu=["itemLocation",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ITEM_LOCATION_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invItemLocationEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_ITEM_LOCATION_ENTRY_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindItemLocation.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


//inv function/window buildUnbuildAssembly

i = 21;
menu=["buildUnbuildAssemblyBatch",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invBuildUnbuildAssemblyBatchEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindBuildUnbuildAssemblyBatch.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


// inv function/window buildUnbuildAssembly

i = 21;
menu=["buildUnbuildAssembly",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New Order"; menu[++i] = "invBuildUnbuildAssemblyOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invBuildUnbuildAssemblyEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_UNBUILD_ASSEMBLY_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindBuildUnbuildAssembly.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window adjustment

i = 21;
menu=["adjustment",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

 <%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_REQUEST_ID) != null) { %>
  menu[++i] = "New Request"; menu[++i] = "invAdjustmentRequest.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
   <% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invAdjustmentEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>


<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_ADJUSTMENT_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindAdjustment.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window physicalInventory

i = 21;
menu=["physicalInventory",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_PHYSICAL_INVENTORY_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invPhysicalInventoryEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_PHYSICAL_INVENTORY_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindPhysicalInventory.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window overhead

i = 21;
menu=["overhead",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_OVERHEAD_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invOverheadEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_OVERHEAD_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindOverhead.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window buildOrder

i = 21;
menu=["buildOrder",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invBuildOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_BUILD_ORDER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindBuildOrder.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window stockIssuance

i = 21;
menu=["stockIssuance",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_STOCK_ISSUANCE_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invStockIssuanceEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_ISSUANCE_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindStockIssuance.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window assemblyTransfer

i = 21;
menu=["assemblyTransfer",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ASSEMBLY_TRANSFER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invAssemblyTransferEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_ASSEMBLY_TRANSFER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindAssemblyTransfer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window stockTransfer

i = 21;
menu=["stockTransfer",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_STOCK_TRANSFER_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invStockTransferEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindStockTransfer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


// inv function/window branchStockTransfer

i = 21;
menu=["branchStockTransfer",,,110,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]
// null?
<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_ORDER_ENTRY_ID) != null) { %>
menu[++i] = "New Transfer Order"; menu[++i] = "invBranchStockTransferOrderEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_OUT_ENTRY_ID) != null) { %>
menu[++i] = "New Transfer-Out"; menu[++i] = "invBranchStockTransferOutEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BRANCH_STOCK_TRANSFER_IN_ENTRY_ID) != null) { %>
menu[++i] = "New Transfer-In"; menu[++i] = "invBranchStockTransferInEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_FIND_BRANCH_STOCK_TRANSFER_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invFindBranchStockTransfer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

//inv transactional budget

i = 21;
menu=["transactionalBudget",,,150,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_TRANSACTIONAL_BUDGET_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "invTransactionalBudgetEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_POST_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "invTransactionalBudgetFind.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window posting

i = 21;
menu=["posting",,,150,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_ADJUSTMENT_POST_ID) != null) { %>
menu[++i] = "Adjustment"; menu[++i] = "invAdjustmentPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.INV_BUILD_UNBUILD_ASSEMBLY_POST_ID) != null) { %>
menu[++i] = "Build/Unbuild Assembly"; menu[++i] = "invBuildUnbuildAssemblyPost.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);


<% } else if (sideBarUser.getCurrentAppCode() == 7) { %> // end inv menu/submenu

// pr menu/submenu

<%  if (true) { %>


addmenu(menu);

menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;

<% } %> // end transaction menu

<%  if (true) { %>

menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;
<% } %> // end maintenance menu




<%  if (Common.getUserPermission(sideBarUser, Constants.HR_SYNCHRONIZER) != null) { %>
menu[++i] = "Synchronizer"; menu[++i] = "hrSynchronizer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>





<%  if (true) { %>

menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<% } %> // end reports/list menu

addmenu(menu);





<% } else if (sideBarUser.getCurrentAppCode() == 8) { %> // end inv menu/submenu

//inv menu/submenu

<%  if (true) { %>
menu[++i] = "<b>Transactions</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;



addmenu(menu);

<% } %> // end transaction menu

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_SYNCHRONIZER) != null ||
		Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_TYPE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_TYPE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "<b>Maintenance</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = "0"; menu[++i] = 0;

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_SYNCHRONIZER) != null) { %>
menu[++i] = "Synchronizer"; menu[++i] = "pmSynchronizer.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_ID) != null) { %>
menu[++i] = "Project"; menu[++i] = "show-menu=project"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "Project Type"; menu[++i] = "show-menu=projectType"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_TYPE_ENTRY_ID) != null ||
        Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "Project Type Type"; menu[++i] = "show-menu=projectTypeType"; menu[++i]; menu[++i] = ""; menu[++i] = 0;
<% } %>



<% } %> // end maintenance menu

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_REP_PROJECT_PROFITABILITY_ID) != null ) { %>


menu[++i] = "<b>Reports/List</b>"; menu[++i] = "# type=header;align=left"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 1;

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_REP_PROJECT_PROFITABILITY_ID) != null) { %>
menu[++i] = "Project Profitability"; menu[++i] = "pmRepProjectProfitability.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>



<% } %> // end reports/list menu

addmenu(menu);

// inv function/window item

i = 21;
menu=["project",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_ENTRY_ID) != null) { %>
menu[++i] = "New Project"; menu[++i] = "pmProjectEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "pmFindProject.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window location

i = 21;
menu=["projectType",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "pmProjectTypeEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "pmFindProjectType.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);

// inv function/window item location

i = 21;
menu=["projectTypeType",,,100,1,,menu_style,0,"left",effect,0,,,,,,,,,,,]

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_PROJECT_TYPE_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "New"; menu[++i] = "pmProjectTypeTypeEntry.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

<%  if (Common.getUserPermission(sideBarUser, Constants.PM_FIND_PROJECT_TYPE_TYPE_ENTRY_ID) != null) { %>
menu[++i] = "Find"; menu[++i] = "pmFindProjectTypeType.do"; menu[++i] = ""; menu[++i] = ""; menu[++i] = 0;
<% } %>

addmenu(menu);






<% } %> //end pr menu/submenu






dumpmenus()
</SCRIPT>

<SCRIPT language=JavaScript src="js/mmenu.js" type=text/javascript></SCRIPT>

