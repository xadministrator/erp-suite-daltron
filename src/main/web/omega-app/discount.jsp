<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="discountItemEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

  function initFormatAmount(name, event)
  {
    if(name=="fixedDiscountAmount"){
    	document.forms[0].elements["discount1"].value = "0.00";
    	document.forms[0].elements["discount2"].value = "0.00";
    	document.forms[0].elements["discount3"].value = "0.00";
    	document.forms[0].elements["discount4"].value = "0.00";
    }else{
    	document.forms[0].elements["fixedDiscountAmount"].value = "0.00";
    }
    
  	formatAmount(name, event);
  }
  
  function winLoad()
  {    
     var totalDiscount =  0;
     document.forms[0].elements["discount1"].value = window.opener.document.forms[0].elements[window.opener.currProperty + ".discount1"].value;
     document.forms[0].elements["discount2"].value = window.opener.document.forms[0].elements[window.opener.currProperty + ".discount2"].value;
     document.forms[0].elements["discount3"].value = window.opener.document.forms[0].elements[window.opener.currProperty + ".discount3"].value;
     document.forms[0].elements["discount4"].value = window.opener.document.forms[0].elements[window.opener.currProperty + ".discount4"].value;
	
	 totalDiscount = (document.forms[0].elements["discount1"].value * 1) + (document.forms[0].elements["discount2"].value * 1) +
	 					(document.forms[0].elements["discount3"].value * 1) + (document.forms[0].elements["discount4"].value * 1);
	 
	 if(totalDiscount == 0)
     	document.forms[0].elements["fixedDiscountAmount"].value = window.opener.document.forms[0].elements[window.opener.currProperty + ".fixedDiscountAmount"].value;
     else
     	document.forms[0].elements["fixedDiscountAmount"].value = "0.00";

	 return false;
    	   
  }
  
  function returnDiscount()
  {

     window.opener.document.forms[0].elements[window.opener.currProperty + ".discount1"].value = document.forms[0].elements["discount1"].value;
     window.opener.document.forms[0].elements[window.opener.currProperty + ".discount2"].value = document.forms[0].elements["discount2"].value;
     window.opener.document.forms[0].elements[window.opener.currProperty + ".discount3"].value = document.forms[0].elements["discount3"].value;
     window.opener.document.forms[0].elements[window.opener.currProperty + ".discount4"].value = document.forms[0].elements["discount4"].value;     
     window.opener.document.forms[0].elements[window.opener.currProperty + ".fixedDiscountAmount"].value = document.forms[0].elements["fixedDiscountAmount"].value;
     
     window.opener.calculateAmount(window.opener.currName,2);
     
	 window.close();  
	 
	 return false;
  }

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onload="return winLoad();" onkeydown="return enterSubmit(event, new Array('okButton'));">
<form name="discountForm">
<table border="0" cellpadding="0" cellspacing="0" width="250" height="80">
      <tr valign="top">
        <td width="250" height="80">
          <table border="0" cellpadding="0" cellspacing="0" width="250" height="80" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
		        <td width="250" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		            <bean:message key="discountItemEntry.title"/>
			    </td>
		    </tr>
			<input type="hidden" name="enableFields">
			<input type="hidden" name="okButton">
		    <tr>
	          <td width="150" height="25" colspan="2" class="statusBar">
	          </td>
	        </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="discountItemEntry.prompt.discount1"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount1" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="initFormatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="discountItemEntry.prompt.discount2"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount2" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="initFormatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="discountItemEntry.prompt.discount3"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount3" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="initFormatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="discountItemEntry.prompt.discount4"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="discount4" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="initFormatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="discountItemEntry.prompt.fixedDiscountAmount"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="fixedDiscountAmount" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="initFormatAmount(name, (event)?event:window.event);">
                </td>
            </tr>            
            
            <tr>
	         <td width="400" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" onclick="returnDiscount();">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" onclick="window.close();">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
			  </td>
	        </tr> 
		  </table>
        </td>
      </tr>
</table>
</form>
<script language=JavaScript type=text/javascript>
  <!--
     var myObject = window.dialogArguments;
     
     if (myObject) {
         
	     document.forms[0].elements["discount1"].value = myObject.discount1;
	     document.forms[0].elements["discount2"].value = myObject.discount2;
	     document.forms[0].elements["discount3"].value = myObject.discount3;
	     document.forms[0].elements["discount4"].value = myObject.discount4;
		 document.forms[0].enableFields.value = myObject.enableFields;

		 if (document.forms[0].enableFields.value == "false") {

 	     	document.forms[0].discount1.disabled=true;
 	     	document.forms[0].discount2.disabled=true;
 	     	document.forms[0].discount3.disabled=true;
 	     	document.forms[0].discount4.disabled=true; 	     	 	     	
 	     	
 	     }

	 }
     
	       // -->
</script>
</body>
</html>
