<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="accountingCalendarLines.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function confirmGo()
{
   if(document.forms[0].elements["periodPrefix"] != null){
      if(document.forms[0].elements["periodPrefix"].value != ""){
         if(confirm("Go without adding and saving?")) return true;
         else return false;
      }
   }
   return true;
}

function confirmSave()
{
	 if(confirm("Are you sure you want to save and initialize balances? This calendar cannot be updated once it is saved.")) return true;
	 else return false;

     return true;
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('updateButton', 'goButton'));">
<html:form action="/glAccountingCalendarLines.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="accountingCalendarLines.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		<logic:equal name="glAccountingCalendarLinesForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="150" height="25">
	          <bean:message key="accountingCalendarLines.prompt.accountingCalendar"/>
	       </td>
	       <td class="control" width="407" height="25" colspan="3">	          
		      <div id="buttons">
		      <html:select property="accountingCalendar" styleClass="comboRequired">
			     <html:options property="accountingCalendarList"/>
			  </html:select>
	          <html:submit property="goButton" styleClass="mainButtonSmall" onclick="return confirmGo();">
                     <bean:message key="button.go"/>
                  </html:submit>
              </div>
              <div id="buttonsDisabled" style="display: none;">
              <html:select property="accountingCalendar" styleClass="comboRequired">
			     <html:options property="accountingCalendarList"/>
			  </html:select>
	          <html:submit property="goButton" styleClass="mainButtonSmall" disabled="true">
                     <bean:message key="button.go"/>
                  </html:submit>
              </div>
	       </td>
	       </tr>
	       <tr>
		  <td class="prompt" width="150" height="25">
                     <bean:message key="accountingCalendarLines.prompt.description"/>
                  </td>
		  <td class="label" width="407" height="25" colspan="3">
		     <bean:write name="glAccountingCalendarLinesForm" property="description"/>
		  </td>
	        </tr>
		<tr>
 		  <td class="prompt" width="150" height="25">
	             <bean:message key="accountingCalendarLines.prompt.periodType"/>
	          </td>
	          <td class="label" width="407" height="25" colspan="3">
		     <bean:write name="glAccountingCalendarLinesForm" property="periodType"/>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
			   <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
			      value="<%=Constants.FULL_ACCESS%>">
			   <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			      value="true">
			   <html:submit property="saveButton" styleClass="mainButtonBig" onclick="return confirmSave();">
			      <bean:message key="button.saveInitialize"/>
			   </html:submit>
			   </logic:equal>
			   </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
			      <bean:message key="button.close"/>
			   </html:submit>
			  </div>
			  <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
			   <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
			      value="<%=Constants.FULL_ACCESS%>">
			   <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			      value="true">
			   <html:submit property="saveButton" styleClass="mainButtonBig" disabled="true">
			      <bean:message key="button.saveInitialize"/>
			   </html:submit>
			   </logic:equal>
			   </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			      <bean:message key="button.close"/>
			   </html:submit>
			  </div>
              </td>
	       </tr>
	       <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
	          value="<%=Constants.FULL_ACCESS%>">
	       <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			      value="true">
	       <tr>
                  <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
                  </td>
               </tr>
	       <tr>
	          <td width="575" height="5" colspan="4">
		  </td>
	       </tr>
	       <tr>
	          <td class="prompt" width="150" height="25">
		     <bean:message key="accountingCalendarLines.prompt.periodPrefix"/>
		  </td>	
                  <td class="control" width="200" height="25">
		     <html:text property="periodPrefix" size="15" maxlength="15" styleClass="textRequired"/>
		  </td>
		  <td class="prompt" width="118" height="25">
		     <bean:message key="accountingCalendarLines.prompt.quarterNumber"/>
		  </td>
		  <td class="control" width="89" height="25">
		     <html:select property="quarterNumber" styleClass="textRequired">
		        <html:options property="quarterNumberList"/>
		     </html:select>
		  </td>
	       </tr>
	       <tr>
                  <td class="prompt" width="150" height="25">
                     <bean:message key="accountingCalendarLines.prompt.dateFrom"/>
                  </td>
                  <td class="control" width="200" height="25">
                     <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
                  </td>
                  <td class="prompt" width="118" height="25">
                     <bean:message key="accountingCalendarLines.prompt.dateTo"/>
                  </td>
                  <td class="control" width="89" height="25">
                     <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
                  </td>
               </tr>
	       <tr>
	          <td class="prompt" width="150" height="25">
		     <bean:message key="accountingCalendarLines.prompt.periodNumber"/>
		  </td>
		  <td class="control" width="407" height="25" colspan="3">
		     <html:select property="periodNumber" styleClass="textRequired">
		        <html:options property="periodNumberList"/>
		     </html:select>
		  </td>
	       </tr>
	       </logic:equal>
	       </logic:equal>
               <tr>
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	                     
                  <td width="415" height="50" colspan="3"> 
                     <div id="buttons">
                     <p align="right">				  	          
	                 <logic:equal name="glAccountingCalendarLinesForm" property="userPermission"
                        value="<%=Constants.FULL_ACCESS%>">
                     <logic:equal name="glAccountingCalendarLinesForm" property="pageState"
                        value="<%=Constants.PAGE_STATE_SAVE%>">
                     <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			            value="true">
                      <html:submit property="addButton" styleClass="mainButton">
                         <bean:message key="button.add"/>
                      </html:submit>
                      </logic:equal>
                      </logic:equal>
                      <logic:equal name="glAccountingCalendarLinesForm" property="pageState"
                         value="<%=Constants.PAGE_STATE_EDIT%>">
                      <html:submit property="updateButton" styleClass="mainButton">
                         <bean:message key="button.update"/>
                      </html:submit>
                      <html:submit property="cancelButton" styleClass="mainButton">
                         <bean:message key="button.cancel"/>
                      </html:submit>
                      </logic:equal>
                   	  </logic:equal>
                      </div>
                      <div id="buttonsDisabled" style="display: none;">
                     <p align="right">				  	          
	                 <logic:equal name="glAccountingCalendarLinesForm" property="userPermission"
                        value="<%=Constants.FULL_ACCESS%>">
                     <logic:equal name="glAccountingCalendarLinesForm" property="pageState"
                        value="<%=Constants.PAGE_STATE_SAVE%>">
                     <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			           value="true">
                      <html:submit property="addButton" styleClass="mainButton" disabled="true">
                         <bean:message key="button.add"/>
                      </html:submit>
                      </logic:equal>
                      </logic:equal>
                      <logic:equal name="glAccountingCalendarLinesForm" property="pageState"
                         value="<%=Constants.PAGE_STATE_EDIT%>">
                      <html:submit property="updateButton" styleClass="mainButton" disabled="true">
                         <bean:message key="button.update"/>
                      </html:submit>
                      <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
                         <bean:message key="button.cancel"/>
                      </html:submit>
                      </logic:equal>
                   	  </logic:equal>
                      </div>
		   		</td>
	       </tr>
	       <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="accountingCalendarLines.gridTitle.ACVDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="accountingCalendarLines.prompt.periodNumber"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="accountingCalendarLines.prompt.periodPrefix"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="accountingCalendarLines.prompt.quarterNumber"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="accountingCalendarLines.prompt.dateFrom"/>
			       </td>			       			       			       
			       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="accountingCalendarLines.prompt.dateTo"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glACVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="periodNumber"/>
			       </td>
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="periodPrefix"/>
			       </td>
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="quarterNumber"/>
			       </td>
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="dateFrom"/>
			       </td>			       			       			       
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="dateTo"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			               value="true">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glAccountingCalendarLinesForm" property="pageState" 
				           value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                            onclick="return confirmDelete();"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  		</logic:equal>
				  		</logic:equal>
				  		</div>
				  		<div id="buttonsDisabled" style="display: none;">
			            <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			      		   value="true">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glAccountingCalendarLinesForm" property="pageState" 
				           value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                             disabled="true"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  		</logic:equal>
				  		</logic:equal>
				  		</div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="glAccountingCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glACVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="accountingCalendarLines.prompt.periodPrefix"/>
			       </td>
			       <td width="250" height="1" class="gridLabel">
			          <nested:write property="periodPrefix"/>
			       </td>
			       <td width="200"  height="1" class="gridHeader">
			          <bean:message key="accountingCalendarLines.prompt.quarterNumber"/>
			       </td>
			       <td width="120" height="1" class="gridLabelNum">
			          <nested:write property="quarterNumber"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
				      <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
		 			       value="true">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glAccountingCalendarLinesForm" property="pageState" 
				           value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                            onclick="return confirmDelete();"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  		</logic:equal>
				  		</logic:equal>
				  		</div>
				  		<div id="buttonsDisabled" style="display: none;">
			            <logic:equal name="glAccountingCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="glAccountingCalendarLinesForm" property="showButtons" 
			      		   value="true">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glAccountingCalendarLinesForm" property="pageState" 
				           value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                             disabled="true"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  		</logic:equal>
				  		</logic:equal>
				  		</div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="accountingCalendarLines.prompt.periodNumber"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="4">
			         <nested:write property="periodNumber"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="accountingCalendarLines.prompt.dateFrom"/>
			       </td>
			       <td width="250" height="1" class="gridLabel">
			          <nested:write property="dateFrom"/>
			       </td>
			       <td width="200" height="1" class="gridHeader">
			          <bean:message key="accountingCalendarLines.prompt.dateTo"/>
			       </td>
			       <td width="269" height="1" class="gridLabel" colspan="2">
			          <nested:write property="dateTo"/>
			       </td>
			    </tr>
			    </nested:iterate>			    
			    </logic:equal>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["periodPrefix"] != null)
             document.forms[0].elements["periodPrefix"].focus()
   // -->
</script>
</body>
</html>
