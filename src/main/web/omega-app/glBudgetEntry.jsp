<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="budgetEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 
  
function populateAccountDesc(name)
{
   var property = name.substring(0,name.indexOf("."));
   document.forms[0].elements['accountDescription'].value = document.forms[0].elements[property + ".accountDescription"].value;
         
}

function fnSetValues(){
   var iHeight=195;
   var iWidth=257;
   var sFeatures="dialogHeight: " + iHeight + "px;" + "dialogWidth: " + iWidth + "px;";
   return sFeatures;
}

function fnOpen(){

   var sFeatures=fnSetValues();
   var myObject = new Object();
   myObject.ruleType = document.forms[0].elements["ruleType"].value;
   myObject.ruleAmount = document.forms[0].elements["ruleAmount"].value;
   
   var retObject = window.showModalDialog("glBudgetRule.jsp", myObject, sFeatures)
   
   if (retObject.status == 1) {
   
      calculateBudgetRule(retObject.ruleType, retObject.ruleAmount);
   	
   }
   
   return false;
}

function calculateBudgetRule(ruleType, ruleAmount) {

    if (!isNaN(parseFloat(ruleAmount))) {
  
   		ruleAmount = ruleAmount.replace(/,/g,'');
      	
    } else {
    
        return;
    
    }

    document.forms[0].elements["ruleType"].value = ruleType;
    document.forms[0].elements["ruleAmount"].value = ruleAmount;    
    
    if(ruleType == "Divide Evenly") {
    
        var periodNumber = 0;
                
        while (document.forms[0].elements['glBgaPeriodList[' + periodNumber + '].periodAmount'] != null) {
                   
           periodNumber++;
        
        }
       
        var quotient = (ruleAmount / periodNumber).toFixed(2);
        var i = 0;
       
        while (document.forms[0].elements['glBgaPeriodList[' + i + '].periodAmount'] != null) {
        
            document.forms[0].elements['glBgaPeriodList[' + i + '].periodAmount'].value = quotient;
            i++;
        
        }                    
               
    } else if (ruleType == "Repeat Per Period") {
        
        var i = 0;
        
        while (document.forms[0].elements['glBgaPeriodList[' + i + '].periodAmount'] != null) {
        
            document.forms[0].elements['glBgaPeriodList[' + i + '].periodAmount'].value = ruleAmount;
            i++;
        
        }
    
    } else if (ruleType == "4/4/5") {
        
        
        var periodNumber = 0;
                
        while (document.forms[0].elements['glBgaPeriodList[' + periodNumber + '].periodAmount'] != null) {
                   
           periodNumber++;
        
        }
        
        if (periodNumber >= 12) {

	        document.forms[0].elements['glBgaPeriodList[0].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[1].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[2].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[3].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[4].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[5].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[6].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[7].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[8].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[9].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[10].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[11].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);

	      }      
	      
    } else if (ruleType == "4/5/4") {
    
        var periodNumber = 0;
                
        while (document.forms[0].elements['glBgaPeriodList[' + periodNumber + '].periodAmount'] != null) {
                   
           periodNumber++;
        
        }
        
        if (periodNumber >= 12) {
        
	        document.forms[0].elements['glBgaPeriodList[0].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[1].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[2].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[3].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[4].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[5].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[6].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[7].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[8].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[9].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[10].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[11].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);    
	        
        }
        
    } else if (ruleType == "5/4/4") {
    
        var periodNumber = 0;
                
        while (document.forms[0].elements['glBgaPeriodList[' + periodNumber + '].periodAmount'] != null) {
                   
           periodNumber++;
        
        }
        
        if (periodNumber >= 12) {
    
	        document.forms[0].elements['glBgaPeriodList[0].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[1].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[2].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[3].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[4].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[5].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[6].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[7].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[8].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[9].periodAmount'].value =  (5 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[10].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);
	        document.forms[0].elements['glBgaPeriodList[11].periodAmount'].value =  (4 / 52 * ruleAmount).toFixed(2);        
	    
        }
    }
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glBudgetEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="budgetEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glBudgetEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>	
	        </td>
	     </tr>
	     
	     <html:hidden property="isBudgetOrganizationEntered" value=""/>
	     <html:hidden property="isBudgetEntered" value=""/>
         <html:hidden property="ruleType"/>
		 <html:hidden property="ruleAmount"/>
			  	                 
	     <logic:equal name="glBudgetEntryForm" property="enableFields" value="true">
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.budgetOrganization"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="budgetOrganization" styleClass="comboRequired" onchange="return enterSelect('budgetOrganization','isBudgetOrganizationEntered');">
		               <html:options property="budgetOrganizationList"/>
		           </html:select>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.budgetName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="budgetName" styleClass="comboRequired" onchange="return enterSelect('budgetName','isBudgetEntered');">
		               <html:options property="budgetNameList"/>
		           </html:select>
                </td>
         </tr> 
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.password"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:password property="password" size="25" maxlength="25" styleClass="text"/>  
                </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.accountNumber"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountNumber" size="25" maxlength="255" styleClass="textRequired"/>
                   <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('accountNumber','accountDescription');"/>	            
                </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.accountDescription"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>  
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.description"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="description" size="75" maxlength="255" style="font-size:8pt;" styleClass="text"/>  
                </td>
         </tr>
         

         
         <tr>
       			<td width="130" height="25" class="prompt">
          				<bean:message key="budgetEntry.prompt.rulePercentage1"/>
       			</td>
       			<td width="158" height="25" class="control">
          				<html:text property="rulePercentage1" size="15" maxlength="25" styleClass="textAmount"/>
       			</td>
         </tr>
         
         <tr>
       			<td width="130" height="25" class="prompt">
          				<bean:message key="budgetEntry.prompt.rulePercentage2"/>
       			</td>
       			<td width="158" height="25" class="control">
          				<html:text property="rulePercentage2" size="15" maxlength="25" styleClass="textAmount"/>
       			</td>
         </tr>
                    
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <div class="tabbertab" title="Budget">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					 <tr valign="top">
				          <td width="100" height="185">
					      <div align="center">
					        <table border="1" cellpadding="0" cellspacing="0" width="250" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
				            <tr>
				               <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="budgetEntry.prompt.period"/>
						       </td>
						       <td width="149" height="1" align="center" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="budgetEntry.prompt.ruleAmount"/>
						       </td> 						       
					        </tr>	            				            
						    <%
						       int i = 0;	
						       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
						       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
						       String rowBgc = null;
						    %>
						    <nested:iterate property="glBgaPeriodList">						    
						    <%
						       i++;
						       if((i % 2) != 0){
						           rowBgc = ROW_BGC1;
						       }else{
						           rowBgc = ROW_BGC2;
						       }  
						    %>
						    <tr bgcolor="<%= rowBgc %>">					
						       <td width="100" height="1" class="control">
						          <nested:write property="period"/>
						       </td>
						       <td width="149" height="1" class="control">
						          <nested:text property="periodAmount" size="12" maxlength="25" styleClass="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);"/>
						       </td> 				       
						    </tr>				    
						    </nested:iterate>
						  </table>
					      </div>
					   </td>
				     </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="budgetRuleButton" styleClass="mainButton" onclick="return fnOpen();">
				  <bean:message key="button.budgetRule"/>
               </html:submit>
			   <logic:equal name="glBudgetEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="budgetRuleButton" styleClass="mainButton" disabled="true">
				  <bean:message key="button.budgetRule"/>
			   </html:submit>
			   <logic:equal name="glBudgetEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
			  </td>
	     </tr>	     	     
	     </logic:equal>
	     
	     <logic:equal name="glBudgetEntryForm" property="enableFields" value="false">
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.budgetOrganization"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="budgetOrganization" styleClass="comboRequired" disabled="true">
		               <html:options property="budgetOrganizationList"/>
		           </html:select>
                </td>
         </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.budgetName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="budgetName" styleClass="comboRequired" disabled="true">
		               <html:options property="budgetNameList"/>
		           </html:select>
                </td>
         </tr> 
          <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.password"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:password property="password" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.accountNumber"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountNumber" size="25" maxlength="255" styleClass="textRequired" disabled="true"/>
                   <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>	            
                </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.accountDescription"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>  
                </td>
         </tr>           
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <div id="budget">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					 <tr valign="top">
				          <td width="100" height="185">
					      <div align="center">
					        <table border="1" cellpadding="0" cellspacing="0" width="100" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
				            <tr>
				               <td width="350" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="budgetEntry.prompt.period"/>
						       </td>
						       <td width="149" height="1" align="center" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="budgetEntry.prompt.periodAmount"/>
						       </td> 						       
					        </tr>	            				            
						    <%
						       int i = 0;	
						       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
						       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
						       String rowBgc = null;
						    %>
						    <nested:iterate property="glBgaPeriodList">						    
						    <%
						       i++;
						       if((i % 2) != 0){
						           rowBgc = ROW_BGC1;
						       }else{
						           rowBgc = ROW_BGC2;
						       }  
						    %>
						    <tr bgcolor="<%= rowBgc %>">					
						       <td width="350" height="1" class="control">
						          <nested:write property="period"/>
						       </td>
						       <td width="149" height="1" class="control">
						          <nested:text property="periodAmount" size="12" maxlength="25" styleClass="textAmount" disabled="true"/>
						       </td> 				       
						    </tr>				    
						    </nested:iterate>
						  </table>
					      </div>
					   </td>
				     </tr>
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="budgetEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="budgetRuleButton" styleClass="mainButton" disabled="true">
				  <bean:message key="button.budgetRule"/>
			   </html:submit>
			   <logic:equal name="glBudgetEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="budgetRuleButton" styleClass="mainButton" disabled="true">
				  <bean:message key="button.budgetRule"/>
			   </html:submit>
			   <logic:equal name="glBudgetEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
			  </td>
	     </tr>	     	     
	     </logic:equal>
	     
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		      </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["budgetName"] != null &&
        document.forms[0].elements["budgetName"].disabled == false)
        document.forms[0].elements["budgetName"].focus()
	       // -->
</script>
</body>
</html>
