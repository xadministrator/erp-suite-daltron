<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="budgetEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 
  
  function closeWin()
  {
    var ret = new Object();
    ret.status = 0;       
    window.returnValue = ret;
    window.close();
    return false;
	   
  }
  
  function returnBudgetRule()
  {
    
    var ret = new Object();
    ret.status = 1;
    ret.ruleType = document.forms[0].elements["ruleType"].value;
    ret.ruleAmount = document.forms[0].elements["ruleAmount"].value;
    window.returnValue = ret;
	window.close();
	return false;       
  }

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('okButton'));">
<form name="glBudgetRuleForm">
<table border="0" cellpadding="0" cellspacing="0" width="250" height="80">
      <tr valign="top">
        <td width="250" height="80">
          <table border="0" cellpadding="0" cellspacing="0" width="250" height="80" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
		        <td width="250" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		            <bean:message key="budgetEntry.budgetRule"/>
			    </td>
		    </tr>
		    <tr>
	          <td width="150" height="25" colspan="2" class="statusBar">
	          </td>
	        </tr>
		    <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.ruleType"/>
                </td>
                <td width="130" height="25" class="control">
                    <select NAME="ruleType" class="combo">
					   <option value="Divide Evenly" selected="selected">Divide Evenly</option>
					   <option value="Repeat Per Period">Repeat Per Period</option>
					   <option value="4/4/5">4/4/5</option>
					   <option value="4/5/4">4/5/4</option>
					   <option value="5/4/4">5/4/4</option>		   					   
					</select>
                </td>
            </tr>
            <tr>
                <td width="120" height="25" class="prompt">
                   <bean:message key="budgetEntry.prompt.ruleAmount"/>
                </td>
                <td width="130" height="25" class="control">
                   <input type="text" name="ruleAmount" maxlength="25" size="15" class="textAmount" onblur="addZeroes(name);" onkeyup="formatAmount(name, (event)?event:window.event);">
                </td>
            </tr>
            <tr>
	         <td width="400" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" onclick="return returnBudgetRule();">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" onclick="return closeWin();">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
			  </td>
	        </tr> 
		  </table>
        </td>
      </tr>
</table>
</form>
<script language=JavaScript type=text/javascript>
  <!--
     var myObject = window.dialogArguments;
     
     if (myObject) {
         
         if (myObject.ruleType != null && myObject.ruleType.length > 0) {
         
	     	document.forms[0].elements["ruleType"].value = myObject.ruleType;
	     	
	     }
	     document.forms[0].elements["ruleAmount"].value = myObject.ruleAmount;
	     
	 }
     
	       // -->
</script>
</body>
</html>
