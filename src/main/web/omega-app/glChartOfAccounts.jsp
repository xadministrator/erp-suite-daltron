<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="chartOfAccounts.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 

<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glChartOfAccounts.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="300">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="chartOfAccounts.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
   		   <logic:equal name="glChartOfAccountsForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
		      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="isAccountEntered"/>
		 <html:hidden property="accountType"/>		  
		  <tr>
		  <td width="575" height="10" colspan="4">
	      <logic:equal name="glChartOfAccountsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">

		  <div class="tabber">
		  <div class="tabbertab" title="Header">
		  <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		  <tr> 
				<td class="prompt" width="575" height="25" colspan="4">
			    </td>
		  </tr>
	      <tr>
	       	   <td class="prompt" width="130" height="25">
		          <bean:message key="chartOfAccounts.prompt.account"/>
		       </td>
	       	   <td width="221" height="25" class="control">
	          		<html:text property="account" size="30" maxlength="255" styleClass="textRequired" onblur="return enterSelect('account','isAccountEntered');"/>
		       </td>
	       	   <td class="prompt" width="118" height="25">
	          		<bean:message key="chartOfAccounts.prompt.enabled"/>
	       	   </td>
		       <td width="109" height="25" class="control">
		          <html:checkbox property="enabled"/>
	       	   </td>
          </tr>
          <tr>
		      <td class="prompt" width="130" height="25">
	          	  <bean:message key="chartOfAccounts.prompt.description"/>
	       	  </td>
 	       	  <td width="448" height="25" class="control" colspan="3">
	          	  <html:text property="description" size="85" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
	       	  </td>
          </tr>
          
          <tr>
	          <td class="prompt" width="130" height="25">
	             <bean:message key="chartOfAccounts.prompt.accountType"/>
	          </td>
	          <td width="448" height="25" class="control">
	             <html:text property="accountType" size="30" maxlength="255" styleClass="text" disabled="true"/>
	          </td>
	      	 <td class="prompt" width="130" height="25">
	         	<bean:message key="chartOfAccounts.prompt.forRevaluation"/>
	       	 </td>
		     <td width="109" height="25" class="control">
		     	<html:checkbox property="forRevaluation"/>
         	 </td>
          </tr>
	      
	      <tr>
		      <td class="prompt" width="130" height="25">
                 <bean:message key="chartOfAccounts.prompt.effectiveDateFrom"/>
              </td>
		      <td width="221" height="25" class="control">
		         <html:text property="effectiveDateFrom" size="10" maxlength="10" styleClass="textRequired"/>
		      </td>
 		      <td class="prompt" width="118" height="25">
	             <bean:message key="chartOfAccounts.prompt.effectiveDateTo"/>
	          </td>
	          <td width="109" height="25" class="control">
		     	<html:text property="effectiveDateTo" size="10" maxlength="10"  styleClass="text"/>
		  	  </td>
	      </tr>
	      
	      <logic:equal name="glChartOfAccountsForm" property="accountType" value="EXPENSE">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.taxType"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="taxType" styleClass="combo" >	              
	               <html:options property="taxTypeList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
	      
		  <logic:equal name="glChartOfAccountsForm" property="accountType" value="ASSET">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" >	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
		  
		  <logic:equal name="glChartOfAccountsForm" property="accountType" value="LIABILITY">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" >	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
		  
		  <logic:equal name="glChartOfAccountsForm" property="accountType" value="OWNERS EQUITY">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" >	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
		  
		  <logic:equal name="glChartOfAccountsForm" property="accountType" value="REVENUE">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" >	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
		  
		  <logic:equal name="glChartOfAccountsForm" property="accountType" value="EXPENSE">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" >	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:equal>
		  
		   
		  <tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="chartOfAccounts.prompt.citCategory"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="citCategory" styleClass="combo" >
						<html:options property="citCategoryList"/>
					</html:select>
				</td>

				
			</tr>
			
			
			 <tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="chartOfAccounts.prompt.sawCategory"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="sawCategory" styleClass="combo" >
						<html:options property="sawCategoryList"/>
					</html:select>
				</td>

				
			</tr>
	
	
			
			
			
			<tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="chartOfAccounts.prompt.iitCategory"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="iitCategory" styleClass="combo" >
						<html:options property="iitCategoryList"/>
					</html:select>
				</td>

				
			</tr>
		 </table>   
		 </div> 
		 <div class="tabbertab" title="Branch Map">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
		 <nested:iterate property="glBCOAList">
		 <tr>
			  <td class="prompt" width="140" height="25">
			  		<nested:write property="bcoaName"/>
			  </td>
			  <td width="435" class="control">
			        <nested:checkbox property="branchCheckbox"/>
			  </td>
		 </tr>
		 </nested:iterate>
		 </table>
		 </div> 
		 </div><script>tabberAutomatic(tabberOptions)</script>
	     </logic:equal>	  

	     <logic:equal name="glChartOfAccountsForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
		 <div class="tabber">
		 <div class="tabbertab" title="Header">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
			  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
	     <tr>
	       	  <td class="prompt" width="130" height="25">
	          	<bean:message key="chartOfAccounts.prompt.account"/>
	       	  </td>
	       	  <td width="221" height="25" class="control">
	          	<html:text property="account" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
	       	  </td>
	       	  <td class="prompt" width="118" height="25">
	          	<bean:message key="chartOfAccounts.prompt.enabled"/>
	       	  </td>
	       	  <td width="109" height="25" class="control">
	          	<html:checkbox property="enabled" disabled="true"/>
	       	  </td>
         </tr>
         <tr>
	       	  <td class="prompt" width="130" height="25">
	          	<bean:message key="chartOfAccounts.prompt.description"/>
	       	  </td>
	       	  <td width="448" height="25" class="control" colspan="3">
	          	<html:text property="description" size="85" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
	       	  </td>
         </tr>
         <tr>
	       	  <td class="prompt" width="130" height="25">
	          	<bean:message key="chartOfAccounts.prompt.accountType"/>
	          </td>
	     	  <td width="448" height="25" class="control" colspan="3">
	          	<html:text property="accountType" size="30" maxlength="255" styleClass="text" disabled="true"/>
	       	  </td>
         </tr>
	     <tr>
		  	  <td class="prompt" width="130" height="25">
                <bean:message key="chartOfAccounts.prompt.effectiveDateFrom"/>
              </td>
		  	  <td width="221" height="25" class="control">
		     	<html:text property="effectiveDateFrom" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
		  	  </td>
 		  	  <td class="prompt" width="118" height="25">
	            <bean:message key="chartOfAccounts.prompt.effectiveDateTo"/>
	          </td>
	          <td width="109" height="25" class="control">
		     	<html:text property="effectiveDateTo" size="10" maxlength="10"  styleClass="text" disabled="true"/>
		  	  </td>
	      </tr>
		  <logic:notEqual name="glChartOfAccountsForm" property="accountType" value="REVENUE">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" disabled="true">	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:notEqual>
		  <logic:notEqual name="glChartOfAccountsForm" property="accountType" value="EXPENSE">
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="chartOfAccounts.prompt.currency"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="currency" styleClass="combo" disabled="true">	              
	               <html:options property="currencyList"/>
	            </html:select>
	         </td>
          </tr>
		  </logic:notEqual>
		  
		  <tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="chartOfAccounts.prompt.accountCategory"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="accountCategory" styleClass="combo" disabled="true">
						<html:options property="citCategoryList"/>
					</html:select>
				</td>

				
			</tr>
		 </table>   
		 </div> 
		 <div class="tabbertab" title="Branch Map">
		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		 <tr> 
		 	  <td class="prompt" width="575" height="25" colspan="4">
			  </td>
		 </tr>
		 <nested:iterate property="glBCOAList">
		 <tr>
			  <td class="prompt" width="140" height="25">
			  		<nested:write property="bcoaName"/>
			  </td>
			  <td width="435" class="control">
			        <nested:checkbox property="branchCheckbox" disabled="true"/>
			  </td>
		 </tr>
		 </nested:iterate>
		 </table>
		 </div> 
		 </div><script>tabberAutomatic(tabberOptions)</script>
	     </logic:equal>
		 </td>
		 </tr>
	       <tr>
	          <td width="575" height="50" colspan="4"> 
	             <div id="buttons">
	             <p align="right">
   		         <logic:equal name="glChartOfAccountsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		         <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		         </html:submit>
		         <logic:notEmpty name="glChartOfAccountsForm" property="accountCode">
		         <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
		            <bean:message key="button.delete"/>
		         </html:submit>
		         </logic:notEmpty>
		         </logic:equal>		      
				 <html:submit property="closeButton" styleClass="mainButton">
				    <bean:message key="button.close"/>
				 </html:submit>
				 </div>
				 <div id="buttonsDisabled" style="display: none;">	
	             <p align="right">
   		         <logic:equal name="glChartOfAccountsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		         <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		         </html:submit>
		         <logic:notEmpty name="glChartOfAccountsForm" property="accountCode">
		         <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.delete"/>
		         </html:submit>
		         </logic:notEmpty>
		         </logic:equal>		      
				 <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				    <bean:message key="button.close"/>
				 </html:submit>
				 </div>
              </td>
	       </tr>	       
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
    if(document.forms[0].elements["account"] != null &&
       document.forms[0].elements["account"].value == "")
       document.forms[0].elements["account"].focus()
  // -->
</script>
</body>
</html>
