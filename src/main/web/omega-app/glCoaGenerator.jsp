<%@ page language="java" import="com.struts.util.Constants,com.struts.gl.coagenerator.*" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>

<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="coaGenerator.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function confirmGenerate()
{
	 if(confirm("Are you sure you want to generate these accounts?")) return true;
	 else return false;

     return true;
}

function confirmDelete()
{
	 if(confirm("Are you sure you want to delete these accounts?")) return true;
	 else return false;

     return true;
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["glCoaGenList[" + i + "].segmentChecked"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["glCoaGenList[" + i + "].segmentChecked"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('generateButton'));">
<html:form action="/glCoaGenerator.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="500">
      <tr valign="top">
        <td width="187" height="500"></td> 
        <td width="581" height="500">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="490" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   	<tr>
	     	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
				<bean:message key="coaGenerator.title"/>
		 	</td>
	   	</tr>
       	<tr>
	    	<td width="575" height="44" colspan="4" class="statusBar">
		   	<logic:equal name="glCoaGeneratorForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
            	<bean:message key="app.success"/>
           	</logic:equal>
		   	<html:errors/>
		   	<html:messages id="msg" message="true">
		    	<bean:write name="msg"/>		   
		   	</html:messages>	
	        </td>
	 	</tr>
	    
	   	<logic:equal name="glCoaGeneratorForm" property="enableFields" value="true">
       	<tr>
       		<td class="prompt" width="130" height="25">
        		<bean:message key="coaGenerator.prompt.effectiveDateFrom"/>
       		</td>
			<td width="130" height="25" class="control">
				<html:text property="effectiveDateFrom" size="10" maxlength="10" styleClass="textRequired"/>
			</td>
 			<td class="prompt" width="118" height="25">
	    		<bean:message key="coaGenerator.prompt.effectiveDateTo"/>
	   		</td>
	   		<td width="200" height="25" class="control">
				<html:text property="effectiveDateTo" size="10" maxlength="10"  styleClass="text"/>
			</td>
	 	</tr>
		<tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="coaGenerator.prompt.currency"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:select property="currency" styleClass="combo">	              
	              <html:options property="currencyList"/>
	            </html:select>
	        </td>
          	<td class="prompt" width="130" height="25">
	        	<bean:message key="coaGenerator.prompt.enabled"/>
	       	</td>
	       	<td width="130" height="25" class="control">
	          	<html:checkbox property="enabled"/>
	       	</td>
      	</tr>
       	<tr valign="top">
		<td width="575" height="185" colspan="4">
			<div class="tabber">
			<div class="tabbertab" title="Segments">
		    <table border="1" cellpadding="0" cellspacing="0" width="560" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
			<tr>
            	<td width="560" height="1" colspan="4" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	               <bean:message key="coaGenerator.gridTitle.SegmentDetails"/>
               	</td>
	     	</tr>
			<html:hidden property="naturalAcctName"/>
			<tr>
				<td width="100" height="1" class="gridHeader">
					<bean:message key="coaGenerator.prompt.accountFrom"/>
				</td>
				<td width="135" height="1" class="control">	
					<html:text property="naturalAcctFrom" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				</td>
				<td width="250" height="1" class="control">
					<html:text property="naturalAcctFromDesc" size="30" maxlength="255" styleClass="textRequired"/>
					<html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlFindSegmentCoaGenLookup('naturalAcctName','naturalAcctFrom','naturalAcctFromDesc');"/>
				</td>
			</tr>
			<tr>
				<td width="100" height="1" class="gridHeader">
					<bean:message key="coaGenerator.prompt.accountTo"/>
				</td>
				<td width="135" height="1" class="control">	
					<html:text property="naturalAcctTo" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				</td>
				<td width="250" height="1" class="control">
					<html:text property="naturalAcctToDesc" size="30" maxlength="255" styleClass="textRequired"/>
					<html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlFindSegmentCoaGenLookup('naturalAcctName','naturalAcctTo','naturalAcctToDesc');"/>
				</td>
			</tr>
			<logic:equal name="glCoaGeneratorForm" property="showSegments" value="true">
			<tr>
				<td width="485" height="1" class="control" colspan="3"/>
				<td width="75" height="1" class="control">
				<div id="buttons">
					<p align="right">
					<html:submit property="selectAllButton" styleClass="gridButtonBig" onclick="return selectAll();">
					<bean:message key="button.selectAll"/>
					</html:submit>
				</div>
				<div id="buttonsDisabled" style="display: none;">
					<p align="right">
					<html:submit property="selectAllButton" styleClass="gridButtonBig" disabled="true">
					<bean:message key="button.selectAll"/>
					</html:submit>
				</div>
				</td>
			</tr>
			</logic:equal>
			<%
				int i = 0;	
				String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				String rowBgc = null;
				String segmentTitle = "";
				String current = "";
				boolean first=false;
			%>
			<bean:define id="actionForm" name="glCoaGeneratorForm" type="com.struts.gl.coagenerator.GlCoaGeneratorForm"/>
			<nested:iterate property="glCoaGenList">
			<bean:define id="account" name="glCoaGenList" type="com.struts.gl.coagenerator.GlCoaGeneratorList"/>	
				<%
					current = account.getSegmentTitle();
					if(!segmentTitle.equals(current)) {
						segmentTitle = current;	
					   	first=true;
					}

					i++;
					if((i % 2) != 0){
						rowBgc = ROW_BGC1;
					}else{
						rowBgc = ROW_BGC2;
					}  

					if(first) {
					%>
						<tr> 
							<td width="560" height="1" align="right" colspan="4" class="gridBoldHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">							
								<nested:write property="segmentTitle" />	
							</td>
						</tr>      
						<tr bgcolor="<%= rowBgc %>">
							<td width="100" height="1" class="control">	
								<nested:text property="segment" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="385" height="1" class="control" colspan="2">							
								<nested:text property="segmentDescription" size="35" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="75" height="1" class="control">		
								<p align="center">					
								<nested:checkbox property="segmentChecked"/>	
							</td>
						</tr>  
					<%
						first=false;									
					} else {
					%>
						<tr bgcolor="<%= rowBgc %>">	
							<td width="100" height="1" class="control">	
								<nested:text property="segment" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="385" height="1" class="control" colspan="2">							
								<nested:text property="segmentDescription" size="35" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="75" height="1" class="control">		
								<p align="center">					
								<nested:checkbox property="segmentChecked"/>	
							</td>
						</tr>		
					<%
					}
				%>
			</nested:iterate>
			</table>
			</div>
			
            <div class="tabbertab" title="Branch Map">
 		    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		    <tr> 
		 	  	<td class="prompt" width="575" height="25" colspan="4">
			    </td>
		    </tr>
		    <nested:iterate property="glBCOAList">
		    <tr>
			  	<td class="prompt" width="140" height="25">
			  		<nested:write property="bcoaName"/>
			    </td>
			  	<td width="435" class="control">
			        <nested:checkbox property="branchCheckbox"/>
			  	</td>
		 	</tr>
		 	</nested:iterate>
		 	</table>
 		 	</div>
		   </div><script>tabberAutomatic(tabberOptions)</script>
		</td>
		</tr>
		<tr>			
	    	<td width="575" height="50" colspan="9"> 
	        <div id="buttons">
	        <p align="right">
            	<logic:equal name="glCoaGeneratorForm" property="showSaveButton" value="true">
               	<html:submit property="generateButton" styleClass="mainButton" onclick="return confirmGenerate();">
	              	<bean:message key="button.generate"/>
		       	</html:submit>
		       	</logic:equal>
				<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
		          	<bean:message key="button.delete"/>
		       	</html:submit>
		       	<html:submit property="closeButton" styleClass="mainButton">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
	        <div id="buttonsDisabled" style="display: none;">
	       	<p align="right">
	           	<logic:equal name="glCoaGeneratorForm" property="showSaveButton" value="true">
	           	<html:submit property="generateButton" styleClass="mainButton" disabled="true">
	              	<bean:message key="button.generate"/>
		       	</html:submit>
		       	</logic:equal>
		       	<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.delete"/>
		       	</html:submit>
		       	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
			</td>
	   	</tr>	     	     
	  	</logic:equal>
	    
	    <logic:equal name="glCoaGeneratorForm" property="enableFields" value="false">
	  	<tr>
       		<td class="prompt" width="130" height="25">
        		<bean:message key="coaGenerator.prompt.effectiveDateFrom"/>
       		</td>
			<td width="130" height="25" class="control">
				<html:text property="effectiveDateFrom" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
			</td>
 			<td class="prompt" width="118" height="25">
	    		<bean:message key="coaGenerator.prompt.effectiveDateTo"/>
	   		</td>
	   		<td width="200" height="25" class="control">
				<html:text property="effectiveDateTo" size="10" maxlength="10" styleClass="text" disabled="true"/>
			</td>
	 	</tr>
 		<tr>
	       	<td class="prompt" width="118" height="25">
	           <bean:message key="chartOfAccounts.prompt.forRevaluation"/>
	       	</td>
		    <td width="109" height="25" class="control">
		       <html:checkbox property="forRevaluation"/>
	       	</td>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="coaGenerator.prompt.currency"/>
	        </td>
	        <td width="148" height="25" class="control">
	           <html:select property="currency" styleClass="combo">
	              <html:options property="currencyList"/>
	            </html:select>
	        </td>
		</tr>
 		<tr>
          	<td class="prompt" width="130" height="25">
	        	<bean:message key="coaGenerator.prompt.enabled"/>
	       	</td>
	       	<td width="130" height="25" class="control">
	          	<html:checkbox property="enabled" disabled="true"/>
	       	</td>
      	</tr>           
       	<tr valign="top">
		<td width="575" height="185" colspan="4">
			<div class="tabber">
			<div id="segment">
		    <table border="1" cellpadding="0" cellspacing="0" width="560" height="50" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
			<tr>
            	<td width="450" height="1" colspan="3" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	               <bean:message key="coaGenerator.gridTitle.SegmentDetails"/>
               	</td>
	     	</tr>
			<html:hidden property="naturalAcctName"/>
			<tr>
				<td width="100" height="1" class="gridHeader">
					<bean:message key="coaGenerator.prompt.accountFrom"/>
				</td>
				<td width="135" height="1" class="control">	
					<html:text property="naturalAcctFrom" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				</td>
				<td width="250" height="1" class="control">
					<html:text property="naturalAcctFromDesc" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				</td>
			</tr>
			<tr>
				<td width="100" height="1" class="gridHeader">
					<bean:message key="coaGenerator.prompt.accountTo"/>
				</td>
				<td width="135" height="1" class="control">	
					<html:text property="naturalAcctTo" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				</td>
				<td width="250" height="1" class="control">
					<html:text property="naturalAcctToDesc" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
					<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				</td>
			</tr>
			<logic:equal name="glCoaGeneratorForm" property="showSegments" value="true">
			<tr>
				<td width="375" height="1" class="control" colspan="2"/>
				<td width="75" height="1" class="control">
				<div id="buttons">
					<p align="right">
					<html:submit property="selectAllButton" styleClass="gridButtonBig" onclick="return selectAll();">
					<bean:message key="button.selectAll"/>
					</html:submit>
				</div>
				<div id="buttonsDisabled" style="display: none;">
					<p align="right">
					<html:submit property="selectAllButton" styleClass="gridButtonBig" disabled="true">
					<bean:message key="button.selectAll"/>
					</html:submit>
				</div>
				</td>
			</tr>
			</logic:equal>
			<%
				int i = 0;	
				String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				String rowBgc = null;
				String segmentTitle="";
				String current="";
				boolean first=false;
			%>
			<bean:define id="actionForm" name="glCoaGeneratorForm" type="com.struts.gl.coagenerator.GlCoaGeneratorForm"/>
			<nested:iterate property="glCoaGenList">
			<bean:define id="account" name="glCoaGenList" type="com.struts.gl.coagenerator.GlCoaGeneratorList"/>	
				<%
					current = account.getSegmentTitle();
					if(!segmentTitle.equals(current)) {
						segmentTitle = current;	
					   	first=true;
					}

					i++;
					if((i % 2) != 0){
						rowBgc = ROW_BGC1;
					}else{
						rowBgc = ROW_BGC2;
					}  

					if(first) {
					%>
						<tr> 
							<td width="450" height="1" align="right" colspan="3" class="gridBoldHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">							
								<nested:write property="segmentTitle" />	
							</td>
						</tr>      
						<tr bgcolor="<%= rowBgc %>">
							<td width="100" height="1" class="control">	
								<nested:text property="segment" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="385" height="1" class="control" colspan="2">							
								<nested:text property="segmentDescription" size="35" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="75" height="1" class="control">		
								<p align="center">					
								<nested:checkbox property="segmentChecked" disabled="true"/>	
							</td>
						</tr>    
					<%
						first=false;									
					} else {
					%>
						<tr bgcolor="<%= rowBgc %>">	
							<td width="100" height="1" class="control">	
								<nested:text property="segment" size="15" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="385" height="1" class="control" colspan="2">							
								<nested:text property="segmentDescription" size="35" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>	
							</td>
							<td width="75" height="1" class="control">		
								<p align="center">					
								<nested:checkbox property="segmentChecked" disabled="true"/>	
							</td>
						</tr>		
					<%
					}
				%>
				</nested:iterate>
				</table>
			</div>
			<div class="tabbertab" title="Branch Map">
 		    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		    <tr> 
		 	  	<td class="prompt" width="575" height="25" colspan="4">
			    </td>
		    </tr>
		    <nested:iterate property="glBCOAList">
		    <tr>
			  	<td class="prompt" width="140" height="25">
			  		<nested:write property="bcoaName"/>
			    </td>
			  	<td width="435" class="control">
			        <nested:checkbox property="branchCheckbox" disabled="true"/>
			  	</td>
		 	</tr>
		 	</nested:iterate>
		 	</table>
 		 	</div>
		   </div><script>tabberAutomatic(tabberOptions)</script>
		</td>
		</tr>

		<tr>			
	    	<td width="575" height="50" colspan="9"> 
	        <div id="buttons">
	        <p align="right">
            	<logic:equal name="glCoaGeneratorForm" property="showSaveButton" value="true">
               	<html:submit property="generateButton" styleClass="mainButton" onclick="return confirmGenerate();">
	              	<bean:message key="button.generate"/>
		       	</html:submit>
		       	</logic:equal>
				<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
		          	<bean:message key="button.delete"/>
		       	</html:submit>
		       	<html:submit property="closeButton" styleClass="mainButton">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
	        <div id="buttonsDisabled" style="display: none;">
	       	<p align="right">
	           	<logic:equal name="glCoaGeneratorForm" property="showSaveButton" value="true">
	           	<html:submit property="generateButton" styleClass="mainButton" disabled="true">
	              	<bean:message key="button.generate"/>
		       	</html:submit>
		       	</logic:equal>
		       	<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.delete"/>
		       	</html:submit>
		       	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
			</td>
	   	</tr>	     	     
	  	</logic:equal>

	   	<tr>
	    	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	   	</tr>
       	  
       	  </table>
		</td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["effectiveDateFrom"] != null &&
        document.forms[0].elements["effectiveDateFrom"].disabled == false)
        document.forms[0].elements["effectiveDateFrom"].focus()
	       // -->
</script>
</body>
</html>
