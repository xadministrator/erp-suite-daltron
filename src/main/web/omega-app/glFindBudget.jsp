<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findBudget.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glFindBudget.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findBudget.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glFindBudgetForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>        
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findBudget.prompt.budgetOrganization"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="budgetOrganization" styleClass="combo">
			      <html:options property="budgetOrganizationList"/>
			   </html:select>
	         </td>
	     </tr>         
         <tr>    
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findBudget.prompt.budgetName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="budgetName" styleClass="combo">
			      <html:options property="budgetNameList"/>
			   </html:select>
	         </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findBudget.prompt.accountNumber"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountNumber" size="25" maxlength="255" styleClass="text"/>
                   <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('accountNumber','accountDescription');"/>	            
                </td>
         </tr>  
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findBudget.prompt.accountDescription"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="accountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>  
                </td>
         </tr>       
         <tr>     
	         <td class="prompt" width="140" height="25">
	            <bean:message key="findBudget.prompt.orderBy"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	         </td>	              
         </tr>
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="glFindBudgetForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
					         <bean:message key="button.first"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.first"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="glFindBudgetForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableLastButton" value="false">
					     <html:submit property="lastButton" styleClass="mainButtonSmall">
					         <bean:message key="button.last"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableLastButton" value="true">
					     <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.last"/>
					     </html:submit>
					     </logic:equal>		     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="glFindBudgetForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.first"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.first"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="glFindBudgetForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableLastButton" value="false">
					     <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.last"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glFindBudgetForm" property="disableLastButton" value="true">
					     <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.last"/>
					     </html:submit>
					     </logic:equal>	     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                  <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                      <bean:message key="findBudget.gridTitle.FBDetails"/>
                   </td>
                </tr>       			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glFBList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="findBudget.prompt.budgetOrganization"/>
			       </td>
			       <td width="570" height="1" class="gridLabel">
			          <nested:write property="budgetOrganization"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.open"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findBudget.prompt.budgetName"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="budgetName"/>
			      </td>		      
			    </tr>		 
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findBudget.prompt.accountNumber"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="accountNumber"/>
			      </td>		      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="findBudget.prompt.accountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="accountDescription"/>
			      </td>		      
			    </tr>   				    			    
			    </nested:iterate>			    		    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["budgetOrganization"] != null &&
        document.forms[0].elements["budgetOrganization"].disabled == false)
        document.forms[0].elements["budgetOrganization"].focus()
   // -->
</script>
</body>
</html>
