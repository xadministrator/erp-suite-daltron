<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findSegment.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickSegment(name) 
{
   
   var property = name.substring(0,name.indexOf("."));    

   if (window.opener && !window.opener.closed) {
       
       window.opener.document.forms[0].elements[document.forms[0].elements["selectedValueSetValueDescription"].value].value = 
           document.forms[0].elements[property + ".valueSetValueDescription"].value;

	   if (document.forms[0].elements["selectedValueSetValue"] != null &&
			document.forms[0].elements["selectedValueSetValue"].value != null &&
				document.forms[0].elements["selectedValueSetValue"].value != "" &&
					window.opener.document.forms[0].elements[document.forms[0].elements["selectedValueSetValue"].value] != undefined) {

			window.opener.document.forms[0].elements[document.forms[0].elements["selectedValueSetValue"].value].value = 
	           document.forms[0].elements[property + ".valueSetValue"].value;
	           
	   }

       if(window.opener.document.forms[0].elements["goFSButton"] != undefined) {
       
	       window.opener.document.forms[0].elements["goFSButton"].value = "true";
    	   window.opener.document.forms[0].submit();
       
       }
	
   }
   
   window.close();

   return false;
   
}

function closeWin() 
{
   window.close();
   
   return false;
}

//Done Hiding--> 
</script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glFindSegment.do?child=1" onsubmit="return disableButtons();">  
  <table border="0" cellpadding="0" cellspacing="0" width="581" height="510">
      <tr valign="top">
        <td width="581" height="510">
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	    	<tr>
	        	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   		<bean:message key="findSegment.title"/>
				</td>
	    	</tr>
            <tr>
	        	<td width="575" height="30" colspan="4" class="statusBar">
		   		<logic:equal name="glFindSegmentForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                <bean:message key="app.success"/>
                </logic:equal>
		   		<html:errors/>	
	        	</td>
	    	</tr>
			<html:hidden property="selectedValueSetName"/>
			<html:hidden property="selectedValueSetValueDescription"/>
			<html:hidden property="selectedValueSetValue"/>			
         	<tr>
				<td width="200" height="2" class="prompt" align="left">
				<bean:message key="findSegment.prompt.segmentName"/>
				</td>
           		<td width="20" height="2" class="control" >
		     	<html:text property="valueSetName" disabled="true" size="15"/>
		   		</td>
				<td width="300" height="2" class="prompt" >
				<bean:message key="findSegment.prompt.segmentDescription"/>
				</td>
		   		<td width="50" height="2" class="control" >
		     	<html:text property="valueSetValueDescription" size="35" maxlength="255" styleClass="text" disabled="true" />
		   		</td>
	     	</tr>          
			<tr>
				<td width="570" height="30" colspan="4">
			    <div id="buttons">
			    <p align="right">
				<html:button property="closeButton" styleClass="mainButton" onclick="return closeWin();">
				<bean:message key="button.close"/>
				</html:button>
				</div>
				<div id="buttonsDisabled" style="display: none;">
			    <p align="right">
				<html:button property="closeButton" styleClass="mainButton" disabled="true">
				<bean:message key="button.close"/>
				</html:button>
				</div>		         
				</td>
			</tr>
			<tr valign="top">
	        	<td width="575" height="185" colspan="4">
		      	<div align="center">
		        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			    	bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     	bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    	<tr>
                    	<td width="575" height="1" colspan="6" class="gridTitle" 
			            	 bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="findSegment.gridTitle.segmentDetails"/>
	                    </td>
	                </tr>
	            	<logic:equal name="glFindSegmentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    	<tr>
			       		<td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          	<bean:message key="findSegment.prompt.segment"/>
			       		</td>
			       		<td width="521" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          	<bean:message key="findSegment.prompt.description"/>
			       		</td>
                	</tr>			   	                    
	            	</tr>			   	                                    
			    		<%
			       			int i = 0;	
			       			String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       			String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       			String rowBgc = null;
			    		%>
			    		<nested:iterate property="glFSList">
			    		<%
			       			i++;
			       			if((i % 2) != 0){
			           			rowBgc = ROW_BGC1;
			       			}else{
			           			rowBgc = ROW_BGC2;
			       			}  
			    		%>
			    		<tr bgcolor="<%= rowBgc %>">
			    			<nested:hidden property="valueSetValueDescription"/>
			    			<nested:hidden property="valueSetValue"/>			    			
			       			<td width="200" height="1" class="gridLabel">
			          		<nested:write property="valueSetValue"/>
			       			</td>
			       			<td width="372" height="1" class="gridLabel">
			          		<nested:write property="valueSetValueDescription"/>
			       			</td>
			       			<td width="149" align="center" height="1">  	
			           		<div id="buttons">                          
	  			            <nested:submit property="openButton" styleClass="gridButton" onclick="return pickSegment(name);">
	                        <bean:message key="button.select"/>
	   	                    </nested:submit>		
  	                        </div>	             
  	                        <div id="buttonsDisabled" style="display: none;">                       
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                        <bean:message key="button.select"/>
  	                        </nested:submit>		
  	                   		</div>
			      			</td>
			    		</tr>
			    </nested:iterate>
			    </logic:equal>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
</body>
</html>


		