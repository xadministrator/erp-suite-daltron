<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="forexRevaluation.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers
//Done Hiding--> 
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButtonButton'));">
<html:form action="/glForexRevaluation.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="forexRevaluation.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="30" colspan="4" class="statusBar">
			   <logic:equal name="glForexRevaluationForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
					<bean:message key="app.success"/>
			   </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	     	<td width="575" height="10" colspan="4">
		     	 <div class="tabber">
	     		 <div class="tabbertab" title="Header">
	     		 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
	     		 	<tr> 
				         <td class="prompt" width="575" height="25" colspan="4">
		                 </td>
		             </tr>
					 <tr>
						<td class="prompt" width="200" height="25">
							<bean:message key="forexRevaluation.prompt.currency"/>
						</td>
						<td width="375" height="25" class="control">
							<html:select property="currency" styleClass="comboRequired" >	              
								<html:options property="currencyList"/>
							</html:select>
						 </td>
					 </tr>
					 </tr>
						 <td class="prompt" width="200" height="25">
							<bean:message key="forexRevaluation.prompt.documentNumber"/>
						 </td>
						 <td width="375" height="25" class="control">
							<html:text property="documentNumber" size="30" maxlength="225" styleClass="text"/>
						 </td>
					 </tr>
					 </tr>
						 <td class="prompt" width="100" height="25">
							<bean:message key="forexRevaluation.prompt.conversionRate"/>
						 </td>
						 <td width="375" height="25" class="control">
							<html:text property="conversionRate" size="20" maxlength="20" styleClass="textRequired"/>
						 </td>
					 </tr>
			         <tr>
						<td class="prompt" width="200" height="25">
						   <bean:message key="forexRevaluation.prompt.accountFrom"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="accountFrom" styleClass="textRequired" size="30" maxlength="225"/>
			               <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountFrom','accountFromDescription');"/>
					    </td>		 
			         </tr>  
			         <tr>
						<td class="prompt" width="180" height="25">
						   <bean:message key="forexRevaluation.prompt.accountFromDescription"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="accountFromDescription" styleClass="text" size="70" maxlength="255" style="font-size:8pt;" disabled="true"/>
					    </td>		 
			         </tr>
			         <tr>
						<td class="prompt" width="200" height="25">
						   <bean:message key="forexRevaluation.prompt.accountTo"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="accountTo" styleClass="textRequired" size="30" maxlength="225"/>
			               <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountTo','accountToDescription');"/>
					    </td>		 
			         </tr>  
			         <tr>
						<td class="prompt" width="200" height="25">
						   <bean:message key="forexRevaluation.prompt.accountToDescription"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="accountToDescription" styleClass="text" size="70" maxlength="255" style="font-size:8pt;" disabled="true"/>
					    </td>		 
			         </tr>
			         <tr>
						<td class="prompt" width="200" height="25">
						   <bean:message key="forexRevaluation.prompt.unrealizedGainLossAccount"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="unrealizedGainLossAccount" styleClass="textRequired" size="30" maxlength="225"/>
			               <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('unrealizedGainLossAccount','unrealizedGainLossAccountDescription');"/>
					    </td>		 
			         </tr>  
			         <tr>
						<td class="prompt" width="200" height="25">
						   <bean:message key="forexRevaluation.prompt.unrealizedGainLossAccountDescription"/>
						</td>
					    <td width="375" height="25" class="control" colspan="3">
			               <html:text property="unrealizedGainLossAccountDescription" styleClass="text" size="70" maxlength="255" style="font-size:8pt;" disabled="true"/>
					    </td>		 
			         </tr>
					 <tr>
						<td width="200" height="25" class="prompt">
							<bean:message key="forexRevaluation.prompt.period"/>
						</td>
						<td width="375" height="25" class="control">
							<html:select property="period" styleClass="comboRequired">
								<html:options property="periodList"/>
							</html:select>
						</td>
					 </tr>
				</table>
				</div></div><script>tabberAutomatic(tabberOptions)</script>
	 		</td>
	     </tr>
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="revalueButton" styleClass="mainButton">
		         <bean:message key="button.revalue"/>
		      </html:submit>
		      <html:submit property="printButton" styleClass="mainButton">
		         <bean:message key="button.print"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="revalueButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.revalue"/>
		      </html:submit>
		      <html:submit property="printButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.print"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["currency"] != null)
             document.forms[0].elements["currency"].focus()
   // -->
</script>

<logic:equal name="glForexRevaluationForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="glForexRevaluationForm" type="com.struts.gl.forexrevaluation.GlForexRevaluationForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
  
  	var conversionRate = "&conversionRate=<%=actionForm.getConversionRate()%>" ;
	var unrealizedGainLossAccount = "&unrealizedGainLossAccount=<%=actionForm.getUnrealizedGainLossAccount()%>";
	var period = "&period=<%=actionForm.getPeriod()%>";
	var accountFrom = "&accountFrom=<%=actionForm.getAccountFrom()%>";
	var accountTo = "&accountTo=<%=actionForm.getAccountTo()%>";
	var currency = "&currency=<%=actionForm.getCurrency()%>";
	var path = conversionRate + unrealizedGainLossAccount + period + accountFrom + accountTo + currency;
  	 
	win = window.open("<%=request.getContextPath()%>/glRepForexRevaluationPrint.do?forward=1" + path,"","height=550,width=750,resizable,menubar,scrollbars");

  //-->
</script>
</logic:equal>

</body>
</html>
