<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="journalBatchPrint.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmPrint()
{
   if(confirm("Are you sure you want to Print selected records?")) return true;
      else return false;     
      
}

function selectAll()
{

   var i = 0;
   
   while(true) {
   
      if (document.forms[0].elements["glJBPList[" + i + "].print"] == null) {
          
          break;
      
      }
      
      document.forms[0].elements["glJBPList[" + i + "].print"].checked = true;
          
      i++;   
   }  
   
   return false;   
      
}

var documentWindow;

function showAdDocument(name) 
{

   var property = name.substring(0,name.indexOf(".")); 
      
   var documentCode = document.forms[0].elements[property + ".journalCode"].value;
   
   var wihe = 'width=' + screen.availWidth + ',height=' + screen.availHeight;
      
   documentWindow = window.open("glJournalEntry.do?forward=1&journalCode=" + documentCode, "glJournalEntry", "resizable,scrollbars,status,titlebar,screenX=0,screenY=0,left=0,top=0," + wihe);     
   
   return false;

}

function refreshPage()
{
   if (documentWindow && documentWindow.closed) {
   
       window.location = window.location.protocol + '//' + window.location.host + window.location.pathname + "?refresh=1";
   
   }

}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));" onfocus="refreshPage();">
<html:form action="/glJournalBatchPrint.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="journalBatchPrint.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glJournalBatchPrintForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="glJournalBatchPrintForm" property="showBatchName" value="true">
	     <tr>
            <td width="160" height="25" class="prompt">
               <bean:message key="journalBatchPrint.prompt.batchName"/>
            </td>
            <td width="415" height="25" class="control" colspan="3">
               <html:select property="batchName" styleClass="combo">
                   <html:options property="batchNameList"/>
               </html:select>
            </td>
         </tr>
         </logic:equal>	 	     
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.name"/>
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="name" size="35" maxlength="50" styleClass="text"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.dateFrom"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.dateTo"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.documentNumberFrom"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.documentNumberTo"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.category"/>
	       </td>
	       <td width="103" height="25" class="control" colspan="3">
	          <html:select property="category" styleClass="combo">
		        <html:options property="categoryList"/>
		      </html:select>
	       </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.source"/>
	       </td>
	       <td width="127" height="25" class="control" colspan="3">
	          <html:select property="source" styleClass="combo">
		        <html:options property="sourceList"/>
		      </html:select>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.currency"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:select property="currency" styleClass="combo">
		        <html:options property="currencyList"/>
		      </html:select>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.posted"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:select property="posted" styleClass="combo">
		        <html:options property="postedList"/>
		      </html:select>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.approvalStatus"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:select property="approvalStatus" styleClass="combo">
		        <html:options property="approvalStatusList"/>
		      </html:select>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.orderBy"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:select property="orderBy" styleClass="combo">
		        <html:options property="orderByList"/>
		      </html:select>
	       </td>
         </tr>      
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="journalBatchPrint.prompt.maxRows"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:text property="maxRows" size="3" maxlength="3" styleClass="textRequired"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="journalBatchPrint.prompt.queryCount"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="queryCount" size="10" maxlength="10" styleClass="text" disabled="true"/>
	       </td>
         </tr>   
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="200" height="50">
			             <div id="buttons">
			             <logic:equal name="glJournalBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="glJournalBatchPrintForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glJournalBatchPrintForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
			             <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="375" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.print"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" onclick="return confirmPrint();">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" onclick="return selectAll();">
				         <bean:message key="button.selectAll"/>
				         </html:submit>	
				         </logic:equal>				         
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="printButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.print"/>
				         </html:submit>
				         <html:submit property="editListButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.editList"/>
				         </html:submit>
				         <html:submit property="selectAllButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.selectAll"/>
				         </html:submit>
				         </logic:equal>				         	
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	        <td width="575" height="185" colspan="4">
		    <div align="center">
		        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			       bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			       bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			       <tr>
                      <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                     <bean:message key="journalBatchPrint.gridTitle.JBPDetails"/>
	                  </td>
	               </tr>	               
	            <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
                <tr>
			       <td width="464" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="journalBatchPrint.prompt.name"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="journalBatchPrint.prompt.category"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="journalBatchPrint.prompt.totalDebit"/>
			       </td>			       			       			       
			       <td width="210" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="journalBatchPrint.prompt.totalCredit"/>
			       </td>
                </tr>	
			    <tr>
			       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="journalBatchPrint.prompt.description"/>
			       </td>
                </tr>		   	               
			       <%
				      int i = 0;	
				      String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				      String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				      String rowBgc = null;
			       %>
			       <nested:iterate property="glJBPList">
			       <%
			          i++;
				      if((i % 2) != 0){
				         rowBgc = ROW_BGC1;
				      }else{
				         rowBgc = ROW_BGC2;
				      }  
			       %>
			       <nested:hidden property="journalCode"/>
			       <tr bgcolor="<%= rowBgc %>">
			          <td width="464" height="1" class="gridLabel">
			             <nested:write property="name"/>
			          </td>
			          <td width="110" height="1" class="gridLabel">
			             <nested:write property="category"/>
			          </td>
			          <td width="110" height="1" class="gridLabelNum">
			             <nested:write property="totalDebit"/>
			          </td>
			          <td width="110" height="1" class="gridLabelNum">
			             <nested:write property="totalCredit"/>
			          </td>			          
			          <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
			          <td width="50" align="center" height="1">
			             <nested:checkbox property="print"/>
			          </td>
			          </logic:equal>
			          <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
			          <td width="50" align="center" height="1">
			             <nested:checkbox property="print" disabled="true"/>
			          </td>
			          </logic:equal>
			          <td width="50" height="1" class="gridLabel">
		                <div id="buttons">
		                <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
				         <bean:message key="button.open"/>
				        </nested:submit>
				        </div>
				        <div id="buttonsDisabled" style="display: none;">
				        <nested:submit property="openButton" disabled="true" styleClass="gridButton">
				         <bean:message key="button.open"/>
				        </nested:submit>
				        </div>
		             </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			        <td width="894" height="1" class="gridLabel" colspan="6">
			         <nested:write property="description"/>
			        </td>
			      </tr>
			    </nested:iterate>
			    </logic:equal>
			    <logic:equal name="glJournalBatchPrintForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			       <%
				      int i = 0;	
				      String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				      String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				      String rowBgc = null;
			       %>
			       <nested:iterate property="glJBPList">
			       <%
			          i++;
				      if((i % 2) != 0){
				         rowBgc = ROW_BGC1;
				      }else{
				         rowBgc = ROW_BGC2;
				      }  
			       %>
			       <nested:hidden property="journalCode"/>
			       <tr bgcolor="<%= rowBgc %>">
			          <td width="175" height="1" class="gridHeader">
			             <bean:message key="journalBatchPrint.prompt.name"/>
			          </td>
			          <td width="570" height="1" class="gridLabel" colspan="2">
			             <nested:write property="name"/>
			          </td>
			          <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">		          
			          <td width="49" align="center" height="1">
		                 <nested:checkbox property="print"/>
			          </td>
			          </logic:equal>
			          <logic:equal name="glJournalBatchPrintForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">		          
			          <td width="49" align="center" height="1">
		                 <nested:checkbox property="print" disabled="true"/>
			          </td>
			          </logic:equal>
			          <td width="100" height="1" class="gridLabel">
		                <div id="buttons">
		                <nested:submit property="openButton" styleClass="gridButton" onclick="return showAdDocument(name);">
				         <bean:message key="button.open"/>
				        </nested:submit>
				        </div>
				        <div id="buttonsDisabled" style="display: none;">
				        <nested:submit property="openButton" disabled="true" styleClass="gridButton">
				         <bean:message key="button.open"/>
				        </nested:submit>
				        </div>
		             </td>
			      </tr>
			      <tr bgcolor="<%= rowBgc %>">
			          <td width="175" height="1" class="gridHeader">
                          <bean:message key="journalBatchPrint.prompt.description"/>
                      </td>
                      <td width="570" height="1" class="gridLabel" colspan="4">
			              <nested:write property="description"/>
			          </td>
			     </tr>
			     <tr bgcolor="<%= rowBgc %>">
			          <td width="175" height="1" class="gridHeader">
			             <bean:message key="journalBatchPrint.prompt.date"/>
			          </td>
			          <td width="350" height="1" class="gridLabel">
			             <nested:write property="date"/>
			          </td>
			          <td width="220" height="1" class="gridHeader">
			             <bean:message key="journalBatchPrint.prompt.documentNumber"/>
			          </td>
			          <td width="149" height="1" class="gridLabel" colspan="2">
			             <nested:write property="documentNumber"/>
			          </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
	                  <td width="175" height="1" class="gridHeader">
	                     <bean:message key="journalBatchPrint.prompt.category"/>
	                  </td>
                      <td width="350" height="1" class="gridLabel">
                         <nested:write property="category"/>
                      </td>
			          <td width="220" height="1" class="gridHeader">
                         <bean:message key="journalBatchPrint.prompt.source"/>
                      </td>
                      <td width="149" height="1" class="gridLabel" colspan="2">
                         <nested:write property="source"/>
                      </td>
		        </tr>
			    <tr bgcolor="<%= rowBgc %>">
                      <td width="175" height="1" class="gridHeader">
                         <bean:message key="journalBatchPrint.prompt.currency"/>
                      </td>
                      <td width="719" height="1" class="gridLabel" colspan="4">
                         <nested:write property="currency"/>
                      </td>
                </tr>
			    <tr bgcolor="<%= rowBgc %>">
                      <td width="175" height="1" class="gridHeader">
                         <bean:message key="journalBatchPrint.prompt.totalDebit"/>
                      </td>
                      <td width="350" height="1" class="gridLabelNum">
                         <nested:write property="totalDebit"/>
                      </td>
                      <td width="220" height="1" class="gridHeader">
                         <bean:message key="journalBatchPrint.prompt.totalCredit"/>
                      </td>
                      <td width="149" height="1" class="gridLabelNum" colspan="2">
                         <nested:write property="totalCredit"/>
                      </td>
                </tr>
			    </nested:iterate>			    
			    </logic:equal>
			 </table>
		     </div>
		    </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["name"] != null)
        document.forms[0].elements["name"].focus()
	       // -->
</script>
<logic:equal name="glJournalBatchPrintForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="glJournalBatchPrintForm" type="com.struts.gl.journalbatchprint.GlJournalBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
    win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='glJournalBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/glRepJournalPrint.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getJournalCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='journalCode[" +<%=j%>+ "]' value='<%=actionForm.getJournalCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>");
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>
<logic:equal name="glJournalBatchPrintForm" property="journalEditListReport" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="glJournalBatchPrintForm" type="com.struts.gl.journalbatchprint.GlJournalBatchPrintForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
    win = window.open("","","height=550,width=750,resizable,menubar,scrollbars");
  	win.document.write("<head></head><body><form name='glJournalBatchPrintForm' id='printForm' action='<%=request.getContextPath()%>/glRepJournalEditList.do?forwardBatch=1' method=post>");
	<%
	for (int j=0; j<actionForm.getJournalCodeList().size(); j++) {
	%>
		win.document.write("<input type=hidden name='journalCode[" +<%=j%>+ "]' value='<%=actionForm.getJournalCodeList().get(j)%>'>");
	<%
	}
	%>
	win.document.write("</form></body>");
	win.document.getElementById("printForm").submit();
	win.focus();		
  //-->
</script>
</logic:equal>
</body>
</html>
