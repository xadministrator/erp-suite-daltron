<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="journalInterfaceMaintenance.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glJournalInterfaceMaintenance.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="journalInterfaceMaintenance.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glJournalInterfaceMaintenanceForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.name"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:text property="name" size="50" maxlength="50" styleClass="text"/>
                </td>
         </tr>
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.description"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="description" size="25" maxlength="25" styleClass="text"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.effectiveDate"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="effectiveDate" size="10" maxlength="10" styleClass="textRequired"/>
                </td>
         </tr>
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.journalCategory"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="journalCategory" size="25" maxlength="25" styleClass="textRequired"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.journalSource"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="journalSource" size="25" maxlength="25" styleClass="textRequired"/>
                </td>
         </tr>         
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.currency"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="currency" size="25" maxlength="25" styleClass="textRequired"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.fundStatus"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="fundStatus" size="10" maxlength="10" styleClass="textRequired"/>
                </td>
         </tr>                  
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.documentNumber"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.dateReversal"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="dateReversal" size="10" maxlength="10" styleClass="text"/>
                </td>
         </tr>                           
	     <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.conversionDate"/>
                </td>
                <td width="128" height="25" class="control">
                   <html:text property="conversionDate" size="10" maxlength="10" styleClass="text"/>
                </td>

                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.conversionRate"/>
                </td>
                <td width="127" height="25" class="control">
                   <html:text property="conversionRate" size="20" maxlength="20" styleClass="textRequired"/>
                </td>
         </tr>                   
         <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="journalInterfaceMaintenance.prompt.reversed"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:checkbox property="reversed"/>
                </td>
         </tr>                          
         <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="saveButton" styleClass="mainButton" onclick="return confirmJournalInterface();">
		            <bean:message key="button.save"/>
		       </html:submit>
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save" />
		       </html:submit>
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	      </tr>	      
	       <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                    <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="journalInterfaceMaintenance.gridTitle.JLIDetails"/>
	                </td>
	            </tr>
	            <tr>
	               <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.lineNumber"/>
			       </td>
			       <td width="275" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.account"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.debitAmount"/>
			       </td>
			       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.creditAmount"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.delete"/>
			       </td>			       			    
			    </tr>
			    <tr>
			       <td width="894" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="journalInterfaceMaintenance.prompt.accountDescription"/>
			       </td>			       		    
			    </tr>
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glJLIList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    
			    <tr bgcolor="<%= rowBgc %>">			       
			       <td width="70" height="1" class="control">
			          <nested:text property="lineNumber" size="4" maxlength="5" styleClass="text" disabled="true"/>
			       </td>
			       <td width="275" height="1" class="control">
			          <nested:text property="account" size="25" maxlength="255" styleClass="textRequired"/>
			          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name, 'account', 'accountDescription');"/>
			       <td width="200" height="1" class="control">
			          <nested:text property="debitAmount" size="17" maxlength="25" styleClass="text"/>
			       </td>
			       <td width="200" height="1" class="control">
			          <nested:text property="creditAmount" size="17" maxlength="25" styleClass="text"/>
			       </td>			       
			       <td width="149" align="center" height="1" class="control">
			          <nested:notEmpty property="journalLineInterfaceCode">
			          <nested:checkbox property="deleteCheckbox"/>
			          </nested:notEmpty>
			       </td>			       
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">			       
			       <td width="894" height="1" class="control" colspan="5">
			          <nested:text property="accountDescription" size="92" maxlength="255" styleClass="text" disabled="true"/>
			       </td>
			    </tr>
			  </nested:iterate>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	         <td width="575" height="25" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
		            <bean:message key="button.addLines"/>
		       </html:submit>			   
	           <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
		            <bean:message key="button.deleteLines"/>
		       </html:submit>			   		       
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.addLines"/>
		       </html:submit>			   
	           <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.deleteLines"/>
		       </html:submit>			   		       
	           </div>
			  </td>
	      </tr>	
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["account"] != null)
        document.forms[0].elements["account"].focus()
	       // -->
</script>
</body>
</html>
