<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="openClosePeriods.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function confirmUpdate()
{

  if (document.forms[0].elements["needsConfirmation"].value != null &&
      document.forms[0].elements["needsConfirmation"].value == 'true') {
    
	  if (document.forms[0].elements["status"].value == 'FUTURE ENTRY') {	                   
	      
	      if(confirm("Never Opened periods exists in prior periods. Do you want to continue and set prior periods to Future Entry?")) return true;
		  else return false;	     
	     
	  } else if (document.forms[0].elements["status"].value == 'OPEN' && 
	      document.forms[0].elements["currentStatus"].value != 'CLOSE') {
	  
	      if(confirm("Never Opened or Future Entry periods exists in prior periods. Do you want to continue and set prior periods to Open?")) return true;
		  else return false;
	   
	  } else if (document.forms[0].elements["status"].value == 'CLOSE') {
	  
	      if(confirm("Open periods exists in prior periods. Do you want to continue and set prior periods to Close?")) return true;
		  else return false;
	  
	  } else if (document.forms[0].elements["status"].value == 'PERMANENTLY CLOSE') {
	  
	      if(confirm("Open or Close periods exists in prior periods. Do you want to continue and set prior periods to Permanently Close?")) return true;
		  else return false;
	  
	  }
	  
  }    
  
  return true;
   
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('updateButton', 'goButton'));">
<html:form action="/glOpenClosePeriods.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="openClosePeriods.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glOpenClosePeriodsForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="needsConfirmation"/>
	     <html:hidden property="currentStatus"/>
	     <tr>
	        <td class="prompt" width="160" height="25">
                   <bean:message key="openClosePeriods.prompt.year"/>
                </td>
                <td width="415" height="25" class="control">            
                   <div id="buttons">
                   <html:select property="year" styleClass="comboRequired">
                      <html:options property="yearList"/>
                   </html:select>
		   		   <html:submit property="goButton" styleClass="mainButtonSmall">
                      <bean:message key="button.go"/>
                   </html:submit>
                   </div>
                   <div id="buttonsDisabled" style="display: none;">
                   <html:select property="year" styleClass="comboRequired">
                      <html:options property="yearList"/>
                   </html:select>
		   		   <html:submit property="goButton" styleClass="mainButtonSmall" disabled="true">
                      <bean:message key="button.go"/>
                   </html:submit>
                   </div>
                </td>
	     </tr>
	     <tr>
	        <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		</td>
	     </tr>
	     <tr>
	        <td width="575" height="5" colspan="4">
		</td>
	     </tr>
	     <logic:equal name="glOpenClosePeriodsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <logic:equal name="glOpenClosePeriodsForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
	     <tr>
	         <td class="prompt" width="160" height="25">
	            <bean:message key="openClosePeriods.prompt.status"/>
	         </td>
	         <td width="115" height="25" class="control">
	            <html:select property="status" styleClass="comboRequired">
		       <html:options property="statusList"/>
	  	    </html:select>
	          </td>
		  <td class="prompt" width="200" height="25">
                     <bean:message key="openClosePeriods.prompt.period"/>
                  </td>
		  <td width="100" height="25" class="label">
		     <bean:write name="glOpenClosePeriodsForm" property="period"/>
		  </td>
	       </tr>
	       <tr>
	          <td width="160" height="25" class="prompt">
		     <bean:message key="openClosePeriods.prompt.periodNumber"/>
		  </td>
		  <td width="115" height="25" class="label">
		     <bean:write name="glOpenClosePeriodsForm" property="periodNumber"/>
		  </td>
		  <td width="200" height="25" class="prompt">
                     <bean:message key="openClosePeriods.prompt.quarterNumber"/>
                  </td>
                  <td width="100" height="25" class="label">
                     <bean:write name="glOpenClosePeriodsForm" property="quarterNumber"/>
                  </td>
	       </tr>
               <tr>
                  <td width="160" height="25" class="prompt">
                     <bean:message key="openClosePeriods.prompt.dateFrom"/>
                  </td>
                  <td width="115" height="25" class="label">
                     <bean:write name="glOpenClosePeriodsForm" property="dateFrom"/>
                  </td>
                  <td width="200" height="25" class="prompt">
                      <bean:message key="openClosePeriods.prompt.dateTo"/>
                  </td>
                  <td width="100" height="25" class="label">
                      <bean:write name="glOpenClosePeriodsForm" property="dateTo"/>
                 </td>
               </tr>	      
	       </logic:equal>
	       </logic:equal>
	       <tr>
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	       
	          <td width="415" height="50" colspan="3"> 
	          <div id="buttons">
	          <p align="right">
		   <logic:equal name="glOpenClosePeriodsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="glOpenClosePeriodsForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton" onclick="return confirmUpdate();">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   <html:submit property="closeButton" styleClass="mainButton">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
		   <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		   <logic:equal name="glOpenClosePeriodsForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="glOpenClosePeriodsForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
                  </td>
	       </tr>
	       <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="openClosePeriods.gridTitle.OCPDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="248" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="openClosePeriods.prompt.periodNumber"/>
			       </td>
			       <td width="248" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="openClosePeriods.prompt.period"/>
			       </td>			       
			       <td width="398" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="openClosePeriods.prompt.status"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glOCPList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="248" height="1" class="gridLabel">
			          <nested:write property="periodNumber"/>
			       </td>
			       <td width="248" height="1" class="gridLabel">
			          <nested:write property="period"/>
			       </td>
			       <td width="249" height="1" class="gridLabel">
			          <nested:write property="status"/>
			       </td>			       
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glOpenClosePeriodsForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				       </div>
				       <div id="buttonsDisabled" style="display: none;">
			          <logic:equal name="glOpenClosePeriodsForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				       </div>
			       </td>
			    </tr>
			    </nested:iterate>
			    </logic:equal>
			    <logic:equal name="glOpenClosePeriodsForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glOCPList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="openClosePeriods.prompt.status"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="status"/>
			       </td>
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glOpenClosePeriodsForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				       </div>
				       <div id="buttonsDisabled" style="display: none;">
			          <logic:equal name="glOpenClosePeriodsForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				       </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="openClosePeriods.prompt.period"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="period"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="openClosePeriods.prompt.periodNumber"/>
			      </td>
			      <td width="149" height="1" class="gridLabelNum">
			         <nested:write property="periodNumber"/>
			      </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
                               <td width="175" height="1" class="gridHeader">
                                  <bean:message key="openClosePeriods.prompt.dateFrom"/>
                               </td>
                               <td width="350" height="1" class="gridLabel">
                                  <nested:write property="dateFrom"/>
                               </td>
                               <td width="220" height="1" class="gridHeader">
                                  <bean:message key="openClosePeriods.prompt.dateTo"/>
                               </td>
                               <td width="149" height="1" class="gridLabel">
                                   <nested:write property="dateTo"/>
                               </td>
                            </tr>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
                                  <bean:message key="openClosePeriods.prompt.quarterNumber"/>
                               </td>
                               <td width="719" height="1" class="gridLabel" colspan="3">
                                  <nested:write property="quarterNumber"/>
                               </td>
			    </tr>
			    </nested:iterate>			    
			    </logic:equal>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
</body>
</html>
