<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="organizationAccountAssignment.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function confirmGo()
{
   if(document.forms[0].elements["accountLow"] != null){
      if(document.forms[0].elements["accountLow"].value != "" ||
         document.forms[0].elements["accountHigh"].value != ""){
         if(confirm("Go without saving?")) return true;
         else return false;
      }
   }
   return true;
}
//Done Hiding--> 
</script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/glOrganizationAccountAssignment.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="organizationAccountAssignment.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="2" class="statusBar">
		<logic:equal name="glOrganizationAccountAssignmentForm" property="txnStatus" 
		   value="<%=Constants.STATUS_SUCCESS%>">
		   <bean:message key="app.success"/>
		</logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="150" height="25">
	          <bean:message key="organizationAccountAssignment.prompt.organization"/>
	       </td>
               <td width="425" height="25" class="control">	          
		  <div id="buttons">
		  <html:select property="organization" styleClass="comboRequired">
			     <html:options property="organizationList"/>
			  </html:select>
		  <html:submit property="goButton" styleClass="mainButtonSmall"  onclick="return confirmGo();">
                      <bean:message key="button.go"/>
                   </html:submit>	
           </div>						 
           <div id="buttonsDisabled" style="display: none;">
           <html:select property="organization" styleClass="comboRequired">
			     <html:options property="organizationList"/>
			  </html:select>
		  <html:submit property="goButton" styleClass="mainButtonSmall" disabled="true">
                      <bean:message key="button.go"/>
                   </html:submit>	
           </div>
	       </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="150" height="25">
	          <bean:message key="organizationAccountAssignment.prompt.description"/>
	       </td>
	       <td width="425" height="25" class="label">
	          <bean:write name="glOrganizationAccountAssignmentForm" property="description"/>
	       </td>
	     </tr>  
             <tr>
                  <td width="575" height="50" colspan="2"> 
                    <div id="buttons">
                    <p align="right">
                    <html:submit property="closeButton" styleClass="mainButton">
                       <bean:message key="button.close"/>
                    </html:submit>
                    </div>
                    <div id="buttonsDisabled" style="display: none;">
                    <p align="right">
                    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
                       <bean:message key="button.close"/>
                    </html:submit>
                    </div>
                 </td>
              </tr>
             <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission"
                value="<%=Constants.FULL_ACCESS%>">
                <tr>
                   <td width="575" height="3" colspan="2" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
                   </td>
                </tr>
                <tr>
                   <td width="575" height="5" colspan="2">
                   </td>
                </tr>
                <tr>
	           <td class="prompt" width="150" height="25">
	              <bean:message key="organizationAccountAssignment.prompt.accountLow"/>
	           </td>
	           <td width="425" height="25" class="control">
		      <html:text property="accountLow" size="30" maxlength="255"  styleClass="textRequired"/>
		      <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountLow','');"/>	            
	           </td>
                </tr>
                <tr>
                   <td class="prompt" width="150" height="25">
	              <bean:message key="organizationAccountAssignment.prompt.accountHigh"/>
	           </td>
	           <td width="425" height="25" class="control">
		      <html:text property="accountHigh" size="30" maxlength="255"  styleClass="textRequired"/>
		      <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountHigh','');"/>	            
	           </td>
	        </tr>
             </logic:equal> 
	       <tr>
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	      	       
	          <td width="415" height="50"> 
	          <div id="buttons">
	          <p align="right">
		   <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
		         value="<%=Constants.PAGE_STATE_SAVE%>">
		         <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		         </html:submit>
		      </logic:equal>
		      <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
		         value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   </div>
		   <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		   <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		      <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
		         value="<%=Constants.PAGE_STATE_SAVE%>">
		         <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		         </html:submit>
		      </logic:equal>
		      <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
		         value="<%=Constants.PAGE_STATE_EDIT%>">
		         <html:submit property="updateButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.update"/>
		         </html:submit>
		         <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.cancel"/>
		         </html:submit>
		      </logic:equal>
		   </logic:equal>
		   </div>
                  </td>
	       </tr>
	       <tr valign="top">
	          <td width="575" height="185" colspan="2">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="organizationAccountAssignment.gridTitle.ARDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="248" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="organizationAccountAssignment.prompt.lineNumber"/>
			       </td>
			       <td width="248" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="organizationAccountAssignment.prompt.accountLow"/>
			       </td>			       
			       <td width="398" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="organizationAccountAssignment.prompt.accountHigh"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glARList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="248" height="1" class="gridLabel">
			          <nested:write property="lineNumber"/>
			       </td>
			       <td width="248" height="1" class="gridLabel">
			          <nested:write property="accountLow"/>
			       </td>			       
			       <td width="249" height="1" class="gridLabel">
			          <nested:write property="accountHigh"/>
			       </td>
			       <td width="149" align="center" height="1">
			             <div id="buttons">
  	                          <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" 
				          value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
				          value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                            onclick="return confirmDelete();"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  </logic:equal>
				  </div>
				          <div id="buttonsDisabled" style="display: none;">
  	                          <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" 
				          value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
				          value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                             disabled="true"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  </logic:equal>
				  </div>
			       </td>
			    </tr>
		            </nested:iterate>
                    </logic:equal>
                    <logic:equal name="glOrganizationAccountAssignmentForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        		            
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glARList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="275" height="1" class="gridHeader">
			          <bean:message key="organizationAccountAssignment.prompt.lineNumber"/>
			       </td>
			       <td width="441" height="1" class="gridLabel">
			          <nested:write property="lineNumber"/>
			       </td>
			       <td width="149" align="center" height="1">
			             <div id="buttons">
  	                          <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" 
				          value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
				          value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                            onclick="return confirmDelete();"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  </logic:equal>
				  </div>
				          <div id="buttonsDisabled" style="display: none;">
  	                          <logic:equal name="glOrganizationAccountAssignmentForm" property="userPermission" 
				          value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
			             <logic:equal name="glOrganizationAccountAssignmentForm" property="pageState" 
				          value="<%=Constants.PAGE_STATE_SAVE%>">
   			                <nested:submit property="deleteButton" styleClass="gridButton" 
					                             disabled="true"> 
			                   <bean:message key="button.delete"/>
				        </nested:submit>
			             </logic:equal>
				  </logic:equal>
				  </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="275"  height="1" class="gridHeader">
			          <bean:message key="organizationAccountAssignment.prompt.accountLow"/>
			       </td>
			       <td width="590" height="1" class="gridLabel" colspan="2">
			          <nested:write property="accountLow"/>
			       </td>
                            </tr>
			    <tr  bgcolor="<%= rowBgc %>">
  			       <td width="275" height="1" class="gridHeader">
                                  <bean:message key="organizationAccountAssignment.prompt.accountHigh"/>
                               </td>
                               <td width="590" height="1" class="gridLabel" colspan="2">
                                  <nested:write property="accountHigh"/>
                               </td>     										
			    </tr>	    
		            </nested:iterate>		            
                    </logic:equal>		            
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="2" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
    if(document.forms[0].elements["accountLow"] != null)
       document.forms[0].elements["accountLow"].focus()
  // -->
</script>
</body>
</html>
