<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="recurringJournalEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

  var tempAmount = 0;
  
  function initializeTempDebit(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalDebit(name)
  {
        
      var debitAmount = 0;
      var totalDebit = (document.forms[0].elements["totalDebit"].value).replace(/,/g,'');
      var amount;
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          debitAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalDebit - (tempAmount - debitAmount))) {
      
	      amount = (1 * totalDebit - (1 * tempAmount - 1 * debitAmount)).toFixed(2);
	      document.forms[0].elements["totalDebit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = debitAmount;     
	      
	  }
	        
  }  
  
    
  function initializeTempCredit(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalCredit(name)
  {
        
      var creditAmount = 0;
      var totalCredit = (document.forms[0].elements["totalCredit"].value).replace(/,/g,'');
      var amount;
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          creditAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalCredit - (tempAmount - creditAmount))) {
      
	      amount = (1 * totalCredit - (1 * tempAmount - 1 * creditAmount)).toFixed(2);
	      document.forms[0].elements["totalCredit"].value = formatDisabledAmount(amount.toString());
	      tempAmount = creditAmount;     
	      
	  }
	        
  }  

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glRecurringJournalEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="recurringJournalEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glRecurringJournalEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="glRecurringJournalEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <logic:equal name="glRecurringJournalEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
                       			<html:options property="batchNameList"/>
                   				</html:select>
               	 			</td>
         				</tr>
         				</logic:equal>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.name"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:text property="name" size="25" maxlength="50" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.description"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.category"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:select property="category" styleClass="comboRequired" style="width:130;">
                       			<html:options property="categoryList"/>
                   				</html:select>
                			</td>
         				</tr>
         				<tr>
         					<td width="160" height="25" class="prompt">
         						<bean:message key="recurringJournalEntry.prompt.totalDebit"/>
         					</td>
         					<td width="128" height="25" class="control">
         						<html:text property="totalDebit" size="17" maxlength="25" styleClass="textAmount" disabled="true"/>
         					</td>
         					<td width="160" height="25" class="prompt">
         						<bean:message key="recurringJournalEntry.prompt.totalCredit"/>
         					</td>
         					<td width="127" height="25" class="control">
         						<html:text property="totalCredit" size="17" maxlength="25" styleClass="textAmount" disabled="true"/>
         					</td>
         				</tr>         
					</table>
					</div>
				    <div class="tabbertab" title="Schedule">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="reminderSchedule" styleClass="comboRequired">
			                       <html:options property="reminderScheduleList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired"/>
			                </td>
			                <td class="prompt" width="160" height="25">
			                   <bean:message key="recurringJournalEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>
			        <div class="tabbertab" title="Notification">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			             <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser1"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser1" styleClass="comboRequired">
			                       <html:options property="notifiedUser1List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser2"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser2" styleClass="combo">
			                       <html:options property="notifiedUser2List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser3"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser3" styleClass="combo">
			                       <html:options property="notifiedUser3List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser4"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser4" styleClass="combo">
			                       <html:options property="notifiedUser4List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser5"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="notifiedUser3" styleClass="combo">
			                       <html:options property="notifiedUser5List"/>
			                   </html:select>
			                </td>
				         </tr>
				    </table>
				    </div>				    					 
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="glRecurringJournalEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="glRecurringJournalEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="recurringJournalEntry.gridTitle.RJDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.lineNumber"/>
				       </td>
				       <td width="395" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.account"/>
				       </td>
				       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.debitAmount"/>
				       </td>
				       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       <td width="894" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.accountDescription"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="glRJList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="5" styleClass="text" disabled="true"/>
				       </td>
				       <td width="395" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'account', 'accountDescription');"/>
				       <td width="200" height="1" class="control">
				          <nested:text property="debitAmount" size="17" maxlength="25" styleClass="textAmount" onblur="calculateTotalDebit(name); addZeroes(name);" onkeyup="calculateTotalDebit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempDebit(name);"/>
				       </td>
				       <td width="200" height="1" class="control">
				          <nested:text property="creditAmount" size="17" maxlength="25" styleClass="textAmount" onblur="calculateTotalCredit(name); addZeroes(name);" onkeyup="calculateTotalCredit(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempCredit(name);"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="894" height="1" class="control" colspan="5">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="glRecurringJournalEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="glRecurringJournalEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	     
	     <logic:equal name="glRecurringJournalEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <logic:equal name="glRecurringJournalEntryForm" property="showBatchName" value="true">
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.batchName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
               	 			</td>
         				</tr>
         				</logic:equal>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.name"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:text property="name" size="25" maxlength="50" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.description"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="recurringJournalEntry.prompt.category"/>
                			</td>
                			<td width="415" height="25" class="control" colspan="3">
                   				<html:select property="category" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="categoryList"/>
                   				</html:select>
                			</td>
         				</tr>         
					</table>
					</div>
				    <div class="tabbertab" title="Schedule">
   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.reminderSchedule"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="reminderSchedule" styleClass="comboRequired" disabled="true">
			                       <html:options property="reminderScheduleList"/>
			                   </html:select>
			                </td>
				         </tr>
					     <tr>				                
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.nextRunDate"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
			                </td>
			                <td class="prompt" width="160" height="25">
			                   <bean:message key="recurringJournalEntry.prompt.lastRunDate"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="lastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                
					</table>
					</div>
			        <div class="tabbertab" title="Notification">
			        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
			             <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
					     <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser1"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser1" styleClass="comboRequired" disabled="true">
			                       <html:options property="notifiedUser1List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser2"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser2" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser2List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser3"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:select property="notifiedUser3" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser3List"/>
			                   </html:select>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser4"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:select property="notifiedUser4" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser4List"/>
			                   </html:select>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="recurringJournalEntry.prompt.notifiedUser5"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:select property="notifiedUser3" styleClass="combo" disabled="true">
			                       <html:options property="notifiedUser5List"/>
			                   </html:select>
			                </td>
				         </tr>
				    </table>
				    </div>				    					 
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="glRecurringJournalEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="glRecurringJournalEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="recurringJournalEntry.gridTitle.RJDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.lineNumber"/>
				       </td>
				       <td width="395" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.account"/>
				       </td>
				       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.debitAmount"/>
				       </td>
				       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.creditAmount"/>
				       </td>
				       <td width="49" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.delete"/>
				       </td>			       			    
				    </tr>							    
				    <tr>
				       <td width="894" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalEntry.prompt.accountDescription"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="glRJList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="5" styleClass="text" disabled="true"/>
				       </td>
				       <td width="395" height="1" class="control">
				          <nested:text property="account" size="28" maxlength="255" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       <td width="200" height="1" class="control">
				          <nested:text property="debitAmount" size="17" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				       <td width="200" height="1" class="control">
				          <nested:text property="creditAmount" size="17" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>			       
				       <td width="49" align="center" height="1" class="control">
				          <nested:checkbox property="deleteCheckbox" disabled="true"/>
				       </td>			       
				    </tr>							    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="894" height="1" class="control" colspan="5">
				          <nested:text property="accountDescription" size="80" maxlength="255" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="glRecurringJournalEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="glRecurringJournalEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="glRecurringJournalEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
	       // -->
</script>
</body>
</html>
