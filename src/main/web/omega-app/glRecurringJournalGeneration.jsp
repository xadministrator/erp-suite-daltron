<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="recurringJournalGeneration.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers
function confirmGenerate()
{
   if(confirm("Are you sure you want to generate selected records?")) return true;
      else return false;
}
//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRecurringJournalGeneration.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="recurringJournalGeneration.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glRecurringJournalGenerationForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>	     
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="recurringJournalGeneration.prompt.name"/>
	       </td>
	       <td width="390" height="25" class="control" colspan="3">
	          <html:text property="name" size="35" maxlength="50" styleClass="text"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="recurringJournalGeneration.prompt.nextRunDateFrom"/>
	       </td>
	       <td width="103" height="25" class="control">
	          <html:text property="nextRunDateFrom" size="10" maxlength="10" styleClass="text"/>
	       </td>
	       <td class="prompt" width="160" height="25">
	          <bean:message key="recurringJournalGeneration.prompt.nextRunDateTo"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:text property="nextRunDateTo" size="10" maxlength="10" styleClass="text"/>
	       </td>
         </tr>               
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="recurringJournalGeneration.prompt.category"/>
	       </td>
	       <td width="103" height="25" class="control" colspan="3">
	          <html:select property="category" styleClass="combo">
		        <html:options property="categoryList"/>
		      </html:select>
	       </td>
	       </tr>
	       <tr>
	       <td class="prompt" width="160" height="25" colspan="3">
	          <bean:message key="recurringJournalGeneration.prompt.orderBy"/>
	       </td>
	       <td width="127" height="25" class="control">
	          <html:select property="orderBy" styleClass="combo">
		        <html:options property="orderByList"/>
		      </html:select>
	       </td>
         </tr>        
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="glRecurringJournalGenerationForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>		     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="glRecurringJournalGenerationForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="glRecurringJournalGenerationForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="glRecurringJournalGenerationForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="generateButton" styleClass="mainButton" onclick="return confirmGenerate();">
				         <bean:message key="button.generate"/>
				         </html:submit>
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <logic:equal name="glRecurringJournalGenerationForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <html:submit property="generateButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.generate"/>
				         </html:submit>
				         </logic:equal>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="575" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                    <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                        	<bean:message key="recurringJournalGeneration.gridTitle.RJDetails"/>
		                </td>
		            </tr>
		            <tr>
		               <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.name"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.nextRunDate"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.documentNumber"/>
				       </td>
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.journalDate"/>
				       </td>
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.generate"/>
				       </td>				       			       			    
				    </tr>
		            <tr>
				       <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.reminderSchedule"/>
				       </td>		            
				       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.lastRunDate"/>
				       </td>		            
		               <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.totalDebit"/>
				       </td>
				       <td width="219" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="recurringJournalGeneration.prompt.totalCredit"/>
				       </td>			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="glRJList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    
				    <tr bgcolor="<%= rowBgc %>">			       
				       <td width="200" height="1" class="control">
				          <nested:text property="name" size="35" maxlength="25" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="nextRunDate" size="10" maxlength="10" styleClass="textRequired"/>		 
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="documentNumber" size="15" maxlength="25" styleClass="text"/>
				       </td>				       
				       <td width="149" height="1" class="control">
				          <nested:text property="journalDate" size="15" maxlength="10" styleClass="textRequired"/>
				       </td>
				       <td width="70" align="center" height="1" class="control">
				          <logic:equal name="glRecurringJournalGenerationForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				          <nested:checkbox property="generate"/>
				          </logic:equal>
				          <logic:equal name="glRecurringJournalGenerationForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
				          <nested:checkbox property="generate" disabled="true"/>
				          </logic:equal>
				       </td>				       			       			       
				    </tr>				    
				    <tr bgcolor="<%= rowBgc %>">
				       <td width="200" height="1" class="control">
				          <nested:text property="reminderSchedule" size="35" maxlength="25" styleClass="text" disabled="true"/>
				       </td>				    			       
				       <td width="149" height="1" class="control">
				          <nested:text property="lastRunDate" size="10" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="149" height="1" class="control">
				          <nested:text property="totalDebit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>		 			       			       
				       </td>
				       <td width="219" height="1" class="control" colspan="2">
				          <nested:text property="totalCredit" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
				       </td>
				    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
	       // -->
</script>
</body>
</html>
