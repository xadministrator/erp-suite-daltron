<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>


<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="csvQuarterlyVatReturn.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 

  function submitForm()
  {            
      disableButtons();
      enableInputControls();
  }
  
  var tempAmount = 0;
  
  function initializeTempServices(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalServices(name)
  {
        
      var services = 0;
      var totalServices = (document.forms[0].elements["totalServices"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          services = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalServices - (tempAmount - services))) {
      
	      document.forms[0].elements["totalServices"].value = (1 * totalServices - (1 * tempAmount - 1 * services)).toFixed(2);
	      tempAmount = services;     
	      
	  }
            
  }
  
  function initializeTempCapitalGoods(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalCapitalGoods(name)
  {
        
      var capitalGoods = 0;
      var totalCapitalGoods = (document.forms[0].elements["totalCapitalGoods"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          capitalGoods = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalCapitalGoods - (tempAmount - capitalGoods))) {
      
	      document.forms[0].elements["totalCapitalGoods"].value = (1 * totalCapitalGoods - (1 * tempAmount - 1 * capitalGoods)).toFixed(2);
	      tempAmount = capitalGoods;     
	      
	  }
            
  }
  
  function initializeTempGoodsOtherThanCp(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalGoodsOtherThanCp(name)
  {
        
      var goodsOtherThanCp = 0;
      var totalGoodsOtherThanCp = (document.forms[0].elements["totalGoodsOtherThanCp"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          goodsOtherThanCp = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalGoodsOtherThanCp - (tempAmount - goodsOtherThanCp))) {
      
	      document.forms[0].elements["totalGoodsOtherThanCp"].value = (1 * totalGoodsOtherThanCp - (1 * tempAmount - 1 * goodsOtherThanCp)).toFixed(2);
	      tempAmount = goodsOtherThanCp;     
	      
	  }
            
  }
  
  function initializeTempExemptSales(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalExemptSales(name)
  {
        
      var exemptSales = 0;
      var totalExemptSales = (document.forms[0].elements["totalExemptSales"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          exemptSales = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalExemptSales - (tempAmount - exemptSales))) {
      
	      document.forms[0].elements["totalExemptSales"].value = (1 * totalExemptSales - (1 * tempAmount - 1 * exemptSales)).toFixed(2);
	      tempAmount = exemptSales;     
	      
	  }
            
  }
  
  function initializeTempZeroRatedSales(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalZeroRatedSales(name)
  {
        
      var zeroRatedSales = 0;
      var totalZeroRatedSales = (document.forms[0].elements["totalZeroRatedSales"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          zeroRatedSales = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalZeroRatedSales - (tempAmount - zeroRatedSales))) {
      
	      document.forms[0].elements["totalZeroRatedSales"].value = (1 * totalZeroRatedSales - (1 * tempAmount - 1 * zeroRatedSales)).toFixed(2);
	      tempAmount = zeroRatedSales;     
	      
	  }
            
  }
  
  function initializeTempTaxableSales(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalTaxableSales(name)
  {
        
      var taxableSales = 0;
      var totalTaxableSales = (document.forms[0].elements["totalTaxableSales"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          taxableSales = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalTaxableSales - (tempAmount - taxableSales))) {
      
	      document.forms[0].elements["totalTaxableSales"].value = (1 * totalTaxableSales - (1 * tempAmount - 1 * taxableSales)).toFixed(2);
	      tempAmount = taxableSales;     
	      
	  }
            
  }
  
  function initializeTempOutputTax(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalOutputTax(name)
  {
        
      var outputTax = 0;
      var totalOutputTax = (document.forms[0].elements["totalOutputTax"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          outputTax = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalOutputTax - (tempAmount - outputTax))) {
      
	      document.forms[0].elements["totalOutputTax"].value = (1 * totalOutputTax - (1 * tempAmount - 1 * outputTax)).toFixed(2);
	      tempAmount = outputTax;     
	      
	  }
            
  }
  
  function initializeTempExemptPurchases(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalExemptPurchases(name)
  {
        
      var exemptPurchases = 0;
      var totalExemptPurchases = (document.forms[0].elements["totalExemptPurchases"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          exemptPurchases = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalExemptPurchases - (tempAmount - exemptPurchases))) {
      
	      document.forms[0].elements["totalExemptPurchases"].value = (1 * totalExemptPurchases - (1 * tempAmount - 1 * exemptPurchases)).toFixed(2);
	      tempAmount = exemptPurchases;     
	      
	  }
            
  }
  
  function initializeTempDutiableValue(name)
  {
     
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      } else {
      
          tempAmount = 0;
  
      }
            
  }
  
  function calculateTotalDutiableValue(name)
  {
  
  	  var dutiableValue = 0;
  	  var totalDutiableValue = (document.forms[0].elements["totalDutiableValue"].value).replace(/,/g,'');
  
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          dutiableValue = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      }         
      
      if (!isNaN(totalDutiableValue - (tempAmount - dutiableValue))) {
      
	      document.forms[0].elements["totalDutiableValue"].value = (1 * totalDutiableValue - (1 * tempAmount - 1 * dutiableValue)).toFixed(2);
	      tempAmount = dutiableValue;     
	
	      
	  }
	  
  }
  
  function initializeTempOtherCharges(name)
  {
     
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      } else {
      
          tempAmount = 0;
  
      }
            
  }
  
  function calculateTotalOtherCharges(name)
  {
  
  	  var otherCharges = 0;
  	  var totalOtherCharges = (document.forms[0].elements["totalOtherCharges"].value).replace(/,/g,'');
  
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          otherCharges = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      }         
      
      if (!isNaN(totalOtherCharges - (tempAmount - otherCharges))) {
      
	      document.forms[0].elements["totalOtherCharges"].value = (1 * totalOtherCharges - (1 * tempAmount - 1 * otherCharges)).toFixed(2);
	      tempAmount = otherCharges;     
	
	      
	  }
	  
  }
  
  function initializeTempExemptImportation(name)
  {
     
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      } else {
      
          tempAmount = 0;
  
      }
            
  }  
  
  function calculateTotalExemptImportation(name)
  {
  
  	  var exemptImportation = 0;
  	  var totalExemptImportation = (document.forms[0].elements["totalExemptImportations"].value).replace(/,/g,'');
  
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          exemptImportation = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      }         
      
      if (!isNaN(totalExemptImportation - (tempAmount - exemptImportation))) {
      
	      document.forms[0].elements["totalExemptImportations"].value = (1 * totalExemptImportation - (1 * tempAmount - 1 * exemptImportation)).toFixed(2);
	      tempAmount = exemptImportation;     
		      
	  }
	  
  }
  
  function initializeTempTaxableImportation(name)
  {
     
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      } else {
      
          tempAmount = 0;
  
      }
            
  }
  
  function calculateTotalTaxableImportation(name)
  {
  
  	  var taxableImportation = 0;
  	  var totalTaxableImportation = (document.forms[0].elements["totalTaxableImportations"].value).replace(/,/g,'');
  
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          taxableImportation = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      }         
      
      if (!isNaN(totalTaxableImportation - (tempAmount - taxableImportation))) {
      
	      document.forms[0].elements["totalTaxableImportations"].value = (1 * totalTaxableImportation - (1 * tempAmount - 1 * taxableImportation)).toFixed(2);
	      tempAmount = taxableImportation;     
		      
	  }
	  
  }
  
  function initializeTempVatPaid(name)
  {
     
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      } else {
      
          tempAmount = 0;
  
      }
            
  }
  
  function calculateTotalVatPaid(name)
  {
  
  	  var vatPaid = 0;
  	  var totalVatPaid = (document.forms[0].elements["totalVatPaid"].value).replace(/,/g,'');
  
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          vatPaid = (document.forms[0].elements[name].value).replace(/,/g,'');
  
      }         
      
      if (!isNaN(totalVatPaid - (tempAmount - vatPaid))) {
      
	      document.forms[0].elements["totalVatPaid"].value = (1 * totalVatPaid - (1 * tempAmount - 1 * vatPaid)).toFixed(2);
	      tempAmount = vatPaid;     
		      
	  }
	  
  }
  
  function initializeTempZeroRatedPurchases(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalZeroRatedPurchases(name)
  {
        
      var zeroRatedPurchases = 0;
      var totalZeroRatedPurchases = (document.forms[0].elements["totalZeroRatedPurchases"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          zeroRatedPurchases = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalZeroRatedPurchases - (tempAmount - zeroRatedPurchases))) {
      
	      document.forms[0].elements["totalZeroRatedPurchases"].value = (1 * totalZeroRatedPurchases - (1 * tempAmount - 1 * zeroRatedPurchases)).toFixed(2);
	      tempAmount = zeroRatedPurchases;     
	      
	  }
            
  }
 
  function initializeTempInputTax(name)
  {
              
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          tempAmount = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      } else {
      
          tempAmount = 0;
      
      }
            
  }
    
  function calculateTotalInputTax(name)
  {
        
      var inputTax = 0;
      var totalInputTax = (document.forms[0].elements["totalInputTax"].value).replace(/,/g,'');
        
      if (!isNaN(parseFloat(document.forms[0].elements[name].value))) {

          inputTax = (document.forms[0].elements[name].value).replace(/,/g,'');
                
      }         
      
      if (!isNaN(totalInputTax - (tempAmount - inputTax))) {
      
	      document.forms[0].elements["totalInputTax"].value = (1 * totalInputTax - (1 * tempAmount - 1 * inputTax)).toFixed(2);
	      tempAmount = inputTax;     
	      
	  }
            
  }

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRepCsvQuarterlyVatReturn.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="825" height="250">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="csvQuarterlyVatReturn.title"/>
		</td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="glRepCsvQuarterlyVatReturnForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>	     
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>	
            <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.registeredName"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="registeredName" size="20" maxlength="50" styleClass="text"/>
	         </td>         	          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.tinOwner"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="tinOwner" size="10" maxlength="9" styleClass="textRequired"/>
	         </td>	         
         </tr>
         <tr>	 
             <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.firstName"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="firstName" size="30" maxlength="30" styleClass="text"/>
	         </td>   
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.tradeName"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="tradeName" size="20" maxlength="50" styleClass="textRequired"/>
	         </td>    	          
         </tr>
         <tr>	         	          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.middleName"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="middleName" size="30" maxlength="30" styleClass="text"/>
	         </td>
	        <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.address1"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="address1" size="30" maxlength="30" styleClass="textRequired"/>
	         </td>  
         </tr>
         <tr>	       
             <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.lastName"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="lastName" size="30" maxlength="30" styleClass="text"/>
	         </td>        	          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.address2"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="address2" size="30" maxlength="30" styleClass="text"/>
	         </td>
         </tr> 
         <tr>	         	          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.rdoCode"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="rdoCode" size="3" maxlength="3" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.taxableMonth"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="taxableMonth" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>	         	          
	         <td class="prompt" width="140" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.fiscalYearEnding"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="fiscalYearEnding" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="160" height="25">
	            <bean:message key="csvQuarterlyVatReturn.prompt.reportType"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="reportType" styleClass="comboRequired">
	               <html:options property="reportTypeList"/>	          
	            </html:select>
	         </td>
         </tr>
         <tr>
           <td width="575" height="50" colspan="4"> 
	         <div id="buttons">
	         <p align="right">	
	             <html:submit property="goButton" styleClass="mainButton">
		            <bean:message key="button.go"/>
		         </html:submit>		         
		         <html:submit property="validateGenerateButton" styleClass="mainButtonBig">
		            <bean:message key="button.validateGenerate"/>
		         </html:submit>		         
		         <html:submit property="closeButton" styleClass="mainButton">
		            <bean:message key="button.close"/>
		         </html:submit>
	         </div>
	         <div id="buttonsDisabled" style="display: none;">
	         <p align="right">	
	             <html:submit property="goButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.go"/>
		         </html:submit>		         
		         <html:submit property="validateGenerateButton" styleClass="mainButtonBig" disabled="true">
		            <bean:message key="button.validateGenerate"/>
		         </html:submit>		         
		         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.close"/>
		         </html:submit>
		      </div>
	        </td>
	      </tr>	
	     <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div id="sales">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
				      <tr>
	                     <td width="575" height="1" class="gridTitle" 
				             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                     <bean:message key="csvQuarterlyVatReturn.gridTitle.SLSDetails"/>
		                 </td>
		              </tr>
			          <tr>
				          <td width="140" height="10">		     
					  	  </td>
				      </tr>
		              <tr valign="top">
			          <td width="140" height="1">
				      <div align="center">
				      <table border="0" cellpadding="0" cellspacing="0" width="577" height="47">
				      <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.exemptSales"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalExemptSales" size="16" maxlength="16" style="font-size:8pt;" styleClass="text" disabled="true"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.zeroRatedSales"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalZeroRatedSales" size="16" maxlength="16" style="font-size:8pt;" styleClass="text" disabled="true"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.taxableSales"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalTaxableSales" size="16" maxlength="16" style="font-size:8pt;" styleClass="text" disabled="true"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.outputTax"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalOutputTax" size="16" maxlength="16" style="font-size:8pt;" styleClass="text" disabled="true"/>
				         </td>
			         </tr>
			         </table>
				     </div>
				     </td>
			       	 </tr>
			         <tr>
				          <td width="140" height="10">		     
					  	  </td>
				     </tr> 
		             <tr valign="top">
			          <td width="575" height="185">
				         <div align="center">
				         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
				             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
						     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					    <tr>
					       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.lineNumber"/>
					       </td>
					       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.tinOfCustomer"/>
					       </td>
						   <td width="154" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.registeredNameSales"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.taxableMonth"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.taxableSales"/>
					       </td>  
						   <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.exemptSales"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.zeroRatedSales"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.outputTax"/>
					       </td>               				       				       					       					                    
						   <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.tinOwner"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.lastNameSales"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.firstNameSales"/>
					       </td>					       					       					       
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.middleNameSales"/>
					       </td>
						   <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.address1Sales"/>
					       </td>					       					       					       
					       <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.address2Sales"/>
					       </td>
					    </tr>      
					    <%
					       int i = 0;	
					       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       String rowBgc = null;
					    %>
					    <nested:iterate property="csvSLList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="50" height="1" class="control">
				              <nested:text property="lineNumber" size="1" maxlength="3" style="font-size:8pt;" styleClass="text" disabled="true"/>
				           </td>
				           <td width="115" height="1" class="control">
				              <nested:text property="tinOfCustomer" size="11" maxlength="9" style="font-size:8pt;" styleClass="textRequired"/>
				           </td>
		                   <td width="154" height="1" class="control">
				              <nested:text property="registeredNameSales" size="15" maxlength="50" style="font-size:8pt;" styleClass="text"/>
				           </td>
					       <td width="110" height="1" class="control">
				              <nested:text property="taxableMonthSales" size="10" maxlength="10" style="font-size:8pt;" styleClass="textRequired"/>
				           </td> 
				           <td width="110" height="1" class="control">
				              <nested:text property="taxableSales" size="10" maxlength="16" style="font-size:8pt;"styleClass="textRequired" onblur="calculateTotalTaxableSales(name);" onkeyup="calculateTotalTaxableSales(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempTaxableSales(name);"/>
				           </td> 
						   <td width="110" height="1" class="control">
				              <nested:text property="exemptSales" size="10" maxlength="16" style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalExemptSales(name);" onkeyup="calculateTotalExemptSales(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempExemptSales(name);"/>
				           </td>
					       <td width="110" height="1" class="control">
				              <nested:text property="zeroRatedSales" size="10" maxlength="16" style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalZeroRatedSales(name);" onkeyup="calculateTotalZeroRatedSales(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempZeroRatedSales(name);"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="outputTax" size="10" maxlength="16" style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalOutputTax(name);" onkeyup="calculateTotalOutputTax(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempOutputTax(name);"/>
				           </td>               	                   		           			            
					       <td width="115" height="1" class="control">
				              <nested:text property="tinOwnerSales" size="11" maxlength="9" style="font-size:8pt;" styleClass="textRequired"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="lastNameSales" size="10" maxlength="30" style="font-size:8pt;" styleClass="text"/>
				           </td>
		                   <td width="110" height="1" class="control">
				              <nested:text property="firstNameSales" size="10" maxlength="30" style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="middleNameSales" size="10" maxlength="30" style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="220" height="1" class="control">
				              <nested:text property="address1Sales" size="20" maxlength="30" style="font-size:8pt;" styleClass="textRequired"/>
				           </td>
				           <td width="220" height="1" class="control">
				              <nested:text property="address2Sales" size="20" maxlength="30" style="font-size:8pt;" styleClass="text"/>
				           </td>
				           
		                </tr>
		                </nested:iterate>					    
					    </table>
				        </div>
				     </td>
			       </tr> 				     				     			     				     
				 </table>
				 </div>
				 <div id="purchases">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
				      <tr>
	                     <td width="575" height="1" class="gridTitle" 
				             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                     <bean:message key="csvQuarterlyVatReturn.gridTitle.PRHSDetails"/>
		                 </td>
		              </tr>
		              <tr>
				          <td width="140" height="10">		     
					  	  </td>
				      </tr>
				 	  <tr valign="top">
			          <td width="140" height="1">
				      <div align="center">
				      <table border="0" cellpadding="0" cellspacing="0" width="577" height="47">
				      <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.exemptPurchases"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalExemptPurchases" size="16" maxlength="16" style="font-size:8pt;" styleClass="text" disabled="true"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.zeroRatedPurchases"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalZeroRatedPurchases" size="16" maxlength="16" style="font-size:8pt;"  styleClass="text" disabled="true"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.services"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalServices" size="16" maxlength="16" style="font-size:8pt;"  styleClass="text" disabled="true"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.capitalGoods"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalCapitalGoods" size="16" maxlength="16" style="font-size:8pt;"  styleClass="text" disabled="true"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.goodsOtherThanCp"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalGoodsOtherThanCp" size="16" maxlength="16" style="font-size:8pt;"  styleClass="text" disabled="true"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.inputTax"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalInputTax" size="16" maxlength="16" style="font-size:8pt;"  styleClass="text" disabled="true"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.creditable"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="creditable" size="16" maxlength="16" style="font-size:8pt;"  styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.nonCreditable"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="nonCreditable" size="16" maxlength="16" style="font-size:8pt;"  styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
			         </tr>
					 </table>
				     </div>
				     </td>
			       	 </tr>
			         <tr>
				          <td width="140" height="10">		     
					  	  </td>
				     </tr>  
				     <tr valign="top">
			          <td width="575" height="185">
				         <div align="center">
				         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
				             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
						     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					    <tr>
					       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.lineNumber"/>
					       </td>
					       <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.tinOfSupplier"/>
					       </td>
					       <td width="154" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.registeredNamePurchases"/>
					       </td>
		                   <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.taxableMonth"/>
					       </td>				       
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.exemptPurchases"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.zeroRatedPurchases"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.inputTax"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.services"/>
					       </td>				       					       					       
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.capitalGoods"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.goodsOtherThanCp"/>
					       </td>
		                   <td width="115" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.tinOwner"/>
					       </td>	
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.lastNamePurchases"/>
					       </td>
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.firstNamePurchases"/>
					       </td>					       					       					       
					       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.middleNamePurchases"/>
					       </td>
		                   <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.address1Purchases"/>
					       </td>
					       <td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.address2Purchases"/>
					       </td>	
		                </tr>	                        
					    <%
					       i = 0;	
					       ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       rowBgc = null;
					    %>
					    <nested:iterate property="csvPRList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
					       <td width="50" height="1" class="control">
				              <nested:text property="lineNumber" size="1" maxlength="3" style="font-size:8pt;" styleClass="text" disabled="true"/>
				           </td>
		                   <td width="115" height="1" class="control">
				              <nested:text property="tinOfSupplier" size="11" maxlength="9"  style="font-size:8pt;" styleClass="textRequired"/>
				           </td>
		                   <td width="154" height="1" class="control">
				              <nested:text property="registeredNamePurchases" size="15" maxlength="50"  style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="taxableMonthPurchases" size="10" maxlength="10"  style="font-size:8pt;" styleClass="textRequired"/>
				           </td> 
				           <td width="110" height="1" class="control">
				              <nested:text property="exemptPurchases" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalExemptPurchases(name);" onkeyup="calculateTotalExemptPurchases(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempExemptPurchases(name);"/>
				           </td>
					       <td width="110" height="1" class="control">
				              <nested:text property="zeroRatedPurchases" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalZeroRatedPurchases(name);" onkeyup="calculateTotalZeroRatedPurchases(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempZeroRatedPurchases(name);"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="inputTax" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalInputTax(name);" onkeyup="calculateTotalInputTax(name); formatAmount(name, (event)?event:window.event);" onfocus="initializeTempInputTax(name);"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="services" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalServices(name);" onkeyup="calculateTotalServices(name);" onfocus="initializeTempServices(name);"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="capitalGoods" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalCapitalGoods(name);" onkeyup="calculateTotalCapitalGoods(name);" onfocus="initializeTempCapitalGoods(name);"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="goodsOtherThanCp" size="10" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onblur="calculateTotalGoodsOtherThanCp(name);" onkeyup="calculateTotalGoodsOtherThanCp(name);" onfocus="initializeTempGoodsOtherThanCp(name);"/>
				           </td>
		                   <td width="115" height="1" class="control">
				              <nested:text property="tinOwnerPurchases" size="11" maxlength="9"  style="font-size:8pt;" styleClass="textRequired"/>
				           </td>
					       <td width="110" height="1" class="control">
				              <nested:text property="lastNamePurchases" size="10" maxlength="30"  style="font-size:8pt;" styleClass="text"/>
				           </td>
		                   <td width="110" height="1" class="control">
				              <nested:text property="firstNamePurchases" size="10" maxlength="30"  style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="110" height="1" class="control">
				              <nested:text property="middleNamePurchases" size="10" maxlength="30"  style="font-size:8pt;" styleClass="text"/>
				           </td> 
		                   <td width="220" height="1" class="control">
				              <nested:text property="address1Purchases" size="20" maxlength="30"  style="font-size:8pt;" styleClass="textRequired"/>
				           </td>           
				           <td width="220" height="1" class="control">
				              <nested:text property="address2Purchases" size="20" maxlength="30"  style="font-size:8pt;" styleClass="text"/>
				           </td>
		                </tr>                	
		                </nested:iterate>					    
					    </table>
				        </div>
				     </td>
			       </tr>      				     				     				     			     				     				     
				 </table>
				 </div>
				 <div id="importations">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="100">
				      <tr>
	                     <td width="575" height="1" class="gridTitle" 
				             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                     <bean:message key="csvQuarterlyVatReturn.gridTitle.IMPTSDetails"/>
		                 </td>
		              </tr>
		              <tr>
				          <td width="140" height="10">		     
					  	  </td>
				      </tr>  
					  <tr valign="top">
			          <td width="140" height="1">
				      <div align="center">
				      <table border="0" cellpadding="0" cellspacing="0" width="577" height="47">
				      <tr>	         	          
				         <td class="prompt" width="140" height="25" >
				            <bean:message key="csvQuarterlyVatReturn.prompt.dutiableValue"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalDutiableValue" size="16" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.otherCharges"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalOtherCharges" size="16" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.exemptImportations"/>
				         </td>
				         <td width="148" height="25" class="control">
				            <html:text property="totalExemptImportations" size="16" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.taxableImportations"/>
				         </td>
				         <td width="147" height="25" class="control">
				            <html:text property="totalTaxableImportations" size="16" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
			         </tr>
			         <tr>	         	          
				         <td class="prompt" width="140" height="25">
				            <bean:message key="csvQuarterlyVatReturn.prompt.vatPaid"/>
				         </td>
				         <td width="435" height="25" class="control" colspan="3">
				            <html:text property="totalVatPaid" size="16" maxlength="16"  style="font-size:8pt;" styleClass="textRequired" onkeyup="formatAmount(name, (event)?event:window.event);"/>
				         </td>
			         <tr>
					 </table>
				     </div>
				     </td>
			       	 </tr>
			         <tr>
				          <td width="140" height="10">		     
					  	  </td>
				     </tr>
				     <tr valign="top">
			          <td width="575" height="185">
				         <div align="center">
				         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
				             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
						     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					    <tr>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.importEntry"/>
					       </td>
					       <td width="298" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.assessmentReleaseDate"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.nameOfSeller"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.dateOfImportations"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.countryOfOrigin"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.dutiableValue"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.otherCharges"/>
					       </td>
					       <td width="298" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.exemptImportations"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.taxableImportations"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.vatPaid"/>
					       </td>					       					       					       
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.orNumber"/>
					       </td>
					       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          <bean:message key="csvQuarterlyVatReturn.prompt.dateOfVatPayment"/>
					       </td>                           
							<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				              <bean:message key="csvQuarterlyVatReturn.prompt.delete"/>
				       	   </td>
		                </tr>   
					    <%
					       i = 0;	
					       ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       rowBgc = null;
					    %>
					    <nested:iterate property="csvIMList">
					    <%
					       i++;
					       if((i % 2) != 0){
					           rowBgc = ROW_BGC1;
					       }else{
					           rowBgc = ROW_BGC2;
					       }  
					    %>
					    <tr bgcolor="<%= rowBgc %>">
		                   <td width="149" height="1" class="control">
				              <nested:text property="importEntry" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
		                   <td width="298" height="1" class="control">
				              <nested:text property="assessmentReleaseDate" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
					       <td width="149" height="1" class="control">
				              <nested:text property="nameOfSeller" size="13" maxlength="50"  style="font-size:8pt;" styleClass="text"/>
				           </td>
		                   <td width="149" height="1" class="control">
				              <nested:text property="dateOfImportation" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="149" height="1" class="control">
				              <nested:text property="countryOfOrigin" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="149" height="1" class="control">
				              <nested:text property="dutiableValue" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text" onblur="calculateTotalDutiableValue(name);" onkeyup="calculateTotalDutiableValue(name);" onfocus="initializeTempDutiableValue(name);"/>
				           </td>
		                   <td width="149" height="1" class="control">
				              <nested:text property="otherCharges" size="13" maxlength="10" style="font-size:8pt;"  styleClass="text" onblur="calculateTotalOtherCharges(name);" onkeyup="calculateTotalOtherCharges(name);" onfocus="initializeTempOtherCharges(name);"/>
				           </td>
		                   <td width="298" height="1" class="control">
				              <nested:text property="exemptImportation" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text" onblur="calculateTotalExemptImportation(name);" onkeyup="calculateTotalExemptImportation(name);" onfocus="initializeTempExemptImportation(name);"/>
				           </td>
					       <td width="149" height="1" class="control">
				              <nested:text property="taxableImportation" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text" onblur="calculateTotalTaxableImportation(name);" onkeyup="calculateTotalTaxableImportation(name);" onfocus="initializeTempTaxableImportation(name);"/>
				           </td>
		                   <td width="149" height="1" class="control">
				              <nested:text property="vatPaid" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text" onblur="calculateTotalVatPaid(name);" onkeyup="calculateTotalVatPaid(name);" onfocus="initializeTempVatPaid(name);"/>
				           </td>
				           <td width="149" height="1" class="control">
				              <nested:text property="orNumber" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
				           <td width="149" height="1" class="control">
				              <nested:text property="dateOfVatPayment" size="13" maxlength="10"  style="font-size:8pt;" styleClass="text"/>
				           </td>
						  <td width="70" align="center" height="1" class="control">
				              <nested:checkbox property="deleteCheckbox" />
				          </td>			 
		                </tr>
		                </nested:iterate>					    
					    </table>
				        </div>
				     </td>
			      </tr>
				  <tr>
		          <td width="550" height="25"> 
		           	<div id="buttons">
		           	<p align="right">		           
		           	<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       	</html:submit>			   			       
			       	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       	</html:submit>
		           	</div>
		           	<div id="buttonsDisabled" style="display: none;">
		           	<p align="right">
		               <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			              <bean:message key="button.addLines"/>
  			           </html:submit>
			           <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			               <bean:message key="button.deleteLines"/>
			           </html:submit>			   
		          	</div>
				  </td>
	     		  </tr>	      				     				     				     			     				     				     
				 </table>
				 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>         	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		  	  </td>
	     </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["dateFrom"] != null)
             document.forms[0].elements["dateFrom"].focus()
   // -->
</script>
<logic:equal name="glRepCsvQuarterlyVatReturnForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar");
  //-->
</script>
</logic:equal>
</body>
</html>
