<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<html>
<head>
<title>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<html:form action="/glRepForexRevaluationPrint.do" target="_top" onsubmit="return disableButtons();"> 
   <table border="0" cellpadding="0" cellspacing="0" width="1024" height="80" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
      <tr>
        <td width="1024" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
	     <bean:message key="forexRevaluationPrint.title"/>
		</td> 
      </tr>
      <tr>
        <td width="1024" height="22" colspan="4" class="statusBar">
	     <html:errors/>
		</td>
      </tr>
	  <tr>
        <td width="575" height="20" colspan="4">         
	    </td>
      </tr>
     <tr>
        <td width="1024" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		</td>
     </tr>
   </table>
</html:form>			
</body>
</html>
