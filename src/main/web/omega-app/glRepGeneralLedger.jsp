<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="generalLedger.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function clickShowZeroesTxn()
{
     
   if (document.forms[0].elements["showZeroesTxn"].checked == true) {
   
       document.forms[0].elements["showZeroesAll"].checked = false;
   
   }
         
   return true;
   
   
}

function clickShowZeroesAll()
{
   
   if (document.forms[0].elements["showZeroesAll"].checked == true) {
   
       document.forms[0].elements["showZeroesTxn"].checked = false;
   
   }
         
   return true;
   
   
}

//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRepGeneralLedger.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="generalLedger.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr><td>
				<div class="tabber">
			    <div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr> 
					    <td class="prompt" width="575" height="25" colspan="6">
						</td>
					</tr>
			        <tr>

	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.accountFrom"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="accountFrom" size="30" maxlength="255" styleClass="textRequired"/>
	            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountFrom','');"/>	            
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.accountTo"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="accountTo" size="30" maxlength="255" styleClass="textRequired"/>
	            <html:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookup('accountTo','');"/>	            
	         </td>
         </tr>
         <nested:iterate property="genVsList">
         <tr>
           <td width="140" height="25" class="prompt">
		     <nested:write property="valueSetName"/>
		   </td>
		   <td width="435" height="25" class="control" colspan="3">
		     <nested:text property="valueSetValueDescription" size="35" maxlength="255" styleClass="text"/>
		   </td>
         </tr>          
         </nested:iterate>
         
         
         <tr>   	            
                <td class="prompt" width="140" height="25">
                   <bean:message key="valueSetValue.prompt.qualifier"/>
                </td>
		
		        
		        <td width="127" height="25" class="control">
							<html:select property="qualifierSelectedList" size="7" multiple="true" styleClass="combo">
							   <html:options property="qualifierList"/>
							</html:select>	            
						  </td>
						  
						  
						  	        
             </tr> 
		<tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.journalSource"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="journalSource" styleClass="combo">
			       <html:options property="journalSourceList"/>
			    </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.currency"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="currency" styleClass="combo">
			       <html:options property="currencyList"/>			       
			    </html:select>
	         </td>
		</tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.date"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.amountType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="amountType" styleClass="comboRequired">
			       <html:options property="amountTypeList"/>
			    </html:select>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.showZeroesTxn"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="showZeroesTxn" onclick="return clickShowZeroesTxn();"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.showZeroesAll"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:checkbox property="showZeroesAll" onclick="return clickShowZeroesAll();"/>
	         </td>	         
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.includeUnpostedTransaction"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includeUnpostedTransaction"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.viewType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>	         
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.includeUnpostedSlTransaction"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includeUnpostedSlTransaction"/>
	         </td> 
	         
	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="generalLedger.prompt.orderBy"/>
	          </td>
	          <td width="128" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	          </td>   
	         
         </tr>
          <tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="generalLedger.prompt.reportType"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="reportType" styleClass="combo">
						<html:options property="reportTypeList"/>
					</html:select>
				</td>
				
				<td class="prompt" width="140" height="25">
		            <bean:message key="generalLedger.prompt.showEntries"/>
		         </td>
		         <td width="128" height="25" class="control">
		            <html:checkbox property="showEntries"/>
		         </td>
				
			
				
			</tr>
				</table>
				</div>
				
				<div class="tabbertab" title="CIT Return">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				
				<tr> 
					    <td class="prompt" width="575" height="25" colspan="2">
						</td>
					</tr>
					
				<tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailSectionNo"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxMailSectionNo" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailLotNo"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="corporateIncomeTaxMailLotNo" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		  
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailStreet"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxMailStreet" size="25" maxlength="25" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailPoBox"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:textarea property="corporateIncomeTaxMailPoBox" cols="20" rows="4" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailCountry"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxMailCountry" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailProvince"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="corporateIncomeTaxMailProvince" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailPostOffice"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxMailPostOffice" size="25" maxlength="25" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxMailCareOff"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="corporateIncomeTaxMailCareOff" size="25" maxlength="25" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 

		 
		 <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxTaxPeriodFrom"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxTaxPeriodFrom" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxTaxPeriodTo"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="corporateIncomeTaxTaxPeriodTo" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
		 
		 
		 
		  <tr>
		    <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxPublicOfficeName"/>
            </td>
		    <td width="148" height="25" class="control">
               <html:text property="corporateIncomeTaxPublicOfficeName" size="20" maxlength="20" styleClass="text"/>
		    </td>
	        <td class="prompt" width="120" height="25">
               <bean:message key="generalLedger.prompt.corporateIncomeTaxDateAppointment"/>
            </td>
		    <td width="147" height="25" class="control">
               <html:text property="corporateIncomeTaxDateAppointment" size="20" maxlength="20" styleClass="text"/>
		    </td>				    		    
		 </tr>
				</table>
				
		</div>
		<div class="tabbertab" title="Salary Wages">
			<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				<tr> 
					    <td class="prompt" width="575" height="25" colspan="2">
						</td>
					</tr>
						<tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailSectionNo"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesMailSectionNo" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailLotNo"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="salaryWagesMailLotNo" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 
				  
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailStreet"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesMailStreet" size="25" maxlength="25" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailPoBox"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="salaryWagesMailPoBox" cols="20" rows="4" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailCountry"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesMailCountry" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailProvince"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="salaryWagesMailProvince" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailPostOffice"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesMailPostOffice" size="25" maxlength="25" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesMailCareOff"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="salaryWagesMailCareOff" size="25" maxlength="25" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 
			
				 
				 
				 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesTaxPeriodFrom"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesTaxPeriodFrom" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesTaxPeriodTo"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="salaryWagesTaxPeriodTo" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 
				 
				  <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesPublicOfficeName"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesPublicOfficeName" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.salaryWagesDateAppointment"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="salaryWagesDateAppointment" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
				 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.field2"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesField2" size="20" maxlength="20" styleClass="text"/>
				    </td>
				    
				  </tr>
				 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.field4"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesField4" size="20" maxlength="20" styleClass="text"/>
				    </td>
				    
				  </tr>
				  
				  <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.field5"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesField5" size="20" maxlength="20" styleClass="text"/>
				    </td>
				    
				  </tr>
				  
				  <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.field6"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="salaryWagesField6" size="20" maxlength="20" styleClass="text"/>
				    </td>
				    
				  </tr>
				 
				 
						</table>

				</div>
				
		<div class="tabbertab" title="IIT Return">
			<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				<tr> 
					<td class="prompt" width="575" height="25" colspan="2">
					</td>
				</tr>
					
				<tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailSectionNo"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="individualIncomeTaxMailSectionNo" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailLotNo"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="individualIncomeTaxMailLotNo" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
		 
		 
		  
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailStreet"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="individualIncomeTaxMailStreet" size="25" maxlength="25" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailPoBox"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:textarea property="individualIncomeTaxMailPoBox" cols="20" rows="4" styleClass="text"/>
				    </td>				    		    
				 </tr>
		 
		 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailCountry"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="individualIncomeTaxMailCountry" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailProvince"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="individualIncomeTaxMailProvince" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
		 
		 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailPostOffice"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="individualIncomeTaxMailPostOffice" size="25" maxlength="25" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxMailCareOff"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="individualIncomeTaxMailCareOff" size="25" maxlength="25" styleClass="text"/>
				    </td>				    		    
				 </tr>
		 
		 

		 
				 <tr>
				    <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxTaxPeriodFrom"/>
		            </td>
				    <td width="148" height="25" class="control">
		               <html:text property="individualIncomeTaxTaxPeriodFrom" size="20" maxlength="20" styleClass="text"/>
				    </td>
			        <td class="prompt" width="120" height="25">
		               <bean:message key="generalLedger.prompt.individualIncomeTaxTaxPeriodTo"/>
		            </td>
				    <td width="147" height="25" class="control">
		               <html:text property="individualIncomeTaxTaxPeriodTo" size="20" maxlength="20" styleClass="text"/>
				    </td>				    		    
				 </tr>
		 
				
			</table>
				
		</div>
				
				
				
				
			    <div class="tabbertab" title="Branch Map">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        	<tr> 
						<td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
					<nested:iterate property="glRepBrGeneralLedgerList">
		            <tr>
			        	<td class="prompt" width="" height="25">
						<nested:write property="brBranchCode"/>
			            </td>
			            <td class="prompt" width="" height="25"> - 
			            </td>
			            <td class="prompt" width="140" height="25">
						<nested:write property="brName"/>
			            </td>
			            <td width="435" height="25" class="control" colspan="3">
						<nested:checkbox property="branchCheckbox"/>
			            </td>
		            </tr>
					</nested:iterate>
		        </table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
		 </td></tr>
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["accountFrom"] != null)
             document.forms[0].elements["accountFrom"].focus()
   // -->
</script>
<logic:equal name="glRepGeneralLedgerForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
