<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="incomeTaxReturn.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRepIncomeTaxReturn.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="300">
      <tr valign="top">
        <td width="187" height="280"></td>
        <td width="581" height="280">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="incomeTaxReturn.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="glRepIncomeTaxReturnForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
	               <bean:message key="app.success"/>
	           </logic:equal>
		       <html:errors/>
	        </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.forTheMonth"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="forTheMonth" size="10" maxlength="7" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.tinNumber"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="tinNumber" size="19" maxlength="25" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.taxpayersName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="taxpayersName" size="70" maxlength="75" styleClass="text"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.registeredAddress"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="registeredAddress" size="70" maxlength="75" styleClass="text"/>
	         </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.lineOfBusiness"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="lineOfBusiness" size="30" maxlength="50" styleClass="text"/>
	         </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.telephoneNumber"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="telephoneNumber" size="10" maxlength="7" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.rdoCode"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="rdoCode" size="3" maxlength="3" styleClass="textRequired"/>
	         </td>
         </tr>
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.zipCode"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="zipCode" size="10" maxlength="4" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="incomeTaxReturn.prompt.numberOfSheetsAttached"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="numberOfSheetsAttached" size="3" maxlength="2" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
               <bean:message key="incomeTaxReturn.prompt.relief"/>
            </td>
	         <td width="148" height="25" class="control">
	            <html:select property="relief" styleClass="combo">
			      <html:options property="reliefList"/>
			   </html:select>
	         </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="incomeTaxReturn.prompt.amended"/>
            </td>
	         <td width="147" height="25" class="control">
	            <html:select property="amended" styleClass="combo">
			      <html:options property="amendedList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
               <bean:message key="incomeTaxReturn.prompt.reliefDescription"/>
            </td>
	         <td width="148" height="25" class="control">
	            <html:text property="reliefDescription" size="25" maxlength="50" styleClass="text"/>
	         </td>
	         <td class="prompt" width="130" height="25">
	            <bean:message key="incomeTaxReturn.prompt.viewType"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>
         </tr>

         <tr>

				<td class="prompt" width="140" height="25">
				    <bean:message key="incomeTaxReturn.prompt.province"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="province" styleClass="combo">
						<html:options property="provinceList"/>
					</html:select>
				</td>

				<td class="prompt" width="140" height="25">
				    <bean:message key="incomeTaxReturn.prompt.reportType"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="reportType" styleClass="combo">
						<html:options property="reportTypeList"/>
					</html:select>
				</td>



			</tr>


	     <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Tax Computation">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="100">
		             <tr>
				        <td class="prompt" width="575" height="20" colspan="4">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.industryClassification"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="industryClassification" size="20" maxlength="50" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.atcCode"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="atcCode" size="3" maxlength="5" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.transitionalPresumptiveInputTax"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="transitionalPresumptiveInputTax" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.carriedOverReturn"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="carriedOverReturn" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.anyRefund"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="anyRefund" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.advancePayments"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="advancePayments" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.creditableVatW"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="creditableVatW" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.vatPaidInReturn"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="vatPaidInReturn" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.surcharge"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="surcharge" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.interest"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="interest" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.compromise"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="compromise" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				 </table>
				 </div>
				 <div class="tabbertab" title="Payment Details">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="100">
				     <tr>
				        <td class="prompt" width="575" height="20" colspan="4">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.checkBank"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkBank" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.otherBank"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="otherBank" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.checkNumber"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.otherNumber"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="otherNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.taxDebitNumber"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="taxDebitNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.checkDate"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="checkDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.taxDebitDate"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="taxDebitDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.otherDate"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="otherDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.cashBankAmount"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="cashBankAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.checkAmount"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.taxDebitAmount"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="taxDebitAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.otherAmount"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="otherAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				  <div class="tabbertab" title="Signatories">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="100">
				     <tr>
				        <td class="prompt" width="575" height="20" colspan="4">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.presidentSignature"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="presidentSignature" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.treasurerSignature"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="treasurerSignature" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.titleOfSignatory1"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="titleOfSignatory1" size="20" maxlength="25" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.titleOfSignatory2"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="titleOfSignatory2" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.tinOfTaxAgent"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="tinOfTaxAgent" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="incomeTaxReturn.prompt.taxAgentAccreditation"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="taxAgentAccreditation" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				  </table>
				  </div>


				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     <tr>
	        <td width="575" height="50" colspan="4">
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>

	     <tr valign="top">
            <td width="575" height="185" colspan="4">
                <div align="center">
                    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                        <tr>
                            <td width="575" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                                <bean:message key="incomeTaxReturn.gridTitle.ITRDetails"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="30" height="1" class="gridHeader" rowspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                <bean:message key="incomeTaxReturn.prompt.lineNumber"/>
                            </td>
                            <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                <bean:message key="incomeTaxReturn.prompt.parameter"/>
                            </td>

                            <td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" colspan="3"]>
                                <bean:message key="incomeTaxReturn.prompt.description"/>
                            </td>



                        </tr>
                        <tr>
                            <td width="100" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                <bean:message key="incomeTaxReturn.prompt.defaultValue"/>
                            </td>

                            <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>" colspan="2">
                                <bean:message key="incomeTaxReturn.prompt.coaAccount"/>
                            </td>

                            <td width="270" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                                <bean:message key="incomeTaxReturn.prompt.coaAccountDescription"/>
                            </td>


                        </tr>
                            <%
                               int i = 0;
                               String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                               String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                               String rowBgc = null;
                            %>
                            <nested:iterate property="glItrList">
                            <%
                               i++;
                               if((i % 2) != 0){
                                   rowBgc = ROW_BGC1;
                               }else{
                                   rowBgc = ROW_BGC2;
                               }
                            %>
                        <nested:hidden property="isCoaAccountEntered" value=""/>

                        <tr bgcolor="<%= rowBgc %>">

                            <td width="15" height="1" class="control" rowspan="2">
                                <nested:text property="lineNumber" size="3" maxlength="25" style="font-size:8pt;width:30px;" styleClass="textAmount" disabled="true"/>

                            </td>


                            <td width="100" height="1" class="control">

                               <nested:text property="rvParameter" size="31" maxlength="255" style="font-size:8pt;width:150px;" styleClass="text"/>


                            </td>

                            <td width="200" height="1" class="control" colspan="4">
                            	<nested:text property="rvDescription" size="100" maxlength="255" style="font-size:8pt;width:378px;" styleClass="text" />


                            </td>


                        </tr>
                        <tr bgcolor="<%= rowBgc %>">

                            <td width="100" height="1" class="control" colspan="1">
                              <nested:text property="rvDefaultValue" size="31" maxlength="255" style="font-size:8pt;width:150px;" styleClass="text"/>
                            </td>

                            <td width="50" height="1" class="control">
                                <nested:text property="accountNumber" size="25" maxlength="25" styleClass="text"/>
                            </td>

                             <td width="15" height="1" class="control">
                                 <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGrid(name,'accountNumber','accountNumberDescription');"/>
                            </td>

                            <td width="222" height="1" class="control">
                                <nested:text property="accountNumberDescription" size="37" maxlength="25" styleClass="text" disabled="true" />
                            </td>


                        </tr>
                        </nested:iterate>
                    </table>
                </div>
            </td>
        </tr>
         <tr>
         <div id="buttons">
             <td width="575" height="25" colspan="4">

                     <p align="right">

                       <html:submit property="addLinesButton" styleClass="mainButtonMedium">
                             <bean:message key="button.addLines"/>
                       </html:submit>





                       <html:submit property="saveDetailsButton" styleClass="mainButtonMedium">
                             <bean:message key="button.saveDetails"/>
                       </html:submit>

                 	</p>

             </td>
	</div>
         </tr>




	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["dateFrom"] != null)
             document.forms[0].elements["dateFrom"].focus()
   // -->
</script>
<logic:equal name="glRepIncomeTaxReturnForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
