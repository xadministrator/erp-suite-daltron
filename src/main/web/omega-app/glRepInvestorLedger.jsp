<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="investorLedger.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRepInvestorLedger.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="320">
      <tr valign="top">
        <td width="187" height="320"></td> 
        <td width="581" height="320">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="320" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="investorLedger.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	     <td>
				<div class="tabber">
			    <div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr> 
					    <td class="prompt" width="575" height="25" colspan="6">
						</td>
					</tr>
					
					
					<tr>
						 	<logic:equal name="glRepInvestorLedgerForm" property="useSupplierPulldown" value="true">
						 	
						 	<html:hidden property="isSupplierEntered" value=""/>
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="investorLedger.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
               					<html:select property="supplier" styleClass="combo" style="width:130;" onchange="return enterSelect('supplier','isSupplierEntered');">
                   					<html:options property="supplierList"/>
	               				</html:select>
								<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>
							<logic:equal name="glRepInvestorLedgerForm" property="useSupplierPulldown" value="false">
        	       			<td width="130" height="25" class="prompt">
            	   				<bean:message key="investorLedger.prompt.supplier"/>
               				</td>               				
							<td width="158" height="25" class="control">
									<html:text property="supplier" size="15" maxlength="25" styleClass="text" readonly="true"/>							
									<html:image property="lookupButton" src="images/lookup.gif" onclick="return showApSplLookup('supplier', '', 'isSupplierEntered');"/>
							</td>
							</logic:equal>        	       			
							
		 				</tr>
		 				
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="investorLedger.prompt.supplierName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="supplierName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>                
         				</tr>
		 				
			        
		         <tr>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="investorLedger.prompt.date"/>
			         </td>
			         <td width="127" height="25" class="control">
			            <html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
			         </td>
			         <td class="prompt" width="140" height="25">
			            <bean:message key="investorLedger.prompt.amountType"/>
			         </td>
			         <td width="128" height="25" class="control">
			            <html:select property="amountType" styleClass="comboRequired">
					       <html:options property="amountTypeList"/>
					    </html:select>
			         </td>
		         </tr>
        
	     <tr>
	     <td class="prompt" width="140" height="25">
	            <bean:message key="investorLedger.prompt.includeUnpostedSlTransaction"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:checkbox property="includeUnpostedSlTransaction"/>
	         </td> 
	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="investorLedger.prompt.viewType"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>	         
         </tr>
	     <tr>
	         
	         
	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="investorLedger.prompt.orderBy"/>
	          </td>
	          <td width="128" height="25" class="control">
	            <html:select property="orderBy" styleClass="combo">
			      <html:options property="orderByList"/>
			   </html:select>
	          </td>   
	         
         </tr>
		          <tr>
		
						<td class="prompt" width="140" height="25">
						    <bean:message key="investorLedger.prompt.reportType"/>
						</td>
						<td width="128" height="25" class="control">
						    <html:select property="reportType" styleClass="combo">
								<html:options property="reportTypeList"/>
							</html:select>
						</td>
					
					
						
					</tr>
				</table>
				</div>
				
				
				
				
				
			    <div class="tabbertab" title="Branch Map">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		        	<tr> 
						<td class="prompt" width="575" height="25" colspan="4">
			            </td>
			        </tr>
					<nested:iterate property="glRepBrInvestorLedgerList">
		            <tr>
			        	<td class="prompt" width="" height="25">
						<nested:write property="brBranchCode"/>
			            </td>
			            <td class="prompt" width="" height="25"> - 
			            </td>
			            <td class="prompt" width="140" height="25">
						<nested:write property="brName"/>
			            </td>
			            <td width="435" height="25" class="control" colspan="3">
						<nested:checkbox property="branchCheckbox"/>
			            </td>
		            </tr>
					</nested:iterate>
		        </table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
		 </td></tr>
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["accountFrom"] != null)
             document.forms[0].elements["accountFrom"].focus()
   // -->
</script>
<logic:equal name="glRepInvestorLedgerForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
