<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="quarterlyVatReturn.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/glRepQuarterlyVatReturn.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="250">
      <tr valign="top">
        <td width="187" height="250"></td>
        <td width="581" height="250">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="250"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="quarterlyVatReturn.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>
	        </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.dateFrom"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.dateTo"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateTo" size="10" maxlength="10" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.forTheMonth"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="forTheMonth" size="10" maxlength="7" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.tinNumber"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="tinNumber" size="19" maxlength="25" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.taxpayersName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="taxpayersName" size="70" maxlength="75" styleClass="text"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.registeredAddress"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="registeredAddress" size="70" maxlength="75" styleClass="text"/>
	         </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.lineOfBusiness"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="lineOfBusiness" size="30" maxlength="50" styleClass="text"/>
	         </td>
	     </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.telephoneNumber"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="telephoneNumber" size="10" maxlength="7" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.rdoCode"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="rdoCode" size="3" maxlength="3" styleClass="textRequired"/>
	         </td>
         </tr>
	      <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.zipCode"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="zipCode" size="10" maxlength="4" styleClass="textRequired"/>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.numberOfSheetsAttached"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="numberOfSheetsAttached" size="3" maxlength="2" styleClass="textRequired"/>
	         </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
               <bean:message key="quarterlyVatReturn.prompt.relief"/>
            </td>
	         <td width="148" height="25" class="control">
	            <html:select property="relief" styleClass="combo">
			      <html:options property="reliefList"/>
			   </html:select>
	         </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="quarterlyVatReturn.prompt.amended"/>
            </td>
	         <td width="147" height="25" class="control">
	            <html:select property="amended" styleClass="combo">
			      <html:options property="amendedList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
               <bean:message key="quarterlyVatReturn.prompt.accountingPeriod"/>
            </td>
	         <td width="148" height="25" class="control">
	            <html:select property="accountingPeriod" styleClass="combo">
			      <html:options property="accountingPeriodList"/>
			   </html:select>
	         </td>
	        <td class="prompt" width="140" height="25">
               <bean:message key="quarterlyVatReturn.prompt.quarter"/>
            </td>
	         <td width="147" height="25" class="control">
	            <html:select property="quarter" styleClass="combo">
			      <html:options property="quarterList"/>
			   </html:select>
	         </td>
         </tr>
         <tr>
	        <td class="prompt" width="140" height="25">
               <bean:message key="quarterlyVatReturn.prompt.reliefDescription"/>
            </td>
	         <td width="148" height="25" class="control">
	            <html:text property="reliefDescription" size="25" maxlength="50" styleClass="text"/>
	         </td>
	         <td class="prompt" width="130" height="25">
	            <bean:message key="quarterlyVatReturn.prompt.viewType"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>

         </tr>

         <tr>
         	<td class="prompt" width="140" height="25">
				    <bean:message key="quarterlyVatReturn.prompt.reportType"/>
				</td>
				<td width="128" height="25" class="control">
				    <html:select property="reportType" styleClass="combo">
						<html:options property="reportTypeList"/>
					</html:select>
				</td>
         </tr>

	     <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">
		        <div class="tabbertab" title="Tax Computation">
		        <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
		             <tr>
				        <td class="prompt" width="575" height="20" colspan="6">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.industryClassification"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="industryClassification" size="20" maxlength="50" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.atcCode"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="atcCode" size="3" maxlength="5" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.zeroRatedSalesReceipts"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="zeroRatedSalesReceipts" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.exemptSalesReceipts"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="exemptSalesReceipts" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.transitionalInputTax"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="transitionalInputTax" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.presumptiveInputTax"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="presumptiveInputTax" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
				     	<td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.inputTaxCarriedOver"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="inputTaxCarriedOver" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.servicesRenderedByNonResident"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="servicesRenderedByNonResident" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.importationsCapitalGoods"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="importationsCapitalGoods" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.importationsGoods"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="importationsGoods" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.purchasesNotQualified"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="purchasesNotQualified" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.vatRefund"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="vatRefund" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.excessInputTax"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="excessInputTax" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.excessPayments"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="excessPayments" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.monthlyVatPayments"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="monthlyVatPayments" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.creditableVatWithheld"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="creditableVatWithheld" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.advancePayments"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="advancePayments" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.vatPaidInReturn"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="vatPaidInReturn" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.surcharge"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="surcharge" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.interest"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="interest" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.compromise"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="compromise" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.dpGoods"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="dpGoods" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
					 </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.dpServices"/>
			            </td>
					    <td width="435" height="25" class="control" colspan="3">
			               <html:text property="dpServices" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				 </table>
				 </div>
				 <div class="tabbertab" title="Payment Details">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="20">
				     <tr>
				        <td class="prompt" width="575" height="20" colspan="4">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.checkBank"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkBank" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.otherBank"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="otherBank" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.checkNumber"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.otherNumber"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="otherNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.taxDebitNumber"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="taxDebitNumber" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.checkDate"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="checkDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.taxDebitDate"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="taxDebitDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.otherDate"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="otherDate" size="10" maxlength="10" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.cashBankAmount"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="cashBankAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.checkAmount"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="checkAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.taxDebitAmount"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="taxDebitAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.otherAmount"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="otherAmount" size="20" maxlength="25" styleClass="text" onkeyup="formatAmount(name, (event)?event:window.event);"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				 <div class="tabbertab" title="Signatories">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="100">
				     <tr>
				        <td class="prompt" width="575" height="20" colspan="4">
		                </td>
		             </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.presidentSignature"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="presidentSignature" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.treasurerSignature"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="treasurerSignature" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.titleOfSignatory1"/>
			            </td>
					    <td width="147" height="25" class="control">
			               <html:text property="titleOfSignatory1" size="20" maxlength="25" styleClass="text"/>
					    </td>
			            <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.titleOfSignatory2"/>
			            </td>
					    <td width="148" height="25" class="control">
			               <html:text property="titleOfSignatory2" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				     <tr>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.tinOfTaxAgent"/>
			            </td>
					    <td width="147" height="25" class="control">
					       <html:text property="tinOfTaxAgent" size="20" maxlength="25" styleClass="text"/>
					    </td>
					    <td class="prompt" width="140" height="25">
			               <bean:message key="quarterlyVatReturn.prompt.taxAgentAccreditation"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:text property="taxAgentAccreditation" size="20" maxlength="25" styleClass="text"/>
					    </td>
				     </tr>
				  </table>
				  </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
		  </tr>
	     <tr>
	        <td width="575" height="50" colspan="4">
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">

		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["dateFrom"] != null)
             document.forms[0].elements["dateFrom"].focus()
   // -->
</script>
<logic:equal name="glRepQuarterlyVatReturnForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","_blank","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
