<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="repStaticReport.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<style type="text/css">
a.linkMain:link {
color: #000000; 
text-decoration: none;
}

a.linkMain:visited {
color: #000000; 
text-decoration: none;
}

a.linkMain:hover {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:link {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:visited {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}

a.linkCurrent:hover {
color: #0000FF;
background: #FFFFE1;
text-decoration: underline;
}
</style>

<%@ include file="cmnSidebar.jsp" %>
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glRepStaticReport.do" onsubmit="return disableButtons();">
   <%@ include file="cmnHeader.jsp" %> 
   <table border="0" cellpadding="0" cellspacing="0" width="768" height="200">
      <tr valign="top">
         <td width="187" height="200"></td> 
         <td width="581" height="200">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="repStaticReport.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="25" colspan="4" class="statusBar">
				<logic:equal name="glRepStaticReportForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
        	    </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        <tr> 
	      	  <td width="575" height="110" colspan="4" class="link">
				<nested:iterate property="glRUSTRList">
					<li><nested:link page="/glRepStaticReport.do?view=1" paramId="fileName" paramProperty="fileName" styleClass="linkMain" target="_blank" ><nested:write property="staticReportName"/></nested:link></li>					  
				</nested:iterate>
			  </td>
			</tr>
	        <tr>          	            	       	      	        	                                   	         	        
	           <td width="575" height="25" colspan="2">
	           <div id="buttons">
	           <p align="right">
		       <html:submit property="closeButton" styleClass="mainButton">
		             <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.close"/>
		       </html:submit>
		       </div>
               </td>
	        </tr>		      	         
	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
           </table>
         </td>
      </tr>
   </table>
</html:form>
<script language=JavaScript type=text/javascript>

</script>
</body>
</html>
