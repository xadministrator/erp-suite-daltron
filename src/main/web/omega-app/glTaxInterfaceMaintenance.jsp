<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="taxInterfaceMaintenance.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function checkIsAr(name, field1, field2)
{   
	
	var property = name.substring(0,name.indexOf("."));    
	
	if (document.forms[0].elements[property + "." + field1].value != "") {
	
		if (document.forms[0].elements[property + "." + field2].value != "") {
		
			if (document.forms[0].elements[property + "." + field2].value == "true") {
				
				alert("a");
				
				document.forms[0].elements[property + "." + field1].value = false;
				document.forms[0].elements[property + "." + field2].value = false;
				
				alert(document.forms[0].elements[property + "." + field1].value);
				alert(document.forms[0].elements[property + "." + field2].value);
				
			} else if (document.forms[0].elements[property + "." + field2].value == "false") {
				
				alert("q");
				
				document.forms[0].elements[property + "." + field1].value = true;
				document.forms[0].elements[property + "." + field2].value = true;
				
				alert(document.forms[0].elements[property + "." + field1].value);
				alert(document.forms[0].elements[property + "." + field2].value);
				
			}
		
		} else {
		
			document.forms[0].elements[property + ".isArCheckboxEntered"].value = true;
			
		}
      	
    	disableButtons();
	    enableInputControls();
    	document.forms[0].submit();
    	
    }
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/glTaxInterfaceMaintenance.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="taxInterfaceMaintenance.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glTaxInterfaceMaintenanceForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>	
	        </td>
	     </tr>
	     
	     <logic:equal name="glTaxInterfaceMaintenanceForm" property="enableFields" value="true">
         <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.documentType"/>
                </td>
                <td width="445" height="25" class="control" colspan="3">
                   <html:select property="documentType" style="width:150;" styleClass="combo">
		               <html:options property="documentTypeList"/>
		           </html:select>
                </td>
         </tr>
         <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.docNumFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="docNumFrom" size="25" maxlength="25" styleClass="text"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.docNumTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="docNumFrom" size="25" maxlength="25" styleClass="text"/>  
                </td>
         </tr>
	     <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.refNumFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="refNumFrom" size="25" maxlength="25" styleClass="text"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.refNumTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="refNumFrom" size="25" maxlength="25" styleClass="text"/>  
                </td>
         </tr>
         <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.subledgerFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="subledgerFrom" size="25" maxlength="25" styleClass="text"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.subledgerTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="subledgerTo" size="25" maxlength="25" styleClass="text"/>  
                </td>
         </tr>           
		 <tr>
		     <td width="288" height="50" colspan="2">
			  <div id="buttons">
				<p align="left">
			  	<logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="false">
			    	<html:submit property="previousButton" styleClass="mainButtonSmall">
				        <bean:message key="button.previous"/>
				  	</html:submit>
			    </logic:equal>
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="true">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.previous"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="false">
					 <html:submit property="nextButton" styleClass="mainButtonSmall">
					 	<bean:message key="button.next"/>
					 </html:submit>
			    </logic:equal>
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="true">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
			   </div>
			   <div id="buttonsDisabled" style="display: none;">
				<p align="left">
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="false">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
						<bean:message key="button.previous"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="true">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.previous"/>
				     </html:submit>
			    </logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="false">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="true">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
			   </div>
			 </td>
	         <td width="287" height="50" colspan="2"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="goButton" styleClass="mainButton">
		          <bean:message key="button.go"/>
		       </html:submit>
               <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="goButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.go"/>
		       </html:submit>
	           <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
			  </td>
	     </tr>
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <div class="tabbertab" title="Tax">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					 <tr valign="top">
				          <td width="100" height="185">
					      <div align="center">
					        <table border="1" cellpadding="0" cellspacing="0" width="100" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
				            <tr>
							   <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           		   <bean:message key="taxInterfaceMaintenance.prompt.lineNumber"/>
				       		   </td>
				               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.documentType"/>
						       </td>
						       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.date"/>
						       </td> 						       
					           <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.documentNumber"/>
						       </td> 						    
                               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.referenceNumber"/>
						       </td> 						          
 							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.accountNumber"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.taxAmount"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.netAmount"/>
						       </td> 						       
							   <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.isAr"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.subledger"/>
						       </td> 						 
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.taxCode"/>
						       </td> 						
							   <td width="150" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.withholdingTaxCode"/>
						       </td> 
							   <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						       </td>
							</tr>	            				            
						    <%
						       int i = 0;	
						       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
						       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
						       String rowBgc = null;
						    %>
						    <nested:iterate property="glTIMList">	
							<%
						       i++;

						       if((i % 2) != 0){
						           rowBgc = ROW_BGC1;
						       }else{
						           rowBgc = ROW_BGC2;
						       }  
						    %>
							<nested:hidden property="isTaxCodeEntered" value=""/>
				    		<nested:hidden property="isWithholdingTaxCodeEntered" value=""/>	
							<nested:hidden property="isArCheckboxEntered"/>	
				    	    <tr bgcolor="<%= rowBgc %>">			
							   <td width="50" height="25" class="control">
				          		  <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       		   </td>		
						       <td width="150" height="25" class="control">
						          <nested:text property="documentType" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						          <nested:hidden property="taxInterfaceCode"/>
						       </td>
						       <td width="100" height="25" class="control">
						          <nested:text property="date" size="10" maxlength="10" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       <td width="150" height="25" class="control">
						          <nested:text property="documentNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="textRequired" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="referenceNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>	
							   <td width="150" height="25" class="control">
						          <nested:text property="accountNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
                               <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxAmount" size="12" maxlength="25" readonly="true" styleClass="textAmount" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxAmount" size="12" maxlength="25" readonly="true" styleClass="textAmount" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
						       <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="netAmount" size="12" maxlength="25" readonly="true" styleClass="textAmount" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="netAmount" size="12" maxlength="25" readonly="true" styleClass="textAmount" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="true">					       
						       <td width="50" align="center" height="25" class="control">
				                  <nested:checkbox property="isArCheckbox" onchange="return enterSelectGrid(name, 'isArCheckbox','isArCheckboxEntered');"/>
				               </td>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="50" align="center" height="25" class="control">
				                  <nested:checkbox property="isArCheckbox" disabled="true"/>
				               </td>
							   </nested:equal>
						       <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="subledgerCode" style="width:100;" styleClass="comboRequired">
		               				<nested:options property="customerList"/>
		           				  </nested:select>
						       </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="subledgerCode" style="width:100;" styleClass="comboRequired">
		               				<nested:options property="supplierList"/>
		           				  </nested:select>
						       </td>
						       </nested:equal>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="subledgerCode" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
						       <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="taxCode" style="width:120;" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'taxCode','isTaxCodeEntered');">
		               				<nested:options property="arTaxCodeList"/>
		           				  </nested:select>
	  						   </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="taxCode" style="width:120;" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'taxCode','isTaxCodeEntered');">
		               				<nested:options property="apTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxCode" size="15" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
                               <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="withholdingTaxCode" style="width:120;" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'withholdingTaxCode','isWithholdingTaxCodeEntered');">
		               				<nested:options property="arWithholdingTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="withholdingTaxCode" style="width:120;" styleClass="comboRequired" onchange="return enterSelectGrid(name, 'withholdingTaxCode','isWithholdingTaxCodeEntered');">
		               				<nested:options property="apWithholdingTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   </nested:equal>				
				               <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="withholdingTaxCode" size="15" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="true">					       
						       <td width="100" align="center" height="25">
						          <div id="buttons">
						          <nested:submit property="saveButton" styleClass="gridButton">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
			  	                  <div id="buttonsDisabled" style="display: none;">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
						       </td> 
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="100" align="center" height="25">
						          <div id="buttons">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
			  	                  <div id="buttonsDisabled" style="display: none;">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
						       </td>
							   </nested:equal>
							</tr>	
						  	</nested:iterate>
						  </table>
					      </div>
					   </td>
				     </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     </logic:equal>

	     <logic:equal name="glTaxInterfaceMaintenanceForm" property="enableFields" value="false">
		 <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.documentType"/>
                </td>
                <td width="445" height="25" class="control" colspan="3">
                   <html:select property="documentType" style="width:150;" styleClass="combo" disabled="true">
		               <html:options property="documentTypeList"/>
		           </html:select>
                </td>
         </tr>
         <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.docNumFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="docNumFrom" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.docNumTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="docNumFrom" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
         </tr>
	     <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.refNumFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="refNumFrom" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.refNumTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="refNumFrom" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
         </tr>
         <tr>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.subledgerFrom"/>
                </td>
                <td width="158" height="25" class="control">
                   <html:text property="subledgerFrom" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
                <td width="130" height="25" class="prompt">
                   <bean:message key="taxInterfaceMaintenance.prompt.subledgerTo"/>
                </td>
                <td width="157" height="25" class="control">
                   <html:text property="subledgerTo" size="25" maxlength="25" styleClass="text" disabled="true"/>  
                </td>
         </tr>           
		 <tr>
			 <td width="288" height="50" colspan="2">
			  <div id="buttons">
				<p align="left">
			  	<logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="false">
			    	<html:submit property="previousButton" styleClass="mainButtonSmall">
				        <bean:message key="button.previous"/>
				  	</html:submit>
			    </logic:equal>
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="true">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.previous"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="false">
					 <html:submit property="nextButton" styleClass="mainButtonSmall">
					 	<bean:message key="button.next"/>
					 </html:submit>
			    </logic:equal>
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="true">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
			   </div>
			   <div id="buttonsDisabled" style="display: none;">
				<p align="left">
			    <logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="false">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
						<bean:message key="button.previous"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disablePreviousButton" value="true">
			         <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.previous"/>
				     </html:submit>
			    </logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="false">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
				<logic:equal name="glTaxInterfaceMaintenanceForm" property="disableNextButton" value="true">
				     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
				         <bean:message key="button.next"/>
				     </html:submit>
				</logic:equal>
			   </div>
			 </td>
	         <td width="287" height="50" colspan="2"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="goButton" styleClass="mainButton">
		          <bean:message key="button.go"/>
		       </html:submit>
               <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="goButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.go"/>
		       </html:submit>
	           <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
	           </div>
			  </td>
	     </tr>
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
			        <div class="tabbertab" title="Tax">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					 <tr valign="top">
				          <td width="100" height="185">
					      <div align="center">
					        <table border="1" cellpadding="0" cellspacing="0" width="100" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">		
				            <tr>
							   <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           		   <bean:message key="taxInterfaceMaintenance.prompt.lineNumber"/>
				       		   </td>
				               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.documentType"/>
						       </td>
						       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.date"/>
						       </td> 						       
					           <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.documentNumber"/>
						       </td> 						    
                               <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.referenceNumber"/>
						       </td> 						          
 							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.accountNumber"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.taxAmount"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.netAmount"/>
						       </td> 						       
							   <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.isAr"/>
						       </td> 						       
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.subledger"/>
						       </td> 						 
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.taxCode"/>
						       </td> 						
							   <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						           <bean:message key="taxInterfaceMaintenance.prompt.withholdingTaxCode"/>
						       </td> 
							   <td width="100" height="1" class="gridHeader"  bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
						       </td>
							</tr>	            				            
						    <%
						       int i = 0;	
						       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
						       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
						       String rowBgc = null;
						    %>
						    <nested:iterate property="glTIMList">	
						    <%
						       i++;

						       if((i % 2) != 0){
						           rowBgc = ROW_BGC1;
						       }else{
						           rowBgc = ROW_BGC2;
						       }  
						    %>
							<nested:hidden property="isTaxCodeEntered" value=""/>
				    		<nested:hidden property="isWithholdingTaxCodeEntered" value=""/>	
				    	    <tr bgcolor="<%= rowBgc %>">		
							   <td width="50" height="25" class="control">
				          		  <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       		   </td>			
						       <td width="150" height="25" class="control">
						          <nested:text property="documentType" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						          <nested:hidden property="taxInterfaceCode"/>
						       </td>
						       <td width="100" height="25" class="control">
						          <nested:text property="date" size="10" maxlength="10" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       <td width="150" height="25" class="control">
						          <nested:text property="documentNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="referenceNumber" size="12" maxlength="25" styleClass="textRequired" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="referenceNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>	
							   <td width="150" height="25" class="control">
						          <nested:text property="accountNumber" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
                               <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxAmount" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxAmount" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
						       <nested:equal property="editGlDoc" value="true">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="netAmount" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="netAmount" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="true">					       
						       <td width="50" align="center" height="25" class="control">
				                  <nested:checkbox property="isArCheckbox" disabled="true"/>
				               </td>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="50" align="center" height="25" class="control">
				                  <nested:checkbox property="isArCheckbox" disabled="true"/>
				               </td>
							   </nested:equal>
						       <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="subledgerCode" style="width:100;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="customerList"/>
		           				  </nested:select>
						       </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="subledgerCode" style="width:100;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="supplierList"/>
		           				  </nested:select>
						       </td>
						       </nested:equal>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="subledgerCode" size="12" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="taxCode" style="width:120;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="arTaxCodeList"/>
		           				  </nested:select>
	  						   </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="taxCode" style="width:120;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="apTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="taxCode" size="15" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
                               <nested:equal property="editGlDoc" value="true">
					       	   <nested:equal property="isArCheckbox" value="true">
						       <td width="150" height="25" class="control">
						          <nested:select property="withholdingTaxCode" style="width:120;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="arWithholdingTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   <nested:equal property="isArCheckbox" value="false">
						       <td width="150" height="25" class="control">
						          <nested:select property="withholdingTaxCode" style="width:120;" styleClass="comboRequired" disabled="true">
		               				<nested:options property="apWithholdingTaxCodeList"/>
		           				  </nested:select>
                               </td>
						       </nested:equal>
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="150" height="25" class="control">
						          <nested:text property="withholdingTaxCode" size="15" maxlength="25" readonly="true" styleClass="text" style="font-size:8pt;" disabled="true"/>
						       </td>
						       </nested:equal>
							   <nested:equal property="editGlDoc" value="true">					       
						       <td width="100" align="center" height="25">
						          <div id="buttons">
						          <nested:submit property="saveButton" styleClass="gridButton">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
			  	                  <div id="buttonsDisabled" style="display: none;">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
						       </td> 
							   </nested:equal>
							   <nested:equal property="editGlDoc" value="false">					       
						       <td width="50" align="center" height="25">
				                  <div id="buttons">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
			  	                  <div id="buttonsDisabled" style="display: none;">
						          <nested:submit property="saveButton" styleClass="gridButton" disabled="true">
				                     <bean:message key="button.save"/>
			  	                  </nested:submit>
			  	                  </div>
				               </td>
							   </nested:equal>
							</tr>	
						  	</nested:iterate>
						  </table>
					      </div>
					   </td>
				     </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     </logic:equal>

	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		      </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["documentType"] != null &&
        document.forms[0].elements["documentType"].disabled == false)
        document.forms[0].elements["documentType"].focus()
	       // -->
</script>
</body>
</html>
