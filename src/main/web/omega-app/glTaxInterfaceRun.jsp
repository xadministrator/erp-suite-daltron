<%@ page language="java" import="com.struts.util.Constants,com.struts.gl.taxinterfacerun.*" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>

<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="taxInterfaceRun.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function confirmInterfaceRun()
{
	 if(confirm("Are you sure you want to run Tax Interface?")) return true;
	 else return false;

     return true;
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('generateButton'));">
<html:form action="/glTaxInterfaceRun.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="75">
      <tr valign="top">
        <td width="187" height="500"></td> 
        <td width="581" height="500">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="100" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	   	<tr>
	     	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
				<bean:message key="taxInterfaceRun.title"/>
		 	</td>
	   	</tr>
       	<tr>
	    	<td width="575" height="44" colspan="4" class="statusBar">
		   	<logic:equal name="glTaxInterfaceRunForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
            	<bean:message key="app.success"/>
           	</logic:equal>
		   	<html:errors/>
		   	<html:messages id="msg" message="true">
		    	<bean:write name="msg"/>		   
		   	</html:messages>	
	        </td>
	 	</tr>
	    
	   	<logic:equal name="glTaxInterfaceRunForm" property="enableFields" value="true">
       	<tr>
          	<td class="prompt" width="130" height="25">
	        	<bean:message key="taxInterfaceRun.prompt.documentType"/>
	       	</td>
	       	<td width="130" height="25" class="control" colspan="3">
				<html:select property="documentType" styleClass="combo" style="width:130;">
   	   			<html:options property="documentTypeList"/>
   				</html:select>
	       	</td>
      	</tr> 
       	<tr>
       		<td class="prompt" width="130" height="25">
        		<bean:message key="taxInterfaceRun.prompt.dateFrom"/>
       		</td>
			<td width="130" height="25" class="control">
				<html:text property="dateFrom" size="10" maxlength="10" styleClass="textRequired" />
			</td>
 			<td class="prompt" width="118" height="25">
	    		<bean:message key="taxInterfaceRun.prompt.dateTo"/>
	   		</td>
	   		<td width="200" height="25" class="control">
				<html:text property="dateTo" size="10" maxlength="10"  styleClass="textRequired"/>
			</td>
	 	</tr>
 		          
       	<tr>
			<td width="575" height="1" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
            	<logic:equal name="glTaxInterfaceRunForm" property="showRunButton" value="true">
               	<html:submit property="runButton" styleClass="mainButton" onclick="return confirmInterfaceRun();">
	              	<bean:message key="button.run"/>
		       	</html:submit>
		       	</logic:equal>
				<html:submit property="closeButton" styleClass="mainButton">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
	        <div id="buttonsDisabled" style="display: none;">
	       	<p align="right">
	           	<logic:equal name="glTaxInterfaceRunForm" property="showRunButton" value="true">
	           	<html:submit property="runButton" styleClass="mainButton" disabled="true">
	              	<bean:message key="button.run"/>
		       	</html:submit>
		       	</logic:equal>
		       	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
			</td>
	   	</tr>	     	     
	  	</logic:equal>
	    
	    <logic:equal name="glTaxInterfaceRunForm" property="enableFields" value="false">
 		<tr>
          	<td class="prompt" width="130" height="25">
	        	<bean:message key="taxInterfaceRun.prompt.documentType"/>
	       	</td>
	       	<td width="130" height="25" class="control" colspan="3">
				<html:select property="documentType" styleClass="combo" style="width:130;" disabled="true">
   	   			<html:options property="documentTypeList"/>
   				</html:select>
	       	</td>
      	</tr>
        <tr>
       		<td class="prompt" width="130" height="25">
        		<bean:message key="taxInterfaceRun.prompt.effectiveDateFrom"/>
       		</td>
			<td width="130" height="25" class="control">
				<html:text property="effectiveDateFrom" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
			</td>
 			<td class="prompt" width="118" height="25">
	    		<bean:message key="taxInterfaceRun.prompt.effectiveDateTo"/>
	   		</td>
	   		<td width="200" height="25" class="control">
				<html:text property="effectiveDateTo" size="10" maxlength="10"  styleClass="text" disabled="true"/>
			</td>
	 	</tr>

	   	<tr>
			<td width="575" height="1" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
            	<logic:equal name="glTaxInterfaceRunForm" property="showRunButton" value="true">
               	<html:submit property="runButton" styleClass="mainButton" onclick="return confirmInterfaceRun();">
	              	<bean:message key="button.run"/>
		       	</html:submit>
		       	</logic:equal>
				<html:submit property="closeButton" styleClass="mainButton">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
	        <div id="buttonsDisabled" style="display: none;">
	       	<p align="right">
	           	<logic:equal name="glTaxInterfaceRunForm" property="showRunButton" value="true">
	           	<html:submit property="runButton" styleClass="mainButton" disabled="true">
	              	<bean:message key="button.run"/>
		       	</html:submit>
		       	</logic:equal>
		       	<html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          	<bean:message key="button.close"/>
		       	</html:submit>
	       	</div>
			</td>
	   	</tr>
	  	</logic:equal>
	    
	   	<tr>
	    	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	   	</tr>
       	  
       	  </table>
		</td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["documentType"] != null &&
        document.forms[0].elements["documentType"].disabled == false)
        document.forms[0].elements["documentType"].focus()
	       // -->
</script>
</body>
</html>
