<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="transactionCalendarLines.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function confirmLoad() 
{ 
   if(confirm("Are you sure you want to load defaults?")) return true;
   else return false;
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('updateButton', 'goButton'));">
<html:form action="/glTransactionCalendarLines.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="transactionCalendarLines.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="glTransactionCalendarLinesForm" property="txnStatus" 
		       value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="180" height="25">
	          <bean:message key="transactionCalendarLines.prompt.transactionCalendar"/>
	       </td>
	       <td class="control" width="395" height="25" colspan="3">	          
		      <div id="buttons">
		      <html:select property="transactionCalendar" styleClass="comboRequired">
			     <html:options property="tcList"/>
			  </html:select>
	          <html:submit property="goButton" styleClass="mainButtonSmall">
                     <bean:message key="button.go"/>
                  </html:submit>
               </div>
               <div id="buttonsDisabled" style="display: none;">
               <html:select property="transactionCalendar" styleClass="comboRequired">
			     <html:options property="tcList"/>
			  </html:select>
	          <html:submit property="goButton" styleClass="mainButtonSmall" disabled="true">
                     <bean:message key="button.go"/>
                  </html:submit>
               </div>
	        </td>
              </tr>
	       <tr>
		  <td class="prompt" width="180" height="25">
                     <bean:message key="transactionCalendarLines.prompt.description"/>
                  </td>
		  <td class="label" width="395" height="25" colspan="3">
		     <bean:write name="glTransactionCalendarLinesForm" property="description"/>
		  </td>
	        </tr>
	       <tr>
	          <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		   <html:submit property="closeButton" styleClass="mainButton">
		      <bean:message key="button.close"/>
		   </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		      <bean:message key="button.close"/>
		   </html:submit>
		      </div>
                  </td>
	       </tr>
	       <td>
	       <logic:equal name="glTransactionCalendarLinesForm" property="userPermission" 
	          value="<%=Constants.FULL_ACCESS%>">
	       <tr>
                  <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
	       </td>
               </tr>
	       <tr>
	          <td width="575" height="5" colspan="4">
		  </td>
	       </tr>
	       <logic:equal name="glTransactionCalendarLinesForm" property="pageState"
	                                value="<%=Constants.PAGE_STATE_EDIT%>">
	       <tr>
	          <td class="prompt" width="180" height="25">
		     <bean:message key="transactionCalendarLines.prompt.date"/>
		  </td>
	          <td class="label" width="395" height="25" colspan="3">
		     <bean:write name="glTransactionCalendarLinesForm" property="date"/>
		  </td>
	       </tr>
	       <tr>
		  <td class="prompt" width="180" height="25">
		     <bean:message key="transactionCalendarLines.prompt.dayOfWeek"/>
		  </td>
		  <td class="label" width="395" height="25" colspan="3">
		     <bean:write name="glTransactionCalendarLinesForm" property="dayOfWeek"/>
		  </td>
	       </tr>
	       <tr>
	       <td class="prompt" width="180" height="25">
                  <bean:message key="transactionCalendarLines.prompt.businessDay"/>
               </td>
               <td class="control" width="395" height="25" colspan="3">
                  <html:checkbox property="businessDay"/>
               </td>
	       </tr>
	       </logic:equal>
	       </logic:equal>
               <tr>
	          <td width="160" height="50">
	             <div id="buttons">
	             <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     			     
			     </div>
			     <div id="buttonsDisabled" style="display: none;">
	             <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
	             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.showDetails"/>
			     </html:submit>
			     </logic:equal>			     
	             <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
	             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
			         <bean:message key="button.hideDetails"/>
			     </html:submit>
			     </logic:equal>			     
			     </div>
	          </td>	       	       	                     
                  <td width="415" height="50" colspan="3"> 
                  <div id="buttons">
                  <p align="right">	  
	          <logic:equal name="glTransactionCalendarLinesForm" property="userPermission"
                        value="<%=Constants.FULL_ACCESS%>">
                      <logic:equal name="glTransactionCalendarLinesForm" property="pageState"
                         value="<%=Constants.PAGE_STATE_EDIT%>">
                      <html:submit property="updateButton" styleClass="mainButton">
                         <bean:message key="button.update"/>
                      </html:submit>
                      <html:submit property="cancelButton" styleClass="mainButton">
                         <bean:message key="button.cancel"/>
                      </html:submit>
                      </logic:equal>
                   </logic:equal>
                   </div>
                   <div id="buttonsDisabled" style="display: none;">
                  <p align="right">	  
	              <logic:equal name="glTransactionCalendarLinesForm" property="userPermission"
                        value="<%=Constants.FULL_ACCESS%>">
                      <logic:equal name="glTransactionCalendarLinesForm" property="pageState"
                         value="<%=Constants.PAGE_STATE_EDIT%>">
                      <html:submit property="updateButton" styleClass="mainButton" disabled="true">
                         <bean:message key="button.update"/>
                      </html:submit>
                      <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
                         <bean:message key="button.cancel"/>
                      </html:submit>
                      </logic:equal>
                   </logic:equal>
                   </div>                   
		   </td>
		</tr>
	       <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="transactionCalendarLines.gridTitle.TCVDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="258" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="transactionCalendarLines.prompt.date"/>
			       </td>
			       <td width="258" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="transactionCalendarLines.prompt.dayOfWeek"/>
			       </td>			       
			       <td width="378" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="transactionCalendarLines.prompt.businessDay"/>
			       </td>
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glTCVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="258" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="258" height="1" class="gridLabel">
			          <nested:write property="dayOfWeek"/>
			       </td>
			       <td width="258" height="1" class="gridLabel">
			          <nested:write property="businessDay"/>
			       </td>
			       <td width="120" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glTransactionCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				      </div>
				      <div id="buttonsDisabled" style="display: none;">
			          <logic:equal name="glTransactionCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				      </div>
			       </td>
			    </tr>
			    </nested:iterate>
			    </logic:equal>
			    <logic:equal name="glTransactionCalendarLinesForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="glTCVList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="75" height="1" class="gridHeader">
			          <bean:message key="transactionCalendarLines.prompt.date"/>
			       </td>
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="date"/>
			       </td>
			       <td width="150" height="1" class="gridHeader">
			          <bean:message key="transactionCalendarLines.prompt.dayOfWeek"/>
			       </td>
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="dayOfWeek"/>
			       </td>
			       <td width="250"  height="1" class="gridHeader">
			          <bean:message key="transactionCalendarLines.prompt.businessDay"/>
			       </td>
			       <td width="70" height="1" class="gridLabel">
			          <nested:write property="businessDay"/>
			       </td>
			       <td width="120" align="center" height="1">
			          <div id="buttons">
			          <logic:equal name="glTransactionCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				      </div>
				      <div id="buttonsDisabled" style="display: none;">
			          <logic:equal name="glTransactionCalendarLinesForm" property="userPermission" 
				           value="<%=Constants.FULL_ACCESS%>">
			             <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                                <bean:message key="button.edit"/>
  	                             </nested:submit>
				  </logic:equal>
				      </div>
			       </td>
			    </tr>
			    </nested:iterate>			    
			    </logic:equal>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["transactionCalendar"] != null &&
         document.forms[0].elements["transactionCalendar"].disabled == false )
         document.forms[0].elements["transactionCalendar"].focus()
   // -->
</script>
</body>
</html>
