<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="yearEndClosing.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function confirmExecute() 
{ 
   if(confirm("Are you sure you want to execute Year-End Closing and set all periods to Closed?")) return true;
   else return false;
}
function confirmDepreciationExecute() 
{ 
   if(confirm("Are you sure you want to execute Fixed Asset Depreciation?")) return true;
   else return false;
}
function submitForm()
{            
      disableButtons();
      enableInputControls();
}


//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('executeClosingButton'));">
<html:form action="/glYearEndClosing.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="1" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="yearEndClosing.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="2" class="statusBar">
		   <logic:equal name="glYearEndClosingForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="yearEndClosing.prompt.accountingCalendar"/>
	       </td>
	       <td width="390" height="25" class="control">
	          <html:text property="accountingCalendar" size="50" maxlength="50" styleClass="text" disabled="true"/>
	       </td>
         </tr>
         <tr>
	       <td class="prompt" width="185" height="25">
	          <bean:message key="yearEndClosing.prompt.description"/>
	       </td>
	       <td width="390" height="25" class="control">
	          <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
	       </td>
         </tr>
         <tr>
         	 <td class="prompt" width="140" height="25">
	            <bean:message key="yearEndClosing.prompt.period"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="period" styleClass="comboRequired">
			      <html:options property="periodList"/>
			   </html:select>
	         </td>
         </tr>
	     <tr>
	          <td width="575" height="80" colspan="2"> 
	          <div id="buttons">
	          <p align="right">
		   <logic:equal name="glYearEndClosingForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		   <logic:notEmpty name="glYearEndClosingForm" property="accountingCalendar">
		   <html:submit property="executeClosingButton" styleClass="mainButtonBig" onclick="return confirmExecute();">
		      <bean:message key="button.executeClosing"/>
		   </html:submit>
		   </logic:notEmpty>
		   </logic:equal>
		   <logic:equal name="glYearEndClosingForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		   <logic:notEmpty name="glYearEndClosingForm" property="accountingCalendar">
		   <html:submit property="executeFADepreciationExecuteButton" styleClass="mainButtonBig" onclick="return confirmDepreciationExecute();">
		      <bean:message key="button.executeFADepreciationExecuteButton"/>
		   </html:submit>
		   </logic:notEmpty>
		   </logic:equal>
		   <html:submit property="closeButton" styleClass="mainButton">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
		   <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		   <logic:equal name="glYearEndClosingForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		   <logic:notEmpty name="glYearEndClosingForm" property="accountingCalendar">
		   <html:submit property="executeClosingButton" styleClass="mainButtonBig" disabled="true">
		      <bean:message key="button.executeClosing"/>
		   </html:submit>
		   <html:submit property="executeFADepreciationExecuteButton" styleClass="mainButtonBig" disabled="true">
		      <bean:message key="button.executeFADepreciationExecuteButton"/>
		   </html:submit>
		   </logic:notEmpty>
		   </logic:equal>
		   <logic:equal name="glYearEndClosingForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		   <logic:notEmpty name="glYearEndClosingForm" property="accountingCalendar">
		   <html:submit property="executeFADepreciationExecuteButton" styleClass="mainButtonBig" disabled="true">
		      <bean:message key="button.executeFADepreciationExecuteButton"/>
		   </html:submit>
		   </logic:notEmpty>
		   </logic:equal>
		   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		      <bean:message key="button.close"/>
		   </html:submit>
		   </div>
                  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="2" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
</body>
</html>
