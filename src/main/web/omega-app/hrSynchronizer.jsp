<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="hrSynchronizer.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/hrSynchronizer.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="700">
      <tr valign="top">
        <td width="187" height="250"></td> 
        <td width="581" height="250">
         <table border="0" cellpadding="0" cellspacing="0" width="585" height="250" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="hrSynchronizer.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="hrSynchronizerForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     
	     
	     
	        
	     <tr>
			 <td width="575" height="50" colspan="4">
			 <div id="buttons">
			 <p align="right">
		     <logic:equal name="hrSynchronizerForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="synchronizeButton" styleClass="mainButton">
			       <bean:message key="button.synchronize"/>
			  	</html:submit>
			  </logic:equal>

			    <html:submit property="closeButton" styleClass="mainButton">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>
			 <div id="buttonsDisabled" style="display: none;">
			 <p align="right">
			 <logic:equal name="hrSynchronizerForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			    <html:submit property="synchronizeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.synchronize"/>
			    </html:submit>
			 </logic:equal>

			    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
			       <bean:message key="button.close"/>
			    </html:submit>
			 </div>		         
			 </td>
		 </tr>
	     <tr>
	         <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>

</script>
</body>
</html>
