<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="assemblyItemEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function submitForm()
{
      
      disableButtons();      
      enableInputControls();    
            
}

function calculateTotalUnitCost(precisionUnit)
{

      var property = "invIEList";
      
      var totalUnitCost = 0;     
      var laborCost = 0;
      var powerCost = 0;
      var overHeadCost = 0;
      var freightCost = 0;
      var fixedCost = 0;
      
      var i = 0;
      
      while (true) {
      
          if (document.forms[0].elements["invIEList[" + i + "].lineNumber"] == null) {
          
              break;
          
          }
          
          var quantityNeeded = 0;
          var unitCost = 0;
          var rawCost = 0;
          var specificGravity = 0;
          var standardFillSize = 0;
          
        
          if (!isNaN(parseFloat(document.forms[0].elements["invIEList[" + i + "].quantityNeeded"].value))) {
      
	      	quantityNeeded = (document.forms[0].elements["invIEList[" + i + "].quantityNeeded"].value).replace(/,/g,'');
	      	
	      }
          if (!isNaN(parseFloat(document.forms[0].elements["invIEList[" + i + "].unitCost"].value))) {
      
	      	unitCost = (document.forms[0].elements["invIEList[" + i + "].unitCost"].value).replace(/,/g,'');
	      	
	      }
          if (!isNaN(parseFloat(document.forms[0].elements["specificGravity"].value))) {
        	  
        	  specificGravity = (document.forms[0].elements["specificGravity"].value).replace(/,/g,'') ;
        	  
    	  }
          
		if (!isNaN(parseFloat(document.forms[0].elements["standardFillSize"].value))) {
        	  
			standardFillSize = (document.forms[0].elements["standardFillSize"].value).replace(/,/g,'') ;
        	  
    	  }
          
          if (!isNaN(parseFloat(document.forms[0].elements["invIEList[" + i + "].rawCost"].value))) {
              
        	  if(document.forms[0].elements["invIEList[" + i + "].category"].value.indexOf("RAWMAT") >= 0){
        		  
        		  rawCost =(unitCost * (quantityNeeded/100) * (standardFillSize/1000) * specificGravity).toFixed(parseInt(precisionUnit.value))
              	
        	  } else {
        		  
        		  rawCost =(unitCost * quantityNeeded).toFixed(parseInt(precisionUnit.value))
                	
        		  
        	  }
        	 
  	      	
  	      }
        
          document.forms[0].elements["invIEList[" + i + "].rawCost"].value = rawCost
         
	      
		  if (document.forms[0].elements["invIEList[" + i + "].quantityNeeded"] != null &&
		      document.forms[0].elements["invIEList[" + i + "].unitCost"] != null) {	      	      
	      
	      	//totalUnitCost = Math.round((totalUnitCost + Math.round((quantityNeeded * unitCost)*100)/100)*100)/100;   
			  totalUnitCost =  totalUnitCost + parseFloat(rawCost); 
				
          }	      	
	                 
          i++;
      
      }
      
      
      if (!isNaN(parseFloat(document.forms[0].elements["laborCost"].value))) {
    	  laborCost = (document.forms[0].elements["laborCost"].value).replace(/,/g,'') * 1;
	  }
      if (!isNaN(parseFloat(document.forms[0].elements["powerCost"].value))) {
    	  powerCost = (document.forms[0].elements["powerCost"].value).replace(/,/g,'') * 1;
	  }
      if (!isNaN(parseFloat(document.forms[0].elements["overHeadCost"].value))) {
    	  overHeadCost = (document.forms[0].elements["overHeadCost"].value).replace(/,/g,'') * 1;
	  }
      if (!isNaN(parseFloat(document.forms[0].elements["freightCost"].value))) {
    	  freightCost = (document.forms[0].elements["freightCost"].value).replace(/,/g,'') * 1;
	  }
      if (!isNaN(parseFloat(document.forms[0].elements["fixedCost"].value))) {
    	  fixedCost = (document.forms[0].elements["fixedCost"].value).replace(/,/g,'') * 1;
	  }
      
     // document.forms[0].elements["unitCost"].value = totalUnitCost.toFixed(2);
       document.forms[0].elements["unitCost"].value = (totalUnitCost  + laborCost + powerCost + overHeadCost + freightCost + fixedCost).toFixed(parseInt(precisionUnit.value));
      recalculatePrice("unitCost", precisionUnit);
       
}  

function recalculatePrice(name , precisionUnit) {

	  var unitCost = 0;
	  var averageCost = 0;
	  var shippingCost = 0;
	  var percentMarkup = 0;
	  var salesPrice = 0;
	 
	  if (!isNaN(parseFloat(document.forms[0].elements["unitCost"].value))) {
	      unitCost = (document.forms[0].elements["unitCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["averageCost"].value))) {
	      averageCost = (document.forms[0].elements["averageCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["shippingCost"].value))) {
	      shippingCost = (document.forms[0].elements["shippingCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["percentMarkup"].value))) {
	      percentMarkup = (document.forms[0].elements["percentMarkup"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["salesPrice"].value))) {
	      salesPrice = (document.forms[0].elements["salesPrice"].value).replace(/,/g,'') * 1;
	  }
	  
	  averageCost = unitCost;
	  
  	  if (name == "percentMarkup") {
  	    	     
  	  	 salesPrice = ((averageCost + shippingCost) * percentMarkup / 100 + averageCost + shippingCost).toFixed(parseInt(precisionUnit.value));

		 
  	  	 document.forms[0].elements["salesPrice"].value = salesPrice;		 
		 
	  } else if (name == "salesPrice" || name == "unitCost"  || name == "shippingCost") {
	  
		  document.forms[0].elements["percentMarkup"].value = ((salesPrice - averageCost - shippingCost) / (averageCost + shippingCost) * 100).toFixed(parseInt(precisionUnit.value));

	  }
  	document.forms[0].elements["grossProfit"].value = (salesPrice - (averageCost + shippingCost)).toFixed(parseInt(precisionUnit.value));
	 
}


//Done Hiding--> 
</script>

<link href="css/tabcontrol.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/tabber.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invAssemblyItemEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width=920 height="420">
      <tr valign="top">
      	<td width="187" height="420"></td> 
       	<td width="581" height="420">
       	<table border="0" cellpadding="0" cellspacing="0" width="585" height="420" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	  	 <tr>
	    	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="assemblyItemEntry.title"/>
		    </td>
		 </tr>
       	 <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="invAssemblyItemEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		    <html:errors/>	
	        </td>
	   	 </tr>

				<html:hidden property="isUnitMeasureEntered" value=""/>
				 
				 <html:hidden property="precisionUnit"/>
				 
				<logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				<tr>
				<td>
				<div class="tabber">				
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="45">  
						 <tr> 
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>						
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.itemName"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="itemName" size="43" maxlength="100" styleClass="textRequired"/>
							</td>
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.description"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="description" size="43" maxlength="100" styleClass="text"/>
							</td>		    
						 </tr>         
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.category"/>
							</td>
							<td width="148" height="25" class="control" colspan="3">
							   <html:select property="category" styleClass="comboRequired">
								  <html:options property="categoryList"/>
							   </html:select>
							</td>	
						 </tr>
						 <tr>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.partNumber"/>
							</td>
							<td width="147" height="25" class="control" colspan="3">
							   <html:text property="partNumber" size="20" maxlength="25" styleClass="text"/>
							</td>
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitMeasure"/>
							</td>
						
							<td width="148" height="25" class="control">
							   <html:select property="unitMeasure" styleClass="comboRequired" onchange="return enterSelect('unitMeasure','isUnitMeasureEntered');">
								  <html:options property="unitMeasureList"/>		          
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
							</td>	    
						 </tr>		
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.costMethod"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:select property="costMethod" styleClass="comboRequired">
								  <html:options property="costMethodList"/>
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.averageCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	        
						 </tr>
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.percentMarkup"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name,precisionUnit)"/>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.shippingCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name,precisionUnit)"/>
							</td>	        
						 </tr>
						 <tr>	
							<td class="prompt" width="140" height="25">
				               <bean:message key="assemblyItemEntry.prompt.supplier"/>
				            </td>
						    <td width="148" height="25" class="control" >
						       <html:select property="supplier" styleClass="combo">
						          <html:options property="supplierList"/>		          
						       </html:select>
						    </td>
							<td class="prompt" width="200" height="25">
								   <bean:message key="assemblyItemEntry.prompt.salesPrice"/>
								</td>		    		    		    		  
					        <td width="147" height="25" class="control">
								   <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name,precisionUnit)"/>
							</td>	
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.enable"/>
							</td>		    		    		    		  
							<td width="148" height="25" class="control">
							   <html:checkbox property="enable"/>
							</td>
						   <td class="prompt" width="200" height="25">
					           <bean:message key="assemblyItemEntry.prompt.grossProfit"/>
				           </td>		    		    		    		  
					        <td width="147" height="25" class="control">
					           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
				           </td>	        		       		  	     
					 </tr>	
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.enableAutoBuild"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:checkbox property="enableAutoBuild"/>
						</td>	
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.enablePo"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:checkbox property="enablePo"/>
			           </td>        		       		  	     
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.specificGravity"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="specificGravity" size="20" maxlength="25" styleClass="textAmount" disabled="false"/>
						</td>
						 <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.standardFillSize"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="standardFillSize" size="20" maxlength="25" styleClass="textAmount" disabled="false"/>
						</td>	
						       		       		  	     
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.yield"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="yield" size="20" maxlength="25" styleClass="textAmount" disabled="false"/>
						</td>	
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.lossPercentage"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="lossPercentage" size="20" maxlength="25" styleClass="textAmount" disabled="false"/>
			           </td>        		       		  	     
					 </tr>	
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.rawmat"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="rawmat" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>	
					 		       		  	     
					 </tr>
					 
					 <tr>
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.laborCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="laborCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>


<tr>
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.powerCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="powerCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>


<tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.overHeadCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="overHeadCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>	


 <tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.freightCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="freightCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>


 <tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.fixedCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="fixedCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>


					 
					 
					 
				</table>
				</div>
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="assemblyItemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="retailUom" styleClass="combo">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge"/>
					  	</td>	        		       		  	     
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.dineInCharge"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="dineInCharge"/>
					  	</td>	        		       		  	     
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.openProduct"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct"/>
						</td>
						
					</tr>
					<tr>
					<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>
					</tr>
					
						
					<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.laborCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="laborCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('laborCostAccount','laborCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.laborCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="laborCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         				 
					 
         			
         			<tr>
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.powerCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="powerCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('powerCostAccount','powerCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.powerCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="powerCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         			 
					 
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.overHeadCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="overHeadCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('overHeadCostAccount','overHeadCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.overHeadCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="overHeadCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         			 
					
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.freightCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="freightCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('freightCostAccount','freightCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.freightCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="freightCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
					 
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.fixedCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('fixedCostAccount','fixedCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.fixedCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
				</table>
				</div>
				<div class="tabbertab" title="Schedule">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSunday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSunday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scMonday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scMonday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scTuesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scTuesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scWednesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scWednesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scThursday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scThursday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scFriday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scFriday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSaturday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSaturday"/>
            			</td>
					</tr>
				</table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			 
			</td>
		</tr>
	     </logic:equal>	     	     			     	          
	     
		 <%-- query --%>
		       
		 <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">	     
		
		<tr>
			<td>			    
				<div class="tabber">

				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
						 <tr> 
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>						
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.itemName"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="itemName" size="43" maxlength="50" styleClass="textRequired" disabled="true"/>
							</td>
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.description"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="description" size="43" maxlength="50" styleClass="text" disabled="true"/>
							</td>		    
						 </tr>         
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.category"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:select property="category" styleClass="comboRequired" disabled="true">
								  <html:options property="categoryList"/>
							   </html:select>
							</td>	
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.partNumber"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="partNumber" size="20" maxlength="25" styleClass="text" disabled="true"/>
							</td>		    
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitMeasure"/>
							</td>
						
							<td width="148" height="25" class="control">
							   <html:select property="unitMeasure" styleClass="comboRequired" disabled="true">
								  <html:options property="unitMeasureList"/>		          
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
							</td>	    
						 </tr>		
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.costMethod"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:select property="costMethod" styleClass="comboRequired" disabled="true">
								  <html:options property="costMethodList"/>
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.averageCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	        
						 </tr>
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.percentMarkup"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.shippingCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	        
						 </tr>
						 <tr>	
							<td class="prompt" width="140" height="25">
			               <bean:message key="assemblyItemEntry.prompt.supplier"/>
			            </td>
					    <td width="148" height="25" class="control" >
					       <html:select property="supplier" styleClass="combo" disabled="true">
					          <html:options property="supplierList"/>		          
					       </html:select>
					    </td>
						<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.salesPrice"/>
							</td>		    		    		    		  
				        <td width="147" height="25" class="control">
							   <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.enable"/>
							</td>		    		    		    		  
							<td width="148" height="25" class="control">
							   <html:checkbox property="enable" disabled="true"/>
							</td>
						   <td class="prompt" width="200" height="25">
					           <bean:message key="assemblyItemEntry.prompt.grossProfit"/>
				           </td>		    		    		    		  
					        <td width="147" height="25" class="control">
					           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
				           </td>	        		       		  	     
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.enableAutoBuild"/>
							</td>		    		    		    		  
							<td width="148" height="25" class="control">
							   <html:checkbox property="enableAutoBuild" disabled="true"/>
							</td>
							<td class="prompt" width="200" height="25">
					           <bean:message key="assemblyItemEntry.prompt.enablePo"/>
				           </td>		    		    		    		  
					        <td width="147" height="25" class="control">
					           <html:checkbox property="enablePo" disabled="true"/>
				           </td>
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.yield"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="yield" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>	
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.lossPercentage"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="lossPercentage" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>        		       		  	     
					 </tr>
					  <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.rawmat"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="rawmat" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>	
					 		       		  	     
					 </tr>
					 
					 <tr>
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.laborCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="laborCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>	


	 
					 <tr>
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.powerCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="powerCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>

 
					 <tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.overHeadCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="overHeadCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>	

 
					 <tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.freightCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="freightCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>

					<tr>

						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.fixedCost"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="fixedCost" size="20" maxlength="25" styleClass="textAmount" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           </td> 
					 </tr>
					 			
				</table>
				</div>
			    <div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo" disabled="true">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text" disabled="true"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="assemblyItemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo" disabled="true">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control" colspan="3">
						<html:select property="retailUom" styleClass="combo" disabled="true">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo" disabled="true">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge" disabled="true"/>
					  	</td>	        		       		  	     
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.dineInCharge"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="dineInCharge" disabled="true"/>
					  	</td>	        		       		  	     
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.openProduct"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct" disabled="true"/>
						</td>
						
					</tr>
					
					
					<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.laborCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="laborCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('laborCostAccount','laborCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.laborCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="laborCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         			
         			
         			<tr>
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.powerCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="powerCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('powerCostAccount','powerCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.powerCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="powerCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         			
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.overHeadCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="overHeadCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('overHeadCostAccount','overHeadCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.overHeadCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="overHeadCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			
         			
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.freightCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="freightCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('freightCostAccount','freightCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.freightCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="freightCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
         			
         			
         			 
					 
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.fixedCostAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedCostAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('fixedCostAccount','fixedCostAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="assemblyItemEntry.prompt.fixedCostAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedCostAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>
					
					
					
					
					
					<tr>
					<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo" disabled="true">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>
					</tr>
				</table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		</tr>
	    </logic:equal>	     
		
		
		<logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">	     
				<tr>
				<td>
				<div class="tabber">
				
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
						 <tr> 
							<td class="prompt" width="575" height="25" colspan="4">
							</td>
						 </tr>						
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.itemName"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="itemName" size="43" maxlength="50" styleClass="textRequired" disabled="true"/>
							</td>
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.description"/>
							</td>
							<td width="435" height="25" class="control" colspan="3">
							   <html:text property="description" size="43" maxlength="50" styleClass="text" disabled="true"/>
							</td>		    
						 </tr>         
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.category"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:select property="category" styleClass="comboRequired" disabled="true">
								  <html:options property="categoryList"/>
							   </html:select>
							</td>	
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.partNumber"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="partNumber" size="20" maxlength="25" styleClass="text" disabled="true"/>
							</td>		    
						 </tr>
						 <tr>
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitMeasure"/>
							</td>
						
							<td width="148" height="25" class="control">
							   <html:select property="unitMeasure" styleClass="comboRequired" disabled="true">
								  <html:options property="unitMeasureList"/>		          
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.unitCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
							</td>	    
						 </tr>		
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.costMethod"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:select property="costMethod" styleClass="comboRequired" disabled="true">
								  <html:options property="costMethodList"/>
							   </html:select>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.averageCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	        
						 </tr>
						 <tr> 
							<td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.percentMarkup"/>
							</td>
							<td width="148" height="25" class="control">
							   <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>
							<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.shippingCost"/>
							</td>
							<td width="147" height="25" class="control">
							   <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	        
						 </tr>
						 <tr>	
							<td class="prompt" width="140" height="25">
			               <bean:message key="assemblyItemEntry.prompt.supplier"/>
			            </td>
					    <td width="148" height="25" class="control" >
					       <html:select property="supplier" styleClass="combo" disabled="true">
					          <html:options property="supplierList"/>		          
					       </html:select>
					    </td>
						<td class="prompt" width="200" height="25">
							   <bean:message key="assemblyItemEntry.prompt.salesPrice"/>
							</td>		    		    		    		  
				        <td width="147" height="25" class="control">
							   <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
							</td>	
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.enable"/>
							</td>		    		    		    		  
							<td width="148" height="25" class="control">
							   <html:checkbox property="enable" disabled="true"/>
							</td>
						   <td class="prompt" width="200" height="25">
					           <bean:message key="assemblyItemEntry.prompt.grossProfit"/>
				           </td>		    		    		    		  
					        <td width="147" height="25" class="control">
					           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
				           </td>	        		       		  	     
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
							   <bean:message key="assemblyItemEntry.prompt.enableAutoBuild"/>
							</td>		    		    		    		  
							<td width="148" height="25" class="control">
							   <html:checkbox property="enableAutoBuild" disabled="true"/>
							</td>
							<td class="prompt" width="200" height="25">
					           <bean:message key="assemblyItemEntry.prompt.enablePo"/>
				           </td>		    		    		    		  
					        <td width="147" height="25" class="control">
					           <html:checkbox property="enablePo" disabled="true"/>
				           </td>
					 </tr>
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.yield"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="yield" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>	
						<td class="prompt" width="200" height="25">
				           <bean:message key="assemblyItemEntry.prompt.lossPercentage"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="lossPercentage" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>        		       		  	     
					 </tr>
					 
					 <tr>	
				        <td class="prompt" width="140" height="25">
						   <bean:message key="assemblyItemEntry.prompt.rawmat"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control">
						   <html:text property="rawmat" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
						</td>	
					 		       		  	     
					 </tr>			
				</table>
				</div>
			    <div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo" disabled="true">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text" disabled="true"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="assemblyItemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo" disabled="true">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control" colspan="3">
						<html:select property="retailUom" styleClass="combo" disabled="true">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo" disabled="true">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge" disabled="true"/>
					  	</td>	        		       		  	     
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.dineInCharge"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="dineInCharge" disabled="true"/>
					  	</td>	        		       		  	     
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.openProduct"/>
						</td>		    		    		    		  
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct" disabled="true"/>
						</td>
						
					</tr>
					<tr>
					<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo" disabled="true">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>
					</tr>
				</table>
				</div>
				
				<div class="tabbertab" title="Schedule">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSunday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSunday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scMonday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scMonday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scTuesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scTuesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scWednesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scWednesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scThursday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scThursday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scFriday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scFriday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSaturday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSaturday"/>
            			</td>
					</tr>
				</table>
				</div>
				
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		</tr>
	     </logic:equal>	
		 
		 
		 <%-- buttons --%>
  		 <tr>
	        <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">				
		       <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
			   <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium">
		          <bean:message key="button.conversion"/>
		       </html:submit>
		       <html:submit property="recalcButton" styleClass="mainButtonMedium" onclick="recalculatePrice('shippingCost'); return false;">
		          <bean:message key="button.recalc"/>
		       </html:submit>
			   <logic:equal name="invAssemblyItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">				
		       <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
			   <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;" disabled="true">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.conversion"/>
		       </html:submit>
		       <html:submit property="recalcButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.recalc"/>
		       </html:submit>
         	   <logic:equal name="invAssemblyItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
            </td>
		 </tr>	     	     
	     
		 <%-- table lines --%>	
		         
    	 <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		 <tr valign="top">
		 	<td width="575" height="185" colspan="4">
				<div align="center">
			    	<table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
			             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
					     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				   	  <tr>
		              	<td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                  <bean:message key="assemblyItemEntry.gridTitle.BOMDetails"/>
		                </td>
		              </tr>		            
				      <tr>
		              	<td width="25" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.lineNumber"/>
				       	</td>				    
					   	<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.itemName"/>
				       	</td>
						<td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.location"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.quantityNeeded"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unit"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.category"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.rawCost"/>
				       	</td>	       
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unitCost"/>
				       	</td>			
						<td width="45" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.delete"/>
				       	</td>				       			       			    				       
		              </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="7" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.description"/>
				       	</td>			          				        			       		    
				      </tr>
					  <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				      %>
				      <nested:iterate property="invIEList">
				      <%
				       i++;
				       if((i % 2) != 0){
				          rowBgc = ROW_BGC1;
				       }else{
				          rowBgc = ROW_BGC2;
				       }  
				      %>
				      <nested:hidden property="isItemNameEntered" value=""/>
					  <nested:hidden property="isLocationEntered" value=""/>
  				      <nested:hidden property="isUnitEntered" value=""/>
  				      	
				      <tr bgcolor="<%= rowBgc %>">
				      	<td width="25" height="1" class="control">
				          <nested:text property="lineNumber" size="3" maxlength="5" styleClass="text" disabled="true"/>
				       	</td>				    
				       	<td width="190" height="1" class="gridLabel">
			              <nested:text property="itemName" size="20" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookup(name, 'itemName', '', 'isItemNameEntered');"/>
				       	</td>
			           	<td width="90" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:85;">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				        </td>								   
				        <td width="70" height="1" class="control">
			              <nested:text property="quantityNeeded" size="8" maxlength="25" styleClass="textRequired" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           	</td>			           	
			       	
			           	<td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name,'unit', 'isUnitEntered');" >
                            <nested:options property="unitList"/>                                               
                          </nested:select>
                       	</td>	
                       	
                       	 <td width="70" height="1" class="control">
			              <nested:text property="category" size="10" maxlength="25" styleClass="text" disabled="true"/>
			           	</td>
                       				 
						<td width="70" height="1" class="control">
			              <nested:text property="rawCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>           
				       	<td width="70" height="1" class="control">
			              <nested:text property="unitCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>	
						<td width="45" align="center" height="1" class="control">
				          <p align="center">
				          <nested:checkbox property="deleteCheckbox"/>
				       	</td>			       			           
				      </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <nested:text property="description" size="104" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>			          				        			       		    
				      </tr>
					  </nested:iterate>     	          			      			      			             
					</table>
			 	</div>			    	    			    
		     </td>
		 </tr>
		 <tr>
	         <td width="575" height="25" colspan="4"> 
	         	<div id="buttons">
	           	<p align="right">
	           	<logic:equal name="invAssemblyItemEntryForm" property="showAddLinesButton" value="true">
	           		<html:submit property="addLinesButton" styleClass="mainButtonMedium">
		           		<bean:message key="button.addLines"/>
		       		</html:submit>			   
		       	</logic:equal>
		       	<logic:equal name="invAssemblyItemEntryForm" property="showDeleteLinesButton" value="true">
		       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
		            	<bean:message key="button.deleteLines"/>
		       		</html:submit>
		       	</logic:equal>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
	           	<logic:equal name="invAssemblyItemEntryForm" property="showAddLinesButton" value="true">
	           		<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.addLines"/>
		       		</html:submit>
		       	</logic:equal>
		       	<logic:equal name="invAssemblyItemEntryForm" property="showDeleteLinesButton" value="true">
		       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.deleteLines"/>
		       		</html:submit>			   
		       	</logic:equal>
	           	</div>
			</td>
		 </tr>	     
		 </logic:equal>	     	     
	       
		<logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
		 <tr valign="top">
		 	<td width="575" height="185" colspan="4">
				<div align="center">
			    	<table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
			             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
					     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				   	  <tr>
		              	<td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                  <bean:message key="assemblyItemEntry.gridTitle.BOMDetails"/>
		                </td>
		              </tr>		            
				      <tr>
		              	<td width="25" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.lineNumber"/>
				       	</td>				    
					   	<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.itemName"/>
				       	</td>
						<td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.location"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.quantityNeeded"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unit"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.category"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.rawCost"/>
				       	</td>	       
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unitCost"/>
				       	</td>			
						<td width="45" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.delete"/>
				       	</td>				       			       			    				       
		              </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="7" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.description"/>
				       	</td>			          				        			       		    
				      </tr>
					  <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				      %>
				      <nested:iterate property="invIEList">
				      <%
				       i++;
				       if((i % 2) != 0){
				          rowBgc = ROW_BGC1;
				       }else{
				          rowBgc = ROW_BGC2;
				       }  
				      %>
				      <nested:hidden property="isItemNameEntered" value=""/>
					  <nested:hidden property="isLocationEntered" value=""/>
  				      <nested:hidden property="isUnitEntered" value=""/>	
  				      
				      <tr bgcolor="<%= rowBgc %>">
				      	<td width="25" height="1" class="control">
				          <nested:text property="lineNumber" size="3" maxlength="5" styleClass="text" disabled="true"/>
				       	</td>				    
				       	<td width="190" height="1" class="gridLabel">
			              <nested:text property="itemName" size="20" maxlength="25" styleClass="textRequired"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookup(name, 'itemName', '', 'isItemNameEntered');"/>
				       	</td>
			           	<td width="90" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:85;">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				        </td>								   
				        <td width="70" height="1" class="control">
			              <nested:text property="quantityNeeded" size="8" maxlength="25" styleClass="textRequired" onblur="calculateTotalUnitCost(precisionUnit);" onkeyup="calculateTotalUnitCost(precisionUnit);"/>
			           	</td>			           	
			       	
			           	<td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name,'unit', 'isUnitEntered');" >
                            <nested:options property="unitList"/>                                               
                          </nested:select>
                       	</td>
                       	<td width="70" height="1" class="control">
			              <nested:text property="category" size="10" maxlength="25" styleClass="text" disabled="true"/>
			           	</td>				 
						 
						 <td width="70" height="1" class="control">
			              <nested:text property="rawCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>
			           	          
				       	<td width="70" height="1" class="control">
			              <nested:text property="unitCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>	
						<td width="45" align="center" height="1" class="control">
				          <p align="center">
				          <nested:checkbox property="deleteCheckbox"/>
				       	</td>			       			           
				      </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <nested:text property="description" size="104" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>			          				        			       		    
				      </tr>
					  </nested:iterate>     	          			      			      			             
					</table>
			 	</div>			    	    			    
		     </td>
		 </tr>
		 <tr>
	         <td width="575" height="25" colspan="4"> 
	         	<div id="buttons">
	           	<p align="right">
	           	<logic:equal name="invAssemblyItemEntryForm" property="showAddLinesButton" value="true">
	           		<html:submit property="addLinesButton" styleClass="mainButtonMedium">
		           		<bean:message key="button.addLines"/>
		       		</html:submit>			   
		       	</logic:equal>
		       	<logic:equal name="invAssemblyItemEntryForm" property="showDeleteLinesButton" value="true">
		       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
		            	<bean:message key="button.deleteLines"/>
		       		</html:submit>
		       	</logic:equal>
	           	</div>
	           	<div id="buttonsDisabled" style="display: none;">
	           	<p align="right">
	           	<logic:equal name="invAssemblyItemEntryForm" property="showAddLinesButton" value="true">
	           		<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.addLines"/>
		       		</html:submit>
		       	</logic:equal>
		       	<logic:equal name="invAssemblyItemEntryForm" property="showDeleteLinesButton" value="true">
		       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
		            	<bean:message key="button.deleteLines"/>
		       		</html:submit>			   
		       	</logic:equal>
	           	</div>
			</td>
		 </tr>	     
		 </logic:equal>
		 
       	 <%-- table lines --%>
       	      
	     <logic:equal name="invAssemblyItemEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
		 <tr valign="top">
		 	<td width="575" height="185" colspan="4">
				<div align="center">
			    	<table border="1" cellpadding="0" cellspacing="0" width="577" height="47" 
			             bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
					     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				   	  <tr>
		              	<td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                  <bean:message key="assemblyItemEntry.gridTitle.BOMDetails"/>
		                </td>
		              </tr>		            
				      <tr>
		              	<td width="25" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.lineNumber"/>
				       	</td>				    
					   	<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.itemName"/>
				       	</td>
						<td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.location"/>
				       	</td>
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.quantityNeeded"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unit"/>
				       	</td>
				       	<td width="85" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.category"/>
				       	</td>	
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.rawCost"/>
				       	</td>	       
				       	<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.unitCost"/>
				       	</td>			
						<td width="45" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.delete"/>
				       	</td>				       			       			    				       
		              </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="7" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <bean:message key="assemblyItemEntry.prompt.description"/>
				       	</td>			          				        			       		    
				      </tr>
					  <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				      %>
				      <nested:iterate property="invIEList">
				      <%
				       i++;
				       if((i % 2) != 0){
				          rowBgc = ROW_BGC1;
				       }else{
				          rowBgc = ROW_BGC2;
				       }  
				      %>
				      <tr bgcolor="<%= rowBgc %>">
				      	<td width="25" height="1" class="control">
				          <nested:text property="lineNumber" size="3" maxlength="5" styleClass="text" disabled="true"/>
				       	</td>				    
				       	<td width="190" height="1" class="gridLabel">
			              <nested:text property="itemName" size="20" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       	</td>
			           	<td width="90" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:85;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				        </td>								   
				        <td width="70" height="1" class="control">
			              <nested:text property="quantityNeeded" size="8" maxlength="25" styleClass="textRequired" disabled="true"/>
			           	</td>			           	
			       	
			           	<td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                            <nested:options property="unitList"/>                                               
                          </nested:select>
                       	</td>	
                       	<td width="70" height="1" class="control">
			              <nested:text property="category" size="10" maxlength="25" styleClass="text" disabled="true"/>
			           	</td>			 
						 <td width="70" height="1" class="control">
			              <nested:text property="rawCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>          
				       	<td width="70" height="1" class="control">
			              <nested:text property="unitCost" size="8" maxlength="25" styleClass="textAmount" disabled="true"/>
			           	</td>	
						<td width="45" align="center" height="1" class="control">
				          <p align="center">
				          <nested:checkbox property="deleteCheckbox"/>
				       	</td>			       			           
				      </tr>
					  <tr>
		               	<td width="35" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
				       	<td width="540" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				          <nested:text property="description" size="104" maxlength="100" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       	</td>			          				        			       		    
				      </tr>
					  </nested:iterate>     	          			      			      			             
					</table>
				</div>			    	    			    
			</td>
		 </tr>	     	     
         </logic:equal>	     
	     
	     <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">	        
		    </td>
	     </tr>
        </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["itemName"] != null &&
        document.forms[0].elements["itemName"].disabled == false)
        document.forms[0].elements["itemName"].focus()
        
      calculateTotalUnitCost(precisionUnit);
	       // -->
</script>
</body>
</html>
