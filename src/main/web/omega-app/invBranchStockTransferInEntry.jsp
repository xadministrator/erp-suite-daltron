<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>

<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>

<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invBranchStockTransferInEntry.title"/>
</title>

<link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript">
	<!-- Hide from non-JavaScript Browsers
	function submitForm()
	{
	      disableButtons();
	      enableInputControls();
	}

	function calculateAmount(name)
	{

	  var property = name.substring(0,name.indexOf("."));

	  var quantity = 0;
	  var unitPrice = 0;
	  var amount = 0;

		if (document.forms[0].elements[property + ".quantityReceived"].value != "" &&
	      document.forms[0].elements[property + ".unitCost"].value != "") {

		  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantityReceived"].value))) {

			quantityReceived = (document.forms[0].elements[property + ".quantityReceived"].value).replace(/,/g,'');

		  }

		  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {

			unitPrice = (document.forms[0].elements[property + "."].value).replace(/,/g,'');

		  }

		  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {

			amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');

		  }

		  amount = (quantityReceived * unitPrice).toFixed(2);

		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());

	 }

	}

	var currProperty;
	var currName;
	function fnOpenMisc(name){

		   var property = name.substring(0,name.indexOf("."));
		   currProperty = property;
		   currName = name;

		   var quantity = Math.abs(document.forms[0].elements[property + ".quantityReceived"].value.replace(/,/g,'') * 1);

		   var miscStr = document.forms[0].elements[property + ".misc"].value;

		   var specs = new Array(); //= document.forms[0].elements["apRILList[" + 0 + "].tagList[" + 1 + "].specs"].value;
		   var custodian = new Array(document.forms[0].elements["userList"].value);
		   var custodian2 = new Array();
		   var propertyCode = new Array();
		   var serialNumber = new Array();
		   var expiryDate = new Array();
		   var tgDocNum = new Array();
		   var arrayMisc = miscStr.split("_");

			//cut miscStr and save values
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("@");
			propertyCode = arrayMisc[0].split(",");

			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("<");
			serialNumber = arrayMisc[0].split(",");

			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split(">");
			specs = arrayMisc[0].split(",");

			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split(";");
			custodian2 = arrayMisc[0].split(",");
			//expiryDate = arrayMisc[1].split(",");
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("%");
			expiryDate = arrayMisc[0].split(",");
			tgDocNum = arrayMisc[1].split(",");


		   //check at least one disabled field
		   var isDisabled = document.forms[0].elements[property + ".quantityReceived"].disabled;
		   var userList = document.forms[0].elements["userList"].value;

		   var index = 0;
		/*	 //alert("before while")
		   while (true) {
			  //alert(document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value);
			  if (document.forms[0].elements[property + ".tagList[" + index + "].specs"].value!= null ||
				  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value!= null ||
				  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value!= null ||
				  document.forms[0].elements[property + ".tagList[" + index + "].propertyCode"].value!=null) {
					 // alert("d2?");
			 	  specs[index] = document.forms[0].elements[property + ".tagList[" + index + "].specs"].value;

			 	  expiryDate[index] = document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value;
			 	  serialNumber[index] = document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value;
				  propertyCode[index] = document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value;
				  custodian2[index] = document.forms[0].elements[property + ".tagList[" + index + "].custodian"].value;
					  //alert("inside if");
			  }else{
				  //alert("o d2?");
				  document.forms[0].elements[property + ".tagList[" + index + "].specs"].value= "";
				  document.forms[0].elements[property + ".tagList[" + index + "].expiryDate"].value="";
				  document.forms[0].elements[property + ".tagList[" + index + "].serialNumber"].value="";
				  document.forms[0].elements[property +".tagList[" + index + "].propertyCode"].value="";
				  document.forms[0].elements[property +".tagList[" + index + "].custodian"].value="";
				  //alert("inside else")
				  break;
			  }
			  //alert("after while")
			  if (++index == quantity) break;
		   }*/

		   //alert(document.forms[0].elements["apRILList[" + index + "].tagList[" + index2 + "].propertyCode"].value);
		   //var specsStr = document.forms[0].elements[property + ".specs"].value;

		   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+/*"&miscStr="+miscStr+*/"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
				   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2
				   + "&serialNumber=" + serialNumber +"&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");

		   return false;
		}
	//Done Hiding-->
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
<script Language="JavaScript" type="text/javascript">
	<!-- Hide from non-JavaScript Browsers



	function enterTransferOutNumber()
  	{

    	disableButtons();
    	enableInputControls();
		document.forms[0].elements["isTransferOutNumberEntered"].value = true;
		document.forms[0].submit();

  	}


	//Done Hiding-->
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invBranchStockTransferInEntry.do" onsubmit="return submitForm();" method="post" enctype="multipart/form-data">
	<%@ include file="cmnHeader.jsp" %>
	<%@ include file="cmnSidebar.jsp" %>

	<table border="0" cellpadding="0" cellspacing="0" width="768" height="510">

      		<tr valign="top">

        		<td width="187" height="510"></td>



			<!-- ===================================================================== -->
			<!--  Title Bar                                                            -->
			<!-- ===================================================================== -->

        		<td width="581" height="510">

          			<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">

				<tr>
					<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
					<bean:message key="invBranchStockTransferInEntry.title"/>
					</td>
				</tr>


					<!-- ===================================================================== -->
					<!--  Status Msg                                                           -->
					<!-- ===================================================================== -->
				<tr>
					<td width="575" height="44" colspan="4" class="statusBar">

					<logic:equal name="invBranchStockTransferInEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
					<bean:message key="app.success"/>
					</logic:equal>

					<html:errors/>

					<html:messages id="msg" message="true">
					<bean:write name="msg"/>
					</html:messages>

					</td>
				</tr>

	     		 <html:hidden property="isTransferOutNumberEntered" value=""/>
				 <html:hidden property="isTypeEntered" value=""/>

<!-- ===================================================================== -->
<!--  Screen when enabled                                                  -->
<!-- ===================================================================== -->

				<html:hidden property="userList" />
				<logic:equal name="invBranchStockTransferInEntryForm" property="enableFields" value="true">

					<!-- TAB TABLE ROW -->
					<tr>
						<td width="575" height="10" colspan="4">
						<div class="tabber">

							<!-- Header Tab -->
							<div class="tabbertab" title="Header">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

								<tr>
								<td class="prompt" height="25" colspan="4"/>
								</tr>

								<html:hidden property="transferOutCode" value=""/>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.type"/>
								</td>
								<td width="158" height="25" class="control">
        	           			<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type', 'isTypeEntered');"> <!-- -->
            	       			<html:options property="typeList"/>
	            	   			</html:select>
                				</td>

								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.date"/>
								</td>
								<td width="158" height="25" class="control">
								<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
								</td>

								</tr>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transferOutNumber"/>
								</td>


								<td width="158" height="25" class="control">
								<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="true">
								<html:text property="transferOutNumber" size="18" maxlength="50" styleClass="textrequired" onblur="return enterTransferOutNumber();" />
								<html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvBSTLookup('transferOutCode','transferOutNumber','isTransferOutNumberEntered');"/>
								</logic:equal>
								<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="false">
								<html:text property="transferOutNumber" size="18" maxlength="50" styleClass="textrequired"/>
								</logic:equal>
								</td>

								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transferInNumber"/>
								</td>
								<td width="157" height="25" class="control">
								<html:text property="transferInNumber" size="18" maxlength="50" styleClass="text"/>
								</td>
								</tr>
								<tr>

								<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="true">
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.branchFrom"/>
								</td>
								<td width="158" height="25" class="control">
								<html:text property="branchFrom" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transitLocation"/>
								</td>
								<td width="157" height="25" class="control">
								<html:text property="transitLocation" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								</logic:equal>

								<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="false">
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.branchFrom"/>
								</td>
								<td width="90" height="1" class="control">
								<nested:select property="branchFrom" styleClass="comboRequired" style="width:130;">
								<nested:options property="branchFromList"/>
								</nested:select>
								</td>
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transitLocation"/>
								</td>
								<td width="90" height="1" class="control">
								<nested:select property="transitLocation" styleClass="comboRequired" style="width:130;">
								<nested:options property="locationList"/>
								</nested:select>
								</td>
								</logic:equal>

								</tr>



								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.description"/>
								</td>
								<td width="158" height="25" class="control" colspan="3">
								<html:text property="description" size="25" maxlength="200" styleClass="text"/>
								</td>
								</tr>

							</table>
							</div>


							<!-- Status Tab -->
							<div class="tabbertab" title="Status">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

								<tr>
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvalStatus"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.posted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.reasonForRejection"/>
								</td>
								<td width="415" height="25" class="control" colspan="3">
								<html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
								</td>
								</tr>

							</table>
							</div>



							<!-- Log Tab -->
							<div class="tabbertab" title="Log">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

								<tr>
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>
								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.createdBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateCreated"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>
								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.lastModifiedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateLastModified"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>
								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvedRejectedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateApprovedRejected"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>
								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.postedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.datePosted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

							</table>
							</div>
							<div class="tabbertab" title="Attachment">
			   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
								     <tr>
								        <td class="prompt" width="575" height="25" colspan="4">
						                </td>
						             </tr>
						             <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename1"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename1" size="35" styleClass="text"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton1" value="true">
								           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton1" value="true">
						                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
								     <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename2"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename2" size="35" styleClass="text"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton2" value="true">
								           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton2" value="true">
						                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename3"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename3" size="35" styleClass="text"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton3" value="true">
								           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton3" value="true">
						                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename4"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename4" size="35" styleClass="text"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton4" value="true">
								           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton4" value="true">
						                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
								</table>
							</div>
							</div><script>tabberAutomatic(tabberOptions)</script>
						</td>

					</tr>

					<!-- BUTTONS TABLE ROW -->
					<tr>
						<td width="575" height="50" colspan="4">

							<div id="buttons">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showSaveButton" value="true">
								<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
								<bean:message key="button.saveSubmit"/>
								</html:submit>
								<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
								<bean:message key="button.saveAsDraft"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteButton" value="true">
								<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
								<bean:message key="button.delete"/>
								</html:submit>
							</logic:equal>


							<html:submit property="printButton" styleClass="mainButton">
								<bean:message key="button.print"/>
							</html:submit>

							<html:submit property="journalButton" styleClass="mainButton">
								<bean:message key="button.journal"/>
							</html:submit>

							<html:submit property="closeButton" styleClass="mainButton">
								<bean:message key="button.close"/>
							</html:submit>

							</div>


							<div id="buttonsDisabled" style="display: none;">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showSaveButton" value="true">
								<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveSubmit"/>
								</html:submit>
								<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveAsDraft"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteButton" value="true">
								<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
								<bean:message key="button.delete"/>
								</html:submit>
							</logic:equal>

							<html:submit property="printButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.print"/>
							</html:submit>
							<html:submit property="journalButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.journal"/>
							</html:submit>
							<html:submit property="closeButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.close"/>
							</html:submit>
							</div>

						</td>
					</tr>



					<tr valign="top">

						<td width="575" height="185" colspan="4">

							<div align="center">

							<table border="1" cellpadding="0" cellspacing="0" width="587" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="true">
							<tr>
								<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
								<bean:message key="invBranchStockTransferEntry.gridTitle.BSTDetails"/>
								</td>
							</tr>

							<!-- ===================================================================== -->
							<!--  Lines column label (enabled)                                        -->
							<!-- ===================================================================== -->
							<!-- BST Matched Selected -->

							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.lineNumber"/>
								</td>

								<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemName"/>
								</td>

								<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.location"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantity"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantityReceived"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unit"/>
								</td>

								<td width="100" height="1" colspan="" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unitCost"/>
								</td>
								<td width="100" height="1" colspan="" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.misc"/>
								</td>

							</tr>



							<tr>
								<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>

								<td width="590" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemDescription"/>
								</td>

								<td width="190" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.amount"/>
								</td>
							</tr>


							<%
								int i = 0;
								String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
								String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
								String rowBgc = null;
							%>

							<nested:iterate property="invBSTList">

							<%
								i++;
								if((i % 2) != 0)
								{
									rowBgc = ROW_BGC1;
								}else{
									rowBgc = ROW_BGC2;
								}
							%>

							<nested:hidden property="isItemEntered" value=""/>
							<nested:hidden property="isUnitEntered" value=""/>
							<nested:hidden property="shippingCost" />
							<tr bgcolor="<%= rowBgc %>">
<%-- 								<nested:iterate property="tagList"> --%>
<%-- 									<nested:hidden property="propertyCode" /> --%>
<%-- 									<nested:hidden property="specs" /> --%>
<%-- 									<nested:hidden property="custodian" /> --%>
<%-- 									<nested:hidden property="expiryDate" /> --%>
<%-- 									<nested:hidden property="serialNumber" /> --%>
<%-- 								</nested:iterate>		 --%>
								<td width="50" height="1" class="control" disabled="true">
								<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text"/>
								</td>

								<td width="220" height="1" class="control">
								<nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired" readonly="true"/>
								</td>

								<td width="190" height="1" class="control">
								<nested:select property="location" styleClass="comboRequired" style="width:100">
								<nested:options property="locationList"/>
								</nested:select>
								</td>

								<td width="80" height="1" class="control">
								<nested:text property="quantity" size="5" maxlength="10" styleClass="text" readonly="true"/>
								</td>

								<td width="80" height="1" class="control">
								<nested:text property="quantityReceived" size="5" maxlength="10" styleClass="textRequired" onkeyup="calculateAmount(name);"/>
								</td>

				       			<td width="80" height="1" class="control">
								<nested:text property="unit" size="5" maxlength="10" styleClass="text" disabled="true"/>
								</td>

								<td width="100" height="1" class="control">
								<nested:text property="unitCost" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
								</td>

								<td width="50" align="center" height="1">
								<div id="buttons">
							   	 <nested:hidden property="misc"/>
							   	 <nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
							   	 <div id="buttonsDisabled" style="display: none;">
							   	 	<nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
							   	 </td>

							</tr>

							<tr bgcolor="<%= rowBgc %>">
								<td width="50" height="1" class="control"/>

								<td width="550" height="1" class="control" colspan="5">
								<nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
								</td>

								<td width="170" height="1" class="control" colspan="1">
								<nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" disabled="true"/>
								</td>
								<td width="170" height="1" class="control" colspan="1">
								</td>

							</tr>

							</nested:iterate>
							</logic:equal>

							<!-- Non BST Matched Selected -->

							<logic:equal name="invBranchStockTransferInEntryForm" property="showBSTMatchedLinesDetails" value="false">

							<tr>
								<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
								<bean:message key="invBranchStockTransferEntry.gridTitle.BSTDetails"/>
								</td>
							</tr>

								<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.lineNumber"/>
								</td>

								<td width="200" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemName"/>
								</td>

								<td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.location"/>
								</td>

								<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantity"/>
								</td>

								<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantityReceived"/>
								</td>

								<td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unit"/>
								</td>

								<td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unitCost"/>
								</td>

								<td width="20" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.delete"/>
								</td>

							</tr>



							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>

								<td width="570" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemDescription"/>
								</td>

								<td width="60" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.amount"/>
								</td>

							</tr>


							<%
								int i = 0;
								String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
								String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
								String rowBgc = null;
							%>


							<nested:iterate property="invBSTList">

							<%
								i++;
								if((i % 2) != 0)
								{
									rowBgc = ROW_BGC1;
								}else{
									rowBgc = ROW_BGC2;
								}
							%>

							<nested:hidden property="isItemEntered" value=""/>
							<nested:hidden property="isUnitEntered" value=""/>
							<nested:hidden property="shippingCost" />
							<tr bgcolor="<%= rowBgc %>">
<%-- 								<nested:iterate property="tagList"> --%>
<%-- 									<nested:hidden property="propertyCode" /> --%>
<%-- 									<nested:hidden property="specs" /> --%>
<%-- 									<nested:hidden property="custodian" /> --%>
<%-- 									<nested:hidden property="expiryDate" /> --%>
<%-- 									<nested:hidden property="serialNumber" /> --%>
<%-- 								</nested:iterate>		 --%>
								<td width="50" height="1" class="control" disabled="true">
								<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text"/>
								</td>

								<td width="200" height="1" class="control">
								<nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired"/>
								<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
								</td>

								<td width="90" height="1" class="control">
								<nested:select property="location" styleClass="comboRequired" style="width:90">
								<nested:options property="locationList"/>
								</nested:select>
								</td>

								<td width="60" height="1" class="control">
								<nested:text property="quantity" size="5" maxlength="10" styleClass="text"/>
								</td>

								<td width="60" height="1" class="control">
								<nested:text property="quantityReceived" size="5" maxlength="10" styleClass="textRequired" onkeyup="calculateAmount(name);"/>
								</td>

				       			<td width="50" height="1" class="control">
								<nested:select property="unit" styleClass="comboRequired" style="width:50;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
								<nested:options property="unitList"/>
								</nested:select>
								</td>

								<td width="40" height="1" class="control">
								<nested:text property="unitCost" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
								</td>


								<td width="20" height="1" class="control">
								<p align="center">
								<nested:checkbox property="deleteCheckbox"/>
								</td>

								<td width="50" align="center" height="1">
								<div id="buttons">
							   	 <nested:hidden property="misc"/>
							   	 <nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
							   	 <div id="buttonsDisabled" style="display: none;">
							   	 <nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
							   	 </td>

							</tr>

							<tr bgcolor="<%= rowBgc %>">
								<td width="50" height="1" class="control"/>

								<td width="570" height="1" class="control" colspan="5">
								<nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
								</td>

								<td width="60" height="1" class="control" colspan="2">
								<nested:text property="amount" size="20" maxlength="10" styleClass="textAmount" disabled="true"/>
								</td>

							</tr>

							</nested:iterate>
							</logic:equal>

							</table>
							</div>

						</td>
					</tr>
					<tr>
						<td width="575" height="25" colspan="4">

							<div id="buttons">

							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showAddLinesButton" value="true">
							<html:submit property="addLinesButton" styleClass="mainButtonMedium">
							<bean:message key="button.addLines"/>
							</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteLinesButton" value="true">
							<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
							<bean:message key="button.deleteLines"/>
							</html:submit>
							</logic:equal>

							</div>

							<div id="buttonsDisabled" style="display: none;">

							<p align="right">
							<logic:equal name="invBranchStockTransferInEntryForm" property="showAddLinesButton" value="true">
							<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
							<bean:message key="button.addLines"/>
							</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteLinesButton" value="true">
							<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
							<bean:message key="button.deleteLines"/>
							</html:submit>
							</logic:equal>

							</div>
						</td>

					</tr>

				</logic:equal>




<!-- ===================================================================== -->
<!--  Screen when disabled                                                 -->
<!-- ===================================================================== -->






				<logic:equal name="invBranchStockTransferInEntryForm" property="enableFields" value="false">
					<tr>
						<td width="575" height="10" colspan="4">

							<div class="tabber">

							<div class="tabbertab" title="Header">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

								<tr>
								<td class="prompt" height="25" colspan="4"/>
								</tr>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.date"/>
								</td>
								<td width="158" height="25" class="control">
								<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
								</td>
								</tr>

								<htm:hidden property="transferOutCode" value=""/>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.type"/>
								</td>
								<td width="158" height="25" class="control">
        	           			<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
            	       				<html:options property="typeList"/>
	            	   			</html:select>
                				</td>
								</tr>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transferOutNumber"/>
								</td>

								<td width="158" height="25" class="control">
								<html:text property="transferOutNumber" size="18" maxlength="50" styleClass="text" disabled="true"/>
								<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
								</td>

								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transferInNumber"/>
								</td>
								<td width="157" height="25" class="control">
								<html:text property="transferInNumber" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.branchFrom"/>
								</td>
								<td width="158" height="25" class="control">
								<html:text property="branchFrom" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								<td width="120" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transitLocation"/>
								</td>
								<td width="157" height="25" class="control">
								<html:text property="transitLocation" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="127" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.description"/>
								</td>
								<td width="158" height="25" class="control" colspan="3">
								<html:text property="description" size="25" maxlength="200" styleClass="text" disabled="true"/>
								</td>
								</tr>

							</table>
							</div>


							<div class="tabbertab" title="Status">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
								<tr>
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvalStatus"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.posted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.reasonForRejection"/>
								</td>
								<td width="415" height="25" class="control" colspan="3">
								<html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
								</td>
								</tr>
							</table>
							</div>



							<div class="tabbertab" title="Log">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
								<tr>
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.createdBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateCreated"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.lastModifiedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateLastModified"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvedRejectedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateApprovedRejected"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.postedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.datePosted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>
							</table>
							</div>
							<div class="tabbertab" title="Attachment">
			   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
								     <tr>
								        <td class="prompt" width="575" height="25" colspan="4">
						                </td>
						             </tr>
						             <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename1"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton1" value="true">
								           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton1" value="true">
						                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
								     <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename2"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton2" value="true">
								           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton2" value="true">
						                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename3"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename3" size="35" styleClass="text" disabled="true" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton3" value="true">
								           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton3" value="true">
						                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename4"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton4" value="true">
								           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferInEntryForm" property="showViewAttachmentButton4" value="true">
						                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
								</table>
							</div>

							</div><script>tabberAutomatic(tabberOptions)</script>
						</td>
					</tr>




					<tr>
						<td width="575" height="50" colspan="4">

						<div id="buttons">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showSaveButton" value="true">
								<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
								<bean:message key="button.saveSubmit"/>
								</html:submit>
								<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
								<bean:message key="button.saveAsDraft"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteButton" value="true">
								<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
								<bean:message key="button.delete"/>
								</html:submit>
							</logic:equal>

							<html:submit property="printButton" styleClass="mainButton">
							<bean:message key="button.print"/>
							</html:submit>

							<html:submit property="journalButton" styleClass="mainButton">
							<bean:message key="button.journal"/>
							</html:submit>

							<html:submit property="closeButton" styleClass="mainButton">
							<bean:message key="button.close"/>
							</html:submit>
						</div>

						<div id="buttonsDisabled" style="display: none;">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showSaveButton" value="true">
								<html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveSubmit"/>
								</html:submit>
								<html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.saveAsDraft"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteButton" value="true">
								<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
								<bean:message key="button.delete"/>
								</html:submit>
							</logic:equal>

							<html:submit property="printButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.print"/>
							</html:submit>

							<html:submit property="journalButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.journal"/>
							</html:submit>

							<html:submit property="closeButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.close"/>
							</html:submit>
						</div>

						</td>
					</tr>



					<tr valign="top">

						<td width="575" height="185" colspan="4">

							<div align="center">

							<table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">

							<tr>
								<td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
								<bean:message key="invBranchStockTransferEntry.gridTitle.BSTDetails"/>
								</td>
							</tr>

							<!-- ===================================================================== -->
							<!--  Lines column label (enabled)                                        -->
							<!-- ===================================================================== -->

							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.lineNumber"/>
								</td>

								<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemName"/>
								</td>

								<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.location"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantity"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantityReceived"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unit"/>
								</td>

								<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unitCost"/>
								</td>
								<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.misc"/>
								</td>

								<!--td width="50" align="center" height="1">
								<div id="buttons">
							   	 <nested:hidden property="misc"/>
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   	 </div>
							   	 <div id="buttonsDisabled" style="display: none;">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   	 </div>
							   	 </td-->
							</tr>



							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>

								<td width="570" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemDescription"/>
								</td>

								<td width="170" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.amount"/>
								</td>

							</tr>


							<%
								int i = 0;
								String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
								String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
								String rowBgc = null;
							%>

							<nested:iterate property="invBSTList">

							<%
								i++;
								if((i % 2) != 0)
								{
									rowBgc = ROW_BGC1;
								}else{
									rowBgc = ROW_BGC2;
								}
							%>

							<tr bgcolor="<%= rowBgc %>">
<%-- 								<nested:iterate property="tagList"> --%>
<%-- 									<nested:hidden property="propertyCode" /> --%>
<%-- 									<nested:hidden property="specs" /> --%>
<%-- 									<nested:hidden property="custodian" /> --%>
<%-- 									<nested:hidden property="expiryDate" /> --%>
<%-- 									<nested:hidden property="serialNumber" /> --%>
<%-- 								</nested:iterate>		 --%>
								<td width="50" height="1" class="control" disabled="true">
								<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
								</td>

								<td width="220" height="1" class="control">
								<nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired" disabled="true"/>
								</td>

								<td width="190" height="1" class="control">
								<nested:select property="location" styleClass="comboRequired" style="width:100" disabled="true">
								<nested:options property="locationList"/>
								</nested:select>
								</td>

								<td width="80" height="1" class="control">
								<nested:text property="quantity" size="5" maxlength="10" styleClass="text" disabled="true"/>
								</td>

								<td width="80" height="1" class="control">
								<nested:text property="quantityReceived" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
								</td>

								<td width="80" height="1" class="control">
								<nested:text property="unit" size="5" maxlength="10" styleClass="text" disabled="true"/>
								</td>

								<td width="100" height="1" class="control">
								<nested:text property="unitCost" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
								</td>
								<td>
									<div id="buttons">
							   	 <nested:hidden property="misc"/>
							   	 <nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
							   	 <div id="buttonsDisabled" style="display: none;">
							   	 <nested:equal property="isTraceMisc" value="true">
							   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
							   			<bean:message key="button.miscButton"/>
							   		</nested:submit>
							   		</nested:equal>
							   	 </div>
								</td>


							</tr>

							<tr bgcolor="<%= rowBgc %>">
								<td width="50" height="1" class="control"/>

								<td width="570" height="1" class="control" colspan="4">
								<nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
								</td>

								<td width="170" height="1" class="control" colspan="3">
								<nested:text property="amount" size="23" maxlength="10" styleClass="textAmount" disabled="true"/>
								</td>
							</tr>

							</nested:iterate>

							</table>
							</div>

						</td>
					</tr>

					<tr>
						<td width="575" height="25" colspan="4">
						<div id="buttons">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton" styleClass="mainButtonMedium">
								<bean:message key="button.addLines"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
								<bean:message key="button.deleteLines"/>
								</html:submit>
							</logic:equal>
						</div>

						<div id="buttonsDisabled" style="display: none;">
							<p align="right">

							<logic:equal name="invBranchStockTransferInEntryForm" property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.addLines"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferInEntryForm" property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.deleteLines"/>
								</html:submit>
							</logic:equal>
						</div>
						</td>
					</tr>

				</logic:equal>

				<tr>
				<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
				</td>
				</tr>

				</table>
			</td>
		</tr>
	</table>
</html:form>





<script language=JavaScript type=text/javascript>
	<!--
	if(document.forms[0].elements["date"] != null && document.forms[0].elements["date"].disabled == false)
	 					 document.forms[0].elements["date"].focus();
	// -->
</script>

<logic:equal name="invBranchStockTransferInEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">

	<bean:define id="actionForm" name="invBranchStockTransferInEntryForm" type="com.struts.inv.branchstocktransferinentry.InvBranchStockTransferInEntryForm"/>

	<script type="text/javascript" langugage="JavaScript">
	  <!--
	     win = window.open("<%=request.getContextPath()%>/invRepBranchStockTransferInPrint.do?forward=1&branchStockTransferCode=<%=actionForm.getBranchStockTransferCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
	  //-->
	</script>

</logic:equal>
<logic:equal name="invBranchStockTransferInEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invBranchStockTransferInEntryForm" type="com.struts.inv.branchstocktransferinentry.InvBranchStockTransferInEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="invBranchStockTransferInEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invBranchStockTransferInEntryForm" type="com.struts.inv.branchstocktransferinentry.InvBranchStockTransferInEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>


</body>
</html>
