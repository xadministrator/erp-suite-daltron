<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>

<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>

<html>
<head>
<title>
<%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invBranchStockTransferOrderEntry.title"/> 
</title>




<link rel="stylesheet" href="css/styles.css" charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>





<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>




</head>

 




<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invBranchStockTransferOrderEntry.do" onsubmit="return submitForm();" method = "POST" enctype="multipart/form-data">

	<table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
	
      		<tr valign="top">

			<!-- ===================================================================== -->
			<!--  Title Bar                                                            -->
			<!-- ===================================================================== -->
        
        		<td width="581" height="510">
        		
          			<table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">

				<tr>
					<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
					<bean:message key="invBranchStockTransferOrderEntry.title"/>
					</td>
				</tr>


					<!-- ===================================================================== -->
					<!--  Status Msg                                                           -->
					<!-- ===================================================================== -->
				<tr>
					<td width="575" height="44" colspan="4" class="statusBar">
					
					<logic:equal name="invBranchStockTransferOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
					<bean:message key="app.success"/>
					</logic:equal>
					
					<html:errors/>	
					
					<html:messages id="msg" message="true">
					<bean:write name="msg"/>		   
					</html:messages>
					
					</td>
				</tr>

<!-- ===================================================================== -->
<!--  Screen when disabled                                                 -->
<!-- ===================================================================== -->

				<logic:equal name="invBranchStockTransferOrderEntryForm" property="enableFields" value="false">
					<tr>
						<td width="575" height="10" colspan="4">

							<div class="tabber">		

							<div class="tabbertab" title="Header">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
							
								
								<tr>
								<td width="130" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transferOrderNumber"/>
								</td>
								<td width="158" height="25" class="control">
								<html:text property="transferOrderNumber" size="18" maxlength="50" styleClass="text" disabled="true"/>
								</td>
								<td width="130" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.date"/>
								</td>
								<td width="157" height="25" class="control">
								<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
								</td>							
								</tr>
								
								<tr>                										
								<td width="130" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.branchFrom"/>
								</td>
								<td width="158" height="25" class="control">
								<html:select property="branchFrom" styleClass="comboRequired" style="width:130;" disabled="true">
								<html:options property="branchFromList"/>
								</html:select>
								</td>
								<td width="130" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.transitLocation"/>
								</td>
								<td width="157" height="25" class="control">
								<html:select property="transitLocation" styleClass="comboRequired" style="width:130;" disabled="true">
								<html:options property="transitLocationList"/>
								</html:select>
								</td>
								</tr>

								<tr>                										
								<td width="130" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.description"/>
								</td>
								<td width="158" height="25" class="control" colspan="3">
								<html:text property="description" size="50" maxlength="200" styleClass="text" disabled="true"/>
								</td>
								</tr>

							</table>
							</div>			        		    						        		    


							<div class="tabbertab" title="Status">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
								<tr> 
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvalStatus"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.posted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>	

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.reasonForRejection"/>
								</td>
								<td width="415" height="25" class="control" colspan="3">
								<html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
								</td>
								</tr>                      			         
							</table>
							</div>



							<div class="tabbertab" title="Log">
							<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
								<tr> 
								<td class="prompt" width="575" height="25" colspan="4">
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.createdBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateCreated"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>			                
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.lastModifiedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateLastModified"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>			                
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.approvedRejectedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.dateApprovedRejected"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>

								<tr>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.postedBy"/>
								</td>
								<td width="128" height="25" class="control">
								<html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								<td width="160" height="25" class="prompt">
								<bean:message key="invBranchStockTransferEntry.prompt.datePosted"/>
								</td>
								<td width="127" height="25" class="control">
								<html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
								</td>
								</tr>
							</table>
							</div>
							<div class="tabbertab" title="Attachment">
			   				    <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
								     <tr> 
								        <td class="prompt" width="575" height="25" colspan="4">
						                </td>
						             </tr>
						             <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename1"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename1" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton1" value="true">			                   
								           <html:submit property="viewAttachmentButton1" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton1" value="true">
						                   <html:submit property="viewAttachmentButton1" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
								     <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename2"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename2" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton2" value="true">
								           <html:submit property="viewAttachmentButton2" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton2" value="true">
						                   <html:submit property="viewAttachmentButton2" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr> 
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename3"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename3" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton3" value="true">
								           <html:submit property="viewAttachmentButton3" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton3" value="true">
						                   <html:submit property="viewAttachmentButton3" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>
							         <tr>
						                <td width="160" height="25" class="prompt">
						                   <bean:message key="invBranchStockTransferEntry.prompt.filename4"/>
						                </td>
						                <td width="288" height="25" class="control" colspan="2">
						                   <html:file property="filename4" size="35" styleClass="text" disabled="true"/>
						                </td>
						                <td width="127" height="25" class="control">
						                   <div id="buttons">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton4" value="true">
								           <html:submit property="viewAttachmentButton4" styleClass="mainButton">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                   <div id="buttonsDisabled" style="display: none;">
						                   <p align="center">
						                   <logic:equal name="invBranchStockTransferOrderEntryForm" property="showViewAttachmentButton4" value="true">
						                   <html:submit property="viewAttachmentButton4" styleClass="mainButton" disabled="true">
								              <bean:message key="button.viewAttachment"/>
								           </html:submit>
								           </logic:equal>
						                   </div>
						                </td>
							         </tr>               
								</table>
							</div>
							</div><script>tabberAutomatic(tabberOptions)</script>
						</td>
					</tr>




					<tr>
						<td width="575" height="50" colspan="4"> 
							<div id="buttons">
							<p align="right">
							<html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
								<bean:message key="button.close"/>
							</html:submit>
							</div>

							<div id="buttonsDisabled" style="display: none;">
							<p align="right">
							<html:submit property="closeButton" styleClass="mainButton" disabled="true">
							<bean:message key="button.close"/>
							</html:submit>
							</div>
						</td>
					</tr>



					<tr valign="top">

						<td width="575" height="185" colspan="4">

							<div align="center">

							<table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">

							<tr>
								<td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
								<bean:message key="invBranchStockTransferEntry.gridTitle.BSTDetails"/>
								</td>
							</tr>

							<!-- ===================================================================== -->
							<!--  Lines column label (enabled)                                        -->
							<!-- ===================================================================== -->

							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.lineNumber"/>
								</td>		            

								<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemName"/>
								</td>

								<td width="190" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.location"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.quantity"/>
								</td>

								<td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.unit"/>
								</td>

<%-- 								<td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"> --%>
<%-- 								   <bean:message key="invBranchStockTransferEntry.prompt.unitCost"/> --%>
<!-- 								</td>        -->

								<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								   <bean:message key="invBranchStockTransferEntry.prompt.delete"/>
								</td>				       				       	                                             
							</tr>				       			    



							<tr>
								<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>

								<td width="570" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
								<bean:message key="invBranchStockTransferEntry.prompt.itemDescription"/>
								</td>

<%-- 								<td width="170" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"> --%>
<%-- 								<bean:message key="invBranchStockTransferEntry.prompt.amount"/> --%>
<!-- 								</td>			          				        			       		     -->

							</tr>


							<%
								int i = 0;	
								String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
								String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
								String rowBgc = null;
							%>


								<nested:iterate property="invBSTList">

								<%
									i++;
									if((i % 2) != 0)
									{
										rowBgc = ROW_BGC1;
									}else
									{
										rowBgc = ROW_BGC2;
									}  
								%>


								<nested:hidden property="isItemEntered" value=""/>
								<nested:hidden property="isLocationEntered" value=""/>
								<nested:hidden property="isUnitEntered" value=""/>		


								<tr bgcolor="<%= rowBgc %>">		
									<td width="50" height="1" class="control" disabled="true">
									<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
									</td>				    								    				       	       

									<td width="220" height="1" class="control">
									<nested:text property="itemName" size="13" maxlength="25" styleClass="textRequired" disabled="true"/>
									<nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
									</td>
									
									<td width="190" height="1" class="control">
									<nested:select property="location" styleClass="comboRequired" style="width:100" disabled="true">
									<nested:options property="locationList"/>				          				          
									</nested:select>
									</td>	

									<td width="80" height="1" class="control">
									<nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
									</td>									   				       

									<td width="80" height="1" class="control">
									<nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
									<nested:options property="unitList"/>                                               
									</nested:select>
									</td>			       

									<td width="134" height="1" class="control">
									<nested:hidden property="unitCost"/>
									</td>    		       

									<td width="70" height="1" class="control">
									<p align="center">				       
									<nested:checkbox property="deleteCheckbox" disabled="true"/>
									</td>    				       
								</tr>



								<tr bgcolor="<%= rowBgc %>">
									<td width="50" height="1" class="control"/>				   

									<td width="570" height="1" class="control" colspan="4">
									<nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
									</td>

									<td width="170" height="1" class="control" colspan="2"> 
									<nested:hidden property="amount" />
									</td>				                                     
								</tr>

							</nested:iterate>

							</table>
							</div>

						</td>
					</tr>


					<tr>
						<td width="575" height="25" colspan="4"> 
						<div id="buttons">
							<p align="right">

							<logic:equal name="invBranchStockTransferOrderEntryForm" property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton" styleClass="mainButtonMedium">
								<bean:message key="button.addLines"/>
								</html:submit>			   
							</logic:equal>

							<logic:equal name="invBranchStockTransferOrderEntryForm" property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
								<bean:message key="button.deleteLines"/>
								</html:submit>
							</logic:equal>
						</div>

						<div id="buttonsDisabled" style="display: none;">
							<p align="right">

							<logic:equal name="invBranchStockTransferOrderEntryForm" property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.addLines"/>
								</html:submit>
							</logic:equal>

							<logic:equal name="invBranchStockTransferOrderEntryForm" property="showDeleteLinesButton" value="true">
								<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
								<bean:message key="button.deleteLines"/>
								</html:submit>			   
							</logic:equal>
						</div>
						</td>
					</tr>  
				</logic:equal>  
				
				<tr>
				<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
				</td>
				</tr>
				
				</table>
			</td>
		</tr>
	</table>
</html:form>





<script language=JavaScript type=text/javascript>
	<!--
	if(document.forms[0].elements["date"] != null && document.forms[0].elements["date"].disabled == false)        
	 					 document.forms[0].elements["date"].focus();
	// -->
</script>

<logic:equal name="invBranchStockTransferOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">

	<bean:define id="actionForm" name="invBranchStockTransferOrderEntryForm" type="com.struts.inv.branchstocktransferorderentry.InvBranchStockTransferOrderEntryForm"/>
	
	<script type="text/javascript" langugage="JavaScript">
	  <!--
	     win = window.open("<%=request.getContextPath()%>/invRepBranchStockTransferOrderPrint.do?forward=1&branchStockTransferCode=<%=actionForm.getBranchStockTransferCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
	  //-->
	</script>
	
</logic:equal>
<logic:equal name="invBranchStockTransferOrderEntryForm" property="attachment" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invBranchStockTransferOrderEntryForm" type="com.struts.inv.branchstocktransferorderentry.InvBranchStockTransferOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnImage.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>
<logic:equal name="invBranchStockTransferOrderEntryForm" property="attachmentPDF" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invBranchStockTransferOrderEntryForm" type="com.struts.inv.branchstocktransferorderentry.InvBranchStockTransferOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">

     win = window.open("<%=request.getContextPath()%>/cmnPDF.jsp","","height=550,width=750,resizable,menubar,scrollbars");

</script>
</logic:equal>


</body>
</html>
