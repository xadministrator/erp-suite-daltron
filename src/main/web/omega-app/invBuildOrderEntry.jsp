<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invBuildOrderEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{
	
	  var property = name.substring(0,name.indexOf("."));
       
      var quantityRequired = 0;
      var unitCost = 0;
      var totalCost = 0;  
          
      if (document.forms[0].elements[property + ".quantityRequired"].value != "" &&
      		document.forms[0].elements[property + ".unitCost"].value != "") {  
          
          if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantityRequired"].value))) {
      
	      	quantityRequired = (document.forms[0].elements[property + ".quantityRequired"].value).replace(/,/g,'');
	      	
	      }
          if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {
      
	      	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');
	      	
	      }	      
	      
	      if (!isNaN(parseFloat(document.forms[0].elements[property + ".totalCost"].value))) {
	  
	  	    totalCost = (document.forms[0].elements[property + ".totalCost"].value).replace(/,/g,'');
	  	
	      }
	  
          totalCost = (quantityRequired * unitCost).toFixed(2);
	      
		  document.forms[0].elements[property + ".totalCost"].value = formatDisabledAmount(totalCost.toString());	
	                 
      }
           	
}  

function enterSalesOrder()
{
    
    disableButtons();
    enableInputControls();   	
	document.forms[0].elements["isSalesOrderEntered"].value = true;
	document.forms[0].submit();
	   
}

function fnOpenMisc(name){

   var property = name.substring(0,name.indexOf("."));  
   currProperty = property;
   currName = name;   
   var quantity = document.forms[0].elements[property + ".quantityRequired"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");

   return false;

}


//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invBuildOrderEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="1" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        
        <!-- ===================================================================== -->
		<!--  Title Bar                                                            -->
		<!-- ===================================================================== -->
        
        <td width="581" height="510">
          <table border="1" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invBuildOrderEntry.title"/>
		</td>
		
		<!-- ===================================================================== -->
        <!--  Status Msg                                                           -->
        <!-- ===================================================================== -->
		
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invBuildOrderEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	
		 <html:hidden property="isTypeEntered" value=""/>
 		 <html:hidden property="isSalesOrderEntered" value=""/>
	     
	     <!-- ===================================================================== -->
         <!--  Screen when enabled                                                  -->
         <!-- ===================================================================== -->
	     
	     <logic:equal name="invBuildOrderEntryForm" property="enableFields" value="true">	        
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>

						<tr>
							<td width="127" height="25" class="prompt">
								<bean:message key="invBuildOrderEntry.prompt.type"/>
							</td> 
							<td width="158" height="25" class="control">
	           					<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type', 'isTypeEntered');"> 
		    		       			<html:options property="typeList"/>
        			   			</html:select>
        					</td>    
<!--        				</tr>	-->
        				
        				<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
<!--        				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.customer"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered'); enterSalesOrder();">
	                       			<html:options property="customerList"/>
                   				</html:select>
	                   			<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered'); enterSalesOrder();"/>
                			</td>
-->
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invBuildOrderEntry.prompt.soNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
<!--                   				<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" onblur="return enterSalesOrder();" onkeypress="javascript:if (event.keyCode == 13) return false;"/>	-->
								<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" />	
								<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArSoLookup('soNumber');"/>	            
                			</td>
        				</logic:equal>
        				</tr>

						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.date"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>                

         				<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
         				<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
         				</logic:equal>

				    </table> 
					</div>		        		    
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
							<logic:equal name="invBuildOrderEntryForm" property="enableBuildOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.buildOrderVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="borVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="invBuildOrderEntryForm" property="enableBuildOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.buildOrderVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="borVoid" disabled="true"/>
                			</td>
                			</logic:equal>
		 				</tr> 
				    </table> 
					</div>						
 					<div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invBuildOrderEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invBuildOrderEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
				   <html:submit property="printButton" styleClass="mainButton">
		               <bean:message key="button.print"/>
		           </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invBuildOrderEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invBuildOrderEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
				   <html:submit property="printButton" styleClass="mainButton" disabled="true">
		               <bean:message key="button.print"/>
		           </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">

					<!-- ===================================================================== -->
         			<!--  Lines column label (enabled)                                         -->
         			<!-- ===================================================================== -->
			        
			        <!-- ITEMS selected -->
					<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
			        
				    <tr>
	                <td width="577" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invBuildOrderEntry.gridTitle.BORDetails"/>
		                </td>
		            </tr>
					
		            <tr>            
		               <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.itemName"/>
				       </td>
		               <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.location"/>
				       </td>				       				    
				       <td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.quantityRequired"/>
                       </td>
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.unit"/>
                       </td>
                       
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.unitCost"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
				       <td width="844" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.itemDescription"/>
				       </td>
				       	<td width="894" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.totalCost"/>
				       </td>		          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invBORList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">		  								    				       	       
				       <td width="250" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" readonly="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiAssemblyLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="160" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:120;" onchange="return enterSelectGrid(name, 'location');">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="180" height="1" class="control">
				          <nested:text property="quantityRequired" size="8" maxlength="10" styleClass="textRequired" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:text property="unit" size="5" maxlength="25" styleClass="text" disabled="true"/>
                      </td>
                      
                      <td width="80" height="1" class="control">
                          <nested:text property="unitCost" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
                      </td>
					  <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>    	
				   <tr bgcolor="<%= rowBgc %>">			   
				       <td width="510" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="70" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>	
				      
				       <td width="125" height="1" class="control" colspan="2">
				          <nested:text property="totalCost" size="15" maxlength="70" styleClass="textAmount" disabled="true"/>
				       </td>	
				    </tr>
				  </nested:iterate>
				  </logic:equal>
				  
				  
				  <!-- SO MATCHED selected -->
				  <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
				  
					<tr>
					  	<td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
		                	<bean:message key="invBuildOrderEntry.gridTitle.SOLDetails"/>
			            </td>
			        </tr>
			        <tr>
			          	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.lineNumber"/>
						</td>
			            <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					    	<bean:message key="invBuildOrderEntry.prompt.itemName"/>
						</td>
			            <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					    	<bean:message key="invBuildOrderEntry.prompt.location"/>
					    </td>
					    <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="invBuildOrderEntry.prompt.remaining"/>
	                    </td>				       				    
						<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.quantity"/>
	                    </td>
		                <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
		                	<bean:message key="invBuildOrderEntry.prompt.unit"/>
		                </td>
		                <td width="135" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
		                    <bean:message key="invBuildOrderEntry.prompt.unitPrice"/>
		                </td>
		                <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.issue"/>
						</td>
					</tr>				       			    
					<tr>
						<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
					    <td width="300" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					    	<bean:message key="invBuildOrderEntry.prompt.itemDescription"/>
					    </td>
					    <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					        <bean:message key="invBuildOrderEntry.prompt.itemDiscount"/>
					    </td>
					    <td width="165" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="invBuildOrderEntry.prompt.amount"/>
	                    </td>
	                </tr>
					<%
						int i = 0;	
					    String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					    String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					    String rowBgc = null;
					%>
					<nested:iterate name="invBuildOrderEntryForm" property="invBORList">
						<%
							i++;
						    if((i % 2) != 0){
						    	rowBgc = ROW_BGC1;
							}else{
						    	rowBgc = ROW_BGC2;
						    }  
						%>
						<nested:hidden property="isItemEntered" value=""/>
						<tr bgcolor="<%= rowBgc %>">
							<td width="30" height="1" class="control">
						    	<nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
							</td>						       			    								    				       	       
						    <td width="120" height="1" class="control">
						    	<nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
						        <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
						    </td>
						    <td width="80" height="1" class="control">
						    	<nested:select property="location" styleClass="comboRequired" style="width:80;" disabled="true">
						        	<nested:options property="locationList"/>				          				          
						        </nested:select>
						    </td>				
						    <td width="50" height="1" class="control">
								<nested:text property="remaining" size="5" maxlength="10" styleClass="text"  disabled="true"/>
						    </td>				       				   
						    <td width="50" height="1" class="control">
						        <nested:text property="quantityRequired" size="5" maxlength="10" styleClass="textRequired"  onblur="calculateAmount(name);" onkeyup="calculateAmount(name);"/>
						    </td>
						    <td width="70" height="1" class="control">
		                        <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');" disabled="true">
		                        	<nested:options property="unitList"/>                                               
		                        </nested:select>
		                    </td>			       
		                    <td width="135" height="1" class="control" colspan="2">
		                        <nested:text property="unitCost" size="10" maxlength="25" styleClass="textAmountRequired" onchange="calculateAmount(name);" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name);" disabled="true"/>
		                    </td>
		                    <td width="30" align="center" height="1" class="control">
								<nested:checkbox property="issueCheckbox" onclick="return calculateAmount(name);"/>
							</td>
						</tr>
						<tr bgcolor="<%= rowBgc %>">
							<td width="30" height="1" class="control"/>
						    <td width="300" height="1" class="control" colspan="4">
						    	<nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
						    </td>
						    <td width="70" height="1" class="control">
						    	<nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
						    </td>		
						    <td width="165" height="1" class="control" colspan="3">
						    	<nested:text property="totalCost" size="15" maxlength="10" styleClass="textAmount" disabled="true"/>
						    </td>
						</tr>
					</nested:iterate>
				</logic:equal>
				  
				  
				  </table>
			      </div>
			      </td>
		       </tr>
		       
		    <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
			<tr>
				<td width="575" height="25" colspan="4"> 
		        	<div id="buttons">
			        	<p align="right">
			           	<logic:equal name="invBuildOrderEntryForm" property="showAddLinesButton" value="true">
							<html:submit property="addLinesButton" styleClass="mainButtonMedium">
					        	<bean:message key="button.addLines"/>
				    	   	</html:submit>			   
			       		</logic:equal>
			       		<logic:equal name="invBuildOrderEntryForm" property="showDeleteLinesButton" value="true">
				       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            		<bean:message key="button.deleteLines"/>
				       		</html:submit>
			       		</logic:equal>
		           	</div>
		           	<div id="buttonsDisabled" style="display: none;">
		           		<p align="right">
		           		<logic:equal name="invBuildOrderEntryForm" property="showAddLinesButton" value="true">
		           			<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            		<bean:message key="button.addLines"/>
			       			</html:submit>
			       		</logic:equal>
			       		<logic:equal name="invBuildOrderEntryForm" property="showDeleteLinesButton" value="true">
			       			<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            		<bean:message key="button.deleteLines"/>
			       			</html:submit>			   
			       		</logic:equal>
	           		</div>
			  	</td>
			</tr>	
			</logic:equal>
	     </logic:equal>
	     	     
	     <!-- ===================================================================== -->
         <!--  Screen when disabled                                                 -->
         <!-- ===================================================================== -->
	     
	     <logic:equal name="invBuildOrderEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             
						<tr>
							<td width="127" height="25" class="prompt">
								<bean:message key="invBuildOrderEntry.prompt.type"/>
							</td> 
							<td width="158" height="25" class="control">
	           					<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
		    		       			<html:options property="typeList"/>
        			   			</html:select>
        					</td>    
<!--        				</tr>	-->
        				
        				<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
<!--        				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.customer"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('customer','isCustomerEntered'); enterSalesOrder();">
	                       			<html:options property="customerList"/>
                   				</html:select>
	                   			<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isCustomerEntered'); enterSalesOrder();"/>
                			</td>
-->
                			<td width="130" height="25" class="prompt">
                  	 			<bean:message key="invBuildOrderEntry.prompt.soNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
<!--                   				<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" onblur="return enterSalesOrder();" onkeypress="javascript:if (event.keyCode == 13) return false;"/>	-->
								<html:text property="soNumber" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>	
								<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
        				</logic:equal>
        				</tr>
			             
	     				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.date"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>         
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
	                	</tr>
	                	
	                	<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
         				<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>  
         				</logic:equal>
	                	
				    </table> 
					</div>		        		    
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
		 				<tr>
							<logic:equal name="invBuildOrderEntryForm" property="enableBuildOrderVoid" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.buildOrderVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="borVoid"/>
                			</td>
                			</logic:equal>
                			<logic:equal name="invBuildOrderEntryForm" property="enableBuildOrderVoid" value="false">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invBuildOrderEntry.prompt.buildOrderVoid"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="borVoid" disabled="true"/>
                			</td>
                			</logic:equal>	
		 				</tr>          
				    </table> 
					</div>						
			        <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                      			         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invBuildOrderEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
         <tr>
             <td width="575" height="50" colspan="4"> 
               <div id="buttons">
               <p align="right">
               <logic:equal name="invBuildOrderEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
               <logic:equal name="invBuildOrderEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
				   <html:submit property="printButton" styleClass="mainButton">
		               <bean:message key="button.print"/>
		           </html:submit>
		       </logic:equal>
			   <html:submit property="closeButton" styleClass="mainButton">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
               <div id="buttonsDisabled" style="display: none;">
               <p align="right">
               <logic:equal name="invBuildOrderEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
               <logic:equal name="invBuildOrderEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
			   <logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
				   <html:submit property="printButton" styleClass="mainButton" disabled="true">
		               <bean:message key="button.print"/>
		           </html:submit>
		       </logic:equal>
               <html:submit property="closeButton" styleClass="mainButton" disabled="true">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
              </td>
         </tr>
         <tr valign="top">
                  <td width="575" height="185" colspan="4">
                  <div align="center">
                    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">

					<!-- ===================================================================== -->
         			<!--  Lines column label (disabled)                                        -->
         			<!-- ===================================================================== -->
					
			        <!-- ITEMS selected -->
					<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">

                    <tr>
                    <td width="577" height="1" colspan="6" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                            <bean:message key="invBuildOrderEntry.gridTitle.BORDetails"/>
                        </td>
                    </tr>
					
		            <tr>            
		               <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.itemName"/>
				       </td>
		               <td width="160" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.location"/>
				       </td>				       				    
				       <td width="180" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.quantityRequired"/>
                       </td>
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.unit"/>
                       </td>
                       
                       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.unitCost"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invBuildOrderEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>				       			    
				    <tr>
				       <td width="844" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.itemDescription"/>
				       </td>
				       	<td width="894" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invBuildOrderEntry.prompt.totalCost"/>
				       </td>		          				        			       		    
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invBORList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    
				    <tr bgcolor="<%= rowBgc %>">		  								    				       	       
				       <td width="250" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true" />
				       </td>
				       <td width="160" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:120;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="180" height="1" class="control">
				          <nested:text property="quantityRequired" size="8" maxlength="10" styleClass="textRequired" disabled="true" />
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:text property="unit" size="5" maxlength="25" styleClass="text" disabled="true"/>
                      </td>
                      
                      <td width="80" height="1" class="control">
                          <nested:text property="unitCost" size="10" maxlength="25" styleClass="textAmount" disabled="true"/>
                      </td>
					  <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox"/>
                          
                       </td>    	
				   <tr bgcolor="<%= rowBgc %>">			   
				       <td width="510" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="70" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>	
				      
				       <td width="125" height="1" class="control" colspan="2">
				          <nested:text property="totalCost" size="15" maxlength="70" styleClass="textAmount" disabled="true"/>
				       </td>	
				    </tr>
				  </nested:iterate>
				  </logic:equal>
				  
				
				<!-- SO MATCHED selected -->
				<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="true">
				
					<tr>
						<td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			            	<bean:message key="invBuildOrderEntry.gridTitle.SOLDetails"/>
						</td>
					</tr>
					<tr>
				    	<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.lineNumber"/>
						</td>
				        <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.itemName"/>
						</td>
				        <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.location"/>
						</td>
						<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
		                	<bean:message key="invBuildOrderEntry.prompt.remaining"/>
		                </td>				       				    
						<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.quantity"/>
		                </td>
			            <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            	<bean:message key="invBuildOrderEntry.prompt.unit"/>
			            </td>
			            <td width="135" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			            	<bean:message key="invBuildOrderEntry.prompt.unitPrice"/>
			            </td>
			            <td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.issue"/>
						</td>
					</tr>				       			    
					<tr>
						<td width="30" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>"/>
						<td width="300" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.itemDescription"/>
						</td>
						<td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
							<bean:message key="invBuildOrderEntry.prompt.itemDiscount"/>
						</td>
						<td width="165" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
		                	<bean:message key="invBuildOrderEntry.prompt.amount"/>
		                </td>
					</tr>
					<%
						int i = 0;	
						String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
						String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
						String rowBgc = null;
					%>
					<nested:iterate name="invBuildOrderEntryForm" property="invBORList">
						<%
							i++;
							if((i % 2) != 0){
								rowBgc = ROW_BGC1;
							}else{
								rowBgc = ROW_BGC2;
							}  
						%>
						<nested:hidden property="isItemEntered" value=""/>
						<tr bgcolor="<%= rowBgc %>">
							<td width="30" height="1" class="control">
						    	<nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" disabled="true"/>
							</td>						       			    								    				       	       
						    <td width="120" height="1" class="control">
						    	<nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" disabled="true"/>
						        <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
						    </td>
						    <td width="80" height="1" class="control">
						    	<nested:select property="location" styleClass="comboRequired" style="width:80;" disabled="true">
						        	<nested:options property="locationList"/>				          				          
						        </nested:select>
						    </td>				
						    <td width="50" height="1" class="control">
								<nested:text property="remaining" size="5" maxlength="10" styleClass="text" disabled="true"/>
						    </td>				       				   
						    <td width="50" height="1" class="control">
						        <nested:text property="quantityRequired" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
						    </td>
						    <td width="70" height="1" class="control">
		                        <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
		                        	<nested:options property="unitList"/>                                               
		                        </nested:select>
		                    </td>			       
		                    <td width="135" height="1" class="control" colspan="2">
		                        <nested:text property="unitCost" size="10" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
		                    </td>
		                    <td width="30" align="center" height="1" class="control">
								<nested:checkbox property="issueCheckbox" disabled="true"/>
							</td>
						</tr>
						<tr bgcolor="<%= rowBgc %>">
							<td width="30" height="1" class="control"/>
						    <td width="300" height="1" class="control" colspan="4">
						    	<nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
						    </td>
						    <td width="70" height="1" class="control">
						    	<nested:text property="totalDiscount" size="9" maxlength="10" styleClass="textAmount" disabled="true"/>
						    </td>		
						    <td width="165" height="1" class="control" colspan="3">
						    	<nested:text property="totalCost" size="15" maxlength="10" styleClass="textAmount" disabled="true"/>
						    </td>
						</tr>
					</nested:iterate>
				</logic:equal>
			  
				  
				  
				  </table>
			      </div>
			      </td>
		       </tr>
		       
			<logic:equal name="invBuildOrderEntryForm" property="showSOMatchedLinesDetails" value="false">
		    	<tr>
		        	<td width="575" height="25" colspan="4"> 
		           		<div id="buttons">
						<p align="right">
							<logic:equal name="invBuildOrderEntryForm" property="showAddLinesButton" value="true">
								<html:submit property="addLinesButton" styleClass="mainButtonMedium">
						            <bean:message key="button.addLines"/>
		    					</html:submit>			   
							</logic:equal>
					       	<logic:equal name="invBuildOrderEntryForm" property="showDeleteLinesButton" value="true">
					       		<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
					            	<bean:message key="button.deleteLines"/>
					       		</html:submit>
					       	</logic:equal>
						</div>
						<div id="buttonsDisabled" style="display: none;">
					    <p align="right">
					    	<logic:equal name="invBuildOrderEntryForm" property="showAddLinesButton" value="true">
					        	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
						        	<bean:message key="button.addLines"/>
								</html:submit>
							</logic:equal>
						    <logic:equal name="invBuildOrderEntryForm" property="showDeleteLinesButton" value="true">
						    	<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
						        	<bean:message key="button.deleteLines"/>
								</html:submit>			   
							</logic:equal>
		           		</div>
					</td>
				</tr>  
			</logic:equal>  
		</logic:equal>  
	     <tr>
	     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)        
        document.forms[0].elements["date"].focus();
	       // -->
</script>
<logic:equal name="invBuildOrderEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invBuildOrderEntryForm" type="com.struts.inv.buildorderentry.InvBuildOrderEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/invRepBuildOrderPrint.do?forward=1&buildOrderCode=<%=actionForm.getBuildOrderCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
