<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="buildUnbuildAssemblyEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function fnOpenMisc(name){

   var property = name.substring(0,name.indexOf("."));  
   currProperty = property;
   currName = name;   
   var quantity = document.forms[0].elements[property + ".buildQuantityBy"].value.replace(/,/g,'') * 1;
   var miscStr = document.forms[0].elements[property + ".misc"].value;
   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&miscStr="+miscStr, "", "width=260,height=230,scrollbars=yes,status=no");

   return false;

}


function purchaseOrderEnterSubmit()
{   

    if (document.activeElement.name != 'boNumber') {
    
        return enterSubmit(event, new Array('saveSubmitButton'));
    
    } else {
    
        return true;
    
    }
     
}

function enterPurchaseOrder()
{
  
  disableButtons();
  enableInputControls();   	
	document.forms[0].elements["isPurchaseOrderEntered"].value = true;
	document.forms[0].submit();
	   
}


//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invBuildUnbuildAssemblyEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="900" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        
        <!-- ===================================================================== -->
		<!--  Title Bar                                                            -->
		<!-- ===================================================================== -->
        
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="buildUnbuildAssemblyEntry.title"/>
		</td>
		
		<!-- ===================================================================== -->
        <!--  Status Msg                                                           -->
        <!-- ===================================================================== -->
		
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	     
	     <!-- ===================================================================== -->
         <!--  Screen when enabled                                                  -->
         <!-- ===================================================================== -->
	     
	    <html:hidden property="isPurchaseOrderEntered" value="" />
		<html:hidden property="isTypeEntered" value="" />
				
	     <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
			             <tr>
								<td width="130" height="25" class="prompt">
									<bean:message key="buildUnbuildAssemblyEntry.prompt.type"/>
								</td>
								<td width="158" height="25" class="control">
									<html:select property="type" styleClass="comboRequired" style="width:130;" onchange="return enterSelect('type','isTypeEntered');">
										<html:options property="typeList" />
									</html:select>
								</td>
								
								 <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showBatchName" value="true">
			               			<td width="130" height="25" class="prompt">
			                  				<bean:message key="buildUnbuildAssemblyEntry.prompt.batchName"/>
			               			</td>
			               			<td width="157" height="25" class="control">
	                  				<html:select property="batchName" styleClass="comboRequired" style="width:130;">
	                      			<html:options property="batchNameList"/>
	                  				</html:select>
			               			</td>
		        				</logic:equal>
							</tr>
							
			            
			             <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.customer"/>
                			</td>
							<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" onchange="return enterPurchaseOrder();">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isPurchaseOrderEntered');"/>
                			</td>
							</logic:equal>
							<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customer', '', 'isPurchaseOrderEntered');"/>
                			</td>
							</logic:equal>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" onblur="return setEffectivityDate('date','effectivityDate');"/>
                			</td>
						</tr>
						
						<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
						
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.dueDate"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				
         				<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type"
								value="BO MATCHED">
								<tr>
								<td width="130" height="25" class="prompt">
									
							<bean:message key="buildUnbuildAssemblyEntry.prompt.boNumber" /></td>
								<td width="445" height="25" class="control" colspan="3">
							<html:text property="boNumber" size="15" maxlength="25"
										styleClass="textRequired"
										onblur="return enterPurchaseOrder();"
										onkeypress="javascript:if (event.keyCode == 13) return false;" />
									<html:image property="lookupButton" src="images/lookup.gif"
										onclick="return showInvBoLookup('boNumber');" /></td>
								</tr>
							</logic:equal>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>

						<td class="prompt" width="140" height="25">
						    <bean:message key="buildUnbuildAssemblyEntry.prompt.reportType"/>
						</td>
						<td width="128" height="25" class="control">
						    <html:select property="reportType" styleClass="combo">
								<html:options property="reportTypeList"/>
							</html:select>
						</td>
						</tr>      
						
						
						<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="buildUnbuildAssemblyEntry.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>
						
						   
				    </table>
					</div>		        		    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
			   </html:submit>
		       <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>	          
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       	<html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   		</html:submit>
		       <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type" value="BO MATCHED">
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="590" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="buildUnbuildAssemblyEntry.gridTitle.BUADetails"/>
		                </td>
		            </tr>
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.lineNumber"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemName"/>
				       </td>
		               <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.location"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.specificGravity"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.standardFillSize"/>
				       </td>
				       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.quantityOnHand"/>
                       </td>
                       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.remaining"/>
                       </td> 
                       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.buildQuantityBy"/>
                       </td> 
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.unit"/>
                       </td>   
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.delete"/>
                       </td>    
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       </td>  			       		                                               
				    </tr>				       			    
				    <tr>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				       </td>			       				       
				       <td width="844" height="1" class="gridHeader" colspan="10" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemDescription"/>
				       </td>
				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invBUAList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isLocationEntered" value=""/>
				    
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isBuildQuantityByEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:text property="itemName" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiAssemblyLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:130;" onchange="return enterSelectGrid(name, 'location','isLocationEntered');">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				        <td width="90" height="1" class="control">
				          <nested:text property="specificGravity" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="standardFillSize" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				       
				       <td width="90" height="1" class="control">
				          <nested:text property="quantity" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="remaining" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="buildQuantityBy" size="6" maxlength="10" styleClass="textRequired"/>
				       </td>
				       <td width="120" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="70" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
                       <td width="50" align="center" height="1">
					   	 <div id="buttons">
					   	 <nested:hidden property="misc"/>
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   	 </div>
					   </td>                             
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>
				       <td width="840" height="1" class="control" colspan="6">
                          <nested:text property="itemDescription" size="80" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
                       </td>	
				   </tr>							    
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       
		       </logic:equal>
		       
		       
		       
		       
		       <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type" value="ITEMS">
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                	<td width="590" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="buildUnbuildAssemblyEntry.gridTitle.BUADetails"/>
		                </td>
		            </tr>
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.lineNumber"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemName"/>
				       </td>
		               <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.location"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.specificGravity"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.standardFillSize"/>
				       </td>
				       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.quantityOnHand"/>
                       </td>
                       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.buildQuantityBy"/>
                       </td> 
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.unit"/>
                       </td>   
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.delete"/>
                       </td>    
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       </td>  			       		                                               
				    </tr>				       			    
				    <tr>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				       </td>			       				       
				       <td width="844" height="1" class="gridHeader" colspan="10" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemDescription"/>
				       </td>
				       
				    </tr>
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invBUAList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isLocationEntered" value=""/>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isBuildQuantityByEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:text property="itemName" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiAssemblyLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:130;" onchange="return enterSelectGrid(name, 'location','isLocationEntered');">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				        <td width="90" height="1" class="control">
				          <nested:text property="specificGravity" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				        <td width="90" height="1" class="control">
				          <nested:text property="standardFillSize" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="quantity" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>

				       <td width="90" height="1" class="control">
				          <nested:text property="buildQuantityBy" size="6" maxlength="10" styleClass="textRequired"/>
				       </td>
				       <td width="120" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" onchange="return enterSelectGrid(name, 'unit','isUnitEntered');">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="70" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
                       <td width="50" align="center" height="1">
					   	 <div id="buttons">
					   	 <nested:hidden property="misc"/>
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   	 </div>
					   </td>                             
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>
				       <td width="840" height="1" class="control" colspan="6">
                          <nested:text property="itemDescription" size="80" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
                       </td>	
				   </tr>							    
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       
		       </logic:equal>
		       
		       
		       
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	     	     
	     <!-- ===================================================================== -->
         <!--  Screen when disabled                                                 -->
         <!-- ===================================================================== -->
	     
	     <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="enableFields" value="false">
         <tr>
                <td width="575" height="10" colspan="4">
                    <div class="tabber">  
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">          
                         <tr> 
                            <td class="prompt" width="575" height="25" colspan="4">
                            </td>
                         </tr>
                         <tr>
                         
                         <td width="130" height="25" class="prompt">
								<bean:message key="buildUnbuildAssemblyEntry.prompt.type"/>
							</td>
							<td width="158" height="25" class="control">
								<html:select property="type" styleClass="comboRequired" style="width:130;" disabled="true">
									<html:options property="typeList" />
								</html:select>
							</td>
								
                         <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showBatchName" value="true">
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.batchName"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:select property="batchName" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="batchNameList"/>
                   				</html:select>
                			</td>
         					</logic:equal>
         				</tr>	
                         <tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.customer"/>
                			</td>
							<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="useCustomerPulldown" value="true">
                			<td width="158" height="25" class="control">
                   				<html:select property="customer" styleClass="comboRequired" style="width:130;" disabled="true">
                       			<html:options property="customerList"/>
                   				</html:select>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
							<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="useCustomerPulldown" value="false">
                			<td width="158" height="25" class="control">
                   				<html:text property="customer" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                       			<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
							</logic:equal>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.date"/>
                			</td>
                			<td width="157" eight="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
						</tr>
						
						<tr>    
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invoiceEntry.prompt.customerName"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="customerName" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.dueDate"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="dueDate" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				
         				<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type"
								value="BO MATCHED">
								<tr>
									<td width="130" height="25" class="prompt"><bean:message
										key="buildUnbuildAssemblyEntry.prompt.boNumber" /></td>
									<td width="445" height="25" class="control" colspan="3"><html:text
										property="boNumber" size="15" maxlength="25"
										styleClass="textRequired" disabled="true" /> <html:image
										property="lookupButton" src="images/lookup.gif"
										disabled="true" /></td>
								</tr>
							</logic:equal>
							
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="buildUnbuildAssemblyEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
         				<td class="prompt" width="140" height="25">
				    <bean:message key="buildUnbuildAssemblyEntry.prompt.reportType"/>
						</td>
						<td width="128" height="25" class="control">
						    <html:select property="reportType" styleClass="combo">
								<html:options property="reportTypeList"/>
							</html:select>
						</td>
						
		
					</tr>
					
					
					<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="buildUnbuildAssemblyEntry.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>
			
                    </table>
					</div>                            
                     <div class="tabbertab" title="Status">
                     <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">          
                         <tr> 
                            <td class="prompt" width="575" height="25" colspan="4">
                            </td>
                         </tr>
                         <tr>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.approvalStatus"/>
                            </td>
                            <td width="128" height="25" class="control">
                               <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.posted"/>
                            </td>
                            <td width="127" height="25" class="control">
                               <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                         </tr>
                         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="buildUnbuildAssemblyEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>                       
                     </table>
                     </div>
                     <div class="tabbertab" title="Log">
                     <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">          
                         <tr> 
                            <td class="prompt" width="575" height="25" colspan="4">
                            </td>
                         </tr>
                         <tr>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.createdBy"/>
                            </td>
                            <td width="128" height="25" class="control">
                               <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.dateCreated"/>
                            </td>
                            <td width="127" height="25" class="control">
                               <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>                           
                         </tr>
                         <tr>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.lastModifiedBy"/>
                            </td>
                            <td width="128" height="25" class="control">
                               <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.dateLastModified"/>
                            </td>
                            <td width="127" height="25" class="control">
                               <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>                           
                         </tr>
                         <tr>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.approvedRejectedBy"/>
                            </td>
                            <td width="128" height="25" class="control">
                               <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.dateApprovedRejected"/>
                            </td>
                            <td width="127" height="25" class="control">
                               <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                         </tr>
                         <tr>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.postedBy"/>
                            </td>
                            <td width="128" height="25" class="control">
                               <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                            <td width="160" height="25" class="prompt">
                               <bean:message key="buildUnbuildAssemblyEntry.prompt.datePosted"/>
                            </td>
                            <td width="127" height="25" class="control">
                               <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
                            </td>
                         </tr>
                     </table>
                     </div>
                     </div><script>tabberAutomatic(tabberOptions)</script>
                 </td>
         </tr>
         <tr>
             <td width="575" height="50" colspan="4"> 
               <div id="buttons">
               <p align="right">
               <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
                <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   		</html:submit>
               <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>             
               <html:submit property="closeButton" styleClass="mainButton">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
               <div id="buttonsDisabled" style="display: none;">
               <p align="right">
               <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
                <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   		</html:submit>
               <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
               <html:submit property="closeButton" styleClass="mainButton" disabled="true">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
              </td>
         </tr>
         
         <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type" value="BO MATCHED">
         <tr valign="top">
                  <td width="575" height="185" colspan="4">
                  <div align="center">
                    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                    <tr>
                    <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                            <bean:message key="buildUnbuildAssemblyEntry.gridTitle.BUADetails"/>
                        </td>
                    </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (disabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.lineNumber"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemName"/>
				       </td>
		               <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.location"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.specificGravity"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.standardFillSize"/>
				       </td>
				       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.quantityOnHand"/>
                       </td>

						<td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.remaining"/>
                       </td>

                       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.buildQuantityBy"/>
                       </td>                       
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.unit"/>
                       </td>   
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.delete"/>
                       </td>    			       		                                               
				    </tr>				       			    
				    <tr>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				       </td>			       				       
				       <td width="844" height="1" class="gridHeader" colspan="10" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemDescription"/>
				       </td>

				    </tr>
                    <%
                       int i = 0;   
                       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                       String rowBgc = null;
                    %>
                    <nested:iterate property="invBUAList">
                    <%
                       i++;
                       if((i % 2) != 0){
                           rowBgc = ROW_BGC1;
                       }else{
                           rowBgc = ROW_BGC2;
                       }  
                    %>
                    <nested:hidden property="isLocationEntered"/>
                    <nested:hidden property="isItemEntered"/>
                    <nested:hidden property="isBuildQuantityByEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:text property="itemName" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:130;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				        <td width="90" height="1" class="control">
				          <nested:text property="specificGravity" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="standardFillSize" size="6" maxlength="10" styleClass="textRequired" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="quantity" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
					<td width="90" height="1" class="control">
				          <nested:text property="remaining" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="buildQuantityBy" size="6" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="70" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
                       <td width="50" align="center" height="1">
					   	 <div id="buttons">
					   	 <nested:hidden property="misc"/>
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   	 </div>
					   </td>                             
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>
				       <td width="840" height="1" class="control" colspan="7">
                          <nested:text property="itemDescription" size="80" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
                       </td>	
				    </tr>
                  </nested:iterate>
                  </table>
                  </div>
                  </td>
               </tr>
               </logic:equal>
         
         <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="type" value="ITEMS">
         <tr valign="top">
                  <td width="575" height="185" colspan="4">
                  <div align="center">
                    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                    <tr>
                    <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                            <bean:message key="buildUnbuildAssemblyEntry.gridTitle.BUADetails"/>
                        </td>
                    </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (disabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.lineNumber"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemName"/>
				       </td>
		               <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.location"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.specificGravity"/>
				       </td>
				       <td width="237" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyOrderEntry.prompt.standardFillSize"/>
				       </td>
				       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.quantityOnHand"/>
                       </td>
                       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.buildQuantityBy"/>
                       </td>                       
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.unit"/>
                       </td>   
				       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="buildUnbuildAssemblyEntry.prompt.delete"/>
                       </td>    			       		                                               
				    </tr>				       			    
				    <tr>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				       </td>			       				       
				       <td width="844" height="1" class="gridHeader" colspan="10" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="buildUnbuildAssemblyEntry.prompt.itemDescription"/>
				       </td>

				    </tr>
                    <%
                       int i = 0;   
                       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
                       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
                       String rowBgc = null;
                    %>
                    <nested:iterate property="invBUAList">
                    <%
                       i++;
                       if((i % 2) != 0){
                           rowBgc = ROW_BGC1;
                       }else{
                           rowBgc = ROW_BGC2;
                       }  
                    %>
                    <nested:hidden property="isLocationEntered"/>
                    <nested:hidden property="isItemEntered"/>
                    <nested:hidden property="isBuildQuantityByEntered" value=""/>
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:text property="itemName" size="15" maxlength="25" styleClass="textRequired" disabled="true"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
				       </td>
				       <td width="237" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:130;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				        <td width="90" height="1" class="control">
				          <nested:text property="specificGravity" size="6" maxlength="10" styleClass="text" disabled="true" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="standardFillSize" size="6" maxlength="10" styleClass="text" disabled="true" />
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="quantity" size="6" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="90" height="1" class="control">
				          <nested:text property="buildQuantityBy" size="6" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="120" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="70" height="1" class="control">
				          <p align="center">
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>
                       <td width="50" align="center" height="1">
					   	 <div id="buttons">
					   	 <nested:hidden property="misc"/>
						   	 <nested:submit property="miscButton" styleClass="mainButtonSmall" onclick="return fnOpenMisc(name);">
						   			<bean:message key="button.miscButton"/>
						   	 </nested:submit>
					   	 </div>
					   	 <div id="buttonsDisabled" style="display: none;">
					   		<nested:submit property="miscButton" styleClass="mainButtonSmall" disabled="true">
					   			<bean:message key="button.miscButton"/>
					   		</nested:submit>
					   	 </div>
					   </td>                             
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
				       <td width="50" height="1" class="control"/>
				       <td width="840" height="1" class="control" colspan="7">
                          <nested:text property="itemDescription" size="80" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
                       </td>	
				    </tr>
                  </nested:iterate>
                  </table>
                  </div>
                  </td>
               </tr>
               </logic:equal>
               
               
               
               
               
               <tr>
                 <td width="575" height="25" colspan="4"> 
                   <div id="buttons">
                   <p align="right">
                   <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showAddLinesButton" value="true">
                   <html:submit property="addLinesButton" styleClass="mainButtonMedium">
                        <bean:message key="button.addLines"/>
                   </html:submit>              
                   </logic:equal>
                   <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteLinesButton" value="true">
                   <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
                        <bean:message key="button.deleteLines"/>
                   </html:submit>
                   </logic:equal>
                   </div>
                   <div id="buttonsDisabled" style="display: none;">
                   <p align="right">
                   <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showAddLinesButton" value="true">
                   <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
                        <bean:message key="button.addLines"/>
                   </html:submit>
                   </logic:equal>
                   <logic:equal name="invBuildUnbuildAssemblyEntryForm" property="showDeleteLinesButton" value="true">
                   <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
                        <bean:message key="button.deleteLines"/>
                   </html:submit>              
                   </logic:equal>
                   </div>
                  </td>
         </tr>  
         </logic:equal>  
	     <tr>
	     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)        
        document.forms[0].elements["date"].focus();
	       // -->
</script>

<logic:equal name="invBuildUnbuildAssemblyEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
	<bean:define id="actionForm" name="invBuildUnbuildAssemblyEntryForm" type="com.struts.inv.buildunbuildassemblyentry.InvBuildUnbuildAssemblyEntryForm" />
	<script type="text/javascript" langugage="JavaScript">
	<!--
    	win = window.open("<%=request.getContextPath()%>/invRepBuildUnbuildPrint.do?forward=1&buildUnbuildAssemblyCode=<%=actionForm.getBuildUnbuildAssemblyCode()%>&reportType=<%=actionForm.getReportType()%>&viewType=<%=actionForm.getViewType()%>","","height=550,width=750,resizable,menubar,scrollbars");
	//-->
	</script>
</logic:equal>

</body>
</html>
