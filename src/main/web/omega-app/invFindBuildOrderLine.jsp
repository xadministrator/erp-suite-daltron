<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findBuildOrderLine.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function pickAccount(name) 
{
   
   var property = name.substring(0,name.indexOf("."));      
      
   if (window.opener && !window.opener.closed) {


       window.opener.document.forms[0].elements[document.forms[0].elements["selectedItemName"].value].value = 
           document.forms[0].elements[property + ".itemName"].value;                         

       if (document.forms[0].elements["selectedItemEntered"] != null &&
           document.forms[0].elements["selectedItemEntered"].value != "" &&
           window.opener.document.forms[0].elements[document.forms[0].elements["selectedItemEntered"].value] != null) {

           var divList = window.opener.document.body.getElementsByTagName("div");
      
	       for (var i = 0; i < divList.length; i++) {
	      
	          var currDiv = divList[i];
	          var currId = currDiv.id;
	          
	          if (currId == "buttons") {
	          
	              currDiv.style.display = "none";
	          
	          } else if (currId == "buttonsDisabled") {
	          
	              currDiv.style.display = "block";
	          
	          }
	      
	       }
           
		   for (var i=0; i < window.opener.document.forms[0].elements.length; i++) {
		      
		      if (window.opener.document.forms[0].elements[i].type != 'submit') {
		          
		          window.opener.document.forms[0].elements[i].disabled=false;
		          
		      }
		   }
		   
		   
		   window.opener.document.forms[0].elements["buildOrder"].value = document.forms[0].elements[property + ".documentNumber"].value;
		   window.opener.document.forms[0].elements["itemName"].value = document.forms[0].elements[property + ".itemName"].value;
		   window.opener.document.forms[0].elements["location"].value = document.forms[0].elements[property + ".location"].value;
		   window.opener.document.forms[0].elements["bolCode"].value = document.forms[0].elements[property + ".buildOrderLineCode"].value;
           window.opener.document.forms[0].elements[document.forms[0].elements["selectedItemEntered"].value].value = true;           
           window.opener.document.forms[0].submit();
                      
       }
     
   }
   
   window.close();
   
   return false;

}

function closeWin() 
{
   window.close();
   
   return false;
}

//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<html:form action="/invFindBuildOrderLine.do" onsubmit="return disableButtons();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="findBuildOrderLine.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="15" colspan="4" class="statusBar">
		   <logic:equal name="invFindBuildOrderLineForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="selectedItemName"/>
	     <html:hidden property="selectedItemEntered"/>	     
	     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="invFindBuildOrderLineForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="invFindBuildOrderLineForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildOrderLineForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildOrderLineForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="closeButton" styleClass="mainButton" onclick="return closeWin();">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="findBuildOrderLine.gridTitle.FBLDetails"/>
	                       </td>
	                    </tr>
			    <tr>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.itemName"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.location"/>
			       </td>
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.documentNumber"/>
			       </td>			       
			       <td width="149" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.quantityRequired"/>
			       </td>
			       <td width="298" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.quantityAssembled"/>
			       </td>
                </tr>
                 <tr>
			       <td width="894" height="1" class="gridHeader" colspan="6" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildOrderLine.prompt.description"/>
			       </td>
                </tr>	   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="invFBLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
				<nested:hidden property="buildOrderLineCode"/>
			    <tr bgcolor="<%= rowBgc %>">				    
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="itemName"/>
			          <nested:hidden property="itemName"/>
			       </td>
                   <td width="149" height="1" class="gridLabel">
			          <nested:write property="location"/>
					  <nested:hidden property="location"/>					  
			       </td>
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="documentNumber"/>
					  <nested:hidden property="documentNumber"/>                      
			       </td>
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="quantityRequired"/>
			       </td>			       
			       <td width="149" height="1" class="gridLabel">
			          <nested:write property="quantityAssembled"/>
			       </td>			       
			       <td width="149" align="center" height="1">
			          <div id="buttons">
			          <nested:submit property="openButton" styleClass="gridButton" onclick="return pickAccount(name);">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
  	                  <div id="buttonsDisabled" style="display: none;">
			          <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.select"/>
  	                  </nested:submit>
  	                  </div>
			       </td>
			    </tr>
                <tr bgcolor="<%= rowBgc %>">
			       <td width="894" height="1" colspan="6" class="gridLabel">
			          <nested:write property="description"/>
			       </td>
                </tr>
			    </nested:iterate>
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		  </td>
	       </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["itemName"] != null &&
        document.forms[0].elements["itemName"].disabled == false)
        document.forms[0].elements["itemName"].focus()
   // -->
</script>
</body>
</html>
