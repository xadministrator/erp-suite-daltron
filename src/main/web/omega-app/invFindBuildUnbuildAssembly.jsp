<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="findBuildUnbuildAssembly.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/invFindBuildUnbuildAssembly.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
	            <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		           <bean:message key="findBuildUnbuildAssembly.title"/>
		        </td>
	         </tr>
             <tr>
	            <td width="575" height="44" colspan="4" class="statusBar">
		        <logic:equal name="invFindBuildUnbuildAssemblyForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                   <bean:message key="app.success"/>
                </logic:equal>
		        <html:errors/>	
	            </td>
	         </tr>
	         <logic:equal name="invFindBuildUnbuildAssemblyForm" property="showBatchName" value="true">
	     	 <tr>
                <td width="160" height="25" class="prompt">
                   <bean:message key="findInvoice.prompt.batchName"/>
                </td>
                <td width="415" height="25" class="control" colspan="3">
                   <html:select property="batchName" styleClass="combo">
                       <html:options property="batchNameList"/>
                   </html:select>
                </td>
	         </tr>
	         </logic:equal>
	        
	         <tr>
		        <td class="prompt" width="185" height="25">
                   <bean:message key="findInvoice.prompt.customerCode"/>
                </td>
				<logic:equal name="invFindBuildUnbuildAssemblyForm" property="useCustomerPulldown" value="true">
		        <td width="390" height="25" class="control" colspan="3">
	               <html:select property="customerCode" styleClass="combo">
                      <html:options property="customerCodeList"/>
                   </html:select>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
				<logic:equal name="invFindBuildUnbuildAssemblyForm" property="useCustomerPulldown" value="false">
		        <td width="390" height="25" class="control" colspan="3">
	               <html:text property="customerCode" size="15" maxlength="25" styleClass="text" readonly="true"/>
                   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showArCstLookup('customerCode', '', '');"/>
		        </td>
				</logic:equal>
             </tr>
             
	         <tr>
		        <td class="prompt" width="180" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.receiving"/>
                </td>
		        <td width="120" height="25" class="control">
	               <html:checkbox property="receiving"/>
		        </td>
		      
             </tr>
             
             <tr>
             	<td class="prompt" width="180" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.numberOfCopies"/>
                </td>
		        <td width="148" height="25" class="control">
	               <html:text property="numberOfCopies" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
             
             </tr>
			<tr>
		        <td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.dateFrom"/>
                </td>
		        <td width="148" height="25" class="control">
	               <html:text property="dateFrom" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.dateTo"/>
                </td>
		        <td width="147" height="25" class="control">
	               <html:text property="dateTo" size="10" maxlength="10" styleClass="text"/>                                       
		        </td>		        
	         </tr> 
             <tr>
		        <td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.documentNumberFrom"/>
                </td>
		        <td width="148" height="25" class="control">
	               <html:text property="documentNumberFrom" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>
		        <td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.documentNumberTo"/>
                </td>
		        <td width="147" height="25" class="control">
	               <html:text property="documentNumberTo" size="15" maxlength="25" styleClass="text"/>                                       
		        </td>		        
	         </tr>         		        	         
             <tr>
				<td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.referenceNumber"/>
                </td>
		        <td width="148" height="25" class="control">
	               <html:text property="referenceNumber" size="25" maxlength="25" styleClass="text"/>                                       
		        </td>
		       <td class="prompt" width="140" height="25">
		          <bean:message key="findBuildUnbuildAssembly.prompt.posted"/>
		       </td>
		       <td width="147" height="25" class="control">
		          <html:select property="posted" styleClass="combo">
			        <html:options property="postedList"/>
			      </html:select>
		       </td>	        
	         </tr>
	         <tr>
				<td class="prompt" width="140" height="25">
                   <bean:message key="findBuildUnbuildAssembly.prompt.approvalStatus"/>
                </td>
		        <td width="148" height="25" class="control">
	               <html:select property="approvalStatus" styleClass="combo">
	                  <html:options property="approvalStatusList"/>
		           </html:select>
		        </td>
		       <td class="prompt" width="140" height="25">
		          <bean:message key="findBuildUnbuildAssembly.prompt.orderBy"/>
		       </td>
		       <td width="147" height="25" class="control">
		          <html:select property="orderBy" styleClass="combo">
		             <html:options property="orderByList"/>
			      </html:select>
		       </td>	         
	        </tr>	         
	       <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="290" height="50">
			             <div id="buttons">
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableFirstButton" value="false">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableFirstButton" value="true">
			             <html:submit property="firstButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.first"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disablePreviousButton" value="false">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disablePreviousButton" value="true">
			             <html:submit property="previousButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.previous"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableNextButton" value="false">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableNextButton" value="true">
					     <html:submit property="nextButton" styleClass="mainButtonSmall" disabled="true">
					         <bean:message key="button.next"/>
					     </html:submit>
					     </logic:equal>
					     <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableLastButton" value="false">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="disableLastButton" value="true">
			             <html:submit property="lastButton" styleClass="mainButtonSmall" disabled="true">
			             	<bean:message key="button.last"/>
			             </html:submit>
			             </logic:equal>
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="285" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <html:submit property="goButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.go"/>
				         </html:submit>
				         <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				         <bean:message key="button.close"/>
				         </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>                 	                                 
	         <tr valign="top">
	            <td width="575" height="185" colspan="4">
		        <div align="center">
		           <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			          bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			          bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
		              <tr>
                         <td width="575" height="1" colspan="7" class="gridTitle" 
			                bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                           <bean:message key="findBuildUnbuildAssembly.gridTitle.FBADetails"/>
	                     </td>
	                  </tr>
	            <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.date"/>
			       </td>
				   <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.documentNumber"/>
			       </td>
			       <td width="90" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.referenceNumber"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.quantity"/>
			       </td>
			       <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.received"/>
			       </td>
			       <td width="90" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="findBuildUnbuildAssembly.prompt.customer"/>
			       </td>		       			       			       
                </tr>			   	                  
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="invFBAList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			             <td width="90" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
					     <td width="90" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			                <nested:hidden property="documentNumber"/>
			             </td>
			             <td width="90" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>	
			             <td width="110" height="1" class="gridLabel">
			                <nested:write property="quantity"/>
			             </td>
			             <td width="110" height="1" class="gridLabel">
			                <nested:write property="received"/>
			             </td>
			             
			             <td width="110" height="1" class="gridLabel">
			                <nested:write property="customerCode"/>
			                <nested:hidden property="customerCode"/>
			             </td>		             		             			             
			             <td width="180" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="copyButton" styleClass="gridButton" onclick="return confirmCopy();">
	                       <bean:message key="button.copy"/>
  	                    	</nested:submit>
  	                    	
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        
  	                        </div>
  	                        <div id="buttonsDisabled" style="display: none;">
  	                        <nested:submit property="copyButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.copy"/>
  	                        </nested:submit>
  	                        
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        
  	                        
  	               
  	                        
			             </td>
			          </tr>
			          </nested:iterate>
                      </logic:equal>
                      <logic:equal name="invFindBuildUnbuildAssemblyForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			          
			          <%
			             int i = 0;	
			             String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			             String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			             String rowBgc = null;
			          %>
			          <nested:iterate property="invFBAList">
			          <%
			             i++;
			             if((i % 2) != 0){
			                rowBgc = ROW_BGC1;
			             }else{
			                rowBgc = ROW_BGC2;
			             }  
			          %>
			          <tr bgcolor="<%= rowBgc %>">
			          <td width="175" height="1" class="gridHeader">
			            	<bean:message key="findPurchaseOrder.prompt.supplierCode"/>
			           	</td>
			           	<td width="570" height="1" class="gridLabel" colspan="2">
			            	<nested:write property="supplierCode"/>
			           	</td>
			           
			             <td width="180" align="center" height="1">
			                <div id="buttons">
			                <nested:submit property="copyButton" styleClass="gridButton" onclick="return confirmCopy();">
	                       <bean:message key="button.copy"/>
  	                    	</nested:submit>
  	                        
			                <nested:submit property="openButton" styleClass="gridButton">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
  	                        
  	                        <div id="buttonsDisabled" style="display: none;">
  	                        
  	                        <nested:submit property="copyButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.copy"/>
  	                        </nested:submit>
  	                        
			                <nested:submit property="openButton" styleClass="gridButton" disabled="true">
	                           <bean:message key="button.open"/>
  	                        </nested:submit>
  	                        </div>
			             </td>
			          </tr>
					  <tr bgcolor="<%= rowBgc %>">
					  
					    <td width="90" height="1" class="gridHeader">
			                <bean:message key="findBuildUnbuildAssembly.prompt.date"/>
			             </td>
			             <td width="90" height="1" class="gridLabel">
			                <nested:write property="date"/>
			             </td>
			             
			             <td width="90" height="1" class="gridHeader">
			                <bean:message key="findBuildUnbuildAssembly.prompt.documentNumber"/>
			             </td>
			             <td width="90" height="1" class="gridLabel">
			                <nested:write property="documentNumber"/>
			                <nested:hidden property="documentNumber"/>
			             </td>
			             <td width="90" height="1" class="gridHeader">
			                <bean:message key="findBuildUnbuildAssembly.prompt.referenceNumber"/>
			             </td>
			             <td width="90" height="1" class="gridLabel">
			                <nested:write property="referenceNumber"/>
			             </td>
			          
			             
			             
                      </tr>			    
				    
			          </nested:iterate>			          
			          </logic:equal>
	               </table>
		        </div>
		        </td>
	         </tr>
	         <tr>
	            <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		        </td>
	         </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["referenceNumber"] != null &&
        document.forms[0].elements["referenceNumber"].disabled == false)
        document.forms[0].elements["referenceNumber"].focus()
   // -->
</script>
</body>
</html>
