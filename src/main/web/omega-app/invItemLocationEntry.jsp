<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="itemLocationEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers


function submitForm()
{

      disableButtons();
      enableInputControls();

}

function confirmGenerateItemLocation()
{
   if(confirm("Are you sure you want to Save and Generate Item Location?")) return true;
      else return false;
}

function confirmRecalculation()
{
   if(confirm("Are you sure you want to recalculate reorder point and reorder quantity?")) return true;
      else return false;
}

function allocateAccounts(name) {

	  var property = name.substring(0,name.indexOf("."));

	  if (document.forms[0].elements[property + ".branchCheckbox"].checked == true &&
	  		document.forms[0].elements[property + ".coaSegment"].value != "") {
	  	  var coaSegment = document.forms[0].elements[property + ".coaSegment"].value;
	  	  var coaSegments = coaSegment.split("-");


		  	if (document.forms[0].elements("inventoryAccount").value != "") {
		  	  var inventoryAccounts = (document.forms[0].elements("inventoryAccount").value).split("-");
		  	  var inventoryAccount = "";
		  	  for (var i = 0; i < inventoryAccounts.length; i++) {

		  	    if (i < coaSegments.length) {
		  	    	inventoryAccount = inventoryAccount + coaSegments[i];
		  	    } else {
		  	  	  	inventoryAccount = inventoryAccount + inventoryAccounts[i];
		  	  	}

		  	  	if (i < inventoryAccounts.length - 1) inventoryAccount = inventoryAccount + "-";

		  	  	document.forms[0].elements(property + ".bilInventoryAccount").value = inventoryAccount;
		  	  	document.forms[0].elements(property + ".bilInventoryAccountDescription").value = "";


		  	  }
		  	}

		  	if (document.forms[0].elements("salesAccount").value != "") {

		  	  if (document.forms[0].elements(property + ".type").value == "WAREHOUSE") {

		  	    document.forms[0].elements(property + ".bilSalesAccount").value = document.forms[0].elements(property + ".bilInventoryAccount").value;
		  	    document.forms[0].elements(property + ".bilSalesAccountDescription").value = "";

		  	  } else {

			  	  var salesAccounts = (document.forms[0].elements("salesAccount").value).split("-");
			  	  var salesAccount = "";
			  	  for (var i = 0; i < salesAccounts.length; i++) {

			  	    if (i < coaSegments.length) {
			  	    	salesAccount = salesAccount + coaSegments[i];
			  	    } else {
			  	  	  	salesAccount = salesAccount + salesAccounts[i];
			  	  	}

			  	  	if (i < salesAccounts.length - 1) salesAccount = salesAccount + "-";

			  	  	document.forms[0].elements(property + ".bilSalesAccount").value = salesAccount;
			  	  	document.forms[0].elements(property + ".bilSalesAccountDescription").value = "";
			   }


		  	  }
		  	}


		  	if (document.forms[0].elements("salesReturnAccount").value != "") {

			  	  if (document.forms[0].elements(property + ".type").value == "WAREHOUSE") {

			  	    document.forms[0].elements(property + ".bilSalesReturnAccount").value = document.forms[0].elements(property + ".bilInventoryAccount").value;
			  	    document.forms[0].elements(property + ".bilSalesReturnAccountDescription").value = "";

			  	  } else {

				  	  var salesReturnAccounts = (document.forms[0].elements("salesReturnAccount").value).split("-");
				  	  var salesReturnAccount = "";
				  	  for (var i = 0; i < salesReturnAccounts.length; i++) {

				  	    if (i < coaSegments.length) {
				  	    	salesReturnAccount = salesReturnAccount + coaSegments[i];
				  	    } else {
				  	  	  	salesReturnAccount = salesReturnAccount + salesReturnAccounts[i];
				  	  	}

				  	  	if (i < salesReturnAccounts.length - 1) salesReturnAccount = salesReturnAccount + "-";

				  	  	document.forms[0].elements(property + ".bilSalesReturnAccount").value = salesReturnAccount;
				  	  	document.forms[0].elements(property + ".bilSalesReturnAccountDescription").value = "";
				   }


			  	  }
			  	}

		  	if (document.forms[0].elements("costOfSalesAccount").value != "") {

		  	  if (document.forms[0].elements(property + ".type").value == "WAREHOUSE") {

		  	    document.forms[0].elements(property + ".bilCostOfSalesAccount").value = document.forms[0].elements(property + ".bilInventoryAccount").value;
		  	    document.forms[0].elements(property + ".bilCostOfSalesAccountDescription").value = "";


		  	  } else {
			  	  var costOfSalesAccounts = (document.forms[0].elements("costOfSalesAccount").value).split("-");
			  	  var costOfSalesAccount = "";
			  	  for (var i = 0; i < costOfSalesAccounts.length; i++) {

			  	    if (i < coaSegments.length) {
			  	    	costOfSalesAccount = costOfSalesAccount + coaSegments[i];
			  	    } else {
			  	  	  	costOfSalesAccount = costOfSalesAccount + costOfSalesAccounts[i];
			  	  	}

			  	  	if (i < costOfSalesAccounts.length - 1) costOfSalesAccount = costOfSalesAccount + "-";

			  	  	document.forms[0].elements(property + ".bilCostOfSalesAccount").value = costOfSalesAccount;
			  	  	document.forms[0].elements(property + ".bilCostOfSalesAccountDescription").value = "";
			  	}


		  	  }
		  	}

		  	if (document.forms[0].elements("wipAccount").value != "") {
		  	  var wipAccounts = (document.forms[0].elements("wipAccount").value).split("-");
		  	  var wipAccount = "";
		  	  for (var i = 0; i < wipAccounts.length; i++) {

		  	    if (i < coaSegments.length) {
		  	    	wipAccount = wipAccount + coaSegments[i];
		  	    } else {
		  	  	  	wipAccount = wipAccount + wipAccounts[i];
		  	  	}

		  	  	if (i < wipAccounts.length - 1) wipAccount = wipAccount + "-";

		  	  	document.forms[0].elements(property + ".bilWipAccount").value = wipAccount;
		  	  	document.forms[0].elements(property + ".bilWipAccountDescription").value = "";


		  	  }
		  	}

		  	if (document.forms[0].elements("accruedInventoryAccount").value != "") {
		  	  var accruedInventoryAccounts = (document.forms[0].elements("accruedInventoryAccount").value).split("-");
		  	  var accruedInventoryAccount = "";
		  	  for (var i = 0; i < accruedInventoryAccounts.length; i++) {

		  	    if (i < coaSegments.length) {
		  	    	accruedInventoryAccount = accruedInventoryAccount + coaSegments[i];
		  	    } else {
		  	  	  	accruedInventoryAccount = accruedInventoryAccount + accruedInventoryAccounts[i];
		  	  	}

		  	  	if (i < accruedInventoryAccounts.length - 1) accruedInventoryAccount = accruedInventoryAccount + "-";

		  	  	document.forms[0].elements(property + ".bilAccruedInventoryAccount").value = accruedInventoryAccount;
		  	  	document.forms[0].elements(property + ".bilAccruedInventoryAccountDescription").value = "";


		  	  }
		  	}
	  } else {

	  	  document.forms[0].elements(property + ".bilSalesAccount").value = "";
		  document.forms[0].elements(property + ".bilSalesAccountDescription").value = "";
		  document.forms[0].elements(property + ".bilSalesReturnAccount").value = "";
		  document.forms[0].elements(property + ".bilSalesReturnAccountDescription").value = "";
	  	  document.forms[0].elements(property + ".bilInventoryAccount").value = "";
		  document.forms[0].elements(property + ".bilInventoryAccountDescription").value = "";
	  	  document.forms[0].elements(property + ".bilCostOfSalesAccount").value = "";
		  document.forms[0].elements(property + ".bilCostOfSalesAccountDescription").value = "";
	  	  document.forms[0].elements(property + ".bilWipAccount").value = "";
		  document.forms[0].elements(property + ".bilWipAccountDescription").value = "";
	  	  document.forms[0].elements(property + ".bilAccruedInventoryAccount").value = "";
		  document.forms[0].elements(property + ".bilAccruedInventoryAccountDescription").value = "";
	  }

}


//Done Hiding-->
</script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invItemLocationEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %>
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="600">
      <tr valign="top">
        <td width="187" height="510"></td>
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="itemLocationEntry.title"/>
		 </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   		<logic:equal name="invItemLocationEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
            	</logic:equal>
		   <html:errors/>
	        </td>
	     </tr>

	     <tr>
	     	<td width="575" height="10" colspan="4">

				<!--FULL_ACCESS-->

				<logic:equal name="invItemLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">

				<div class="tabber">

				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				<tr>
					<td class="prompt" width="575" height="25" colspan="4">
					</td>
				</tr>

				<tr>
					 <td class="prompt" width="140" height="25">
						<bean:message key="itemLocationEntry.prompt.itemClass"/>
					 </td>
					 <td width="147" height="25" class="control">
						<html:select property="itemClass" styleClass="combo">
						   <html:options property="itemClassList"/>
						</html:select>
					 </td>
				</tr>
				<logic:equal name="invItemLocationEntryForm" property="addByItemList" value="false">
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.categoryName"/>
				 </td>
				 <td width="148" height="25" class="control">
					<html:select property="categoryNameSelectedList" size="6" multiple="true" styleClass="comboRequired" style="width:230;">
					   <html:options property="categoryNameList"/>
					</html:select>
				 </td>

				</tr>

				</logic:equal>
				<logic:equal name="invItemLocationEntryForm" property="addByItemList" value="true">



				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.itemName"/>
				 </td>


				    <input type="hidden" name="findItem.itemName" value="false">
				    <input type="hidden" name="findItem.itemDescription" value="">
				    <input type="hidden" name="findItem.location" value="">
				    <input type="hidden" name="findItem.isItemEntered" value="false">

				 <td width="10" height="25" class="control" colspan="3">
					<html:select property="itemNameSelectedList" size="6" multiple="true" styleClass="comboRequired" style="width:450;height:300;">
					   <nested:options labelProperty="itemDescriptionList" property="itemNameList"/>
					</html:select>
				 </td>
				 <logic:equal name="invItemLocationEntryForm" property="itemShowAll" value="false">
				 		<td width="270" height="1" class="control">
					   		<html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid('findItem.', 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				 		</td>
                 </logic:equal>
				</tr>
				</logic:equal>

				<!-- <td width="187" height="5"></td> -->
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.locationName"/>
				 </td>
				 <td width="147" height="25" class="control">
					<html:select property="locationNameSelectedList" size="6" multiple="true" styleClass="comboRequired" style="width:200;">
					   <html:options property="locationNameList"/>
					</html:select>
				 </td>
				</tr>

				<td width="187" height="5"></td>

				<tr>
				<td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.subjectToCommission"/>
				</td>
				<td height="25" class="control">
					<html:checkbox property="subjectToCommission"/>
				</td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.salesAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="salesAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('salesAccount','salesAccountDescription');"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.salesAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="salesAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.inventoryAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="inventoryAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('inventoryAccount','inventoryAccountDescription');"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.inventoryAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="inventoryAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.costOfSalesAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="costOfSalesAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('costOfSalesAccount','costOfSalesAccountDescription');"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.costOfSalesAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="costOfSalesAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.wipAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="wipAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('wipAccount','wipAccountDescription');"/>
				 </td>
				</tr>

				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.wipAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="wipAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>

				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.accruedInventoryAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="accruedInventoryAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('accruedInventoryAccount','accruedInventoryAccountDescription');"/>
				 </td>
				</tr>

				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.accruedInventoryAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="accruedInventoryAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>


				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.salesReturnAccount"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="salesReturnAccount" size="30" maxlength="255" styleClass="textRequired"/>
					<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('salesReturnAccount','salesReturnAccountDescription');"/>
				 </td>
				</tr>
				<tr>
				 <td class="prompt" width="140" height="25">
					<bean:message key="itemLocationEntry.prompt.salesReturnAccountDescription"/>
				 </td>
				 <td width="435" height="25" class="control" colspan="3">
					<html:text property="salesReturnAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				 </td>
				</tr>
				</table>
				</div>

				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

				<tr>
				<td class="prompt" width="575" height="25" colspan="4"></td>
				</tr>

				<tr>
				<td class="prompt" width="250" height="25">
				<bean:message key="itemLocationEntry.prompt.reorderPoint"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="reorderPoint" size="10" maxlength="15" styleClass="text"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="250" height="25">
				<bean:message key="itemLocationEntry.prompt.reorderQuantity"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="reorderQuantity" size="10" maxlength="15" styleClass="text"/>
				</td>
				</tr>
  			    </table>
				</div>

				<div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				<tr>
				<nested:iterate property="invBrIlList">
							<nested:hidden property="coaSegment"/>
							<nested:hidden property="type"/>
					<tr>
						<td class="prompt" width="575" height="25" colspan="4">
						</td>
					</tr>
					<tr>
						<td width="30" height="25" class="control">
							<nested:checkbox property="branchCheckbox" onclick="allocateAccounts(name);"/>
						</td>
						<td class="prompt" width="540" height="25" colspan="3">
							<nested:write property="branchName"/>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.rack"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilRack" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.bin"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilBin" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.reorderPoint"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilReorderPoint" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.reorderQuantity"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilReorderQuantity" size="25" maxlength="25" styleClass="text"/>
						</td>
					</tr>
					 <tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<bean:message key="standardMemoLine.prompt.subjectToCommission"/>
						</td>
						<td height="25" class="control">
							<nested:checkbox property="bilSubjectToCommission"/>
						</td>
					 </tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.salesAccount"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilSalesAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilSalesAccount','bilSalesAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.salesAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilSalesAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.inventoryAccount"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilInventoryAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilInventoryAccount','bilInventoryAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.inventoryAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilInventoryAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.costOfSalesAccount"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilCostOfSalesAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilCostOfSalesAccount','bilCostOfSalesAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.costOfSalesAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilCostOfSalesAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.wipAccount"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilWipAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilWipAccount','bilWipAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.wipAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilWipAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.accruedInventoryAccount"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilAccruedInventoryAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilAccruedInventoryAccount','bilAccruedInventoryAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.accruedInventoryAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilAccruedInventoryAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>


					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.salesReturnAccount"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilSalesReturnAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilSalesReturnAccount','bilSalesReturnAccountDescription');"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.salesReturnAccountDescription"/>
						</td>
						<td width="420" height="25" class="control" colspan="3">
							<nested:text property="bilSalesReturnAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
						</td>
					</tr>

					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.hist1Sales"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilHist1Sales" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.hist2Sales"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilHist2Sales" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.projectedSales"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilProjectedSales" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.deliveryTime"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilDeliveryTime" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.deliveryBuffer"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilDeliveryBuffer" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
							<nested:message key="itemLocationEntry.prompt.orderPerYear"/>
						</td>
						<td width="425" height="25" class="control" colspan="3">
							<nested:text property="bilOrderPerYear" size="25" maxlength="25" styleClass="textAmount"/>
						</td>
					</tr>
				</nested:iterate>
				</tr>
				</table>
				</div>

				</div><script>tabberAutomatic(tabberOptions)</script>

				</logic:equal>



				<!--QUERY_ONLY-->

				<logic:equal name="invItemLocationEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">

				<div class="tabber">

				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

				<tr>
				<td class="prompt" width="575" height="25" colspan="4">
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.categoryName"/>
				</td>
				<td width="148" height="25" class="control">
				<html:select property="categoryName" styleClass="combo" disabled="true">
				<html:options property="categoryNameList"/>
				</html:select>
				</td>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.itemClass"/>
				</td>
				<td width="147" height="25" class="control">
				<html:select property="itemClass" styleClass="combo" disabled="true">
				<html:options property="itemClassList"/>
				</html:select>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.itemName"/>
				</td>
				<td width="148" height="25" class="control" colspan="3">
				<html:select property="itemNameSelectedList" size="5" multiple="true" styleClass="comboRequired" disabled="true">
				<html:options labelProperty="itemDescriptionList" property="itemNameList"/>
				</html:select>
				</td>
				</tr>

				<td width="187" height="5"></td>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.locationName"/>
				</td>
				<td width="147" height="25" class="control">
				<html:select property="locationNameSelectedList" size="5" multiple="true" styleClass="comboRequired" disabled=
				"true">
				<html:options property="locationNameList"/>
				</html:select>
				</td>
				</tr>

				<td width="187" height="5"></td>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.subjectToCommission"/>
				</td>
				<td height="25" class="control">
				<html:checkbox property="subjectToCommission" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.salesAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="salesAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('salesAccount','salesAccountDescription');" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.salesAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="salesAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text"
				disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.inventoryAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="inventoryAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('inventoryAccount','inventoryAccountDescription');" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.inventoryAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="inventoryAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass=
				"text" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.costOfSalesAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="costOfSalesAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('costOfSalesAccount','costOfSalesAccountDescription');" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.costOfSalesAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="costOfSalesAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass=
				"text" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.wipAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="wipAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('wipAccount','wipAccountDescription');" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.wipAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="wipAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text"
				disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.accruedInventoryAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="accruedInventoryAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"
				/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('accruedInventoryAccount','accruedInventoryAccountDescription');" disabled="true"/>	            	</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.accruedInventoryAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="accruedInventoryAccountDescription" size="75" maxlength="255" style="font-size:8pt;"
				styleClass="text" disabled="true"/>
				</td>
				</tr>


				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.salesReturnAccount"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="salesReturnAccount" size="30" maxlength="255" styleClass="textRequired" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return
				showGlCoaLookupTxn('salesReturnAccount','salesReturnAccountDescription');" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="140" height="25">
				<bean:message key="itemLocationEntry.prompt.salesReturnAccountDescription"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="salesReturnAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text"
				disabled="true"/>
				</td>
				</tr>

				</table>
				</div>

				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">

				<tr>
				<td class="prompt" width="575" height="25" colspan="4"></td>
				</tr>

				<tr>
				<td class="prompt" width="250" height="25">
				<bean:message key="itemLocationEntry.prompt.reorderPoint"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="reorderPoint" size="10" maxlength="15" styleClass="text" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td class="prompt" width="250" height="25">
				<bean:message key="itemLocationEntry.prompt.reorderQuantity"/>
				</td>
				<td width="435" height="25" class="control" colspan="3">
				<html:text property="reorderQuantity" size="10" maxlength="15" styleClass="text" disabled="true"/>
				</td>
				</tr>
				</table>
				</div>

				<div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
				<tr>
				<nested:iterate property="invBrIlList">
				<tr>
				<td class="prompt" width="575" height="25" colspan="4">
				</td>
				</tr>
				<tr>
				<td width="30" height="25" class="control">
				<nested:checkbox property="branchCheckbox" disabled="true"/>
				</td>
				<td class="prompt" width="540" height="25" colspan="3">
				<nested:write property="branchName"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.reorderPoint"/>
				</td>
				<td width="425" height="25" class="control" colspan="3">
				<nested:text property="bilReorderPoint" size="25" maxlength="25" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.reorderQuantity"/>
				</td>
				<td width="425" height="25" class="control" colspan="3">
				<nested:text property="bilReorderQuantity" size="25" maxlength="25" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
				<bean:message key="itemLocationEntry.prompt.subjectToCommission"/>
				</td>
				<td height="25" class="control">
				<nested:checkbox property="bilSubjectToCommission" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.salesAccount"/>
				</td>
				<td width="425" height="25" class="control" colspan="3">
				<nested:text property="bilSalesAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilSalesAccount','bilSalesAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.salesAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilSalesAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.inventoryAccount"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilInventoryAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilInventoryAccount','bilInventoryAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.inventoryAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilInventoryAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.costOfSalesAccount"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilCostOfSalesAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilCostOfSalesAccount','bilCostOfSalesAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.costOfSalesAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilCostOfSalesAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.wipAccount"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilWipAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilWipAccount','bilWipAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.wipAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilWipAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.accruedInventoryAccount"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilAccruedInventoryAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilAccruedInventoryAccount','bilAccruedInventoryAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.accruedInventoryAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilAccruedInventoryAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>

				<tr>
				<td></td>
				<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.salesReturnAccount"/>
				</td>
				<td width="425" height="25" class="control" colspan="3">
				<nested:text property="bilSalesReturnAccount" size="30" maxlength="255" styleClass="text" disabled="true"/>
				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'bilSalesReturnAccount','bilSalesReturnAccountDescription');" disabled="true"/>
				</td>
				</tr>
				<tr>
				<td></td>
				<td class="prompt" width="155" height="25" style="text-indent:25.000000pt;">
				<nested:message key="itemLocationEntry.prompt.salesReturnAccountDescription"/>
				</td>
				<td width="420" height="25" class="control" colspan="3">
				<nested:text property="bilSalesReturnAccountDescription" size="65" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
				</td>
				</tr>

				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.hist1Sales"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilHist1Sales" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.hist2Sales"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilHist2Sales" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.projectedSales"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilProjectedSales" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.deliveryTime"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilDeliveryTime" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.deliveryBuffer"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilDeliveryBuffer" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="prompt" width="150" height="25" style="text-indent:25.000000pt;">
						<nested:message key="itemLocationEntry.prompt.orderPerYear"/>
					</td>
					<td width="425" height="25" class="control" colspan="3">
						<nested:text property="bilOrderPerYear" size="25" maxlength="25" styleClass="textAmount" disabled="true"/>
					</td>
				</tr>
				</nested:iterate>
				</tr>
				</table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			    </logic:equal>
			</td>
		 </tr>
	     <tr>
	        <td width="575" height="50" colspan="4">
				<div id="buttons">
					<p align="right">
					<logic:equal name="invItemLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
					<html:submit property="saveButton" styleClass="mainButton" onclick="return confirmGenerateItemLocation();">
					<bean:message key="button.save"/>
					</html:submit>
					<html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
					<bean:message key="button.delete"/>
					</html:submit>
					<html:submit property="recalcButton" styleClass="mainButton" onclick="return confirmRecalculation();">
					<bean:message key="button.recalc"/>
					</html:submit>
					</logic:equal>
					<html:submit property="closeButton" styleClass="mainButton">
					<bean:message key="button.close"/>
					</html:submit>
				</div>

				<div id="buttonsDisabled" style="display: none;">
					<p align="right">
					<logic:equal name="invItemLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
					<html:submit property="saveButton" styleClass="mainButton" disabled="true">
					<bean:message key="button.save"/>
					</html:submit>
					<html:submit property="deleteButton" styleClass="mainButton" disabled="true">
					<bean:message key="button.delete"/>
					</html:submit>
					<html:submit property="recalcButton" styleClass="mainButton" disabled="true">
					<bean:message key="button.recalc"/>
					</html:submit>
					</logic:equal>
					<html:submit property="closeButton" styleClass="mainButton" disabled="true">
					<bean:message key="button.close"/>
					</html:submit>
				</div>
	        </td>
	     </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  </td>
	       </tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>

<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["locationName"] != null &&
        document.forms[0].elements["locationName"].disabled == false)
        document.forms[0].elements["locationName"].focus()
   // -->
</script>

</body>
</html>
