<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="lineItemTemplate.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/invLineItemTemplate.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
      	<td width="187" height="510"></td> 
        <td width="581" height="510">
       	  <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     	<tr>
	        	<td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   			<bean:message key="lineItemTemplate.title"/>
				</td>
	     	</tr>
            <tr>
	        	<td width="575" height="44" colspan="4" class="statusBar">
		   			<logic:equal name="invLineItemTemplateForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                    <bean:message key="app.success"/>
                   	</logic:equal>
		   			<html:errors/>	
	        	</td>
	     	</tr>

			<tr>
	         	<td width="575">
			    	<div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25"> 
						<tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			           	</tr>
	     				<tr>
	        				<td class="prompt" width="140" height="25">
	           					<bean:message key="lineItemTemplate.prompt.templateName"/>
	        				</td>
	        				<td width="435" height="25" class="control" colspan="3">
	           					<html:text property="templateName" size="25" maxlength="25" styleClass="textRequired"/>
	        				</td>
	       	 			</tr>
	      				<tr>
		    				<td class="prompt" width="140" height="25">
               					<bean:message key="lineItemTemplate.prompt.description"/>
            				</td>
		    				<td width="435" height="25" class="control" colspan="3">
		       					<html:text property="description" size="50" maxlength="100" styleClass="text"/>
		    				</td>		    
		  				</tr>
					</table>		  
			  		</div>			    	    			    
                	<div class="tabbertab" title="Lines">
			       	<table border="1" cellpadding="0" cellspacing="0" width="555" height="25" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
					    <tr>
		                   	<td width="553" height="1" colspan="5" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
			                   	<bean:message key="lineItemTemplate.gridTitle.LIDetails"/>
			               	</td>
			            </tr>	              
					    <tr>
					       	<td width="33" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					           	<bean:message key="lineItemTemplate.prompt.line"/>
					       	</td>
					       	<td width="220" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					          	<bean:message key="lineItemTemplate.prompt.itemName"/>
					       	</td>
							<td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					           	<bean:message key="lineItemTemplate.prompt.location"/>
					       	</td>
							<td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					           	<bean:message key="lineItemTemplate.prompt.unit"/>
					       	</td>
							<td width="40" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					           	<bean:message key="lineItemTemplate.prompt.delete"/>
					       	</td>
		                </tr>
						<tr>
					       	<td width="33" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					        </td>
					       	<td width="520" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
					           	<bean:message key="lineItemTemplate.prompt.description"/>
					       	</td>
		                </tr>			                   	              	              
					    <%
					       	int i = 0;	
					       	String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
					       	String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
					       	String rowBgc = null;
					    %>
						<nested:iterate property="invLIList">
					    <%
					       	i++;
					       	if((i % 2) != 0){
					          	rowBgc = ROW_BGC1;
					       	}else{
					          	rowBgc = ROW_BGC2;
					       	}  
					    %>
						<nested:hidden property="isItemEntered" value=""/>
				        <tr bgcolor="<%= rowBgc %>">		
				       		<td width="33" height="1" class="control">
				        		<nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       		</td>				    								    				       	       
				       		<td width="220" height="1" class="control">
				        		<nested:text property="itemName" size="25" maxlength="25" styleClass="textRequired"/>
								<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       		</td>
				       		<td width="140" height="1" class="control">
				        		<nested:select property="location" styleClass="comboRequired" style="width:115;">
				         			<nested:options property="locationList"/>				          				          
				        		</nested:select>
				       		</td>								   
				       		<td width="120" height="1" class="control">
                        		<nested:select property="unit" styleClass="comboRequired" style="width:95;">
                            		<nested:options property="unitList"/>                                               
                          		</nested:select>
                       		</td>			       
				       		<td width="40" height="1" class="control">
				          	<p align="center">				       
                          		<nested:checkbox property="deleteCheckbox"/>
                       		</td>    				       
				   		</tr>
						<tr bgcolor="<%= rowBgc %>">
				       		<td width="33" height="1" class="control"/>				   
				       		<td width="520" height="1" class="control" colspan="4">
				        		<nested:text property="itemDescription" size="95" maxlength="135" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       		</td>				                                     
				    	</tr>
			            </nested:iterate>
					</table>
				   	</div>
              		</div><script>tabberAutomatic(tabberOptions)</script> 
		     	</td>
         	</tr> 
			

			<tr>
	         	<td width="575" height="50" colspan="4">
	         	<table border="0" cellpadding="0" cellspacing="0" width="575" height="50" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     	<tr>
		         		<td width="384" height="50" colspan="2"> 
		           		<div id="buttons">
		           		<p align="left">
		           		<logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       		<html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            		<bean:message key="button.addLines"/>
			       			</html:submit>			   
			       			<html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            		<bean:message key="button.deleteLines"/>
			       			</html:submit>
			       		</logic:equal>
		           		</div>
		           		<div id="buttonsDisabled" style="display: none;">
		           		<p align="right">
		           		<logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				        	<html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            		<bean:message key="button.addLines"/>
			       			</html:submit>
			       			<html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            		<bean:message key="button.deleteLines"/>
			       			</html:submit>			   
			       		</logic:equal>
		           		</div>
				  		</td>
	     				<td width="384" height="50" colspan="2">
			        	<div id="buttons">
			            <p align="right">
				       	<logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	<logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				        	<html:submit property="saveButton" styleClass="mainButton">
				        		<bean:message key="button.save"/>
				          	</html:submit>
				       	</logic:equal>
				       	<logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				       	</logic:equal>
				        </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				        </div>
				        <div id="buttonsDisabled" style="display: none;">
			            <p align="right">
				        <logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				        <logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				        	<html:submit property="saveButton" styleClass="mainButton" disabled="true">
				            	<bean:message key="button.save"/>
				            </html:submit>
				        </logic:equal>
				        <logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				        </logic:equal>
				        </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				        </div>		         
				   		</td>
					</tr>
	         	</table>
	         	</td>        
	     	</tr>

			<tr valign="top">
	         	<td width="575" height="185" colspan="6">
		    	<div align="center">
		       	<table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			  		bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			  		bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			  
			  		<tr>
                    	<td width="575" height="1" colspan="3" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                        	<bean:message key="lineItemTemplate.gridTitle.LITDetails"/>
	                   	</td>
	               	</tr>
	                  
	            	<tr>
			       		<td width="140" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="lineItemTemplate.prompt.templateName"/>
			       		</td>
			       		<td width="435" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          		<bean:message key="lineItemTemplate.prompt.description"/>
			       		</td>
                	</tr>			     	                  
			  		<%
			     		int j = 0;	
			     		ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
	                    ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
		             	rowBgc = null;
	              	%>
	             	<nested:iterate property="invLITList">
			  	 	<%
			     		j++;
			     		if((j % 2) != 0){
			         		rowBgc = ROW_BGC1;
		             	}else{
			         		rowBgc = ROW_BGC2;
		             	}  
			    	%>
			     	<tr bgcolor="<%= rowBgc %>">
			        	<td width="140" height="1" class="gridLabel">
			           		<nested:write property="templateName"/>
			        	</td>
			        	<td width="286" height="1" class="gridLabel">
			           		<nested:write property="description"/>
			        	</td>			        
			        	<td width="149" align="center" height="1">
			        	<div id="buttons">
			         	<logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            	<nested:submit property="editButton" styleClass="gridButton">
	                       		<bean:message key="button.edit"/>
  	                    	</nested:submit>
  	                 	<logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      		<nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     		<bean:message key="button.delete"/>
  	                  		</nested:submit>
  	                  	</logic:equal>				     
  	                  	</logic:equal>  
  	                 	</div>	                  				     				     
  	                 	<div id="buttonsDisabled" style="display: none;">
			         	<logic:equal name="invLineItemTemplateForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			            	<nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                       		<bean:message key="button.edit"/>
  	                    	</nested:submit>
  	                 	<logic:equal name="invLineItemTemplateForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				      		<nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     		<bean:message key="button.delete"/>
  	                  		</nested:submit>
  	                  	</logic:equal>				     
  	                  	</logic:equal>  
  	                 	</div>
			       		</td>
			     	</tr>
		          	</nested:iterate>
				</table>
				</div>
				</td>
			</tr>

	     	<tr>
	          	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		  		</td>
	       	</tr>
          </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["templateName"] != null &&
        document.forms[0].elements["templateName"].disabled == false)
        document.forms[0].elements["templateName"].focus()
   // -->
</script>
</body>
</html>
