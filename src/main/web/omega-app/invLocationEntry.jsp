<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="locationEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invLocationEntry.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="locationEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invLocationEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <logic:equal name="invLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.locationName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="locationName" size="43" maxlength="50" styleClass="textRequired"/>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.description"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="description" size="43" maxlength="50" styleClass="text"/>
	         </td>
         </tr>         
         <tr>
	         <td class="prompt" width="140" height="25">
                <bean:message key="locationEntry.prompt.address"/>
             </td>
		     <td width="147" height="25" class="control">
		        <html:textarea property="address" cols="20" rows="4" styleClass="text"/>
		     </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.type"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="type" styleClass="comboRequired">
			      <html:options property="typeList"/>
			   </html:select>
	         </td> 
         </tr>         
         <tr>
         	 <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.branch"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="branch" styleClass="combo">
			      <html:options property="branchList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.department"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="department" styleClass="combo">
			      <html:options property="departmentList"/>
			   </html:select>
	         </td> 
         </tr>    
         <tr>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.position"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="position" size="25" maxlength="50" styleClass="text"/>
	         </td>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.dateHired"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="dateHired" size="25" maxlength="15" styleClass="text"/>
	         </td>	         
         </tr>
	     <tr>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.contactPerson"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="contactPerson" size="25" maxlength="50" styleClass="text"/>
	         </td>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.contactNumber"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="contactNumber" size="25" maxlength="15" styleClass="text"/>
	         </td>	         
         </tr>    
	     <tr>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.emailAddress"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="emailAddress" size="25" maxlength="50" styleClass="text"/>
	         </td>
		     
	     </tr>
	     <tr>
	     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.employmentStatus"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="employmentStatus" size="25" maxlength="50" styleClass="text"/>
	         </td>  
	      </tr>
	     </logic:equal>
	     <logic:equal name="invLocationEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.locationName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="locationName" size="25" maxlength="25" styleClass="textRequired" disabled="true"/>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.description"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
	         </td>
         </tr>         
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.type"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:select property="type" styleClass="comboRequired" disabled="true">
			      <html:options property="typeList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
                <bean:message key="locationEntry.prompt.address"/>
             </td>
		     <td width="147" height="25" class="control">
		        <html:textarea property="address" cols="20" rows="4" styleClass="text" disabled="true"/>
		     </td> 
         </tr>          

	     <tr>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.contactPerson"/>
	         </td>
	         <td width="148" height="25" class="control">
	            <html:text property="contactPerson" size="25" maxlength="50" styleClass="text" disabled="true"/>
	         </td>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.contactNumber"/>
	         </td>
	         <td width="147" height="25" class="control">
	            <html:text property="contactNumber" size="25" maxlength="15" styleClass="text" disabled="true"/>
	         </td>	         
         </tr>    
	     
	     <tr>
		     <td class="prompt" width="140" height="25">
	            <bean:message key="locationEntry.prompt.emailAddress"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="emailAddress" size="25" maxlength="50" styleClass="text" disabled="true"/>
	         </td>
	     </tr>	     
	     </logic:equal>	                   	                   	                   	                   	                   	                             
	     <tr>
	        <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="invLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		        <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	              <bean:message key="button.delete"/>
	           </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="invLocationEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		        <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.delete"/>
	           </html:submit>
		       </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
            </td>
	     </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["locationName"] != null &&
        document.forms[0].elements["locationName"].disabled == false)
        document.forms[0].elements["locationName"].focus()
   // -->
</script>
</body>
</html>
