<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>

<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invOverheadEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function calculateAmount(name)
{
  
  var property = name.substring(0,name.indexOf("."));
  
  var quantity = 0;
  var unitCost = 0;
  var amount = 0;      
  
  if (document.forms[0].elements[property + ".quantity"].value != "" &&
      document.forms[0].elements[property + ".unitCost"].value != "") {  
        
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".quantity"].value))) {
	  
	  	quantity = (document.forms[0].elements[property + ".quantity"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {
	  
	  	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
	  
	  	amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');
	  	
	  }
	  
	  amount = (quantity * unitCost).toFixed(2);
	  
	  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());
	  
  }
   
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invOverheadEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        
        <!-- ===================================================================== -->
		<!--  Title Bar                                                            -->
		<!-- ===================================================================== -->
        
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invOverheadEntry.title"/>
		</td>
		
		<!-- ===================================================================== -->
        <!--  Status Msg                                                           -->
        <!-- ===================================================================== -->
		
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invOverheadEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	     
	     <!-- ===================================================================== -->
         <!--  Screen when enabled                                                  -->
         <!-- ===================================================================== -->
	     
	     <logic:equal name="invOverheadEntryForm" property="enableFields" value="true">	          
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
							<logic:equal name="invOverheadEntryForm" property="showShift" value="true">         
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.shift"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;">
                      			<html:options property="shiftList"/>
            	   				</html:select>
                			</td>
         					</logic:equal>	 
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>               
				    </table> 
					</div>		        		    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.posted"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>	
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invOverheadEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invOverheadEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>          
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invOverheadEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invOverheadEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invOverheadEntry.gridTitle.OHDetails"/>
		                </td>
		            </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (enabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invOverheadEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invOverheadEntry.prompt.overheadMemoLineName"/>
				       </td>
				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.quantity"/>
                       </td>
                       <td width="154" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.unit"/>
                       </td>                                              
                       <td width="270" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.unitCost"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>
				    <tr>
					    <td width="554" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    </td> 
	                    <td width="340" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="invOverheadEntry.prompt.amount"/>
	                    </td> 
					</tr>				       			    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invOHList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isOverheadMemoLineNameEntered" value=""/>
				    <nested:hidden property="isQuantityEntered" value=""/>				    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="250" height="1" class="control">
				          <nested:select property="overheadMemoLineName" styleClass="comboRequired" style="width:130;" onchange="return enterSelectGrid(name, 'overheadMemoLineName', 'isOverheadMemoLineNameEntered');">
				              <nested:options property="overheadMemoLineNameList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="100" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" onblur="calculateAmount(name);" onkeyup="calculateAmount(name);"/>
				       </td>
				       <td width="154" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:110;" onchange="return enterSelectGrid(name, 'unit','isQuantityEntered');">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="270" height="1" class="control" colspan="2">
                          <nested:text property="unitCost" size="8" maxlength="25" styleClass="textAmountRequired" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
                       </td>    		       
				       <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">	
					    <td width="554" height="1" class="gridHeader" colspan="4">
	                    </td> 
	                    <td width="340" height="1" class="gridHeader" colspan="3">
	                    	<nested:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
	                    </td> 
					</tr>	
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="invOverheadEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="invOverheadEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="invOverheadEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invOverheadEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>	
	     </logic:equal>
	     	     
	     <!-- ===================================================================== -->
         <!--  Screen when disabled                                                 -->
         <!-- ===================================================================== -->
	     
	     <logic:equal name="invOverheadEntryForm" property="enableFields" value="false">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
         				<tr>
							<logic:equal name="invOverheadEntryForm" property="showShift" value="true">         
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.shift"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:select property="shift" styleClass="comboRequired" style="width:130;" disabled="true">
                      			<html:options property="shiftList"/>
            	   				</html:select>
                			</td>
         					</logic:equal>	 
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="160" height="25" class="prompt">
                   				<bean:message key="invOverheadEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>               
				    </table> 
					</div>		        		    		        		    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.posted"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>	
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invOverheadEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
         <tr>
             <td width="575" height="50" colspan="4"> 
               <div id="buttons">
               <p align="right">
               <logic:equal name="invOverheadEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
               <logic:equal name="invOverheadEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>          
               <html:submit property="closeButton" styleClass="mainButton">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
               <div id="buttonsDisabled" style="display: none;">
               <p align="right">
               <logic:equal name="invOverheadEntryForm" property="showSaveButton" value="true">
               <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveSubmit"/>
               </html:submit>
               <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
                    <bean:message key="button.saveAsDraft"/>
               </html:submit>
               </logic:equal>
               <logic:equal name="invOverheadEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
               <html:submit property="closeButton" styleClass="mainButton" disabled="true">
                  <bean:message key="button.close"/>
               </html:submit>
               </div>
              </td>
         </tr>
         <tr valign="top">
                  <td width="575" height="185" colspan="4">
                  <div align="center">
                    <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                    <tr>
                    <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                            <bean:message key="invOverheadEntry.gridTitle.OHDetails"/>
                        </td>
                    </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (disabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invOverheadEntry.prompt.lineNumber"/>
				       </td>		            
		               <td width="250" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invOverheadEntry.prompt.overheadMemoLineName"/>
				       </td>
				       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.quantity"/>
                       </td>
                       <td width="154" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.unit"/>
                       </td>                                              
                       <td width="270" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.unitCost"/>
                       </td>
                       <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invOverheadEntry.prompt.delete"/>
                       </td>				       				       	                                             
				    </tr>
				    <tr>
					    <td width="554" height="1" class="gridHeader" colspan="4" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    </td> 
	                    <td width="340" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
	                    	<bean:message key="invOverheadEntry.prompt.amount"/>
	                    </td> 
					</tr>					       			    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invOHList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>			    
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="250" height="1" class="control">
				          <nested:select property="overheadMemoLineName" styleClass="comboRequired" style="width:130;" disabled="true">
				              <nested:options property="overheadMemoLineNameList"/>				          				          
				          </nested:select>
				       </td>								   
				       <td width="100" height="1" class="control">
				          <nested:text property="quantity" size="5" maxlength="10" styleClass="textRequired" disabled="true"/>
				       </td>
				       <td width="154" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:110;" disabled="true">
                              <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>			       
                       <td width="270" height="1" class="control" colspan="2">
                          <nested:text property="unitCost" size="8" maxlength="25" styleClass="textAmountRequired" disabled="true"/>
                       </td>    		       
                       <td width="70" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox" disabled="true"/>
                       </td>    				       
				   </tr>
				   <tr bgcolor="<%= rowBgc %>">
					    <td width="554" height="1" class="gridHeader" colspan="4">
	                    </td> 
	                    <td width="340" height="1" class="gridHeader" colspan="3">
	                    	<nested:text property="amount" size="15" maxlength="25" styleClass="textAmount" disabled="true"/>
	                    </td> 
					</tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
		       <tr>
		         <td width="575" height="25" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="invOverheadEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="invOverheadEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="invOverheadEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invOverheadEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     </tr>  
         </logic:equal>  
	     <tr>
	     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)        
        document.forms[0].elements["date"].focus();
	       // -->
</script>
</body>
</html>
