<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="overheadMemoLine.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
      
<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>


<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton', 'updateButton'));">
<html:form action="/invOverheadMemoLines.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="overheadMemoLine.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invOverheadMemoLineForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <tr>
		<td width="575">
       	<div class="tabber">
		<div class="tabbertab" title="Header">
		<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
		<tr>
			<td height="25" width="575">
			</td>
		</tr>
		<tr>			
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.overheadMemoLineName"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="overheadMemoLineName" size="25" maxlength="25" styleClass="textRequired"/>
	         </td>
         </tr>
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.description"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="description" size="50" maxlength="50" styleClass="text"/>
	         </td>
         </tr>
         <tr>
             <td class="prompt" width="140" height="25">
                <bean:message key="overheadMemoLine.prompt.unitMeasure"/>
             </td>
		     <td width="435" height="25" class="control" colspan="3">
		        <html:select property="unitMeasure" styleClass="comboRequired">
		           <html:options property="unitMeasureList"/>		          
		        </html:select>
		     </td>
         </tr>         
	     <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.unitCost"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="unitCost" size="25" maxlength="25" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);"/>
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.overheadAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="overheadAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('overheadAccount','overheadAccountDescription');"/>	            
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.overheadAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="overheadAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.liabilityAccount"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="liabilityAccount" size="30" maxlength="255" styleClass="textRequired"/>
			    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('liabilityAccount','liabilityAccountDescription');"/>	            
	         </td>
         </tr>
         <tr>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="overheadMemoLine.prompt.liabilityAccountDescription"/>
	         </td>
	         <td width="435" height="25" class="control" colspan="3">
	            <html:text property="liabilityAccountDescription" size="75" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
	         </td>
         </tr>        
		</table>
		</div>
	            <div class="tabbertab" title="Branch Map">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="575" height="25" colspan="4">
						</td>
					</tr>
					<nested:iterate property="invBOMLList">
					<tr>
			        	<td class="prompt" height="25" width="30" align="left">
			            	<nested:checkbox property="branchCheckbox"/>
			            </td>
			            <td class="prompt" valign="top" width="200" align="left">
			        		<nested:write property="brName"/>
			            </td>
						<td width="395">
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
							<bean:message key="overheadMemoLine.prompt.overheadAccount"/>
						</td>
						<td width="435" height="25" class="control" colspan="3" align="left">
							<nested:text property="branchOverheadAccount" size="30" maxlength="255" styleClass="text"/>
							<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'branchOverheadAccount','branchOverheadDescription');"/>	            
						</td> 
					</tr>
					<tr>
						<td></td>
					    <td class="prompt" height="25" align="left">
					        <bean:message key="overheadMemoLine.prompt.overheadAccountDescription"/>
			    	    </td>
					    <td width="435" height="25" class="control" colspan="3 align="left">
						   <nested:text property="branchOverheadDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>
					<tr>    
						<td></td>
				        <td class="prompt" height="25" align="left">
			    		    <bean:message key="overheadMemoLine.prompt.liabilityAccount"/>
					    </td>
						<td width="435" height="25" class="control" colspan="3" align="left">
			            	<nested:text property="branchLiabilityAccount" size="30" maxlength="255" styleClass="text"/>
			    		    <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupGridTxn(name, 'branchLiabilityAccount','branchLiabilityDescription');"/>	            
						</td> 			                     
					</tr>	
					<tr>
						<td></td>
						<td class="prompt" height="25" align="left">
					    	<bean:message key="overheadMemoLine.prompt.liabilityAccountDescription"/>
					    </td>
					    <td width="435" height="25" class="control" colspan="3" align="left">
			    		    <nested:text property="branchLiabilityDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/> 
						</td> 
					</tr>			       			       			     				   				   				    
		            </nested:iterate>
				</table>
		        </div> 
		        </div><script>tabberAutomatic(tabberOptions)</script>
		      </td>
		     </tr>
		     <tr>
	         <td width="575" height="50" colspan="4">
	         <table border="0" cellpadding="0" cellspacing="0" width="575" height="50" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
			     <tr>
			        <td width="160" height="50">
			             <div id="buttons">
			             <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     			     			     
					     </div>
					     <div id="buttonsDisabled" style="display: none;">
			             <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">
			             <html:submit property="showDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.showDetails"/>
					     </html:submit>
					     </logic:equal>			     
			             <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">
			             <html:submit property="hideDetailsButton" styleClass="mainButtonMedium" disabled="true">
					         <bean:message key="button.hideDetails"/>
					     </html:submit>
					     </logic:equal>			     
					     </div>
					  </td>
					  <td width="415" height="50" colspan="3">
			             <div id="buttons">
			             <p align="right">
				       	 <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				       	 <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton">
				               <bean:message key="button.save"/>
				          	</html:submit>
				         </logic:equal>
				         <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>
				         <div id="buttonsDisabled" style="display: none;">
			             <p align="right">
				         <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
				         <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
				            <html:submit property="saveButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.save"/>
				            </html:submit>
				         </logic:equal>
				         <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_EDIT%>">
				            <html:submit property="updateButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.update"/>
				            </html:submit>
				            <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.cancel"/>
				            </html:submit>
				         </logic:equal>
				         </logic:equal>
				            <html:submit property="closeButton" styleClass="mainButton" disabled="true">
				               <bean:message key="button.close"/>
				            </html:submit>
				         </div>		         
				      </td>
			     </tr>
	         </table>
	         </td>        
	     </tr>
	     <tr valign="top">
	          <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="7" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="overheadMemoLine.gridTitle.OMLDetails"/>
	                       </td>
	                    </tr>
	            <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_SUMMARIZED%>">        
			    <tr>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.overheadMemoLineName"/>
			       </td>
			       <td width="145" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.description"/>
			       </td>			       
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.unitMeasure"/>
			       </td>
			       <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.unitCost"/>
			       </td>
			       <td width="150" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.overheadAccount"/>
			       </td>			       
			       <td width="299" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="overheadMemoLine.prompt.liabilityAccount"/>
			       </td>			       			        			       
                </tr>			   	                    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="invOMLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="overheadMemoLineName"/>
			       </td>
			       <td width="145" height="1" class="gridLabel">
			          <nested:write property="description"/>
			       </td>
			       <td width="100" height="1" class="gridLabel">
			          <nested:write property="unitMeasure"/>
			       </td>
			       <td width="100" height="1" class="gridLabelNum">
			          <nested:write property="unitCost"/>
			       </td>
			       <td width="150" height="1" class="gridLabel">
			          <nested:write property="overheadAccount"/>
			       </td>
			       <td width="150" height="1" class="gridLabel">
			          <nested:write property="liabilityAccount"/>
			       </td>			       
			       <td width="149" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
   			       </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		              <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    </nested:iterate>
                </logic:equal>
                <logic:equal name="invOverheadMemoLineForm" property="tableType" value="<%=Constants.GLOBAL_DETAILED%>">        			    
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="invOMLList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>
			    <tr bgcolor="<%= rowBgc %>">
			       <td width="175" height="1" class="gridHeader">
			          <bean:message key="overheadMemoLine.prompt.overheadMemoLineName"/>
			       </td>
			       <td width="570" height="1" class="gridLabel" colspan="2">
			          <nested:write property="overheadMemoLineName"/>
			       </td>
			       <td width="149" align="center" height="1">
			       <div id="buttons">
			       <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" onclick="return confirmDelete();">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
				   <div id="buttonsDisabled" style="display: none;">
			       <logic:equal name="invOverheadMemoLineForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
			          <nested:submit property="editButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.edit"/>
  	                  </nested:submit>
  	               <logic:equal name="invOverheadMemoLineForm" property="pageState" value="<%=Constants.PAGE_STATE_SAVE%>">
  	                  <nested:submit property="deleteButton" styleClass="gridButton" disabled="true">
	                     <bean:message key="button.delete"/>
  	                  </nested:submit>
				   </logic:equal>  	                      
				   </logic:equal>
				   </div>
			       </td>
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.description"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="description"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.unitMeasure"/>
			      </td>
			      <td width="149" height="1" class="gridLabel">
			         <nested:write property="unitMeasure"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.overheadAccount"/>
			      </td>
			      <td width="350" height="1" class="gridLabel">
			         <nested:write property="overheadAccount"/>
			      </td>
			      <td width="220" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.unitCost"/>
			      </td>
			      <td width="149" height="1" class="gridLabelNum">
			         <nested:write property="unitCost"/>
			      </td>			      
			    </tr>
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.overheadAccountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="overheadAccountDescription"/>
			      </td>		      
			    </tr>	
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.liabilityAccount"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="liabilityAccount"/>
			      </td>		      
			    </tr>	
			    <tr bgcolor="<%= rowBgc %>">
			      <td width="175" height="1" class="gridHeader">
			         <bean:message key="overheadMemoLine.prompt.liabilityAccountDescription"/>
			      </td>
			      <td width="719" height="1" class="gridLabel" colspan="3">
			         <nested:write property="liabilityAccountDescription"/>
			      </td>		      
			    </tr>			    
			    </nested:iterate>			    
                </logic:equal>			    
			 </table>
		    </div>
		  </td>
	       </tr>
	       <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">		     
		  </td>
	       </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["name"] != null &&
        document.forms[0].elements["name"].disabled == false)
        document.forms[0].elements["name"].focus()
   // -->
</script>
</body>
</html>
