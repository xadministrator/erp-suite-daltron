<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="physicalInventoryEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers

function fnOpenMisc(name){
		
	   var property = name.substring(0,name.indexOf("."));  
	   currProperty = property;
	   currName = name;   
	   var quantity = document.forms[0].elements[property + ".endingInventory"].value.replace(/,/g,'') * 1;
	   
	   var miscStr = document.forms[0].elements[property + ".misc"].value;
	   var specs = new Array();
	   var custodian = new Array(document.forms[0].elements["userList"].value);
	   var custodian2 = new Array();
	   var propertyCode = new Array(); 
	   var expiryDate = new Array(); 
	   var serialNumber = new Array();	   

	   if (miscStr!="") {
		   var tgDocNum = new Array();
		   var arrayMisc = miscStr.split("_");
			
			//cut miscStr and save values
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("@");
			propertyCode = arrayMisc[0].split(",");
			
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("<");
			serialNumber = arrayMisc[0].split(",");
			
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split(">");
			specs = arrayMisc[0].split(",");
			
			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split(";");
			custodian2 = arrayMisc[0].split(",");

			miscStr = arrayMisc[1];
			arrayMisc = miscStr.split("%");
			expiryDate = arrayMisc[0].split(",");
			tgDocNum = arrayMisc[1].split(",");
	   }

	   var isDisabled = document.forms[0].elements[property + ".endingInventory"].disabled;
	   var index = 0;

	   window.open("misc.jsp?miscCtr="+Math.ceil(quantity)+"&specs="+ specs +"&custodian="+custodian+"&propertyCode="
			   +propertyCode+"&expiryDate="+expiryDate +"&property=" + property + "&custodian2="+custodian2 + "&serialNumber=" + serialNumber
			   + "&tgDocumentNumber=" + tgDocNum + "&isDisabled=" + isDisabled, "", "width=800,height=230,scrollbars=yes,status=no");
		
	   return false;

}


function confirmAdjustWastage()
{
   if(confirm("Are you sure you want to Save and Adjust Wastage?")) return true;
      else return false;
} 

function confirmAdjustVariance()
{
   if(confirm("Are you sure you want to Adjust Variance?")) return true;
      else return false;
}	

function calculateVariance(name)
{
  
  var property = name.substring(0,name.indexOf("."));
  
  var actual = 0;
  var wastage = 0;
  var endingInventory = 0;
  var variance = 0;      
  
  if (document.forms[0].elements[property + ".actual"].value != "" &&
      document.forms[0].elements[property + ".wastage"].value != "" &&
      document.forms[0].elements[property + ".endingInventory"].value != "") {  
        
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".actual"].value))) {
	  
	  	actual = (document.forms[0].elements[property + ".actual"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".wastage"].value))) {
	  
	  	wastage = (document.forms[0].elements[property + ".wastage"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".endingInventory"].value))) {
	  
	  	endingInventory = (document.forms[0].elements[property + ".endingInventory"].value).replace(/,/g,'');
	  	
	  }
	  
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".variance"].value))) {
	  
	  	variance = (document.forms[0].elements[property + ".variance"].value).replace(/,/g,'');
	  	
	  }
	  
	  variance = (actual - endingInventory - wastage);
	  
	  document.forms[0].elements[property + ".variance"].value = variance.toFixed(3);
	  
  }
   
}

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invPhysicalInventoryEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="1000" height="510">

   <tr valign="top">
        <td width="170" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="300" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     
          <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="physicalInventoryEntry.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invPhysicalInventoryEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                      <bean:message key="app.success"/>
                   </logic:equal>
		   <html:errors/>	
	        </td>
	     </tr>
	     <html:hidden property="isCategoryEntered" value=""/>
	     <html:hidden property="isDateEntered" value=""/>
	     <html:hidden property="isUnitEntered" value=""/>
	     <html:hidden property="isItemEntered" value=""/>
	     <html:hidden property="isLocationEntered" value=""/>
	     <html:hidden property="userList" />
	     <logic:equal name="invPhysicalInventoryEntryForm" property="enableFields" value="true">
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     					
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						 <tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" onblur="return enterSelect('date', 'isDateEntered');"/>
               				</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="textRequired"/>
                			</td>
         				</tr>      
         				<tr>
	            			<td class="prompt" width="130" height="25">
	            				<bean:message key="physicalInventoryEntry.prompt.category"/>
	            			</td>
			    			<td width="158" height="25" class="control">
	            				<html:select property="category" styleClass="combo" style="width:130;" onchange="return enterSelect('category', 'isCategoryEntered');">
	               				<html:options property="categoryList"/>
	            				</html:select>
			    			</td>
		        			<td class="prompt" width="130" height="25">
	               				<bean:message key="physicalInventoryEntry.prompt.location"/>
	            			</td>
			    			<td width="157" height="25" class="control">
			       				<html:select property="location" styleClass="combo" style="width:130;" onchange="return enterSelect('location', 'isLocationEntered');">
			          			<html:options property="locationList"/>		          
			      				</html:select>
			   	 			</td>
		 				</tr>
		 				<logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.wastageAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="wastageAccount" size="25" maxlength="255" styleClass="textRequired"/>
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('wastageAccount','wastageAccountDescription');"/>           
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.wastageAccountDescription"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="wastageAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>
                			</td>
         				</tr> 
         				</logic:equal>
         				<logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.varianceAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="varianceAccount" size="25" maxlength="255" styleClass="textRequired"/>
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('varianceAccount','varianceAccountDescription');"/>           
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.varianceAccountDescription"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="varianceAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>
                			</td>
         				</tr> 
        	 			</logic:equal>
        	 			
       	 				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>
        	 			
        	 			
				    </table>
					</div>		        		                       
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     					
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>		  
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invPhysicalInventoryEntryForm" property="showSaveButton" value="true">
	           <html:submit property="saveButton" styleClass="mainButton">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       
		       <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   		</html:submit>
		   		
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
		            <bean:message key="button.delete"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
		       <html:submit property="adjustWastageButton" styleClass="mainButtonBig" onclick="return confirmAdjustWastage();">
		            <bean:message key="button.adjustWastage"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
		       <html:submit property="adjustVarianceButton" styleClass="mainButtonBig" onclick="return confirmAdjustVariance();">
		            <bean:message key="button.adjustVariance"/>
		       </html:submit>
		       </logic:equal>	
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invPhysicalInventoryEntryForm" property="showSaveButton" value="true">	           
	           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       
		       <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   		</html:submit>
		   			
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteButton" value="true">
	           <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
		            <bean:message key="button.delete"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
		       <html:submit property="adjustWastageButton" styleClass="mainButtonBig" disabled="true">
		            <bean:message key="button.adjustWastage"/>
		       </html:submit>
		       </logic:equal>      
		       <logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
			       <html:submit property="adjustVarianceButton" styleClass="mainButtonBig" disabled="true">
			            <bean:message key="button.adjustVariance"/>
			       </html:submit>
		       </logic:equal>	
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="11" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="physicalInventoryEntry.gridTitle.PILDetails"/>
		                </td>
		            </tr>
		            <tr>
		            	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.item"/>
				       </td>
				       <td width="320" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.itemDescription"/>
				       </td>
				       <td width="84" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.location"/>
				       </td>
				       <td width="84" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.unit"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.actual"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.wastage"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.endingInventory"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.variance"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.delete"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.misc"/>
				       </td>
				    </tr>							    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invPIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <nested:hidden property="isItemEntered" value=""/>
				    <nested:hidden property="isUnitEntered" value=""/>
				    
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>					       	       
				    
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="textRequired" style="font-size:8pt;"/>
				          <nested:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
				       </td>

				       <td width="320" height="1" class="control">
				          <nested:text property="itemDescription" size="30" maxlength="50" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       
				       <td width="100" height="1" class="control">
				          <nested:select property="location" styleClass="combo" style="width:100;">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				       
				       <td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="combo" style="width:80;" onchange="return enterSelectGrid(name, 'unit', 'isUnitEntered');" >
                            <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="actual" size="5" maxlength="10" styleClass="text" disabled="true" style="font-size:8pt;" onblur="calculateVariance(name);" onkeyup="calculateVariance(name);"/>
				       </td>
				       <logic:equal name="invPhysicalInventoryEntryForm" property="wastageAdjusted" value="false"> 
				       <td width="80" height="1" class="control">
				          <nested:text property="wastage" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" onblur="calculateVariance(name);" onkeyup="calculateVariance(name);"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="endingInventory" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" onblur="calculateVariance(name);" onkeyup="calculateVariance(name);"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="variance" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" />
				       </td>
				       </logic:equal>
		               <logic:equal name="invPhysicalInventoryEntryForm" property="wastageAdjusted" value="true"> 
				       <td width="80" height="1" class="control">
				          <nested:text property="wastage" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="endingInventory" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="variance" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       </logic:equal>
				       
				       <td width="60" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="deleteCheckbox"/>
                       </td>
                       
				       <td width="50" align="center" height="1">
						<div id="buttons"><nested:hidden property="misc" /><nested:submit property="miscButton" styleClass="mainButtonSmall" 
						onclick="return fnOpenMisc(name);">
						<bean:message key="button.miscButton" />
						</nested:submit>
						</div>
						<div id="buttonsDisabled" style="display: none;"><nested:submit
						property="miscButton" styleClass="mainButtonSmall"
						disabled="true">
						<bean:message key="button.miscButton" />
						</nested:submit>
						</div>
						</td>
				    </tr>							    
				  </nested:iterate>

				
				  </table>
				  </div>
				  </td>
				  </tr>
				  
				  
				  <tr>
		         <td width="575" height="25" colspan="11"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="invPhysicalInventoryEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.addLines"/>
			       </html:submit>			   
			       </logic:equal>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>
			       </logic:equal>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="invPhysicalInventoryEntryForm" property="showAddLinesButton" value="true">
		           <html:submit property="addLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.addLines"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteLinesButton" value="true">
			       <html:submit property="deleteLinesButton" styleClass="mainButtonMedium" disabled="true">
			            <bean:message key="button.deleteLines"/>
			       </html:submit>			   
			       </logic:equal>
		           </div>
				  </td>
	     		</tr>
				  
				  
				  </logic:equal>
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  <logic:equal name="invPhysicalInventoryEntryForm" property="enableFields" value="false">
		          <tr>
		          <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     					
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						 <tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
               				</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>      
         				<tr>
	            			<td class="prompt" width="130" height="25">
	            				<bean:message key="physicalInventoryEntry.prompt.category"/>
	            			</td>
			    			<td width="158" height="25" class="control">
	            				<html:select property="category" styleClass="comboRequired" style="width:130;" disabled="true">
	               				<html:options property="categoryList"/>
	            				</html:select>
			    			</td>
		        			<td class="prompt" width="130" height="25">
	               				<bean:message key="physicalInventoryEntry.prompt.location"/>
	            			</td>
			    			<td width="157" height="25" class="control">
			       				<html:select property="location" styleClass="comboRequired" style="width:130;" disabled="true">
			          			<html:options property="locationList"/>		          
			      				</html:select>
			   	 			</td>
		 				</tr>
		 				<logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.wastageAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="wastageAccount" size="25" maxlength="255" styleClass="textRequired"/>
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('wastageAccount','wastageAccountDescription');"/>           
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.wastageAccountDescription"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="wastageAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>
                			</td>
         				</tr> 
         				</logic:equal>
         				<logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.varianceAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="varianceAccount" size="25" maxlength="255" styleClass="textRequired"/>
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('varianceAccount','varianceAccountDescription');"/>           
                			</td>
         				</tr> 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="physicalInventoryEntry.prompt.varianceAccountDescription"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="varianceAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" readonly="true"/>
                			</td>
         				</tr> 
        	 			</logic:equal>
        	 			
       	 				<tr>
         					<td class="prompt" width="140" height="25">
				            <bean:message key="sales.prompt.viewType"/>
				          </td>
				          <td width="128" height="25" class="control">
				            <html:select property="viewType" styleClass="comboRequired">
						      <html:options property="viewTypeList"/>
						   </html:select>
				          </td>
         				</tr>
        	 			
				    </table>
					</div>		        		                       
		        		                       
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     					
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="physicalInventoryEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
		     </tr>		  
		     <tr>
		         <td width="575" height="50" colspan="4"> 
		           <div id="buttons">
		           <p align="right">
		           <logic:equal name="invPhysicalInventoryEntryForm" property="showSaveButton" value="true">
		           <html:submit property="saveButton" styleClass="mainButton">
			            <bean:message key="button.save"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteButton" value="true">
		           <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
			            <bean:message key="button.delete"/>
			       </html:submit>
			       </logic:equal>
			       <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   			</html:submit>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
			       <html:submit property="adjustWastageButton" styleClass="mainButtonBig" onclick="return confirmAdjustWastage();">
			            <bean:message key="button.adjustWastage"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
			       <html:submit property="adjustVarianceButton" styleClass="mainButtonBig" onclick="return confirmAdjustVariance();">
			            <bean:message key="button.adjustVariance"/>
			       </html:submit>
			       </logic:equal>	
				   <html:submit property="closeButton" styleClass="mainButton">
		              <bean:message key="button.close"/>
		           </html:submit>
		           </div>
		           <div id="buttonsDisabled" style="display: none;">
		           <p align="right">
		           <logic:equal name="invPhysicalInventoryEntryForm" property="showSaveButton" value="true">	           
		           <html:submit property="saveButton" styleClass="mainButton" disabled="true">
			            <bean:message key="button.save"/>
			       </html:submit>
			       </logic:equal>
			       <html:submit property="printButton" styleClass="mainButton">
		       		<bean:message key="button.print"/>
		   			</html:submit>
		   			
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showDeleteButton" value="true">
		           <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
			            <bean:message key="button.delete"/>
			       </html:submit>
			       </logic:equal>
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showWastage" value="true">
			       <html:submit property="adjustWastageButton" styleClass="mainButtonBig" disabled="true">
			            <bean:message key="button.adjustWastage"/>
			       </html:submit>
			       </logic:equal>      
			       <logic:equal name="invPhysicalInventoryEntryForm" property="showVariance" value="true">
				       <html:submit property="adjustVarianceButton" styleClass="mainButtonBig" disabled="true">
				            <bean:message key="button.adjustVariance"/>
				       </html:submit>
			       </logic:equal>	       
				   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		              <bean:message key="button.close"/>
		           </html:submit>
		           </div>
				  </td>
		     </tr>
		     
		     
		     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="physicalInventoryEntry.gridTitle.PILDetails"/>
		                </td>
		            </tr>
		            <tr>
		            	<td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.lineNumber"/>
				       </td>
		               <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.item"/>
				       </td>
				       <td width="320" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.itemDescription"/>
				       </td>
				       <td width="84" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.location"/>
				       </td>
				       <td width="84" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.unit"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.actual"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.wastage"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.endingInventory"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.variance"/>
				       </td>
				       <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="physicalInventoryEntry.prompt.misc"/>
				       </td>
				    </tr>							    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invPIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				    <tr bgcolor="<%= rowBgc %>">	
				       <td width="50" height="1" class="control">
				          <nested:text property="lineNumber" size="1" maxlength="3" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>					       	       
				       <td width="120" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="320" height="1" class="control">
				          <nested:text property="itemDescription" size="35" maxlength="50" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
                          <nested:select property="location" styleClass="comboRequired" style="width:80;" disabled="true">
                            <nested:options property="locationList"/>                                               
                          </nested:select>
                       </td>
                       
				       <td width="80" height="1" class="control">
                          <nested:select property="unit" styleClass="comboRequired" style="width:80;" disabled="true">
                            <nested:options property="unitList"/>                                               
                          </nested:select>
                       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="actual" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="wastage" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="endingInventory" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="variance" size="5" maxlength="10" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>
				       <td width="50" align="center" height="1">
				         <div id="buttons"><nested:hidden property="misc" /><nested:submit property="miscButton" styleClass="mainButtonSmall" 
						 onclick="return fnOpenMisc(name);">
				       	 </nested:submit> <nested:hidden property="misc" /> <nested:submit
						 property="miscButton" styleClass="mainButtonSmall"
						 onclick="return fnOpenMisc(name);">
						 <bean:message key="button.miscButton" />
						 </nested:submit>
						 </div>
						 <div id="buttonsDisabled" style="display: none;"><nested:submit
						 property="discountButton" styleClass="mainButtonSmall"
						 disabled="true">
						 <bean:message key="button.discounts" />
						 </nested:submit> <nested:submit property="miscButton"
						 styleClass="mainButtonSmall" disabled="true">
						 <bean:message key="button.miscButton" />
						 </nested:submit>
						 </div>
					  </td>
				    </tr>							    
				  </nested:iterate>
				  
				  </table>
			      </div>
			      </td>
		       </tr>
		       
		      </logic:equal>
	         <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		      </td>
	        </tr>
          </table>
          
		</td>
      </tr>
     
 </table>
</html:form>

<logic:equal name="invPhysicalInventoryEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invPhysicalInventoryEntryForm" type="com.struts.inv.physicalinventoryentry.InvPhysicalInventoryEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/invRepPhysicalInventoryPrint.do?forward=1&physicalInventoryCode=<%=actionForm.getPhysicalInventoryCode()%>&viewType=<%=actionForm.getViewType()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>

</body>
</html>


