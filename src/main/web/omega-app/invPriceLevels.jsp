<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="priceLevels.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function recalculatePrice(name) {
		
	  var property = name.substring(0,name.indexOf("."));
	  var unitCost = 0;
	  var averageCost = 0;
	  var shippingCost = 0;
	  var percentMarkup = 0;
	  var amount = 0;
	  
	  if (!isNaN(parseFloat(document.forms[0].elements["unitCost"].value))) {
	      unitCost = (document.forms[0].elements["unitCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["averageCost"].value))) {
	      averageCost = (document.forms[0].elements["averageCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".shippingCost"].value))) {
	      shippingCost = (document.forms[0].elements[property + ".shippingCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".percentMarkup"].value))) {
	      percentMarkup = (document.forms[0].elements[property + ".percentMarkup"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
	      amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'') * 1;
	  }
	  
	  averageCost = unitCost;
	  
  	  if (name == property + ".percentMarkup") {
  	    	     
  	  	 amount = ((averageCost + shippingCost) * percentMarkup / 100 + averageCost + shippingCost).toFixed(2);		 
		 document.forms[0].elements[property + ".amount"].value = amount;
		 
	  } else if (name == property + ".amount" || name == property + ".shippingCost") {
	  
	  	 document.forms[0].elements[property + ".percentMarkup"].value = ((amount - averageCost - shippingCost) / (averageCost + shippingCost) * 100).toFixed(3);

	  }
	  
	  document.forms[0].elements[property + ".grossProfit"].value = (amount - (averageCost + shippingCost)).toFixed(2);
		  
}

function recalculateAll() {

	var i = 0;
    while (document.forms[0].elements["invPLList[" + i + "].percentMarkup"] != null) {
    
    	recalculatePrice("invPLList[" + i + "].percentMarkup");
    	i++;
    
    }


}


//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invPriceLevels.do" onsubmit="return submitForm();">
	<%@ include file="cmnHeader.jsp" %> 
    <%@ include file="cmnSidebar.jsp" %>
    <table border="0" cellpadding="0" cellspacing="0" width="768" height="200">
      <tr valign="top">
		 <td width="187" height="200"></td> 
         <td width="581" height="200">
            <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	        <tr>
	           <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		          <bean:message key="priceLevels.title"/>
		       </td>
	        </tr>
            <tr>
	           <td width="575" height="44" colspan="4" class="statusBar">
		       <logic:equal name="invPriceLevelForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
                  <bean:message key="app.success"/>
               </logic:equal>
		       <html:errors/>	
	           </td>
	        </tr>
	        
	        <tr>
	        	<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.itemName"/>
			    </td>
				<td width="435" height="25" class="control" colspan="3">
			    <html:text property="itemName" size="25" maxlength="25"  styleClass="text" disabled="true"/>
				</td>
			</tr>
			<tr>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.itemDescription"/>
			    </td>
				<td width="435" height="25" class="control" colspan="3">
			    <html:text property="itemDescription" size="50" maxlength="100"  styleClass="text" disabled="true"/>
				</td>	
			</tr>
			
			<tr>	        	        
	           <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="backButton" styleClass="mainButton">
		          <bean:message key="button.back"/>
			   </html:submit>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="backButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.back"/>
			   </html:submit>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		       </div>
               </td>
	        </tr>

			<tr>
            	<td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
            	</td>
         	</tr>

			<logic:equal name="invPriceLevelForm" property="enableFields" value="true">
			<tr>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.unitCost"/>
			    </td>
				<td width="150" height="25" class="control">
			    <html:text property="unitCost" size="15" maxlength="25"  styleClass="textAmount" disabled="true"/>
				</td>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.averageCost"/>
			    </td>
				<td width="145" height="25" class="control">
			    <html:text property="averageCost" size="15" maxlength="25"  styleClass="textAmount" disabled="true"/>
				</td>
	     	</tr>
	     	<tr>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.percentMarkup"/>
			    </td>
				<td width="150" height="25" class="control">
			    <html:text property="percentMarkup" size="15" maxlength="25"  styleClass="textAmount" disabled="true"/>
				</td>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.shippingCost"/>
			    </td>
				<td width="145" height="25" class="control">
			    <html:text property="shippingCost" size="15" maxlength="25"  styleClass="textAmount" disabled="true"/>
				</td>
	     	</tr>
	     	<tr>
				<td class="prompt" width="140" height="25">
			    <bean:message key="priceLevels.prompt.salesPrice"/>
			    </td>
				<td width="425" height="25" class="control" colspan="3">
			    <html:text property="salesPrice" size="15" maxlength="25"  styleClass="textAmount" disabled="true"/>
				</td>
	     	</tr>
			</logic:equal>
			<tr>	        	        
	           <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
				  <logic:equal name="invPriceLevelForm" property="showSaveButton" value="true">
		          <html:submit property="saveButton" styleClass="mainButton">
		             <bean:message key="button.save"/>
		          </html:submit>
				  </logic:equal>
		          <logic:equal name="invPriceLevelForm" property="enableFields" value="true">
		          <html:submit property="recalcButton" styleClass="mainButton" onclick="recalculateAll(); return false;">
		             <bean:message key="button.salesPriceRecalc"/>
		          </html:submit>
				  </logic:equal>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		          <logic:equal name="invPriceLevelForm" property="showSaveButton" value="true">
		          <html:submit property="saveButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.save"/>
		          </html:submit>
		          </logic:equal>
		          <logic:equal name="invPriceLevelForm" property="enableFields" value="true">
		          <html:submit property="recalcButton" styleClass="mainButton" disabled="true">
		             <bean:message key="button.salesPriceRecalc"/>
		          </html:submit>
				  </logic:equal>
		       </div>
               </td>
	        </tr>
			
			<logic:equal name="invPriceLevelForm" property="enableFields" value="true">
	        <tr valign="top">
	           <td width="575" height="185" colspan="5">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="5" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="priceLevels.gridTitle.PLDetails"/>
	                 </td>
	              </tr>	              
			    <tr>
			       <td width="144" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.priceLevel"/>
			       </td>
			       <td width="107" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.shippingCost"/>
			       </td>
			       <td width="108" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.percentMarkup"/>
			       </td>
			       <td width="107" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.amount"/>
			       </td>
			       <td width="108" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.grossProfit"/>
			       </td>
                </tr>			    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="invPLList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
					 <td width="144" height="1" class="control" colspan="1">
			            <nested:text property="priceLevel" size="25" maxlength="25" styleClass="text" disabled="true"/>
			         </td>
			         <td width="107" height="1" class="control" colspan="1">
			            <nested:text property="shippingCost" size="12" maxlength="35" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name);"/>
			         </td>	
			         <td width="108" height="1" class="control" colspan="1">
			            <nested:text property="percentMarkup" size="12" maxlength="35" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name);"/>
			         </td>
			         <td width="107" height="1" class="control" colspan="1">
			            <nested:text property="amount" size="12" maxlength="35" styleClass="textAmountRequired" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name);"/>
			         </td>	
			         <td width="108" height="1" class="control" colspan="1">
			            <nested:text property="grossProfit" size="12" maxlength="35" styleClass="textAmount" disabled="true"/>
			         </td>
				  </tr>
			      </nested:iterate>
			      </table>
		          </div>
		       </td>
	        </tr>
			</logic:equal>

			<logic:equal name="invPriceLevelForm" property="enableFields" value="false">
			<tr valign="top">
	           <td width="575" height="185" colspan="5">
		          <div align="center">
		          <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			         bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			         bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			      <tr>
                     <td width="575" height="1" colspan="5" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    <bean:message key="priceLevels.gridTitle.PLDetails"/>
	                 </td>
	              </tr>	              
			    <tr>
			       <td width="144" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.priceLevel"/>
			       </td>
			       <td width="107" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.shippingCost"/>
			       </td>
			       <td width="108" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.percentMarkup"/>
			       </td>
			       <td width="107" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.amount"/>
			       </td>
			       <td width="108" height="1" class="gridHeader" colspan="1" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			          <bean:message key="priceLevels.prompt.grossProfit"/>
			       </td>
                </tr>			    	              	              
			      <%
			         int i = 0;	
			         String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			         String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			         String rowBgc = null;
			      %>
			      <nested:iterate property="invPLList">
			      <%
			         i++;
			         if((i % 2) != 0){
			            rowBgc = ROW_BGC1;
			         }else{
			            rowBgc = ROW_BGC2;
			         }  
			      %>
			      <tr bgcolor="<%= rowBgc %>">
					<td width="144" height="1" class="control" colspan="2">
			            <nested:text property="priceLevel" size="25" maxlength="25" styleClass="text" disabled="true"/>
			         </td>
			         <td width="107" height="1" class="control" colspan="1">
			            <nested:text property="shippingCost" size="12" maxlength="35" styleClass="textAmount" disabled="true"/>
			         </td>	
			         <td width="108" height="1" class="control" colspan="1">
			            <nested:text property="percentMarkup" size="12" maxlength="35" styleClass="textAmount" disabled="true"/>
			         </td>	
			         <td width="107" height="1" class="control" colspan="1">
			            <nested:text property="amount" size="12" maxlength="35" styleClass="textAmountRequired" disabled="true"/>
			         </td>	
			         <td width="108" height="1" class="control" colspan="1">
			            <nested:text property="grossProfit" size="12" maxlength="35" styleClass="textAmount" disabled="true"/>
			         </td>
			      </tr>
			      </nested:iterate>
			      </table>
		          </div>
		       </td>
	        </tr>
			</logic:equal>

	        <tr>
	           <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       </td>
	        </tr>
            </table>
         </td>
      </tr>
   </table>
</html:form>
</body>
</html>
