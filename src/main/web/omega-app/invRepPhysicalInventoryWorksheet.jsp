<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="physicalInventoryWorksheet.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('goButton'));">
<html:form action="/invRepPhysicalInventoryWorksheet.do" onsubmit="return disableButtons();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="260">
      <tr valign="top">
        <td width="187" height="260"></td> 
        <td width="581" height="260">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="260" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="physicalInventoryWorksheet.title"/>
		</td>
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		       <html:errors/>	
	        </td>
	     </tr>
	     <tr>	         
	         <td class="prompt" width="140" height="25">
	            <bean:message key="physicalInventoryWorksheet.prompt.category"/>
	         </td>
	         <td width="127" height="25" class="control">
	            <html:select property="category" styleClass="combo">
			      <html:options property="categoryList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="physicalInventoryWorksheet.prompt.date"/>
	         </td>
	         <td width="128" height="25" class="control">
	            <html:text property="date" size="10" maxlength="25" styleClass="textRequired"/>
	         </td>
	    </tr>
         <tr> 
          	 <td class="prompt" width="140" height="25">
	            <bean:message key="physicalInventoryWorksheet.prompt.location"/>
	         </td>
	          <td width="128" height="25" class="control">
	            <html:select property="location" styleClass="comboRequired">
			      <html:options property="locationList"/>
			   </html:select>
	         </td>
	         <td class="prompt" width="140" height="25">
	            <bean:message key="physicalInventoryWorksheet.prompt.branch"/>
	         </td>
	          <td width="128" height="25" class="control">
	            <html:select property="branch" styleClass="comboRequired">
			      <html:options property="branchList"/>
			   </html:select>
	         </td>
	     </tr>    
         <tr>
         	<td class="prompt" width="140" height="25">
	        	<bean:message key="physicalInventoryWorksheet.prompt.itemClass"/>
			</td>
	        <td width="127" height="25" class="control">
	        	<html:select property="itemClass" styleClass="combo">
			    	<html:options property="itemClassList"/>
			   	</html:select>
	        </td>
	        
			<td class="prompt" width="140" height="25">
				<bean:message key="physicalInventoryWorksheet.prompt.includeUnposted" />
	        </td>
	        <td width="128" height="25">
	        	<html:checkbox property="includeUnposted" styleClass="checkbox" />
	        </td>

	     </tr>    
         <tr>
	        <td class="prompt" width="140" height="25">
	            <bean:message key="physicalInventoryWorksheet.prompt.viewType"/>
	         </td>
	          <td width="128" height="25" class="control">
	            <html:select property="viewType" styleClass="comboRequired">
			      <html:options property="viewTypeList"/>
			   </html:select>
	         </td>   
	         <td class="prompt" width="140" height="25">
				<bean:message key="physicalInventoryWorksheet.prompt.includeEncoded" />
	        </td>
	         <td width="128" height="25">
	        	<html:checkbox property="includeEncoded" styleClass="checkbox" />
	        </td>
	        
         </tr>
         <tr>
         	<td class="prompt" width="140" height="25">
				<bean:message key="physicalInventoryWorksheet.prompt.includeIntransit" />
	        </td>
	        <td width="128" height="25">
	        	<html:checkbox property="includeIntransit" styleClass="checkbox" />
	        </td>
	        <td class="prompt" width="140" height="25">
	        	<bean:message key="physicalInventoryWorksheet.prompt.customer"/>
			</td>
	        <td width="127" height="25" class="control">
	        	<html:select property="customer" styleClass="combo">
			    	<html:options property="customerList"/>
			   	</html:select>
	        </td>
         </tr>
        <tr>
	        <td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	          <p align="right">
		      <html:submit property="goButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.go"/>
		      </html:submit>
		      <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		         <bean:message key="button.close"/>
		      </html:submit>
		      </div>
            </td>
	     </tr>	     
	     <tr>
	          <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		     
		  	  </td>
	     </tr>
          </table>

        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
      if(document.forms[0].elements["category"] != null)
             document.forms[0].elements["category"].focus()
   // -->
</script>
<logic:equal name="invRepPhysicalInventoryWorksheetForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/cmnReport.jsp","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
