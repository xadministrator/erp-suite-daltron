<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invStockIssuanceEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function showInvBolLookup(selectedItemName, selectedItemEntered) 
{
   window.open("invFindBuildOrderLine.do?selectedItemName=" + selectedItemName + "&selectedItemEntered=" + selectedItemEntered,"invFindBuildOrderLine","width=605,height=500,scrollbars,status");
   
   return false;
      
}

function calculateAmount(name)
{
	
	  var property = name.substring(0,name.indexOf("."));
       
      var issueQuantity = 0;
      var unitCost = 0;
      var amount = 0;  
          
      if (document.forms[0].elements[property + ".issueQuantity"].value != "" &&
      		document.forms[0].elements[property + ".unitCost"].value != "") {  
          
          if (!isNaN(parseFloat(document.forms[0].elements[property + ".issueQuantity"].value))) {
      
	      	issueQuantity = (document.forms[0].elements[property + ".issueQuantity"].value).replace(/,/g,'');
	      	
	      }
          if (!isNaN(parseFloat(document.forms[0].elements[property + ".unitCost"].value))) {
      
	      	unitCost = (document.forms[0].elements[property + ".unitCost"].value).replace(/,/g,'');
	      	
	      }	      
	      
	      if (!isNaN(parseFloat(document.forms[0].elements[property + ".amount"].value))) {
	  
	  	    amount = (document.forms[0].elements[property + ".amount"].value).replace(/,/g,'');
	  	
	      }
	  
          amount = (issueQuantity * unitCost).toFixed(2);
	      
		  document.forms[0].elements[property + ".amount"].value = formatDisabledAmount(amount.toString());	
	                 
      }
           	
}  



//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveSubmitButton'));">
<html:form action="/invStockIssuanceEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
        <td width="187" height="510"></td>
        
        <!-- ===================================================================== -->
		<!--  Title Bar                                                            -->
		<!-- ===================================================================== -->
        
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		   <bean:message key="invStockIssuanceEntry.title"/>
		</td>
		
		<!-- ===================================================================== -->
        <!--  Status Msg                                                           -->
        <!-- ===================================================================== -->
		
	     </tr>
             <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invStockIssuanceEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	        </td>
	     </tr>
	      
	     <!-- ===================================================================== -->
         <!--  Screen when enabled                                                  -->
         <!-- ===================================================================== -->


         <html:hidden property="isItemEntered" value=""/>
	     <html:hidden property="bolCode"/>
	     <logic:equal name="invStockIssuanceEntryForm" property="enableFields" value="true">     
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">	
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>				     
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.itemName"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="itemName" size="15" maxlength="25" styleClass="textRequired" readonly="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvBolLookup('itemName', 'isItemEntered');"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired"/>
                			</td>
         				</tr>
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>		 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
								<html:text property="description" size="50" maxlength="50" styleClass="text"/>
                			</td>
         				</tr>
		        	</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>				     
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.buildOrder"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="buildOrder" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.location"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="location" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr> 
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.void"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="voidCheckbox"/>
                			</td>
         				</tr>                   
					</table>
					</div>
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invStockIssuanceEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
			   </logic:equal>
			   <logic:equal name="invStockIssuanceEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invStockIssuanceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>        
               <html:submit property="printButton" styleClass="mainButton">
	                <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invStockIssuanceEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
			   </logic:equal>
			   <logic:equal name="invStockIssuanceEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invStockIssuanceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="printButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="7" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invStockIssuanceEntry.gridTitle.SIDetails"/>
		                </td>
		            </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (enabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="25" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.itemName"/>
				       </td>		            
		               <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.location"/>
				       </td>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.unit"/>
				       </td>				       				    
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.quantityRequired"/>
                       </td>
                      
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issueQty"/>
                       </td>
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issueCost"/>
                       </td>
                       <td width="54" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issue"/>
                       </td>       				       				       	                                             
				    </tr>
				    <tr>
				    	 <td width="100" height="1" colspan="4" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.itemDescription"/>
                       </td>
                        
				    	 <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.quantityIssued"/>
                       </td>
                       
                       <td width="60" height="1" colspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.amount"/>
                       </td>
                   </tr>				       			    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invSIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>		 
					<nested:hidden property="isUnitEntered"/>
					<nested:hidden property="isLocationEntered"/>
				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="25" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="110" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:90;" onchange="return enterSelectGrid(name, 'location', 'isLocationEntered');" >
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:select property="unit" styleClass="comboRequired" style="width:70;" onchange="return enterSelectGrid(name, 'unit', 'isUnitEntered');" >
				              <nested:options property="unitList"/>                                               
				          </nested:select>
                       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="quantityRequired" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       
				       <td width="80" height="1" class="control">
				          <nested:text property="issueQuantity" size="5" maxlength="10" styleClass="text" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);"/>
				       </td>
				        <td width="100" height="1" class="control">
				          <nested:text property="unitCost" size="8" maxlength="17" styleClass="textAmount" disabled="true"/>
				       </td>			       
                       <td width="80" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="issueCheckbox"/>
                       </td>
                       <tr>
                      
				       	<td width="510" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>	
				       	<td width="80" height="1" class="control">
				          <nested:text property="quantityIssued" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="80" height="1" colspan="2" class="control">
				          <nested:text property="amount" size="15" maxlength="17" styleClass="textAmount" disabled="true"/>
				       </td>	
				      </tr>      		         				       
				    </tr>
					</nested:iterate>
				    </table>
			      </div>
			      </td>
		 </tr>
	     </logic:equal>
	     
	     <logic:equal name="invStockIssuanceEntryForm" property="enableFields" value="false">     
         <tr>
		        <td width="575" height="10" colspan="4">
			        <div class="tabber">
					<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>				     
         				<tr>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.itemName"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="itemName" size="15" maxlength="25" styleClass="textRequired" readonly="true" disabled="true"/>
                   				<html:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.date"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>
         				</tr>
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>		 
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.description"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
								<html:text property="description" size="50" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
		        	</table>
					</div>
					<div class="tabbertab" title="Misc">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>				     
						<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.buildOrder"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="buildOrder" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.location"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="location" size="15" maxlength="25" styleClass="text" disabled="true"/>
                			</td>
         				</tr> 
		 				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockIssuanceEntry.prompt.void"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:checkbox property="voidCheckbox" disabled="true"/>
                			</td>
         				</tr>                   
					</table>
					</div>			        		    
					 <div class="tabbertab" title="Status">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.approvalStatus"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.posted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>	
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.reasonForRejection"/>
			                </td>
			                <td width="415" height="25" class="control" colspan="3">
			                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
			                </td>
				         </tr>				         
					 </table>
					 </div>
					 <div class="tabbertab" title="Log">
					 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.createdBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateCreated"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
				            <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.lastModifiedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateLastModified"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>			                
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.approvedRejectedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.dateApprovedRejected"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
				         <tr>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.postedBy"/>
			                </td>
			                <td width="128" height="25" class="control">
			                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
			                <td width="160" height="25" class="prompt">
			                   <bean:message key="invStockIssuanceEntry.prompt.datePosted"/>
			                </td>
			                <td width="127" height="25" class="control">
			                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
			                </td>
				         </tr>
					 </table>
					 </div>
					 </div><script>tabberAutomatic(tabberOptions)</script>
			     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
	           <logic:equal name="invStockIssuanceEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" onclick="return confirmSaveSubmit();">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
			   </logic:equal>
			   <logic:equal name="invStockIssuanceEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invStockIssuanceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>        
               <html:submit property="printButton" styleClass="mainButton">
	                <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="journalButton" styleClass="mainButton">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <logic:equal name="invStockIssuanceEntryForm" property="showSaveSubmitButton" value="true">
	           <html:submit property="saveSubmitButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveSubmit"/>
		       </html:submit>
			   </logic:equal>
			   <logic:equal name="invStockIssuanceEntryForm" property="showSaveAsDraftButton" value="true">
	           <html:submit property="saveAsDraftButton" styleClass="mainButtonMedium" disabled="true">
		            <bean:message key="button.saveAsDraft"/>
		       </html:submit>
		       </logic:equal>
		       <logic:equal name="invStockIssuanceEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
               <html:submit property="printButton" styleClass="mainButton" disabled="true">
	                <bean:message key="button.print"/>
	           </html:submit>
			   <html:submit property="journalButton" styleClass="mainButton" disabled="true">
	               <bean:message key="button.journal"/>
	           </html:submit>
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	     </tr>
	     <tr valign="top">
		          <td width="575" height="185" colspan="4">
			      <div align="center">
			        <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
				    <tr>
	                <td width="575" height="1" colspan="8" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                    	<bean:message key="invStockIssuanceEntry.gridTitle.SIDetails"/>
		                </td>
		            </tr>
					
					<!-- ===================================================================== -->
         			<!--  Lines column label (disabled)                                        -->
         			<!-- ===================================================================== -->
					
		            <tr>
		               <td width="25" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.itemName"/>
				       </td>		            
		               <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.location"/>
				       </td>
		               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="invStockIssuanceEntry.prompt.unit"/>
				       </td>				       				    
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.quantityRequired"/>
                       </td>
                      
                       <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issueQty"/>
                       </td>
                        <td width="120" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issueCost"/>
                       </td>
                       <td width="54" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.issue"/>
                       </td>       				       				       	                                             
				    </tr>
				      <tr>
				    	 <td width="100" height="1" colspan="4" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.itemDescription"/>
                       </td>
                        
				    	 <td width="60" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.quantityIssued"/>
                       </td>
                       
                       <td width="60" height="1" colspan="2" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                           <bean:message key="invStockIssuanceEntry.prompt.amount"/>
                       </td>
                   </tr>								       			    
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invSIList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>		 

				    <tr bgcolor="<%= rowBgc %>">		
				       <td width="25" height="1" class="control">
				          <nested:text property="itemName" size="12" maxlength="25" styleClass="text" disabled="true"/>
				       </td>				    								    				       	       
				       <td width="110" height="1" class="control">
				          <nested:select property="location" styleClass="comboRequired" style="width:90;" disabled="true">
				              <nested:options property="locationList"/>				          				          
				          </nested:select>
				       </td>
				       <td width="50" height="1" class="control">
				          <nested:select property="unit" styleClass="comboRequired" style="width:70;" disabled="true">
				              <nested:options property="unitList"/>                                               
				          </nested:select>
                       </td>
				       <td width="80" height="1" class="control">
				          <nested:text property="quantityRequired" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       
				       <td width="80" height="1" class="control">
				          <nested:text property="issueQuantity" size="5" maxlength="10" styleClass="text" onblur="calculateAmount(name); addZeroes(name);" onkeyup="calculateAmount(name); formatAmount(name, (event)?event:window.event);" disabled="true"/>
				       </td>
				        <td width="100" height="1" class="control">
				          <nested:text property="unitCost" size="8" maxlength="17" styleClass="textAmount" disabled="true"/>
				       </td>			       
                       <td width="50" height="1" class="control">
				          <p align="center">				       
                          <nested:checkbox property="issueCheckbox" disabled="true"/>
                       </td> 
                      <tr>
                      
				       	<td width="510" height="1" class="control" colspan="4">
				          <nested:text property="itemDescription" size="40" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
				       </td>	
				       	<td width="80" height="1" class="control">
				          <nested:text property="quantityIssued" size="5" maxlength="10" styleClass="text" disabled="true"/>
				       </td>
				       <td width="80" height="1" colspan="2" class="control">
				          <nested:text property="amount" size="15" maxlength="17" styleClass="textAmount" disabled="true"/>
				       </td>	
				      </tr>      	     		         				       
				    </tr>
					</nested:iterate>
				    </table>
			      </div>
			      </td>
		 </tr>
	     </logic:equal>
	     
	     <tr>
	     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["date"] != null &&
        document.forms[0].elements["date"].disabled == false)        
        document.forms[0].elements["date"].focus();
	       // -->
</script>
<logic:equal name="invStockIssuanceEntryForm" property="report" value="<%=Constants.STATUS_SUCCESS%>">
<bean:define id="actionForm" name="invStockIssuanceEntryForm" type="com.struts.inv.stockissuanceentry.InvStockIssuanceEntryForm"/>
<script type="text/javascript" langugage="JavaScript">
  <!--
     win = window.open("<%=request.getContextPath()%>/invRepStockIssuancePrint.do?forward=1&stockIssuanceCode=<%=actionForm.getStockIssuanceCode()%>","","height=550,width=750,resizable,menubar,scrollbars");
  //-->
</script>
</logic:equal>
</body>
</html>
