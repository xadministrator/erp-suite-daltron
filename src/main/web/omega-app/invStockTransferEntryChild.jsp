<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="invStockTransferEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

//Done Hiding--> 
</script>

<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('closeButton'));">
<html:form action="/invStockTransferEntry.do" onsubmit="return submitForm();">
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="510">
      <tr valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="510" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		      <bean:message key="invStockTransferEntry.title"/>
		    </td>
	     </tr>
         <tr>
	       <td width="575" height="44" colspan="4" class="statusBar">
		   <logic:equal name="invStockTransferEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
           </logic:equal>
		   <html:errors/>	
		   <html:messages id="msg" message="true">
		       <bean:write name="msg"/>		   
		   </html:messages>
	       </td>
	     </tr>
         <tr>
	        <td width="575" height="10" colspan="4">
		        <div class="tabber">	
				<div class="tabbertab" title="Header">
					<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
					     <tr> 
					        <td class="prompt" width="575" height="25" colspan="4">
			                </td>
			             </tr>
						<tr>                		
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockTransferEntry.prompt.date"/>
                			</td>
                			<td width="445" height="25" class="control">
                   				<html:text property="date" size="10" maxlength="10" styleClass="textRequired" disabled="true"/>
                			</td>                			
         				</tr>
         				<tr>							
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockTransferEntry.prompt.referenceNumber"/>
                			</td>
                			<td width="158" height="25" class="control">
                   				<html:text property="referenceNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
							<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockTransferEntry.prompt.documentNumber"/>
                			</td>
                			<td width="157" height="25" class="control">
                   				<html:text property="documentNumber" size="15" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>
         				<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="invStockTransferEntry.prompt.description"/>
               	 			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="description" size="60" maxlength="50" styleClass="text" disabled="true"/>
                			</td>
         				</tr>                  				         
				    </table>
					</div>		        		    
				 <div class="tabbertab" title="Status">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
			         <tr>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.approvalStatus"/>
		                </td>
		                <td width="128" height="25" class="control">
		                   <html:text property="approvalStatus" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.posted"/>
		                </td>
		                <td width="127" height="25" class="control">
		                   <html:text property="posted" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
			         </tr>	
			         <tr>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.reasonForRejection"/>
		                </td>
		                <td width="415" height="25" class="control" colspan="3">
		                   <html:textarea property="reasonForRejection" cols="44" rows="2" styleClass="text" disabled="true"/>
		                </td>
			         </tr>                      			         
				 </table>
				 </div>
				 <div class="tabbertab" title="Log">
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  	     
				     <tr> 
				        <td class="prompt" width="575" height="25" colspan="4">
		                </td>
		             </tr>
			         <tr>
			            <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.createdBy"/>
		                </td>
		                <td width="128" height="25" class="control">
		                   <html:text property="createdBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.dateCreated"/>
		                </td>
		                <td width="127" height="25" class="control">
		                   <html:text property="dateCreated" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>			                
			         </tr>
			         <tr>
			            <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.lastModifiedBy"/>
		                </td>
		                <td width="128" height="25" class="control">
		                   <html:text property="lastModifiedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.dateLastModified"/>
		                </td>
		                <td width="127" height="25" class="control">
		                   <html:text property="dateLastModified" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>			                
			         </tr>
			         <tr>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.approvedRejectedBy"/>
		                </td>
		                <td width="128" height="25" class="control">
		                   <html:text property="approvedRejectedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.dateApprovedRejected"/>
		                </td>
		                <td width="127" height="25" class="control">
		                   <html:text property="dateApprovedRejected" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
			         </tr>
			         <tr>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.postedBy"/>
		                </td>
		                <td width="128" height="25" class="control">
		                   <html:text property="postedBy" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
		                <td width="160" height="25" class="prompt">
		                   <bean:message key="invStockTransferEntry.prompt.datePosted"/>
		                </td>
		                <td width="127" height="25" class="control">
		                   <html:text property="datePosted" size="10" maxlength="10" styleClass="text" disabled="true"/>
		                </td>
			         </tr>
				 </table>
				 </div>
				 </div><script>tabberAutomatic(tabberOptions)</script>
		     </td>
	     </tr>
	     <tr>
	         <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" onclick="window.close();">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
			   <html:submit property="closeButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.close"/>
	           </html:submit>
	           </div>
			  </td>
	        </tr>
         <tr valign="top">
              <td width="575" height="185" colspan="4">
              <div align="center">
                <table border="1" cellpadding="0" cellspacing="0" width="577" height="47" bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
                <tr>
                <td width="575" height="1" colspan="9" class="gridTitle" bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
                        <bean:message key="invStockTransferEntry.gridTitle.STDetails"/>
                    </td>
                </tr>
				
				<!-- ===================================================================== -->
     			<!--  Lines column label (disabled)                                        -->
     			<!-- ===================================================================== -->
				
	            <tr>
	               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="invStockTransferEntry.prompt.lineNumber"/>
			       </td>		            
	               <td width="240" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="invStockTransferEntry.prompt.itemName"/>
			       </td>
	               <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="invStockTransferEntry.prompt.locationFrom"/>
			       </td>				       				    
	               <td width="110" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="invStockTransferEntry.prompt.locationTo"/>
			       </td>					      
                   <td width="80" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       <bean:message key="invStockTransferEntry.prompt.qtyDelivered"/>
                   </td>
                   <td width="100" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       <bean:message key="invStockTransferEntry.prompt.unit"/>
                   </td>
                   <td width="134" height="1" class="gridHeader" colspan="2" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       <bean:message key="invStockTransferEntry.prompt.unitCost"/>
                   </td>       
 				   <td width="70" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       <bean:message key="invStockTransferEntry.prompt.delete"/>
                   </td>				       				       	                                             
			    </tr>				       			    
			    <tr>
	               <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			       </td>		   
			       <td width="640" height="1" class="gridHeader" colspan="5" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
			           <bean:message key="invStockTransferEntry.prompt.itemDescription"/>
			       </td>	
			       <td width="204" height="1" class="gridHeader" colspan="3" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
                       <bean:message key="invStockTransferEntry.prompt.amount"/>
                   </td>		          				        			       		    
			    </tr>
			    <%
			       int i = 0;	
			       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
			       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
			       String rowBgc = null;
			    %>
			    <nested:iterate property="invSTList">
			    <%
			       i++;
			       if((i % 2) != 0){
			           rowBgc = ROW_BGC1;
			       }else{
			           rowBgc = ROW_BGC2;
			       }  
			    %>			    
			    <tr bgcolor="<%= rowBgc %>">		
			       <td width="50" height="1" class="control">
			          <nested:text property="lineNumber" size="1" maxlength="4" styleClass="text" disabled="true"/>
			       </td>				    								    				       	       
			       <td width="240" height="1" class="control">
			          <nested:text property="itemName" size="8" maxlength="25" styleClass="textRequired" disabled="true"/>
			          <nested:image property="lookupButton" src="images/lookup.gif" disabled="true"/>
			       </td>
			       <td width="110" height="1" class="control">
			          <nested:select property="locationFrom" styleClass="comboRequired" style="width:90;"  disabled="true">
			              <nested:options property="locationList"/>				          				          
			          </nested:select>
			       </td>								   
			       <td width="110" height="1" class="control">
			          <nested:select property="locationTo" styleClass="comboRequired" style="width:90;"  disabled="true">
			              <nested:options property="locationList"/>				          				          
			          </nested:select>
			       </td>								   			       
			       <td width="80" height="1" class="control">
			          <nested:text property="qtyDelivered" size="4" maxlength="10" styleClass="textRequired" disabled="true"/>
			       </td>
			       <td width="100" height="1" class="control">
                      <nested:select property="unit" styleClass="comboRequired" style="width:80;"  disabled="true">
                          <nested:options property="unitList"/>                                               
                      </nested:select>
                   </td>			       
                   <td width="134" height="1" class="control" colspan="2">
                      <nested:text property="unitCost" size="10" maxlength="25" styleClass="textAmount"  disabled="true"/>
                   </td>    		       
				   <td width="70" height="1" class="control">
			          <p align="center">				       
                      <nested:checkbox property="deleteCheckbox"  disabled="true"/>
                   </td>    				       
			   </tr>
			   <tr bgcolor="<%= rowBgc %>">
			       <td width="50" height="1" class="control"/>				   
			       <td width="640" height="1" class="control" colspan="5">
			          <nested:text property="itemDescription" size="50" maxlength="70" styleClass="text" style="font-size:8pt;" disabled="true"/>
			       </td>
			       <td width="204" height="1" class="control" colspan="3">
			          <nested:text property="amount" size="17" maxlength="10" styleClass="textAmount" disabled="true"/>
			       </td>				                                     
			    </tr>
			  </nested:iterate>
			  </table>
		      </div>
		      </td>
	       </tr>  	        
		     <tr>
		     	<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>"></td>
		     </tr>
         </table>
      </tr>
  </table>
</html:form>
</body>
</html>

