<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="arJournal.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	

function submitForm()
{            
      disableButtons();
      enableInputControls();
}

function clickBaseUnit(name)
{
   
   var ctr = 0;
   
   while (true) {

       if (document.forms[0].elements["invUMCList[" + ctr + "].baseUnit"] == null) break;
       if (name != "invUMCList[" + ctr + "].baseUnit") {

	       document.forms[0].elements["invUMCList[" + ctr + "].baseUnit"].checked = false;
	       
	   }
	   
	   ctr++;
   
   }  
   
   return true;
}


//Done Hiding--> 
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invUnitOfMeasureConversion.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="768" height="200">
      <tr valign="top">
        <td width="187" height="510"></td> 
        <td width="581" height="510">
          <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" 
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="unitOfMeasureConversion.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="invUnitOfMeasureConversionForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	     </tr>

	     <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="unitOfMeasureConversion.prompt.itemName"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
	           <html:text property="itemName" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
		 </tr>
		 <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="unitOfMeasureConversion.prompt.itemDescription"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
	           <html:text property="itemDescription" size="50" maxlength="100" styleClass="text" disabled="true"/>
	        </td>
		 </tr>
		 <tr>
	        <td class="prompt" width="140" height="25">
	           <bean:message key="unitOfMeasureConversion.prompt.unitMeasure"/>
	        </td>
	        <td width="435" height="25" class="control" colspan="3">
			   <html:text property="unitMeasure" size="25" maxlength="25" styleClass="text" disabled="true"/>
	        </td>
         </tr>  
     	 <tr>
	       <td width="575" height="50" colspan="4"> 
	        <div id="buttons">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		    <div id="buttonsDisabled" style="display: none;">
	        <p align="right">
	        <html:submit property="backButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.back"/>
		    </html:submit>
		    <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		    </html:submit>
		    </div>
		   <td>
         </tr>	     
	     
	     <tr>
            <td width="575" height="3" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
            </td>
         </tr>
	     
	     <tr>    	       	      	     
	        <td width="575" height="50" colspan="4"> 
	           <div id="buttons">
	           <p align="right">
		       <logic:equal name="invUnitOfMeasureConversionForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       </div>
		       <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
		       <logic:equal name="invUnitOfMeasureConversionForm" property="showSaveButton" value="true">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       </div>
		    </td>
	     </tr>

         <logic:equal name="invUnitOfMeasureConversionForm" property="enableFields" value="true">
	     <tr valign="top">
	        <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="unitOfMeasureConversion.gridTitle.UMCDetails"/>
	                       </td>
	                    </tr>	
                 <tr>
		               <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.unitMeasure"/>
				       </td>
				       <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.shortName"/>
				       </td>
				       <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.adLvClass"/>
				       </td>
				       <td width="173.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.conversion"/>
				       </td>
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.baseUnit"/>
				       </td>				     
				 </tr>							    
				   
				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invUMCList"> 
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				
				<tr bgcolor="<%= rowBgc %>">
			    	   <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomName"/>
			       </td>
			       <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomShortName"/>
			       </td>
			       <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomAdLvClass"/>
			       </td>
				   <td width="173.5" height="1" class="control">
				       <nested:text property="conversionFactor" size="12" maxlength="25" styleClass="text"/>
				   </td>
				   <td width="50" height="1" class="control">
				       <nested:checkbox property="baseUnit" onclick="return clickBaseUnit(name);"/>
				   </td>
			    </tr>
				  </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
         </logic:equal>
         
         <!----part2-->
         <logic:equal name="invUnitOfMeasureConversionForm" property="enableFields" value="false">
	     <tr valign="top">
	        <td width="575" height="185" colspan="4">
		    <div align="center">
		         <table border="1" cellpadding="0" cellspacing="0" width="577" height="47"
			     bordercolordark="<%=Constants.TXN_TABLE_BORDER_DARK_BGC%>" 
			     bordercolor="<%=Constants.TXN_TABLE_BORDER_BGC%>">
			    <tr>
                               <td width="575" height="1" colspan="6" class="gridTitle" 
			             bgcolor="<%=Constants.TXN_TABLE_TITLE_BGC%>">
	                          <bean:message key="unitOfMeasureConversion.gridTitle.UMCDetails"/>
	                       </td>
	                    </tr>	
                 <tr>
		               <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.unitMeasure"/>
				       </td>
				       <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.shortName"/>
				       </td>
				       <td width="223.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.adLvClass"/>
				       </td>
				       <td width="173.5" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.conversion"/>
				       </td>				     
				       <td width="50" height="1" class="gridHeader" bgcolor="<%=Constants.TXN_TABLE_HEADER_BGC%>">
				           <bean:message key="unitOfMeasureConversion.prompt.baseUnit"/>
				       </td>				     
				 </tr>									    
				    				    <%
				       int i = 0;	
				       String ROW_BGC1 = Constants.TXN_TABLE_ROW_BGC1;
				       String ROW_BGC2 = Constants.TXN_TABLE_ROW_BGC2;
				       String rowBgc = null;
				    %>
				    <nested:iterate property="invUMCList">
				    <%
				       i++;
				       if((i % 2) != 0){
				           rowBgc = ROW_BGC1;
				       }else{
				           rowBgc = ROW_BGC2;
				       }  
				    %>
				   <tr bgcolor="<%= rowBgc %>">
			    	   <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomName" />
			       </td>
			       <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomShortName" />
			       </td>
			       <td width="223.5" height="1" class="gridLabel">
			          <nested:write property="uomAdLvClass"/>
			       </td>
			       <td width="173.5" height="1" class="control">
				      <nested:text property="conversionFactor" size="12" maxlength="25" styleClass="text" disabled="true"/>
				   </td>
				   <td width="50" height="1" class="control">
				       <nested:checkbox property="baseUnit" disabled="true"/>
				   </td>
			    </tr>
				 </nested:iterate>
				  </table>
			      </div>
			      </td>
		       </tr>
        	 </logic:equal>
	
			 </table>
		    </div>
		  </td>
	     </tr>
	     <tr>
	        <td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		    </td>
	     </tr>
         </table>
        </td>
      </tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
 
</script>
</body>
</html>
