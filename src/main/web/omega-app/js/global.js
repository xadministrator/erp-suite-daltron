function disableButtons()
{

      var divList = document.body.getElementsByTagName("div");

      for (var i = 0; i < divList.length; i++) {

          var currDiv = divList[i];
          var currId = currDiv.id;

          if (currId == "buttons") {

              currDiv.style.display = "none";

          } else if (currId == "buttonsDisabled") {

              currDiv.style.display = "block";

          }

      }

}

function enableInputControls()
{
      for (var i=0; i < document.forms[0].elements.length; i++) {

          if (document.forms[0].elements[i].type != 'submit') {

              document.forms[0].elements[i].disabled=false;

          }
      }
}

function confirmCopy()
{

   if(confirm("Are you sure you want to copy this row?")) return true;
   else return false;

}

function confirmDelete()
{

   if(confirm("Are you sure you want to delete this row?")) return true;
   else return false;

}

function confirmComplete()
{

   if(confirm("Are you sure you want to complete this transaction?")) return true;
   else return false;

}

function confirmSaveSubmit()
{

   if(confirm("Are you sure you want to save & submit this document?")) return true;
   else return false;

}

function enterSelectSupplier(evt, field1,field2) {

	 if (document.forms[0].elements[field1].value=="") {
		if (navigator.appName=="Netscape")
		{
			if(evt.target.value!=undefined) document.forms[0].elements[field1].value=evt.target.value;
			else document.forms[0].elements[field1].value = evt.target.innerHTML.replace(/\s/g, "");
		}
		else
		{
			if(evt.target.value!=undefined) document.forms[0].elements[field1].value=evt.target.value;
			else document.forms[0].elements[field1].value = evt.srcElement.innerHTML.replace(/\s/g, "");
		}

		document.forms[0].elements[field2].value = true;
		disableButtons();
		enableInputControls();
		document.forms[0].submit();
	 }

}

function enterSelect(field1,field2)
{


   if (document.forms[0].elements[field1].value != "") {

      document.forms[0].elements[field2].value = true;
      disableButtons();
      enableInputControls();
      document.forms[0].submit();

   }

}

function enterSelectWithBlank(field1,field2)
{

    document.forms[0].elements[field2].value = true;
    disableButtons();
    enableInputControls();
    document.forms[0].submit();

}

function enterSelectGrid(name, field1,field2)
{

   var property = name.substring(0,name.indexOf("."));
   if (document.forms[0].elements[property + "." + field1].value != "") {

      document.forms[0].elements[property + "." + field2].value = true;
      disableButtons();
      enableInputControls();
      document.forms[0].submit();

   }

}

function enterSelectGridBlank(name, field1,field2)
{

   var property = name.substring(0,name.indexOf("."));

      document.forms[0].elements[property + "." + field2].value = true;
      disableButtons();
      enableInputControls();
      document.forms[0].submit();




}







function showGlCoaLookup(selectedAccount, selectedAccountDescription)
{

   window.open("glFindChartOfAccount.do?child=1&selectedAccount=" + selectedAccount + "&selectedAccountDescription=" + selectedAccountDescription,"glFindChartOfAccountChild","width=605,height=500,scrollbars,status");

   return false;

}

function showGlCoaLookupTxn(selectedAccount, selectedAccountDescription)
{

   window.open("glFindChartOfAccount.do?childTxn=1&selectedAccount=" + selectedAccount + "&selectedAccountDescription=" + selectedAccountDescription,"glFindChartOfAccountChildTxn","width=605,height=500,scrollbars,status");

   return false;

}

function showGlCoaLookupGrid(name, selectedAccount, selectedAccountDescription)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("glFindChartOfAccount.do?child=1&selectedAccount=" + property + "." + selectedAccount + "&selectedAccountDescription=" + property + "." + selectedAccountDescription,"glFindChartOfAccountChild","width=605,height=500,scrollbars,status");

   return false;

}



function showGlCoaLookupGridTxn(name, selectedAccount, selectedAccountDescription)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("glFindChartOfAccount.do?childTxn=1&selectedAccount=" + property + "." + selectedAccount + "&selectedAccountDescription=" + property + "." + selectedAccountDescription,"glFindChartOfAccountChildTxn","width=605,height=500,scrollbars,status");

   return false;

}


function showInvIiLookupGrid(name, selectedItemName, selectedItemDescription, selectedItemDefaultLocation, selectedItemEntered)
{
   var property = name.substring(0,name.indexOf("."));

   if (document.title.indexOf("AP Purchase Requisition") > 0) window.open("invFindItem.do?child=1&prType="+ document.forms[0].elements["prType"].value +"&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered  ,"invFindItemChild","width=605,height=500,scrollbars,status");
   else window.open("invFindItem.do?child=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showInvIiLookupGridCst(name, selectedItemName, selectedItemDescription, selectedItemDefaultLocation, selectedItemEntered)
{

   var property = name.substring(0,name.indexOf("."));
   var customer = document.forms[0].elements["customer"].value

   if (document.title.indexOf("AP Purchase Requisition") > 0) window.open("invFindItem.do?child=1&prType="+ document.forms[0].elements["prType"].value +"&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered  ,"invFindItemChild","width=605,height=500,scrollbars,status");
   else window.open("invFindItem.do?child=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered + "&customerEntered="+ customer,"invFindItemChild","width=605,height=500,scrollbars,status");
   
   return false;

}


function showScInvIiLookupGrid(name, selectedItemName, selectedItemDescription, selectedItemDefaultLocation, selectedItemEntered, bstDate)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("invFindItem.do?itemSchedule=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered + "&bstDate=" + bstDate,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showPrInvIiLookupGrid(name, selectedItemName, selectedItemDescription, selectedItemDefaultLocation, selectedItemEntered, prType)
{
   var property = name.substring(0,name.indexOf("."));

   window.open("invFindItem.do?child=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered + "&prType=" + prType,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}


function showArPrsnlPeLookupGrid(name, selectedIdNumber,  selectedPersonelEntered)
{
   var property = name.substring(0,name.indexOf("."));

   window.open("arFindPersonel.do?child=1&selectedIdNumber=" + property + "." + selectedIdNumber + "&selectedPersonelEntered=" + property + "." + selectedPersonelEntered ,"arFindPersonelChild","width=605,height=500,scrollbars,status");

   return false;

}



function showInvIiLookup(name, selectedItemName, selectedItemDescription, selectedItemEntered)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("invFindItem.do?childAssembly=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemEntered=" + property + "." + selectedItemEntered,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showArMlLookupGrid(name)
{

   var property = name.substring(0,name.indexOf("."));

   window.open("arStandardMemoLines.do?child=1&selectedItemName=" + property + ".memoLine&sourceType=", "invFindMemoLinesChild","width=605,height=500,scrollbars,status");

   return false;

}


function showArMlLookupGridwithType(name,sourceType)
{

   var property = name.substring(0,name.indexOf("."));

   window.open("arStandardMemoLines.do?child=1&selectedItemName=" + property + ".memoLine&sourceType=" + sourceType, "invFindMemoLinesChild","width=605,height=500,scrollbars,status");

   return false;

}


function showArCstLookupGridwithType(name,sourceType)
{

   var property = name.substring(0,name.indexOf("."));

   window.open("arFindCustomer.do?child=1&selectedLocation=" + property + "&sourceType=" + sourceType, "arFindCustomerChild","width=605,height=500,scrollbars,status");

   return false;

}


function enterSubmit(e, element) {

   var key = (e.which)?e.which:event.keyCode;
   if (key == 13 && document.activeElement.type != 'submit' && document.activeElement.type != 'image') {

       var isFound = false;

	   for (var i=0; i<element.length; i++) {

	       if (document.getElementById(element[i]) != null && document.getElementById(element[i]).disabled == false) {

		       document.getElementById(element[i]).click();
		       isFound = true;
		       break;

		   }

	   }

	   if (isFound == true) return false;

   }

   return true;
}

function showApSplLookup(selectedSupplierCode, selectedSupplierName, selectedSupplierEntered)
{

   window.open("apFindSupplier.do?child=1&selectedSupplierCode=" + selectedSupplierCode + "&selectedSupplierName=" + selectedSupplierName + "&selectedSupplierEntered=" + selectedSupplierEntered,"apFindSupplierChild","width=605,height=500,scrollbars,status");

   return false;

}

function showInvItemLookup(selectedItemName, selectedItemDescription, selectedItemEntered)
{

   window.open("invFindItem.do?child=1&selectedItemName=" + selectedItemName + "&selectedItemDescription=" + selectedItemDescription + "&selectedItemEntered=" + selectedItemEntered,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showApSplLookupGrid(name,selectedSupplierCode, selectedSupplierName, selectedSupplierEntered)
{
	var property = name.substring(0,name.indexOf("."));
   window.open("apFindSupplier.do?child=1&selectedSupplierCode=" + property + "." + selectedSupplierCode + "&selectedSupplierName=" + property + "." + selectedSupplierName + "&selectedSupplierEntered=" + property + "." + selectedSupplierEntered,"apFindSupplierChild","width=605,height=500,scrollbars,status");

   return false;

}




function showArCstLookup(selectedCustomerCode, selectedCustomerName, selectedCustomerEntered)
{

   window.open("arFindCustomer.do?child=1&selectedCustomerCode=" + selectedCustomerCode + "&selectedCustomerName=" + selectedCustomerName + "&selectedCustomerEntered=" + selectedCustomerEntered + "&sourceType=","arFindCustomerChild","width=605,height=500,scrollbars,status");

   return false;

}

function showApVouLookup(selectedVoucherNumber)
{

   window.open("apFindVoucher.do?child=1&selectedVoucherNumber=" + selectedVoucherNumber,"apFindVoucherChild","width=605,height=500,scrollbars,status");

   return false;

}

function showApPoLookup(selectedPoNumber)
{

   window.open("apFindPurchaseOrder.do?child=1&selectedPoNumber=" + selectedPoNumber,"apFindPurchaseOrderChild","width=605,height=500,scrollbars,status");

   return false;

}


function showApPoByVouLookup(selectedPoNumber)
{
	
   window.open("apFindPurchaseOrder.do?child=1&isVoucher=1&selectedPoNumber=" + selectedPoNumber,"apFindPurchaseOrderChild","width=605,height=500,scrollbars,status");

   return false;

}

function showInvBoLookup(selectedBoNumber)
{

   window.open("invFindBuildUnbuildAssembly.do?child=1&selectedBoNumber=" + selectedBoNumber,"invFindBuildUnbuildAssemblyChild","width=605,height=500,scrollbars,status");

   return false;

}


function showArInvLookup(selectedInvoiceNumber, selectedInvoiceType,selectedInvoiceEntered,typeOfSearch)
{

   window.open("arFindInvoice.do?child=1&selectedInvoiceNumber=" + selectedInvoiceNumber + "&selectedInvoiceType="+ selectedInvoiceType + "&selectedInvoiceEntered=" + selectedInvoiceEntered + "&typeOfSearch=" + typeOfSearch,"arFindInvoiceChild","width=605,height=500,scrollbars,status");

   return false;

}


function showInvIiAssemblyLookupGrid(name, selectedItemName, selectedItemDescription, selectedItemDefaultLocation, selectedItemEntered)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("invFindItem.do?assembly=1&selectedItemName=" + property + "." + selectedItemName + "&selectedItemDescription=" + property + "." + selectedItemDescription + "&selectedItemDefaultLocation=" + property + "." + selectedItemDefaultLocation + "&selectedItemEntered=" + property + "." + selectedItemEntered,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showArSoLookup(selectedSoNumber)
{
	window.open("arFindSalesOrder.do?child=1&selectedSoNumber=" + selectedSoNumber,"arFindSalesOrderChild","width=605,height=500,scrollbars,status");

	return false;

}

function showArJoLookup(selectedSoNumber)
{
	window.open("arFindJobOrder.do?child=1&selectedJoNumber=" + selectedSoNumber,"arFindJobOrderChild","width=605,height=500,scrollbars,status");

	return false;

}


function showArSoLookupForAdj(selectedSoNumber)
{
	window.open("arFindSalesOrder.do?child=1&selectedSoNumber=" + selectedSoNumber + "&cmFind=1","arFindSalesOrderChild","width=605,height=500,scrollbars,status");

	return false;

}


function doGetCaretPosition (oField)
{

	// Initialize
	var iCaretPos = 0;

	if (document.selection) {

		// Set focus on the element
		oField.focus ();

		// To get cursor position, get empty selection range
		var oSel = document.selection.createRange ();

		// Move selection start to 0 position
		oSel.moveStart ('character', -oField.value.length);

		// The caret position is selection length
		iCaretPos = oSel.text.length;
	}

	// Return results

	return (iCaretPos);
}

function doSetCaretPosition (oField, iCaretPos, isDecimal)
{

	if (document.selection) {

	// Set focus on the element
		oField.focus ();

	// Create empty selection range
		var oSel = document.selection.createRange ();

	// Move selection start and end to 0 position
		oSel.moveStart ('character', -oField.value.length);

	// Move selection start and end to desired position
		if(parseInt(isDecimal) == 0) {
			oSel.moveStart ('character', iCaretPos);
			oSel.moveEnd ('character', iCaretPos - oField.value.length);
		} else {
			oSel.moveStart ('character', iCaretPos);
			oSel.moveEnd ('character', iCaretPos - oField.value.length + 1);
		}

		oSel.select ();
	}
}

function formatAmount(name, e)
{


      var num = "";
      var caretPos = parseInt(doGetCaretPosition(document.forms[0].elements[name]));
      var currAmount = document.forms[0].elements[name].value;

      var key = (e.which)?e.which:event.keyCode;

      if((key >= 96 && key <= 105) || (key >= 48 && key <= 57)
	  	 		|| key == 110 || key == 190 || key == 8 || key == 46) {

	  	  //format whole number part
	  	  if((key >= 96 && key <= 105) || (key >= 48 && key <= 57)
	  	 		|| key == 8 || key == 46) {

	  	 	var currCommaCount = 0;
	  		for(var i=0; i<currAmount.length; i++) {

	  			if(!isNaN(currAmount.charAt(i)) || currAmount.charAt(i) == '.') {

		  			num = num + currAmount.charAt(i);

		  		}

		  		if(currAmount.charAt(i) == ',') {

		  			currCommaCount++;

		  		}
		  	}

		  	var wholePart = "";
		  	var decimalPart = "";

	  		if(num.indexOf(".") != -1) {

	  			wholePart = num.substring(0, num.indexOf("."));
	  			decimalPart = num.substring(num.indexOf("."), num.length);

	  		} else {

	  			wholePart = num;
	  			decimalPart = "";

	  		}

	  		var newCommaCount = 0;
	  		if(wholePart.length > 3 && wholePart.length <= 6) {

		  		wholePart = wholePart.substring(0, wholePart.length - 3) + "," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 1;

		  	} else if(wholePart.length > 6 && wholePart.length <= 9) {

		  		wholePart = wholePart.substring(0, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
			  				"," + wholePart.substring(wholePart.length - 3, wholePart.length);
			  	newCommaCount = 2;

		  	} else if(wholePart.length > 9 && wholePart.length <= 12) {

		  		wholePart = wholePart.substring(0, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
		  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) + "," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 3;

		  	} else if(wholePart.length > 12 && wholePart.length <= 15) {

		  		wholePart = wholePart.substring(0, wholePart.length - 12) + "," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) +
		  					"," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
		  					"," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 4;

		  	} else if(wholePart.length > 15 && wholePart.length <= 18) {

		  		wholePart = wholePart.substring(0, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
		  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
		  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 5;

			} else if(wholePart.length > 18 && wholePart.length <= 21) {

		  		wholePart = wholePart.substring(0, wholePart.length - 18) + "," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) +
		  					"," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) + "," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) +
		  					"," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
		  					"," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 6;

			} else if(wholePart.length > 21 && wholePart.length <= 24) {

		  		wholePart = wholePart.substring(0, wholePart.length - 21) + "," + wholePart.substring(wholePart.length - 21, wholePart.length - 18) +
		  					"," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
		  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
		  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 7;

			} else if(wholePart.length == 25) {

		  		wholePart = wholePart.substring(0, 1) + "," + wholePart.substring(1, wholePart.length - 21) + "," + wholePart.substring(wholePart.length - 21, wholePart.length - 18) +
		  					"," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
		  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
		  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);
		  		newCommaCount = 8;

			}

		  	var amount = "";
		  	amount = wholePart + decimalPart;

		  	document.forms[0].elements[name].value = amount.toString();

		  	//set caret position
	  		if(currCommaCount == newCommaCount)
		 		doSetCaretPosition (document.forms[0].elements[name], caretPos, 0);
		 	else if(newCommaCount-currCommaCount == 1)
		 		doSetCaretPosition (document.forms[0].elements[name], caretPos + 1, 0);
		 	else if(newCommaCount-currCommaCount == -1)
		 		doSetCaretPosition (document.forms[0].elements[name], caretPos - 1, 0);

		 }

		 //this code places a '0' before the '.'
		 //if no digit was found before the '.'

		 if(currAmount.charAt(0) == '.' && key != 8 && key != 46) {

			currAmount = 0 + currAmount;
			caretPos++;

			document.forms[0].elements[name].value = currAmount;

		 	//set caret position
		 	doSetCaretPosition (document.forms[0].elements[name], caretPos, 1);

		 }

		 //format decimal part
		 if(key == 110 || key == 190 || (currAmount.indexOf(".") != -1 && caretPos > currAmount.indexOf("."))) {

		 	if(key == 110 || key == 190) {

		 		if(num.substring(num.indexOf(".") - 1, num.length).length == 0)
			 		//currAmount = currAmount + "00";
		 			currAmount = currAmount + "";
			}

		 	document.forms[0].elements[name].value = currAmount;

		 	//set caret position
		 	doSetCaretPosition (document.forms[0].elements[name], caretPos, 1);

		 }

	  }
}

function addZeroes(name) {

      var currAmount = document.forms[0].elements[name].value;

      if(currAmount != "" && currAmount.indexOf(".") == -1) {

      		document.forms[0].elements[name].value = currAmount + ".00";

      }

}

function formatDisabledAmount(num) {

		var wholePart = num.substring(0, num.indexOf("."));
		var decimal = num.substring(num.indexOf("."), num.length);

		if(wholePart.length > 3 && wholePart.length <= 6) {

	  		wholePart = wholePart.substring(0, wholePart.length - 3) + "," + wholePart.substring(wholePart.length - 3, wholePart.length);

	  	} else if(wholePart.length > 6 && wholePart.length <= 9) {

	  		wholePart = wholePart.substring(0, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
		  				"," + wholePart.substring(wholePart.length - 3, wholePart.length);

	  	} else if(wholePart.length > 9 && wholePart.length <= 12) {

	  		wholePart = wholePart.substring(0, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
	  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) + "," + wholePart.substring(wholePart.length - 3, wholePart.length);

	  	} else if(wholePart.length > 12 && wholePart.length <= 15) {

	  		wholePart = wholePart.substring(0, wholePart.length - 12) + "," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) +
	  					"," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
	  					"," + wholePart.substring(wholePart.length - 3, wholePart.length);

	  	} else if(wholePart.length > 15 && wholePart.length <= 18) {

	  		wholePart = wholePart.substring(0, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
	  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
	  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);

		} else if(wholePart.length > 18 && wholePart.length <= 21) {

	  		wholePart = wholePart.substring(0, wholePart.length - 18) + "," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) +
	  					"," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) + "," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) +
	  					"," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) + "," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +
	  					"," + wholePart.substring(wholePart.length - 3, wholePart.length);

		} else if(wholePart.length > 21 && wholePart.length <= 24) {

	  		wholePart = wholePart.substring(0, wholePart.length - 21) + "," + wholePart.substring(wholePart.length - 21, wholePart.length - 18) +
	  					"," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
	  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
	  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);

		} else if(wholePart.length == 25) {

	  		wholePart = wholePart.substring(0, 1) + "," + wholePart.substring(1, wholePart.length - 21) + "," + wholePart.substring(wholePart.length - 21, wholePart.length - 18) +
	  					"," + wholePart.substring(wholePart.length - 18, wholePart.length - 15) + "," + wholePart.substring(wholePart.length - 15, wholePart.length - 12) +
	  					"," + wholePart.substring(wholePart.length - 12, wholePart.length - 9) + "," + wholePart.substring(wholePart.length - 9, wholePart.length - 6) +
	  					"," + wholePart.substring(wholePart.length - 6, wholePart.length - 3) +	"," + wholePart.substring(wholePart.length - 3, wholePart.length);

		}

		return wholePart + decimal;

}

function enableSubjectToCommission() {

	if (document.forms[0].salesperson.value == "" || document.forms[0].salesperson.value == null) {
		document.forms[0].subjectToCommission.disabled=true;
		document.forms[0].subjectToCommission.checked=false;
	} else {
		document.forms[0].subjectToCommission.disabled=false;
	}

}

function showGlFindSegmentLookup(name, selectedValueSetName, selectedValueSetValueDescription)
{

	var property = name.substring(0,name.indexOf("."));
	var selectedCoaSegmentNumber = property.substring(name.indexOf("[")+1, name.indexOf("]"));
	var genVsList =  property.substring(0,property.indexOf("["));
	var valueSetName = document.forms[0].elements[property + ".valueSetName"].value;
	var valueSetValueDescription = document.forms[0].elements[property + ".valueSetValueDescription"].value;
	var genVsListSize = parseInt(document.forms[0].elements["genVsListSize"].value);
	var path = "";
	var ctr = 0;

	for (ctr = 0; ctr < genVsListSize; ctr++) {

		var genVsListValueSetName = document.forms[0].elements[genVsList + "[" + ctr +"].valueSetName"].value;
		var genVsListValueSetValueDescription = document.forms[0].elements[genVsList + "[" + ctr +"].valueSetValueDescription"].value;

		if (genVsListValueSetValueDescription != null && genVsListValueSetValueDescription != ""){

			path = path + "&genVsList[" + ctr + "].valueSetValueName=" + genVsListValueSetName + "&genVsList[" + ctr + "].valueSetValueDescription=" + genVsListValueSetValueDescription;

		}

	}

	var glBCOAListSize = parseInt(document.forms[0].elements["glBCOAListSize"].value);

	for (ctr = 0; ctr < glBCOAListSize; ctr++) {

		if (document.forms[0].elements["glBCOAList[" + ctr + "].branchCheckbox"].checked == true ) {

			var bcoaCode = document.forms[0].elements["glBrCOAList[" + ctr + "].bcoaCode"].value;

			path = path + "&glBCOAList[" + ctr + "].bcoaCode=" + bcoaCode;

		}

	}

	window.open("glFindSegment.do?child=1&selectedCoaSegmentNumber=" + selectedCoaSegmentNumber + "&selectedValueSetName=" + property + "." + selectedValueSetName + "&selectedValueSetValueDescription=" +  property + "." + selectedValueSetValueDescription + "&valueSetName=" + valueSetName + "&valueSetValueDescription=" + valueSetValueDescription + "&genVsListSize=" + genVsListSize + "&glBCOAListSize=" + glBCOAListSize + path ,"glFindSegment","width=605,height=500,scrollbars,status");

 	return false;

}

function showGlFindSegmentLookupTxn(name, selectedValueSetName, selectedValueSetValueDescription)
{
	var property = name.substring(0,name.indexOf("."));
	var selectedCoaSegmentNumber = property.substring(name.indexOf("[")+1, name.indexOf("]"));
	var genVsList =  property.substring(0,property.indexOf("["));
	var valueSetName = document.forms[0].elements[property + ".valueSetName"].value;
	var valueSetValueDescription = document.forms[0].elements[property + ".valueSetValueDescription"].value;
	var genVsListSize = parseInt(document.forms[0].elements["genVsListSize"].value);
	var path = "";
	var ctr = 0;

	for (ctr = 0; ctr < genVsListSize; ctr++) {

		var genVsListValueSetName = document.forms[0].elements[genVsList + "[" + ctr +"].valueSetName"].value;
		var genVsListValueSetValueDescription = document.forms[0].elements[genVsList + "[" + ctr +"].valueSetValueDescription"].value;

		if (genVsListValueSetValueDescription != null && genVsListValueSetValueDescription != ""){

			path = path + "&genVsList[" + ctr + "].valueSetValueName=" + genVsListValueSetName + "&genVsList[" + ctr + "].valueSetValueDescription=" + genVsListValueSetValueDescription;

		}

	}

	window.open("glFindSegment.do?childTxn=1&selectedCoaSegmentNumber=" + selectedCoaSegmentNumber + "&selectedValueSetName=" + property + "." + selectedValueSetName + "&selectedValueSetValueDescription=" +  property + "." + selectedValueSetValueDescription + "&valueSetName=" + valueSetName + "&valueSetValueDescription=" + valueSetValueDescription + "&genVsListSize=" + genVsListSize + path ,"glFindSegment","width=605,height=500,scrollbars,status");

 	return false;

}

function showGlFindSegmentCoaGenLookup(selectedValueSetName, selectedValueSetValue, selectedValueSetValueDescription)
{

	var valueSetName = document.forms[0].elements[selectedValueSetName].value;
	var valueSetValueDescription = document.forms[0].elements[selectedValueSetValueDescription].value;

	window.open("glFindSegment.do?childCoaGen=1&selectedValueSetName=" + selectedValueSetName + "&selectedValueSetValueDescription=" + selectedValueSetValueDescription + "&valueSetName=" + valueSetName + "&valueSetValueDescription=" + valueSetValueDescription + "&selectedValueSetValue=" + selectedValueSetValue,"glFindSegment","width=605,height=500,scrollbars,status");

 	return false;

}

function showInvBSTLookup(selectedBSTCode, selectedTransferOutNumber, selectedTransferOutNumberEntered)
{
	/*
   	if (selectedBSTCode == "transferOutCode") {
	   	var selectedTransferOutNumber = selectedTransferNumber;
	   	var selectedTransferOutNumberEntered = selectedTransferNumberEntered;
	   	window.open("invFindBranchStockTransfer.do?child=1&selectedBSTCode=" + selectedBSTCode + "&selectedTransferOutNumber=" + selectedTransferOutNumber + "&selectedTransferOutNumberEntered=" + selectedTransferOutNumberEntered,"invFindBranchStockTransferChild","width=605,height=500,scrollbars,status");

   	}else {
	   	var selectedTransferOrderNumber = selectedTransferNumber;
	   	var selectedTransferOrderNumberEntered = selectedTransferNumberEntered;
	   	window.open("invFindBranchStockTransfer.do?child=1&selectedBSTCode=" + selectedBSTCode + "&selectedTransferOrderNumber=" + selectedTransferOrderNumber + "&selectedTransferOrderNumberEntered=" + selectedTransferOrderNumberEntered,"invFindBranchStockTransferChild","width=605,height=500,scrollbars,status");

   	}
   	*/
	window.open("invFindBranchStockTransfer.do?child=1&selectedBSTCode=" + selectedBSTCode + "&selectedTransferOutNumber=" + selectedTransferOutNumber + "&selectedTransferOutNumberEntered=" + selectedTransferOutNumberEntered,"invFindBranchStockTransferChild","width=605,height=500,scrollbars,status");

	return false;

}
function showInvBSTOrderLookup(selectedBSTCode, selectedTransferOrderNumber, selectedTransferOrderNumberEntered)
{

	window.open("invFindBranchStockTransfer.do?child=1&selectedBSTCode=" + selectedBSTCode + "&selectedTransferOrderNumber=" + selectedTransferOrderNumber + "&selectedTransferOrderNumberEntered=" + selectedTransferOrderNumberEntered,"invFindBranchStockTransferChild","width=605,height=500,scrollbars,status");

	return false;

}

function confirmJournalInterface()
{

   if(confirm("Editing this journal interface may cause discrepancy once posted. Are you sure you want to continue?")) return true;
   else return false;

}
function confirmGeneratePO()
{

   if(confirm("Are you sure you want to generate purchase order(s) and submit this document?")) return true;
   else return false;

}
function confirmSaveCanvass()
{

   if(confirm("Are you sure you want to submit this canvass?")) return true;
   else return false;

}
function confirmGenerateVoucher()
{

   if(confirm("Are you sure you want to generate voucher(s) and submit this document?")) return true;
   else return false;

}

function showInvIiLookupRep(selectedItemName, selectedItemDescription)
{

   var selectedBranch = "";
   var arRepBrSlsListSize = parseInt(document.forms[0].elements["arRepBrSlsListSize"].value);

   for (ctr = 0; ctr < arRepBrSlsListSize; ctr++) {

		if (document.forms[0].elements["arRepBrSlsList[" + ctr + "].branchCheckbox"].checked == true) {

			selectedBranch = document.forms[0].elements["arRepBrSlsList[" + ctr + "].branchCode"].value;
			break;

		}

   }

   window.open("invFindItem.do?childRep=1&selectedItemName=" + selectedItemName + "&selectedItemDescription=" + selectedItemDescription + "&selectedBranch=" + selectedBranch,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}

function showInvIiLookupRep2(selectedItemName, selectedItemDescription)
{

   var selectedBranch = "";
   var ctr = 0;

   while (document.forms[0].elements["invBrIlList[" + ctr + "].branchCheckbox"] != null) {

		if (document.forms[0].elements["invBrIlList[" + ctr + "].branchCheckbox"].checked == true) {

			selectedBranch = document.forms[0].elements["invBrIlList[" + ctr + "].branchCode"].value;

			break;

		}
		ctr++;

   }

   window.open("invFindItem.do?childRep=1&selectedItemName=" + selectedItemName + "&selectedItemDescription=" + selectedItemDescription + "&selectedBranch=" + selectedBranch,"invFindItemChild","width=605,height=500,scrollbars,status");

   return false;

}





function showPmPrjLookup(selectedProjectName, selectedProjectDescription)
{
   var property = name.substring(0,name.indexOf("."));
   window.open("pmFindProject.do?child=1&selectedProjectName=" + selectedProjectName + "&selectedProjectDescription=" + selectedProjectDescription ,"pmFindProjectChild","width=605,height=500,scrollbars,status");

   return false;

}