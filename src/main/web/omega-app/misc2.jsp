<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="miscItemEntry.title"/>
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>
<script Language="JavaScript" type="text/javascript">
<!-- Hide from non-JavaScript Browsers

function noenter(e) {
	//1. window is an object that represents an open window in a browser.
	//2. event is an action that can be detected by javascript.
	//Sometimes we want to execute a JavaScript when an event
	//occurs, such as when a user clicks a button.
	//3. Internet Explorer doesn't pass event to handler.
	//Instead you can use window.event object which is being
	//updated immediately after the event was fired.
	//So the crossbrowser way to handle events:
	//4. window is an object, event is a property that contains the
	//last event that took place.
	//5. The followig code means e argument is optional. So if you call the method
	//with no arguments it will use a default value of "window.event"
    e = e || window.event;
    var key = e.keyCode || e.charCode;
    //alert('e.type: ' + e.type + '; key: ' + key);
    return key !== 13;
}


  function initFormatAmount(name, event)
  {
    if(name=="fixedDiscountAmount"){
    	document.forms[0].elements["misc"].value = "0.00";

    }else{
    	document.forms[0].elements["fixedDiscountAmount"].value = "0.00";
    }

  	formatAmount(name, event);
  }



  function winLoad()
  {
	//alert("qwewee");
<%-- 	var miscStr = "<% out.print(request.getParameter("miscStr"));%>"; --%>
	/*
    if(miscStr!=""){
		miscStr = miscStr.substr(1);
		var ctr = Math.ceil(miscStr.substr(0,miscStr.indexOf("@")) * 1);

		miscStr = miscStr.substr(miscStr.indexOf("@")+1);

		for(i=0;i<ctr;i++){

			 var dateStr= miscStr.substr(0,miscStr.indexOf("$"))

			 if (dateStr=="null"){
			 	document.forms[0].elements["misc"+i].value = "";
			 }else{
			 	document.forms[0].elements["misc"+i].value = dateStr;
			 }

			 miscStr = miscStr.substr(miscStr.indexOf("$")+1);

		}*/

<%-- 		var miscCtr = "<% out.print(request.getParameter("miscCtr"));%>"; --%>

	//}
<%--     var property = "<% out.print(request.getParameter("property"));%>"; --%>


	
	
	var propertyName = "<% out.print(request.getParameter("propertyName"));%>";
	
	//alert(propertyName);
	
	//var test = window.opener.document.forms[0].elements[propertyName + ".serialNumber"].value;

	//alert(test);
	
     var custodian =  window.opener.document.forms[0].elements[propertyName + ".custodian"].value;
     var arrCustodian2 = "<% out.print(request.getParameter("custodian2"));%>";
     var isDisabled = "<% out.print(request.getParameter("isDisabled"));%>";

     var arrSpecs =  window.opener.document.forms[0].elements[propertyName + ".specs"].value;
     var arrPropertyCode = window.opener.document.forms[0].elements[propertyName + ".propertyCode"].value;
     var arrSerialNumber =  window.opener.document.forms[0].elements[propertyName + ".serialNumber"].value;
     var arrExpiryDate =  window.opener.document.forms[0].elements[propertyName + ".expiryDate"].value;
     var arrTgDocumentNumber = "<% out.print(request.getParameter("tgDocumentNumber"));%>";


   
     
     var custodian2 = arrCustodian2.split(",");
     var specs = arrSpecs.split(",");
     var propertyCode = arrPropertyCode.split(",");
     var serialNumber = arrSerialNumber.split(",");
     
   
     
     var expiryDate = arrExpiryDate.split(",");
     var tgDocumentNumber = arrTgDocumentNumber.split(",");
//     clean up custodian variable, remove brackets and spaces
     var str = custodian.replace("[","");
     str = str.replace("]","");
     str = str.replace(" ","");

//     set the cleaned up variable to arrayCustodian
     var arrayCustodian = str.split(',');

     var index = 0;
     var j=0;
     var miscCtr = "<% out.print(request.getParameter("miscCtr"));%>";
     for (j=0; j<arrayCustodian.length; j++) {

    	arrayCustodian[j] = arrayCustodian[j].replace(" ","");

     }

    while (true) {

   		if(specs != "" || propertyCode != "" || serialNumber != "" || expiryDate != ""){

	    	document.forms[0].elements["specs"+index].value = specs[index];
	    	document.forms[0].elements["serialNumber"+index].value = serialNumber[index];
	    	document.forms[0].elements["propertyCode"+index].value = propertyCode[index];
	    	document.forms[0].elements["expiryDate"+index].value = expiryDate[index];
	    	document.forms[0].elements["tgDocumentNumber"+index].value = tgDocumentNumber[index];

	    	if (specs[index]==undefined || specs[index]==null) document.forms[0].elements["specs"+index].value="";
	    	if (serialNumber[index]==undefined || serialNumber[index]==null) document.forms[0].elements["serialNumber"+index].value="";
	    	if (propertyCode[index]==undefined || propertyCode[index]==null) document.forms[0].elements["propertyCode"+index].value="";
	    	if (expiryDate[index]==undefined || expiryDate[index]==null) document.forms[0].elements["expiryDate"+index].value="";
	    	if (tgDocumentNumber[index]==undefined || tgDocumentNumber[index]==null) document.forms[0].elements["tgDocumentNumber"+index].value="";

    	}else{
    		document.forms[0].elements["specs"+index].value = "";
    		document.forms[0].elements["propertyCode"+index].value = "";
    		document.forms[0].elements["expiryDate"+index].value ="";
    		document.forms[0].elements["serialNumber"+index].value = "";
    		document.forms[0].elements["tgDocumentNumber"+index].value ="";
    	}
		
   		
   

    	if (isDisabled == "true"){

    		document.forms[0].elements["specs"+index].disabled = true;
    		document.forms[0].elements["propertyCode"+index].disabled = true;
    		document.forms[0].elements["expiryDate"+index].disabled = true;
    		document.forms[0].elements["serialNumber"+index].disabled = true;
    		document.forms[0].elements["custodian"+index].disabled = true;
    		document.forms[0].elements["applyToSucceedingButton"+index].disabled = true;
    	}


 	    if (++index == miscCtr) break;
    }


	 return false;

  }

  function returnMisc()
  {

		
	var propertyName = "<% out.print(request.getParameter("propertyName"));%>";

	//while(ctr<

	<%/*
		int ctr=0;
		if (request.getParameter("miscCtr")!=null) ctr = Integer.parseInt(request.getParameter("miscCtr"));
		out.print(Math.ceil(ctr));*/
	%>/*){
		if(document.forms[0].elements[elemName].value!=""){
		allDate = "" + allDate+ "$" +document.forms[0].elements[elemName].value;
		}else{
		allDate = "" + allDate+ "$" +null;
		}

		ctr++;
		elemName = "misc"+ctr;
	}
	finalDate="$" + ctr  + allDate + "$";*/
	var misc;
	var propertyCode = "";
	var serialNumber = "";
	var specs = "";
	var custodian = "";
	var expiryDate = "";
	var tgDocNum = "";
	var miscCtr = "<% out.print(request.getParameter("miscCtr"));%>";

	var ctr = 0;

	for (ctr = 0; ctr < miscCtr; ctr++) { 

		if(ctr>0){
			propertyCode +=",";
			serialNumber += ",";
			specs += ",";
			custodian += ",";
			expiryDate += ",";
		}
		
	
		var temppropertyCode = document.forms[0].elements["propertyCode"+ctr].value;
	
		var tempserialNumber = document.forms[0].elements["serialNumber"+ctr].value;
	
		var tempspecs = document.forms[0].elements["specs"+ctr].value;

		var tempcustodian = document.forms[0].elements["custodian"+ctr].value;

		var tempexpiryDate = document.forms[0].elements["expiryDate"+ctr].value;
		
	
		var lengthAll = temppropertyCode.trim().length + tempserialNumber.trim().length + tempspecs.trim().length + tempcustodian.trim().length + tempexpiryDate.trim().length;
		
		if(lengthAll<=0){
		
			continue;
		}
		
		
		
		
		propertyCode += temppropertyCode;
		serialNumber += tempserialNumber;
		specs +=tempspecs;
		custodian += tempcustodian;
		expiryDate += tempexpiryDate;
		
		
		
		
		
	}
	
	
	window.opener.document.forms[0].elements[propertyName + ".custodian"].value = custodian;
	
    window.opener.document.forms[0].elements[propertyName + ".specs"].value = specs;
  
    window.opener.document.forms[0].elements[propertyName + ".propertyCode"].value = propertyCode;

    window.opener.document.forms[0].elements[propertyName + ".serialNumber"].value = serialNumber;
    
    window.opener.document.forms[0].elements[propertyName + ".expiryDate"].value = expiryDate;
  
	
   

	//window.opener.document.forms[0].elements[window.opener.currProperty + ".misc"].value = misc;

	
	window.close();


	return false;
  }

  function applyToSucceeding(counter,max)
  {
  	var tbox = document.getElementById('myText'+counter).value;
	var custodian = document.getElementById('custodian'+counter).value;
	var specs = document.getElementById('specs'+counter).value;
	var propertyCode = document.getElementById('propertyCode'+counter).value;
	var expiryDate = document.getElementById('expiryDate'+counter).value;
	var serialNumber = document.getElementById('serialNumber'+counter).value;

	for(var ctr2 = counter+1; ctr2<max; ctr2++)
	{
	//	if(ctr2==counter)
		//{

			var txt = document.getElementById('myText'+ctr2);
			var txtSpecs = document.getElementById('specs'+ctr2);
			var txtPropertyCode = document.getElementById('propertyCode'+ctr2);
			var txtExpiryDate = document.getElementById('expiryDate'+ctr2);
			var txtCustodian = document.getElementById('custodian'+ctr2);
			var txtSerialNumber = document.getElementById('serialNumber'+ctr2);
			//window.alert(txt.value);
			txt.value = tbox ;
			txtSpecs.value = specs;
			txtPropertyCode.value = propertyCode;
			txtExpiryDate.value = expiryDate;
			txtSerialNumber.value = serialNumber;
			txtCustodian.value = custodian;
		//}
	}
  }




//Done Hiding-->
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onload="return winLoad();" onkeydown="return enterSubmit(event, new Array('okButton'));">
<form name="miscForm">
<table border="0" cellpadding="0" cellspacing="0" width="250" height="80">
      <tr valign="top">
        <td width="250" height="80">
          <table border="0" cellpadding="0" cellspacing="0" width="250" height="80"
	                               bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	         <tr>
		        <td width="250" height="1" colspan="2" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		            <bean:message key="miscItemEntry.title"/>
			    </td>
		    </tr>
			<input type="hidden" name="enableFields">
			<input type="hidden" name="okButton">
		    <tr>
	          <td width="150" height="25" colspan="2" class="statusBar">
	          </td>
	        </tr>
            <tr>

                <td width="120" height="25" class="prompt">
						<tr>
		          			<td width="120" height="25" class="prompt"></td>
		          			<td width="120" height="25" class="prompt">Property Code</td>
		          			<td width="130" height="25" class="prompt">Serial Number</td>
		          			<td width="130" height="25" class="prompt">Specs</td>
		          			<td width="130" height="25" class="prompt">Custodian</td>
		          			<td width="130" height="25" class="prompt">Expiry Date</td>
		          		</tr>
                <%
                	int count=0;
                	if (request.getParameter("miscCtr")!=null) count = Integer.parseInt(request.getParameter("miscCtr"));
					double c = Math.ceil(count);

					for(int i=0;i<c;i++){
		          		%>

			          	<tr>
							<td width="120" height="25" class="prompt">
			                   <%
			                   String a;
			                   a="Misc "+(i+1);
			                   out.println("<br />" + a);
			                   %>
			                </td>

			                <td width="130" height="25" class="control">
			                   <input type="text" id="propertyCode<% out.print(i); %>" name="propertyCode<% out.print(i); %>" maxlength="25" size="15" class="text" tabindex="<% out.print(i+1); %>" onkeypress="return noenter(event)">
			                </td>
			                <td width="130" height="25" class="control">
			                   <input type="text" id="serialNumber<% out.print(i); %>" name="serialNumber<% out.print(i); %>" maxlength="25" size="17" class="text" tabindex="<% out.print(i+1); %>"  onkeypress="return noenter(event)">
			                </td>

			                <td width="130" height="25" class="control">
			                   <textarea rows="4" cols="20" id="specs<% out.print(i); %>" name="specs<% out.print(i); %>" class="text" tabindex="<% out.print(i+1); %>"  onkeypress="return noenter(event)"></textarea>

			                </td>
			                <td width="130" height="25" class="control">
<%-- 			                   <input type="text" id="custodian<% out.print(i); %>" name="custodian<% out.print(i); %>" maxlength="25" size="15" class="text" tabindex="<% out.print(i+1); %>"> --%>
								<select id="custodian<% out.print(i); %>" name="custodian<% out.print(i); %>" maxlength="25" size="1" class="text" tabindex="<% out.print(i+1); %>"  onkeypress="return noenter(event)">

								</select>
			                </td>

			                <td width="130" height="25" class="control">
			                   <input type="text" id="expiryDate<% out.print(i); %>" name="expiryDate<% out.print(i); %>" maxlength="25" size="15" class="text" tabindex="<% out.print(i+1); %>"  onkeypress="return noenter(event)">
			                </td>

			                <td width="130" height="25" class="control">
 			                	<input type="button" name="applyToSucceedingButton<% out.print(i); %>" value="ApplyToSucceeding" onclick="applyToSucceeding(<% out.print(i); %>,<% out.print(count); %>)" class="mainButtonBig">

				       		</td>
				       		<td width="130" height="25" class="control">
			                   <input type="hidden" id="tgDocumentNumber<% out.print(i); %>" name="tgDocumentNumber<% out.print(i); %>" maxlength="25" size="15" class="textAmount" tabindex="<% out.print(i+1); %>">
			                </td>
				       		<td width="130" height="25" class="control">
			                   <input type="hidden" id="myText<% out.print(i); %>" name="misc<% out.print(i); %>" maxlength="25" size="15" class="textAmount" tabindex="<% out.print(i+1); %>">
			                </td>
			          	</tr>
		          		<%
		          	}

                %>
                </td>

            </tr>



            <tr>
	         <td width="400" height="50" colspan="4">
	           <div id="buttons">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" onclick="returnMisc();">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" onclick="window.close();">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
	           <div id="buttonsDisabled" style="display: none;">
	           <p align="right">
	           <html:submit property="okButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.ok"/>
		       </html:submit>
		       <html:submit property="cancelButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.cancel"/>
		       </html:submit>
	           </div>
			  </td>
	        </tr>
		  </table>
        </td>
      </tr>
</table>
</form>
<script language=JavaScript type=text/javascript>
  <!--
     var myObject = window.dialogArguments;

     if (myObject) {

	     document.forms[0].elements["misc"].value = myObject.misc;
		 document.forms[0].enableFields.value = myObject.enableFields;

		 if (document.forms[0].enableFields.value == "false") {

 	     	document.forms[0].misc.disabled=true;

 	     }

	 }

	       // -->
</script>
</body>
</html>
